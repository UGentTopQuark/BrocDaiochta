\documentclass{beamer}

% Setup appearance:

\usetheme{Gent}
\usecolortheme{lily} % Beamer color theme
%\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}


% Standard packages

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage{caption}
\usepackage{fancyvrb,relsize}
\usepackage{amsmath,amssymb}
\usepackage{epstopdf}
\usepackage{booktabs,array}
\usepackage{listings}
\usepackage{xspace}
\usepackage{fp}
\usepackage{ifthen}
\usepackage{graphics}
\usepackage{xcolor} 
\usepackage{calc} 


% Setup TikZ

\usepackage{tikz}
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]


% Author, Title, etc.

\title[Top Mass Measurement with the MEM] 
{%
  Top Mass Measurement with the Matrix Element Method%
}

\author[]
{
  Volker~Adler\inst{1} \and
  Anna~Cimmino\inst{1} \and
  Shannon~Crucy\inst{1} \and
  Guillaume~Garcia\inst{1} \and
  Martin~Grunewald\inst{1}\inst{2} \and
  Joseph~McCartin\inst{1}
  }

\institute[UGent and others]
{
  \inst{1}%
  Universiteit Gent
  \and
  \vskip-2mm
  \inst{2}%
  University College Dublin

}

\date[]
{2013 Gent Group Meeting}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%   BACKGROUND IMAGE    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\setbeamertemplate{background}{\includegraphics[height=\paperheight,width=1.15\paperwidth]{images/gradient-grey-light.png}}


% Text size settings
\newcommand{\gargantuan}{\fontsize{50}{55}\selectfont}
\newcommand{\vtiny}{\fontsize{5.5}{6.5}\selectfont}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%       CONTENT      %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The main document

\begin{document}

\begin{frame}
\mode<presentation>{\includegraphics[width=1.0\textwidth]{fig/logo/ugent_faculty_of_sciences.pdf}}
  \titlepage
\end{frame}

%\addtobeamertemplate{frametitle}{}{%
%\begin{textblock*}{100mm}(.85\textwidth,-1cm)
%\includegraphics[height=1cm,width=2cm]{fig/logo/ugent_blue.pdf}
%\end{textblock*}}

\addtobeamertemplate{frametitle}{}{%
\begin{tikzpicture}[remember picture,overlay]
\node[anchor=north east,yshift=-8pt] at (current page.north east) {\includegraphics[height=0.8cm]{fig/logo/ugent_blue.png}};
\end{tikzpicture}}

\begin{frame}{Presentation Outline}
  \tableofcontents
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction to the LHC and CMS Experiments}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Particles and Forces}

\begin{frame}{Particles and Forces of the Standard Model}
\begin{columns}
\column{0.6\textwidth}
  \begin{figure}
  	\includegraphics[height=0.50\textheight]{fig/intro/Standard_Model_of_Elementary_Particles.png}
  \end{figure}
\column{0.4\textwidth}
\end{columns}
\end{frame}

\begin{frame}{Particles and Forces of the Standard Model}
\begin{columns}
\column{0.6\textwidth}
  \begin{figure}
  	\includegraphics[height=0.50\textheight]{fig/intro/Standard_Model_of_Elementary_Particles.png}
  \end{figure}
\column{0.4\textwidth}
  \begin{figure}
  	\includegraphics[height=0.50\textheight]{fig/intro/john_ellis_sm_lagrangian_tshirt.jpg}
  \end{figure}
  \end{columns}
\end{frame}

\subsection{The Large Hadron Collider}

\begin{frame}{The Large Hadron Collider (LHC)}
\vspace{-0.8cm}
  \begin{figure}
  	\includegraphics[height=0.90\textheight]{fig/intro/LHC_and_experiments_aerial_photo.jpg}
  \end{figure}
\end{frame}

\begin{frame}{Introduction to the LHC and Experiments}
\vspace{-0.5cm}
  \begin{figure}
  	\includegraphics[height=0.99\textheight]{fig/intro/Cern-Accelerator-Complex.jpg}
  \end{figure}
\end{frame}

\subsection{The Compact Muon Solenoid (CMS) Detector}

\begin{frame}{The Compact Muon Solenoid (CMS) Detector}
\begin{columns}
\column{0.4\textwidth}
\vspace{-1.2cm}
  \begin{figure}
  	\includegraphics[height=0.3\textheight]{fig/intro/cms_detector_3d_small.jpg}
  \end{figure}
\column{0.4\textwidth}
\end{columns}
  \begin{figure}
  	\includegraphics[height=0.6\textheight]{fig/theory/CMS_Slice.png}
  \end{figure}
\end{frame}

\begin{frame}{}
\vspace{-0.2cm}
  \begin{figure}
  	\includegraphics[height=0.6\textheight]{fig/intro/gg-run177878-evt188723900-3d-with-barrel.jpg}
  \end{figure}
  \vspace{-0.4cm}
  \begin{figure}
  	\includegraphics[height=0.6\textheight]{fig/intro/eta_phi_PR040413_lhc_lrg.jpg}
  \end{figure}
\end{frame}

\begin{frame}{Typical Events at CMS}
\vspace{-0.5cm}
  \begin{figure}
  	\includegraphics[height=1.05\textheight]{fig/intro/hiY-run150887-event1792020-3D-highres.png}
  \end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction to the MEM}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The Top Quark}

\begin{frame}{Top Quark Decay at the LHC}
\begin{columns}
  \column{0.4\textwidth}
  \begin{figure}
  	\includegraphics[height=0.4\textheight]{fig/theory/top_quark_semileptonic_decay_wikipedia.pdf}
  \end{figure}
  \vspace{-0.5cm}
  \begin{figure}
  	\includegraphics[height=0.4\textheight]{fig/theory/top_pair_branching_frac.eps}
  \end{figure}
\column{0.6\textwidth}
	\begin{itemize}
		\item Produced predominantly by gluon-gluon fusion at the LHC.  
		\item Total of 776k $t\bar{t}$ pairs produced during the 2011 7TeV data-taking period (4.7fb$^{-1}$).
		\item For semi-leptonic decay, the event signature is 4 jets, one lepton ($e$ or $\mu$) and missing energy (MET).
		\item After selection (tight), 12k $t\bar{t} \to qqbb\mu\nu$ events remain.
	\end{itemize}
\end{columns}
\end{frame}

\begin{frame}{The Mass of the Top Quark}
\begin{columns}
  \column{0.5\textwidth}
  \begin{figure}
  	\includegraphics[height=0.4\textheight]{fig/theory/mw_mt_68CL_both.png}
  \end{figure}
\column{0.5\textwidth}
	\begin{itemize}
	\item Historically was used to constrain mass of the Higgs
	\item Heaviest SM particle, forms no bound state.
	\item $t\bar{t}$ pairs are a major background for several other analyses.
	\end{itemize}
\end{columns}
	\begin{itemize}
	\item Most top mass measurements have a large jet energy scale (JES) systematic uncertainty.
	\item The Matrix Element Method (MEM) solves this by doing a simultaneous measurement \emph{in situ} of the mass and JES.
	\item The MEM is the most precise method for measuring properties from a given sample of events!
	\end{itemize}
\end{frame}

\subsection{The Matrix Element Method}

\begin{frame}{An Event-Based Likelihood Function}
For a given event, reconstructed as $x_m$, the process likelihood $L$
is given as: \vspace{-0.2cm}
\begin{footnotesize}
\begin{columns}
\column{0.5\textwidth}
	\begin{multline} \label{eq:LikeTTBar}
	L_{t\bar t} (x_{m}; m_t ,S) = \frac{1}{{n_{jp}\sigma_{t\bar t} }}\sum\limits_{jp} \\
 \int\limits_{z1,z2} \sum\limits_{flavour} \textcolor{green}{{dz_1 dz_2 }f_{PDF} (z_1 )f_{PDF} (z_2)} \\
\int\limits_{x_p} \textcolor{blue}{d\sigma _{t\bar t}} \textcolor{red}{W(x_m,x_p,JES)}.
	\end{multline}
\column{0.5\textwidth}
\begin{figure}
	\includegraphics[height=0.31\textheight]{fig/theory/top_quark_semileptonic_decay_sections.png}
\end{figure}
\end{columns}
\end{footnotesize}
\vspace{0.5cm}
  \begin{itemize}
	\item \textcolor{green}{PDFs/Momentum Fraction of Incident Partons}
	\item \textcolor{blue}{Matrix Elements/Production of Decay Products}
	\item \textcolor{red}{Transfer Functions/Detector Smearing of Products}
  \end{itemize}
\end{frame}

\begin{frame}{Signal and Background Processes}
\begin{small}
An event likelihood is created by combining all process likelihoods under consideration that contribute to the final state.
\end{small} 
\vspace{0.2cm}
\begin{columns}
\column{0.5\textwidth}
  \begin{scriptsize}
  \; Signal (20 diagrams):
  \end{scriptsize}
\column{0.5\textwidth}
  \begin{scriptsize}
  Background, W+jets (3362 diagrams!):
  \end{scriptsize}
\end{columns}
\vspace{-0.9cm}
\begin{columns}
\column{0.5\textwidth}
  \begin{figure}
  	\includegraphics[height=0.45\textheight]{fig/theory/matrix_pp_ttbar_1.pdf}
  \end{figure}
\column{0.5\textwidth}
  \begin{figure}
  	\includegraphics[height=0.45\textheight]{fig/theory/matrix_pp_wjets_1.pdf}
  \end{figure}
\end{columns}
\vspace{-0.9cm}
\begin{columns}
\column{0.5\textwidth}
  \begin{figure}
  	\includegraphics[height=0.45\textheight]{fig/theory/matrix_pp_ttbar_2.pdf}
  \end{figure}
\column{0.5\textwidth}
  \begin{center}
	\dots
  \end{center}
\end{columns}
\vspace{-0.4cm}
\end{frame}

\begin{frame}{The Transfer Function}
\begin{scriptsize}
  \begin{itemize}
  	\item The energy of jets is the least well measured kinematic property. The mapping between partons and associated jets is modelled by a double Gaussian function in parton energy:
  	\begin{figure}
		\includegraphics[height=0.6\textheight]{fig/theory/transfer_functions_50-450GeV.png}
  	\end{figure}
  	\item Leptons are comparatively well-measured by the detector, and so no such transfer functions are used in the likelihood calculation.
  \end{itemize}
\end{scriptsize}
\end{frame}


\begin{frame}{The Sample Likelihood and Mass Extraction}\
\begin{small}
The sample likelihood for $N$ events is given as:
\begin{equation}\label{eq:LikeSample}
L_s (x_{i}, \dots, x_{N}; m_t , JES) = \prod\limits_{i =
1}^N {L_{evt} (x_i , m_t, JES)},
\end{equation}
For a given pool of events, sample likelihoods for a range of JES and mass hypotheses are calculated and then fitted with a parabola:
\end{small}
\vspace{-0.75cm}
\begin{figure}
	\includegraphics[height=0.8\textheight]{fig/theory/nllh_pseudoexp_0.pdf}
\end{figure}
\end{frame}

\begin{frame}{Acceptance}
The integrals in the likelihood calculation are evalutated over the entire phase space.  A bias in the measurement is possible if this is not taken into account when selecting events.
\begin{columns}
  \column{0.5\textwidth}
	\begin{small}
	Normalisation with acceptance:
	\begin{equation}
		\bar{L}_{evt} = \frac{1}{C} L_{evt} f_{acc}(x)
	\end{equation}
	When taking the sample likelihood, the acceptance reduces to:
	\begin{equation}
		\bar{f}_{acc} = \frac{N_{AcceptedEvents}}{N_{GeneratedEvents}}
	\end{equation}
	\end{small}
  \column{0.5\textwidth}
  	\begin{figure}
		\includegraphics[height=0.6\textheight]{fig/theory/parton_smearing_and_acceptance.png}
  	\end{figure}
\end{columns}
\end{frame}


\begin{frame}{Ensemble Testing}
Likelihood calculations take a long time!  Solution: use ensemble testing to get a better estimate of the uncertainty.
\begin{small}
\begin{itemize}
	\item A \emph{pseudo-experiment} is created, drawn from a fixed pool of events.
	\item Likelihoods are calculated for each hypothesis, and the mass is extracted.  
	\item The events are drawn with resampling for $n$ pseudo-experiments.
	\item Final measurement of the top mass and associated uncertainty is the combination of all pseudo-experiments, with a resampling correction added to the uncertainties of the averages and widths of the distributions.
\end{itemize}
\end{small}
\vspace{0.2cm}
\begin{equation}
N_{ind} = \frac{N_{pool}}{N_{evt}}, \sigma_{m_{t}} = \frac{\sigma_{m_t}}{\sqrt{N_{ind}}}
\end{equation}
\end{frame}

\subsection{Kinematic Fitting}

\begin{frame}{Jet Permutations and Motivations}
For every likelihood calculation, a sum over all possible jet-parton permutations must be made:
\begin{itemize}
\item Events with $n=4$ jets, no. possible permutations $n = 4! = 24$.  With effective use of b-tagging, $n = 4$. \\
\item Single mass point and JES performance test (10 events on UGent's Raichu HPC): \\
\vspace{0.2cm}
\begin{columns}
  	\column{0.45\textwidth}
		$\quad$ \emph{n Permutations = 4:}\\
		$\qquad$ 7h43m
	\column{0.45\textwidth}
		$\quad$ \emph{n Permutations = 0:}\\
		$\qquad$ 2h33m
\end{columns}
\vspace{0.2cm}
\item Could we use something to estimate the true jet permutation to help us with the computation time?
\end{itemize}
\end{frame}

\begin{frame}{Kinematic Fitting - The $\chi^2$ Function}
Construct a $\chi^2$ function based upon the measured and fitted event kinematics:
\begin{equation}\label{eq:KFchi2}
\chi^2 = (\mathbf{x}^f - \mathbf{x}^m)^T\mathbf{G}(\mathbf{x}^f - \mathbf{x}^m)
\end{equation}
And then minimise the function via the method of Lagrange Multipliers, constrained by the following requirements:
\begin{align}\label{eq:KFconstraints}
m_{l\nu}& = m_W \\
m_{q\bar{q}} & = m_W \\
m_t & = m_{\bar{t}}.
\end{align}
By ranking each possible jet-parton permutation in $\chi^2$, a best estimate for the permutation can be found.
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Validation of the MEM}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Validation of the MEM}
The validation of the MEM for top mass measurement is performed using parton-level generated events. A number of milestones must be reached:\\
\begin{itemize}
	\item The implementation of the method
	\item Smearing of events using a realistic detector transfer function
	\item Applying selection cuts and introducing an acceptance term
	\item Simultaneous mass and JES extraction	
\end{itemize}
For each case we check the robustness of the method.
\end{frame}

%%%%%%%%%%%
\subsection{Event Generation}
%%%%%%%%%%%
\setlength{\belowcaptionskip}{-8pt}
\begin{frame}{Signal events generation with \textsc{MadGraph}}
\scriptsize{$t\bar{t}\to\mu\nu jjb\bar{b}$ events are generated using \textsc{MadGraph5}, allowing closure tests of the method on simulated events:\\
\begin{itemize}
	\item The same Parton Density Function (PDF) and matrix elements used in the event generation are used in the likelihood calculation done with \textsc{MadWeight}\\
	\item 8 samples are generated, each one with a different top quark mass:\\
	\begin{itemize}
		\scriptsize{\item{163.5, 166.5, 169.5, 172.5, 175.5, 178.5, 181.5, 184.5}}\\
	\end{itemize}
	\item The JES is also varied for each mass point:\\
	\begin{itemize}
		\scriptsize{\item{0.80, 0.85, 0.90, 0.95, 1.00, 1.05, 1.10, 1.15, 1.20, 1.25}}\\
	\end{itemize}
	\item Each sample (80 in total) contains 100 000 events.
	\item We typically use $\sim 5000$ events per sample for likelihood calculations	
	\item More samples are generated than needed for likelihood calculations, in order to determine an accurate acceptance grid.
\end{itemize}}
\end{frame}


\begin{frame}{Smearing of the Jet $p_T$}
\scriptsize{The energyies of the jets are smeared to resemble detector effects:\\
\begin{itemize}
	\item Using a double Gaussian resolution function derived from a sample of events with full detector simulation.\\
	\item Smearing leads to differing jet transverse momentum ($p_T$) distributions.\\
\end{itemize}
\vspace{0.2cm}
This function is the Likelihood Transfer Function (TF).  We fit the smeared jets to ensure that we correctly get back the TF parameters for different slices of jet energies
\begin{figure}
	\includegraphics[width=0.8\textwidth]{fig/validation/parton_smearing.png}
\end{figure}}
\end{frame}

\subsection{Selection and Acceptance term}

\begin{frame}{Selection}
\scriptsize{A kinematic selection is applied on the smeared events :
\begin{itemize}
	\item $p_T^\mu > 26$ GeV, $|\eta^\mu| < 2.1$, $\Delta R(\mu,jet) > 0.3$
	\item $p_T^{jet} > 30$ GeV, $|\eta^{jet}| < 2.4$
\end{itemize}
\vspace{0.2cm}
Such cuts allow the reduction of background contributions, but a bias is introduced with such a phase space limitation
\vspace{-0.25cm}
\begin{figure}
	\includegraphics[width=0.28\textwidth]{fig/validation/F_jet1pT_noAcc.png}
	\includegraphics[width=0.28\textwidth]{fig/validation/F_jet2pT_noAcc.png}\\
	\includegraphics[width=0.28\textwidth]{fig/validation/F_jet1pT_Acc.png}
	\includegraphics[width=0.28\textwidth]{fig/validation/F_jet2pT_Acc.png}	
\end{figure}
}
\end{frame}

\begin{frame}{Acceptance term}
\scriptsize{
\begin{itemize}
	\item For each sample (mass, JES), we check how many events are selected/rejected by the selection
	\item A surface is fitted to the result
\end{itemize}
\begin{equation}
f_{acc} = a + b\times m_t^* 
\end{equation}
\begin{equation}
a = p_0 + p_1 \times JES^* + p_2\times{JES^*}^2 + p_3\times{JES^*}^3
\end{equation}
\begin{equation}
b = p_4 + p_5\times{JES^*} + p_6\times{JES^*}^2 + p_7\times{JES^*}^3
\end{equation}
\begin{equation}
m_t^* = m_t -172.5,  JES^* = JES - 1.00
\end{equation}
\begin{itemize}
	\item Parameters are extracted and we check the goodness of the fit
\end{itemize}
\begin{figure}
	\includegraphics[width=0.25\textwidth]{fig/validation/massCan.png}
	\includegraphics[width=0.25\textwidth]{fig/validation/jesCan.png}
	\includegraphics[width=0.25\textwidth]{fig/validation/pull_forAcc.png}
\end{figure}
$\to$parameters correctly describe acceptance measured at generator level}
\end{frame}



%%%%%%%%%%%
\subsection{Validation steps}
%%%%%%%%%%%

\begin{frame}{Likelihoods calculation}
\scriptsize{Samples likelihoods are calculated using \textsc{MadWeight}.  For each sample:	
	\begin{itemize}\scriptsize{
		\item 7 mass hypotheses, fixed $JES = 1.00$
		\item 2000 pseudo-experiments
		\item 1000 events per pseudo-experiment}
	\end{itemize}
\begin{figure}[ht]
	\captionsetup{font=scriptsize,labelfont=scriptsize}
	\includegraphics[width=0.5\textwidth]{fig/validation/F_exPseudoExpNoAcc.pdf}	
	\includegraphics[width=0.5\textwidth]{fig/validation/F_exPseudoExpWithAcc.pdf}
	\caption{Results for one single pseudo experiment, from the sample generated with $m_t = 166.5$ and $JES = 1.00$, without (left) and with (right) acceptance term applied.}
\end{figure}
\vspace{0.5cm}
$\to$ Acceptance term needed to correct the bias}
\end{frame}

\begin{frame}{Mass extraction (1)}
\scriptsize{Pseudo-experiments are run multiple times by randomly selecting a fixed number of events within the pool.
\begin{figure}
	\captionsetup{font=scriptsize,labelfont=scriptsize}
	\includegraphics[width=0.33\textwidth]{fig/validation/Mass_172_5.png}
	\includegraphics[width=0.33\textwidth]{fig/validation/MassPull_172_5.png}
	\includegraphics[width=0.33\textwidth]{fig/validation/MassRelErr_172_5.png}
	\caption{Results for the sample generated with $m_t =172.5$ and $JES = 1.00$, using 2000 pseudo-experiments, containing 1000 events each.}
\end{figure}
\vspace{0.5cm}
$\to$ Fitted mass is in agreement with input value}
\end{frame}

\begin{frame}{Mass extraction (2)}
\scriptsize{This procedure is done for all the samples with a different $m_t$ input value.\\
We check the quality of the measurement:
\begin{figure}
	\captionsetup{font=scriptsize,labelfont=scriptsize}
	\includegraphics[width=0.5\textwidth]{fig/validation/mass.png}
	\includegraphics[width=0.5\textwidth]{fig/validation/error.png}
	\caption{Extracted top mass for the samples generated with $m_t = 163.5,166.5,172.5,175.5,178.5,181.5$ GeV. Measured values and statistical errors on the measurements are shown.}
\end{figure}
\vspace{0.5cm}
$\to$ Fitted masses are in agreement with input values}
\end{frame}

\begin{frame}{Mass extraction (3)}
\scriptsize{We check an eventual bias between the measurement and the input value
\begin{figure}
	\captionsetup{font=scriptsize,labelfont=scriptsize}
	\includegraphics[width=0.5\textwidth]{fig/validation/pull.png}
	\includegraphics[width=0.5\textwidth]{fig/validation/pullwidth.png}
	\caption{Extracted top mass for the samples generated with $m_t = 163.5,166.5,172.5,175.5,178.5,181.5$ GeV. The pulls and pull widths for each measurement are shown}
\end{figure}
\vspace{0.5cm}
$\to$ Pull distributions are consistent with zero, errors are correctly estimated}
\end{frame}

\begin{frame}{Simultaneous Mass and JES extraction (1)}
\scriptsize{Samples likelihood are calculated using \textsc{MadWeight}
\begin{itemize}
	\item For each sample:	
	\begin{itemize}\scriptsize{
		\item 7 mass hypotheses
		\item 5 JES hypotheses
		\item 1000 pseudo-experiments
		\item 1000 events per pseudo-experiment}
	\end{itemize}
	\item On-going study, results are shown for the sample generated with $m_{top} = 172$ GeV and $JES=1.00$
\end{itemize}}
\begin{figure}
	\captionsetup{font=scriptsize,labelfont=scriptsize}
	\includegraphics[width=0.25\textwidth]{fig/validation/F_slice1.png}
	\includegraphics[width=0.25\textwidth]{fig/validation/F_slice2.png}
	\includegraphics[width=0.25\textwidth]{fig/validation/F_slice3.png}
	\includegraphics[width=0.25\textwidth]{fig/validation/F_slice4.png}
	\caption{1D projections, different slices are shown, the red curve is the 2D fit projection}
\end{figure}
\end{frame}

\begin{frame}{Simultaneous Mass and JES extraction (2)}
\scriptsize{A 2D-parabola is fitted, allowing to simultaneously extract $m_t$ and $JES$ values
\begin{figure}
	\captionsetup{font=scriptsize,labelfont=scriptsize}
	\includegraphics[width=0.7\textwidth]{fig/validation/F_exPseudoExpSignalOnlyMassJES.pdf}
	\includegraphics[width=0.25\textwidth]{fig/validation/JES_M_corr.pdf}\\
	\caption{Results shown for the sample generated with $m_t = 172.5$ and $JES = 1.00$}
\end{figure}
\vspace{0.5cm}
$\to$ Fitted mass and JES are in agreement with input values, no correlations observed on the fit parameters}
\end{frame}


% A verbatim example.  Leave this here, as it's very sensitive to get working, the fragile frame is important!
%\begin{frame}[fragile]{Example of Verbatim}
%  \begin{Verbatim}[fontsize=\relsize{-1},commandchars=\\\{\}] 
%\textcolor{green}{ example of green verbatim}
%\textcolor{red}{ example of red verbatim}
%  \end{Verbatim}
%\end{frame}

\subsection{Calibration and Measurement}

\begin{frame}{Calibration of the Method}
\begin{itemize}
	\item Once the method has been fully validated with generator-level simulated events, we must calibrate the method to events that have been simulated with all theoretical considerations in mind (Hard process, showering, propagation though and detection in the CMS detector).
	\item A properly calibrated method will allow us to remove any possible bias from a measurement performed on data taken with the CMS detector.
	\item Any uncertainties in the calibration will be propagated though, as a systematic uncertainty on the measurement.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Summary}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Summary}
\begin{itemize}
	\item We have implemented a powerful but complicated analysis method (the MEM)
	\item The MEM can be used to measure any parameters of the matrix element
	\item An overview of the method and some of the validation steps have been presented, for each step the method has been shown to be robust.
	\item Calibration of the method to detector-simulated data will now be undertaken, with a measurement on real data to follow.
\end{itemize}
\end{frame}

\end{document}
