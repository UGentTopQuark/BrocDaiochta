import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
#       'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_0_pat_tuple_226.root',
#       'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_1_pat_tuple_226.root'
      # 'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_6_pat_tuple_226.root',
      # 'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_7_pat_tuple_226.root',
      # 'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_8_pat_tuple_226.root',
      # 'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_9_pat_tuple_226.root',
   #   'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_89_pat_tuple_226.root',
      'file:///user/bklein/default_cmssw/CMSSW_3_3_6_patch5/src/create_pat/PATLayer1_Output.fromAOD_full.root',
 #     'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_88_pat_tuple_226.root',
 #     'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_87_pat_tuple_226.root'
#      'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_86_pat_tuple_226.root',
#      'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_85_pat_tuple_226.root',
#      'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_90_pat_tuple_226.root',
#      'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_91_pat_tuple_226.root',
#      'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_92_pat_tuple_226.root',
#    'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/wjets/job_101_pat_tuple_226.root',
#    'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/wjets/job_102_pat_tuple_226.root',
#    'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/wjets/job_103_pat_tuple_226.root',
#    'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/wjets/job_104_pat_tuple_226.root',
#    'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/wjets/job_105_pat_tuple_226.root',
#    'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/wjets/job_106_pat_tuple_226.root',
#    'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/wjets/job_107_pat_tuple_226.root',
#    'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/wjets/job_108_pat_tuple_226.root'
    )
)

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(300) )

process.produceNTuples = cms.EDAnalyzer('FlatNTuples',
    electronTag = cms.untracked.InputTag("selectedLayer1Electrons"),
    tauTag      = cms.untracked.InputTag("selectedLayer1Taus"),
    muonTag     = cms.untracked.InputTag("selectedLayer1Muons"),
    jetTag      = cms.untracked.InputTag("selectedLayer1Jets"),
    photonTag   = cms.untracked.InputTag("selectedLayer1Photons"),
    metTag      = cms.untracked.InputTag("layer1METs"),
    hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypGenMatch:Key"),
      semiLepTag      = cms.untracked.InputTag("ttSemiLepEvent"),
      HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT8E29' ),
      TriggerList      = cms.vstring("HLT_Ele15_LW_L1R","HLT_Mu9"),
      TriggerMenu      = cms.string("HLT8E29"),
      BTagAlgorithms	= cms.vstring("trackCountingHighEffBJetTags",
      				      "jetProbabilityBJetTags"),
	MCMatching	= cms.bool(True),
	outfile		= cms.string("ttbar.root")
)


process.p1 = cms.Path(process.produceNTuples)

process.schedule = cms.Schedule( process.p1 )
