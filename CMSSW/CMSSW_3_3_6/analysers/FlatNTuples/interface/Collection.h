#ifndef BEAG_COLLECTION_H
#define BEAG_COLLECTION_H

#include <DataFormats/Common/interface/Ref.h>
#include "DataFormats/Common/interface/View.h"
#include "TTree.h"
#include "TFile.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "Jet.h"
#include "Muon.h"
#include "Electron.h"
#include "MET.h"
#include "PtSorter.h"

namespace beag{
	template<class CMSSWObject, class BeagObject>
	class Collection{
		public:
			Collection(TTree *tree, TFile *outfile, std::string ident);
			~Collection();
			void fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle);
			void set_selection_list(std::vector<std::string> selection_list);
			void set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle);
			void set_mc_matching(bool match_mc);
			std::vector<BeagObject>* get_beag_objects();
		private:
			void fill_common_vars(typename edm::View<CMSSWObject>::const_iterator handle, BeagObject &beag_obj);
			void fill_special_vars(typename edm::View<CMSSWObject>::const_iterator handle, BeagObject &beag_obj);
			void match_trigger(typename edm::View<CMSSWObject>::const_iterator handle, BeagObject &beag_obj);
			void write_gen_info(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj);

			char* get_type_string();
			std::vector<BeagObject> *beag_objects;
			std::vector<std::string> selected_items;	// list of selected variables written to output
									// eg. list of b-tag algorithms

			edm::Handle<reco::BeamSpot> beamSpotHandle;
			PtSorter<BeagObject> *pt_sorter;
			TTree *tree;
			TFile *outfile;
			bool mc_matching;
	};
}

#endif
