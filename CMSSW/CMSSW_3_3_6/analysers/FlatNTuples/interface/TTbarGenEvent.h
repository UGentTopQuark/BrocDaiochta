#ifndef BEAG_TTBARGENEVENT_H
#define BEAG_TTBARGENEVENT_H

#include "Particle.h"
#include "Jet.h"
#include "Lepton.h"

#include "Rtypes.h"

namespace beag{
	class TTbarGenEvent{
		public:
			TTbarGenEvent():hadB_matched(false),lepB_matched(false),
				q_matched(false), qbar_matched(false), lep_matched(false),
				decay_channel(0){};
			virtual ~TTbarGenEvent(){};

			beag::Particle hadT, lepT, hadW, lepW, hadB, lepB, lep, neu, q, qbar;
			beag::Jet hadB_jet, lepB_jet, q_jet, qbar_jet;
			beag::Lepton reco_lep;
			bool hadB_matched, lepB_matched, q_matched, qbar_matched, lep_matched;
			char decay_channel;	// 0: background
						// 1: semi-lept electron
						// 2: semi-lept muon

		ClassDef(TTbarGenEvent, 1);
	};
}

#endif
