#ifndef BEAG_MUON_H
#define BEAG_MUON_H

#include "Lepton.h"
#include "Rtypes.h"

namespace beag{
	class Muon : public beag::Lepton{
		public:
			Muon():chi2(0),nHits(0){};
			virtual ~Muon(){};

			double chi2;
			double nHits;

		ClassDef(Muon, 1);
	};
}

#endif
