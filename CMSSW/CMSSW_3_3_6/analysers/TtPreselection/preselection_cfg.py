import FWCore.ParameterSet.Config as cms

process = cms.Process("preselection")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(#'dcap:///pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/0_PYTHIA6_TTbar_10TeV_cff_PAT_1.root')
    'dcap:///pnfs/iihe/cms/store/user/bklein/skims/Summer10MC_v1/Zjets_job_1423_TopTrigSkim.root'
    #'dcap:///pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/TrigSkim_v1/Data_muPD_2010BPR_job_94_TopTrigSkim.root'
    )
)

process.MessageLogger = cms.Service("MessageLogger")

process.preselection = cms.EDFilter("TtPreselection",
    electronTag = cms.untracked.InputTag("selectedPatElectrons"),
    tauTag      = cms.untracked.InputTag("selectedPatTaus"),
    muonTag     = cms.untracked.InputTag("selectedPatMuons"),
    jetTag      = cms.untracked.InputTag("selectedPatJets"),
    photonTag   = cms.untracked.InputTag("selectedPatPhotons"),
    metTag      = cms.untracked.InputTag("patMETs"),
    isMC	= cms.bool(True),
    HLTriggerResults = cms.InputTag( 'TriggerResults','','REDIGI' )
)

process.out = cms.OutputModule("PoolOutputModule",
	fileName = cms.untracked.string('preselection.root'),
	outputCommands = cms.untracked.vstring('keep *', 
		#'drop *_*_*_preselection'
		'drop TString_findTtSemiLepJetCombMVA_Method_*',
		'drop TtGenEvent_genEvt__*',
		'drop TtSemiLeptonicEvent_ttSemiLepEvent__*',
		'drop doubles_findTtSemiLepJetCombMVA_Discriminators_*',
		'drop doubles_kinFitTtSemiLepEventHypothesis_Chi2_*',
		'drop doubles_kinFitTtSemiLepEventHypothesis_Prob_*',
		'drop doubles_ttSemiLepJetPartonMatch_SumDR_*',
		'drop doubles_ttSemiLepJetPartonMatch_SumPt_*',
		'drop edmTriggerResults_TriggerResults__*',
		'drop int_ttSemiLepHypGenMatch_Key_*',
		'drop int_ttSemiLepHypGeom_Key_*',
		'drop int_ttSemiLepHypKinFit_Key_*',
		'drop int_ttSemiLepHypMVADisc_Key_*',
		'drop int_ttSemiLepHypMaxSumPtWMass_Key_*',
		'drop int_ttSemiLepHypWMassMaxSumPt_Key_*',
		'drop ints_kinFitTtSemiLepEventHypothesis_Status_*',
		'drop intss_findTtSemiLepJetCombMVA__*',
		'drop intss_kinFitTtSemiLepEventHypothesis__*',
		'drop intss_ttSemiLepJetPartonMatch__*',
		'drop patParticles_kinFitTtSemiLepEventHypothesis_*_*',
		'drop recoCompositeCandidateintsstdpairs_*_*_*',
		'drop recoGenParticles_decaySubset_*_*',
		'drop recoGenParticles_initSubset__*'
	)
)


process.p1 = cms.Path(process.preselection * process.out)

process.schedule = cms.Schedule( process.p1 )
