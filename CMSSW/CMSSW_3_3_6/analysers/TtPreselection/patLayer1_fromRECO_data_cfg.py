# This is an example PAT configuration showing the usage of PAT on minbias data

# Starting with a skeleton process which gets imported with the following line
import FWCore.ParameterSet.Config as cms

process = cms.Process("PAT")

## MessageLogger
process.load("FWCore.MessageLogger.MessageLogger_cfi")

## Options and Output Report
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

## Source
process.source = cms.Source("PoolSource",
#    fileNames = cms.untracked.vstring(
#        'file:///user/bklein/default_cmssw/CMSSW_3_3_6_patch5/src/em80to170_RECO.root'
#        'file:///user/bklein/default_cmssw/CMSSW_3_3_6_patch5/src/ttbar_RECO.root'
#    )
    fileNames = cms.untracked.vstring( __FILE_NAMES__ ),
    skipEvents = cms.untracked.uint32( __SKIP_EVENTS__ )
)
## Maximal Number of Events
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(__MAX_EVENTS__) )
#process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(200) )

## Geometry and Detector Conditions (needed for a few patTuple production steps)
process.load("Configuration.StandardSequences.Geometry_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
## global tag for 33X
process.GlobalTag.globaltag = cms.string('STARTUP31X_V1::All')
## global tag for 34X
## process.GlobalTag.globaltag = cms.string('STARTUP3XY_V9::All')
process.load("Configuration.StandardSequences.MagneticField_cff")

## Standard PAT Configuration File
process.load("PhysicsTools.PatAlgos.patSequences_cff")

from PhysicsTools.PatAlgos.tools.jetTools import *
print "*********************************************************************"
print "Switching all processes to use the anti-kT algorithm by default."
print "Switch the jet collection to your desired algorithm if this is not"
print "what you want to use. Note that L7Parton correction are taken from"
print "SC5 instead of AK5. This is an intermediate solution for the time "
print "being."
print "*********************************************************************"
switchJetCollection(process, 
                    cms.InputTag('ak5CaloJets'),   
                    doJTA            = True,
                    doBTagging       = True,            
                    jetCorrLabel     = ('AK5','Calo'),  
                    doType1MET       = True,            
                    genJetCollection = cms.InputTag("ak5GenJets")
                    ) 

from PhysicsTools.PatAlgos.tools.cmsswVersionTools import *

# run the 3.3.x software on Summer 09 MC from 3.1.x:
#   - change the name from "ak" (3.3.x) to "antikt) (3.1.x)
#   - run jet ID (not run in 3.1.x)
run33xOn31xMC( process,
               jetSrc = cms.InputTag("antikt5CaloJets"),
	       jetIdTag = "antikt5"
)

#from PhysicsTools.PatAlgos.tools.cmsswVersionTools import *

# run the 3.3.x software on Summer 09 MC from 3.1.x,
#   re-recoed with 3.3.2:
#     - Run GenJets for process in question
#run33xOnReRecoMC( process, "ak5GenJets" )

# Output Module Configuration (expects a path 'p')
from PhysicsTools.PatAlgos.patEventContent_cff import patEventContentNoLayer1Cleaning
from PhysicsTools.PatAlgos.patEventContent_cff import patExtraAodEventContent
patEventContentNoLayer1Cleaning.extend(patExtraAodEventContent)

process.out = cms.OutputModule("PoolOutputModule",
#                               fileName = cms.untracked.string( __OUTFILE__ ),
                               fileName = cms.untracked.string('PATLayer1_Output.fromAOD_full.root'),
                               # save only events passing the full path
                               SelectEvents   = cms.untracked.PSet( SelectEvents = cms.vstring('p') ),
                               # save PAT Layer 1 output; you need a '*' to
                               # unpack the list of commands 'patEventContent'
                               outputCommands = cms.untracked.vstring('drop *', *patEventContentNoLayer1Cleaning)
                               )
process.outpath = cms.EndPath(process.out)

from PhysicsTools.PatAlgos.tools.coreTools import *


#switch off new tau features introduced in 33X to restore 31X defaults
# new feaures: - shrinkingConeTaus instead of fixedCone ones
#              - TaNC discriminants attached for shrinkingConeTaus
#              - default preselection on cleaningLayer1
from PhysicsTools.PatAlgos.tools.tauTools import *
switchTo31Xdefaults(process)

## turn off MC matching for the process
#removeMCMatching(process, 'All')

process.preselection = cms.EDFilter("TtPreselection",
    electronTag = cms.untracked.InputTag("selectedLayer1Electrons"),
    tauTag      = cms.untracked.InputTag("selectedLayer1Taus"),
    muonTag     = cms.untracked.InputTag("selectedLayer1Muons"),
    jetTag      = cms.untracked.InputTag("selectedLayer1Jets"),
    photonTag   = cms.untracked.InputTag("selectedLayer1Photons"),
    metTag      = cms.untracked.InputTag("layer1METs"),
    semiLepTag      = cms.untracked.InputTag("ttSemiLepEvent"),
    hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypGenMatch:Key"),
    HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT8E29' ),
    datasetName = cms.untracked.string("Preselection")
)


# let it run
process.p = cms.Path(
    process.patDefaultSequence
    + process.preselection
    )


# Add the files for run 122314
#readFiles = cms.untracked.vstring()
#secFiles = cms.untracked.vstring()
