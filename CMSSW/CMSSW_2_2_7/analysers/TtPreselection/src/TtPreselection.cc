// -*- C++ -*-
//
// Package:    TtPreselection
// Class:      TtPreselection
// 
/**\class TtPreselection TtPreselection.cc analyzers/TtPreselection/src/TtPreselection.cc

 Description: <one line class summary>

 Implementation:
     <Notes on implementation>
*/
//
// Original Author:  local user
//         Created:  Wed Apr 22 14:23:46 CEST 2009
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDFilter.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"
#include "FWCore/ParameterSet/interface/InputTag.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"

#include "DataFormats/Candidate/interface/Candidate.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "analyzers/TtPreselection/interface/Cuts.h"
#include "analyzers/TtPreselection/interface/GenMatch.h"

#include "DataFormats/Common/interface/View.h"


//
// class declaration
//

class TtPreselection : public edm::EDFilter {
   public:
      explicit TtPreselection(const edm::ParameterSet&);
      ~TtPreselection();

   private:
      virtual void beginJob(const edm::EventSetup&) ;
      virtual bool filter(edm::Event&, const edm::EventSetup&);
      virtual void endJob() ;

        edm::InputTag eleLabel_;
        edm::InputTag muoLabel_;
        edm::InputTag jetLabel_;
        edm::InputTag tauLabel_;
        edm::InputTag metLabel_;
        edm::InputTag phoLabel_;
        edm::InputTag semiLepEvt_;
        edm::InputTag hypoClassKey_;
        edm::InputTag hlTriggerResults_;
  // Set dataset name in configuration file
        std::string datasetName_;
      
      // ----------member data ---------------------------
      Cuts *cuts;
      std::vector<double> *min_lep_pt;
      std::vector<double> *min_jet_pt;

      double is_signal_dataset;

      GenMatch *gen_match;

      std::vector<int> signal_ids;

      double accepted_counter;
      double rejected_counter;
      std::map<int, int> signal_accepted_counter;
      std::map<int, int> signal_rejected_counter;
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
TtPreselection::TtPreselection(const edm::ParameterSet& iConfig):
  eleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("electronTag")),
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  tauLabel_(iConfig.getUntrackedParameter<edm::InputTag>("tauTag")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  phoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("photonTag")),
  semiLepEvt_(iConfig.getUntrackedParameter<edm::InputTag>("semiLepTag")),
  hypoClassKey_(iConfig.getUntrackedParameter<edm::InputTag>("hypoClassKey")),
  hlTriggerResults_ (iConfig.getParameter<edm::InputTag> ("HLTriggerResults")),
  datasetName_(iConfig.getUntrackedParameter<std::string>("datasetName"))
{
	gen_match = new GenMatch();

	accepted_counter = 0;
	rejected_counter = 0;

	// electron and muon as signal channels
	signal_ids.push_back(11);
	signal_ids.push_back(13);

	for(std::vector<int>::iterator signal_id = signal_ids.begin();
	    signal_id != signal_ids.end();
	    ++signal_id){
		signal_accepted_counter[*signal_id] = 0;
		signal_rejected_counter[*signal_id] = 0;
	}

	is_signal_dataset = false;

   //now do what ever initialization is needed
	cuts = new Cuts("TtPreselection");

	min_lep_pt = new std::vector<double>();
	min_lep_pt->push_back(15);
	min_jet_pt = new std::vector<double>();
	min_jet_pt->push_back(25);

	cuts->set_min_njets(3);
	cuts->set_min_jet_pt(min_jet_pt);
	cuts->set_min_nisolated_lep(1);
//	cuts->set_max_nisolated_lep(1);
	cuts->set_min_e_pt(min_lep_pt);
	cuts->set_min_mu_pt(min_lep_pt);
	cuts->set_max_e_eta(2.6);
	cuts->set_max_mu_eta(2.6);
	cuts->set_max_jet_eta(2.6);
}


TtPreselection::~TtPreselection()
{
	delete gen_match;
	gen_match = NULL;
	delete cuts;
	cuts = NULL;
	delete min_lep_pt;
	min_lep_pt = NULL;
	delete min_jet_pt;
	min_jet_pt = NULL;
	
	std::cout << "-----------STATISTICS-----------" << std::endl;
	for(std::map<int,int>::iterator signal = signal_accepted_counter.begin();
	    signal != signal_accepted_counter.end();
	    ++signal){
		std::cout << "signal_accepted_" << signal->first << ": " << signal_accepted_counter[signal->first] << std::endl;
		std::cout << "signal_rejected_" << signal->first << ": " << signal_rejected_counter[signal->first] << std::endl;
	}
	std::cout << "accepted: " << accepted_counter << std::endl;
	std::cout << "rejected: " << rejected_counter << std::endl;
	std::cout << "----------END-STATISTICS---------" << std::endl;
}


//
// member functions
//

// ------------ method called on each new Event  ------------
bool
TtPreselection::filter(edm::Event& iEvent, const edm::EventSetup& iSetup)
{
        edm::Handle<edm::View<pat::Muon> > muons;
        iEvent.getByLabel(muoLabel_,muons);

        edm::Handle<edm::View<pat::Jet> > jets;
        iEvent.getByLabel(jetLabel_,jets);

        edm::Handle<edm::View<pat::Electron> > electrons;
        iEvent.getByLabel(eleLabel_,electrons);

        edm::Handle<edm::View<pat::MET> > mets;
        iEvent.getByLabel(metLabel_,mets);

        edm::Handle<edm::TriggerResults> HLTR;
        iEvent.getByLabel(hlTriggerResults_,HLTR);

        edm::Handle<reco::GenParticleCollection> genParticles;
        iEvent.getByLabel("genParticles", genParticles);

        gen_match->fill_events_from_collection(genParticles);

	bool cut_out = false;

	if(!is_signal_dataset && gen_match != NULL && gen_match->is_ok()){
		is_signal_dataset = true;
	}

	/*
	 *	cut on trigger information
	 *	(cut outside of Cuts because OR-relation necessary for
	 *	preselection between the triggers)
	 */
	if(HLTR.isValid()) {
		//HLT_QuadJet30 = 24, //HLT_Mu11 = 83,//HLT_Mu15 = 85,  //HLT_Ele_15__LW_L1R = 50
		if(!HLTR->accept(85) && !HLTR->accept(50)){
			cut_out = true;
		}
	}

	/*
	 *	cut on remaining cuts
	 */
	if(!cut_out){
		cuts->set_handles(muons, jets, electrons, mets);
		cut_out = cuts->cut();
	}

	if(is_signal_dataset && gen_match->is_ok() && gen_match->get_decay_mode() == 1){
		bool registered_event = false;
		for(std::vector<int>::iterator signal_id = signal_ids.begin();
		    signal_id != signal_ids.end();
		    ++signal_id){
			if( (gen_match->t_decay_is_leptonic() && abs(gen_match->get_particle("Wplus_dp1")->pdgId()) == *signal_id ) ||
		  	    (gen_match->tbar_decay_is_leptonic() && abs(gen_match->get_particle("Wminus_dp1")->pdgId()) == *signal_id) )
			{
				if(cut_out)
					signal_rejected_counter[*signal_id]++;
				else
					signal_accepted_counter[*signal_id]++;
				registered_event = true;
			}
		}
		if(!registered_event){
			if(cut_out)
				rejected_counter++;
			else
				accepted_counter++;
		}
	}else{
		if(cut_out)
			rejected_counter++;
		else
			accepted_counter++;
	}

	// return true if event PASSES cuts
	return !cut_out;
}

// ------------ method called once each job just before starting event loop  ------------
void 
TtPreselection::beginJob(const edm::EventSetup&)
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
TtPreselection::endJob() {
}

//define this as a plug-in
DEFINE_FWK_MODULE(TtPreselection);
