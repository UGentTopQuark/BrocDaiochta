#ifndef CUTS_H
#define CUTS_H

#include "DataFormats/Common/interface/View.h"
#include "FWCore/Framework/interface/Event.h"

#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Lepton.h"
#include "DataFormats/PatCandidates/interface/Jet.h"

#include "analyzers/TtPreselection/interface/LeptonSelector.h"
#include "analyzers/TtPreselection/interface/JetSelector.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Framework/interface/TriggerNames.h"


#include "Math/LorentzVector.h" 
#include "Math/VectorUtil.h" 

//for truth matching TQAF
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

#include <map>

class Cuts{
	public:
		Cuts(std::string ident);
		~Cuts();
		void set_handles(edm::Handle<edm::View<pat::Muon> > analyzer_muons,
                           edm::Handle<edm::View<pat::Jet> > analyzer_jets,
                           edm::Handle<edm::View<pat::Electron> > analyzer_electrons,
                           edm::Handle<edm::View<pat::MET> > analyzer_mets);
		bool cut();

		void set_min_mu_pt(std::vector<double> *min);
		void set_min_e_pt(std::vector<double> *min);
		void set_met_cut(double min);
		void set_min_jet_pt(std::vector<double> *min);
		void set_min_njets(int min);
		void set_min_mu_ht(double min);
		void set_min_e_ht(double min);
		void set_min_M3(double min);
		void set_min_nisolated_e(int n);
		void set_min_nisolated_mu(int n);
		void set_min_nisolated_lep(int n);
		void set_max_nisolated_e(int n);
		void set_max_nisolated_mu(int n);
		void set_max_nisolated_lep(int n);
		void set_max_e_trackiso(std::vector<double> *max);
		void set_max_e_caliso(std::vector<double> *max);
		void set_max_e_ecaliso(std::vector<double> *max);
		void set_max_e_hcaliso(std::vector<double> *max);
		void set_max_e_hcal_veto_cone(std::vector<double> *max);
		void set_max_e_ecal_veto_cone(std::vector<double> *max);
		void set_min_e_dR(std::vector<double> *min);
		void set_min_e_relIso(std::vector<double> *min);
		void set_min_e_nHits(std::vector<double> *min);
		void set_max_e_chi2(std::vector<double> *max);
		void set_max_e_d0(std::vector<double> *max);
		void set_e_electronID(std::vector<double> *eid);
		void set_max_mu_trackiso(std::vector<double> *max);
		void set_max_mu_caliso(std::vector<double> *max);
		void set_max_mu_ecaliso(std::vector<double> *max);
		void set_max_mu_hcaliso(std::vector<double> *max);
		void set_max_mu_hcal_veto_cone(std::vector<double> *max);
		void set_max_mu_ecal_veto_cone(std::vector<double> *max);
		void set_min_mu_dR(std::vector<double> *min);
		void set_min_mu_relIso(std::vector<double> *min);
		void set_min_mu_nHits(std::vector<double> *min);
		void set_max_mu_chi2(std::vector<double> *max);
		void set_max_mu_d0(std::vector<double> *max);
		void set_mu_electronID(std::vector<double> *eid);
		void set_max_jet_eta(double max);
		void set_max_mu_eta(double max);
		void set_max_e_eta(double max);
		void set_Z_rejection_width(double width);

		std::vector<double>* get_max_e_trackiso();
		std::vector<double>* get_max_e_caliso();
		std::vector<double>* get_max_e_ecaliso();
		std::vector<double>* get_max_e_hcaliso();
		std::vector<double>* get_max_e_hcal_veto_cone();
		std::vector<double>* get_max_e_ecal_veto_cone();
		std::vector<double>* get_min_e_dR();
		std::vector<double>* get_min_e_relIso();
		std::vector<double>* get_min_e_pt();
		std::vector<double>* get_e_electronID();
		std::vector<double>* get_max_e_d0();
		std::vector<double>* get_max_e_chi2();
		std::vector<double>* get_min_e_nHits();

		std::vector<double>* get_max_mu_trackiso();
		std::vector<double>* get_max_mu_caliso();
		std::vector<double>* get_max_mu_ecaliso();
		std::vector<double>* get_max_mu_hcaliso();
		std::vector<double>* get_max_mu_hcal_veto_cone();
		std::vector<double>* get_max_mu_ecal_veto_cone();
		std::vector<double>* get_min_mu_dR();
		std::vector<double>* get_min_mu_relIso();
		std::vector<double>* get_min_mu_pt();
		std::vector<double>* get_min_jet_pt();
		
		std::vector<double>* get_mu_electronID();
		std::vector<double>* get_max_mu_d0();
		std::vector<double>* get_max_mu_chi2();
		std::vector<double>* get_min_mu_nHits();
		double get_max_jet_eta();
		double get_max_e_eta();
		double get_max_mu_eta();

		void print_cuts();
		void print_cuts_vector(std::string id, std::vector<double> *cuts);
	
		
	private:

		typedef ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > MyLorentzVector;
 
		bool cut_M3();
		bool cut_mu_pt();
		bool cut_e_pt();
		bool cut_njets();
		bool cut_met();
		bool cut_mu_ht();
		bool cut_e_ht();
		bool cut_nisolated_electrons();
		bool cut_nisolated_muons();
		bool cut_nisolated_leptons();
		bool cut_Zrejection();
		bool cut_btag();
		bool cut_hltrigger();
		bool get_triggerInfo();
		//	template <class myPATClass> 
		//	bool cut_trigger(myPATClass patclass, int &min_trigmatch, std::string name_trigger);
		//
		double deltaR(MyLorentzVector p4_1,MyLorentzVector p4_2);

		edm::Handle<edm::View<pat::Electron> > electrons;
		edm::Handle<edm::View<pat::Jet> > uncut_jets;
//		edm::Handle<edm::View<pat::Jet> > jets;
		std::vector<pat::Jet>* jets;
		edm::Handle<edm::View<pat::Muon> > muons;
		edm::Handle<edm::View<pat::MET> > mets;
		std::vector<pat::Muon>* isolated_muons;
		std::vector<pat::Electron>* isolated_electrons;
		//for truth matching using TQAF:
		edm::Handle<TtGenEvent> genEvt;
		edm::Handle<TtSemiLeptonicEvent> semiLepEvt;
		edm::Handle<int> hypoClassKeyHandle;
		//for trigger information
		edm::Handle<edm::TriggerResults> HLTR;

		std::vector<double> *min_jet_pt;

		std::vector<double> *max_mu_trackiso;
		std::vector<double> *max_mu_hcaliso;
		std::vector<double> *max_mu_hcal_veto_cone;
		std::vector<double> *max_mu_ecal_veto_cone;
		std::vector<double> *max_mu_caliso;
		std::vector<double> *max_mu_ecaliso;
		std::vector<double> *min_mu_relIso;
		std::vector<double> *min_mu_dR;
		std::vector<double> *min_mu_pt;
		std::vector<double> *mu_electronID;
		std::vector<double> *min_mu_nHits;
		std::vector<double> *max_mu_d0;
		std::vector<double> *max_mu_chi2;

		std::vector<double> *max_e_trackiso;
		std::vector<double> *max_e_hcaliso;
		std::vector<double> *max_e_hcal_veto_cone;
		std::vector<double> *max_e_ecal_veto_cone;
		std::vector<double> *max_e_caliso;
		std::vector<double> *max_e_ecaliso;
		std::vector<double> *min_e_relIso;
		std::vector<double> *min_e_dR;
		std::vector<double> *min_e_pt;
		std::vector<double> *e_electronID;
		std::vector<double> *min_e_nHits;
		std::vector<double> *max_e_d0;
		std::vector<double> *max_e_chi2;

		double min_M3;
		double min_met;
		double min_mu_ht;
		double min_e_ht;
		int min_no_jets;
		int max_nisolated_e;
		int max_nisolated_mu;
		int max_nisolated_lep;
		int min_nisolated_e;
		int min_nisolated_mu;
		int min_nisolated_lep;
		double max_jet_eta;
		double max_e_eta;
		double max_mu_eta;
		double Z_rejection_width;
		//	int min_trigmatch_num;
		//	std::string name_trigger;
		
		int cuts_passed_counter;
		int cuts_not_passed_counter;
		double btag_start;
		double btag_cut[5];
		float eff[5], pur[5];
		float Stmpc[5],Stm[5], Bumpc[5]; 

		int ngen_events;int naccgen_mu15;int ntrigacc_mu15;int naccgen_mu11;int ntrigacc_mu11;
		int naccgen_e15;int ntrigacc_e15;


		bool already_cut; // avoid two calls of cut() for the same event
				  // reset for every set_handles() or set_min/max_xyz()
		bool already_cut_result; // value to be returned if cut() called twice
					 // for the same event

		std::string identifier;

		LeptonSelector<pat::Electron> *e_selector;
		LeptonSelector<pat::Muon> *mu_selector;
		JetSelector *jet_selector;
};

#endif
