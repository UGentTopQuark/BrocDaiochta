import FWCore.ParameterSet.Config as cms

process = cms.Process("eventselection")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
#       'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_0_pat_tuple_226.root',
#       'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_1_pat_tuple_226.root'
      # 'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_6_pat_tuple_226.root',
      # 'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_7_pat_tuple_226.root',
      # 'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_8_pat_tuple_226.root',
      # 'dcap://pnfs/iihe/cms/store/user/walsh/preselection_v5/ttbar/job_9_pat_tuple_226.root',
      'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_89_pat_tuple_226.root',
      'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_88_pat_tuple_226.root',
      'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v3/ttbar/job_87_pat_tuple_226.root',
      #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/ttbar/job_86_pat_tuple_226.root',
      #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/ttbar/job_85_pat_tuple_226.root',
     # 'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/ttbar/job_90_pat_tuple_226.root',
     # 'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/ttbar/job_91_pat_tuple_226.root',
     # 'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/ttbar/job_92_pat_tuple_226.root'
    #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/wjets/job_101_pat_tuple_226.root',
    #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/wjets/job_102_pat_tuple_226.root',
    #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/wjets/job_103_pat_tuple_226.root',
    #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/wjets/job_104_pat_tuple_226.root',
    #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/wjets/job_105_pat_tuple_226.root',
    #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/wjets/job_106_pat_tuple_226.root',
    #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/wjets/job_107_pat_tuple_226.root',
    #'dcap://pnfs/iihe/cms/store/user/bklein/preselection_v4/wjets/job_108_pat_tuple_226.root'
    )
)


process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

  
process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttGenEvent_cff")
## enable additional per-event printout from the TtSemiLeptonicEvent
#process.ttSemiLepEvent.verbosity = 1
## change maximum number of jets taken into account per event (default: 4)
#process.ttSemiLepEvent.maxNJets = 5
process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttSemiLepEvtBuilder_cff")
#process.ttSemiLepHypGenMatch.leps = "selectedLayer1Electrons"

# process.load( "HLTrigger.HLTcore.hltEventAnalyzerAOD_cfi" )
## process.hltEventAnalyzerAOD.triggerName = cms.string("HLT_Mu11")
# process.load( "HLTrigger.HLTcore.triggerSummaryAnalyzerAOD_cfi" )

#Run trigReport to get information on all triggers run and passed for this job in stdout
process.load( "HLTrigger.HLTanalyzers.hlTrigReport_cfi" )

 #process.p1 = cms.Path(
 #    process.hltEventAnalyzerAOD       +
 ###    process.triggerSummaryAnalyzerAOD
 #)



process.MessageLogger = cms.Service("MessageLogger")

process.eventselection = cms.EDAnalyzer("EventSelection",
    electronTag = cms.untracked.InputTag("selectedLayer1Electrons"),
    tauTag      = cms.untracked.InputTag("selectedLayer1Taus"),
    muonTag     = cms.untracked.InputTag("selectedLayer1Muons"),
    jetTag      = cms.untracked.InputTag("selectedLayer1Jets"),
    photonTag   = cms.untracked.InputTag("selectedLayer1Photons"),
    metTag      = cms.untracked.InputTag("selectedLayer1METs"),                          
    semiLepTag      = cms.untracked.InputTag("ttSemiLepEvent"),
    hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypGenMatch:Key"),
    HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT' ),                               
    #hlTriggerTag    =cms.untracked.InputTag("HLT"),                                
    datasetName = cms.untracked.string("TTbar"),
    tprimeMass = cms.untracked.double(175.0)
                                  

)

process.TFileService = cms.Service("TFileService", fileName = cms.string('tprime.root') )

process.p0 = cms.Path(process.makeGenEvt * process.makeTtSemiLepEvent)

process.p2 = cms.Path(process.eventselection)

# necessary for trigger report
process.p3 = cms.Path(process.hlTrigReport)

process.schedule = cms.Schedule( process.p0,
                                 #process.p1,
                                 process.p2,

				 # show trigger report
				 #process.p3 
				 )
