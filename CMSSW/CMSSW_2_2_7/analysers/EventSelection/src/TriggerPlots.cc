#include "analysers/EventSelection/interface/TriggerPlots.h"

TriggerPlots::TriggerPlots(std::string ident)
{
	id = ident;
	book_histos();

	reco_gen_match = NULL;
}

TriggerPlots::~TriggerPlots()
{
}

void TriggerPlots::set_hltr_handle(edm::Handle<edm::TriggerResults> HLTR)
{
	this->HLTR = HLTR;
}

void TriggerPlots::set_reco_gen_match(RecoGenMatch *reco_gen_match)
{
	this->reco_gen_match = reco_gen_match;
}

void TriggerPlots::set_muon_handle(edm::Handle<edm::View<pat::Muon> > muons)
{
	this->muons = muons;
}

void TriggerPlots::plot()
{
	plot_trigger();
	plot_trigger_muons();
}

void TriggerPlots::book_histos()
{
	edm::Service<TFileService> fs;

	//for plot_trigger_muons: what are we losing by requiring muon trigger match
	histos1d[("muons_triggered_checker"+id).c_str()]=fs->make<TH1D>(("muons_triggered_checker"+id).c_str(),"Explains what trigger matching is doing",6,0.5,6.5);
	histos2d[("muon_from_top_trigged_reco_pt_eta"+id).c_str()]=fs->make<TH2D>(("muon_from_top_trigged_reco_pt_eta"+id).c_str(),"eta vs pt of triggered top muons",100,-2.5,2.5,200,0,200);
	histos2d[("muon_from_top_nottrigged_reco_pt_eta"+id).c_str()]=fs->make<TH2D>(("muon_from_top_nottrigged_reco_pt_eta"+id).c_str(),"eta vs pt of top muons not triggeres",100,-2.5,2.5,200,0,200);
	histos2d[("muon_trig_eta_vs_reco_eta"+id).c_str()]=fs->make<TH2D>(("muon_trig_eta_vs_reco_eta"+id).c_str(),"trigger muon eta vs reco eta",100,-2.5,2.5,100,-2.5,2.5);
	histos2d[("muon_trig_pt_vs_reco_pt"+id).c_str()]=fs->make<TH2D>(("muon_trig_pt_vs_reco_pt"+id).c_str(),"trigger muon pt vs reco pt",200,0,200,200,0,200);
	histos2d[("muon_sel_trig_dR_vs_dPt"+id).c_str()]=fs->make<TH2D>(("muon_sel_trig_dR_vs_dPt"+id).c_str(),"selected_nontrig_muon-trig_muon dR vs dPt",210, 0, 7,400, -2,2);

	//trigger plots
	histos1d[("untrig_e15_pt"+id).c_str()]=fs->make<TH1D>(("untrig_e15_pt"+id).c_str(),"HLT_Ele15_LW_L1R electron pt",30,0,150);
	histos1d[("untrig_mu15_pt"+id).c_str()]=fs->make<TH1D>(("untrig_mu15_pt"+id).c_str(),"HLT_Mu15 muon pt",30,0,150);
	histos1d[("untrig_e15_eta"+id).c_str()]=fs->make<TH1D>(("untrig_e15_eta"+id).c_str(),"HLT_Ele15_LW_L1R electron eta",50,-2.5,2.5);
	histos1d[("untrig_mu15_eta"+id).c_str()]=fs->make<TH1D>(("untrig_mu15_eta"+id).c_str(),"HLT_Mu15 ",50,-2.5,2.5);

	//following plots to see if an event passed but no lepton within that event passed
	histos1d[("muon_event_triggered_lepton_not"+id).c_str()]=fs->make<TH1D>(("muon_event_triggered_lepton_not"+id).c_str(),"1 = event and lep triggered. 2 = event trig lep not",2,0.5,2.5);
	histos1d[("electron_event_triggered_lepton_not"+id).c_str()]=fs->make<TH1D>(("electron_event_triggered_lepton_not"+id).c_str(),"1 = event and lep triggered. 2 = event trig lep not",2,0.5,2.5);

	histos1d[("trig_mu15_pt"+id).c_str()]=fs->make<TH1D>(("trig_mu15_pt"+id).c_str(),"HLT_Mu15 muon pt",30,0,150);
	histos1d[("trig_e15_pt"+id).c_str()]=fs->make<TH1D>(("trig_e15_pt"+id).c_str(),"HLT_Ele15_LW_L1R electron pt",30,0,150);
	histos1d[("trig_mu15_eta"+id).c_str()]=fs->make<TH1D>(("trig_mu15_eta"+id).c_str(),"HLT_Mu15 muon eta",50,-2.5,2.5);
	histos1d[("trig_e15_eta"+id).c_str()]=fs->make<TH1D>(("trig_e15_eta"+id).c_str(),"HLT_Ele15_LW_L1R electron eta",50,-2.5,2.5);

	//Following CMS IN-2008/039 trigger note
	histos1d[("untrig_acc_e15_pt"+id).c_str()]=fs->make<TH1D>(("untrig_acc_e15_pt"+id).c_str(),"acc/gen HLT_Ele15_LW_L1R electron pt",30,0,150);
	histos1d[("untrig_acc_mu15_pt"+id).c_str()]=fs->make<TH1D>(("untrig_acc_mu15_pt"+id).c_str(),"acc/gen HLT_Mu15 muon pt",30,0,150);
	histos1d[("untrig_acc_e15_eta"+id).c_str()]=fs->make<TH1D>(("untrig_acc_e15_eta"+id).c_str(),"acc/gen HLT_Ele15_LW_L1R electron eta",50,-2.5,2.5);
	histos1d[("untrig_acc_mu15_eta"+id).c_str()]=fs->make<TH1D>(("untrig_acc_mu15_eta"+id).c_str(),"acc/gen HLT_Mu15 muon eta",50,-2.5,2.5);

	histos1d[("trig_acc_mu15_pt"+id).c_str()]=fs->make<TH1D>(("trig_acc_mu15_pt"+id).c_str(),"acc/gen HLT_Mu15 muon pt",30,0,150);
	histos1d[("trig_acc_e15_pt"+id).c_str()]=fs->make<TH1D>(("trig_acc_e15_pt"+id).c_str(),"acc/gen HLT_Ele15_LW_L1R electron pt",30,0,150);
	histos1d[("trig_acc_mu15_eta"+id).c_str()]=fs->make<TH1D>(("trig_acc_mu15_eta"+id).c_str(),"acc/gen HLT_Mu15 muon eta",50,-2.5,2.5);
	histos1d[("trig_acc_e15_eta"+id).c_str()]=fs->make<TH1D>(("trig_acc_e15_eta"+id).c_str(),"acc/gen HLT_Ele15_LW_L1R electron eta",50,-2.5,2.5);
}

/**************************************************Trigger*********************************************************/

//plot trigger efficiency vs eta||pt for pt_gen and pt_reco
void TriggerPlots::plot_trigger()
{ 
	//CMS IN-2008/039 = trig_acc plots
	//NOTE: untrig = all events (trig+untrig). untrig_acc = all events with at least one lepton passing acceptance cuts on gen level. trig = events with at least one lepton passing trigger.  
	int max_nmuons = isolated_muons->size(),max_nelectrons = isolated_electrons->size();
	int mu_acc_trig_id = -1,mu_trig_id  = -1,e_acc_trig_id  = -1, e_trig_id  = -1;
	std::string muon_trigger = "hltSingleMuNoIsoL3PreFiltered15";
	std::string electron_trigger = "hltL1NonIsoHLTNonIsoSingleElectronLWEt15TrackIsolFilter";

	if(muon_trigger == "hltSingleMuNoIsoL3PreFiltered15")
		{
			//step1: check if event passes. It not, take highest pt lepton and plot this vs pt/eta
			if (HLTR.isValid() && !(HLTR->accept(85)) && max_nmuons != 0) {
				//HLT_QuadJet30 = 24, //HLT_Mu11 = 83,//HLT_Mu15 = 85,  //HLT_Ele_15__LW_L1R = 50
				histos1d[("untrig_mu15_pt"+id).c_str()]->Fill((*isolated_muons)[0].pt());
				histos1d[("untrig_mu15_eta"+id).c_str()]->Fill((*isolated_muons)[0].eta());
				
				for(int i=0;i<max_nmuons;i++)
					{	

						if ((*isolated_muons)[i].genLepton() != NULL && fabs((*isolated_muons)[i].genLepton()->pdgId()) == 13 &&  (*isolated_muons)[i].genLepton()->pt() >= 15 &&  fabs((*isolated_muons)[i].genLepton()->pt()) > 2.1) 
							{
								histos1d[("untrig_acc_mu15_pt"+id).c_str()]->Fill((*isolated_muons)[i].pt());
								histos1d[("untrig_acc_mu15_eta"+id).c_str()]->Fill((*isolated_muons)[i].eta());
								break; 
							}
				}
				
			} 
			//step2: Find the lowest pt triggered lepton. 
			else if(max_nmuons != 0){
				std::pair<double,double> trig_pt_dR (0.0,0.0);
				for(int i=0;i<max_nmuons;i++)
					{
						bool lep_passed_acc_cuts = ((*isolated_muons)[i].genLepton() != NULL && fabs((*isolated_muons)[i].genLepton()->pdgId()) == 13 &&  (*isolated_muons)[i].genLepton()->pt() >= 15 &&  fabs((*isolated_muons)[i].genLepton()->pt()) > 2.1);	
					       
						if((*isolated_muons)[i].triggerMatchesByFilter(muon_trigger).size() != 0)
							{
								const std::vector<pat::TriggerPrimitive> &triggers = (*isolated_muons)[i].triggerMatchesByFilter(muon_trigger);
								double dR = ROOT::Math::VectorUtil::DeltaR(triggers[0].p4(),(*isolated_muons)[i].p4());

								//If one triggered lepton matches two reco chose closest in dR 
								if(trig_pt_dR.first != 0.0 && trig_pt_dR.first == triggers[0].pt())
									{
										std::cout << "In plot_trigger need to add check for mu_acc_trig_id if using standard trig efficiency definition" << std::endl;
										if (dR < trig_pt_dR.second) 
											{
											mu_trig_id = i;
											std::pair<double,double> trig_pt_dR (triggers[0].pt(),dR);
											
											if(lep_passed_acc_cuts)
												{
												mu_acc_trig_id = i;
												continue;
												}
											}
									}
								else //This will happen almost all of the time. Feel free to ignore above
 									{
										std::pair<double,double> trig_pt_dR (triggers[0].pt(),dR);
										mu_trig_id = i;
									    
										if(lep_passed_acc_cuts)
											{ 
												mu_acc_trig_id = i;	
											}
									}
							}
							
					}
				
				if (mu_trig_id != -1){
					histos1d[("muon_event_triggered_lepton_not"+id).c_str()]->Fill(1);
					histos1d[("untrig_mu15_pt"+id).c_str()]->Fill((*isolated_muons)[mu_trig_id].pt());
					histos1d[("untrig_mu15_eta"+id).c_str()]->Fill((*isolated_muons)[mu_trig_id].eta());
					histos1d[("trig_mu15_pt"+id).c_str()]->Fill((*isolated_muons)[mu_trig_id].pt());
					histos1d[("trig_mu15_eta"+id).c_str()]->Fill((*isolated_muons)[mu_trig_id].eta());
					if (mu_acc_trig_id != -1){
						histos1d[("untrig_acc_mu15_pt"+id).c_str()]->Fill((*isolated_muons)[mu_acc_trig_id].pt());
						histos1d[("untrig_acc_mu15_eta"+id).c_str()]->Fill((*isolated_muons)[mu_acc_trig_id].eta());
						histos1d[("trig_acc_mu15_pt"+id).c_str()]->Fill((*isolated_muons)[mu_acc_trig_id].pt());
						histos1d[("trig_acc_mu15_eta"+id).c_str()]->Fill((*isolated_muons)[mu_acc_trig_id].eta());
						
					}
				}
				else if (mu_trig_id == -1){
					histos1d[("muon_event_triggered_lepton_not"+id).c_str()]->Fill(2);
				}
			}
		}

	//Electron. step 1:check if event passes. It not, take highest pt lepton and plot this vs pt/eta
	if (HLTR.isValid() && !(HLTR->accept(50)) && max_nelectrons != 0){
		histos1d[("untrig_e15_pt"+id).c_str()]->Fill((*isolated_electrons)[0].pt());
		histos1d[("untrig_e15_eta"+id).c_str()]->Fill((*isolated_electrons)[0].eta());
		for(int i=0;i<max_nelectrons;i++)
			{
				if ((*isolated_electrons)[i].genLepton() != NULL && fabs((*isolated_electrons)[i].genLepton()->pdgId()) == 11 &&  (*isolated_electrons)[i].genLepton()->pt() >= 15 &&  fabs((*isolated_electrons)[i].genLepton()->pt()) > 2.1) 
					{
						histos1d[("untrig_acc_e15_pt"+id).c_str()]->Fill((*isolated_electrons)[i].pt());
						histos1d[("untrig_acc_e15_eta"+id).c_str()]->Fill((*isolated_electrons)[i].eta());
						break;
					}
			}
		
	}         
	//step2: Find the lowest pt triggered lepton. 
	else if (max_nelectrons != 0){
		std::pair<double,double> trig_pt_dR (0.0,0.0);
		for(int i=0;i<max_nelectrons;i++)
			{
				bool lep_passed_acc_cuts = ((*isolated_electrons)[i].genLepton() != NULL && fabs((*isolated_electrons)[i].genLepton()->pdgId()) == 11 &&  (*isolated_electrons)[i].genLepton()->pt() >= 15 &&  fabs((*isolated_electrons)[i].genLepton()->pt()) > 2.1);
	
				if((*isolated_electrons)[i].triggerMatchesByFilter(electron_trigger).size() != 0)
					{
						//If one triggered lepton matches two reco chose closest in dR 
						const std::vector<pat::TriggerPrimitive> &triggers = (*isolated_electrons)[i].triggerMatchesByFilter(electron_trigger);
						double dR = ROOT::Math::VectorUtil::DeltaR(triggers[0].p4(),(*isolated_electrons)[i].p4());

						//If one triggered lepton matches two reco chose closest in dR 
						if(trig_pt_dR.first != 0.0 && trig_pt_dR.first == triggers[0].pt())
							{
								std::cout << "In plot_trigger need to add check for e_acc_trig_id if using standard trig efficiency definition" << std::endl;
								if (dR < trig_pt_dR.second) 
									{
										e_trig_id = i;
										std::pair<double,double> trig_pt_dR (triggers[0].pt(),dR);
										
										if(lep_passed_acc_cuts)
											{
												e_acc_trig_id = i;
												continue;
											}
									}
							}
						else //This will happen almost all of the time. Feel free to ignore above
							{
								std::pair<double,double> trig_pt_dR (triggers[0].pt(),dR);
								e_trig_id = i;
								if (lep_passed_acc_cuts) 
									{
										e_acc_trig_id = i;
									}
							} 
					}
			}

		if (e_trig_id != -1){
			histos1d[("electron_event_triggered_lepton_not"+id).c_str()]->Fill(1);
			histos1d[("untrig_e15_pt"+id).c_str()]->Fill((*isolated_electrons)[e_trig_id].pt());
			histos1d[("untrig_e15_eta"+id).c_str()]->Fill((*isolated_electrons)[e_trig_id].eta());
			histos1d[("trig_e15_pt"+id).c_str()]->Fill((*isolated_electrons)[e_trig_id].pt());
			histos1d[("trig_e15_eta"+id).c_str()]->Fill((*isolated_electrons)[e_trig_id].eta());
			if (e_acc_trig_id != -1){
				histos1d[("untrig_acc_e15_pt"+id).c_str()]->Fill((*isolated_electrons)[e_acc_trig_id].pt());
				histos1d[("untrig_acc_e15_eta"+id).c_str()]->Fill((*isolated_electrons)[e_acc_trig_id].eta());
				histos1d[("trig_acc_e15_pt"+id).c_str()]->Fill((*isolated_electrons)[e_acc_trig_id].pt());
				histos1d[("trig_acc_e15_eta"+id).c_str()]->Fill((*isolated_electrons)[e_acc_trig_id].eta());
			}
		}
		else if(e_trig_id == -1)
			{
				histos1d[("electron_event_triggered_lepton_not"+id).c_str()]->Fill(2);
			}

	}
}

void TriggerPlots::plot_trigger_muons()
{
  	std::string muon_trigger = "hltSingleMuNoIsoL3PreFiltered11";
  	int top_mu_i = reco_gen_match->uncut_lepton_from_top("muon");

  	double min_dR = -1,dPtrel = -1;
	bool top_mu_reconstructed = false,top_mu_passed_sel = false, top_mu_trigged = false, non_top_mu_passed_trigged = false;
	int i=0;
	for(edm::View<pat::Muon>::const_iterator uncut_muon = muons->begin();uncut_muon != muons->end();++uncut_muon){
		if (i == top_mu_i){
			top_mu_reconstructed = true;
			int j=0;
			for(std::vector<pat::Muon>::iterator muon = isolated_muons->begin();
			    muon != isolated_muons->end();
			    ++muon){
				if (reco_gen_match->lepton_from_top(j,"muon")){
					top_mu_passed_sel = true;
					if(muon->triggerMatchesByFilter(muon_trigger).size() != 0)
						top_mu_trigged = true;
					
				}
				j++;
			}
		}
		i++;	
	}

	if (top_mu_passed_sel == false){
		for(std::vector<pat::Muon>::iterator muon = isolated_muons->begin();
		    muon != isolated_muons->end();
		    ++muon){
			if(muon->triggerMatchesByFilter(muon_trigger).size() != 0)
				non_top_mu_passed_trigged = true;
			
		}
	}

	if(top_mu_reconstructed && top_mu_passed_sel && top_mu_trigged)
		histos1d[("muons_triggered_checker"+id).c_str()]->Fill(1);
	else if(top_mu_reconstructed && top_mu_passed_sel && !top_mu_trigged)
		histos1d[("muons_triggered_checker"+id).c_str()]->Fill(2);
	else if(top_mu_reconstructed && !top_mu_passed_sel && non_top_mu_passed_trigged)
		histos1d[("muons_triggered_checker"+id).c_str()]->Fill(3);
	else if(top_mu_reconstructed && !top_mu_passed_sel && !non_top_mu_passed_trigged)
		histos1d[("muons_triggered_checker"+id).c_str()]->Fill(4);
	else if(!top_mu_reconstructed && !top_mu_passed_sel && non_top_mu_passed_trigged)
		histos1d[("muons_triggered_checker"+id).c_str()]->Fill(5);
	else if(!top_mu_reconstructed && !top_mu_passed_sel && !non_top_mu_passed_trigged)
		histos1d[("muons_triggered_checker"+id).c_str()]->Fill(6);


	if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
		if((*isolated_muons)[0].triggerMatchesByFilter(muon_trigger).size() == 0){
			for(edm::View<pat::Muon>::const_iterator uncut_muon = muons->begin();
			    uncut_muon != muons->end();
			    ++uncut_muon){
				if(uncut_muon->triggerMatchesByFilter(muon_trigger).size() != 0){
					double current_min_dR = ROOT::Math::VectorUtil::DeltaR(uncut_muon->p4(),(*isolated_muons)[0].p4());
					if(min_dR == -1 || current_min_dR < min_dR){
						min_dR = current_min_dR;
						dPtrel = (uncut_muon->pt() - (*isolated_muons)[0].pt())/(*isolated_muons)[0].pt();
					}
				}
			}
		histos2d[("muon_sel_trig_dR_vs_dPt"+id).c_str()]->Fill(min_dR,dPtrel);
		}
		
	}

	int lep_i = 0;
	for(std::vector<pat::Muon>::iterator muon = isolated_muons->begin();
	    muon != isolated_muons->end();
	    ++muon){
		
		const std::vector<pat::TriggerPrimitive> trig_muon =  muon->triggerMatchesByFilter(muon_trigger);
		if(!trig_muon.empty()){
			histos2d[("muon_trig_pt_vs_reco_pt"+id).c_str()]->Fill(trig_muon[0].pt(),muon->pt());
			histos2d[("muon_trig_eta_vs_reco_eta"+id).c_str()]->Fill(trig_muon[0].eta(),muon->eta());
			if(reco_gen_match->lepton_from_top(lep_i,"muon")){
			histos2d[("muon_from_top_trigged_reco_pt_eta"+id).c_str()]->Fill(muon->eta(),muon->pt());
			}

		}
		else if (reco_gen_match->lepton_from_top(lep_i,"muon")){
			histos2d[("muon_from_top_nottrigged_reco_pt_eta"+id).c_str()]->Fill(muon->eta(),muon->pt());

		}
		lep_i++;
	}
}
