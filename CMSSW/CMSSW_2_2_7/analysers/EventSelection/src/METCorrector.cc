#include "analysers/EventSelection/interface/METCorrector.h"

METCorrector::METCorrector()
{
	MEzCal = new MyMEzCalculator();
	mets = new std::vector<pat::MET>();
	uncorrected_mets = new std::vector<pat::MET>();
	corrected = false;
	mets = NULL;
	uncorrected_mets = NULL;
	correction_method = 1;
}

METCorrector::~METCorrector()
{
	if(MEzCal != NULL){
		delete MEzCal;
		MEzCal = NULL;
	}
	if(mets != uncorrected_mets && mets != NULL){
		delete mets;
		mets = NULL;
	}
	if(uncorrected_mets != NULL){
		delete uncorrected_mets;
		uncorrected_mets=NULL;
	}
}

void METCorrector::set_handles(edm::Handle<edm::View<pat::MET> > &mets_handle, pat::Particle *lepton, bool is_muon)
{
	corrected = false;
	if(mets != NULL && mets != uncorrected_mets)
		delete this->mets;
	if(uncorrected_mets != NULL)
		delete this->uncorrected_mets;
	uncorrected_mets = new std::vector<pat::MET>();

	this->lepton = lepton;
	this->mets_handle = mets_handle;
	this->is_muon = is_muon;

	prepare_uncorrected_MET();

	if(lepton != NULL){
		mets = new std::vector<pat::MET>();
		correct_MET();
		corrected=true;
	}else{
		corrected=false;
		mets = uncorrected_mets;
	}
}

bool METCorrector::is_corrected()
{
	return corrected;
}

void METCorrector::set_correction_method(int method)
{
	correction_method = method;
}

void METCorrector::prepare_uncorrected_MET()
{
	for(edm::View<pat::MET>::const_iterator met = mets_handle->begin();
		met != mets_handle->end();
		++met){
		uncorrected_mets->push_back(*met);
	}
}

void METCorrector::correct_MET()
{
	double p_z=0, p_z_solution1=0, p_z_solution2=0;
	for(edm::View<pat::MET>::const_iterator met = mets_handle->begin();
		met != mets_handle->end();
		++met){

		if(verbose){
			std::cout << "uncorrected MET: " << std::endl;
			std::cout << "uMET pt: " << met->pt() << std::endl;
			std::cout << "uMET et: " << met->et() << std::endl;
			std::cout << "uMET px: " << met->px() << std::endl;
			std::cout << "uMET py: " << met->py() << std::endl;
			std::cout << "uMET pz: " << met->pz() << std::endl;
			std::cout << "uMET mass: " << met->mass() << std::endl;
		}
		
		MEzCal->SetLepton(*lepton);
		MEzCal->SetMET(*met);
		p_z = MEzCal->Calculate(correction_method);
		p_z_solution1 = MEzCal->get_solution1();
		p_z_solution2 = MEzCal->get_solution2();
		pat::Particle::LorentzVector p(met->p4());
		p.SetPz(p_z);
		p.SetE(sqrt(p.px()*p.px() + p.py()*p.py() + p.pz()*p.pz()));
		pat::MET corrected_met;
		corrected_met.setP4(p);
		mets->push_back(corrected_met);

		pat::Particle::LorentzVector p2(met->p4());
		if(p_z == p_z_solution1)
			p2.SetPz(p_z_solution2);
		else{
			p2.SetPz(p_z_solution1);
		}
		p2.SetE(sqrt(p2.px()*p2.px() + p2.py()*p2.py() + p2.pz()*p2.pz()));

		pat::MET corrected_met2(*met);
		corrected_met2.setP4(p2);
		if(corrected_met2.pz() != 0){
			mets->push_back(corrected_met2);
		}

		if(verbose){
			std::cout << "corrected MET: " << std::endl;
			std::cout << "cMET pt: " << corrected_met.pt() << std::endl;
			std::cout << "cMET et: " << corrected_met.et() << std::endl;
			std::cout << "cMET px: " << corrected_met.px() << std::endl;
			std::cout << "cMET py: " << corrected_met.py() << std::endl;
			std::cout << "cMET pz: " << corrected_met.pz() << std::endl;
			std::cout << "cMET mass: " << corrected_met.mass() << std::endl;
			std::cout << "corrected MET (2nd solution): " << std::endl;
			std::cout << "cMET pt: " << corrected_met2.pt() << std::endl;
			std::cout << "cMET et: " << corrected_met2.et() << std::endl;
			std::cout << "cMET px: " << corrected_met2.px() << std::endl;
			std::cout << "cMET py: " << corrected_met2.py() << std::endl;
			std::cout << "cMET pz: " << corrected_met2.pz() << std::endl;
			std::cout << "cMET mass: " << corrected_met2.mass() << std::endl;
		}
	}
}

std::vector<pat::MET>* METCorrector::get_MET()
{
	return mets;
}

std::vector<pat::MET>* METCorrector::get_uncorrected_MET()
{
	return uncorrected_mets;
}
