#include "analysers/EventSelection/interface/Cuts.h"

Cuts::Cuts(std::string ident)
{
	identifier=ident;

	// initialise cuts
	min_M3=-1;
	min_mindiffM3=-1;
	min_no_jets=-1;
	max_no_jets=-1;
	min_met=-1;
	max_ht=-1;
	min_ht=-1;
	min_nisolated_lep=-1;
	max_nisolated_e=-1;
	max_nisolated_mu=-1;
	min_nisolated_e=-1;
	min_nisolated_mu=-1;
	max_jet_eta=-1;
	mu_type=-1;
	e_type=-1;
	Z_rejection_width=-1;
	num_name_btag = -1;
	name_btag = "-1";
	min_chi2 = -1;
	max_chi2 = -1;

	cuts_passed_counter=0;
	cuts_not_passed_counter=0;

	already_cut = false;


	jets = NULL;
	all_electrons = NULL;
	isolated_electrons = NULL;
	loose_electrons = NULL;
	isolated_muons = NULL;
	loose_muons = NULL;
	corrected_mets = NULL;
	uncorrected_mets = NULL;

	min_jet_pt = NULL;

	max_mu_trackiso = NULL;
	max_mu_ecaliso = NULL;
	max_mu_caliso = NULL;
	max_mu_hcaliso = NULL;
	min_mu_relIso = NULL;
	max_mu_hcal_veto_cone = NULL;
	max_mu_ecal_veto_cone = NULL;
	min_mu_dR = NULL;
	min_mu_pt = NULL;
	min_mu_et = NULL;
	max_mu_chi2 = NULL;
	max_mu_d0 = NULL;
	max_mu_d0sig = NULL;
	min_mu_nHits = NULL;
	mu_electronID = NULL;
	max_mu_eta=NULL;
	max_e_eta=NULL;

	max_e_trackiso = NULL;
	max_e_caliso = NULL;
	max_e_ecaliso = NULL;
	max_e_hcaliso = NULL;
	max_e_hcal_veto_cone = NULL;
	max_e_ecal_veto_cone = NULL;
	min_e_relIso = NULL;
	min_e_dR = NULL;
	min_e_pt = NULL;
	min_e_et = NULL;
	max_e_chi2 = NULL;
	max_e_d0 = NULL;
	max_e_d0sig = NULL;
	min_e_nHits = NULL;
	e_electronID = NULL;

	max_loose_mu_trackiso = NULL;
	max_loose_mu_ecaliso = NULL;
	max_loose_mu_caliso = NULL;
	max_loose_mu_hcaliso = NULL;
	min_loose_mu_relIso = NULL;
	max_loose_mu_hcal_veto_cone = NULL;
	max_loose_mu_ecal_veto_cone = NULL;
	min_loose_mu_dR = NULL;
	min_loose_mu_pt = NULL;
	min_loose_mu_et = NULL;
	max_loose_mu_chi2 = NULL;
	max_loose_mu_d0 = NULL;
	max_loose_mu_d0sig = NULL;
	min_loose_mu_nHits = NULL;
	loose_mu_electronID = NULL;
	max_loose_mu_eta=NULL;
	max_loose_e_eta=NULL;

	max_loose_e_trackiso = NULL;
	max_loose_e_caliso = NULL;
	max_loose_e_ecaliso = NULL;
	max_loose_e_hcaliso = NULL;
	max_loose_e_hcal_veto_cone = NULL;
	max_loose_e_ecal_veto_cone = NULL;
	min_loose_e_relIso = NULL;
	min_loose_e_dR = NULL;
	min_loose_e_pt = NULL;
	min_loose_e_et = NULL;
	max_loose_e_chi2 = NULL;
	max_loose_e_d0 = NULL;
	max_loose_e_d0sig = NULL;
	min_loose_e_nHits = NULL;
	loose_e_electronID = NULL;

	min_btag = NULL;
	trigger = NULL;

	double_mu_remover = NULL;
	double_e_remover = NULL;

	bjet_finder = new BJetFinder();
	all_e_selector = new LeptonSelector<pat::Electron>();
	e_selector = new LeptonSelector<pat::Electron>();
	mu_selector = new LeptonSelector<pat::Muon>();
	loose_e_selector = new LeptonSelector<pat::Electron>();
	loose_mu_selector = new LeptonSelector<pat::Muon>();
	jet_selector = new JetSelector();
	METCor = new METCorrector();
	mass_reco = new MassReconstruction();

	double_mu_remover = new DoubleCountedLeptonRemover<pat::Muon>();
	double_e_remover = new DoubleCountedLeptonRemover<pat::Electron>();

	ht_calc = new HtCalculator();

	// FIXME those vectors are never deleted
	std::vector<double> *e_eta = new std::vector<double>();
	e_eta->push_back(2.4);
	all_e_selector->set_max_eta(e_eta);
	std::vector<double> *e_pt = new std::vector<double>();
	e_pt->push_back(30.0);
	all_e_selector->set_min_et(e_pt);
//	std::vector<double> *e_eID = new std::vector<double>();
//	e_eID->push_back(1.0);
//	all_e_selector->set_electronID(e_eID);
	std::vector<double> *e_d0 = new std::vector<double>();
	e_d0->push_back(0.02);
	all_e_selector->set_max_d0(e_d0);
//	std::vector<double> *e_reliso = new std::vector<double>();
//	e_reliso->push_back(0.9090909);
//	all_e_selector->set_min_relIso(e_reliso);
}

Cuts::~Cuts()
{
	delete jet_selector;
	jet_selector=NULL;
	delete e_selector;
	e_selector=NULL;
	delete all_e_selector;
	all_e_selector=NULL;
	delete mu_selector;
	mu_selector=NULL;
	delete loose_e_selector;
	loose_e_selector=NULL;
	delete loose_mu_selector;
	loose_mu_selector=NULL;

	delete double_mu_remover;
	double_mu_remover = NULL;
	delete double_e_remover;
	double_e_remover = NULL;

	if(jets != NULL){
		delete jets;
		jets = NULL;
	}
	if(isolated_electrons != NULL){
		delete isolated_electrons;
		isolated_electrons = NULL;
	}
	if(all_electrons != NULL){
		delete all_electrons;
		all_electrons = NULL;
	}
	if(isolated_muons != NULL){
		delete isolated_muons;
		isolated_muons = NULL;
	}

	if(loose_electrons != NULL){
		delete loose_electrons;
		loose_electrons = NULL;
	}
	if(loose_muons != NULL){
		delete loose_muons;
		loose_muons = NULL;
	}

	delete mass_reco;
	mass_reco = NULL;

	if(bjet_finder != NULL){
		delete bjet_finder;
		bjet_finder = NULL;
	}

}



void Cuts::set_handles(edm::Handle<edm::View<pat::Muon> > analyzer_muons,
                           edm::Handle<edm::View<pat::Jet> > analyzer_jets,
                           edm::Handle<edm::View<pat::Electron> > analyzer_electrons,
                           edm::Handle<edm::View<pat::MET> > analyzer_mets,
			   edm::Handle<TtSemiLeptonicEvent> semiLepEvt,
			   edm::Handle<int> hypoClassKeyHandle,
		           edm::Handle<TtGenEvent> genEvt,
		           edm::Handle<edm::TriggerResults> HLTR,
			   edm::Handle<reco::BeamSpot> recoBeamSpotHandle)
{
	ht = -1;
	electrons = analyzer_electrons;
	muons = analyzer_muons;
	uncut_jets = analyzer_jets;
	mets = analyzer_mets;
	this->genEvt = genEvt;
	this->semiLepEvt = semiLepEvt;
	this->hypoClassKeyHandle = hypoClassKeyHandle;
	this->HLTR = HLTR;

	if(jets != NULL){
		delete jets;
		jets = NULL;
	}
	if(isolated_electrons != NULL){
		delete isolated_electrons;
		isolated_electrons = NULL;
	}
	isolated_electrons = e_selector->get_leptons(electrons, uncut_jets, recoBeamSpotHandle);

	if(all_electrons != NULL){
		delete all_electrons;
		all_electrons = NULL;
	}

	all_electrons = all_e_selector->get_leptons(electrons, uncut_jets, recoBeamSpotHandle);

	if(isolated_muons != NULL){
		delete isolated_muons;
		isolated_muons = NULL;
	}
	isolated_muons = mu_selector->get_leptons(muons, uncut_jets, recoBeamSpotHandle);

	if(loose_electrons != NULL){
		delete loose_electrons;
		loose_electrons = NULL;
	}
	loose_electrons = loose_e_selector->get_leptons(electrons, uncut_jets, recoBeamSpotHandle);

	if(loose_muons != NULL){
		delete loose_muons;
		loose_muons = NULL;
	}
	loose_muons = loose_mu_selector->get_leptons(muons, uncut_jets, recoBeamSpotHandle);

	// remove leptons that appear in loose as well as in tight lepton selection
//	double_mu_remover->set_loose_leptons(loose_muons);
//	double_mu_remover->set_tight_leptons(isolated_muons);
//	double_mu_remover->clean_loose_leptons();
//
//	double_e_remover->set_loose_leptons(loose_electrons);
//	double_e_remover->set_tight_leptons(isolated_electrons);
//	double_e_remover->clean_loose_leptons();

	jets = jet_selector->get_jets(analyzer_jets, all_electrons);

        if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
		pat::Particle *muon = new pat::Particle();
		muon->setP4((*isolated_muons)[0].p4());
                METCor->set_handles(analyzer_mets, muon);
                corrected_mets = METCor->get_MET();
		delete muon;
		muon = NULL;
        }
        else if(isolated_electrons->size() == 1 && isolated_muons->size() == 0){
		pat::Particle *electron = new pat::Particle();
		electron->setP4((*isolated_electrons)[0].p4());
                METCor->set_handles(analyzer_mets, electron);
                corrected_mets = METCor->get_MET();
		delete electron;
		electron = NULL;
        }else{
                METCor->set_handles(mets);
                corrected_mets = METCor->get_uncorrected_MET();
	}

        uncorrected_mets = METCor->get_uncorrected_MET();

	
	mass_reco->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	ht_calc->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	bjet_finder->set_handles(jets);

	already_cut = false;
}

bool Cuts::cut()
{
	bool cut = false;

	if(already_cut)
		return already_cut_result;
	
	// Call functions to apply cuts set
	if(trigger != NULL) cut = cut || cut_hltrigger();
	if(min_no_jets != -1 ||
	   max_no_jets != -1 ||
	   min_jet_pt->size() > 0 ||
	   max_jet_eta != -1)
		cut = cut || cut_njets();
	if((max_e_trackiso != NULL && max_e_trackiso->size() > 0) ||
	   (max_e_caliso != NULL && max_e_caliso->size() > 0) ||
	   (max_e_ecaliso != NULL && max_e_ecaliso->size() > 0) ||
	   (max_e_hcaliso != NULL && max_e_hcaliso->size() > 0) ||
	   (max_e_hcal_veto_cone != NULL && max_e_hcal_veto_cone->size() > 0) ||
	   (max_e_ecal_veto_cone != NULL && max_e_ecal_veto_cone->size() > 0) ||
	   (min_e_relIso != NULL && min_e_relIso->size() > 0) ||
	   (min_e_dR != NULL && min_e_dR->size() > 0) ||
	   max_nisolated_e != -1 ||
	   min_nisolated_e != -1 ||
	   max_e_eta != NULL ||
	   e_type != -1 ||
	   (max_e_chi2 != NULL && max_e_chi2->size() > 0) ||
	   (max_e_d0 != NULL && max_e_d0->size() > 0) ||
	   (max_e_d0sig != NULL && max_e_d0sig->size() > 0) ||
	   (min_e_nHits != NULL && min_e_nHits->size() > 0) ||
	   (e_electronID != NULL && e_electronID->size() > 0) ||
	   (min_e_et != NULL && min_e_et->size() > 0) ||
	   (min_e_pt != NULL && min_e_pt->size() > 0))
		cut = cut || cut_nisolated_electrons();
	if((max_loose_e_trackiso != NULL && max_loose_e_trackiso->size() > 0) ||
	   (max_loose_e_caliso != NULL && max_loose_e_caliso->size() > 0) ||
	   (max_loose_e_ecaliso != NULL && max_loose_e_ecaliso->size() > 0) ||
	   (max_loose_e_hcaliso != NULL && max_loose_e_hcaliso->size() > 0) ||
	   (max_loose_e_hcal_veto_cone != NULL && max_loose_e_hcal_veto_cone->size() > 0) ||
	   (max_loose_e_ecal_veto_cone != NULL && max_loose_e_ecal_veto_cone->size() > 0) ||
	   (min_loose_e_relIso != NULL && min_loose_e_relIso->size() > 0) ||
	   (min_loose_e_dR != NULL && min_loose_e_dR->size() > 0) ||
	   max_nloose_e != -1 ||
	   max_loose_e_eta != NULL ||
	   loose_e_type != -1 ||
	   (max_loose_e_chi2 != NULL && max_loose_e_chi2->size() > 0) ||
	   (max_loose_e_d0 != NULL && max_loose_e_d0->size() > 0) ||
	   (max_loose_e_d0sig != NULL && max_loose_e_d0sig->size() > 0) ||
	   (min_loose_e_nHits != NULL && min_loose_e_nHits->size() > 0) ||
	   (loose_e_electronID != NULL && loose_e_electronID->size() > 0) ||
	   (min_loose_e_et != NULL && min_loose_e_et->size() > 0) ||
	   (min_loose_e_pt != NULL && min_loose_e_pt->size() > 0))
		cut = cut || cut_nloose_electrons();
	if((max_loose_mu_trackiso != NULL && max_loose_mu_trackiso->size() > 0) ||
	   (max_loose_mu_caliso != NULL && max_loose_mu_caliso->size() > 0) ||
	   (max_loose_mu_ecaliso != NULL && max_loose_mu_ecaliso->size() > 0) ||
	   (max_loose_mu_hcaliso != NULL && max_loose_mu_hcaliso->size() > 0) ||
	   (max_loose_mu_hcal_veto_cone != NULL && max_loose_mu_hcal_veto_cone->size() > 0) ||
	   (max_loose_mu_ecal_veto_cone != NULL && max_loose_mu_ecal_veto_cone->size() > 0) ||
	   (min_loose_mu_relIso != NULL && min_loose_mu_relIso->size() > 0) ||
	   (min_loose_mu_dR != NULL && min_loose_mu_dR->size() > 0) ||
	   max_nloose_mu != -1 ||
	   max_loose_mu_eta != NULL ||
	   loose_mu_type != -1 ||
	   (max_loose_mu_chi2 != NULL && max_loose_mu_chi2->size() > 0) ||
	   (max_loose_mu_d0 != NULL && max_loose_mu_d0->size() > 0) ||
	   (max_loose_mu_d0sig != NULL && max_loose_mu_d0sig->size() > 0) ||
	   (min_loose_mu_nHits != NULL && min_loose_mu_nHits->size() > 0) ||
	   (loose_mu_electronID != NULL && loose_mu_electronID->size() > 0) ||
	   (min_loose_mu_et != NULL && min_loose_mu_et->size() > 0) ||
	   (min_loose_mu_pt != NULL && min_loose_mu_pt->size() > 0))
		cut = cut || cut_nloose_muons();
	if((max_e_trackiso != NULL && max_mu_trackiso->size() > 0) ||
	   (max_mu_caliso != NULL && max_mu_caliso->size() > 0) ||
	   (max_mu_ecaliso != NULL && max_mu_ecaliso->size() > 0) ||
	   (max_mu_hcaliso != NULL && max_mu_hcaliso->size() > 0) ||
	   (max_mu_hcal_veto_cone != NULL && max_mu_hcal_veto_cone->size() > 0) ||
	   (max_mu_ecal_veto_cone != NULL && max_mu_ecal_veto_cone->size() > 0) ||
	   (min_mu_relIso != NULL && min_mu_relIso->size() > 0) ||
	   (min_mu_dR != NULL && min_mu_dR->size() > 0) ||
           max_nisolated_mu != -1 ||
           min_nisolated_mu != -1 ||
	   max_mu_eta != NULL ||
	   mu_type != -1 ||
	   (max_mu_chi2 != NULL && max_mu_chi2->size() > 0) ||
	   (max_mu_d0 != NULL && max_mu_d0->size() > 0) ||
	   (max_mu_d0sig != NULL && max_mu_d0sig->size() > 0) ||
	   (min_mu_nHits != NULL && min_mu_nHits->size() > 0) ||
	   (mu_electronID != NULL && mu_electronID->size() > 0) ||
	   (min_mu_et != NULL && min_mu_et->size() > 0) ||
	   (min_mu_pt != NULL && min_mu_pt->size() > 0) )
		cut = cut || cut_nisolated_muons();
	if(min_nisolated_lep != -1) cut = cut || cut_nisolated_leptons();
	if(min_met != -1) cut = cut || cut_met();
	if(min_M3 != -1) cut = cut || cut_M3();
	if(min_mindiffM3 != -1) cut = cut || cut_mindiffM3();
	if(max_ht != -1) cut = cut || cut_max_ht();
	if(min_ht != -1) cut = cut || cut_min_ht();
	if(Z_rejection_width != -1) cut = cut || cut_Zrejection();
	if(min_btag != NULL && name_btag != "-1") cut = cut || cut_btag();
	if(min_chi2 != -1 || max_chi2 != -1) cut = cut || cut_chi2();
	//Should always be last because need to check if event passed selection
	cut = cut || get_triggerInfo();

	

	// count events that passed cuts and those that were discarded
	if(cut)
		cuts_not_passed_counter++;
	else
		cuts_passed_counter++;

	already_cut = true;
	already_cut_result = cut;
	// Returns false if event passed cuts, true if event should be discarded
	return cut;
}



// Apply cut for min pt and number of jets
bool Cuts::cut_njets()
{
	int njets=jets->size();
//	std::cout << "min_no_jets: " << min_no_jets << std::endl;
//	std::cout << "njets: " << njets << std::endl;
//	std::cout << "min_jet_pt_size: " << min_jet_pt->size() << std::endl;

	//if((njets < min_no_jets) || (njets < (int) min_jet_pt->size())){
	if(njets < min_no_jets || (njets > max_no_jets && max_no_jets >= 0)){
		//std::cout << "returning true" << std::endl;
		if(min_jet_pt->size() == 1 && (*min_jet_pt)[0] <= 0)
			return false;
		else
			return true;
	}
	else{
		//std::cout << "returning false" << std::endl;
		return false;
	}
}

bool Cuts::cut_mindiffM3()
{
	if(mass_reco->calculate_min_diff_M3() > min_mindiffM3)
		return false;
	else
		return true;
}

bool Cuts::cut_M3()
{
	if(mass_reco->calculate_M3() > min_M3)
		return false;
	else
		return true;
}

bool Cuts::cut_met()
{

	if(mets->size() < 1)
		return true;

        edm::View<pat::MET>::const_iterator met_iter = mets->begin();

	// FIXME: is this correct?
        if(met_iter->et() > min_met)
                return false;
        else
                return true;
}

bool Cuts::cut_chi2()
{
	if((min_chi2 != -1 && mass_reco->get_chi2() < min_chi2) || (max_chi2 != -1 && mass_reco->get_chi2() > max_chi2))
		return true;
	else
		return false;
}

// Cut on total energy
bool Cuts::cut_min_ht()
{
	if(ht == -1)
		ht = ht_calc->get_ht();

	// Sum pt of all jets + one lepton

	if(ht < min_ht)
		return true;
	else
		return false;
}

// Cut on total energy (at the moment only max_ht cut)
bool Cuts::cut_max_ht()
{
	if(ht == -1)
		ht = ht_calc->get_ht();

	if(ht > max_ht)
		return true;
	else
		return false;
}

bool Cuts::cut_Zrejection()
{
	double Z_mass = 91.2;

	if(isolated_electrons->size() == 1 && loose_electrons->size() >= 1){
		for(std::vector<pat::Electron>::iterator electron1 =
			isolated_electrons->begin();
		    electron1 != isolated_electrons->end();
		    ++electron1)
		{
			for(std::vector<pat::Electron>::iterator electron2 =
				loose_electrons->begin();
			    electron2 != loose_electrons->end();
			    ++electron2)
			{
				// don't use the same lepton twice
				if(electron1->pt() == electron2->pt() &&
					electron1->eta() == electron2->eta())
					continue;

				if((electron1->charge() != electron2->charge()) && (((electron1->p4() + electron2->p4()).mass() -
					Z_mass) < Z_rejection_width))
					return true;
			}
		}
	}

	if(isolated_muons->size() == 1 && loose_muons->size() >= 1){
		for(std::vector<pat::Muon>::iterator muon1 = isolated_muons->begin();
		    muon1 != isolated_muons->end();
		    ++muon1)
		{
			for(std::vector<pat::Muon>::iterator muon2 =
				loose_muons->begin();
			    muon2 != loose_muons->end();
			    ++muon2)
			{
				// don't use the same lepton twice
				if(muon1->pt() == muon2->pt() &&
					muon1->eta() == muon2->eta())
					continue;

				if((muon1->charge() != muon2->charge()) && (((muon1->p4() + muon2->p4()).mass() -
					Z_mass) < Z_rejection_width))
					return true;
			}
		}
	}

	return false;
}

// Calculate dR
double Cuts::deltaR(MyLorentzVector p4_1,MyLorentzVector p4_2)
{ 
	  double delta_phi = ROOT::Math::VectorUtil::DeltaPhi(p4_1,p4_2);         
	  double delta_eta = p4_1.eta()-p4_2.eta();        
	  double dR = sqrt(delta_eta * delta_eta + delta_phi * delta_phi);        
	  return dR;
	 
}


bool Cuts::cut_nisolated_leptons()
{
	int niso_lep = isolated_electrons->size() + isolated_muons->size();
		
	if((niso_lep >= min_nisolated_lep) || (min_nisolated_lep == -1))
		return false;
	else
		return true;   		
}

// Set max and min number of each lepton allowed
bool Cuts::cut_nisolated_electrons()
{
	int niso_e = isolated_electrons->size();

	if(niso_e == 0 && max_nisolated_e == 0)
		return false;
	
	if((niso_e >= min_nisolated_e && niso_e <= max_nisolated_e ) || ((niso_e >= min_nisolated_e) && (max_nisolated_e == -1)) || ((min_nisolated_e == -1) && (niso_e <= max_nisolated_e)) ||  ((min_nisolated_e == -1) && (max_nisolated_e == -1)))
		return false;
	else
		return true;		
}

bool Cuts::cut_nloose_electrons()
{
	int nloose_e = loose_electrons->size();

	if(nloose_e == 0 && max_nloose_e == 0)
		return false;
	
	if((nloose_e <= max_nloose_e ) || (max_nloose_e == -1))
		return false;
	else
		return true;		
}

bool Cuts::cut_nloose_muons()
{
	int nloose_mu = loose_muons->size();

	if(nloose_mu == 0 && max_nloose_mu == 0)
		return false;
	
	if((nloose_mu <= max_nloose_mu ) || (max_nloose_mu == -1))
		return false;
	else
		return true;		
}

bool Cuts::cut_nisolated_muons()
{
	int niso_mu = isolated_muons->size();

	if(niso_mu == 0 && max_nisolated_mu == 0)
		return false;

	if(((niso_mu >= min_nisolated_mu) && (niso_mu <= max_nisolated_mu)) || ((niso_mu >= min_nisolated_mu) && (max_nisolated_mu == -1)) || ((min_nisolated_mu == -1) && (niso_mu <= max_nisolated_mu)) ||  ((min_nisolated_mu == -1) && (max_nisolated_mu == -1))) 
	  {
		return false;		
	}
	else
	  {
		return true;
	  }
}

//Calculate efficiency and purity for differenet btag cuts
bool Cuts::cut_btag()
{

	//get back vector of jet(id,btag) in order of decreasing btag
	bjet_finder->set_min_btag_value(*min_btag);		
	std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(name_btag);
	
	if(btag_ids_this_algo->size() >= min_btag->size())
		return false;
	else
		return true;
 
}




//Apply trigger cuts
bool Cuts::cut_hltrigger()
{

  for(std::vector<double>::iterator trig_iter = trigger->begin(); trig_iter!=trigger->end(); ++ trig_iter){ 
 
    if (HLTR.isValid()) {
      //HLT_QuadJet30 = 24, //HLT_Mu11 = 83,//HLT_Mu15 = 85,  //HLT_Ele_15__LW_L1R = 50
      if (!(HLTR->accept(int(*trig_iter))))
	return true;             
    }
  }

  return false;
}

bool Cuts::get_triggerInfo()
{

  /*******Version 1 trig eff calculation where eff = Ntrig_acc / Nacc_gen********/
//   bool accgen_mu15 = false,accgen_e = false,accgen_mu11 = false;

 
           
//       //Check if event should pass trigger 
//       for(std::vector<pat::Muon>::iterator isolated_mu = isolated_muons->begin();
// 	  isolated_mu!=isolated_muons->end(); 
// 	  ++isolated_mu){
	
// 	if ((genEvt->singleLepton() != NULL && genEvt->singleLepton()->pt() >= 11 && fabs(genEvt->singleLepton()->eta()) <= 2.1)){
// 	  accgen_mu11 = true;
// 	  if ((genEvt->singleLepton() != NULL && genEvt->singleLepton()->pt() >= 15 && fabs(genEvt->singleLepton()->eta()) <= 2.1))
// 	    accgen_mu15 = true;
// 	}
	
// 	if(accgen_mu15 == true && accgen_mu11 == true)
// 	  continue;
//       }
      
//       for(std::vector<pat::Electron>::const_iterator e_iter = isolated_electrons->begin();
// 	  e_iter != isolated_electrons->end();
// 	  e_iter++){
// 	if ((genEvt->singleLepton() != NULL && genEvt->singleLepton()->pt() >= 15 && fabs(genEvt->singleLepton()->eta()) <= 2.4))
// 	  accgen_e = true;
	
// 	if(accgen_e == true)
// 	  continue;
//       }
      
//       //Count gen events
//       ngen_events++;
            
//       //Count acc/gen: events that should pass trigger
//       //Count trig/gen ie. acc/gen leptons that also pass trigger
//       if(accgen_mu11 == true)
// 	{
// 	  naccgen_mu11++;
// 	  if (HLTR.isValid()) {
// 	    //HLT_Mu11 = 83
// 	    if (HLTR->accept(83)) 
// 	      ntrigacc_mu11++;
// 	  }
	  
// 	}
            
//       if(accgen_mu15 == true)
// 	{
// 	  naccgen_mu15++;
// 	if (HLTR.isValid()) {
// 	  //HLT_Mu15 = 85
// 	  if (HLTR->accept(85)) 
// 	    ntrigacc_mu15++;
// 	}
	
// 	}
           
//       if(accgen_e == true)
// 	{
// 	  naccgen_e15++;
// 	  //HLT_Ele_15__LW_L1R = 50
	  
// 	  if (HLTR->accept(50)) 
// 	    ntrigacc_e15++;
// 	}

 /*******Version 2 trig eff calculation where eff = Ntrig_sel / Nselected (all must also pass four jet trigger)********/  

 //Everything must first pass four jet trigger to be counted
  if (HLTR.isValid()) {
    //HLT_QuadJet30 = 24
    if (HLTR->accept(24)){ 


      //In this version trig eff ngen_events actually means no events passed selection and 4 jet trigger cut
       ngen_events++;      
      
      //Count trig/gen ie. acc/gen leptons that also pass trigger
      
      if (HLTR.isValid()) {
	//HLT_Mu11 = 83
	if (HLTR->accept(83)) 
	  ntrigacc_mu11++;
      }
      
      
      
      if (HLTR.isValid()) {
	//HLT_Mu15 = 85
	if (HLTR->accept(85)) 
	    ntrigacc_mu15++;
      }
      
      
       if (HLTR.isValid()) {
      //HLT_Ele_15__LW_L1R = 50
      if (HLTR->accept(50)) 
	ntrigacc_e15++;
      }
    }
  }
  
  return false;



}


void Cuts::set_min_mu_pt(std::vector<double> *min)
{
	already_cut = false;
	min_mu_pt=min;
	mu_selector->set_min_pt(min);
}

void Cuts::set_min_mu_et(std::vector<double> *min)
{
	already_cut = false;
	min_mu_et=min;
	mu_selector->set_min_et(min);
}

void Cuts::set_min_e_pt(std::vector<double> *min)
{
	already_cut = false;
	min_e_pt=min;
	e_selector->set_min_pt(min);
}

void Cuts::set_min_e_et(std::vector<double> *min)
{
	already_cut = false;
	min_e_et=min;
	e_selector->set_min_et(min);
}

void Cuts::set_min_e_nHits(std::vector<double> *min)
{
	already_cut = false;
	min_e_nHits=min;
	e_selector->set_min_nHits(min);
}

void Cuts::set_max_e_chi2(std::vector<double> *max)
{
	already_cut = false;
	max_e_chi2=max;
	e_selector->set_max_chi2(max);
}

void Cuts::set_max_e_d0(std::vector<double> *max)
{
	already_cut = false;
	max_e_d0=max;
	e_selector->set_max_d0(max);
}

void Cuts::set_max_e_d0sig(std::vector<double> *max)
{
	already_cut = false;
	max_e_d0sig=max;
	e_selector->set_max_d0sig(max);
}

void Cuts::set_e_electronID(std::vector<double> *eid)
{
	already_cut = false;
	e_electronID=eid;
	e_selector->set_electronID(eid);
}

void Cuts::set_min_loose_mu_pt(std::vector<double> *min)
{
	already_cut = false;
	min_loose_mu_pt=min;
	loose_mu_selector->set_min_pt(min);
}

void Cuts::set_min_loose_mu_et(std::vector<double> *min)
{
	already_cut = false;
	min_loose_mu_et=min;
	loose_mu_selector->set_min_et(min);
}

void Cuts::set_min_loose_e_pt(std::vector<double> *min)
{
	already_cut = false;
	min_loose_e_pt=min;
	loose_e_selector->set_min_pt(min);
}

void Cuts::set_min_loose_e_et(std::vector<double> *min)
{
	already_cut = false;
	min_loose_e_et=min;
	loose_e_selector->set_min_et(min);
}

void Cuts::set_min_loose_e_nHits(std::vector<double> *min)
{
	already_cut = false;
	min_loose_e_nHits=min;
	loose_e_selector->set_min_nHits(min);
}

void Cuts::set_max_loose_e_chi2(std::vector<double> *max)
{
	already_cut = false;
	max_loose_e_chi2=max;
	loose_e_selector->set_max_chi2(max);
}

void Cuts::set_max_loose_e_d0(std::vector<double> *max)
{
	already_cut = false;
	max_loose_e_d0=max;
	loose_e_selector->set_max_d0(max);
}

void Cuts::set_max_loose_e_d0sig(std::vector<double> *max)
{
	already_cut = false;
	max_loose_e_d0sig=max;
	loose_e_selector->set_max_d0sig(max);
}

void Cuts::set_loose_e_electronID(std::vector<double> *eid)
{
	already_cut = false;
	loose_e_electronID=eid;
	loose_e_selector->set_electronID(eid);
}

void Cuts::set_met_cut(double min)
{
	already_cut = false;
	min_met = min;
}

void Cuts::set_min_M3(double min)
{
	already_cut = false;
	min_M3 = min;
}

void Cuts::set_min_mindiffM3(double min)
{
	already_cut = false;
	min_mindiffM3 = min;
}
void Cuts::set_min_njets(int min)
{
	already_cut = false;
	min_no_jets = min;
}

void Cuts::set_max_njets(int max)
{
	already_cut = false;
	max_no_jets = max;
}

void Cuts::set_JES_factor(double factor)
{
	already_cut = false;
	JES_factor=factor;
	jet_selector->change_JES(factor);
}

void Cuts::set_min_jet_pt(std::vector<double> *min)
{
	already_cut = false;
	min_jet_pt=min;
	jet_selector->set_min_pt(min);
}

void Cuts::set_max_jet_eta(double max)
{
	already_cut = false;
	jet_selector->set_max_eta(max);
	max_jet_eta=max;
}

void Cuts::set_min_jet_e_dR(double min)
{
	already_cut = false;
	jet_selector->set_min_e_dR(min);
	min_jet_e_dR=min;
}

// loose selection

void Cuts::set_max_loose_mu_eta(std::vector<double> *max)
{
	already_cut = false;
	loose_mu_selector->set_max_eta(max);
	max_loose_mu_eta=max;
}

void Cuts::set_loose_mu_type(double type)
{
	already_cut = false;
	loose_mu_type = (int) type;
	loose_mu_selector->set_lepton_type(mu_type);
}

void Cuts::set_max_loose_e_eta(std::vector<double> *max)
{
	already_cut = false;
	loose_e_selector->set_max_eta(max);
	max_loose_e_eta=max;
}

void Cuts::set_loose_e_type(double type)
{
	already_cut = false;
	loose_e_type = (int) type;
	loose_e_selector->set_lepton_type(loose_e_type);
}

void Cuts::set_min_loose_mu_nHits(std::vector<double> *min)
{
	already_cut = false;
	min_loose_mu_nHits=min;
	loose_mu_selector->set_min_nHits(min);
}

void Cuts::set_max_loose_mu_chi2(std::vector<double> *max)
{
	already_cut = false;
	max_loose_mu_chi2=max;
	loose_mu_selector->set_max_chi2(max);
}

void Cuts::set_max_loose_mu_d0(std::vector<double> *max)
{
	already_cut = false;
	max_loose_mu_d0=max;
	loose_mu_selector->set_max_d0(max);
}

void Cuts::set_max_loose_mu_d0sig(std::vector<double> *max)
{
	already_cut = false;
	max_loose_mu_d0sig=max;
	loose_mu_selector->set_max_d0sig(max);
}

void Cuts::set_loose_mu_electronID(std::vector<double> *eid)
{
	already_cut = false;
	loose_mu_electronID=eid;
	loose_mu_selector->set_electronID(eid);
}

void Cuts::set_max_loose_mu_trackiso(std::vector<double> *max)
{
	already_cut = false;
	max_loose_mu_trackiso=max;
	loose_mu_selector->set_max_trackiso(max);
}

void Cuts::set_max_loose_e_trackiso(std::vector<double> *max)
{
	already_cut = false;
	max_loose_e_trackiso=max;
	loose_e_selector->set_max_trackiso(max);
}
void Cuts::set_max_loose_mu_ecaliso(std::vector<double> *max)
{
	already_cut = false;
	max_loose_mu_ecaliso=max;
	loose_mu_selector->set_max_ecaliso(max);
}
void Cuts::set_max_loose_mu_caliso(std::vector<double> *max)
{
	already_cut = false;
	max_loose_mu_caliso=max;
	loose_mu_selector->set_max_caliso(max);
}

void Cuts::set_max_loose_e_ecaliso(std::vector<double> *max)
{
	already_cut = false;
	max_loose_e_ecaliso=max;
	loose_e_selector->set_max_ecaliso(max);
}

void Cuts::set_max_loose_e_caliso(std::vector<double> *max)
{
	already_cut = false;
	max_loose_e_caliso=max;
	loose_e_selector->set_max_caliso(max);
}

void Cuts::set_max_loose_e_hcal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_loose_e_hcal_veto_cone=max;
	loose_e_selector->set_max_hcal_veto_cone(max);
}

void Cuts::set_max_loose_e_ecal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_loose_e_ecal_veto_cone=max;
	loose_e_selector->set_max_ecal_veto_cone(max);
}

void Cuts::set_max_loose_mu_hcaliso(std::vector<double> *max)
{
	already_cut = false;
	max_loose_mu_hcaliso=max;
	loose_mu_selector->set_max_hcaliso(max);
}

void Cuts::set_max_loose_mu_hcal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_loose_mu_hcal_veto_cone=max;
	loose_mu_selector->set_max_hcal_veto_cone(max);
}

void Cuts::set_max_loose_mu_ecal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_loose_mu_ecal_veto_cone=max;
	loose_mu_selector->set_max_ecal_veto_cone(max);
}

void Cuts::set_max_loose_e_hcaliso(std::vector<double> *max)
{
	already_cut = false;
	max_loose_e_hcaliso=max;
	loose_e_selector->set_max_hcaliso(max);
}

void Cuts::set_min_loose_e_relIso(std::vector<double> *min)
{
	already_cut = false;
	min_loose_e_relIso=min;
	loose_e_selector->set_min_relIso(min);
}

void Cuts::set_min_loose_mu_dR(std::vector<double> *min)
{
	already_cut = false;
	min_loose_mu_dR=min;
	loose_mu_selector->set_min_dR(min);
}

void Cuts::set_min_loose_mu_relIso(std::vector<double> *min)
{
	already_cut = false;
	min_loose_mu_relIso=min;
	loose_mu_selector->set_min_relIso(min);
}

void Cuts::set_min_loose_e_dR(std::vector<double> *min)
{
	already_cut = false;
	min_loose_e_dR=min;
	loose_e_selector->set_min_dR(min);
}


// end loose selection
void Cuts::set_max_mu_eta(std::vector<double> *max)
{
	already_cut = false;
	mu_selector->set_max_eta(max);
	max_mu_eta=max;
}

void Cuts::set_mu_type(double type)
{
	already_cut = false;
	mu_type = (int) type;
	mu_selector->set_lepton_type(mu_type);
}

void Cuts::set_max_e_eta(std::vector<double> *max)
{
	already_cut = false;
	e_selector->set_max_eta(max);
	max_e_eta=max;
}

void Cuts::set_e_type(double type)
{
	already_cut = false;
	e_type = (int) type;
	e_selector->set_lepton_type(e_type);
}

void Cuts::set_min_mu_nHits(std::vector<double> *min)
{
	already_cut = false;
	min_mu_nHits=min;
	mu_selector->set_min_nHits(min);
}

void Cuts::set_max_mu_chi2(std::vector<double> *max)
{
	already_cut = false;
	max_mu_chi2=max;
	mu_selector->set_max_chi2(max);
}

void Cuts::set_max_mu_d0(std::vector<double> *max)
{
	already_cut = false;
	max_mu_d0=max;
	mu_selector->set_max_d0(max);
}

void Cuts::set_max_mu_d0sig(std::vector<double> *max)
{
	already_cut = false;
	max_mu_d0sig=max;
	mu_selector->set_max_d0sig(max);
}

void Cuts::set_mu_electronID(std::vector<double> *eid)
{
	already_cut = false;
	mu_electronID=eid;
	mu_selector->set_electronID(eid);
}

void Cuts::set_max_mu_trackiso(std::vector<double> *max)
{
	already_cut = false;
	max_mu_trackiso=max;
	mu_selector->set_max_trackiso(max);
}

void Cuts::set_max_e_trackiso(std::vector<double> *max)
{
	already_cut = false;
	max_e_trackiso=max;
	e_selector->set_max_trackiso(max);
}
void Cuts::set_max_mu_ecaliso(std::vector<double> *max)
{
	already_cut = false;
	max_mu_ecaliso=max;
	mu_selector->set_max_ecaliso(max);
}
void Cuts::set_max_mu_caliso(std::vector<double> *max)
{
	already_cut = false;
	max_mu_caliso=max;
	mu_selector->set_max_caliso(max);
}

void Cuts::set_max_e_ecaliso(std::vector<double> *max)
{
	already_cut = false;
	max_e_ecaliso=max;
	e_selector->set_max_ecaliso(max);
}

void Cuts::set_max_e_caliso(std::vector<double> *max)
{
	already_cut = false;
	max_e_caliso=max;
	e_selector->set_max_caliso(max);
}

void Cuts::set_max_e_hcal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_e_hcal_veto_cone=max;
	e_selector->set_max_hcal_veto_cone(max);
}

void Cuts::set_max_e_ecal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_e_ecal_veto_cone=max;
	e_selector->set_max_ecal_veto_cone(max);
}

void Cuts::set_max_mu_hcaliso(std::vector<double> *max)
{
	already_cut = false;
	max_mu_hcaliso=max;
	mu_selector->set_max_hcaliso(max);
}

void Cuts::set_max_mu_hcal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_mu_hcal_veto_cone=max;
	mu_selector->set_max_hcal_veto_cone(max);
}

void Cuts::set_max_mu_ecal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_mu_ecal_veto_cone=max;
	mu_selector->set_max_ecal_veto_cone(max);
}

void Cuts::set_max_e_hcaliso(std::vector<double> *max)
{
	already_cut = false;
	max_e_hcaliso=max;
	e_selector->set_max_hcaliso(max);
}

void Cuts::set_min_e_relIso(std::vector<double> *min)
{
	already_cut = false;
	min_e_relIso=min;
	e_selector->set_min_relIso(min);
}

void Cuts::set_min_mu_dR(std::vector<double> *min)
{
	already_cut = false;
	min_mu_dR=min;
	mu_selector->set_min_dR(min);
}

void Cuts::set_min_mu_relIso(std::vector<double> *min)
{
	already_cut = false;
	min_mu_relIso=min;
	mu_selector->set_min_relIso(min);
}

void Cuts::set_min_e_dR(std::vector<double> *min)
{
	already_cut = false;
	min_e_dR=min;
	e_selector->set_min_dR(min);
}

void Cuts::set_min_ht(double min)
{
	already_cut = false;
	min_ht = min;
}

void Cuts::set_max_ht(double max)
{
	already_cut = false;
	max_ht = max;
}

void Cuts::set_min_nisolated_lep(int n)
{
	already_cut = false;
	min_nisolated_lep = n;
}

void Cuts::set_min_nisolated_e(int n)
{
	already_cut = false;
	min_nisolated_e = n;
}

void Cuts::set_min_nisolated_mu(int n)
{
	already_cut = false;
	min_nisolated_mu = n;
}

void Cuts::set_max_nisolated_mu(int n)
{
	already_cut = false;
	max_nisolated_mu = n;
}


void Cuts::set_max_nisolated_e(int n)
{
	already_cut = false;
	max_nisolated_e = n;
}

void Cuts::set_max_nloose_mu(int n)
{
	already_cut = false;
	max_nloose_mu = n;
}

void Cuts::set_max_nloose_e(int n)
{
	already_cut = false;
	max_nloose_e = n;
}

void Cuts::set_Z_rejection_width(double width)
{
	already_cut = false;
	Z_rejection_width = width;
}

void Cuts::set_min_btag(std::vector<double> *min)
{
	already_cut = false;
	min_btag= min;
}

void Cuts::set_min_chi2(double min)
{
	already_cut = false;
	min_chi2= min;
}

void Cuts::set_max_chi2(double max)
{
	already_cut = false;
	max_chi2= max;
}

void Cuts::set_name_btag(double n)
{
	already_cut = false;
	num_name_btag = n;
	if (n == 1) name_btag = "jetBProbabilityBJetTags";
	else if (n == 2) name_btag = "jetProbabilityBJetTags";
	else if (n == 3) name_btag = "trackCountingHighPurBJetTags";
	else if (n == 4) name_btag = "trackCountingHighEffBJetTags";
	else name_btag = "-1";
	

}

void Cuts::set_trigger(std::vector<double> *trigger)
{
  already_cut = false;
  this->trigger=trigger;
}


/*
 *	get cut information
 */

std::vector<double>* Cuts::get_max_mu_trackiso()
{
	return max_mu_trackiso;
}

std::vector<double>* Cuts::get_max_e_trackiso()
{
	return max_e_trackiso;
}
std::vector<double>* Cuts::get_max_mu_ecaliso()
{
	return max_mu_ecaliso;
}
std::vector<double>* Cuts::get_max_mu_caliso()
{
	return max_mu_caliso;
}

std::vector<double>* Cuts::get_max_e_ecaliso()
{
	return max_e_ecaliso;
}

std::vector<double>* Cuts::get_max_e_caliso()
{
	return max_e_caliso;
}

std::vector<double>* Cuts::get_max_e_hcal_veto_cone()
{
	return max_e_hcal_veto_cone;
}

std::vector<double>* Cuts::get_max_e_ecal_veto_cone()
{
	return max_e_ecal_veto_cone;
}

std::vector<double>* Cuts::get_max_mu_hcaliso()
{
	return max_mu_hcaliso;
}

std::vector<double>* Cuts::get_max_mu_hcal_veto_cone()
{
	return max_mu_hcal_veto_cone;
}

std::vector<double>* Cuts::get_max_mu_ecal_veto_cone()
{
	return max_mu_ecal_veto_cone;
}

std::vector<double>* Cuts::get_max_e_hcaliso()
{
	return max_e_hcaliso;
}

std::vector<double>* Cuts::get_min_e_relIso()
{
	return min_e_relIso;
}


std::vector<double>* Cuts::get_min_mu_dR()
{
	return min_mu_dR;
}

std::vector<double>* Cuts::get_min_e_dR()
{
	return min_e_dR;
}

std::vector<double>* Cuts::get_min_e_nHits()
{
	return min_e_nHits;
}

std::vector<double>* Cuts::get_max_e_chi2()
{
	return max_e_chi2;
}

std::vector<double>* Cuts::get_max_e_d0()
{
	return max_e_d0;
}

std::vector<double>* Cuts::get_max_e_d0sig()
{
	return max_e_d0sig;
}

std::vector<double>* Cuts::get_e_electronID()
{
	return e_electronID;
}

std::vector<double>* Cuts::get_min_e_pt()
{
	return min_e_pt;
}

std::vector<double>* Cuts::get_min_e_et()
{
	return min_e_et;
}

std::vector<double>* Cuts::get_min_mu_pt()
{
	return min_mu_pt;
}

std::vector<double>* Cuts::get_min_mu_et()
{
	return min_mu_et;
}

std::vector<double>* Cuts::get_min_mu_relIso()
{
	return min_mu_relIso;
}

std::vector<double>* Cuts::get_min_jet_pt()
{
	return min_jet_pt;
}

double Cuts::get_max_jet_eta()
{
	return max_jet_eta;
}

double Cuts::get_min_jet_e_dR()
{
	return min_jet_e_dR;
}

double Cuts::get_JES_factor()
{
	return JES_factor;
}

std::vector<double>* Cuts::get_max_mu_eta()
{
	return max_mu_eta;
}

int Cuts::get_mu_type()
{
	return mu_type;
}

std::vector<double>* Cuts::get_max_e_eta()
{
	return max_e_eta;
}

int Cuts::get_e_type()
{
	return e_type;
}

std::vector<double>* Cuts::get_min_mu_nHits()
{
	return min_mu_nHits;
}

std::vector<double>* Cuts::get_max_mu_chi2()
{
	return max_mu_chi2;
}

std::vector<double>* Cuts::get_max_mu_d0()
{
	return max_mu_d0;
}

std::vector<double>* Cuts::get_max_mu_d0sig()
{
	return max_mu_d0sig;
}

std::vector<double>* Cuts::get_mu_electronID()
{
	return mu_electronID;
}

std::vector<double>* Cuts::get_min_btag()
{
	return min_btag;
}

double Cuts::get_name_btag()
{
	return num_name_btag;
}

MassReconstruction* Cuts::get_mass_reco()
{
	return mass_reco;
}

HtCalculator* Cuts::get_ht_calc()
{
	return ht_calc;
}

/*
 *	Print all cuts for Cuts object
 */

void Cuts::print_cuts()
{
	// FIXME: loose lepton cuts should also be printed
	std::cout << "=-----------IMPOSED-CUTS-----------=" << std::endl;
	std::cout << "identifier: " << identifier << std::endl;
	std::cout << "min_no_jets: " << min_no_jets << std::endl;
	print_cuts_vector("min_e_pt", min_e_pt);
	print_cuts_vector("min_mu_pt", min_mu_pt);
	print_cuts_vector("min_e_et", min_e_et);
	print_cuts_vector("min_mu_et", min_mu_et);
	std::cout << "min_met: " << min_met << std::endl;
	std::cout << "max_ht: " << max_ht << std::endl;
	std::cout << "min_ht: " << min_ht << std::endl;
	print_cuts_vector("max_mu_eta", max_mu_eta);
	print_cuts_vector("max_e_eta", max_e_eta);
	std::cout << "mu_type: " << mu_type << std::endl;
	std::cout << "e_type: " << e_type << std::endl;
	std::cout << "max_jet_eta: " << max_jet_eta << std::endl;
	std::cout << "min_jet_e_dR: " << min_jet_e_dR << std::endl;
	print_cuts_vector("max_mu_trackiso", max_mu_trackiso);
	print_cuts_vector("max_mu_ecaliso", max_mu_ecaliso);
	print_cuts_vector("max_mu_caliso", max_mu_caliso);
	print_cuts_vector("max_mu_hcaliso", max_mu_hcaliso);
	print_cuts_vector("max_mu_hcal_veto_cone", max_mu_hcal_veto_cone);
	print_cuts_vector("max_mu_ecal_veto_cone", max_mu_ecal_veto_cone);
	print_cuts_vector("min_mu_relIso", min_mu_relIso);
	print_cuts_vector("min_mu_dR", min_mu_dR);
	print_cuts_vector("max_mu_d0", max_mu_d0);
	print_cuts_vector("max_mu_d0sig", max_mu_d0sig);
	print_cuts_vector("max_mu_chi2", max_mu_chi2);
	print_cuts_vector("min_mu_nHits", min_mu_nHits);
	print_cuts_vector("mu_electronID", mu_electronID);
	print_cuts_vector("max_e_trackiso", max_e_trackiso);
	print_cuts_vector("max_e_ecaliso", max_e_ecaliso);
	print_cuts_vector("max_e_caliso", max_e_caliso);
	print_cuts_vector("max_e_hcal_veto_cone", max_e_hcal_veto_cone);
	print_cuts_vector("max_e_ecal_veto_cone", max_e_ecal_veto_cone);
	print_cuts_vector("max_e_hcaliso", max_e_hcaliso);
	print_cuts_vector("min_e_relIso", min_e_relIso);
	print_cuts_vector("min_e_dR", min_e_dR);
	print_cuts_vector("max_e_d0", max_e_d0);
	print_cuts_vector("max_e_d0sig", max_e_d0sig);
	print_cuts_vector("max_e_chi2", max_e_chi2);
	print_cuts_vector("min_e_nHits", min_e_nHits);
	print_cuts_vector("e_electronID", e_electronID);
	std::cout << "max_nloose_e: " << max_nloose_e << std::endl;
	std::cout << "max_nloose_mu: " << max_nloose_mu << std::endl;
	std::cout << "min_nisolated_lep: " << min_nisolated_lep << std::endl;
	std::cout << "max_nisolated_e: " << max_nisolated_e << std::endl;
	std::cout << "max_nisolated_mu: " << max_nisolated_mu << std::endl;
	std::cout << "min_nisolated_e: " << min_nisolated_e << std::endl;
	std::cout << "min_nisolated_mu: " << min_nisolated_mu << std::endl;
	std::cout << "min_M3: " << min_M3 << std::endl;
	std::cout << "min_mindiffM3: " << min_mindiffM3 << std::endl;
	std::cout << "min_chi2: " << min_chi2 << std::endl;
	print_cuts_vector("trigger", trigger);	
	std::cout << "name_btag: " << name_btag << std::endl;
	print_cuts_vector("min_btag", min_btag);
	print_cuts_vector("min_jet_pt", min_jet_pt);
	std::cout << "cuts_passed: " << cuts_passed_counter << std::endl;
	std::cout << "cuts_not_passed: " << cuts_not_passed_counter << std::endl;
	std::cout << "=----------------------------------=" << std::endl;
}

void Cuts::print_cuts_vector(std::string id, std::vector<double> *cuts)
{
        std::cout << id << ": ";

        if(cuts == NULL || cuts->size() == 0)
                std::cout << "-1" << std::endl;
        else{
                for(std::vector<double>::iterator cut = cuts->begin();
                    cut != cuts->end();
                    ++cut)
                        if(cut != cuts->begin())
                                std::cout << ":" << *cut;
                        else
                                std::cout << *cut;
                std::cout << std::endl;
        }
}
