#include "analysers/EventSelection/interface/MassReconstructionMethodBTag.h"

void MassReconstructionMethodBTag::book_id_vectors()
{
	mass_jet_ids_1btag = new std::vector<int>();
	mass_jet_ids_2btag = new std::vector<int>();

	event_is_processed_1btag = false;
	event_is_processed_2btag = false;
}

MassReconstructionMethodBTag::MassReconstructionMethodBTag()
{
	book_id_vectors();
}

MassReconstructionMethodBTag::~MassReconstructionMethodBTag()
{
	free_id_vectors();
}

void MassReconstructionMethodBTag::free_id_vectors()
{
	delete mass_jet_ids_1btag;
	mass_jet_ids_1btag = NULL;
	delete mass_jet_ids_2btag;
	mass_jet_ids_2btag = NULL;
}

void MassReconstructionMethodBTag::reset_values()
{
	hadT_mass = -1;
	hadW_mass = -1;
	lepT_mass = -1;
	lepW_mass = -1;

	hadT_mass_1btag = -1;
	hadW_mass_1btag = -1;
	lepT_mass_1btag = -1;
	lepW_mass_1btag = -1;

	hadT_mass_2btag = -1;
	hadW_mass_2btag = -1;
	lepT_mass_2btag = -1;
	lepW_mass_2btag = -1;

	mass_jet_ids->clear();
	mass_jet_ids_1btag->clear();
	mass_jet_ids_2btag->clear();

	event_is_processed = false;
	event_is_processed_1btag = false;
	event_is_processed_2btag = false;
}

void MassReconstructionMethodBTag::set_bjet_finder(BJetFinder *bj_finder)
{
	bjet_finder = bj_finder;
}

bool MassReconstructionMethodBTag::is_processed_1btag()
{
	return event_is_processed_1btag;
}

bool MassReconstructionMethodBTag::is_processed_2btag()
{
	return event_is_processed_2btag;
}

//Check if relevant functions have been called and return calculated values
double MassReconstructionMethodBTag::get_hadT_mass_1btag()
{
	if(hadT_mass_1btag != -1)
		return hadT_mass_1btag;
	else{
		calculate_mass_1btag();
		return hadT_mass_1btag;
	}
}

double MassReconstructionMethodBTag::get_lepT_mass_1btag()
{
	if(lepT_mass_1btag != -1)
		return lepT_mass_1btag;
	else{
		calculate_mass_1btag();
		return lepT_mass_1btag;
	}
}

double MassReconstructionMethodBTag::get_hadW_mass_1btag()
{
	if(hadW_mass_1btag != -1)
		return hadW_mass_1btag;
	else{
		calculate_mass_1btag();
		return hadW_mass_1btag;
	}
}

double MassReconstructionMethodBTag::get_lepW_mass_1btag()
{
	if(lepW_mass_1btag != -1)
		return lepW_mass_1btag;
	else{
		calculate_mass_1btag();
		return lepW_mass_1btag;
	}
}

//Check if relevant functions have been called and return calculated values
double MassReconstructionMethodBTag::get_hadT_mass_2btag()
{
	if(hadT_mass_2btag != -1)
		return hadT_mass_2btag;
	else{
		calculate_mass_2btag();
		return hadT_mass_2btag;
	}
}

double MassReconstructionMethodBTag::get_lepT_mass_2btag()
{
	if(lepT_mass_2btag != -1)
		return lepT_mass_2btag;
	else{
		calculate_mass_2btag();
		return lepT_mass_2btag;
	}
}

double MassReconstructionMethodBTag::get_hadW_mass_2btag()
{
	if(hadW_mass_2btag != -1)
		return hadW_mass_2btag;
	else{
		calculate_mass_2btag();
		return hadW_mass_2btag;
	}
}

double MassReconstructionMethodBTag::get_lepW_mass_2btag()
{
	if(lepW_mass_2btag != -1)
		return lepW_mass_2btag;
	else{
		calculate_mass_2btag();
		return lepW_mass_2btag;
	}
}

std::vector<int>* MassReconstructionMethodBTag::get_mass_jet_ids_1btag()
{
	if(mass_jet_ids_1btag->size() > 0)
		return mass_jet_ids_1btag;
	else{
		calculate_mass_1btag();
		return mass_jet_ids_1btag;
	}
}

std::vector<int>* MassReconstructionMethodBTag::get_mass_jet_ids_2btag()
{
	if(mass_jet_ids_2btag->size() > 0)
		return mass_jet_ids_2btag;
	else{
		calculate_mass_2btag();
		return mass_jet_ids_2btag;
	}
}

void MassReconstructionMethodBTag::calculate_mass_1btag()
{
}

void MassReconstructionMethodBTag::calculate_mass_2btag()
{
}
