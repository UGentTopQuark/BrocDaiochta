#include "analysers/EventSelection/interface/METPlots.h"

METPlots::METPlots(std::string ident)
{
	id = ident;
	book_histos();

        METCor_method0 = new METCorrector();
        METCor_method0->set_correction_method(0);
        METCor_method1 = new METCorrector();
        METCor_method1->set_correction_method(1);
        METCor_method2 = new METCorrector();
        METCor_method2->set_correction_method(2);
}

METPlots::~METPlots()
{
	if(METCor_method0){
		delete METCor_method0;
		METCor_method0 = NULL;
	}
	if(METCor_method1){
		delete METCor_method1;
		METCor_method1 = NULL;
	}
	if(METCor_method2){
		delete METCor_method2;
		METCor_method2 = NULL;
	}
}

void METPlots::set_met_handle(edm::Handle<edm::View<pat::MET> > analyser_mets)
{
	mets = analyser_mets;
}

void METPlots::calculate_MET()
{
        if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
		pat::Particle *muon = new pat::Particle();
		muon->setP4((*isolated_muons)[0].p4());
                METCor_method0->set_handles(mets, muon);
                method0_mets = METCor_method0->get_MET();
		delete muon;
		muon = NULL;
        }
        else if(isolated_electrons->size() == 1 && isolated_muons->size() == 0){
		pat::Particle *electron = new pat::Particle();
		electron->setP4((*isolated_electrons)[0].p4());
                METCor_method0->set_handles(mets, electron);
                method0_mets = METCor_method0->get_MET();
		delete electron;
		electron = NULL;
        }else{
                METCor_method0->set_handles(mets);
                method0_mets = METCor_method0->get_uncorrected_MET();
	}

        if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
		pat::Particle *muon = new pat::Particle();
		muon->setP4((*isolated_muons)[0].p4());
                METCor_method1->set_handles(mets, muon);
                method1_mets = METCor_method1->get_MET();
		delete muon;
		muon = NULL;
        }
        else if(isolated_electrons->size() == 1 && isolated_muons->size() == 0){
		pat::Particle *electron = new pat::Particle();
		electron->setP4((*isolated_electrons)[0].p4());
                METCor_method1->set_handles(mets, electron);
                method1_mets = METCor_method1->get_MET();
		delete electron;
		electron = NULL;
        }else{
                METCor_method1->set_handles(mets);
                method1_mets = METCor_method1->get_uncorrected_MET();
	}

        uncorrected_mets = METCor_method1->get_uncorrected_MET();

        if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
		pat::Particle *muon = new pat::Particle();
		muon->setP4((*isolated_muons)[0].p4());
                METCor_method2->set_handles(mets, muon);
                method2_mets = METCor_method2->get_MET();
		delete muon;
		muon = NULL;
        }
        else if(isolated_electrons->size() == 1 && isolated_muons->size() == 0){
		pat::Particle *electron = new pat::Particle();
		electron->setP4((*isolated_electrons)[0].p4());
                METCor_method2->set_handles(mets, electron);
                method2_mets = METCor_method2->get_MET();
		delete electron;
		electron = NULL;
        }else{
                METCor_method2->set_handles(mets);
                method2_mets = METCor_method2->get_uncorrected_MET();
	}
}

void METPlots::plot()
{
	calculate_MET();
	plot_MET_pz();
	plot_MET_MtW();
}

int METPlots::get_pt_bin(double neutrino_pt)
{
	double min_pt = 0;
	double max_pt = 200.;
	double bin_width = (max_pt - min_pt)/((double) PT_BINS);

	int bin = (int) ((neutrino_pt-min_pt)/bin_width);

	if(bin < PT_BINS)
		return bin;
	else
		return -1;
}

int METPlots::get_pz_bin(double neutrino_pz)
{
	double min_pz = 0;
	double max_pz = 200.;
	double bin_width = (max_pz - min_pz)/((double) PT_BINS);

	int bin = (int) ((neutrino_pz-min_pz)/bin_width);

	if((bin > 0) && (bin < PT_BINS))
		return bin;
	else
		return -1;
}

void METPlots::plot_MET_pz()
{
	if(gen_match == NULL || !gen_match->is_ok() || gen_match->get_decay_mode() != 1)
		return;

	if(isolated_muons->size() < 1)
		return;

	reco::GenParticle *neutrino = NULL;
	if(gen_match->t_decay_is_leptonic()){
		neutrino = gen_match->get_particle("Wplus_dp2");
	}else if(gen_match->tbar_decay_is_leptonic()){
		neutrino = gen_match->get_particle("Wminus_dp2");
	}else{
		std::cerr << "WARNING: METPlots::plot_MET_pz() neutrino NULL" << std::endl;
	}

	pat::MET corrected_MET = *(corrected_mets->begin());
	pat::MET uncorrected_MET = *(uncorrected_mets->begin());
	histos1d[("dpz_corMET_neutrino"+id).c_str()]->Fill(neutrino->pz() - corrected_MET.pz());
	histos1d[("dR_corMET_neutrino"+id).c_str()]->Fill(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), corrected_MET.p4()));
	histos1d[("neutrino_pz"+id).c_str()]->Fill(neutrino->pz());
	histos1d[("neutrino_pt"+id).c_str()]->Fill(neutrino->pt());
	histos1d[("dpt_neutrino_MET"+id).c_str()]->Fill(neutrino->pt() - corrected_MET.pt());
//	histos1d[("dpz_MET_neutrino"+id).c_str()]->Fill(neutrino->pz() - uncorrected_MET.pz());
	histos1d[("dR_uncorMET_neutrino"+id).c_str()]->Fill(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), uncorrected_MET.p4()));

	// how often do neutrino and MET p4 match?
//	if(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), corrected_MET.p4()) < 0.4){
//		histos1d[("match_MET_neutrino"+id).c_str()]->Fill(1);
//	}else{
//		histos1d[("match_MET_neutrino"+id).c_str()]->Fill(0);
//	}
//	if(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), method0_mets->begin()->p4()) < 0.4){
//		histos1d[("match_MET_neutrino_method0"+id).c_str()]->Fill(1);
//	}else{
//		histos1d[("match_MET_neutrino_method0"+id).c_str()]->Fill(0);
//	}
//	if(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), method2_mets->begin()->p4()) < 0.4){
//		histos1d[("match_MET_neutrino_method2"+id).c_str()]->Fill(1);
//	}else{
//		histos1d[("match_MET_neutrino_method2"+id).c_str()]->Fill(0);
//	}
//	if(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), uncorrected_MET.p4()) < 0.4){
//		histos1d[("match_MET_neutrino_uncorMET"+id).c_str()]->Fill(1);
//	}else{
//		histos1d[("match_MET_neutrino_uncorMET"+id).c_str()]->Fill(0);
//	}

	histos2d[("pt_neutrino_vs_MET"+id).c_str()]->Fill(neutrino->pt(), corrected_MET.pt());
	histos2d[("pz_neutrino_vs_MET"+id).c_str()]->Fill(neutrino->pz(), corrected_MET.pz());

	histos2d[("pt_neutrino_vs_MET_method0"+id).c_str()]->Fill(neutrino->pt(), method0_mets->begin()->pt());
	histos2d[("pz_neutrino_vs_MET_method0"+id).c_str()]->Fill(neutrino->pz(), method0_mets->begin()->pz());

	histos2d[("pt_neutrino_vs_MET_method2"+id).c_str()]->Fill(neutrino->pt(), method2_mets->begin()->pt());
	histos2d[("pz_neutrino_vs_MET_method2"+id).c_str()]->Fill(neutrino->pz(), method2_mets->begin()->pz());

	double npx = neutrino->px();
	double npy = neutrino->py();
	double nmet_pt = npx*npx+npy*npy;
        pat::Particle::LorentzVector p;
        p.SetPx(npx);
        p.SetPy(npy);
        p.SetPz(0.);
        p.SetE(nmet_pt);

        pat::MET neutrino_MET;
        neutrino_MET.setP4(p);

	MyMEzCalculator MEzCal;
	MEzCal.SetMET(neutrino_MET);
	pat::Particle muon;
	muon.setP4(isolated_muons->begin()->p4());
	MEzCal.SetLepton(*(isolated_muons->begin()));

	double neutrinoMET_recolep_pz = MEzCal.Calculate();

	histos2d[("pz_neutrinoMET_recolep_vs_neutrino"+id).c_str()]->Fill(neutrino->pz(), neutrinoMET_recolep_pz);

	histos1d[("dpz_neutrino_recolep"+id).c_str()]->Fill(neutrino->pz() - neutrinoMET_recolep_pz);

	reco::GenParticle *gen_lepton = NULL;
	if(gen_match->t_decay_is_leptonic()){
		gen_lepton = gen_match->get_particle("Wplus_dp1");
	}else if(gen_match->tbar_decay_is_leptonic()){
		gen_lepton = gen_match->get_particle("Wminus_dp1");
	}else{
		std::cerr << "WARNING: METPlots::plot_MET_pz() lepton NULL" << std::endl;
	}

	histos2d[("pz_neutrino_gen_lep"+id).c_str()]->Fill(neutrino->pz(), gen_lepton->pz());

//	Tools tool;
//	int pt_bin = get_pt_bin(neutrino->pt());
//	if(pt_bin != -1){
//		histos1d[("MET_pt_bin"+tool.stringify(pt_bin)+id).c_str()]->Fill((neutrino->pt() - corrected_MET.pt())/neutrino->pz());
//	}
//	int pz_bin = get_pz_bin(fabs(neutrino->pz()));
//	if(pz_bin != -1){
//		histos1d[("MET_pz_method0_bin"+tool.stringify(pz_bin)+id).c_str()]->Fill((neutrino->pz() - method0_mets->begin()->pz())/neutrino->pz());
//		histos1d[("MET_pz_method1_bin"+tool.stringify(pz_bin)+id).c_str()]->Fill((neutrino->pz() - method1_mets->begin()->pz())/neutrino->pz());
//		histos1d[("MET_pz_method2_bin"+tool.stringify(pz_bin)+id).c_str()]->Fill((neutrino->pz() - method2_mets->begin()->pz())/neutrino->pz());
//	}
}

// Plot leptonic transverse mass and MET 
void METPlots::plot_MET_MtW()
{
	if(mets->size() < 1)
		return;

	// Loop to get total MET, should only be one iteration of loop.
	double e_MtW=0.0,mu_MtW=0.0;
	for(std::vector<pat::MET>::iterator met_iter = corrected_mets->begin();
	    met_iter!=corrected_mets->end();
	    ++met_iter){
		
		// Check for at least one lepton before calculating Mt	
		if(isolated_muons->size() > 0){
	   		mu_MtW = ((*isolated_muons)[0].p4()+met_iter->p4()).Mt(); 
	   		histos1d[("muon_MtW"+id).c_str()]->Fill(mu_MtW);
		}
		       
		if(isolated_electrons->size() > 0){
	   		e_MtW = ((*isolated_electrons)[0].p4()+met_iter->p4()).Mt();
	   		histos1d[("electron_MtW"+id).c_str()]->Fill(e_MtW); 
		}
	}
	histos1d[("Event_MET"+id).c_str()]->Fill(corrected_mets->begin()->et());
	histos1d[("Event_MET_pt"+id).c_str()]->Fill(corrected_mets->begin()->pt());	
}

void METPlots::book_histos()
{
	edm::Service<TFileService> fs;

	// METpz plots
//	histos1d[("dpz_MET_neutrino"+id).c_str()]=fs->make<TH1D>(("dpz_MET_neutrino"+id).c_str(),"delta p_z between MET and generator neutrino",100,-150,150);
	histos1d[("dR_uncorMET_neutrino"+id).c_str()]=fs->make<TH1D>(("dR_uncorMET_neutrino"+id).c_str(),"delta R between MET and generator neutrino",30,0,8);
	histos1d[("dpz_neutrino_recolep"+id).c_str()]=fs->make<TH1D>(("dpz_neutrino_recolep"+id).c_str(),"delta p_z between neutrino-MET-pz from reco lepton and generator neutrino",100,-150,150);
	histos1d[("dpz_corMET_neutrino"+id).c_str()]=fs->make<TH1D>(("dpz_corMET_neutrino"+id).c_str(),"delta p_{z} between MET and generator neutrino",100,-200,200);
	histos1d[("dR_corMET_neutrino"+id).c_str()]=fs->make<TH1D>(("dR_corMET_neutrino"+id).c_str(),"delta R between MET and generator neutrino",30,0,8);
	histos1d[("neutrino_pz"+id).c_str()]=fs->make<TH1D>(("neutrino_pz"+id).c_str(),"p_{z} of generator neutrino",100,-200,200);
	histos1d[("neutrino_pt"+id).c_str()]=fs->make<TH1D>(("neutrino_pt"+id).c_str(),"p_{T} of generator neutrino",40,0,400);
	histos1d[("dpt_neutrino_MET"+id).c_str()]=fs->make<TH1D>(("dpt_neutrino_MET"+id).c_str(),"dp_{T}(generator neutrino, MET)",100,-200,200);

	histos1d[("Event_MET"+id).c_str()]=fs->make<TH1D>(("Event_MET"+id).c_str(),"missing Et in event",40,0,400);
	histos1d[("Event_MET_pt"+id).c_str()]=fs->make<TH1D>(("Event_MET_pt"+id).c_str(),"Neutrino pt in event",40,0,400);
	histos1d[("muon_MtW"+id).c_str()]=fs->make<TH1D>(("muon_MtW"+id).c_str(),"Leptonic Transverse Mass with muon",30,0, 200);
	histos1d[("electron_MtW"+id).c_str()]=fs->make<TH1D>(("electron_MtW"+id).c_str(),"Leptonic Transverse Mass with electron",30,0, 200);

//	// 1 = matched, 0 = unmatched dR < 0.4
//	histos1d[("match_MET_neutrino"+id).c_str()]=fs->make<TH1D>(("match_MET_neutrino"+id).c_str(),"dR(nu, MET) < 0.4) 0 = unmatched, 1 = matched",5,-0.5,4.5);
//	histos1d[("match_MET_neutrino_method0"+id).c_str()]=fs->make<TH1D>(("match_MET_neutrino_method0"+id).c_str(),"dR(nu, MET) < 0.4) 0 = unmatched, 1 = matched",5,-0.5,4.5);
//	histos1d[("match_MET_neutrino_method2"+id).c_str()]=fs->make<TH1D>(("match_MET_neutrino_method2"+id).c_str(),"dR(nu, MET) < 0.4) 0 = unmatched, 1 = matched",5,-0.5,4.5);
//	histos1d[("match_MET_neutrino_uncorMET"+id).c_str()]=fs->make<TH1D>(("match_MET_neutrino_uncorMET"+id).c_str(),"dR(nu, MET) < 0.4) 0 = unmatched, 1 = matched",5,-0.5,4.5);

	// neutrino vs MET
	histos2d[("pz_neutrinoMET_recolep_vs_neutrino"+id).c_str()]=fs->make<TH2D>(("pz_neutrinoMET_recolep_vs_neutrino"+id).c_str(),"correlation between neutrino pz and ``neutrino-MET''-pz",150, -300, 300, 150,-300,300);
	histos2d[("pt_neutrino_vs_MET"+id).c_str()]=fs->make<TH2D>(("pt_neutrino_vs_MET"+id).c_str(),"correlation between neutrino pt and MET",150, 0, 300, 150,0,300);
	histos2d[("pz_neutrino_vs_MET"+id).c_str()]=fs->make<TH2D>(("pz_neutrino_vs_MET"+id).c_str(),"correlation between neutrino pz and pz component of MET",150, -300, 300, 150,-300,300);

	histos2d[("pt_neutrino_vs_MET_method0"+id).c_str()]=fs->make<TH2D>(("pt_neutrino_vs_MET_method0"+id).c_str(),"correlation between neutrino pt and MET",150, 0, 300, 150,0,300);
	histos2d[("pz_neutrino_vs_MET_method0"+id).c_str()]=fs->make<TH2D>(("pz_neutrino_vs_MET_method0"+id).c_str(),"correlation between neutrino pz and pz component of MET",150, -300, 300, 150,-300,300);

	histos2d[("pt_neutrino_vs_MET_method2"+id).c_str()]=fs->make<TH2D>(("pt_neutrino_vs_MET_method2"+id).c_str(),"correlation between neutrino pt and MET",150, 0, 300, 150,0,300);
	histos2d[("pz_neutrino_vs_MET_method2"+id).c_str()]=fs->make<TH2D>(("pz_neutrino_vs_MET_method2"+id).c_str(),"correlation between neutrino pz and pz component of MET",150, -300, 300, 150,-300,300);

	histos2d[("pz_neutrino_gen_lep"+id).c_str()]=fs->make<TH2D>(("pz_neutrino_gen_lep"+id).c_str(),"correlation between neutrino pz and generator charged lepton pz",150, -300, 300, 150,-300,300);

//	Tools tool;
//	for(int bin = 0; bin < PT_BINS; ++bin){
//		histos1d[("MET_pt_bin"+tool.stringify(bin)+id).c_str()]=fs->make<TH1D>(("MET_pt_bin"+tool.stringify(bin)+id).c_str(),"MET resolution in pt bin",100,-3.,3.);
//		histos1d[("MET_pz_method0_bin"+tool.stringify(bin)+id).c_str()]=fs->make<TH1D>(("MET_pz_method0_bin"+tool.stringify(bin)+id).c_str(),"MET resolution in pt bin",100,-3.,3.);
//		histos1d[("MET_pz_method1_bin"+tool.stringify(bin)+id).c_str()]=fs->make<TH1D>(("MET_pz_method1_bin"+tool.stringify(bin)+id).c_str(),"MET resolution in pt bin",100,-3.,3.);
//		histos1d[("MET_pz_method2_bin"+tool.stringify(bin)+id).c_str()]=fs->make<TH1D>(("MET_pz_method2_bin"+tool.stringify(bin)+id).c_str(),"MET resolution in pt bin",100,-3.,3.);
//	}
}
