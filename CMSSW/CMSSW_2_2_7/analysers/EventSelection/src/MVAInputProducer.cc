#include "analysers/EventSelection/interface/MVAInputProducer.h"

MVAInputProducer::MVAInputProducer()
{
	mass_reco=NULL;
	event_counter = 0;
	ht = -1.0;
	ident = "";
	bjet_finder = NULL;
}

MVAInputProducer::~MVAInputProducer()
{

}

void MVAInputProducer::set_ident(std::string ident)
{
	this->ident = ident;	
}

void MVAInputProducer::set_handles(std::vector<pat::Jet>* jets, std::vector<pat::Electron>* electrons, std::vector<pat::Muon>* muons, std::vector<pat::MET> *mets)
{
	this->jets = jets;
	this->electrons = electrons;
	this->muons = muons;
	this->mets = mets;
}

void MVAInputProducer::print_MVA_input()
{
	std::cout << "MVA" << ident << "-" << event_counter << " ";
	print_jets();
	print_muons();
	print_electrons();
//	print_masses();
	print_ht();
	print_btag();
	print_mu_isolation();
	print_e_isolation();
	
	++event_counter;
	// print all information in one line and add at the end here the line break
	std::cout << std::endl;
}

void MVAInputProducer::print_muons()
{
	// plot only first muon
	if(muons->begin() != muons->end()){
		if(verbose)
			std::cout << "mu_pt: ";
		std::cout << muons->begin()->pt() << " ";
		if(verbose)
			std::cout << "mu_eta: ";
		std::cout << muons->begin()->eta() << " ";
	}
	else{
		// to keep a consistent format
		if(verbose)
			std::cout << "mu_pt: ";
		std::cout << "0.0 ";
		if(verbose)
			std::cout << "mu_eta: ";
		std::cout << "0.0 ";
	}
}

void MVAInputProducer::print_mu_isolation()
{
	// plot only first muon
	if(muons->begin() != muons->end()){
		if(verbose)
			std::cout << "mu_trkiso: ";
		std::cout << muons->begin()->trackIso() << " ";
		if(verbose)
			std::cout << "mu_caliso: ";
		std::cout << muons->begin()->caloIso() << " ";
	}
	else{
		// to keep a consistent format
		if(verbose)
			std::cout << "mu_trkiso: ";
		std::cout << "0.0 ";
		if(verbose)
			std::cout << "mu_caliso: ";
		std::cout << "0.0 ";
	}
}

void MVAInputProducer::print_masses()
{
	if(mass_reco == NULL){
		std::cerr << "WARNING: no mass reco object found in MVAInputProducer::plot_masses()" << std::endl;
		return;
	}

	// print M3
	double m3 = mass_reco->calculate_M3();
	if(verbose)
		std::cout << "m3: ";

	if(m3 != -1)
		std::cout << m3 << " ";
	else
		std::cout << "-1.0 ";

	// print min.diff.M3
// 	double mindiffm3 = mass_reco->calculate_min_diff_M3();
// 	if(verbose)
// 		std::cout << "mindiff: ";
// 	if(mindiffm3 != -1)
// 		std::cout << mindiffm3 << " ";
// 	else
// 		std::cout << "-1.0 ";

	// print chi2
	double chi2 = mass_reco->calculate_chihadTmass();
	if(verbose)
		std::cout << "chi2: ";
	if(chi2 != -1)
		std::cout << chi2 << " ";
	else
		std::cout << "-1.0 ";
}

void MVAInputProducer::print_ht()
{
	if(verbose)
		std::cout << "ht: ";
	std::cout << ht << " ";
}

void MVAInputProducer::print_electrons()
{

	// plot only first muon
	if(electrons->begin() != electrons->end()){
		if(verbose)
			std::cout << "e_pt: ";
		std::cout << electrons->begin()->pt() << " ";
		if(verbose)
			std::cout << "e_eta: ";
		std::cout << electrons->begin()->eta() << " ";
	}
	else{
		// to keep a consistent format
		if(verbose)
			std::cout << "e_pt: ";
		std::cout << "0.0 ";
		if(verbose)
			std::cout << "e_eta: ";
		std::cout << "0.0 ";
	}
}

void MVAInputProducer::print_e_isolation()
{
	// plot only first electron
	if(electrons->begin() != electrons->end()){
		if(verbose)
			std::cout << "e_trkiso: ";
		std::cout << electrons->begin()->trackIso() << " ";
		if(verbose)
			std::cout << "e_caliso: ";
		std::cout << electrons->begin()->caloIso() << " ";
	}
	else{
		// to keep a consistent format
		if(verbose)
			std::cout << "e_trkiso: ";
		std::cout << "0.0 ";
		if(verbose)
			std::cout << "e_caliso: ";
		std::cout << "0.0 ";
	}
}
void MVAInputProducer::print_jets()
{
	int jet_counter=0;
	const int MAX_JET_NUMBER=4;
	for(std::vector<pat::Jet>::iterator jet_iter = jets->begin();
		jet_iter != jets->end() && jet_counter < MAX_JET_NUMBER;
		++jet_iter, ++jet_counter)
	{
		if(verbose)
			std::cout << "j" << jet_counter << "-pt: ";
		std::cout << jet_iter->pt() << " ";
	}

	for(;jet_counter < MAX_JET_NUMBER; ++jet_counter){
		if(verbose)
			std::cout << "j" << jet_counter << "-pt: ";
		std::cout << "-1.0 ";
	}

	jet_counter = 0;
	for(std::vector<pat::Jet>::iterator jet_iter = jets->begin();
		jet_iter != jets->end() && jet_counter < MAX_JET_NUMBER;
		++jet_iter, ++jet_counter)
	{
		if(verbose)
			std::cout << "j" << jet_counter << "-eta: ";
		std::cout << jet_iter->eta() << " ";
	}

	for(;jet_counter < MAX_JET_NUMBER; ++jet_counter){
		if(verbose)
			std::cout << "j" << jet_counter << "-eta: ";
		std::cout << "-1.0 ";
	}

}
void MVAInputProducer::print_btag()
{
	//print btag value of highest btagged jet
	std::string name_btag = "trackCountingHighEffBJetTags";	
	std::vector<double> set_min_btag (1,-1000);
	bjet_finder->set_min_btag_value(set_min_btag);	
	std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(name_btag);
	if(verbose)
		std::cout << "btag1: ";
	if(btag_ids_this_algo->size() > 0)
		std::cout << (*btag_ids_this_algo)[0].second << " ";
	else
		std::cout << "-1.0 ";

	
// 	if(verbose)
// 		std::cout << "btag2: ";
// 	if(btag_ids_this_algo->size() > 1)
// 		std::cout << (*btag_ids_this_algo)[1].second() << " ";
// 	else
// 		std::cout << "-1.0 ";
}


void MVAInputProducer::set_mass_reconstruction(MassReconstruction *mass_reco)
{
	this->mass_reco = mass_reco;
}

void MVAInputProducer::set_ht(double ht)
{
	this->ht = ht;
}

void MVAInputProducer::set_bjet_finder(BJetFinder *bjet_finder)
{
	this->bjet_finder = bjet_finder;
}
