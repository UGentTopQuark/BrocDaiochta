#include "analysers/EventSelection/interface/PlotGenerator.h" 

PlotGenerator::PlotGenerator(std::string ident)
{
	id = "_"+ident;

	// book histos by means of cmssw file service
	book_histos();

	jets=NULL;
	isolated_muons = NULL;
	isolated_electrons = NULL;
	all_electrons = NULL;
	corrected_mets = NULL;
	uncorrected_mets = NULL;

	// Initialise LeptonSelector for both leptons
	e_selector = new LeptonSelector<pat::Electron>();
	all_e_selector = new LeptonSelector<pat::Electron>();
        mu_selector = new LeptonSelector<pat::Muon>();
	jet_selector = new JetSelector();
        METCor = new METCorrector();

	// objects for generator truth information
	gen_match = new GenMatch();

        cuts=NULL;
	mass_reco = NULL;	// mass reco from cuts class used
	ht_calc = NULL;

	bjet_finder = new BJetFinder();
	mass_jet_match = new MassJetMatch();
	reco_gen_match = new RecoGenMatch();

	tprime_mass = -1;

	/*
	 *	book plot classes
	 */
	met_plots = new METPlots(id);
	met_plots->set_gen_match(gen_match);

	abcd_plots = new ABCDMethodPlots(id);

	trigger_plots = new TriggerPlots(id);
	trigger_plots->set_reco_gen_match(reco_gen_match);

	lept_iso_plots = new LeptonIsolationPlots(id);
	lept_iso_plots->set_reco_gen_match(reco_gen_match);

	btag_plots = new BTagPlots(id);
	btag_plots->set_reco_gen_match(reco_gen_match);
	btag_plots->set_gen_match(gen_match);
	btag_plots->set_bjet_finder(bjet_finder);

	mass_plots = new MassPlots(id);
	mass_plots->set_reco_gen_match(reco_gen_match);
	mass_plots->set_gen_match(gen_match);
	mass_plots->set_bjet_finder(bjet_finder);

	mva_producer= new MVAInputProducer();
	mva_producer->set_ident(id);
	mva_producer->set_bjet_finder(bjet_finder);
}

PlotGenerator::~PlotGenerator()
{
	histos1d[("W_jets_assignment"+id).c_str()]->Sumw2();
	histos1d[("W_jets_assignment"+id).c_str()]->Scale(1.0/histos1d[("W_jets_assignment"+id).c_str()]->Integral());

	if(trigger_plots){
		delete trigger_plots;
		trigger_plots = NULL;
	}

	if(btag_plots){
		delete btag_plots;
		btag_plots = NULL;
	}

	if(mass_plots){
		delete mass_plots;
		mass_plots = NULL;
	}

	if(lept_iso_plots){
		delete lept_iso_plots;
		lept_iso_plots = NULL;
	}
	
	if(met_plots != NULL){
		delete met_plots;
		met_plots = NULL;
	}

	if(abcd_plots){
		delete abcd_plots;
		abcd_plots = NULL;
	}

	if(e_selector != NULL){
		delete e_selector;
		e_selector=NULL;
	}
	if(all_e_selector != NULL){
		delete all_e_selector;
		all_e_selector=NULL;
	}
	if(mu_selector != NULL){
		delete mu_selector;
		mu_selector=NULL;
	}

	if(jet_selector != NULL){
		delete jet_selector;
		jet_selector = NULL;
	}

        if(jets != NULL){
                delete jets;
                jets = NULL;
        }
        if(isolated_electrons != NULL){
                delete isolated_electrons;
                isolated_electrons = NULL;
        }
        if(all_electrons != NULL){
                delete all_electrons;
                all_electrons = NULL;
        }
        if(isolated_muons != NULL){
                delete isolated_muons;
                isolated_muons = NULL;
        }
	
	if(mass_jet_match != NULL){
		delete mass_jet_match;
		mass_jet_match = NULL;
	}
	if(gen_match != NULL){
		delete gen_match;
		gen_match = NULL;
	}
	if(reco_gen_match != NULL){
		delete reco_gen_match;
		reco_gen_match = NULL;
	}

	if(bjet_finder != NULL){
		delete bjet_finder;
		bjet_finder = NULL;	
	}

	if(METCor != NULL){
		delete METCor;
		METCor = NULL;
	}

	if(mva_producer != NULL){
		delete mva_producer;
		mva_producer = NULL;
	}
}

void PlotGenerator::set_lepton_selector_cuts()
{
	if(cuts != NULL){
		e_selector->set_max_trackiso(cuts->get_max_e_trackiso());
		e_selector->set_max_caliso(cuts->get_max_e_caliso());
		e_selector->set_max_ecaliso(cuts->get_max_e_ecaliso());
		e_selector->set_max_hcaliso(cuts->get_max_e_hcaliso());
		e_selector->set_max_hcal_veto_cone(cuts->get_max_e_hcal_veto_cone());
		e_selector->set_max_ecal_veto_cone(cuts->get_max_e_hcal_veto_cone());
		e_selector->set_min_pt(cuts->get_min_e_pt());
		e_selector->set_min_et(cuts->get_min_e_et());
		e_selector->set_min_dR(cuts->get_min_e_dR());
		e_selector->set_min_relIso(cuts->get_min_e_relIso());
		e_selector->set_max_d0(cuts->get_max_e_d0());
		e_selector->set_max_d0sig(cuts->get_max_e_d0sig());
		e_selector->set_max_chi2(cuts->get_max_e_chi2());
		e_selector->set_min_nHits(cuts->get_min_e_nHits());
		e_selector->set_electronID(cuts->get_e_electronID());
		e_selector->set_max_eta(cuts->get_max_e_eta());
		e_selector->set_lepton_type(cuts->get_e_type());

		mu_selector->set_max_trackiso(cuts->get_max_mu_trackiso());
		mu_selector->set_max_caliso(cuts->get_max_mu_caliso());
		mu_selector->set_max_ecaliso(cuts->get_max_mu_ecaliso());
		mu_selector->set_max_hcaliso(cuts->get_max_mu_hcaliso());
		mu_selector->set_max_hcal_veto_cone(cuts->get_max_mu_hcal_veto_cone());
		mu_selector->set_max_ecal_veto_cone(cuts->get_max_mu_hcal_veto_cone());
		mu_selector->set_min_pt(cuts->get_min_mu_pt());
		mu_selector->set_min_et(cuts->get_min_mu_et());
		mu_selector->set_min_dR(cuts->get_min_mu_dR());
		mu_selector->set_min_relIso(cuts->get_min_mu_relIso());
		mu_selector->set_max_d0(cuts->get_max_mu_d0());
		mu_selector->set_max_d0sig(cuts->get_max_mu_d0sig());
		mu_selector->set_max_chi2(cuts->get_max_mu_chi2());
		mu_selector->set_min_nHits(cuts->get_min_mu_nHits());
		mu_selector->set_electronID(cuts->get_mu_electronID());
		mu_selector->set_max_eta(cuts->get_max_mu_eta());
		mu_selector->set_lepton_type(cuts->get_mu_type());

		jet_selector->set_min_e_dR(cuts->get_min_jet_e_dR());
		jet_selector->set_max_eta(cuts->get_max_jet_eta());
		jet_selector->set_min_pt(cuts->get_min_jet_pt());
		jet_selector->change_JES(cuts->get_JES_factor());
	}
}

void PlotGenerator::unset_lepton_selector_cuts()
{
	e_selector->set_max_trackiso(NULL);
	e_selector->set_max_caliso(NULL);
	e_selector->set_max_ecaliso(NULL);
	e_selector->set_max_hcaliso(NULL);
	e_selector->set_max_hcal_veto_cone(NULL);
	e_selector->set_max_ecal_veto_cone(NULL);
	e_selector->set_min_pt(NULL);
	e_selector->set_min_et(NULL);
	e_selector->set_min_dR(NULL);
	e_selector->set_min_relIso(NULL);
	e_selector->set_max_d0(NULL);
	e_selector->set_max_d0sig(NULL);
	e_selector->set_max_chi2(NULL);
	e_selector->set_min_nHits(NULL);
	e_selector->set_electronID(NULL);
	e_selector->set_max_eta(NULL);

	mu_selector->set_max_trackiso(NULL);
	mu_selector->set_max_caliso(NULL);
	mu_selector->set_max_ecaliso(NULL);
	mu_selector->set_max_hcaliso(NULL);
	mu_selector->set_max_hcal_veto_cone(NULL);
	mu_selector->set_max_ecal_veto_cone(NULL);
	mu_selector->set_min_pt(NULL);
	mu_selector->set_min_et(NULL);
	mu_selector->set_min_dR(NULL);
	mu_selector->set_min_relIso(NULL);
	mu_selector->set_max_d0(NULL);
	mu_selector->set_max_d0sig(NULL);
	mu_selector->set_max_chi2(NULL);
	mu_selector->set_min_nHits(NULL);
	mu_selector->set_electronID(NULL);
	mu_selector->set_max_eta(NULL);

	jet_selector->set_min_pt(NULL);
	jet_selector->set_max_eta(-1.0);
	jet_selector->change_JES(-1.0);
}

void PlotGenerator::set_handles(edm::Handle<edm::View<pat::Muon> > analyser_muons,
                           edm::Handle<edm::View<pat::Jet> > analyser_jets,
                           edm::Handle<edm::View<pat::Electron> > analyser_electrons,
			   edm::Handle<edm::View<pat::MET> > analyser_mets,
			   edm::Handle<TtSemiLeptonicEvent> semiLepEvt,
			   edm::Handle<int> hypoClassKeyHandle,
			   edm::Handle<TtGenEvent> genEvt,
			   edm::Handle<edm::TriggerResults> HLTR,
			   edm::Handle<reco::GenParticleCollection> genParticles,
			   edm::Handle<reco::BeamSpot> recoBeamSpotHandle)
{
	ht = -1;

	electrons = analyser_electrons;
	muons = analyser_muons;
	uncut_jets = analyser_jets;
	this->semiLepEvt = semiLepEvt;
	this->hypoClassKeyHandle = hypoClassKeyHandle;
	this->genEvt = genEvt;
	this->HLTR = HLTR;
	this->genParticles = genParticles;
	beamSpotHandle = recoBeamSpotHandle;

        if(isolated_electrons != NULL){
                delete isolated_electrons;
                isolated_electrons = NULL;
        }
        isolated_electrons = e_selector->get_leptons(electrons, uncut_jets, recoBeamSpotHandle);

        if(all_electrons != NULL){
                delete all_electrons;
                all_electrons = NULL;
        }

        all_electrons = all_e_selector->get_leptons(electrons, uncut_jets, recoBeamSpotHandle);

        if(isolated_muons != NULL){
                delete isolated_muons;
                isolated_muons = NULL;
        }
        isolated_muons = mu_selector->get_leptons(muons, uncut_jets, recoBeamSpotHandle);

	if(jets != NULL){
		delete jets;
		jets = NULL;
	}

	jets = jet_selector->get_jets(analyser_jets, all_electrons);
	mets = analyser_mets;

        if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
		pat::Particle *muon = new pat::Particle();
		muon->setP4((*isolated_muons)[0].p4());
                METCor->set_handles(analyser_mets, muon);
                corrected_mets = METCor->get_MET();
		delete muon;
		muon = NULL;
        }
        else if(isolated_electrons->size() == 1 && isolated_muons->size() == 0){
		pat::Particle *electron = new pat::Particle();
		electron->setP4((*isolated_electrons)[0].p4());
                METCor->set_handles(analyser_mets, electron);
                corrected_mets = METCor->get_MET();
		delete electron;
		electron = NULL;
        }else{
                METCor->set_handles(mets);
                corrected_mets = METCor->get_uncorrected_MET();
	}

	reco_gen_match->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets,genParticles,muons,electrons);
	mass_jet_match->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets,genParticles,muons,electrons);
	gen_match->fill_events_from_collection(genParticles);
	bjet_finder->set_handles(jets);

	/*
	 *	setting handles for plot classes
	 */
	met_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	met_plots->set_met_handle(mets);

//	abcd_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);

	trigger_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	trigger_plots->set_muon_handle(muons);
	trigger_plots->set_hltr_handle(HLTR);

	lept_iso_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	lept_iso_plots->set_genEvt_handle(genEvt);

	btag_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);

	mass_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	mass_plots->set_genEvt_handle(genEvt);
	mass_plots->set_semiLepEvt_handle(semiLepEvt);
	mass_plots->set_hypoClassKey_handle(hypoClassKeyHandle);

	mva_producer->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
}

void PlotGenerator::plot()
{

	/*
	 *	Plots with applied cuts
	 */
	// plot if cuts are applied (cuts != NULL) and event passes cut
	// cirteria or if cuts are not applied (cuts == NULL)
	if((cuts != NULL && cuts->cut() == false)
	   || cuts == NULL){

		plot_jets();
 		plot_muons();
 		plot_electrons();
 		plot_ht();
  		plot_dR();
  		plot_muon_quality();
  		plot_electron_quality();
		//determine sigmas,dR cut
		//analyse_recogen_mindD();
		//analyse reco gen matching with dR cut applied
		analyse_recogen_matchedjets();

		/*
		 *	plot individual plot classes
		 */

		met_plots->plot();
//		abcd_plots->plot();
		trigger_plots->plot();
		lept_iso_plots->plot();
		btag_plots->plot();
		mass_plots->plot();
		
		mva_producer->print_MVA_input();
	
		histos1d[("event_counter"+id).c_str()]->Fill(1);
	}

	/*
	 *	Plots created even for events that don't pass the cuts
	 */
	histos1d[("event_counter"+id).c_str()]->Fill(0);

}

void PlotGenerator::plot_muon_quality()
{
	for(std::vector<pat::Muon>::iterator muon_iter = isolated_muons->begin();
		muon_iter != isolated_muons->end();
		++muon_iter){
		reco::TrackRef track  = muon_iter->track();
		reco::TrackRef globaltrack  = muon_iter->globalTrack();

		if(!track.isNull()){
			if(!globaltrack.isNull())
				histos1d[("muon_chi2"+id).c_str()]->Fill(muon_iter->globalTrack()->chi2()/muon_iter->globalTrack()->ndof());
			histos1d[("muon_nHits"+id).c_str()]->Fill(muon_iter->track()->numberOfValidHits());
			histos1d[("muon_d0_wo_BScorr"+id).c_str()]->Fill((muon_iter->track()->d0()));
			histos2d[("muon_d0_vs_phi_wo_BScorr"+id).c_str()]->Fill(muon_iter->phi(),(muon_iter->track()->d0()));

		}
		double d0 = -1000;
		
		if(beamSpotHandle.isValid()){
			if(!track.isNull() && track.isAvailable()){
				reco::BeamSpot beamSpot=*beamSpotHandle;
				math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
				d0 = -1.*track->dxy(point);
				histos2d[("muon_d0_vs_phi"+id).c_str()]->Fill(muon_iter->phi(),d0);
				histos1d[("muon_d0"+id).c_str()]->Fill(d0);
			}
		}
	}
}


void PlotGenerator::plot_electron_quality()
{
	for(std::vector<pat::Electron>::iterator electron_iter = isolated_electrons->begin();
		electron_iter != isolated_electrons->end();
		++electron_iter){
		reco::GsfTrackRef track  = electron_iter->gsfTrack();

		if(!track.isNull()){
			histos1d[("electron_chi2"+id).c_str()]->Fill(electron_iter->gsfTrack()->chi2()/electron_iter->gsfTrack()->ndof());
			histos1d[("electron_nHits"+id).c_str()]->Fill(electron_iter->gsfTrack()->numberOfValidHits());
			histos1d[("electron_d0_wo_BScorr"+id).c_str()]->Fill((electron_iter->gsfTrack()->d0()));
			histos2d[("electron_d0_vs_phi_wo_BScorr"+id).c_str()]->Fill(electron_iter->phi(),(electron_iter->gsfTrack()->d0()));

		}
		double d0 = -1000;
		
		if(beamSpotHandle.isValid()){
			if(!track.isNull() && track.isAvailable()){
				reco::BeamSpot beamSpot=*beamSpotHandle;
				math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
				d0 = -1.*track->dxy(point);
				histos2d[("electron_d0_vs_phi"+id).c_str()]->Fill(electron_iter->phi(),d0);
				histos1d[("electron_d0"+id).c_str()]->Fill(d0);
			}
		}
	}
}
//plot pt, eta, and numbet of jets
void PlotGenerator::plot_jets()
{
	int njets = jets->size();
	
	if(njets >= 1){
		histos1d[("jet1_pt"+id).c_str()]->Fill((*jets)[0].pt());
		histos1d[("jet1_eta"+id).c_str()]->Fill((*jets)[0].eta());
		histos2d[("jet1_pt_vs_et"+id).c_str()]->Fill((*jets)[0].pt(),(*jets)[0].eta());
	}

	if(njets >= 2){
		histos1d[("jet2_pt"+id).c_str()]->Fill((*jets)[1].pt());
		histos1d[("jet2_eta"+id).c_str()]->Fill((*jets)[1].eta());
		histos2d[("jet2_pt_vs_et"+id).c_str()]->Fill((*jets)[1].pt(),(*jets)[1].eta());

		histos2d[("jet1_pt_vs_jet2_pt"+id).c_str()]->Fill((*jets)[1].pt(),(*jets)[0].pt());
	}

	if(njets >= 3){
		histos1d[("jet3_pt"+id).c_str()]->Fill((*jets)[2].pt());
		histos1d[("jet3_eta"+id).c_str()]->Fill((*jets)[2].eta());
	}
	if(njets >= 4){
		histos1d[("jet4_pt"+id).c_str()]->Fill((*jets)[3].pt());
		histos1d[("jet4_eta"+id).c_str()]->Fill((*jets)[3].eta());
	}
	

	for(std::vector<pat::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		
		histos2d[("jet_pt_vs_et"+id).c_str()]->Fill(jet_iter->pt(),jet_iter->et());
		histos1d[("jet_pt"+id).c_str()]->Fill(jet_iter->pt());
		histos1d[("jet_eta"+id).c_str()]->Fill(jet_iter->eta());
		histos1d[("jet_phi"+id).c_str()]->Fill(jet_iter->phi());
	}
	
	histos1d[("jet_number"+id).c_str()]->Fill(njets);
}

 // Plot eta,pt and number of muons
void PlotGenerator::plot_muons()
{
	// Get back vector of muons that passed all cuts in LeptonSelector class
   
	for(std::vector<pat::Muon>::iterator isolated_mu = isolated_muons->begin();
	    isolated_mu!=isolated_muons->end(); 
	    ++isolated_mu){
                histos1d[("muon_pt"+id).c_str()]->Fill(isolated_mu->pt());
	        histos1d[("muon_eta"+id).c_str()]->Fill(isolated_mu->eta());
	        histos1d[("muon_phi"+id).c_str()]->Fill(isolated_mu->phi());
        }
	
	histos1d[("muon_number"+id).c_str()]->Fill(isolated_muons->size());

}

// Plot eta,pt and number of electrons
void PlotGenerator::plot_electrons()
{
	// Get back vector of electrons that passed all cuts in LeptonSelector class
	
	for(std::vector<pat::Electron>::iterator isolated_e = isolated_electrons->begin();isolated_e != isolated_electrons->end();++isolated_e){
                histos1d[("electron_pt"+id).c_str()]->Fill(isolated_e->pt());
	        histos1d[("electron_eta"+id).c_str()]->Fill(isolated_e->eta());
	        histos1d[("electron_phi"+id).c_str()]->Fill(isolated_e->phi());
        }

	histos1d[("electron_number"+id).c_str()]->Fill(isolated_electrons->size());

}

// Plot transverse energy:sum of all jets and the muon pt
void PlotGenerator::plot_ht()
{
	if(ht == -1)
		ht_calc->get_ht();
	
	histos1d[("Ht"+id).c_str()]->Fill(ht+corrected_mets->begin()->pt());
	histos1d[("Ht_wo_MET"+id).c_str()]->Fill(ht);
}

void PlotGenerator::analyse_recogen_mindD()
{
	if(!reco_gen_match->is_geninfo_available()){
		return;
	}
	

	std::map<std::string,int> mindD_jet_ids = reco_gen_match->get_recojets_id_no_dRcut(); //no dR cut
	std::map<std::string,reco::GenParticle*> genquark;

	//truth matching  
	if(gen_match != NULL && gen_match->is_ok() && gen_match->get_decay_mode() == 1){
		
		//All from hadronic top decay
		if(!(gen_match->t_decay_is_leptonic())){	
			genquark["Wquark1"] = gen_match->get_particle("Wplus_dp1");
			genquark["Wquark2"] = gen_match->get_particle("Wplus_dp2");
			genquark["hadb"] = gen_match->get_particle("b");
			genquark["lepb"] = gen_match->get_particle("bbar");
		}
		
		else if(!(gen_match->tbar_decay_is_leptonic())){
			genquark["Wquark1"]= gen_match->get_particle("Wminus_dp1");
			genquark["Wquark2"]= gen_match->get_particle("Wminus_dp2");
			genquark["hadb"]= gen_match->get_particle("bbar");
			genquark["lepb"]= gen_match->get_particle("b");
		  
		}
	}

	//determining sigmas
  	double const sigma_dphi = 0.03;
  	double const sigma_deta = 0.03;
  	double const sigma_dpt = 0.16;
	for(std::map<std::string,reco::GenParticle*>::iterator quark = genquark.begin();quark!=genquark.end();++quark){
		if(mindD_jet_ids[quark->first] != -1){
			double dR = ROOT::Math::VectorUtil::DeltaR(quark->second->p4(),(*jets)[mindD_jet_ids[quark->first]].p4());
			if(dR < 1){
			double dPt = (((*jets)[mindD_jet_ids[quark->first]].pt()- quark->second->pt())/quark->second->pt());
			double deta = ((*jets)[mindD_jet_ids[quark->first]].eta() - quark->second->eta());
			double dphi = ((*jets)[mindD_jet_ids[quark->first]].phi() - quark->second->phi());
			double dD = ((deta*deta)/(sigma_deta*sigma_deta)+(dphi*dphi)/(sigma_dphi*sigma_dphi)+(dPt*dPt)/(sigma_dpt*sigma_dpt)); 	
			histos1d[("jet_dR_genreco"+id).c_str()]->Fill(dR);
			histos1d[("jet_dPt_genreco"+id).c_str()]->Fill(dPt);
			histos2d[("jet_delPt_vs_dR"+id).c_str()]->Fill(dR,dPt);
			histos2d[("jet_dR_vs_Pt"+id).c_str()]->Fill((*jets)[mindD_jet_ids[quark->first]].pt(),dR);
			histos2d[("jet_delPt_vs_dD"+id).c_str()]->Fill(dR,dD);
			histos1d[("jet_deta_genreco"+id).c_str()]->Fill(deta);
			histos1d[("jet_dphi_genreco"+id).c_str()]->Fill(dphi);
			}
		}

	}
 
	if (reco_gen_match->dR_Wquarks() != -1)
		histos1d[("dR_Wquarks"+id).c_str()]->Fill(reco_gen_match->dR_Wquarks());
	if (reco_gen_match->min_dR_genquarks() != -1)
		histos1d[("min_dR_genquarks"+id).c_str()]->Fill(reco_gen_match->min_dR_genquarks());
	
	
	
	//find md dR for reco jets
	int njets = jets->size();
	double min_dR = -1;
	for(int i=0;i < njets; ++i){
		for(int j=0;j < njets; ++j){
			if(i != j){
				
				double current_dR = ROOT::Math::VectorUtil::DeltaR((*jets)[i].p4(),(*jets)[j].p4());
				if (current_dR <= min_dR ||min_dR == -1)
					min_dR = current_dR;
			}
		}
	}
	histos1d[("jet_reco_min_dR"+id).c_str()]->Fill(min_dR);
	
}

void PlotGenerator::analyse_recogen_matchedjets()
{
	if(!reco_gen_match->is_geninfo_available()){
		return;
	}

	std::map<std::string,int> matched_jet_ids = reco_gen_match->get_matched_recojets_id(); //min_dD with dR cut
	std::map<std::string,reco::GenParticle*> genquark;

	//truth matching  
	if(gen_match != NULL && gen_match->is_ok() && gen_match->get_decay_mode() == 1){
		
		//All from hadronic top decay
		if(!(gen_match->t_decay_is_leptonic())){	
			genquark["Wquark1"] = gen_match->get_particle("Wplus_dp1");
			genquark["Wquark2"] = gen_match->get_particle("Wplus_dp2");
			genquark["hadb"] = gen_match->get_particle("b");
			genquark["lepb"] = gen_match->get_particle("bbar");
		}
		
		else if(!(gen_match->tbar_decay_is_leptonic())){
			genquark["Wquark1"]= gen_match->get_particle("Wminus_dp1");
			genquark["Wquark2"]= gen_match->get_particle("Wminus_dp2");
			genquark["hadb"]= gen_match->get_particle("bbar");
			genquark["lepb"]= gen_match->get_particle("b");
		  
		}
	}

	//count number of reco jets matched to gen level
	int num_had_jets = 0, num_top_jets = 0;
	double sum_dR = 0;
	for(std::map<std::string,int>::iterator matched_jet = matched_jet_ids.begin();matched_jet!=matched_jet_ids.end();++matched_jet){
		if(matched_jet->second != -1){
			if(matched_jet->first == "lepb"){
				++num_top_jets;
			} 
			else if(matched_jet->first != "lepb"){
				++num_had_jets;
				++num_top_jets;
			}


			double dR = ROOT::Math::VectorUtil::DeltaR((*jets)[matched_jet->second].p4(),genquark[matched_jet->first]->p4());
			sum_dR = sum_dR + dR;

		}
	}


	double mean_dR = sum_dR/num_top_jets; 
	
	histos1d[("top_had_jets_genmatched"+id).c_str()]->Fill(num_had_jets);
	histos1d[("top_jets_genmatched"+id).c_str()]->Fill(num_top_jets);
	histos2d[("jet_mean_dR_vs_numjets"+id).c_str()]->Fill(num_top_jets,mean_dR);

	
	/*
	 *	test hypothesis: 2 jets closest to W mass in event are jets
	 *	that come really from W decay
	 */

	int njets = jets->size();

	if(njets < 2)
		return;

	double min_mass_diff = -1;
	double j1_id = -1;
	double j2_id = -1;
	double W_mass = 80.0;
	for(int i = 0; i < njets; ++i){
		for(int j = i+1; j < njets; ++j){
			if(i != j){
				double curr_diff = ((*jets)[i].p4() + (*jets)[j].p4()).mass() - W_mass;
				if(curr_diff < min_mass_diff || min_mass_diff == -1){
					min_mass_diff = curr_diff;
					j1_id = i;
					j2_id = j;
				}
			}
		}
	}

	if((j1_id == matched_jet_ids["Wquark1"]) && (j2_id == matched_jet_ids["Wquark2"]) ||
	   (j1_id == matched_jet_ids["Wquark2"]) && (j2_id == matched_jet_ids["Wquark1"])){
		// both W jets correctly identified
		histos1d[("W_jets_assignment"+id).c_str()]->Fill(2);
	}else if((j1_id == matched_jet_ids["Wquark1"]) || (j2_id == matched_jet_ids["Wquark1"]) ||
		 (j1_id == matched_jet_ids["Wquark2"]) || (j2_id == matched_jet_ids["Wquark2"])){
		// one jet correctly identified
		histos1d[("W_jets_assignment"+id).c_str()]->Fill(1);
	}else{
		// both W jets wrong
		histos1d[("W_jets_assignment"+id).c_str()]->Fill(0);
	}

	/*
	 *	test hypothesis: highest 4 pt jets in event are from top decay
	 */
	if(matched_jet_ids["Wquark1"] != -1 &&
	   matched_jet_ids["Wquark2"] != -1 &&
	   matched_jet_ids["hadb"] != -1 &&
	   matched_jet_ids["lepb"] != -1){
		histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(matched_jet_ids["Wquark1"]);
		histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(matched_jet_ids["Wquark2"]);
		histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(matched_jet_ids["hadb"]);
		histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(matched_jet_ids["lepb"]);
	}
	
}
// Plot delta R between muon and closest jet
void PlotGenerator::plot_dR()
{
	for(std::vector<pat::Muon>::const_iterator mu_iter = isolated_muons->begin();
	    mu_iter != isolated_muons->end();
	    mu_iter++)
	{
		double min_dR = -1;
		double min_deta = -1;
		double min_dphi = -1;
		for(edm::View<pat::Jet>::const_iterator jet_iter = uncut_jets->begin();
		    jet_iter!=uncut_jets->end();
		    ++jet_iter)
			{
				double dR = fabs(ROOT::Math::VectorUtil::DeltaR(mu_iter->p4(),jet_iter->p4()));
				if ((dR < min_dR) || (min_dR == -1))
					min_dR = dR;
				double deta = fabs(mu_iter->eta()-jet_iter->eta());
				if ((deta < min_deta) || (min_deta == -1))
					min_deta = deta;
				double dphi = fabs(ROOT::Math::VectorUtil::DeltaPhi(mu_iter->p4(),jet_iter->p4()));
				if ((dphi < min_dphi) || (min_dphi == -1))
					min_dphi = dphi;
				
			}
		histos1d[("min_mu_dR"+id).c_str()]->Fill(min_dR);
		histos1d[("min_mu_eta"+id).c_str()]->Fill(min_deta);
		histos1d[("min_mu_phi"+id).c_str()]->Fill(min_dphi);
		return;
	}

	for(std::vector<pat::Electron>::const_iterator e_iter = isolated_electrons->begin();
	    e_iter != isolated_electrons->end();
	    e_iter++)
		{
			double min_dR = -1;
			for(std::vector<pat::Jet>::iterator jet_iter = jets->begin();
			    jet_iter!=jets->end();
			    ++jet_iter)
				{
					double dR = ROOT::Math::VectorUtil::DeltaR(e_iter->p4(),jet_iter->p4());
					if ((dR < min_dR) || (min_dR = -1))
						min_dR = dR;
				}
			histos1d[("min_e_dR"+id).c_str()]->Fill(min_dR);
		}
	
}

//Book all the histograms
void PlotGenerator::book_histos()
{
	// file service to write histos automatically to root files
	edm::Service<TFileService> fs;
	/*
	 *	1D Histos
	 */
	histos1d[("W_jets_assignment"+id).c_str()]=fs->make<TH1D>(("W_jets_assignment"+id).c_str(),"correctly identified W jets closest to W mass",5,-0.5,4.5);

	histos1d[("jet_number"+id).c_str()]=fs->make<TH1D>(("jet_number"+id).c_str(),"jet multiplicity",10,-0.5,9.5);
	histos1d[("muon_number"+id).c_str()]=fs->make<TH1D>(("muon_number"+id).c_str(),"muon multiplicity ",10,-0.5,9.5);
	histos1d[("muon_pt"+id).c_str()]=fs->make<TH1D>(("muon_pt"+id).c_str(),"p_{T} of muons in an event",70,0,350);
	histos1d[("muon_eta"+id).c_str()]=fs->make<TH1D>(("muon_eta"+id).c_str(),"eta of muons in an event",40,-6.0,6.0);
	histos1d[("muon_phi"+id).c_str()]=fs->make<TH1D>(("muon_phi"+id).c_str(),"phi of muons in an event",40,-4.0,4.0);
	histos1d[("muon_d0"+id).c_str()]=fs->make<TH1D>(("muon_d0"+id).c_str(),"d0 of muons in an event",30,-0.3,0.3);
	histos2d[("muon_d0_vs_phi"+id).c_str()]=fs->make<TH2D>(("muon_d0_vs_phi"+id).c_str(),"d0 vs phi of muons in an event",80,-4,4,80,-0.4,0.4);
	histos2d[("muon_d0_vs_phi_wo_BScorr"+id).c_str()]=fs->make<TH2D>(("muon_d0_vs_phi_wo_BScorr"+id).c_str(),"d0 w/o beamspot correction vs phi of muons in an event",80,-4,4,80,-0.4,0.4);
	histos1d[("muon_d0_wo_BScorr"+id).c_str()]=fs->make<TH1D>(("muon_d0_wo_BScorr"+id).c_str(),"d0 w/o beamspot correction of muons in an event",30,-0.3,0.3);
	histos1d[("muon_nHits"+id).c_str()]=fs->make<TH1D>(("muon_nHits"+id).c_str(),"number of Hits in the tracker of muons in an event",30,-0.5,29.5);
	histos1d[("muon_chi2"+id).c_str()]=fs->make<TH1D>(("muon_chi2"+id).c_str(),"normalised #chi^{2} of muons in an event",50,0.0,50);
	histos1d[("electron_number"+id).c_str()]=fs->make<TH1D>(("electron_number"+id).c_str(),"electron multiplicity ",10,-0.5,9.5);
	histos1d[("electron_pt"+id).c_str()]=fs->make<TH1D>(("electron_pt"+id).c_str(),"p_{T} of electron in an event",70,0,350);
	histos1d[("electron_eta"+id).c_str()]=fs->make<TH1D>(("electron_eta"+id).c_str(),"eta of electron in an event",30,-6.0,6.0);
	histos1d[("electron_phi"+id).c_str()]=fs->make<TH1D>(("electron_phi"+id).c_str(),"phi of electrons in an event",50,-4.0,4.0);
	histos1d[("electron_d0"+id).c_str()]=fs->make<TH1D>(("electron_d0"+id).c_str(),"d0 of electrons in an event",30,-0.3,0.3);
	histos2d[("electron_d0_vs_phi"+id).c_str()]=fs->make<TH2D>(("electron_d0_vs_phi"+id).c_str(),"d0 vs phi of electrons in an event",80,-4,4,80,-0.4,0.4);
	histos2d[("electron_d0_vs_phi_wo_BScorr"+id).c_str()]=fs->make<TH2D>(("electron_d0_vs_phi_wo_BScorr"+id).c_str(),"d0 w/o beamspot correction vs phi of electrons in an event",80,-4,4,80,-0.4,0.4);
	histos1d[("electron_d0_wo_BScorr"+id).c_str()]=fs->make<TH1D>(("electron_d0_wo_BScorr"+id).c_str(),"d0 w/o beamspot correction of electrons in an event",30,-0.3,0.3);
	histos1d[("electron_nHits"+id).c_str()]=fs->make<TH1D>(("electron_nHits"+id).c_str(),"number of Hits in the tracker of electrons in an event",30,-0.5,29.5);
	histos1d[("electron_chi2"+id).c_str()]=fs->make<TH1D>(("electron_chi2"+id).c_str(),"normalised #chi^{2} of electrons in an event",50,0.0,50);

	histos1d[("Ht"+id).c_str()]=fs->make<TH1D>(("Ht"+id).c_str(),"Ht: Sum pt of all jets and muon and MET",80,0,2000);
	histos1d[("Ht_wo_MET"+id).c_str()]=fs->make<TH1D>(("Ht_wo_MET"+id).c_str(),"Ht: Sum pt of all jets and muon",80,0,2000);

	histos1d[("jet_pt"+id).c_str()]=fs->make<TH1D>(("jet_pt"+id).c_str(),"p_{T} of jets in an event",50,0,500);
	histos1d[("jet_eta"+id).c_str()]=fs->make<TH1D>(("jet_eta"+id).c_str(),"eta of jets in an event",15,-2.5,2.5);
	histos1d[("jet_phi"+id).c_str()]=fs->make<TH1D>(("jet_phi"+id).c_str(),"phi of jets in an event",50,-4.0,4.0);
  	histos1d[("jet1_pt"+id).c_str()]=fs->make<TH1D>(("jet1_pt"+id).c_str(),"p_{T} of leading jet",50,0,500);
	histos1d[("jet1_eta"+id).c_str()]=fs->make<TH1D>(("jet1_eta"+id).c_str(),"eta of leading jet",30,-6.0,6.0);
	histos1d[("jet2_pt"+id).c_str()]=fs->make<TH1D>(("jet2_pt"+id).c_str(),"p_{T} of jet 2 ",50,0,500);
	histos1d[("jet2_eta"+id).c_str()]=fs->make<TH1D>(("jet2_eta"+id).c_str(),"eta of jet 2 ",30,-6.0,6.0);
	histos1d[("jet3_pt"+id).c_str()]=fs->make<TH1D>(("jet3_pt"+id).c_str(),"p_{T} of jet 3",50,0,500);
	histos1d[("jet3_eta"+id).c_str()]=fs->make<TH1D>(("jet3_eta"+id).c_str(),"eta of jet 3 ",30,-6.0,6.0);
	histos1d[("jet4_pt"+id).c_str()]=fs->make<TH1D>(("jet4_pt"+id).c_str(),"p_{T} of jet 4",50,0,500);
	histos1d[("jet4_eta"+id).c_str()]=fs->make<TH1D>(("jet4_eta"+id).c_str(),"eta of jet 4 ",30,-6.0,6.0);

	histos2d[("jet_pt_vs_et"+id).c_str()]=fs->make<TH2D>(("jet_pt_vs_et"+id).c_str(),"pt vs et for all jets",85, 0, 500, 85,0,500);
	histos2d[("jet1_pt_vs_et"+id).c_str()]=fs->make<TH2D>(("jet1_pt_vs_et"+id).c_str(),"pt vs et for leading jet",85, 0, 500, 85,0,500);
	histos2d[("jet2_pt_vs_et"+id).c_str()]=fs->make<TH2D>(("jet2_pt_vs_et"+id).c_str(),"pt vs et for jet 2",85, 0, 500, 85,0,500);
	histos2d[("jet1_pt_vs_jet2_pt"+id).c_str()]=fs->make<TH2D>(("jet1_pt_vs_jet2_pt"+id).c_str(),"1st jet pt vs 2nd jet pt",85, 0, 500, 85,0,500);

	histos1d[("jet_pt_id_truth"+id).c_str()]=fs->make<TH1D>(("jet_pt_id_truth"+id).c_str(),"id of truth matched jets in event",10,-0.5,9.5);

	//Figuring out whatdR cut should be between gen quark and reco jet, and sigmas for dD equation
	histos2d[("jet_mean_dR_vs_numjets"+id).c_str()]=fs->make<TH2D>(("jet_mean_dR_vs_numjets"+id).c_str(),"mean dR vs jets found",6, -0.5, 5.5,100,0,5);
	histos1d[("jet_reco_min_dR"+id).c_str()]=fs->make<TH1D>(("jet_reco_min_dR"+id).c_str(),"deltaR between closest reco jets",50,0,7);
	histos2d[("jet_delPt_vs_dR"+id).c_str()]=fs->make<TH2D>(("jet_delPt_vs_dR"+id).c_str(),"jet delPt vs dR",210, 0, 7,400, -2,2);
	histos2d[("jet_dR_vs_Pt"+id).c_str()]=fs->make<TH2D>(("jet_dR_vs_Pt"+id).c_str(),"jet reco Pt vs gen-reco dR",600,0, 300, 210, 0,7);
	histos2d[("jet_delPt_vs_dD"+id).c_str()]=fs->make<TH2D>(("jet_delPt_vs_dD"+id).c_str(),"jet delPt vs dD",210, 0, 7,400, -2,2);
	histos1d[("jet_dR_genreco"+id).c_str()]=fs->make<TH1D>(("jet_dR_genreco"+id).c_str(),"jet_dR_genreco",50,0,7);
	histos1d[("jet_dPt_genreco"+id).c_str()]=fs->make<TH1D>(("jet_dPt_genreco"+id).c_str(),"jet_dPt_genreco dPt = dpt/pt_gen",100,-2,2);
	histos1d[("jet_deta_genreco"+id).c_str()]=fs->make<TH1D>(("jet_deta_genreco"+id).c_str(),"jet_deta_genreco ",200,-1,1);
	histos1d[("jet_dphi_genreco"+id).c_str()]=fs->make<TH1D>(("jet_dphi_genreco"+id).c_str(),"jet_dphi_genreco ",200,-1,1);
	histos1d[("dR_Wquarks"+id).c_str()]=fs->make<TH1D>(("dR_Wquarks"+id).c_str(),"deltaR between W quarks",140,0,7);
	histos1d[("min_dR_genquarks"+id).c_str()]=fs->make<TH1D>(("min_dR_genquarks"+id).c_str(),"deltaR between closest two quarks",140,0,7);


	// 0 = all events, 1 = events passing cuts
	histos1d[("event_counter"+id).c_str()]=fs->make<TH1D>(("event_counter"+id).c_str(),"processed events",5,-0.5,4.5);

	histos1d[("min_mu_dR"+id).c_str()]=fs->make<TH1D>(("min_mu_dR"+id).c_str(),"deltaR between muon(s) and closest jet",50,0,7);
	histos1d[("min_mu_eta"+id).c_str()]=fs->make<TH1D>(("min_mu_eta"+id).c_str(),"deltaR between muon(s) and closest jet",50,-8,8);
	histos1d[("min_mu_phi"+id).c_str()]=fs->make<TH1D>(("min_mu_phi"+id).c_str(),"deltaR between muon(s) and closest jet",50,-8,8);

	histos1d[("min_e_dR"+id).c_str()]=fs->make<TH1D>(("min_e_dR"+id).c_str(),"deltaR for electron(s) and closest jet",50,0,7);
	histos1d[("jet_e_dR"+id).c_str()]=fs->make<TH1D>(("jet_e_dR"+id).c_str(),"deltaR for uncut electrons and uncut jet",50,0,7);

	histos1d[("top_had_jets_genmatched"+id).c_str()]=fs->make<TH1D>(("top_had_jets_genmatched"+id).c_str(),"Jets passing event which are really from semi-lep hadronic decay channel",10,-0.5, 9.5);
	histos1d[("top_jets_genmatched"+id).c_str()]=fs->make<TH1D>(("top_jets_genmatched"+id).c_str(),"Jets passing event which are really from semi-lep decay channel",10,-0.5, 9.5);
	/*
	 *=========================================================================================
	 */

	/*
	 *	2D Histos
	 */
}

void PlotGenerator::apply_cuts(Cuts *mycuts)
{
	cuts = mycuts;
	set_lepton_selector_cuts();
	btag_plots->set_btag_cuts(cuts->get_min_btag(),cuts->get_name_btag());		
	mass_reco = cuts->get_mass_reco();
	ht_calc = cuts->get_ht_calc();
	mass_plots->set_ht_calc(ht_calc);
	mass_plots->set_mass_reco(mass_reco);
	mass_reco->set_tprime_mass(tprime_mass);
	mass_jet_match->set_mass_reco(mass_reco);
	mva_producer->set_mass_reconstruction(mass_reco);
}

void PlotGenerator::deactivate_cuts()
{
	cuts = NULL;
	unset_lepton_selector_cuts();
}

void PlotGenerator::set_tprime_mass(double mass)
{
	tprime_mass = mass;
}
