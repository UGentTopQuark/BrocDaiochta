#include "analysers/EventSelection/interface/LeptonIsolationPlots.h"

LeptonIsolationPlots::LeptonIsolationPlots(std::string ident)
{
	id = ident;
	book_histos();

	reco_gen_match = NULL;
}

LeptonIsolationPlots::~LeptonIsolationPlots()
{
}

void LeptonIsolationPlots::set_reco_gen_match(RecoGenMatch *reco_gen_match)
{
	this->reco_gen_match = reco_gen_match;
}

void LeptonIsolationPlots::set_genEvt_handle(edm::Handle<TtGenEvent> genEvt)
{
	this->genEvt = genEvt;
}

void LeptonIsolationPlots::plot()
{
	matchleptons_plotiso_plotveto();
}

void LeptonIsolationPlots::book_histos()
{
	edm::Service<TFileService> fs;

	//isolation plots
	histos1d[("match_mu_trackiso"+id).c_str()]=fs->make<TH1D>(("match_mu_trackiso"+id).c_str(),"matched Muon Track Iso",120,0,60);
	histos1d[("match_mu_hcaliso"+id).c_str()]=fs->make<TH1D>(("match_mu_hcaliso"+id).c_str(),"matched Muon Hcal Iso",120,0,60);
	histos1d[("match_mu_ecaliso"+id).c_str()]=fs->make<TH1D>(("match_mu_ecaliso"+id).c_str(),"matched Muon Ecal Iso",120,0,60);
	histos1d[("match_mu_caloiso"+id).c_str()]=fs->make<TH1D>(("match_mu_caloiso"+id).c_str(),"matched Muon CalIso",120,0,60);
	histos1d[("match_mu_sumiso"+id).c_str()]=fs->make<TH1D>(("match_mu_sumiso"+id).c_str(),"matched Muon Ecal+Hcal+trackIso",70,0,140);
	histos1d[("match_mu_CombRelIso"+id).c_str()]=fs->make<TH1D>(("match_mu_CombRelIso"+id).c_str(),"matched Muon CombRelIso",75,0,1.5);
	histos1d[("match_e_trackiso"+id).c_str()]=fs->make<TH1D>(("match_e_trackiso"+id).c_str(),"matched Electron Track Iso",120,0,60);
	histos1d[("match_e_hcaliso"+id).c_str()]=fs->make<TH1D>(("match_e_hcaliso"+id).c_str(),"matched Electron Hcal Iso",120,0,60);
	histos1d[("match_e_ecaliso"+id).c_str()]=fs->make<TH1D>(("match_e_ecaliso"+id).c_str(),"matched Electron Ecal Iso",120,0,60);
	histos1d[("match_e_caloiso"+id).c_str()]=fs->make<TH1D>(("match_e_caloiso"+id).c_str(),"matched Electron CalIso",120,0,60);
	histos1d[("match_e_sumiso"+id).c_str()]=fs->make<TH1D>(("match_e_sumiso"+id).c_str(),"matched Electron SumIso",70,0,140);
	histos1d[("match_e_CombRelIso"+id).c_str()]=fs->make<TH1D>(("match_e_CombRelIso"+id).c_str(),"matched Electron CombRelIso",75,0,1.5);
	histos1d[("unmatch_mu_trackiso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_trackiso"+id).c_str(),"unmatched Muon TrackIso",120,0,60);
	histos1d[("unmatch_mu_hcaliso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_hcaliso"+id).c_str(),"unmatched Muon Hcal Iso",120,0,60);
	histos1d[("unmatch_mu_ecaliso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_ecaliso"+id).c_str(),"unmatched Muon Ecal Iso",120,0,60);
	histos1d[("unmatch_mu_caloiso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_caloiso"+id).c_str(),"unmatched Muon CaloIso",120,0,60);
	histos1d[("unmatch_mu_sumiso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_sumiso"+id).c_str(),"unmatched Muon Ecal+Hcal+trackIso",70,0,140);
	histos1d[("unmatch_mu_CombRelIso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_CombRelIso"+id).c_str(),"unmatched Muon CombRelIso",75,0,1.5);
	histos1d[("unmatch_e_trackiso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_trackiso"+id).c_str(),"unmatched Electron Track Iso",120,0,60);
	histos1d[("unmatch_e_hcaliso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_hcaliso"+id).c_str(),"unmatched Electron Hcal Iso",120,0,60);
	histos1d[("unmatch_e_ecaliso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_ecaliso"+id).c_str(),"unmatched Electron Ecal Iso",120,0,60);
	histos1d[("unmatch_e_caloiso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_caloiso"+id).c_str(),"unmatched Electron CaloIso",120,0,60);
	histos1d[("unmatch_e_sumiso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_sumiso"+id).c_str(),"unmatched Electron SumIso",70,0,140);
	histos1d[("unmatch_e_CombRelIso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_CombRelIso"+id).c_str(),"unmatched Electron CombRelIso",75,0,1.5);
	//vetocone plots
	histos1d[("match_mu_veto_hcalEnergy"+id).c_str()]=fs->make<TH1D>(("match_mu_veto_hcalEnergy"+id).c_str(),"matched muon vetocone(size 0.1) hcalEnergy",50,0,100);
	histos1d[("match_mu_veto_ecalEnergy"+id).c_str()]=fs->make<TH1D>(("match_mu_veto_ecalEnergy"+id).c_str(),"matched muon vetocone(size 0.07) ecalEnergy",50,0,100);
	histos1d[("match_e_veto_ecalEnergy"+id).c_str()]=fs->make<TH1D>(("match_e_veto_ecalEnergy"+id).c_str(),"matched electron vetocone(size 0.07) ecalEnergy",50,0,100);
	histos1d[("match_e_veto_hcalEnergy"+id).c_str()]=fs->make<TH1D>(("match_e_veto_hcalEnergy"+id).c_str(),"matched electron vetocone(size(0.1) hcalEnergy",50,0,100);
	histos1d[("unmatch_mu_veto_hcalEnergy"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_veto_hcalEnergy"+id).c_str(),"unmatched muon vetocone(size 0.1) hcalEnergy",50,0,100);
	histos1d[("unmatch_mu_veto_ecalEnergy"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_veto_ecalEnergy"+id).c_str(),"unmatched muon vetocone(size 0.07) ecalEnergy",50,0,100);
	histos1d[("unmatch_e_veto_ecalEnergy"+id).c_str()]=fs->make<TH1D>(("unmatch_e_veto_ecalEnergy"+id).c_str(),"unmatched electron vetocone(size 0.07) ecalEnergy",50,0,100);
	histos1d[("unmatch_e_veto_hcalEnergy"+id).c_str()]=fs->make<TH1D>(("unmatch_e_veto_hcalEnergy"+id).c_str(),"unmatched electron vetocone(size(0.1) hcalEnergy",50,0,100);
	
	histos2d[("match_e_caloiso_vs_trackiso"+id).c_str()]=fs->make<TH2D>(("match_e_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60);
	histos2d[("unmatch_e_caloiso_vs_trackiso"+id).c_str()]=fs->make<TH2D>(("unmatch_e_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60);
	histos2d[("match_mu_caloiso_vs_trackiso"+id).c_str()]=fs->make<TH2D>(("match_mu_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60);
	histos2d[("unmatch_mu_caloiso_vs_trackiso"+id).c_str()]=fs->make<TH2D>(("unmatch_mu_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60);
	histos2d[("match_e_veto_hcal_vs_veto_ecal"+id).c_str()]=fs->make<TH2D>(("match_e_veto_hcal_vs_veto_ecal"+id).c_str(),"matched electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100);
	histos2d[("unmatch_e_veto_hcal_vs_veto_ecal"+id).c_str()]=fs->make<TH2D>(("unmatch_e_veto_hcal_vs_veto_ecal"+id).c_str(),"unmatched electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100);
	histos2d[("match_mu_veto_hcal_vs_veto_ecal"+id).c_str()]=fs->make<TH2D>(("match_mu_veto_hcal_vs_veto_ecal"+id).c_str(),"matched electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100);
	histos2d[("unmatch_mu_veto_hcal_vs_veto_ecal"+id).c_str()]=fs->make<TH2D>(("unmatch_mu_veto_hcal_vs_veto_ecal"+id).c_str(),"unmatched electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100);
}

//plot isolation and vetocone(vetocone to replace dR cut) for semilep and non semilep events
void LeptonIsolationPlots::matchleptons_plotiso_plotveto()
{
	
	//	if(reco_gen_match == NULL || !reco_gen_match->is_geninfo_available())
	//	return;
	
	
	//	if(!(genEvt->isTtBar() && genEvt->isSemiLeptonic()))
	//	{ 
	for(std::vector<pat::Muon>::iterator muon = isolated_muons->begin();
	    muon != isolated_muons->end();
	    ++muon){
		
		histos1d[("unmatch_mu_hcaliso"+id).c_str()]->Fill(muon->hcalIso());
		histos1d[("unmatch_mu_ecaliso"+id).c_str()]->Fill(muon->ecalIso());
		histos1d[("unmatch_mu_trackiso"+id).c_str()]->Fill(muon->trackIso());
		histos1d[("unmatch_mu_caloiso"+id).c_str()]->Fill(muon->caloIso());
		double unmatch_mu_sumIso = (muon->trackIso()+muon->hcalIso()+muon->ecalIso());
		double unmatch_mu_CombRelIso = (unmatch_mu_sumIso)/(muon->pt());
		histos1d[("unmatch_mu_CombRelIso"+id).c_str()]->Fill(unmatch_mu_CombRelIso);
		histos1d[("unmatch_mu_sumiso"+id).c_str()]->Fill(unmatch_mu_sumIso);
		histos1d[("unmatch_mu_veto_hcalEnergy"+id).c_str()]->Fill(muon->hcalIsoDeposit()->candEnergy());
		histos1d[("unmatch_mu_veto_ecalEnergy"+id).c_str()]->Fill(muon->ecalIsoDeposit()->candEnergy()); 
		
		// 2d plots lepton isolation to see correlations
		histos2d[("unmatch_mu_caloiso_vs_trackiso"+id).c_str()]->Fill(muon->trackIso(), muon->caloIso());
		histos2d[("unmatch_mu_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill(muon->ecalIsoDeposit()->candEnergy(), muon->hcalIsoDeposit()->candEnergy());
	}
	
	for(std::vector<pat::Electron>::iterator electron = isolated_electrons->begin();electron != isolated_electrons->end();++electron){
		
		histos1d[("unmatch_e_hcaliso"+id).c_str()]->Fill(electron->hcalIso());
		histos1d[("unmatch_e_ecaliso"+id).c_str()]->Fill(electron->ecalIso());
		histos1d[("unmatch_e_trackiso"+id).c_str()]->Fill(electron->trackIso());
		histos1d[("unmatch_e_caloiso"+id).c_str()]->Fill(electron->caloIso());
		double unmatch_e_sumIso = (electron->trackIso()+electron->hcalIso()+electron->ecalIso());
		double unmatch_e_CombRelIso = (unmatch_e_sumIso)/(electron->pt());
		histos1d[("unmatch_e_CombRelIso"+id).c_str()]->Fill(unmatch_e_CombRelIso);
		histos1d[("unmatch_e_sumiso"+id).c_str()]->Fill(unmatch_e_sumIso);
		histos1d[("unmatch_e_veto_hcalEnergy"+id).c_str()]->Fill(electron->hcalIsoDeposit()->candEnergy());
		histos1d[("unmatch_e_veto_ecalEnergy"+id).c_str()]->Fill(electron->ecalIsoDeposit()->candEnergy());
		
		// 2d plots lepton isolation to see correlations
		histos2d[("unmatch_e_caloiso_vs_trackiso"+id).c_str()]->Fill(electron->trackIso(), electron->caloIso());
		histos2d[("unmatch_e_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill(electron->ecalIsoDeposit()->candEnergy(), electron->hcalIsoDeposit()->candEnergy());
	}
	
	return; 
	
// 		}
// 	else{
	
	
// 	int max_nmuons = isolated_muons->size(),max_nelectrons = isolated_electrons->size();	
	
// 	// For each candidate loop over all pat leptons and check if pt and eta match
// 	for(int i = 0;i < max_nmuons; i++){
		
// 		//    double dR = ROOT::Math::VectorUtil::DeltaR(muon->p4(),genEvt->singleLepton()->p4());
// 		if(reco_gen_match->lepton_from_top(i,"muon")){
// 			histos1d[("match_mu_hcaliso"+id).c_str()]->Fill((*isolated_muons)[i].hcalIso());
// 			histos1d[("match_mu_ecaliso"+id).c_str()]->Fill((*isolated_muons)[i].ecalIso());
// 			histos1d[("match_mu_trackiso"+id).c_str()]->Fill((*isolated_muons)[i].trackIso());
// 			histos1d[("match_mu_caloiso"+id).c_str()]->Fill((*isolated_muons)[i].caloIso());	
// 			double match_mu_sumIso = ((*isolated_muons)[i].trackIso()+(*isolated_muons)[i].hcalIso()+(*isolated_muons)[i].ecalIso());
// 			double match_mu_CombRelIso = (match_mu_sumIso)/((*isolated_muons)[i].pt());
// 			histos1d[("match_mu_CombRelIso"+id).c_str()]->Fill(match_mu_CombRelIso);
// 			histos1d[("match_mu_sumiso"+id).c_str()]->Fill(match_mu_sumIso);
// 			histos1d[("match_mu_veto_hcalEnergy"+id).c_str()]->Fill((*isolated_muons)[i].hcalIsoDeposit()->candEnergy());
// 			histos1d[("match_mu_veto_ecalEnergy"+id).c_str()]->Fill((*isolated_muons)[i].ecalIsoDeposit()->candEnergy()); 

// 			// 2d plots lepton isolation to see correlations
// 			histos2d[("match_mu_caloiso_vs_trackiso"+id).c_str()]->Fill((*isolated_muons)[i].trackIso(), (*isolated_muons)[i].caloIso());
// 			histos2d[("match_mu_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill((*isolated_muons)[i].ecalIsoDeposit()->candEnergy(), (*isolated_muons)[i].hcalIsoDeposit()->candEnergy());
// 		}
// 		else{
// 			histos1d[("unmatch_mu_hcaliso"+id).c_str()]->Fill((*isolated_muons)[i].hcalIso());
// 			histos1d[("unmatch_mu_ecaliso"+id).c_str()]->Fill((*isolated_muons)[i].ecalIso());
// 			histos1d[("unmatch_mu_trackiso"+id).c_str()]->Fill((*isolated_muons)[i].trackIso());
// 			histos1d[("unmatch_mu_caloiso"+id).c_str()]->Fill((*isolated_muons)[i].caloIso());
// 			double unmatch_mu_sumIso = ((*isolated_muons)[i].trackIso()+(*isolated_muons)[i].hcalIso()+(*isolated_muons)[i].ecalIso());
// 			double unmatch_mu_CombRelIso = (unmatch_mu_sumIso)/((*isolated_muons)[i].pt());
// 			histos1d[("unmatch_mu_CombRelIso"+id).c_str()]->Fill(unmatch_mu_CombRelIso);
// 			histos1d[("unmatch_mu_sumiso"+id).c_str()]->Fill(unmatch_mu_sumIso);
// 			histos1d[("unmatch_mu_veto_hcalEnergy"+id).c_str()]->Fill((*isolated_muons)[i].hcalIsoDeposit()->candEnergy());
// 			histos1d[("unmatch_mu_veto_ecalEnergy"+id).c_str()]->Fill((*isolated_muons)[i].ecalIsoDeposit()->candEnergy()); 

// 			// 2d plots lepton isolation to see correlations
// 			histos2d[("unmatch_mu_caloiso_vs_trackiso"+id).c_str()]->Fill((*isolated_muons)[i].trackIso(), (*isolated_muons)[i].caloIso());
// 			histos2d[("unmatch_mu_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill((*isolated_muons)[i].ecalIsoDeposit()->candEnergy(), (*isolated_muons)[i].hcalIsoDeposit()->candEnergy());
// 		}
// 	}
	
// 	//fill matched amd unmatched semileptonic electron
// 	for(int i = 0;i < max_nelectrons; i++){
		
// 		if(reco_gen_match->lepton_from_top(i,"electron")){
			
// 			histos1d[("match_e_hcaliso"+id).c_str()]->Fill((*isolated_electrons)[i].hcalIso());
// 			histos1d[("match_e_ecaliso"+id).c_str()]->Fill((*isolated_electrons)[i].ecalIso());
// 			histos1d[("match_e_trackiso"+id).c_str()]->Fill((*isolated_electrons)[i].trackIso());
// 			histos1d[("match_e_caloiso"+id).c_str()]->Fill((*isolated_electrons)[i].caloIso());
// 			double match_e_sumIso = ((*isolated_electrons)[i].trackIso()+(*isolated_electrons)[i].hcalIso()+(*isolated_electrons)[i].ecalIso());
// 			double match_e_CombRelIso = (match_e_sumIso)/((*isolated_electrons)[i].pt());
// 			histos1d[("match_e_CombRelIso"+id).c_str()]->Fill(match_e_CombRelIso);
// 			histos1d[("match_e_sumiso"+id).c_str()]->Fill(match_e_sumIso);
// 			histos1d[("match_e_veto_hcalEnergy"+id).c_str()]->Fill((*isolated_electrons)[i].hcalIsoDeposit()->candEnergy());
// 			histos1d[("match_e_veto_ecalEnergy"+id).c_str()]->Fill((*isolated_electrons)[i].ecalIsoDeposit()->candEnergy());

// 			// 2d plots lepton isolation to see correlations
// 			histos2d[("match_e_caloiso_vs_trackiso"+id).c_str()]->Fill((*isolated_electrons)[i].trackIso(), (*isolated_electrons)[i].caloIso());
// 			histos2d[("match_e_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill((*isolated_electrons)[i].ecalIsoDeposit()->candEnergy(), (*isolated_electrons)[i].hcalIsoDeposit()->candEnergy());
// 		}
// 		else{
// 			histos1d[("unmatch_e_hcaliso"+id).c_str()]->Fill((*isolated_electrons)[i].hcalIso());
// 			histos1d[("unmatch_e_ecaliso"+id).c_str()]->Fill((*isolated_electrons)[i].ecalIso());
// 			histos1d[("unmatch_e_trackiso"+id).c_str()]->Fill((*isolated_electrons)[i].trackIso());
// 			histos1d[("unmatch_e_caloiso"+id).c_str()]->Fill((*isolated_electrons)[i].caloIso());
// 			double unmatch_e_sumIso = ((*isolated_electrons)[i].trackIso()+(*isolated_electrons)[i].hcalIso()+(*isolated_electrons)[i].ecalIso());
// 			double unmatch_e_CombRelIso = (unmatch_e_sumIso)/((*isolated_electrons)[i].pt());
// 			histos1d[("unmatch_e_CombRelIso"+id).c_str()]->Fill(unmatch_e_CombRelIso);
// 			histos1d[("unmatch_e_sumiso"+id).c_str()]->Fill(unmatch_e_sumIso);
// 			histos1d[("unmatch_e_veto_hcalEnergy"+id).c_str()]->Fill((*isolated_electrons)[i].hcalIsoDeposit()->candEnergy());
// 			histos1d[("unmatch_e_veto_ecalEnergy"+id).c_str()]->Fill((*isolated_electrons)[i].ecalIsoDeposit()->candEnergy());
				
// 			// 2d plots lepton isolation to see correlations
// 			histos2d[("unmatch_e_caloiso_vs_trackiso"+id).c_str()]->Fill((*isolated_electrons)[i].trackIso(), (*isolated_electrons)[i].caloIso());
// 			histos2d[("unmatch_e_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill((*isolated_electrons)[i].ecalIsoDeposit()->candEnergy(), (*isolated_electrons)[i].hcalIsoDeposit()->candEnergy());
// 		}
// 	}
// 	}
}
