#include "analysers/EventSelection/interface/DoubleCountedLeptonRemover.h"

template DoubleCountedLeptonRemover<pat::Electron>::DoubleCountedLeptonRemover();
template DoubleCountedLeptonRemover<pat::Electron>::~DoubleCountedLeptonRemover();
template DoubleCountedLeptonRemover<pat::Muon>::DoubleCountedLeptonRemover();
template DoubleCountedLeptonRemover<pat::Muon>::~DoubleCountedLeptonRemover();

template void DoubleCountedLeptonRemover<pat::Electron>::set_tight_leptons(std::vector<pat::Electron> *leptons);
template void DoubleCountedLeptonRemover<pat::Muon>::set_tight_leptons(std::vector<pat::Muon> *leptons);

template void DoubleCountedLeptonRemover<pat::Electron>::set_loose_leptons(std::vector<pat::Electron> *leptons);
template void DoubleCountedLeptonRemover<pat::Muon>::set_loose_leptons(std::vector<pat::Muon> *leptons);

template void DoubleCountedLeptonRemover<pat::Electron>::clean_loose_leptons();
template void DoubleCountedLeptonRemover<pat::Muon>::clean_loose_leptons();

template <class myLepton>
DoubleCountedLeptonRemover<myLepton>::DoubleCountedLeptonRemover()
{
	tight_leptons = NULL;
	loose_leptons = NULL;
}

template <class myLepton>
DoubleCountedLeptonRemover<myLepton>::~DoubleCountedLeptonRemover()
{
}

template <class myLepton>
void DoubleCountedLeptonRemover<myLepton>::set_tight_leptons(std::vector<myLepton> *leptons)
{
	tight_leptons = leptons;
}

template <class myLepton>
void DoubleCountedLeptonRemover<myLepton>::set_loose_leptons(std::vector<myLepton> *leptons)
{
	loose_leptons = leptons;
}

template <class myLepton>
void DoubleCountedLeptonRemover<myLepton>::clean_loose_leptons()
{
	if(!tight_leptons){
		std::cerr << "WARNING: DoubleCountedLeptonRemover::clean_loose_leptons(): tight_leptons not set" << std::endl;
		return;
	}
	if(!loose_leptons){
		std::cerr << "WARNING: DoubleCountedLeptonRemover::clean_loose_leptons(): loose_leptons not set" << std::endl;
		return;
	}

	typename std::vector<myLepton>::iterator loose_lep_for_removal = loose_leptons->end();

	for(typename std::vector<myLepton>::iterator tight_lep = tight_leptons->begin();
	    tight_lep != tight_leptons->end();
	    ++tight_lep){
		for(typename std::vector<myLepton>::iterator loose_lep = loose_leptons->begin();
		    loose_lep != loose_leptons->end();
		    ++loose_lep){
			// if the two leptons are identical, mark tight lepton from loose lepton collection for removal
			// assume leptons don't appear twice in loose lepton collection
			if(loose_lep->eta() == tight_lep->eta() &&
			   loose_lep->pt() == tight_lep->pt()){
				loose_lep_for_removal = loose_lep;
				break;
			}
		}
		if(loose_lep_for_removal != loose_leptons->end()){
			loose_leptons->erase(loose_lep_for_removal);
			loose_lep_for_removal = loose_leptons->end();
		}
	}

	loose_leptons = NULL;
	tight_leptons = NULL;
}
