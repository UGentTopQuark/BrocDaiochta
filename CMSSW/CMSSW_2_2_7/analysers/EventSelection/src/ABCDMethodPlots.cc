#include "analysers/EventSelection/interface/ABCDMethodPlots.h"

ABCDMethodPlots::ABCDMethodPlots(std::string ident)
{
	id = ident;
	book_histos();
}

ABCDMethodPlots::~ABCDMethodPlots()
{
}

void ABCDMethodPlots::plot()
{
	plot_leptIso_muon();
	plot_leptIso_electron();
}

void ABCDMethodPlots::book_histos()
{
	// file service to write histos automatically to root files
	edm::Service<TFileService> fs;

	// ABCD method plots
	histos2d[("isolation_vs_met_muon"+id).c_str()]=fs->make<TH2D>(("isolation_vs_met_muon"+id).c_str(),"lepton isolation vs MET",100, 0, 150, 50,0,50);
	histos2d[("isolation_vs_met_electron"+id).c_str()]=fs->make<TH2D>(("isolation_vs_met_electron"+id).c_str(),"lepton isolation vs MET",100, 0, 150, 50,0,50);
}

// For estimation of QCD usingABCD method, plot lepton isolation vs MET
void ABCDMethodPlots::plot_leptIso_muon()
{
	if(corrected_mets->size() < 1 || isolated_muons->size() < 1)
		return;

	double event_met = 0.0, lepton_isolation = 0.0;
	for(std::vector<pat::MET>::iterator met_iter = corrected_mets->begin();
	    met_iter!=corrected_mets->end();
	    ++met_iter){
		event_met=met_iter->et(); 
	}
	
	std::vector<pat::Muon>::const_iterator muon_iter = isolated_muons->begin();
	lepton_isolation = muon_iter->trackIso();
	
	histos2d[("isolation_vs_met_muon"+id).c_str()]->Fill(lepton_isolation, event_met);
}

// For estimation of QCD usingABCD method, plot lepton isolation vs MET
void ABCDMethodPlots::plot_leptIso_electron()
{
	if(corrected_mets->size() < 1 || isolated_electrons->size() < 1)
		return;

	double event_met = 0.0, lepton_isolation = 0.0;
	for(std::vector<pat::MET>::iterator met_iter = corrected_mets->begin();
                met_iter!=corrected_mets->end();
                ++met_iter){
		event_met=met_iter->et(); 
	}
	
	std::vector<pat::Electron>::const_iterator electron_iter = isolated_electrons->begin();
	lepton_isolation = electron_iter->trackIso();
	
	histos2d[("isolation_vs_met_electron"+id).c_str()]->Fill(lepton_isolation, event_met);
}
