#include "analysers/EventSelection/interface/HtCalculator.h"

HtCalculator::HtCalculator()
{
	ht = -1;

	jets = NULL;
	isolated_electrons = NULL;
	isolated_muons = NULL;
	corrected_mets = NULL;
}

HtCalculator::~HtCalculator()
{
}

void HtCalculator::set_handles(std::vector<pat::Jet>* jets,
		 std::vector<pat::Electron>* isolated_electrons,
		 std::vector<pat::Muon>* isolated_muons,
		 std::vector<pat::MET>* corrected_mets)
{
	this->jets = jets;
	this->isolated_electrons = isolated_electrons;
	this->isolated_muons = isolated_muons;
	this->corrected_mets = corrected_mets;

	ht = -1;
}

double HtCalculator::get_ht()
{
	if(ht != -1)
		return ht;
	else
		return calculate_ht();
}

double HtCalculator::calculate_ht()
{
  	double pt_sum=0;

	for(std::vector<pat::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		pt_sum += jet_iter->pt();
	}

	std::vector<pat::Muon>::const_iterator mu_iter = isolated_muons->begin();

	if(mu_iter != isolated_muons->end())
		pt_sum += mu_iter->pt();

	std::vector<pat::Electron>::const_iterator e_iter = isolated_electrons->begin();

	if(e_iter != isolated_electrons->end())
		pt_sum += e_iter->pt();

	//pt_sum += corrected_mets->begin()->pt();
	ht = pt_sum;

	return ht;
}
