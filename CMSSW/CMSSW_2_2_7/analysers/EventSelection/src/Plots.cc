#include "analysers/EventSelection/interface/Plots.h"

Plots::Plots()
{
	gen_match = NULL;
	book_histos();
}

Plots::~Plots()
{
}

void Plots::set_gen_match(GenMatch *gen_match)
{
	this->gen_match = gen_match;
}

void Plots::book_histos()
{
	// edm::Service<TFileService> fs;

	// histos2d[("bjet_eta_vs_bDiscrim_cut"+id).c_str()]=fs->make<TH2D>(("bjet_eta_vs_bDiscrim_cut"+id).c_str(),"eta of b jets vs bDiscrim",nbins,min_x,max_x,50,-2.5,2.5);
	// histos1d[("jets_btagged_cut"+id).c_str()]=fs->make<TH1D>(("jets_btagged_cut"+id).c_str(),"number of jets btagged in this event",11,-0.5,10.5);
}

void Plots::prepare_objects()
{
}

void Plots::set_handles(std::vector<pat::Jet>* jets,
		 std::vector<pat::Electron>* isolated_electrons,
		 std::vector<pat::Muon>* isolated_muons,
		 std::vector<pat::MET>* corrected_mets)
{
	this->jets = jets;
	this->isolated_electrons = isolated_electrons;
	this->isolated_muons = isolated_muons;
	this->corrected_mets = corrected_mets;
}
