#ifndef DOUBLECOUNTEDLEPTONREMOVER_H
#define DOUBLECOUNTEDLEPTONREMOVER_H

#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Lepton.h"

template <class myLepton>
class DoubleCountedLeptonRemover{
	public:
		DoubleCountedLeptonRemover();
		~DoubleCountedLeptonRemover();
		void set_tight_leptons(std::vector<myLepton> *leptons);
		void set_loose_leptons(std::vector<myLepton> *leptons);
		void clean_loose_leptons();
	private:
		std::vector<myLepton> *tight_leptons;
		std::vector<myLepton> *loose_leptons;
};

#endif
