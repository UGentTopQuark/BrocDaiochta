#ifndef RECOGENMATCH_H
#define RECOGENMATCH_H

#include "DataFormats/Common/interface/View.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"

#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"

#include "analysers/EventSelection/interface/GenMatch.h"
#include "analysers/EventSelection/interface/PtSorter.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class RecoGenMatch{
	public:
		RecoGenMatch();
		~RecoGenMatch();
		void set_handles(std::vector<pat::Jet>* jets, std::vector<pat::Electron>* electrons, std::vector<pat::Muon>* muons, std::vector<pat::MET> *mets,edm::Handle<reco::GenParticleCollection> genParticles,edm::Handle<edm::View<pat::Muon> > uncut_muons,edm::Handle<edm::View<pat::Electron> > uncut_electronns);	

		bool is_geninfo_available();
		double min_dR_genquarks();
		double dR_Wquarks();

		void set_matching_method(double method);

		//returns map of matched ids
		std::map<std::string,int> get_matched_recojets_id();
		std::map<std::string,int> get_recojets_id_no_dRcut();
		//matched by id using genParton, not necessarily from top decay
		std::vector<int> get_matched_bjets_id();
		bool lepton_from_top(int lep_id = 0,std::string ident = "muon");
                int uncut_lepton_from_top(std::string ident = "muon");

		/*
		 * 	possible entries in map:
	 	 *	Wquark1
	 	 *	Wquark2
	 	 *	hadb
	 	 *	lepb
		 */	

	private:
		void reset_values();
		double deltaR(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
		    ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2);
		void get_GenMatch_quarks();
		void get_matched_genPartonjets_id();
		void pt_sorted_partons_matching();
		
		//calo jet ids of jets matching gen quarks
		bool geninfo_available;
		double matching_method;
		double default_matching_method;
		bool ids_filled;
		bool ids_filled_no_dRcut;
		int top_id;
		

		std::map<std::string,int> found_id;
		std::map<std::string,int> found_id_no_dRcut;

		std::map<std::string,reco::GenParticle*> genquark;

		static double const max_dR_cut;
		static double const sigma_dphi;
		static double const sigma_deta;
		static double const sigma_dpt;

		GenMatch *gen_match;

		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* electrons;
		std::vector<pat::Muon>* muons;
		std::vector<pat::MET>* mets;
		edm::Handle<reco::GenParticleCollection> genParticles;
		edm::Handle<edm::View<pat::Muon> >uncut_muons;
		edm::Handle<edm::View<pat::Electron> > uncut_electrons;
};

#endif
