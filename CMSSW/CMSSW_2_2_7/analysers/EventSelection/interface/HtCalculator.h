#ifndef HTCALCULATOR_H
#define HTCALCULATOR_H

#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"

class HtCalculator{
	public:
		HtCalculator();
		~HtCalculator();

		void set_handles(std::vector<pat::Jet>* jets,
				 std::vector<pat::Electron>* isolated_electrons,
				 std::vector<pat::Muon>* isolated_muons,
				 std::vector<pat::MET>* corrected_mets);
		double get_ht();

	private:
		double calculate_ht();

		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* isolated_electrons;
		std::vector<pat::Muon>* isolated_muons;
		std::vector<pat::MET>* corrected_mets;

		double ht;
};

#endif
