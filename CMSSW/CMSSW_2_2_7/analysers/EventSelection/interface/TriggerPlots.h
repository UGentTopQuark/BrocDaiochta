#ifndef TRIGGERPLOTS_H
#define TRIGGERPLOTS_H

#include "analysers/EventSelection/interface/Plots.h"
#include "analysers/EventSelection/interface/Tools.h"
#include "analysers/EventSelection/interface/RecoGenMatch.h"
#include "DataFormats/PatCandidates/interface/TriggerPrimitive.h" 
#include "DataFormats/Common/interface/TriggerResults.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"


class TriggerPlots : public Plots{
	public:
		TriggerPlots(std::string ident);
		~TriggerPlots();
		void set_reco_gen_match(RecoGenMatch *reco_gen_match);
		void set_hltr_handle(edm::Handle<edm::TriggerResults> HLTR);
		void set_muon_handle(edm::Handle<edm::View<pat::Muon> > muons);
		void plot();

	private:
		void book_histos();
		void plot_trigger();
		void plot_trigger_muons();

		edm::Handle<edm::TriggerResults> HLTR;
		edm::Handle<edm::View<pat::Muon> > muons;
		RecoGenMatch *reco_gen_match;
};

#endif
