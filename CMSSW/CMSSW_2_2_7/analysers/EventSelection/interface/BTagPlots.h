#ifndef BTAGPLOTS_H
#define BTAGPLOTS_H

#include "analysers/EventSelection/interface/Plots.h"
#include "analysers/EventSelection/interface/RecoGenMatch.h"
#include "analysers/EventSelection/interface/GenMatch.h"
#include "analysers/EventSelection/interface/BJetFinder.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class BTagPlots : public Plots{
	public:
		BTagPlots(std::string ident);
		~BTagPlots();

		void plot();
		void set_reco_gen_match(RecoGenMatch *reco_gen_match);
		void set_gen_match(GenMatch *gen_match);
		void set_bjet_finder(BJetFinder *bjet_finder);
		void set_btag_cuts(std::vector<double> *min,double n);
	private:
		void book_histos();

		void plot_MCmethod_comparison();
		void plot_bDiscriminator_find_btagcut();
		void analyse_btag_algorithms();
		void plot_bDiscriminator();
		void plot_bDiscriminator_allbjets();

		//get these values from Cuts.cc
		std::vector<double> *min_btag;
		std::string name_btag;

		RecoGenMatch *reco_gen_match;
		GenMatch *gen_match;
		BJetFinder *bjet_finder;
};

#endif
