#ifndef M3MASS_H
#define M3MASS_H

#include "analysers/EventSelection/interface/MassReconstructionMethod.h"

class M3Mass: public MassReconstructionMethod {
	public:

	protected:
		void calculate_mass();
};

#endif
