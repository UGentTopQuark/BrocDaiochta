#ifndef MASSPLOTS_H
#define MASSPLOTS_H

#include "analysers/EventSelection/interface/Plots.h"
#include "analysers/EventSelection/interface/MassReconstruction.h"
#include "analysers/EventSelection/interface/MassJetMatch.h"
#include "analysers/EventSelection/interface/RecoGenMatch.h"
#include "analysers/EventSelection/interface/HtCalculator.h"
#include "analysers/EventSelection/interface/BJetFinder.h"
#include "TMath.h"

//for truth matching TQAF
// TtEvent necessary for new versions of PAT
//#include "AnalysisDataFormats/TopObjects/interface/TtEvent.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

class MassPlots : public Plots{
	public:
		MassPlots(std::string ident);
		~MassPlots();

		void plot();
		void set_mass_reco(MassReconstruction *mass_reco);
		void set_bjet_finder(BJetFinder *bjet_finder);
		void set_genEvt_handle(edm::Handle<TtGenEvent> genEvt);
		void set_semiLepEvt_handle(edm::Handle<TtSemiLeptonicEvent> semiLepEvt);
		void set_reco_gen_match(RecoGenMatch *reco_gen_match);
		void set_hypoClassKey_handle(edm::Handle<int> hypoClassKeyHandle);
		void set_ht_calc(HtCalculator *ht_calc);

	private:
		void book_histos();

		void plot_masstop();
		void plot_massjetmatch_top();
		void plot_Wb_mass();
		void plot_recogenmatch_top();
		void plot_Kinfit_top();
		void plot_minmass_1btag_top();
		void plot_minmass_2btag_top();
		void plot_genmass_top();
		void plot_chimass_top();
		void plot_chimass_1btag_top();
		void plot_chimass_2btag_top();

   		void plot_massW(double nominal_massW);

		MassReconstruction *mass_reco;
		MassJetMatch *mass_jet_match;
		RecoGenMatch *reco_gen_match;
		BJetFinder *bjet_finder;

		HtCalculator *ht_calc;

		edm::Handle<TtGenEvent> genEvt;
		edm::Handle<TtSemiLeptonicEvent> semiLepEvt;
		edm::Handle<int> hypoClassKeyHandle;

		double ht;
};

#endif
