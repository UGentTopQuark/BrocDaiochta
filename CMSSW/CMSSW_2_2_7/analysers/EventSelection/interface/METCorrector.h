#ifndef METCORRECTOR_H
#define METCORRECTOR_H

#include "FWCore/Framework/interface/Event.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/PatCandidates/interface/MET.h"


#include "DataFormats/PatCandidates/interface/Particle.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"

#include "analysers/EventSelection/interface/MyMEzCalculator.h"
//#include "TopQuarkAnalysis/TopTools/interface/MEzCalculator.h"


class METCorrector{
	public:
		METCorrector();
		~METCorrector();
		void set_handles(edm::Handle<edm::View<pat::MET> > &mets_handle, pat::Particle *lepton=NULL, bool is_muon=true);
		std::vector<pat::MET>* get_MET();
		std::vector<pat::MET>* get_uncorrected_MET();
		bool is_corrected();
		void set_correction_method(int method);
	private:
		void correct_MET();
		void prepare_uncorrected_MET();

		pat::Particle *lepton;
		edm::Handle<edm::View<pat::MET> > mets_handle;
		std::vector<pat::MET>* mets;
		std::vector<pat::MET>* uncorrected_mets;
		MyMEzCalculator *MEzCal;
		bool is_muon;
		bool corrected;
		int correction_method;
		static const bool verbose = 0;
};

#endif
