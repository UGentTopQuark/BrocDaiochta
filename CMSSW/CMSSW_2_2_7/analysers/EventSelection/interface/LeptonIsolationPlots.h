#ifndef LEPTONISOLATIONPLOTS_H
#define LEPTONISOLATIONPLOTS_H

#include "analysers/EventSelection/interface/Plots.h"
#include "analysers/EventSelection/interface/RecoGenMatch.h"

class LeptonIsolationPlots : public Plots {
	public:
		LeptonIsolationPlots(std::string ident);
		~LeptonIsolationPlots();

		void set_reco_gen_match(RecoGenMatch *reco_gen_match);
		void set_genEvt_handle(edm::Handle<TtGenEvent> genEvt);
		void plot();

	private:
		void book_histos();
		void matchleptons_plotiso_plotveto();

		RecoGenMatch *reco_gen_match;
		edm::Handle<TtGenEvent> genEvt;
};

#endif
