#ifndef LEPTONSELECTOR_H
#define LEPTONSELECTOR_H

#include "DataFormats/Common/interface/View.h"
#include "FWCore/Framework/interface/Event.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Lepton.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include <DataFormats/Common/interface/Ref.h>

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

template <class myLepton>
class LeptonSelector{
	public:
		LeptonSelector();
		~LeptonSelector();
                void set_max_trackiso(std::vector<double> *max);
                void set_max_caliso(std::vector<double> *max);
                void set_max_ecaliso(std::vector<double> *max);
                void set_max_hcaliso(std::vector<double> *max);
                void set_max_hcal_veto_cone(std::vector<double> *max);
                void set_max_ecal_veto_cone(std::vector<double> *max);
                void set_min_dR(std::vector<double> *min);
		void set_min_pt(std::vector<double> *min);
		void set_min_et(std::vector<double> *min);
		void set_min_relIso(std::vector<double> *min);
		void set_max_eta(std::vector<double> *max);

		/*
		 *	electron IDs:
		 *	0: "eidRobustLoose";
		 *	1: "eidRobustTight";
		 *	2: "eidLoose";
		 *	3: "eidTight";
		 */
		void set_electronID(std::vector<double> *eid);
		void set_min_nHits(std::vector<double> *min);
		void set_max_d0(std::vector<double> *max);
		void set_max_d0sig(std::vector<double> *max);
		void set_max_chi2(std::vector<double> *max);

		/*
		 *	muons:
		 *	0: isGlobalMuon()
		 *	1: isStandAloneMuon()
		 *	2: isTrackerMuon()
		 */
		void set_lepton_type(int lepton_type);
		std::vector<myLepton>* get_leptons(edm::Handle<edm::View<myLepton> > leptons, edm::Handle<edm::View<pat::Jet> > myjets, edm::Handle<reco::BeamSpot> recoBeamSpotHandle);
		
	private:
                std::vector<double> *max_trackiso;
                std::vector<double> *max_caliso;
                std::vector<double> *max_ecaliso;
                std::vector<double> *max_hcaliso;
                std::vector<double> *max_hcal_veto_cone;
                std::vector<double> *max_ecal_veto_cone;
		std::vector<double> *min_dR;
		std::vector<double> *min_pt;
		std::vector<double> *min_et;
		std::vector<double> *min_relIso;
		std::vector<double> *electronID;
		std::vector<double> *min_nHits;
		std::vector<double> *max_d0;
		std::vector<double> *max_d0sig;
		std::vector<double> *max_chi2;
		std::vector<double> *max_eta;
		std::vector<myLepton> *isolated_leptons;
		int lepton_type;

		static bool const cut_e_trigger;
		static bool const cut_mu_trigger;

		edm::Handle<reco::BeamSpot> beamSpotHandle;

                bool cut_trackiso(const myLepton *lepton_iter, double &max_trackiso);
                bool cut_caliso(const myLepton *lepton_iter, double &max_caliso);
                bool cut_ecaliso(const myLepton *lepton_iter, double &max_ecaliso);
                bool cut_hcaliso(const myLepton *lepton_iter, double &max_hcaliso);
                bool cut_hcal_veto_cone(const myLepton *lepton_iter, double &max_hcal_veto_cone);
                bool cut_ecal_veto_cone(const myLepton *lepton_iter, double &max_ecal_veto_cone);
                bool cut_dR(const myLepton *lepton_iter, double &min_dR);
		bool cut_pt(const myLepton *lepton_iter, double &min_pt);
		bool cut_et(const myLepton *lepton_iter, double &min_et);
		bool cut_eta(const myLepton *lepton_iter, double &max_eta);
		bool cut_relIso(const myLepton *lepton_iter, double &min_relIso);
		bool cut_electronID(const myLepton *lepton_iter, double &electron_id);
		bool cut_lepton_type(const myLepton *lepton_iter, int &lepton_type);
		bool cut_chi2(const myLepton *lepton_iter, double &max_chi2);
		bool cut_d0(const myLepton *lepton_iter, double &max_d0);
		bool cut_d0sig(const myLepton *lepton_iter, double &max_d0sig);
		bool cut_nHits(const myLepton *lepton_iter, double &min_nHits);
		bool cut_trigger(const myLepton *lepton_iter);

		double deltaR(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
			      ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2);

		edm::Handle<edm::View<pat::Jet> > jets;
		static const bool verbose = false;
};

template<class myLepton> bool const LeptonSelector<myLepton>::cut_e_trigger = false; 	//	match electrons to trigger electrons
template<class myLepton> bool const LeptonSelector<myLepton>::cut_mu_trigger = false;	//	match muons to trigger muons

#endif
