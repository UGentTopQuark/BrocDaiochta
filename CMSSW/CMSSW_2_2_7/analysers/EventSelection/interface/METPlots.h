#ifndef METPLOTS_H
#define METPLOTS_H

#include "analysers/EventSelection/interface/Plots.h"
#include "analysers/EventSelection/interface/METCorrector.h"
#include "analysers/EventSelection/interface/MyMEzCalculator.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "analysers/EventSelection/interface/METCorrector.h"
#include "analysers/EventSelection/interface/Tools.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class METPlots : public Plots{
	public:
		METPlots(std::string ident);
		~METPlots();
		void plot();
		void set_met_handle(edm::Handle<edm::View<pat::MET> > analyser_mets);
	protected:
		void plot_MET_pz();
		void plot_MET_MtW();
		void calculate_MET();
		int get_pt_bin(double neutrino_pt);
		int get_pz_bin(double neutrino_pz);
		virtual void book_histos();
		std::vector<pat::MET> *method0_mets;
		std::vector<pat::MET> *method1_mets;
		std::vector<pat::MET> *method2_mets;
		std::vector<pat::MET> *uncorrected_mets;

        	METCorrector *METCor_method0;
        	METCorrector *METCor_method1;
        	METCorrector *METCor_method2;
		edm::Handle<edm::View<pat::MET> > mets;

		static const int PT_BINS=10;	// compare MET and nu pt in x pt-bins
};

#endif
