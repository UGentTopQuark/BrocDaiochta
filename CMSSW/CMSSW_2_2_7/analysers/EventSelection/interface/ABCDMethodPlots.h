#ifndef ABCDMETHODPLOTS_H
#define ABCDMETHODPLOTS_H

#include "analysers/EventSelection/interface/Plots.h"

class ABCDMethodPlots : public Plots{
	public:
		ABCDMethodPlots(std::string ident);
		~ABCDMethodPlots();
		void plot();
	
	private:
		void book_histos();
		void plot_leptIso_electron();
		void plot_leptIso_muon();
};

#endif
