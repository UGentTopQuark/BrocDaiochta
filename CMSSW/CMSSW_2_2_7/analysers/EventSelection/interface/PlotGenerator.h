#ifndef PLOTGENERATOR_H
#define PLOTGENERATOR_H

#include "DataFormats/Common/interface/View.h"
#include "FWCore/Framework/interface/Event.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/TriggerPrimitive.h" 
#include "DataFormats/Common/interface/TriggerResults.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"

#include "analysers/EventSelection/interface/Cuts.h"
#include "analysers/EventSelection/interface/LeptonSelector.h"
#include "analysers/EventSelection/interface/MassReconstruction.h"
#include "analysers/EventSelection/interface/METCorrector.h"
#include "analysers/EventSelection/interface/MassJetMatch.h"
#include "analysers/EventSelection/interface/GenMatch.h"
#include "analysers/EventSelection/interface/RecoGenMatch.h"
#include "analysers/EventSelection/interface/METPlots.h"
#include "analysers/EventSelection/interface/TriggerPlots.h"
#include "analysers/EventSelection/interface/LeptonIsolationPlots.h"
#include "analysers/EventSelection/interface/BTagPlots.h"
#include "analysers/EventSelection/interface/MassPlots.h"
#include "analysers/EventSelection/interface/ABCDMethodPlots.h"
#include "analysers/EventSelection/interface/MVAInputProducer.h"

//for truth matching TQAF
// TtEvent necessary for new versions of PAT
//#include "AnalysisDataFormats/TopObjects/interface/TtEvent.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

//#include "TopQuarkAnalysis/Examples/plugins/HypothesisAnalyzer.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include "TMath.h"

#include "TH1D.h"
#include "TH2D.h"
#include <map>
#include "TGraph.h"

#include "analysers/EventSelection/interface/MyMEzCalculator.h"

class PlotGenerator{
	public:
		PlotGenerator(std::string ident);
		~PlotGenerator();
		void set_handles(edm::Handle<edm::View<pat::Muon> > analyser_muons,
                           edm::Handle<edm::View<pat::Jet> > analyser_jets,
                           edm::Handle<edm::View<pat::Electron> > analyser_electrons,
			   edm::Handle<edm::View<pat::MET> > analyser_mets,
			   edm::Handle<TtSemiLeptonicEvent> semiLepEvt,
			   edm::Handle<int> hypoClassKeyHandle,
			   edm::Handle<TtGenEvent> genEvt,
			   edm::Handle<edm::TriggerResults> HLTR,
			   edm::Handle<reco::GenParticleCollection> genParticles,
			   edm::Handle<reco::BeamSpot> recoBeamSpotHandle);
		void plot();
		void apply_cuts(Cuts *mycuts);
		void deactivate_cuts();
		void set_tprime_mass(double mass);

	private:
		void plot_jets();
		void plot_muons();
		void plot_electrons();
		void plot_ht();
       		void plot_masstop();
		void plot_Kinfit_top();
		void plot_minmass_1btag_top();
		void plot_minmass_2btag_top();
		void plot_chimass_top();
		void plot_chimass_1btag_top();
		void plot_chimass_2btag_top();
		void plot_genmass_top();
		void plot_recogenmatch_top();
		void plot_massW(double nominal_massW);
		void plot_Wb_mass();
		void book_histos();
		void plot_electron_quality();
		void plot_muon_quality();
		void plot_dR();
		void set_lepton_selector_cuts();
		void unset_lepton_selector_cuts();
		void get_triggereff();
		void set_btag_cuts(std::vector<double> *min, double n);
		void get_btagInfo();
		void analyse_recogen_mindD();
		void analyse_recogen_matchedjets();
		void plot_massjetmatch_top();
		bool static compare_btag(const std::pair<int,double> p1, const std::pair<int,double> p2);

		edm::Handle<edm::View<pat::Electron> > electrons;
		edm::Handle<edm::View<pat::Jet> > uncut_jets;
		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* isolated_electrons;
		std::vector<pat::Electron>* all_electrons;
		std::vector<pat::Muon>* isolated_muons;
		std::vector<pat::MET>* corrected_mets;
		std::vector<pat::MET>* uncorrected_mets;
		edm::Handle<edm::View<pat::Muon> > muons;
		edm::Handle<edm::View<pat::MET> > mets;
		edm::Handle<TtSemiLeptonicEvent> semiLepEvt;
		edm::Handle<int> hypoClassKeyHandle;
		edm::Handle<TtGenEvent> genEvt;
		edm::Handle<edm::TriggerResults> HLTR;
		edm::Handle<reco::GenParticleCollection> genParticles;
		edm::Handle<reco::BeamSpot> beamSpotHandle;

		std::map<std::string,TH1D*> histos1d;
		std::map<std::string,TH2D*> histos2d;
		std::map<std::string,TGraph*> Tgraphs;

		Cuts *cuts;
		LeptonSelector<pat::Electron> *e_selector;
		LeptonSelector<pat::Electron> *all_e_selector;
		LeptonSelector<pat::Muon> *mu_selector;
		JetSelector *jet_selector;
		MassReconstruction *mass_reco;
		MassReconstruction *mass_reco_corMET;
                METCorrector *METCor;
		MassJetMatch *mass_jet_match;
		GenMatch *gen_match;
		RecoGenMatch *reco_gen_match;
		BJetFinder *bjet_finder;
		HtCalculator *ht_calc;
		MVAInputProducer *mva_producer;

		// plot classes
		METPlots *met_plots;
		MassPlots *mass_plots;
		TriggerPlots *trigger_plots;
		LeptonIsolationPlots *lept_iso_plots;
		BTagPlots *btag_plots;
		ABCDMethodPlots *abcd_plots;

		// to avoid calculating H_T several times... this should be solved properly one day
		double ht;

		double tprime_mass;
	
		// to distinguish the plots of multiple instances of the class
		// (muon-channel, electron-channel...)
		std::string id;
};

#endif
