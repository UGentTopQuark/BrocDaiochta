#ifndef MASSJETMATCH_H
#define MASSJETMATCH_H

#include "DataFormats/Common/interface/View.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"

#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"

#include "analysers/EventSelection/interface/RecoGenMatch.h"
#include "analysers/EventSelection/interface/GenMatch.h"
#include "analysers/EventSelection/interface/MassReconstruction.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class MassJetMatch{
	public:
		MassJetMatch();
		~MassJetMatch();
		void set_handles(std::vector<pat::Jet>* jets, std::vector<pat::Electron>* electrons, std::vector<pat::Muon>* muons, std::vector<pat::MET> *mets,edm::Handle<reco::GenParticleCollection> genParticles,edm::Handle<edm::View<pat::Muon> > uncut_muons,edm::Handle<edm::View<pat::Electron> > uncut_electrons);
		void set_mass_reco(MassReconstruction *mass_reco);
		int nmatches_chi2();
		int nmatches_chi2_1btag();
		int nmatches_chi2_2btag();
		int nmatches_m3();
		int nmatches_Wb();
		int nmatches_min_diff_m3();
		int nmatches_min_diff_m3_1btag();
		int nmatches_min_diff_m3_2btag();
	
	private:
		int match_jets(int jet_id1, int jet_id2, int jet_id3);
		void reset_values();
		void get_GenMatch_quarks();

		bool is_hadW_quark(int jet_id);
		bool is_hadb(int jet_id);
		bool is_lepb(int jet_id);

		void get_matched_recojets_id();
		std::map<std::string,int> found_id;
	
		double min_dRq1;
		double min_dRq2;
		double min_dRq3;

		bool geninfo_available;
		
		RecoGenMatch *reco_gen_match;
		MassReconstruction *mass_reco;

		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* electrons;
		std::vector<pat::Muon>* muons;
		std::vector<pat::MET>* mets;
		edm::Handle<reco::GenParticleCollection> genParticles;
		edm::Handle<edm::View<pat::Muon> > uncut_muons;
		edm::Handle<edm::View<pat::Electron> > uncut_electrons;

};

#endif
