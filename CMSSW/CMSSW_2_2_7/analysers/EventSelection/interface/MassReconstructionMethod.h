#ifndef MASSRECONSTRUCTIONMETHOD_H
#define MASSRECONSTRUCTIONMETHOD_H

#include "DataFormats/Common/interface/View.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "Math/VectorUtil.h"

class MassReconstructionMethod{
	public:
		MassReconstructionMethod();
		virtual ~MassReconstructionMethod();

		// return mass values for had and lep W/Top mass
		double get_hadT_mass();
		double get_lepT_mass();
		double get_hadW_mass();
		double get_lepW_mass();

		bool is_processed();

		// maximum number of jets considered for mass reconstruction
		void set_max_njets(int max);

		// get ids of jets that were assigned in mass reconstruction
		/*
		 *	definition of ids:
		 *	0: b-jet had t
		 *	1: 1st jet had W
		 *	2: 2nd jet had W
		 *	3: b-jet lep t
		 */
		std::vector<int>* get_mass_jet_ids();

		virtual void set_handles(std::vector<pat::Jet>* jets, std::vector<pat::Electron>* electrons, std::vector<pat::Muon>* muons, std::vector<pat::MET> *mets);

	protected:
		virtual void calculate_mass();
		double top_Had_candidate_mass(int jet_id1,int jet_id2,int jet_id3);
		double top_Lep_candidate_mass(int jet_id4);
		double W_Had_candidate_mass(int jet_id2,int jet_id3, int jet_id1=-1);
		double W_Lep_candidate_mass(pat::MET *met = NULL);

		void book_id_vectors();
		void free_id_vectors();
		virtual void reset_values();

		double hadT_mass;
		double lepT_mass;
		double hadW_mass;
		double lepW_mass;

		int max_njets;

		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* electrons;
		std::vector<pat::Muon>* muons;
		std::vector<pat::MET>* mets;

		std::vector<int> *mass_jet_ids;

		bool event_is_processed;
};

#endif
