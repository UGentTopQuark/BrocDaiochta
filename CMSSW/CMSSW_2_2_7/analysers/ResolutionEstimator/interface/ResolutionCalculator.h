#ifndef RESOLUTIONCALCULATOR_H
#define RESOLUTIONCALCULATOR_H

#include "analyzers/ResolutionEstimator/interface/Tools.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"
#include "TH1D.h"
#include "TH2D.h"
#include <map>

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include "TMath.h"

class ResolutionCalculator{
	public:
		ResolutionCalculator(std::string identifier);
		~ResolutionCalculator();
		void calculate_resolution(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > &reco_p4,
					  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > &gen_p4);

	private:
		void book_histos();
		int eta_to_eta_range(double eta);
		int pt_to_pt_range(double pt);
		void set_variables();

		/*
		 *	strings identifying the resolution-type, eta/phi/...
		 *	vector for different eta ranges of vector of different pt ranges
		 *	x-axis: pt
		 */
		std::map<std::string, std::vector<std::vector<TH1D*> > > histos;

		std::vector<std::string> variables;

		std::string id;

		static const int ETA_RANGES;
		static const int PT_RANGES;
		static const double MIN_PT;	// min pt for overall pt range
		static const double MAX_PT;	// max pt for overall pt range
};

#endif
