import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttGenEvent_cff")

process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttSemiLepEvtBuilder_cff")


process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(100) )

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
        'dcap://pnfs/iihe/cms/store/user/bklein/tprime_375_PAT_v2/0_PYTHIA6_Tprime_10TeV_cff_PAT_prod2_1.root',
        'dcap://pnfs/iihe/cms/store/user/bklein/tprime_375_PAT_v2/1_PYTHIA6_Tprime_10TeV_cff_PAT_prod2_1.root',
        'dcap://pnfs/iihe/cms/store/user/bklein/tprime_375_PAT_v2/2_PYTHIA6_Tprime_10TeV_cff_PAT_prod2_1.root',
        'dcap://pnfs/iihe/cms/store/user/bklein/tprime_375_PAT_v2/3_PYTHIA6_Tprime_10TeV_cff_PAT_prod2_1.root',
        'dcap://pnfs/iihe/cms/store/user/bklein/tprime_375_PAT_v2/4_PYTHIA6_Tprime_10TeV_cff_PAT_prod2_1.root',
        'dcap://pnfs/iihe/cms/store/user/bklein/tprime_375_PAT_v2/5_PYTHIA6_Tprime_10TeV_cff_PAT_prod2_1.root'
    )
)

process.resolution = cms.EDAnalyzer('ResolutionEstimator',
    electronTag = cms.untracked.InputTag("selectedLayer1Electrons"),
    tauTag      = cms.untracked.InputTag("selectedLayer1Taus"),
    muonTag     = cms.untracked.InputTag("selectedLayer1Muons"),
    jetTag      = cms.untracked.InputTag("selectedLayer1Jets"),
    photonTag   = cms.untracked.InputTag("selectedLayer1Photons"),
    metTag      = cms.untracked.InputTag("selectedLayer1METs"),
    hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypGenMatch:Key"),
      semiLepTag      = cms.untracked.InputTag("ttSemiLepEvent")
)

process.TFileService = cms.Service("TFileService", fileName = cms.string('histo.root') )

process.p = cms.Path(process.resolution)

process.p0 = cms.Path(process.makeGenEvt * process.makeTtSemiLepEvent)

process.schedule = cms.Schedule( process.p0, process.p )
