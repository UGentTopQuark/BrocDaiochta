#include "analyzers/ResolutionEstimator/interface/ResolutionCalculator.h"

const int ResolutionCalculator::ETA_RANGES = 10;
const int ResolutionCalculator::PT_RANGES = 10;
const double ResolutionCalculator::MIN_PT = 0.0;	// min pt for overall pt range
const double ResolutionCalculator::MAX_PT = 200.0;	// max pt for overall pt range

ResolutionCalculator::ResolutionCalculator(std::string identifier)
{
	id = identifier;

	set_variables();
	book_histos();
}

ResolutionCalculator::~ResolutionCalculator()
{

}

void ResolutionCalculator::calculate_resolution(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > &reco_p4,
						ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > &gen_p4)
{
	int pt_bin = pt_to_pt_range(reco_p4.pt());
	int eta_bin = eta_to_eta_range(gen_p4.pt());
	histos["pt"][eta_bin][pt_bin]->Fill(gen_p4.pt() - reco_p4.pt());
	histos["eta"][eta_bin][pt_bin]->Fill(gen_p4.eta() - reco_p4.eta());
	histos["phi"][eta_bin][pt_bin]->Fill(gen_p4.phi() - reco_p4.phi());
	histos["theta"][eta_bin][pt_bin]->Fill(gen_p4.theta() - reco_p4.theta());
}

void ResolutionCalculator::set_variables()
{
	variables.push_back("eta");
	variables.push_back("phi");
	variables.push_back("theta");
	variables.push_back("pt");
}

void ResolutionCalculator::book_histos()
{
	Tools tool;

	// file service to write histos automatically to root files
	edm::Service<TFileService> fs;

	const int PT_BINS = 25;		// bins per pt range

	const double PT_BIN_WIDTH = (MAX_PT - MIN_PT)/PT_RANGES;

	std::string histo_id = "";
	for(std::vector<std::string>::iterator variable = variables.begin();
	    variable != variables.end();
	    ++variable){
		for(int eta_range = 0; eta_range <= ETA_RANGES; ++eta_range){
			std::vector<TH1D*> pt_vec;
			for(int pt_range = 0; pt_range <= PT_RANGES; ++pt_range){
				histo_id = id+"_"+*variable+"_eta_"+tool.stringify(eta_range)+"_pt_"+tool.stringify(pt_range);
				pt_vec.push_back(fs->make<TH1D>(histo_id.c_str(),
						 histo_id.c_str(),
						 PT_BINS,
						 MIN_PT+PT_BIN_WIDTH*pt_range,
						 MIN_PT+PT_BIN_WIDTH*(pt_range+1)));
			}
			histos[*variable].push_back(pt_vec);
		}
	}
}

int ResolutionCalculator::eta_to_eta_range(double eta)
{
	const double MIN_ETA = 0.0;
	const double MAX_ETA = 2.5;
	
	int eta_range=-1;

	double bin_with = (MAX_ETA - MIN_ETA)/(double) ETA_RANGES;
	eta_range = (int) ((eta-MIN_ETA) / bin_with);

	if(eta_range > ETA_RANGES || eta_range < 0)
		return -1;
	else
		return eta_range;
}

int ResolutionCalculator::pt_to_pt_range(double pt)
{
	int pt_range=-1;

	double bin_with = (MAX_PT - MIN_PT)/(double) PT_RANGES;
	pt_range = (int) ((pt-MIN_PT) / bin_with);

	if(pt_range > PT_RANGES || pt_range < 0)
		return -1;
	else
		return pt_range;
}
