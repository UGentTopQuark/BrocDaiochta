// -*- C++ -*-
//
// Package:    ResolutionEstimator
// Class:      ResolutionEstimator
// 
/**\class ResolutionEstimator ResolutionEstimator.cc analyzers/ResolutionEstimator/src/ResolutionEstimator.cc

 Description: <one line class summary>

 Implementation:
     <Notes on implementation>
*/
//
// Original Author:  local user
//         Created:  Tue Aug  4 11:37:32 CEST 2009
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"

#include <DataFormats/Common/interface/Ref.h>

#include "TH1D.h"
#include "TH2D.h"
#include <map>

#include "DataFormats/Common/interface/View.h"
#include <string>

#include "AnalysisDataFormats/TopObjects/interface/TtGenEvent.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

//
// class decleration
//

class ResolutionEstimator : public edm::EDAnalyzer {
   public:
      explicit ResolutionEstimator(const edm::ParameterSet&);
      ~ResolutionEstimator();


   private:
      virtual void beginJob(const edm::EventSetup&) ;
      virtual void analyze(const edm::Event&, const edm::EventSetup&);
      virtual void endJob() ;

        edm::InputTag eleLabel_;
        edm::InputTag muoLabel_;
        edm::InputTag jetLabel_;
        edm::InputTag tauLabel_;
        edm::InputTag semiLepEvt_;
        edm::InputTag hypoClassKey_;
        edm::InputTag metLabel_;
        edm::InputTag phoLabel_;


      // ----------member data ---------------------------
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
ResolutionEstimator::ResolutionEstimator(const edm::ParameterSet& iConfig):
  eleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("electronTag")),
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  tauLabel_(iConfig.getUntrackedParameter<edm::InputTag>("tauTag")),
semiLepEvt_(iConfig.getUntrackedParameter<edm::InputTag>("semiLepTag")),
  hypoClassKey_(iConfig.getUntrackedParameter<edm::InputTag>("hypoClassKey")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  phoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("photonTag"))
{
   //now do what ever initialization is needed

}


ResolutionEstimator::~ResolutionEstimator()
{
 
   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called to for each event  ------------
void
ResolutionEstimator::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
        using namespace edm;

        edm::Handle<edm::View<pat::Electron> > electronHandle;
        iEvent.getByLabel(eleLabel_,electronHandle);

        edm::Handle<edm::View<pat::Muon> > muonHandle;
        iEvent.getByLabel(muoLabel_,muonHandle);

        edm::Handle<edm::View<pat::MET> > metHandle;
        iEvent.getByLabel(metLabel_,metHandle);

        edm::Handle<reco::GenParticleCollection> genParticles;
        iEvent.getByLabel("genParticles", genParticles);

        edm::Handle<TtGenEvent> genEvt;
        iEvent.getByLabel("genEvt", genEvt);
        
        edm::Handle<TtSemiLeptonicEvent> semiLepEvt;
        iEvent.getByLabel(semiLepEvt_, semiLepEvt);

        edm::Handle<int> hypoClassKeyHandle;
        iEvent.getByLabel(hypoClassKey_, hypoClassKeyHandle);

}


// ------------ method called once each job just before starting event loop  ------------
void 
ResolutionEstimator::beginJob(const edm::EventSetup&)
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
ResolutionEstimator::endJob() {
}

//define this as a plug-in
DEFINE_FWK_MODULE(ResolutionEstimator);
