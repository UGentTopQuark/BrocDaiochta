#ifndef BEAG_JET_H
#define BEAG_JET_H

#include "Particle.h"
#include <map>
#include <string>

#include "Rtypes.h"

namespace beag{
	class Jet : public Particle{
		public:
			Jet():ttbar_decay_product(0){};
			virtual ~Jet(){};

			std::map<std::string, double> btags;	// algo - bDiscriminator
			/*
			 *	ttbar_decay_product:
			 *	is jet from quark from ttbar decay
			 *	possible values:
			 *	1: quark jet
			 *	2: anti-quark jet
			 *	3: hadronically decaying top b-jet
			 *	4: leptonically decaying top b-jet
			 */
			int ttbar_decay_product;

		ClassDef(Jet, 1);
	};
}

#endif
