#ifndef BEAG_ELECTRON_H
#define BEAG_ELECTRON_H

#include "Lepton.h"
#include "Rtypes.h"

namespace beag{
	class Electron : public beag::Lepton{
		public:
			virtual ~Electron(){};

		ClassDef(Electron, 1);
	};
}

#endif
