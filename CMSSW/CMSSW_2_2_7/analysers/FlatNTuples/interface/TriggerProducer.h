#ifndef BEAG_TRIGGER_PRODUCER
#define BEAG_TRIGGER_PRODUCER

#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "Trigger.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Framework/interface/TriggerNames.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

namespace beag{
	class TriggerProducer{
		public:
			TriggerProducer(TTree *tree, TFile *outfile, std::string ident);
			~TriggerProducer();
			void set_triggers_to_write(std::vector<std::string> triggers, std::string trigger_menu);
			void fill_trigger_information(edm::Handle<edm::TriggerResults> HLTR);
		private:
			std::vector<std::string> triggers_to_write;
			std::string trigger_menu;
			std::vector<beag::Trigger> *trigger;
			TTree *tree;
			TFile *outfile;

			static const bool verbose = false;
	};
}

#endif
