#include "../interface/TriggerProducer.h"

beag::TriggerProducer::TriggerProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	trigger = new std::vector<beag::Trigger>();
	trigger->push_back(beag::Trigger());
	tree->Bronch(ident.c_str(),"std::vector<beag::Trigger>",&trigger);
}

beag::TriggerProducer::~TriggerProducer()
{
	if(trigger){
		delete trigger;
		trigger = NULL;
	}
}

void beag::TriggerProducer::set_triggers_to_write(std::vector<std::string> triggers, std::string trigger_menu)
{
	this->triggers_to_write = triggers;
	this->trigger_menu = trigger_menu;
}

void beag::TriggerProducer::fill_trigger_information(edm::Handle<edm::TriggerResults> HLTR)
{
	HLTConfigProvider hltConfig;
	hltConfig.init(trigger_menu);

	trigger->begin()->triggered.clear();
	outfile->cd();
	if(!HLTR.isValid()){
		std::cerr << "WARNING: beag::TriggerProducer::fill_trigger_information() no valid trigger object found" << std::endl;
		return;
	}

	for(std::vector<std::string>::iterator trigger_str = triggers_to_write.begin();
		trigger_str != triggers_to_write.end();
		++trigger_str){
		unsigned int trigger_id( hltConfig.triggerIndex(*trigger_str) );
		if(verbose){
			if(HLTR->accept(trigger_id))
				std::cout << "accepted by: " << *trigger_str << " id: " << trigger_id << " : " << HLTR->accept(trigger_id) << std::endl; 
			else
				std::cout << "rejected by: " << *trigger_str << " id: " << trigger_id << " : " << HLTR->accept(trigger_id) << std::endl; 
		}
		if(HLTR->accept(trigger_id))
			trigger->begin()->triggered[*trigger_str] = true;
		else
			trigger->begin()->triggered[*trigger_str] = false;
	}
}
