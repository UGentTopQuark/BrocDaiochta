#ifndef LEPTONSELECTOR_H
#define LEPTONSELECTOR_H

#include "DataFormats/Common/interface/View.h"
#include "FWCore/Framework/interface/Event.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Lepton.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include <DataFormats/Common/interface/Ref.h>

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

template <class myLepton>
class LeptonSelector{
	public:
		LeptonSelector();
		~LeptonSelector();
                void set_max_trackiso(std::vector<double> *max);
                void set_max_caliso(std::vector<double> *max);
                void set_max_ecaliso(std::vector<double> *max);
                void set_max_hcaliso(std::vector<double> *max);
                void set_max_hcal_veto_cone(std::vector<double> *max);
                void set_max_ecal_veto_cone(std::vector<double> *max);
                void set_min_dR(std::vector<double> *min);
		void set_min_pt(std::vector<double> *min);
		void set_min_relIso(std::vector<double> *min);
		void set_max_eta(double max);
		void set_electronID(std::vector<double> *eid);
		void set_min_nHits(std::vector<double> *min);
		void set_max_d0(std::vector<double> *max);
		void set_max_chi2(std::vector<double> *max);
		std::vector<myLepton>* get_leptons(edm::Handle<edm::View<myLepton> > leptons, edm::Handle<edm::View<pat::Jet> > myjets);
		
	private:
                std::vector<double> *max_trackiso;
                std::vector<double> *max_caliso;
                std::vector<double> *max_ecaliso;
                std::vector<double> *max_hcaliso;
                std::vector<double> *max_hcal_veto_cone;
                std::vector<double> *max_ecal_veto_cone;
		std::vector<double> *min_dR;
		std::vector<double> *min_pt;
		std::vector<double> *min_relIso;
		std::vector<double> *electronID;
		std::vector<double> *min_nHits;
		std::vector<double> *max_d0;
		std::vector<double> *max_chi2;
		std::vector<myLepton> *isolated_leptons;
		double max_eta;

                bool cut_trackiso(const myLepton *lepton_iter, double &max_trackiso);
                bool cut_caliso(const myLepton *lepton_iter, double &max_caliso);
                bool cut_ecaliso(const myLepton *lepton_iter, double &max_ecaliso);
                bool cut_hcaliso(const myLepton *lepton_iter, double &max_hcaliso);
                bool cut_hcal_veto_cone(const myLepton *lepton_iter, double &max_hcal_veto_cone);
                bool cut_ecal_veto_cone(const myLepton *lepton_iter, double &max_ecal_veto_cone);
                bool cut_dR(const myLepton *lepton_iter, double &min_dR);
		bool cut_pt(const myLepton *lepton_iter, double &min_pt);
		bool cut_eta(const myLepton *lepton_iter, double &max_eta);
		bool cut_relIso(const myLepton *lepton_iter, double &min_relIso);
		bool cut_electronID(const myLepton *lepton_iter, double &electron_id);
		bool cut_chi2(const myLepton *lepton_iter, double &max_chi2);
		bool cut_d0(const myLepton *lepton_iter, double &max_d0);
		bool cut_nHits(const myLepton *lepton_iter, double &min_nHits);
		bool cut_trigger(const myLepton *lepton_iter);

		double deltaR(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
			      ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2);

		edm::Handle<edm::View<pat::Jet> > jets;
};

#endif
