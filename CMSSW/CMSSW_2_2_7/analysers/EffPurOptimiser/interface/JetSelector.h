#ifndef JETSELECTOR_H
#define JETSELECTOR_H

#include "DataFormats/Common/interface/View.h"
#include "FWCore/Framework/interface/Event.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"

#include "analyzers/EffPurOptimiser/interface/PtSorter.h"
#include "analyzers/EffPurOptimiser/interface/Tools.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class JetSelector{
	public:
		JetSelector();
		~JetSelector();
		void set_min_pt(std::vector<double> *min);
		void set_max_eta(double max);

		template <typename myLepton>
		std::vector<pat::Jet>* get_jets(edm::Handle<edm::View<pat::Jet> > uncut_jets, edm::Handle<edm::View<myLepton> > leptons);

	private:
		double max_eta;

		// needed for dR calculation
		Tools *tools;

		static double const min_lepton_jet_dR;

		std::vector<double> *min_pt;

		std::vector<pat::Jet> *cut_jets;

		bool cut_pt(const pat::Jet *jet_iter, double &min_pt);
		bool cut_eta(const pat::Jet *jet_iter, double &max_eta);
		template <typename myLepton>
		bool cut_leptons(const pat::Jet *jet_iter, edm::Handle<edm::View<myLepton> > leptons);
};
#endif
