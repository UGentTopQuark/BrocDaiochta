#ifndef GLOBALMINIMISATION_H
#define GLOBALMINIMISATION_H

#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1F.h"
#include <vector>

class GlobalMinimisation{
	public:
		GlobalMinimisation(std::string outfile_name);
		~GlobalMinimisation();
		void set_handles(std::vector<pat::Jet>* jets, std::vector<pat::Electron>* electrons, std::vector<pat::Muon>* muons);

	private:
		void analyse_event();
		int valuetobin_jet_pt(double pt);
		int valuetobin_lepton_pt(double pt);
	//	int valuetobin_lepton_eta(double eta);

		// number of bins per dimension
		static const int NBINS;
		// number of dimensions (j1 pt, j2 pt, ...)
		static const int NDIM;

		const static bool verbose = false;

		TFile *outfile;
		TH1F *evt_counter;
		std::vector<std::vector<std::vector<std::vector<std::vector<unsigned int> > > > > *events;
		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* electrons;
		std::vector<pat::Muon>* muons;
};
#endif
