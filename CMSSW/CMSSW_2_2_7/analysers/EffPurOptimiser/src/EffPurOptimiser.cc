// -*- C++ -*-
//
// Package:    EffPurOptimiser
// Class:      EffPurOptimiser
// 
/**\class EffPurOptimiser EffPurOptimiser.cc analyzers/EffPurOptimiser/src/EffPurOptimiser.cc

 Description: <one line class summary>

 Implementation:
     <Notes on implementation>
*/
//
// Original Author:  local user
//         Created:  Tue Jul  7 12:21:31 CEST 2009
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"
#include "FWCore/ParameterSet/interface/InputTag.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/TriggerPrimitive.h"

#include "AnalysisDataFormats/TopObjects/interface/TtGenEvent.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

#include "DataFormats/Candidate/interface/Candidate.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Framework/interface/TriggerNames.h"

//#include <DataFormats/Common/interface/Ref.h>

#include "TH1D.h"
#include "TH2D.h"
#include <map>

#include "DataFormats/Common/interface/View.h"
#include <string>

#include "analyzers/EffPurOptimiser/interface/JetSelector.h"
#include "analyzers/EffPurOptimiser/interface/LeptonSelector.h"
#include "analyzers/EffPurOptimiser/interface/GlobalMinimisation.h"


//
// class decleration
//

class EffPurOptimiser : public edm::EDAnalyzer {
   public:
      explicit EffPurOptimiser(const edm::ParameterSet&);
      ~EffPurOptimiser();


   private:
      virtual void beginJob(const edm::EventSetup&) ;
      virtual void analyze(const edm::Event&, const edm::EventSetup&);
      virtual void endJob() ;

        std::map<std::string,TH1D*> histocontainer_;

        edm::InputTag eleLabel_;
        edm::InputTag muoLabel_;
        edm::InputTag jetLabel_;
        edm::InputTag tauLabel_;
        edm::InputTag semiLepEvt_;
        edm::InputTag hypoClassKey_;
        edm::InputTag metLabel_;
        edm::InputTag phoLabel_;
	edm::InputTag hlTriggerResults_;
	std::string outfile_;
	int dataset_;

	JetSelector *jet_selector;
	LeptonSelector<pat::Muon> *mu_selector;
	LeptonSelector<pat::Electron> *e_selector;

	GlobalMinimisation *glob_min;

	std::vector<pat::Jet> *jets;
	std::vector<pat::Muon> *muons;
	std::vector<pat::Electron> *electrons;

	std::vector<std::vector<double>* > to_delete;

      // ----------member data ---------------------------
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
EffPurOptimiser::EffPurOptimiser(const edm::ParameterSet& iConfig):
  histocontainer_(),
  eleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("electronTag")),
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  tauLabel_(iConfig.getUntrackedParameter<edm::InputTag>("tauTag")),
semiLepEvt_(iConfig.getUntrackedParameter<edm::InputTag>("semiLepTag")),
  hypoClassKey_(iConfig.getUntrackedParameter<edm::InputTag>("hypoClassKey")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  phoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("photonTag")),
  hlTriggerResults_(iConfig.getParameter<edm::InputTag>("HLTriggerResults")),
  outfile_(iConfig.getUntrackedParameter<std::string>("filename")),
  dataset_(iConfig.getUntrackedParameter<int>("dataset"))
{
	// book GlobalMinimisation object
	glob_min = new GlobalMinimisation(outfile_);


	// prepare object selectors
	jet_selector = new JetSelector();
	mu_selector = new LeptonSelector<pat::Muon>();
	e_selector = new LeptonSelector<pat::Electron>();

	std::vector<double> *mu_caliso = new std::vector<double>();
	to_delete.push_back(mu_caliso);
	mu_caliso->push_back(1.0);
	std::vector<double> *mu_trackiso = new std::vector<double>();
	to_delete.push_back(mu_trackiso);
	mu_trackiso->push_back(3.0);
	std::vector<double> *mu_d0 = new std::vector<double>();
	to_delete.push_back(mu_d0);
	mu_d0->push_back(0.2);
	std::vector<double> *e_caliso = new std::vector<double>();
	to_delete.push_back(e_caliso);
	e_caliso->push_back(10.0);
	std::vector<double> *e_trackiso = new std::vector<double>();
	to_delete.push_back(e_trackiso);
	e_trackiso->push_back(5.0);
	std::vector<double> *e_d0 = new std::vector<double>();
	to_delete.push_back(e_d0);
	e_d0->push_back(0.2);

	// muon selection cuts
	mu_selector->set_max_eta(2.0);
	mu_selector->set_max_caliso(mu_caliso);
	mu_selector->set_max_trackiso(mu_trackiso);
	mu_selector->set_max_d0(mu_d0);

	// electron selection cuts
	e_selector->set_max_eta(2.0);
	e_selector->set_max_caliso(e_caliso);
	e_selector->set_max_trackiso(e_trackiso);
	e_selector->set_max_d0(e_d0);

	// jet selection cuts
	jet_selector->set_max_eta(2.4);
}


EffPurOptimiser::~EffPurOptimiser()
{
	delete jet_selector;
	jet_selector = NULL; 
	delete e_selector;
	e_selector = NULL; 
	delete mu_selector;
	mu_selector = NULL; 
	delete glob_min;
	glob_min = NULL;


	for(std::vector<std::vector<double>* >::iterator del_iter = to_delete.begin();
		del_iter != to_delete.end();
		++del_iter){
		delete *del_iter;
	}
}


//
// member functions
//

// ------------ method called to for each event  ------------
void
EffPurOptimiser::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{

        using namespace edm;

        edm::Handle<edm::View<pat::Electron> > electronHandle;
        iEvent.getByLabel(eleLabel_,electronHandle);

        edm::Handle<edm::View<pat::Muon> > muonHandle;
        iEvent.getByLabel(muoLabel_,muonHandle);

        edm::Handle<edm::View<pat::MET> > metHandle;
        iEvent.getByLabel(metLabel_,metHandle);

        edm::Handle<edm::View<pat::Jet> > jetHandle;
        iEvent.getByLabel(jetLabel_,jetHandle);

        edm::Handle<reco::GenParticleCollection> genParticles;
        iEvent.getByLabel("genParticles", genParticles);

        edm::Handle<TtGenEvent> genEvt;
        iEvent.getByLabel("genEvt", genEvt);

	edm::Handle<edm::TriggerResults> HLTR;
	iEvent.getByLabel(hlTriggerResults_,HLTR);

	const int LEPTON_ID = 13;
	// single mu 15 trigger
	const int TRIGGER_BIT = 85;
	// electron: const int TRIGGER_BIT = 50;

	// cut on trigger-bit
	if(!(HLTR.isValid())){
		std::cerr << "WARNING: Trigger information not available" << std::endl;
		return;
	}

	if(!(HLTR->accept(TRIGGER_BIT)))
		return;

	if(dataset_ == 0){ // semileptonic ttbar
		if(!(genEvt->isTtBar() && !genEvt->isSemiLeptonic() && genEvt->lepton() != NULL && abs(genEvt->lepton()->pdgId()) == LEPTON_ID))
			return;
	}else if(dataset_ == 1){ // ttbar other
		if(!(genEvt->isTtBar() && (genEvt->isSemiLeptonic() && genEvt->lepton() != NULL && abs(genEvt->lepton()->pdgId()) == LEPTON_ID)))
			return;
	}

	// prepare leptons and jets
	electrons = e_selector->get_leptons(electronHandle, jetHandle);
	muons = mu_selector->get_leptons(muonHandle, jetHandle);
	jets = jet_selector->get_jets(jetHandle, electronHandle);

	glob_min->set_handles(jets, electrons, muons);
}

// ------------ method called once each job just before starting event loop  ------------
void 
EffPurOptimiser::beginJob(const edm::EventSetup&)
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
EffPurOptimiser::endJob() {
}

//define this as a plug-in
DEFINE_FWK_MODULE(EffPurOptimiser);
