#include "analyzers/EffPurOptimiser/interface/GlobalMinimisation.h"

const int GlobalMinimisation::NBINS = 15;
const int GlobalMinimisation::NDIM = 5;

GlobalMinimisation::GlobalMinimisation(std::string outfile_name)
{
	// prepare vector
	outfile = new TFile(outfile_name.c_str(),"RECREATE");

	outfile->cd();
	events = new std::vector<std::vector<std::vector<std::vector<std::vector<unsigned int> > > > >();
	
	for(int d1 = 0; d1 < NBINS; ++d1){
		std::vector<std::vector<std::vector<std::vector<unsigned int> > > > d1_vec;
		for(int d2 = 0; d2 < NBINS; ++d2){
			std::vector<std::vector<std::vector<unsigned int> > > d2_vec;
			for(int d3 = 0; d3 < NBINS; ++d3){
				std::vector<std::vector<unsigned int> > d3_vec;
				for(int d4 = 0; d4 < NBINS; ++d4){
					std::vector<unsigned int> d4_vec(NBINS, 0);
					d3_vec.push_back(d4_vec);
				}
				d2_vec.push_back(d3_vec);
			}
			d1_vec.push_back(d2_vec);
		}
		events->push_back(d1_vec);
	}

	evt_counter = new TH1F("evt_counter", "event counter", 5, -0.5,4.5);
}

GlobalMinimisation::~GlobalMinimisation()
{
	outfile->cd();
	TTree *t = new TTree("t","Simple Class");
	t->Bronch("Bronch","std::vector<std::vector<std::vector<std::vector<std::vector<unsigned int> > > > >",&events);
	t->Fill();

	evt_counter->Write();
	outfile->Write();
	outfile->Close();
}

void GlobalMinimisation::set_handles(std::vector<pat::Jet>* jets, std::vector<pat::Electron>* electrons, std::vector<pat::Muon>* muons)
{
	this->jets = jets;
	this->electrons = electrons;
	this->muons = muons;

	analyse_event();
}

void GlobalMinimisation::analyse_event()
{
	// at least 4 jets and 1 lepton
	if(jets->size() < 4 || (!((electrons->size() == 1) && (muons->size() == 0)) && !((electrons->size() == 0) && (muons->size() == 1))) )
		return;

	int min_value_d[NDIM];
	int max_value_d[NDIM];

	// jet 1 pt
	min_value_d[0] = valuetobin_jet_pt((*jets)[0].pt());
	max_value_d[0] = NBINS-1;
	// jet 2 pt
	min_value_d[1] = valuetobin_jet_pt((*jets)[1].pt());
	max_value_d[1] = NBINS-1;
	// jet 3 pt
	min_value_d[2] = valuetobin_jet_pt((*jets)[2].pt());
	max_value_d[2] = NBINS-1;
	// jet 4 pt
	min_value_d[3] = valuetobin_jet_pt((*jets)[3].pt());
	max_value_d[3] = NBINS-1;
	// lepton 1 pt
	if((electrons->size() == 1) && (muons->size() == 0)){
		min_value_d[4] = valuetobin_lepton_pt((*electrons)[4].pt());
	}
	if((electrons->size() == 0) && (muons->size() == 1)){
		min_value_d[4] = valuetobin_lepton_pt((*muons)[4].pt());
	}
	max_value_d[4] = NBINS-1;

	evt_counter->Fill(0);
	
	if(verbose)
		for(int i=0; i < NDIM; ++i){
			std::cout << "min_bin " << i << " : " << min_value_d[i] << std::endl;
			std::cout << "max_bin " << i << " : " << max_value_d[i] << std::endl;
		}

	for(int d0 = min_value_d[0]; d0 <= max_value_d[0]; ++d0){
		for(int d1 = min_value_d[1]; d1 <= max_value_d[1]; ++d1){
			for(int d2 = min_value_d[2]; d2 <= max_value_d[2]; ++d2){
				for(int d3 = min_value_d[3]; d3 <= max_value_d[3]; ++d3){
					for(int d4 = min_value_d[4]; d4 <= max_value_d[4]; ++d4){
						(*events)[d0][d1][d2][d3][d4]++;
					}
				}
			}
		}
	}
}

int GlobalMinimisation::valuetobin_jet_pt(double pt)
{
	double jet_pt_min = 30;
	double jet_pt_max = 200;

	if(pt > jet_pt_max)
		return NBINS-1;

	if(pt < jet_pt_min)
		return 0;

	double bin_size = (jet_pt_max - jet_pt_min) / (double) NBINS;

	int bin = (int) (((pt-jet_pt_min) / bin_size) + 0.5);

	if(bin < 0)
		return 0;
	else if(bin < NBINS)
		return bin;
	else
		return NBINS-1;
}

int GlobalMinimisation::valuetobin_lepton_pt(double pt)
{
	double lepton_pt_min = 15;
	double lepton_pt_max = 100;

	if(pt > lepton_pt_max)
		return NBINS-1;

	if(pt < lepton_pt_min)
		return 0;

	double bin_size = (lepton_pt_max - lepton_pt_min) / (double) NBINS;

	int bin = (int) (((pt-lepton_pt_min) / bin_size) + 0.5);

	if(bin < 0)
		return 0;
	else if(bin < NBINS)
		return bin;
	else
		return NBINS-1;
}

//int GlobalMinimisation::valuetobin_lepton_eta(double eta)
//{
//	double lepton_eta_min = 0;
//	double lepton_eta_max = 2.2;
//
//	if(eta > lepton_eta_max || eta < lepton_eta_min)
//		return NBINS+1;
//
//	double bin_size = (lepton_eta_max - lepton_eta_min) / (double) NBINS;
//
//	int bin = (int) (((eta-lepton_eta_min) / bin_size) + 0.5);
//
//	return bin;
//}
