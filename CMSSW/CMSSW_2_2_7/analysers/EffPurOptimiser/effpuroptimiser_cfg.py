import FWCore.ParameterSet.Config as cms

process = cms.Process("EffPur")

process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttGenEvent_cff")

process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttSemiLepEvtBuilder_cff")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(150) )

process.load("FWCore.MessageService.MessageLogger_cfi")

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
    'dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/6_PYTHIA6_Tprime_10TeV_cff_PAT_prod2_1.root',
    'dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/1_PYTHIA6_TTbar_10TeV_cff_PAT_1.root',
    'dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/0_PYTHIA6_TTbar_10TeV_cff_PAT_1.root',
    'dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/2_PYTHIA6_TTbar_10TeV_cff_PAT_1.root'
    )
)

process.effpur = cms.EDAnalyzer('EffPurOptimiser',
    electronTag = cms.untracked.InputTag("selectedLayer1Electrons"),
    tauTag      = cms.untracked.InputTag("selectedLayer1Taus"),
    muonTag     = cms.untracked.InputTag("selectedLayer1Muons"),
    jetTag      = cms.untracked.InputTag("selectedLayer1Jets"),
    photonTag   = cms.untracked.InputTag("selectedLayer1Photons"),
    metTag      = cms.untracked.InputTag("selectedLayer1METs"),
    hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypGenMatch:Key"),
      semiLepTag      = cms.untracked.InputTag("ttSemiLepEvent"),
      HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT' ),
            filename  = cms.untracked.string( 'minimisation.root' ),
	######
	#	0:	semi-leptonic ttbar (set channel in analyzer)
	#	1:	ttbar other (everything except signal channel 0)
	#	2:	background (wjets, zjets, qcd...)
	######
	    dataset = cms.untracked.int32(0)
)

process.TFileService = cms.Service("TFileService", fileName = cms.string('histo.root') )

process.p = cms.Path(process.effpur)

process.p0 = cms.Path(process.makeGenEvt * process.makeTtSemiLepEvent)

process.schedule = cms.Schedule( process.p0, process.p )
