# Auto generated configuration file
# using: 
# Revision: 1.222 
# Source: /cvs_server/repositories/CMSSW/CMSSW/Configuration/PyReleaseValidation/python/ConfigBuilder.py,v 
# with command line options: step3_DT1_1 -s HARVESTING:dqmHarvesting --conditions auto:craft09 --filein file:step2_DT1_1_DQM.root --data --customise DQMOffline/Configuration/customHarvesting.py
import FWCore.ParameterSet.Config as cms

process = cms.Process('HARVESTING')

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_38T_cff')
process.load('Configuration.StandardSequences.EDMtoMEAtRunEnd_cff')
process.load('Configuration.StandardSequences.Harvesting_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.EventContent.EventContent_cff')
process.load("DQMOffline.Trigger.FourVectorHLTOfflineClient_cfi")

process.configurationMetadata = cms.untracked.PSet(
    version = cms.untracked.string('$Revision: 1.222 $'),
    annotation = cms.untracked.string('step3_DT1_1 nevts:1'),
    name = cms.untracked.string('PyReleaseValidation')
)
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(1)
)
process.options = cms.untracked.PSet(
    Rethrow = cms.untracked.vstring('ProductNotFound'),
    fileMode = cms.untracked.string('FULLMERGE')
)

# Output definition

# Additional output definition

# Other statements
process.GlobalTag.globaltag = 'CRFT9_38R_V9::All'

# Path and EndPath definitions
process.edmtome_step = cms.Path(process.EDMtoME)
process.validationprodHarvesting = cms.Path(process.hltpostvalidation_prod)
process.validationHarvesting = cms.Path(process.postValidation*process.hltpostvalidation)
process.dqmHarvestingPOGMC = cms.Path(process.DQMOffline_SecondStep_PrePOGMC)
process.validationHarvestingHI = cms.Path(process.postValidationHI)
process.validationHarvestingFS = cms.Path(process.HarvestingFastSim)
process.validationpreprodHarvesting = cms.Path(process.postValidation_preprod*process.hltpostvalidation_preprod)
process.genHarvesting = cms.Path(process.postValidation_gen)
process.dqmHarvestingPOG = cms.Path(process.DQMOffline_SecondStep_PrePOG)
#process.dqmHarvesting = cms.Path(process.DQMOffline_SecondStep*process.DQMOffline_Certification)
process.dqmHarvesting = cms.Path(process.hltFourVectorClient)
process.dqmsave_step = cms.Path(process.DQMSaver)
process.DQMStore.verbose = 0
process.dqmSaver.convention = 'Offline'
process.dqmSaver.workflow = '/Mu/Run2010B-v2/FVHarvested'

# Schedule definition
process.schedule = cms.Schedule(process.edmtome_step,process.dqmHarvesting,process.dqmsave_step)

# Input source
#process.load("DQMOffline.Trigger.curRun_files_cfi")
process.source = cms.Source("PoolSource",
    processingMode = cms.untracked.string('RunsAndLumis'),
    fileNames = cms.untracked.vstring(
        'file:///user/walsh/CMSSW_3_9_8_patch2/src/DQMOffline/Trigger/step2_DT1_1_DQM_test.root',
#     'file:crab_0_101013_185405/res/step2_DT1_1_DQM_10_2_Bs5.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_11_1_Pxs.root',
#     'file:crab_0_101013_185405/res/step2_DT1_1_DQM_12_1_mti.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_13_1_viq.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_14_1_DTk.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_15_1_bOo.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_1_2_qGe.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_2_1_gqf.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_3_2_2H6.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_4_1_nOy.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_5_1_qS8.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_6_1_Rju.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_7_1_bvd.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_8_1_u65.root',
#      'file:crab_0_101013_185405/res/step2_DT1_1_DQM_9_1_QUs.root'
#
      )
)

# Automatic addition of the customisation function


def customise(process):

#    process.dtDataIntegrityUnpacker.inputLabel = cms.untracked.InputTag('rawDataCollector')

#    process.DQMOffline_SecondStep.remove(process.hcalOfflineDQMClient)

    process.options = cms.untracked.PSet(
       wantSummary = cms.untracked.bool(True) 
    )

#    process.load("FWCore.Modules.printContent_cfi")
#    process.myPath1 = cms.Path( process.printContent )

#    process.output.outputCommands.append('drop *')
#    process.output.outputCommands.append('keep *_MEtoEDMConverter_*_*')

#    process.SimpleMemoryCheck = cms.Service("SimpleMemoryCheck",
#     ignoreTotal=cms.untracked.int32(1),
#     oncePerEventMode=cms.untracked.bool(False)
#    )

    process.load("DQMServices.Components.DQMStoreStats_cfi")
    process.stats = cms.EndPath(process.dqmStoreStats)

    process.schedule.append(process.stats)

    return(process)



# End of customisation function definition

process = customise(process)
