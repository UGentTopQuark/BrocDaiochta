# Auto generated configuration file
# using: 
# Revision: 1.222 
# Source: /cvs_server/repositories/CMSSW/CMSSW/Configuration/PyReleaseValidation/python/ConfigBuilder.py,v 
# with command line options: step2_DT1_1 -s DQM -n 1000 --eventcontent RECO --conditions auto:craft09 --geometry Ideal --filein /store/data/Run2010B/Mu/RECO/PromptReco-v2//000/146/644/FE8EF2D6-12C9-DF11-957B-0030487CD7CA.root --data --customise DQMOffline/Configuration/customReco.py
import FWCore.ParameterSet.Config as cms

process = cms.Process('DQM')

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.StandardSequences.GeometryIdeal_cff')
process.load('Configuration.StandardSequences.MagneticField_38T_cff')
#process.load('DQMOffline.Configuration.DQMOffline_cff')
process.load("DQMOffline.Trigger.FourVectorHLTOffline_cfi")
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.EventContent.EventContent_cff')

process.configurationMetadata = cms.untracked.PSet(
    version = cms.untracked.string('$Revision: 1.222 $'),
    annotation = cms.untracked.string('step2_DT1_1 nevts:1000'),
    name = cms.untracked.string('PyReleaseValidation')
)
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(1000)
)
process.options = cms.untracked.PSet(

)
# Input source
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring('file:////user/bklein/trigger_cmssw/CMSSW_3_9_7/src/analysers/FlatNTuples/Dec22_Mu_RECO.root')
)

# Output definition

process.RECOoutput = cms.OutputModule("PoolOutputModule",
    #splitLevel = cms.untracked.int32(0),
    #outputCommands = process.RECOEventContent.outputCommands,
    fileName = cms.untracked.string('step2_DT1_1_DQM_test.root'),
    #dataset = cms.untracked.PSet(
    #    filterName = cms.untracked.string(''),
    #    dataTier = cms.untracked.string('')
    #)
)

# Additional output definition

# Other statements
process.GlobalTag.globaltag = 'CRFT9_38R_V9::All'

# Path and EndPath definitions
#process.dqmoffline_step = cms.Path(process.DQMOffline)
process.dqmoffline_step = cms.Path(process.hltResults)
process.endjob_step = cms.Path(process.endOfProcess)
process.RECOoutput_step = cms.EndPath(process.RECOoutput)

# Schedule definition
#process.schedule = cms.Schedule(process.dqmoffline_step,process.endjob_step,process.RECOoutput_step)
process.schedule = cms.Schedule(process.dqmoffline_step,process.RECOoutput_step)


# Automatic addition of the customisation function


#def customise(process):

#    process.dtDataIntegrityUnpacker.inputLabel = cms.untracked.InputTag('rawDataCollector')

#    process.DQMOfflineCosmics.remove(process.hcalOfflineDQMSource)

#    process.load("FWCore.Modules.printContent_cfi")
#    process.myPath1 = cms.Path( process.printContent )

#    process.options = cms.untracked.PSet(
#       wantSummary = cms.untracked.bool(False) 
#    )
#
#    try :
#      process.output.outputCommands.append('drop *')
#      process.output.outputCommands.append('keep *_MEtoEDMConverter_*_*')
#    except :
#      pass
#
#    try :
#      process.RECOoutput.outputCommands.append('drop *')
#      process.RECOoutput.outputCommands.append('keep *_MEtoEDMConverter_*_*')
#    except :
#      pass
#
#    try :
#      process.RECOSIMoutput.outputCommands.append('drop *')
#      process.RECOSIMoutput.outputCommands.append('keep *_MEtoEDMConverter_*_*')
#    except :
#      pass
#
#    try :
#      process.FEVTDEBUGHLToutput.outputCommands.append('drop *')
#      process.FEVTDEBUGHLToutput.outputCommands.append('keep *_MEtoEDMConverter_*_*')
#    except :
#      pass
#
#    process.SimpleMemoryCheck = cms.Service("SimpleMemoryCheck",
#     ignoreTotal=cms.untracked.int32(1),
#     oncePerEventMode=cms.untracked.bool(False)
#    )

    #process.load("DQMServices.Components.DQMStoreStats_cfi")
    #process.stats = cms.Path(process.dqmStoreStats)
    #process.schedule.insert(-2,process.stats)

 #   return(process)



# End of customisation function definition

#process = customise(process)
