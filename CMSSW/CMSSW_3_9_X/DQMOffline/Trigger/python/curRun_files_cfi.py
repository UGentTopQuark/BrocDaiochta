import FWCore.ParameterSet.Config as cms
source = cms.Source ("PoolSource",
processingMode = cms.untracked.string('RunsAndLumis'),
fileNames = cms.untracked.vstring(
'file:Run_149294_Mu/res/step2_DT1_1_DQM_1_1_Eks.root',
'file:Run_149294_Mu/res/step2_DT1_1_DQM_2_1_2mf.root',
'file:Run_149294_Mu/res/step2_DT1_1_DQM_3_1_h0S.root',
'file:Run_149294_Mu/res/step2_DT1_1_DQM_4_1_FiI.root',
'file:Run_149294_Mu/res/step2_DT1_1_DQM_5_1_Q9O.root',
'file:Run_149294_Mu/res/step2_DT1_1_DQM_6_1_BO1.root',
));

