#include "analyzers/EventSelection/interface/SynchronisationExercise.h"

SynchronisationExercise::SynchronisationExercise()
{
	mass_reco=NULL;
	event_counter = 0;
	jets = NULL;
	electrons = NULL;
	cleaning_electrons = NULL;
	ht = -1.0;
	ident = "";
	pre = "sync_";

	tool = new Tools();
	mu_selector = new LeptonSelector<pat::Muon>();
	e_selector = new LeptonSelector<pat::Electron>();
	cleaning_e_selector = new LeptonSelector<pat::Electron>();
	jet_selector = new JetSelector();
}

SynchronisationExercise::~SynchronisationExercise()
{
	for(std::vector<std::vector<double>* >::iterator to_del = to_delete.begin();
		to_del != to_delete.end();
		++to_del){
		delete *to_del;
	}

	if(tool){
		delete tool;
		tool = NULL;
	}

	if(mu_selector){
		delete mu_selector;
		mu_selector = NULL;
	}
	if(e_selector){
		delete e_selector;
		e_selector = NULL;
	}
	if(jet_selector){
		delete jet_selector;
		jet_selector = NULL;
	}
}

void SynchronisationExercise::set_selection_cuts(std::string ident)
{
	std::vector<double> *max_eta_24 = new std::vector<double>();
	max_eta_24->push_back(2.4);
	to_delete.push_back(max_eta_24);
	std::vector<double> *max_eta_21 = new std::vector<double>();
	max_eta_21->push_back(2.1);
	to_delete.push_back(max_eta_21);
	std::vector<double> *min_pt_30 = new std::vector<double>();
	min_pt_30->push_back(30.0);
	to_delete.push_back(min_pt_30);
	std::vector<double> *max_d0_02 = new std::vector<double>();
	max_d0_02->push_back(0.2);
	to_delete.push_back(max_d0_02);
	std::vector<double> *min_reliso_095 = new std::vector<double>();
	min_reliso_095->push_back(0.95);
	to_delete.push_back(min_reliso_095);
	std::vector<double> *eid1 = new std::vector<double>();
	eid1->push_back(1);
	to_delete.push_back(eid1);

	bool e_event = false;
	bool mu_event = false;

	size_t found = std::string::npos;
	size_t found_bkg = std::string::npos;

	found = ident.find("muon");
	found_bkg = ident.find("mu_background");

	if(found != std::string::npos || found_bkg != std::string::npos){
		/*
		 *	mu selection cuts:
		 */

		mu_selector->set_lepton_type(0);
		jet_selector->set_max_eta(2.4);
		jet_selector->set_min_pt(min_pt_30);
		mu_event = true;
	}

	found = ident.find("electron");
	found_bkg = ident.find("e_background");

	if(found != std::string::npos || found_bkg != std::string::npos){
		/*
		 *	e selection cuts:
		 */

		cleaning_e_selector->set_min_et(min_pt_30);
		cleaning_e_selector->set_max_eta(max_eta_24);
		cleaning_e_selector->set_max_d0(max_d0_02);
		cleaning_e_selector->set_electronID(eid1);

		mu_selector->set_min_pt(min_pt_30);
		mu_selector->set_max_eta(max_eta_21);
		mu_selector->set_min_relIso(min_reliso_095);

		e_event = true;
	}

	if((!e_event && !mu_event) || (e_event && mu_event)){
		std::cerr << "WARNING: couldn't recognise event in SynchronisationExercise::set_selection_cuts()" << std::endl;
	}
}

void SynchronisationExercise::set_ident(std::string ident)
{
	this->ident = ident;	

	set_selection_cuts(ident);
	book_histos();
}

void SynchronisationExercise::set_handles(edm::Handle<edm::View<pat::Muon> > all_mus, edm::Handle<edm::View<pat::Electron> > all_es, edm::Handle<edm::View<pat::Jet> > all_jets, edm::Handle<reco::BeamSpot> recoBeamSpotHandle)
{
	if(jets != NULL){
		delete jets;
		jets = NULL;
	}

	if(electrons != NULL){
		delete electrons;
		electrons = NULL;
	}

	if(cleaning_electrons != NULL){
		delete cleaning_electrons;
		cleaning_electrons = NULL;
	}

	electrons = e_selector->get_leptons(all_es, all_jets, recoBeamSpotHandle);
	cleaning_electrons = cleaning_e_selector->get_leptons(all_es, all_jets, recoBeamSpotHandle);
	muons = mu_selector->get_leptons(all_mus, all_jets, recoBeamSpotHandle);

	jets = jet_selector->get_jets(all_jets, cleaning_electrons);

	beamSpotHandle = recoBeamSpotHandle;
}

void SynchronisationExercise::print()
{
	std::cerr << "sync" << ident << "-" << event_counter << " ";
	print_jets();
	print_muons();
	print_electrons();
	print_masses();
	
	++event_counter;
	// print all information in one line and add at the end here the line break
	std::cerr << std::endl;
}

void SynchronisationExercise::print_muons()
{
	// plot only first muon
	if(muons->begin() != muons->end()){
		print_value("mu_pt", muons->begin()->pt());
		print_value("mu_eta", muons->begin()->eta());

		reco::TrackRef track  = muons->begin()->track();

		double d0 = -1000;
		double chi2 = -1000;
		double pure_chi2 = -1000;
		double nHits = -1000;
	
		if(beamSpotHandle.isValid()){
			if(!track.isNull() && track.isAvailable()){
				reco::BeamSpot beamSpot=*beamSpotHandle;
				math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
				d0 = -1.*track->dxy(point);
				chi2 = muons->begin()->globalTrack()->chi2() / muons->begin()->globalTrack()->ndof();
				pure_chi2 = muons->begin()->globalTrack()->chi2();
				nHits = muons->begin()->track()->numberOfValidHits();
			}else{
				std::cerr << "WARNING: SynchronisationExercise::print_muons(): track not available" << std::endl;
			}
		}else{
			std::cerr << "WARNING: SynchronisationExercise::print_muons(): no valid beamspot found" << std::endl;
		}
		print_value("mu_d0", d0);
		print_value("mu_nHits", nHits,true);
		print_value("mu_chi2", chi2);
		print_value("mu_pure_chi2", chi2);
		print_value("mu_trackiso", muons->begin()->trackIso());
		print_value("mu_ecaliso", muons->begin()->ecalIso());
		print_value("mu_hcaliso", muons->begin()->hcalIso());
		print_value("mu_caliso", muons->begin()->caloIso());
		print_value("mu_hvetocone", muons->begin()->hcalIsoDeposit()->candEnergy());
		print_value("mu_evetocone", muons->begin()->ecalIsoDeposit()->candEnergy());

		double relIso=0;
		relIso = (muons->begin()->caloIso() + muons->begin()->trackIso())/muons->begin()->pt();
		print_value("mu_relIso", relIso);
	}
	else{
		// to keep a consistent format
		print_value("mu_pt", -1);
		print_value("mu_eta", -1);
	}
	print_value("nmuons", muons->size(), true);
}

void SynchronisationExercise::print_masses()
{
	if(mass_reco == NULL){
		std::cerr << "WARNING: no mass reco object found in SynchronisationExercise::plot_masses()" << std::endl;
		return;
	}

	// print M3
	double m3 = mass_reco->calculate_M3();
	print_value("m3", m3);
}

void SynchronisationExercise::print_electrons()
{

	// plot only first muon
	if(electrons->begin() != electrons->end()){
		print_value("e_pt", electrons->begin()->pt());
		print_value("e_eta", electrons->begin()->eta());
		print_value("e_phi", electrons->begin()->phi());
		print_value("e_trackiso", electrons->begin()->trackIso());
		print_value("e_ecaliso", electrons->begin()->ecalIso());
		print_value("e_hcaliso", electrons->begin()->hcalIso());

		double relIso=-1;
		relIso = (electrons->begin()->ecalIso() + electrons->begin()->hcalIso() + electrons->begin()->trackIso())/electrons->begin()->et();
		print_value("e_relIso", relIso);

		reco::GsfTrackRef track  = electrons->begin()->gsfTrack();

		double d0 = -1000;

		if(beamSpotHandle.isValid()){
			if(!track.isNull() && track.isAvailable()){
				reco::BeamSpot beamSpot=*beamSpotHandle;
				math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
				d0 = -1.*track->dxy(point);
			}else{
				std::cerr << "WARNING: SynchronisationExercise::print_electrons(): track not available" << std::endl;
			}
		}else{
			std::cerr << "WARNING: SynchronisationExercise::print_electrons(): no valid beamspot found" << std::endl;
		}
		print_value("e_d0", d0);
	}
	else{
		// to keep a consistent format
		print_value("e_pt", -1);
		print_value("e_eta", -1);
	}
}

void SynchronisationExercise::print_jets()
{
	int jet_counter=0;
	const int MAX_JET_NUMBER=4;
	for(std::vector<pat::Jet>::iterator jet_iter = jets->begin();
		jet_iter != jets->end() && jet_counter < MAX_JET_NUMBER;
		++jet_iter, ++jet_counter)
	{
		print_value("j"+tool->stringify(jet_counter)+"_pt", jet_iter->pt());
	}

	for(;jet_counter < MAX_JET_NUMBER; ++jet_counter){
		print_value("j"+tool->stringify(jet_counter)+"_pt", -1.0);
	}

	jet_counter = 0;
	for(std::vector<pat::Jet>::iterator jet_iter = jets->begin();
		jet_iter != jets->end() && jet_counter < MAX_JET_NUMBER;
		++jet_iter, ++jet_counter)
	{
		print_value("j"+tool->stringify(jet_counter)+"_eta", jet_iter->eta());
	}

	for(;jet_counter < MAX_JET_NUMBER; ++jet_counter){
		print_value("j"+tool->stringify(jet_counter)+"_eta", -1.0);
	}

	print_value("njets", jets->size(), true);
}

void SynchronisationExercise::set_mass_reconstruction(MassReconstruction *mass_reco)
{
	this->mass_reco = mass_reco;
}

void SynchronisationExercise::set_ht(double ht)
{
	this->ht = ht;
}

void SynchronisationExercise::print_value(std::string variable, double value, bool integer)
{
	if(fill_histos && histos1d.find((pre+variable+ident).c_str()) != histos1d.end()){
		histos1d[(pre+variable+ident).c_str()]->Fill(value);
	}
	if(verbose)
		std::cerr << variable << ": ";
	if(integer)
		std::cerr << value << " ";
	else
		fprintf(stderr,"%.3E ",value);
}

void SynchronisationExercise::book_histos()
{
	edm::Service<TFileService> fs;

	int jpt_bins = 50;
	double jpt_min = 0.;
	double jpt_max = 200.;

	histos1d[(pre+"j0_pt"+ident).c_str()]=fs->make<TH1D>((pre+"j0_pt"+ident).c_str(),"j0_pt",jpt_bins,jpt_min,jpt_max);
	histos1d[(pre+"j1_pt"+ident).c_str()]=fs->make<TH1D>((pre+"j1_pt"+ident).c_str(),"j1_pt",jpt_bins,jpt_min,jpt_max);
	histos1d[(pre+"j2_pt"+ident).c_str()]=fs->make<TH1D>((pre+"j2_pt"+ident).c_str(),"j2_pt",jpt_bins,jpt_min,jpt_max);
	histos1d[(pre+"j3_pt"+ident).c_str()]=fs->make<TH1D>((pre+"j3_pt"+ident).c_str(),"j3_pt",jpt_bins,jpt_min,jpt_max);

	int jeta_bins = 50;
	double jeta_min = -6.;
	double jeta_max = 6.;

	histos1d[(pre+"j0_eta"+ident).c_str()]=fs->make<TH1D>((pre+"j0_eta"+ident).c_str(),"j0_eta",jeta_bins,jeta_min,jeta_max);
	histos1d[(pre+"j1_eta"+ident).c_str()]=fs->make<TH1D>((pre+"j1_eta"+ident).c_str(),"j1_eta",jeta_bins,jeta_min,jeta_max);
	histos1d[(pre+"j2_eta"+ident).c_str()]=fs->make<TH1D>((pre+"j2_eta"+ident).c_str(),"j2_eta",jeta_bins,jeta_min,jeta_max);
	histos1d[(pre+"j3_eta"+ident).c_str()]=fs->make<TH1D>((pre+"j3_eta"+ident).c_str(),"j3_eta",jeta_bins,jeta_min,jeta_max);

	histos1d[(pre+"njets"+ident).c_str()]=fs->make<TH1D>((pre+"njets"+ident).c_str(),"njets",10,-0.5,9.5);
	histos1d[(pre+"nmuons"+ident).c_str()]=fs->make<TH1D>((pre+"nmuons"+ident).c_str(),"nmuons",10,-0.5,9.5);
	histos1d[(pre+"nelectrons"+ident).c_str()]=fs->make<TH1D>((pre+"nelectrons"+ident).c_str(),"nelectrons",10,-0.5,9.5);

	// FIXME: random values for binning and bin borders
	histos1d[(pre+"mu_ecaliso"+ident).c_str()]=fs->make<TH1D>((pre+"mu_ecaliso"+ident).c_str(),"mu_ecaliso",10,-0.5,9.5);
	histos1d[(pre+"mu_hcaliso"+ident).c_str()]=fs->make<TH1D>((pre+"mu_hcaliso"+ident).c_str(),"mu_hcaliso",10,-0.5,9.5);
	histos1d[(pre+"mu_caliso"+ident).c_str()]=fs->make<TH1D>((pre+"mu_caliso"+ident).c_str(),"mu_caliso",10,-0.5,9.5);
	histos1d[(pre+"mu_evetocone"+ident).c_str()]=fs->make<TH1D>((pre+"mu_evetocone"+ident).c_str(),"mu_evetocone",10,-0.5,9.5);
	histos1d[(pre+"mu_hvetocone"+ident).c_str()]=fs->make<TH1D>((pre+"mu_hvetocone"+ident).c_str(),"mu_hvetocone",10,-0.5,9.5);
	histos1d[(pre+"mu_nHits"+ident).c_str()]=fs->make<TH1D>((pre+"mu_nHits"+ident).c_str(),"mu_nHits",10,-0.5,9.5);
	histos1d[(pre+"mu_chi2"+ident).c_str()]=fs->make<TH1D>((pre+"mu_chi2"+ident).c_str(),"mu_chi2",10,-0.5,9.5);
	histos1d[(pre+"mu_phi"+ident).c_str()]=fs->make<TH1D>((pre+"mu_phi"+ident).c_str(),"mu_phi",10,-0.5,9.5);
	histos1d[(pre+"mu_eta"+ident).c_str()]=fs->make<TH1D>((pre+"mu_eta"+ident).c_str(),"mu_eta",10,-0.5,9.5);
	histos1d[(pre+"mu_pt"+ident).c_str()]=fs->make<TH1D>((pre+"mu_pt"+ident).c_str(),"mu_pt",10,-0.5,9.5);
	histos1d[(pre+"mu_d0"+ident).c_str()]=fs->make<TH1D>((pre+"mu_d0"+ident).c_str(),"mu_d0",10,-0.5,9.5);
	histos1d[(pre+"mu_relIso"+ident).c_str()]=fs->make<TH1D>((pre+"mu_reliso"+ident).c_str(),"mu_reliso",10,-0.5,9.5);

	histos1d[(pre+"e_pt"+ident).c_str()]=fs->make<TH1D>((pre+"e_pt"+ident).c_str(),"e_pt",10,-0.5,9.5);
	histos1d[(pre+"e_eta"+ident).c_str()]=fs->make<TH1D>((pre+"e_eta"+ident).c_str(),"e_eta",10,-0.5,9.5);
	histos1d[(pre+"e_d0"+ident).c_str()]=fs->make<TH1D>((pre+"e_d0"+ident).c_str(),"e_d0",10,-0.5,9.5);
	histos1d[(pre+"e_relIso"+ident).c_str()]=fs->make<TH1D>((pre+"e_reliso"+ident).c_str(),"e_reliso",10,-0.5,9.5);
	histos1d[(pre+"e_phi"+ident).c_str()]=fs->make<TH1D>((pre+"e_phi"+ident).c_str(),"e_phi",10,-0.5,9.5);
}
