// -*- C++ -*-
//
// Package:    EventSelection
// Class:      EventSelection
// 
/**\class EventSelection EventSelection.cc analyzers/EventSelection/src/EventSelection.cc

 Description: <one line class summary>

 Implementation:
     <Notes on implementation>
*/
//
// Original Author:  local user
//         Created:  Wed Feb 18 17:06:16 CET 2009
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"
#include "FWCore/ParameterSet/interface/InputTag.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"

#include "AnalysisDataFormats/TopObjects/interface/TtGenEvent.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

//#include "TopQuarkAnalysis/Examples/plugins/HypothesisAnalyzer.h"
#include "DataFormats/Candidate/interface/Candidate.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "analyzers/EventSelection/interface/PlotGenerator.h"
#include "analyzers/EventSelection/interface/Cuts.h"
//#include "analyzers/EventSelection/interface/TTbar.h"
#include "analyzers/EventSelection/interface/CutSelector.h"

#include "TH1D.h"
#include "TH2D.h"
#include <map>


#include "DataFormats/Common/interface/View.h"
#include <string>


//
// class decleration
//

class EventSelection : public edm::EDAnalyzer {
   public:
      explicit EventSelection(const edm::ParameterSet&);
      ~EventSelection();


   private:
      virtual void beginJob(const edm::EventSetup&) ;
      virtual void analyze(const edm::Event&, const edm::EventSetup&);
      virtual void endJob() ;

	std::map<std::string,TH1D*> histocontainer_;

	edm::InputTag eleLabel_;
	edm::InputTag muoLabel_;
	edm::InputTag jetLabel_;
	edm::InputTag tauLabel_;
	edm::InputTag metLabel_;
	edm::InputTag phoLabel_;
  //edm::InputTag hltLabel_;
        edm::InputTag semiLepEvt_;
        edm::InputTag hypoClassKey_;
        edm::InputTag hlTriggerResults_;
  // Set dataset name in configuration file
	std::string datasetName_;
	double tprimeMass;

	CutSelector *cut_selector;
     

      // ----------member data ---------------------------
};

//analyzers/EventSelection

// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
EventSelection::EventSelection(const edm::ParameterSet& iConfig):
  histocontainer_(),
  eleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("electronTag")),
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  tauLabel_(iConfig.getUntrackedParameter<edm::InputTag>("tauTag")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  phoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("photonTag")),
  // hltLabel_(iConfig.getUntrackedParameter<edm::InputTag>("hltTag")),
  semiLepEvt_(iConfig.getUntrackedParameter<edm::InputTag>("semiLepTag")),
  hypoClassKey_(iConfig.getUntrackedParameter<edm::InputTag>("hypoClassKey")),
  hlTriggerResults_ (iConfig.getParameter<edm::InputTag> ("HLTriggerResults")),
  datasetName_(iConfig.getUntrackedParameter<std::string>("datasetName")),
  tprimeMass (iConfig.getUntrackedParameter<double> ("tprimeMass"))
{
	cut_selector = new CutSelector(datasetName_, tprimeMass);
	cut_selector->set_tprime_mass(tprimeMass);
}


EventSelection::~EventSelection()
{
	delete cut_selector;
	cut_selector=NULL;
}


//
// member functions
//

// ------------ method called to for each event  ------------
void
EventSelection::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	using namespace edm;

	edm::Handle<edm::View<pat::Muon> > muonHandle;
	iEvent.getByLabel(muoLabel_,muonHandle);

	edm::Handle<edm::View<pat::Jet> > jetHandle;
	iEvent.getByLabel(jetLabel_,jetHandle);
	
	edm::Handle<edm::View<pat::Electron> > electronHandle;
	iEvent.getByLabel(eleLabel_,electronHandle);
	
	edm::Handle<edm::View<pat::MET> > metHandle;
	iEvent.getByLabel(metLabel_,metHandle);

	//if doesnt work try adding reco:: before
	edm::Handle<reco::GenParticleCollection> genParticles;
	iEvent.getByLabel("genParticles", genParticles);

	edm::Handle<edm::TriggerResults> HLTR;
	iEvent.getByLabel(hlTriggerResults_,HLTR);


	 // set genEvent
	edm::Handle<TtGenEvent> genEvt;
	iEvent.getByLabel("genEvt", genEvt); 

	edm::Handle<TtSemiLeptonicEvent> semiLepEvt;
	iEvent.getByLabel(semiLepEvt_, semiLepEvt);

	 edm::Handle<int> hypoClassKeyHandle;
	 iEvent.getByLabel(hypoClassKey_, hypoClassKeyHandle);

	 edm::Handle<reco::BeamSpot> recoBeamSpotHandle;
	 iEvent.getByLabel("offlineBeamSpot", recoBeamSpotHandle);

	 cut_selector->set_handles(muonHandle, jetHandle, electronHandle, metHandle, genEvt, semiLepEvt, hypoClassKeyHandle, HLTR,genParticles, recoBeamSpotHandle);
	 cut_selector->plot();
	
	//print(const std::vector<pat::TriggerPrimitive> triggerMatchesByFilter("hltSingleMuNoIsoL3PreFiltered11").size() const);


}


// ------------ method called once each job just before starting event loop  ------------
void 
EventSelection::beginJob(const edm::EventSetup&)
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
EventSelection::endJob() {
}

//define this as a plug-in
DEFINE_FWK_MODULE(EventSelection);
