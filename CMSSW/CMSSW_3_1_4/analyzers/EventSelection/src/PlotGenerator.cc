#include "analyzers/EventSelection/interface/PlotGenerator.h" 

PlotGenerator::PlotGenerator(std::string ident)
{
        cuts=NULL;
	id = "_"+ident;

	// book histos by means of cmssw file service
	book_histos();

	jets=NULL;
	isolated_muons = NULL;
	isolated_electrons = NULL;
	all_electrons = NULL;
	corrected_mets = NULL;
	uncorrected_mets = NULL;

	// Initialise LeptonSelector for both leptons
	e_selector = new LeptonSelector<pat::Electron>();
	all_e_selector = new LeptonSelector<pat::Electron>();
        mu_selector = new LeptonSelector<pat::Muon>();
	jet_selector = new JetSelector();
        METCor = new METCorrector();

	// objects for generator truth information
	gen_match = new GenMatch();

	// FIXME: mass_reco set when cuts object set (defined there)
	// -> leads to segfault when cuts not defined
	mass_reco = NULL;
	mass_reco_corMET = NULL;
	mass_jet_match = NULL;
	reco_gen_match = NULL;
	bjet_finder = NULL;

	bjet_finder = new BJetFinder();
//	mass_reco_corMET = new MassReconstruction();
	mass_jet_match = new MassJetMatch();
	reco_gen_match = new RecoGenMatch();
//	reco_gen_match->set_matching_method(2);
	sync_ex = new SynchronisationExercise();
	sync_ex->set_ident(id);

	tprime_mass = -1;

	//initialise btag variables
	name_btag = "-1";

	/*
	 *	book plot classes
	 */
	met_plots = new METPlots(id);
	met_plots->set_gen_match(gen_match);
}

PlotGenerator::~PlotGenerator()
{
	histos1d[("W_jets_assignment"+id).c_str()]->Sumw2();
	histos1d[("W_jets_assignment"+id).c_str()]->Scale(1.0/histos1d[("W_jets_assignment"+id).c_str()]->Integral());
	//Divide histograms to get one of trigger eff vs pt||eta
	//get_triggereff();
	
	if(met_plots != NULL){
		delete met_plots;
		met_plots = NULL;
	}

	if(e_selector != NULL){
		delete e_selector;
		e_selector=NULL;
	}
	if(all_e_selector != NULL){
		delete all_e_selector;
		all_e_selector=NULL;
	}
	if(mu_selector != NULL){
		delete mu_selector;
		mu_selector=NULL;
	}

	if(jet_selector != NULL){
		delete jet_selector;
		jet_selector = NULL;
	}

        if(jets != NULL){
                delete jets;
                jets = NULL;
        }
        if(isolated_electrons != NULL){
                delete isolated_electrons;
                isolated_electrons = NULL;
        }
        if(all_electrons != NULL){
                delete all_electrons;
                all_electrons = NULL;
        }
        if(isolated_muons != NULL){
                delete isolated_muons;
                isolated_muons = NULL;
        }
	
	if(mass_reco_corMET != NULL){
		delete mass_reco_corMET;
		mass_reco_corMET = NULL;
	}

	if(mass_jet_match != NULL){
		delete mass_jet_match;
		mass_jet_match = NULL;
	}
	if(gen_match != NULL){
		delete gen_match;
		gen_match = NULL;
	}
	if(reco_gen_match != NULL){
		delete reco_gen_match;
		reco_gen_match = NULL;
	}
	if(sync_ex != NULL){
		delete sync_ex;
		sync_ex = NULL;	
	}

	if(bjet_finder != NULL){
		delete bjet_finder;
		bjet_finder = NULL;	
	}

	if(METCor != NULL){
		delete METCor;
		METCor = NULL;
	}
}

void PlotGenerator::set_lepton_selector_cuts()
{
	if(cuts != NULL){
		e_selector->set_max_trackiso(cuts->get_max_e_trackiso());
		e_selector->set_max_caliso(cuts->get_max_e_caliso());
		e_selector->set_max_ecaliso(cuts->get_max_e_ecaliso());
		e_selector->set_max_hcaliso(cuts->get_max_e_hcaliso());
		e_selector->set_max_hcal_veto_cone(cuts->get_max_e_hcal_veto_cone());
		e_selector->set_max_ecal_veto_cone(cuts->get_max_e_hcal_veto_cone());
		e_selector->set_min_pt(cuts->get_min_e_pt());
		e_selector->set_min_et(cuts->get_min_e_et());
		e_selector->set_min_dR(cuts->get_min_e_dR());
		e_selector->set_min_relIso(cuts->get_min_e_relIso());
		e_selector->set_max_d0(cuts->get_max_e_d0());
		e_selector->set_max_d0sig(cuts->get_max_e_d0sig());
		e_selector->set_max_chi2(cuts->get_max_e_chi2());
		e_selector->set_min_nHits(cuts->get_min_e_nHits());
		e_selector->set_electronID(cuts->get_e_electronID());
		e_selector->set_max_eta(cuts->get_max_e_eta());
		e_selector->set_lepton_type(cuts->get_e_type());

		mu_selector->set_max_trackiso(cuts->get_max_mu_trackiso());
		mu_selector->set_max_caliso(cuts->get_max_mu_caliso());
		mu_selector->set_max_ecaliso(cuts->get_max_mu_ecaliso());
		mu_selector->set_max_hcaliso(cuts->get_max_mu_hcaliso());
		mu_selector->set_max_hcal_veto_cone(cuts->get_max_mu_hcal_veto_cone());
		mu_selector->set_max_ecal_veto_cone(cuts->get_max_mu_hcal_veto_cone());
		mu_selector->set_min_pt(cuts->get_min_mu_pt());
		mu_selector->set_min_et(cuts->get_min_mu_et());
		mu_selector->set_min_dR(cuts->get_min_mu_dR());
		mu_selector->set_min_relIso(cuts->get_min_mu_relIso());
		mu_selector->set_max_d0(cuts->get_max_mu_d0());
		mu_selector->set_max_d0sig(cuts->get_max_mu_d0sig());
		mu_selector->set_max_chi2(cuts->get_max_mu_chi2());
		mu_selector->set_min_nHits(cuts->get_min_mu_nHits());
		mu_selector->set_electronID(cuts->get_mu_electronID());
		mu_selector->set_max_eta(cuts->get_max_mu_eta());
		mu_selector->set_lepton_type(cuts->get_mu_type());

		jet_selector->set_min_e_dR(cuts->get_min_jet_e_dR());
		jet_selector->set_max_eta(cuts->get_max_jet_eta());
		jet_selector->set_min_pt(cuts->get_min_jet_pt());
		jet_selector->change_JES(cuts->get_JES_factor());
	}
}

void PlotGenerator::unset_lepton_selector_cuts()
{
	e_selector->set_max_trackiso(NULL);
	e_selector->set_max_caliso(NULL);
	e_selector->set_max_ecaliso(NULL);
	e_selector->set_max_hcaliso(NULL);
	e_selector->set_max_hcal_veto_cone(NULL);
	e_selector->set_max_ecal_veto_cone(NULL);
	e_selector->set_min_pt(NULL);
	e_selector->set_min_et(NULL);
	e_selector->set_min_dR(NULL);
	e_selector->set_min_relIso(NULL);
	e_selector->set_max_d0(NULL);
	e_selector->set_max_d0sig(NULL);
	e_selector->set_max_chi2(NULL);
	e_selector->set_min_nHits(NULL);
	e_selector->set_electronID(NULL);
	e_selector->set_max_eta(NULL);

	mu_selector->set_max_trackiso(NULL);
	mu_selector->set_max_caliso(NULL);
	mu_selector->set_max_ecaliso(NULL);
	mu_selector->set_max_hcaliso(NULL);
	mu_selector->set_max_hcal_veto_cone(NULL);
	mu_selector->set_max_ecal_veto_cone(NULL);
	mu_selector->set_min_pt(NULL);
	mu_selector->set_min_et(NULL);
	mu_selector->set_min_dR(NULL);
	mu_selector->set_min_relIso(NULL);
	mu_selector->set_max_d0(NULL);
	mu_selector->set_max_d0sig(NULL);
	mu_selector->set_max_chi2(NULL);
	mu_selector->set_min_nHits(NULL);
	mu_selector->set_electronID(NULL);
	mu_selector->set_max_eta(NULL);

	jet_selector->set_min_pt(NULL);
	jet_selector->set_max_eta(-1.0);
	jet_selector->change_JES(-1.0);
}

void PlotGenerator::set_handles(edm::Handle<edm::View<pat::Muon> > analyser_muons,
                           edm::Handle<edm::View<pat::Jet> > analyser_jets,
                           edm::Handle<edm::View<pat::Electron> > analyser_electrons,
			   edm::Handle<edm::View<pat::MET> > analyser_mets,
			   edm::Handle<TtSemiLeptonicEvent> semiLepEvt,
			   edm::Handle<int> hypoClassKeyHandle,
			   edm::Handle<TtGenEvent> genEvt,
			   edm::Handle<edm::TriggerResults> HLTR,
			   edm::Handle<reco::GenParticleCollection> genParticles,
			   edm::Handle<reco::BeamSpot> recoBeamSpotHandle)
{
	ht = -1;

	electrons = analyser_electrons;
	muons = analyser_muons;
	uncut_jets = analyser_jets;
	this->semiLepEvt = semiLepEvt;
	this->hypoClassKeyHandle = hypoClassKeyHandle;
	this->genEvt = genEvt;
	this->HLTR = HLTR;
	this->genParticles = genParticles;
	beamSpotHandle = recoBeamSpotHandle;

        if(isolated_electrons != NULL){
                delete isolated_electrons;
                isolated_electrons = NULL;
        }
        isolated_electrons = e_selector->get_leptons(electrons, uncut_jets, recoBeamSpotHandle);

        if(all_electrons != NULL){
                delete all_electrons;
                all_electrons = NULL;
        }

        all_electrons = all_e_selector->get_leptons(electrons, uncut_jets, recoBeamSpotHandle);

        if(isolated_muons != NULL){
                delete isolated_muons;
                isolated_muons = NULL;
        }
        isolated_muons = mu_selector->get_leptons(muons, uncut_jets, recoBeamSpotHandle);

	if(jets != NULL){
		delete jets;
		jets = NULL;
	}

	jets = jet_selector->get_jets(analyser_jets, all_electrons);
	mets = analyser_mets;

        if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
		pat::Particle *muon = new pat::Particle();
		muon->setP4((*isolated_muons)[0].p4());
                METCor->set_handles(analyser_mets, muon);
                corrected_mets = METCor->get_MET();
		delete muon;
		muon = NULL;
        }
        else if(isolated_electrons->size() == 1 && isolated_muons->size() == 0){
		pat::Particle *electron = new pat::Particle();
		electron->setP4((*isolated_electrons)[0].p4());
                METCor->set_handles(analyser_mets, electron);
                corrected_mets = METCor->get_MET();
		delete electron;
		electron = NULL;
        }else{
                METCor->set_handles(mets);
                corrected_mets = METCor->get_uncorrected_MET();
	}

	//mass_reco_corMET->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	reco_gen_match->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets,genParticles,muons);
	mass_jet_match->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets,genParticles,muons);
	gen_match->fill_events_from_collection(genParticles);
	bjet_finder->set_handles(jets);

	sync_ex->set_handles(analyser_muons, analyser_electrons, analyser_jets, recoBeamSpotHandle);

	/*
	 *	setting handles for plot classes
	 */
	met_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
}


void PlotGenerator::plot()
{

	//std::vector<double> temp_vec (1,5);
	//std::cout<<"Temp Vector first "<< temp_vec.size() <<" "<< temp_vec[0]<<std::endl;
	// The current W mass got from http://pdg.lbl.gov
	double nominal_massW = 80.398;

	/*
	 *	Plots with applied cuts
	 */
	// plot if cuts are applied (cuts != NULL) and event passes cut
	// cirteria or if cuts are not applied (cuts == NULL)
	if((cuts != NULL && cuts->cut() == false)
	   || cuts == NULL){

		plot_jets();
		plot_masstop();
		plot_chimass_top_012btag();
 		plot_massjetmatch_top();
   		plot_massW(nominal_massW);
  		plot_MET_MtW();
 		plot_muons();
 		plot_electrons();
 		plot_ht();
  		plot_dR();
 		plot_leptIso_muon();
 		plot_leptIso_electron();
		//the following three use highest and second highest btagged jets, for selecting correct jet for mass reco
   		plot_bDiscriminator();
   		plot_bDiscriminator_allbjets();
  		analyse_btag_algorithms();
 		plot_MCmethod_comparison();
		//this function is to plot the difference between signal btag value and background in order to apply cut
		plot_bDiscriminator_find_btagcut();
  		plot_muon_quality();
		plot_Wb_mass();

 		plot_recogenmatch_top();

		//FIXME: must comment out one of following two for which hypothesis is not defines in cfg

		matchleptons_plotiso_plotveto();
		//plot_Kinfit_top();
  		plot_minmass_1btag_top();
  		plot_minmass_2btag_top();
  		plot_genmass_top();
  		plot_chimass_top();
  		plot_chimass_1btag_top();
  		plot_chimass_2btag_top();
		//plot_trigger(); //get_triggereff in destructor

		//determine sigmas,dR cut
		//analyse_recogen_mindD();
		//analyse reco gen matching with dR cut applied
		analyse_recogen_matchedjets();

		sync_ex->set_ht(ht);
		sync_ex->print();

		/*
		 *	plot individual plot classes
		 */

		met_plots->plot();
	
		histos1d[("event_counter"+id).c_str()]->Fill(1);
	}

	/*
	 *	Plots created even for events that don't pass the cuts
	 */
	histos1d[("event_counter"+id).c_str()]->Fill(0);

}

void PlotGenerator::plot_muon_quality()
{
	for(std::vector<pat::Muon>::iterator muon_iter = isolated_muons->begin();
		muon_iter != isolated_muons->end();
		++muon_iter){
		reco::TrackRef track  = muon_iter->track();
		reco::TrackRef globaltrack  = muon_iter->globalTrack();

		if(!track.isNull()){
			if(!globaltrack.isNull())
				histos1d[("muon_chi2"+id).c_str()]->Fill(muon_iter->globalTrack()->chi2()/muon_iter->globalTrack()->ndof());
			histos1d[("muon_nHits"+id).c_str()]->Fill(muon_iter->track()->numberOfValidHits());
			histos1d[("muon_d0"+id).c_str()]->Fill(muon_iter->track()->d0());
			histos2d[("muon_d0_vs_phi_wo_BScorr"+id).c_str()]->Fill(muon_iter->phi(),muon_iter->track()->d0());

		}
		double d0 = -1000;
		
		if(beamSpotHandle.isValid()){
			if(!track.isNull() && track.isAvailable()){
				reco::BeamSpot beamSpot=*beamSpotHandle;
				math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
				d0 = -1.*track->dxy(point);
				histos2d[("muon_d0_vs_phi"+id).c_str()]->Fill(muon_iter->phi(),d0);
			}
		}
	}
}

//plot pt, eta, and numbet of jets
void PlotGenerator::plot_jets()
{
  
	int jet_cut_id = 0;
	for(std::vector<pat::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		
		// Plot pt of first four jets
		if(jet_cut_id ==0 ){
			histos1d[("jet1_pt"+id).c_str()]->Fill(jet_iter->pt());
			histos1d[("jet1_eta"+id).c_str()]->Fill(jet_iter->eta());
			histos2d[("jet1_pt_vs_et"+id).c_str()]->Fill(jet_iter->pt(),jet_iter->et());	     
		}
		else if(jet_cut_id == 1 ){
			histos1d[("jet2_pt"+id).c_str()]->Fill(jet_iter->pt());
			histos1d[("jet2_eta"+id).c_str()]->Fill(jet_iter->eta()); 
			histos2d[("jet2_pt_vs_et"+id).c_str()]->Fill(jet_iter->pt(),jet_iter->et());
		}
		else if(jet_cut_id == 2 ){
			histos1d[("jet3_pt"+id).c_str()]->Fill(jet_iter->pt());
		}
		else if(jet_cut_id == 3 ){
			histos1d[("jet4_pt"+id).c_str()]->Fill(jet_iter->pt());
		}
		
		++jet_cut_id;
		
		
		histos2d[("jet_pt_vs_et"+id).c_str()]->Fill(jet_iter->pt(),jet_iter->et());
		histos1d[("jet_pt"+id).c_str()]->Fill(jet_iter->pt());
		histos1d[("jet_eta"+id).c_str()]->Fill(jet_iter->eta());
		histos1d[("jet_phi"+id).c_str()]->Fill(jet_iter->phi());
	}
	
	histos1d[("jet_number"+id).c_str()]->Fill(jets->size());
}

 // Plot eta,pt and number of muons
void PlotGenerator::plot_muons()
{
	// Get back vector of muons that passed all cuts in LeptonSelector class
   
	for(std::vector<pat::Muon>::iterator isolated_mu = isolated_muons->begin();
	    isolated_mu!=isolated_muons->end(); 
	    ++isolated_mu){
                histos1d[("muon_pt"+id).c_str()]->Fill(isolated_mu->pt());
	        histos1d[("muon_eta"+id).c_str()]->Fill(isolated_mu->eta());
	        histos1d[("muon_phi"+id).c_str()]->Fill(isolated_mu->phi());
        }
	
	histos1d[("muon_number"+id).c_str()]->Fill(isolated_muons->size());

}

// Plot eta,pt and number of electrons
void PlotGenerator::plot_electrons()
{
	// Get back vector of electrons that passed all cuts in LeptonSelector class
	
	for(std::vector<pat::Electron>::iterator isolated_e = isolated_electrons->begin();isolated_e != isolated_electrons->end();++isolated_e){
                histos1d[("electron_pt"+id).c_str()]->Fill(isolated_e->pt());
	        histos1d[("electron_eta"+id).c_str()]->Fill(isolated_e->eta());
	        histos1d[("electron_phi"+id).c_str()]->Fill(isolated_e->phi());
        }

	histos1d[("electron_number"+id).c_str()]->Fill(isolated_electrons->size());

}

void PlotGenerator::calculate_ht()
{
  	double pt_sum=0;

	for(std::vector<pat::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		pt_sum += jet_iter->pt();
	}

	std::vector<pat::Muon>::const_iterator mu_iter = isolated_muons->begin();

	if(mu_iter != isolated_muons->end())
		pt_sum += mu_iter->pt();

	std::vector<pat::Electron>::const_iterator e_iter = isolated_electrons->begin();

	if(e_iter != isolated_electrons->end())
		pt_sum += e_iter->pt();

	//pt_sum += corrected_mets->begin()->pt();
	ht = pt_sum;
}

// Plot transverse energy:sum of all jets and the muon pt
void PlotGenerator::plot_ht()
{
	if(ht == -1)
		calculate_ht();
	
	histos1d[("Ht"+id).c_str()]->Fill(ht+corrected_mets->begin()->pt());
	histos1d[("Ht_wo_MET"+id).c_str()]->Fill(ht);
}

// Plot leptonic transverse mass and MET 
void PlotGenerator::plot_MET_MtW()
{
	if(mets->size() < 1)
		return;

	// Loop to get total MET, should only be one iteration of loop.
	double event_met = 0.0,e_MtW=0.0,mu_MtW=0.0;
	for(edm::View<pat::MET>::const_iterator met_iter = mets->begin();
	    met_iter!=mets->end();
	    ++met_iter){
		event_met=met_iter->et(); 
		
		// Check for at least one lepton before calculating Mt	
		if(isolated_muons->size() > 0){
	   		mu_MtW = ((*isolated_muons)[0].p4()+met_iter->p4()).Mt(); 
	   		histos1d[("muon_MtW"+id).c_str()]->Fill(mu_MtW);
		}
		       
		if(isolated_electrons->size() > 0){
	   		e_MtW = ((*isolated_electrons)[0].p4()+met_iter->p4()).Mt();
	   		histos1d[("electron_MtW"+id).c_str()]->Fill(e_MtW); 
		}
	}
	histos1d[("Event_MET"+id).c_str()]->Fill(mets->begin()->et());
	histos1d[("Event_MET_pt"+id).c_str()]->Fill(mets->begin()->pt());	
	//histos1d[("Event_MET_w_pz"+id).c_str()]->Fill(corrected_mets->begin()->et());
	histos1d[("Event_MET_pt_w_pz"+id).c_str()]->Fill(corrected_mets->begin()->pt());
}

// For estimation of QCD usingABCD method, plot lepton isolation vs MET
void PlotGenerator::plot_leptIso_muon()
{
	if(mets->size() < 1 || isolated_muons->size() < 1)
		return;

	double event_met = 0.0, lepton_isolation = 0.0;
	for(edm::View<pat::MET>::const_iterator met_iter = mets->begin();
	    met_iter!=mets->end();
	    ++met_iter){
		event_met=met_iter->et(); 
	}
	
	std::vector<pat::Muon>::const_iterator muon_iter = isolated_muons->begin();
	lepton_isolation = muon_iter->trackIso(); // FIXME:Saw trackIso in one talk check if correct
	
	histos2d[("isolation_vs_met_muon"+id).c_str()]->Fill(lepton_isolation, event_met);
}

// For estimation of QCD usingABCD method, plot lepton isolation vs MET
void PlotGenerator::plot_leptIso_electron()
{
	if(mets->size() < 1 || isolated_electrons->size() < 1)
		return;

	double event_met = 0.0, lepton_isolation = 0.0;
	for(edm::View<pat::MET>::const_iterator met_iter = mets->begin();
                met_iter!=mets->end();
                ++met_iter){
		event_met=met_iter->et(); 
	}
	
	std::vector<pat::Electron>::const_iterator electron_iter = isolated_electrons->begin();
	lepton_isolation = electron_iter->trackIso(); // FIXME:Saw trackIso in one talk check if correct
	
	histos2d[("isolation_vs_met_electron"+id).c_str()]->Fill(lepton_isolation, event_met);
}


//M3: Plot the invariant mass of the three jets with the highest sum pt for the final selection
void PlotGenerator::plot_masstop()
{
	if(ht == -1)
		calculate_ht();
	
	histos1d[("top_mass"+id).c_str()]->Fill(mass_reco->calculate_M3());
	histos2d[("top_mass_vs_ht"+id).c_str()]->Fill(ht,mass_reco->calculate_M3());
	
	//histos1d[("top_mass_minimisation_w_METpz"+id).c_str()]->Fill(mass_reco_corMET->calculate_min_diff_M3());
	histos1d[("top_mass_minimisation"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
	histos2d[("top_mass_minimisation_vs_ht"+id).c_str()]->Fill(ht,mass_reco->calculate_min_diff_M3());
}

void PlotGenerator::plot_Wb_mass()
{
	if(mass_reco == NULL)
		return;

	histos1d[("top_hadmass_Wb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_top());
	histos1d[("W_hadmass_Wb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_W_mass());
	histos1d[("top_lepmass_Wb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_lepTmass_top_2btag());
	histos1d[("Wb_dR_W_1stb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_dR_W_bjet1());
	histos1d[("Wb_dR_W_2ndb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_dR_W_bjet2());

	histos1d[("top_hadmass_Wb_1btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_hadTmass_top_1btag());
	histos1d[("top_lepmass_Wb_1btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_lepTmass_top_1btag());
	histos1d[("W_hadmass_Wb_1btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_hadWmass_1btag());

	histos1d[("top_hadmass_Wb_0btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_hadTmass_top_0btag());
	histos1d[("top_lepmass_Wb_0btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_lepTmass_top_0btag());
	histos1d[("W_hadmass_Wb_0btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_hadWmass_0btag());
}

void PlotGenerator::plot_massjetmatch_top()
{
	if(mass_jet_match == NULL)
		return;
		
	//for jet matching
	//top mass
	
	switch(mass_jet_match->nmatches_m3()){
		case 0:
			histos1d[("top_mass_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
		case 1:
			histos1d[("top_mass_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
		case 2:
			histos1d[("top_mass_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
		case 3:
			histos1d[("top_mass_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
		default:
			histos1d[("top_mass_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
	}
	
	//   //top mass minimisation
	switch(mass_jet_match->nmatches_min_diff_m3()){
		case 0:
			histos1d[("top_mass_minimisation_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
		case 1:
			histos1d[("top_mass_minimisation_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
		case 2:
			histos1d[("top_mass_minimisation_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
		case 3:
			histos1d[("top_mass_minimisation_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
		default:
			histos1d[("top_mass_minimisation_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
	}

	//chimass
	switch(mass_jet_match->nmatches_chi2()){
		case 0:
			histos1d[("top_Had_chimass_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
		case 1:
			histos1d[("top_Had_chimass_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
		case 2:
			histos1d[("top_Had_chimass_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
		case 3:
			histos1d[("top_Had_chimass_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
		default:
			histos1d[("top_Had_chimass_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
	}

	//chimass 1btag
	switch(mass_jet_match->nmatches_chi2_1btag()){
		case 0:
			histos1d[("top_Had_1btag_chimass_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
		case 1:
			histos1d[("top_Had_1btag_chimass_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
		case 2:
			histos1d[("top_Had_1btag_chimass_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
		case 3:
			histos1d[("top_Had_1btag_chimass_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
		default:
			histos1d[("top_Had_1btag_chimass_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
	}

	//chimass 2btag
	switch(mass_jet_match->nmatches_chi2_2btag()){
		case 0:
			histos1d[("top_Had_2btag_chimass_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
		case 1:
			histos1d[("top_Had_2btag_chimass_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
		case 2:
			histos1d[("top_Had_2btag_chimass_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
		case 3:
			histos1d[("top_Had_2btag_chimass_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
		default:
			histos1d[("top_Had_2btag_chimass_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
	}

	//minmass 1btag
	switch(mass_jet_match->nmatches_min_diff_m3_1btag()){
		case 0:
			histos1d[("top_mass_minimisation_1btag_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
		case 1:
			histos1d[("top_mass_minimisation_1btag_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
		case 2:
			histos1d[("top_mass_minimisation_1btag_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
		case 3:
			histos1d[("top_mass_minimisation_1btag_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
		default:
			histos1d[("top_mass_minimisation_1btag_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
	}
		
	//minmass 2btag
	switch(mass_jet_match->nmatches_min_diff_m3_2btag()){
		case 0:
			histos1d[("top_mass_minimisation_2btag_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
		case 1:
			histos1d[("top_mass_minimisation_2btag_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
		case 2:
			histos1d[("top_mass_minimisation_2btag_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
		case 3:
			histos1d[("top_mass_minimisation_2btag_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
		default:
			histos1d[("top_mass_minimisation_2btag_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
	}
		
}

void PlotGenerator::analyse_recogen_mindD()
{
	if(!reco_gen_match->is_geninfo_available()){
		return;
	}
	

	std::map<std::string,int> mindD_jet_ids = reco_gen_match->get_recojets_id_no_dRcut(); //no dR cut
	std::map<std::string,reco::GenParticle*> genquark;

	//truth matching  
	if(gen_match != NULL && gen_match->is_ok() && gen_match->get_decay_mode() == 1){
		
		//All from hadronic top decay
		if(!(gen_match->t_decay_is_leptonic())){	
			genquark["Wquark1"] = gen_match->get_particle("Wplus_dp1");
			genquark["Wquark2"] = gen_match->get_particle("Wplus_dp2");
			genquark["hadb"] = gen_match->get_particle("b");
			genquark["lepb"] = gen_match->get_particle("bbar");
		}
		
		else if(!(gen_match->tbar_decay_is_leptonic())){
			genquark["Wquark1"]= gen_match->get_particle("Wminus_dp1");
			genquark["Wquark2"]= gen_match->get_particle("Wminus_dp2");
			genquark["hadb"]= gen_match->get_particle("bbar");
			genquark["lepb"]= gen_match->get_particle("b");
		  
		}
	}

	//determining sigmas
	//double const pi = 3.1416;
  	double const sigma_dphi = 0.03;
  	double const sigma_deta = 0.03;
  	double const sigma_dpt = 0.16;
//   	double const sigma_dphi = 1;
//   	double const sigma_deta = 1;
//  	double const sigma_dpt = 10;
	for(std::map<std::string,reco::GenParticle*>::iterator quark = genquark.begin();quark!=genquark.end();++quark){
		if(mindD_jet_ids[quark->first] != -1){
			double dR = ROOT::Math::VectorUtil::DeltaR(quark->second->p4(),(*jets)[mindD_jet_ids[quark->first]].p4());
			if(dR < 1){
			double dPt = (((*jets)[mindD_jet_ids[quark->first]].pt()- quark->second->pt())/quark->second->pt());
			double deta = ((*jets)[mindD_jet_ids[quark->first]].eta() - quark->second->eta());
			double dphi = ((*jets)[mindD_jet_ids[quark->first]].phi() - quark->second->phi());
			double dD = ((deta*deta)/(sigma_deta*sigma_deta)+(dphi*dphi)/(sigma_dphi*sigma_dphi)+(dPt*dPt)/(sigma_dpt*sigma_dpt)); 	
			histos1d[("jet_dR_genreco"+id).c_str()]->Fill(dR);
			histos1d[("jet_dPt_genreco"+id).c_str()]->Fill(dPt);
			histos2d[("jet_delPt_vs_dR"+id).c_str()]->Fill(dR,dPt);
			histos2d[("jet_dR_vs_Pt"+id).c_str()]->Fill((*jets)[mindD_jet_ids[quark->first]].pt(),dR);
			histos2d[("jet_delPt_vs_dD"+id).c_str()]->Fill(dR,dD);
			histos1d[("jet_deta_genreco"+id).c_str()]->Fill(deta);
			histos1d[("jet_dphi_genreco"+id).c_str()]->Fill(dphi);
			}
		}

	}
 
	if (reco_gen_match->dR_Wquarks() != -1)
		histos1d[("dR_Wquarks"+id).c_str()]->Fill(reco_gen_match->dR_Wquarks());
	if (reco_gen_match->min_dR_genquarks() != -1)
		histos1d[("min_dR_genquarks"+id).c_str()]->Fill(reco_gen_match->min_dR_genquarks());
	
	
	
	//find md dR for reco jets
	int njets = jets->size();
	double min_dR = -1;
	for(int i=0;i < njets; ++i){
		for(int j=0;j < njets; ++j){
			if(i != j){
				
				double current_dR = deltaR((*jets)[i].p4(),(*jets)[j].p4());
				if (current_dR <= min_dR ||min_dR == -1)
					min_dR = current_dR;
			}
		}
	}
	histos1d[("jet_reco_min_dR"+id).c_str()]->Fill(min_dR);
	
}

void PlotGenerator::analyse_recogen_matchedjets()
{
	if(!reco_gen_match->is_geninfo_available()){
		return;
	}
	

	std::map<std::string,int> matched_jet_ids = reco_gen_match->get_matched_recojets_id(); //min_dD with dR cut
	std::map<std::string,reco::GenParticle*> genquark;

	//truth matching  
	if(gen_match != NULL && gen_match->is_ok() && gen_match->get_decay_mode() == 1){
		
		//All from hadronic top decay
		if(!(gen_match->t_decay_is_leptonic())){	
			genquark["Wquark1"] = gen_match->get_particle("Wplus_dp1");
			genquark["Wquark2"] = gen_match->get_particle("Wplus_dp2");
			genquark["hadb"] = gen_match->get_particle("b");
			genquark["lepb"] = gen_match->get_particle("bbar");
		}
		
		else if(!(gen_match->tbar_decay_is_leptonic())){
			genquark["Wquark1"]= gen_match->get_particle("Wminus_dp1");
			genquark["Wquark2"]= gen_match->get_particle("Wminus_dp2");
			genquark["hadb"]= gen_match->get_particle("bbar");
			genquark["lepb"]= gen_match->get_particle("b");
		  
		}
	}

	//count number of reco jets matched to gen level
	int num_had_jets = 0, num_top_jets = 0;
	double sum_dR = 0;
	for(std::map<std::string,int>::iterator matched_jet = matched_jet_ids.begin();matched_jet!=matched_jet_ids.end();++matched_jet){
		if(matched_jet->second != -1){
			if(matched_jet->first == "lepb"){
				++num_top_jets;
			} 
			else if(matched_jet->first != "lepb"){
				++num_had_jets;
				++num_top_jets;
			}


			double dR = ROOT::Math::VectorUtil::DeltaR((*jets)[matched_jet->second].p4(),genquark[matched_jet->first]->p4());
			sum_dR = sum_dR + dR;

		}
	}


	double mean_dR = sum_dR/num_top_jets; 
	
	histos1d[("top_had_jets_genmatched"+id).c_str()]->Fill(num_had_jets);
	histos1d[("top_jets_genmatched"+id).c_str()]->Fill(num_top_jets);
	histos2d[("jet_mean_dR_vs_numjets"+id).c_str()]->Fill(num_top_jets,mean_dR);

	
	/*
	 *	test hypothesis: 2 jets closest to W mass in event are jets
	 *	that come really from W decay
	 */

	int njets = jets->size();

	if(njets < 2)
		return;

	double min_mass_diff = -1;
	double j1_id = -1;
	double j2_id = -1;
	double W_mass = 80.0;
	for(int i = 0; i < njets; ++i){
		for(int j = i+1; j < njets; ++j){
			if(i != j){
				double curr_diff = ((*jets)[i].p4() + (*jets)[j].p4()).mass() - W_mass;
				if(curr_diff < min_mass_diff || min_mass_diff == -1){
					min_mass_diff = curr_diff;
					j1_id = i;
					j2_id = j;
				}
			}
		}
	}

	if((j1_id == matched_jet_ids["Wquark1"]) && (j2_id == matched_jet_ids["Wquark2"]) ||
	   (j1_id == matched_jet_ids["Wquark2"]) && (j2_id == matched_jet_ids["Wquark1"])){
		// both W jets correctly identified
		histos1d[("W_jets_assignment"+id).c_str()]->Fill(2);
	}else if((j1_id == matched_jet_ids["Wquark1"]) || (j2_id == matched_jet_ids["Wquark1"]) ||
		 (j1_id == matched_jet_ids["Wquark2"]) || (j2_id == matched_jet_ids["Wquark2"])){
		// one jet correctly identified
		histos1d[("W_jets_assignment"+id).c_str()]->Fill(1);
	}else{
		// both W jets wrong
		histos1d[("W_jets_assignment"+id).c_str()]->Fill(0);
	}

	/*
	 *	test hypothesis: highest 4 pt jets in event are from top decay
	 */
	if(matched_jet_ids["Wquark1"] != -1 &&
	   matched_jet_ids["Wquark2"] != -1 &&
	   matched_jet_ids["hadb"] != -1 &&
	   matched_jet_ids["lepb"] != -1){
		histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(matched_jet_ids["Wquark1"]);
		histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(matched_jet_ids["Wquark2"]);
		histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(matched_jet_ids["hadb"]);
		histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(matched_jet_ids["lepb"]);
	}
	
}

void PlotGenerator::plot_minmass_1btag_top()
{
	histos1d[("top_mass_minimisation_1btag"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
}

void PlotGenerator::plot_minmass_2btag_top()
{
	histos1d[("top_mass_minimisation_2btag"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
}


//Plot top mass of truth matched top candidate, used to get sigma for min chi² measurement 
void PlotGenerator::plot_genmass_top()
{
	//TtSemiLeptonicEvent::HypoKey& hypoClassKey = (TtSemiLeptonicEvent::HypoKey&) *hypoClassKeyHandle;
	// new PAT versions
	TtEvent::HypoClassKey& hypoClassKey = (TtEvent::HypoClassKey&) *hypoClassKeyHandle;
	
	if( !semiLepEvt->isHypoAvailable(hypoClassKey) ){
		//std::cerr << "Hypothesis not available for this event" << std::endl;
		return;
	}
	if( !semiLepEvt->isHypoValid(hypoClassKey) ){
		//std::cerr << "Hypothesis not valid for this event" << std::endl;
		return;
	}

	const reco::Candidate* HadTop = semiLepEvt->hadronicDecayTop(hypoClassKey);
	if(!(HadTop == NULL))
		histos1d[("top_hadmass_genmatch"+id).c_str()]->Fill(HadTop->mass()); 
	
	const reco::Candidate* LepTop = semiLepEvt->leptonicDecayTop(hypoClassKey);
	if(!(LepTop == NULL))
		histos1d[("top_lepmass_genmatch"+id).c_str()]->Fill(LepTop->mass()); 
	
	const reco::Candidate* HadW = semiLepEvt->hadronicDecayW(hypoClassKey);
	if(!(HadW == NULL))
		histos1d[("W_hadmass_genmatch"+id).c_str()]->Fill(HadW->mass());
	
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > Htop_candidate;
	const reco::GenParticle* genHadB = genEvt->hadronicDecayB();
	const reco::GenParticle* genHadP = genEvt->hadronicDecayQuark();
	const reco::GenParticle* genHadQ = genEvt->hadronicDecayQuarkBar();
	
	if(genHadB != NULL && genHadP != NULL && genHadQ != NULL){
		Htop_candidate = genHadB->p4()+genHadP->p4()+genHadQ->p4();
		histos1d[("top_hadmass_BQP"+id).c_str()]->Fill(Htop_candidate.mass()); 
	}
	
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > HWtop_candidate;
	const reco::GenParticle* genHadW = genEvt->hadronicDecayW();
	if(genHadB != NULL && genHadW != NULL ){
		HWtop_candidate = genHadB->p4()+genHadW->p4();
		histos1d[("top_hadmass_BW"+id).c_str()]->Fill(HWtop_candidate.mass()); 
	}
		
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > Ltop_candidate;
	const reco::GenParticle* genLepB = genEvt->leptonicDecayB();
	const reco::GenParticle* genLep = genEvt->lepton();
	const reco::GenParticle* genNu = genEvt->neutrino();
	if(genLepB != NULL && genLep != NULL && genNu != NULL){
		Ltop_candidate = genLepB->p4()+genLep->p4()+genNu->p4();
		histos1d[("top_lepmass_BLN"+id).c_str()]->Fill(Ltop_candidate.mass()); 
	}
	
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > W_candidate;
	if(genHadP != NULL && genHadQ != NULL){
		W_candidate = genHadP->p4()+genHadQ->p4(); 
		histos1d[("W_mass_QQ"+id).c_str()]->Fill(W_candidate.mass()); 
	}
	
}

void PlotGenerator::plot_recogenmatch_top()
{
	if(reco_gen_match == NULL || !reco_gen_match->is_geninfo_available())
		return;

	std::map<std::string, int> matching_map = reco_gen_match->get_matched_recojets_id();
	if(matching_map.find("Wquark1") == matching_map.end() ||
	   matching_map.find("Wquark2") == matching_map.end() ||
	   matching_map.find("hadb") == matching_map.end() ||
	   matching_map.find("lepb") == matching_map.end()){
		std::cout << "WARNING: plot_recogenmatch_top(): map not complete" << std::endl;
		return;
	}

	if(matching_map["Wquark1"] == -1 ||
	   matching_map["Wquark2"] == -1 ||
	   matching_map["hadb"] == -1 ||
	   matching_map["lepb"] == -1)
		return;

	if(corrected_mets->begin()->pz() == 0.0){
		return;
	}

	// hadronic gen matched mass
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > W_candidate;
	if(matching_map["Wquark1"] != -1 && matching_map["Wquark2"] != -1 && matching_map["hadb"] != -1){
		W_candidate = (*jets)[matching_map["Wquark1"]].p4() + (*jets)[matching_map["Wquark2"]].p4();
		ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > had_top_candidate;
		had_top_candidate = W_candidate + (*jets)[matching_map["hadb"]].p4();
		histos1d[("top_hadmass_RecoGenMatch"+id).c_str()]->Fill(had_top_candidate.mass());
		histos1d[("W_hadmass_RecoGenMatch"+id).c_str()]->Fill(W_candidate.mass());
	}

	if(matching_map["lepb"] != -1){
		if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
			if(reco_gen_match->lepton_from_top()){
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_top_candidate;
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_W_candidate;
				lep_W_candidate = isolated_muons->begin()->p4()+corrected_mets->begin()->p4();
				if(lep_W_candidate.mass() < 150.0){
					lep_top_candidate = (*jets)[matching_map["lepb"]].p4()+ lep_W_candidate;
					histos1d[("top_lepmass_RecoGenMatch"+id).c_str()]->Fill(lep_top_candidate.mass());
				}
			}
		}
			
		
		if(isolated_muons->size() == 0 && isolated_electrons->size() == 1){
			if(reco_gen_match->lepton_from_top()){				
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_top_candidate;
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_W_candidate;
				lep_W_candidate = isolated_electrons->begin()->p4()+corrected_mets->begin()->p4();
				if(lep_W_candidate.mass() < 150.0){
					lep_top_candidate = (*jets)[matching_map["lepb"]].p4()+ lep_W_candidate;
					histos1d[("top_lepmass_RecoGenMatch"+id).c_str()]->Fill(lep_top_candidate.mass());
				}
			}
		}
		
	}

	if(gen_match == NULL || !gen_match->is_ok())
		return;

	reco::GenParticle *neutrino = NULL;
	if(gen_match->t_decay_is_leptonic()){
		neutrino = gen_match->get_particle("Wplus_dp2");
	}else if(gen_match->tbar_decay_is_leptonic()){
		neutrino = gen_match->get_particle("Wminus_dp2");
	}

	if(ROOT::Math::VectorUtil::DeltaR(corrected_mets->begin()->p4(), neutrino->p4()) > 0.3)
		return;

	if(matching_map["lepb"] != -1){
		if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
			if(reco_gen_match->lepton_from_top()){
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_top_candidate;
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_W_candidate;
				lep_W_candidate = isolated_muons->begin()->p4()+corrected_mets->begin()->p4();

				if(lep_W_candidate.mass() < 150.0){
					lep_top_candidate = (*jets)[matching_map["lepb"]].p4()+ lep_W_candidate;
					histos1d[("top_lepmass_RecoGenMatch_truthmatch"+id).c_str()]->Fill(lep_top_candidate.mass());
				}
			}
		}
			
		if(isolated_muons->size() == 0 && isolated_electrons->size() == 1){
			if(reco_gen_match->lepton_from_top()){				
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_top_candidate;
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_W_candidate;
				lep_W_candidate = isolated_electrons->begin()->p4()+corrected_mets->begin()->p4();
				if(lep_W_candidate.mass() < 150.0){
					lep_top_candidate = (*jets)[matching_map["lepb"]].p4()+ lep_W_candidate;
					histos1d[("top_lepmass_RecoGenMatch_truthmatch"+id).c_str()]->Fill(lep_top_candidate.mass());
				}
			}
		}
		
	}
}

//Plot mass of top got from kinematic fit hypothesis
void PlotGenerator::plot_Kinfit_top()
{

	TtEvent::HypoClassKey& hypoClassKey = (TtEvent::HypoClassKey&) *hypoClassKeyHandle;
//	TtSemiLeptonicEvent::HypoKey& hypoClassKey = (TtSemiLeptonicEvent::HypoKey&) *hypoClassKeyHandle;
	
	if( !semiLepEvt->isHypoAvailable(hypoClassKey) ){
		//std::cerr << "Hypothesis not available for this event" << std::endl;
		return;
	}
	if( !semiLepEvt->isHypoValid(hypoClassKey) ){
		//std::cerr << "Hypothesis not valid for this event" << std::endl;
		return;
	}
	
	
	const reco::Candidate* HadTop = semiLepEvt->hadronicDecayTop(hypoClassKey);
	if(HadTop == NULL)
		return;
	
	histos1d[("top_kinmass"+id).c_str()]->Fill(HadTop->mass());
	
	//Plot also top kinfit vs ht
	if(ht == -1)
		calculate_ht();

	histos2d[("top_kinmass_vs_ht"+id).c_str()]->Fill(ht,HadTop->mass());
 
}

void PlotGenerator::plot_chimass_top_012btag()
{
	if(mass_reco->calculate_chihadTmass_2btag() != -1){
		histos1d[("top_Had_chimass_012_2btag"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
		histos1d[("W_Had_chimass_012_2btag"+id).c_str()]->Fill(mass_reco->calculate_chihadWmass());
		
		histos1d[("top_Lep_chimass_012_2btag"+id).c_str()]->Fill(mass_reco->calculate_chilepTmass());
		histos1d[("W_Lep_chimass_012_2btag"+id).c_str()]->Fill(mass_reco->calculate_chilepWmass());
		histos2d[("top_Had_vs_Lep_chimass_012_2btag"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass(), mass_reco->calculate_chilepTmass());
		
		histos1d[("chi2_012_2btag"+id).c_str()]->Fill(mass_reco->get_chi2()/3.0);
		histos1d[("chi2_012_2btag_prob"+id).c_str()]->Fill(1-TMath::Prob(mass_reco->get_chi2(),3));
	}else if(mass_reco->calculate_chihadTmass_1btag() != -1){
		histos1d[("top_Had_chimass_012_1btag"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
		histos1d[("W_Had_chimass_012_1btag"+id).c_str()]->Fill(mass_reco->calculate_chihadWmass());
		
		histos1d[("top_Lep_chimass_012_1btag"+id).c_str()]->Fill(mass_reco->calculate_chilepTmass());
		histos1d[("W_Lep_chimass_012_1btag"+id).c_str()]->Fill(mass_reco->calculate_chilepWmass());
		histos2d[("top_Had_vs_Lep_chimass_012_1btag"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass(), mass_reco->calculate_chilepTmass());
		
		histos1d[("chi2_012_1btag"+id).c_str()]->Fill(mass_reco->get_chi2()/3.0);
		histos1d[("chi2_012_1btag_prob"+id).c_str()]->Fill(1-TMath::Prob(mass_reco->get_chi2(),3));
	}else if(mass_reco->calculate_chihadTmass() != -1){
		histos1d[("top_Had_chimass_012_0btag"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
		histos1d[("W_Had_chimass_012_0btag"+id).c_str()]->Fill(mass_reco->calculate_chihadWmass());
		
		histos1d[("top_Lep_chimass_012_0btag"+id).c_str()]->Fill(mass_reco->calculate_chilepTmass());
		histos1d[("W_Lep_chimass_012_0btag"+id).c_str()]->Fill(mass_reco->calculate_chilepWmass());
		histos2d[("top_Had_vs_Lep_chimass_012_0btag"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass(), mass_reco->calculate_chilepTmass());
		
		histos1d[("chi2_012_0btag"+id).c_str()]->Fill(mass_reco->get_chi2()/3.0);
		histos1d[("chi2_012_0btag_prob"+id).c_str()]->Fill(1-TMath::Prob(mass_reco->get_chi2(),3));
	}


}

//Find combination of jets which minimise chi² and use these to plot mass
void PlotGenerator::plot_chimass_top()
{
	histos1d[("top_Had_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
	histos1d[("W_Had_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadWmass());
	
	histos1d[("top_Lep_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepTmass());
	histos1d[("W_Lep_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepWmass());
	histos2d[("top_Had_vs_Lep_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass(), mass_reco->calculate_chilepTmass());
	
	histos1d[("chi2_wo_btag"+id).c_str()]->Fill(mass_reco->get_chi2()/3.0);
	histos1d[("chi2_wo_btag_prob"+id).c_str()]->Fill(1-TMath::Prob(mass_reco->get_chi2(),3));
	//mass plots of events which will be cut if btag cut 3 applied
	std::vector<double> set_min_btag (1,3);
	bjet_finder->set_min_btag_value(set_min_btag);		
	std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets("trackCountingHighEffBJetTags");
	if(btag_ids_this_algo->begin() == btag_ids_this_algo->end()){
		histos1d[("top_Had_chimass_max_btag"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
		histos1d[("top_Had_1btag_chimass_max_btag"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top());
	}

	if(jets->size() > 2){
		histos2d[("pt_1st_jet_vs_Lep_chimass"+id).c_str()]->Fill((*jets)[0].pt(), mass_reco->calculate_chilepTmass());
		histos2d[("pt_1st_jet_vs_Had_chimass"+id).c_str()]->Fill((*jets)[0].pt(), mass_reco->calculate_chihadTmass());
		histos2d[("pt_2nd_jet_vs_Lep_chimass"+id).c_str()]->Fill((*jets)[1].pt(), mass_reco->calculate_chilepTmass());
		histos2d[("pt_2nd_jet_vs_Had_chimass"+id).c_str()]->Fill((*jets)[1].pt(), mass_reco->calculate_chihadTmass());
	}

	std::vector<int>* reco_mass_ids = mass_reco->get_ids_chi2();
	if(reco_mass_ids->size() <= 4 ||
	   (*reco_mass_ids)[0] == -1 || 
	   (*reco_mass_ids)[1] == -1 || 
	   (*reco_mass_ids)[2] == -1 || 
	   (*reco_mass_ids)[3] == -1 ||
	   jets->size() < 4)
		return;

	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lept;
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > hadt;

	if(isolated_muons->size() == 1 && isolated_electrons->size() ==0)
		lept = (*jets)[(*reco_mass_ids)[3]].p4() + isolated_muons->begin()->p4() + corrected_mets->begin()->p4();
	else if(isolated_muons->size() == 0 && isolated_electrons->size() ==1)
		lept = (*jets)[(*reco_mass_ids)[3]].p4() + isolated_muons->begin()->p4() + corrected_mets->begin()->p4();
	else
		return;

	hadt = (*jets)[(*reco_mass_ids)[0]].p4() + (*jets)[(*reco_mass_ids)[1]].p4() + (*jets)[(*reco_mass_ids)[2]].p4();

	histos1d[("pt_lept"+id).c_str()]->Fill(lept.pt());
	histos1d[("pt_hadt"+id).c_str()]->Fill(hadt.pt());
	histos2d[("pt_Had_vs_pt_Lep"+id).c_str()]->Fill(hadt.pt(), lept.pt());
}

//Find combination of jets which minimise chi² and use these to plot mass
void PlotGenerator::plot_chimass_1btag_top()
{
	histos1d[("top_Had_1btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top());
	histos1d[("top_Lep_1btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepTmass_1btag());

	histos1d[("W_Had_1btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadWmass_1btag());
	histos1d[("W_Lep_1btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepWmass_1btag());

	histos2d[("top_Had_vs_Lep_chimass_1btag"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top(), mass_reco->calculate_chilepTmass_1btag());

	histos1d[("chi2_1_btag"+id).c_str()]->Fill(mass_reco->get_chi2_1btag()/3.0);
	histos1d[("chi2_1_btag_prob"+id).c_str()]->Fill(1-TMath::Prob(mass_reco->get_chi2_1btag(),3));
}

//Find combination of jets which minimise chi² and use these to plot mass
void PlotGenerator::plot_chimass_2btag_top()
{
	histos1d[("top_Had_2btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chimass_2btag_top());
	histos1d[("top_Lep_2btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepTmass_2btag());

	histos1d[("W_Had_2btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadWmass_2btag());
	histos1d[("W_Lep_2btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepWmass_2btag());

	histos2d[("top_Had_vs_Lep_chimass_2btag"+id).c_str()]->Fill(mass_reco->calculate_chimass_2btag_top(), mass_reco->calculate_chilepTmass_2btag());

	histos1d[("chi2_2_btag"+id).c_str()]->Fill(mass_reco->get_chi2_2btag()/3.0);
	histos1d[("chi2_2_btag_prob"+id).c_str()]->Fill(1-TMath::Prob(mass_reco->get_chi2_2btag(),3));
}

// Plot the invariant mass of the two jets which best corresponds to the nominal value for the W mass, set in plot()
void PlotGenerator::plot_massW(double nominal_massW)
{
	histos1d[("W_mass"+id).c_str()]->Fill(mass_reco->calculate_massW(nominal_massW));
}


// Plot delta R between muon and closest jet
void PlotGenerator::plot_dR()
{
	for(std::vector<pat::Muon>::const_iterator mu_iter = isolated_muons->begin();
	    mu_iter != isolated_muons->end();
	    mu_iter++)
	{
		double min_dR = -1;
		double min_deta = -1;
		double min_dphi = -1;
		for(edm::View<pat::Jet>::const_iterator jet_iter = uncut_jets->begin();
		    jet_iter!=uncut_jets->end();
		    ++jet_iter)
			{
				double dR = fabs(ROOT::Math::VectorUtil::DeltaR(mu_iter->p4(),jet_iter->p4()));
				if ((dR < min_dR) || (min_dR == -1))
					min_dR = dR;
				double deta = fabs(mu_iter->eta()-jet_iter->eta());
				if ((deta < min_deta) || (min_deta == -1))
					min_deta = deta;
				double dphi = fabs(ROOT::Math::VectorUtil::DeltaPhi(mu_iter->p4(),jet_iter->p4()));
				if ((dphi < min_dphi) || (min_dphi == -1))
					min_dphi = dphi;
				
			}
		histos1d[("min_mu_dR"+id).c_str()]->Fill(min_dR);
		histos1d[("min_mu_eta"+id).c_str()]->Fill(min_deta);
		histos1d[("min_mu_phi"+id).c_str()]->Fill(min_dphi);
		return;
	}

	for(std::vector<pat::Electron>::const_iterator e_iter = isolated_electrons->begin();
	    e_iter != isolated_electrons->end();
	    e_iter++)
		{
			double min_dR = -1;
			for(std::vector<pat::Jet>::iterator jet_iter = jets->begin();
			    jet_iter!=jets->end();
			    ++jet_iter)
				{
					double dR = ROOT::Math::VectorUtil::DeltaR(e_iter->p4(),jet_iter->p4());
					if ((dR < min_dR) || (min_dR = -1))
						min_dR = dR;
				}
			histos1d[("min_e_dR"+id).c_str()]->Fill(min_dR);
		}
	
}

double PlotGenerator::deltaR(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
			     ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2)
{
	double delta_phi = ROOT::Math::VectorUtil::DeltaPhi(p4_1,p4_2);
	double delta_eta = p4_1.eta()-p4_2.eta();
	double dR = sqrt(delta_eta * delta_eta + delta_phi * delta_phi);
	return dR;
	
}

//Book all the histograms
void PlotGenerator::book_histos()
{
   	const int top_mass_bins = 150;
   	const int W_mass_bins = 100;
	// file service to write histos automatically to root files
	edm::Service<TFileService> fs;
	/*
	 *	1D Histos
	 */
	 // FIXME: can be removed
	histos1d[("W_jets_assignment"+id).c_str()]=fs->make<TH1D>(("W_jets_assignment"+id).c_str(),"correctly identified W jets closest to W mass",5,-0.5,4.5);

	histos1d[("jet_number"+id).c_str()]=fs->make<TH1D>(("jet_number"+id).c_str(),"jet multiplicity",10,-0.5,9.5);
	histos1d[("muon_number"+id).c_str()]=fs->make<TH1D>(("muon_number"+id).c_str(),"muon multiplicity ",10,-0.5,9.5);
	histos1d[("muon_pt"+id).c_str()]=fs->make<TH1D>(("muon_pt"+id).c_str(),"p_{T} of muons in an event",70,0,350);
	histos1d[("muon_eta"+id).c_str()]=fs->make<TH1D>(("muon_eta"+id).c_str(),"eta of muons in an event",30,-6.0,6.0);
	histos1d[("muon_phi"+id).c_str()]=fs->make<TH1D>(("muon_phi"+id).c_str(),"phi of muons in an event",50,-4.0,4.0);
	histos1d[("muon_d0"+id).c_str()]=fs->make<TH1D>(("muon_d0"+id).c_str(),"d0 of muons in an event",50,-0.4,0.4);
	histos2d[("muon_d0_vs_phi"+id).c_str()]=fs->make<TH2D>(("muon_d0_vs_phi"+id).c_str(),"d0 vs phi of muons in an event",80,-4.0,4.0,80,-0.4,0.4);
	histos2d[("muon_d0_vs_phi_wo_BScorr"+id).c_str()]=fs->make<TH2D>(("muon_d0_vs_phi_wo_BScorr"+id).c_str(),"d0 w/o beamspot correction vs phi of muons in an event",80,-4.0,4.0,80,-0.4,0.4);
	histos1d[("muon_nHits"+id).c_str()]=fs->make<TH1D>(("muon_nHits"+id).c_str(),"number of Hits in the tracker of muons in an event",30,-0.5,29.5);
	histos1d[("muon_chi2"+id).c_str()]=fs->make<TH1D>(("muon_chi2"+id).c_str(),"normalised #chi^{2} of muons in an event",50,0.0,50);
	histos1d[("electron_number"+id).c_str()]=fs->make<TH1D>(("electron_number"+id).c_str(),"electron multiplicity ",10,-0.5,9.5);
	histos1d[("electron_pt"+id).c_str()]=fs->make<TH1D>(("electron_pt"+id).c_str(),"p_{T} of electron in an event",70,0,350);
	histos1d[("electron_eta"+id).c_str()]=fs->make<TH1D>(("electron_eta"+id).c_str(),"eta of electron in an event",30,-6.0,6.0);
	histos1d[("electron_phi"+id).c_str()]=fs->make<TH1D>(("electron_phi"+id).c_str(),"phi of electrons in an event",50,-4.0,4.0);

	histos1d[("Ht"+id).c_str()]=fs->make<TH1D>(("Ht"+id).c_str(),"Ht: Sum pt of all jets and muon and MET",40,0,1500);
	histos1d[("Ht_wo_MET"+id).c_str()]=fs->make<TH1D>(("Ht_wo_MET"+id).c_str(),"Ht: Sum pt of all jets and muon",40,0,1500);
	histos1d[("Event_MET"+id).c_str()]=fs->make<TH1D>(("Event_MET"+id).c_str(),"missing Et in event",70,0,300);
	histos1d[("Event_MET_pt"+id).c_str()]=fs->make<TH1D>(("Event_MET_pt"+id).c_str(),"Neutrino pt in event",70,0,300);
	histos1d[("Event_MET_w_pz"+id).c_str()]=fs->make<TH1D>(("Event_MET_w_pz"+id).c_str(),"missing E in event",70,0,300);
	histos1d[("Event_MET_pt_w_pz"+id).c_str()]=fs->make<TH1D>(("Event_MET_pt_w_pz"+id).c_str(),"Neutrino p in event",70,0,300);

	histos1d[("jet_pt"+id).c_str()]=fs->make<TH1D>(("jet_pt"+id).c_str(),"p_{T} of jets in an event",60,0,350);
	histos1d[("jet_eta"+id).c_str()]=fs->make<TH1D>(("jet_eta"+id).c_str(),"eta of jets in an event",15,-2.5,2.5);
	histos1d[("jet_phi"+id).c_str()]=fs->make<TH1D>(("jet_phi"+id).c_str(),"phi of jets in an event",50,-4.0,4.0);
  	histos1d[("jet1_pt"+id).c_str()]=fs->make<TH1D>(("jet1_pt"+id).c_str(),"p_{T} of leading jet",60,0,350);
	histos1d[("jet1_eta"+id).c_str()]=fs->make<TH1D>(("jet1_eta"+id).c_str(),"eta of leading jet",30,-6.0,6.0);
	histos1d[("jet2_pt"+id).c_str()]=fs->make<TH1D>(("jet2_pt"+id).c_str(),"p_{T} of jet 2 ",60,0,350);
	histos1d[("jet2_eta"+id).c_str()]=fs->make<TH1D>(("jet2_eta"+id).c_str(),"eta of jet 2 ",30,-6.0,6.0);
	histos1d[("jet3_pt"+id).c_str()]=fs->make<TH1D>(("jet3_pt"+id).c_str(),"p_{T} of jet 3",60,0,350);
	histos1d[("jet4_pt"+id).c_str()]=fs->make<TH1D>(("jet4_pt"+id).c_str(),"p_{T} of jet 4",60,0,350);

	histos1d[("jet_pt_id_truth"+id).c_str()]=fs->make<TH1D>(("jet_pt_id_truth"+id).c_str(),"id of truth matched jets in event",10,-0.5,9.5);

	// chi^2 from jet sorting
	histos1d[("chi2_wo_btag"+id).c_str()]=fs->make<TH1D>(("chi2_wo_btag"+id).c_str(),"#chi^{2} from jet sorting w/o b-tagging",125,0,10);
	histos1d[("chi2_1_btag"+id).c_str()]=fs->make<TH1D>(("chi2_1_btag"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",125,0,10);
	histos1d[("chi2_2_btag"+id).c_str()]=fs->make<TH1D>(("chi2_2_btag"+id).c_str(),"#chi^{2} from jet sorting w/ 2 b-tag",125,0,10);
	histos1d[("chi2_wo_btag_prob"+id).c_str()]=fs->make<TH1D>(("chi2_wo_btag_prob"+id).c_str(),"#chi^{2} from jet sorting w/o b-tagging",25,0,1);
	histos1d[("chi2_1_btag_prob"+id).c_str()]=fs->make<TH1D>(("chi2_1_btag_prob"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",25,0,1);
	histos1d[("chi2_2_btag_prob"+id).c_str()]=fs->make<TH1D>(("chi2_2_btag_prob"+id).c_str(),"#chi^{2} from jet sorting w/ 2 b-tag",25,0,1);

	//plotting masses
	histos1d[("top_mass"+id).c_str()]=fs->make<TH1D>(("top_mass"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins,0, 800);

	histos1d[("top_mass_w_METpz"+id).c_str()]=fs->make<TH1D>(("top_mass_w_METpz"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins,0, 800);
	histos1d[("top_kinmass"+id).c_str()]=fs->make<TH1D>(("top_kinmass"+id).c_str(),"Kinematic fit hadronic top mass",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_w_METpz"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_w_METpz"+id).c_str(),"Hadronic top mass, minimal Mjjj-Mjln",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation"+id).c_str(),"Hadronic top mass, minimal Mjjj-Mjln",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_1btag"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_1btag"+id).c_str(),"Hadronic top mass, minimal Mjjj-Mjln,1 btagged jet",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_2btag"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_2btag"+id).c_str(),"Hadronic top mass, minimal Mjjj-Mjln,2 btagged jets",top_mass_bins,0, 800);

	/*
	 * plots for chi mass with forcing 1, 2 or 3 b-tags
	 */
	// w/o btag
	histos1d[("top_Had_chimass_012_0btag"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_012_0btag"+id).c_str(),"Hadronic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("top_Lep_chimass_012_0btag"+id).c_str()]=fs->make<TH1D>(("top_Lep_chimass_012_0btag"+id).c_str(),"Leptonic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("W_Had_chimass_012_0btag"+id).c_str()]=fs->make<TH1D>(("W_Had_chimass_012_0btag"+id).c_str(),"Hadronic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("W_Lep_chimass_012_0btag"+id).c_str()]=fs->make<TH1D>(("W_Lep_chimass_012_0btag"+id).c_str(),"Leptonic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("chi2_012_0btag"+id).c_str()]=fs->make<TH1D>(("chi2_012_0btag"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",125,0,10);
	histos1d[("chi2_012_0btag_prob"+id).c_str()]=fs->make<TH1D>(("chi2_012_0btag_prob"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",25,0,1);
	// 1btag
	histos1d[("top_Had_chimass_012_1btag"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_012_1btag"+id).c_str(),"Hadronic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("top_Lep_chimass_012_1btag"+id).c_str()]=fs->make<TH1D>(("top_Lep_chimass_012_1btag"+id).c_str(),"Leptonic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("W_Had_chimass_012_1btag"+id).c_str()]=fs->make<TH1D>(("W_Had_chimass_012_1btag"+id).c_str(),"Hadronic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("W_Lep_chimass_012_1btag"+id).c_str()]=fs->make<TH1D>(("W_Lep_chimass_012_1btag"+id).c_str(),"Leptonic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("chi2_012_1btag"+id).c_str()]=fs->make<TH1D>(("chi2_012_1btag"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",125,0,10);
	histos1d[("chi2_012_1btag_prob"+id).c_str()]=fs->make<TH1D>(("chi2_012_1btag_prob"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",25,0,1);

	// 2btag
	histos1d[("top_Had_chimass_012_2btag"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_012_2btag"+id).c_str(),"Hadronic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("top_Lep_chimass_012_2btag"+id).c_str()]=fs->make<TH1D>(("top_Lep_chimass_012_2btag"+id).c_str(),"Leptonic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("W_Had_chimass_012_2btag"+id).c_str()]=fs->make<TH1D>(("W_Had_chimass_012_2btag"+id).c_str(),"Hadronic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("W_Lep_chimass_012_2btag"+id).c_str()]=fs->make<TH1D>(("W_Lep_chimass_012_2btag"+id).c_str(),"Leptonic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("chi2_012_2btag"+id).c_str()]=fs->make<TH1D>(("chi2_012_2btag"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",125,0,10);
	histos1d[("chi2_012_2btag_prob"+id).c_str()]=fs->make<TH1D>(("chi2_012_2btag_prob"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",25,0,1);

	// top mass w/o b-tagging
	histos1d[("top_Had_chimass_max_btag"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_max_btag"+id).c_str(),"Hadronic top mass using chi squared sorting: events cut by btag > 3",top_mass_bins,0, 800);
	histos1d[("top_Had_1btag_chimass_max_btag"+id).c_str()]=fs->make<TH1D>(("top_Had_1btag_chimass_max_btag"+id).c_str(),"Hadronic top mass, chi squared sorting 1 jet btagged: events cut by btag > 3",top_mass_bins,0, 800);
	histos1d[("top_Had_chimass"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass"+id).c_str(),"Hadronic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("top_Lep_chimass"+id).c_str()]=fs->make<TH1D>(("top_Lep_chimass"+id).c_str(),"Leptonic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("top_Had_chimass_w_METpz"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_w_METpz"+id).c_str(),"Hadronic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("top_Lep_chimass_w_METpz"+id).c_str()]=fs->make<TH1D>(("top_Lep_chimass_w_METpz"+id).c_str(),"Leptonic top mass using chi squared sorting",top_mass_bins,0, 800);
	histos1d[("top_Had_1btag_chimass"+id).c_str()]=fs->make<TH1D>(("top_Had_1btag_chimass"+id).c_str(),"Hadronic top mass, chi squared sorting 1 jet btagged",top_mass_bins,0, 800);
	histos1d[("top_Lep_1btag_chimass"+id).c_str()]=fs->make<TH1D>(("top_Lep_1btag_chimass"+id).c_str(),"Leptonic top mass, chi squared sorting 1 jet btagged",top_mass_bins,0, 800);
	histos1d[("top_Had_2btag_chimass"+id).c_str()]=fs->make<TH1D>(("top_Had_2btag_chimass"+id).c_str(),"Hadronic top mass, chi squared sorting, 2 jets btagged",top_mass_bins,0, 800);
	histos1d[("top_Lep_2btag_chimass"+id).c_str()]=fs->make<TH1D>(("top_Lep_2btag_chimass"+id).c_str(),"Leptonic top mass, chi squared sorting, 2 jets btagged",top_mass_bins,0, 800);
	histos1d[("W_mass"+id).c_str()]=fs->make<TH1D>(("W_mass"+id).c_str(),"Invariant mass of 2 jets with vectorially summed pt closest to nominal W mass",W_mass_bins,0, 300);
	histos1d[("W_Had_chimass"+id).c_str()]=fs->make<TH1D>(("W_Had_chimass"+id).c_str(),"Hadronic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("W_Lep_chimass"+id).c_str()]=fs->make<TH1D>(("W_Lep_chimass"+id).c_str(),"Leptonic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("W_Had_chimass_w_METpz"+id).c_str()]=fs->make<TH1D>(("W_Had_chimass_w_METpz"+id).c_str(),"Hadronic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("W_Lep_chimass_w_METpz"+id).c_str()]=fs->make<TH1D>(("W_Lep_chimass_w_METpz"+id).c_str(),"Leptonic W mass using chi squared method",W_mass_bins,0, 300);
	histos1d[("W_Had_1btag_chimass"+id).c_str()]=fs->make<TH1D>(("W_Had_1btag_chimass"+id).c_str(),"Hadronic W mass, chi squared method 1 jet btagged",W_mass_bins,0, 300);
	histos1d[("W_Lep_1btag_chimass"+id).c_str()]=fs->make<TH1D>(("W_Lep_1btag_chimass"+id).c_str(),"Leptonic W mass, chi squared method 1 jet btagged",W_mass_bins,0, 300);
	histos1d[("W_Had_2btag_chimass"+id).c_str()]=fs->make<TH1D>(("W_Had_2btag_chimass"+id).c_str(),"Hadronic W mass, chi squared method 2 jets btagged",W_mass_bins,0, 300);
	histos1d[("W_Lep_2btag_chimass"+id).c_str()]=fs->make<TH1D>(("W_Lep_2btag_chimass"+id).c_str(),"Leptonic W mass, chi squared method 2 jets btagged",W_mass_bins,0, 300);
	//FIXME: doesnt work, may not even be useful if did.checking chimass parameters 
	histos1d[("W_Lep_chipt_w_METpz"+id).c_str()]=fs->make<TH1D>(("W_Lep_chipt_w_METpz"+id).c_str(),"Lep_chipt_w_METpz",W_mass_bins,0,1500);
	histos1d[("W_Lep_chip_w_METpz"+id).c_str()]=fs->make<TH1D>(("W_Lep_chip_w_METpz"+id).c_str(),"Lep_chip_w_METpz",W_mass_bins,0,1500);
	histos1d[("W_Lep_chiet_w_METpz"+id).c_str()]=fs->make<TH1D>(("W_Lep_chiet_w_METpz"+id).c_str(),"Lep_chiet_w_METpz",W_mass_bins,0,1500);
	histos1d[("W_Lep_chie_w_METpz"+id).c_str()]=fs->make<TH1D>(("W_Lep_chie_w_METpz"+id).c_str(),"Lep_chie_w_METpz",W_mass_bins,0,1500);
	histos1d[("top_Lep_chipt_w_METpz"+id).c_str()]=fs->make<TH1D>(("top_Lep_chipt_w_METpz"+id).c_str(),"Lep_chipt_w_METpz",100,0,1500);
	histos1d[("top_Lep_chip_w_METpz"+id).c_str()]=fs->make<TH1D>(("top_Lep_chip_w_METpz"+id).c_str(),"Lep_chip_w_METpz",100,0,1500);
	histos1d[("top_Lep_chiet_w_METpz"+id).c_str()]=fs->make<TH1D>(("top_Lep_chiet_w_METpz"+id).c_str(),"Lep_chiet_w_METpz",100,0,1500);
	histos1d[("top_Lep_chie_w_METpz"+id).c_str()]=fs->make<TH1D>(("top_Lep_chie_w_METpz"+id).c_str(),"Lep_chie_w_METpz",100,0,1500);
	//truth matching jets
	histos1d[("top_had_jets_genmatched"+id).c_str()]=fs->make<TH1D>(("top_had_jets_genmatched"+id).c_str(),"Jets passing event which are really from semi-lep hadronic decay channel",10,-0.5, 9.5);
	histos1d[("top_jets_genmatched"+id).c_str()]=fs->make<TH1D>(("top_jets_genmatched"+id).c_str(),"Jets passing event which are really from semi-lep decay channel",10,-0.5, 9.5);
	histos1d[("top_mass_3jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_3jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_2jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_2jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_1jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_1jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_0jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_0jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_lostjetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_lostjetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, 800);

	histos1d[("top_mass_minimisation_3jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_3jetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_2jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_2jetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_1jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_1jetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_0jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_0jetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_lostjetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_lostjetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, 800);

	histos1d[("top_Had_chimass_3jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_3jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_chimass_2jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_2jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_chimass_1jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_1jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_chimass_0jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_0jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_chimass_lostjetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_chimass_lostjetmatched"+id).c_str(),"chimass lostjetmatched",top_mass_bins,0, 800);

	histos1d[("top_Had_1btag_chimass_3jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_1btag_chimass_3jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_1btag_chimass_2jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_1btag_chimass_2jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_1btag_chimass_1jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_1btag_chimass_1jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_1btag_chimass_0jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_1btag_chimass_0jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_1btag_chimass_lostjetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_1btag_chimass_lostjetmatched"+id).c_str(),"chimass lostjetmatched",top_mass_bins,0, 800);

	histos1d[("top_Had_2btag_chimass_3jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_2btag_chimass_3jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_2btag_chimass_2jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_2btag_chimass_2jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_2btag_chimass_1jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_2btag_chimass_1jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_2btag_chimass_0jetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_2btag_chimass_0jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_Had_2btag_chimass_lostjetmatched"+id).c_str()]=fs->make<TH1D>(("top_Had_2btag_chimass_lostjetmatched"+id).c_str(),"chimass lostjetmatched",top_mass_bins,0, 800);

	histos1d[("top_mass_genmatch_3jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_genmatch_3jetmatched"+id).c_str(),"mass 3 jets jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_genmatch_2jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_genmatch_2jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_genmatch_1jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_genmatch_1jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_genmatch_0jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_genmatch_0jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, 800);
	histos1d[("top_mass_genmatch"+id).c_str()]=fs->make<TH1D>(("top_mass_genmatch"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, 800);

	histos1d[("top_mass_minimisation_1btag_3jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_1btag_3jetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_1btag_2jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_1btag_2jetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_1btag_1jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_1btag_1jetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_1btag_0jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_1btag_0jetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_1btag_lostjetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_1btag_lostjetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, 800);

	histos1d[("top_mass_minimisation_2btag_3jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_2btag_3jetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_2btag_2jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_2btag_2jetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_2btag_1jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_2btag_1jetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_2btag_0jetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_2btag_0jetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, 800);
	histos1d[("top_mass_minimisation_2btag_lostjetmatched"+id).c_str()]=fs->make<TH1D>(("top_mass_minimisation_2btag_lostjetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, 800);

	//Figuring out whatdR cut should be between gen quark and reco jet, and sigmas for dD equation
	histos2d[("jet_mean_dR_vs_numjets"+id).c_str()]=fs->make<TH2D>(("jet_mean_dR_vs_numjets"+id).c_str(),"mean dR vs jets found",6, -0.5, 5.5,100,0,5);
	histos1d[("jet_reco_min_dR"+id).c_str()]=fs->make<TH1D>(("jet_reco_min_dR"+id).c_str(),"deltaR between closest reco jets",50,0,7);
	histos2d[("jet_delPt_vs_dR"+id).c_str()]=fs->make<TH2D>(("jet_delPt_vs_dR"+id).c_str(),"jet delPt vs dR",210, 0, 7,400, -2,2);
	histos2d[("jet_dR_vs_Pt"+id).c_str()]=fs->make<TH2D>(("jet_dR_vs_Pt"+id).c_str(),"jet reco Pt vs gen-reco dR",600,0, 300, 210, 0,7);
	histos2d[("jet_delPt_vs_dD"+id).c_str()]=fs->make<TH2D>(("jet_delPt_vs_dD"+id).c_str(),"jet delPt vs dD",210, 0, 7,400, -2,2);
	histos1d[("jet_dR_genreco"+id).c_str()]=fs->make<TH1D>(("jet_dR_genreco"+id).c_str(),"jet_dR_genreco",50,0,7);
	histos1d[("jet_dPt_genreco"+id).c_str()]=fs->make<TH1D>(("jet_dPt_genreco"+id).c_str(),"jet_dPt_genreco dPt = dpt/pt_gen",100,-2,2);
	histos1d[("jet_deta_genreco"+id).c_str()]=fs->make<TH1D>(("jet_deta_genreco"+id).c_str(),"jet_deta_genreco ",200,-1,1);
	histos1d[("jet_dphi_genreco"+id).c_str()]=fs->make<TH1D>(("jet_dphi_genreco"+id).c_str(),"jet_dphi_genreco ",200,-1,1);
	histos1d[("dR_Wquarks"+id).c_str()]=fs->make<TH1D>(("dR_Wquarks"+id).c_str(),"deltaR between W quarks",140,0,7);
	histos1d[("min_dR_genquarks"+id).c_str()]=fs->make<TH1D>(("min_dR_genquarks"+id).c_str(),"deltaR between closest two quarks",140,0,7);


	histos1d[("muon_MtW"+id).c_str()]=fs->make<TH1D>(("muon_MtW"+id).c_str(),"Leptonic Transverse Mass with muon",30,0, 200);
	histos1d[("electron_MtW"+id).c_str()]=fs->make<TH1D>(("electron_MtW"+id).c_str(),"Leptonic Transverse Mass with electron",30,0, 200);
	// 0 = all events, 1 = events passing cuts
	histos1d[("event_counter"+id).c_str()]=fs->make<TH1D>(("event_counter"+id).c_str(),"processed events",5,-0.5,4.5);

	histos1d[("min_mu_dR"+id).c_str()]=fs->make<TH1D>(("min_mu_dR"+id).c_str(),"deltaR between muon(s) and closest jet",50,0,7);
	histos1d[("min_mu_eta"+id).c_str()]=fs->make<TH1D>(("min_mu_eta"+id).c_str(),"deltaR between muon(s) and closest jet",50,-8,8);
	histos1d[("min_mu_phi"+id).c_str()]=fs->make<TH1D>(("min_mu_phi"+id).c_str(),"deltaR between muon(s) and closest jet",50,-8,8);

	histos1d[("min_e_dR"+id).c_str()]=fs->make<TH1D>(("min_e_dR"+id).c_str(),"deltaR for electron(s) and closest jet",50,0,7);
	histos1d[("jet_e_dR"+id).c_str()]=fs->make<TH1D>(("jet_e_dR"+id).c_str(),"deltaR for uncut electrons and uncut jet",50,0,7);

	//truth matched mass histograms used to get sigma for chi² calculation
	// TQAF truth matching
	histos1d[("top_hadmass_genmatch"+id).c_str()]=fs->make<TH1D>(("top_hadmass_genmatch"+id).c_str(),"TQAF Gen matched hadronic top mass",25,0, 800);
	histos1d[("top_lepmass_genmatch"+id).c_str()]=fs->make<TH1D>(("top_lepmass_genmatch"+id).c_str(),"TQAF Gen matched leptonic top mass",25,0, 800);
	histos1d[("top_hadmass_BQP"+id).c_str()]=fs->make<TH1D>(("top_hadmass_BQP"+id).c_str(),"TQAF Gen matched hadronic top mass",25,0, 800);
	histos1d[("top_hadmass_BW"+id).c_str()]=fs->make<TH1D>(("top_hadmass_BW"+id).c_str(),"TQAF Gen matched hadronic top mass",25,0, 800);
	histos1d[("top_lepmass_BLN"+id).c_str()]=fs->make<TH1D>(("top_lepmass_BLN"+id).c_str(),"TQAF Gen matched leptonic top mass",25,0, 800);
	histos1d[("W_hadmass_genmatch"+id).c_str()]=fs->make<TH1D>(("W_hadmass_genmatch"+id).c_str(),"TQAF Gen matched hadronic W mass",W_mass_bins,0, 300);
	histos1d[("W_mass_QQ"+id).c_str()]=fs->make<TH1D>(("W_mass_QQ"+id).c_str(),"TQAF Gen level reconstructed W mass",W_mass_bins,0, 300);

	//truth matched mass histograms used to get sigma for chi² calculation
	// private truth matching
	histos1d[("top_hadmass_RecoGenMatch"+id).c_str()]=fs->make<TH1D>(("top_hadmass_RecoGenMatch"+id).c_str(),"private gen matched hadronic top mass",100,0, 800);
	histos1d[("top_lepmass_RecoGenMatch"+id).c_str()]=fs->make<TH1D>(("top_lepmass_RecoGenMatch"+id).c_str(),"private gen matched leptonic top mass",100,0, 800);
	histos1d[("top_lepmass_RecoGenMatch_truthmatch"+id).c_str()]=fs->make<TH1D>(("top_lepmass_RecoGenMatch_truthmatch"+id).c_str(),"private gen matched leptonic top mass",100,0, 800);
	histos1d[("W_hadmass_RecoGenMatch"+id).c_str()]=fs->make<TH1D>(("W_hadmass_RecoGenMatch"+id).c_str(),"private gen matched hadronic W mass",W_mass_bins,0, 300);

	// Wb mass
	histos1d[("top_hadmass_Wb_0btag"+id).c_str()]=fs->make<TH1D>(("top_hadmass_Wb_0btag"+id).c_str(),"Wb hadronic top mass",top_mass_bins,0, 800);
	histos1d[("top_lepmass_Wb_0btag"+id).c_str()]=fs->make<TH1D>(("top_lepmass_Wb_0btag"+id).c_str(),"Wb leptonic top mass",top_mass_bins,0, 800);
	histos1d[("W_hadmass_Wb_0btag"+id).c_str()]=fs->make<TH1D>(("W_hadmass_Wb_0btag"+id).c_str(),"Wb hadronic W mass",W_mass_bins,0, 300);

	histos1d[("top_hadmass_Wb_1btag"+id).c_str()]=fs->make<TH1D>(("top_hadmass_Wb_1btag"+id).c_str(),"Wb hadronic top mass",top_mass_bins,0, 800);
	histos1d[("top_lepmass_Wb_1btag"+id).c_str()]=fs->make<TH1D>(("top_lepmass_Wb_1btag"+id).c_str(),"Wb leptonic top mass",top_mass_bins,0, 800);
	histos1d[("W_hadmass_Wb_1btag"+id).c_str()]=fs->make<TH1D>(("W_hadmass_Wb_1btag"+id).c_str(),"Wb hadronic W mass",W_mass_bins,0, 300);

	histos1d[("top_hadmass_Wb_2btag"+id).c_str()]=fs->make<TH1D>(("top_hadmass_Wb_2btag"+id).c_str(),"Wb hadronic top mass",top_mass_bins,0, 800);
	histos1d[("top_lepmass_Wb_2btag"+id).c_str()]=fs->make<TH1D>(("top_lepmass_Wb_2btag"+id).c_str(),"Wb leptonic top mass",top_mass_bins,0, 800);
	histos1d[("W_hadmass_Wb_2btag"+id).c_str()]=fs->make<TH1D>(("W_hadmass_Wb_2btag"+id).c_str(),"Wb hadronic W mass",W_mass_bins,0, 300);
	histos1d[("Wb_dR_W_1stb_2btag"+id).c_str()]=fs->make<TH1D>(("Wb_dR_W_1stb"+id).c_str(),"deltaR between hadronic W and highest b-jet",50,0,7);
	histos1d[("Wb_dR_W_2ndb_2btag"+id).c_str()]=fs->make<TH1D>(("Wb_dR_W_2ndb"+id).c_str(),"deltaR between hadronic W and second b-jet",50,0,7);

	/*
	 *=========================================================================================
	 */

	/*
	 *	2D Histos
	 */

	// mass reconstruction plots
	// switching between 0, 1 or 2 btags as suggested by Martin
	histos2d[("top_Had_vs_Lep_chimass_012_0btag"+id).c_str()]=fs->make<TH2D>(("top_Had_vs_Lep_chimass_012_0btag"+id).c_str(),"correlation between hadronic and leptonic chimass",top_mass_bins, 0, 800, top_mass_bins,0,800);
	histos2d[("top_Had_vs_Lep_chimass_012_1btag"+id).c_str()]=fs->make<TH2D>(("top_Had_vs_Lep_chimass_012_1btag"+id).c_str(),"correlation between hadronic and leptonic chimass",top_mass_bins, 0, 800, top_mass_bins,0,800);
	histos2d[("top_Had_vs_Lep_chimass_012_2btag"+id).c_str()]=fs->make<TH2D>(("top_Had_vs_Lep_chimass_012_2btag"+id).c_str(),"correlation between hadronic and leptonic chimass",top_mass_bins, 0, 800, top_mass_bins,0,800);

	// pt of hadronic and leptonic top
	histos1d[("pt_lept"+id).c_str()]=fs->make<TH1D>(("pt_lept"+id).c_str(),"pt leptonic top",(int)top_mass_bins/2, 0, 400);
	histos1d[("pt_hadt"+id).c_str()]=fs->make<TH1D>(("pt_hadt"+id).c_str(),"pt hadronic top",(int)top_mass_bins/2, 0, 400);
	histos2d[("pt_Had_vs_pt_Lep"+id).c_str()]=fs->make<TH2D>(("pt_Had_vs_pt_Lep"+id).c_str(),"pt had t vs pt lep t",(int)top_mass_bins/2, 0, 400, (int) top_mass_bins/2,0,800);


	// pt first jet vs mass
	histos2d[("pt_1st_jet_vs_Lep_chimass"+id).c_str()]=fs->make<TH2D>(("pt_1st_jet_vs_Lep_chimass"+id).c_str(),"1st jet pt vs had chimass",(int)top_mass_bins/2, 0, 400, top_mass_bins,0,800);
	histos2d[("pt_1st_jet_vs_Had_chimass"+id).c_str()]=fs->make<TH2D>(("pt_1st_jet_vs_Had_chimass"+id).c_str(),"1st jet pt vs had chimass",(int)top_mass_bins/2, 0, 400, top_mass_bins,0,800);
	histos2d[("pt_2nd_jet_vs_Lep_chimass"+id).c_str()]=fs->make<TH2D>(("pt_2nd_jet_vs_Lep_chimass"+id).c_str(),"2nd jet pt vs had chimass",(int)top_mass_bins/2, 0, 400, top_mass_bins,0,800);
	histos2d[("pt_2nd_jet_vs_Had_chimass"+id).c_str()]=fs->make<TH2D>(("pt_2nd_jet_vs_Had_chimass"+id).c_str(),"2nd jet pt vs had chimass",(int)top_mass_bins/2, 0, 400, top_mass_bins,0,800);

	histos2d[("top_Had_vs_Lep_chimass"+id).c_str()]=fs->make<TH2D>(("top_Had_vs_Lep_chimass"+id).c_str(),"correlation between hadronic and leptonic chimass",top_mass_bins, 0, 800, top_mass_bins,0,800);
	histos2d[("top_Had_vs_Lep_chimass_1btag"+id).c_str()]=fs->make<TH2D>(("top_Had_vs_Lep_chimass_1btag"+id).c_str(),"correlation between hadronic and leptonic chimass",top_mass_bins, 0, 800, top_mass_bins,0,800);
	histos2d[("top_Had_vs_Lep_chimass_2btag"+id).c_str()]=fs->make<TH2D>(("top_Had_vs_Lep_chimass_2btag"+id).c_str(),"correlation between hadronic and leptonic chimass",top_mass_bins, 0, 800, top_mass_bins,0,800);

	// ABCD method plots
	histos2d[("isolation_vs_met_muon"+id).c_str()]=fs->make<TH2D>(("isolation_vs_met_muon"+id).c_str(),"lepton isolation vs MET",100, 0, 150, 50,0,50);
	histos2d[("isolation_vs_met_electron"+id).c_str()]=fs->make<TH2D>(("isolation_vs_met_electron"+id).c_str(),"lepton isolation vs MET",100, 0, 150, 50,0,50);
	histos2d[("top_mass_vs_ht"+id).c_str()]=fs->make<TH2D>(("top_mass_vs_ht"+id).c_str(),"M3 mass vs HT",50, 0, 900, 50,0,800);
	histos2d[("top_kinmass_vs_ht"+id).c_str()]=fs->make<TH2D>(("top_kinmass_vs_ht"+id).c_str(),"kinematic fit top mass vs HT",50, 0, 900, 50,0,800);
	histos2d[("top_mass_minimisation_vs_ht"+id).c_str()]=fs->make<TH2D>(("top_mass_minimisation_vs_ht"+id).c_str(),"M3 mass (with min Mjjj-Mjln) vs HT",50, 0, 900, 50,0,800);
	histos2d[("jet_pt_vs_et"+id).c_str()]=fs->make<TH2D>(("jet_pt_vs_et"+id).c_str(),"pt vs et for all jets",60, 0, 350, 60,0,350);
	histos2d[("jet1_pt_vs_et"+id).c_str()]=fs->make<TH2D>(("jet1_pt_vs_et"+id).c_str(),"pt vs et for leading jet",60, 0, 350, 60,0,350);
	histos2d[("jet2_pt_vs_et"+id).c_str()]=fs->make<TH2D>(("jet2_pt_vs_et"+id).c_str(),"pt vs et for jet 2",60, 0, 350, 60,0,350);

	//isolation plots
	histos1d[("match_mu_trackiso"+id).c_str()]=fs->make<TH1D>(("match_mu_trackiso"+id).c_str(),"matched Muon Track Iso",60,0,50);
	histos1d[("match_mu_hcaliso"+id).c_str()]=fs->make<TH1D>(("match_mu_hcaliso"+id).c_str(),"matched Muon Hcal Iso",60,0,50);
	histos1d[("match_mu_ecaliso"+id).c_str()]=fs->make<TH1D>(("match_mu_ecaliso"+id).c_str(),"matched Muon Ecal Iso",60,0,50);
	histos1d[("match_mu_caloiso"+id).c_str()]=fs->make<TH1D>(("match_mu_caloiso"+id).c_str(),"matched Muon CalIso",60,0,50);
	histos1d[("match_mu_sumiso"+id).c_str()]=fs->make<TH1D>(("match_mu_sumiso"+id).c_str(),"matched Muon Ecal+Hcal+trackIso",70,0,150);
	histos1d[("match_mu_CombRelIso"+id).c_str()]=fs->make<TH1D>(("match_mu_CombRelIso"+id).c_str(),"matched Muon CombRelIso",75,0,1.5);
	histos1d[("match_e_trackiso"+id).c_str()]=fs->make<TH1D>(("match_e_trackiso"+id).c_str(),"matched Electron Track Iso",60,0,50);
	histos1d[("match_e_hcaliso"+id).c_str()]=fs->make<TH1D>(("match_e_hcaliso"+id).c_str(),"matched Electron Hcal Iso",60,0,50);
	histos1d[("match_e_ecaliso"+id).c_str()]=fs->make<TH1D>(("match_e_ecaliso"+id).c_str(),"matched Electron Ecal Iso",60,0,50);
	histos1d[("match_e_caloiso"+id).c_str()]=fs->make<TH1D>(("match_e_caloiso"+id).c_str(),"matched Electron CalIso",60,0,50);
	histos1d[("match_e_sumiso"+id).c_str()]=fs->make<TH1D>(("match_e_sumiso"+id).c_str(),"matched Electron SumIso",70,0,150);
	histos1d[("match_e_CombRelIso"+id).c_str()]=fs->make<TH1D>(("match_e_CombRelIso"+id).c_str(),"matched Electron CombRelIso",75,0,1.5);
	histos1d[("unmatch_mu_trackiso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_trackiso"+id).c_str(),"unmatched Muon Track Iso",40,0,50);
	histos1d[("unmatch_mu_hcaliso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_hcaliso"+id).c_str(),"unmatched Muon Hcal Iso",40,0,50);
	histos1d[("unmatch_mu_ecaliso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_ecaliso"+id).c_str(),"unmatched Muon Ecal Iso",40,0,50);
	histos1d[("unmatch_mu_caloiso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_caloiso"+id).c_str(),"unmatched Muon CalIso",40,0,50);
	histos1d[("unmatch_mu_sumiso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_sumiso"+id).c_str(),"unmatched Muon Ecal+Hcal+trackIso",70,0,150);
	histos1d[("unmatch_mu_CombRelIso"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_CombRelIso"+id).c_str(),"unmatched Muon CombRelIso",75,0,1.5);
	histos1d[("unmatch_e_trackiso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_trackiso"+id).c_str(),"unmatched Electron Track Iso",40,0,50);
	histos1d[("unmatch_e_hcaliso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_hcaliso"+id).c_str(),"unmatched Electron Hcal Iso",40,0,50);
	histos1d[("unmatch_e_ecaliso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_ecaliso"+id).c_str(),"unmatched Electron Ecal Iso",40,0,50);
	histos1d[("unmatch_e_caloiso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_caloiso"+id).c_str(),"unmatched Electron CalIso",40,0,50);
	histos1d[("unmatch_e_sumiso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_sumiso"+id).c_str(),"unmatched Electron SumIso",70,0,150);
	histos1d[("unmatch_e_CombRelIso"+id).c_str()]=fs->make<TH1D>(("unmatch_e_CombRelIso"+id).c_str(),"unmatched Electron CombRelIso",75,0,1.5);
	//vetocone plots
	histos1d[("match_mu_veto_hcalEnergy"+id).c_str()]=fs->make<TH1D>(("match_mu_veto_hcalEnergy"+id).c_str(),"matched muon vetocone(size 0.1) hcalEnergy",70,0,100);
	histos1d[("match_mu_veto_ecalEnergy"+id).c_str()]=fs->make<TH1D>(("match_mu_veto_ecalEnergy"+id).c_str(),"matched muon vetocone(size 0.07) ecalEnergy",70,0,100);
	histos1d[("match_e_veto_ecalEnergy"+id).c_str()]=fs->make<TH1D>(("match_e_veto_ecalEnergy"+id).c_str(),"matched electron vetocone(size 0.07) ecalEnergy",70,0,100);
	histos1d[("match_e_veto_hcalEnergy"+id).c_str()]=fs->make<TH1D>(("match_e_veto_hcalEnergy"+id).c_str(),"matched electron vetocone(size(0.1) hcalEnergy",70,0,100);
	histos1d[("unmatch_mu_veto_hcalEnergy"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_veto_hcalEnergy"+id).c_str(),"unmatched muon vetocone(size 0.1) hcalEnergy",70,0,100);
	histos1d[("unmatch_mu_veto_ecalEnergy"+id).c_str()]=fs->make<TH1D>(("unmatch_mu_veto_ecalEnergy"+id).c_str(),"unmatched muon vetocone(size 0.07) ecalEnergy",70,0,100);
	histos1d[("unmatch_e_veto_ecalEnergy"+id).c_str()]=fs->make<TH1D>(("unmatch_e_veto_ecalEnergy"+id).c_str(),"unmatched electron vetocone(size 0.07) ecalEnergy",70,0,100);
	histos1d[("unmatch_e_veto_hcalEnergy"+id).c_str()]=fs->make<TH1D>(("unmatch_e_veto_hcalEnergy"+id).c_str(),"unmatched electron vetocone(size(0.1) hcalEnergy",70,0,100);

	//trigger plots
// 	histos1d[("trig_e15_eff_vs_pt"+id).c_str()]=fs->make<TH1D>(("trig_e15_eff_vs_pt"+id).c_str(),"trigger efficiency vs pt HLT_Ele15_LW_L1R   ",40,0,200);
// 	histos1d[("untrig_e15_pt"+id).c_str()]=fs->make<TH1D>(("untrig_e15_pt"+id).c_str(),"acc/gen pt HLT_Ele15_LW_L1R ",40,0,200);
// 	histos1d[("trig_mu11_eff_vs_pt"+id).c_str()]=fs->make<TH1D>(("trig_mu11_eff_vs_pt"+id).c_str(),"trigger efficiency vs pt, HLT_Mu11",40,0,200);
// 	histos1d[("untrig_mu11_pt"+id).c_str()]=fs->make<TH1D>(("untrig_mu11_pt"+id).c_str(),"acc/gen pt, HLT_Mu11 ",40,0,200);
	
// 	histos1d[("trig_e15_eff_vs_eta"+id).c_str()]=fs->make<TH1D>(("trig_e15_eff_vs_eta"+id).c_str(),"trigger efficiency vs eta HLT_Ele15_LW_L1R   ",25,-2.5,2.5);
// 	histos1d[("untrig_e15_eta"+id).c_str()]=fs->make<TH1D>(("untrig_e15_eta"+id).c_str(),"acc/gen eta HLT_Ele15_LW_L1R (used to calculate eff vs eta) ",25,-2.5,2.5);
// 	histos1d[("trig_mu11_eff_vs_eta"+id).c_str()]=fs->make<TH1D>(("trig_mu11_eff_vs_eta"+id).c_str(),"trigger efficiency vs eta, HLT_Mu11",25,-2.5,2.5);
// 	histos1d[("untrig_mu11_eta"+id).c_str()]=fs->make<TH1D>(("untrig_mu11_eta"+id).c_str(),"acc/gen eta, HLT_Mu11 non iso (used to calculate eff vs eta) ",25,-2.5,2.5);

// 	histos1d[("trig_e15_eff_vs_pt_gen"+id).c_str()]=fs->make<TH1D>(("trig_e15_eff_vs_pt_gen"+id).c_str(),"trigger efficiency vs pt_gen HLT_Ele15_LW_L1R   ",40,0,200);
// 	histos1d[("untrig_e15_pt_gen"+id).c_str()]=fs->make<TH1D>(("untrig_e15_pt_gen"+id).c_str(),"acc/gen ntrigger pt_gen HLT_Ele15_LW_L1R ",40,0,200);
// 	histos1d[("trig_mu11_eff_vs_pt_gen"+id).c_str()]=fs->make<TH1D>(("trig_mu11_eff_vs_pt_gen"+id).c_str(),"trigger efficiency vs pt_gen, HLT_Mu11",40,0,200);
// 	histos1d[("untrig_mu11_pt_gen"+id).c_str()]=fs->make<TH1D>(("untrig_mu11_pt_gen"+id).c_str(),"acc/gen pt_gen, HLT_Mu11 ",40,0,200);
	
// 	histos1d[("trig_e15_eff_vs_eta_gen"+id).c_str()]=fs->make<TH1D>(("trig_e15_eff_vs_eta_gen"+id).c_str(),"trigger efficiency vs eta_gen HLT_Ele15_LW_L1R   ",25,-2.5,2.5);
// 	histos1d[("untrig_e15_eta_gen"+id).c_str()]=fs->make<TH1D>(("untrig_e15_eta_gen"+id).c_str(),"acc/gen eta_gen HLT_Ele15_LW_L1R ",25,-2.5,2.5);
// 	histos1d[("trig_mu11_eff_vs_eta_gen"+id).c_str()]=fs->make<TH1D>(("trig_mu11_eff_vs_eta_gen"+id).c_str(),"trigger efficiency vs eta_gen, HLT_Mu11",25,-2.5,2.5);
// 	histos1d[("untrig_mu11_eta_gen"+id).c_str()]=fs->make<TH1D>(("untrig_mu11_eta_gen"+id).c_str(),"N_acc/gen eta_gen, HLT_Mu11 ",25,-2.5,2.5);

// 	histos1d[("trig_mu11_pt_gen"+id).c_str()]=fs->make<TH1D>(("trig_mu11_pt_gen"+id).c_str(),"trig pt_gen, HLT_Mu11 ",40,0,200);
// 	histos1d[("trig_e15_pt_gen"+id).c_str()]=fs->make<TH1D>(("trig_e15_pt_gen"+id).c_str(),"trig pt_gen HLT_Ele15_LW_L1R ",40,0,200);
// 	histos1d[("trig_mu11_eta_gen"+id).c_str()]=fs->make<TH1D>(("trig_mu11_eta_gen"+id).c_str(),"trig eta_gen, HLT_Mu11 ",25,-2.5,2.5);
// 	histos1d[("trig_e15_eta_gen"+id).c_str()]=fs->make<TH1D>(("trig_e15_eta_gen"+id).c_str(),"trig eta_gen HLT_Ele15_LW_L1R ",25,-2.5,2.5);

// 	histos1d[("trig_mu11_pt"+id).c_str()]=fs->make<TH1D>(("trig_mu11_pt"+id).c_str(),"trig pt, HLT_Mu11 ",40,0,200);
// 	histos1d[("trig_e15_pt"+id).c_str()]=fs->make<TH1D>(("trig_e15_pt"+id).c_str(),"trig pt HLT_Ele15_LW_L1R ",40,0,200);
// 	histos1d[("trig_mu11_eta"+id).c_str()]=fs->make<TH1D>(("trig_mu11_eta"+id).c_str(),"trig eta, HLT_Mu11 ",25,-2.5,2.5);
// 	histos1d[("trig_e15_eta"+id).c_str()]=fs->make<TH1D>(("trig_e15_eta"+id).c_str(),"trig eta HLT_Ele15_LW_L1R ",25,-2.5,2.5);

	// plots for comparing bid and recogen information, ie for comparing MC matching in recogenmatch with another method
	histos1d[("bjets_in_event_MC1"+id).c_str()]=fs->make<TH1D>(("bjets_in_event_MC1"+id).c_str(),"bjets in event method 1",5,-0.5,3.5);
	histos1d[("jets_in_event_MC1"+id).c_str()]=fs->make<TH1D>(("jets_in_event_MC1"+id).c_str(),"jets in event method 1",8,-0.5,6.5);
	histos1d[("bjets_in_event_MC2"+id).c_str()]=fs->make<TH1D>(("bjets_in_event_MC2"+id).c_str(),"bjets found by matching method 2",4,-0.5,3.5);
	histos1d[("jets_in_event_MC2"+id).c_str()]=fs->make<TH1D>(("jets_in_event_MC2"+id).c_str(),"jets found by matching method 2",7,-0.5,6.5);
	histos1d[("bjets_in_event_MCboth"+id).c_str()]=fs->make<TH1D>(("bjets_in_event_MCboth"+id).c_str(),"bjets found where 2 methods agree",4,-0.5,3.5);
	histos1d[("jets_in_event_MCboth"+id).c_str()]=fs->make<TH1D>(("jets_in_event_MCboth"+id).c_str(),"jets found where 2 methods agree",7,-0.5,6.5);

	histos1d[("bjets_MCmethods_compare"+id).c_str()]=fs->make<TH1D>(("bjets_MCmethods_compare"+id).c_str(),"bjets:2=MCagree,1=MC1b,-1=MC2b,-2diffb's,-3MC1bMC2W,-4MC2bMC1W ",7,-4.5,2.5);
	histos1d[("jets_MCmethods_compare"+id).c_str()]=fs->make<TH1D>(("jets_MCmethods_compare"+id).c_str(),"jets:2=MC agree,1=MC1only,-1=MC2only,-2=diffpartons",5,-2.5,2.5);
	histos2d[("dPt_vs_dR_bjet_MCmethods_agree"+id).c_str()]=fs->make<TH2D>(("dPt_vs_dR_bjet_MCmethods_agree"+id).c_str(),"bjets: dPt vs dR both MC's match",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str()]=fs->make<TH2D>(("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str(),"bjets: dPt vs dR MC1 only matched",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str()]=fs->make<TH2D>(("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str(),"bjets: dPt vs dR MC2 only matched",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_jet_MCmethods_agree"+id).c_str()]=fs->make<TH2D>(("dPt_vs_dR_jet_MCmethods_agree"+id).c_str(),"dPt vs dR both MC's match",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_jet_MC1_disagrees"+id).c_str()]=fs->make<TH2D>(("dPt_vs_dR_jet_MC1_disagrees"+id).c_str(),"dPt vs dR MC1 only matched",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_jet_MC2_disagrees"+id).c_str()]=fs->make<TH2D>(("dPt_vs_dR_jet_MC2_disagrees"+id).c_str(),"dPt vs dR MC2 only matched",210, 0, 7,800, -4,4);
	histos2d[("jet_MCnumber_when_disagree"+id).c_str()]=fs->make<TH2D>(("jet_MCnumber_when_disagree"+id).c_str(),"1=hadb,2=lepb,3=Wq1,4=Wq2",160, 0.5, 4.5,160, 0.5,4.5);

	//Btag...
	histos1d[("bjet_pt_order"+id).c_str()]=fs->make<TH1D>(("bjet_pt_order"+id).c_str(),"MC matched bjet has #th highest pt in event",11,-0.5,10.5);
	histos1d[("bjets_in_event"+id).c_str()]=fs->make<TH1D>(("bjets_in_event"+id).c_str(),"How many bjets from top decay in event",4,-0.5,3.5);
	histos1d[("bjets_in_event_allbjets"+id).c_str()]=fs->make<TH1D>(("bjets_in_event_allbjets"+id).c_str(),"How many bjets from any decay in event",6,-0.5,5.5);
	
	//book btag plots for more than one algorithm if necessary
	std::vector<std::pair<std::string,std::string> > btag_algos;
	btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighEffBJetTags","_HighEff"));
	btag_algos.push_back(std::pair<std::string,std::string>("jetProbabilityBJetTags","_JetProb"));
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighPurBJetTags","_HighPur"));
	btag_algos.push_back(std::pair<std::string,std::string>("softMuonNoIPBJetTags","_SoftMuNoIP"));
	btag_algos.push_back(std::pair<std::string,std::string>("softMuonBJetTags","_SoftMu"));
	btag_algos.push_back(std::pair<std::string,std::string>("simpleSecondaryVertexBJetTags","_SSVertex"));
	btag_algos.push_back(std::pair<std::string,std::string>("impactParameterMVABJetTags","_IPMVA"));
	btag_algos.push_back(std::pair<std::string,std::string>("combinedSecondaryVertexMVABJetTags","_CSVertexMVA"));
	btag_algos.push_back(std::pair<std::string,std::string>("combinedSecondaryVertexBJetTags","_CSVertex"));
	btag_algos.push_back(std::pair<std::string,std::string>("softElectronBJetTags","_SoftEle"));
	//IF adding an algorithm don´t forget to set nbins,minX and maxX below.

	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){
		std::pair<std::string,std::string> btag = (*btag_algo);
	
		//bin ranges when plotting btag value different for each algorithm
		int nbins = 35;
		double min_x = 0,max_x = 35;	
		if(btag.first == "trackCountingHighEffBJetTags")        {nbins = 35; min_x = 0;max_x = 35; }
		if(btag.first == "jetBProbabilityBJetTags")             {nbins = 100; min_x = 0;max_x = 10; }
		if(btag.first == "jetProbabilityBJetTags")              {nbins = 30; min_x = 0;max_x = 3; }
		if(btag.first == "trackCountingHighPurBJetTags")        {nbins = 35; min_x = 0;max_x = 35; }
		if(btag.first == "softMuonNoIPBJetTags")                {nbins = 40; min_x = -1;max_x = 1; }
		if(btag.first == "softMuonBJetTags")                    {nbins = 40; min_x = -1;max_x = 1; }
		if(btag.first == "simpleSecondaryVertexBJetTags")       {nbins = 40; min_x = -2;max_x = 8; }
		if(btag.first == "impactParameterMVABJetTags")          {nbins = 40; min_x = 0;max_x = 1; }
		if(btag.first == "combinedSecondaryVertexMVABJetTags")  {nbins = 40; min_x = 0;max_x = 1; }
		if(btag.first == "combinedSecondaryVertexBJetTags")     {nbins = 40; min_x = 0;max_x = 1; }
		if(btag.first == "softElectronBJetTags")                {nbins = 40; min_x = -1;max_x = 1; }
		

		//Do full analysis for these two algorithms, only short analysis for the others
		if(btag.first == "trackCountingHighEffBJetTags" || btag.first == "jetBProbabilityBJetTags"){
			
			//btag plots using genParton id (identifies all bjets not jsut those from semileptonic decay
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_sorted_werent_found_bid"+btag.second+id).c_str(),(btag.second+": -1=2nd found,0=1st found, 1=1st not found,2=2nd not found").c_str(),4,-1.5,2.5);
			
			histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_highest_is_real_bid"+btag.second+id).c_str(),(btag.second+":1 matched b, 0 unmatched b").c_str(),2,-0.5,1.5);
			histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_second_highest_is_real_bid"+btag.second+id).c_str(),(btag.second+":1 matched b id, 0 unmatched b id").c_str(),2,-0.5,1.5);
			//pt
			histos1d[("bjet_highest_pt_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_highest_pt_bid"+btag.second+id).c_str(),(btag.second+":pt for most likely bjet matched id").c_str(),52,0,260);	
			histos1d[("nonbjet_highest_pt_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_highest_pt_bid"+btag.second+id).c_str(),(btag.second+":pt for most likely bjet unmatched id").c_str(),52,0,260);	
			histos1d[("bjet_second_highest_pt_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_second_highest_pt_bid"+btag.second+id).c_str(),(btag.second+":pt for second most likely bjet matched id").c_str(),52,0,260);
			histos1d[("nonbjet_second_highest_pt_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_second_highest_pt_bid"+btag.second+id).c_str(),(btag.second+":pt for second most likely bjet unmatched id").c_str(),52,0,260);
			//eta
			histos1d[("bjet_highest_eta_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_highest_eta_bid"+btag.second+id).c_str(),(btag.second+":eta for most likely bjet matched id").c_str(),60,-3,3);	
			histos1d[("nonbjet_highest_eta_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_highest_eta_bid"+btag.second+id).c_str(),(btag.second+":eta for most likely bjet unmatched id").c_str(),60,-3,3);	
			histos1d[("bjet_second_highest_eta_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_second_highest_eta_bid"+btag.second+id).c_str(),(btag.second+":eta for second most likely bjet matched id").c_str(),60,-3,3);
			histos1d[("nonbjet_second_highest_eta_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_second_highest_eta_bid"+btag.second+id).c_str(),(btag.second+":eta for second most likely bjet unmatched id").c_str(),60,-3,3);
			//btag
			histos1d[("bjet_highest_bDiscrim_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_highest_bDiscrim_bid"+btag.second+id).c_str(),(btag.second+":btag value for most likely bjet matched id").c_str(),nbins,min_x,max_x);
			histos1d[("nonbjet_highest_bDiscrim_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_highest_bDiscrim_bid"+btag.second+id).c_str(),(btag.second+":btag value for most likely bjet unmatched id").c_str(),nbins,min_x,max_x);	
			histos1d[("bjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_second_highest_bDiscrim_bid"+btag.second+id).c_str(),(btag.second+":btag value for second most likely bjet matched id").c_str(),nbins,min_x,max_x);
			histos1d[("nonbjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_second_highest_bDiscrim_bid"+btag.second+id).c_str(),(btag.second+":btag value for second most likely bjet unmatched id").c_str(),nbins,min_x,max_x);
			
			//btag plots using reco gen match 
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_highest_is_real"+btag.second+id).c_str(),(btag.second+":1 matched b, 0 unmatched b").c_str(),2,-0.5,1.5);
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_second_highest_is_real"+btag.second+id).c_str(),(btag.second+":1 matched b, 0 unmatched b").c_str(),2,-0.5,1.5);
			histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_two_highest_are_real"+btag.second+id).c_str(),(btag.second+":3 both matched , 2 2nd only, 1 1st only, 0 none").c_str(),4,-0.5,3.5);
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_sorted_werent_found"+btag.second+id).c_str(),(btag.second+": -1=2nd found,0=1st found, 1=1st not found,2=2nd not found").c_str(),4,-1.5,2.5);
			histos1d[("bjets_highest_identify"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_highest_identify"+btag.second+id).c_str(),(btag.second+":1 hadronic b, 0 leptonic b").c_str(),2,-0.5,1.5);
			histos1d[("bjets_second_highest_identify"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_second_highest_identify"+btag.second+id).c_str(),(btag.second+":1 hadronic b, 0 leptonic b").c_str(),2,-0.5,1.5);
			//pt
			histos1d[("bjet_highest_pt"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_highest_pt"+btag.second+id).c_str(),(btag.second+":pt for most likely bjet matched").c_str(),52,0,260);	
			histos1d[("nonbjet_highest_pt"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_highest_pt"+btag.second+id).c_str(),(btag.second+":pt for most likely bjet unmatched").c_str(),52,0,260);	
			histos1d[("bjet_second_highest_pt"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_second_highest_pt"+btag.second+id).c_str(),(btag.second+":pt for second most likely bjet matched").c_str(),52,0,260);
			histos1d[("nonbjet_second_highest_pt"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_second_highest_pt"+btag.second+id).c_str(),(btag.second+":pt for second most likely bjet unmatched").c_str(),52,0,260);	
			//eta
			histos1d[("bjet_highest_eta"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_highest_eta"+btag.second+id).c_str(),(btag.second+":eta for most likely bjet matched").c_str(),60,-3,3);	
			histos1d[("nonbjet_highest_eta"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_highest_eta"+btag.second+id).c_str(),(btag.second+":eta for most likely bjet unmatched").c_str(),60,-3,3);	
			histos1d[("bjet_second_highest_eta"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_second_highest_eta"+btag.second+id).c_str(),(btag.second+":eta for second most likely bjet matched").c_str(),60,-3,3);
			histos1d[("nonbjet_second_highest_eta"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_second_highest_eta"+btag.second+id).c_str(),(btag.second+":eta for second most likely bjet unmatched").c_str(),60,-3,3);
			//dR
			histos1d[("dR_highest_bjet_Wjj"+btag.second+id).c_str()]=fs->make<TH1D>(("dR_highest_bjet_Wjj"+btag.second+id).c_str(),(btag.second+":dR between highest btagged jet and genmatched Wjj direction").c_str(),50,0,7);
			histos1d[("dR_second_highest_bjet_Wjj"+btag.second+id).c_str()]=fs->make<TH1D>(("dR_second_highest_bjet_Wjj"+btag.second+id).c_str(),(btag.second+":dR between second highest btagged jet and genmatched Wjj direction").c_str(),50,0,7);
			histos1d[("dR_highest_bjet_muon"+btag.second+id).c_str()]=fs->make<TH1D>(("dR_highest_bjet_muon"+btag.second+id).c_str(),(btag.second+":dR between highest btagged jet and muon direction").c_str(),50,0,7);
			histos1d[("dR_second_highest_bjet_muon"+btag.second+id).c_str()]=fs->make<TH1D>(("dR_second_highest_bjet_muon"+btag.second+id).c_str(),(btag.second+":dR between second highest btagged jet and muon direction").c_str(),50,0,7);
			//btag
			histos1d[("bjet_bDiscrim"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for b jets matched").c_str(),nbins,min_x,max_x);
			histos1d[("nonbjet_bDiscrim"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for non b jets unmatched").c_str(),nbins,min_x,max_x);
			histos1d[("bjet_highest_bDiscrim"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_highest_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for most likely bjet matched").c_str(),nbins,min_x,max_x);	
			histos1d[("nonbjet_highest_bDiscrim"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_highest_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for most likely bjet unmatched").c_str(),nbins,min_x,max_x);	
			histos1d[("bjet_second_highest_bDiscrim"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_second_highest_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for second most likely bjet matched").c_str(),nbins,min_x,max_x);
			histos1d[("nonbjet_second_highest_bDiscrim"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_second_highest_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for second most likely bjet unmatched").c_str(),nbins,min_x,max_x);
			histos1d[("bjet_bDiscrim_hadb"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_bDiscrim_hadb"+btag.second+id).c_str(),(btag.second+":btag value for hadronic bjets").c_str(),nbins,min_x,max_x);
			histos1d[("bjet_bDiscrim_lepb"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_bDiscrim_lepb"+btag.second+id).c_str(),(btag.second+":btag value for leptonic bjets").c_str(),nbins,min_x,max_x);
			
			//for plot_bDiscrim_find_btagcut
			histos1d[("bjet_efficiency_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_efficiency_cut"+btag.second+id).c_str(),(btag.second+": 1= tagged bjet, 0=untagged bjet").c_str(),2,-0.5,1.5);
			histos1d[("bjet_passed_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_passed_cut"+btag.second+id).c_str(),(btag.second+": 1= tagged bjet, 0=mistag").c_str(),2,-0.5,1.5);
			histos1d[("bjet_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for b jets").c_str(),nbins,min_x,max_x);
			histos1d[("cjet_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("cjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for c jets").c_str(),nbins,min_x,max_x);
			histos1d[("lightjet_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("lightjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for light jets").c_str(),nbins,min_x,max_x);
			histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for non b jets").c_str(),nbins,min_x,max_x);
			histos1d[("bjet_pt_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_pt_cut"+btag.second+id).c_str(),(btag.second+":pt of b jets").c_str(),52,0,260);
			histos1d[("nonbjet_pt_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_pt_cut"+btag.second+id).c_str(),(btag.second+":pt of non b jets").c_str(),52,0,260);
			histos1d[("bjet_eta_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_eta_cut"+btag.second+id).c_str(),(btag.second+":eta of b jets").c_str(),50,-2.5,2.5);
			histos1d[("nonbjet_eta_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_eta_cut"+btag.second+id).c_str(),(btag.second+":eta of non b jets").c_str(),50,-2.5,2.5);
			histos2d[("bjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH2D>(("bjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":eta of b jets vs bDiscrim").c_str(),nbins,min_x,max_x,50,-2.5,2.5);
			histos2d[("nonbjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH2D>(("nonbjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":eta of non b jets vs bDiscrim").c_str(),nbins,min_x,max_x,50,-2.5,2.5);
			histos2d[("bjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH2D>(("bjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":pt of b jets vs bDiscrim").c_str(),nbins,min_x,max_x,52,0,260);
			histos2d[("nonbjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH2D>(("nonbjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":pt of non b jets vs bDiscrim").c_str(),nbins,min_x,max_x,52,0,260);
			histos1d[("bjet_event_passed_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_event_passed_cut"+btag.second+id).c_str(),(btag.second+":how often does event pass btag cut").c_str(),2,-0.5,1.5);
			
			histos1d[("jets_btagged_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("jets_btagged_cut"+btag.second+id).c_str(),(btag.second+":number of jets btagged in this event").c_str(),11,-0.5,10.5);
			histos1d[("jet_highest_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("jet_highest_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":highest bDiscrim jet in event").c_str(),nbins,min_x,max_x);
			histos1d[("jet_second_highest_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("jet_second_highest_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":second highest bDiscrim jet in event").c_str(),nbins,min_x,max_x);
			histos1d[("jet_highest_pt_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("jet_highest_pt_cut"+btag.second+id).c_str(),(btag.second+":pt of highest bDiscrim jet").c_str(),52,0,260);
			histos1d[("jet_second_highest_pt_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("jet_second_highest_pt_cut"+btag.second+id).c_str(),(btag.second+":pt of second highest bDiscrim jet").c_str(),52,0,260);
			histos1d[("jet_highest_eta_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("jet_highest_eta_cut"+btag.second+id).c_str(),(btag.second+":eta of highest bDiscrim  jet").c_str(),50,-2.5,2.5);
			histos1d[("jet_second_highest_eta_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("jet_second_highest_eta_cut"+btag.second+id).c_str(),(btag.second+":eta of second highest bDiscrim  jet").c_str(),50,-2.5,2.5);
			histos2d[("jet_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH2D>(("jet_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":highest bDiscrim jet pt vs bDiscrim").c_str(),nbins,min_x,max_x,52,0,260);
			histos2d[("jet_second_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH2D>(("jet_second_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":second highest bDiscrim jet pt vs bDiscrim").c_str(),nbins,min_x,max_x,52,0,260);
			
			// plots to test hypothesis if only considering first 4 b-jets would make sense
			histos2d[("1st_b_pt_vs_4th_jet_pt"+btag.second+id).c_str()]=fs->make<TH2D>(("1st_b_pt_vs_4th_jet_pt"+btag.second+id).c_str(),"pt 1st btagged jet vs pt 4th pt jet",150, 0, 500, 150,0,500);
			histos2d[("2nd_b_pt_vs_4th_jet_pt"+btag.second+id).c_str()]=fs->make<TH2D>(("2nd_b_pt_vs_4th_jet_pt"+btag.second+id).c_str(),"pt 2nd btagged jet vs pt 4th pt jet",150, 0, 500, 150,0,500);
		}
		//less detailed comparison of btag algorithms (efficiency in chosing real bjet with highest btag)
		else{
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_highest_is_real"+btag.second+id).c_str(),"1 matched b, 0 unmatched b",2,-0.5,1.5);
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]=fs->make<TH1D>(("bjets_second_highest_is_real"+btag.second+id).c_str(),"1 matched b, 0 unmatched b",2,-0.5,1.5);
			histos1d[("bjet_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("bjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for b jets").c_str(),nbins,min_x,max_x);
			histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]=fs->make<TH1D>(("nonbjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for non b jets").c_str(),nbins,min_x,max_x);
		}

	}
}

void PlotGenerator::apply_cuts(Cuts *mycuts)
{
	cuts = mycuts;
	set_lepton_selector_cuts();
	set_btag_cuts(cuts->get_min_btag(),cuts->get_name_btag());		
	mass_reco = cuts->get_mass_reco();
	mass_reco->set_tprime_mass(tprime_mass);
	sync_ex->set_mass_reconstruction(mass_reco);
	mass_jet_match->set_mass_reco(mass_reco);
	//mass_reco_corMET->set_tprime_mass(tprime_mass);
}

void PlotGenerator::deactivate_cuts()
{
	cuts = NULL;
	unset_lepton_selector_cuts();
}
												        												    
void PlotGenerator::set_tprime_mass(double mass)
{
	tprime_mass = mass;
}
 void PlotGenerator::set_btag_cuts(std::vector<double> *min,double n)
{
	min_btag= min;

	if (n == 1) name_btag = "jetBProbabilityBJetTags";
	else if (n == 2) name_btag = "jetProbabilityBJetTags";
	else if (n == 3) name_btag = "trackCountingHighPurBJetTags";
	else if (n == 4) name_btag = "trackCountingHighEffBJetTags";
	else name_btag = "-1";
	
}
//plot isolation and vetocone(vetocone to replace dR cut) for semilep and non semilep events
void PlotGenerator::matchleptons_plotiso_plotveto()
{
	/*
  if(!(genEvt->isTtBar() && genEvt->isSemiLeptonic()))
    { 
	    */
  
	for(std::vector<pat::Muon>::iterator muon = isolated_muons->begin();
	    muon != isolated_muons->end();
	    ++muon){

	  histos1d[("unmatch_mu_hcaliso"+id).c_str()]->Fill(muon->hcalIso());
	  histos1d[("unmatch_mu_ecaliso"+id).c_str()]->Fill(muon->ecalIso());
	  histos1d[("unmatch_mu_trackiso"+id).c_str()]->Fill(muon->trackIso());
	  histos1d[("unmatch_mu_caloiso"+id).c_str()]->Fill(muon->caloIso());
	  double unmatch_mu_sumIso = (muon->trackIso()+muon->hcalIso()+muon->ecalIso());
	  double unmatch_mu_CombRelIso = (unmatch_mu_sumIso)/(muon->pt());
	  histos1d[("unmatch_mu_CombRelIso"+id).c_str()]->Fill(unmatch_mu_CombRelIso);
	  histos1d[("unmatch_mu_sumiso"+id).c_str()]->Fill(unmatch_mu_sumIso);
	  histos1d[("unmatch_mu_veto_hcalEnergy"+id).c_str()]->Fill(muon->hcalIsoDeposit()->candEnergy());
	  histos1d[("unmatch_mu_veto_ecalEnergy"+id).c_str()]->Fill(muon->ecalIsoDeposit()->candEnergy()); 
	}
         
	for(std::vector<pat::Electron>::iterator electron = isolated_electrons->begin();electron != isolated_electrons->end();++electron){

	  histos1d[("unmatch_e_hcaliso"+id).c_str()]->Fill(electron->hcalIso());
	  histos1d[("unmatch_e_ecaliso"+id).c_str()]->Fill(electron->ecalIso());
	  histos1d[("unmatch_e_trackiso"+id).c_str()]->Fill(electron->trackIso());
	  histos1d[("unmatch_e_caloiso"+id).c_str()]->Fill(electron->caloIso());
	  double unmatch_e_sumIso = (electron->trackIso()+electron->hcalIso()+electron->ecalIso());
	  double unmatch_e_CombRelIso = (unmatch_e_sumIso)/(electron->pt());
	  histos1d[("unmatch_e_CombRelIso"+id).c_str()]->Fill(unmatch_e_CombRelIso);
	  histos1d[("unmatch_e_sumiso"+id).c_str()]->Fill(unmatch_e_sumIso);
	  histos1d[("unmatch_e_veto_hcalEnergy"+id).c_str()]->Fill(electron->hcalIsoDeposit()->candEnergy());
	  histos1d[("unmatch_e_veto_ecalEnergy"+id).c_str()]->Fill(electron->ecalIsoDeposit()->candEnergy());
	  
	}
      
	    
      return; 
      
  //  }	  
	
/*
 TtSemiLeptonicEvent::HypoKey& hypoClassKey = (TtSemiLeptonicEvent::HypoKey&) *hypoClassKeyHandle;

 if( !semiLepEvt->isHypoAvailable(hypoClassKey) ){
   //std::cerr << "Hypothesis not available for this event" << std::endl;
   return;
 }
 if( !semiLepEvt->isHypoValid(hypoClassKey) ){
   //std::cerr << "Hypothesis not valid for this event" << std::endl;
   return;
 }
	const reco::Candidate* Lep = semiLepEvt->lepton(hypoClassKey);
	if(Lep == NULL)
	  return;
	//fill matched amd unmatched semileptonic muon
	if ((genEvt->lepton() != NULL && genEvt->lepton()->pdgId() == 13) ||
	    (genEvt->leptonBar() != NULL && genEvt->leptonBar()->pdgId()
	     == -13)){
	  // For each candidate loop over all pat leptons and check if pt and eta match
	  for(std::vector<pat::Muon>::iterator muon = isolated_muons->begin();
	      muon != isolated_muons->end();
	      ++muon){
	    
	    //    double dR = ROOT::Math::VectorUtil::DeltaR(muon->p4(),genEvt->singleLepton()->p4());
	    if(muon->pt() == Lep->pt() && muon->eta() == Lep->eta()){
	    // if(dR < 0.3){
	      histos1d[("match_mu_hcaliso"+id).c_str()]->Fill(muon->hcalIso());
	      histos1d[("match_mu_ecaliso"+id).c_str()]->Fill(muon->ecalIso());
	      histos1d[("match_mu_trackiso"+id).c_str()]->Fill(muon->trackIso());
	      histos1d[("match_mu_caloiso"+id).c_str()]->Fill(muon->caloIso());	
	      double match_mu_sumIso = (muon->trackIso()+muon->hcalIso()+muon->ecalIso());
	      double match_mu_CombRelIso = (match_mu_sumIso)/(muon->pt());
	      histos1d[("match_mu_CombRelIso"+id).c_str()]->Fill(match_mu_CombRelIso);
	      histos1d[("match_mu_sumiso"+id).c_str()]->Fill(match_mu_sumIso);
	      histos1d[("match_mu_veto_hcalEnergy"+id).c_str()]->Fill(muon->hcalIsoDeposit()->candEnergy());
	      histos1d[("match_mu_veto_ecalEnergy"+id).c_str()]->Fill(muon->ecalIsoDeposit()->candEnergy()); 
	      
	    }
	    else{
	      histos1d[("unmatch_mu_hcaliso"+id).c_str()]->Fill(muon->hcalIso());
	      histos1d[("unmatch_mu_ecaliso"+id).c_str()]->Fill(muon->ecalIso());
	      histos1d[("unmatch_mu_trackiso"+id).c_str()]->Fill(muon->trackIso());
	      histos1d[("unmatch_mu_caloiso"+id).c_str()]->Fill(muon->caloIso());
	      double unmatch_mu_sumIso = (muon->trackIso()+muon->hcalIso()+muon->ecalIso());
	      double unmatch_mu_CombRelIso = (unmatch_mu_sumIso)/(muon->pt());
	      histos1d[("unmatch_mu_CombRelIso"+id).c_str()]->Fill(unmatch_mu_CombRelIso);
	      histos1d[("unmatch_mu_sumiso"+id).c_str()]->Fill(unmatch_mu_sumIso);
	      histos1d[("unmatch_mu_veto_hcalEnergy"+id).c_str()]->Fill(muon->hcalIsoDeposit()->candEnergy());
	      histos1d[("unmatch_mu_veto_ecalEnergy"+id).c_str()]->Fill(muon->ecalIsoDeposit()->candEnergy()); 
	    }
	  }
	}
	// FIXME: are electrons not filled but muons are working fine
	//fill matched amd unmatched semileptonic electron
	else if ((genEvt->lepton() != NULL && genEvt->lepton()->pdgId() == 11)
		 || (genEvt->leptonBar() != NULL && genEvt->leptonBar()->pdgId() == -11)){
	   for(std::vector<pat::Electron>::iterator electron = isolated_electrons->begin();
	       electron != isolated_electrons->end();
	       ++electron){
	     
	        double dR = ROOT::Math::VectorUtil::DeltaR(electron->p4(),genEvt->singleLepton()->p4());
	    if(dR < 0.2){
	      //   if(electron->pt() == Lep->pt() && electron->eta() == Lep->eta()){
	       
	       histos1d[("match_e_hcaliso"+id).c_str()]->Fill(electron->hcalIso());
	       histos1d[("match_e_ecaliso"+id).c_str()]->Fill(electron->ecalIso());
	       histos1d[("match_e_trackiso"+id).c_str()]->Fill(electron->trackIso());
	       histos1d[("match_e_caloiso"+id).c_str()]->Fill(electron->caloIso());
	       double match_e_sumIso = (electron->trackIso()+electron->hcalIso()+electron->ecalIso());
	       double match_e_CombRelIso = (match_e_sumIso)/(electron->pt());
	       histos1d[("match_e_CombRelIso"+id).c_str()]->Fill(match_e_CombRelIso);
	       histos1d[("match_e_sumiso"+id).c_str()]->Fill(match_e_sumIso);
	       histos1d[("match_e_veto_hcalEnergy"+id).c_str()]->Fill(electron->hcalIsoDeposit()->candEnergy());
	       histos1d[("match_e_veto_ecalEnergy"+id).c_str()]->Fill(electron->ecalIsoDeposit()->candEnergy());
	     }
	     else{
	       histos1d[("unmatch_e_hcaliso"+id).c_str()]->Fill(electron->hcalIso());
	       histos1d[("unmatch_e_ecaliso"+id).c_str()]->Fill(electron->ecalIso());
	       histos1d[("unmatch_e_trackiso"+id).c_str()]->Fill(electron->trackIso());
	       histos1d[("unmatch_e_caloiso"+id).c_str()]->Fill(electron->caloIso());
	       double unmatch_e_sumIso = (electron->trackIso()+electron->hcalIso()+electron->ecalIso());
	       double unmatch_e_CombRelIso = (unmatch_e_sumIso)/(electron->pt());
	       histos1d[("unmatch_e_CombRelIso"+id).c_str()]->Fill(unmatch_e_CombRelIso);
	       histos1d[("unmatch_e_sumiso"+id).c_str()]->Fill(unmatch_e_sumIso);
	       histos1d[("unmatch_e_veto_hcalEnergy"+id).c_str()]->Fill(electron->hcalIsoDeposit()->candEnergy());
	       histos1d[("unmatch_e_veto_ecalEnergy"+id).c_str()]->Fill(electron->ecalIsoDeposit()->candEnergy());
	     }
	   }
	}
	*/
	 
}
/***********************************************Btagging*********************************************************/
//Plot value returned by bDiscrim for real b jets and non bjets, for two jets with highest btag value in event (using genParton to see if real bjet from any decay)
void PlotGenerator::plot_bDiscriminator_allbjets()
{
	int njets = jets->size();
	if (njets < 2)
		return;
	if(reco_gen_match == NULL)
		return;
	
	std::vector<std::pair<std::string,std::string> > btag_algos;
	btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighEffBJetTags","_HighEff"));

	//now get back the ids for all bjets in event (genParton =5)
	std::vector<int> matched_bjet_ids = reco_gen_match->get_matched_bjets_id();

	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){
		std::pair<std::string,std::string> btag = (*btag_algo);
		
		int  max_b_id = -1,max2_b_id = -1;

		//get back highest and second highest btagged ids for this btagging algorithm. from BJetFinder
		std::vector<double> set_min_btag (1,-100);
		bjet_finder->set_min_btag_value(set_min_btag);	
		std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(btag.first);
		if(btag_ids_this_algo->size() > 0)
			max_b_id = (*btag_ids_this_algo)[0].first;
		if(btag_ids_this_algo->size() > 1)
			max2_b_id = (*btag_ids_this_algo)[1].first;


		//If no bjets found in event fill plots for when highest and second highest are non-bjets
		if (matched_bjet_ids.empty() == true){	
			if(max_b_id != -1){
				histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("bjets_in_event_allbjets"+id).c_str()]->Fill(0);
				histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("nonbjet_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());}
			else if(max_b_id == -1){
				histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(1);}
			
			if(max2_b_id != -1){
				histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(-1);
				histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_second_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("nonbjet_second_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());}
			else if(max2_b_id == -1){
				histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(2);}
			continue;
		}

		histos1d[("bjets_in_event_allbjets"+id).c_str()]->Fill(matched_bjet_ids.size());

		std::vector<int>::iterator max_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),max_b_id);
		std::vector<int>::iterator max2_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),max2_b_id);
		
		//truth match highest 
		if(max_b_id != -1){
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(0);
			if(max_is_b != matched_bjet_ids.end()){
				histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("bjet_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("bjet_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());
			}
			else{
				histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("nonbjet_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());}
		}
		else if (max_b_id == -1){
			histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(1);}	
		
	       
		//now examine second highest btagged, if id != -1 && highest jet info is available (because will only be using second highest if highest is also good)
		if(max2_b_id != -1 && max_b_id != -1){
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(-1);
			if(max2_is_b != matched_bjet_ids.end()){
				histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("bjet_second_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("bjet_second_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());
			}
			else {
				histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_second_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("nonbjet_second_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());}
		}
		else if (max2_b_id == -1){
			histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(2);}		

		// check how often 5th or lower pt jet is 1st or 2nd b-jet
		if(max_b_id != -1 && jets->size() > 4)
			histos2d[("1st_b_pt_vs_4th_jet_pt"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt(), (*jets)[3].pt());
		if(max2_b_id != -1 && jets->size() > 4)
			histos2d[("2nd_b_pt_vs_4th_jet_pt"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt(), (*jets)[3].pt());
	}
}

//Plot value returned by bDiscrim for real b jets and non bjets, for two jets with highest btag value in event (using RecoGenMatch to see if real bjet from semileptonic decay)
void PlotGenerator::plot_bDiscriminator()
{
	if(!reco_gen_match->is_geninfo_available()){
		return;
	}
	if(reco_gen_match == NULL)
		return;
	if(bjet_finder == NULL)
		return;

	int njets = jets->size();
	if ( njets < 2)
		return;
	
	std::map<std::string,int> matched_jet_ids = reco_gen_match->get_matched_recojets_id(); //min_dD with dR cut

	//number of bjets available after reconstruction & selection
	int num_b_jets = 0;
	if(matched_jet_ids["hadb"] != -1){
		++num_b_jets;			
	}
	if(matched_jet_ids["lepb"] != -1){
		++num_b_jets;
	}
	histos1d[("bjets_in_event"+id).c_str()]->Fill(num_b_jets);


	std::vector<std::pair<std::string,std::string> > btag_algos;
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighEffBJetTags","_HighEff"));
        btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));


	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){
		std::pair<std::string,std::string> btag = (*btag_algo);

		//bDiscrim of genmatched jets
		if(matched_jet_ids["hadb"] != -1){
			histos1d[("bjet_bDiscrim_hadb"+btag.second+id).c_str()]->Fill((*jets)[matched_jet_ids["hadb"]].bDiscriminator(btag.first));		
			histos1d[("bjet_pt_order"+id).c_str()]->Fill(matched_jet_ids["hadb"]);}
		if(matched_jet_ids["lepb"] != -1){
			histos1d[("bjet_bDiscrim_lepb"+btag.second+id).c_str()]->Fill((*jets)[matched_jet_ids["lepb"]].bDiscriminator(btag.first));		
			histos1d[("bjet_pt_order"+id).c_str()]->Fill(matched_jet_ids["lepb"]);}
					
		int max_b_id = -1,max2_b_id = -1;	
		//get back highest and second highest btagged ids for this btagging algorithm. from BJetFinder
		std::vector<double> set_min_btag (1,-100);
		bjet_finder->set_min_btag_value(set_min_btag);
		std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(btag.first);
		if(btag_ids_this_algo->size() >= 1)
			max_b_id = (*btag_ids_this_algo)[0].first;
		if(btag_ids_this_algo->size() >= 2)
			max2_b_id = (*btag_ids_this_algo)[1].first;
				
		if(max_b_id != -1){	
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]->Fill(0);	
			if(matched_jet_ids["hadb"] == max_b_id || matched_jet_ids["lepb"] == max_b_id ){
				histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_highest_bDiscrim"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("bjet_highest_pt"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("bjet_highest_eta"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());
				if(matched_jet_ids["hadb"] == max_b_id)
					histos1d[("bjets_highest_identify"+btag.second+id).c_str()]->Fill(1);			
				else if(matched_jet_ids["lepb"] == max_b_id)
					histos1d[("bjets_highest_identify"+btag.second+id).c_str()]->Fill(0);}
			
			else{
				histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_highest_bDiscrim"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_highest_pt"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("nonbjet_highest_eta"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());}
			
		}
		else if(max_b_id == -1){
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(0);	
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]->Fill(1);}
			

		if(max2_b_id != -1 && max_b_id != -1){
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]->Fill(-1);
			if(matched_jet_ids["hadb"] == max2_b_id || matched_jet_ids["lepb"] == max2_b_id ){
				histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_second_highest_bDiscrim"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("bjet_second_highest_pt"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("bjet_second_highest_eta"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());
				if(matched_jet_ids["hadb"] == max2_b_id)
					histos1d[("bjets_second_highest_identify"+btag.second+id).c_str()]->Fill(1);			
				else if(matched_jet_ids["lepb"] == max2_b_id)
					histos1d[("bjets_second_highest_identify"+btag.second+id).c_str()]->Fill(0);}
			else{
				histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_second_highest_bDiscrim"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_second_highest_pt"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("nonbjet_second_highest_eta"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());}
		}
		else if(max2_b_id == -1){
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(0);
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]->Fill(2);}

		//how often are both found.
		if(max2_b_id != -1 && max_b_id != -1 &&(matched_jet_ids["hadb"] == max2_b_id || matched_jet_ids["lepb"] == max2_b_id) && (matched_jet_ids["hadb"] == max_b_id || matched_jet_ids["lepb"] == max_b_id ))
				histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]->Fill(3);
		else if(max_b_id != -1 && (matched_jet_ids["hadb"] == max_b_id || matched_jet_ids["lepb"] == max_b_id ))
				histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]->Fill(1);
		else if(max2_b_id != -1 && (matched_jet_ids["hadb"] == max2_b_id || matched_jet_ids["lepb"] == max2_b_id ))
				histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]->Fill(2);
		else
				histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]->Fill(0);
		
			

		//plot btag value for all jets
		for(int i=0;i < njets; ++i){
			double bDiscrim = (*jets)[i].bDiscriminator(btag.first) ;
			if(matched_jet_ids["hadb"] == i || matched_jet_ids["lepb"] == i){
				histos1d[("bjet_bDiscrim"+btag.second+id).c_str()]->Fill(bDiscrim);	
			}
			else{
				histos1d[("nonbjet_bDiscrim"+btag.second+id).c_str()]->Fill(bDiscrim);
			}
		}
	
		//dR of highest and second highest btagged jets vs. genmatched W
		if(max_b_id != -1 && max2_b_id != -1){
			if(matched_jet_ids["Wquark1"] != -1 && matched_jet_ids["Wquark2"] != -1){
				double maxb_Wjj = ROOT::Math::VectorUtil::DeltaR((*jets)[max_b_id].p4(),((*jets)[matched_jet_ids["Wquark1"]].p4()+(*jets)[matched_jet_ids["Wquark2"]].p4()));
				double max2b_Wjj = ROOT::Math::VectorUtil::DeltaR((*jets)[max2_b_id].p4(),((*jets)[matched_jet_ids["Wquark1"]].p4()+(*jets)[matched_jet_ids["Wquark2"]].p4()));
				histos1d[("dR_highest_bjet_Wjj"+btag.second+id).c_str()]->Fill(maxb_Wjj);
				histos1d[("dR_second_highest_bjet_Wjj"+btag.second+id).c_str()]->Fill(max2b_Wjj);				
			}			
			// 		//dR b´s and leptons
			if((*isolated_muons).size() > 0){
				double maxb_muon = ROOT::Math::VectorUtil::DeltaR((*jets)[max_b_id].p4(),(*isolated_muons)[0].p4());
				double max2b_muon = ROOT::Math::VectorUtil::DeltaR((*jets)[max2_b_id].p4(),(*isolated_muons)[0].p4());
				histos1d[("dR_highest_bjet_muon"+btag.second+id).c_str()]->Fill(maxb_muon);
				histos1d[("dR_second_highest_bjet_muon"+btag.second+id).c_str()]->Fill(max2b_muon);
			}
		}
	}
}

//Matching to all bjets in order to use for background also
void PlotGenerator::analyse_btag_algorithms()
{
	if(reco_gen_match == NULL)
		return;
	int njets = jets->size(), max_b_id = -1,max2_b_id = -1;
	if(njets < 2)
		return;
	if(bjet_finder == NULL)
		return;
		
	//	std::map<std::string,int> matched_jet_ids = reco_gen_match->get_matched_recojets_id(); //min_dD with dR cut	
	//now get back the ids for all bjets in event (genParton =5)
	std::vector<int> matched_bjet_ids = reco_gen_match->get_matched_bjets_id();

	std::vector<std::pair<std::string,std::string> > btag_algos;
	btag_algos.push_back(std::pair<std::string,std::string>("jetProbabilityBJetTags","_JetProb"));
	//btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighPurBJetTags","_HighPur"));
	btag_algos.push_back(std::pair<std::string,std::string>("softMuonNoIPBJetTags","_SoftMuNoIP"));
	btag_algos.push_back(std::pair<std::string,std::string>("softMuonBJetTags","_SoftMu"));
	btag_algos.push_back(std::pair<std::string,std::string>("simpleSecondaryVertexBJetTags","_SSVertex"));
	btag_algos.push_back(std::pair<std::string,std::string>("impactParameterMVABJetTags","_IPMVA"));
	btag_algos.push_back(std::pair<std::string,std::string>("combinedSecondaryVertexMVABJetTags","_CSVertexMVA"));
	btag_algos.push_back(std::pair<std::string,std::string>("combinedSecondaryVertexBJetTags","_CSVertex"));
	btag_algos.push_back(std::pair<std::string,std::string>("softElectronBJetTags","_SoftEle"));
	
	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){

		std::pair<std::string,std::string> btag = (*btag_algo);	
		max_b_id = -1;max2_b_id = -1;
		
		//If no bjets found in event fill plots for when highest and second highest are non-bjets
		if (matched_bjet_ids.empty() == true){
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(0);
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(0);
			for (int i = 0;i < njets;i++){

				double bDiscrim = (*jets)[i].bDiscriminator(btag.first);				
				histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
			}
			continue;
		}
		
		//get back highest and second highest btagged ids for this btagging algorithm. from BJetFinder
		std::vector<double> set_min_btag (1,-1000);
		bjet_finder->set_min_btag_value(set_min_btag);		
		std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(btag.first);
		if(btag_ids_this_algo->size() >= 1)
			max_b_id = (*btag_ids_this_algo)[0].first;
		if(btag_ids_this_algo->size() >= 2)
			max2_b_id = (*btag_ids_this_algo)[1].first;
		
		std::vector<int>::iterator max_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),max_b_id);
		std::vector<int>::iterator max2_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),max2_b_id);			
		
		if(max_b_id != -1 && max_is_b != matched_bjet_ids.end()){
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(1);}
		else{
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(0);}
		
		if(max2_b_id != -1 && max2_is_b != matched_bjet_ids.end()){
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(1);}
		else{
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(0);}
		
		for (int i = 0;i < njets;i++){
			std::vector<int>::iterator i_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),i);

			double bDiscrim = (*jets)[i].bDiscriminator(btag.first);

			if(i_is_b != matched_bjet_ids.end())
				histos1d[("bjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
			else
				histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
		}		
	}	
	btag_algos.clear();
}

void PlotGenerator::plot_bDiscriminator_find_btagcut()
{
	int njets = jets->size();
	if ( njets < 2)
		return;
	if(reco_gen_match == NULL)
		return;
	
	
	//Matching all bjets
	std::vector<std::pair<std::string,std::string> > btag_algos;
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighEffBJetTags","_HighEff"));
	btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));

	int def_min_btag = -1000; //default value if vector not set
	if(min_btag->size() < 1)
		min_btag->push_back(def_min_btag);

	//get back the ids for all bjets in event (genParton =5)
	std::vector<int> matched_bjet_ids = reco_gen_match->get_matched_bjets_id();

	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){

		std::pair<std::string,std::string> btag = (*btag_algo);	


		//get back vector of jet(id,btag) in order of decreasing btag
		bjet_finder->set_min_btag_value(*min_btag);	
		std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(btag.first);
	
		double jets_passed_btag = btag_ids_this_algo->size();
		histos1d[("jets_btagged_cut"+btag.second+id).c_str()]->Fill(jets_passed_btag);
		
		double bDiscrim = -1 ,pt = -1,eta = -1;
		if (jets_passed_btag > 0){
			bDiscrim =(*btag_ids_this_algo)[0].second;
			pt = (*jets)[(*btag_ids_this_algo)[0].first].pt();
			eta = (*jets)[(*btag_ids_this_algo)[0].first].eta();
			histos1d[("jet_highest_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
			histos1d[("jet_highest_pt_cut"+btag.second+id).c_str()]->Fill(pt);
			histos2d[("jet_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,pt);
			histos1d[("jet_highest_eta_cut"+btag.second+id).c_str()]->Fill(eta);}
		if (jets_passed_btag > 1){
			bDiscrim =(*btag_ids_this_algo)[1].second;
			pt = (*jets)[(*btag_ids_this_algo)[1].first].pt();
			eta = (*jets)[(*btag_ids_this_algo)[1].first].eta();
			histos1d[("jet_second_highest_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
			histos1d[("jet_second_highest_pt_cut"+btag.second+id).c_str()]->Fill(pt);
			histos2d[("jet_second_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,pt);
			histos1d[("jet_second_highest_eta_cut"+btag.second+id).c_str()]->Fill(eta);}
	
		
		int bjets_tagged = 0;
		for (std::vector<std::pair<int,double> >::iterator btag_ids_iter = btag_ids_this_algo->begin();btag_ids_iter != btag_ids_this_algo->end();btag_ids_iter++){	
			
			//check if jet id is in the matched bjets vector
			std::vector<int>::iterator jet_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),(*btag_ids_iter).first);
			bDiscrim = (*btag_ids_iter).second;
			//std::cout << " int " << (*btag_ids_iter).first << " bDiscrim " << (*btag_ids_iter).second << std::endl;
		
			if(jet_is_b != matched_bjet_ids.end()){
				bjets_tagged++;
				histos1d[("bjet_efficiency_cut"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_passed_cut"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
				histos1d[("bjet_pt_cut"+btag.second+id).c_str()]->Fill((*jets)[(*btag_ids_iter).first].pt());
				histos2d[("bjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,(*jets)[(*btag_ids_iter).first].pt());
				histos1d[("bjet_eta_cut"+btag.second+id).c_str()]->Fill((*jets)[(*btag_ids_iter).first].eta());
				histos2d[("bjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,(*jets)[(*btag_ids_iter).first].eta());
			}
			else{
				histos1d[("bjet_passed_cut"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
				histos1d[("nonbjet_pt_cut"+btag.second+id).c_str()]->Fill((*jets)[(*btag_ids_iter).first].pt());
				histos2d[("nonbjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,(*jets)[(*btag_ids_iter).first].pt());
				histos1d[("nonbjet_eta_cut"+btag.second+id).c_str()]->Fill((*jets)[(*btag_ids_iter).first].eta());
				histos2d[("nonbjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,(*jets)[(*btag_ids_iter).first].eta());

				//To replicate mistag efficiency from AN-2007/048. e mistag = Nlight,tagged/Nlight,taggable. taggable = has >=2 tracks.
				if((*jets)[(*btag_ids_iter).first].associatedTracks().size() >= 2 && (*jets)[(*btag_ids_iter).first].genParton() != NULL && fabs((*jets)[(*btag_ids_iter).first].genParton()->pdgId()) == 4){
					histos1d[("cjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
				}
				else if((*jets)[(*btag_ids_iter).first].associatedTracks().size() >= 2 && (*jets)[(*btag_ids_iter).first].genParton() != NULL && fabs((*jets)[(*btag_ids_iter).first].genParton()->pdgId()) != 4){
					histos1d[("lightjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
				}
			}

		}
		
		int size_bjets_not_tagged = (matched_bjet_ids.size()-bjets_tagged);
		for (int i=0;i < size_bjets_not_tagged;i++)
			histos1d[("bjet_efficiency_cut"+btag.second+id).c_str()]->Fill(0);
		
	}
	if((*min_btag)[0] == def_min_btag)
		min_btag->clear();
}

/******************************not currently used***************************************/
				     


//Currently examines differeence between whichever matching method set and genParton->bid->mother==top. comparison only made for bjets
void PlotGenerator::plot_MCmethod_comparison()
{
	int njets = jets->size();
	if ( njets < 1)
		return;
	if(!reco_gen_match->is_geninfo_available()){
		return;
	}
	if(reco_gen_match == NULL)
		return;	
	
	reco_gen_match->set_matching_method(2);
	std::map<std::string,int> matched_jet_ids = reco_gen_match->get_matched_recojets_id(); 
	reco_gen_match->set_matching_method(3);
	std::map<std::string,int> matched_jet_ids2 = reco_gen_match->get_matched_recojets_id();
	reco_gen_match->set_matching_method(-1);
	std::map<std::string,reco::GenParticle*> genquark;

	//truth matching  
	if(gen_match != NULL && gen_match->is_ok() && gen_match->get_decay_mode() == 1){
		
		//All from hadronic top decay
		if(!(gen_match->t_decay_is_leptonic())){	
			genquark["Wquark1"] = gen_match->get_particle("Wplus_dp1");
			genquark["Wquark2"] = gen_match->get_particle("Wplus_dp2");
			genquark["hadb"] = gen_match->get_particle("b");
			genquark["lepb"] = gen_match->get_particle("bbar");
		}
		
		else if(!(gen_match->tbar_decay_is_leptonic())){
			genquark["Wquark1"]= gen_match->get_particle("Wminus_dp1");
			genquark["Wquark2"]= gen_match->get_particle("Wminus_dp2");
			genquark["hadb"]= gen_match->get_particle("bbar");
			genquark["lepb"]= gen_match->get_particle("b");
		  
		}
	}


	int num_MC1_bjets =0,num_MC2_bjets = 0, num_agreed_bjets = 0, num_MC1_jets =0,num_MC2_jets = 0, num_agreed_jets = 0;
	//for counting number of bjets in event when genInfo is available matched by recogenmatch
	for(int i=0;i < njets; ++i){
		std::string genmatch_MC1 = "-1",genmatch_MC2 = "-1";
		int num_MC1 = -1,num_MC2 = -1;
		if(matched_jet_ids["hadb"] == i){ genmatch_MC1 = "hadb"; num_MC1 = 1;}
		if(matched_jet_ids["lepb"] == i){ genmatch_MC1 = "lepb"; num_MC1 = 2;}
		if(matched_jet_ids["Wquark1"] == i){ genmatch_MC1 = "Wquark1"; num_MC1 = 3;}
		if(matched_jet_ids["Wquark2"] == i){ genmatch_MC1 = "Wquark2"; num_MC1 = 4;}

		if(matched_jet_ids2["hadb"] == i){ genmatch_MC2 = "hadb"; num_MC2 = 1;}
		if(matched_jet_ids2["lepb"] == i){ genmatch_MC2 = "lepb"; num_MC2 = 2;}
		if(matched_jet_ids2["Wquark1"] == i){ genmatch_MC2 = "Wquark1"; num_MC2 = 3;}
		if(matched_jet_ids2["Wquark2"] == i){ genmatch_MC2 = "Wquark2"; num_MC2 = 4;}
		

		//calculate dR vs dPT. for closest jet-parton match
		double dR_MC1= 0,dR_MC2= 0,dPt_MC1 = 0,dPt_MC2 = 0 ;
		if(genmatch_MC1 != "-1"){
			dR_MC1 = ROOT::Math::VectorUtil::DeltaR(genquark[genmatch_MC1]->p4(),(*jets)[i].p4());
			dPt_MC1 = (((*jets)[i].pt()- genquark[genmatch_MC1]->pt())/genquark[genmatch_MC1]->pt());}
		if(genmatch_MC2 != "-1"){
			dR_MC2 = ROOT::Math::VectorUtil::DeltaR(genquark[genmatch_MC2]->p4(),(*jets)[i].p4());
			dPt_MC2 = (((*jets)[i].pt()- genquark[genmatch_MC2]->pt())/genquark[genmatch_MC2]->pt());}
		
		if(genmatch_MC1 == genmatch_MC2 && genmatch_MC1 != "-1"){
			++num_MC1_jets; 
			++num_MC2_jets;
			++num_agreed_jets;
			histos1d[("jets_MCmethods_compare"+id).c_str()]->Fill(2);
			histos2d[("dPt_vs_dR_jet_MCmethods_agree"+id).c_str()]->Fill(dR_MC1,dPt_MC1);
			if(genmatch_MC1 == "hadb" || genmatch_MC1 == "lepb" ){
				++num_MC1_bjets; 
				++num_MC2_bjets;
				++num_agreed_bjets;
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(2);
				histos2d[("dPt_vs_dR_jet_MCmethods_agree"+id).c_str()]->Fill(dR_MC1,dPt_MC1);}
			
		}
		else if(genmatch_MC1 != "-1" && genmatch_MC2 == "-1"){
			++num_MC1_jets; 
			histos2d[("dPt_vs_dR_jet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);
			histos1d[("jets_MCmethods_compare"+id).c_str()]->Fill(1);
			if(matched_jet_ids["hadb"] == i || matched_jet_ids["lepb"] == i){
				++num_MC1_bjets; 
				histos2d[("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(1);}
		}

		else if(genmatch_MC2 != "-1" && genmatch_MC1 == "-1"){
			++num_MC2_jets; 
			histos2d[("dPt_vs_dR_jet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);
			histos1d[("jets_MCmethods_compare"+id).c_str()]->Fill(-1);
			if(matched_jet_ids2["hadb"] == i||matched_jet_ids2["lepb"] == i ){
				++num_MC2_bjets; 
				histos2d[("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(-1);}
			
		}
		//If both found partons but not the same one
		else if(genmatch_MC2 != "-1" && genmatch_MC1 != "-1"){
			histos2d[("dPt_vs_dR_jet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);
			histos2d[("dPt_vs_dR_jet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);
			histos1d[("jets_MCmethods_compare"+id).c_str()]->Fill(-2);

			if((genmatch_MC1 == "hadb" || genmatch_MC1 == "lepb")&& (genmatch_MC2 == "hadb" || genmatch_MC2 == "lepb")){
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(-2);
				histos2d[("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);
				histos2d[("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);}
			else if((genmatch_MC1 == "hadb" || genmatch_MC1 == "lepb")&& !(genmatch_MC2 == "hadb" || genmatch_MC2 == "lepb")){
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(-3);
				histos2d[("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);}
			else if(!(genmatch_MC1 == "hadb" || genmatch_MC1 == "lepb")&& (genmatch_MC2 == "hadb" || genmatch_MC2 == "lepb")){
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(-4);
				histos2d[("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);}


			histos2d[("jet_MCnumber_when_disagree"+id).c_str()]->Fill(num_MC1,num_MC2);

			
		}

			
	}
	
	histos1d[("bjets_in_event_MC1"+id).c_str()]->Fill(num_MC1_bjets);	
	histos1d[("bjets_in_event_MC2"+id).c_str()]->Fill(num_MC2_bjets);	
	histos1d[("bjets_in_event_MCboth"+id).c_str()]->Fill(num_agreed_bjets);

	histos1d[("jets_in_event_MC1"+id).c_str()]->Fill(num_MC1_jets);	
	histos1d[("jets_in_event_MC2"+id).c_str()]->Fill(num_MC2_jets);	
	histos1d[("jets_in_event_MCboth"+id).c_str()]->Fill(num_agreed_jets);
	
}
/**************************************************Trigger*********************************************************/

//plot trigger efficiency vs eta||pt for pt_gen and pt_reco
void PlotGenerator::plot_trigger()
{ 
  for(std::vector<pat::Muon>::iterator isolated_mu = isolated_muons->begin();
      isolated_mu!=isolated_muons->end(); 
      ++isolated_mu){

    //Check if it should pass the trigger cut (truth matching with generator level)
    if (genEvt->singleLepton() != NULL && genEvt->singleLepton()->pt() >= 11 && fabs(genEvt->singleLepton()->eta()) <= 2.1) 	
      {
	//Of those that should,how many do pass trigger cut
	if(isolated_mu->triggerObjectMatchesByFilter("hltSingleMuNoIsoL3PreFiltered11").size() != 0){
	 
	  histos1d[("untrig_mu11_pt"+id).c_str()]->Fill(isolated_mu->pt());
	  histos1d[("untrig_mu11_eta"+id).c_str()]->Fill(isolated_mu->eta());
	  histos1d[("untrig_mu11_pt_gen"+id).c_str()]->Fill(genEvt->singleLepton()->pt());
	  histos1d[("untrig_mu11_eta_gen"+id).c_str()]->Fill(genEvt->singleLepton()->eta());

	  histos1d[("trig_mu11_pt_gen"+id).c_str()]->Fill(genEvt->singleLepton()->pt());
	  histos1d[("trig_mu11_eta_gen"+id).c_str()]->Fill(genEvt->singleLepton()->eta());
	  histos1d[("trig_mu11_pt"+id).c_str()]->Fill(isolated_mu->pt());
	  histos1d[("trig_mu11_eta"+id).c_str()]->Fill(isolated_mu->eta());
	}
	else{
	  histos1d[("untrig_mu11_pt"+id).c_str()]->Fill(isolated_mu->pt());	
	  histos1d[("untrig_mu11_eta"+id).c_str()]->Fill(isolated_mu->eta());
	  histos1d[("untrig_mu11_pt_gen"+id).c_str()]->Fill(genEvt->singleLepton()->pt());
	  histos1d[("untrig_mu11_eta_gen"+id).c_str()]->Fill(genEvt->singleLepton()->eta());
	}	
      }
  }
    
  for(std::vector<pat::Electron>::const_iterator e_iter = isolated_electrons->begin();
      e_iter != isolated_electrons->end();
      e_iter++){

    //Check if it should pass the trigger cut
    if ((genEvt->singleLepton() != NULL && genEvt->singleLepton()->pt() >= 15 && fabs(genEvt->singleLepton()->eta()) <= 2.4)) 
	
      {
	//Of those that should,how many do pass trigger cut
	if(e_iter->triggerObjectMatchesByFilter("hltL1IsoLargeWindowSingleElectronTrackIsolFilter").size() != 0)
	  {	   
	    histos1d[("untrig_e15_pt"+id).c_str()]->Fill(e_iter->pt());
	    histos1d[("untrig_e15_eta"+id).c_str()]->Fill(e_iter->eta());
	    histos1d[("untrig_e15_pt_gen"+id).c_str()]->Fill(genEvt->singleLepton()->pt());
	    histos1d[("untrig_e15_eta_gen"+id).c_str()]->Fill(genEvt->singleLepton()->eta());

	    histos1d[("trig_e15_pt_gen"+id).c_str()]->Fill(genEvt->singleLepton()->pt());
	    histos1d[("trig_e15_eta_gen"+id).c_str()]->Fill(genEvt->singleLepton()->eta());
	    histos1d[("trig_e15_pt"+id).c_str()]->Fill(e_iter->pt());
	    histos1d[("trig_e15_eta"+id).c_str()]->Fill(e_iter->eta());	    
	  }
	else{
	  histos1d[("untrig_e15_pt"+id).c_str()]->Fill(e_iter->pt());
	  histos1d[("untrig_e15_eta"+id).c_str()]->Fill(e_iter->eta());
	  histos1d[("untrig_e15_eta_gen"+id).c_str()]->Fill(genEvt->singleLepton()->eta());
	  histos1d[("untrig_e15_pt_gen"+id).c_str()]->Fill(genEvt->singleLepton()->pt());
	}
      }
  }  
}

/**************not useful when jobs need to be combined********/
void PlotGenerator::get_triggereff()
{
//trigger_unmatched is really sum trig matched+unmatched
  //trigger_matched->Divide(trigger_unmatched)

  //error bars
  histos1d[("trig_e15_pt"+id).c_str()]->Sumw2();
  histos1d[("untrig_e15_pt"+id).c_str()]->Sumw2();
  histos1d[("trig_mu11_pt"+id).c_str()]->Sumw2();
  histos1d[("untrig_mu11_pt"+id).c_str()]->Sumw2();
  histos1d[("trig_e15_eta"+id).c_str()]->Sumw2();
  histos1d[("untrig_e15_eta"+id).c_str()]->Sumw2();
  histos1d[("trig_mu11_eta"+id).c_str()]->Sumw2();
  histos1d[("untrig_mu11_eta"+id).c_str()]->Sumw2();
  
  histos1d[("trig_e15_pt_gen"+id).c_str()]->Sumw2();
  histos1d[("untrig_e15_pt_gen"+id).c_str()]->Sumw2();
  histos1d[("trig_mu11_pt_gen"+id).c_str()]->Sumw2();
  histos1d[("untrig_mu11_pt_gen"+id).c_str()]->Sumw2();
  histos1d[("trig_e15_eta_gen"+id).c_str()]->Sumw2();
  histos1d[("untrig_e15_eta_gen"+id).c_str()]->Sumw2();
  histos1d[("trig_mu11_eta_gen"+id).c_str()]->Sumw2();
  histos1d[("untrig_mu11_eta_gen"+id).c_str()]->Sumw2();
  
  //divide to calc efficiency
  histos1d[("trig_e15_eff_vs_pt"+id).c_str()]->Divide(histos1d[("trig_e15_pt"+id).c_str()],histos1d[("untrig_e15_pt"+id).c_str()],1.0,1.0,"B");
  histos1d[("trig_mu11_eff_vs_pt"+id).c_str()]->Divide(histos1d[("trig_mu11_pt"+id).c_str()],histos1d[("untrig_mu11_pt"+id).c_str()],1.0,1.0,"B");
  histos1d[("trig_e15_eff_vs_eta"+id).c_str()]->Divide(histos1d[("trig_e15_eta"+id).c_str()],histos1d[("untrig_e15_eta"+id).c_str()],1.0,1.0,"B");
  histos1d[("trig_mu11_eff_vs_eta"+id).c_str()]->Divide(histos1d[("trig_mu11_eta"+id).c_str()],histos1d[("untrig_mu11_eta"+id).c_str()],1.0,1.0,"B");

  histos1d[("trig_e15_eff_vs_pt_gen"+id).c_str()]->Divide(histos1d[("trig_e15_pt_gen"+id).c_str()],histos1d[("untrig_e15_pt_gen"+id).c_str()],1.0,1.0,"B");
  histos1d[("trig_mu11_eff_vs_pt_gen"+id).c_str()]->Divide(histos1d[("trig_mu11_pt_gen"+id).c_str()],histos1d[("untrig_mu11_pt_gen"+id).c_str()],1.0,1.0,"B");
  histos1d[("trig_e15_eff_vs_eta_gen"+id).c_str()]->Divide(histos1d[("trig_e15_eta_gen"+id).c_str()],histos1d[("untrig_e15_eta_gen"+id).c_str()],1.0,1.0,"B");
  histos1d[("trig_mu11_eff_vs_eta_gen"+id).c_str()]->Divide(histos1d[("trig_mu11_eta_gen"+id).c_str()],histos1d[("untrig_mu11_eta_gen"+id).c_str()],1.0,1.0,"B");
	    
}
