#include "analyzers/EventSelection/interface/METPlots.h"

METPlots::METPlots(std::string ident)
{
	id = ident;
	book_histos();
}

METPlots::~METPlots()
{
}

void METPlots::plot()
{
	plot_MET_pz();
}

void METPlots::plot_MET_pz()
{
	if(gen_match == NULL || !gen_match->is_ok() || gen_match->get_decay_mode() != 1)
		return;

	if(isolated_muons->size() < 1)
		return;

	reco::GenParticle *neutrino = NULL;
	if(gen_match->t_decay_is_leptonic()){
		neutrino = gen_match->get_particle("Wplus_dp2");
	}else if(gen_match->tbar_decay_is_leptonic()){
		neutrino = gen_match->get_particle("Wminus_dp2");
	}else{
		std::cerr << "WARNING: METPlots::plot_MET_pz() neutrino NULL" << std::endl;
	}


	pat::MET corrected_MET = *(corrected_mets->begin());
//	pat::MET uncorrected_MET = *(uncorrected_mets->begin());
	histos1d[("dpz_corMET_neutrino"+id).c_str()]->Fill(neutrino->pz() - corrected_MET.pz());
	histos1d[("dR_corMET_neutrino"+id).c_str()]->Fill(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), corrected_MET.p4()));
	histos1d[("neutrino_pz"+id).c_str()]->Fill(neutrino->pz());
	histos1d[("neutrino_pt"+id).c_str()]->Fill(neutrino->pt());
	histos1d[("dpt_neutrino_MET"+id).c_str()]->Fill(neutrino->pt() - corrected_MET.pt());
//	histos1d[("dpz_MET_neutrino"+id).c_str()]->Fill(neutrino->pz() - uncorrected_MET.pz());
	histos1d[("dR_MET_neutrino"+id).c_str()]->Fill(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), corrected_MET.p4()));

	// how often do neutrino and MET p4 match?
	if(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), corrected_MET.p4()) < 0.4){
		histos1d[("match_MET_neutrino"+id).c_str()]->Fill(1);
	}else{
		histos1d[("match_MET_neutrino"+id).c_str()]->Fill(0);
	}

	histos2d[("pt_neutrino_vs_MET"+id).c_str()]->Fill(neutrino->pt(), corrected_MET.pt());
	histos2d[("pz_neutrino_vs_MET"+id).c_str()]->Fill(neutrino->pz(), corrected_MET.pz());

	// FIXME: fix energy-scaling
        pat::Particle::LorentzVector p;
        p.SetPx(neutrino->px());
        p.SetPy(neutrino->py());
        p.SetPz(0.);
        p.SetE(neutrino->pt());

        pat::MET neutrino_MET;
        neutrino_MET.setP4(p);

	MyMEzCalculator MEzCal;
	MEzCal.SetMET(neutrino_MET);
	pat::Particle muon;
	muon.setP4(isolated_muons->begin()->p4());
	MEzCal.SetLepton(*(isolated_muons->begin()));

	double neutrinoMET_recolep_pz = MEzCal.Calculate();

	histos2d[("pz_neutrinoMET_recolep_vs_neutrino"+id).c_str()]->Fill(neutrino->pz(), neutrinoMET_recolep_pz);

	histos1d[("dpz_neutrino_recolep"+id).c_str()]->Fill(neutrino->pz() - neutrinoMET_recolep_pz);
}

void METPlots::book_histos()
{
	edm::Service<TFileService> fs;

	// METpz plots
//	histos1d[("dpz_MET_neutrino"+id).c_str()]=fs->make<TH1D>(("dpz_MET_neutrino"+id).c_str(),"delta p_z between MET and generator neutrino",100,-150,150);
	histos1d[("dR_MET_neutrino"+id).c_str()]=fs->make<TH1D>(("dR_MET_neutrino"+id).c_str(),"delta R between MET and generator neutrino",30,0,8);
	histos1d[("dpz_neutrino_recolep"+id).c_str()]=fs->make<TH1D>(("dpz_neutrino_recolep"+id).c_str(),"delta p_z between neutrino-MET-pz from reco lepton and generator neutrino",100,-150,150);
	histos1d[("dpz_corMET_neutrino"+id).c_str()]=fs->make<TH1D>(("dpz_corMET_neutrino"+id).c_str(),"delta p_{z} between MET and generator neutrino",100,-200,200);
	histos1d[("dR_corMET_neutrino"+id).c_str()]=fs->make<TH1D>(("dR_corMET_neutrino"+id).c_str(),"delta R between MET and generator neutrino",20,0,8);
	histos1d[("neutrino_pz"+id).c_str()]=fs->make<TH1D>(("neutrino_pz"+id).c_str(),"p_{z} of generator neutrino",100,-200,200);
	histos1d[("neutrino_pt"+id).c_str()]=fs->make<TH1D>(("neutrino_pt"+id).c_str(),"p_{T} of generator neutrino",70,0,300);
	histos1d[("dpt_neutrino_MET"+id).c_str()]=fs->make<TH1D>(("dpt_neutrino_MET"+id).c_str(),"dp_{T}(generator neutrino, MET)",100,-200,200);

	// 1 = matched, 0 = unmatched dR < 0.4
	histos1d[("match_MET_neutrino"+id).c_str()]=fs->make<TH1D>(("match_MET_neutrino"+id).c_str(),"dR(nu, MET) < 0.4) 0 = unmatched, 1 = matched",5,-0.5,4.5);

	// neutrino vs MET
	histos2d[("pz_neutrinoMET_recolep_vs_neutrino"+id).c_str()]=fs->make<TH2D>(("pz_neutrinoMET_recolep_vs_neutrino"+id).c_str(),"correlation between neutrino pz and ``neutrino-MET''-pz",150, -300, 300, 150,-300,300);
	histos2d[("pt_neutrino_vs_MET"+id).c_str()]=fs->make<TH2D>(("pt_neutrino_vs_MET"+id).c_str(),"correlation between neutrino pt and MET",150, 0, 300, 150,0,300);
	histos2d[("pz_neutrino_vs_MET"+id).c_str()]=fs->make<TH2D>(("pz_neutrino_vs_MET"+id).c_str(),"correlation between neutrino pz and pz component of MET",150, -300, 300, 150,-300,300);
}

