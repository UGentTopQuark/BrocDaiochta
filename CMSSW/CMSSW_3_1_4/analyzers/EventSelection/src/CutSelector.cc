#include "analyzers/EventSelection/interface/CutSelector.h"

CutSelector::CutSelector(std::string ident, double mass)
{
        tprime_mass = mass;
	dataset_id = ident;

	//define_cuts_14VIII09();
	//	define_cuts_OctX_mu();
//	define_cuts_OctX_e();
	define_cuts_01X09();
	//define_cuts_Francisco();
//		define_preselection_cuts();
	complete_cuts();
	set_cuts();


	ttmu_count = 0,tte_count = 0,ttbg_count = 0;
	//To check if this event has already been called for different
	//cut.Reinitiliased and end of event in plot()
	Event_firstcall = 1;

	edm::Service<TFileService> fs;
	event_counter_histo = fs->make<TH1D>("event_counter", "event counter", 10, -0.5, 9.5);
	
}

CutSelector::~CutSelector()
{
	/*
	 *	delete vector cuts
	 */
	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
	    type_iter != plot_generators.end();
	    ++type_iter)
	{
        	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
        	    pgen_iter != type_iter->second.end();
        	    ++pgen_iter){
			delete pgen_iter->second;
			pgen_iter->second=NULL;
		}
	}

	for(std::map<std::string, std::map<std::string, Cuts*> >::iterator type_iter = cuts.begin();
	    type_iter != cuts.end();
	    ++type_iter)
	{
		for(std::map<std::string,Cuts*>::iterator cuts_iter = type_iter->second.begin();
        	    cuts_iter != type_iter->second.end();
        	    ++cuts_iter){
			cuts_iter->second->print_cuts();
			delete cuts_iter->second;
			cuts_iter->second=NULL;
		}
	}

	for(std::vector<std::vector<double>* >::iterator del_it = cuts_to_be_deleted.begin();
	    del_it != cuts_to_be_deleted.end();
	    ++del_it){
		delete *del_it;
	}

	//Print number of ttbar muon,electron and backgound
	std::cout << "=++++++++++Event-Count:"<< dataset_id <<"++++++++++=" << std::endl;
	std::cout << "ttbar_mu_events: " << ttmu_count << std::endl;
	std::cout << "ttbar_e_events: " << tte_count << std::endl;
	std::cout << "ttbar_bg_events: " << ttbg_count << std::endl;
	std::cout << "=++++++++++++++++++++++++++++++=" << std::endl;
}

void CutSelector::plot()
{

   Event_firstcall = 1;
 	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
 	    type_iter != plot_generators.end();
 	    ++type_iter)
 	{
         	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
         	    pgen_iter != type_iter->second.end();
         	    ++pgen_iter){
 			if(type_iter->first == get_event_type() ||
 			   type_iter->first == ("e_"+get_event_type()) ||
 			   type_iter->first == ("mu_"+get_event_type()) ||
 			   (type_iter->first == "e_background" && get_event_type() == "muon") ||
 			   (type_iter->first == "mu_background" && get_event_type() == "electron") )
 				pgen_iter->second->plot();
				
 		}
 	}
}

std::string CutSelector::get_event_type()
{
	if( genEvt->isTtBar()){
	  if (genEvt->isSemiLeptonic()){

	    if ((genEvt->lepton() != NULL && genEvt->lepton()->pdgId() == 13) || (genEvt->leptonBar() != NULL && genEvt->leptonBar()->pdgId() == -13))
	      {
		if (Event_firstcall == 1){
		  event_counter_histo->Fill(1);
		  ttmu_count++;
		  Event_firstcall = false;
		}
		return "muon";
		
	      }
	    if ((genEvt->lepton() != NULL && genEvt->lepton()->pdgId() == 11) || (genEvt->leptonBar() != NULL && genEvt->leptonBar()->pdgId() == -11))
	      {
		if (Event_firstcall == 1){
		  event_counter_histo->Fill(2);
		  tte_count++;
		  Event_firstcall = false;
		}
		return "electron";
		
	      }	  
	    else
	      {
		if (Event_firstcall == 1){
		  event_counter_histo->Fill(3);
		  ttbg_count++;
		  Event_firstcall = false;
		}
		return "background";
		
	      }
	  }
	  else
	    {
	      if (Event_firstcall == 1){
		event_counter_histo->Fill(3);
		ttbg_count++;
		Event_firstcall = false;
	      }
	      return "background";
	      
	    } 
	}
	else
	  return "background";
}



void CutSelector::set_handles(edm::Handle<edm::View<pat::Muon> > analyzer_muons,
                           edm::Handle<edm::View<pat::Jet> > analyzer_jets,
                           edm::Handle<edm::View<pat::Electron> > analyzer_electrons,
			   edm::Handle<edm::View<pat::MET> > analyzer_mets,
			   edm::Handle<TtGenEvent> genEvt,
			   edm::Handle<TtSemiLeptonicEvent> semiLepEvt,
			   edm::Handle<int> hypoClassKeyHandle,
			   edm::Handle<edm::TriggerResults> HLTR,
			   edm::Handle<reco::GenParticleCollection> genParticles,
			   edm::Handle<reco::BeamSpot> recoBeamSpotHandle)
{
        electrons = analyzer_electrons;
        muons = analyzer_muons;
        jets = analyzer_jets;
        mets = analyzer_mets;
	this->genEvt = genEvt;
	this->semiLepEvt = semiLepEvt;
	this->hypoClassKeyHandle = hypoClassKeyHandle;
	this->HLTR = HLTR;
	this->genParticles = genParticles;


	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
	    type_iter != plot_generators.end();
	    ++type_iter)
	{
        	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
        	    pgen_iter != type_iter->second.end();
        	    ++pgen_iter){
			pgen_iter->second->set_handles(muons, jets, electrons,
			mets, semiLepEvt, hypoClassKeyHandle, genEvt,HLTR,genParticles, recoBeamSpotHandle);
		}
	}
	for(std::map<std::string, std::map<std::string, Cuts*> >::iterator type_iter = cuts.begin();
	    type_iter != cuts.end();
	    ++type_iter)
	{
		for(std::map<std::string,Cuts*>::iterator cuts_iter = type_iter->second.begin();
        	    cuts_iter != type_iter->second.end();
        	    ++cuts_iter){
			cuts_iter->second->set_handles(muons, jets, electrons, mets, semiLepEvt, hypoClassKeyHandle, genEvt,HLTR, recoBeamSpotHandle);
		}
	}
}

/*
 *	For N-1 Plots:
 *	- put ALL cuts to the preselection-loop at the end of define_cuts()
 *	- disable in the upper part of the function the different cuts by 
 *	  setting them to -1
 */

void CutSelector::define_cuts()
{
	/*
	 *	declare cut vectors
	 *	remember to delete them in the destructor!
	 */

	std::vector<double> *jet_cuts = new std::vector<double>();
	jet_cuts->push_back(30);
	jet_cuts->push_back(30);
	jet_cuts->push_back(30);
	jet_cuts->push_back(30);
	cuts_to_be_deleted.push_back(jet_cuts);

	std::vector<double> *e_trackiso = new std::vector<double>();
	e_trackiso->push_back(3);
	e_trackiso->push_back(10);
	cuts_to_be_deleted.push_back(e_trackiso);

	std::vector<double> *e_ecaliso = new std::vector<double>();
	e_ecaliso->push_back(5);
	e_ecaliso->push_back(15);
	cuts_to_be_deleted.push_back(e_ecaliso);

	std::vector<double> *mu_trackiso = new std::vector<double>();
	mu_trackiso->push_back(3);
	mu_trackiso->push_back(10);
	cuts_to_be_deleted.push_back(mu_trackiso);

	std::vector<double> *mu_ecaliso = new std::vector<double>();
	mu_ecaliso->push_back(5);
	mu_ecaliso->push_back(15);
	cuts_to_be_deleted.push_back(mu_ecaliso);


	/*
	 *	cuts for MUONS
	 */
	/*
	cut_defs["muon"]["preselection"]["min_met"] = -1;
	cut_defs["muon"]["preselection"]["max_ht"] = -1;
	cut_defs["muon"]["preselection"]["min_ht"] = -1;
	cut_defs["muon"]["preselection"]["min_ht"] = -1;
	cut_defs["muon"]["preselection"]["max_nisolated_e"] = -1;
	cut_defs["muon"]["preselection"]["max_nisolated_mu"] = 1;
	cut_defs["muon"]["preselection"]["min_nisolated_e"] = -1;
	cut_defs["muon"]["preselection"]["min_nisolated_mu"] = 1;
	cut_defs["muon"]["preselection"]["min_no_jets"] = -1;
	cut_defs["muon"]["preselection"]["max_no_jets"] = -1;
	cut_defs["muon"]["preselection"]["name_btag"] = -1;
	cut_defs["muon"]["preselection"]["min_chi2"] = -1;
	cut_defs["muon"]["preselection"]["max_chi2"] = -1;
	vcuts("muon", "preselection", "min_mu_pt", -1);
	vcuts("muon", "preselection", "max_mu_trackiso", -1);
	vcuts("muon", "preselection", "max_mu_caliso", -1);
	vcuts("muon", "preselection", "max_mu_ecaliso", -1);
	vcuts("muon", "preselection", "max_mu_hcaliso", -1);
	vcuts("muon", "preselection", "max_mu_hcal_veto_cone", -1);
	vcuts("muon", "preselection", "max_mu_ecal_veto_cone", -1);
	vcuts("muon", "preselection", "max_mu_d0", -1);
	vcuts("muon", "preselection", "max_mu_d0sig", -1);
	vcuts("muon", "preselection", "max_mu_chi2", -1);
	vcuts("muon", "preselection", "min_mu_nHits", -1);
	vcuts("muon", "preselection", "mu_electronID", -1);
	vcuts("muon", "preselection", "max_e_hcal_veto_cone", -1);
	vcuts("muon", "preselection", "max_e_ecal_veto_cone", -1);
	vcuts("muon", "preselection", "min_e_pt", -1);
	vcuts("muon", "preselection", "max_e_trackiso", -1);
	vcuts("muon", "preselection", "max_e_caliso", -1);
	vcuts("muon", "preselection", "max_e_ecaliso", -1);
	vcuts("muon", "preselection", "max_e_hcaliso", -1);
	vcuts("muon", "preselection", "min_e_relIso", -1);
	vcuts("muon", "preselection", "max_e_d0", -1);
	vcuts("muon", "preselection", "max_e_d0sig", -1);
	vcuts("muon", "preselection", "max_e_chi2", -1);
	vcuts("muon", "preselection", "min_e_nHits", -1);
	vcuts("muon", "preselection", "e_electronID", -1);
	vcuts("muon", "preselection", "min_mu_dR", -1);
	vcuts("muon", "preselection", "min_mu_relIso", -1);
	vcuts("muon", "preselection", "min_e_dR", -1);
	vcuts("muon", "preselection", "min_jet_pt", 30.0);
	vcuts("muon", "preselection", "trigger", -1);
	vcuts("muon", "preselection", "max_mu_eta", -1);
	cut_defs["muon"]["preselection"]["mu_type"] = -1;
	cut_defs["muon"]["preselection"]["e_type"] = -1;
	cut_defs["muon"]["preselection"]["max_jet_eta"] = -1; 
	cut_defs["muon"]["preselection"]["min_jet_e_dR"] = -1; 
	cut_defs["muon"]["preselection"]["JES_factor"] = -1; 
	cut_defs["muon"]["preselection"]["Z_rejection_width"] = -1; 
	cut_defs["muon"]["preselection"]["min_M3"] = -1; 
	cut_defs["muon"]["preselection"]["min_mindiffM3"] = -1; 
	*/

	/*
	 *	global cuts for all selections defined so far
	 */
	synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
		type_iter != cut_defs.end();
		++type_iter)
	{
		for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
		{
			set_if_not_set(type_iter->first, set_iter->first, "min_no_jets", 4);
			vset_if_not_set(type_iter->first, set_iter->first, "min_jet_pt", jet_cuts);
			set_if_not_set(type_iter->first, set_iter->first, "max_jet_eta", 2.4);
			set_if_not_set(type_iter->first, set_iter->first, "mu_type", 0);
			vset_if_not_set(type_iter->first, set_iter->first, "max_e_eta", 2.4);
			vset_if_not_set(type_iter->first, set_iter->first, "max_mu_eta", 2.1);
			vset_if_not_set(type_iter->first, set_iter->first, "min_e_pt", 30);
			vset_if_not_set(type_iter->first, set_iter->first, "min_mu_pt", 30);
			vset_if_not_set(type_iter->first, set_iter->first, "min_mu_relIso", 0.0);
			vset_if_not_set(type_iter->first, set_iter->first, "min_e_relIso", 0.0);
			if(type_iter->first == "muon" || type_iter->first == "mu_background"){
				set_if_not_set(type_iter->first, set_iter->first, "max_nisolated_mu", 1.0);
				set_if_not_set(type_iter->first, set_iter->first, "min_nisolated_mu", 1.0);
				set_if_not_set(type_iter->first, set_iter->first, "max_nisolated_e", 0.0);
			}else if(type_iter->first == "electron" || type_iter->first == "e_background"){
				set_if_not_set(type_iter->first, set_iter->first, "max_nisolated_mu", 0.0);
				set_if_not_set(type_iter->first, set_iter->first, "max_nisolated_e", 1.0);
				set_if_not_set(type_iter->first, set_iter->first, "min_nisolated_e", 1.0);
			}
		}
	}

	/*
	 *	cuts for which the global selection cuts are not applied
	 */

	vcuts("muon", "without_cuts", "min_jet_pt", 0.0);
	vcuts("electron", "without_cuts", "min_jet_pt", 0.0);
	vcuts("mu_background", "without_cuts", "min_jet_pt", 0.0);
	vcuts("e_background", "without_cuts", "min_jet_pt", 0.0);
}


// void CutSelector::define_preselection_cuts()
// {
// 	/*
// 	 *	initialise preselection
// 	 */
// 	cut_defs["muon"]["preselection_step00"]["min_nisolated_mu"] = 1.0; 
// 	cut_defs["mu_background"]["preselection_step00"]["min_nisolated_mu"] = 1.0; 
// 	cut_defs["electron"]["preselection_step00"]["min_nisolated_e"] = 1.0; 
// 	cut_defs["e_background"]["preselection_step00"]["min_nisolated_e"] = 1.0; 
// 
// 	std::vector<double> *jet_pt_cuts = new std::vector<double>();
// 	jet_pt_cuts->push_back(65);
// 	jet_pt_cuts->push_back(40);
// 	jet_pt_cuts->push_back(40);
// 	jet_pt_cuts->push_back(40);
// 	cuts_to_be_deleted.push_back(jet_pt_cuts);
// 
// 	/*
// 	 *	set here the number of applied preselection cuts in the lower
// 	 *	part of this function (the initialisation has to be added to
// 	 *	this value, eg.: 1 initialisation + 4 subsequent cuts -> 5
// 	 */
// 	const int MAX_PRESELECTIONS=23;
// 
// 	synchronise_maps();
//         for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
// 		type_iter != cut_defs.end();
// 		++type_iter){
// 		for(int npreselection=1; npreselection < MAX_PRESELECTIONS; ++npreselection){
// 			cut_defs[type_iter->first][get_preselection_id(npreselection)]["dummy"] = -1;
// 			vcut_defs[type_iter->first][get_preselection_id(npreselection)]["dummy"] = NULL;
// 			std::map<std::string,std::map<std::string, double> >::reverse_iterator set_iter=type_iter->second.rbegin();
// 			++set_iter;
// 
// 			for(std::map<std::string, double>::iterator cut_iter = set_iter->second.begin();
// 			    cut_iter != set_iter->second.end();
// 			    ++cut_iter){
// 				std::string cur_set = get_preselection_id(npreselection);
// 				double value = (set_iter->second)[cut_iter->first];
// 				cut_defs[type_iter->first][cur_set][cut_iter->first] = value;
// 			}
// 
// 			std::map<std::string,std::map<std::string, std::vector<double>* > >::reverse_iterator vset_iter=vcut_defs[type_iter->first].rbegin();
// 			++vset_iter;
// 
// 			for(std::map<std::string, std::vector<double>* >::iterator cut_iter = vset_iter->second.begin();
// 			    cut_iter != vset_iter->second.end();
// 			    ++cut_iter){
// 				std::string cur_set = get_preselection_id(npreselection);
// 				std::vector<double> *value = (vset_iter->second)[cut_iter->first];
// 				vcut_defs[type_iter->first][cur_set][cut_iter->first] = value;
// 			}
// 
// 			/*
// 			 *	Put here the subsequent additional cuts
// 			 *	the cuts are then combined with all previous
// 			 *	cuts of this selection
// 			 */
// 			if(type_iter->first == "muon" || type_iter->first == "mu_background"){
// 				switch(npreselection){
// 					case 1:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "max_mu_eta", 2.1); 
// 						break;
// 					case 2: 
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_pt", 10);
// 						break;
// 					case 3:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_pt", 15);
// 						break;
// 					case 4:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						break;
// 					case 5:
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_jet_eta"] = 2.4; 
// 						break;
// 					case 6:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 1; 
// 						break;
// 					case 7:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 1; 
// 						break;
// 					case 8:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 2; 
// 						break;
// 					case 9:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 2; 
// 						break;
// 					case 10:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 11:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 12:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 13:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 4; 
// 						break;
// 					case 14:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 4; 
// 						break;
// 					case 15:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_relIso", 0.6);
// 						break;
// 					case 16:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_relIso", 0.8);
// 						break;
// 					case 17:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_relIso", 0.95);
// 						break;
// 					case 18:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_pt", 30);
// 						break;
// 					case 19:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						break;
// 					case 20:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", jet_pt_cuts);
// 						break;
// 					case 21:
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_nisolated_mu"] = 1.0; 
// 						break;
// 					case 22:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_relIso", 0.95);
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_pt", 30);
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "max_e_eta", 2.4); 
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_nisolated_e"] = 0.0; 
// 						break;
// 				}
// 			}else if(type_iter->first == "electron" || type_iter->first == "e_background"){
// 				switch(npreselection){
// 					case 1:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "max_e_eta", 2.4); 
// 						break;
// 					case 2: 
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_pt", 10);
// 						break;
// 					case 3:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_pt", 15);
// 						break;
// 					case 4:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						break;
// 					case 5:
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_jet_eta"] = 2.4; 
// 						break;
// 					case 6:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 1; 
// 						break;
// 					case 7:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 1; 
// 						break;
// 					case 8:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 2; 
// 						break;
// 					case 9:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 2; 
// 						break;
// 					case 10:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 11:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 12:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 13:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 4; 
// 						break;
// 					case 14:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 4; 
// 						break;
// 					case 15:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_relIso", 0.6);
// 						break;
// 					case 16:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_relIso", 0.8);
// 						break;
// 					case 17:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_relIso", 0.95);
// 						break;
// 					case 18:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_pt", 30);
// 						break;
// 					case 19:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						break;
// 					case 20:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", jet_pt_cuts);
// 						break;
// 					case 21:
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_nisolated_e"] = 1.0; 
// 						break;
// 					case 22:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_pt", 30);
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_relIso", 0.95);
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "max_mu_eta", 2.1); 
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_nisolated_mu"] = 0.0; 
// 						break;
// 				}
// 			}
// 			synchronise_maps();
// 		}
// 	}
// }

// void CutSelector::define_cuts_14VIII09()
// {
// 	/*
// 	 *	declare cut vectors
// 	 *	remember to delete them in the destructor!
// 	 */
// 
// 	std::vector<double> *jetcuts100603030 = new std::vector<double>();
// 	jetcuts100603030->push_back(100);
// 	jetcuts100603030->push_back(60);
// 	jetcuts100603030->push_back(30);
// 	jetcuts100603030->push_back(30);
// 	cuts_to_be_deleted.push_back(jetcuts100603030);
// 
// 	std::vector<double> *btag_cuts_3_3 = new std::vector<double>();
// 	btag_cuts_3_3->push_back(3);
// 	btag_cuts_3_3->push_back(3);
// 	cuts_to_be_deleted.push_back(btag_cuts_3_3);
// 
// 	std::vector<double> *btag_cuts_5_5 = new std::vector<double>();
// 	btag_cuts_5_5->push_back(5);
// 	btag_cuts_5_5->push_back(5);
// 	cuts_to_be_deleted.push_back(btag_cuts_5_5);
// 
// 	std::vector<double> *btag_cuts_7_7 = new std::vector<double>();
// 	btag_cuts_7_7->push_back(7);
// 	btag_cuts_7_7->push_back(7);
// 	cuts_to_be_deleted.push_back(btag_cuts_7_7);
// 
// 	/*
// 	 *	MUON CUTS
// 	 */
// 
//  	// 01_cutset: preselection + trigger
//  	vcuts("muon", "01_cutset", "max_mu_d0", -1);
//  	vcuts("muon", "01_cutset", "max_e_d0", -1);
//  	cut_defs["muon"]["01_cutset"]["max_nisolated_e"] = -1;
//  	cut_defs["muon"]["01_cutset"]["max_nisolated_mu"] = -1;
//  	cut_defs["muon"]["01_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["01_cutset"]["min_nisolated_mu"] = -1;
//  	vcuts("muon", "01_cutset", "max_mu_eta", -1);
//  	vcuts("muon", "01_cutset", "max_e_eta", -1);
//  	cut_defs["muon"]["01_cutset"]["max_jet_eta"] = -1; 
//  	vcuts("muon", "01_cutset", "min_jet_pt", -1);	
//  	vcuts("muon", "01_cutset", "min_e_pt", -1);	
//  	vcuts("muon", "01_cutset", "min_mu_pt", -1);	
//  	vcuts("muon", "01_cutset", "max_mu_trackiso", -1);
//  	vcuts("muon", "01_cutset", "max_mu_caliso", -1);
//  	vcuts("muon", "01_cutset", "max_e_trackiso", -1);
//  	vcuts("muon", "01_cutset", "max_e_caliso", -1);
//  	cut_defs["muon"]["01_cutset"]["min_no_jets"] = -1;
//  	cut_defs["muon"]["01_cutset"]["mu_type"] = 0;
// 
//   	// 02_cutset: > 1 muon, pt>20, eta < 2.1; > 4 jets, pt>30, eta < 2.4
//   	vcuts("muon", "02_cutset", "max_mu_d0", -1);
//   	vcuts("muon", "02_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["02_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["02_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["02_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["02_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "02_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "02_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["02_cutset"]["max_jet_eta"] =2.4; 
//   	vcuts("muon", "02_cutset", "min_jet_pt",30.0);	
//   	vcuts("muon", "02_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "02_cutset", "min_mu_pt", 20.0);	
//   	vcuts("muon", "02_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "02_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "02_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "02_cutset", "max_e_caliso", -1);
//   	cut_defs["muon"]["02_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["02_cutset"]["mu_type"] = 0;
// 
//   	// 03_cutset: > 1 muon, pt > 30, eta < 2.1
//   	vcuts("muon", "03_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["03_cutset"]["min_no_jets"] = 4;
//   	vcuts("muon", "03_cutset", "min_mu_pt", 30);	
//   	vcuts("muon", "03_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "03_cutset", "max_mu_d0", 0.02);
//   	vcuts("muon", "03_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["03_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["03_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["03_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["03_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "03_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "03_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["03_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "03_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "03_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "03_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "03_cutset", "max_e_caliso", -1);
//  	cut_defs["muon"]["03_cutset"]["mu_type"] = 0;
// 
//  	// 04_cutset: lepton isolation, exactly 1 lepton
//  	vcuts("muon", "04_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["04_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "04_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "04_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["04_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["04_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["04_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["04_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "04_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "04_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["04_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "04_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "04_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "04_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "04_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "04_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "04_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["04_cutset"]["mu_type"] = 0;
//  
//  	//05_cutset: running Ht cut
//  	vcuts("muon", "05_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["05_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "05_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "05_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["05_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["05_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["05_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["05_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "05_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "05_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["05_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "05_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "05_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "05_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "05_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "05_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "05_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["05_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["muon"]["05_cutset"]["mu_type"] = 0;
// 
// 	// 06_cutset: asymmetric jet cuts
//  	vcuts("muon", "06_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["06_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "06_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "06_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["06_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["06_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["06_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["06_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "06_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "06_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["06_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "06_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "06_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "06_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "06_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "06_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "06_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["06_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["muon"]["06_cutset"]["mu_type"] = 0;
// 
//  	// 07_cutset: asymmetric jet cuts bcut 3 + 11% bjet eff
//  	vcuts("muon", "07_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["07_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "07_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "07_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["07_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["07_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["07_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["07_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "07_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "07_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["07_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "07_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "07_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "07_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "07_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "07_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "07_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["07_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["muon"]["07_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "07_cutset", "min_btag", 1.8);
//  	cut_defs["muon"]["07_cutset"]["mu_type"] = 0;
// 
//  	// 08_cutset: asymmetric jet cuts bcut 3
//  	vcuts("muon", "08_cutset", "min_jet_pt", jetcuts100603030);	
//  	cut_defs["muon"]["08_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "08_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "08_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["08_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["08_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["08_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["08_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "08_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "08_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["08_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "08_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "08_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "08_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "08_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "08_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "08_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["08_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["muon"]["08_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "08_cutset", "min_btag", 3);
//  	cut_defs["muon"]["08_cutset"]["mu_type"] = 0;
// 
//  	// 09_cutset: asymmetric jet cuts bcut 3 - 4.7% lightjet eff
//  	vcuts("muon", "09_cutset", "min_jet_pt", jetcuts100603030);	
//  	cut_defs["muon"]["09_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "09_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "09_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["09_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["09_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["09_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["09_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "09_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "09_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["09_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "09_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "09_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "09_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "09_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "09_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "09_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["09_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["muon"]["09_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "09_cutset", "min_btag", 5);
//  	cut_defs["muon"]["09_cutset"]["mu_type"] = 0;
// 
//  	// 10_cutset: asymmetric jet cuts bcut 2.3 + 11% bjet eff
//  	vcuts("muon", "10_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["10_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "10_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "10_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["10_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["10_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["10_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["10_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "10_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "10_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["10_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "10_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "10_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "10_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "10_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "10_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "10_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["10_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["muon"]["10_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "10_cutset", "min_btag", 1.4);
//  	cut_defs["muon"]["10_cutset"]["mu_type"] = 0;
// 
//  	// 11_cutset: asymmetric jet cuts bcut 2.3
//  	vcuts("muon", "11_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["11_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "11_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "11_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["11_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["11_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["11_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["11_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "11_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "11_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["11_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "11_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "11_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "11_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "11_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "11_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "11_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["11_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["muon"]["11_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "11_cutset", "min_btag", 2.3);
//  	cut_defs["muon"]["11_cutset"]["mu_type"] = 0;
// 
//  	// 12_cutset: asymmetric jet cuts bcut 2.3 - 11% bjet eff
//  	vcuts("muon", "12_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["12_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "12_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "12_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["12_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["12_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["12_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["12_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "12_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "12_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["12_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "12_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "12_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "12_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "12_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "12_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "12_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["12_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["muon"]["12_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "12_cutset", "min_btag", 3.7);
//  	cut_defs["muon"]["12_cutset"]["mu_type"] = 0;
// 
//  	// 13_cutset: Francisco Yumicevas Selection
//  	vcuts("muon", "13_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "13_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "13_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "13_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "13_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "13_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["13_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["13_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "13_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "13_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "13_cutset", "min_e_pt", 30);	
//         vcuts("muon", "13_cutset", "e_electronID", 1.0);
//         vcuts("muon", "13_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "13_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["13_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "13_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["13_cutset"]["min_no_jets"] = 4;
//   	cut_defs["muon"]["13_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["13_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["13_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["13_cutset"]["mu_type"] = 0;
// 
// 
// 	/*
// 	 *	JES - 10 %
// 	 */
// 
//  	// 13_cutset: preselection + trigger
//  	vcuts("muon", "13_cutset", "max_mu_d0", -1);
//  	vcuts("muon", "13_cutset", "max_e_d0", -1);
//  	cut_defs["muon"]["13_cutset"]["max_nisolated_e"] = -1;
//  	cut_defs["muon"]["13_cutset"]["max_nisolated_mu"] = -1;
//  	cut_defs["muon"]["13_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["13_cutset"]["min_nisolated_mu"] = -1;
//  	vcuts("muon", "13_cutset", "max_mu_eta", -1);
//  	vcuts("muon", "13_cutset", "max_e_eta", -1);
//  	cut_defs["muon"]["13_cutset"]["max_jet_eta"] = -1; 
//  	vcuts("muon", "13_cutset", "min_jet_pt", -1);	
//  	vcuts("muon", "13_cutset", "min_e_pt", -1);	
//  	vcuts("muon", "13_cutset", "min_mu_pt", -1);	
//  	vcuts("muon", "13_cutset", "max_mu_trackiso", -1);
//  	vcuts("muon", "13_cutset", "max_mu_caliso", -1);
//  	vcuts("muon", "13_cutset", "max_e_trackiso", -1);
//  	vcuts("muon", "13_cutset", "max_e_caliso", -1);
//  	cut_defs["muon"]["13_cutset"]["min_no_jets"] = -1;
// 	cut_defs["muon"]["13_cutset"]["JES_factor"] = 0.9; 
// 
//   	// 14_cutset: > 1 muon, pt>20, eta < 2.0; > 4 jets, pt>30, eta < 2.4
//   	vcuts("muon", "14_cutset", "max_mu_d0", -1);
//   	vcuts("muon", "14_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["14_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["14_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["14_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["14_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "14_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "14_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["14_cutset"]["max_jet_eta"] =2.4; 
//   	vcuts("muon", "14_cutset", "min_jet_pt",30.0);	
//   	vcuts("muon", "14_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "14_cutset", "min_mu_pt", 20.0);	
//   	vcuts("muon", "14_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "14_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "14_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "14_cutset", "max_e_caliso", -1);
//   	cut_defs["muon"]["14_cutset"]["min_no_jets"] = 4;
// 	cut_defs["muon"]["14_cutset"]["JES_factor"] = 0.9; 
// 
//   	// 15_cutset: > 1 muon, pt > 30, eta < 2.0
//   	vcuts("muon", "15_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["15_cutset"]["min_no_jets"] = 4;
//   	vcuts("muon", "15_cutset", "min_mu_pt", 30);	
//   	vcuts("muon", "15_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "15_cutset", "max_mu_d0", 0.02);
//   	vcuts("muon", "15_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["15_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["15_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["15_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["15_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "15_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "15_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["15_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "15_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "15_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "15_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "15_cutset", "max_e_caliso", -1);
// 	cut_defs["muon"]["15_cutset"]["JES_factor"] = 0.9; 
// 
//  	// 16_cutset: lepton isolation, exactly 1 lepton
//  	vcuts("muon", "16_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["16_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "16_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "16_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["16_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["16_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["16_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["16_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "16_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "16_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["16_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "16_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "16_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "16_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "16_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "16_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "16_cutset", "max_e_d0", 0.02);
// 	cut_defs["muon"]["16_cutset"]["JES_factor"] = 0.9; 
//  
//  	//17_cutset: running Ht cut
//  	vcuts("muon", "17_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["17_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "17_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "17_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["17_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["17_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["17_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["17_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "17_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "17_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["17_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "17_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "17_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "17_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "17_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "17_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "17_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["17_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["17_cutset"]["JES_factor"] = 0.9; 
// 
//   	// 18_cutset: 1 bjet bDiscrim > 1.91
//  	vcuts("muon", "18_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["18_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "18_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "18_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["18_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["18_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["18_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["18_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "18_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "18_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["18_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "18_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "18_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "18_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "18_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "18_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "18_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["18_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "18_cutset", "min_btag", 1.91);
//  	cut_defs["muon"]["18_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["18_cutset"]["JES_factor"] = 0.9; 
// 
//   	// 19_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "19_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["19_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "19_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "19_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["19_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["19_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["19_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["19_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "19_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "19_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["19_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "19_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "19_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "19_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "19_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "19_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "19_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["19_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "19_cutset", "min_btag", 3);
//  	cut_defs["muon"]["19_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["19_cutset"]["JES_factor"] = 0.9; 
// 
//   	// 20_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "20_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["20_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "20_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "20_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["20_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["20_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["20_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["20_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "20_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "20_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["20_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "20_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "20_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "20_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "20_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "20_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "20_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["20_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "20_cutset", "min_btag", 3.78);
//  	cut_defs["muon"]["20_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["20_cutset"]["JES_factor"] = 0.9; 
// 
//  	// 21_cutset: Francisco Yumicevas Selection
//  	vcuts("muon", "21_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "21_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "21_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "21_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "21_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "21_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["21_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["21_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "21_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "21_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "21_cutset", "min_e_pt", 30);	
//         vcuts("muon", "21_cutset", "e_electronID", 1.0);
//         vcuts("muon", "21_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "21_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["21_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "21_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["21_cutset"]["min_no_jets"] = 4;
//   	cut_defs["muon"]["21_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["21_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["21_cutset"]["min_nisolated_e"] = -1;
// 	cut_defs["muon"]["21_cutset"]["JES_factor"] = 0.9; 
// 
//  	// 22_cutset: asymmetric jet cuts
//  	vcuts("muon", "22_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["22_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "22_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "22_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["22_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["22_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["22_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["22_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "22_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "22_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["22_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "22_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "22_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "22_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "22_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "22_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "22_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["22_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["22_cutset"]["JES_factor"] = 0.9; 
// 
//  	// 23_cutset: asymmetric jet cuts
//  	vcuts("muon", "23_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["23_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "23_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "23_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["23_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["23_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["23_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["23_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "23_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "23_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["23_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "23_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "23_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "23_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "23_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "23_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "23_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["23_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "23_cutset", "min_btag", 2);
//  	cut_defs["muon"]["23_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["23_cutset"]["JES_factor"] = 0.9; 
// 
//  	// 24_cutset: asymmetric jet cuts
//  	vcuts("muon", "24_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["24_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "24_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "24_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["24_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["24_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["24_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["24_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "24_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "24_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["24_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "24_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "24_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "24_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "24_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "24_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "24_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["24_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "24_cutset", "min_btag", 3);
//  	cut_defs["muon"]["24_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["24_cutset"]["JES_factor"] = 0.9; 
// 
// 	/*
// 	 *	JES + 10 %
// 	 */
// 
//  	// 25_cutset: preselection + trigger
//  	vcuts("muon", "25_cutset", "max_mu_d0", -1);
//  	vcuts("muon", "25_cutset", "max_e_d0", -1);
//  	cut_defs["muon"]["25_cutset"]["max_nisolated_e"] = -1;
//  	cut_defs["muon"]["25_cutset"]["max_nisolated_mu"] = -1;
//  	cut_defs["muon"]["25_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["25_cutset"]["min_nisolated_mu"] = -1;
//  	vcuts("muon", "25_cutset", "max_mu_eta", -1);
//  	vcuts("muon", "25_cutset", "max_e_eta", -1);
//  	cut_defs["muon"]["25_cutset"]["max_jet_eta"] = -1; 
//  	vcuts("muon", "25_cutset", "min_jet_pt", -1);	
//  	vcuts("muon", "25_cutset", "min_e_pt", -1);	
//  	vcuts("muon", "25_cutset", "min_mu_pt", -1);	
//  	vcuts("muon", "25_cutset", "max_mu_trackiso", -1);
//  	vcuts("muon", "25_cutset", "max_mu_caliso", -1);
//  	vcuts("muon", "25_cutset", "max_e_trackiso", -1);
//  	vcuts("muon", "25_cutset", "max_e_caliso", -1);
//  	cut_defs["muon"]["25_cutset"]["min_no_jets"] = -1;
// 	cut_defs["muon"]["25_cutset"]["JES_factor"] = 1.1; 
// 
//   	// 26_cutset: > 1 muon, pt>20, eta < 2.0; > 4 jets, pt>30, eta < 2.4
//   	vcuts("muon", "26_cutset", "max_mu_d0", -1);
//   	vcuts("muon", "26_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["26_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["26_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["26_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["26_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "26_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "26_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["26_cutset"]["max_jet_eta"] =2.4; 
//   	vcuts("muon", "26_cutset", "min_jet_pt",30.0);	
//   	vcuts("muon", "26_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "26_cutset", "min_mu_pt", 20.0);	
//   	vcuts("muon", "26_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "26_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "26_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "26_cutset", "max_e_caliso", -1);
//   	cut_defs["muon"]["26_cutset"]["min_no_jets"] = 4;
// 	cut_defs["muon"]["26_cutset"]["JES_factor"] = 1.1; 
// 
//   	// 27_cutset: > 1 muon, pt > 30, eta < 2.0
//   	vcuts("muon", "27_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["27_cutset"]["min_no_jets"] = 4;
//   	vcuts("muon", "27_cutset", "min_mu_pt", 30);	
//   	vcuts("muon", "27_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "27_cutset", "max_mu_d0", 0.02);
//   	vcuts("muon", "27_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["27_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["27_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["27_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["27_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "27_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "27_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["27_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "27_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "27_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "27_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "27_cutset", "max_e_caliso", -1);
// 	cut_defs["muon"]["27_cutset"]["JES_factor"] = 1.1; 
// 
//  	// 28_cutset: lepton isolation, exactly 1 lepton
//  	vcuts("muon", "28_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["28_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "28_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "28_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["28_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["28_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["28_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["28_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "28_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "28_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["28_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "28_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "28_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "28_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "28_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "28_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "28_cutset", "max_e_d0", 0.02);
// 	cut_defs["muon"]["28_cutset"]["JES_factor"] = 1.1; 
//  
//  	//29_cutset: running Ht cut
//  	vcuts("muon", "29_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["29_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "29_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "29_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["29_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["29_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["29_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["29_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "29_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "29_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["29_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "29_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "29_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "29_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "29_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "29_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "29_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["29_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["29_cutset"]["JES_factor"] = 1.1; 
// 
//   	// 30_cutset: 1 bjet bDiscrim > 1.91
//  	vcuts("muon", "30_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["30_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "30_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "30_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["30_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["30_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["30_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["30_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "30_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "30_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["30_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "30_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "30_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "30_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "30_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "30_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "30_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["30_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "30_cutset", "min_btag", 1.91);
//  	cut_defs["muon"]["30_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["30_cutset"]["JES_factor"] = 1.1; 
// 
//   	// 31_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "31_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["31_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "31_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "31_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["31_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["31_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["31_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["31_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "31_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "31_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["31_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "31_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "31_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "31_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "31_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "31_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "31_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["31_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "31_cutset", "min_btag", 3);
//  	cut_defs["muon"]["31_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["31_cutset"]["JES_factor"] = 1.1; 
// 
//   	// 32_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "32_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["32_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "32_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "32_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["32_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["32_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["32_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["32_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "32_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "32_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["32_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "32_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "32_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "32_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "32_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "32_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "32_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["32_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "32_cutset", "min_btag", 3.78);
//  	cut_defs["muon"]["32_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["32_cutset"]["JES_factor"] = 1.1; 
// 
//  	// 33_cutset: Francisco Yumicevas Selection
//  	vcuts("muon", "33_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "33_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "33_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "33_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "33_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "33_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["33_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["33_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "33_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "33_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "33_cutset", "min_e_pt", 30);	
//         vcuts("muon", "33_cutset", "e_electronID", 1.0);
//         vcuts("muon", "33_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "33_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["33_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "33_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["33_cutset"]["min_no_jets"] = 4;
//   	cut_defs["muon"]["33_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["33_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["33_cutset"]["min_nisolated_e"] = -1;
// 	cut_defs["muon"]["33_cutset"]["JES_factor"] = 1.1; 
// 
//  	// 34_cutset: asymmetric jet cuts
//  	vcuts("muon", "34_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["34_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "34_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "34_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["34_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["34_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["34_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["34_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "34_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "34_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["34_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "34_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "34_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "34_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "34_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "34_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "34_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["34_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["34_cutset"]["JES_factor"] = 1.1; 
// 
//  	// 35_cutset: asymmetric jet cuts
//  	vcuts("muon", "35_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["35_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "35_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "35_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["35_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["35_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["35_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["35_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "35_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "35_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["35_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "35_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "35_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "35_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "35_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "35_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "35_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["35_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "35_cutset", "min_btag", 2);
//  	cut_defs["muon"]["35_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["35_cutset"]["JES_factor"] = 1.1; 
// 
//  	// 36_cutset: asymmetric jet cuts
//  	vcuts("muon", "36_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["36_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "36_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "36_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["36_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["36_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["36_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["36_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "36_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "36_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["36_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "36_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "36_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "36_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "36_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "36_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "36_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["36_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "36_cutset", "min_btag", 3);
//  	cut_defs["muon"]["36_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["36_cutset"]["JES_factor"] = 1.1; 
// 
// 	/*
// 	 *	JES - 5 %
// 	 */
// 
//  	// 37_cutset: preselection + trigger
//  	vcuts("muon", "37_cutset", "max_mu_d0", -1);
//  	vcuts("muon", "37_cutset", "max_e_d0", -1);
//  	cut_defs["muon"]["37_cutset"]["max_nisolated_e"] = -1;
//  	cut_defs["muon"]["37_cutset"]["max_nisolated_mu"] = -1;
//  	cut_defs["muon"]["37_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["37_cutset"]["min_nisolated_mu"] = -1;
//  	vcuts("muon", "37_cutset", "max_mu_eta", -1);
//  	vcuts("muon", "37_cutset", "max_e_eta", -1);
//  	cut_defs["muon"]["37_cutset"]["max_jet_eta"] = -1; 
//  	vcuts("muon", "37_cutset", "min_jet_pt", -1);	
//  	vcuts("muon", "37_cutset", "min_e_pt", -1);	
//  	vcuts("muon", "37_cutset", "min_mu_pt", -1);	
//  	vcuts("muon", "37_cutset", "max_mu_trackiso", -1);
//  	vcuts("muon", "37_cutset", "max_mu_caliso", -1);
//  	vcuts("muon", "37_cutset", "max_e_trackiso", -1);
//  	vcuts("muon", "37_cutset", "max_e_caliso", -1);
//  	cut_defs["muon"]["37_cutset"]["min_no_jets"] = -1;
// 	cut_defs["muon"]["37_cutset"]["JES_factor"] = 0.95; 
// 
//   	// 38_cutset: > 1 muon, pt>20, eta < 2.0; > 4 jets, pt>30, eta < 2.4
//   	vcuts("muon", "38_cutset", "max_mu_d0", -1);
//   	vcuts("muon", "38_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["38_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["38_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["38_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["38_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "38_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "38_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["38_cutset"]["max_jet_eta"] =2.4; 
//   	vcuts("muon", "38_cutset", "min_jet_pt",30.0);	
//   	vcuts("muon", "38_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "38_cutset", "min_mu_pt", 20.0);	
//   	vcuts("muon", "38_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "38_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "38_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "38_cutset", "max_e_caliso", -1);
//   	cut_defs["muon"]["38_cutset"]["min_no_jets"] = 4;
// 	cut_defs["muon"]["38_cutset"]["JES_factor"] = 0.95; 
// 
//   	// 39_cutset: > 1 muon, pt > 30, eta < 2.0
//   	vcuts("muon", "39_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["39_cutset"]["min_no_jets"] = 4;
//   	vcuts("muon", "39_cutset", "min_mu_pt", 30);	
//   	vcuts("muon", "39_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "39_cutset", "max_mu_d0", 0.02);
//   	vcuts("muon", "39_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["39_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["39_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["39_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["39_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "39_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "39_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["39_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "39_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "39_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "39_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "39_cutset", "max_e_caliso", -1);
// 	cut_defs["muon"]["39_cutset"]["JES_factor"] = 0.95; 
// 
//  	// 40_cutset: lepton isolation, exactly 1 lepton
//  	vcuts("muon", "40_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["40_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "40_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "40_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["40_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["40_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["40_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["40_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "40_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "40_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["40_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "40_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "40_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "40_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "40_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "40_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "40_cutset", "max_e_d0", 0.02);
// 	cut_defs["muon"]["40_cutset"]["JES_factor"] = 0.95; 
//  
//  	//41_cutset: running Ht cut
//  	vcuts("muon", "41_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["41_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "41_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "41_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["41_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["41_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["41_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["41_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "41_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "41_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["41_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "41_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "41_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "41_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "41_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "41_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "41_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["41_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["41_cutset"]["JES_factor"] = 0.95; 
// 
//   	// 42_cutset: 1 bjet bDiscrim > 1.91
//  	vcuts("muon", "42_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["42_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "42_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "42_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["42_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["42_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["42_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["42_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "42_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "42_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["42_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "42_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "42_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "42_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "42_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "42_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "42_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["42_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "42_cutset", "min_btag", 1.91);
//  	cut_defs["muon"]["42_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["42_cutset"]["JES_factor"] = 0.95; 
// 
//   	// 43_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "43_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["43_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "43_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "43_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["43_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["43_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["43_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["43_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "43_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "43_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["43_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "43_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "43_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "43_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "43_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "43_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "43_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["43_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "43_cutset", "min_btag", 3);
//  	cut_defs["muon"]["43_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["43_cutset"]["JES_factor"] = 0.95; 
// 
//   	// 44_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "44_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["44_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "44_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "44_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["44_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["44_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["44_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["44_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "44_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "44_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["44_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "44_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "44_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "44_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "44_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "44_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "44_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["44_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "44_cutset", "min_btag", 3.78);
//  	cut_defs["muon"]["44_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["44_cutset"]["JES_factor"] = 0.95; 
// 
//  	// 45_cutset: Francisco Yumicevas Selection
//  	vcuts("muon", "45_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "45_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "45_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "45_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "45_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "45_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["45_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["45_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "45_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "45_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "45_cutset", "min_e_pt", 30);	
//         vcuts("muon", "45_cutset", "e_electronID", 1.0);
//         vcuts("muon", "45_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "45_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["45_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "45_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["45_cutset"]["min_no_jets"] = 4;
//   	cut_defs["muon"]["45_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["45_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["45_cutset"]["min_nisolated_e"] = -1;
// 	cut_defs["muon"]["45_cutset"]["JES_factor"] = 0.95; 
// 
//  	// 46_cutset: asymmetric jet cuts
//  	vcuts("muon", "46_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["46_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "46_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "46_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["46_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["46_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["46_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["46_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "46_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "46_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["46_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "46_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "46_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "46_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "46_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "46_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "46_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["46_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["46_cutset"]["JES_factor"] = 0.95; 
// 
//  	// 47_cutset: asymmetric jet cuts
//  	vcuts("muon", "47_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["47_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "47_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "47_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["47_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["47_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["47_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["47_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "47_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "47_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["47_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "47_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "47_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "47_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "47_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "47_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "47_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["47_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "47_cutset", "min_btag", 2);
//  	cut_defs["muon"]["47_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["47_cutset"]["JES_factor"] = 0.95; 
// 
//  	// 48_cutset: asymmetric jet cuts
//  	vcuts("muon", "48_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["48_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "48_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "48_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["48_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["48_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["48_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["48_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "48_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "48_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["48_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "48_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "48_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "48_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "48_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "48_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "48_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["48_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "48_cutset", "min_btag", 3);
//  	cut_defs["muon"]["48_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["48_cutset"]["JES_factor"] = 0.95; 
// 
// 	/*
// 	 *	JES + 5 %
// 	 */
// 
//  	// 49_cutset: preselection + trigger
//  	vcuts("muon", "49_cutset", "max_mu_d0", -1);
//  	vcuts("muon", "49_cutset", "max_e_d0", -1);
//  	cut_defs["muon"]["49_cutset"]["max_nisolated_e"] = -1;
//  	cut_defs["muon"]["49_cutset"]["max_nisolated_mu"] = -1;
//  	cut_defs["muon"]["49_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["49_cutset"]["min_nisolated_mu"] = -1;
//  	vcuts("muon", "49_cutset", "max_mu_eta", -1);
//  	vcuts("muon", "49_cutset", "max_e_eta", -1);
//  	cut_defs["muon"]["49_cutset"]["max_jet_eta"] = -1; 
//  	vcuts("muon", "49_cutset", "min_jet_pt", -1);	
//  	vcuts("muon", "49_cutset", "min_e_pt", -1);	
//  	vcuts("muon", "49_cutset", "min_mu_pt", -1);	
//  	vcuts("muon", "49_cutset", "max_mu_trackiso", -1);
//  	vcuts("muon", "49_cutset", "max_mu_caliso", -1);
//  	vcuts("muon", "49_cutset", "max_e_trackiso", -1);
//  	vcuts("muon", "49_cutset", "max_e_caliso", -1);
//  	cut_defs["muon"]["49_cutset"]["min_no_jets"] = -1;
// 	cut_defs["muon"]["49_cutset"]["JES_factor"] = 1.05; 
// 
//   	// 50_cutset: > 1 muon, pt>20, eta < 2.0; > 4 jets, pt>30, eta < 2.4
//   	vcuts("muon", "50_cutset", "max_mu_d0", -1);
//   	vcuts("muon", "50_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["50_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["50_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["50_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["50_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "50_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "50_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["50_cutset"]["max_jet_eta"] =2.4; 
//   	vcuts("muon", "50_cutset", "min_jet_pt",30.0);	
//   	vcuts("muon", "50_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "50_cutset", "min_mu_pt", 20.0);	
//   	vcuts("muon", "50_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "50_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "50_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "50_cutset", "max_e_caliso", -1);
//   	cut_defs["muon"]["50_cutset"]["min_no_jets"] = 4;
// 	cut_defs["muon"]["50_cutset"]["JES_factor"] = 1.05; 
// 
//   	// 51_cutset: > 1 muon, pt > 30, eta < 2.0
//   	vcuts("muon", "51_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["51_cutset"]["min_no_jets"] = 4;
//   	vcuts("muon", "51_cutset", "min_mu_pt", 30);	
//   	vcuts("muon", "51_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "51_cutset", "max_mu_d0", 0.02);
//   	vcuts("muon", "51_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["51_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["51_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["51_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["51_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "51_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "51_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["51_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "51_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "51_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "51_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "51_cutset", "max_e_caliso", -1);
// 	cut_defs["muon"]["51_cutset"]["JES_factor"] = 1.05; 
// 
//  	// 52_cutset: lepton isolation, exactly 1 lepton
//  	vcuts("muon", "52_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["52_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "52_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "52_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["52_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["52_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["52_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["52_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "52_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "52_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["52_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "52_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "52_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "52_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "52_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "52_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "52_cutset", "max_e_d0", 0.02);
// 	cut_defs["muon"]["52_cutset"]["JES_factor"] = 1.05; 
//  
//  	//53_cutset: running Ht cut
//  	vcuts("muon", "53_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["53_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "53_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "53_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["53_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["53_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["53_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["53_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "53_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "53_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["53_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "53_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "53_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "53_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "53_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "53_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "53_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["53_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["53_cutset"]["JES_factor"] = 1.05; 
// 
//   	// 54_cutset: 1 bjet bDiscrim > 1.91
//  	vcuts("muon", "54_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["54_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "54_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "54_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["54_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["54_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["54_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["54_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "54_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "54_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["54_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "54_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "54_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "54_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "54_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "54_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "54_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["54_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "54_cutset", "min_btag", 1.91);
//  	cut_defs["muon"]["54_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["54_cutset"]["JES_factor"] = 1.05; 
// 
//   	// 55_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "55_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["55_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "55_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "55_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["55_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["55_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["55_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["55_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "55_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "55_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["55_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "55_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "55_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "55_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "55_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "55_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "55_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["55_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "55_cutset", "min_btag", 3);
//  	cut_defs["muon"]["55_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["55_cutset"]["JES_factor"] = 1.05; 
// 
//   	// 56_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "56_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["56_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "56_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "56_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["56_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["56_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["56_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["56_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "56_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "56_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["56_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "56_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "56_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "56_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "56_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "56_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "56_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["56_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "56_cutset", "min_btag", 3.78);
//  	cut_defs["muon"]["56_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["56_cutset"]["JES_factor"] = 1.05; 
// 
//  	// 57_cutset: Francisco Yumicevas Selection
//  	vcuts("muon", "57_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "57_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "57_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "57_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "57_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "57_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["57_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["57_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "57_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "57_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "57_cutset", "min_e_pt", 30);	
//         vcuts("muon", "57_cutset", "e_electronID", 1.0);
//         vcuts("muon", "57_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "57_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["57_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "57_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["57_cutset"]["min_no_jets"] = 4;
//   	cut_defs["muon"]["57_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["57_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["57_cutset"]["min_nisolated_e"] = -1;
// 	cut_defs["muon"]["57_cutset"]["JES_factor"] = 1.05; 
// 
//  	// 58_cutset: asymmetric jet cuts
//  	vcuts("muon", "58_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["58_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "58_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "58_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["58_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["58_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["58_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["58_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "58_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "58_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["58_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "58_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "58_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "58_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "58_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "58_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "58_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["58_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["58_cutset"]["JES_factor"] = 1.05; 
// 
//  	// 59_cutset: asymmetric jet cuts
//  	vcuts("muon", "59_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["59_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "59_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "59_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["59_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["59_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["59_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["59_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "59_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "59_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["59_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "59_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "59_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "59_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "59_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "59_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "59_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["59_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "59_cutset", "min_btag", 2);
//  	cut_defs["muon"]["59_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["59_cutset"]["JES_factor"] = 1.05; 
// 
//  	// 60_cutset: asymmetric jet cuts
//  	vcuts("muon", "60_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["60_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "60_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "60_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["60_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["60_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["60_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["60_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "60_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "60_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["60_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "60_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "60_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "60_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "60_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "60_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "60_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["60_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "60_cutset", "min_btag", 3);
//  	cut_defs["muon"]["60_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["60_cutset"]["JES_factor"] = 1.05; 
// 
// 	/*
// 	 *	JES - 2 %
// 	 */
// 
//  	// 61_cutset: preselection + trigger
//  	vcuts("muon", "61_cutset", "max_mu_d0", -1);
//  	vcuts("muon", "61_cutset", "max_e_d0", -1);
//  	cut_defs["muon"]["61_cutset"]["max_nisolated_e"] = -1;
//  	cut_defs["muon"]["61_cutset"]["max_nisolated_mu"] = -1;
//  	cut_defs["muon"]["61_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["61_cutset"]["min_nisolated_mu"] = -1;
//  	vcuts("muon", "61_cutset", "max_mu_eta", -1);
//  	vcuts("muon", "61_cutset", "max_e_eta", -1);
//  	cut_defs["muon"]["61_cutset"]["max_jet_eta"] = -1; 
//  	vcuts("muon", "61_cutset", "min_jet_pt", -1);	
//  	vcuts("muon", "61_cutset", "min_e_pt", -1);	
//  	vcuts("muon", "61_cutset", "min_mu_pt", -1);	
//  	vcuts("muon", "61_cutset", "max_mu_trackiso", -1);
//  	vcuts("muon", "61_cutset", "max_mu_caliso", -1);
//  	vcuts("muon", "61_cutset", "max_e_trackiso", -1);
//  	vcuts("muon", "61_cutset", "max_e_caliso", -1);
//  	cut_defs["muon"]["61_cutset"]["min_no_jets"] = -1;
// 	cut_defs["muon"]["61_cutset"]["JES_factor"] = 0.98; 
// 
//   	// 62_cutset: > 1 muon, pt>20, eta < 2.0; > 4 jets, pt>30, eta < 2.4
//   	vcuts("muon", "62_cutset", "max_mu_d0", -1);
//   	vcuts("muon", "62_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["62_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["62_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["62_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["62_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "62_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "62_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["62_cutset"]["max_jet_eta"] =2.4; 
//   	vcuts("muon", "62_cutset", "min_jet_pt",30.0);	
//   	vcuts("muon", "62_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "62_cutset", "min_mu_pt", 20.0);	
//   	vcuts("muon", "62_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "62_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "62_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "62_cutset", "max_e_caliso", -1);
//   	cut_defs["muon"]["62_cutset"]["min_no_jets"] = 4;
// 	cut_defs["muon"]["62_cutset"]["JES_factor"] = 0.98; 
// 
//   	// 63_cutset: > 1 muon, pt > 30, eta < 2.0
//   	vcuts("muon", "63_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["63_cutset"]["min_no_jets"] = 4;
//   	vcuts("muon", "63_cutset", "min_mu_pt", 30);	
//   	vcuts("muon", "63_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "63_cutset", "max_mu_d0", 0.02);
//   	vcuts("muon", "63_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["63_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["63_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["63_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["63_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "63_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "63_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["63_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "63_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "63_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "63_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "63_cutset", "max_e_caliso", -1);
// 	cut_defs["muon"]["63_cutset"]["JES_factor"] = 0.98; 
// 
//  	// 64_cutset: lepton isolation, exactly 1 lepton
//  	vcuts("muon", "64_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["64_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "64_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "64_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["64_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["64_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["64_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["64_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "64_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "64_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["64_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "64_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "64_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "64_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "64_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "64_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "64_cutset", "max_e_d0", 0.02);
// 	cut_defs["muon"]["64_cutset"]["JES_factor"] = 0.98; 
//  
//  	//65_cutset: running Ht cut
//  	vcuts("muon", "65_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["65_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "65_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "65_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["65_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["65_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["65_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["65_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "65_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "65_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["65_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "65_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "65_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "65_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "65_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "65_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "65_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["65_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["65_cutset"]["JES_factor"] = 0.98; 
// 
//   	// 66_cutset: 1 bjet bDiscrim > 1.91
//  	vcuts("muon", "66_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["66_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "66_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "66_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["66_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["66_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["66_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["66_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "66_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "66_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["66_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "66_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "66_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "66_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "66_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "66_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "66_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["66_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "66_cutset", "min_btag", 1.91);
//  	cut_defs["muon"]["66_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["66_cutset"]["JES_factor"] = 0.98; 
// 
//   	// 67_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "67_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["67_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "67_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "67_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["67_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["67_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["67_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["67_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "67_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "67_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["67_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "67_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "67_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "67_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "67_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "67_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "67_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["67_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "67_cutset", "min_btag", 3);
//  	cut_defs["muon"]["67_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["67_cutset"]["JES_factor"] = 0.98; 
// 
//   	// 68_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "68_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["68_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "68_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "68_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["68_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["68_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["68_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["68_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "68_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "68_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["68_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "68_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "68_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "68_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "68_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "68_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "68_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["68_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "68_cutset", "min_btag", 3.78);
//  	cut_defs["muon"]["68_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["68_cutset"]["JES_factor"] = 0.98; 
// 
//  	// 69_cutset: Francisco Yumicevas Selection
//  	vcuts("muon", "69_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "69_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "69_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "69_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "69_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "69_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["69_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["69_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "69_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "69_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "69_cutset", "min_e_pt", 30);	
//         vcuts("muon", "69_cutset", "e_electronID", 1.0);
//         vcuts("muon", "69_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "69_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["69_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "69_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["69_cutset"]["min_no_jets"] = 4;
//   	cut_defs["muon"]["69_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["69_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["69_cutset"]["min_nisolated_e"] = -1;
// 	cut_defs["muon"]["69_cutset"]["JES_factor"] = 0.98; 
// 
//  	// 70_cutset: asymmetric jet cuts
//  	vcuts("muon", "70_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["70_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "70_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "70_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["70_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["70_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["70_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["70_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "70_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "70_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["70_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "70_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "70_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "70_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "70_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "70_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "70_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["70_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["70_cutset"]["JES_factor"] = 0.98; 
// 
//  	// 71_cutset: asymmetric jet cuts
//  	vcuts("muon", "71_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["71_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "71_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "71_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["71_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["71_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["71_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["71_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "71_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "71_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["71_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "71_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "71_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "71_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "71_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "71_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "71_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["71_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "71_cutset", "min_btag", 2);
//  	cut_defs["muon"]["71_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["71_cutset"]["JES_factor"] = 0.98; 
// 
//  	// 72_cutset: asymmetric jet cuts
//  	vcuts("muon", "72_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["72_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "72_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "72_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["72_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["72_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["72_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["72_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "72_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "72_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["72_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "72_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "72_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "72_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "72_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "72_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "72_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["72_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "72_cutset", "min_btag", 3);
//  	cut_defs["muon"]["72_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["72_cutset"]["JES_factor"] = 0.98; 
// 
// 	/*
// 	 *	JES + 5 %
// 	 */
// 
//  	// 73_cutset: preselection + trigger
//  	vcuts("muon", "73_cutset", "max_mu_d0", -1);
//  	vcuts("muon", "73_cutset", "max_e_d0", -1);
//  	cut_defs["muon"]["73_cutset"]["max_nisolated_e"] = -1;
//  	cut_defs["muon"]["73_cutset"]["max_nisolated_mu"] = -1;
//  	cut_defs["muon"]["73_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["73_cutset"]["min_nisolated_mu"] = -1;
//  	vcuts("muon", "73_cutset", "max_mu_eta", -1);
//  	vcuts("muon", "73_cutset", "max_e_eta", -1);
//  	cut_defs["muon"]["73_cutset"]["max_jet_eta"] = -1; 
//  	vcuts("muon", "73_cutset", "min_jet_pt", -1);	
//  	vcuts("muon", "73_cutset", "min_e_pt", -1);	
//  	vcuts("muon", "73_cutset", "min_mu_pt", -1);	
//  	vcuts("muon", "73_cutset", "max_mu_trackiso", -1);
//  	vcuts("muon", "73_cutset", "max_mu_caliso", -1);
//  	vcuts("muon", "73_cutset", "max_e_trackiso", -1);
//  	vcuts("muon", "73_cutset", "max_e_caliso", -1);
//  	cut_defs["muon"]["73_cutset"]["min_no_jets"] = -1;
// 	cut_defs["muon"]["73_cutset"]["JES_factor"] = 1.02; 
// 
//   	// 74_cutset: > 1 muon, pt>20, eta < 2.0; > 4 jets, pt>30, eta < 2.4
//   	vcuts("muon", "74_cutset", "max_mu_d0", -1);
//   	vcuts("muon", "74_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["74_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["74_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["74_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["74_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "74_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "74_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["74_cutset"]["max_jet_eta"] =2.4; 
//   	vcuts("muon", "74_cutset", "min_jet_pt",30.0);	
//   	vcuts("muon", "74_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "74_cutset", "min_mu_pt", 20.0);	
//   	vcuts("muon", "74_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "74_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "74_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "74_cutset", "max_e_caliso", -1);
//   	cut_defs["muon"]["74_cutset"]["min_no_jets"] = 4;
// 	cut_defs["muon"]["74_cutset"]["JES_factor"] = 1.02; 
// 
//   	// 75_cutset: > 1 muon, pt > 30, eta < 2.0
//   	vcuts("muon", "75_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["75_cutset"]["min_no_jets"] = 4;
//   	vcuts("muon", "75_cutset", "min_mu_pt", 30);	
//   	vcuts("muon", "75_cutset", "min_e_pt", -1);	
//   	vcuts("muon", "75_cutset", "max_mu_d0", 0.02);
//   	vcuts("muon", "75_cutset", "max_e_d0", -1);
//   	cut_defs["muon"]["75_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["muon"]["75_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["muon"]["75_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["75_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("muon", "75_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "75_cutset", "max_e_eta", -1);
//   	cut_defs["muon"]["75_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "75_cutset", "max_mu_trackiso", -1);
//   	vcuts("muon", "75_cutset", "max_mu_caliso", -1);
//   	vcuts("muon", "75_cutset", "max_e_trackiso", -1);
//   	vcuts("muon", "75_cutset", "max_e_caliso", -1);
// 	cut_defs["muon"]["75_cutset"]["JES_factor"] = 1.02; 
// 
//  	// 76_cutset: lepton isolation, exactly 1 lepton
//  	vcuts("muon", "76_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["76_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "76_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "76_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["76_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["76_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["76_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["76_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "76_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "76_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["76_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "76_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "76_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "76_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "76_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "76_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "76_cutset", "max_e_d0", 0.02);
// 	cut_defs["muon"]["76_cutset"]["JES_factor"] = 1.02; 
//  
//  	//77_cutset: running Ht cut
//  	vcuts("muon", "77_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["77_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "77_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "77_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["77_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["77_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["77_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["77_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "77_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "77_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["77_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "77_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "77_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "77_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "77_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "77_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "77_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["77_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["77_cutset"]["JES_factor"] = 1.02; 
// 
//   	// 78_cutset: 1 bjet bDiscrim > 1.91
//  	vcuts("muon", "78_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["78_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "78_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "78_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["78_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["78_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["78_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["78_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "78_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "78_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["78_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "78_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "78_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "78_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "78_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "78_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "78_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["78_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "78_cutset", "min_btag", 1.91);
//  	cut_defs["muon"]["78_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["78_cutset"]["JES_factor"] = 1.02; 
// 
//   	// 79_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "79_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["79_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "79_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "79_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["79_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["79_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["79_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["79_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "79_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "79_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["79_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "79_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "79_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "79_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "79_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "79_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "79_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["79_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "79_cutset", "min_btag", 3);
//  	cut_defs["muon"]["79_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["79_cutset"]["JES_factor"] = 1.02; 
// 
//   	// 80_cutset: 1 bjet bDiscrim > 3
//  	vcuts("muon", "80_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["80_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "80_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "80_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["80_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["80_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["80_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["80_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "80_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "80_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["80_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "80_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "80_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "80_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "80_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "80_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "80_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["80_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "80_cutset", "min_btag", 3.78);
//  	cut_defs["muon"]["80_cutset"]["min_ht"] = get_ht_cut();
// 	cut_defs["muon"]["80_cutset"]["JES_factor"] = 1.02; 
// 
//  	// 81_cutset: Francisco Yumicevas Selection
//  	vcuts("muon", "81_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "81_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "81_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "81_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "81_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "81_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["81_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["81_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "81_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "81_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "81_cutset", "min_e_pt", 30);	
//         vcuts("muon", "81_cutset", "e_electronID", 1.0);
//         vcuts("muon", "81_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "81_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["81_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "81_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["81_cutset"]["min_no_jets"] = 4;
//   	cut_defs["muon"]["81_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["81_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["81_cutset"]["min_nisolated_e"] = -1;
// 	cut_defs["muon"]["81_cutset"]["JES_factor"] = 1.02; 
// 
//  	// 82_cutset: asymmetric jet cuts
//  	vcuts("muon", "82_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["82_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "82_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "82_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["82_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["82_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["82_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["82_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "82_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "82_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["82_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "82_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "82_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "82_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "82_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "82_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "82_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["82_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["82_cutset"]["JES_factor"] = 1.02; 
// 
//  	// 83_cutset: asymmetric jet cuts
//  	vcuts("muon", "83_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["83_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "83_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "83_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["83_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["83_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["83_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["83_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "83_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "83_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["83_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "83_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "83_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "83_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "83_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "83_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "83_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["83_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "83_cutset", "min_btag", 2);
//  	cut_defs["muon"]["83_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["83_cutset"]["JES_factor"] = 1.02; 
// 
//  	// 84_cutset: asymmetric jet cuts
//  	vcuts("muon", "84_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["muon"]["84_cutset"]["min_no_jets"] = 4;
//  	vcuts("muon", "84_cutset", "min_mu_pt", 30);	
//  	vcuts("muon", "84_cutset", "min_e_pt", 30);	
//  	cut_defs["muon"]["84_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["84_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["84_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["muon"]["84_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "84_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "84_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["84_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "84_cutset", "max_mu_trackiso", 3);
//  	vcuts("muon", "84_cutset", "max_mu_caliso", 1);
//  	vcuts("muon", "84_cutset", "max_e_trackiso", 5);
//  	vcuts("muon", "84_cutset", "max_e_caliso", 10);
//  	vcuts("muon", "84_cutset", "max_mu_d0", 0.02);
//  	vcuts("muon", "84_cutset", "max_e_d0", 0.02);
//  	cut_defs["muon"]["84_cutset"]["name_btag"] = 4; 
//  	vcuts("muon", "84_cutset", "min_btag", 3);
//  	cut_defs["muon"]["84_cutset"]["min_ht"] = 400.0;
// 	cut_defs["muon"]["84_cutset"]["JES_factor"] = 1.02; 
// 
         /*
	 *	MUON BACKGROUND CUTS
         */

//  	// 01_cutset: preselection + trigger
//  	vcuts("mu_background", "01_cutset", "max_mu_d0", -1);
//  	vcuts("mu_background", "01_cutset", "max_e_d0", -1);
//  	cut_defs["mu_background"]["01_cutset"]["max_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["01_cutset"]["max_nisolated_mu"] = -1;
//  	cut_defs["mu_background"]["01_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["01_cutset"]["min_nisolated_mu"] = -1;
//  	vcuts("mu_background", "01_cutset", "max_mu_eta", -1);
//  	vcuts("mu_background", "01_cutset", "max_e_eta", -1);
//  	cut_defs["mu_background"]["01_cutset"]["max_jet_eta"] = -1; 
//  	vcuts("mu_background", "01_cutset", "min_jet_pt", -1);	
//  	vcuts("mu_background", "01_cutset", "min_e_pt", -1);	
//  	vcuts("mu_background", "01_cutset", "min_mu_pt", -1);	
//  	vcuts("mu_background", "01_cutset", "max_mu_trackiso", -1);
//  	vcuts("mu_background", "01_cutset", "max_mu_caliso", -1);
//  	vcuts("mu_background", "01_cutset", "max_e_trackiso", -1);
//  	vcuts("mu_background", "01_cutset", "max_e_caliso", -1);
//  	cut_defs["mu_background"]["01_cutset"]["min_no_jets"] = -1;
//  	cut_defs["mu_background"]["01_cutset"]["mu_type"] = 0;
// 
//   	// 02_cutset: > 1 mu_background, pt>20, eta < 2.0; > 4 jets, pt>30, eta < 2.4
//   	vcuts("mu_background", "02_cutset", "max_mu_d0", -1);
//   	vcuts("mu_background", "02_cutset", "max_e_d0", -1);
//   	cut_defs["mu_background"]["02_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["02_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["mu_background"]["02_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["02_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("mu_background", "02_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "02_cutset", "max_e_eta", -1);
//   	cut_defs["mu_background"]["02_cutset"]["max_jet_eta"] =2.4; 
//   	vcuts("mu_background", "02_cutset", "min_jet_pt",30.0);	
//   	vcuts("mu_background", "02_cutset", "min_e_pt", -1);	
//   	vcuts("mu_background", "02_cutset", "min_mu_pt", 20.0);	
//   	vcuts("mu_background", "02_cutset", "max_mu_trackiso", -1);
//   	vcuts("mu_background", "02_cutset", "max_mu_caliso", -1);
//   	vcuts("mu_background", "02_cutset", "max_e_trackiso", -1);
//   	vcuts("mu_background", "02_cutset", "max_e_caliso", -1);
//   	cut_defs["mu_background"]["02_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["02_cutset"]["mu_type"] = 0;
// 
//   	// 03_cutset: > 1 mu_background, pt > 30, eta < 2.0
//   	vcuts("mu_background", "03_cutset", "min_jet_pt",30.0);	
//   	cut_defs["mu_background"]["03_cutset"]["min_no_jets"] = 4;
//   	vcuts("mu_background", "03_cutset", "min_mu_pt", 30);	
//   	vcuts("mu_background", "03_cutset", "min_e_pt", -1);	
//   	vcuts("mu_background", "03_cutset", "max_mu_d0", 0.02);
//   	vcuts("mu_background", "03_cutset", "max_e_d0", -1);
//   	cut_defs["mu_background"]["03_cutset"]["max_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["03_cutset"]["max_nisolated_mu"] = -1;
//   	cut_defs["mu_background"]["03_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["03_cutset"]["min_nisolated_mu"] = 1;
//   	vcuts("mu_background", "03_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "03_cutset", "max_e_eta", -1);
//   	cut_defs["mu_background"]["03_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("mu_background", "03_cutset", "max_mu_trackiso", -1);
//   	vcuts("mu_background", "03_cutset", "max_mu_caliso", -1);
//   	vcuts("mu_background", "03_cutset", "max_e_trackiso", -1);
//   	vcuts("mu_background", "03_cutset", "max_e_caliso", -1);
//  	cut_defs["mu_background"]["03_cutset"]["mu_type"] = 0;
// 
//  	// 04_cutset: lepton isolation, exactly 1 lepton
//  	vcuts("mu_background", "04_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["04_cutset"]["min_no_jets"] = 4;
//  	vcuts("mu_background", "04_cutset", "min_mu_pt", 30);	
//  	vcuts("mu_background", "04_cutset", "min_e_pt", 30);	
//  	cut_defs["mu_background"]["04_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["04_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["04_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["04_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "04_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "04_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["04_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "04_cutset", "max_mu_trackiso", 3);
//  	vcuts("mu_background", "04_cutset", "max_mu_caliso", 1);
//  	vcuts("mu_background", "04_cutset", "max_e_trackiso", 5);
//  	vcuts("mu_background", "04_cutset", "max_e_caliso", 10);
//  	vcuts("mu_background", "04_cutset", "max_mu_d0", 0.02);
//  	vcuts("mu_background", "04_cutset", "max_e_d0", 0.02);
//  	cut_defs["mu_background"]["04_cutset"]["mu_type"] = 0;
//  
//  	//05_cutset: running Ht cut
//  	vcuts("mu_background", "05_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["05_cutset"]["min_no_jets"] = 4;
//  	vcuts("mu_background", "05_cutset", "min_mu_pt", 30);	
//  	vcuts("mu_background", "05_cutset", "min_e_pt", 30);	
//  	cut_defs["mu_background"]["05_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["05_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["05_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["05_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "05_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "05_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["05_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "05_cutset", "max_mu_trackiso", 3);
//  	vcuts("mu_background", "05_cutset", "max_mu_caliso", 1);
//  	vcuts("mu_background", "05_cutset", "max_e_trackiso", 5);
//  	vcuts("mu_background", "05_cutset", "max_e_caliso", 10);
//  	vcuts("mu_background", "05_cutset", "max_mu_d0", 0.02);
//  	vcuts("mu_background", "05_cutset", "max_e_d0", 0.02);
//  	cut_defs["mu_background"]["05_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["mu_background"]["05_cutset"]["mu_type"] = 0;
// 
// 	// 06_cutset: asymmetric jet cuts
//  	vcuts("mu_background", "06_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["mu_background"]["06_cutset"]["min_no_jets"] = 4;
//  	vcuts("mu_background", "06_cutset", "min_mu_pt", 30);	
//  	vcuts("mu_background", "06_cutset", "min_e_pt", 30);	
//  	cut_defs["mu_background"]["06_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["06_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["06_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["06_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "06_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "06_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["06_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "06_cutset", "max_mu_trackiso", 3);
//  	vcuts("mu_background", "06_cutset", "max_mu_caliso", 1);
//  	vcuts("mu_background", "06_cutset", "max_e_trackiso", 5);
//  	vcuts("mu_background", "06_cutset", "max_e_caliso", 10);
//  	vcuts("mu_background", "06_cutset", "max_mu_d0", 0.02);
//  	vcuts("mu_background", "06_cutset", "max_e_d0", 0.02);
//  	cut_defs["mu_background"]["06_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["mu_background"]["06_cutset"]["mu_type"] = 0;
// 
//  	// 07_cutset: asymmetric jet cuts bcut 3 + 11% bjet eff
//  	vcuts("mu_background", "07_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["mu_background"]["07_cutset"]["min_no_jets"] = 4;
//  	vcuts("mu_background", "07_cutset", "min_mu_pt", 30);	
//  	vcuts("mu_background", "07_cutset", "min_e_pt", 30);	
//  	cut_defs["mu_background"]["07_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["07_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["07_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["07_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "07_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "07_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["07_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "07_cutset", "max_mu_trackiso", 3);
//  	vcuts("mu_background", "07_cutset", "max_mu_caliso", 1);
//  	vcuts("mu_background", "07_cutset", "max_e_trackiso", 5);
//  	vcuts("mu_background", "07_cutset", "max_e_caliso", 10);
//  	vcuts("mu_background", "07_cutset", "max_mu_d0", 0.02);
//  	vcuts("mu_background", "07_cutset", "max_e_d0", 0.02);
//  	cut_defs["mu_background"]["07_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["mu_background"]["07_cutset"]["name_btag"] = 4; 
//  	vcuts("mu_background", "07_cutset", "min_btag", 1.8);
//  	cut_defs["mu_background"]["07_cutset"]["mu_type"] = 0;
// 
//  	// 08_cutset: asymmetric jet cuts bcut 3
//  	vcuts("mu_background", "08_cutset", "min_jet_pt", jetcuts100603030);	
//  	cut_defs["mu_background"]["08_cutset"]["min_no_jets"] = 4;
//  	vcuts("mu_background", "08_cutset", "min_mu_pt", 30);	
//  	vcuts("mu_background", "08_cutset", "min_e_pt", 30);	
//  	cut_defs["mu_background"]["08_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["08_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["08_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["08_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "08_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "08_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["08_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "08_cutset", "max_mu_trackiso", 3);
//  	vcuts("mu_background", "08_cutset", "max_mu_caliso", 1);
//  	vcuts("mu_background", "08_cutset", "max_e_trackiso", 5);
//  	vcuts("mu_background", "08_cutset", "max_e_caliso", 10);
//  	vcuts("mu_background", "08_cutset", "max_mu_d0", 0.02);
//  	vcuts("mu_background", "08_cutset", "max_e_d0", 0.02);
//  	cut_defs["mu_background"]["08_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["mu_background"]["08_cutset"]["name_btag"] = 4; 
//  	vcuts("mu_background", "08_cutset", "min_btag", 3);
//  	cut_defs["mu_background"]["08_cutset"]["mu_type"] = 0;
// 
//  	// 09_cutset: asymmetric jet cuts bcut 3 - 4.7% lightjet eff
//  	vcuts("mu_background", "09_cutset", "min_jet_pt", jetcuts100603030);	
//  	cut_defs["mu_background"]["09_cutset"]["min_no_jets"] = 4;
//  	vcuts("mu_background", "09_cutset", "min_mu_pt", 30);	
//  	vcuts("mu_background", "09_cutset", "min_e_pt", 30);	
//  	cut_defs["mu_background"]["09_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["09_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["09_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["09_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "09_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "09_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["09_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "09_cutset", "max_mu_trackiso", 3);
//  	vcuts("mu_background", "09_cutset", "max_mu_caliso", 1);
//  	vcuts("mu_background", "09_cutset", "max_e_trackiso", 5);
//  	vcuts("mu_background", "09_cutset", "max_e_caliso", 10);
//  	vcuts("mu_background", "09_cutset", "max_mu_d0", 0.02);
//  	vcuts("mu_background", "09_cutset", "max_e_d0", 0.02);
//  	cut_defs["mu_background"]["09_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["mu_background"]["09_cutset"]["name_btag"] = 4; 
//  	vcuts("mu_background", "09_cutset", "min_btag", 5);
//  	cut_defs["mu_background"]["09_cutset"]["mu_type"] = 0;
// 
//  	// 10_cutset: asymmetric jet cuts bcut 2.3 + 11% bjet eff
//  	vcuts("mu_background", "10_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["mu_background"]["10_cutset"]["min_no_jets"] = 4;
//  	vcuts("mu_background", "10_cutset", "min_mu_pt", 30);	
//  	vcuts("mu_background", "10_cutset", "min_e_pt", 30);	
//  	cut_defs["mu_background"]["10_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["10_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["10_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["10_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "10_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "10_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["10_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "10_cutset", "max_mu_trackiso", 3);
//  	vcuts("mu_background", "10_cutset", "max_mu_caliso", 1);
//  	vcuts("mu_background", "10_cutset", "max_e_trackiso", 5);
//  	vcuts("mu_background", "10_cutset", "max_e_caliso", 10);
//  	vcuts("mu_background", "10_cutset", "max_mu_d0", 0.02);
//  	vcuts("mu_background", "10_cutset", "max_e_d0", 0.02);
//  	cut_defs["mu_background"]["10_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["mu_background"]["10_cutset"]["name_btag"] = 4; 
//  	vcuts("mu_background", "10_cutset", "min_btag", 1.4);
//  	cut_defs["mu_background"]["10_cutset"]["mu_type"] = 0;
// 
//  	// 11_cutset: asymmetric jet cuts bcut 2.3
//  	vcuts("mu_background", "11_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["mu_background"]["11_cutset"]["min_no_jets"] = 4;
//  	vcuts("mu_background", "11_cutset", "min_mu_pt", 30);	
//  	vcuts("mu_background", "11_cutset", "min_e_pt", 30);	
//  	cut_defs["mu_background"]["11_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["11_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["11_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["11_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "11_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "11_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["11_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "11_cutset", "max_mu_trackiso", 3);
//  	vcuts("mu_background", "11_cutset", "max_mu_caliso", 1);
//  	vcuts("mu_background", "11_cutset", "max_e_trackiso", 5);
//  	vcuts("mu_background", "11_cutset", "max_e_caliso", 10);
//  	vcuts("mu_background", "11_cutset", "max_mu_d0", 0.02);
//  	vcuts("mu_background", "11_cutset", "max_e_d0", 0.02);
//  	cut_defs["mu_background"]["11_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["mu_background"]["11_cutset"]["name_btag"] = 4; 
//  	vcuts("mu_background", "11_cutset", "min_btag", 2.3);
//  	cut_defs["mu_background"]["11_cutset"]["mu_type"] = 0;
// 
//  	// 12_cutset: asymmetric jet cuts bcut 2.3 - 11% bjet eff
//  	vcuts("mu_background", "12_cutset", "min_jet_pt", jetcuts100603030);
//  	cut_defs["mu_background"]["12_cutset"]["min_no_jets"] = 4;
//  	vcuts("mu_background", "12_cutset", "min_mu_pt", 30);	
//  	vcuts("mu_background", "12_cutset", "min_e_pt", 30);	
//  	cut_defs["mu_background"]["12_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["12_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["12_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["12_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "12_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "12_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["12_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "12_cutset", "max_mu_trackiso", 3);
//  	vcuts("mu_background", "12_cutset", "max_mu_caliso", 1);
//  	vcuts("mu_background", "12_cutset", "max_e_trackiso", 5);
//  	vcuts("mu_background", "12_cutset", "max_e_caliso", 10);
//  	vcuts("mu_background", "12_cutset", "max_mu_d0", 0.02);
//  	vcuts("mu_background", "12_cutset", "max_e_d0", 0.02);
//  	cut_defs["mu_background"]["12_cutset"]["min_ht"] = get_ht_cut();
//  	cut_defs["mu_background"]["12_cutset"]["name_btag"] = 4; 
//  	vcuts("mu_background", "12_cutset", "min_btag", 3.7);
//  	cut_defs["mu_background"]["12_cutset"]["mu_type"] = 0;
// 
//  	// 13_cutset: Francisco Yumicevas Selection
//  	vcuts("mu_background", "13_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "13_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "13_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "13_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "13_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "13_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["13_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["13_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "13_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("mu_background", "13_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("mu_background", "13_cutset", "min_e_pt", 30);	
//         vcuts("mu_background", "13_cutset", "e_electronID", 1.0);
//         vcuts("mu_background", "13_cutset", "min_e_relIso", 0.9);
//   	vcuts("mu_background", "13_cutset", "max_e_eta", 2.4);
//   	cut_defs["mu_background"]["13_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("mu_background", "13_cutset", "min_jet_pt",30.0);	
//   	cut_defs["mu_background"]["13_cutset"]["min_no_jets"] = 4;
//   	cut_defs["mu_background"]["13_cutset"]["max_no_jets"] = 7;
//   	cut_defs["mu_background"]["13_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["mu_background"]["13_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["13_cutset"]["mu_type"] = 0;
// 
// 
// 
// // 
//      FIXME: mu_type not added from here on

// 	/*
// 	 *	JES - 10 %
// 	 */
// 
//  	// 13_cutset: preselection + trigger
//  	vcuts("mu_background", "13_cutset", "max_mu_d0", -1);
//  	vcuts("mu_background", "13_cutset", "max_e_d0", -1);
//  	cut_defs["mu_background"]["13_cutset"]["max_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["13_cutset"]["max_nisolated_mu"] = -1;
//  	cut_defs["mu_background"]["13_cutset"]["min_nisolated_e"] = -1;
//  	cut_defs["mu_background"]["13_cutset"]["min_nisolated_mu"] = -1;
//  	vcuts("mu_background", "13_cutset", "max_mu_eta", -1);
//  	vcuts("mu_background", "13_cutset", "max_e_eta", -1);
//  	cut_defs["mu_background"]["13_cutset"]["max_jet_eta"] = -1; 
//  	vcuts("mu_background", "13_cutset", "min_jet_pt", -1);	
//  	vcuts("mu_background", "13_cutset", "min_e_pt", -1);	
//  	vcuts("mu_background", "13_cutset", "min_mu_pt", -1);	
//  	vcuts("mu_background", "13_cutset", "max_mu_trackiso", -1);
//  	vcuts("mu_background", "13_cutset", "max_mu_caliso", -1);
//  	vcuts("mu_background", "13_cutset", "max_e_trackiso", -1);
//  	vcuts("mu_background", "13_cutset", "max_e_caliso", -1);
//  	cut_defs["mu_background"]["13_cutset"]["min_no_jets"] = -1;
// 	cut_defs["mu_background"]["13_cutset"]["JES_factor"] = 0.9; 

//           *      ELECTRON CUTS
//           */
// 
//          /*
//           *      ELECTRON BACKGROUND CUTS
//           */
// 
//         /*
//          *      global cuts for all selections defined so far
//          */
//         synchronise_maps();
// 
//         for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
//                 type_iter != cut_defs.end();
//                 ++type_iter)
//         {
//                 for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
//                     set_iter != type_iter->second.end();
//                     ++set_iter)
//                 {
//                         if(type_iter->first == "muon" || type_iter->first == "mu_background"){
// 				vset_if_not_set(type_iter->first, set_iter->first, "trigger", 85);
//                         }else if(type_iter->first == "electron" || type_iter->first == "e_background"){
// 				vset_if_not_set(type_iter->first, set_iter->first, "trigger", 50);
//                         }
//                 }
//         }
// 
//         /*
//          *      cuts for which the global selection cuts are not applied
//          */
// 	vcuts("muon", "without_cuts", "min_jet_pt", 0.0);
// 	vcuts("mu_background", "without_cuts", "min_jet_pt", 0.0);
// }

void CutSelector::define_cuts_OctX_mu()
{

	/************* MUON *****************/

         HLTConfigProvider hltConfig;
         hltConfig.init("HLT8E29");
         unsigned int e_triggerIndex( hltConfig.triggerIndex("HLT_Ele15_LW_L1R") );
         unsigned int mu_triggerIndex( hltConfig.triggerIndex("HLT_Mu9") );

	 std::cout << "mu_tigger: " << mu_triggerIndex << std::endl;
	 std::cout << "e_tigger: " << e_triggerIndex << std::endl;

 	// 01_cutset: trigger + global muon
 	vcuts("muon", "01_cutset", "max_mu_d0", -1);
 	vcuts("muon", "01_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["muon"]["01_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["01_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "01_cutset", "max_mu_eta", -1);
 	vcuts("muon", "01_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("muon", "01_cutset", "min_jet_pt", -1);	
 	vcuts("muon", "01_cutset", "min_e_pt", -1);	
 	vcuts("muon", "01_cutset", "min_mu_pt", -1);	
 	vcuts("muon", "01_cutset", "max_mu_trackiso", -1);
 	vcuts("muon", "01_cutset", "max_mu_caliso", -1);
 	vcuts("muon", "01_cutset", "max_e_trackiso", -1);
 	vcuts("muon", "01_cutset", "max_e_caliso", -1);
 	cut_defs["muon"]["01_cutset"]["min_no_jets"] = -1;
 	cut_defs["muon"]["01_cutset"]["mu_type"] = 0;
 	vcuts("muon", "01_cutset", "trigger", mu_triggerIndex);

 	// 02_cutset: 1 iso muon
 	vcuts("muon", "02_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "02_cutset", "max_mu_chi2", 10);
 	vcuts("muon", "02_cutset", "max_mu_d0", 0.02);
 	vcuts("muon", "02_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["02_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["02_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["02_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["02_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "02_cutset", "min_mu_relIso", 0.95);
 	vcuts("muon", "02_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "02_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("muon", "02_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "02_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["02_cutset"]["max_jet_eta"] = -1; 
 	vcuts("muon", "02_cutset", "min_jet_pt", -1);	
 	vcuts("muon", "02_cutset", "min_e_pt", -1);	
 	vcuts("muon", "02_cutset", "min_mu_pt", 20);	
 	cut_defs["muon"]["02_cutset"]["min_no_jets"] = -1;
 	cut_defs["muon"]["02_cutset"]["mu_type"] = 0;
 	vcuts("muon", "02_cutset", "trigger", mu_triggerIndex);

 	// 01_cutset: 4 jets
 	vcuts("muon", "03_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "03_cutset", "max_mu_chi2", 10);
 	vcuts("muon", "03_cutset", "max_mu_d0", 0.02);
 	vcuts("muon", "03_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["03_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["03_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["03_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["03_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "03_cutset", "min_mu_relIso", 0.95);
 	vcuts("muon", "03_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "03_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("muon", "03_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "03_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["03_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "03_cutset", "min_jet_pt", 30);	
 	vcuts("muon", "03_cutset", "min_e_pt", -1);	
 	vcuts("muon", "03_cutset", "min_mu_pt", 20);	
 	cut_defs["muon"]["03_cutset"]["min_no_jets"] = 4;
 	cut_defs["muon"]["03_cutset"]["mu_type"] = 0;
 	vcuts("muon", "03_cutset", "trigger", mu_triggerIndex);

 	// 01_cutset:no loose muon
 	vcuts("muon", "04_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "04_cutset", "max_mu_chi2", 10);
 	vcuts("muon", "04_cutset", "max_mu_d0", 0.02);
 	vcuts("muon", "04_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["04_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["04_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["04_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["04_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "04_cutset", "min_mu_relIso", 0.95);
 	vcuts("muon", "04_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "04_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("muon", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "04_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["04_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "04_cutset", "min_jet_pt", 30);	
 	vcuts("muon", "04_cutset", "min_e_pt", -1);	
 	vcuts("muon", "04_cutset", "min_mu_pt", 20);	
 	cut_defs["muon"]["04_cutset"]["min_no_jets"] = 4;
 	cut_defs["muon"]["04_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["04_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "04_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "04_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "04_cutset", "min_loose_mu_relIso", 0.8);
 	cut_defs["muon"]["04_cutset"]["loose_mu_type"] = 0;
 	vcuts("muon", "04_cutset", "trigger", mu_triggerIndex);

 	// 01_cutset:no loose electron
 	vcuts("muon", "05_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "05_cutset", "max_mu_chi2", 10);
 	vcuts("muon", "05_cutset", "max_mu_d0", 0.02);
 	vcuts("muon", "05_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["05_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["05_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["05_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["05_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "05_cutset", "min_mu_relIso", 0.95);
 	vcuts("muon", "05_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "05_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("muon", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "05_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "05_cutset", "min_jet_pt", 30);	
 	vcuts("muon", "05_cutset", "min_e_pt", -1);	
 	vcuts("muon", "05_cutset", "min_mu_pt", 20);	
 	cut_defs["muon"]["05_cutset"]["min_no_jets"] = 4;
 	cut_defs["muon"]["05_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["05_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "05_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "05_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "05_cutset", "min_loose_mu_relIso", 0.8);
 	cut_defs["muon"]["05_cutset"]["loose_mu_type"] = 0;
  	cut_defs["muon"]["05_cutset"]["max_nloose_e"] = 0;
  	vcuts("muon", "05_cutset", "min_loose_e_pt", 15);	
  	vcuts("muon", "05_cutset", "max_loose_e_eta", 2.5);
  	vcuts("muon", "05_cutset", "min_loose_e_relIso", 0.8);
 	vcuts("muon", "05_cutset", "trigger", mu_triggerIndex);

	/***********MU-BACKGROUND*******/

 	// 01_cutset: trigger + global muon
 	vcuts("mu_background", "01_cutset", "max_mu_d0", -1);
 	vcuts("mu_background", "01_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "01_cutset", "max_mu_eta", -1);
 	vcuts("mu_background", "01_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("mu_background", "01_cutset", "min_jet_pt", -1);	
 	vcuts("mu_background", "01_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "01_cutset", "min_mu_pt", -1);	
 	vcuts("mu_background", "01_cutset", "max_mu_trackiso", -1);
 	vcuts("mu_background", "01_cutset", "max_mu_caliso", -1);
 	vcuts("mu_background", "01_cutset", "max_e_trackiso", -1);
 	vcuts("mu_background", "01_cutset", "max_e_caliso", -1);
 	cut_defs["mu_background"]["01_cutset"]["min_no_jets"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["mu_type"] = 0;
 	vcuts("mu_background", "01_cutset", "trigger", mu_triggerIndex);

 	// 02_cutset: 1 iso mu_background
 	vcuts("mu_background", "02_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "02_cutset", "max_mu_chi2", 10);
 	vcuts("mu_background", "02_cutset", "max_mu_d0", 0.02);
 	vcuts("mu_background", "02_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["02_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["02_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["02_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["02_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "02_cutset", "min_mu_relIso", 0.95);
 	vcuts("mu_background", "02_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "02_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("mu_background", "02_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "02_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["02_cutset"]["max_jet_eta"] = -1; 
 	vcuts("mu_background", "02_cutset", "min_jet_pt", -1);	
 	vcuts("mu_background", "02_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "02_cutset", "min_mu_pt", 20);	
 	cut_defs["mu_background"]["02_cutset"]["min_no_jets"] = -1;
 	cut_defs["mu_background"]["02_cutset"]["mu_type"] = 0;
 	vcuts("mu_background", "02_cutset", "trigger", mu_triggerIndex);

 	// 01_cutset: 4 jets
 	vcuts("mu_background", "03_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "03_cutset", "max_mu_chi2", 10);
 	vcuts("mu_background", "03_cutset", "max_mu_d0", 0.02);
 	vcuts("mu_background", "03_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["03_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["03_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["03_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["03_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "03_cutset", "min_mu_relIso", 0.95);
 	vcuts("mu_background", "03_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "03_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("mu_background", "03_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "03_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["03_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "03_cutset", "min_jet_pt", 30);	
 	vcuts("mu_background", "03_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "03_cutset", "min_mu_pt", 20);	
 	cut_defs["mu_background"]["03_cutset"]["min_no_jets"] = 4;
 	cut_defs["mu_background"]["03_cutset"]["mu_type"] = 0;
 	vcuts("mu_background", "03_cutset", "trigger", mu_triggerIndex);

 	// 01_cutset:no loose muon
 	vcuts("mu_background", "04_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "04_cutset", "max_mu_chi2", 10);
 	vcuts("mu_background", "04_cutset", "max_mu_d0", 0.02);
 	vcuts("mu_background", "04_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["04_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["04_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["04_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["04_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "04_cutset", "min_mu_relIso", 0.95);
 	vcuts("mu_background", "04_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "04_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("mu_background", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "04_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["04_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "04_cutset", "min_jet_pt", 30);	
 	vcuts("mu_background", "04_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "04_cutset", "min_mu_pt", 20);	
 	cut_defs["mu_background"]["04_cutset"]["min_no_jets"] = 4;
 	cut_defs["mu_background"]["04_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["04_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "04_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "04_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "04_cutset", "min_loose_mu_relIso", 0.8);
 	vcuts("mu_background", "04_cutset", "trigger", mu_triggerIndex);

 	// 01_cutset:no loose electron
 	vcuts("mu_background", "05_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "05_cutset", "max_mu_chi2", 10);
 	vcuts("mu_background", "05_cutset", "max_mu_d0", 0.02);
 	vcuts("mu_background", "05_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["05_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["05_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["05_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["05_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "05_cutset", "min_mu_relIso", 0.95);
 	vcuts("mu_background", "05_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "05_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("mu_background", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "05_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "05_cutset", "min_jet_pt", 30);	
 	vcuts("mu_background", "05_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "05_cutset", "min_mu_pt", 20);	
 	cut_defs["mu_background"]["05_cutset"]["min_no_jets"] = 4;
 	cut_defs["mu_background"]["05_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["05_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "05_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "05_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "05_cutset", "min_loose_mu_relIso", 0.8);
 	cut_defs["mu_background"]["05_cutset"]["loose_mu_type"] = 0;
  	cut_defs["mu_background"]["05_cutset"]["max_nloose_e"] = 0;
  	vcuts("mu_background", "05_cutset", "min_loose_e_pt", 15);	
  	vcuts("mu_background", "05_cutset", "max_loose_e_eta", 2.5);
  	vcuts("mu_background", "05_cutset", "min_loose_e_relIso", 0.8);
 	vcuts("mu_background", "05_cutset", "trigger", mu_triggerIndex);
        /*
         *      cuts for which the global selection cuts are not applied
         */
	vcuts("muon", "without_cuts", "min_jet_pt", 0.0);
	vcuts("mu_background", "without_cuts", "min_jet_pt", 0.0);
}

void CutSelector::define_cuts_OctX_e()
{

         HLTConfigProvider hltConfig;
         hltConfig.init("HLT8E29");
         unsigned int e_triggerIndex( hltConfig.triggerIndex("HLT_Ele15_LW_L1R") );

	/**********ELECTRON*********/

 	// 01_cutset:trigger + 1 electron 
 	vcuts("electron", "01_cutset", "max_mu_d0", -1);
 	vcuts("electron", "01_cutset", "max_e_d0", -1);
 	cut_defs["electron"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["electron"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["electron"]["01_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["01_cutset"]["min_nisolated_mu"] = -1;
 	vcuts("electron", "01_cutset", "max_mu_eta", -1);
 	vcuts("electron", "01_cutset", "max_e_eta", -1);
 	cut_defs["electron"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("electron", "01_cutset", "min_jet_pt", -1);	
 	vcuts("electron", "01_cutset", "min_e_et", -1);	
 	vcuts("electron", "01_cutset", "min_mu_pt", -1);
 	cut_defs["electron"]["01_cutset"]["min_no_jets"] = -1;
	cut_defs["electron"]["01_cutset"]["min_jet_e_dR"] = -1; 
  	vcuts("electron", "01_cutset", "min_loose_e_et", -1);	
  	vcuts("electron", "01_cutset", "max_loose_e_eta", -1);
  	vcuts("electron", "01_cutset", "max_loose_e_d0", -1);
        vcuts("electron", "01_cutset", "e_electronID", -1);
 	vcuts("electron", "01_cutset", "trigger", e_triggerIndex);

 	// 02_cutset: exactly 1 electron
 	vcuts("electron", "02_cutset", "max_mu_d0", -1);
 	vcuts("electron", "02_cutset", "max_e_d0", 0.02);
        vcuts("electron", "02_cutset", "e_electronID", 1.0);
 	cut_defs["electron"]["02_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["electron"]["02_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["electron"]["02_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["02_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("electron", "02_cutset", "min_e_relIso", 0.9);
 	vcuts("electron", "02_cutset", "max_mu_eta", -1);
 	vcuts("electron", "02_cutset", "max_e_eta", 2.4);
 	cut_defs["electron"]["02_cutset"]["max_jet_eta"] = -1; 
 	vcuts("electron", "02_cutset", "min_jet_pt", -1);	
 	vcuts("electron", "02_cutset", "min_e_et", 30);	
 	vcuts("electron", "02_cutset", "min_mu_pt", -1);
 	cut_defs["electron"]["02_cutset"]["min_no_jets"] = -1;
	cut_defs["electron"]["02_cutset"]["min_jet_e_dR"] = -1; 
  	vcuts("electron", "02_cutset", "min_loose_e_et", -1);	
  	vcuts("electron", "02_cutset", "max_loose_e_eta", -1);
  	vcuts("electron", "02_cutset", "max_loose_e_d0", -1);
        vcuts("electron", "02_cutset", "e_electronID", -1);
 	vcuts("electron", "02_cutset", "trigger", e_triggerIndex);

 	// 03_cutset: no muon
 	vcuts("electron", "03_cutset", "min_mu_nHits", 11);
 	vcuts("electron", "03_cutset", "max_mu_chi2", 10);
 	vcuts("electron", "03_cutset", "max_mu_d0", -1);
 	vcuts("electron", "03_cutset", "max_e_d0", 0.02);
        vcuts("electron", "03_cutset", "e_electronID", 1.0);
 	cut_defs["electron"]["03_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["electron"]["03_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["electron"]["03_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["03_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("electron", "03_cutset", "min_e_relIso", 0.9);
  	vcuts("electron", "03_cutset", "min_mu_relIso", 0.95);
 	vcuts("electron", "03_cutset", "max_mu_eta", 2.1);
 	vcuts("electron", "03_cutset", "max_e_eta", 2.4);
 	cut_defs["electron"]["03_cutset"]["max_jet_eta"] = -1; 
 	vcuts("electron", "03_cutset", "min_jet_pt", -1);	
 	vcuts("electron", "03_cutset", "min_e_et", 30);	
 	vcuts("electron", "03_cutset", "min_mu_pt", 30);
 	cut_defs["electron"]["03_cutset"]["min_no_jets"] = -1;
	cut_defs["electron"]["03_cutset"]["min_jet_e_dR"] = -1; 
  	vcuts("electron", "03_cutset", "min_loose_e_et", -1);	
  	vcuts("electron", "03_cutset", "max_loose_e_eta", -1);
  	vcuts("electron", "03_cutset", "max_loose_e_d0", -1);
        vcuts("electron", "03_cutset", "e_electronID", -1);
 	vcuts("electron", "03_cutset", "trigger", e_triggerIndex);

 	// 01_cutset: jet electron cleaning
 	vcuts("electron", "04_cutset", "min_mu_nHits", 11);
 	vcuts("electron", "04_cutset", "max_mu_chi2", 10);
 	vcuts("electron", "04_cutset", "max_mu_d0", -1);
 	vcuts("electron", "04_cutset", "max_e_d0", 0.02);
        vcuts("electron", "04_cutset", "e_electronID", 1.0);
 	cut_defs["electron"]["04_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["electron"]["04_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["electron"]["04_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["04_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("electron", "04_cutset", "min_e_relIso", 0.9);
  	vcuts("electron", "04_cutset", "min_mu_relIso", 0.95);
 	vcuts("electron", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("electron", "04_cutset", "max_e_eta", 2.4);
 	cut_defs["electron"]["04_cutset"]["max_jet_eta"] = -1; 
 	vcuts("electron", "04_cutset", "min_jet_pt", -1);	
 	vcuts("electron", "04_cutset", "min_e_et", 30);	
 	vcuts("electron", "04_cutset", "min_mu_pt", 30);
 	cut_defs["electron"]["04_cutset"]["min_no_jets"] = -1;
	cut_defs["electron"]["04_cutset"]["min_jet_e_dR"] = 0.3; 
  	vcuts("electron", "04_cutset", "min_loose_e_et", 30);	
  	vcuts("electron", "04_cutset", "max_loose_e_eta", 2.4);
  	vcuts("electron", "04_cutset", "max_loose_e_d0", 0.02);
        vcuts("electron", "04_cutset", "e_electronID", 1.0);
 	vcuts("electron", "04_cutset", "trigger", e_triggerIndex);

 	// 01_cutset: 4 jets
 	vcuts("electron", "05_cutset", "min_mu_nHits", 11);
 	vcuts("electron", "05_cutset", "max_mu_chi2", 10);
 	vcuts("electron", "05_cutset", "max_mu_d0", -1);
 	vcuts("electron", "05_cutset", "max_e_d0", 0.02);
        vcuts("electron", "05_cutset", "e_electronID", 1.0);
 	cut_defs["electron"]["05_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["electron"]["05_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["electron"]["05_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["05_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("electron", "05_cutset", "min_e_relIso", 0.9);
  	vcuts("electron", "05_cutset", "min_mu_relIso", 0.95);
 	vcuts("electron", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("electron", "05_cutset", "max_e_eta", 2.4);
 	cut_defs["electron"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("electron", "05_cutset", "min_jet_pt", 30);	
 	vcuts("electron", "05_cutset", "min_e_et", 30);	
 	vcuts("electron", "05_cutset", "min_mu_pt", 30);
 	cut_defs["electron"]["05_cutset"]["min_no_jets"] = 4;
	cut_defs["electron"]["05_cutset"]["min_jet_e_dR"] = 0.3;
  	vcuts("electron", "05_cutset", "min_loose_e_et", 30);	
  	vcuts("electron", "05_cutset", "max_loose_e_eta", 2.4);
  	vcuts("electron", "05_cutset", "max_loose_e_d0", 0.02);
        vcuts("electron", "05_cutset", "e_electronID", 1.0);
 	vcuts("electron", "05_cutset", "trigger", e_triggerIndex);

	/***********E-BACKGROUND****************/

 	// 01_cutset:trigger + 1 electron 
 	vcuts("e_background", "01_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "01_cutset", "max_e_d0", -1);
 	cut_defs["e_background"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["e_background"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["e_background"]["01_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["01_cutset"]["min_nisolated_mu"] = -1;
 	vcuts("e_background", "01_cutset", "max_mu_eta", -1);
 	vcuts("e_background", "01_cutset", "max_e_eta", -1);
 	cut_defs["e_background"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("e_background", "01_cutset", "min_jet_pt", -1);	
 	vcuts("e_background", "01_cutset", "min_e_et", -1);	
 	vcuts("e_background", "01_cutset", "min_mu_pt", -1);
 	cut_defs["e_background"]["01_cutset"]["min_no_jets"] = -1;
	cut_defs["e_background"]["01_cutset"]["min_jet_e_dR"] = -1; 
  	vcuts("e_background", "01_cutset", "min_loose_e_et", -1);	
  	vcuts("e_background", "01_cutset", "max_loose_e_eta", -1);
  	vcuts("e_background", "01_cutset", "max_loose_e_d0", -1);
        vcuts("e_background", "01_cutset", "e_electronID", -1);
 	vcuts("e_background", "01_cutset", "trigger", e_triggerIndex);

 	// 02_cutset: exactly 1 electron
 	vcuts("e_background", "02_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "02_cutset", "max_e_d0", 0.02);
        vcuts("e_background", "02_cutset", "e_electronID", 1.0);
 	cut_defs["e_background"]["02_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["e_background"]["02_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["e_background"]["02_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["02_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("e_background", "02_cutset", "min_e_relIso", 0.9);
 	vcuts("e_background", "02_cutset", "max_mu_eta", -1);
 	vcuts("e_background", "02_cutset", "max_e_eta", 2.4);
 	cut_defs["e_background"]["02_cutset"]["max_jet_eta"] = -1; 
 	vcuts("e_background", "02_cutset", "min_jet_pt", -1);	
 	vcuts("e_background", "02_cutset", "min_e_et", 30);	
 	vcuts("e_background", "02_cutset", "min_mu_pt", -1);
 	cut_defs["e_background"]["02_cutset"]["min_no_jets"] = -1;
	cut_defs["e_background"]["02_cutset"]["min_jet_e_dR"] = -1; 
  	vcuts("e_background", "02_cutset", "min_loose_e_et", -1);	
  	vcuts("e_background", "02_cutset", "max_loose_e_eta", -1);
  	vcuts("e_background", "02_cutset", "max_loose_e_d0", -1);
        vcuts("e_background", "02_cutset", "e_electronID", -1);
 	vcuts("e_background", "02_cutset", "trigger", e_triggerIndex);

 	// 03_cutset: no muon
 	vcuts("e_background", "03_cutset", "min_mu_nHits", 11);
 	vcuts("e_background", "03_cutset", "max_mu_chi2", 10);
 	vcuts("e_background", "03_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "03_cutset", "max_e_d0", 0.02);
        vcuts("e_background", "03_cutset", "e_electronID", 1.0);
 	cut_defs["e_background"]["03_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["e_background"]["03_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["e_background"]["03_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["03_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("e_background", "03_cutset", "min_e_relIso", 0.9);
  	vcuts("e_background", "03_cutset", "min_mu_relIso", 0.95);
 	vcuts("e_background", "03_cutset", "max_mu_eta", 2.1);
 	vcuts("e_background", "03_cutset", "max_e_eta", 2.4);
 	cut_defs["e_background"]["03_cutset"]["max_jet_eta"] = -1; 
 	vcuts("e_background", "03_cutset", "min_jet_pt", -1);	
 	vcuts("e_background", "03_cutset", "min_e_et", 30);	
 	vcuts("e_background", "03_cutset", "min_mu_pt", 30);
 	cut_defs["e_background"]["03_cutset"]["min_no_jets"] = -1;
	cut_defs["e_background"]["03_cutset"]["min_jet_e_dR"] = -1; 
  	vcuts("e_background", "03_cutset", "min_loose_e_et", -1);	
  	vcuts("e_background", "03_cutset", "max_loose_e_eta", -1);
  	vcuts("e_background", "03_cutset", "max_loose_e_d0", -1);
        vcuts("e_background", "03_cutset", "e_electronID", -1);
 	vcuts("e_background", "03_cutset", "trigger", e_triggerIndex);

 	// 01_cutset: jet electron cleaning
 	vcuts("e_background", "04_cutset", "min_mu_nHits", 11);
 	vcuts("e_background", "04_cutset", "max_mu_chi2", 10);
 	vcuts("e_background", "04_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "04_cutset", "max_e_d0", 0.02);
        vcuts("e_background", "04_cutset", "e_electronID", 1.0);
 	cut_defs["e_background"]["04_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["e_background"]["04_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["e_background"]["04_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["04_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("e_background", "04_cutset", "min_e_relIso", 0.9);
  	vcuts("e_background", "04_cutset", "min_mu_relIso", 0.95);
 	vcuts("e_background", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("e_background", "04_cutset", "max_e_eta", 2.4);
 	cut_defs["e_background"]["04_cutset"]["max_jet_eta"] = -1; 
 	vcuts("e_background", "04_cutset", "min_jet_pt", -1);	
 	vcuts("e_background", "04_cutset", "min_e_et", 30);	
 	vcuts("e_background", "04_cutset", "min_mu_pt", 30);
 	cut_defs["e_background"]["04_cutset"]["min_no_jets"] = -1;
	cut_defs["e_background"]["04_cutset"]["min_jet_e_dR"] = 0.3; 
  	vcuts("e_background", "04_cutset", "min_loose_e_et", 30);	
  	vcuts("e_background", "04_cutset", "max_loose_e_eta", 2.4);
  	vcuts("e_background", "04_cutset", "max_loose_e_d0", 0.02);
        vcuts("e_background", "04_cutset", "e_electronID", 1.0);
 	vcuts("e_background", "04_cutset", "trigger", e_triggerIndex);

 	// 01_cutset: 4 jets
 	vcuts("e_background", "05_cutset", "min_mu_nHits", 11);
 	vcuts("e_background", "05_cutset", "max_mu_chi2", 10);
 	vcuts("e_background", "05_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "05_cutset", "max_e_d0", 0.02);
        vcuts("e_background", "05_cutset", "e_electronID", 1.0);
 	cut_defs["e_background"]["05_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["e_background"]["05_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["e_background"]["05_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["05_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("e_background", "05_cutset", "min_e_relIso", 0.9);
  	vcuts("e_background", "05_cutset", "min_mu_relIso", 0.95);
 	vcuts("e_background", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("e_background", "05_cutset", "max_e_eta", 2.4);
 	cut_defs["e_background"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("e_background", "05_cutset", "min_jet_pt", 30);	
 	vcuts("e_background", "05_cutset", "min_e_et", 30);	
 	vcuts("e_background", "05_cutset", "min_mu_pt", 30);
 	cut_defs["e_background"]["05_cutset"]["min_no_jets"] = 4;
	cut_defs["e_background"]["05_cutset"]["min_jet_e_dR"] = 0.3;
  	vcuts("e_background", "05_cutset", "min_loose_e_et", 30);	
  	vcuts("e_background", "05_cutset", "max_loose_e_eta", 2.4);
  	vcuts("e_background", "05_cutset", "max_loose_e_d0", 0.02);
        vcuts("e_background", "05_cutset", "e_electronID", 1.0);
 	vcuts("e_background", "05_cutset", "trigger", e_triggerIndex);


//        /*
//         *      cuts for which the global selection cuts are not applied
//         */
//	vcuts("electron", "without_cuts", "min_jet_pt", 0.0);
//	vcuts("e_background", "without_cuts", "min_jet_pt", 0.0);

	
}

void CutSelector::define_cuts_01X09()
{
	/*
	 *	declare cut vectors
	 *	remember to delete them in the destructor!
	 */

	std::vector<double> *jetcuts100603030 = new std::vector<double>();
	jetcuts100603030->push_back(100);
	jetcuts100603030->push_back(60);
	jetcuts100603030->push_back(30);
	jetcuts100603030->push_back(30);
	cuts_to_be_deleted.push_back(jetcuts100603030);

	/*
	 *	MUON CUTS
	 */

 	// 05_cutset: preselection
 	vcuts("muon", "01_cutset", "max_mu_d0", -1);
 	vcuts("muon", "01_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["muon"]["01_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["01_cutset"]["min_nisolated_mu"] = -1;
 	vcuts("muon", "01_cutset", "max_mu_eta", -1);
 	vcuts("muon", "01_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("muon", "01_cutset", "min_jet_pt", -1);	
 	vcuts("muon", "01_cutset", "min_e_pt", -1);	
 	vcuts("muon", "01_cutset", "min_mu_pt", -1);	
 	vcuts("muon", "01_cutset", "max_mu_trackiso", -1);
 	vcuts("muon", "01_cutset", "max_mu_caliso", -1);
 	vcuts("muon", "01_cutset", "max_e_trackiso", -1);
 	vcuts("muon", "01_cutset", "max_e_caliso", -1);
 	cut_defs["muon"]["01_cutset"]["min_no_jets"] = -1;
 	cut_defs["muon"]["01_cutset"]["mu_type"] = 0;

  	// 02_cutset: > 1 muon, pt>20, eta < 2.1; > 4 jets, pt>30, eta < 2.4
  	vcuts("muon", "02_cutset", "max_mu_d0", -1);
  	vcuts("muon", "02_cutset", "max_e_d0", -1);
  	cut_defs["muon"]["02_cutset"]["max_nisolated_e"] = -1;
  	cut_defs["muon"]["02_cutset"]["max_nisolated_mu"] = -1;
  	cut_defs["muon"]["02_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["02_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "02_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "02_cutset", "max_e_eta", -1);
  	cut_defs["muon"]["02_cutset"]["max_jet_eta"] =2.4; 
  	vcuts("muon", "02_cutset", "min_jet_pt",30.0);	
  	vcuts("muon", "02_cutset", "min_e_pt", -1);	
  	vcuts("muon", "02_cutset", "min_mu_pt", 20.0);	
  	vcuts("muon", "02_cutset", "max_mu_trackiso", -1);
  	vcuts("muon", "02_cutset", "max_mu_caliso", -1);
  	vcuts("muon", "02_cutset", "max_e_trackiso", -1);
  	vcuts("muon", "02_cutset", "max_e_caliso", -1);
  	cut_defs["muon"]["02_cutset"]["min_no_jets"] = 4;
 	cut_defs["muon"]["02_cutset"]["mu_type"] = 0;

  	// 03_cutset: > 1 muon, pt > 30, eta < 2.1
  	vcuts("muon", "03_cutset", "min_jet_pt",30.0);	
  	cut_defs["muon"]["03_cutset"]["min_no_jets"] = 4;
  	vcuts("muon", "03_cutset", "min_mu_pt", 30);	
  	vcuts("muon", "03_cutset", "min_e_pt", -1);	
  	vcuts("muon", "03_cutset", "max_mu_d0", 0.02);
  	vcuts("muon", "03_cutset", "max_e_d0", -1);
  	cut_defs["muon"]["03_cutset"]["max_nisolated_e"] = -1;
  	cut_defs["muon"]["03_cutset"]["max_nisolated_mu"] = -1;
  	cut_defs["muon"]["03_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["03_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "03_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "03_cutset", "max_e_eta", -1);
  	cut_defs["muon"]["03_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("muon", "03_cutset", "max_mu_trackiso", -1);
  	vcuts("muon", "03_cutset", "max_mu_caliso", -1);
  	vcuts("muon", "03_cutset", "max_e_trackiso", -1);
  	vcuts("muon", "03_cutset", "max_e_caliso", -1);
 	cut_defs["muon"]["03_cutset"]["mu_type"] = 0;

 	// 04_cutset: lepton isolation, exactly 1 lepton
 	vcuts("muon", "04_cutset", "min_jet_pt",30.0);	
 	cut_defs["muon"]["04_cutset"]["min_no_jets"] = 4;
 	vcuts("muon", "04_cutset", "min_mu_pt", 30);	
 	vcuts("muon", "04_cutset", "min_e_pt", 30);	
 	cut_defs["muon"]["04_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["muon"]["04_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["04_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["04_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "04_cutset", "max_e_eta", 2.4);
 	cut_defs["muon"]["04_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "04_cutset", "max_mu_trackiso", 3);
 	vcuts("muon", "04_cutset", "max_mu_caliso", 1);
 	vcuts("muon", "04_cutset", "max_e_trackiso", 5);
 	vcuts("muon", "04_cutset", "max_e_caliso", 10);
 	vcuts("muon", "04_cutset", "max_mu_d0", 0.02);
 	vcuts("muon", "04_cutset", "max_e_d0", 0.02);
 	cut_defs["muon"]["04_cutset"]["mu_type"] = 0;

 	// 05_cutset: asymmetric jet cuts bcut 3 + 11% bjet eff
 	cut_defs["muon"]["05_cutset"]["min_no_jets"] = 4;
 	vcuts("muon", "05_cutset", "min_mu_pt", 30);	
 	vcuts("muon", "05_cutset", "min_e_pt", 30);	
 	cut_defs["muon"]["05_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["muon"]["05_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["05_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["05_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "05_cutset", "max_e_eta", 2.4);
 	cut_defs["muon"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "05_cutset", "max_mu_trackiso", 3);
 	vcuts("muon", "05_cutset", "max_mu_caliso", 1);
 	vcuts("muon", "05_cutset", "max_e_trackiso", 5);
 	vcuts("muon", "05_cutset", "max_e_caliso", 10);
 	vcuts("muon", "05_cutset", "max_mu_d0", 0.02);
 	vcuts("muon", "05_cutset", "max_e_d0", 0.02);
 	cut_defs["muon"]["05_cutset"]["name_btag"] = 4; 
 	vcuts("muon", "05_cutset", "min_btag", 1.8);
 	cut_defs["muon"]["05_cutset"]["mu_type"] = 0;

 	// 06_cutset: asymmetric jet cuts bcut 3	
 	cut_defs["muon"]["06_cutset"]["min_no_jets"] = 4;
 	vcuts("muon", "06_cutset", "min_mu_pt", 30);	
 	vcuts("muon", "06_cutset", "min_e_pt", 30);	
 	cut_defs["muon"]["06_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["muon"]["06_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["06_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["06_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "06_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "06_cutset", "max_e_eta", 2.4);
 	cut_defs["muon"]["06_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "06_cutset", "max_mu_trackiso", 3);
 	vcuts("muon", "06_cutset", "max_mu_caliso", 1);
 	vcuts("muon", "06_cutset", "max_e_trackiso", 5);
 	vcuts("muon", "06_cutset", "max_e_caliso", 10);
 	vcuts("muon", "06_cutset", "max_mu_d0", 0.02);
 	vcuts("muon", "06_cutset", "max_e_d0", 0.02);
 	cut_defs["muon"]["06_cutset"]["name_btag"] = 4; 
 	vcuts("muon", "06_cutset", "min_btag", 3);
 	cut_defs["muon"]["06_cutset"]["mu_type"] = 0;

 	// 07_cutset: asymmetric jet cuts bcut 3 - 4.7% lightjet eff	
 	cut_defs["muon"]["07_cutset"]["min_no_jets"] = 4;
 	vcuts("muon", "07_cutset", "min_mu_pt", 30);	
 	vcuts("muon", "07_cutset", "min_e_pt", 30);	
 	cut_defs["muon"]["07_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["muon"]["07_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["07_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["07_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "07_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "07_cutset", "max_e_eta", 2.4);
 	cut_defs["muon"]["07_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "07_cutset", "max_mu_trackiso", 3);
 	vcuts("muon", "07_cutset", "max_mu_caliso", 1);
 	vcuts("muon", "07_cutset", "max_e_trackiso", 5);
 	vcuts("muon", "07_cutset", "max_e_caliso", 10);
 	vcuts("muon", "07_cutset", "max_mu_d0", 0.02);
 	vcuts("muon", "07_cutset", "max_e_d0", 0.02);
 	cut_defs["muon"]["07_cutset"]["name_btag"] = 4; 
 	vcuts("muon", "07_cutset", "min_btag", 5);
 	cut_defs["muon"]["07_cutset"]["mu_type"] = 0;

         /*
	 *	MUON BACKGROUND CUTS
         */

 	// 01_cutset: preselection + trigger
 	vcuts("mu_background", "01_cutset", "max_mu_d0", -1);
 	vcuts("mu_background", "01_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["min_nisolated_mu"] = -1;
 	vcuts("mu_background", "01_cutset", "max_mu_eta", -1);
 	vcuts("mu_background", "01_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("mu_background", "01_cutset", "min_jet_pt", -1);	
 	vcuts("mu_background", "01_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "01_cutset", "min_mu_pt", -1);	
 	vcuts("mu_background", "01_cutset", "max_mu_trackiso", -1);
 	vcuts("mu_background", "01_cutset", "max_mu_caliso", -1);
 	vcuts("mu_background", "01_cutset", "max_e_trackiso", -1);
 	vcuts("mu_background", "01_cutset", "max_e_caliso", -1);
 	cut_defs["mu_background"]["01_cutset"]["min_no_jets"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["mu_type"] = 0;

  	// 02_cutset: > 1 mu_background, pt>20, eta < 2.0; > 4 jets, pt>30, eta < 2.4
  	vcuts("mu_background", "02_cutset", "max_mu_d0", -1);
  	vcuts("mu_background", "02_cutset", "max_e_d0", -1);
  	cut_defs["mu_background"]["02_cutset"]["max_nisolated_e"] = -1;
  	cut_defs["mu_background"]["02_cutset"]["max_nisolated_mu"] = -1;
  	cut_defs["mu_background"]["02_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["02_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "02_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "02_cutset", "max_e_eta", -1);
  	cut_defs["mu_background"]["02_cutset"]["max_jet_eta"] =2.4; 
  	vcuts("mu_background", "02_cutset", "min_jet_pt",30.0);	
  	vcuts("mu_background", "02_cutset", "min_e_pt", -1);	
  	vcuts("mu_background", "02_cutset", "min_mu_pt", 20.0);	
  	vcuts("mu_background", "02_cutset", "max_mu_trackiso", -1);
  	vcuts("mu_background", "02_cutset", "max_mu_caliso", -1);
  	vcuts("mu_background", "02_cutset", "max_e_trackiso", -1);
  	vcuts("mu_background", "02_cutset", "max_e_caliso", -1);
  	cut_defs["mu_background"]["02_cutset"]["min_no_jets"] = 4;
 	cut_defs["mu_background"]["02_cutset"]["mu_type"] = 0;

  	// 03_cutset: > 1 mu_background, pt > 30, eta < 2.0
  	vcuts("mu_background", "03_cutset", "min_jet_pt",30.0);	
  	cut_defs["mu_background"]["03_cutset"]["min_no_jets"] = 4;
  	vcuts("mu_background", "03_cutset", "min_mu_pt", 30);	
  	vcuts("mu_background", "03_cutset", "min_e_pt", -1);	
  	vcuts("mu_background", "03_cutset", "max_mu_d0", 0.02);
  	vcuts("mu_background", "03_cutset", "max_e_d0", -1);
  	cut_defs["mu_background"]["03_cutset"]["max_nisolated_e"] = -1;
  	cut_defs["mu_background"]["03_cutset"]["max_nisolated_mu"] = -1;
  	cut_defs["mu_background"]["03_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["03_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "03_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "03_cutset", "max_e_eta", -1);
  	cut_defs["mu_background"]["03_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("mu_background", "03_cutset", "max_mu_trackiso", -1);
  	vcuts("mu_background", "03_cutset", "max_mu_caliso", -1);
  	vcuts("mu_background", "03_cutset", "max_e_trackiso", -1);
  	vcuts("mu_background", "03_cutset", "max_e_caliso", -1);
 	cut_defs["mu_background"]["03_cutset"]["mu_type"] = 0;

 	// 04_cutset: lepton isolation, exactly 1 lepton
 	vcuts("mu_background", "04_cutset", "min_jet_pt",30.0);	
 	cut_defs["mu_background"]["04_cutset"]["min_no_jets"] = 4;
 	vcuts("mu_background", "04_cutset", "min_mu_pt", 30);	
 	vcuts("mu_background", "04_cutset", "min_e_pt", 30);	
 	cut_defs["mu_background"]["04_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["mu_background"]["04_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["04_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["04_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "04_cutset", "max_e_eta", 2.4);
 	cut_defs["mu_background"]["04_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "04_cutset", "max_mu_trackiso", 3);
 	vcuts("mu_background", "04_cutset", "max_mu_caliso", 1);
 	vcuts("mu_background", "04_cutset", "max_e_trackiso", 5);
 	vcuts("mu_background", "04_cutset", "max_e_caliso", 10);
 	vcuts("mu_background", "04_cutset", "max_mu_d0", 0.02);
 	vcuts("mu_background", "04_cutset", "max_e_d0", 0.02);
 	cut_defs["mu_background"]["04_cutset"]["mu_type"] = 0;

 	// 05_cutset: asymmetric jet cuts bcut 3 + 11% bjet eff
 	cut_defs["mu_background"]["05_cutset"]["min_no_jets"] = 4;
 	vcuts("mu_background", "05_cutset", "min_mu_pt", 30);	
 	vcuts("mu_background", "05_cutset", "min_e_pt", 30);	
 	cut_defs["mu_background"]["05_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["mu_background"]["05_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["05_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["05_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "05_cutset", "max_e_eta", 2.4);
 	cut_defs["mu_background"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "05_cutset", "max_mu_trackiso", 3);
 	vcuts("mu_background", "05_cutset", "max_mu_caliso", 1);
 	vcuts("mu_background", "05_cutset", "max_e_trackiso", 5);
 	vcuts("mu_background", "05_cutset", "max_e_caliso", 10);
 	vcuts("mu_background", "05_cutset", "max_mu_d0", 0.02);
 	vcuts("mu_background", "05_cutset", "max_e_d0", 0.02);
 	cut_defs["mu_background"]["05_cutset"]["name_btag"] = 4; 
 	vcuts("mu_background", "05_cutset", "min_btag", 1.8);
 	cut_defs["mu_background"]["05_cutset"]["mu_type"] = 0;

 	// 06_cutset: asymmetric jet cuts bcut 3	
 	cut_defs["mu_background"]["06_cutset"]["min_no_jets"] = 4;
 	vcuts("mu_background", "06_cutset", "min_mu_pt", 30);	
 	vcuts("mu_background", "06_cutset", "min_e_pt", 30);	
 	cut_defs["mu_background"]["06_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["mu_background"]["06_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["06_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["06_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "06_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "06_cutset", "max_e_eta", 2.4);
 	cut_defs["mu_background"]["06_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "06_cutset", "max_mu_trackiso", 3);
 	vcuts("mu_background", "06_cutset", "max_mu_caliso", 1);
 	vcuts("mu_background", "06_cutset", "max_e_trackiso", 5);
 	vcuts("mu_background", "06_cutset", "max_e_caliso", 10);
 	vcuts("mu_background", "06_cutset", "max_mu_d0", 0.02);
 	vcuts("mu_background", "06_cutset", "max_e_d0", 0.02);
 	cut_defs["mu_background"]["06_cutset"]["name_btag"] = 4; 
 	vcuts("mu_background", "06_cutset", "min_btag", 3);
 	cut_defs["mu_background"]["06_cutset"]["mu_type"] = 0;

 	// 07_cutset: asymmetric jet cuts bcut 3 - 4.7% lightjet eff	
 	cut_defs["mu_background"]["07_cutset"]["min_no_jets"] = 4;
 	vcuts("mu_background", "07_cutset", "min_mu_pt", 30);	
 	vcuts("mu_background", "07_cutset", "min_e_pt", 30);	
 	cut_defs["mu_background"]["07_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["mu_background"]["07_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["07_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["07_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "07_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "07_cutset", "max_e_eta", 2.4);
 	cut_defs["mu_background"]["07_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "07_cutset", "max_mu_trackiso", 3);
 	vcuts("mu_background", "07_cutset", "max_mu_caliso", 1);
 	vcuts("mu_background", "07_cutset", "max_e_trackiso", 5);
 	vcuts("mu_background", "07_cutset", "max_e_caliso", 10);
 	vcuts("mu_background", "07_cutset", "max_mu_d0", 0.02);
 	vcuts("mu_background", "07_cutset", "max_e_d0", 0.02);
 	cut_defs["mu_background"]["07_cutset"]["name_btag"] = 4; 
 	vcuts("mu_background", "07_cutset", "min_btag", 5);
 	cut_defs["mu_background"]["07_cutset"]["mu_type"] = 0;
  /*
          *      ELECTRON CUTS
          */

         /*
          *      ELECTRON BACKGROUND CUTS
          */

        /*
         *      global cuts for all selections defined so far
         */
        synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
                        if(type_iter->first == "muon" || type_iter->first == "mu_background"){
				//vset_if_not_set(type_iter->first, set_iter->first, "trigger", 85);
                        }else if(type_iter->first == "electron" || type_iter->first == "e_background"){
				//vset_if_not_set(type_iter->first, set_iter->first, "trigger", 50);
                        }
                }
        }

        /*
         *      cuts for which the global selection cuts are not applied
         */
	vcuts("muon", "without_cuts", "min_jet_pt", 0.0);
	vcuts("mu_background", "without_cuts", "min_jet_pt", 0.0);
}

std::string CutSelector::get_preselection_id(int npreselection)
{
	Tools tools;

	if(npreselection < 10)
		return "preselection_step0"+tools.stringify(npreselection);
	else
		return "preselection_step"+tools.stringify(npreselection);
}

void CutSelector::set_if_not_set(std::string type, std::string set, std::string cut, double value)
{
	if(cut_defs[type][set].find(cut) == cut_defs[type][set].end()){
		cut_defs[type][set][cut] = value;	
	}
}

void CutSelector::vset_if_not_set(std::string type, std::string set, std::string cut, double value)
{
	if(vcut_defs[type][set].find(cut) == vcut_defs[type][set].end()){
		vcuts(type, set, cut, value);	
	}
}

void CutSelector::vset_if_not_set(std::string type, std::string set, std::string cut, std::vector<double> *value)
{
	if(vcut_defs[type][set].find(cut) == vcut_defs[type][set].end()){
		vcuts(type, set, cut, value);	
	}
}


void CutSelector::synchronise_maps()
{
        for(std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > >::iterator type_iter = vcut_defs.begin();
                type_iter != vcut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, std::vector<double>* > >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			cut_defs[type_iter->first][set_iter->first]["dummy"] = -1;
		}
	}

        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			vcuts(type_iter->first, set_iter->first, "dummy", -1);
		}
	}
}

void CutSelector::vcuts(std::string type, std::string set, std::string cut, double value)
{
	std::vector<double> *cut_vector = new std::vector<double>();
	if(value != -1)
		cut_vector->push_back(value);
	vcut_defs[type][set][cut] = cut_vector;	
	cuts_to_be_deleted.push_back(cut_vector);
}

void CutSelector::vcuts(std::string type, std::string set, std::string cut, std::vector<double> *cut_vector)
{
	vcut_defs[type][set][cut] = cut_vector;
}

void CutSelector::complete_cuts()
{
	std::vector<std::string> all_cuts;
	std::vector<std::string> all_v_cuts;

	all_cuts.push_back("min_met");
	all_cuts.push_back("max_ht");
	all_cuts.push_back("min_ht");
	all_cuts.push_back("min_no_jets");
	all_cuts.push_back("max_no_jets");
	all_cuts.push_back("max_nisolated_e");
	all_cuts.push_back("max_nisolated_mu");
	all_cuts.push_back("min_nisolated_e");
	all_cuts.push_back("min_nisolated_mu");
	all_cuts.push_back("max_nloose_e");
	all_cuts.push_back("max_nloose_mu");
	all_cuts.push_back("max_jet_eta");
	all_cuts.push_back("min_jet_e_dR");
	all_cuts.push_back("JES_factor");
	all_cuts.push_back("mu_type");
	all_cuts.push_back("e_type");
	all_cuts.push_back("loose_mu_type");
	all_cuts.push_back("loose_e_type");
	all_cuts.push_back("Z_rejection_width");
	all_cuts.push_back("name_btag");
	all_cuts.push_back("min_M3");
	all_cuts.push_back("min_mindiffM3");
	all_cuts.push_back("min_chi2");
	all_cuts.push_back("max_chi2");

	all_v_cuts.push_back("min_mu_pt");
	all_v_cuts.push_back("min_mu_et");
	all_v_cuts.push_back("max_mu_trackiso");
	all_v_cuts.push_back("max_mu_caliso");
	all_v_cuts.push_back("max_mu_ecaliso");
	all_v_cuts.push_back("max_mu_hcaliso");
	all_v_cuts.push_back("max_mu_hcal_veto_cone");
	all_v_cuts.push_back("max_mu_ecal_veto_cone");
	all_v_cuts.push_back("min_mu_relIso");
	all_v_cuts.push_back("min_mu_dR");
	all_v_cuts.push_back("min_e_pt");
	all_v_cuts.push_back("min_e_et");
	all_v_cuts.push_back("max_e_trackiso");
	all_v_cuts.push_back("max_e_caliso");
	all_v_cuts.push_back("max_e_ecaliso");
	all_v_cuts.push_back("max_e_hcaliso");
	all_v_cuts.push_back("max_e_hcal_veto_cone");
	all_v_cuts.push_back("max_e_ecal_veto_cone");
	all_v_cuts.push_back("min_e_relIso");
	all_v_cuts.push_back("min_e_dR");
	all_v_cuts.push_back("min_jet_pt");
        all_v_cuts.push_back("max_mu_chi2");
        all_v_cuts.push_back("max_mu_d0");
        all_v_cuts.push_back("max_mu_d0sig");
        all_v_cuts.push_back("min_mu_nHits");
        all_v_cuts.push_back("mu_electronID");
        all_v_cuts.push_back("max_e_chi2");
        all_v_cuts.push_back("max_e_d0");
        all_v_cuts.push_back("max_e_d0sig");
        all_v_cuts.push_back("min_e_nHits");
        all_v_cuts.push_back("e_electronID");
        all_v_cuts.push_back("trigger");
	all_v_cuts.push_back("min_btag");
	all_v_cuts.push_back("max_mu_eta");
	all_v_cuts.push_back("max_e_eta");

	// loose lepton cuts
	all_v_cuts.push_back("min_loose_mu_pt");
	all_v_cuts.push_back("min_loose_mu_et");
	all_v_cuts.push_back("max_loose_mu_trackiso");
	all_v_cuts.push_back("max_loose_mu_caliso");
	all_v_cuts.push_back("max_loose_mu_ecaliso");
	all_v_cuts.push_back("max_loose_mu_hcaliso");
	all_v_cuts.push_back("max_loose_mu_hcal_veto_cone");
	all_v_cuts.push_back("max_loose_mu_ecal_veto_cone");
	all_v_cuts.push_back("min_loose_mu_relIso");
	all_v_cuts.push_back("min_loose_mu_dR");
	all_v_cuts.push_back("min_loose_e_pt");
	all_v_cuts.push_back("min_loose_e_et");
	all_v_cuts.push_back("max_loose_e_trackiso");
	all_v_cuts.push_back("max_loose_e_caliso");
	all_v_cuts.push_back("max_loose_e_ecaliso");
	all_v_cuts.push_back("max_loose_e_hcaliso");
	all_v_cuts.push_back("max_loose_e_hcal_veto_cone");
	all_v_cuts.push_back("max_loose_e_ecal_veto_cone");
	all_v_cuts.push_back("min_loose_e_relIso");
	all_v_cuts.push_back("min_loose_e_dR");
        all_v_cuts.push_back("max_loose_mu_chi2");
        all_v_cuts.push_back("max_loose_mu_d0");
        all_v_cuts.push_back("max_loose_mu_d0sig");
        all_v_cuts.push_back("min_loose_mu_nHits");
        all_v_cuts.push_back("loose_mu_electronID");
        all_v_cuts.push_back("max_loose_e_chi2");
        all_v_cuts.push_back("max_loose_e_d0");
        all_v_cuts.push_back("max_loose_e_d0sig");
        all_v_cuts.push_back("min_loose_e_nHits");
        all_v_cuts.push_back("loose_e_electronID");
	all_v_cuts.push_back("max_loose_mu_eta");
	all_v_cuts.push_back("max_loose_e_eta");

	synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > >::iterator type_iter = vcut_defs.begin();
                type_iter != vcut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, std::vector<double>* > >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			for(std::vector<std::string>::iterator cut_name = all_v_cuts.begin();
			    cut_name != all_v_cuts.end();
			    ++cut_name){
				if(set_iter->second.find(*cut_name) == set_iter->second.end()){
					vcuts(type_iter->first, set_iter->first, *cut_name, -1);
				}
			}
		}
	}


        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			for(std::vector<std::string>::iterator cut_name = all_cuts.begin();
			    cut_name != all_cuts.end();
			    ++cut_name){
				if(set_iter->second.find(*cut_name) == set_iter->second.end()){
					set_iter->second[*cut_name] = -1;
				}
			}
		}
	}

}

void CutSelector::set_cuts()
{
        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			std::string full_id = dataset_id+"|"+type_iter->first+"|"+set_iter->first;
			std::string id = dataset_id+"_"+set_iter->first;
			plot_generators[type_iter->first][id] = new PlotGenerator(full_id);
			cuts[type_iter->first][id] = new Cuts(full_id);

			// jet selection cuts
			cuts[type_iter->first][id]->set_JES_factor(cut_defs[type_iter->first][set_iter->first]["JES_factor"]);
			cuts[type_iter->first][id]->set_min_njets((int) cut_defs[type_iter->first][set_iter->first]["min_no_jets"]);
			cuts[type_iter->first][id]->set_max_njets((int) cut_defs[type_iter->first][set_iter->first]["max_no_jets"]);
			cuts[type_iter->first][id]->set_max_jet_eta(cut_defs[type_iter->first][set_iter->first]["max_jet_eta"]);
			cuts[type_iter->first][id]->set_min_jet_e_dR(cut_defs[type_iter->first][set_iter->first]["min_jet_e_dR"]);
			cuts[type_iter->first][id]->set_min_jet_pt(vcut_defs[type_iter->first][set_iter->first]["min_jet_pt"]);

			// tight muon selection cuts
			cuts[type_iter->first][id]->set_min_nisolated_mu((int) cut_defs[type_iter->first][set_iter->first]["min_nisolated_mu"]);
			cuts[type_iter->first][id]->set_max_nisolated_mu((int) cut_defs[type_iter->first][set_iter->first]["max_nisolated_mu"]);
			cuts[type_iter->first][id]->set_max_mu_trackiso(vcut_defs[type_iter->first][set_iter->first]["max_mu_trackiso"]);
			cuts[type_iter->first][id]->set_max_mu_ecaliso(vcut_defs[type_iter->first][set_iter->first]["max_mu_ecaliso"]);
			cuts[type_iter->first][id]->set_max_mu_caliso(vcut_defs[type_iter->first][set_iter->first]["max_mu_caliso"]);
			cuts[type_iter->first][id]->set_max_mu_hcaliso(vcut_defs[type_iter->first][set_iter->first]["max_mu_hcaliso"]);
			cuts[type_iter->first][id]->set_max_mu_hcal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_mu_hcal_veto_cone"]);
			cuts[type_iter->first][id]->set_max_mu_ecal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_mu_ecal_veto_cone"]);
			cuts[type_iter->first][id]->set_min_mu_dR(vcut_defs[type_iter->first][set_iter->first]["min_mu_dR"]);
			cuts[type_iter->first][id]->set_min_mu_relIso(vcut_defs[type_iter->first][set_iter->first]["min_mu_relIso"]);
			cuts[type_iter->first][id]->set_min_mu_pt(vcut_defs[type_iter->first][set_iter->first]["min_mu_pt"]);
			cuts[type_iter->first][id]->set_min_mu_et(vcut_defs[type_iter->first][set_iter->first]["min_mu_et"]);
			cuts[type_iter->first][id]->set_min_mu_nHits(vcut_defs[type_iter->first][set_iter->first]["min_mu_nHits"]);
			cuts[type_iter->first][id]->set_max_mu_d0(vcut_defs[type_iter->first][set_iter->first]["max_mu_d0"]);
			cuts[type_iter->first][id]->set_max_mu_d0sig(vcut_defs[type_iter->first][set_iter->first]["max_mu_d0sig"]);
			cuts[type_iter->first][id]->set_max_mu_chi2(vcut_defs[type_iter->first][set_iter->first]["max_mu_chi2"]);
			cuts[type_iter->first][id]->set_mu_type(cut_defs[type_iter->first][set_iter->first]["mu_type"]);
			cuts[type_iter->first][id]->set_mu_electronID(vcut_defs[type_iter->first][set_iter->first]["mu_electronID"]);
			cuts[type_iter->first][id]->set_max_mu_eta(vcut_defs[type_iter->first][set_iter->first]["max_mu_eta"]);

			// loose muon selection cuts
			cuts[type_iter->first][id]->set_max_nloose_mu((int) cut_defs[type_iter->first][set_iter->first]["max_nloose_mu"]);
			cuts[type_iter->first][id]->set_max_loose_mu_trackiso(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_trackiso"]);
			cuts[type_iter->first][id]->set_max_loose_mu_ecaliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_ecaliso"]);
			cuts[type_iter->first][id]->set_max_loose_mu_caliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_caliso"]);
			cuts[type_iter->first][id]->set_max_loose_mu_hcaliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_hcaliso"]);
			cuts[type_iter->first][id]->set_max_loose_mu_hcal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_hcal_veto_cone"]);
			cuts[type_iter->first][id]->set_max_loose_mu_ecal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_ecal_veto_cone"]);
			cuts[type_iter->first][id]->set_min_loose_mu_dR(vcut_defs[type_iter->first][set_iter->first]["min_loose_mu_dR"]);
			cuts[type_iter->first][id]->set_min_loose_mu_relIso(vcut_defs[type_iter->first][set_iter->first]["min_loose_mu_relIso"]);
			cuts[type_iter->first][id]->set_min_loose_mu_pt(vcut_defs[type_iter->first][set_iter->first]["min_loose_mu_pt"]);
			cuts[type_iter->first][id]->set_min_loose_mu_et(vcut_defs[type_iter->first][set_iter->first]["min_loose_mu_et"]);
			cuts[type_iter->first][id]->set_min_loose_mu_nHits(vcut_defs[type_iter->first][set_iter->first]["min_loose_mu_nHits"]);
			cuts[type_iter->first][id]->set_max_loose_mu_d0(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_d0"]);
			cuts[type_iter->first][id]->set_max_loose_mu_d0sig(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_d0sig"]);
			cuts[type_iter->first][id]->set_max_loose_mu_chi2(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_chi2"]);
			cuts[type_iter->first][id]->set_loose_mu_type(cut_defs[type_iter->first][set_iter->first]["loose_mu_type"]);
			cuts[type_iter->first][id]->set_loose_mu_electronID(vcut_defs[type_iter->first][set_iter->first]["loose_mu_electronID"]);
			cuts[type_iter->first][id]->set_max_loose_mu_eta(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_eta"]);

			// tight electron selection cuts
			cuts[type_iter->first][id]->set_min_nisolated_e((int) cut_defs[type_iter->first][set_iter->first]["min_nisolated_e"]);
			cuts[type_iter->first][id]->set_max_nisolated_e((int) cut_defs[type_iter->first][set_iter->first]["max_nisolated_e"]);
			cuts[type_iter->first][id]->set_max_e_trackiso(vcut_defs[type_iter->first][set_iter->first]["max_e_trackiso"]);
			cuts[type_iter->first][id]->set_max_e_ecaliso(vcut_defs[type_iter->first][set_iter->first]["max_e_ecaliso"]);
			cuts[type_iter->first][id]->set_max_e_caliso(vcut_defs[type_iter->first][set_iter->first]["max_e_caliso"]);
			cuts[type_iter->first][id]->set_max_e_hcaliso(vcut_defs[type_iter->first][set_iter->first]["max_e_hcaliso"]);
			cuts[type_iter->first][id]->set_max_e_hcal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_e_hcal_veto_cone"]);
			cuts[type_iter->first][id]->set_max_e_ecal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_e_ecal_veto_cone"]);
			cuts[type_iter->first][id]->set_min_e_dR(vcut_defs[type_iter->first][set_iter->first]["min_e_dR"]);
			cuts[type_iter->first][id]->set_min_e_relIso(vcut_defs[type_iter->first][set_iter->first]["min_e_relIso"]);
			cuts[type_iter->first][id]->set_max_e_eta(vcut_defs[type_iter->first][set_iter->first]["max_e_eta"]);
			cuts[type_iter->first][id]->set_e_type(cut_defs[type_iter->first][set_iter->first]["e_type"]);
			cuts[type_iter->first][id]->set_min_e_pt(vcut_defs[type_iter->first][set_iter->first]["min_e_pt"]);
			cuts[type_iter->first][id]->set_min_e_et(vcut_defs[type_iter->first][set_iter->first]["min_e_et"]);
			cuts[type_iter->first][id]->set_min_e_nHits(vcut_defs[type_iter->first][set_iter->first]["min_e_nHits"]);
			cuts[type_iter->first][id]->set_max_e_d0(vcut_defs[type_iter->first][set_iter->first]["max_e_d0"]);
			cuts[type_iter->first][id]->set_max_e_d0sig(vcut_defs[type_iter->first][set_iter->first]["max_e_d0sig"]);
			cuts[type_iter->first][id]->set_max_e_chi2(vcut_defs[type_iter->first][set_iter->first]["max_e_chi2"]);
			cuts[type_iter->first][id]->set_e_electronID(vcut_defs[type_iter->first][set_iter->first]["e_electronID"]);

			// loose electron selection cuts
			cuts[type_iter->first][id]->set_max_nloose_e((int) cut_defs[type_iter->first][set_iter->first]["max_nloose_e"]);
			cuts[type_iter->first][id]->set_max_loose_e_trackiso(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_trackiso"]);
			cuts[type_iter->first][id]->set_max_loose_e_ecaliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_ecaliso"]);
			cuts[type_iter->first][id]->set_max_loose_e_caliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_caliso"]);
			cuts[type_iter->first][id]->set_max_loose_e_hcaliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_hcaliso"]);
			cuts[type_iter->first][id]->set_max_loose_e_hcal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_hcal_veto_cone"]);
			cuts[type_iter->first][id]->set_max_loose_e_ecal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_ecal_veto_cone"]);
			cuts[type_iter->first][id]->set_min_loose_e_dR(vcut_defs[type_iter->first][set_iter->first]["min_loose_e_dR"]);
			cuts[type_iter->first][id]->set_min_loose_e_relIso(vcut_defs[type_iter->first][set_iter->first]["min_loose_e_relIso"]);
			cuts[type_iter->first][id]->set_max_loose_e_eta(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_eta"]);
			cuts[type_iter->first][id]->set_loose_e_type(cut_defs[type_iter->first][set_iter->first]["loose_e_type"]);
			cuts[type_iter->first][id]->set_min_loose_e_pt(vcut_defs[type_iter->first][set_iter->first]["min_loose_e_pt"]);
			cuts[type_iter->first][id]->set_min_loose_e_et(vcut_defs[type_iter->first][set_iter->first]["min_loose_e_et"]);
			cuts[type_iter->first][id]->set_min_loose_e_nHits(vcut_defs[type_iter->first][set_iter->first]["min_loose_e_nHits"]);
			cuts[type_iter->first][id]->set_max_loose_e_d0(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_d0"]);
			cuts[type_iter->first][id]->set_max_loose_e_d0sig(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_d0sig"]);
			cuts[type_iter->first][id]->set_max_loose_e_chi2(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_chi2"]);
			cuts[type_iter->first][id]->set_loose_e_electronID(vcut_defs[type_iter->first][set_iter->first]["loose_e_electronID"]);

			//other cuts
			cuts[type_iter->first][id]->set_Z_rejection_width(cut_defs[type_iter->first][set_iter->first]["Z_rejection_width"]);
			cuts[type_iter->first][id]->set_min_btag(vcut_defs[type_iter->first][set_iter->first]["min_btag"]);
			cuts[type_iter->first][id]->set_name_btag(cut_defs[type_iter->first][set_iter->first]["name_btag"]);
			cuts[type_iter->first][id]->set_trigger(vcut_defs[type_iter->first][set_iter->first]["trigger"]);
			cuts[type_iter->first][id]->set_min_M3(cut_defs[type_iter->first][set_iter->first]["min_M3"]);
			cuts[type_iter->first][id]->set_min_mindiffM3(cut_defs[type_iter->first][set_iter->first]["min_mindiffM3"]);
			cuts[type_iter->first][id]->set_min_chi2(cut_defs[type_iter->first][set_iter->first]["min_chi2"]);
			cuts[type_iter->first][id]->set_max_chi2(cut_defs[type_iter->first][set_iter->first]["max_chi2"]);
			cuts[type_iter->first][id]->set_met_cut(cut_defs[type_iter->first][set_iter->first]["min_met"]);
			cuts[type_iter->first][id]->set_max_ht(cut_defs[type_iter->first][set_iter->first]["max_ht"]);
			cuts[type_iter->first][id]->set_min_ht(cut_defs[type_iter->first][set_iter->first]["min_ht"]);

			// pass t' mass to plot_generator
			plot_generators[type_iter->first][id]->set_tprime_mass(tprime_mass);

			// initialise plot generator with according cuts
			plot_generators[type_iter->first][id]->apply_cuts(cuts[type_iter->first][id]);
		}
        }
}

void CutSelector::set_tprime_mass(double tprimeMass)
{
	if(tprimeMass > 0)
		tprime_mass = tprimeMass;
}

double CutSelector::get_ht_cut()
{
	if(tprime_mass == 175) return 400;
	if(tprime_mass == 250) return 400;
	if(tprime_mass == 275) return 400;
	if(tprime_mass == 300) return 400;
	if(tprime_mass == 325) return 400;
	if(tprime_mass == 350) return 400;
	if(tprime_mass == 375) return 400;
	if(tprime_mass == 400) return 400;

	std::cerr << "WARNING: no ht cut found but accessed in CutSelector::get_ht_cut()" << std::endl;
	return -1;
}

// void CutSelector::define_cuts_Francisco()
// {
// 	/*
// 	 *	MUON CUTS
// 	 */
// 
//  	// 01_cutset: HLT
// // 	vcuts("muon", "01_cutset", "min_mu_nHits", 11);
// // 	vcuts("muon", "01_cutset", "max_mu_chi2", 10);
// //  	vcuts("muon", "01_cutset", "max_mu_d0sig", 3);
// //  	vcuts("muon", "01_cutset", "min_mu_pt", 20);
// //  	vcuts("muon", "01_cutset", "max_mu_eta", 2.1);
// //  	vcuts("muon", "01_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["muon"]["01_cutset"]["max_nisolated_mu"] = 1;
// //  	cut_defs["muon"]["01_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("muon", "01_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("muon", "01_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("muon", "01_cutset", "min_e_pt", 30);	
// //        vcuts("muon", "01_cutset", "e_electronID", 1.0);
// //        vcuts("muon", "01_cutset", "min_e_relIso", 0.9);
// //  	vcuts("muon", "01_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["01_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "01_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["01_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["01_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["muon"]["01_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["muon"]["01_cutset"]["min_nisolated_e"] = -1;
//  	vcuts("muon", "01_cutset", "trigger", 82);	// added as default
// //  	cut_defs["muon"]["01_cutset"]["mu_type"] = 0;
// 
//  	// 02_cutset: global muons
// // 	vcuts("muon", "02_cutset", "min_mu_nHits", 11);
// // 	vcuts("muon", "02_cutset", "max_mu_chi2", 10);
// //  	vcuts("muon", "02_cutset", "max_mu_d0sig", 3);
// //  	vcuts("muon", "02_cutset", "min_mu_pt", 20);
// //  	vcuts("muon", "02_cutset", "max_mu_eta", 2.1);
// //  	vcuts("muon", "02_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["muon"]["02_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["02_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("muon", "02_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("muon", "02_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("muon", "02_cutset", "min_e_pt", 30);	
// //        vcuts("muon", "02_cutset", "e_electronID", 1.0);
// //        vcuts("muon", "02_cutset", "min_e_relIso", 0.9);
// //  	vcuts("muon", "02_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["02_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "02_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["02_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["02_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["muon"]["02_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["muon"]["02_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["02_cutset"]["mu_type"] = 0;
// 
//  	// 03_cutset: global muons
// // 	vcuts("muon", "03_cutset", "min_mu_nHits", 11);
// // 	vcuts("muon", "03_cutset", "max_mu_chi2", 10);
// //  	vcuts("muon", "03_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "03_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "03_cutset", "max_mu_eta", 2.1);
// //  	vcuts("muon", "03_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["muon"]["03_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["03_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("muon", "03_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("muon", "03_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("muon", "03_cutset", "min_e_pt", 30);	
// //        vcuts("muon", "03_cutset", "e_electronID", 1.0);
// //        vcuts("muon", "03_cutset", "min_e_relIso", 0.9);
// //  	vcuts("muon", "03_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["03_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "03_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["03_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["03_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["muon"]["03_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["muon"]["03_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["03_cutset"]["mu_type"] = 0;
// 
//  	// 04_cutset: global muons
// 	vcuts("muon", "04_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "04_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "04_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "04_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "04_cutset", "max_mu_eta", 2.1);
// //  	vcuts("muon", "04_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["muon"]["04_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["04_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("muon", "04_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("muon", "04_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("muon", "04_cutset", "min_e_pt", 30);	
// //        vcuts("muon", "04_cutset", "e_electronID", 1.0);
// //        vcuts("muon", "04_cutset", "min_e_relIso", 0.9);
// //  	vcuts("muon", "04_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["04_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "04_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["04_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["04_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["muon"]["04_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["muon"]["04_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["04_cutset"]["mu_type"] = 0;
// 
//  	// 05_cutset: global muons
//  	vcuts("muon", "05_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "05_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "05_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "05_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "05_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "05_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["muon"]["05_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["05_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("muon", "05_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("muon", "05_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("muon", "05_cutset", "min_e_pt", 30);	
// //        vcuts("muon", "05_cutset", "e_electronID", 1.0);
// //        vcuts("muon", "05_cutset", "min_e_relIso", 0.9);
// //  	vcuts("muon", "05_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["05_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "05_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["05_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["05_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["muon"]["05_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["muon"]["05_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["05_cutset"]["mu_type"] = 0;
// 
//  	// 06_cutset: global muons
//  	vcuts("muon", "06_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "06_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "06_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "06_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "06_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "06_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["06_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["06_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("muon", "06_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("muon", "06_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("muon", "06_cutset", "min_e_pt", 30);	
// //        vcuts("muon", "06_cutset", "e_electronID", 1.0);
// //        vcuts("muon", "06_cutset", "min_e_relIso", 0.9);
// //  	vcuts("muon", "06_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["06_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "06_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["06_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["06_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["muon"]["06_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["muon"]["06_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["06_cutset"]["mu_type"] = 0;
// 
//  	// 07_cutset: global muons
//  	vcuts("muon", "07_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "07_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "07_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "07_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "07_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "07_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["07_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["07_cutset"]["min_nisolated_mu"] = 1;
//   	cut_defs["muon"]["07_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("muon", "07_cutset", "min_loose_mu_pt", 10);
//   	vcuts("muon", "07_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("muon", "07_cutset", "min_loose_mu_relIso", 0.8);
// // 	vcuts("muon", "07_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("muon", "07_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("muon", "07_cutset", "min_e_pt", 30);	
// //        vcuts("muon", "07_cutset", "e_electronID", 1.0);
// //        vcuts("muon", "07_cutset", "min_e_relIso", 0.9);
// //  	vcuts("muon", "07_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["07_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "07_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["07_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["07_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["muon"]["07_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["muon"]["07_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["07_cutset"]["mu_type"] = 0;
// 
//  	// 08_cutset: global muons
//  	vcuts("muon", "08_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "08_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "08_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "08_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "08_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "08_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["08_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["08_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "08_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "08_cutset", "max_mu_ecal_veto_cone", 4);
//   	cut_defs["muon"]["08_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("muon", "08_cutset", "min_loose_mu_pt", 10);
//   	vcuts("muon", "08_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("muon", "08_cutset", "min_loose_mu_relIso", 0.8);
// //  	vcuts("muon", "08_cutset", "min_e_pt", 30);	
// //        vcuts("muon", "08_cutset", "e_electronID", 1.0);
// //        vcuts("muon", "08_cutset", "min_e_relIso", 0.9);
// //  	vcuts("muon", "08_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["08_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "08_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["08_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["08_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["muon"]["08_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["muon"]["08_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["08_cutset"]["mu_type"] = 0;
// 
//  	// 09_cutset: global muons
//  	vcuts("muon", "09_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "09_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "09_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "09_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "09_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "09_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["09_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["09_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "09_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "09_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "09_cutset", "min_e_pt", 20);	
//         vcuts("muon", "09_cutset", "e_electronID", 1.0);
//         vcuts("muon", "09_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "09_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["09_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "09_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["09_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["09_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["09_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["09_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["09_cutset"]["mu_type"] = 0;
//   	cut_defs["muon"]["09_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("muon", "09_cutset", "min_loose_mu_pt", 10);
//   	vcuts("muon", "09_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("muon", "09_cutset", "min_loose_mu_relIso", 0.8);
// 
//  	// 10_cutset: global muons
//  	vcuts("muon", "10_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "10_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "10_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "10_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "10_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "10_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["10_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["10_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "10_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "10_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "10_cutset", "min_e_pt", 20);	
//         vcuts("muon", "10_cutset", "e_electronID", 1.0);
//         vcuts("muon", "10_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "10_cutset", "max_e_eta", 2.4);
// //  	cut_defs["muon"]["10_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("muon", "10_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["muon"]["10_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["10_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["10_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["10_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["10_cutset"]["mu_type"] = 0;
//   	cut_defs["muon"]["10_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("muon", "10_cutset", "min_loose_mu_pt", 10);
//   	vcuts("muon", "10_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("muon", "10_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["muon"]["10_cutset"]["max_nloose_e"] = 1;
//   	vcuts("muon", "10_cutset", "min_loose_e_pt", 15);	
//   	vcuts("muon", "10_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("muon", "10_cutset", "min_loose_e_relIso", 0.8);
// 
//  	// 11_cutset: global muons
//  	vcuts("muon", "11_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "11_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "11_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "11_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "11_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "11_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["11_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["11_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "11_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "11_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "11_cutset", "min_e_pt", 20);	
//         vcuts("muon", "11_cutset", "e_electronID", 1.0);
//         vcuts("muon", "11_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "11_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["11_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "11_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["11_cutset"]["min_no_jets"] = 1;
//   	cut_defs["muon"]["11_cutset"]["max_no_jets"] = 1;
//   	cut_defs["muon"]["11_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["11_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["11_cutset"]["mu_type"] = 0;
//   	cut_defs["muon"]["11_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("muon", "11_cutset", "min_loose_mu_pt", 10);
//   	vcuts("muon", "11_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("muon", "11_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["muon"]["11_cutset"]["max_nloose_e"] = 1;
//   	vcuts("muon", "11_cutset", "min_loose_e_pt", 15);	
//   	vcuts("muon", "11_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("muon", "11_cutset", "min_loose_e_relIso", 0.8);
// 
//  	// 12_cutset: global muons
//  	vcuts("muon", "12_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "12_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "12_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "12_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "12_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "12_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["12_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["12_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "12_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "12_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "12_cutset", "min_e_pt", 20);	
//         vcuts("muon", "12_cutset", "e_electronID", 1.0);
//         vcuts("muon", "12_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "12_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["12_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "12_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["12_cutset"]["min_no_jets"] = 2;
//   	cut_defs["muon"]["12_cutset"]["max_no_jets"] = 2;
//   	cut_defs["muon"]["12_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["12_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["12_cutset"]["mu_type"] = 0;
//   	cut_defs["muon"]["12_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("muon", "12_cutset", "min_loose_mu_pt", 10);
//   	vcuts("muon", "12_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("muon", "12_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["muon"]["12_cutset"]["max_nloose_e"] = 1;
//   	vcuts("muon", "12_cutset", "min_loose_e_pt", 15);	
//   	vcuts("muon", "12_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("muon", "12_cutset", "min_loose_e_relIso", 0.8);
// 
//  	// 13_cutset: global muons
//  	vcuts("muon", "13_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "13_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "13_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "13_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "13_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "13_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["13_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["13_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "13_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "13_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "13_cutset", "min_e_pt", 20);	
//         vcuts("muon", "13_cutset", "e_electronID", 1.0);
//         vcuts("muon", "13_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "13_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["13_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "13_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["13_cutset"]["min_no_jets"] = 3;
//   	cut_defs["muon"]["13_cutset"]["max_no_jets"] = 3;
//   	cut_defs["muon"]["13_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["13_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["13_cutset"]["mu_type"] = 0;
//   	cut_defs["muon"]["13_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("muon", "13_cutset", "min_loose_mu_pt", 10);
//   	vcuts("muon", "13_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("muon", "13_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["muon"]["13_cutset"]["max_nloose_e"] = 1;
//   	vcuts("muon", "13_cutset", "min_loose_e_pt", 15);	
//   	vcuts("muon", "13_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("muon", "13_cutset", "min_loose_e_relIso", 0.8);
// 
//  	// 14_cutset: global muons
//  	vcuts("muon", "14_cutset", "min_mu_nHits", 11);
//  	vcuts("muon", "14_cutset", "max_mu_chi2", 10);
//   	vcuts("muon", "14_cutset", "max_mu_d0sig", 3);
//   	vcuts("muon", "14_cutset", "min_mu_pt", 20);
//   	vcuts("muon", "14_cutset", "max_mu_eta", 2.1);
//   	vcuts("muon", "14_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["muon"]["14_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["muon"]["14_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("muon", "14_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("muon", "14_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("muon", "14_cutset", "min_e_pt", 20);	
//         vcuts("muon", "14_cutset", "e_electronID", 1.0);
//         vcuts("muon", "14_cutset", "min_e_relIso", 0.9);
//   	vcuts("muon", "14_cutset", "max_e_eta", 2.4);
//   	cut_defs["muon"]["14_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("muon", "14_cutset", "min_jet_pt",30.0);	
//   	cut_defs["muon"]["14_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["muon"]["14_cutset"]["max_no_jets"] = 7;
//   	cut_defs["muon"]["14_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["muon"]["14_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["muon"]["14_cutset"]["mu_type"] = 0;
//   	cut_defs["muon"]["14_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("muon", "14_cutset", "min_loose_mu_pt", 10);
//   	vcuts("muon", "14_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("muon", "14_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["muon"]["14_cutset"]["max_nloose_e"] = 1;
//   	vcuts("muon", "14_cutset", "min_loose_e_pt", 15);	
//   	vcuts("muon", "14_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("muon", "14_cutset", "min_loose_e_relIso", 0.8);
// 
// 	/*
// 	 *	MUON BACKGROUND CUTS
// 	 */
// 
//  	// 01_cutset: HLT
// // 	vcuts("mu_background", "01_cutset", "min_mu_nHits", 11);
// // 	vcuts("mu_background", "01_cutset", "max_mu_chi2", 10);
// //  	vcuts("mu_background", "01_cutset", "max_mu_d0sig", 3);
// //  	vcuts("mu_background", "01_cutset", "min_mu_pt", 20);
// //  	vcuts("mu_background", "01_cutset", "max_mu_eta", 2.1);
// //  	vcuts("mu_background", "01_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["mu_background"]["01_cutset"]["max_nisolated_mu"] = 1;
// //  	cut_defs["mu_background"]["01_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("mu_background", "01_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("mu_background", "01_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("mu_background", "01_cutset", "min_e_pt", 30);	
// //        vcuts("mu_background", "01_cutset", "e_electronID", 1.0);
// //        vcuts("mu_background", "01_cutset", "min_e_relIso", 0.9);
// //  	vcuts("mu_background", "01_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["01_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "01_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["01_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["01_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["mu_background"]["01_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["mu_background"]["01_cutset"]["min_nisolated_e"] = -1;
//  	vcuts("mu_background", "01_cutset", "trigger", 82);	// added as default
// //  	cut_defs["mu_background"]["01_cutset"]["mu_type"] = 0;
// 
//  	// 02_cutset: global mu_backgrounds
// // 	vcuts("mu_background", "02_cutset", "min_mu_nHits", 11);
// // 	vcuts("mu_background", "02_cutset", "max_mu_chi2", 10);
// //  	vcuts("mu_background", "02_cutset", "max_mu_d0sig", 3);
// //  	vcuts("mu_background", "02_cutset", "min_mu_pt", 20);
// //  	vcuts("mu_background", "02_cutset", "max_mu_eta", 2.1);
// //  	vcuts("mu_background", "02_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["mu_background"]["02_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["02_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("mu_background", "02_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("mu_background", "02_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("mu_background", "02_cutset", "min_e_pt", 30);	
// //        vcuts("mu_background", "02_cutset", "e_electronID", 1.0);
// //        vcuts("mu_background", "02_cutset", "min_e_relIso", 0.9);
// //  	vcuts("mu_background", "02_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["02_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "02_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["02_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["02_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["mu_background"]["02_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["mu_background"]["02_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["02_cutset"]["mu_type"] = 0;
// 
//  	// 03_cutset: global mu_backgrounds
// // 	vcuts("mu_background", "03_cutset", "min_mu_nHits", 11);
// // 	vcuts("mu_background", "03_cutset", "max_mu_chi2", 10);
// //  	vcuts("mu_background", "03_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "03_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "03_cutset", "max_mu_eta", 2.1);
// //  	vcuts("mu_background", "03_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["mu_background"]["03_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["03_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("mu_background", "03_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("mu_background", "03_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("mu_background", "03_cutset", "min_e_pt", 30);	
// //        vcuts("mu_background", "03_cutset", "e_electronID", 1.0);
// //        vcuts("mu_background", "03_cutset", "min_e_relIso", 0.9);
// //  	vcuts("mu_background", "03_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["03_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "03_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["03_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["03_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["mu_background"]["03_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["mu_background"]["03_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["03_cutset"]["mu_type"] = 0;
// 
//  	// 04_cutset: global mu_backgrounds
//  	vcuts("mu_background", "04_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "04_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "04_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "04_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "04_cutset", "max_mu_eta", 2.1);
// //  	vcuts("mu_background", "04_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["mu_background"]["04_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["04_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("mu_background", "04_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("mu_background", "04_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("mu_background", "04_cutset", "min_e_pt", 30);	
// //        vcuts("mu_background", "04_cutset", "e_electronID", 1.0);
// //        vcuts("mu_background", "04_cutset", "min_e_relIso", 0.9);
// //  	vcuts("mu_background", "04_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["04_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "04_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["04_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["04_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["mu_background"]["04_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["mu_background"]["04_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["04_cutset"]["mu_type"] = 0;
// 
//  	// 05_cutset: global mu_backgrounds
//  	vcuts("mu_background", "05_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "05_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "05_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "05_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "05_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "05_cutset", "min_mu_relIso", 0.95);
// //  	cut_defs["mu_background"]["05_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["05_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("mu_background", "05_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("mu_background", "05_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("mu_background", "05_cutset", "min_e_pt", 30);	
// //        vcuts("mu_background", "05_cutset", "e_electronID", 1.0);
// //        vcuts("mu_background", "05_cutset", "min_e_relIso", 0.9);
// //  	vcuts("mu_background", "05_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["05_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "05_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["05_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["05_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["mu_background"]["05_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["mu_background"]["05_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["05_cutset"]["mu_type"] = 0;
// 
//  	// 06_cutset: global mu_backgrounds
//  	vcuts("mu_background", "06_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "06_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "06_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "06_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "06_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "06_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["06_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["06_cutset"]["min_nisolated_mu"] = 1;
// // 	vcuts("mu_background", "06_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("mu_background", "06_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("mu_background", "06_cutset", "min_e_pt", 30);	
// //        vcuts("mu_background", "06_cutset", "e_electronID", 1.0);
// //        vcuts("mu_background", "06_cutset", "min_e_relIso", 0.9);
// //  	vcuts("mu_background", "06_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["06_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "06_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["06_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["06_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["mu_background"]["06_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["mu_background"]["06_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["06_cutset"]["mu_type"] = 0;
// 
//  	// 07_cutset: global mu_backgrounds
//  	vcuts("mu_background", "07_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "07_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "07_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "07_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "07_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "07_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["07_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["07_cutset"]["min_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["07_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("mu_background", "07_cutset", "min_loose_mu_pt", 10);
//   	vcuts("mu_background", "07_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("mu_background", "07_cutset", "min_loose_mu_relIso", 0.8);
// // 	vcuts("mu_background", "07_cutset", "max_mu_hcal_veto_cone", 6);
// // 	vcuts("mu_background", "07_cutset", "max_mu_ecal_veto_cone", 4);
// //  	vcuts("mu_background", "07_cutset", "min_e_pt", 30);	
// //        vcuts("mu_background", "07_cutset", "e_electronID", 1.0);
// //        vcuts("mu_background", "07_cutset", "min_e_relIso", 0.9);
// //  	vcuts("mu_background", "07_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["07_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "07_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["07_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["07_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["mu_background"]["07_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["mu_background"]["07_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["07_cutset"]["mu_type"] = 0;
// 
//  	// 08_cutset: global mu_backgrounds
//  	vcuts("mu_background", "08_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "08_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "08_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "08_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "08_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "08_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["08_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["08_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "08_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("mu_background", "08_cutset", "max_mu_ecal_veto_cone", 4);
//   	cut_defs["mu_background"]["08_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("mu_background", "08_cutset", "min_loose_mu_pt", 10);
//   	vcuts("mu_background", "08_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("mu_background", "08_cutset", "min_loose_mu_relIso", 0.8);
// //  	vcuts("mu_background", "08_cutset", "min_e_pt", 30);	
// //        vcuts("mu_background", "08_cutset", "e_electronID", 1.0);
// //        vcuts("mu_background", "08_cutset", "min_e_relIso", 0.9);
// //  	vcuts("mu_background", "08_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["08_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "08_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["08_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["08_cutset"]["max_no_jets"] = 7;
// //  	cut_defs["mu_background"]["08_cutset"]["max_nisolated_e"] = 0;
// //  	cut_defs["mu_background"]["08_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["08_cutset"]["mu_type"] = 0;
// 
//  	// 09_cutset: global mu_backgrounds
//  	vcuts("mu_background", "09_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "09_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "09_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "09_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "09_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "09_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["09_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["09_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "09_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("mu_background", "09_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("mu_background", "09_cutset", "min_e_pt", 20);	
//         vcuts("mu_background", "09_cutset", "e_electronID", 1.0);
//         vcuts("mu_background", "09_cutset", "min_e_relIso", 0.9);
//   	vcuts("mu_background", "09_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["09_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "09_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["09_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["09_cutset"]["max_no_jets"] = 7;
//   	cut_defs["mu_background"]["09_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["mu_background"]["09_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["09_cutset"]["mu_type"] = 0;
//   	cut_defs["mu_background"]["09_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("mu_background", "09_cutset", "min_loose_mu_pt", 10);
//   	vcuts("mu_background", "09_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("mu_background", "09_cutset", "min_loose_mu_relIso", 0.8);
// 
//  	// 10_cutset: global mu_backgrounds
//  	vcuts("mu_background", "10_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "10_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "10_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "10_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "10_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "10_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["10_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["10_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "10_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("mu_background", "10_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("mu_background", "10_cutset", "min_e_pt", 20);	
//         vcuts("mu_background", "10_cutset", "e_electronID", 1.0);
//         vcuts("mu_background", "10_cutset", "min_e_relIso", 0.9);
//   	vcuts("mu_background", "10_cutset", "max_e_eta", 2.4);
// //  	cut_defs["mu_background"]["10_cutset"]["max_jet_eta"] = 2.4; 
// //  	vcuts("mu_background", "10_cutset", "min_jet_pt",30.0);	
// //  	cut_defs["mu_background"]["10_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["10_cutset"]["max_no_jets"] = 7;
//   	cut_defs["mu_background"]["10_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["mu_background"]["10_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["10_cutset"]["mu_type"] = 0;
//   	cut_defs["mu_background"]["10_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("mu_background", "10_cutset", "min_loose_mu_pt", 10);
//   	vcuts("mu_background", "10_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("mu_background", "10_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["mu_background"]["10_cutset"]["max_nloose_e"] = 1;
//   	vcuts("mu_background", "10_cutset", "min_loose_e_pt", 15);	
//   	vcuts("mu_background", "10_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("mu_background", "10_cutset", "min_loose_e_relIso", 0.8);
// 
//  	// 11_cutset: global mu_backgrounds
//  	vcuts("mu_background", "11_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "11_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "11_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "11_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "11_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "11_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["11_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["11_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "11_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("mu_background", "11_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("mu_background", "11_cutset", "min_e_pt", 20);	
//         vcuts("mu_background", "11_cutset", "e_electronID", 1.0);
//         vcuts("mu_background", "11_cutset", "min_e_relIso", 0.9);
//   	vcuts("mu_background", "11_cutset", "max_e_eta", 2.4);
//   	cut_defs["mu_background"]["11_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("mu_background", "11_cutset", "min_jet_pt",30.0);	
//   	cut_defs["mu_background"]["11_cutset"]["min_no_jets"] = 1;
//   	cut_defs["mu_background"]["11_cutset"]["max_no_jets"] = 1;
//   	cut_defs["mu_background"]["11_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["mu_background"]["11_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["11_cutset"]["mu_type"] = 0;
//   	cut_defs["mu_background"]["11_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("mu_background", "11_cutset", "min_loose_mu_pt", 10);
//   	vcuts("mu_background", "11_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("mu_background", "11_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["mu_background"]["11_cutset"]["max_nloose_e"] = 1;
//   	vcuts("mu_background", "11_cutset", "min_loose_e_pt", 15);	
//   	vcuts("mu_background", "11_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("mu_background", "11_cutset", "min_loose_e_relIso", 0.8);
// 
//  	// 12_cutset: global mu_backgrounds
//  	vcuts("mu_background", "12_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "12_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "12_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "12_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "12_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "12_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["12_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["12_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "12_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("mu_background", "12_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("mu_background", "12_cutset", "min_e_pt", 20);	
//         vcuts("mu_background", "12_cutset", "e_electronID", 1.0);
//         vcuts("mu_background", "12_cutset", "min_e_relIso", 0.9);
//   	vcuts("mu_background", "12_cutset", "max_e_eta", 2.4);
//   	cut_defs["mu_background"]["12_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("mu_background", "12_cutset", "min_jet_pt",30.0);	
//   	cut_defs["mu_background"]["12_cutset"]["min_no_jets"] = 2;
//   	cut_defs["mu_background"]["12_cutset"]["max_no_jets"] = 2;
//   	cut_defs["mu_background"]["12_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["mu_background"]["12_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["12_cutset"]["mu_type"] = 0;
//   	cut_defs["mu_background"]["12_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("mu_background", "12_cutset", "min_loose_mu_pt", 10);
//   	vcuts("mu_background", "12_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("mu_background", "12_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["mu_background"]["12_cutset"]["max_nloose_e"] = 1;
//   	vcuts("mu_background", "12_cutset", "min_loose_e_pt", 15);	
//   	vcuts("mu_background", "12_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("mu_background", "12_cutset", "min_loose_e_relIso", 0.8);
// 
//  	// 13_cutset: global mu_backgrounds
//  	vcuts("mu_background", "13_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "13_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "13_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "13_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "13_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "13_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["13_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["13_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "13_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("mu_background", "13_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("mu_background", "13_cutset", "min_e_pt", 20);	
//         vcuts("mu_background", "13_cutset", "e_electronID", 1.0);
//         vcuts("mu_background", "13_cutset", "min_e_relIso", 0.9);
//   	vcuts("mu_background", "13_cutset", "max_e_eta", 2.4);
//   	cut_defs["mu_background"]["13_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("mu_background", "13_cutset", "min_jet_pt",30.0);	
//   	cut_defs["mu_background"]["13_cutset"]["min_no_jets"] = 3;
//   	cut_defs["mu_background"]["13_cutset"]["max_no_jets"] = 3;
//   	cut_defs["mu_background"]["13_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["mu_background"]["13_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["13_cutset"]["mu_type"] = 0;
//   	cut_defs["mu_background"]["13_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("mu_background", "13_cutset", "min_loose_mu_pt", 10);
//   	vcuts("mu_background", "13_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("mu_background", "13_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["mu_background"]["13_cutset"]["max_nloose_e"] = 1;
//   	vcuts("mu_background", "13_cutset", "min_loose_e_pt", 15);	
//   	vcuts("mu_background", "13_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("mu_background", "13_cutset", "min_loose_e_relIso", 0.8);
// 
//  	// 14_cutset: global mu_backgrounds
//  	vcuts("mu_background", "14_cutset", "min_mu_nHits", 11);
//  	vcuts("mu_background", "14_cutset", "max_mu_chi2", 10);
//   	vcuts("mu_background", "14_cutset", "max_mu_d0sig", 3);
//   	vcuts("mu_background", "14_cutset", "min_mu_pt", 20);
//   	vcuts("mu_background", "14_cutset", "max_mu_eta", 2.1);
//   	vcuts("mu_background", "14_cutset", "min_mu_relIso", 0.95);
//   	cut_defs["mu_background"]["14_cutset"]["max_nisolated_mu"] = 1;
//   	cut_defs["mu_background"]["14_cutset"]["min_nisolated_mu"] = 1;
//  	vcuts("mu_background", "14_cutset", "max_mu_hcal_veto_cone", 6);
//  	vcuts("mu_background", "14_cutset", "max_mu_ecal_veto_cone", 4);
//   	vcuts("mu_background", "14_cutset", "min_e_pt", 20);	
//         vcuts("mu_background", "14_cutset", "e_electronID", 1.0);
//         vcuts("mu_background", "14_cutset", "min_e_relIso", 0.9);
//   	vcuts("mu_background", "14_cutset", "max_e_eta", 2.4);
//   	cut_defs["mu_background"]["14_cutset"]["max_jet_eta"] = 2.4; 
//   	vcuts("mu_background", "14_cutset", "min_jet_pt",30.0);	
//   	cut_defs["mu_background"]["14_cutset"]["min_no_jets"] = 4;
// //  	cut_defs["mu_background"]["14_cutset"]["max_no_jets"] = 7;
//   	cut_defs["mu_background"]["14_cutset"]["max_nisolated_e"] = 0;
//   	cut_defs["mu_background"]["14_cutset"]["min_nisolated_e"] = -1;
//   	cut_defs["mu_background"]["14_cutset"]["mu_type"] = 0;
//   	cut_defs["mu_background"]["14_cutset"]["max_nloose_mu"] = 1;
//   	vcuts("mu_background", "14_cutset", "min_loose_mu_pt", 10);
//   	vcuts("mu_background", "14_cutset", "max_loose_mu_eta", 2.5);
//   	vcuts("mu_background", "14_cutset", "min_loose_mu_relIso", 0.8);
//   	cut_defs["mu_background"]["14_cutset"]["max_nloose_e"] = 1;
//   	vcuts("mu_background", "14_cutset", "min_loose_e_pt", 15);	
//   	vcuts("mu_background", "14_cutset", "max_loose_e_eta", 2.5);
//   	vcuts("mu_background", "14_cutset", "min_loose_e_relIso", 0.8);
// 
//          /*
//           *      ELECTRON CUTS
//           */
// 
//          /*
//           *      ELECTRON BACKGROUND CUTS
//           */
// 
//         /*
//          *      global cuts for all selections defined so far
//          */
//         synchronise_maps();
// 
//         for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
//                 type_iter != cut_defs.end();
//                 ++type_iter)
//         {
//                 for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
//                     set_iter != type_iter->second.end();
//                     ++set_iter)
//                 {
//                         if(type_iter->first == "muon" || type_iter->first == "mu_background"){
// 				vset_if_not_set(type_iter->first, set_iter->first, "trigger", 82);
//                         }else if(type_iter->first == "electron" || type_iter->first == "e_background"){
//                         }
//                 }
//         }
// 
//         /*
//          *      cuts for which the global selection cuts are not applied
//          */
// 	vcuts("muon", "without_cuts", "min_jet_pt", 0.0);
// 	vcuts("mu_background", "without_cuts", "min_jet_pt", 0.0);
// }
