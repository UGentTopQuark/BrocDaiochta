#include "analyzers/EventSelection/interface/BJetFinder.h"

BJetFinder::BJetFinder()
{
	btag_values = new std::vector<std::pair<int, double> >();	
	sorted = false;
	btag_algo_ids.clear();
}

BJetFinder::~BJetFinder()
{
	if(btag_values != NULL){
		delete btag_values;
		btag_values = NULL;
	}
}

std::vector<std::pair<int, double> >* BJetFinder::get_btag_sorted_jets(std::string btag_algo)
{
	if(sorted && (old_min_btag_value[btag_algo] == min_btag) && (btag_algo_ids.find(btag_algo) != btag_algo_ids.end()))
		return btag_algo_ids[btag_algo];
	else{
		sort_btagged_jets(btag_algo);
		return btag_algo_ids[btag_algo];
	}
}

void BJetFinder::set_min_btag_value(std::vector<double> min_btag_value)
{
	min_btag = min_btag_value;
}

bool BJetFinder::compare_btag(const std::pair<int,double> p1, const std::pair<int,double> p2)
{
	return p1.second > p2.second;
}

void BJetFinder::sort_btagged_jets(std::string btag_algo) 
{
	btag_values->clear();

	int i = 0;
	for(std::vector<pat::Jet>::iterator jet = jets->begin();
		jet != jets->end();
		++jet){
    		double bDiscrim = jet->bDiscriminator(btag_algo);
	
		btag_values->push_back(std::pair<int,double>(i,bDiscrim));
		++i;
	}

	std::sort(btag_values->begin(), btag_values->end(), BJetFinder::compare_btag);
	cut_sorted_jets();
	btag_algo_ids[btag_algo] = btag_values;
	sorted = true;
	old_min_btag_value[btag_algo] = min_btag;
}
void BJetFinder::cut_sorted_jets()
{
	//remove all elements of vector which do not pass btag cut
	for(std::vector<std::pair<int,double> >::iterator b_v_iter = btag_values->begin();b_v_iter != btag_values->end();b_v_iter++){
		//std::cout << " btag cut set. point three " << min_btag[0] << std::endl;
		std::vector<double>::iterator min_btag_iter = min_btag.begin();
		
		if((*b_v_iter).second > *min_btag_iter){
			if(min_btag_iter + 1 != min_btag.end())
				min_btag_iter++;

			continue;
		}
		else{
			btag_values->erase(b_v_iter,btag_values->end());
			break;
		}

	}

}
void BJetFinder::set_handles(std::vector<pat::Jet>* jets)
{
	this->jets = jets;
	sorted = false;
	btag_algo_ids.clear();
}
