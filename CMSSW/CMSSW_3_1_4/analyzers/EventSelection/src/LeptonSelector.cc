#include "analyzers/EventSelection/interface/LeptonSelector.h"

template LeptonSelector<pat::Electron>::LeptonSelector();
template LeptonSelector<pat::Electron>::~LeptonSelector();
template LeptonSelector<pat::Muon>::LeptonSelector();
template LeptonSelector<pat::Muon>::~LeptonSelector();

template void LeptonSelector<pat::Electron>::set_max_trackiso(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_max_ecaliso(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_max_caliso(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_max_hcaliso(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_max_hcal_veto_cone(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_max_ecal_veto_cone(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_min_dR(std::vector<double> *min);
template void LeptonSelector<pat::Electron>::set_min_pt(std::vector<double> *min);
template void LeptonSelector<pat::Electron>::set_min_et(std::vector<double> *min);
template void LeptonSelector<pat::Electron>::set_min_relIso(std::vector<double> *min);
template void LeptonSelector<pat::Electron>::set_max_eta(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_electronID(std::vector<double> *eid);
template void LeptonSelector<pat::Electron>::set_max_d0(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_max_d0sig(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_max_chi2(std::vector<double> *max);
template void LeptonSelector<pat::Electron>::set_min_nHits(std::vector<double> *min);
template void LeptonSelector<pat::Electron>::set_lepton_type(int lepton_type);

template void LeptonSelector<pat::Muon>::set_max_trackiso(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_max_ecaliso(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_max_hcaliso(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_max_caliso(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_max_hcal_veto_cone(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_max_ecal_veto_cone(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_min_dR(std::vector<double> *min);
template void LeptonSelector<pat::Muon>::set_min_pt(std::vector<double> *min);
template void LeptonSelector<pat::Muon>::set_min_et(std::vector<double> *min);
template void LeptonSelector<pat::Muon>::set_min_relIso(std::vector<double> *min);
template void LeptonSelector<pat::Muon>::set_max_eta(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_electronID(std::vector<double> *eid);
template void LeptonSelector<pat::Muon>::set_max_d0(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_max_d0sig(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_max_chi2(std::vector<double> *max);
template void LeptonSelector<pat::Muon>::set_min_nHits(std::vector<double> *min);
template void LeptonSelector<pat::Muon>::set_lepton_type(int lepton_type);

template bool LeptonSelector<pat::Electron>::cut_trackiso(const pat::Electron *lepton_iter, double &max_trackiso);
template bool LeptonSelector<pat::Electron>::cut_ecaliso(const pat::Electron *lepton_iter, double &max_ecaliso);
template bool LeptonSelector<pat::Electron>::cut_caliso(const pat::Electron *lepton_iter, double &max_caliso);
template bool LeptonSelector<pat::Electron>::cut_hcaliso(const pat::Electron *lepton_iter, double &max_hcaliso);
template bool LeptonSelector<pat::Electron>::cut_hcal_veto_cone(const pat::Electron *lepton_iter, double &max_hcal_veto_cone);
template bool LeptonSelector<pat::Electron>::cut_ecal_veto_cone(const pat::Electron *lepton_iter, double &max_ecal_veto_cone);
template bool LeptonSelector<pat::Electron>::cut_dR(const pat::Electron *lepton_iter, double &min_dR);
template bool LeptonSelector<pat::Electron>::cut_pt(const pat::Electron *lepton_iter, double &min_pt);
template bool LeptonSelector<pat::Electron>::cut_et(const pat::Electron *lepton_iter, double &min_et);
template bool LeptonSelector<pat::Electron>::cut_eta(const pat::Electron *lepton_iter, double &max_eta);
template bool LeptonSelector<pat::Electron>::cut_relIso(const pat::Electron *lepton_iter, double &min_relIso);
//template bool LeptonSelector<pat::Electron>::cut_chi2(const pat::Electron *lepton_iter, double &max_chi2);
template bool LeptonSelector<pat::Electron>::cut_nHits(const pat::Electron *lepton_iter, double &min_nHits);

template bool LeptonSelector<pat::Muon>::cut_trackiso(const pat::Muon *lepton_iter, double &max_trackiso);
template bool LeptonSelector<pat::Muon>::cut_ecaliso(const pat::Muon *lepton_iter, double &max_ecaliso);
template bool LeptonSelector<pat::Muon>::cut_caliso(const pat::Muon *lepton_iter, double &max_caliso);
template bool LeptonSelector<pat::Muon>::cut_hcaliso(const pat::Muon *lepton_iter, double &max_hcaliso);
template bool LeptonSelector<pat::Muon>::cut_hcal_veto_cone(const pat::Muon *lepton_iter, double &max_hcal_veto_cone);
template bool LeptonSelector<pat::Muon>::cut_ecal_veto_cone(const pat::Muon *lepton_iter, double &max_ecal_veto_cone);
template bool LeptonSelector<pat::Muon>::cut_dR(const pat::Muon *lepton_iter, double &min_dR);
template bool LeptonSelector<pat::Muon>::cut_pt(const pat::Muon *lepton_iter, double &min_pt);
template bool LeptonSelector<pat::Muon>::cut_et(const pat::Muon *lepton_iter, double &min_et);
template bool LeptonSelector<pat::Muon>::cut_eta(const pat::Muon *lepton_iter, double &max_eta);
template bool LeptonSelector<pat::Muon>::cut_relIso(const pat::Muon *lepton_iter, double &min_relIso);
//template bool LeptonSelector<pat::Muon>::cut_chi2(const pat::Muon *lepton_iter, double &max_chi2);
template bool LeptonSelector<pat::Muon>::cut_nHits(const pat::Muon *lepton_iter, double &min_nHits);

template std::vector<pat::Muon>* LeptonSelector<pat::Muon>::get_leptons(edm::Handle<edm::View<pat::Muon> > leptons, edm::Handle<edm::View<pat::Jet> > myjets, edm::Handle<reco::BeamSpot> recoBeamSpotHandle);
template std::vector<pat::Electron>* LeptonSelector<pat::Electron>::get_leptons(edm::Handle<edm::View<pat::Electron> > leptons, edm::Handle<edm::View<pat::Jet> > myjets, edm::Handle<reco::BeamSpot> recoBeamSpotHandle);

template <class myLepton>
LeptonSelector<myLepton>::LeptonSelector()
{
	max_trackiso = NULL;
	max_caliso = NULL;
	max_hcaliso = NULL;
	max_ecaliso = NULL;
	max_ecal_veto_cone = NULL;
	max_hcal_veto_cone = NULL;
	min_dR = NULL;
	min_pt = NULL;
	min_et = NULL;
	min_relIso = NULL;
	electronID = NULL;
	max_chi2 = NULL;
	max_d0 = NULL;
	max_d0sig = NULL;
	min_nHits = NULL;
	lepton_type = -1;
	max_eta = NULL;

	isolated_leptons = NULL;
}

template <class myLepton>
LeptonSelector<myLepton>::~LeptonSelector()
{
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_trackiso(std::vector<double> *max)
{
	max_trackiso = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_ecaliso(std::vector<double> *max)
{
	max_ecaliso = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_caliso(std::vector<double> *max)
{
	max_caliso = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_hcaliso(std::vector<double> *max)
{
	max_hcaliso = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_hcal_veto_cone(std::vector<double> *max)
{
	max_hcal_veto_cone = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_ecal_veto_cone(std::vector<double> *max)
{
	max_ecal_veto_cone = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_dR(std::vector<double> *min)
{
	min_dR = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_pt(std::vector<double> *min)
{
	min_pt = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_et(std::vector<double> *min)
{
	min_et = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_relIso(std::vector<double> *min)
{
	min_relIso = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_eta(std::vector<double> *max)
{
	max_eta = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_lepton_type(int type)
{
	lepton_type = type;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_electronID(std::vector<double> *eid)
{
	electronID = eid;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_nHits(std::vector<double> *min)
{
	min_nHits = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_d0(std::vector<double> *max)
{
	max_d0 = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_d0sig(std::vector<double> *max)
{
	max_d0sig = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_chi2(std::vector<double> *max)
{
	max_chi2 = max;
}

// Here the functions for any cuts which have been set are called
template <class myLepton>
std::vector<myLepton>* LeptonSelector<myLepton>::get_leptons(edm::Handle<edm::View<myLepton> > leptons,
edm::Handle<edm::View<pat::Jet> > myjets, edm::Handle<reco::BeamSpot> recoBeamSpotHandle)
{
	beamSpotHandle = recoBeamSpotHandle;

	bool const force_triggered_lepton=cut_e_trigger || cut_mu_trigger;

	jets = myjets;
        isolated_leptons = new std::vector<myLepton>();

	if((max_trackiso != NULL && max_trackiso->size() > 1) ||
	   (max_ecaliso != NULL && max_ecaliso->size() > 1) ||
	   (max_caliso != NULL && max_caliso->size() > 1) ||
	   (max_hcaliso != NULL && max_hcaliso->size() > 1) ||
	   (max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > 1) ||
	   (max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > 1) ||
	   (min_dR != NULL && min_dR->size() > 1) ||
	   (min_relIso != NULL && min_relIso->size() > 1) ||
	   (min_pt != NULL && min_pt->size() >1) ||
	   (min_et != NULL && min_et->size() >1) ||
	   (max_d0 != NULL && max_d0->size() >1) ||
	   (max_eta != NULL && max_eta->size() >1) ||
	   (max_d0sig != NULL && max_d0sig->size() >1) ||
	   (max_chi2 != NULL && max_chi2->size() >1) ||
	   (min_nHits != NULL && min_nHits->size() >1) ||
	   (electronID != NULL && electronID->size() >1)){

		// copy of leptons to be able to delete the ones that pass the
		// currently tightest cut
		std::vector<const myLepton*> tmp_leptons;
		for(typename edm::View<myLepton>::const_iterator lepton_iter = leptons->begin(); lepton_iter!=leptons->end();
		++lepton_iter){
			tmp_leptons.push_back(&(*lepton_iter));
		}

		double max_size = max_trackiso->size();
		if(max_ecaliso != NULL && max_ecaliso->size() > max_size) max_size = max_ecaliso->size();
		if(max_caliso != NULL && max_caliso->size() > max_size) max_size = max_caliso->size();
		if(max_hcaliso != NULL && max_hcaliso->size() > max_size) max_size = max_hcaliso->size();
		if(max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > max_size) max_size = max_hcal_veto_cone->size();
		if(max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > max_size) max_size = max_ecal_veto_cone->size();
		if(min_dR != NULL && min_dR->size() > max_size) max_size = min_dR->size();
		if(min_relIso != NULL && min_relIso->size() > max_size) max_size = min_relIso->size();
		if(min_pt != NULL && min_pt->size() > max_size) max_size = min_pt->size();
		if(min_et != NULL && min_et->size() > max_size) max_size = min_et->size();
	   	if(electronID != NULL && electronID->size() > max_size) max_size = electronID->size();
	   	if(max_chi2 != NULL && max_chi2->size() > max_size) max_size = max_chi2->size();
	   	if(max_d0 != NULL && max_d0->size() > max_size) max_size = max_d0->size();
	   	if(max_d0sig != NULL && max_d0sig->size() > max_size) max_size = max_d0sig->size();
	   	if(min_nHits != NULL && min_nHits->size() > max_size) max_size = min_nHits->size();
	   	if(max_eta != NULL && max_eta->size() > max_size) max_size = max_eta->size();

		for(unsigned int nasym_cut = 0; nasym_cut < max_size; ++nasym_cut){
        		typename std::vector<const myLepton*>::iterator lepton_iter;
        		for(lepton_iter = tmp_leptons.begin();
			lepton_iter!=tmp_leptons.end();){
        		        double cut_out=false;
			 
        		        if(max_trackiso != NULL && max_trackiso->size() > nasym_cut && (*max_trackiso)[nasym_cut] != -1)
					cut_out = cut_out || cut_trackiso(*lepton_iter, (*max_trackiso)[nasym_cut]);
        		        if(max_ecaliso != NULL && max_ecaliso->size() > nasym_cut && (*max_ecaliso)[nasym_cut] != -1)
					cut_out = cut_out || cut_ecaliso(*lepton_iter, (*max_ecaliso)[nasym_cut]);
        		        if(max_caliso != NULL && max_caliso->size() > nasym_cut && (*max_caliso)[nasym_cut] != -1)
					cut_out = cut_out || cut_caliso(*lepton_iter, (*max_caliso)[nasym_cut]);
	       		        if(max_hcaliso != NULL && max_hcaliso->size() > nasym_cut && (*max_hcaliso)[nasym_cut] != -1)
					cut_out = cut_out || cut_hcaliso(*lepton_iter,  (*max_hcaliso)[nasym_cut]);
	       		        if(max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > nasym_cut && (*max_hcal_veto_cone)[nasym_cut] != -1)
					cut_out = cut_out || cut_hcal_veto_cone(*lepton_iter,  (*max_hcal_veto_cone)[nasym_cut]);
	       		        if(max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > nasym_cut && (*max_ecal_veto_cone)[nasym_cut] != -1)
					cut_out = cut_out || cut_ecal_veto_cone(*lepton_iter,  (*max_ecal_veto_cone)[nasym_cut]);
        		        if(min_dR != NULL && min_dR->size() > nasym_cut && (*min_dR)[nasym_cut] != -1)
					cut_out = cut_out || cut_dR(*lepton_iter,  (*min_dR)[nasym_cut]);
        		        if(min_relIso != NULL && min_relIso->size() > nasym_cut && (*min_relIso)[nasym_cut] != -1)
					cut_out = cut_out || cut_relIso(*lepton_iter,  (*min_relIso)[nasym_cut]);
				if(min_pt != NULL && min_pt->size() > nasym_cut && (*min_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_pt(*lepton_iter, (*min_pt)[nasym_cut]);
				if(min_et != NULL && min_et->size() > nasym_cut && (*min_et)[nasym_cut] != -1)
					cut_out = cut_out || cut_et(*lepton_iter, (*min_et)[nasym_cut]);
				if(electronID != NULL && electronID->size() > nasym_cut && (*electronID)[nasym_cut] != -1)
					cut_out = cut_out || cut_electronID(*lepton_iter, (*electronID)[nasym_cut]);
				if(max_chi2 != NULL && max_chi2->size() > nasym_cut && (*max_chi2)[nasym_cut] != -1)
					cut_out = cut_out || cut_chi2(*lepton_iter, (*max_chi2)[nasym_cut]);
				if(max_d0 != NULL && max_d0->size() > nasym_cut && (*max_d0)[nasym_cut] != -1)
					cut_out = cut_out || cut_d0(*lepton_iter, (*max_d0)[nasym_cut]);
				if(max_d0sig != NULL && max_d0sig->size() > nasym_cut && (*max_d0sig)[nasym_cut] != -1)
					cut_out = cut_out || cut_d0sig(*lepton_iter, (*max_d0sig)[nasym_cut]);
				if(min_nHits != NULL && min_nHits->size() > nasym_cut && (*min_nHits)[nasym_cut] != -1)
					cut_out = cut_out || cut_nHits(*lepton_iter, (*min_nHits)[nasym_cut]);
				if(max_eta != NULL && max_eta->size() > nasym_cut && (*max_eta)[nasym_cut] != -1)
					cut_out = cut_out || cut_eta(*lepton_iter, (*max_eta)[nasym_cut]);
				if(lepton_type != -1)
					cut_out = cut_out || cut_lepton_type(*lepton_iter, lepton_type);
				if(force_triggered_lepton)
					cut_out = cut_out || cut_trigger(*lepton_iter);

        		        if(!cut_out)
        		        {
        		                isolated_leptons->push_back(**lepton_iter);
					lepton_iter=tmp_leptons.erase(lepton_iter);
        		        }else{
					lepton_iter++;
				}
        		}

			if(isolated_leptons->size() < nasym_cut+1)
				return isolated_leptons;
		}
	}
	else{
        	typename edm::View<myLepton>::const_iterator lepton_iter;
        	for(lepton_iter = leptons->begin(); lepton_iter!=leptons->end(); ++lepton_iter){
        	        double cut_out=false;
		 
        	        if(max_trackiso != NULL && max_trackiso->size() > 0 && (*max_trackiso)[0] != -1)
				cut_out = cut_out || cut_trackiso(&(*lepton_iter), (*max_trackiso)[0]);
        	        if(max_ecaliso != NULL && max_ecaliso->size() > 0 && (*max_ecaliso)[0] != -1)
				cut_out = cut_out || cut_ecaliso(&(*lepton_iter), (*max_ecaliso)[0]);
        	        if(max_caliso != NULL && max_caliso->size() > 0 && (*max_caliso)[0] != -1)
				cut_out = cut_out || cut_caliso(&(*lepton_iter), (*max_caliso)[0]);
        	        if(max_hcaliso != NULL && max_hcaliso->size() > 0 && (*max_hcaliso)[0] != -1)
				cut_out = cut_out || cut_hcaliso(&(*lepton_iter), (*max_hcaliso)[0]);
        	        if(max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > 0 && (*max_hcal_veto_cone)[0] != -1)
				cut_out = cut_out || cut_hcal_veto_cone(&(*lepton_iter), (*max_hcal_veto_cone)[0]);
        	        if(max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > 0 && (*max_ecal_veto_cone)[0] != -1)
				cut_out = cut_out || cut_ecal_veto_cone(&(*lepton_iter), (*max_ecal_veto_cone)[0]);
        	        if(min_dR != NULL && min_dR->size() > 0 && (*min_dR)[0] != -1)
				cut_out = cut_out || cut_dR(&(*lepton_iter), (*min_dR)[0]);
        	        if(min_relIso != NULL && min_relIso->size() > 0 && (*min_relIso)[0] != -1)
				cut_out = cut_out || cut_relIso(&(*lepton_iter), (*min_relIso)[0]);
			if(min_pt != NULL && min_pt->size() > 0 && (*min_pt)[0] != -1)
				cut_out = cut_out || cut_pt(&(*lepton_iter), (*min_pt)[0]);
			if(min_et != NULL && min_et->size() > 0 && (*min_et)[0] != -1)
				cut_out = cut_out || cut_et(&(*lepton_iter), (*min_et)[0]);
			if(electronID != NULL && electronID->size() > 0 && (*electronID)[0] != -1)
				cut_out = cut_out || cut_electronID(&(*lepton_iter), (*electronID)[0]);
        	        if(min_nHits != NULL && min_nHits->size() > 0 && (*min_nHits)[0] != -1)
				cut_out = cut_out || cut_nHits(&(*lepton_iter), (*min_nHits)[0]);
        	        if(max_chi2 != NULL && max_chi2->size() > 0 && (*max_chi2)[0] != -1)
				cut_out = cut_out || cut_chi2(&(*lepton_iter), (*max_chi2)[0]);
        	        if(max_d0sig != NULL && max_d0sig->size() > 0 && (*max_d0sig)[0] != -1)
				cut_out = cut_out || cut_d0sig(&(*lepton_iter), (*max_d0sig)[0]);
			if(max_eta != NULL && max_eta->size() > 0 && (*max_eta)[0] != -1)
				cut_out = cut_out || cut_eta(&(*lepton_iter), (*max_eta)[0]);
			if(lepton_type != -1)
				cut_out = cut_out || cut_lepton_type(&(*lepton_iter), lepton_type);
			if(force_triggered_lepton)
				cut_out = cut_out || cut_trigger(&(*lepton_iter));

        	        if(!cut_out)
        	        {
        	                isolated_leptons->push_back(*lepton_iter);
        	        }
        	}
	}

	if(verbose)
		std::cout << "isolated leptons in event: " << isolated_leptons->size()  << std::endl;
 
        return isolated_leptons;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_chi2(const myLepton *lepton_iter, double &max_chi2)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_chi2(), dropping ALL LEPTONS" << std::endl;
	return true;
}

template <>
bool LeptonSelector<pat::Muon>::cut_chi2(const pat::Muon *lepton_iter, double &max_chi2)
{
	reco::TrackRef track  = lepton_iter->globalTrack();
	if(!track.isNull() && (lepton_iter->globalTrack()->chi2() / lepton_iter->globalTrack()->ndof()) < max_chi2){
		if(verbose){
			std::cout << "cut chi2: " << lepton_iter->globalTrack()->chi2() / lepton_iter->globalTrack()->ndof() << " max_chi2: " << max_chi2 << std::endl;
			std::cout << "cut false." << std::endl;
		}
		return false;
	}
	else{
		if(verbose){
			std::cout << "cut chi2: " << lepton_iter->globalTrack()->chi2() / lepton_iter->globalTrack()->ndof() << " max_chi2: " << max_chi2 << std::endl;
			std::cout << "cut true." << std::endl;
		}
		return true;
	}
}

template <>
bool LeptonSelector<pat::Electron>::cut_chi2(const pat::Electron *lepton_iter, double &max_chi2)
{
	reco::TrackRef track  = lepton_iter->track();
	if(!track.isNull() && (lepton_iter->track()->chi2() / lepton_iter->track()->ndof()) < max_chi2){
		if(verbose){
			std::cout << "cut chi2: " << lepton_iter->track()->chi2() / lepton_iter->track()->ndof() << " max_chi2: " << max_chi2 << std::endl;
			std::cout << "cut false." << std::endl;
		}
		return false;
	}
	else{
		if(verbose){
			std::cout << "cut chi2: " << lepton_iter->track()->chi2() / lepton_iter->track()->ndof() << " max_chi2: " << max_chi2 << std::endl;
			std::cout << "cut true." << std::endl;
		}
		return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_d0(const myLepton *lepton_iter, double &max_d0)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_d0()" << std::endl;
	return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_d0sig(const myLepton *lepton_iter, double &max_d0sig)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_d0sig()" << std::endl;
	return true;
}

template <>
bool LeptonSelector<pat::Muon>::cut_d0(const pat::Muon *lepton_iter, double &max_d0)
{
	reco::TrackRef track  = lepton_iter->track();

	double d0 = -1000;

	if(beamSpotHandle.isValid()){
		if(!track.isNull() && track.isAvailable()){
			reco::BeamSpot beamSpot=*beamSpotHandle;
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
			d0 = -1.*track->dxy(point);
		}else{
			std::cerr << "WARNING: LeptonSelector::cut_d0(): track not available" << std::endl;
		}
	}else{
		std::cerr << "WARNING: LeptonSelector::cut_d0(): no valid beamspot found" << std::endl;
	}

	if(fabs(d0) < max_d0){
		if(verbose){
			std::cout << "cut d0: " << fabs(d0) << " max_d0: " << max_d0 << std::endl;
			std::cout << "cut false." << std::endl;
		}
		return false;
	}
	else{
		if(verbose){
			std::cout << "cut d0: " << fabs(d0) << " max_d0: " << max_d0 << std::endl;
			std::cout << "cut true." << std::endl;
		}

		return true;
	}
}

template <>
bool LeptonSelector<pat::Electron>::cut_d0(const pat::Electron *lepton_iter, double &max_d0)
{
	reco::GsfTrackRef track  = lepton_iter->gsfTrack();

	double d0 = -1000;

	if(beamSpotHandle.isValid()){
		if(!track.isNull() && track.isAvailable()){
			reco::BeamSpot beamSpot=*beamSpotHandle;
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
			d0 = -1.*track->dxy(point);
		}
	}else{
		std::cerr << "WARNING: LeptonSelector::cut_d0(): no valid beamspot found" << std::endl;
	}

	if(fabs(d0) < max_d0)
		return false;
	else
		return true;
}

template <>
bool LeptonSelector<pat::Muon>::cut_d0sig(const pat::Muon *lepton_iter, double &max_d0sig)
{
	reco::TrackRef track  = lepton_iter->track();

	double d0sig = -1000;

	if(beamSpotHandle.isValid()){
		if(!track.isNull() && track.isAvailable()){
			reco::BeamSpot beamSpot=*beamSpotHandle;
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
			double d0 = -1.*track->dxy(point);
			double d0sigma = sqrt( track->d0Error() * track->d0Error() + sqrt(beamSpot.BeamWidthX()*beamSpot.BeamWidthX()+beamSpot.BeamWidthY()*beamSpot.BeamWidthY()) );
			d0sig = d0/d0sigma;
		}else{
			std::cerr << "WARNING: LeptonSelector: muon track not available" << std::endl;
		}
	}else{
		std::cerr << "WARNING: LeptonSelector::cut_d0(): no valid beamspot found" << std::endl;
	}

	if(fabs(d0sig) < max_d0sig){
		if(verbose){
			std::cout << "cut d0sig: " << fabs(d0sig) << " max_d0sig: " << max_d0sig << std::endl;
			std::cout << "cut false." << std::endl;
		}
		return false;
	}
	else{
		if(verbose){
			std::cout << "cut d0sig: " << fabs(d0sig) << " max_d0sig: " << max_d0sig << std::endl;
			std::cout << "cut true." << std::endl;
		}
		return true;
	}
}

template <>
bool LeptonSelector<pat::Electron>::cut_d0sig(const pat::Electron *lepton_iter, double &max_d0sig)
{
	reco::GsfTrackRef track  = lepton_iter->gsfTrack();

	double d0sig = -1000;

	if(beamSpotHandle.isValid()){
		if(!track.isNull() && track.isAvailable()){
			reco::BeamSpot beamSpot=*beamSpotHandle;
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
			double d0 = -1.*track->dxy(point);
			double d0sigma = sqrt( track->d0Error() * track->d0Error() + sqrt(beamSpot.BeamWidthX()*beamSpot.BeamWidthX()+beamSpot.BeamWidthY()*beamSpot.BeamWidthY()) );
			d0sig = d0/d0sigma;
		}else{
			std::cerr << "WARNING: LeptonSelector: electron track not available" << std::endl;
		}
	}else{
		std::cerr << "WARNING: LeptonSelector::cut_d0(): no valid beamspot found" << std::endl;
	}

	if(fabs(d0sig) < max_d0sig)
		return false;
	else
		return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_trigger(const myLepton *lepton_iter)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_trigger()" << std::endl;
		return true;
}

template <>
bool LeptonSelector<pat::Electron>::cut_trigger(const pat::Electron *lepton_iter)
{
	std::string electron_trigger = "hltL1NonIsoHLTNonIsoSingleElectronLWEt15TrackIsolFilter";

	if(lepton_iter->triggerObjectMatchesByFilter(electron_trigger).size() != 0 || !cut_e_trigger)
		return false;
	else
		return true;
}

template <>
bool LeptonSelector<pat::Muon>::cut_trigger(const pat::Muon *lepton_iter)
{
	std::string muon_trigger = "hltSingleMuNoIsoL3PreFiltered11";
// 	const std::vector<pat::TriggerPrimitive> &triggers =  lepton_iter->triggerMatches();
// 	std::cout << "Num Triggers Muon: " << triggers.size() << std::endl;
// 	for (size_t j = 0; j < triggers.size(); ++j)
// 		{
// 			std::cout << triggers[j].filterName() << std::endl;
// 		}

	if(lepton_iter->triggerObjectMatchesByFilter(muon_trigger).size() != 0 || !cut_mu_trigger){
		if(verbose){
			std::cout << "cut trigger: " << lepton_iter->triggerObjectMatchesByFilter(muon_trigger).size() << " tigger: " << 0 << std::endl;
			std::cout << "cut false." << std::endl;
		}
		return false;
	}
	else{
		if(verbose){
			std::cout << "cut trigger: " << lepton_iter->triggerObjectMatchesByFilter(muon_trigger).size() << " tigger: " << 0 << std::endl;
			std::cout << "cut true." << std::endl;
		}
		return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nHits(const myLepton *lepton_iter, double &min_nHits)
{
	reco::TrackRef track  = lepton_iter->track();
        if((!track.isNull()) && (lepton_iter->track()->numberOfValidHits()) >= min_nHits){
		if(verbose){
			std::cout << "cut nhits: " << (lepton_iter->track()->numberOfValidHits()) << " min_nHits: " << min_nHits << std::endl;
			std::cout << "cut false." << std::endl;
		}
                return false;
	}
        else{
		if(verbose){
			std::cout << "cut nhits: " << (lepton_iter->track()->numberOfValidHits()) << " min_nHits: " << min_nHits << std::endl;
			std::cout << "cut true." << std::endl;
		}
                return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_electronID(const myLepton *lepton_iter, double &electronID)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_electronID()" << std::endl;
	return true;
}

template <>
bool LeptonSelector<pat::Muon>::cut_electronID(const pat::Muon *lepton_iter, double &electronID)
{
	std::cerr << "WARNING: electonID set for muons" << std::endl;
	return true;
}

template <>
bool LeptonSelector<pat::Electron>::cut_electronID(const pat::Electron *lepton_iter, double &electronID)
{
	std::string eid;
	int electronID_int = (int) electronID;
	switch(electronID_int){
		case 0: eid = "eidRobustLoose";
			break;
		case 1: eid = "eidRobustTight";
			break;
		case 2: eid = "eidLoose";
			break;
		case 3: eid = "eidTight";
			break;
		default: std::cerr << "WARNING: No valid electronID in LeptonSelector" << std::endl;
	}

	if(lepton_iter->isElectronIDAvailable(eid)){
		if(lepton_iter->electronID(eid) == 1.0)
			return false;
		else
			return true;
	}else{
		std::cerr << "WARNING: LeptonSelector: electron ID not available" << std::endl;
		return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_lepton_type(const myLepton *lepton_iter, int &lepton_type)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_lepton_type()" << std::endl;
	return true;
}

template <>
bool LeptonSelector<pat::Muon>::cut_lepton_type(const pat::Muon *lepton_iter, int &lepton_type)
{
	bool cut_out = false;
	switch(lepton_type){
		case 0:
			if(!lepton_iter->isGlobalMuon())
				cut_out = true;
			break;
		case 1:
			if(!lepton_iter->isStandAloneMuon())
				cut_out = true;
			break;
		case 2:
			if(!lepton_iter->isTrackerMuon())
				cut_out = true;
			break;
		default:
			cut_out = true;
			std::cerr << "WARNING: lepton type is not valid in LeptonSelector::cut_lepton_type()" << std::endl;
			break;
	}

	if(verbose){
		std::cout << "cut lepton_type (is glob muon?): " << lepton_iter->isGlobalMuon() << " type: " << lepton_type << std::endl;

		if(cut_out){
			std::cout << "cut true." << std::endl;
		}else
			std::cout << "cut false." << std::endl;
	}

	return cut_out;
}

template <>
bool LeptonSelector<pat::Electron>::cut_lepton_type(const pat::Electron *lepton_iter, int &lepton_type)
{
	std::cerr << "WARNING: lepton_type unknown for electrons: FILTERING ALL ELECTRONS" << std::endl;
	return true;
}


template <typename myLepton>
bool LeptonSelector<myLepton>::cut_trackiso(const myLepton *lepton_iter, double &max_trackiso)
{
        if(lepton_iter->trackIso()<max_trackiso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_ecal_veto_cone(const myLepton *lepton_iter, double &max_ecal_veto_cone)
{
        if(lepton_iter->ecalIsoDeposit()->candEnergy() < max_ecal_veto_cone)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_hcal_veto_cone(const myLepton *lepton_iter, double &max_hcal_veto_cone)
{
        if(lepton_iter->hcalIsoDeposit()->candEnergy() < max_hcal_veto_cone)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_ecaliso(const myLepton *lepton_iter, double &max_ecaliso)
{
        if(lepton_iter->ecalIso() < max_ecaliso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_caliso(const myLepton *lepton_iter, double &max_caliso)
{
        if(lepton_iter->caloIso() < max_caliso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_eta(const myLepton *lepton_iter, double &max_eta)
{
        if(fabs(lepton_iter->eta()) < max_eta){
		if(verbose){
			std::cout << "cut eta: " << fabs(lepton_iter->eta()) << " max_eta: " << max_eta << std::endl;
			std::cout << "cut false." << std::endl;
		}
                return false;
	}
        else{
		if(verbose){
			std::cout << "cut eta: " << fabs(lepton_iter->eta()) << " max_eta: " << max_eta << std::endl;
			std::cout << "cut true." << std::endl;
		}
                return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_hcaliso(const myLepton *lepton_iter, double &max_hcaliso)
{
        if(lepton_iter->hcalIso() < max_hcaliso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_pt(const myLepton *lepton_iter, double &min_pt)
{
        if(lepton_iter->pt() > min_pt){
		if(verbose){
			std::cout << "cut pt: " << fabs(lepton_iter->pt()) << " min_pt: " << min_pt << std::endl;
			std::cout << "cut false." << std::endl;
		}
                return false;
	}
        else{
		if(verbose){
			std::cout << "cut pt: " << fabs(lepton_iter->pt()) << " min_pt: " << min_pt << std::endl;
			std::cout << "cut true." << std::endl;
		}
                return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_et(const myLepton *lepton_iter, double &min_et)
{
        if(lepton_iter->et() > min_et){
		if(verbose){
			std::cout << "cut et: " << fabs(lepton_iter->et()) << " min_et: " << min_et << std::endl;
			std::cout << "cut false." << std::endl;
		}
                return false;
	}
        else{
		if(verbose){
			std::cout << "cut et: " << fabs(lepton_iter->et()) << " min_et: " << min_et << std::endl;
			std::cout << "cut true." << std::endl;
		}
                return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_dR(const myLepton *lepton_iter, double &min_dR)
{
        for(edm::View<pat::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter)
          {
            double dR = deltaR(lepton_iter->p4(),jet_iter->p4());

            if(dR < min_dR)
              return true;
          }

        return false;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_relIso(const myLepton *lepton_iter, double &min_relIso)
{
	double relIso=0;
	relIso = lepton_iter->pt()/(lepton_iter->pt() + lepton_iter->ecalIso()
		 + lepton_iter->hcalIso() + lepton_iter->trackIso());

	if(relIso <= min_relIso){
		if(verbose){
			std::cout << "cut relIso: " << relIso << " min_relIso: " << min_relIso << std::endl;
			std::cout << "cut true." << std::endl;
		}
		return true;
	}
	else{
		if(verbose){
			std::cout << "cut relIso: " << relIso << " min_relIso: " << min_relIso << std::endl;
			std::cout << "cut false." << std::endl;
		}
        	return false;
	}
}

template <typename myLepton>
double LeptonSelector<myLepton>::deltaR(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
		    ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2)
{
          double delta_phi = ROOT::Math::VectorUtil::DeltaPhi(p4_1,p4_2);
          double delta_eta = p4_1.eta()-p4_2.eta();
          double dR = sqrt(delta_eta * delta_eta + delta_phi * delta_phi);
          return dR;

}
