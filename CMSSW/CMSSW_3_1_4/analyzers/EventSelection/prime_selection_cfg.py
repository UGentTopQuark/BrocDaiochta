
import FWCore.ParameterSet.Config as cms

process = cms.Process("eventselection")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
    'dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/6_PYTHIA6_Tprime_10TeV_cff_PAT_prod2_1.root',
    'dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/1_PYTHIA6_TTbar_10TeV_cff_PAT_1.root',
    'dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/0_PYTHIA6_TTbar_10TeV_cff_PAT_1.root',
    'dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/2_PYTHIA6_TTbar_10TeV_cff_PAT_1.root','dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/3_PYTHIA6_TTbar_10TeV_cff_PAT_1.root','dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/4_PYTHIA6_TTbar_10TeV_cff_PAT_1.root','dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/5_PYTHIA6_TTbar_10TeV_cff_PAT_1.root','dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/6_PYTHIA6_TTbar_10TeV_cff_PAT_1.root','dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/7_PYTHIA6_TTbar_10TeV_cff_PAT_1.root','dcap://pnfs/iihe/cms/store/user/bklein/tprime_250_PAT/8_PYTHIA6_TTbar_10TeV_cff_PAT_1.root')
)

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(1000) )
  
process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttGenEvent_cff")
## enable additional per-event printout from the TtSemiLeptonicEvent
#process.ttSemiLepEvent.verbosity = 1
## change maximum number of jets taken into account per event (default: 4)
#process.ttSemiLepEvent.maxNJets = 5
process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttSemiLepEvtBuilder_cff")
#process.ttSemiLepHypGenMatch.leps = "selectedLayer1Electrons"

## process.load( "HLTrigger.HLTcore.hltEventAnalyzerAOD_cfi" )
## process.hltEventAnalyzerAOD.triggerName = cms.string("HLT_Mu11")
## process.load( "HLTrigger.HLTcore.triggerSummaryAnalyzerAOD_cfi" )

#Run trigReport to get information on all triggers run and passed for this job in stdout
#process.load( "HLTrigger.HLTanalyzers.hlTrigReport_cfi" )
#process.hlTrigReport.triggerName = cms.string("HLT_Mu11")

## process.p1 = cms.Path(
##     process.hltEventAnalyzerAOD       +
##     process.triggerSummaryAnalyzerAOD
## )




process.MessageLogger = cms.Service("MessageLogger")

process.eventselection = cms.EDAnalyzer("EventSelection",
    electronTag = cms.untracked.InputTag("selectedLayer1Electrons"),
    tauTag      = cms.untracked.InputTag("selectedLayer1Taus"),
    muonTag     = cms.untracked.InputTag("selectedLayer1Muons"),
    jetTag      = cms.untracked.InputTag("selectedLayer1Jets"),
    photonTag   = cms.untracked.InputTag("selectedLayer1Photons"),
    metTag      = cms.untracked.InputTag("selectedLayer1METs"),                          
    semiLepTag      = cms.untracked.InputTag("ttSemiLepEvent"),
    hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypGenMatch:Key"),
    #hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypKinFit:Key"), 
    HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT' ),                                   
    datasetName = cms.untracked.string("Tprime250"),
    tprimeMass = cms.untracked.double(250.0)
)

process.TFileService = cms.Service("TFileService", fileName = cms.string('tprime.root') )

process.p0 = cms.Path(process.makeGenEvt * process.makeTtSemiLepEvent)

process.p2 = cms.Path(process.eventselection)

process.schedule = cms.Schedule( process.p0,
                                 #process.p1,
                                 process.p2 )
