#ifndef PLOTGENERATOR_H
#define PLOTGENERATOR_H

#include "DataFormats/Common/interface/View.h"
#include "FWCore/Framework/interface/Event.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
// #include "DataFormats/PatCandidates/interface/TriggerPrimitive.h" 
#include "DataFormats/BeamSpot/interface/BeamSpot.h"

#include "analyzers/EventSelection/interface/Cuts.h"
#include "analyzers/EventSelection/interface/LeptonSelector.h"
#include "analyzers/EventSelection/interface/MassReconstruction.h"
#include "analyzers/EventSelection/interface/METCorrector.h"
#include "analyzers/EventSelection/interface/MassJetMatch.h"
#include "analyzers/EventSelection/interface/GenMatch.h"
#include "analyzers/EventSelection/interface/RecoGenMatch.h"
#include "analyzers/EventSelection/interface/SynchronisationExercise.h"
#include "analyzers/EventSelection/interface/METPlots.h"

//for truth matching TQAF
// TtEvent necessary for new versions of PAT
//#include "AnalysisDataFormats/TopObjects/interface/TtEvent.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

//#include "TopQuarkAnalysis/Examples/plugins/HypothesisAnalyzer.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include "TMath.h"

#include "TH1D.h"
#include "TH2D.h"
#include <map>
#include "TGraph.h"

// FIXME: METCHECK this can be removed later on
#include "analyzers/EventSelection/interface/MyMEzCalculator.h"
//#include "TopQuarkAnalysis/TopTools/interface/MEzCalculator.h"

class PlotGenerator{
	public:
		PlotGenerator(std::string ident);
		~PlotGenerator();
		void set_handles(edm::Handle<edm::View<pat::Muon> > analyser_muons,
                           edm::Handle<edm::View<pat::Jet> > analyser_jets,
                           edm::Handle<edm::View<pat::Electron> > analyser_electrons,
			   edm::Handle<edm::View<pat::MET> > analyser_mets,
			   edm::Handle<TtSemiLeptonicEvent> semiLepEvt,
			   edm::Handle<int> hypoClassKeyHandle,
			   edm::Handle<TtGenEvent> genEvt,
			   edm::Handle<edm::TriggerResults> HLTR,
			   edm::Handle<reco::GenParticleCollection> genParticles,
			   edm::Handle<reco::BeamSpot> recoBeamSpotHandle);
		void plot();
		void apply_cuts(Cuts *mycuts);
		void deactivate_cuts();
		void set_tprime_mass(double mass);

	private:
		void calculate_ht();
		void plot_jets();
		void plot_muons();
		void plot_electrons();
		void plot_ht();
       		void plot_masstop();
		void plot_Kinfit_top();
		void plot_minmass_1btag_top();
		void plot_minmass_2btag_top();
		void plot_chimass_top_012btag();
		void plot_chimass_top();
		void plot_chimass_1btag_top();
		void plot_chimass_2btag_top();
		void plot_genmass_top();
		void plot_recogenmatch_top();
		void plot_massW(double nominal_massW);
		void plot_Wb_mass();
		void plot_MET_MtW();
		void book_histos();
		void plot_leptIso_electron();
		void plot_leptIso_muon();
		void plot_muon_quality();
		void plot_dR();
		void matchleptons_plotiso_plotveto();
		void set_lepton_selector_cuts();
		void unset_lepton_selector_cuts();
		void plot_trigger();
		void plot_bDiscriminator();
		void plot_bDiscriminator_find_btagcut();
		void plot_bDiscriminator_allbjets();
		void plot_MCmethod_comparison();
		void analyse_btag_algorithms();
		void get_triggereff();
		void set_btag_cuts(std::vector<double> *min, double n);
		void get_btagInfo();
		void analyse_recogen_mindD();
		void analyse_recogen_matchedjets();
		void plot_massjetmatch_top();
		double deltaR(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
		    ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2);
		bool static compare_btag(const std::pair<int,double> p1, const std::pair<int,double> p2);

		edm::Handle<edm::View<pat::Electron> > electrons;
		edm::Handle<edm::View<pat::Jet> > uncut_jets;
		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* isolated_electrons;
		std::vector<pat::Electron>* all_electrons;
		std::vector<pat::Muon>* isolated_muons;
		std::vector<pat::MET>* corrected_mets;
		std::vector<pat::MET>* uncorrected_mets;
		edm::Handle<edm::View<pat::Muon> > muons;
		edm::Handle<edm::View<pat::MET> > mets;
		edm::Handle<TtSemiLeptonicEvent> semiLepEvt;
		edm::Handle<int> hypoClassKeyHandle;
		edm::Handle<TtGenEvent> genEvt;
		edm::Handle<edm::TriggerResults> HLTR;
		edm::Handle<reco::GenParticleCollection> genParticles;
		edm::Handle<reco::BeamSpot> beamSpotHandle;

		std::map<std::string,TH1D*> histos1d;
		std::map<std::string,TH2D*> histos2d;
		std::map<std::string,TGraph*> Tgraphs;

		Cuts *cuts;
		LeptonSelector<pat::Electron> *e_selector;
		LeptonSelector<pat::Electron> *all_e_selector;
		LeptonSelector<pat::Muon> *mu_selector;
		JetSelector *jet_selector;
		MassReconstruction *mass_reco;
		MassReconstruction *mass_reco_corMET;
                METCorrector *METCor;
		MassJetMatch *mass_jet_match;
		GenMatch *gen_match;
		RecoGenMatch *reco_gen_match;
		SynchronisationExercise *sync_ex;
		BJetFinder *bjet_finder;

		// plot classes
		METPlots *met_plots;

		//get these values from Cuts.cc
		std::vector<double> *min_btag;
		std::string name_btag;

		// to avoid calculating H_T several times... this should be solved properly one day
		double ht;

		double tprime_mass;
	
		// to distinguish the plots of multiple instances of the class
		// (muon-channel, electron-channel...
		std::string id;
};

#endif
