#ifndef SYNCHRONISATIONEXERCISE_H
#define SYNCHRONISATIONEXERCISE_H

#include "DataFormats/Common/interface/View.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "analyzers/EventSelection/interface/MassReconstruction.h"
#include "analyzers/EventSelection/interface/JetSelector.h"
#include "analyzers/EventSelection/interface/LeptonSelector.h"
#include "analyzers/EventSelection/interface/Tools.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include <TH1D.h>

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"

class SynchronisationExercise{
	public:
		SynchronisationExercise();
		~SynchronisationExercise();
		void set_handles(edm::Handle<edm::View<pat::Muon> > all_mus, edm::Handle<edm::View<pat::Electron> > all_es, edm::Handle<edm::View<pat::Jet> > all_jets, edm::Handle<reco::BeamSpot> recoBeamSpotHandle);
		void print();
		void set_mass_reconstruction(MassReconstruction *mass_reco);
		void set_ht(double ht);
		void set_ident(std::string ident);

	private:
		void print_jets();
		void print_electrons();
		void print_muons();
		void print_masses();
		void print_value(std::string variable, double value, bool integer=false);
		void book_histos();

		void set_selection_cuts(std::string ident);

		std::string ident;

		LeptonSelector<pat::Electron> *e_selector;
		LeptonSelector<pat::Electron> *cleaning_e_selector; // electrons for jet cleaning
		LeptonSelector<pat::Muon> *mu_selector;
		JetSelector *jet_selector;
		edm::Handle<reco::BeamSpot> beamSpotHandle;

		int event_counter;
		double ht;
		MassReconstruction *mass_reco;
		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* electrons;
		std::vector<pat::Electron>* cleaning_electrons;
		std::vector<pat::Muon>* muons;
		std::vector<pat::MET>* mets;

		Tools *tool;

		std::string pre; // prefix for histos

		std::vector<std::vector<double>* > to_delete;

                std::map<std::string,TH1D*> histos1d;

		const static bool verbose = true;
		const static bool fill_histos = true;
};
#endif
