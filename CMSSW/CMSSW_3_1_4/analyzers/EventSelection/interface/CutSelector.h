#ifndef CUTSELECTOR_H
#define CUTSELECTOR_H

#include "analyzers/EventSelection/interface/PlotGenerator.h"
#include "analyzers/EventSelection/interface/Cuts.h"
#include "analyzers/EventSelection/interface/Tools.h"

#include "DataFormats/Common/interface/View.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "FWCore/Framework/interface/Event.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"

#include "FWCore/Framework/interface/TriggerNames.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
//#include "DataFormats/PatCandidates/interface/TriggerPrimitive.h" 
#include "AnalysisDataFormats/TopObjects/interface/TtGenEvent.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

//#include "TopQuarkAnalysis/Examples/plugins/HypothesisAnalyzer.h"


#include <string>
#include <map>
#include <vector>
#include "TH1D.h"

class CutSelector{
	public:
		CutSelector(std::string ident, double mass=250.0);
		~CutSelector();
                void set_handles(edm::Handle<edm::View<pat::Muon> > analyzer_muons,
				edm::Handle<edm::View<pat::Jet> > analyzer_jets,
				edm::Handle<edm::View<pat::Electron> > analyzer_electrons,
				edm::Handle<edm::View<pat::MET> > analyzer_mets,
				edm::Handle<TtGenEvent> genEvt,
				edm::Handle<TtSemiLeptonicEvent> semiLepEvt,
				edm::Handle<int> hypoClassKeyHandle,
				edm::Handle<edm::TriggerResults> HLTR,
				edm::Handle<reco::GenParticleCollection> genParticles,
				edm::Handle<reco::BeamSpot> recoBeamSpotHandle);
		void plot();
		void set_tprime_mass(double tprimeMass);

	private:
		std::vector<std::vector<double>* > cuts_to_be_deleted;

		std::string get_event_type();
		void set_cuts();
		void complete_cuts();
		void define_cuts();
		//void define_cuts_14VIII09();
		//void define_cuts_Francisco();
		void define_cuts_OctX_mu();
		void define_cuts_OctX_e();
		void define_cuts_01X09();

		void synchronise_maps();
		void vcuts(std::string type, std::string set, std::string cut, std::vector<double> *cut_vector);
		void vcuts(std::string type, std::string set, std::string cut, double value);
		void set_if_not_set(std::string type, std::string set, std::string cut, double value);
		void vset_if_not_set(std::string type, std::string set, std::string cut, double value);
		void vset_if_not_set(std::string type, std::string set, std::string cut, std::vector<double>* value);

		double get_ht_cut();

		std::string get_preselection_id(int npreselection);

		std::map<std::string, std::map<std::string,PlotGenerator*> > plot_generators;
		std::map<std::string, std::map<std::string,Cuts*> > cuts;

		std::map<std::string,std::map<std::string,std::map<std::string,double> > > cut_defs;
		std::map<std::string,std::map<std::string,std::map<std::string,std::vector<double>* > > > vcut_defs;

		std::string dataset_id;

                edm::Handle<edm::View<pat::Electron> > electrons;
                edm::Handle<edm::View<pat::Jet> > jets;
                edm::Handle<edm::View<pat::Muon> > muons;
                edm::Handle<edm::View<pat::MET> > mets;
		//for truth matching using TQAF
		edm::Handle<TtGenEvent> genEvt;
		edm::Handle<TtSemiLeptonicEvent> semiLepEvt;
		edm::Handle<int> hypoClassKeyHandle;
		edm::Handle<edm::TriggerResults> HLTR;
		edm::Handle<reco::GenParticleCollection> genParticles;
		

		/*
		 *	count different event types:
		 *	muon: 1
		 *	electron: 2
		 *	background: 3
		 */
		TH1D *event_counter_histo;
		
		int ttmu_count;
		int tte_count;
		int ttbg_count;
		double tprime_mass;
		bool Event_firstcall;
};

#endif
