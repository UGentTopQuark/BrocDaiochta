#ifndef MASSRECONSTRUCTION_H
#define MASSRECONSTRUCTION_H

//#include "DataFormats/Common/interface/View.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

#include "analyzers/EventSelection/interface/M3Mass.h"
#include "analyzers/EventSelection/interface/MinDiffM3.h"
#include "analyzers/EventSelection/interface/Chi2JetSortingMass.h"
#include "analyzers/EventSelection/interface/WbMass.h"

class MassReconstruction{
	public:
		MassReconstruction();
		~MassReconstruction();
		void set_handles(std::vector<pat::Jet>* jets, std::vector<pat::Electron>* electrons, std::vector<pat::Muon>* muons, std::vector<pat::MET> *mets);

		void set_tprime_mass(double mass);

		// return M3 mass, in case < 3 jets: return -1
		double calculate_M3();

		double calculate_Wb_top();
		double calculate_Wb_lepTmass_top_2btag();
		double calculate_Wb_W_mass();
		double calculate_Wb_dR_W_bjet1();
		double calculate_Wb_dR_W_bjet2();

		double calculate_Wb_hadTmass_top_1btag();
		double calculate_Wb_hadWmass_1btag();
		double calculate_Wb_lepTmass_top_1btag();
		double calculate_Wb_hadWmass_0btag();
		double calculate_Wb_hadTmass_top_0btag();
		double calculate_Wb_lepTmass_top_0btag();

		double calculate_min_diff_M3();
		double calculate_minmass_2btag_top();
		double calculate_minmass_1btag_top();

		double calculate_chimass();
		double calculate_chimass_2btag_top();
		double calculate_chihadTmass_2btag();
		double calculate_chihadWmass_2btag();
		double calculate_chilepTmass_2btag();
		double calculate_chilepWmass_2btag();

		double calculate_chimass_1btag_top();
		double calculate_chihadTmass_1btag();
		double calculate_chihadWmass_1btag();
		double calculate_chilepTmass_1btag();
		double calculate_chilepWmass_1btag();

		double calculate_massW(double nominal_massW);
		double top_Had_candidate_mass(int jet_id1,int jet_id2,int jet_id3);
		double top_Lep_candidate_mass(int jet_id4);
		double W_Had_candidate_mass(int jet_id2,int jet_id3);
		double W_Lep_candidate_mass();
		double calculate_chihadTmass();
		double calculate_chihadWmass();
		double calculate_chilepTmass();
		double calculate_chilepWmass();

		/*
		 *	definition of ids:
		 *	0: b-jet had t
		 *	1: 1st jet had W
		 *	2: 2nd jet had W
		 *	3: b-jet lep t
		 */
		std::vector<int>* get_ids_chi2();
		std::vector<int>* get_ids_chi2_1btag();
		std::vector<int>* get_ids_chi2_2btag();
		std::vector<int>* get_ids_m3();
		std::vector<int>* get_ids_min_diff_m3();
		std::vector<int>* get_ids_min_diff_m3_1btag();
		std::vector<int>* get_ids_min_diff_m3_2btag();
		std::vector<int>* get_ids_Wb();
	
		double get_chi2();
		double get_chi2_1btag();
		double get_chi2_2btag();

	private:
		M3Mass *m3_mass;
		MinDiffM3 *min_diff_mass;
		Chi2JetSortingMass *chi2_mass;
		WbMass *Wb_mass;

		BJetFinder *b_jet_finder;

		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* electrons;
		std::vector<pat::Muon>* muons;
		std::vector<pat::MET>* mets;
};

#endif
