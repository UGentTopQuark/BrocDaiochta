#ifndef MVAINPUTPRODUCER_H
#define MVAINPUTPRODUCER_H

#include "DataFormats/Common/interface/View.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "analyzers/EventSelection/interface/MassReconstruction.h"

class MVAInputProducer{
	public:
		MVAInputProducer();
		~MVAInputProducer();
		void set_handles(std::vector<pat::Jet>* jets, std::vector<pat::Electron>* electrons, std::vector<pat::Muon>* muons, std::vector<pat::MET> *mets);
		void print_MVA_input();
		void set_mass_reconstruction(MassReconstruction *mass_reco);
		void set_ht(double ht);
		void set_ident(std::string ident);

	private:
		void print_jets();
		void print_electrons();
		void print_muons();
		void print_masses();
		void print_ht();

		std::string ident;

		int event_counter;
		double ht;
		MassReconstruction *mass_reco;
		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* electrons;
		std::vector<pat::Muon>* muons;
		std::vector<pat::MET>* mets;
		const static bool verbose = false;
};
#endif
