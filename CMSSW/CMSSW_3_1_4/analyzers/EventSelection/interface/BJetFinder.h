#ifndef BJETFINDER_H
#define BJETFINDER_H

#include "DataFormats/PatCandidates/interface/Jet.h"
#include <vector>
#include <string>

class BJetFinder{
	public:
		BJetFinder();
		~BJetFinder();
		
		std::vector<std::pair<int, double> >* get_btag_sorted_jets(std::string btag_algo="trackCountingHighEffBJetTags");

		void set_handles(std::vector<pat::Jet>* jets);
		void set_min_btag_value(std::vector<double> min_btag_value);

	private:
		bool static compare_btag(const std::pair<int,double> p1, const std::pair<int,double> p2);
		void sort_btagged_jets(std::string btag_algo="trackCountingHighEffBJetTags");
		void cut_sorted_jets();

		std::vector<std::pair<int, double> > *btag_values;
		std::vector<pat::Jet>* jets;

		bool sorted; // btagged jets already sorted for this event?
		std::map<std::string,std::vector<double> > old_min_btag_value;
		std::vector<double> min_btag;
		//1st and 2nd highest ids per algorithm
		std::map<std::string,std::vector<std::pair<int, double> >* > btag_algo_ids;

};

#endif
