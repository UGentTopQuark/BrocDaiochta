#ifndef METPLOTS_H
#define METPLOTS_H

#include "analyzers/EventSelection/interface/Plots.h"
#include "analyzers/EventSelection/interface/METCorrector.h"
#include "analyzers/EventSelection/interface/MyMEzCalculator.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class METPlots : public Plots{
	public:
		METPlots(std::string ident);
		~METPlots();
		void plot();
	protected:
		void plot_MET_pz();
		virtual void book_histos();
};

#endif
