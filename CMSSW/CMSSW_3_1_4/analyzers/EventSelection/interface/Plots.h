#ifndef PLOTS_H
#define PLOTS_H

#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"

#include <iostream>
#include <map>
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "PhysicsTools/UtilAlgos/interface/TFileService.h"
#include "TH1D.h"
#include "TH2D.h"

#include "analyzers/EventSelection/interface/GenMatch.h"

class Plots{
	public:
		Plots();
		virtual ~Plots();
		void set_handles(std::vector<pat::Jet>* jets,
				 std::vector<pat::Electron>* isolated_electrons,
				 std::vector<pat::Muon>* isolated_muons,
				 std::vector<pat::MET>* corrected_mets);
		void set_gen_match(GenMatch *gen_match);
		virtual void plot() = 0;
	protected:
		std::string id;
                std::map<std::string,TH1D*> histos1d;
                std::map<std::string,TH2D*> histos2d;

		virtual void prepare_objects();
		virtual void book_histos();

		std::vector<pat::Jet>* jets;
		std::vector<pat::Electron>* isolated_electrons;
		std::vector<pat::Muon>* isolated_muons;
		std::vector<pat::MET>* corrected_mets;

		GenMatch *gen_match;
};

#endif
