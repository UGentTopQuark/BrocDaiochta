#include "../interface/FlatNTuples.h"

FlatNTuples::FlatNTuples(const edm::ParameterSet& iConfig):
  eleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("electronTag")),
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  tauLabel_(iConfig.getUntrackedParameter<edm::InputTag>("tauTag")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  phoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("photonTag")),
  PriVertexLabel_(iConfig.getUntrackedParameter<edm::InputTag>("primaryVertexTag")),
  hlTriggerResults_ (iConfig.getParameter<edm::InputTag> ("HLTriggerResults")),
  hltAodSummary_ (iConfig.getParameter<edm::InputTag> ("HLTAodSummary"))
{
	do_mc_matching = iConfig.getParameter<bool>("MCMatching");
	fill_trigger_objects = iConfig.getParameter<bool>("FillTriggerObjects");
	write_prescales = iConfig.getParameter<bool>("WriteTriggerPrescales");
	write_d0_wrt_pv = iConfig.getParameter<bool>("Writed0wrtPV");
	write_gen_particles = iConfig.getParameter<bool>("WriteGenParticles");

	muonIDs = new std::vector<std::string>(iConfig.getParameter<std::vector<std::string> >("MuonIDs"));
	electronIDs = new std::vector<std::string>(iConfig.getParameter<std::vector<std::string> >("ElectronIDs"));

	std::string outfile_name = iConfig.getParameter<std::string>("outfile");
   	outfile = new TFile(outfile_name.c_str(),"RECREATE");
	outfile->cd();
   	tree = new TTree("tree","tree");
   	trigger_name_mapping = new TTree("trigger_name_mapping","trigger_name_mapping");
   	tag_name_mapping = new TTree("tag_name_mapping","tag_name_mapping");

	beag_jets = new beag::Collection<pat::Jet, beag::Jet>(tree, outfile, "jets");
	beag_mets = new beag::Collection<pat::MET, beag::MET>(tree, outfile, "mets");
	beag_muons = new beag::Collection<pat::Muon, beag::Muon>(tree, outfile, "muons");
	beag_electrons = new beag::Collection<pat::Electron, beag::Electron>(tree, outfile, "electrons");
	trigger_prod = new beag::TriggerProducer(tree, outfile, "trigger");
	trigger_obj_prod = NULL;
	if(fill_trigger_objects) trigger_obj_prod = new beag::TriggerObjectProducer(tree, outfile, "trigger_objects");
	btag_algos = new std::vector<std::string>(iConfig.getParameter<std::vector<std::string> >("BTagAlgorithms"));
	beag_jets->set_selection_list(btag_algos);

	evt_info_prod = new beag::EventInformationProducer(tree, outfile, "evt_info");
	pvertex_prod = new beag::PrimaryVertexProducer(tree, outfile, "pvertices");
	trigger_name_mapper = new beag::TriggerNameMapper(trigger_name_mapping, outfile, "trigger_names", write_prescales, true);
	veto_trigger_name_mapper = new beag::TriggerNameMapper(trigger_name_mapping, outfile, "trigger_names", false, false);
	eID_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "eID_names");
	eID_mapper->set_tag_names(electronIDs);
	beag_electrons->set_tag_list(electronIDs);
	muID_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "muID_names");
	muID_mapper->set_tag_names(muonIDs);
	beag_muons->set_tag_list(muonIDs);
	btag_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "btag_names");
	btag_mapper->set_tag_names(btag_algos);
	// fill tag names only once at the beginning of job
	tag_name_mapping->Fill();

	gen_part_prod = NULL;
	if(write_gen_particles){
		gen_part_prod = new beag::GenParticleProducer(tree, outfile, "gen_particles");
		gen_part_prod->set_min_pt(iConfig.getParameter<double>("SelectedGenParticlesMinPt"));
		gen_part_prod->set_max_eta(iConfig.getParameter<double>("SelectedGenParticlesMaxEta"));
		gen_part_prod->set_selected_particle_ids(iConfig.getParameter<std::vector<int> >("SelectedGenParticles"));
	}

	gen_evt_prod = NULL;
	if(do_mc_matching) gen_evt_prod = new beag::TTbarGenEventProducer(tree, outfile, "gen_evt");
	reco_gen_match = NULL;
	if(do_mc_matching) reco_gen_match = new beag::RecoGenMatch();

	valid_triggers = new std::vector<std::string>();

	hltConfig = new HLTConfigProvider();
	std::vector<std::string> triggers = iConfig.getParameter<std::vector<std::string> >("TriggerList");
	trigger_menu = hlTriggerResults_.process();
	trigger_prod->set_hlt_config_provider(hltConfig);
	trigger_name_mapper->set_trigger_expressions(triggers);
	if(fill_trigger_objects) trigger_obj_prod->set_hlt_config_provider(hltConfig);
	if(fill_trigger_objects) trigger_obj_prod->set_trigger_menu(trigger_menu);

	std::vector<std::string> veto_triggers = iConfig.getParameter<std::vector<std::string> >("VetoObjectTriggers");
	veto_trigger_name_mapper->set_trigger_expressions(veto_triggers);

	beag_muons->set_mc_matching(do_mc_matching);

	beag_electrons->set_mc_matching(do_mc_matching);

	current_lumi_block = -1;
	prev_lumi_block = -1;

	hltConfigChanged = true;
	run_changed = true;
	lumi_block_changed = true;
	first_in_job = true;
}

FlatNTuples::~FlatNTuples()
{
	if(gen_part_prod){
		delete gen_part_prod;
		gen_part_prod = NULL;
	}
	if(btag_algos){
		delete btag_algos;
		btag_algos = NULL;
	}
	if(valid_triggers){
		delete valid_triggers;
		valid_triggers = NULL;
	}
	if(hltConfig){
		delete hltConfig;
		hltConfig = NULL;
	}
	if(reco_gen_match){
		delete reco_gen_match;
		reco_gen_match = NULL;
	}
	if(trigger_obj_prod){
		delete trigger_obj_prod;
		trigger_obj_prod = NULL;
	}
	if(trigger_prod){
		delete trigger_prod;
		trigger_prod = NULL;
	}
	if(evt_info_prod){
		delete evt_info_prod;
		evt_info_prod = NULL;
	}
	if(beag_jets){
		delete beag_jets;
		beag_jets = NULL;
	}
	if(beag_electrons){
		delete beag_electrons;
		beag_electrons = NULL;
	}
	if(beag_muons){
		delete beag_muons;
		beag_muons = NULL;
	}
	if(beag_mets){
		delete beag_mets;
		beag_mets = NULL;
	}
	outfile->cd();
   	tree->Write();
   	if(tree){
   		delete tree;
		tree = NULL;
	}
   	if(trigger_name_mapping){
   		trigger_name_mapping->Write();
   		delete trigger_name_mapping;
		trigger_name_mapping = NULL;
	}
   	outfile->Write();
   	outfile->Close();
   	if(outfile){
   		delete outfile;
		outfile = NULL;
	}
}


//
// member functions
//

// ------------ method called to for each event  ------------
void
FlatNTuples::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	using namespace edm;

        edm::Handle<edm::View<pat::Electron> > electronHandle;
        iEvent.getByLabel(eleLabel_,electronHandle);

        edm::Handle<edm::View<pat::Muon> > muonHandle;
        iEvent.getByLabel(muoLabel_,muonHandle);

        edm::Handle<edm::View<pat::MET> > metHandle;
        //iEvent.getByLabel(metLabel_,metHandle);

        edm::Handle<edm::View<pat::Jet> > jetHandle;
        iEvent.getByLabel(jetLabel_,jetHandle);

        edm::Handle<reco::GenParticleCollection> genParticles;

	outfile->cd();

	if(do_mc_matching || write_gen_particles){
        	iEvent.getByLabel("genParticles", genParticles);
	}

	edm::Handle<edm::TriggerResults> HLTR;
	iEvent.getByLabel(hlTriggerResults_,HLTR);

	edm::Handle<trigger::TriggerEvent> aodTriggerEvent;
	iEvent.getByLabel(hltAodSummary_, aodTriggerEvent);

	edm::Handle<reco::BeamSpot> beamSpotHandle;
	iEvent.getByLabel("offlineBeamSpot", beamSpotHandle);

	edm::ESHandle<TransientTrackBuilder> trackBuilder;
	if(write_d0_wrt_pv) iSetup.get<TransientTrackRecord>().get("TransientTrackBuilder", trackBuilder);

	edm::Handle<std::vector<reco::Vertex> > pvertexHandle;	
	iEvent.getByLabel(PriVertexLabel_, pvertexHandle);

	if(lumi_block_changed){
		hltConfig->init(iEvent, trigger_menu, hltConfigChanged);
		if(write_prescales) prescale_set = hltConfig->prescaleSet(iEvent, iSetup);
	}

	// if hlt config changes, write out mapping between names and ids if necessary
	if(hltConfigChanged || first_in_job){
		valid_triggers->clear();
		*valid_triggers = hltConfig->triggerNames();
		std::cout << "----------" << std::endl;
		for(std::vector<std::string>::iterator valid_trigger = valid_triggers->begin();
			valid_trigger != valid_triggers->end();
			++valid_trigger){
			std::cout << *valid_trigger << std::endl;
		}
		std::cout << "----------" << std::endl;
		selected_triggers = trigger_name_mapper->get_selected_trigger_names(valid_triggers, hltConfig, prescale_set);
		std::cout << "==========" << std::endl;
		for(std::vector<std::string>::iterator valid_trigger = selected_triggers->begin();
			valid_trigger != selected_triggers->end();
			++valid_trigger){
			std::cout << *valid_trigger << std::endl;
		}
		std::cout << "==========" << std::endl;
		std::cout << "nselected triggers: " << selected_triggers->size() << std::endl;
		trigger_prod->set_triggers_to_write(selected_triggers);
		vetoed_triggers = veto_trigger_name_mapper->get_selected_trigger_names(valid_triggers, hltConfig, false);
		if(fill_trigger_objects) trigger_obj_prod->set_triggers_to_write(selected_triggers);
		if(fill_trigger_objects) trigger_obj_prod->set_triggers_to_veto(vetoed_triggers);
		trigger_name_mapping->Fill();
		first_in_job = false;
	}

	pvertex_prod->fill_primary_vertices(pvertexHandle);

	if(write_gen_particles) gen_part_prod->fill_particles(genParticles);

	beag_jets->fill_collection(jetHandle);
	//beag_mets->fill_collection(metHandle);

	beag_muons->set_beamspot_handle(beamSpotHandle);
	beag_muons->set_trackbuilder_handle(trackBuilder, pvertexHandle);
	beag_muons->set_selection_list(selected_triggers);
	beag_muons->fill_collection(muonHandle);

	beag_electrons->set_beamspot_handle(beamSpotHandle);
	beag_electrons->set_trackbuilder_handle(trackBuilder, pvertexHandle);
	beag_electrons->set_selection_list(selected_triggers);
	beag_electrons->fill_collection(electronHandle);

	trigger_prod->fill_trigger_information(HLTR);

	if(fill_trigger_objects) trigger_obj_prod->fill_trigger_objects(aodTriggerEvent,HLTR);

	evt_info_prod->fill_event_information(iEvent.run(), iEvent.luminosityBlock(), iEvent.id().event(), hltConfigChanged, -1);

	if(do_mc_matching) gen_evt_prod->fill_mc_information(genParticles);

	if(do_mc_matching) reco_gen_match->set_gen_event(gen_evt_prod->get_gen_event());
	if(do_mc_matching) reco_gen_match->match_jets_to_partons(beag_jets->get_beag_objects());
	if(do_mc_matching) reco_gen_match->match_leptons_to_particles<beag::Muon>(beag_muons->get_beag_objects());
	if(do_mc_matching) reco_gen_match->match_leptons_to_particles<beag::Electron>(beag_electrons->get_beag_objects());
	// Fill tree for all variables
	tree->Fill();

	run_changed = false;
}

void FlatNTuples::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}

void FlatNTuples::beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup)
{
}

//define this as a plug-in
DEFINE_FWK_MODULE(FlatNTuples);
