#include "../interface/GenParticleProducer.h"

beag::GenParticleProducer::GenParticleProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	gen_particles = new std::vector<beag::Particle>();
	gen_particles->push_back(beag::Particle());
	tree->Bronch(ident.c_str(),"std::vector<beag::Particle>",&gen_particles);

	min_pt = -1;
	max_eta = -1;
}

beag::GenParticleProducer::~GenParticleProducer()
{
	if(gen_particles){
		delete gen_particles;
		gen_particles = NULL;
	}
}

void beag::GenParticleProducer::set_min_pt(double pt)
{
	this->min_pt = pt;
}

void beag::GenParticleProducer::set_max_eta(double eta)
{
	this->max_eta = eta;
}

void beag::GenParticleProducer::set_selected_particle_ids(std::vector<int> pdg_ids)
{
	particle_ids.clear();

	for(std::vector<int>::iterator pdg_id = pdg_ids.begin();
		pdg_id != pdg_ids.end();
		++pdg_id){
		particle_ids[*pdg_id] = true;	
	}
}

void beag::GenParticleProducer::fill_particles(edm::Handle<reco::GenParticleCollection> part_collection)
{
	gen_particles->clear();
	for(unsigned int ngen_part = 0;
		ngen_part < part_collection->size();
		++ngen_part){
		if(accept_particle(&((*part_collection)[ngen_part]))){
			gen_particles->push_back(create_beag_particle(&((*part_collection)[ngen_part])));
			if(verbose) std::cout << "Filling gen particle " << (*part_collection)[ngen_part].pdgId() << " pt: " << (*part_collection)[ngen_part].pt() << std::endl;
		}
	}
}

beag::Particle beag::GenParticleProducer::create_beag_particle(const reco::GenParticle *gen_part)
{
	beag::Particle beag_particle;
	beag_particle.px = gen_part->px();
	beag_particle.py = gen_part->py();
	beag_particle.pz = gen_part->pz();
	beag_particle.e = gen_part->energy();

	beag_particle.mc_id = gen_part->pdgId();

	return beag_particle;
}

bool beag::GenParticleProducer::accept_particle(const reco::GenParticle *gen_part)
{
	if((particle_ids.find(fabs(gen_part->pdgId())) != particle_ids.end()) &&
		(fabs(gen_part->eta()) < max_eta || max_eta == -1) &&
		(fabs(gen_part->pt()) < min_pt || min_pt == -1))
		return true;
	else
		return false;
}
