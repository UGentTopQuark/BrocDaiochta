#include "../interface/TriggerObjectProducer.h"

beag::TriggerObjectProducer::TriggerObjectProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	trigger_objects = new std::vector<beag::TriggerObject>();
	tree->Bronch(ident.c_str(),"std::vector<beag::TriggerObject>",&trigger_objects);

	trigger_menu = "HLT";
}

beag::TriggerObjectProducer::~TriggerObjectProducer()
{
	if(trigger_objects){
		delete trigger_objects;
		trigger_objects = NULL;
	}
}

void beag::TriggerObjectProducer::set_hlt_config_provider(HLTConfigProvider *hltConfig)
{
	this->hltConfig = hltConfig;
}

void beag::TriggerObjectProducer::set_triggers_to_write(std::vector<std::string> *triggers)
{
	this->triggers_to_write = triggers;
}

void beag::TriggerObjectProducer::set_triggers_to_veto(std::vector<std::string> *veto_triggers)
{
	if(!triggers_to_veto.empty()) triggers_to_veto.clear();
	for(std::vector<std::string>::iterator vtrig = veto_triggers->begin();vtrig != veto_triggers->end();vtrig++)
		triggers_to_veto[*vtrig] = true;
}

void beag::TriggerObjectProducer::fill_trigger_objects(edm::Handle<trigger::TriggerEvent> aodTriggerEvent, edm::Handle<edm::TriggerResults> HLTR)
{
	this->aodTriggerEvent = aodTriggerEvent;
	trigger_objects->clear();
	objects_to_write.clear();

	if(triggers_to_write->size() >= 64)
		std::cerr << " ERROR: more than 64 triggers examined, skipping object selection " << std::endl;

	if(!HLTR.isValid()){
		std::cerr << "WARNING: beag::TriggerObjectProducer::fill_trigger_information() no valid trigger object found" << std::endl;
		return;
	}

	//Loop over all triggers and save results for those that passed
	for(int nbeag_trig = 0; nbeag_trig < int(triggers_to_write->size());nbeag_trig++){
		std::string trigger_name = (*triggers_to_write)[nbeag_trig];

		//Skip if is one of vetoed triggers
		if(triggers_to_veto.find(trigger_name) != triggers_to_veto.end())
			continue;

      		get_trigger_objects(1,nbeag_trig); //objects for L1 seeds

		//Check if event passed trigger before checking hlt objects. 
		unsigned int trigger_id( hltConfig->triggerIndex(trigger_name) );
		if(!(HLTR->accept(trigger_id)))
			continue; 
		
		if(verbose) std::cout << " Event passed trigger: " << trigger_name << std::endl;

		get_trigger_objects(0,nbeag_trig); //hlt trigger objects
	
		//NB: If you add another long long to objects_to_write remember to change assign(?, 0) in get_trigger_objects
	
	}
	
  
	const trigger::TriggerObjectCollection objects = aodTriggerEvent->getObjects();
	
	//Loop over objects to write and fill to beag::trigger_objects
	for(std::map<int,std::vector<unsigned long long> >::iterator nobj = objects_to_write.begin();nobj != objects_to_write.end();nobj++){
		trigger::TriggerObject foundObject = objects[(*nobj).first];

		if (verbose) std::cout << " Filling trigger objects: Key, " << (*nobj).first << ", hlt trig_results: " << ((*nobj).second)[0] << ", l1seed trig_results: " << ((*nobj).second)[1]  << ", pt: " << foundObject.pt() <<  std::endl;
 
		beag::TriggerObject tmp;
		tmp.px = foundObject.px();
		tmp.py = foundObject.py();
		tmp.pz = foundObject.pz();
		tmp.e = foundObject.energy();
		tmp.triggered = ((*nobj).second)[0];
		tmp.l1triggered = ((*nobj).second)[1];
			
		trigger_objects->push_back(tmp);
	}
			
}

void beag::TriggerObjectProducer::set_trigger_menu(std::string trigger_menu)
{
	this->trigger_menu = trigger_menu;
}

//Get trigger objects passing a given module. map<keys,trigger_results>
void beag::TriggerObjectProducer::get_trigger_objects(int module,int nbeag_trig)
{
	std::string trigger_name = (*triggers_to_write)[nbeag_trig];

	std::vector<std::string> module_names = hltConfig->moduleLabels(trigger_name);
	std::string module_name;

	//Get the name of the module you want
	if(module == 0){
		//last module is always hltBoolEnd, want second last module name
		module_name = module_names[module_names.size() - 2];
		if(std::string(module_names[ module_names.size() - 1]) != "hltBoolEnd")
			std::cerr << " ERROR: trigger modules wrong "<< std::string(module_names[ module_names.size() - 1]) << std::endl;

	}
	else if(module == 1){
		for ( size_t i = 0; i < module_names.size(); i++ ) {
			std::string tmp_module = module_names[i];
			if ( hltConfig->moduleType(tmp_module) == "HLTLevel1GTSeed" ) 
				module_name = tmp_module;
		}
	}
	else {
		std::cerr << "ERROR: No module set in TriggerObjectProvider " << std::endl;
	}
	
	if(verbose) std::cout << " Trigger: " << trigger_name << ", nbeag_trig " << nbeag_trig << ", module: " <<module << ", module name: " << module_name << std::endl; 
	       
	//Now use module name to get keys for trigger objects
	//FIXME: process should be passed from main analyser
	edm::InputTag filterTag = edm::InputTag(module_name, "", trigger_menu);
		
	int filterIndex = aodTriggerEvent->filterIndex(filterTag);
		
	if ( filterIndex < aodTriggerEvent->sizeFilters() ) {
		//These trigger keys point you to the trigger objects passing this filter in the objects collection	
		const trigger::Keys &keys = aodTriggerEvent->filterKeys( filterIndex );
			
			for ( size_t i = 0; i < keys.size(); i++ ){
				if(objects_to_write.find(keys[i]) == objects_to_write.end()){
					objects_to_write[keys[i]].assign(2,0);
				}

				objects_to_write[keys[i]][module] |= ((unsigned long long) 1 << nbeag_trig);
 			}
	}

} 
