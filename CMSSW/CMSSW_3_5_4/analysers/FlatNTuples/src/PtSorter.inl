//#include "analyzers/EventSelection/interface/PtSorter.h"

template <class MyObject>
void PtSorter<MyObject>::add(MyObject *obj)
{
	objects.push_back(obj);
}

template <class MyObject>
bool PtSorter<MyObject>::compare_pt(const MyObject *obj1,
		          const MyObject *obj2)
{
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1;
	p4_1.SetPx(obj1->px);
	p4_1.SetPy(obj1->py);
	p4_1.SetPz(obj1->pz);
	p4_1.SetE(obj1->e);
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2;
	p4_2.SetPx(obj2->px);
	p4_2.SetPy(obj2->py);
	p4_2.SetPz(obj2->pz);
	p4_2.SetE(obj2->e);

	return p4_1.pt() > p4_2.pt();
}

template <class MyObject>
std::vector<MyObject*> PtSorter<MyObject>::get_sorted()
{
	std::sort(objects.begin(), objects.end(), PtSorter::compare_pt);
	return objects;
}

template <class MyObject>
void PtSorter<MyObject>::clear()
{
	objects.clear();
}
