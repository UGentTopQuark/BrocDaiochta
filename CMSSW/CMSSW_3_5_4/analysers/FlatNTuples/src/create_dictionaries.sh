#!/bin/bash

for i in Jet Electron Muon MET Trigger TTbarGenEvent Lepton Particle EventInformation PrimaryVertex TriggerObject; do
	rootcint -f ${i}Dict.cc -c ../interface/${i}.h ../interface/Headers.h ../interface/${i}LinkDef.h
done
