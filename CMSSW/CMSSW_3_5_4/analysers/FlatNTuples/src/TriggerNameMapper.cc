#include "../interface/TriggerNameMapper.h"

beag::TriggerNameMapper::TriggerNameMapper(TTree *tree, TFile *outfile, std::string ident, bool write_prescales, bool write)
{
	this->tree = tree;
	this->outfile = outfile;

	this->write_prescales = write_prescales;

	selected_triggers = new std::vector<std::string>();
	prescales = NULL;
	if(write_prescales) prescales = new std::vector<unsigned int>();
	if(write) tree->Bronch(ident.c_str(),"std::vector<std::string>",&selected_triggers);
	if(write_prescales) tree->Bronch(ident.c_str(),"std::vector<unsigned int>",&prescales);
}

beag::TriggerNameMapper::~TriggerNameMapper()
{
	if(selected_triggers){
		delete selected_triggers;
		selected_triggers = NULL;
	}
	if(prescales){
		delete prescales;
		prescales = NULL;
	}
}

void beag::TriggerNameMapper::set_trigger_expressions(std::vector<std::string> &trigger_expressions)
{
	this->trigger_expressions = trigger_expressions;
}

std::vector<std::string>* beag::TriggerNameMapper::get_selected_trigger_names(std::vector<std::string> *valid_triggers, HLTConfigProvider *hltConfig, unsigned int prescale_set)
{
	if(verbose) std::cout << "getting selected trigger names..." << std::endl;
	selected_triggers->clear();
	if(write_prescales) prescales->clear();

	boost::regex re;
	boost::cmatch matches;
	std::map<std::string,bool> already_selected_triggers;

	int nselected_triggers = 0;
	for(std::vector<std::string>::iterator trigger_expression = trigger_expressions.begin();
		trigger_expression != trigger_expressions.end();
		++trigger_expression){
		//re.assign(*trigger_expression, boost::regex_constants::icase);
		re.assign(*trigger_expression);
		if(verbose) std::cout << "testing for regex: " << *trigger_expression << std::endl;
		for(std::vector<std::string>::iterator valid_trigger = valid_triggers->begin();
			valid_trigger != valid_triggers->end();
			++valid_trigger){
		        bool matched = boost::regex_match(*valid_trigger, re);
		
			if(verbose) std::cout << "comparing with trigger: " << *valid_trigger << std::endl;
			
			if(matched){
				// make sure every trigger is added only once to the vector
				if(already_selected_triggers.find(*valid_trigger) == already_selected_triggers.end()){
					if(verbose) std::cout << "trigger " << *valid_trigger << " MATCHED regex " << *trigger_expression << std::endl;
					selected_triggers->push_back(*valid_trigger);
					if(write_prescales) prescales->push_back(hltConfig->prescaleValue(prescale_set, *valid_trigger));
					already_selected_triggers[*valid_trigger] = true;
					++nselected_triggers;
					if(nselected_triggers >= 64){
						std::cerr << "ERROR: TriggerNameMapper::get_selected_trigger_names(): too many triggers defined > 64: " << nselected_triggers <<  std::endl;
						
						exit(1);
					}
				}
			}
		}
	}

	return selected_triggers;
}
