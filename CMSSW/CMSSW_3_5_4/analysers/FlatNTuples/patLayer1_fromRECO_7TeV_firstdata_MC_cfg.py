# This is an example PAT configuration showing the usage of PAT on minbias data

# Starting with a skeleton process which gets imported with the following line
from PhysicsTools.PatAlgos.patTemplate_cfg import *

from PhysicsTools.PatAlgos.tools.coreTools import *

## global tag for data
process.GlobalTag.globaltag = cms.string('GR_R_35X_V7A::All')

# turn off MC matching for the process
#removeMCMatching(process, ['All'])

# add pf met
from PhysicsTools.PatAlgos.tools.metTools import *
#removeMCMatching(process, ['All'])
addPfMET(process, 'PF')



# get the 900 GeV jet corrections
from PhysicsTools.PatAlgos.tools.jetTools import *
switchJECSet( process, "Summer09_7TeV_ReReco332")

# run ak5 gen jets
from PhysicsTools.PatAlgos.tools.cmsswVersionTools import *
run33xOnReRecoMC( process, "ak5GenJets")

# Add PF jets
addJetCollection(process,cms.InputTag('ak5PFJets'),
                 'AK5', 'PF',
                 doJTA        = False,
                 doBTagging   = False,
                 jetCorrLabel = ('AK5','PF'),
                 doType1MET   = False,
                 doL1Cleaning = False,                 
                 doL1Counters = False,
                 genJetCollection=cms.InputTag("ak5GenJets"),
                 doJetID      = False
                 )

# require physics declared
process.physDecl = cms.EDFilter("PhysDecl",
    applyfilter = cms.untracked.bool(True)
)

# require scraping filter
process.scrapingVeto = cms.EDFilter("FilterOutScraping",
                                    applyfilter = cms.untracked.bool(True),
                                    debugOn = cms.untracked.bool(False),
                                    numtrack = cms.untracked.uint32(10),
                                    thresh = cms.untracked.double(0.2)
                                    )


# configure HLT
process.load('L1TriggerConfig.L1GtConfigProducers.L1GtTriggerMaskTechTrigConfig_cff')
process.load('HLTrigger/HLTfilters/hltLevel1GTSeed_cfi')
#process.hltLevel1GTSeed.L1TechTriggerSeeding = cms.bool(True)
#process.hltLevel1GTSeed.L1SeedsLogicalExpression = cms.string('0 AND (40 OR 41) AND NOT (36 OR 37 OR 38 OR 39)')

# switch on PAT trigger
from PhysicsTools.PatAlgos.tools.trigTools import switchOnTrigger
switchOnTrigger( process )

process.primaryVertexFilter = cms.EDFilter("GoodVertexFilter",
                                           vertexCollection = cms.InputTag('offlinePrimaryVertices'),
                                           minimumNDOF = cms.uint32(4) ,
                                           maxAbsZ = cms.double(15), 
                                           maxd0 = cms.double(2) 
                                           )

# Select jets
#process.selectedPatJets.cut = cms.string('pt > 15. & abs(eta) < 3.0')
#process.countPatJets.minNumber      = 1
#process.selectedPatJetsAK5PF.cut = cms.string('pt > 2 & abs(eta) < 3.0')


# Add the files 
readFiles = cms.untracked.vstring()
secFiles = cms.untracked.vstring()

#readFiles.extend( [
#'__FILE_NAMES__'
#        ] );
#process.source.fileNames = readFiles

# let it run

#print
#print "============== Warning =============="
#print "technical trigger filter:    DISABLED"
#print "physics declare bit filter:  DISABLED"
#print "primary vertex filter:       DISABLED"

# rename output file
process.out.fileName = cms.untracked.string('reco_7TeV_firstdata_356_pat.root')

# reduce verbosity
#process.MessageLogger.cerr.FwkReport.reportEvery = cms.untracked.int32(1000)

# process all the events
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_1_1.root',
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_2_1.root',
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_3_1.root',
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_4_1.root',
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_5_1.root',
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_6_1.root',
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_7_1.root',
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_8_1.root',
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_9_1.root',
        'file:////user/bklein/default_cmssw/CMSSW_3_5_7/src/analysers/FlatNTuples/sync_ex_RECO/copied_RECO_file_10_1.root'
    )
#    fileNames = cms.untracked.vstring( __FILE_NAMES__ ),
#        skipEvents = cms.untracked.uint32( __SKIP_EVENTS__ )
)

process.produceNTuples = cms.EDAnalyzer('FlatNTuples',
	electronTag = cms.untracked.InputTag("selectedPatElectrons"),
	tauTag      = cms.untracked.InputTag("selectedPatTaus"),
	muonTag     = cms.untracked.InputTag("selectedPatMuons"),
	jetTag      = cms.untracked.InputTag("selectedPatJets"),
	photonTag   = cms.untracked.InputTag("selectedPatPhotons"),
	metTag      = cms.untracked.InputTag("patMETs"),
	primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD','','HLT'),
	HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT'),
	MuonIDs	= cms.vstring("AllGlobalMuons","AllStandAloneMuons", "AllTrackerMuons", "AllArbitrated", "GlobalMuonPromptTight", "TrackerMuonArbitrated"),
	ElectronIDs	= cms.vstring("eidRobustTight", "eidRobustLoose", "eidTight", "eidLoose"),
	TriggerList      = cms.vstring("HLT_.*Ele.*","HLT_.*Mu.*","HLT_.*Jet.*"),
	VetoObjectTriggers = cms.vstring("HLT_.*Jet.*"),
	BTagAlgorithms	= cms.vstring("trackCountingHighEffBJetTags",
      				      "jetProbabilityBJetTags"),
	MCMatching	= cms.bool(True),
	FillTriggerObjects	= cms.bool(False),
	WriteTriggerPrescales	= cms.bool(False),
	Writed0wrtPV	= cms.bool(False),		# write d0 wrt PV
	WriteGenParticles = cms.bool(False),		# write selected MC particles
	SelectedGenParticles = cms.vint32(11,13),	# pdgIds of MC particles to write
	SelectedGenParticlesMinPt = cms.double(1),	# min pt of MC particles to write
	SelectedGenParticlesMaxEta = cms.double(3.0),	# max eta of MC particles to write
	outfile		= cms.string("sync_ex.root")
)

process.p = cms.Path(
#    process.hltLevel1GTSeed*
#    process.scrapingVeto*
#    process.physDecl*
 #   process.primaryVertexFilter*
    process.patDefaultSequence * process.produceNTuples
    )


process.maxEvents.input = 10000
process.options.wantSummary = True

from PhysicsTools.PatAlgos.patEventContent_cff import patEventContentNoCleaning
from PhysicsTools.PatAlgos.patEventContent_cff import patExtraAodEventContent
from PhysicsTools.PatAlgos.patEventContent_cff import patTriggerEventContent
process.out.outputCommands = patEventContentNoCleaning
process.out.outputCommands += patExtraAodEventContent
process.out.outputCommands += patTriggerEventContent
process.out.outputCommands += [
        'keep recoPFCandidates_particleFlow_*_*'
        ]
