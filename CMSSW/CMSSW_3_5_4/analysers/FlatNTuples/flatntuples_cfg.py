import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
#      'dcap:///pnfs/iihe/cms/store/user/bklein/data/7TeV1stData/MinimumBiasjob_5_reco_7TeV_firstdata_356_pat.root'
      #'file:////user/walsh/CMSSW_3_5_7/src/trigger_sync_0510/PAT_354_RelVal.root'
      
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_10_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_11_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_12_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_13_0.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_14_0.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_15_0.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_16_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_2_2.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_3_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_4_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_5_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_6_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_7_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_8_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135059_9_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_10_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_11_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_12_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_13_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_1_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_2_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_3_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_4_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_5_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_6_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_7_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_8_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/135059-135534-35X/TopTrigSkim_135525_9_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/136033-136036-36X//TopTrigSkim_1_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/136033-136036-36X//TopTrigSkim_2_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/136033-136036-36X//TopTrigSkim_3_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/136033-136036-36X//TopTrigSkim_4_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/136033-136036-36X//TopTrigSkim_5_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/136033-136036-36X//TopTrigSkim_6_1.root',
'rfio:/castor/cern.ch/user/s/slaunwhj/TopTrigSkims/136033-136036-36X//TopTrigSkim_7_1.root',
'rfio:/castor/cern.ch/user/l/lobelle/lobelle/JetMETTau/TopTrig_v2_136080_136082_136087/c08891effd034446e5ce5096809b3a8e/TopTrigSkim_1_1.root',
'rfio:/castor/cern.ch/user/l/lobelle/lobelle/JetMETTau/TopTrig_v2_136080_136082_136087/c08891effd034446e5ce5096809b3a8e/TopTrigSkim_2_1.root',
'rfio:/castor/cern.ch/user/l/lobelle/lobelle/JetMETTau/TopTrig_v2_136080_136082_136087/c08891effd034446e5ce5096809b3a8e/TopTrigSkim_3_1.root',
'rfio:/castor/cern.ch/user/l/lobelle/lobelle/JetMETTau/TopTrig_v2_136080_136082_136087/c08891effd034446e5ce5096809b3a8e/TopTrigSkim_4_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_10_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_11_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_12_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_13_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_13_2.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_14_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_15_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_16_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_1_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_2_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_3_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_4_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_5_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_6_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_7_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_8_1.root',
'rfio:/castor/cern.ch/user/k/kuessel/skimv2/kuessel/JetMETTau/TopTrig_v2_136088_136297/c08891effd034446e5ce5096809b3a8e//TopTrigSkim_9_1.root'

    )
)
	
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.produceNTuples = cms.EDAnalyzer('FlatNTuples',
	electronTag = cms.untracked.InputTag("selectedPatElectrons"),
	tauTag      = cms.untracked.InputTag("selectedPatTaus"),
	muonTag     = cms.untracked.InputTag("selectedPatMuons"),
	jetTag      = cms.untracked.InputTag("selectedPatJets"),
	photonTag   = cms.untracked.InputTag("selectedPatPhotons"),
	metTag      = cms.untracked.InputTag("patMETs"),
	primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD','','HLT'),
	HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT'),
	MuonIDs	= cms.vstring("AllGlobalMuons","AllStandAloneMuons", "AllTrackerMuons", "AllArbitrated", "GlobalMuonPromptTight", "TrackerMuonArbitrated"),
	ElectronIDs	= cms.vstring("eidRobustTight", "eidRobustLoose", "eidTight", "eidLoose"),
	TriggerList      = cms.vstring("HLT_(L[[:digit:]])?Ele(?:(?!(No|Anti)BPTX).)*","HLT_(L[[:digit:]])?Mu(?:(?!(No|Anti)BPTX).)*","HLT_(L[[:digit:]]+|Quad)?Jet(?:(?!NoBPTX).)*", "HLT_(L[[:digit:]])?MET(?:(?!(No|Anti)BPTX).)*"),
	VetoObjectTriggers = cms.vstring("HLT_.*Jet.*", "HLT_.*MET.*"),
	BTagAlgorithms	= cms.vstring("trackCountingHighEffBJetTags",
      				      "jetProbabilityBJetTags"),
	MCMatching	= cms.bool(False),
	FillTriggerObjects	= cms.bool(True),
	WriteTriggerPrescales	= cms.bool(False),
	Writed0wrtPV	= cms.bool(False),		# write d0 wrt PV
	WriteGenParticles = cms.bool(False),		# write selected MC particles
	SelectedGenParticles = cms.vint32(11,13),	# pdgIds of MC particles to write
	SelectedGenParticlesMinPt = cms.double(1),	# min pt of MC particles to write
	SelectedGenParticlesMaxEta = cms.double(3.0),	# max eta of MC particles to write
	outfile		= cms.string("Data_TC_100610.root")
)

process.p1 = cms.Path(process.produceNTuples)

process.schedule = cms.Schedule( process.p1)
