#ifdef __CINT__

#include "TriggerObject.h"
#include <vector>

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class beag::TriggerObject;
#pragma link C++ class beag;
#pragma link C++ class std::vector<beag::TriggerObject>;

#endif
