#ifndef BEAG_PRIMARYVERTEX_H
#define BEAG_PRIMARYVERTEX_H

#include <map>
#include <string>

#include "Rtypes.h"

namespace beag{
	class PrimaryVertex{
		public:
			PrimaryVertex(): z(0),rho(0),is_fake(true), ndof(0){};
			virtual ~PrimaryVertex(){};

			double z;
			double rho;
			bool is_fake;
			double ndof;

		ClassDef(PrimaryVertex, 1);
	};
}

#endif
