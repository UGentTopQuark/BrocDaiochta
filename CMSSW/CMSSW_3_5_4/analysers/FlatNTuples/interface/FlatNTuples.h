// -*- C++ -*-
//
// Package:    FlatNTuples
// Class:      FlatNTuples
// 
/**\class FlatNTuples FlatNTuples.cc analysers/FlatNTuples/src/FlatNTuples.cc

 Description: <one line class summary>

 Implementation:
     <Notes on implementation>
*/
//
// Original Author:  local user
//         Created:  Thu Dec  3 17:34:46 CET 2009
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/Common/interface/TriggerResults.h"

#include <DataFormats/Common/interface/Ref.h>

#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include <map>

#include "DataFormats/Common/interface/View.h"
#include <string>

#include "Jet.h"
#include "Electron.h"
#include "Muon.h"
#include "MET.h"
#include "Collection.h"
#include "TriggerProducer.h"
#include "EventInformationProducer.h"
#include "TTbarGenEventProducer.h"
#include "PrimaryVertexProducer.h"
#include "RecoGenMatch.h"
#include "TriggerNameMapper.h"
#include "TagNameMapper.h"
#include "TriggerObjectProducer.h"
#include "GenParticleProducer.h"

class FlatNTuples : public edm::EDAnalyzer {
   public:
      explicit FlatNTuples(const edm::ParameterSet&);
      ~FlatNTuples();


   private:
	virtual void beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup);
	void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&);
        virtual void analyze(const edm::Event&, const edm::EventSetup&);

      // ----------member data ---------------------------
        edm::InputTag eleLabel_;
        edm::InputTag muoLabel_;
        edm::InputTag jetLabel_;
        edm::InputTag tauLabel_;
        edm::InputTag metLabel_;
        edm::InputTag phoLabel_;
	edm::InputTag PriVertexLabel_;
	edm::InputTag hlTriggerResults_;
	edm::InputTag hltAodSummary_;

	TFile *outfile;
	TTree *tree;
	TTree *trigger_name_mapping;
	TTree *tag_name_mapping;
	beag::Collection<pat::Jet, beag::Jet> *beag_jets;
	beag::Collection<pat::Muon, beag::Muon> *beag_muons;
	beag::Collection<pat::Electron, beag::Electron> *beag_electrons;
	beag::Collection<pat::MET, beag::MET> *beag_mets;
	beag::TriggerProducer *trigger_prod;
	beag::PrimaryVertexProducer *pvertex_prod;
	beag::TTbarGenEventProducer *gen_evt_prod;
	beag::RecoGenMatch *reco_gen_match;
	beag::EventInformationProducer *evt_info_prod;
	beag::TriggerNameMapper *trigger_name_mapper;
	beag::TriggerNameMapper *veto_trigger_name_mapper;
	beag::TagNameMapper *eID_mapper;
	beag::TagNameMapper *muID_mapper;
	beag::TagNameMapper *btag_mapper;
	beag::TriggerObjectProducer *trigger_obj_prod;
	beag::GenParticleProducer *gen_part_prod;

	HLTConfigProvider *hltConfig;

	double current_lumi_block;
	double prev_lumi_block;

	bool write_prescales;
	bool write_d0_wrt_pv;
	bool write_gen_particles;

	int prescale_set;

	std::string trigger_menu;

	std::vector<std::string> *selected_triggers;
	std::vector<std::string> *vetoed_triggers;
	std::vector<std::string> *valid_triggers;

	std::vector<std::string> *muonIDs;
	std::vector<std::string> *electronIDs;

	std::vector<std::string> *btag_algos;

	bool hltConfigChanged;

	bool do_mc_matching;
	bool fill_trigger_objects;
	bool run_changed;
	bool lumi_block_changed;
	bool first_in_job;
};
