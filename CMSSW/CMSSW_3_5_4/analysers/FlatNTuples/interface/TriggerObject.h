#ifndef BEAGTRIGGEROBJECT_H
#define BEAGTRIGGEROBJECT_H

#include "Particle.h"
#include "Rtypes.h"

namespace beag{
	class TriggerObject: public Particle{
		public:
			TriggerObject(){};
			virtual ~TriggerObject(){};
			
			unsigned long long triggered;//results for final module passed
			unsigned long long l1triggered;//trigger results for l1 seeds
		ClassDef(TriggerObject, 1);
	};
}

#endif
