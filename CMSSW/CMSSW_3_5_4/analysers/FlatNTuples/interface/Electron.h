#ifndef BEAG_ELECTRON_H
#define BEAG_ELECTRON_H

#include "Lepton.h"
#include "Rtypes.h"

namespace beag{
	class Electron : public beag::Lepton{
		public:
			Electron():supercluster_eta(0){};
			virtual ~Electron(){};
			
			double supercluster_eta;

		ClassDef(Electron, 1);
	};
}

#endif
