#ifndef BEAG_LEPTON_H
#define BEAG_LEPTON_H

#include "Particle.h"
#include <map>
#include <string>

#include "Rtypes.h"

namespace beag{
	class Lepton : public Particle{
		public:
			Lepton():ecal_iso(0),hcal_iso(0),track_iso(0),
				 hcal_vcone(0),ecal_vcone(0), d0(0), d0_sigma(0),
				 d0_pv(0), d0_sigma_pv(0),
				 lepton_id(0),track_available(false){};
			virtual ~Lepton(){};
			double ecal_iso;	
			double hcal_iso;	
			double track_iso;	

			double hcal_vcone;
			double ecal_vcone;

			double d0;		// d0 wrt offline beamspot
			double d0_sigma;	// error d0 wrt offline beamspot

			double d0_pv;		// d0 wrt primary vertex
			double d0_sigma_pv;	// error d0 wrt primary vertex

			unsigned long long lepton_id;
			bool track_available;

			unsigned long long trigger;	// Trigger bits -> lepton triggered by certain trigger: true / false

		ClassDef(Lepton, 1);
	};
}

#endif
