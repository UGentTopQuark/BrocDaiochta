#ifndef BEAGTRIGGERNAMEMAPPER_H
#define BEAGTRIGGERNAMEMAPPER_H

#include <vector>
#include <map>
#include <string>
#include <boost/regex.hpp>
#include "TFile.h"
#include "TTree.h"
#include "stdlib.h"
#include <iostream>
#include "DataFormats/Common/interface/View.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

namespace beag{
	class TriggerNameMapper{
		public:
		TriggerNameMapper(TTree *tree, TFile *outfile, std::string ident, bool write_prescales,bool write = true);
			~TriggerNameMapper();
			std::vector<std::string>* get_selected_trigger_names(std::vector<std::string> *valid_triggers, HLTConfigProvider *hltConfig, unsigned int prescale_set);
			void set_trigger_expressions(std::vector<std::string> &trigger_expressions);
			
		private:
			std::vector<std::string> *selected_triggers;
			std::vector<unsigned int> *prescales;
			std::vector<std::string> trigger_expressions;

			TTree *tree;
			TFile *outfile;
			
			static const bool verbose = false;

			bool write;
			bool write_prescales;
	};
}

#endif
