#ifndef BEAGTRIGGEROBJECTPRODUCER_H
#define BEAGTRIGGEROBJECTPRODUCER_H

#include "TTree.h"
#include "TFile.h"
#include <vector>
#include <string>
#include "TriggerObject.h"
#include "DataFormats/Common/interface/View.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/Common/interface/TriggerResults.h"

namespace beag{
	class TriggerObjectProducer{
		public:
			TriggerObjectProducer(TTree *tree, TFile *outfile, std::string ident);
			~TriggerObjectProducer();
			void set_triggers_to_write(std::vector<std::string> *triggers);
			void set_triggers_to_veto(std::vector<std::string> *veto_triggers);
			void set_hlt_config_provider(HLTConfigProvider *hltConfig);
			void fill_trigger_objects(edm::Handle<trigger::TriggerEvent> aodTriggerEvent, edm::Handle<edm::TriggerResults> HLTR);
			void set_trigger_menu(std::string trigger_menu);

		private:
			std::vector<beag::TriggerObject> *trigger_objects;
                        std::vector<std::string> *triggers_to_write;

			void get_trigger_objects(int module,int nbeag_trig);
			std::map<int,std::vector<unsigned long long> >  objects_to_write;
			
			edm::Handle<trigger::TriggerEvent> aodTriggerEvent;
			std::map<std::string,bool> triggers_to_veto;

			TTree *tree;
			TFile *outfile;

			HLTConfigProvider *hltConfig;

			std::string trigger_menu;

                        static const bool verbose = false;
	};
}

#endif
