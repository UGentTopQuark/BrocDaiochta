#ifndef BEAG_JET_H
#define BEAG_JET_H

#include "Particle.h"
#include <map>
#include <string>

#include "Rtypes.h"

namespace beag{
	class Jet : public Particle{
		public:
			Jet():ttbar_decay_product(0), emf(0), n90Hits(0), fHPD(0){};
			virtual ~Jet(){};

			std::vector<double> btags;	// bDiscriminator
			/*
			 *	ttbar_decay_product:
			 *	is jet from quark from ttbar decay
			 *	possible values:
			 *	1: quark jet
			 *	2: anti-quark jet
			 *	3: hadronically decaying top b-jet
			 *	4: leptonically decaying top b-jet
			 */
			int ttbar_decay_product;

			double emf;	// electromagnetic fraction
			double n90Hits;	// minimal number of RecHits containing 90% of the jet energy
			double fHPD;	// fraction of energy in the hottest HPD readout

		ClassDef(Jet, 1);
	};
}

#endif
