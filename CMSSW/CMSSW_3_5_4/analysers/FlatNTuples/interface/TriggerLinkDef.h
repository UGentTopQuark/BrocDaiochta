#ifdef __CINT__

#include "Trigger.h"
#include <vector>

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class beag::Trigger;
#pragma link C++ class beag;
#pragma link C++ class std::vector<beag::Trigger>;

#endif
