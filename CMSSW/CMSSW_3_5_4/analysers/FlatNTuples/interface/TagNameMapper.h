#ifndef BEAGTAGNAMEMAPPER_H
#define BEAGTAGNAMEMAPPER_H

#include "TTree.h"
#include "TFile.h"
#include <string>
#include <vector>

namespace beag{
	class TagNameMapper{
		public:
			TagNameMapper(TTree *tree, TFile *outfile, std::string ident);
			~TagNameMapper();
			
			void set_tag_names(std::vector<std::string> *tag_names);
		private:	
			TTree *tree;
			TFile *outfile;

			std::vector<std::string> *selected_tags;
	};
}

#endif
