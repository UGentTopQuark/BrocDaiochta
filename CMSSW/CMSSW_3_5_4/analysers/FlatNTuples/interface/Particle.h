#ifndef BEAG_PARTICLE_H
#define BEAG_PARTICLE_H

#include "Rtypes.h"

namespace beag{
	class Particle{
		public:
			Particle():px(0),py(0),pz(0),e(0), mc_matched(false),from_ttbar_decay(false),charge(0),
				   mc_px(0),mc_py(0),mc_pz(0),mc_e(0),mc_id(0){};
			virtual ~Particle(){};
			double px;
			double py;
			double pz;
			double e;

			bool mc_matched;	// flag if monte carlo truth matched

			bool from_ttbar_decay;	// if particle is from ttbar decay

			char charge;

			double mc_px;
			double mc_py;
			double mc_pz;
			double mc_e;
			double mc_id;		// id of monte carlo truth matched parton

		ClassDef(Particle, 1);
	};
}

#endif
