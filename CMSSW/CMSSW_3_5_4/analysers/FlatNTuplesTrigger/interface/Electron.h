#ifndef BEAG_ELECTRON_H
#define BEAG_ELECTRON_H

#include "Lepton.h"
#include "Rtypes.h"

namespace beag{
	class Electron : public beag::Lepton{
		public:
			Electron():eidRobustLoose(false){};
			virtual ~Electron(){};
			
			bool eidRobustLoose;

		ClassDef(Electron, 1);
	};
}

#endif
