#ifndef BEAG_TRIGGER_H
#define BEAG_TRIGGER_H

#include <map>
#include <string>

#include "Rtypes.h"

namespace beag{
	class Trigger{
		public:
			virtual ~Trigger(){};

			std::map<std::string,bool> triggered;

		ClassDef(Trigger, 1);
	};
}

#endif
