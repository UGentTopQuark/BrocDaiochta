#ifdef __CINT__

#include "Particle.h"
#include <vector>

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;
#pragma link C++ class beag::Particle;
#pragma link C++ class beag;
#pragma link C++ class std::vector<beag::Particle>;

#endif
