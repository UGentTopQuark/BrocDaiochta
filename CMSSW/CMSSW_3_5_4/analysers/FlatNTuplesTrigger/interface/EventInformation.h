#ifndef BEAG_EVENTINFORMATION_H
#define BEAG_EVENTINFORMATION_H

#include "Rtypes.h"

namespace beag{
	class EventInformation{
		public:
			EventInformation():lumi_block(0), event_number(0), run(0),
					trigger_changed(false){};
			virtual ~EventInformation(){};

			double lumi_block;
			double event_number;
			double run;

			bool trigger_changed;
		
		ClassDef(EventInformation, 1);
	};
}

#endif
