// -*- C++ -*-
//
// Package:    FlatNTuples
// Class:      FlatNTuples
// 
/**\class FlatNTuples FlatNTuples.cc analysers/FlatNTuples/src/FlatNTuples.cc

 Description: <one line class summary>

 Implementation:
     <Notes on implementation>
*/
//
// Original Author:  local user
//         Created:  Thu Dec  3 17:34:46 CET 2009
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

#include <DataFormats/Common/interface/Ref.h>

#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include <map>

#include "DataFormats/Common/interface/View.h"
#include <string>

#include "Jet.h"
#include "Electron.h"
#include "Muon.h"
#include "MET.h"
#include "Collection.h"
#include "TriggerProducer.h"
#include "EventInformationProducer.h"
#include "TTbarGenEventProducer.h"
#include "PrimaryVertexProducer.h"
#include "RecoGenMatch.h"

class FlatNTuplesTrigger : public edm::EDAnalyzer {
   public:
      explicit FlatNTuplesTrigger(const edm::ParameterSet&);
      ~FlatNTuplesTrigger();


   private:
	virtual void beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup);
        virtual void analyze(const edm::Event&, const edm::EventSetup&);

      // ----------member data ---------------------------
        edm::InputTag eleLabel_;
        edm::InputTag muoLabel_;
        edm::InputTag jetLabel_;
        edm::InputTag tauLabel_;
        edm::InputTag metLabel_;
        edm::InputTag phoLabel_;
	edm::InputTag PriVertexLabel_;
	edm::InputTag hlTriggerResults_;

	TFile *outfile;
	TTree *tree;
	TTree *name_mapping;
	beag::Collection<pat::Jet, beag::Jet> *beag_jets;
	beag::Collection<pat::Muon, beag::Muon> *beag_muons;
	beag::Collection<pat::Electron, beag::Electron> *beag_electrons;
	beag::Collection<pat::MET, beag::MET> *beag_mets;
	beag::TriggerProducer *trigger_prod;
	beag::PrimaryVertexProducer *pvertex_prod;
	beag::TTbarGenEventProducer *gen_evt_prod;
	beag::RecoGenMatch *reco_gen_match;
	beag::EventInformationProducer *evt_info_prod;

	HLTConfigProvider *hltConfig;
	
	double current_lumi_block;
	double prev_lumi_block;

	std::string trigger_menu;

	bool hltConfigChanged;

	bool do_mc_matching;
	bool run_changed;
};
