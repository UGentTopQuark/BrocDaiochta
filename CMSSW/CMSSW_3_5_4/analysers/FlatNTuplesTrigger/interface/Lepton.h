#ifndef BEAG_LEPTON_H
#define BEAG_LEPTON_H

#include "Particle.h"
#include <map>
#include <string>

#include "Rtypes.h"

namespace beag{
	class Lepton : public Particle{
		public:
			Lepton():ecal_iso(0),hcal_iso(0),track_iso(0),
				 hcal_vcone(0),ecal_vcone(0), d0(0), d0_sigma(0),
				 charge(0),lepton_id(0),track_available(false){};
			virtual ~Lepton(){};
			double ecal_iso;	
			double hcal_iso;	
			double track_iso;	

			double hcal_vcone;
			double ecal_vcone;

			double d0;
			double d0_sigma;

			int charge;

			bool lepton_id;
			bool track_available;

			std::map<std::string,bool> trigger;	// Trigger bits -> lepton triggered by certain trigger:
								// true / false

		ClassDef(Lepton, 1);
	};
}

#endif
