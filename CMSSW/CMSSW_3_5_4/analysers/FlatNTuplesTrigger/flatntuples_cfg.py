import FWCore.ParameterSet.Config as cms

process = cms.Process("NTuples")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
#      'dcap:///pnfs/iihe/cms/store/user/bklein/data/7TeV1stData/MinimumBiasjob_5_reco_7TeV_firstdata_356_pat.root',
      'dcap:///pnfs/iihe/cms/store/user/bklein/data/7TeV_MC_MinBias/MinBiasjob_214_reco_7TeV_firstdata_356_pat.root',
    )
)

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(300) )

process.produceNTuples = cms.EDAnalyzer('FlatNTuplesTrigger',
    electronTag = cms.untracked.InputTag("selectedPatElectrons"),
    tauTag      = cms.untracked.InputTag("selectedPatTaus"),
    muonTag     = cms.untracked.InputTag("selectedPatMuons"),
    jetTag      = cms.untracked.InputTag("selectedPatJets"),
    photonTag   = cms.untracked.InputTag("selectedPatPhotons"),
    metTag      = cms.untracked.InputTag("patMETs"),
    primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
      HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT'),
      TriggerList      = cms.vstring("HLT_Ele15_LW_L1R","HLT_Mu9"),
      BTagAlgorithms	= cms.vstring("trackCountingHighEffBJetTags",
      				      "jetProbabilityBJetTags"),
	MCMatching	= cms.bool(True),
	outfile		= cms.string("ttbar.root")
)


process.p1 = cms.Path(process.produceNTuples)

process.schedule = cms.Schedule( process.p1 )
