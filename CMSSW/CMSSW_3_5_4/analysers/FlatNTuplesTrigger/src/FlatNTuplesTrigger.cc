#include "../interface/FlatNTuplesTrigger.h"

FlatNTuplesTrigger::FlatNTuplesTrigger(const edm::ParameterSet& iConfig):
  eleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("electronTag")),
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  tauLabel_(iConfig.getUntrackedParameter<edm::InputTag>("tauTag")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  phoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("photonTag")),
  PriVertexLabel_(iConfig.getUntrackedParameter<edm::InputTag>("primaryVertexTag")),
  hlTriggerResults_ (iConfig.getParameter<edm::InputTag> ("HLTriggerResults"))
{
	do_mc_matching = iConfig.getParameter<bool>("MCMatching");

	std::string outfile_name = iConfig.getParameter<std::string>("outfile");
   	outfile = new TFile(outfile_name.c_str(),"RECREATE");
	outfile->cd();
   	tree = new TTree("tree","tree");
   	//name_mapping = new TTree("name_mapping","name_mapping");
   	name_mapping = NULL;

	beag_jets = new beag::Collection<pat::Jet, beag::Jet>(tree, outfile, "jets");
	beag_mets = new beag::Collection<pat::MET, beag::MET>(tree, outfile, "mets");
	beag_muons = new beag::Collection<pat::Muon, beag::Muon>(tree, outfile, "muons");
	beag_electrons = new beag::Collection<pat::Electron, beag::Electron>(tree, outfile, "electrons");
	trigger_prod = new beag::TriggerProducer(tree, outfile, "trigger");
	evt_info_prod = new beag::EventInformationProducer(tree, outfile, "evt_info");
	pvertex_prod = new beag::PrimaryVertexProducer(tree, outfile, "pvertices");

	gen_evt_prod = NULL;
	if(do_mc_matching) gen_evt_prod = new beag::TTbarGenEventProducer(tree, outfile, "gen_evt");
	reco_gen_match = NULL;
	if(do_mc_matching) reco_gen_match = new beag::RecoGenMatch();

	hltConfig = new HLTConfigProvider();
	std::vector<std::string> triggers = iConfig.getParameter<std::vector<std::string> >("TriggerList");
	trigger_menu = hlTriggerResults_.process();
	trigger_prod->set_triggers_to_write(triggers, trigger_menu);
	trigger_prod->set_hlt_config_provider(hltConfig);

	std::vector<std::string> btag_algos = iConfig.getParameter<std::vector<std::string> >("BTagAlgorithms");
	beag_jets->set_selection_list(btag_algos);

	std::vector<std::string> mu_trigger;
	mu_trigger.push_back("hltSingleMuNoIsoL3PreFiltered11");
	beag_muons->set_selection_list(mu_trigger);
	beag_muons->set_mc_matching(do_mc_matching);

	std::vector<std::string> e_trigger;
	e_trigger.push_back("hltL1NonIsoHLTNonIsoSingleElectronLWEt15TrackIsolFilter");
	beag_electrons->set_selection_list(e_trigger);
	beag_electrons->set_mc_matching(do_mc_matching);

	current_lumi_block = -1;
	prev_lumi_block = -1;

	hltConfigChanged = true;
	run_changed = true;
}

FlatNTuplesTrigger::~FlatNTuplesTrigger()
{
	if(hltConfig){
		delete hltConfig;
		hltConfig = NULL;
	}
	if(reco_gen_match){
		delete reco_gen_match;
		reco_gen_match = NULL;
	}
	if(trigger_prod){
		delete trigger_prod;
		trigger_prod = NULL;
	}
	if(evt_info_prod){
		delete evt_info_prod;
		evt_info_prod = NULL;
	}
	if(beag_jets){
		delete beag_jets;
		beag_jets = NULL;
	}
	if(beag_electrons){
		delete beag_electrons;
		beag_electrons = NULL;
	}
	if(beag_muons){
		delete beag_muons;
		beag_muons = NULL;
	}
	if(beag_mets){
		delete beag_mets;
		beag_mets = NULL;
	}
	outfile->cd();
   	tree->Write();
   	if(tree){
   		delete tree;
		tree = NULL;
	}
   	//name_mapping->Write();
   	if(name_mapping){
   		delete name_mapping;
		name_mapping = NULL;
	}
   	outfile->Write();
   	outfile->Close();
   	if(outfile){
   		delete outfile;
		outfile = NULL;
	}
}


//
// member functions
//

// ------------ method called to for each event  ------------
void
FlatNTuplesTrigger::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	using namespace edm;

        edm::Handle<edm::View<pat::Electron> > electronHandle;
        iEvent.getByLabel(eleLabel_,electronHandle);

        edm::Handle<edm::View<pat::Muon> > muonHandle;
        iEvent.getByLabel(muoLabel_,muonHandle);

        edm::Handle<edm::View<pat::MET> > metHandle;
        iEvent.getByLabel(metLabel_,metHandle);

        edm::Handle<edm::View<pat::Jet> > jetHandle;
        iEvent.getByLabel(jetLabel_,jetHandle);

        edm::Handle<reco::GenParticleCollection> genParticles;

	outfile->cd();

	if(do_mc_matching){
        	iEvent.getByLabel("genParticles", genParticles);
	}

	edm::Handle<edm::TriggerResults> HLTR;
	iEvent.getByLabel(hlTriggerResults_,HLTR);

	edm::Handle<reco::BeamSpot> beamSpotHandle;
	iEvent.getByLabel("offlineBeamSpot", beamSpotHandle);

	edm::Handle<std::vector<reco::Vertex> > pvertexHandle;	
	iEvent.getByLabel(PriVertexLabel_, pvertexHandle);

	pvertex_prod->fill_primary_vertices(pvertexHandle);

	beag_jets->fill_collection(jetHandle);
	beag_mets->fill_collection(metHandle);

	beag_muons->set_beamspot_handle(beamSpotHandle);
	beag_muons->fill_collection(muonHandle);

	beag_electrons->set_beamspot_handle(beamSpotHandle);
	beag_electrons->fill_collection(electronHandle);

	// FIXME: this should go to beginRun and is slowing down the program a lot here
	// FIXME: this should probably anyway change per lumi section, not run
	if(run_changed){
		hltConfig->init(iEvent, trigger_menu, hltConfigChanged);
	}
	trigger_prod->fill_trigger_information(HLTR);

	evt_info_prod->fill_event_information(iEvent.run(), iEvent.luminosityBlock(), iEvent.id().event(), hltConfigChanged);

	if(do_mc_matching) gen_evt_prod->fill_mc_information(genParticles);

	if(do_mc_matching) reco_gen_match->set_gen_event(gen_evt_prod->get_gen_event());
	if(do_mc_matching) reco_gen_match->match_jets_to_partons(beag_jets->get_beag_objects());
	if(do_mc_matching) reco_gen_match->match_leptons_to_particles<beag::Muon>(beag_muons->get_beag_objects());
	if(do_mc_matching) reco_gen_match->match_leptons_to_particles<beag::Electron>(beag_electrons->get_beag_objects());

	// Fill tree for all variables
	tree->Fill();

	// if luminosity section changes, write out mapping between names and ids
	/*
	current_lumi_block = iEvent.luminosityBlock();
	if(current_lumi_block != prev_lumi_block){
		name_mapping->Fill();
	}
	prev_lumi_block = current_lumi_block;
	*/
	run_changed = false;
}

void FlatNTuplesTrigger::beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup)
{
	run_changed = true;
}

//define this as a plug-in
DEFINE_FWK_MODULE(FlatNTuplesTrigger);
