#include "../interface/Collection.h"

namespace beag{
template void beag::Collection<pat::Jet, beag::Jet>::fill_collection(edm::Handle<edm::View<pat::Jet> > handle);
template void beag::Collection<pat::Electron, beag::Electron>::fill_collection(edm::Handle<edm::View<pat::Electron> > handle);
template void beag::Collection<pat::Muon, beag::Muon>::fill_collection(edm::Handle<edm::View<pat::Muon> > handle);
template void beag::Collection<pat::MET, beag::MET>::fill_collection(edm::Handle<edm::View<pat::MET> > handle);

template void beag::Collection<pat::Jet, beag::Jet>::fill_common_vars(edm::View<pat::Jet>::const_iterator cmssw_obj, beag::Jet &beag_obj);
template beag::Collection<pat::Jet, beag::Jet>::Collection(TTree *tree, TFile *outfile, std::string ident);
template beag::Collection<pat::Jet, beag::Jet>::~Collection();
template void beag::Collection<pat::Electron, beag::Electron>::fill_common_vars(edm::View<pat::Electron>::const_iterator cmssw_obj, beag::Electron &beag_obj);
template beag::Collection<pat::Electron, beag::Electron>::Collection(TTree *tree, TFile *outfile, std::string ident);
template beag::Collection<pat::Electron, beag::Electron>::~Collection();
template void beag::Collection<pat::Muon, beag::Muon>::fill_common_vars(edm::View<pat::Muon>::const_iterator cmssw_obj, beag::Muon &beag_obj);
template beag::Collection<pat::Muon, beag::Muon>::Collection(TTree *tree, TFile *outfile, std::string ident);
template beag::Collection<pat::Muon, beag::Muon>::~Collection();
template void beag::Collection<pat::MET, beag::MET>::fill_common_vars(edm::View<pat::MET>::const_iterator cmssw_obj, beag::MET &beag_obj);
template beag::Collection<pat::MET, beag::MET>::Collection(TTree *tree, TFile *outfile, std::string ident);
template beag::Collection<pat::MET, beag::MET>::~Collection();

template void beag::Collection<pat::Jet, beag::Jet>::set_selection_list(std::vector<std::string> selection_list);
template void beag::Collection<pat::Muon, beag::Muon>::set_selection_list(std::vector<std::string> selection_list);
template void beag::Collection<pat::Electron, beag::Electron>::set_selection_list(std::vector<std::string> selection_list);

template void beag::Collection<pat::Muon, beag::Muon>::set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle);
template void beag::Collection<pat::Electron, beag::Electron>::set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle);

template void beag::Collection<pat::Muon, beag::Muon>::match_trigger(edm::View<pat::Muon>::const_iterator cmssw_obj, beag::Muon &beag_obj);
template void beag::Collection<pat::Electron, beag::Electron>::match_trigger(edm::View<pat::Electron>::const_iterator cmssw_obj, beag::Electron &beag_obj);

template void beag::Collection<pat::Muon, beag::Muon>::write_gen_info(edm::View<pat::Muon>::const_iterator cmssw_obj, beag::Muon &beag_obj);
template void beag::Collection<pat::Electron, beag::Electron>::write_gen_info(edm::View<pat::Electron>::const_iterator cmssw_obj, beag::Electron &beag_obj);

template std::vector<beag::Muon>* beag::Collection<pat::Muon, beag::Muon>::get_beag_objects();
template std::vector<beag::Electron>* beag::Collection<pat::Electron, beag::Electron>::get_beag_objects();
template std::vector<beag::Jet>* beag::Collection<pat::Jet, beag::Jet>::get_beag_objects();

template void beag::Collection<pat::Muon, beag::Muon>::set_mc_matching(bool match_mc);
template void beag::Collection<pat::Electron, beag::Electron>::set_mc_matching(bool match_mc);

template <class CMSSWObject, class BeagObject>
beag::Collection<CMSSWObject, BeagObject>::Collection(TTree *tree, TFile *outfile, std::string ident)
{
	mc_matching = true;
	this->tree = tree;
	this->outfile = outfile;

	pt_sorter = new PtSorter<BeagObject>();

	beag_objects = new std::vector<BeagObject>();
	tree->Bronch(ident.c_str(),get_type_string(),&beag_objects);
}

template <class CMSSWObject, class BeagObject>
beag::Collection<CMSSWObject, BeagObject>::~Collection()
{
	if(beag_objects){
		delete beag_objects;
		beag_objects = NULL;
	}
	if(pt_sorter){
		delete pt_sorter;
		pt_sorter = NULL;
	}
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle)
{
	outfile->cd();
	beag_objects->clear();

	for(typename edm::View<CMSSWObject>::const_iterator cmssw_obj = handle->begin();
		cmssw_obj != handle->end();
		++cmssw_obj){
		BeagObject *beag_obj = new BeagObject();

		fill_common_vars(cmssw_obj, *beag_obj);
		fill_special_vars(cmssw_obj, *beag_obj);

		pt_sorter->add(beag_obj);
	}

	std::vector<BeagObject*> tmp_objects = pt_sorter->get_sorted();

	for(typename std::vector<BeagObject*>::iterator tmp_obj = tmp_objects.begin();
		tmp_obj != tmp_objects.end();
		++tmp_obj){
		beag_objects->push_back(**tmp_obj);
		delete *tmp_obj;
	}

	pt_sorter->clear();
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::fill_common_vars(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
	beag_obj.e = cmssw_obj->energy();
	beag_obj.px = cmssw_obj->px();
	beag_obj.py = cmssw_obj->py();
	beag_obj.pz = cmssw_obj->pz();
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::fill_special_vars(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
}

template <>
void beag::Collection<pat::Muon, beag::Muon>::fill_special_vars(edm::View<pat::Muon>::const_iterator pat_muon, beag::Muon &beag_muon)
{
	beag_muon.ecal_iso = pat_muon->isolationR03().emEt;
	beag_muon.hcal_iso = pat_muon->isolationR03().hadEt;
	beag_muon.track_iso = pat_muon->isolationR03().sumPt;

	beag_muon.hcal_vcone = pat_muon->isolationR03().hadVetoEt;
	beag_muon.ecal_vcone = pat_muon->isolationR03().emVetoEt;

	reco::TrackRef track = pat_muon->track();
	if(!track.isNull() && track.isAvailable()){
		if(beamSpotHandle.isValid()){
			reco::BeamSpot beamSpot=*beamSpotHandle;
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());

			beag_muon.d0 = -1.*track->dxy(point);
			beag_muon.d0_sigma = sqrt( track->d0Error() * track->d0Error() + sqrt(beamSpot.BeamWidthX()*beamSpot.BeamWidthX()+beamSpot.BeamWidthY()*beamSpot.BeamWidthY()) );
		}else
			std::cerr << "WARNING: beag::Collection<Muon>::fill_special_vars(): no valid beamspot" << std::endl;

		beag_muon.chi2 = track->chi2() / track->ndof();
		beag_muon.nHits = track->numberOfValidHits();

		beag_muon.track_available = true;
	}else{
		beag_muon.track_available = false;
		std::cerr << "WARNING: beag::Collection<Muon>::fill_special_vars(): no track available" << std::endl;
	}

	if(pat_muon->isGlobalMuon())
		beag_muon.lepton_id = true;
	else
		beag_muon.lepton_id = false;

	beag_muon.charge = pat_muon->charge();

	if(mc_matching) match_trigger(pat_muon, beag_muon);
	if(mc_matching) write_gen_info(pat_muon, beag_muon);
}

template <>
void beag::Collection<pat::Electron, beag::Electron>::fill_special_vars(edm::View<pat::Electron>::const_iterator pat_electron, beag::Electron &beag_electron)
{
	beag_electron.ecal_iso = pat_electron->dr04EcalRecHitSumEt();
	beag_electron.hcal_iso = pat_electron->dr04HcalTowerSumEt();
	beag_electron.track_iso = pat_electron->dr04TkSumPt();

	beag_electron.hcal_vcone = -1;
	beag_electron.ecal_vcone = -1;

	reco::GsfTrackRef track = pat_electron->gsfTrack();
	if(!track.isNull() && track.isAvailable()){
		if(beamSpotHandle.isValid()){
			reco::BeamSpot beamSpot=*beamSpotHandle;
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());

			beag_electron.d0 = -1.*track->dxy(point);
			beag_electron.d0_sigma = sqrt( track->d0Error() * track->d0Error() + sqrt(beamSpot.BeamWidthX()*beamSpot.BeamWidthX()+beamSpot.BeamWidthY()*beamSpot.BeamWidthY()) );
		}else
			std::cerr << "WARNING: beag::Collection<Electron>::fill_special_vars(): no valid beamspot" << std::endl;

		beag_electron.track_available = true;
	}else{
		std::cerr << "WARNING: beag::Collection<Electron>::fill_special_vars(): no track available" << std::endl;
		beag_electron.track_available = false;
	}

	if(pat_electron->isElectronIDAvailable("eidRobustTight") && pat_electron->electronID("eidRobustTight") == 1.0)
		beag_electron.lepton_id = true;
	else
		beag_electron.lepton_id = false;

	if(pat_electron->isElectronIDAvailable("eidRobustLoose") && pat_electron->electronID("eidRobustLoose") == 1.0)
		beag_electron.eidRobustLoose = true;
	else
		beag_electron.eidRobustLoose = false;

	beag_electron.charge = pat_electron->charge();

	if(mc_matching) match_trigger(pat_electron, beag_electron);
	if(mc_matching) write_gen_info(pat_electron, beag_electron);
}

template <>
void beag::Collection<pat::Jet, beag::Jet>::fill_special_vars(edm::View<pat::Jet>::const_iterator pat_jet, beag::Jet &beag_jet)
{
	// fill btag information from pat jet for selected algorithms
	for(std::vector<std::string>::iterator btag_algo = selected_items.begin();
		btag_algo != selected_items.end();
		++btag_algo){
		beag_jet.btags[*btag_algo] = pat_jet->bDiscriminator(*btag_algo);
	}

	beag_jet.emf = pat_jet->emEnergyFraction();
	beag_jet.n90Hits = pat_jet->jetID().n90Hits;
	beag_jet.fHPD = pat_jet->jetID().fHPD;

	// write information of initiating parton
	if(pat_jet->genParton() != NULL){
		beag_jet.mc_px = pat_jet->genParton()->px();
		beag_jet.mc_py = pat_jet->genParton()->py();
		beag_jet.mc_pz = pat_jet->genParton()->pz();
		beag_jet.mc_e = pat_jet->genParton()->energy();
		beag_jet.mc_id = pat_jet->genParton()->pdgId();

		beag_jet.mc_matched = true;
	}else{
		beag_jet.mc_matched = false;
	}
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::write_gen_info(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
	// write information of initiating particle
	if(cmssw_obj->genParticle() != NULL){
		beag_obj.mc_px = cmssw_obj->genParticle()->px();
		beag_obj.mc_py = cmssw_obj->genParticle()->py();
		beag_obj.mc_pz = cmssw_obj->genParticle()->pz();
		beag_obj.mc_e = cmssw_obj->genParticle()->energy();
		beag_obj.mc_id = cmssw_obj->genParticle()->pdgId();

		beag_obj.mc_matched = true;
	}else{
		beag_obj.mc_matched = false;
	}
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::match_trigger(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
	std::map<std::string, std::string> dictionary;
	dictionary["hltSingleMuNoIsoL3PreFiltered11"] = "HLT_Mu11";
	dictionary["hltL1NonIsoHLTNonIsoSingleElectronLWEt15TrackIsolFilter"] = "HLT_Ele15_LW_L1R";

	// check for all leptons if they are triggered leptons
	for(std::vector<std::string>::iterator trigger = selected_items.begin();
		trigger != selected_items.end();
		++trigger){
		if(cmssw_obj->triggerObjectMatchesByFilter(*trigger).size() != 0)
			beag_obj.trigger[dictionary[*trigger]] = true;
		else
			beag_obj.trigger[dictionary[*trigger]] = false;
	}
}

template <class CMSSWObject, class BeagObject>
const char* beag::Collection<CMSSWObject, BeagObject>::get_type_string()
{
	std::cerr << "ERROR: invalid typename in beag::Collection::get_type_string()" << std::endl;
	return "";
}

template <>
const char* beag::Collection<pat::Jet, beag::Jet>::get_type_string()
{
	return "std::vector<beag::Jet>";
}

template <>
const char* beag::Collection<pat::Electron, beag::Electron>::get_type_string()
{
	return "std::vector<beag::Electron>";
}

template <>
const char* beag::Collection<pat::Muon, beag::Muon>::get_type_string()
{
	return "std::vector<beag::Muon>";
}

template <>
const char* beag::Collection<pat::MET, beag::MET>::get_type_string()
{
	return "std::vector<beag::MET>";
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_selection_list(std::vector<std::string> selection_list)
{
	selected_items = selection_list;
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle)
{
	this->beamSpotHandle = beamSpotHandle;
}

template <class CMSSWObject, class BeagObject>
std::vector<BeagObject>* beag::Collection<CMSSWObject, BeagObject>::get_beag_objects()
{
	return beag_objects;
}


template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_mc_matching(bool match_mc)
{
	mc_matching = match_mc;
}

}	// end namespace beag
