#include "../interface/EventInformationProducer.h"

beag::EventInformationProducer::EventInformationProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	event_info = new std::vector<beag::EventInformation>();
	event_info->push_back(beag::EventInformation());
	tree->Bronch(ident.c_str(),"std::vector<beag::EventInformation>",&event_info);
}

beag::EventInformationProducer::~EventInformationProducer()
{
	if(event_info){
		delete event_info;
		event_info = NULL;
	}
}

void beag::EventInformationProducer::fill_event_information(double run, double lumi_block, double event_number, bool trigger_changed)
{
	event_info->begin()->run = run;
	event_info->begin()->lumi_block = lumi_block;
	event_info->begin()->event_number = event_number;
	event_info->begin()->trigger_changed = trigger_changed;
}
