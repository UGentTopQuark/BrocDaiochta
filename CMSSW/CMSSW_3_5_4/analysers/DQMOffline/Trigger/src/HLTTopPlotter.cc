 /** \file DQMOffline/Trigger/HLTTopPlotter.cc
 *
 *  Muon HLT Offline DQM plotting code
 *  This object will make occupancy/efficiency plots for a
 *  specific set of conditions:
 *    1. A set of selection cuts
 *    2. A trigger name
 *  
 *  $Author: slaunwhj $
 *  $Date: 2009/11/13 12:39:32 $
 *  $Revision: 1.2 $
 */



#include "DQMOffline/Trigger/interface/HLTTopPlotter.h"




#include "DataFormats/Math/interface/deltaR.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "DataFormats/Common/interface/Handle.h"
#include "DataFormats/HLTReco/interface/TriggerEventWithRefs.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/Candidate/interface/CandMatchMap.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/Common/interface/TriggerResults.h"


// For storing calorimeter isolation info in the ntuple
#include "DataFormats/RecoCandidate/interface/IsoDeposit.h"
#include "DataFormats/RecoCandidate/interface/IsoDepositFwd.h"

#include "DataFormats/TrackReco/interface/TrackFwd.h"

#include "TPRegexp.h"
#include <iostream>

using namespace std;
using namespace edm;
using namespace reco;
using namespace trigger;
using namespace l1extra;

//using HLTMuonMatchAndPlot::MatchStruct;

typedef std::vector< edm::ParameterSet > Parameters;
typedef std::vector<reco::Muon> MuonCollection;

// const int numCones     = 3;
// const int numMinPtCuts = 1;
// double coneSizes[] = { 0.20, 0.24, 0.30 };
// double minPtCuts[] = { 0. };


/// Constructor
HLTTopPlotter::HLTTopPlotter
( const ParameterSet& pset, string triggerName, vector<string> moduleNames,
  MuonSelectionStruct inputSelection, string customName,
  vector<string> validTriggers)
  : HLTMuonMatchAndPlot(pset, triggerName, moduleNames, inputSelection, customName, validTriggers)
    
{

  CaloJetInputTag       = pset.getParameter<edm::InputTag>("CaloJetInputTag");
  JetIDInputTag       = pset.getParameter<edm::InputTag>("JetIDInputTag");
  EtaCut_               = pset.getUntrackedParameter<double>("EtaCut");
  PtCut_                = pset.getUntrackedParameter<double>("PtCut");   
  NJets_                = pset.getUntrackedParameter<int>("NJets");   
  theJetMParameters    = pset.getUntrackedParameter< vector<double> >("JetMParameters");

  HLT_trigger = triggerName;
  required_triggers = inputSelection.requiredTriggers;
  n_events = 0;
  n_muons_reco_plot = 0;
  n_muons_L1_plot = 0;
  n_muons_HLT_plot = 0;
  n_events_passed_jet = 0;

  //print_string_vector(" List of Valid Triggers", validTriggers); 
  
  LogTrace ("HLTMuonVal") << "\n\n Inside HLTTopPlotter Constructor";
  LogTrace ("HLTMuonVal") << "The trigger name is " << triggerName
                          << "and we've done all the other intitializations";

  LogTrace ("HLTMuonVal") << "exiting constructor\n\n";

}


void HLTTopPlotter::print_string_vector(std::string id,  std::vector<std::string> vresult)
{

        std::cout << id << ": ";

        if(vresult.size() == 0)
                std::cout << "-1" << std::endl;
        else{
                for(std::vector<std::string>::iterator str_iter = vresult.begin();
                    str_iter != vresult.end();
                    ++str_iter)
                        if(str_iter != vresult.begin())
                                std::cout << ":" << *str_iter;
                        else
                                std::cout << *str_iter;
                std::cout << std::endl;
        }
}

void HLTTopPlotter::finish()
{

  // you could do something else in here
  // but for now, just do what the base class
  // would have done
	std::cout << "/******************** Job Summary ****************************/" << std::endl;
	print_string_vector(" Required Triggers", required_triggers);
	std::cout << " HLT Trigger: " << HLT_trigger << std::endl;
	std::cout << " Events examined: " << n_events << std::endl;
	std::cout << " Events passed jet cut: " << n_events_passed_jet << std::endl;
	std::cout << " N muons passed reco cuts (_All): " << n_muons_reco_plot << std::endl;
	std::cout << " N L1 muons matched to reco (_L1): " << n_muons_L1_plot << std::endl;
	std::cout << " N HLT muons matched to reco (_L3): " << n_muons_HLT_plot << std::endl;
	std::cout << "/*************************************************************/" << std::endl;
  
  HLTMuonMatchAndPlot::finish();
}



void HLTTopPlotter::analyze( const Event & iEvent )
{
	//Count number of events you run over
	n_events++;

  LogTrace ("HLTMuonVal") << "Inside of TopPlotter analyze method!"
                          << "calling my match and plot module's analyze..."
                          << endl;

  // Make sure you are valid before proceeding


  // Do some top specific selection, then call the muon matching
  // if the event looks top-like

  LogTrace ("HLTMuonVal") << "Do top-specific selection" << endl;
  

  // get calo jet collection
  Handle<View<CaloJet> > jetsHandle;
  iEvent.getByLabel(CaloJetInputTag, jetsHandle);

  // get jet ID 
  Handle<JetIDValueMap> jetIDHandle;
  iEvent.getByLabel(JetIDInputTag, jetIDHandle);

  int n_jets_20 = 0;
  CaloJetCollection selectedJets;
  
		    std::cout << "bb4 jet handle valid" << std::endl;
  if (jetsHandle.isValid()) {
    LogTrace ("HLTMuonVal") << "Looking in jet collection" << endl;
    //Jet Collection to use
    std::cout << "jet handle valid" << std::endl;
    
    // Raw jets
    const View<CaloJet> *jets = jetsHandle.product();
    View<CaloJet>::const_iterator jet;

    // The parameters for the n jets should be sem.support@cern.ch

    unsigned int idx;
    for (jet = jets->begin(); jet != jets->end(); jet++){

	    if (!(fabs(jet->eta()) <EtaCut_ && jet->et() > PtCut_))
		    continue;

	    std::cout << "Passed eta pt  " << jet->pt() << " : "<< jet->eta() <<std::endl;
	    //Apply quality criteria recommended for jets in minbias data

 	    //Get jetID structure for this jet
 	    idx = jet - jetsHandle->begin();
 	    RefToBase<CaloJet> jetRef = jetsHandle->refAt(idx);
 	    reco::JetID jetId = (*jetIDHandle)[jetRef];
	    
	    //n90Hits recommended != n90
	    if(!(jetId.fHPD < 0.98 && jetId.n90Hits > 1))
		    continue;

	    std::cout << "Passed jetId cuts " <<  jetId.fHPD << "  " << jetId.n90Hits <<std::endl;

	    if (!(jet->emEnergyFraction() > 0.01))
		    continue;
	    std::cout << "Passed em fraction " << jet->emEnergyFraction()  <<std::endl;

	    n_jets_20++;	    
	    selectedJets.push_back((*jet));
      
    } 
    
  }
  
 

  // sort the result
  sortJets(selectedJets);

  
  
  LogTrace ("HLTMuonVal") << "Number of jets in this event = "
                          << n_jets_20
                          << endl;
  /********Jet Cut**********/
 // if (n_jets_20 <= 1 ) {
  if (n_jets_20 < NJets_ ) {
    LogTrace ("HLTMuonVal") << "Not enought jets in this event, skipping it"
                            << endl;

    return;
  }
  n_events_passed_jet++;
 

  /////////////////////////////////////////////////
  //
  //     Call the other analyze method
  //
  /////////////////////////////////////////////////
  


  LogTrace("HLTMuonVal") << "Calling analyze for muon ana" << endl;
  HLTMuonMatchAndPlot::analyze(iEvent);


  LogTrace ("HLTMuonVal") << "TOPPLOT: returned from muon ana, now in top module"
                          << endl
                          << "muon ana stored size rec muons = "
    //<< myMuonAna->recMatches.size()
                          << endl;
  
  vector<HLTMuonMatchAndPlot::MatchStruct>::const_iterator iRecMuon;

  int numCands = 0;
  for ( unsigned int i  = 0;
        i < recMatches.size();
        i++ ) {


    LogTrace ("HLTMuonVal") << "Cand " << numCands
                            << ", Pt = "
                            << recMatches[i].recCand->pt()
                            << ", eta = "
                            << recMatches[i].recCand->eta()
                            << ", phi = "
                            << recMatches[i].recCand->phi()
                            << endl;

    
    double deltaRLeadJetLep;
    if(n_jets_20 != 0)
	    deltaRLeadJetLep = reco::deltaR (recMatches[i].recCand->eta(), recMatches[i].recCand->phi(),
                                       selectedJets[0].eta(), selectedJets[0].phi());

    ////////////////////////////////////////////
    //
    //   Fill Plots for All
    //
    ////////////////////////////////////////////
    
    if(n_jets_20 != 0)
	    hDeltaRMaxJetLep[0]->Fill(deltaRLeadJetLep);
    hJetMultip[0]->Fill(n_jets_20);
    n_muons_reco_plot++;


    ////////////////////////////////////////////
    //
    //   Fill Plots for L1
    //
    ////////////////////////////////////////////

    
    if ( (recMatches[i].l1Cand.pt() > 0) && ((useFullDebugInformation) || (isL1Path)) ) {
      hDeltaRMaxJetLep[1]->Fill(deltaRLeadJetLep);
      hJetMultip[1]->Fill(n_jets_20);
      n_muons_L1_plot++;
    }

    ////////////////////////////////////////////
    //
    //   Fill Plots for HLT
    //
    ////////////////////////////////////////////
    
    for ( size_t j = 0; j < recMatches[i].hltCands.size(); j++ ) {
      if ( recMatches[i].hltCands[j].pt() > 0 ) {
        // you've found it!
        hDeltaRMaxJetLep[j+HLT_PLOT_OFFSET]->Fill(deltaRLeadJetLep);
	hJetMultip[j+HLT_PLOT_OFFSET]->Fill(n_jets_20);
	n_muons_HLT_plot++;
        
      }
    }
    

    
    numCands++;
  }
  
  LogTrace ("HLTMuonVal") << "-----End of top plotter analyze method-----" << endl;
} // Done filling histograms




void HLTTopPlotter::begin() 
{

  TString myLabel, newFolder;
  vector<TH1F*> h;

  LogTrace ("HLTMuonVal") << "Inside begin for top analyzer" << endl;

  
  LogTrace ("HLTMuonVal") << "Calling begin for muon analyzer" << endl;
  HLTMuonMatchAndPlot::begin();

  LogTrace ("HLTMuonVal") << "Continuing with top analyzer begin" << endl;

  if ( dbe_ ) {
    dbe_->cd();
    dbe_->setCurrentFolder("HLT/Muon");

    
    // JMS I think this is trimming all L1 names to
    // to be L1Filtered
    myLabel = theL1CollectionLabel;
    myLabel = myLabel(myLabel.Index("L1"),myLabel.Length());
    myLabel = myLabel(0,myLabel.Index("Filtered")+8);


    // JMS Old way of doing things
    //newFolder = "HLT/Muon/Distributions/" + theTriggerName;
    newFolder = "HLT/Muon/Distributions/" + theTriggerName + "/" + mySelection.customLabel;

    
    
    dbe_->setCurrentFolder( newFolder.Data() );
    
    vector<string> binLabels;
    binLabels.push_back( theL1CollectionLabel.c_str() );
    for ( size_t i = 0; i < theHltCollectionLabels.size(); i++ )
      binLabels.push_back( theHltCollectionLabels[i].c_str() );


    //------- Define labels for plots -------
    
    if (useOldLabels) { 
      myLabel = theL1CollectionLabel;
      myLabel = myLabel(myLabel.Index("L1"),myLabel.Length());
      myLabel = myLabel(0,myLabel.Index("Filtered")+8);
    } else {
      myLabel = "L1Filtered";
    }

    //------ Definte the plots themselves------------------------


    //////////////////////////////////////////////////////////////
    //
    //         ALL + L1 plots 
    //
    //////////////////////////////////////////////////////////////
    
    hDeltaRMaxJetLep.push_back (bookIt("topDeltaRMaxJetLep_All", "delta R between muon and highest pt jet", theDRParameters));
    if (useFullDebugInformation || isL1Path) hDeltaRMaxJetLep.push_back (bookIt("topDeltaRMaxJetLep_" + myLabel, "delta R between muon and highest pt jet", theDRParameters));

   hJetMultip.push_back (bookIt("topJetMultip_All", "Jet multiplicity", theJetMParameters));
    if (useFullDebugInformation || isL1Path) hJetMultip.push_back(bookIt("topJetMultip_" + myLabel, "Jet multiplicity", theJetMParameters));


    //////////////////////////////////////////////////////////////
    //
    //         ALL + L1 plots 
    //
    //////////////////////////////////////////////////////////////
    
    
    // we won't enter this loop if we don't have an hlt label
    // we won't have an hlt label is this is a l1 path
    for ( unsigned int i = 0; i < theHltCollectionLabels.size(); i++ ) {

      if (useOldLabels) {
        myLabel = theHltCollectionLabels[i];
        TString level = ( myLabel.Contains("L2") ) ? "L2" : "L3";
        myLabel = myLabel(myLabel.Index(level),myLabel.Length());
        myLabel = myLabel(0,myLabel.Index("Filtered")+8);
      } else {
        TString tempString = theHltCollectionLabels[i];
        TString level = ( tempString.Contains("L2") ) ? "L2" : "L3";
        myLabel = level + "Filtered";
      } // end if useOldLabels
    

      // Book for L2, L3
      hDeltaRMaxJetLep.push_back (bookIt("topDeltaRMaxJetLep_" + myLabel, "delta R between muon and highest pt jet", theDRParameters)) ;
      hJetMultip.push_back (bookIt("topJetMultip_" + myLabel, "Jet Multiplicity",   theJetMParameters)) ;
      
      
      
    }// end for each collection label

  }// end if dbe_ exists

}// end begin method



///////////////////////////////////////////////////////
//
//  Extra methods
//
///////////////////////////////////////////////////////



// ---------------   Sort a collection of Jets -----------


void HLTTopPlotter::sortJets (CaloJetCollection & theJets) {


  LogTrace ("HLTMuonVal") << "Sorting Jets" << endl;
  
  // bubble sort jets
  
  for ( unsigned int iJet = 0;
        iJet < theJets.size();          
        iJet ++) {

    for ( unsigned int jJet = iJet;
          jJet < theJets.size();
          jJet ++ ){

      if ( theJets[jJet].et() > theJets[iJet].et() ) {
        reco::CaloJet tmpJet = theJets[iJet];
        theJets[iJet] = theJets[jJet];
        theJets[jJet] = tmpJet;        
      }
      
    }// end for each jJet

  }// end for each iJet

  for ( unsigned int iJet = 0;
        iJet != theJets.size();
        iJet ++ ) {

    LogTrace ("HLTMuonVal") << "Jet # " << iJet
                            << "  Et =  " << theJets[iJet].et()
                            << endl;

  }
  
}


