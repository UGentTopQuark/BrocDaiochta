import FWCore.ParameterSet.Config as cms

topTrigOfflineDQM = cms.EDAnalyzer("TopTrigAnalyzer",

    HltProcessName = cms.string("HLT"),
                                   
    createStandAloneHistos = cms.untracked.bool(False),
	histoFileName = cms.untracked.string("MuonTrigHistos_RECO.root"),

   # The muon collection tag must point to a
   # a group of muon-type objects, not just track objects									
   RecoMuonInputTag = cms.InputTag("muons", "", ""),
   BeamSpotInputTag = cms.InputTag("offlineBeamSpot", "", ""),
   HltRawInputTag = cms.InputTag("hltTriggerSummaryRAW", "", ""),
   HltAodInputTag = cms.InputTag("hltTriggerSummaryAOD", "", ""),
   # This is still used
   # to select based on trigger
   TriggerResultLabel = cms.InputTag("TriggerResults","","HLT"),
   
   #### jet selection
   CaloJetInputTag = cms.InputTag("iterativeCone5CaloJets","",""),
   JetIDInputTag = cms.InputTag("ic5JetID","",""),
   
   EtaCut = cms.untracked.double(2.6),
   PtCut  = cms.untracked.double(20.0),
   NJets  = cms.untracked.int32(0),
									
   # Define the cuts for your muon selections
  customCollection = cms.VPSet(


	cms.untracked.PSet(
	  collectionName = cms.untracked.string ("topMuonPt_anyTrig"),
	  trackCollection = cms.untracked.string ("globalTrack"),
	  requiredTriggers = cms.untracked.vstring(""),
          d0cut = cms.untracked.double(99),
          z0cut = cms.untracked.double(99), # 3 meters
          chi2cut = cms.untracked.double(99),
          nHits = cms.untracked.int32(0),
          recoCuts = cms.untracked.string ("pt > 0 && abs(eta) < 5"),
          hltCuts  = cms.untracked.string ("pt > 0 && abs(eta) < 5")
          #d0cut = cms.untracked.double(0.2),
	  #z0cut = cms.untracked.double(25.0), # 3 meters
	  #chi2cut = cms.untracked.double(30.0),
	 # nHits = cms.untracked.int32(20),
	 # recoCuts = cms.untracked.string ("pt > 10 && abs(eta) < 2.1"),
	 # hltCuts  = cms.untracked.string ("pt > 10 && abs(eta) < 2.1")
	),

	

	cms.untracked.PSet(
	  collectionName = cms.untracked.string ("topMuonPt_L1MET20"),
	  trackCollection = cms.untracked.string ("globalTrack"),
	  requiredTriggers = cms.untracked.vstring("HLT_L1MET20"),
          d0cut = cms.untracked.double(99),
          z0cut = cms.untracked.double(99), # 3 meters
          chi2cut = cms.untracked.double(99),
          nHits = cms.untracked.int32(0),
          recoCuts = cms.untracked.string ("pt > 0 && abs(eta) < 5"),
          hltCuts  = cms.untracked.string ("pt > 0 && abs(eta) < 5")
          
	  #d0cut = cms.untracked.double(0.2),
	  #z0cut = cms.untracked.double(25.0),
	  #chi2cut = cms.untracked.double(30.0),
	  #nHits = cms.untracked.int32(20),
	  #recoCuts = cms.untracked.string ("pt > 10 && abs(eta) < 2.1"),
	  #HltCuts  = cms.untracked.string ("pt > 10 && abs(eta) < 2.1")	  
	),

	

	cms.untracked.PSet(
	#  collectionName = cms.untracked.string ("topMuonPt15_QuadJet15U"),
	  collectionName = cms.untracked.string ("topMuonPt_HLT_Jet6U"),
	  trackCollection = cms.untracked.string ("globalTrack"),
	  #requiredTriggers = cms.untracked.vstring("HLT_QuadJet30"),
	  requiredTriggers = cms.untracked.vstring("HLT_L1Jet6U"),
          d0cut = cms.untracked.double(99),
          z0cut = cms.untracked.double(99), # 3 meters
          chi2cut = cms.untracked.double(99),
          nHits = cms.untracked.int32(0),
          recoCuts = cms.untracked.string ("pt > 0 && abs(eta) < 5"),
          hltCuts  = cms.untracked.string ("pt > 0 && abs(eta) < 5")
          
	  #d0cut = cms.untracked.double(0.2),
	  #z0cut = cms.untracked.double(25.0),
	  #chi2cut = cms.untracked.double(30.0),
	  #nHits = cms.untracked.int32(20),
	  #recoCuts = cms.untracked.string ("pt > 10 && abs(eta) < 2.1"),
	  #HltCuts  = cms.untracked.string ("pt > 10 && abs(eta) < 2.1")	  
	),


 	#cms.untracked.PSet(
	#  collectionName = cms.untracked.string ("TopCosmicConfig3"),
	#  trackCollection = cms.untracked.string ("outerTrack"),
	#  requiredTriggers = cms.untracked.vstring(""),
 	#  d0cut = cms.untracked.double(20.0),
 	#  z0cut = cms.untracked.double(50.0), 
	#  requiredTriggers = cms.untracked.vstring(""),
	#  chi2cut = cms.untracked.double(30.0),
	#  nHits = cms.untracked.int32(20),
 	#  recoCuts = cms.untracked.string ("pt > 3 && abs(eta) < 2.1"),
 	#  hltCuts  = cms.untracked.string ("pt > 3 && abs(eta) < 2.1")
 	#),

	
	
									  
    ),

    # Set the ranges and numbers of bins for histograms
	# max pt is not very useful


    #EtaParameters      = cms.vdouble(50, -3.5,3.5),

    EtaParameters      = cms.untracked.vdouble(40, -2.1,2.1),
    PhiParameters      = cms.untracked.vdouble(40, -3.15,3.15),
    ResParameters      = cms.untracked.vdouble(25, -0.15, 0.15),
    DrParameters       = cms.untracked.vdouble(25, 0.0, 0.05),
	
    JetMParameters     = cms.untracked.vdouble(11, -0.5, 10.5),			

    # Use Pt Parameters to set bin edges

    PtParameters       = cms.untracked.vdouble(0.0,  2.0,  4.0, 
									 6.0, 8.0, 10.0, 
									 12.0,  14.0,  16.0, 
									 18.0,  20.0,
									 25.0, 30.0, 35.0, 40.0,
									 50.0, 60.0, 70, 80, 90, 100.0,
									 125.0, 150.0, 175.0, 200.0,
									 400.0),

    Z0Parameters       = cms.untracked.vdouble(25, -25, 25),
    D0Parameters       = cms.untracked.vdouble(25, -1, 1),									




	# valid match types are dr and cosmic
	# future update: make sure default is
	# delta r matching
	matchType = cms.untracked.string("dr"),

    # If you have cosmic matching
	# you will ignore the delta R cuts								
								   									
	L1DrCut   = cms.untracked.double(0.4),
	L2DrCut   = cms.untracked.double(0.25),
	L3DrCut   = cms.untracked.double(0.025),								
									

    DQMStore = cms.untracked.bool(True),

	# still used by overlap analyzer
    # not included in meaningful output									
    CrossSection = cms.double(0.97),
	# ddd 								
    #NSigmas90 = cms.untracked.vdouble(3.0, 3.0, 3.0, 3.0),


	# list of triggers
    # any triggers not in the hlt configuraiton
    # will be ignored
									
	TriggerNames = cms.vstring(
        #"HLT_IsoMu3",
        "HLT_L1MuOpen",
        "HLT_L2Mu3",   
        "HLT_L2Mu9",        
    ),


)
