import FWCore.ParameterSet.Config as cms

maxEvents = -1 
outputDirectory = '/user/walsh/CMSSW_3_5_4/src/trigger_output'
outputFileNamePattern = '/DQMTest/Top/Histos'
inputFile = ['dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_0_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_1_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_2_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_3_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_4_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_5_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_6_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_7_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_8_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_9_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_10_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_11_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_12_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_13_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_14_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_15_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_16_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_17_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_18_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_19_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_20_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_21_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_22_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_23_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_24_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_25_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_26_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_27_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_28_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_29_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_30_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_31_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_32_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_33_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_34_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_35_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_36_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_37_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_38_topTest_900MC_pt5.root',
             'dcap://pnfs/iihe/cms/store/user/walsh/trigger900MC/MinBiasjob_39_topTest_900MC_pt5.root']

#################################################

process = cms.Process("EDMtoMEConvert")



process.load('Configuration.StandardSequences.EDMtoMEAtRunEnd_cff')
process.load("DQMServices.Components.DQMEnvironment_cfi")
process.load("DQMOffline.Trigger.MuonPostProcessor_cfi")
process.load("DQMServices.Components.DQMStoreStats_cfi")




process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(maxEvents)
)

process.MessageLogger = cms.Service("MessageLogger",
									destinations = cms.untracked.vstring('cout'),
									#categories = cms.untracked.vstring('DQMGenericClient'),
									categories = cms.untracked.vstring('HLTMuonVal'),
									debugModules = cms.untracked.vstring('*'),
									threshold = cms.untracked.string('WARNING'),
									HLTMuonVal = cms.untracked.PSet(
	                                     #threshold = cms.untracked.string('DEBUG'),
	                                     limit = cms.untracked.int32(100000)
										 )
									)

process.source = cms.Source("PoolSource",
							fileNames = cms.untracked.vstring(inputFile),
)


process.dqmSaver.workflow = outputFileNamePattern
process.dqmSaver.dirName = outputDirectory




process.path = cms.Path(process.EDMtoME*process.hLTMuonPostVal*process.dqmStoreStats)


process.endpath = cms.EndPath(process.dqmSaver)



