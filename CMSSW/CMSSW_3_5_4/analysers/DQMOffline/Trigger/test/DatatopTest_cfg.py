import FWCore.ParameterSet.Config as cms
#########################################################

# edit the maximum number of events
# -1 for all events

maxEvents = -1

# output file name

#outputFileName = "/afs/cern.ch/user/w/walsh/CMSSW_3_5_4/src/DQMOffline/Trigger/DatatopMC_test.root"
outputFileName = "/user/walsh/CMSSW_3_5_6/src/trigger_output/topMC_test.root"


# edit the files that will be used
# if the files are local,
# make sure to add file: to the beginning of the name

targetFileNames = [
 'file:////user/walsh/CMSSW_3_5_6/src/temp/Data_7TeV_recoskim_1.root'
   
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_1.root', 
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_2.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_3.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_4.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_5.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_6.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_7.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_8.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_9.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_10.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_11.root', 
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_12.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_13.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_14.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_15.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_16.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_17.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_18.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_19.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_20.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_21.root', 
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_22.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_23.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_24.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_25.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_26.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_27.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_28.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_29.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_30.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_31.root', 
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_32.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_33.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_34.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_35.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_36.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_37.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_38.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_39.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_40.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_41.root', 
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_42.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_43.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_44.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_45.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_46.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_47.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_48.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_49.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_50.root',
## 'rfio:/castor/cern.ch/user/t/tdaniels/2010_TPG_SKIMS/ExpressPhysics/run132569/muonPt05Skim_51.root'
]





###################################################################
process = cms.Process("HLTMuonOfflineAnalysis")

# Data cuts to identify good collisions
process.load('L1TriggerConfig.L1GtConfigProducers.L1GtTriggerMaskTechTrigConfig_cff')
process.load('HLTrigger/HLTfilters/hltLevel1GTSeed_cfi')
process.hltLevel1GTSeed.L1TechTriggerSeeding = cms.bool(True)
process.hltLevel1GTSeed.L1SeedsLogicalExpression = cms.string('0 AND (40 OR 41) AND NOT (36 OR 37 OR 38 OR 39) AND NOT ((42 AND NOT 43) OR (43 AND NOT 42))')

process.primaryVertexFilter = cms.EDFilter("VertexSelector",
   src = cms.InputTag("offlinePrimaryVertices"),
   cut = cms.string("!isFake && ndof > 4 && abs(z) <= 15 && position.Rho <= 2"), # tracksSize() > 3 for the older cut
   filter = cms.bool(True),   # otherwise it won't filter the events, just produce an empty vertex collection.
)


process.noscraping = cms.EDFilter("FilterOutScraping",
applyfilter = cms.untracked.bool(True),
debugOn = cms.untracked.bool(False),
numtrack = cms.untracked.uint32(10),
thresh = cms.untracked.double(0.25)
)

process.goodvertex=cms.Path(process.primaryVertexFilter+process.noscraping)



# End good coll cuts

process.load("DQMOffline.Trigger.TopTrigRateAnalyzer_cfi")
process.load("DQMServices.Components.MEtoEDMConverter_cfi")
process.load("DQMServices.Components.DQMStoreStats_cfi")






process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(maxEvents)
)


process.source = cms.Source("PoolSource",
    skipEvents = cms.untracked.uint32(0),							
	fileNames = cms.untracked.vstring(targetFileNames)
)


# take this out and try to run
process.DQMStore = cms.Service("DQMStore")

process.MessageLogger = cms.Service("MessageLogger",
    debugModules   = cms.untracked.vstring('*'),
    cout           = cms.untracked.PSet(
	# Be careful - this can print a lot of debug info
    #        threshold = cms.untracked.string('DEBUG')
	threshold = cms.untracked.string('INFO')
	#threshold = cms.untracked.string('WARNING')
    ),
    categories     = cms.untracked.vstring('HLTMuonVal'),
    destinations   = cms.untracked.vstring('cout')
)

process.out = cms.OutputModule("PoolOutputModule",
	 outputCommands = cms.untracked.vstring('drop *', 'keep *_MEtoEDMConverter_*_*'),
	 # Disable fast cloning to resolve 34X issue
	 fastCloning = cms.untracked.bool(False),						   
	 fileName = cms.untracked.string(outputFileName)

)

process.analyzerpath = cms.Path(
    process.noscraping*
    process.hltLevel1GTSeed*
	process.topTrigOfflineDQM*
    process.MEtoEDMConverter*
	process.dqmStoreStats
)

process.outpath = cms.EndPath(process.out)
