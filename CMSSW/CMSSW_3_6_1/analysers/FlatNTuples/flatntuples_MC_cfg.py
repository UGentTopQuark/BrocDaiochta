import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
'file:////user/walsh/CMSSW_3_6_1_patch3/src/TopComROOT/TopTrigSkims/MC/220610//MinBias_TuneP0_7TeV-pythia6job_0_TopTrigSkimPt5.root',
'file:////user/walsh/CMSSW_3_6_1_patch3/src/TopComROOT/TopTrigSkims/MC/220610//MinBias_TuneP0_7TeV-pythia6job_1_TopTrigSkimPt5.root',
'file:////user/walsh/CMSSW_3_6_1_patch3/src/TopComROOT/TopTrigSkims/MC/220610//MinBias_TuneP0_7TeV-pythia6job_2_TopTrigSkimPt5.root',
'file:////user/walsh/CMSSW_3_6_1_patch3/src/TopComROOT/TopTrigSkims/MC/220610//MinBias_TuneP0_7TeV-pythia6job_3_TopTrigSkimPt5.root',
'file:////user/walsh/CMSSW_3_6_1_patch3/src/TopComROOT/TopTrigSkims/MC/220610//MinBias_TuneP0_7TeV-pythia6job_4_TopTrigSkimPt5.root',
'file:////user/walsh/CMSSW_3_6_1_patch3/src/TopComROOT/TopTrigSkims/MC/220610//MinBias_TuneP0_7TeV-pythia6job_5_TopTrigSkimPt5.root',
'file:////user/walsh/CMSSW_3_6_1_patch3/src/TopComROOT/TopTrigSkims/MC/220610//MinBias_TuneP0_7TeV-pythia6job_6_TopTrigSkimPt5.root'                                                                                                                    

    )
)
	
process.pdfWeights = cms.EDProducer("PdfWeightProducer",
      # Fix POWHEG if buggy (this PDF set will also appear on output,
      # so only two more PDF sets can be added in PdfSetNames if not "")
      #FixPOWHEG = cms.untracked.string("cteq66.LHgrid"),
      #GenTag = cms.untracked.InputTag("genParticles"),
      PdfInfoTag = cms.untracked.InputTag("generator"),
      PdfSetNames = cms.untracked.vstring(
              "cteq66.LHgrid"
      #      , "MRST2006nnlo.LHgrid"
      #      , "MRST2007lomod.LHgrid"
      )
)

process.produceNTuples = cms.EDAnalyzer('FlatNTuples',
	electronTag = cms.untracked.InputTag("selectedPatElectrons"),
	tauTag      = cms.untracked.InputTag("selectedPatTaus"),
	muonTag     = cms.untracked.InputTag("selectedPatMuons"),
	jetTag      = cms.untracked.InputTag("selectedPatJets"),
	photonTag   = cms.untracked.InputTag("selectedPatPhotons"),
	metTag      = cms.untracked.InputTag("patMETs"),
	primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD','','HLT'),
	HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT'),
	MuonIDs	= cms.vstring("AllGlobalMuons","AllStandAloneMuons", "AllTrackerMuons", "AllArbitrated", "GlobalMuonPromptTight", "TrackerMuonArbitrated"),
	ElectronIDs	= cms.vstring("eidRobustTight", "eidRobustLoose", "eidTight", "eidLoose"),
	TriggerList      = cms.vstring("HLT_(L[[:digit:]])?Ele(?:(?!(No|Anti)BPTX).)*","HLT_(L[[:digit:]])?Mu(?:(?!(No|Anti)BPTX).)*","HLT_(L[[:digit:]]+|Quad)?Jet(?:(?!NoBPTX).)*", "HLT_(L[[:digit:]])?MET(?:(?!(No|Anti)BPTX).)*", "HLT_(L[[:digit:]])?Photon(?:(?!(No|Anti)BPTX).)*","HLT_HT100U","HLT_SingleLooseIsoTau20","HLT_DoubleLooseIsoTau15","HLT_DoubleJet15U_ForwardBackward","HLT_BTagMu_Jet10U","HLT_BTagIP_Jet50U","HLT_StoppedHSCP_8E29","HLT_DiJetAve15U_8E29","HLT_FwdJet20U","HLT_DiJetAve30U_8E29"),
	VetoObjectTriggers = cms.vstring("HLT_.*Jet.*", "HLT_.*MET.*","HLT_StoppedHSCP_8E29","HLT_HT100U","HLT_.*Tau.*"),
	BTagAlgorithms	= cms.vstring("trackCountingHighEffBJetTags",
      				      "jetProbabilityBJetTags"),
	MCMatching	= cms.bool(True),
	FillTriggerObjects	= cms.bool(True),
	WriteMET	= cms.bool(False),
	WriteTriggerPrescales	= cms.bool(False),
	WritePDFEventWeights = cms.bool(False),
	PDFWeights = cms.VInputTag(""),
	Writed0wrtPV	= cms.bool(False),		# write d0 wrt PV
	WriteGenParticles = cms.bool(False),		# write selected MC particles
	SelectedGenParticles = cms.vint32(11,13),	# pdgIds of MC particles to write
	SelectedGenParticlesMinPt = cms.double(1),	# min pt of MC particles to write
	SelectedGenParticlesMaxEta = cms.double(3.0),	# max eta of MC particles to write
	outfile		= cms.string("tuneD0_160710.root")
)

#process.p1 = cms.Path(process.pdfWeights * process.produceNTuples)
process.p1 = cms.Path(process.produceNTuples)

process.schedule = cms.Schedule(  process.p1 )
