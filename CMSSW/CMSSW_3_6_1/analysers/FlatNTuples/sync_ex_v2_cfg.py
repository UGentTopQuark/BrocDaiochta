import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
      #'dcap:///pnfs/iihe/cms/store/user/bklein/preselection_7TeV_v1/ttbar/TTbarJets-madgraphjob_275_PATLayer1_Output.fromAOD_full.root'
      #'file:////user/bklein/trigger_cmssw/CMSSW_3_6_1_patch4/src/analysers/TtPreselection/syncex_v2_test_v2.root'
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_0_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_1_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_2_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_3_TopTrigSkim.root',
      'dcap:///pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140175to140334and137437to139558/JetMETTaujob_0_TopTrigSkim.root',
      'dcap:///pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140175to140334and137437to139558/JetMETTaujob_1_TopTrigSkim.root',
      'dcap:///pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140175to140334and137437to139558/JetMETTaujob_99_TopTrigSkim.root',
      'dcap:///pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140175to140334and137437to139558/JetMETTaujob_98_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_5_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_6_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_7_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_8_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_9_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_10_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_11_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_12_TopTrigSkim.root',
    #  'dcap:////pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/JetMETTau_Data_PromptReco_v4_140140to140334/JetMETTaujob_13_TopTrigSkim.root'
    )
)
	
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.pdfWeights = cms.EDProducer("PdfWeightProducer",
      # Fix POWHEG if buggy (this PDF set will also appear on output,
      # so only two more PDF sets can be added in PdfSetNames if not "")
      #FixPOWHEG = cms.untracked.string("cteq66.LHgrid"),
      #GenTag = cms.untracked.InputTag("genParticles"),
      PdfInfoTag = cms.untracked.InputTag("generator"),
      PdfSetNames = cms.untracked.vstring(
              "cteq66.LHgrid"
      #      , "MRST2006nnlo.LHgrid"
      #      , "MRST2007lomod.LHgrid"
      )
)

process.produceNTuples = cms.EDAnalyzer('FlatNTuples',
	electronTag = cms.untracked.InputTag("selectedPatElectrons"),
	tauTag      = cms.untracked.InputTag("selectedPatTaus"),
	muonTag     = cms.untracked.InputTag("selectedPatMuons"),
	jetTag      = cms.untracked.InputTag("selectedPatJets"),
	photonTag   = cms.untracked.InputTag("selectedPatPhotons"),
	metTag      = cms.untracked.InputTag("patMETs"),
	primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD','','HLT'),
	HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT'),
	MuonIDs	= cms.vstring("AllGlobalMuons","AllStandAloneMuons", "AllTrackerMuons", "AllArbitrated", "GlobalMuonPromptTight", "TrackerMuonArbitrated"),
	ElectronIDs	= cms.vstring("eidRobustTight", "eidRobustLoose", "eidTight", "eidLoose", "simpleEleId70cIso", "simpleEleId70relIso"),
	TriggerList      = cms.vstring("HLT_(L[[:digit:]])?Ele(?:(?!(No|Anti)BPTX).)*","HLT_(L[[:digit:]])?Mu(?:(?!(Track|Tk|Vertex|NoBPTX|AntiBPTX|Jpsi|Single)).)*","HLT_(Quad)?Jet(?:(?!NoBPTX).)*", "HLT_MET(?:(?!(No|Anti)BPTX).)*", "HLT_(L[[:digit:]])?Photon(?:(?!(No|Anti)BPTX).)*"),
	VetoObjectTriggers = cms.vstring("HLT_.*Jet.*", "HLT_.*MET.*"),
	BTagAlgorithms	= cms.vstring("trackCountingHighEffBJetTags",
      				      "jetProbabilityBJetTags"),
	MCMatching	= cms.bool(False),
	FillTriggerObjects	= cms.bool(True),
	WriteMET	= cms.bool(True),
	WriteTriggerPrescales	= cms.bool(False),
	WritePDFEventWeights = cms.bool(False),
	PDFWeights = cms.VInputTag("pdfWeights:cteq66"),
	Writed0wrtPV	= cms.bool(False),		# write d0 wrt PV
	WriteGenParticles = cms.bool(False),		# write selected MC particles
	SelectedGenParticles = cms.vint32(11,13),	# pdgIds of MC particles to write
	SelectedGenParticlesMinPt = cms.double(1),	# min pt of MC particles to write
	SelectedGenParticlesMaxEta = cms.double(3.0),	# max eta of MC particles to write
	outfile		= cms.string("NTuple.root")
)

process.p1 = cms.Path(
#	process.pdfWeights *
	process.produceNTuples
	)

process.schedule = cms.Schedule(  process.p1 )
