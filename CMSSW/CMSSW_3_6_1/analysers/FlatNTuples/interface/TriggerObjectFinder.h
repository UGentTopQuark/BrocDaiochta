#ifndef BEAGTRIGGEROBJECTFINDER_H
#define BEAGTRIGGEROBJECTFINDER_H

#include "TFile.h"
#include <vector>
#include <string>
#include "DataFormats/Common/interface/View.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/Common/interface/TriggerResults.h"

namespace beag{
	class TriggerObjectFinder{
		public:
			TriggerObjectFinder(TFile *outfile);
			~TriggerObjectFinder();
			void find_trigger_objects(edm::Handle<trigger::TriggerEvent> aodTriggerEvent, edm::Handle<edm::TriggerResults> HLTR);
			void set_triggers_to_write(std::vector<std::string> *triggers);
			void set_triggers_to_veto(std::vector<std::string> *veto_triggers);
			void set_hlt_config_provider(HLTConfigProvider *hltConfig);
			void set_trigger_menu(std::string trigger_menu);
			bool check_objects_found();
			std::map<int,std::vector<unsigned long long> > get_objects_to_write();
			std::map<int,int> get_keys_found();

		private:

			void get_trigger_objects(int module,int nbeag_trig);
                        std::vector<std::string> *triggers_to_write;
			std::map<std::string,bool> triggers_to_veto;

			std::map<int,std::vector<unsigned long long> >  objects_to_write;
			std::map<int,int> keys_found;
			

			TFile *outfile;

			HLTConfigProvider *hltConfig;
			edm::Handle<trigger::TriggerEvent> aodTriggerEvent;

			std::string trigger_menu;
			bool objects_found;

                        static const bool verbose = false;
	};
}

#endif
