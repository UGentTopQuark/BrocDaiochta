#ifndef BEAG_EVENTINFORMATIONPRODUCER_H
#define BEAG_EVENTINFORMATIONPRODUCER_H

#include "TFile.h"
#include "TTree.h"
#include <string>
#include <vector>

#include "BeagObjects/EventInformation.h"

namespace beag{
	class EventInformationProducer{
		public:
			EventInformationProducer(TTree *tree, TFile *outfile, std::string ident);
			~EventInformationProducer();

			void fill_event_information(double run, double lumi_block, double event_number, bool trigger_changed, double cms_energy);
		private:

			TTree *tree;
			TFile *outfile;

			std::vector<beag::EventInformation> *event_info;

			static const bool verbose = false;
	};
}

#endif
