#ifndef BEAG_COLLECTION_H
#define BEAG_COLLECTION_H

#include <DataFormats/Common/interface/Ref.h>
#include "DataFormats/Common/interface/View.h"
#include "TTree.h"
#include "TFile.h"
#include "Math/VectorUtil.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/IPTools/interface/IPTools.h"
#include "DataFormats/GeometryCommonDetAlgo/interface/Measurement1D.h"
#include "TriggerObjectFinder.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "BeagObjects/Jet.h"
#include "BeagObjects/Muon.h"
#include "BeagObjects/Electron.h"
#include "BeagObjects/MET.h"
#include "PtSorter.h"

namespace beag{
	template<class CMSSWObject, class BeagObject>
	class Collection{
		public:
			Collection(TTree *tree, TFile *outfile, std::string ident);
			~Collection();
			void fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle);
			void set_selection_list(std::vector<std::string> *selection_list);
			void set_tag_list(std::vector<std::string> *tag_list);
			void set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle);
			void set_trackbuilder_handle(edm::ESHandle<TransientTrackBuilder> track_builder, edm::Handle<std::vector<reco::Vertex> > pvertexHandle);
			void set_mc_matching(bool match_mc);
			void set_trigger_object_finder(TriggerObjectFinder *object_finder);
			void set_aod_trigger_event(edm::Handle<trigger::TriggerEvent> aodTriggerEvent);
			std::vector<BeagObject>* get_beag_objects();
		private:
			void fill_common_vars(typename edm::View<CMSSWObject>::const_iterator handle, BeagObject &beag_obj);
			void fill_special_vars(typename edm::View<CMSSWObject>::const_iterator handle, BeagObject &beag_obj);
			void match_trigger(typename edm::View<CMSSWObject>::const_iterator handle, BeagObject &beag_obj);
			void write_gen_info(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj);

			const char* get_type_string();
			std::vector<BeagObject> *beag_objects;
			std::vector<std::string> *selected_items;	// list of selected variables written to output
									// eg. list of b-tag algorithms
			std::vector<std::string> *selected_tags;		// additional tags, eg LeptonID

			beag::TriggerObjectFinder *trigger_obj_find;
			edm::Handle<reco::BeamSpot> beamSpotHandle;
			edm::Handle<std::vector<reco::Vertex> > pvertexHandle;
			edm::ESHandle<TransientTrackBuilder> track_builder;
			edm::Handle<trigger::TriggerEvent> aodTriggerEvent;
			PtSorter<BeagObject> *pt_sorter;
			TTree *tree;
			TFile *outfile;
			bool mc_matching;
	};
}

#endif
