#include "../interface/TTbarGenEventProducer.h"

beag::TTbarGenEventProducer::TTbarGenEventProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	gen_match = new GenMatch();

	gen_event = new std::vector<beag::TTbarGenEvent>();
	gen_event->push_back(beag::TTbarGenEvent());
	tree->Bronch(ident.c_str(),"std::vector<beag::TTbarGenEvent>",&gen_event);
}

beag::TTbarGenEventProducer::~TTbarGenEventProducer()
{
	if(gen_event){
		delete gen_event;
		gen_event = NULL;
	}

	if(gen_match){
		delete gen_match;
		gen_match = NULL;
	}
}

beag::TTbarGenEvent* beag::TTbarGenEventProducer::get_gen_event()
{
	return &(*gen_event->begin());
}

void beag::TTbarGenEventProducer::fill_mc_information(edm::Handle<reco::GenParticleCollection> part_collection)
{
	gen_match->fill_events_from_collection(part_collection);

	outfile->cd();

	if(gen_match->is_ok() && gen_match->get_decay_mode() == 1){
		if(gen_match->t_decay_is_leptonic()){
			fill_particle_p4(gen_match->get_particle("tbar"), gen_event->begin()->hadT);
			fill_particle_p4(gen_match->get_particle("bbar"), gen_event->begin()->hadB);
			fill_particle_p4(gen_match->get_particle("Wminus"), gen_event->begin()->hadW);
			fill_particle_p4(gen_match->get_particle("Wminus_dp1"), gen_event->begin()->q);
			fill_particle_p4(gen_match->get_particle("Wminus_dp2"), gen_event->begin()->qbar);

			fill_particle_p4(gen_match->get_particle("t"), gen_event->begin()->lepT);
			fill_particle_p4(gen_match->get_particle("b"), gen_event->begin()->lepB);
			fill_particle_p4(gen_match->get_particle("Wplus"), gen_event->begin()->lepW);
			fill_particle_p4(gen_match->get_particle("Wplus_dp1"), gen_event->begin()->lep);
			fill_particle_p4(gen_match->get_particle("Wplus_dp2"), gen_event->begin()->neu);

		}else if(gen_match->tbar_decay_is_leptonic()){
			fill_particle_p4(gen_match->get_particle("t"), gen_event->begin()->hadT);
			fill_particle_p4(gen_match->get_particle("b"), gen_event->begin()->hadB);
			fill_particle_p4(gen_match->get_particle("Wplus"), gen_event->begin()->hadW);
			fill_particle_p4(gen_match->get_particle("Wplus_dp1"), gen_event->begin()->q);
			fill_particle_p4(gen_match->get_particle("Wplus_dp2"), gen_event->begin()->qbar);

			fill_particle_p4(gen_match->get_particle("tbar"), gen_event->begin()->lepT);
			fill_particle_p4(gen_match->get_particle("bbar"), gen_event->begin()->lepB);
			fill_particle_p4(gen_match->get_particle("Wminus"), gen_event->begin()->lepW);
			fill_particle_p4(gen_match->get_particle("Wminus_dp1"), gen_event->begin()->lep);
			fill_particle_p4(gen_match->get_particle("Wminus_dp2"), gen_event->begin()->neu);
		}else
			std::cerr << "WARNING: beag::TTbarGenEventProducer::fill_mc_information(): invalid ttbar decay" << std::endl;

		if(fabs(gen_event->begin()->lep.mc_id) == 11)
			gen_event->begin()->decay_channel = 1;
		else if(fabs(gen_event->begin()->lep.mc_id) == 13)
			gen_event->begin()->decay_channel = 2;
		else if(fabs(gen_event->begin()->lep.mc_id) == 15){	// ignore semi-leptonic tau case
			gen_event->begin()->decay_channel = 0;
		}else
			std::cerr << "WARNING: beag::TTbarGenEventProducer::fill_mc_information(): no valid lepton assignment" << std::endl;
	}else{
		gen_event->begin()->decay_channel = 0;
	}
}

void beag::TTbarGenEventProducer::fill_particle_p4(reco::GenParticle *reco_particle, beag::Particle &beag_particle)
{
	beag_particle.eta = reco_particle->polarP4().eta();
	beag_particle.phi = reco_particle->polarP4().phi();
	beag_particle.mass = reco_particle->polarP4().mass();
	beag_particle.pt = reco_particle->polarP4().pt();

	beag_particle.mc_id = reco_particle->pdgId();
}
