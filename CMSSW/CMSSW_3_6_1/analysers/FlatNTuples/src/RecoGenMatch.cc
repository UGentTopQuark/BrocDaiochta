#include "../interface/RecoGenMatch.h"

template void beag::RecoGenMatch::match_leptons_to_particles<beag::Muon>(std::vector<beag::Muon> *leptons);
template void beag::RecoGenMatch::match_leptons_to_particles<beag::Electron>(std::vector<beag::Electron> *leptons);

beag::RecoGenMatch::RecoGenMatch()
{
	gen_event = NULL;
}

beag::RecoGenMatch::~RecoGenMatch()
{
}

template <class beagLepton>
void beag::RecoGenMatch::match_leptons_to_particles(typename std::vector<beagLepton> *leptons)
{
	if(gen_event->decay_channel < 1)	// if not ttbar semi-lept mu or electron
		return;

	for(typename std::vector<beagLepton>::iterator lepton = leptons->begin();
		lepton != leptons->end();
		++lepton){
		if(lepton->mc_eta == gen_event->lep.eta &&
		   lepton->mc_phi == gen_event->lep.phi &&
		   lepton->mc_mass == gen_event->lep.mass &&
		   lepton->mc_pt == gen_event->lep.pt){
			gen_event->reco_lep.eta = lepton->eta;
			gen_event->reco_lep.phi = lepton->phi;
			gen_event->reco_lep.mass = lepton->mass;
			gen_event->reco_lep.pt = lepton->pt;

			gen_event->reco_lep.ecal_iso = lepton->ecal_iso;
			gen_event->reco_lep.hcal_iso = lepton->hcal_iso;
			gen_event->reco_lep.track_iso = lepton->track_iso;

			gen_event->reco_lep.hcal_vcone = lepton->hcal_vcone;
			gen_event->reco_lep.ecal_vcone = lepton->ecal_vcone;

			gen_event->reco_lep.d0 = lepton->d0;
			gen_event->reco_lep.d0_sigma = lepton->d0_sigma;

			gen_event->reco_lep.charge = lepton->charge;

			gen_event->reco_lep.lepton_id = lepton->lepton_id;
			gen_event->reco_lep.track_available = lepton->track_available;


			gen_event->lep_matched = true;
			lepton->from_ttbar_decay = true;
		}
	}
}

void beag::RecoGenMatch::set_gen_event(TTbarGenEvent *gen_event)
{
	this->gen_event = gen_event;
}

void beag::RecoGenMatch::match_jets_to_partons(std::vector<beag::Jet> *jets)
{
	if(gen_event->decay_channel < 1)	// if not ttbar semi-lept mu or electron
		return;

	for(std::vector<beag::Jet>::iterator jet = jets->begin();
		jet != jets->end();
		++jet){
		if(jet_matches_gen_event_quark(*jet, gen_event->q_jet, gen_event->q)){
			gen_event->q_matched = true;
			jet->ttbar_decay_product = 1;
			jet->from_ttbar_decay = true;
		}else if(jet_matches_gen_event_quark(*jet, gen_event->qbar_jet, gen_event->qbar)){
			gen_event->qbar_matched = true;
			jet->ttbar_decay_product = 2;
			jet->from_ttbar_decay = true;
		}else if(jet_matches_gen_event_quark(*jet, gen_event->hadB_jet, gen_event->hadB)){
			gen_event->hadB_matched = true;
			jet->ttbar_decay_product = 3;
			jet->from_ttbar_decay = true;
		}else if(jet_matches_gen_event_quark(*jet, gen_event->lepB_jet, gen_event->lepB)){
			gen_event->lepB_matched = true;
			jet->ttbar_decay_product = 4;
			jet->from_ttbar_decay = true;
		}else{
			jet->ttbar_decay_product = 0;
			jet->from_ttbar_decay = false;
		}
	}
}

bool beag::RecoGenMatch::jet_matches_gen_event_quark(beag::Jet &jet, beag::Jet &gen_event_jet, beag::Particle &gen_event_quark)
{
	bool matched = false;

	if(jet.mc_eta == gen_event_quark.eta &&
	   jet.mc_phi == gen_event_quark.phi &&
	   jet.mc_mass == gen_event_quark.mass &&
	   jet.mc_pt == gen_event_quark.pt){
		gen_event_jet.eta = jet.eta;
		gen_event_jet.phi = jet.phi;
		gen_event_jet.mass = jet.mass;
		gen_event_jet.pt = jet.pt;

		matched = true;
		jet.from_ttbar_decay = true;
	}

	return	matched;
}
