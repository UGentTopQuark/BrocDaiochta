#!/bin/bash

for i in Jet Electron Muon MET Trigger TTbarGenEvent Particle Lepton EventInformation PrimaryVertex TriggerObject; do
	rootcint -f ${i}Dict.cc -c ../interface/BeagObjects/${i}.h ../interface/BeagObjects/Headers.h ../interface/BeagObjects/${i}LinkDef.h
done
rootcint -f VVectorDoubleDict.cc -c ../interface/BeagObjects/Jet.h ../interface/BeagObjects/Headers.h ../interface/BeagObjects/VVectorDoubleLinkDef.h
