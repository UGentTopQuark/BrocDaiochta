#include "../interface/TriggerObjectProducer.h"

beag::TriggerObjectProducer::TriggerObjectProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	trigger_objects = new std::vector<beag::TriggerObject>();
	trigger_obj_find = NULL;
	tree->Bronch(ident.c_str(),"std::vector<beag::TriggerObject>",&trigger_objects);

}

beag::TriggerObjectProducer::~TriggerObjectProducer()
{
	if(trigger_objects){
		delete trigger_objects;
		trigger_objects = NULL;
	}
}

void beag::TriggerObjectProducer::set_trigger_object_finder(TriggerObjectFinder *object_finder)
{
	trigger_obj_find = object_finder;
}


void beag::TriggerObjectProducer::fill_trigger_objects(edm::Handle<trigger::TriggerEvent> aodTriggerEvent)
{
	this->aodTriggerEvent = aodTriggerEvent;
	trigger_objects->clear();

	if(trigger_obj_find == NULL){
		std::cerr << " ERROR: TrigObjProd: TriggerObjectFinder not set. Exiting." << std::endl;
		return;
	}
	
	if(!trigger_obj_find->check_objects_found()){
		std::cerr << "ERROR: TrigObjProd: TriggerObjectFinder find trigger objects not run. Exiting." << std::endl;
		return;
	}		

	std::map<int,std::vector<unsigned long long> > objects_to_write = trigger_obj_find->get_objects_to_write();

	const trigger::TriggerObjectCollection objects = aodTriggerEvent->getObjects();
	
	//Loop over objects to write and fill to beag::trigger_objects
	for(std::map<int,std::vector<unsigned long long> >::iterator nobj = objects_to_write.begin();nobj != objects_to_write.end();nobj++){
		trigger::TriggerObject foundObject = objects[(*nobj).first];

		if (verbose) std::cout << " Filling trigger objects: Key, " << (*nobj).first << ", hlt trig_results: " << ((*nobj).second)[0] << ", l1seed trig_results: " << ((*nobj).second)[1]  << ", pt: " << foundObject.pt() <<  std::endl;
 
		beag::TriggerObject tmp;
		tmp.eta = foundObject.eta();
		tmp.phi = foundObject.phi();
		tmp.mass = foundObject.mass();
		tmp.pt = foundObject.pt();
		tmp.triggered = ((*nobj).second)[0];
		tmp.l1triggered = ((*nobj).second)[1];
			
		trigger_objects->push_back(tmp);
	}
			
}

