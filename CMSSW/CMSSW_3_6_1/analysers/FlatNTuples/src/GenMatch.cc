#include "../interface/GenMatch.h"

int const GenMatch::top_id = 6;

GenMatch::GenMatch()
{
	// so far we haven't filled the particles
	found_t = false;
	found_tbar = false;
	t_leptonic = false;
	tbar_leptonic = false;

	decay_mode=0;


	t = NULL;
	tbar = NULL;
	b = NULL;	
	bbar = NULL;	
	Wplus = NULL;	
	Wminus = NULL;	
	q1 = NULL;	
	qbar1 = NULL;	
	q2 = NULL;	
	qbar2 = NULL;	
	lepton1 = NULL;	
	neutrino1 = NULL;	
	lepton2 = NULL;	
	neutrino2 = NULL;	
}

GenMatch::~GenMatch()
{
}

void GenMatch::reset_particles()
{
	if(t != NULL){
		delete t;
		t = NULL;
	}
	if(tbar != NULL){
		delete tbar;
		tbar = NULL;
	}
	if(b != NULL){
		delete b;
		b = NULL;
	}
	if(bbar != NULL){
		delete bbar;
		bbar = NULL;
	}
	if(q1 != NULL){
		delete q1;
		q1 = NULL;
	}
	if(qbar1 != NULL){
		delete qbar1;
		qbar1 = NULL;
	}
	if(q2 != NULL){
		delete q2;
		q2 = NULL;
	}
	if(qbar2 != NULL){
		delete qbar2;
		qbar2 = NULL;
	}
	if(lepton1 != NULL){
		delete lepton1;
		lepton1 = NULL;
	}
	if(neutrino1 != NULL){
		delete neutrino1;
		neutrino1 = NULL;
	}
	if(lepton2 != NULL){
		delete lepton2;
		lepton2 = NULL;
	}
	if(neutrino2 != NULL){
		delete neutrino2;
		neutrino2 = NULL;
	}
}

void GenMatch::fill_events_from_collection(edm::Handle<reco::GenParticleCollection> part_collection)
{
	reset_particles();

	using namespace edm;
	using namespace reco;
	found_t = false;
	found_tbar = false;
	t_leptonic = false;
	tbar_leptonic = false;
	decay_mode = 0;
	//	if(verbose)
	//std::cout << "entering fill_events_from_collection..." << std::endl;

	// loop over all particles in the event
	for(unsigned int nopart = 0; nopart < part_collection->size(); ++nopart) {
		// assign current particle
		const GenParticle &current_particle = (*part_collection)[nopart];

		//	if(verbose && nopart == 0)
		//std::cout << "particle collection size: " << part_collection->size() << std::endl;

		if(current_particle.pdgId() == top_id) // if the current particle is a top
		{
			bool found_b=false, found_W=false;

			// read out number of decay products of top
			int no_t_decayprods = current_particle.numberOfDaughters();

			// loop over all decay products of the top
			for(int t_decay_part = 0; t_decay_part <
				no_t_decayprods; ++t_decay_part)
			{
				const reco::GenParticle *decay_product = dynamic_cast<const reco::GenParticle *>(current_particle.daughter(t_decay_part));

				// if decay_product is a b-quark
				if(decay_product->pdgId() == 5){
					if(verbose)
						std::cout << "found b quark..." << std::endl;
					found_b=true;
					b= new reco::GenParticle(*decay_product);
				}
				// if decay_product is a Wplus
				else if(decay_product->pdgId() == 24)
				{
					if(verbose)
						std::cout << "found Wplus boson..." << std::endl;
					found_W=true;

					int no_W_decayprods = decay_product->numberOfDaughters();

					// loop over decay products of W-boson
					for(int W_decay_part=0; W_decay_part <
						no_W_decayprods;
						++W_decay_part)
					{
					//	const Candidate *W_decay_prod = decay_product->daughter(W_decay_part);
						const reco::GenParticle *W_decay_prod = dynamic_cast<const reco::GenParticle *>(decay_product->daughter(W_decay_part));
						if(verbose){
							std::cout << "found W decay product... ";
							std::cout << W_decay_prod->pdgId() << std::endl;
						}

						// fill quarks or leptons to different variables to distinguish the decay modes
						switch(W_decay_prod->pdgId()){
							case -1:
								qbar1 = new reco::GenParticle(*W_decay_prod);
								break;
							case 2:
								q1= new reco::GenParticle(*W_decay_prod);
								break;
							case -3:
								qbar1= new reco::GenParticle(*W_decay_prod);
								break;
							case 4:
								q1= new reco::GenParticle(*W_decay_prod);
								break;
							case -5:
								qbar1= new reco::GenParticle(*W_decay_prod);
								break;
							case 6:
								q1= new reco::GenParticle(*W_decay_prod);
								break;
							case -11:
								lepton1= new reco::GenParticle(*W_decay_prod);
								++decay_mode;
								t_leptonic=true;
								break;
							case 12:
								neutrino1= new reco::GenParticle(*W_decay_prod);
								break;
							case -13:
								lepton1= new reco::GenParticle(*W_decay_prod);
								++decay_mode;
								t_leptonic=true;
								break;
							case 14:
								neutrino1= new reco::GenParticle(*W_decay_prod);
								break;
							case -15:
								lepton1= new reco::GenParticle(*W_decay_prod);
								++decay_mode;
								t_leptonic=true;
								break;
							case 16:
								neutrino1= new reco::GenParticle(*W_decay_prod);
								break;
						};
					}

					Wplus= new reco::GenParticle(*decay_product);
				}

				if(found_W && found_b){
					t = new reco::GenParticle(current_particle);
					found_t=true;
					break;
				}
			}
		}else if(current_particle.pdgId() == -1*top_id) // if it is the anti-top
		// same as for top but this time for tbar
		{
			bool found_bbar=false, found_Wbar=false;
			int no_t_decayprods = current_particle.numberOfDaughters();

			for(int t_decay_part = 0; t_decay_part <
				no_t_decayprods; ++t_decay_part)
			{
				//const Candidate *decay_product = current_particle.daughter(t_decay_part);
				const reco::GenParticle *decay_product = dynamic_cast<const reco::GenParticle *>(current_particle.daughter(t_decay_part));
				if(decay_product->pdgId() == -5){
					found_bbar=true;
					bbar= new reco::GenParticle(*decay_product);
				}
				else if(decay_product->pdgId() == -24)
				{
					found_Wbar=true;
					int no_Wbar_decayprods = decay_product->numberOfDaughters();
					for(int Wbar_decay_part=0; Wbar_decay_part <
						no_Wbar_decayprods;
						++Wbar_decay_part)
					{
						//const Candidate *Wbar_decay_prod = decay_product->daughter(Wbar_decay_part);
						const reco::GenParticle *Wbar_decay_prod = dynamic_cast<const reco::GenParticle *>(decay_product->daughter(Wbar_decay_part));
						switch(Wbar_decay_prod->pdgId()){
							case 1:
								q2= new reco::GenParticle(*Wbar_decay_prod);
								break;
							case -2:
								qbar2= new reco::GenParticle(*Wbar_decay_prod);
								break;
							case 3:
								q2= new reco::GenParticle(*Wbar_decay_prod);
								break;
							case -4:
								qbar2= new reco::GenParticle(*Wbar_decay_prod);
								break;
							case 5:
								q2= new reco::GenParticle(*Wbar_decay_prod);
								break;
							case -6:
								qbar2= new reco::GenParticle(*Wbar_decay_prod);
								break;
							case 11:
								lepton2= new reco::GenParticle(*Wbar_decay_prod);
								++decay_mode;
								tbar_leptonic=true;
								break;
							case -12:
								neutrino2= new reco::GenParticle(*Wbar_decay_prod);
								break;
							case 13:
								lepton2= new reco::GenParticle(*Wbar_decay_prod);
								++decay_mode;
								tbar_leptonic=true;
								break;
							case -14:
								neutrino2= new reco::GenParticle(*Wbar_decay_prod);
								break;
							case 15:
								lepton2= new reco::GenParticle(*Wbar_decay_prod);
								++decay_mode;
								tbar_leptonic=true;
								break;
							case -16:
								neutrino2= new reco::GenParticle(*Wbar_decay_prod);
								break;
						};
					}

					Wminus= new reco::GenParticle(*decay_product);
				}

				if(found_Wbar && found_bbar){
					tbar = new reco::GenParticle(current_particle);
					found_tbar=true;
					break;
				}
			}
		}else continue; // we're not interested in the other particles

		// break if we found already all particles
		if(found_t && found_tbar)
			break;
	}
}

void GenMatch::print_evt_content()
{
	if(!(found_t && found_tbar)){
		std::cout << "Could not find TTbar evt." << std::endl;
	}else{
		std::cout << "decay mode: ";
		switch(decay_mode){
			case 0:	std::cout << "fully hadronic" << std::endl;
				break;
			case 1:	std::cout << "semi-leptonic" << std::endl;
				break;
			case 2:	std::cout << "fully leptonic" << std::endl;
				break;
			default: std::cout << "no decay mode found" << std::endl;
				break;
		};
		std::cout << "t " << t->pdgId()  << " status: " << t->status() << std::endl;
		std::cout << "b " << b->pdgId()  << " status: " << b->status() << std::endl;
		std::cout << "W " << Wplus->pdgId()  << " status: " << Wplus->status() << std::endl;
		if(t_leptonic){
			std::cout << "lepton " << lepton1->pdgId()  << " status: " << lepton1->status() << std::endl;	
			std::cout << "neutrino " << neutrino1->pdgId()  << " status: " << neutrino1->status() << std::endl;	
		}
		else{
			std::cout << "quark " << q1->pdgId()  << " status: " << q1->status() << std::endl;	
			std::cout << "anti-quark " << qbar1->pdgId()  << " status: " << qbar1->status() << std::endl;	
		}
		std::cout << "tbar " << tbar->pdgId()  << " status: " << tbar->status() << std::endl;
		std::cout << "bbar " << bbar->pdgId()  << " status: " << bbar->status() << std::endl;
		std::cout << "W " << Wminus->pdgId()  << " status: " << Wminus->status() << std::endl;
		if(tbar_leptonic){
			std::cout << "lepton " << lepton2->pdgId()  << " status: " << lepton2->status() << std::endl;	
			std::cout << "neutrino " << neutrino2->pdgId()  << " status: " << neutrino2->status() << std::endl;	
		}
		else{
			std::cout << "quark " << q2->pdgId()  << " status: " << q2->status() << std::endl;	
			std::cout << "anti-quark " << qbar2->pdgId()  << " status: " << qbar2->status() << std::endl;	
		}
	}
	std::cout << "---------------------------" << std::endl;
}

// check if t, tbar and decay products were successfully found
bool GenMatch::is_ok()
{
	if(found_t && found_tbar){ 
		if(t_leptonic && (lepton1 == NULL || neutrino1 == NULL))
			return false;
		if(!t_leptonic && (q1 == NULL || qbar1 == NULL))
			return false;
		if(tbar_leptonic && (lepton2 == NULL || neutrino2 == NULL))
			return false;
		if(!tbar_leptonic && (q2 == NULL || qbar2 == NULL))
			return false;
		if(t != NULL && tbar != NULL && b != NULL && bbar != NULL && Wplus != NULL && Wminus != NULL)	
			return true;
		else
			return false;
	}
	else
		return false;
}

// access certain particle of the t/tbar decay
reco::GenParticle* GenMatch::get_particle(std::string identifier)
{
	if(identifier == "t")
		return t;
	if(identifier == "tbar")
		return tbar;
	if(identifier == "b")
		return b;
	if(identifier == "bbar")
		return bbar;
	if(identifier == "Wplus")
		return Wplus;
	if(identifier == "Wminus")
		return Wminus;
	if(identifier == "Wplus_dp1") // Wplus first decay product
		if(t_leptonic)
			return lepton1;
		else
			return q1;
	if(identifier == "Wplus_dp2") // Wplus second decay product
		if(t_leptonic)
			return neutrino1;
		else
			return qbar1;
	if(identifier == "Wminus_dp1")
		if(tbar_leptonic)
			return lepton2;
		else
			return q2;
	if(identifier == "Wminus_dp2")
		if(tbar_leptonic)
			return neutrino2;
		else
			return qbar2;

	// this point should never be reached
	std::cerr << "ERROR: GenMatch::get_particle() identifier not found: " << identifier << std::endl;

	return NULL;
}

bool GenMatch::t_decay_is_leptonic()
{
	return t_leptonic;
}

bool GenMatch::tbar_decay_is_leptonic()
{
	return tbar_leptonic;
}

int GenMatch::get_decay_mode()
{
	return decay_mode;
}

int GenMatch::get_top_id()
{
	return top_id;
}
