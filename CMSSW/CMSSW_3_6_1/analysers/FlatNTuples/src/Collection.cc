#include "../interface/Collection.h"

namespace beag{
template void beag::Collection<pat::Jet, beag::Jet>::fill_collection(edm::Handle<edm::View<pat::Jet> > handle);
template void beag::Collection<pat::Electron, beag::Electron>::fill_collection(edm::Handle<edm::View<pat::Electron> > handle);
template void beag::Collection<pat::Muon, beag::Muon>::fill_collection(edm::Handle<edm::View<pat::Muon> > handle);
template void beag::Collection<pat::MET, beag::MET>::fill_collection(edm::Handle<edm::View<pat::MET> > handle);

template void beag::Collection<pat::Jet, beag::Jet>::fill_common_vars(edm::View<pat::Jet>::const_iterator cmssw_obj, beag::Jet &beag_obj);
template beag::Collection<pat::Jet, beag::Jet>::Collection(TTree *tree, TFile *outfile, std::string ident);
template beag::Collection<pat::Jet, beag::Jet>::~Collection();
template void beag::Collection<pat::Electron, beag::Electron>::fill_common_vars(edm::View<pat::Electron>::const_iterator cmssw_obj, beag::Electron &beag_obj);
template beag::Collection<pat::Electron, beag::Electron>::Collection(TTree *tree, TFile *outfile, std::string ident);
template beag::Collection<pat::Electron, beag::Electron>::~Collection();
template void beag::Collection<pat::Muon, beag::Muon>::fill_common_vars(edm::View<pat::Muon>::const_iterator cmssw_obj, beag::Muon &beag_obj);
template beag::Collection<pat::Muon, beag::Muon>::Collection(TTree *tree, TFile *outfile, std::string ident);
template beag::Collection<pat::Muon, beag::Muon>::~Collection();
template void beag::Collection<pat::MET, beag::MET>::fill_common_vars(edm::View<pat::MET>::const_iterator cmssw_obj, beag::MET &beag_obj);
template beag::Collection<pat::MET, beag::MET>::Collection(TTree *tree, TFile *outfile, std::string ident);
template beag::Collection<pat::MET, beag::MET>::~Collection();

template void beag::Collection<pat::Jet, beag::Jet>::set_selection_list(std::vector<std::string> *selection_list);
template void beag::Collection<pat::Muon, beag::Muon>::set_selection_list(std::vector<std::string> *selection_list);
template void beag::Collection<pat::Electron, beag::Electron>::set_selection_list(std::vector<std::string> *selection_list);
template void beag::Collection<pat::Muon, beag::Muon>::set_tag_list(std::vector<std::string> *tag_list);
template void beag::Collection<pat::Electron, beag::Electron>::set_tag_list(std::vector<std::string> *tag_list);

template void beag::Collection<pat::Muon, beag::Muon>::set_trigger_object_finder(TriggerObjectFinder *object_finder);
template void beag::Collection<pat::Electron, beag::Electron>::set_trigger_object_finder(TriggerObjectFinder *object_finder);

template void beag::Collection<pat::Muon, beag::Muon>::set_aod_trigger_event(edm::Handle<trigger::TriggerEvent> aodTriggerEvent);
template void beag::Collection<pat::Electron, beag::Electron>::set_aod_trigger_event(edm::Handle<trigger::TriggerEvent> aodTriggerEvent);

template void beag::Collection<pat::Muon, beag::Muon>::set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle);
template void beag::Collection<pat::Electron, beag::Electron>::set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle);

template void beag::Collection<pat::Muon, beag::Muon>::set_trackbuilder_handle(edm::ESHandle<TransientTrackBuilder> track_builder, edm::Handle<std::vector<reco::Vertex> > pvertexHandle);
template void beag::Collection<pat::Electron, beag::Electron>::set_trackbuilder_handle(edm::ESHandle<TransientTrackBuilder> track_builder, edm::Handle<std::vector<reco::Vertex> > pvertexHandle);

template void beag::Collection<pat::Muon, beag::Muon>::match_trigger(edm::View<pat::Muon>::const_iterator cmssw_obj, beag::Muon &beag_obj);
template void beag::Collection<pat::Electron, beag::Electron>::match_trigger(edm::View<pat::Electron>::const_iterator cmssw_obj, beag::Electron &beag_obj);

template void beag::Collection<pat::Muon, beag::Muon>::write_gen_info(edm::View<pat::Muon>::const_iterator cmssw_obj, beag::Muon &beag_obj);
template void beag::Collection<pat::Electron, beag::Electron>::write_gen_info(edm::View<pat::Electron>::const_iterator cmssw_obj, beag::Electron &beag_obj);

template std::vector<beag::Muon>* beag::Collection<pat::Muon, beag::Muon>::get_beag_objects();
template std::vector<beag::Electron>* beag::Collection<pat::Electron, beag::Electron>::get_beag_objects();
template std::vector<beag::Jet>* beag::Collection<pat::Jet, beag::Jet>::get_beag_objects();

template void beag::Collection<pat::Muon, beag::Muon>::set_mc_matching(bool match_mc);
template void beag::Collection<pat::Electron, beag::Electron>::set_mc_matching(bool match_mc);

template <class CMSSWObject, class BeagObject>
beag::Collection<CMSSWObject, BeagObject>::Collection(TTree *tree, TFile *outfile, std::string ident)
{
	mc_matching = true;
	this->tree = tree;
	this->outfile = outfile;

	pt_sorter = new PtSorter<BeagObject>();

	beag_objects = new std::vector<BeagObject>();
	tree->Bronch(ident.c_str(),get_type_string(),&beag_objects);
}

template <class CMSSWObject, class BeagObject>
beag::Collection<CMSSWObject, BeagObject>::~Collection()
{
	if(beag_objects){
		delete beag_objects;
		beag_objects = NULL;
	}
	if(pt_sorter){
		delete pt_sorter;
		pt_sorter = NULL;
	}
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle)
{
	outfile->cd();
	beag_objects->clear();

	for(typename edm::View<CMSSWObject>::const_iterator cmssw_obj = handle->begin();
		cmssw_obj != handle->end();
		++cmssw_obj){
		BeagObject *beag_obj = new BeagObject();

		fill_common_vars(cmssw_obj, *beag_obj);
		fill_special_vars(cmssw_obj, *beag_obj);

		pt_sorter->add(beag_obj);
	}

	std::vector<BeagObject*> tmp_objects = pt_sorter->get_sorted();

	for(typename std::vector<BeagObject*>::iterator tmp_obj = tmp_objects.begin();
		tmp_obj != tmp_objects.end();
		++tmp_obj){
		beag_objects->push_back(**tmp_obj);
		delete *tmp_obj;
	}

	pt_sorter->clear();
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::fill_common_vars(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
	beag_obj.eta = cmssw_obj->polarP4().eta();
	beag_obj.phi = cmssw_obj->polarP4().phi();
	beag_obj.mass = cmssw_obj->polarP4().mass();
	beag_obj.pt = cmssw_obj->polarP4().pt();
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::fill_special_vars(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
}

template <>
void beag::Collection<pat::Muon, beag::Muon>::fill_special_vars(edm::View<pat::Muon>::const_iterator pat_muon, beag::Muon &beag_muon)
{
	beag_muon.ecal_iso = pat_muon->isolationR03().emEt;
	beag_muon.hcal_iso = pat_muon->isolationR03().hadEt;
	beag_muon.track_iso = pat_muon->isolationR03().sumPt;

	beag_muon.hcal_vcone = pat_muon->isolationR03().hadVetoEt;
	beag_muon.ecal_vcone = pat_muon->isolationR03().emVetoEt;
	
	beag_muon.d0 = pat_muon->dB();
	beag_muon.d0_sigma = pat_muon->edB();

	reco::TrackRef track = pat_muon->innerTrack();
	if(!track.isNull() && track.isAvailable()){
		// old definition: d0 relative to beamspot
		if(beamSpotHandle.isValid()){
			reco::BeamSpot beamSpot=*beamSpotHandle;
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());

			//beag_muon.d0 = -1.*track->dxy(point);
			//beag_muon.d0_sigma = sqrt( track->d0Error() * track->d0Error() + sqrt(beamSpot.BeamWidthX()*beamSpot.BeamWidthX()+beamSpot.BeamWidthY()*beamSpot.BeamWidthY()) );
			//beag_muon.d0_sigma = track->d0Error();
			beag_muon.z0 = track->dz(point);
			if(pvertexHandle->size() > 0){
				beag_muon.z0_pv = track->dz() - pvertexHandle->begin()->z();
			}
		}else
			std::cerr << "WARNING: beag::Collection<Muon>::fill_special_vars(): no valid beamspot" << std::endl;

		if(track_builder.isValid() ){
			// d0 from reco primary vertex
			reco::TransientTrack tt = track_builder->build(track);
			if(pvertexHandle->size() > 0){
				std::pair<bool,Measurement1D> ip = IPTools::absoluteTransverseImpactParameter(tt, *(pvertexHandle->begin()));
				beag_muon.d0_pv = ip.second.value();
				beag_muon.d0_sigma_pv = ip.second.error();
			}else{
				std::cerr << "WARNING: beag::Collection<Electron>::fill_special_vars(): no primary vertex in event, cannot calculate impact parameter significance wrt primary vertex" << std::endl;
			}
		}
		
		beag_muon.nHits = track->numberOfValidHits();
		beag_muon.track_available = true;
	}else{
		beag_muon.track_available = false;
		//std::cerr << "WARNING: beag::Collection<Muon>::fill_special_vars(): no track available" << std::endl;
	}

	beag_muon.nMatchedSegments = pat_muon->numberOfMatches(); 

	reco::TrackRef global_track = pat_muon->globalTrack();
	if(!global_track.isNull() && global_track.isAvailable()){
		//beag_muon.nHits =  pat_muon->numberOfValidHits(); //for trig sync ex.takes from inner track
		//beag_muon.nHits = global_track->numberOfValidHits();
		beag_muon.chi2 = global_track->chi2() / global_track->ndof();
		reco::HitPattern global_pattern = global_track->hitPattern();
		beag_muon.nMuonHits =  global_pattern.numberOfValidMuonHits(); 
		beag_muon.nStripHits =  global_pattern.numberOfValidTrackerHits(); 
		beag_muon.nPixelLayers =  global_pattern.pixelLayersWithMeasurement(); 
		beag_muon.nLostTrackerHits =  global_pattern.numberOfLostStripHits(); 
		beag_muon.nPixelHits = global_pattern.numberOfValidPixelHits(); 
		//Next part is to get nDT and CSC stations
		unsigned stationMask = pat_muon->stationMask();
		unsigned DTMASK = 0xF;
		unsigned CSCMASK = 0xF0;		
		unsigned DTbits = stationMask & DTMASK;
		unsigned CSCBits = (stationMask & CSCMASK) >> 4 ;
		int tmp_nDTStations = 0;
		int tmp_nCSCStations = 0;
		for (tmp_nDTStations = 0; DTbits > 0; DTbits >>= 1) {
			tmp_nDTStations += (DTbits & 0x1);
		}
		for (tmp_nCSCStations = 0; CSCBits > 0; CSCBits >>= 1) {
			tmp_nCSCStations += (CSCBits & 0x1);
		}
		beag_muon.nDTstations =  tmp_nDTStations; 
		beag_muon.nCSCstations =  tmp_nCSCStations; 

		beag_muon.track_available = true;


	}else{
		// FIXME: add addtional variable for global track?
		beag_muon.track_available = false;
		//std::cerr << "WARNING: beag::Collection<Muon>::fill_special_vars(): no global track available" << std::endl;
	}

	// if lepton IDs set
	beag_muon.lepton_id = 0;
	if(selected_tags != NULL){
		unsigned long long nID=0;
		for(std::vector<std::string>::iterator muID = selected_tags->begin();
			muID != selected_tags->end();
			++muID){
			if(pat_muon->muonID(*muID))
				beag_muon.lepton_id |= ((unsigned long long) 1 << nID);

			++nID;
		}
	}

	beag_muon.charge = pat_muon->charge();

	match_trigger(pat_muon, beag_muon);
	if(mc_matching) write_gen_info(pat_muon, beag_muon);
}

template <>
void beag::Collection<pat::Electron, beag::Electron>::fill_special_vars(edm::View<pat::Electron>::const_iterator pat_electron, beag::Electron &beag_electron)
{
	beag_electron.ecal_iso = pat_electron->dr03EcalRecHitSumEt();
	beag_electron.hcal_iso = pat_electron->dr03HcalTowerSumEt();
	beag_electron.track_iso = pat_electron->dr03TkSumPt();

	beag_electron.hcal_vcone = -1;
	beag_electron.ecal_vcone = -1;

	beag_electron.supercluster_eta = pat_electron->superCluster()->eta();

	reco::GsfTrackRef track = pat_electron->gsfTrack();
	if(!track.isNull() && track.isAvailable()){
		if(beamSpotHandle.isValid()){
			reco::BeamSpot beamSpot=*beamSpotHandle;
			math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());

		//	beag_electron.d0 = -1.*track->dxy(point);
			//beag_electron.d0_sigma = sqrt( track->d0Error() * track->d0Error() + sqrt(beamSpot.BeamWidthX()*beamSpot.BeamWidthX()+beamSpot.BeamWidthY()*beamSpot.BeamWidthY()) );
		//	beag_electron.d0_sigma = track->d0Error();
		}else
			std::cerr << "WARNING: beag::Collection<Electron>::fill_special_vars(): no valid beamspot" << std::endl;

		if(track_builder.isValid()){
			reco::TransientTrack tt = track_builder->build(track);
			if(pvertexHandle->size() > 0){
				std::pair<bool,Measurement1D> ip = IPTools::absoluteTransverseImpactParameter(tt, *(pvertexHandle->begin()));
				beag_electron.d0_pv = ip.second.value();
				beag_electron.d0_sigma_pv = ip.second.error();
			}else{
				std::cerr << "WARNING: beag::Collection<Electron>::fill_special_vars(): no primary vertex in event, cannot calculate impact parameter significance wrt primary vertex" << std::endl;
			}
		}

		beag_electron.d0 = pat_electron->dB();
		beag_electron.d0_sigma = pat_electron->edB();

		beag_electron.nLostTrackerHits = track->trackerExpectedHitsInner().numberOfHits();

		beag_electron.track_available = true;
	}else{
		//std::cerr << "WARNING: beag::Collection<Electron>::fill_special_vars(): no track available" << std::endl;
		beag_electron.track_available = false;
	}

	// if lepton IDs set
	beag_electron.lepton_id = 0;
	if(selected_tags != NULL){
		unsigned long long nID=0;
		for(std::vector<std::string>::iterator eID = selected_tags->begin();
			eID != selected_tags->end();
			++eID){
			if(pat_electron->isElectronIDAvailable(*eID)){
				int eid = (int) pat_electron->electronID(*eID);
				//if(eid & 0x1)
				if(eid == 1 || eid == 3 || eid == 5 || eid == 7)
					beag_electron.lepton_id |= ((unsigned long long) 1 << nID);
			}
			++nID;
		}
	}

	beag_electron.charge = pat_electron->charge();

	match_trigger(pat_electron, beag_electron);
	if(mc_matching) write_gen_info(pat_electron, beag_electron);
}

template <>
void beag::Collection<pat::Jet, beag::Jet>::fill_special_vars(edm::View<pat::Jet>::const_iterator pat_jet, beag::Jet &beag_jet)
{
	// fill btag information from pat jet for selected algorithms
	for(std::vector<std::string>::iterator btag_algo = selected_items->begin();
		btag_algo != selected_items->end();
		++btag_algo){
		beag_jet.btags.push_back(pat_jet->bDiscriminator(*btag_algo));
	}

	beag_jet.emf = pat_jet->emEnergyFraction();
	beag_jet.n90Hits = pat_jet->jetID().n90Hits;
	beag_jet.fHPD = pat_jet->jetID().fHPD;

	// write information of initiating parton
	if(pat_jet->genParton() != NULL){
		beag_jet.mc_eta = pat_jet->genParton()->polarP4().eta();
		beag_jet.mc_phi = pat_jet->genParton()->polarP4().phi();
		beag_jet.mc_mass = pat_jet->genParton()->polarP4().mass();
		beag_jet.mc_pt = pat_jet->genParton()->polarP4().pt();
		beag_jet.mc_id = pat_jet->genParton()->pdgId();

		beag_jet.mc_matched = true;
	}else{
		beag_jet.mc_matched = false;
	}
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::write_gen_info(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
	// write information of initiating particle
	if(cmssw_obj->genParticle() != NULL){
		beag_obj.mc_eta = cmssw_obj->genParticle()->polarP4().eta();
		beag_obj.mc_phi = cmssw_obj->genParticle()->polarP4().phi();
		beag_obj.mc_mass = cmssw_obj->genParticle()->polarP4().mass();
		beag_obj.mc_pt = cmssw_obj->genParticle()->polarP4().pt();
		beag_obj.mc_id = cmssw_obj->genParticle()->pdgId();

		beag_obj.mc_matched = true;
	}else{
		beag_obj.mc_matched = false;
	}
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::match_trigger(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, BeagObject &beag_obj)
{
	if(trigger_obj_find == NULL){
		std::cerr << " ERROR: Collection: TriggerObjectFinder not set. Exiting." << std::endl;
		return;
	}
	
	if(!trigger_obj_find->check_objects_found()){
		std::cerr << "ERROR: Collection: TriggerObjectFinder find trigger objects not run. Exiting." << std::endl;
		return;
	}
	beag_obj.trigger = 0;
	const trigger::TriggerObjectCollection objects = aodTriggerEvent->getObjects();
	
	//map<key id, trigger id>
	std::map<int,int> keys_found = trigger_obj_find->get_keys_found();
	std::map<int,bool> trigger_matched;

	for(std::map<int,int>::iterator key = keys_found.begin(); key != keys_found.end();key++){
		if(trigger_matched.empty() || (trigger_matched.find((*key).second) == trigger_matched.end())){

			trigger::TriggerObject foundObject = objects[(*key).first];
			double deta = beag_obj.eta - foundObject.eta();
			double dphi = beag_obj.phi - foundObject.phi();
			double dR = sqrt(deta*deta + dphi*dphi);
 			if(dR < 0.3){
 				beag_obj.trigger |= ((unsigned long long) 1 << (*key).second);
 				trigger_matched[(*key).second] = true;
				
 			}
		}
					     

	}
}

template <class CMSSWObject, class BeagObject>
const char* beag::Collection<CMSSWObject, BeagObject>::get_type_string()
{
	std::cerr << "ERROR: invalid typename in beag::Collection::get_type_string()" << std::endl;
	return "";
}

template <>
const char* beag::Collection<pat::Jet, beag::Jet>::get_type_string()
{
	return "std::vector<beag::Jet>";
}

template <>
const char* beag::Collection<pat::Electron, beag::Electron>::get_type_string()
{
	return "std::vector<beag::Electron>";
}

template <>
const char* beag::Collection<pat::Muon, beag::Muon>::get_type_string()
{
	return "std::vector<beag::Muon>";
}

template <>
const char* beag::Collection<pat::MET, beag::MET>::get_type_string()
{
	return "std::vector<beag::MET>";
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_trigger_object_finder(TriggerObjectFinder *object_finder)
{
	trigger_obj_find = object_finder;
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_aod_trigger_event(edm::Handle<trigger::TriggerEvent> aodTriggerEvent)
{
	this->aodTriggerEvent = aodTriggerEvent;
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_selection_list(std::vector<std::string> *selection_list)
{
	selected_items = selection_list;
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_tag_list(std::vector<std::string> *tag_list)
{
	selected_tags = tag_list;
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_beamspot_handle(edm::Handle<reco::BeamSpot> beamSpotHandle)
{
	this->beamSpotHandle = beamSpotHandle;
}

template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_trackbuilder_handle(edm::ESHandle<TransientTrackBuilder> track_builder, edm::Handle<std::vector<reco::Vertex> > pvertexHandle)
{
	this->track_builder = track_builder;
	this->pvertexHandle = pvertexHandle;
}

template <class CMSSWObject, class BeagObject>
std::vector<BeagObject>* beag::Collection<CMSSWObject, BeagObject>::get_beag_objects()
{
	return beag_objects;
}


template <class CMSSWObject, class BeagObject>
void beag::Collection<CMSSWObject, BeagObject>::set_mc_matching(bool match_mc)
{
	mc_matching = match_mc;
}

}	// end namespace beag
