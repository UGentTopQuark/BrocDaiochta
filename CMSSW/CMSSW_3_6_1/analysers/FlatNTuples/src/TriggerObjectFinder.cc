#include "../interface/TriggerObjectFinder.h"

beag::TriggerObjectFinder::TriggerObjectFinder(TFile *outfile)
{
	this->outfile = outfile;
	trigger_menu = "HLT";
	objects_found = false;
}
beag::TriggerObjectFinder::~TriggerObjectFinder()
{
}


void beag::TriggerObjectFinder::find_trigger_objects(edm::Handle<trigger::TriggerEvent> aodTriggerEvent, edm::Handle<edm::TriggerResults> HLTR)
{

	this->aodTriggerEvent = aodTriggerEvent;

	if(triggers_to_write->size() >= 64){
		std::cerr << " ERROR: more than 64 triggers examined, skipping object selection: " << triggers_to_write->size() <<std::endl;
		return;
	}
	
	if(!HLTR.isValid()){
		std::cerr << "WARNING: beag::TriggerObjectProducer::fill_trigger_information() no valid trigger object found" << std::endl;
		return;
	}
	
	//Loop over all triggers and save results for those that passed
	for(int nbeag_trig = 0; nbeag_trig < int(triggers_to_write->size());nbeag_trig++){
		std::string trigger_name = (*triggers_to_write)[nbeag_trig];
		
		//Skip if is one of vetoed triggers
		if(triggers_to_veto.find(trigger_name) != triggers_to_veto.end())
			continue;
		
      		get_trigger_objects(1,nbeag_trig); //objects for L1 seeds
		
		//Check if event passed trigger before checking hlt objects. 
		unsigned int trigger_id( hltConfig->triggerIndex(trigger_name) );
		if(!(HLTR->accept(trigger_id)))
			continue; 
		
		if(verbose) std::cout << " Event passed trigger: " << trigger_name << std::endl;
		
		get_trigger_objects(0,nbeag_trig); //hlt trigger objects
		
		//NB: If you add another long long to objects_to_write remember to change assign(?, 0) in get_trigger_objects
		
	}
	objects_found = true;
}

//Get trigger objects passing a given module. map<keys,trigger_results>
void beag::TriggerObjectFinder::get_trigger_objects(int module,int nbeag_trig)
{
	std::string trigger_name = (*triggers_to_write)[nbeag_trig];

	std::vector<std::string> module_names = hltConfig->moduleLabels(trigger_name);
	std::string module_name = "";

	for ( size_t i = 0; i < module_names.size(); i++ ) {
		std::string tmp_module = module_names[i];

		if(module == 0 && (!TString(tmp_module).Contains("hltBoolEnd"))){
			module_name = tmp_module;
		}
		else if (module == 1 && hltConfig->moduleType(tmp_module) == "HLTLevel1GTSeed" ) 
			module_name = tmp_module;
		else if(module != 0 && module != 1)
			std::cerr << "ERROR: No module set in TriggerObjectProvider " << std::endl;
	}
	if(module_name == "")
		std::cerr << "ERROR TrigObjProd: No Module Name found for: " <<trigger_name << std::endl;
	
	
	if(verbose) std::cout << " Trigger: " << trigger_name << ", nbeag_trig " << nbeag_trig << ", module: " <<module << ", module name: " << module_name << std::endl; 
	       
	//Now use module name to get keys for trigger objects
	edm::InputTag filterTag = edm::InputTag(module_name, "", trigger_menu);
		
	int filterIndex = aodTriggerEvent->filterIndex(filterTag);
		
	if ( filterIndex < aodTriggerEvent->sizeFilters() ) {
		//These trigger keys point you to the trigger objects passing this filter in the objects collection	
		const trigger::Keys &keys = aodTriggerEvent->filterKeys( filterIndex );
			
			for ( size_t i = 0; i < keys.size(); i++ ){
				if(objects_to_write.find(keys[i]) == objects_to_write.end()){
					objects_to_write[keys[i]].assign(2,0);
				}

				objects_to_write[keys[i]][module] |= ((unsigned long long) 1 << nbeag_trig);
				keys_found[keys[i]] = nbeag_trig;
 			}
	}

}

bool beag::TriggerObjectFinder::check_objects_found()
{
	return objects_found;
}

std::map<int,std::vector<unsigned long long> > beag::TriggerObjectFinder::get_objects_to_write()
{
	return objects_to_write;
}

std::map<int,int> beag::TriggerObjectFinder::get_keys_found()
{
	return keys_found;
}

void beag::TriggerObjectFinder::set_triggers_to_veto(std::vector<std::string> *veto_triggers)
{
	if(!triggers_to_veto.empty()) triggers_to_veto.clear();
	for(std::vector<std::string>::iterator vtrig = veto_triggers->begin();vtrig != veto_triggers->end();vtrig++)
		triggers_to_veto[*vtrig] = true;
}

void beag::TriggerObjectFinder::set_hlt_config_provider(HLTConfigProvider *hltConfig)
{
	this->hltConfig = hltConfig;
}

void beag::TriggerObjectFinder::set_triggers_to_write(std::vector<std::string> *triggers)
{
	this->triggers_to_write = triggers;
}   
void beag::TriggerObjectFinder::set_trigger_menu(std::string trigger_menu)
{
	this->trigger_menu = trigger_menu;
}
