#include "../interface/TriggerObjectProducer.h"

beag::TriggerObjectProducer::TriggerObjectProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	trigger_objects = new std::vector<beag::TriggerObject>();
	trigger_obj_find = NULL;
	tree->Bronch(ident.c_str(),"std::vector<beag::TriggerObject>",&trigger_objects);
}

beag::TriggerObjectProducer::~TriggerObjectProducer()
{
	if(trigger_objects){
		delete trigger_objects;
		trigger_objects = NULL;
	}
}

void beag::TriggerObjectProducer::set_trigger_object_finder(TriggerObjectFinder *object_finder)
{
	trigger_obj_find = object_finder;
}

void beag::TriggerObjectProducer::fill_trigger_objects(edm::Handle<trigger::TriggerEvent> aodTriggerEvent)
{
	this->aodTriggerEvent = aodTriggerEvent;
	trigger_objects->clear();

	if(trigger_obj_find == NULL){
		std::cerr << " ERROR: TrigObjProd: TriggerObjectFinder not set. Exiting." << std::endl;
		return;
	}
	
	if(!trigger_obj_find->check_objects_found()){
		std::cerr << "ERROR: TrigObjProd: TriggerObjectFinder find trigger objects not run. Exiting." << std::endl;
		return;
	}		

	std::map<int,std::vector<std::vector<bool> > > objects_to_write = trigger_obj_find->get_objects_to_write();
	if(verbose) std::cout << " TrigObjProd: N object to write: " << objects_to_write.size() << std::endl;

	const trigger::TriggerObjectCollection objects = aodTriggerEvent->getObjects();
	
	//Loop over objects to write and fill to beag::trigger_objects
	for(std::map<int,std::vector<std::vector<bool> > >::iterator nobj = objects_to_write.begin();nobj != objects_to_write.end();nobj++){
		trigger::TriggerObject foundObject = objects[nobj->first];

		beag::TriggerObject tmp;
		tmp.eta = foundObject.eta();
		tmp.phi = foundObject.phi();
		tmp.mass = foundObject.mass();
		tmp.pt = foundObject.pt();
		tmp.triggered = nobj->second;

		if(verbose){
			int i=0,k=0;
			for(std::vector<std::vector<bool> >::iterator trigger = tmp.triggered.begin();
				trigger != tmp.triggered.end();
				++trigger){
				std::cout << "trigger: " << i << std::endl;
				for(std::vector<bool>::iterator	module = trigger->begin();
					module != trigger->end();
					++module){
					std::cout << "module: " << k << " selected: " << *module << std::endl;
					++k;
				}
				++i;
			}
		}
		if(verbose) std::cout << "key: " << nobj->first << " trig obj pt: " << tmp.pt << " eta: " << tmp.eta << std::endl;
			
		trigger_objects->push_back(tmp);
	}
	if(verbose) std::cout << "--- end fill trigger objects ---" << std::endl;
}
