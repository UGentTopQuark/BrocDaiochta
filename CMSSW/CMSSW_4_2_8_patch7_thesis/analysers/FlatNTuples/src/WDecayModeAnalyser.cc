#include "../interface/WDecayModeAnalyser.h"

WDecayModeAnalyser::WDecayModeAnalyser()
{
	decay_mode=0;
}

WDecayModeAnalyser::~WDecayModeAnalyser()
{
}

void WDecayModeAnalyser::fill_events_from_collection(edm::Handle<reco::GenParticleCollection> part_collection)
{
	using namespace edm;
	using namespace reco;

	// loop over all particles in the event
	for(unsigned int nopart = 0; nopart < part_collection->size(); ++nopart) {
		// assign current particle
		const GenParticle &current_particle = (*part_collection)[nopart];

		if(fabs(current_particle.pdgId()) == 2212) // if the current particle is a proton
		{
			process_daughters(&current_particle, "");
		}
	}
}

bool WDecayModeAnalyser::process_daughters(const reco::GenParticle *current_particle, std::string shift_str)
{
	// read out number of decay products of top
	int no_decayprods = current_particle->numberOfDaughters();

	bool is_final_W = false;
	
	// loop over all decay products of the top
	for(int decay_part = 0; (decay_part <
	        no_decayprods) && !is_final_W; ++decay_part)
	{
	        const reco::GenParticle *decay_product = dynamic_cast<const reco::GenParticle *>(current_particle->daughter(decay_part));
	
	        // std::cout << shift_str << "->ID: " << decay_product->pdgId() << " status: " << decay_product->status() << std::endl;

		if(fabs(decay_product->pdgId()) == 24 && decay_product->status() == 2){
			is_final_W = true;
		}

		if(!is_final_W && process_daughters(decay_product, "---"+shift_str)) return true;
	}

	if(is_final_W){
		for(int decay_part = 0; decay_part <
		        no_decayprods; ++decay_part)
		{
	        	const reco::GenParticle *decay_product = dynamic_cast<const reco::GenParticle *>(current_particle->daughter(decay_part));
	        	//std::cout << shift_str << "->ID: " << decay_product->pdgId() << " status: " << decay_product->status() << std::endl;

			if(fabs(decay_product->pdgId()) != 24){
				switch(std::abs(decay_product->pdgId())){
					case 11:
						decay_mode = 1;
						break;
					case 12:
						decay_mode = 1;
						break;
					case 13:
						decay_mode = 2;
						break;
					case 14:
						decay_mode = 2;
						break;
					case 15:
						decay_mode = 3;
						break;
					case 16:
						decay_mode = 3;
						break;
					default:
						decay_mode = -1;
						break;
				};
			}
		}
	}

	return is_final_W;
}

int WDecayModeAnalyser::get_decay_mode()
{
	return decay_mode;
}
