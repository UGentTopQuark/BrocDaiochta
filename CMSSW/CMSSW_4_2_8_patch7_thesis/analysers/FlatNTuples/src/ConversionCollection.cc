#include "../interface/ConversionCollection.h"

template void beag::ConversionCollection::fill_collection(const edm::Event& iEvent, const edm::EventSetup& iSetup, edm::Handle<edm::View<reco::GsfElectron> > hElectrons);
template void beag::ConversionCollection::fill_collection(const edm::Event& iEvent, const edm::EventSetup& iSetup, edm::Handle<edm::View<pat::Electron> > hElectrons);

beag::ConversionCollection::ConversionCollection(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;
	beag_objects = new std::vector<beag::Conversion>();
	tree->Bronch(ident.c_str(),"std::vector<beag::Conversion>",&this->beag_objects);
}

beag::ConversionCollection::~ConversionCollection()
{
	if(beag_objects){ delete beag_objects; beag_objects = 0; }
}

template <class CMSSWObject>
void beag::ConversionCollection::fill_collection(const edm::Event& iEvent, const edm::EventSetup& iSetup, typename edm::Handle<edm::View<CMSSWObject> > hElectrons)
{
	beag_objects->clear();

	edm::Handle<reco::BeamSpot> bsHandle;
	iEvent.getByLabel("offlineBeamSpot", bsHandle);
	const reco::BeamSpot &beamspot = *bsHandle.product();
	
	edm::Handle<reco::ConversionCollection> hConversions;
	iEvent.getByLabel("allConversions", hConversions);
	
	int iconv=-1;
	for (reco::ConversionCollection::const_iterator conv = hConversions->begin(); conv!= hConversions->end(); ++conv) {
		iconv++;
		beag::Conversion conversion;
	
		reco::Vertex vtx = conv->conversionVertex();
		if (vtx.isValid()) {
			int iel=-1;
			for(typename edm::View<CMSSWObject>::const_iterator gsfEle = hElectrons->begin(); gsfEle!=hElectrons->end(); ++gsfEle) {
			  iel++;
				if (ConversionTools::matchesConversion(*gsfEle, *conv)) {
					conversion.set_eleind(iel);
					conversion.set_vtxProb(TMath::Prob( vtx.chi2(), vtx.ndof()));
					math::XYZVector mom(conv->refittedPairMomentum());
					double dbsx = vtx.x() - beamspot.position().x();
					double dbsy = vtx.y() - beamspot.position().y();
					conversion.set_lxy((mom.x()*dbsx + mom.y()*dbsy)/mom.rho());
					conversion.set_nHitsMax(0);
					for (std::vector<uint8_t>::const_iterator it = conv->nHitsBeforeVtx().begin(); it!=conv->nHitsBeforeVtx().end(); ++it) {
						if ((*it)>conversion.nHitsMax()) conversion.set_nHitsMax(*it);
					}
					beag_objects->push_back(conversion);
					break;
				}
			}
		}
		
	}
}
