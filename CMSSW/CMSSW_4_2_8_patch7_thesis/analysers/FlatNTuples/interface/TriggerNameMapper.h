#ifndef BEAGTRIGGERNAMEMAPPER_H
#define BEAGTRIGGERNAMEMAPPER_H

#include <vector>
#include <map>
#include <string>
#include <boost/regex.hpp>
#include "TFile.h"
#include "TTree.h"
#include "stdlib.h"
#include <iostream>
#include "DataFormats/Common/interface/View.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

namespace beag{
	class TriggerNameMapper{
		public:
		TriggerNameMapper(TTree *tree, TFile *outfile, std::string ident, bool write_prescales,bool write = true);
			~TriggerNameMapper();
			std::vector<std::string>* get_selected_trigger_names(const edm::Event& iEvent, const edm::EventSetup& iSetup, std::vector<std::string> *valid_triggers, HLTConfigProvider *hltConfig, unsigned int prescale_set,bool to_be_written = true);
			void set_trigger_expressions(std::vector<std::string> &trigger_expressions);
			void set_selected_triggers(std::vector<std::string> &selected_triggers);
			inline std::vector<std::vector<std::string> >* get_modules_for_trigger(){ return modules_for_trigger; };
			
		private:
			std::vector<std::string> *selected_triggers;
			std::vector<std::vector<std::string> > *modules_for_trigger;
			std::vector<int> *l1_prescales;
			std::vector<int> *hlt_prescales;
			std::vector<std::string> trigger_expressions;

			TTree *tree;
			TFile *outfile;
			
			static const bool verbose = false;

			bool write;
			bool write_prescales;
	};
}

#endif
