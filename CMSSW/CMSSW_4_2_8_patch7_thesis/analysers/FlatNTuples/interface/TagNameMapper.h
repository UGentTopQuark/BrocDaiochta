#ifndef BEAGTAGNAMEMAPPER_H
#define BEAGTAGNAMEMAPPER_H

#include "TTree.h"
#include "TFile.h"
#include <string>
#include <vector>
#include <iostream>

#include "FWCore/ParameterSet/interface/ParameterSet.h"

namespace beag{
	class TagNameMapper{
		public:
			TagNameMapper(TTree *tree, TFile *outfile, std::string ident);
			~TagNameMapper();
			
			void set_tag_names(std::vector<std::string> *tag_names);
			void set_tag_names(std::vector<edm::InputTag> *tag_names);
		private:	
			TTree *tree;
			TFile *outfile;

			std::vector<std::string> *selected_tags;
	};
}

#endif
