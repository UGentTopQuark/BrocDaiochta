#ifndef BTAG_ASSOCIATOR_H
#define BTAG_ASSOCIATOR_H

#include <DataFormats/Common/interface/Ref.h>
#include "DataFormats/Common/interface/View.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"

#include "Math/VectorUtil.h"

#include "DataFormats/BTauReco/interface/JetTag.h"
#include "DataFormats/BTauReco/interface/TrackProbabilityTagInfo.h"
#include "DataFormats/BTauReco/interface/TrackIPTagInfo.h"
#include "DataFormats/BTauReco/interface/TrackCountingTagInfo.h"
#include "DataFormats/BTauReco/interface/SecondaryVertexTagInfo.h"
#include "DataFormats/BTauReco/interface/SoftLeptonTagInfo.h"

#include <DataFormats/JetReco/interface/Jet.h>
#include <DataFormats/JetReco/interface/CaloJet.h>
#include <DataFormats/JetReco/interface/PFJet.h>
#include <DataFormats/JetReco/interface/JPTJet.h>
#include "DataFormats/PatCandidates/interface/Jet.h"

namespace beag{
	template<class CMSSWObject>
	class BTagAssociator{
		public:
		        BTagAssociator();
			void set_handles(typename edm::Handle<edm::View<CMSSWObject> > jetHandle, std::vector<std::string> btag_algos, const edm::Event& iEvent);
			std::vector<double> fill_bdiscriminators(typename edm::View<CMSSWObject>::const_iterator cmssw_obj);
		private:

		        std::vector<edm::Handle<reco::JetTagCollection> > jetAlgorithms;

			typename edm::Handle<edm::View<CMSSWObject> > jetHandle;
			
			/*
			  https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookBTagging#BtagEdAnalyzer
			*/

	};
}

#endif
