#ifndef BEAGPARTNERTRACKFINDER_H
#define BEAGPARTNERTRACKFINDER_H

#include "RecoEgamma/EgammaTools/interface/ConversionFinder.h"
#include "MagneticField/Records/interface/IdealMagneticFieldRecord.h"
#include "MagneticField/Engine/interface/MagneticField.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackExtra.h"
#include <DataFormats/Common/interface/Ref.h>
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include <DataFormats/Scalers/interface/DcsStatus.h>
#include <FWCore/Framework/interface/ESHandle.h>

namespace beag{
	class PartnerTrackFinder{
		public:
			void set_handles(edm::Handle<reco::TrackCollection> tracks_h,
					edm::Handle<DcsStatusCollection> dcsHandle);
			void next_event(const edm::EventSetup& iSetup, bool is_data);
			void calculate(const reco::GsfElectron *electron);

			inline double get_dist(){ return dist; };
			inline double get_dcot(){ return dcot; };
			inline double get_convradius(){ return convradius; };
			inline math::XYZPoint get_convPoint(){ return convPoint; };
		private:
			edm::Handle<reco::TrackCollection> tracks_h;
			edm::Handle<DcsStatusCollection> dcsHandle;

			double evt_bField;

			double dist;
			double dcot;
			double convradius;
			math::XYZPoint convPoint;
	};
}

#endif
