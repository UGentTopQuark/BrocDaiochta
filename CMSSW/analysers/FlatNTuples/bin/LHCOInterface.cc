#include <memory>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>

#include <TROOT.h>
#include <TFile.h>
#include <TSystem.h>

#include "../interface/LHCOReader.h"

int main(int argc, char *argv[]) 
{

	//for (int thearg = 0; thearg < argc; ++thearg) 
	//	std::cout << "arg[" << thearg << "]" << "  -  "  << argv[thearg] << std::endl;
	
	std::string infile;
	std::string outfile;
	int smearing_mode = 0;
	int max_events = -1;

	if (argc!=4){
		std::cout << "Invalid arguments..." << std::endl;
		return 1;
	}else{
		infile = argv[0];
		outfile = argv[1];
		smearing_mode = atoi(argv[2]);
		max_events = atoi(argv[3]);
	}

	std::cout << "processing infile " << infile << std::endl;
	std::cout << "creating outfile  " << outfile << std::endl;
	std::cout << " " << std::endl;

	LHCOReader lhco_reader(infile, outfile, smearing_mode, max_events);

	lhco_reader.convert_input();

	return 0;
}


