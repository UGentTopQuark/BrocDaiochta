import FWCore.ParameterSet.Config as cms


#process = cms.Process("Demo")

#rerun btag
#Need to import this before doing anything else because process is initialised there
from analysers.FlatNTuples.rerun_btag_cfg import *

process.load("FWCore.MessageService.MessageLogger_cfi")

## Options and Output Report
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

process.load("Configuration.StandardSequences.Geometry_cff")
process.load("Configuration.StandardSequences.MagneticField_cff")
process.load("Configuration.StandardSequences.Reconstruction_cff")

process.load("Configuration.StandardSequences.Services_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")

#process.GlobalTag.globaltag = 'GR_R_39X_V5::All'
######process.GlobalTag.globaltag = 'GR_R_38X_V15::All' ##data
#process.GlobalTag.globaltag = 'START38_V14::All'  ##MC
#process.GlobalTag.globaltag = 'START42_V13::All'  ##MC
process.GlobalTag.globaltag = 'START52_V7::All'
#process.GlobalTag.globaltag = 'GR_R_311_V2::All'

# muon propagator requirements
process.load("TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAny_cfi")
process.load("TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAlong_cfi")
process.load("TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorOpposite_cfi")
process.load("RecoMuon.DetLayers.muonDetLayerGeometry_cfi")

process.load('JetMETCorrections.Configuration.DefaultJEC_cff')

process.load("CommonTools.RecoAlgos.HBHENoiseFilter_cfi")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
#    'file:////user/bklein/cmssw_64/CMSSW_4_1_5/src/analysers/FlatNTuples/2011A_AOD.root'
    #'file:////user/bklein/trigger_cmssw/CMSSW_3_8_3/src/analysers/FlatNTuples/TopTrigSkim.root'
    #'dcap:///pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/TrigSkim_v1r1/Data_muPD_2010BPR_job_4211_TopTrigSkim.root'
    #'file:////user/walsh/CMSSW_3_8_3/src/testfiles/AOD_Data_Mu2010B.root'
    #'file:////user/bklein/trigger_cmssw/CMSSW_3_8_3/src/analysers/FlatNTuples/DYLL_AOD.root'
    #'dcap:///pnfs/iihe/cms/store/user/mccartin/AOD/datasets/Fall10/madgraph/TTbar_norm/TTJets_TuneD6T_7TeV_madgraphjob_Fall10AOD_7_1_zHe.root'
    #'dcap:///pnfs/iihe/cms/store/user/bklein/skims/Zmumu_Fall10MC_v1/DYmumu_Z2_powheg_0020_job_221_TopTrigSkim.root'
    #'dcap:///pnfs/iihe/cms/store/user/bklein/skims/Summer10MC_v1/TsChan_job_1533_TopTrigSkim.root'
#    'dcap:///pnfs/iihe/cms/store/user/mccartin/TTJets_TuneD6T_7TeV-madgraph-tauola/TTJets_LpJlightPRESEL_Fall10AOD/88254b285410dd243c62bda780858b19/TTJets_TuneD6T_7TeV_madgraphjob_Fall10AOD_100_1_6Ri.root',
#'dcap:///pnfs/iihe/cms/store/user/mccartin/TTJets_TuneD6T_7TeV-madgraph-tauola/TTJets_LpJlightPRESEL_Fall10AOD/88254b285410dd243c62bda780858b19/TTJets_TuneD6T_7TeV_madgraphjob_Fall10AOD_101_1_xSN.root',
#'dcap:///pnfs/iihe/cms/store/user/mccartin/TTJets_TuneD6T_7TeV-madgraph-tauola/TTJets_LpJlightPRESEL_Fall10AOD/88254b285410dd243c62bda780858b19/TTJets_TuneD6T_7TeV_madgraphjob_Fall10AOD_102_1_VMg.root',
#'dcap:///pnfs/iihe/cms/store/user/mccartin/TTJets_TuneD6T_7TeV-madgraph-tauola/TTJets_LpJlightPRESEL_Fall10AOD/88254b285410dd243c62bda780858b19/TTJets_TuneD6T_7TeV_madgraphjob_Fall10AOD_103_1_TKx.root',
#'dcap:///pnfs/iihe/cms/store/user/mccartin/TTJets_TuneD6T_7TeV-madgraph-tauola/TTJets_LpJlightPRESEL_Fall10AOD/88254b285410dd243c62bda780858b19/TTJets_TuneD6T_7TeV_madgraphjob_Fall10AOD_104_1_xOu.root',
    #'file:////user/bklein/trigger_cmssw/CMSSW_3_8_3/src/grid-control/DYToEESummer11AOD.root'
    #'dcap:///pnfs/iihe/cms/store/user/mccartin/test/TTJets_TuneZ2_7TeV-madgraph-Summer11-PU_S4_START42_V11-test_8_1_s9p.root'
    #'file:////user/bklein/cmssw_64/CMSSW_4_2_5/src/analysers/FlatNTuples/gentPatSkimPF2PAT.root'
    #'file:////user/bklein/AOD_FlatCrash.root'
    #'file:////user/bklein/RelValZMM_CMSSW_4_2_9_HLT1_RECO.root'
    #'file:////user/bklein/Mu_CMSSW_4_2_9_HLT1_hltpatch1_RECO.root'
    #'file:////user/bklein/AOD_v6_Mu_latest_trigger_menu.root'
    'file:////user/bklein/TTbar_relval_522.root'
    )
)
	
#process.load('RecoJets.JetProducers.kt4PFJets_cfi')
#process.kt6PFJets = process.kt4PFJets.clone( rParam = 0.6, doRhoFastjet = True )
#process.kt6PFJets.Rho_EtaMax = cms.double(2.5)

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(50) )

# Process the PDF weights
process.pdfWeights = cms.EDProducer("PdfWeightProducer",
        PdfInfoTag = cms.untracked.InputTag("generator"),
        PdfSetNames = cms.untracked.vstring("cteq66.LHgrid")
	)

###########   require scraping filter
process.scrapingVeto = cms.EDFilter("FilterOutScraping",
                                    	applyfilter = cms.untracked.bool(True),
					debugOn = cms.untracked.bool(False),
					numtrack = cms.untracked.uint32(10),
					thresh = cms.untracked.double(0.25))

# Processing Mode:
# 0: PAT
# 1: AOD Calo
# 2: AOD PF
# 3: AOD Calo+PF, jl 04.02.11
processing_mode = 2

if(processing_mode == 0):
    rerunBTag = eval('False')
    electron = "selectedPatElectrons"
    muon = "selectedPatMuons"
    jet = "selectedPatJets"
    jetCorService = ""
    jetID = ""
    met = "patMETs"
    pfmet = ""
    pfjet = ""
    pfjetCorService = ""
    pfjetID = ""
    genjet = ""
    tcmet = ""
    pfmetTypeI = ""
    pfmetTypeII = ""
elif(processing_mode == 1):
    rerunBTag = eval('True')
    electron = "gsfElectrons"
    muon = "muons"
    jet = "ak5CaloJets"
    jetCorService = "ak5CaloL2L3"
    jetID = "ak5JetID"
    met = "met"
    pfmet = ""
    pfjet = ""
    pfjetCorService = ""
    pfjetID = ""
elif(processing_mode == 2):
    rerunBTag = eval('True')
    electron = "gsfElectrons"
    muon = "muons"
    jet = "ak5PFJets"
    jetCorService = ""
    #jetCorService = "ak5PFL2L3"
    jetID = "ak5PFJetID"
    met = "pfMet"
    pfmet = ""
    pfjet = ""
    pfjetCorService = ""
    pfjetID = ""
    genjet = "ak5GenJets"
    tcmet = "tcMet"
    pfmetTypeI = "metJESCorPFAK5"
    pfmetTypeII = "metJESCorPFAK5"
elif(processing_mode == 3) :
    rerunBTag = eval('True')
    electron = "gsfElectrons"
    muon = "muons"
    jet = "ak5CaloJets"
    jetCorService = "ak5CaloL2L3"
    jetID = "ak5JetID"
    pfjet = "ak5PFJets"
    pfjetCorService = "ak5PFL2L3"
    pfjetID = "ak5PFJetID"
    met = "met"
    pfmet = "pfMet"
    tcmet = "tcMet"
    pfmetTypeI = "metJESCorPFAK5"
    pfmetTypeII = "metJESCorPFAK5"




#from JetMETCorrections.Type1MET.MetType1Corrections_cff import metJESCorAK5CaloJet
#metMuonJESCorAK5 = metJESCorAK5CaloJet.clone()
#metMuonJESCorAK5.inputUncorMetLabel = "corMetGlobalMuons"
#metCorSequence = cms.Sequence(metMuonJESCorAK5)

##SC, PFMET typeI corrections
## cvs co -r V04-04-00 JetMETCorrections/Type1MET
from JetMETCorrections.Type1MET.MetType1Corrections_cff import metJESCorAK5PFJet
process.metJESCorPFAK5 = metJESCorAK5PFJet.clone()
process.metJESCorPFAK5.inputUncorJetsLabel = "ak5PFJets"
process.metJESCorPFAK5.metType = "PFMET"
process.metJESCorPFAK5.inputUncorMetLabel = "pfMet"
process.metJESCorPFAK5.useTypeII = False
process.metJESCorPFAK5.jetPTthreshold = cms.double(10.0)

##SC, PFMET typeII corrections, needed for releases earlier than 3_11_X 
## cvs co -r V04-04-00 JetMETCorrections/Type1MET

#process.metJESCorPFAK5.useTypeII = True
process.metJESCorPFAK5.jetPTthreshold = cms.double(10.0)
process.metJESCorPFAK5.UscaleA = cms.double(1.5)
process.metJESCorPFAK5.UscaleB = cms.double(0)
process.metJESCorPFAK5.UscaleC = cms.double(0)
process.metJESCorPFAK5.inputUncorUnlusteredLabel = cms.untracked.InputTag("pfNoJetPFlow") ##put your collection of unclustered energy here!
process.metJESCorPFAK5.metType ="PFMET"


process.produceNTuples = cms.EDAnalyzer('FlatNTuples',
	tauTag      = cms.untracked.InputTag("selectedPatTaus"),
	photonTag   = cms.untracked.InputTag("selectedPatPhotons"),

	##########################
	###	Select inputs
	###	For AOD objects make sure you know what you are doing. It's
	###	important to select the correct JetID, JEC, etc.
	##########################
        electronTag = cms.untracked.InputTag(electron),
        muonTag     = cms.untracked.InputTag(muon),
        jetTag      = cms.untracked.InputTag(jet),
        genjetTag      = cms.untracked.InputTag(genjet),
        jetCorServiceName = cms.untracked.string(jetCorService),
        jetIDTag      = cms.untracked.InputTag(jetID),
        metTag      = cms.untracked.InputTag(met),
	#jl 04.02.11: pf tags
	pfmetTag    = cms.untracked.InputTag(pfmet),
	pfjetTag    = cms.untracked.InputTag(pfjet),
	pfjetCorServiceName = cms.untracked.string(pfjetCorService),
	pfjetIDTag = cms.untracked.InputTag(pfjetID),
##SC
	nomujetTag      = cms.untracked.InputTag(met),
	nonisoelectronTag     = cms.untracked.InputTag(met),
	        noejetTag      = cms.untracked.InputTag(met),
		        nonisomuonTag     = cms.untracked.InputTag(met),

	tcmetTag        = cms.untracked.InputTag(tcmet),
	pfmetTypeITag   = cms.untracked.InputTag(pfmetTypeI),
	pfmetTypeIITag   = cms.untracked.InputTag(pfmetTypeII),
                                        

	# propagation to station 2
	muon_propagator_cfg = cms.PSet(
		    # Choice of matching algorithm
		useTrack = cms.string("tracker"),  # 'none' to use Candidate P4; or 'tracker ', 'muon', 'global'
		useState = cms.string("atVertex"), # 'innermost' and 'outermost' require the TrackExtra
		useSimpleGeometry = cms.bool(True), # just use a cylinder plus two disks.
		fallbackToME1 = cms.bool(False)    # If propagation to ME2 fails, propagate to ME1
	),

                                        
	# Processing Mode:
	# 0: PAT
	# 1: AOD Calo
	# 2: AOD PF
	ProcessingMode = cms.int32(processing_mode),

	primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
	#primaryVertexTag   = cms.untracked.InputTag("goodOfflinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD'),
	HLTriggerResults = cms.InputTag( 'TriggerResults'),
	MuonIDs	= cms.vstring("AllGlobalMuons","AllStandAloneMuons", "AllTrackerMuons"),
	ElectronIDs	= cms.vstring("simpleEleId70Run2011", "simpleEleId95Run2011", "simpleEleId70Run2010", "simpleEleId95Run2010"),
	TriggerList      = cms.vstring("HLT_Ele[2-3]\\\d+(?:(?!(No|Anti)BPTX|Tau|MT|MHT|Deta|SC17).)*","HLT_(Iso)?Mu([1-3]\\\d|9)_(?:(?!(Photon|Mu|Deta|MT|Ele|HT|MET|Track|Tk|Vertex|NoBPTX|AntiBPTX|Jpsi|Single|Tau)).)*"),
	VetoObjectTriggers = cms.vstring("HLT_.*Jet.*", "HLT_.*MET.*"),
	BTagAlgorithms	= cms.vstring(
#BTag algorithms for PAT::Jets
                                      "trackCountingHighEffBJetTags",
                                      "trackCountingHighPurBJetTags",
                                      "jetBProbabilityBJetTags",
                                      "jetProbabilityBJetTags",
                                      "softMuonByPtBJetTags",
                                      "softMuonByIP3dBJetTags",
                                      "softMuonBJetTags",
                                      "simpleSecondaryVertexHighEffBJetTags",
                                      "simpleSecondaryVertexHighPurBJetTags",
                                      "combinedSecondaryVertexMVABJetTags",
                                      "combinedSecondaryVertexBJetTags"
### BTag algorithms for PF or JPT jets, after rerunning btag
#				      "newTrackCountingHighEffBJetTags",
#				      "newTrackCountingHighPurBJetTags",
#				      "newJetBProbabilityBJetTags",
#      				      "newJetProbabilityBJetTags",
#				      "newSoftMuonByPtBJetTags",
#				      "newSoftMuonByIP3dBJetTags",
#				      "newSoftMuonBJetTags",
#				      "newSimpleSecondaryVertexHighEffBJetTags",
#				      "newSimpleSecondaryVertexHighPurBJetTags",
#				      "newCombinedSecondaryVertexMVABJetTags",
#				      "newCombinedSecondaryVertexBJetTags"
#				      #"softElectronByPtBJetTags", #excluded when rerunning btagging
				      #"softElectronByIP3dBJetTags"#if want to include, use dR matching in BTagAssociator
				      ),
                                        
        LeptonPFIsoConeSizes = cms.vstring("0.05","0.1","0.15","0.2","0.25","0.3","0.35","0.4","0.45","0.5","0.55","0.6"),
	FillTriggerObjects	= cms.bool(True),
	GetL1FromL1Extra	= cms.bool(False),
	PropagateMuToStation2	= cms.bool(True),
	EnablePartnerTrackFinder = cms.bool(True),	# write partnertrack information for conversion rejection in e channel jl 04.02.11: true
	WriteLooseMuons = cms.bool(False),
	WriteLooseElectrons = cms.bool(False),
	WriteMET	= cms.bool(True),
	WriteTriggerPrescales	= cms.bool(True),
	WritePDFEventWeights = cms.bool(False), #jl 04.02.11
	PDFWeights = cms.VInputTag("pdfWeights:cteq66"),
	Writed0wrtPV	= cms.bool(True),		# write d0 wrt PV
	EnableConvRej2012 = cms.bool(True),
	WriteGenParticles = cms.bool(False),		# write selected MC particles
	WriteWDecayInformation	= cms.bool(False),
	SelectedGenParticles = cms.vint32(11,13),	# pdgIds of MC particles to write
	SelectedGenParticlesMinPt = cms.double(1),	# min pt of MC particles to write
	SelectedGenParticlesMaxEta = cms.double(3.0),	# max eta of MC particles to write
	outfile		= cms.string("TestIsoConeflat.root")
)


#Stuff for rerunning btag
process.newJetTracksAssociatorAtVertex.jets = jet
process.newSoftElectronTagInfos.jets = jet
process.newSoftMuonTagInfos.jets = jet



process.display = cms.OutputModule("PoolOutputModule",
    fileName = cms.untracked.string('out_pfCaloTc.root'),
    outputCommands = cms.untracked.vstring(
        'keep *'
    )
)

process.p1 = cms.Path(
#	process.kt6PFJets# *
#	process.HBHENoiseFilter *
#	process.pdfWeights *
#	process.scrapingVeto *
#        process.metJESCorPFAK5 
#        process.display
#        process.metCorSequence
)




if rerunBTag:
    process.p1 += process.newBtaggingPath 

process.p1 += process.produceNTuples

process.schedule = cms.Schedule(process.p1)
