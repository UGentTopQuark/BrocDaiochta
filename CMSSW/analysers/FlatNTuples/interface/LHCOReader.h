#ifndef LHCO_READER_H
#define LHCO_READER_H

#include "NTupleWriter.h"
#include "LHCOEvent.h"
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <cmath>
#include <cstdlib>

class LHCOReader{
 public:
	LHCOReader(std::string infile, std::string outfile, int smearing_mode, int events);
	~LHCOReader();

	bool convert_input();

 private:

	ifstream lhcofile;
	
	NTupleWriter writer; 
	
	int events_passed_counter;
	int events_total_counter;

	int smearing_mode;
	int max_events;
};

#endif
