#ifndef EIRE_MUPROPAGATOR_H
#define EIRE_MUPROPAGATOR_H

#include <DataFormats/Common/interface/Ref.h>
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "DataFormats/GeometrySurface/interface/Cylinder.h"
#include "DataFormats/GeometrySurface/interface/Plane.h"
#include "TrackingTools/GeomPropagators/interface/Propagator.h"
#include "TrackingTools/Records/interface/TrackingComponentsRecord.h"
#include "TMath.h"

namespace beag{
	class MuPropagator{
		public:
			MuPropagator();
			~MuPropagator();
			inline double get_phi_endcap(){ return propagated_phi_endcap; };
			inline double get_eta_endcap(){ return propagated_eta_endcap; };
			inline double get_phi_barrel(){ return propagated_phi_barrel; };
			inline double get_eta_barrel(){ return propagated_eta_barrel; };

			void set_event_setup(edm::EventSetup const &setup);

			void propagate_track(reco::TrackRef inner_track);
		private:
			TrajectoryStateOnSurface cylExtrapTrkSam(reco::TrackRef track, double rho);
			TrajectoryStateOnSurface surfExtrapTrkSam(reco::TrackRef track, double z);
			FreeTrajectoryState freeTrajStateMuon(reco::TrackRef track);
			double propagated_phi_barrel;
			double propagated_phi_endcap;
			double propagated_eta_barrel;
			double propagated_eta_endcap;

			// The Magnetic field
			edm::ESHandle<MagneticField> theBField;

			// Extrapolator to cylinder
			edm::ESHandle<Propagator> propagatorAlong;
			edm::ESHandle<Propagator> propagatorOpposite;
	};
}

#endif
