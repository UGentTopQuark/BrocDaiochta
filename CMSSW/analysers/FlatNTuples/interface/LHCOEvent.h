#ifndef LHCOEVENT_H
#define LHCOEVENT_H

class LHCOEvent{
 public:
        LHCOEvent();
        ~LHCOEvent();

	double  e_index, e_evt, e_trigger;
	double  j1_index, j1_type, j1_eta, j1_phi, j1_pt, j1_jma, j1_ntracks, j1_btag, j1_hadem, j1_dummy1, j1_dummy2;
        double  j2_index, j2_type, j2_eta, j2_phi, j2_pt, j2_jma, j2_ntracks, j2_btag, j2_hadem, j2_dummy1, j2_dummy2;
        double  j3_index, j3_type, j3_eta, j3_phi, j3_pt, j3_jma, j3_ntracks, j3_btag, j3_hadem, j3_dummy1, j3_dummy2;
        double  j4_index, j4_type, j4_eta, j4_phi, j4_pt, j4_jma, j4_ntracks, j4_btag, j4_hadem, j4_dummy1, j4_dummy2;
        double  l_index, l_type, l_eta, l_phi, l_pt, l_jma, l_ntracks, l_btag, l_hadem, l_dummy1, l_dummy2;
        double  m_index, m_type, m_eta, m_phi, m_pt, m_jma, m_ntracks, m_btag, m_hadem, m_dummy1, m_dummy2;
	
	void clear();

 private:
	
};

#endif
