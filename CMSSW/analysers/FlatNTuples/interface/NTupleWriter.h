#ifndef NTUPLEWRITER_H
#define NTUPLEWRITER_H

#include "LHCOEvent.h"
#include "METCollection.h"
#include "JetCollection.h"
#include "ElectronCollection.h"
#include "MuonCollection.h"
#include "TriggerNameMapper.h"
#include "TagNameMapper.h"
#include "PrimaryVertexProducer.h"
#include "EventInformationProducer.h"
#include "ConversionCollection.h" 
#include "GenParticleProducer.h"
#include "TriggerProducer.h"
#include "TTbarGenEventProducer.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"

#include <boost/random.hpp>

#include "PartonSmearing.h"

class NTupleWriter{
 public:
	NTupleWriter(std::string ntuple_outfile, int smearing);
	~NTupleWriter();

	void book_branches();
	void fill_branches(const LHCOEvent &event);
	int fill_trees(const LHCOEvent &event);

	template <class BeagCollection>       
		void  fill_collection(std::vector<reco::Particle> p4_collection, BeagCollection *beag_objects);

	void fill_jet_collection(std::vector<reco::Particle> reco_p_collection, std::vector<reco::Particle> reco_p_collection_smeared, beag::JetCollection<pat::Jet> *beag_objects, int fill_mode);

	double get_random();
	int set_bdiscriminator(double pt, int jet_type, std::string working_point);


 private:
	
	TFile *outfile;
        TTree *tree;
        TTree *trigger_name_mapping;
        TTree *tag_name_mapping;

	beag::TagNameMapper *eID_mapper;
	beag::TagNameMapper *muID_mapper;
	beag::TagNameMapper *btag_mapper;
	beag::TagNameMapper *pfiso_conesize_mapper;
  
	beag::TriggerNameMapper *trigger_name_mapper;
	beag::TriggerNameMapper *veto_trigger_name_mapper;
	beag::EventInformationProducer *evt_info_prod;
	beag::PrimaryVertexProducer *pvertex_prod;

	beag::ConversionCollection *beag_conv;

	beag::METCollection<pat::MET> *beag_mets;
	beag::JetCollection<pat::Jet> *beag_jets;
	beag::ElectronCollection<pat::Electron> *beag_electrons;
	beag::MuonCollection<pat::Muon> *beag_muons;

	beag::Particle lepB;
	beag::Particle hadB;
	beag::Particle q;
	beag::Particle qbar;

	PartonSmearing parton_smearer;

	//std::vector<std::string> *selected_triggers;
	//std::vector<std::string> *vetoed_triggers;
	//std::vector<std::string> *valid_triggers;
	//std::vector<std::string> *selected_l1seeds;

	std::vector<std::string> *muonIDs;
	std::vector<std::string> *electronIDs;
	std::vector<std::string> *btag_algos;
	std::vector<std::string> *pfiso_conesizes;

	std::vector<std::string> triggers;
	std::vector<std::string> veto_triggers;
	beag::TriggerProducer *trigger_prod;
	beag::TriggerObjectFinder *trigger_obj_find;
	beag::GenParticleProducer *gen_part_prod;
	beag::TTbarGenEventProducer *gen_evt_prod;
	GenMatch *gen_match;

	double run, lumi_block, event_number, cms_energy, event_weight, rhoFastJet, rhoFastJetIso;
        bool trigger_changed, is_real_data;

	std::vector<double> btag_values;

	int smearing_mode;
	int event_iter; // debugging purposes

};

template <class BeagCollection>                                                                                              
void  NTupleWriter::fill_collection(std::vector<reco::Particle> p4_collection, BeagCollection *beag_objects)
{

	beag_objects->get_beag_objects()->clear();	

        //this->cmssw_handle = handle;

        for(typename std::vector<reco::Particle>::const_iterator p4_obj = p4_collection.begin();
	    p4_obj != p4_collection.end();
	    ++p4_obj){
		typename BeagCollection::BeagType *beag_obj = new typename BeagCollection::BeagType();

		(*beag_obj).charge = p4_obj->charge();

		(*beag_obj).eta = p4_obj->eta();
		(*beag_obj).phi = p4_obj->phi();
		(*beag_obj).mass = p4_obj->mass();
		(*beag_obj).pt = p4_obj->pt();

		(*beag_obj).mc_eta = p4_obj->eta();
		(*beag_obj).mc_phi = p4_obj->phi();
		(*beag_obj).mc_mass = p4_obj->mass();
		(*beag_obj).mc_pt = p4_obj->pt();

                //fill_common_vars(p4_obj, *beag_obj);
                //fill_special_vars(p4_obj, *beag_obj);

                //pt_sorter->add(beag_obj);
		beag_objects->get_beag_objects()->push_back(*beag_obj);
        }
	
}
#endif
