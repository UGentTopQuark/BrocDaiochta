#ifndef PARTONSMEARING_H
#define PARTONSMEARING_H

#include "TFile.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include "TF1.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <TString.h>
#include <fstream>
#include <string>
//#include "DataFormats/Candidate/interface/LeafCandidate.h"
#include "DataFormats/Math/interface/LorentzVector.h"
#include "DataFormats/PatCandidates/interface/Jet.h"

class PartonSmearing{
 public:
	PartonSmearing();
	~PartonSmearing();
	
	//ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > smear(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > smeared_part, int32_t type, int &status);
	reco::Particle::PolarLorentzVector smear(reco::Particle::PolarLorentzVector smeared_part, int32_t type, int &status);
	
	
 private:
	std::vector<std::string> smearing_parameters;
};

#endif
