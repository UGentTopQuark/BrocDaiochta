#ifndef BEAG_TTBARGENEVENTPRODUCER_H
#define BEAG_TTBARGENEVENTPRODUCER_H

#include "BeagObjects/TTbarGenEvent.h"
#include "GenMatch.h"
#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "DataFormats/Common/interface/View.h"
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"
#include "LHCOEvent.h"

namespace beag{
	class TTbarGenEventProducer{
		public:
			TTbarGenEventProducer(TTree *tree, TFile *outfile, std::string ident);
			~TTbarGenEventProducer();
			void fill_mc_information(edm::Handle<reco::GenParticleCollection> part_collection, const edm::Event & iEvent);
			void fill_mc_information_lhco(reco::Particle lhco_particle, std::string ident);
			beag::TTbarGenEvent* get_gen_event();
			inline void set_gen_match(GenMatch *gen_match) { this->gen_match = gen_match; };
		private:
			void fill_particle_p4(reco::GenParticle *reco_particle, beag::Particle &beag_particle);
			void fill_reco_p_lhco(reco::Particle lhco_particle, beag::Particle &beag_particle);
			std::vector<beag::TTbarGenEvent> *gen_event;
			TTree *tree;
			TFile *outfile;
			GenMatch *gen_match;
	};
}

#endif
