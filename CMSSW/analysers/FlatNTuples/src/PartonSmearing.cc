#include "../interface/PartonSmearing.h"

PartonSmearing::PartonSmearing()
{
}

PartonSmearing::~PartonSmearing()
{
}

// Define the simple Transfer Function
Double_t simpleTF(Double_t *v, Double_t *par){

	//  Double_t x = v[0];                                                                                                                                                                      
	Double_t y = par[10];
	Double_t d = v[0];


	Double_t a1 = par[0];
	Double_t b1 = par[1];
	Double_t p1 = a1 + b1*y;

	Double_t a2 = par[2];
	Double_t b2 = par[3];
	Double_t p2 = a2 + b2*y;

	Double_t a3 = par[4];
	Double_t b3 = par[5];
	Double_t p3 = a3 + b3*y;

	Double_t a4 = par[6];
	Double_t b4 = par[7];
	Double_t p4 = a4 + b4*y;

	Double_t a5 = par[8];
	Double_t b5 = par[9];
	Double_t p5 = a5 + b5*y;

	Double_t gaus1 = exp(-0.5 * pow((d-p1)/ p2, 2));
	Double_t gaus2 = exp(-0.5 * pow((d-p4)/ p5, 2));

	Double_t result = (1./((p2+p3*p5)*sqrt(2.*3.141592653589793)))*(gaus1 + p3*gaus2);

	return result ;
}

//ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > PartonSmearing::smear(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part, int32_t type, int &status)
reco::Particle::PolarLorentzVector PartonSmearing::smear(reco::Particle::PolarLorentzVector part, int32_t type, int &status)
{
	smearing_parameters = {"-1.99091","-0.0243212","2.89767","0.0886286","0.0618142","0.00103996","-14.7397","0.00169109","5.1975","0.166501","-0.453373","-0.00333984","3.34131","0.066153","0.577011","-0.000841599","-6.3921","0.114462","-0.480468","0.193721"};//(arr, arr+20);

	TF1 *smearF = new TF1("smearFunc", simpleTF, -500, 500, 11);
	//ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > smeared_part = part;
	reco::Particle::PolarLorentzVector smeared_part = part;
	smearF->SetParNames("a1", "b1","a2", "b2","a3", "b3","a4", "b4","a5", "b5","parton");
	// if(type == 1|| type == 2){ //select only jet                                                                                                                                                     
	// 	Double_t Eparton = part.E();
	// 	smearF->SetRange( -500.0, Eparton );
	// 	if( type == 1){//light jets 
	// 		smearF->SetParameters(atof(smearing_parameters[0].c_str()),atof(smearing_parameters[1].c_str()),atof(smearing_parameters[2].c_str()),atof(smearing_parameters[3].c_str()),atof(smearing_parameters[4].c_str()),atof(smearing_parameters[5].c_str()),atof(smearing_parameters[6].c_str()),atof(smearing_parameters[7].c_str()),atof(smearing_parameters[8].c_str()),atof(smearing_parameters[9].c_str()), Eparton); //light jets smearing
	// 	}else{//btag
	// 		smearF->SetParameters(atof(smearing_parameters[10].c_str()),atof(smearing_parameters[11].c_str()),atof(smearing_parameters[12].c_str()),atof(smearing_parameters[13].c_str()),atof(smearing_parameters[14].c_str()),atof(smearing_parameters[15].c_str()),atof(smearing_parameters[16].c_str()),atof(smearing_parameters[17].c_str()),atof(smearing_parameters[18].c_str()),atof(smearing_parameters[19].c_str()), Eparton); //bjets smearing 
	// 	}
	// 	smearF->SetParameters( -1.99091, -0.0243212,2.89767, 0.0886286, 0.0618142,0.00103996,-14.7397,0.00169109,5.1975,0.166501, Eparton);
	        
	// 	int itr_smear = 0;
	// 	Double_t Ejet = 0;
	// 	do{
	// 		Double_t Eshift = smearF->GetRandom();
	// 		//if (Ejet != Ejet){
	// 		//	std::cout << "nan found!" << std::endl;
	// 		//	status = -1;
	// 		//}
	// 		Ejet =  (Eparton - Eshift);
	// 		smeared_part.SetPt(sqrt( ( pow(Ejet,2) - pow(part.M(),2))/ (1+ pow(sinh(part.Eta()),2))));
	// 		itr_smear++;
	// 	}while ((smeared_part.Pt() != smeared_part.Pt()) && itr_smear < 11);
	// 	if (itr_smear > 1){
	// 		std::cout << "WARNING: Smearing parton with pt = " << Eparton << " produced a negative pt value on attempt no " << itr_smear-1 << "! Will retry. " << std::endl;
	// 	}
	// 	if (smeared_part.Pt() != smeared_part.Pt()){
	// 		std::cout << "WARNING: Negative square root found!" << std::endl;
	// 		status = -1;
	// 	}
	// 	return smeared_part;
			
	// }
	//std::cout << "no smearing applied" << std::endl;
	return part;
}
