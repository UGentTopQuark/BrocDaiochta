#include "../interface/NTupleWriter.h"

NTupleWriter::NTupleWriter(std::string ntuple_outfile, int smearing)
	:smearing_mode(smearing)
{

	//std::string filename = outfile;
	       
	outfile = new TFile(ntuple_outfile.c_str(),"RECREATE");
        outfile->cd();

        trigger_name_mapping = NULL;
        tag_name_mapping = NULL;
	
	tree = new TTree("tree","tree");
	trigger_name_mapping = new TTree("trigger_name_mapping","trigger_name_mapping");
	tag_name_mapping = new TTree("tag_name_mapping","tag_name_mapping");

        muonIDs = new std::vector<std::string>;
        electronIDs = new std::vector<std::string>;
        btag_algos = new std::vector<std::string>;
        pfiso_conesizes = new std::vector<std::string>;

        muonIDs->push_back("dummy1");
        muonIDs->push_back("dummy2");
        electronIDs->push_back("dummy1");
        electronIDs->push_back("dummy2");
        btag_algos->push_back("combinedSecondaryVertexBJetTags");
        btag_algos->push_back("combinedSecondaryVertexMVABJetTags");
        pfiso_conesizes->push_back("dummy1");
        //pfiso_conesizes->push_back("dummy2");
	triggers.push_back("PassAll");
	//triggers.push_back("dummy2");
        veto_triggers.push_back("dummy1");
	veto_triggers.push_back("dummy2");

	book_branches();

	parton_smearer = PartonSmearing();

}

NTupleWriter::~NTupleWriter()
{
	if(tree){ tree->Write(0, TObject::kOverwrite); delete tree; tree = NULL; }
	//if(tree){ tree->Write();} // delete tree; tree = NULL; }
	if(beag_mets){ delete beag_mets; beag_mets = NULL;}
	if(beag_jets){ delete beag_jets; beag_jets = NULL;}
	if(beag_muons){ delete beag_muons; beag_muons = NULL;}
	if(beag_electrons){ delete beag_electrons; beag_electrons = NULL;}
        if(trigger_name_mapping){
                trigger_name_mapping->Write();
                delete trigger_name_mapping;
                trigger_name_mapping = NULL;
        }
        if(tag_name_mapping){
                tag_name_mapping->Write();
                delete tag_name_mapping;
                tag_name_mapping = NULL;
        }
	if(gen_evt_prod){ delete gen_evt_prod; gen_evt_prod = NULL;}
        if(outfile){
                outfile->Write();
                outfile->Close();
                delete outfile;
                outfile = NULL;
        }

}


void NTupleWriter::book_branches()
{
	beag_mets = new beag::METCollection<pat::MET>(tree, outfile, "mets");
	beag_electrons = new beag::ElectronCollection<pat::Electron>(tree, outfile, "electrons");
	beag_muons = new beag::MuonCollection<pat::Muon>(tree, outfile, "muons");
	beag_jets = new beag::JetCollection<pat::Jet>(tree, outfile, "jets");
        
	trigger_prod = new beag::TriggerProducer(tree, outfile, "trigger");
        trigger_obj_find = new beag::TriggerObjectFinder(outfile);

	eID_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "eID_names");
        eID_mapper->set_tag_names(electronIDs);
        muID_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "muID_names");
        muID_mapper->set_tag_names(muonIDs);
        btag_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "btag_names");
        btag_mapper->set_tag_names(btag_algos);
	pfiso_conesize_mapper = new beag::TagNameMapper(tag_name_mapping, outfile, "pfiso_conesize_names");
        pfiso_conesize_mapper->set_tag_names(pfiso_conesizes);
	
	beag_conv = new beag::ConversionCollection(tree, outfile, "conversions");
        evt_info_prod = new beag::EventInformationProducer(tree, outfile, "evt_info");
        pvertex_prod = new beag::PrimaryVertexProducer(tree, outfile, "pvertices");
        trigger_name_mapper = new beag::TriggerNameMapper(trigger_name_mapping, outfile, "trigger_names", false, true);
        veto_trigger_name_mapper = new beag::TriggerNameMapper(trigger_name_mapping, outfile, "trigger_names", false, false);

	gen_match = new GenMatch();
	gen_evt_prod = new beag::TTbarGenEventProducer(tree, outfile, "gen_evt");
	gen_evt_prod->set_gen_match(gen_match);

	tag_name_mapping->Fill();
	trigger_name_mapping->Fill();

        trigger_name_mapper->set_trigger_expressions(triggers);
        veto_trigger_name_mapper->set_trigger_expressions(veto_triggers);

}

int NTupleWriter::fill_trees(const LHCOEvent &event)
{
	outfile->cd();
	std::vector<int> jet_fit_status;
	jet_fit_status.clear();

	// Declare the 4 collections used

	//std::vector<reco::Candidate::PolarLorentzVector> p4_mets;
	//std::vector<reco::Candidate::PolarLorentzVector> p4_jets;
	//std::vector<reco::Candidate::PolarLorentzVector> p4_muons;
	//std::vector<reco::Candidate::PolarLorentzVector> p4_electrons;

	std::vector<reco::Particle> reco_p_mets;
	std::vector<reco::Particle> reco_p_jets;
	std::vector<reco::Particle> reco_p_jets_smeared;
	std::vector<reco::Particle> reco_p_electrons;
	std::vector<reco::Particle> reco_p_muons;

	
	// Make a copy of the jets and smear them using the smearing service
	
	int status = 0;
	reco::Particle::PolarLorentzVector p4_jet1_smeared = parton_smearer.smear(reco::Particle::PolarLorentzVector(event.j1_pt,event.j1_eta,event.j1_phi,event.j1_jma), 2, status);
	jet_fit_status.push_back(status);
	status = 0;
	reco::Particle::PolarLorentzVector p4_jet2_smeared = parton_smearer.smear(reco::Particle::PolarLorentzVector(event.j2_pt,event.j2_eta,event.j2_phi,event.j2_jma), 1, status);
	jet_fit_status.push_back(status);
	status = 0;
	reco::Particle::PolarLorentzVector p4_jet3_smeared = parton_smearer.smear(reco::Particle::PolarLorentzVector(event.j3_pt,event.j3_eta,event.j3_phi,event.j3_jma), 1, status);
	jet_fit_status.push_back(status);
	status = 0;
	reco::Particle::PolarLorentzVector p4_jet4_smeared = parton_smearer.smear(reco::Particle::PolarLorentzVector(event.j4_pt,event.j4_eta,event.j4_phi,event.j4_jma), 2, status);
	jet_fit_status.push_back(status);

	std::vector<int> required_status = {0,0,0,0};
	if (jet_fit_status != required_status){
		std::cout << status << std::endl;
		return -1;
	}

	//Define the 4-vectors and fill them from the information provided via the LHCOEvent class

	reco::Particle reco_p_met(event.m_ntracks, reco::Particle::PolarLorentzVector(event.m_pt,event.m_eta,event.m_phi,event.m_jma));
	reco::Particle reco_p_electron(event.l_ntracks, reco::Particle::PolarLorentzVector(event.l_pt,event.l_eta,event.l_phi,event.l_jma)); 
	reco::Particle reco_p_muon(event.l_ntracks, reco::Particle::PolarLorentzVector(event.l_pt,event.l_eta,event.l_phi,event.l_jma));

	// Redefine the individual jets with the smeared 4-vectors if smearing is enabled

	reco::Particle reco_p_jet1_smeared(event.j1_ntracks, p4_jet1_smeared);
	reco::Particle reco_p_jet2_smeared(event.j2_ntracks, p4_jet2_smeared);
	reco::Particle reco_p_jet3_smeared(event.j3_ntracks, p4_jet3_smeared);
	reco::Particle reco_p_jet4_smeared(event.j4_ntracks, p4_jet4_smeared);

	reco::Particle reco_p_jet1(event.j1_ntracks, reco::Particle::PolarLorentzVector(event.j1_pt,event.j1_eta,event.j1_phi,event.j1_jma));
	reco::Particle reco_p_jet2(event.j2_ntracks, reco::Particle::PolarLorentzVector(event.j2_pt,event.j2_eta,event.j2_phi,event.j2_jma));
	reco::Particle reco_p_jet3(event.j3_ntracks, reco::Particle::PolarLorentzVector(event.j3_pt,event.j3_eta,event.j3_phi,event.j3_jma));
	reco::Particle reco_p_jet4(event.j4_ntracks, reco::Particle::PolarLorentzVector(event.j4_pt,event.j4_eta,event.j4_phi,event.j4_jma));


	// Fill the collection with an electron or muon

	if (event.l_type == 1){
		reco_p_electrons.push_back(reco_p_electron);
		fill_collection(reco_p_electrons, beag_electrons);
		gen_evt_prod->fill_mc_information_lhco(reco_p_electron,"lep");
	}else if (event.l_type == 2){	
		reco_p_muons.push_back(reco_p_muon);
		fill_collection(reco_p_muons, beag_muons);
		gen_evt_prod->fill_mc_information_lhco(reco_p_muon,"lep");
	}

	// Fill the MET collection and MC Truth with the neutrino

	reco_p_mets.push_back(reco_p_met);
	fill_collection(reco_p_mets, beag_mets);
	gen_evt_prod->fill_mc_information_lhco(reco_p_met,"neu");

	// Fill the smeared jet collection with the smeared jets if enabled. If not, fill with ordinary jets

	if (smearing_mode == 1 || smearing_mode == 3){
		reco_p_jets_smeared.push_back(reco_p_jet1_smeared);
		reco_p_jets_smeared.push_back(reco_p_jet2_smeared);
		reco_p_jets_smeared.push_back(reco_p_jet3_smeared);
		reco_p_jets_smeared.push_back(reco_p_jet4_smeared);
	}else{
		reco_p_jets_smeared.push_back(reco_p_jet1);
		reco_p_jets_smeared.push_back(reco_p_jet2);
		reco_p_jets_smeared.push_back(reco_p_jet3);
		reco_p_jets_smeared.push_back(reco_p_jet4);
	}

	reco_p_jets.push_back(reco_p_jet1);
	reco_p_jets.push_back(reco_p_jet2);
	reco_p_jets.push_back(reco_p_jet3);
	reco_p_jets.push_back(reco_p_jet4);

	// Fill the MC truth hadron types according to the process type (assumes one of two possibilities!)

	if (event.l_ntracks == -1){
		fill_jet_collection(reco_p_jets, reco_p_jets_smeared, beag_jets, -1);
		gen_evt_prod->fill_mc_information_lhco(reco_p_jet1,"hadB");
		gen_evt_prod->fill_mc_information_lhco(reco_p_jet2,"q");
		gen_evt_prod->fill_mc_information_lhco(reco_p_jet3,"qbar");
		gen_evt_prod->fill_mc_information_lhco(reco_p_jet4,"lepB");
	}else if (event.l_ntracks == 1){
		fill_jet_collection(reco_p_jets, reco_p_jets_smeared, beag_jets, 1);
		gen_evt_prod->fill_mc_information_lhco(reco_p_jet1,"lepB");
		gen_evt_prod->fill_mc_information_lhco(reco_p_jet2,"hadB");
		gen_evt_prod->fill_mc_information_lhco(reco_p_jet3,"q");
		gen_evt_prod->fill_mc_information_lhco(reco_p_jet4,"qbar");
	}

	// Fill the event information

	run = 1;
	lumi_block = 1;
	event_number = event.e_evt;
	cms_energy = 1;
	event_weight = 1;
	rhoFastJet = 1;
	rhoFastJetIso = 1;
	trigger_changed = 0;
	is_real_data = 0;

	evt_info_prod->fill_event_information(run, lumi_block, event_number, trigger_changed, cms_energy, event_weight, is_real_data, rhoFastJet, rhoFastJetIso);

	// Fill the collections to the tree

	status = 0;
	tree->Fill();
	status = 1;

	return status;
}

void  NTupleWriter::fill_jet_collection(std::vector<reco::Particle> reco_p_collection, std::vector<reco::Particle> reco_p_collection_smeared, beag::JetCollection<pat::Jet> *beag_objects, int fill_mode)
{
        beag_objects->get_beag_objects()->clear();

	std::string working_point = "CSVM";

        int jindex = 0;

	double b_discriminator;

	for (int i = 0; i<4; i++){

		beag::Jet *beag_obj = new beag::Jet();

		(*beag_obj).cmulti = reco_p_collection.at(i).charge();
                (*beag_obj).mc_eta = reco_p_collection.at(i).eta();
                (*beag_obj).mc_phi = reco_p_collection.at(i).phi();
                (*beag_obj).mc_mass = reco_p_collection.at(i).mass();
                (*beag_obj).mc_pt = reco_p_collection.at(i).pt();

                (*beag_obj).eta = reco_p_collection_smeared.at(i).eta();
                (*beag_obj).phi = reco_p_collection_smeared.at(i).phi();
                (*beag_obj).mass = reco_p_collection_smeared.at(i).mass();
                (*beag_obj).pt = reco_p_collection_smeared.at(i).pt();

		btag_values.clear();

		double jet_pt = reco_p_collection_smeared.at(i).pt();
		if (fill_mode == 0)
			(*beag_obj).ttbar_decay_product = 0;
		else if (fill_mode == -1){
			if (jindex == 0){
				b_discriminator = set_bdiscriminator(jet_pt, 1, working_point); 
				(*beag_obj).ttbar_decay_product = 3; // HadB
				btag_values.push_back(b_discriminator); // assumes first algo = CSV!
			}else if (jindex == 1){
				b_discriminator = set_bdiscriminator(jet_pt, 0, working_point);
				(*beag_obj).ttbar_decay_product = 1; // Q
				btag_values.push_back(b_discriminator); // assumes first algo = CSV!
			}else if (jindex == 2){
				b_discriminator = set_bdiscriminator(jet_pt, 0, working_point);
				(*beag_obj).ttbar_decay_product = 2; // QBar
				btag_values.push_back(b_discriminator); // assumes first algo = CSV!
			}else if (jindex == 3){
				b_discriminator = set_bdiscriminator(jet_pt, 1, working_point);
				(*beag_obj).ttbar_decay_product = 4; // LepB
				btag_values.push_back(b_discriminator); // assumes first algo = CSV!
			}
		}else if (fill_mode == 1){
			if (jindex == 0){
				b_discriminator = set_bdiscriminator(jet_pt, 1, working_point);
				(*beag_obj).ttbar_decay_product = 4; // LepB
				btag_values.push_back(b_discriminator); // assumes first algo = CSV!
			}else if (jindex == 1){
				b_discriminator = set_bdiscriminator(jet_pt, 1, working_point);
				(*beag_obj).ttbar_decay_product = 3; // HadB
				btag_values.push_back(b_discriminator); // assumes first algo = CSV!
			}else if (jindex == 2){
				b_discriminator = set_bdiscriminator(jet_pt, 0, working_point);
				(*beag_obj).ttbar_decay_product = 1; // Q
				btag_values.push_back(b_discriminator); // assumes first algo = CSV!
			}else if (jindex == 3){
				b_discriminator = set_bdiscriminator(jet_pt, 0, working_point);
				(*beag_obj).ttbar_decay_product = 2; // QBar
				btag_values.push_back(b_discriminator); // assumes first algo = CSV!
			}
		}


		(*beag_obj).btags = btag_values;
		//std::cout << "mass = " << (*beag_obj).mass << " | btag = " << btag_values.at(0) << std::endl;
		beag_objects->get_beag_objects()->push_back(*beag_obj);
		jindex++;
	}

}

// Returns a random double between 0 and 1 based on the Mersenne Twister
double NTupleWriter::get_random(){
	boost::random::mt19937 rng(1);
	static boost::random::uniform_01<boost::random::mt19937> dist(rng);
	return dist();
}

// Returns the bdiscriminator, based on efficiency and mistag rates provided on the twiki page:
// https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagPOG#2011_Data_and_MC
int NTupleWriter::set_bdiscriminator(double pt, int jet_type, std::string working_point){

	int b_discriminator = 0;
	double hj_tag_eff = 1;
	double lj_mistag_eff = 0;
	double x = 0;

	// Set the efficiency based on the working point and/or jet pt.
	if (working_point == "CSVM"){
		x = 0.670;
		lj_mistag_eff = (0.013595+(0.000104538*pt))+(-1.36087e-08*(pt*pt));
	}
	else if (working_point == "CSVT"){
		x = 0.898;
		lj_mistag_eff = 0.00315116*(((1+(-0.00769281*pt))+(2.58066e-05*(pt*pt)))+(-2.02149e-08*(pt*(pt*pt))));
	}
	hj_tag_eff = -1.73338329789*x*x*x*x +  1.26161794785*x*x*x +  0.784721653518*x*x +  -1.03328577451*x +  1.04305075822; // Taken from ttbar MC

	// B-Jets
	if (jet_type == 1){
		if(smearing_mode == 2 || smearing_mode == 3){
			// Set the heavy-quark b-discriminator to have a value of 1 consistent with the b-tag efficiency
			if (get_random() > (1-hj_tag_eff)){
				b_discriminator = 1;
			}else{
				b_discriminator = 0;
			}
			//std::cout << b_discriminator << std::endl;
		}else{ b_discriminator = 1; }
		
	// Light-Jets
	}else if (jet_type == 0){
		if(smearing_mode == 2 || smearing_mode == 3){
			// Set the light-quark b-discriminator to have a value of 1 consistent with the mis-tag rate
			if (get_random() < (lj_mistag_eff)){
				b_discriminator = 1;
			}else{
				b_discriminator = 0;
			}
			//std::cout << b_discriminator << std::endl;
		}else{ b_discriminator = 0; }
	}
        return b_discriminator;
}
