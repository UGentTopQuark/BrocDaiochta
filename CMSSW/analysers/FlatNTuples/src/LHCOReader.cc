#include "../interface/LHCOReader.h"

LHCOReader::LHCOReader(std::string infile, std::string outfile, int smearing_mode, int events)
	:lhcofile(infile.c_str())
	,max_events(events)
	,writer(outfile, smearing_mode)
{
	
}


LHCOReader::~LHCOReader()
{
	lhcofile.close();
	
	// close the writer here!
}


bool LHCOReader::convert_input(){


	float events = 0; float muons = 0; float electrons = 0;
	float acceptedmuons = 0; float acceptedelectrons = 0;   float acceptedevents = 0;

	//Open input file
	std::string  s_index, s_type, s_eta, s_phi, s_pt, s_jma, s_ntracks, s_btag, s_hadem, s_dummy1, s_dummy2;
	std::string  e_index, e_evt, e_trigger;
	std::string  obj_index, obj_type, obj_eta, obj_phi, obj_pt, obj_jma, obj_ntracks, obj_btag, obj_hadem, obj_dummy1, obj_dummy2;
	std::string  j1_index, j1_type, j1_eta, j1_phi, j1_pt, j1_jma, j1_ntracks, j1_btag, j1_hadem, j1_dummy1, j1_dummy2;
	std::string  j2_index, j2_type, j2_eta, j2_phi, j2_pt, j2_jma, j2_ntracks, j2_btag, j2_hadem, j2_dummy1, j2_dummy2;
	std::string  j3_index, j3_type, j3_eta, j3_phi, j3_pt, j3_jma, j3_ntracks, j3_btag, j3_hadem, j3_dummy1, j3_dummy2;
	std::string  j4_index, j4_type, j4_eta, j4_phi, j4_pt, j4_jma, j4_ntracks, j4_btag, j4_hadem, j4_dummy1, j4_dummy2;
	std::string  l_index, l_type, l_eta, l_phi, l_pt, l_jma, l_ntracks, l_btag, l_hadem, l_dummy1, l_dummy2;
	std::string  m_index, m_type, m_eta, m_phi, m_pt, m_jma, m_ntracks, m_btag, m_hadem, m_dummy1, m_dummy2;

	Float_t sample, mass, jes;
	Int_t acc;

	LHCOEvent event;

	events_passed_counter = 0;
	int status = 0;

	if ( lhcofile.is_open() ) {
		while ( lhcofile>>s_index && (events < max_events || max_events == -1)){			
			if(s_index == "#" ){

				event.clear();

				events++;
				//      int flag ;
				//title row
				lhcofile>>s_type;  lhcofile>>s_eta;   lhcofile>>s_phi;  lhcofile>>s_pt;  lhcofile>>s_jma;        
				lhcofile>>s_ntracks;     lhcofile>>s_btag;  lhcofile>>s_hadem; lhcofile>>s_dummy1; lhcofile>>s_dummy2;

				//event row
				lhcofile>>e_index;  lhcofile>>e_evt;  lhcofile>>e_trigger;
				event.e_index=atof(e_index.c_str()); event.e_evt=atof(e_evt.c_str()); event.e_trigger=atof(e_trigger.c_str());
				Int_t jet_index = 0;
				Int_t met_index = 0;
				Int_t lepton_index = 0;
				Int_t obj_iterator = 0;
				bool bad_event = 0;
				//while((jet_index!=4 || met_index!=1 || lepton_index!=1) || obj_iterator!=6){
				while(obj_iterator!=6){
					lhcofile>>obj_index;  lhcofile>>obj_type;  lhcofile>>obj_eta;   lhcofile>>obj_phi;  lhcofile>>obj_pt;  lhcofile>>obj_jma;    
					lhcofile>>obj_ntracks;       lhcofile>>obj_btag;  lhcofile>>obj_hadem; lhcofile>>obj_dummy1; lhcofile>>obj_dummy2;
					
					if(atof(obj_type.c_str())==4){
						if(jet_index==0){
							event.j1_index=atof(obj_index.c_str());  event.j1_type=atof(obj_type.c_str());  event.j1_eta=atof(obj_eta.c_str());   event.j1_phi=atof(obj_phi.c_str());  event.j1_pt=atof(obj_pt.c_str());  event.j1_jma=atof(obj_jma.c_str());
							event.j1_ntracks=atof(obj_ntracks.c_str());    event.j1_btag=atof(obj_btag.c_str());  event.j1_hadem=atof(obj_hadem.c_str()); event.j1_dummy1=atof(obj_dummy1.c_str()); event.j1_dummy2=atof(obj_dummy2.c_str());
							jet_index++;}
						else{
							if(jet_index==1){
								event.j2_index=atof(obj_index.c_str());  event.j2_type=atof(obj_type.c_str());  event.j2_eta=atof(obj_eta.c_str());   event.j2_phi=atof(obj_phi.c_str());  event.j2_pt=atof(obj_pt.c_str());  event.j2_jma=atof(obj_jma.c_str());
								event.j2_ntracks=atof(obj_ntracks.c_str());    event.j2_btag=atof(obj_btag.c_str());  event.j2_hadem=atof(obj_hadem.c_str()); event.j2_dummy1=atof(obj_dummy1.c_str()); event.j2_dummy2=atof(obj_dummy2.c_str());
								jet_index++;}
							else{
								if(jet_index==2){
									event.j3_index=atof(obj_index.c_str());  event.j3_type=atof(obj_type.c_str());  event.j3_eta=atof(obj_eta.c_str());   event.j3_phi=atof(obj_phi.c_str());  event.j3_pt=atof(obj_pt.c_str());  event.j3_jma=atof(obj_jma.c_str());
									event.j3_ntracks=atof(obj_ntracks.c_str());    event.j3_btag=atof(obj_btag.c_str());  event.j3_hadem=atof(obj_hadem.c_str()); event.j3_dummy1=atof(obj_dummy1.c_str()); event.j3_dummy2=atof(obj_dummy2.c_str());
									jet_index++;}
								else{
									if(jet_index==3){
										event.j4_index=atof(obj_index.c_str());  event.j4_type=atof(obj_type.c_str());  event.j4_eta=atof(obj_eta.c_str());   event.j4_phi=atof(obj_phi.c_str());  event.j4_pt=atof(obj_pt.c_str());  event.j4_jma=atof(obj_jma.c_str());
										event.j4_ntracks=atof(obj_ntracks.c_str());    event.j4_btag=atof(obj_btag.c_str());  event.j4_hadem=atof(obj_hadem.c_str()); event.j4_dummy1=atof(obj_dummy1.c_str()); event.j4_dummy2=atof(obj_dummy2.c_str());
										jet_index++;}
									else{jet_index++;}
								}
							}
						}
					}
					if (jet_index == 5){
						std::cerr << "WARNING: Event " << event.e_evt << " has > 4 Jets!  Cutting event... " << std::endl;
						bad_event = 1;
					}
					if(atof(obj_type.c_str()) == 2 || atof(obj_type.c_str()) == 1){
						event.l_index=atof(obj_index.c_str());  event.l_type=atof(obj_type.c_str());  event.l_eta=atof(obj_eta.c_str());   event.l_phi=atof(obj_phi.c_str());  event.l_pt=atof(obj_pt.c_str());  event.l_jma=atof(obj_jma.c_str());
						event.l_ntracks=atof(obj_ntracks.c_str());    event.l_btag=atof(obj_btag.c_str());  event.l_hadem=atof(obj_hadem.c_str()); event.l_dummy1=atof(obj_dummy1.c_str()); event.l_dummy2=atof(obj_dummy2.c_str());
						lepton_index++;}
					if(atof(obj_type.c_str()) == 6){
						event.m_index=atof(obj_index.c_str());  event.m_type=atof(obj_type.c_str());  event.m_eta=atof(obj_eta.c_str());   event.m_phi=atof(obj_phi.c_str());  event.m_pt=atof(obj_pt.c_str());  event.m_jma=atof(obj_jma.c_str());
						event.m_ntracks=atof(obj_ntracks.c_str());    event.m_btag=atof(obj_btag.c_str());  event.m_hadem=atof(obj_hadem.c_str()); event.m_dummy1=atof(obj_dummy1.c_str()); event.m_dummy2=atof(obj_dummy2.c_str());
						met_index++;}
					obj_iterator++;
				} // end object loop

				//check process type
				if (event.l_type == 2){
					muons++;
				}else if(event.l_type == 1){
					electrons++;
				}

				if (!bad_event)
					status += writer.fill_trees(event);

				//std::cout << status << std::endl;
				//if (status != -1){
				//	events_passed_counter++;
				//}

				//if (status != 0){
				//	std::cout << status << std::endl;
				//}
				
			}else{
				std::cout << "[ERROR] First line is NOT:   # typ eta phi pt jmass  ntrk  btag  had/em   dummy  dummy" << std::endl;
				std::cout << "Exiting!" << std::endl;
				return false;

			} // #if index == #

		} // while ( lhcofile>>s_index){
		std::cout << "--------------------------------------------------" << std::endl;
		std::cout << " no. electron events:    " << electrons               << std::endl;
		std::cout << " no. muon events:        " << muons                   << std::endl;
		std::cout << " total no. events:       " << events                  << std::endl;
		// if (events_passed_counter != events){
		// 	if (events - events_passed_counter == 1){
		// 		std::cout << "WARNING: " << (events - events_passed_counter) << " event did not successfully smear!" << std::endl;
		// 	}else{
		// 		std::cout << "WARNING: " << (events - events_passed_counter) << " events were not successfully smeared!" << std::endl;
		// 	}
		// 	std::cout << " events passed:        " << events_passed_counter   << std::endl;
		// }
		std::cout << " events filled to tree = " << status << std::endl;
		std::cout << "--------------------------------------------------" << std::endl;
		std::cout << " \n";

		
		return true;
	} // if ( lhcofile.is_open() ) {
	//else 
	return false;
}
