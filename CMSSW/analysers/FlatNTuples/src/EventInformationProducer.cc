#include "../interface/EventInformationProducer.h"

beag::EventInformationProducer::EventInformationProducer(TTree *tree, TFile *outfile, std::string ident)
{
	this->tree = tree;
	this->outfile = outfile;

	W_decay_info = new WDecayModeAnalyser();

	event_info = new std::vector<beag::EventInformation>();
	event_info->push_back(beag::EventInformation());
	tree->Bronch(ident.c_str(),"std::vector<beag::EventInformation>",&event_info);
}

beag::EventInformationProducer::~EventInformationProducer()
{
	if(event_info){ delete event_info; event_info = NULL; }
	if(W_decay_info){ delete W_decay_info; W_decay_info = NULL; }
}

void beag::EventInformationProducer::fill_event_information(double run, double lumi_block, double event_number, bool trigger_changed, double cms_energy, double event_weight, bool is_real_data, double rhoFastJet, double rhoFastJetIso)
{
	event_info->begin()->run = run;
	event_info->begin()->lumi_block = lumi_block;
	event_info->begin()->event_number = event_number;
	event_info->begin()->trigger_changed = trigger_changed;
	event_info->begin()->event_weight = event_weight;

	event_info->begin()->is_real_data = is_real_data;
	event_info->begin()->rhoFastJet = rhoFastJet;
	event_info->begin()->rhoFastJetIso = rhoFastJetIso;

	event_info->begin()->W_decay_mode = 0;
}

void beag::EventInformationProducer::fill_pu_info(edm::Handle<std::vector<PileupSummaryInfo> > pu_info)
{
	event_info->begin()->pu_info.clear();

	std::vector<PileupSummaryInfo>::const_iterator PVI;
	
	for(PVI = pu_info->begin(); PVI != pu_info->end(); ++PVI) {
		// std::cout << " Pileup Information: bunchXing, nvtx: " << PVI->getBunchCrossing() << " " << PVI->getPU_NumInteractions() << std::endl;

		event_info->begin()->pu_info.push_back(std::pair<int,double>(PVI->getBunchCrossing(), PVI->getPU_NumInteractions()));
		if(PVI->getBunchCrossing() == 0) { 
			event_info->begin()->true_pu = PVI->getTrueNumInteractions();
		}
	}
}

void beag::EventInformationProducer::fill_W_decay_info(edm::Handle<reco::GenParticleCollection> part_collection)
{
	W_decay_info->fill_events_from_collection(part_collection);
	event_info->begin()->W_decay_mode = W_decay_info->get_decay_mode();
}
