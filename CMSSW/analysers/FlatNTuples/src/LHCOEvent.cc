#include "../interface/LHCOEvent.h"

LHCOEvent::LHCOEvent()
{
	clear();
}

LHCOEvent::~LHCOEvent()
{
}

void LHCOEvent::clear()
{
	e_index = -1;
	e_evt = -1;
	e_trigger = -1;

        j1_index = -1;
	j1_type = -1;
	j1_eta = -1;
	j1_phi = -1;
	j1_pt = -1;
	j1_jma = -1;
	j1_ntracks = -1;
	j1_btag = -1;
	j1_hadem = -1;
	j1_dummy1 = -1;
	j1_dummy2 = -1;
	
	j2_index = -1;
	j2_type = -1;
	j2_eta = -1;
	j2_phi = -1;
	j2_pt = -1;
	j2_jma = -1;
	j2_ntracks = -1;
	j2_btag = -1;
	j2_hadem = -1;
	j2_dummy1 = -1;
	j2_dummy2 = -1;
	
	j3_index = -1;
	j3_type = -1;
	j3_eta = -1;
	j3_phi = -1;
	j3_pt = -1;
	j3_jma = -1;
	j3_ntracks = -1;
	j3_btag = -1;
	j3_hadem = -1;
	j3_dummy1 = -1;
	j3_dummy2 = -1;
	
	j4_index = -1;
	j4_type = -1;
	j4_eta = -1;
	j4_phi = -1;
	j4_pt = -1;
	j4_jma = -1;
	j4_ntracks = 0;
	j4_btag = -1;
	j4_hadem = -1;
	j4_dummy1 = -1;
	j4_dummy2 = -1;
	
	l_index = -1;
	l_type = -1;
	l_eta = -1;
	l_phi = -1;
	l_pt = -1;
	l_jma = -1;
	l_ntracks = -1;
	l_btag = -1;
	l_hadem = -1;
	l_dummy1 = -1;
	l_dummy2 = -1;
	
	m_index = -1;
	m_type = -1;
	m_eta = -1;
	m_phi = -1;
	m_pt = -1;
	m_jma = -1;
	m_ntracks = -1;
	m_btag = -1;
	m_hadem = -1;
	m_dummy1 = -1;
	m_dummy2 = -1;
	
}



