#!/usr/bin/env python
from optparse import OptionParser
import ConfigParser, os, sys, re, time, subprocess
import ctypes
import glob as glob

print "-----------------------------------------------------"
print "---------- LHCO Interface to FlatNTuples  -----------"
print "------------ author: Joseph McCartin ----------------"
print "-----------------------------------------------------"
print ""

use_smearing = 0
max_events = -1

infile = []
outfile = []
files_to_read = []

# Define the options that are taken at runtime
parser = OptionParser()
parser.add_option("-i", "--infile", dest="infile", help="specify the input lhco file")
parser.add_option("-o", "--outfile", dest="outfile", help="specify the output ntuple file")
parser.add_option("-x", "--eXecute", dest="execute", help="define config to run")
parser.add_option("-e", "--events_to_run", dest="max_events", help="number of events to run")
parser.add_option("-s", "--smearing_mode", dest="smearing_mode", help="Smearing mode to use (0 - No smearing, 1 - Pt smearing, 2 - B-tag smearing, 3 - Pt and B-tag smearing)")

(options, args) = parser.parse_args()

# If runnning with configuration file as an argument, append the files to run over to a list and get the configuration parameters
if options.execute:
        config_name = options.execute
	if not os.path.exists(config_name):
		print "cannot find configuation:", config_name, ", exiting..."
		sys.exit()

	config = ConfigParser.RawConfigParser()
	config.read(config_name)
	
	lhco_dir = config.get('default', 'lhco_dir').split(":")
	output_dir = config.get('default', 'output_dir')
	smearing_mode = config.get('default', 'smearing_mode')
	max_events = config.get('default', 'max_events')
	
	files_to_read = glob.glob( os.path.join(lhco_dir[0], '*.lhco') )		
	
	print "files to process:"
	for i in files_to_read:
		if (i is not files_to_read[-1]):
			print i + ",",
		else:
			print i
			
			print ""

# In case both the infile and outfile are specified as arguments, get the full system path and clean the input 
if (options.infile and options.outfile):
	files_to_read = (os.popen("readlink -f "+str(options.infile)+" | egrep .lhco").readlines())
	outfile = (os.popen("readlink -f "+str(options.outfile)+" | egrep .root").readlines())[0].strip('\n')

if not ((options.infile and options.outfile) or options.execute):
	print "no input/output files or valid configuration specified!"
	sys.exit()

if options.smearing_mode:
	smearing_mode = options.smearing_mode

if options.max_events:
	max_events = options.max_events

# Look for the shared library to load
print "Looking for libanalysersLHCOInterface_plugins.so in your $CMSSW_BASE path.......",
cmssw_lib = os.popen("find $CMSSW_BASE/lib/ | egrep libanalysersLHCOInterface_plugins.so").readlines()
print "done"
if (len(cmssw_lib) != 1):
    "Error - multiple or no libraries found."
    sys.exit()

# Load the library, it should be able to find it from the $CMSSW_BASE path if all works well!
print "Loading LHCOInterface library via ctypes........................................",
lhco_interface = ctypes.CDLL((cmssw_lib[0].strip(' ')).strip('\n'), 0x100 | 0x2)
print "done"
print ""

# Loop over the input files and call the main for each one
for i in range(0, len(files_to_read)):
	# Clear up the input strings and make an output file of the same name
	if not options.execute:
		infile = re.findall(r"^(?!lhco).*", files_to_read[i].strip('\n'))
		infile_cleaned = re.findall(r"[^\/]{0,99}\.lhco", infile[0])

	if options.execute:
		infile = re.findall(r"^(?!lhco).*", files_to_read[i])
		infile_cleaned = re.findall(r"[^\/]{0,99}\.lhco", infile[0])
		outfile = str(output_dir)+str(infile_cleaned[0])[:-5] + str(".root")

	argv = [infile[0], outfile, str(smearing_mode), str(max_events)]
	
	# Define the call to the main function in the LHCOInterface_plugins library that was loaded earlier
	def call_main(L):
		arr = (ctypes.c_char_p * len(L))()
		arr[:] = L
		lhco_interface.main(len(L), arr)

	# Call the main
	if __name__=="__main__":
		call_main(argv)
	
