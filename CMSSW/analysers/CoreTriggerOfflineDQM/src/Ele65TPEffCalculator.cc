#include "../interface/Ele65TPEffCalculator.h"

namespace tpg{
	tpg::Ele65TPEffCalculator::Ele65TPEffCalculator(TFile *outfile, std::string unversioned_trigger_name) : tpg::HLTEffCalculator(outfile,unversioned_trigger_name)
{
	e_selector = new ElectronSelector();
	dilep_reconstructor = new DiLeptonReconstructor();
	dilep_reconstructor->set_mass_window(76.0,106.0);

	outfile->cd();

	pt_hname = unv_trigger_name+"_TP"+pt_hname;
	eta_hname = unv_trigger_name+"_TP"+eta_hname;

	histos_1D[pt_hname+denom_suffix] = new TH1F((pt_hname+denom_suffix).c_str(),"pt of selected offline electrons", 200, 0, 200);
	histos_1D[eta_hname+denom_suffix] = new TH1F((eta_hname+denom_suffix).c_str(),"eta of selected offline electrons", 50, -2.5, 2.5);
	histos_1D[pt_hname+l3num_suffix] = new TH1F((pt_hname+l3num_suffix).c_str(),"pt of selected offline electrons", 200, 0, 200);
	histos_1D[eta_hname+l3num_suffix] = new TH1F((eta_hname+l3num_suffix).c_str(),"eta of selected offline electrons", 50, -2.5, 2.5);
	histos_1D[pt_hname+l1num_suffix] = new TH1F((pt_hname+l1num_suffix).c_str(),"pt of selected offline electrons", 200, 0, 200);
	histos_1D[eta_hname+l1num_suffix] = new TH1F((eta_hname+l1num_suffix).c_str(),"eta of selected offline electrons", 50, -2.5, 2.5);
}

tpg::Ele65TPEffCalculator::~Ele65TPEffCalculator()
{
	if(e_selector){
		delete e_selector;
		e_selector = NULL;
	}
	if(dilep_reconstructor){
		delete dilep_reconstructor;
		dilep_reconstructor = NULL;
	}
}


void tpg::Ele65TPEffCalculator::calculate_efficiency()
{
	if(!handle_holder){
		std::cerr<<"Ele65TPEffCalc: NULL handle_holder"<<std::endl;
		return;
	}

	std::string l1_module = "hltL1sL1SingleEG20";
	std::string l3_module = "hltEle65CaloIdVTTrkIdTDphiFilter";
	double eta_threshold = 2.5; //to fill pt plots
	double pt_threshold = 70.0; //to fill eta plots
	double max_l3_dR = 0.2;//trigger object matching
	double max_l1_dR = 0.3;

	//Select good isolated electrons. WP80
	e_selector->set_handle_holder(handle_holder);
	e_selector->set_min_pt(15.);

	e_selector->set_max_hadronicOverEmEB(0.04);
	e_selector->set_max_deltaPhiSuperClusterTrackAtVtxEB(0.06);
	e_selector->set_max_deltaEtaSuperClusterTrackAtVtxEB(0.004);
	e_selector->set_max_sigmaIetaIetaEB(0.01);
	e_selector->set_max_ecal_relIsoEB(0.07);
	e_selector->set_max_hcal_relIsoEB(0.10);
	e_selector->set_max_tk_relIsoEB(0.09);
	
	e_selector->set_max_hadronicOverEmEE(0.025);
	e_selector->set_max_deltaPhiSuperClusterTrackAtVtxEE(0.03);
	e_selector->set_max_deltaEtaSuperClusterTrackAtVtxEE(0.007);
	e_selector->set_max_sigmaIetaIetaEE(0.03);
	e_selector->set_max_ecal_relIsoEE(0.05);
	e_selector->set_max_hcal_relIsoEE(0.025);
	e_selector->set_max_tk_relIsoEE(0.04);

	std::vector<reco::GsfElectron> *selected_electrons = e_selector->get_selected_electrons();
	if(selected_electrons->size() < 2){return;}	

	std::vector<reco::GsfElectron> *loose_electron_leg = new std::vector<reco::GsfElectron>();
	std::vector<reco::GsfElectron> *tight_electron_leg = new std::vector<reco::GsfElectron>();
        if((dilep_reconstructor->reconstruct_candidates<reco::GsfElectron>(selected_electrons,selected_electrons,loose_electron_leg,tight_electron_leg)) != 1){
		return;
	}
	reco::GsfElectron tight_electron = (*tight_electron_leg)[0];

	//Only fill loose lepton information id tight lepton passes trigger
        if(trigger_object_matcher->match_trigger_objects<reco::GsfElectron>(tight_electron,l1_module,max_l1_dR) &&
	   trigger_object_matcher->match_trigger_objects<reco::GsfElectron>(tight_electron,l3_module,max_l3_dR)){
		reco::GsfElectron loose_electron = (*loose_electron_leg)[0];

		if(fabs(loose_electron.eta()) < eta_threshold){
			histos_1D[pt_hname+denom_suffix]->Fill(loose_electron.pt());
		}
		if(loose_electron.pt() > pt_threshold){
			histos_1D[eta_hname+denom_suffix]->Fill(loose_electron.eta());
		}
	
		if(trigger_object_matcher->match_trigger_objects<reco::GsfElectron>(loose_electron,l1_module,max_l1_dR)){	
			if(fabs(loose_electron.eta()) < eta_threshold){
				histos_1D[pt_hname+l1num_suffix]->Fill(loose_electron.pt());
			}
			if(loose_electron.pt() > pt_threshold){
				histos_1D[eta_hname+l1num_suffix]->Fill(loose_electron.eta());
			}  
		
			//Match objects from l3 module to selected loose_electrons passing l1.
			if(trigger_object_matcher->match_trigger_objects<reco::GsfElectron>(loose_electron,l3_module,max_l3_dR)){
				if(fabs(loose_electron.eta()) < eta_threshold){
					histos_1D[pt_hname+l3num_suffix]->Fill(loose_electron.pt());
				}
				if(loose_electron.pt() > pt_threshold){
					histos_1D[eta_hname+l3num_suffix]->Fill(loose_electron.eta());
				}
			}
		}
	}

}
}//namespace
