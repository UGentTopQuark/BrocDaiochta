#include "../interface/ElectronSelector.h"

tpg::ElectronSelector::ElectronSelector()
{
	max_relIso = -1;

	max_dr03TkSumPtEB = -1;
	max_dr03EcalRecHitSumEtEB = -1;
	max_dr03HcalTowerSumEtEB = -1;
	max_hadronicOverEmEB = -1;
	max_deltaPhiSuperClusterTrackAtVtxEB = -1;
	max_deltaEtaSuperClusterTrackAtVtxEB = -1;
	max_sigmaIetaIetaEB = -1;
	min_sigmaIetaIetaSpikesEB = -1;
	max_ecal_relIsoEB = -1; 
	max_hcal_relIsoEB = -1; 
	max_tk_relIsoEB = -1; 
	
	max_dr03TkSumPtEE = -1;
	max_dr03EcalRecHitSumEtEE = -1;
	max_dr03HcalTowerSumEtEE = -1;
	max_hadronicOverEmEE = -1;
	max_deltaPhiSuperClusterTrackAtVtxEE = -1;
	max_deltaEtaSuperClusterTrackAtVtxEE = -1;
	max_sigmaIetaIetaEE = -1;
	min_sigmaIetaIetaSpikesEE = -1;
	max_ecal_relIsoEE = -1; 
	max_hcal_relIsoEE = -1; 
	max_tk_relIsoEE = -1; 
	
	selected_electrons = new std::vector<reco::GsfElectron>();
}

tpg::ElectronSelector::~ElectronSelector()
{
	if(selected_electrons){
		delete selected_electrons;
		selected_electrons = NULL;
	}
}

std::vector<reco::GsfElectron>* tpg::ElectronSelector::get_selected_electrons()
{
	selected_electrons->clear();
	if(handle_holder == NULL){
		std::cerr<<"ElectronSelector: NULL handle_holder" << std::endl;
		return selected_electrons;
	}

	edm::Handle<edm::View<reco::GsfElectron> > electrons = handle_holder->get_electronHandle();
	for(edm::View<reco::GsfElectron>::const_iterator electron = electrons->begin();
	    electron != electrons->end();electron++){

		if(is_good_electron(*electron)){
			selected_electrons->push_back(*electron);
		}
	}

	return selected_electrons;
}
					 
bool tpg::ElectronSelector::is_good_electron(reco::GsfElectron electron)
{	
	if(min_pt != -1 && electron.pt() < min_pt){
		return false;
	}
	if(max_eta != -1 && fabs(electron.eta()) > max_eta){
		return false;
	}

	if(!electron.ecalDriven()){ return false;}

	double rel_ecal_iso = electron.dr03EcalRecHitSumEt()/electron.et();
	double rel_hcal_iso = electron.dr03HcalTowerSumEt()/electron.et();
	double rel_tk_iso = electron.dr03TkSumPt()/electron.et();
	
	if(electron.isEB()) {
		if ((max_dr03TkSumPtEB != -1 && electron.dr03TkSumPt() >= max_dr03TkSumPtEB) ||   
		    (max_dr03EcalRecHitSumEtEB != -1 && electron.dr03EcalRecHitSumEt() >= max_dr03EcalRecHitSumEtEB) || 
		    (max_dr03HcalTowerSumEtEB != -1 && electron.dr03HcalTowerSumEt()  >= max_dr03HcalTowerSumEtEB) ||
		    (max_hadronicOverEmEB != -1 && electron.hadronicOverEm() >= max_hadronicOverEmEB) ||
		    (max_deltaPhiSuperClusterTrackAtVtxEB != -1 && 
		     fabs(electron.deltaPhiSuperClusterTrackAtVtx()) >= max_deltaPhiSuperClusterTrackAtVtxEB) ||
		    (max_deltaEtaSuperClusterTrackAtVtxEB != -1 &&
		     fabs(electron.deltaEtaSuperClusterTrackAtVtx()) >=  max_deltaEtaSuperClusterTrackAtVtxEB) ||
		    (max_sigmaIetaIetaEB != -1 && electron.sigmaIetaIeta() >= max_sigmaIetaIetaEB) ||
		    (min_sigmaIetaIetaSpikesEB != -1 && electron.sigmaIetaIeta() <= min_sigmaIetaIetaSpikesEB) ||
		    (max_ecal_relIsoEB != -1 && rel_ecal_iso >= max_ecal_relIsoEB) ||
		    (max_hcal_relIsoEB != -1 && rel_hcal_iso >= max_hcal_relIsoEB) ||
		    (max_tk_relIsoEB != -1 && rel_tk_iso >= max_tk_relIsoEB) ){
			return false;					  
		}
		
	} 
	else if(electron.isEE()) {
		if ((max_dr03TkSumPtEE != -1 && electron.dr03TkSumPt() >= max_dr03TkSumPtEE) ||   
		    (max_dr03EcalRecHitSumEtEE != -1 && electron.dr03EcalRecHitSumEt() >= max_dr03EcalRecHitSumEtEE) || 
		    (max_dr03HcalTowerSumEtEE != -1 && electron.dr03HcalTowerSumEt()  >= max_dr03HcalTowerSumEtEE) ||
		    (max_hadronicOverEmEE != -1 && electron.hadronicOverEm() >= max_hadronicOverEmEE) ||
		    (max_deltaPhiSuperClusterTrackAtVtxEE != -1 && 
		     fabs(electron.deltaPhiSuperClusterTrackAtVtx()) >= max_deltaPhiSuperClusterTrackAtVtxEE) ||
		    (max_deltaEtaSuperClusterTrackAtVtxEE != -1 &&
		     fabs(electron.deltaEtaSuperClusterTrackAtVtx()) >=  max_deltaEtaSuperClusterTrackAtVtxEE) ||
		    (max_sigmaIetaIetaEE != -1 && electron.sigmaIetaIeta() >= max_sigmaIetaIetaEE) ||
		    (min_sigmaIetaIetaSpikesEE != -1 && electron.sigmaIetaIeta() <= min_sigmaIetaIetaSpikesEE) ||
		    (max_ecal_relIsoEE != -1 && rel_ecal_iso >= max_ecal_relIsoEE) ||
		    (max_hcal_relIsoEE != -1 && rel_hcal_iso >= max_hcal_relIsoEE) ||
		    (max_tk_relIsoEE != -1 && rel_tk_iso >= max_tk_relIsoEE) ){
			return false;					  
		}
		
	}

	if(max_relIso != -1){
                double relIso = (electron.dr03EcalRecHitSumEt() + electron.dr03HcalTowerSumEt() + electron.dr03TkSumPt())/electron.et();
                if(relIso > max_relIso){
                        return false;
                }
        }
	

	
	return true;
}
