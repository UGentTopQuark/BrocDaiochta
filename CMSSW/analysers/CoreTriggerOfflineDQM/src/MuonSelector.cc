#include "../interface/MuonSelector.h"

tpg::MuonSelector::MuonSelector()
{
	min_nTrackHits = -1;
	min_nPixelHits = -1;
	min_nMuonHits = -1;
	min_nMatches = -1;
	max_normChi2 = -1;
	max_dxy = -1;
	max_relIso = -1;
	isGlobalMuon = false;
	isTrackerMuon = false;

	selected_muons = new std::vector<reco::Muon>();
}

tpg::MuonSelector::~MuonSelector()
{
	if(selected_muons){
		delete selected_muons;
		selected_muons = NULL;
	}
}

std::vector<reco::Muon>* tpg::MuonSelector::get_selected_muons()
{
	selected_muons->clear();
	if(handle_holder == NULL){
		std::cerr<<"MuonSelector: NULL handle_holder" << std::endl;
		return selected_muons;
	}

	edm::Handle<edm::View<reco::Muon> > muons = handle_holder->get_muonHandle();
	for(edm::View<reco::Muon>::const_iterator muon = muons->begin();
	    muon != muons->end();muon++){

		if(is_good_muon(*muon)){
			selected_muons->push_back(*muon);
		}
	}

	return selected_muons;
}
					 
bool tpg::MuonSelector::is_good_muon(reco::Muon muon)
{
	reco::TrackRef gm = muon.globalTrack();
	reco::TrackRef tk = muon.innerTrack();

	if(isGlobalMuon && !muon.isGlobalMuon()){
		return false;
	}
	if(isTrackerMuon && !muon.isTrackerMuon()){
		return false;
	}

	if(min_pt != -1 && muon.pt() < min_pt){
		return false;
	}
	if(max_eta != -1 && fabs(muon.eta()) > max_eta){
		return false;
	}

	if(min_nTrackHits != -1 &&
	   tk->hitPattern().numberOfValidTrackerHits() < min_nTrackHits){
		return false;
	}
	if(min_nPixelHits != -1 &&
	   tk->hitPattern().numberOfValidPixelHits() < min_nPixelHits){
		return false;
	}
	if(min_nMuonHits != -1 && 
	   gm->hitPattern().numberOfValidMuonHits() < min_nMuonHits){
		return false;
	}
	if(min_nMatches != -1 && muon.numberOfMatches() < min_nMatches){
		return false;
	}

	if(max_normChi2 != -1 && gm->normalizedChi2() > max_normChi2){
		return false;
	}
	edm::Handle<reco::BeamSpot> beamSpotHandle = handle_holder->get_beamSpotHandle();
	if((max_dxy != -1 && !beamSpotHandle.isValid()) ||
	   (max_dxy != -1 && 
	    fabs(gm->dxy(beamSpotHandle->position())) > max_dxy)){
		return false;
	}
	if(max_relIso != -1){
		double relIso = (muon.isolationR03().emEt + muon.isolationR03().hadEt + muon.isolationR03().sumPt)/muon.pt();
		if(relIso > max_relIso){
			return false;
		}
	}
	

	return true;
}
