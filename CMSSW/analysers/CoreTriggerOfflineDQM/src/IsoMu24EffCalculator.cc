#include "../interface/IsoMu24EffCalculator.h"

namespace tpg{
	tpg::IsoMu24EffCalculator::IsoMu24EffCalculator(TFile *outfile, std::string unversioned_trigger_name) : tpg::HLTEffCalculator(outfile,unversioned_trigger_name)
{
	mu_selector = new MuonSelector();

	outfile->cd();

	pt_hname = unv_trigger_name+pt_hname;
	eta_hname = unv_trigger_name+eta_hname;

	histos_1D[pt_hname+denom_suffix] = new TH1F((pt_hname+denom_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[eta_hname+denom_suffix] = new TH1F((eta_hname+denom_suffix).c_str(),"eta of selected offline muons", 48, -2.4, 2.4);
	histos_1D[pt_hname+l3num_suffix] = new TH1F((pt_hname+l3num_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[eta_hname+l3num_suffix] = new TH1F((eta_hname+l3num_suffix).c_str(),"eta of selected offline muons", 48, -2.4, 2.4);
	histos_1D[pt_hname+l1num_suffix] = new TH1F((pt_hname+l1num_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[eta_hname+l1num_suffix] = new TH1F((eta_hname+l1num_suffix).c_str(),"eta of selected offline muons", 48, -2.4, 2.4);
}

tpg::IsoMu24EffCalculator::~IsoMu24EffCalculator()
{
	if(mu_selector){
		delete mu_selector;
		mu_selector = NULL;
	}
}


void tpg::IsoMu24EffCalculator::calculate_efficiency()
{
	if(!handle_holder){
		std::cerr<<"IsoMu24EffCalc: NULL handle_holder"<<std::endl;
		return;
	}

	//Select good isolated muons
	mu_selector->set_handle_holder(handle_holder);
	mu_selector->set_isGlobalMuon(true);
	mu_selector->set_isTrackerMuon(true);
	mu_selector->set_min_nTrackHits(11.);
	mu_selector->set_min_nPixelHits(1.);
	mu_selector->set_min_nMuonHits(1.);
	mu_selector->set_min_nMatches(2.);
	mu_selector->set_max_normChi2(10.);
	mu_selector->set_max_dxy(0.2);
	mu_selector->set_max_relIso(0.15);
	mu_selector->set_min_pt(5.);
	std::vector<reco::Muon> *selected_muons = mu_selector->get_selected_muons();
	if(selected_muons->size() < 1){return;}		
	
	//Fill denominator with selected muons
	for(std::vector<reco::Muon>::iterator muon = selected_muons->begin();
	    muon != selected_muons->end();muon++){
		if(fabs(muon->eta()) < 2.1){
			histos_1D[pt_hname+denom_suffix]->Fill(muon->pt());
		}
		if(muon->pt() > 30){
			histos_1D[eta_hname+denom_suffix]->Fill(muon->eta());
		}
	}

	//FIXME:Is any kind of automation for selection of saved tags necessary?
	//Possibly because l1 seed could change. Can l3 module change?
	//Should be safe to assume the type doesn't change?
	//L1 seed type wont change.
	//Add check to see if modules are in savedTags then 
	//return if not.
	std::string l1_module = "hltL1sL1SingleMu16";
	std::string l3_module = "hltSingleMuL2QualIsoL3IsoFiltered24";


	//Match objects from l1 module to selected muons.
	std::vector<reco::Muon>* matched_l1_muons = trigger_object_matcher->match_trigger_objects<reco::Muon>(selected_muons,l1_module,0.3);

	if(matched_l1_muons->size() < 1){ delete matched_l1_muons; return;}
	
	for(std::vector<reco::Muon>::iterator muon = matched_l1_muons->begin();
	    muon != matched_l1_muons->end();muon++){
		if(fabs(muon->eta()) < 2.1){
			histos_1D[pt_hname+l1num_suffix]->Fill(muon->pt());
		}
		if(muon->pt() > 30){
			histos_1D[eta_hname+l1num_suffix]->Fill(muon->eta());
		}
	}     
	
	//Match objects from l3 module to selected muons passing l1.
	std::vector<reco::Muon>* matched_l3_muons = trigger_object_matcher->match_trigger_objects(matched_l1_muons,l3_module,0.2);

	for(std::vector<reco::Muon>::iterator muon = matched_l3_muons->begin();
	    muon != matched_l3_muons->end();muon++){
		if(fabs(muon->eta()) < 2.1){
			histos_1D[pt_hname+l3num_suffix]->Fill(muon->pt());
		}
		if(muon->pt() > 30){
			histos_1D[eta_hname+l3num_suffix]->Fill(muon->eta());
		}
	}    

}
}
