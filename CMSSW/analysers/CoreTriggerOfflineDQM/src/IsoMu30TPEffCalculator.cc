#include "../interface/IsoMu30TPEffCalculator.h"

namespace tpg{
	tpg::IsoMu30TPEffCalculator::IsoMu30TPEffCalculator(TFile *outfile, std::string unversioned_trigger_name) : tpg::HLTEffCalculator(outfile,unversioned_trigger_name)
{
	mu_selector = new MuonSelector();
	dilep_reconstructor = new DiLeptonReconstructor();
	dilep_reconstructor->set_mass_window(76.0,106.0);

	outfile->cd();

	pt_hname = unv_trigger_name+"_TP"+pt_hname;
	eta_hname = unv_trigger_name+"_TP"+eta_hname;

	histos_1D[pt_hname+denom_suffix] = new TH1F((pt_hname+denom_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[eta_hname+denom_suffix] = new TH1F((eta_hname+denom_suffix).c_str(),"eta of selected offline muons", 50, -2.1, 2.1);
	histos_1D[pt_hname+l3num_suffix] = new TH1F((pt_hname+l3num_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[eta_hname+l3num_suffix] = new TH1F((eta_hname+l3num_suffix).c_str(),"eta of selected offline muons", 50, -2.1, 2.1);
	histos_1D[pt_hname+l1num_suffix] = new TH1F((pt_hname+l1num_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[eta_hname+l1num_suffix] = new TH1F((eta_hname+l1num_suffix).c_str(),"eta of selected offline muons", 48, -2.1, 2.1);
}

tpg::IsoMu30TPEffCalculator::~IsoMu30TPEffCalculator()
{
	if(mu_selector){
		delete mu_selector;
		mu_selector = NULL;
	}
        if(dilep_reconstructor){
                delete dilep_reconstructor;
                dilep_reconstructor = NULL;
        }

}


void tpg::IsoMu30TPEffCalculator::calculate_efficiency()
{
	if(!handle_holder){
		std::cerr<<"IsoMu30TPEffCalc: NULL handle_holder"<<std::endl;
		return;
	}

	std::string l1_module = "hltL1sMu14Eta2p1";
	std::string l3_module = "hltL3IsoL1sMu14Eta2p1L1f0L2f14QL2IsoL3f30L3IsoFiltered";
	double eta_threshold = 2.1; //to fill pt plots
	double pt_threshold = 35.0; //to fill eta plots
	double max_l3_dR = 0.2;//trigger object matching
	double max_l1_dR = 0.3;

	//Select good isolated muons. 
	mu_selector->set_handle_holder(handle_holder);
	mu_selector->set_isGlobalMuon(true);
	mu_selector->set_isTrackerMuon(true);
	mu_selector->set_min_nTrackHits(11.);
	mu_selector->set_min_nPixelHits(1.);
	mu_selector->set_min_nMuonHits(1.);
	mu_selector->set_min_nMatches(2.);
	mu_selector->set_max_normChi2(10.);
	mu_selector->set_max_dxy(0.2);
	mu_selector->set_max_relIso(0.15);
	mu_selector->set_min_pt(10.);
	std::vector<reco::Muon> *selected_muons = mu_selector->get_selected_muons();
	if(selected_muons->size() < 2){return;}	

	std::vector<reco::Muon> *loose_muon_leg = new std::vector<reco::Muon>();
	std::vector<reco::Muon> *tight_muon_leg = new std::vector<reco::Muon>();
        if((dilep_reconstructor->reconstruct_candidates<reco::Muon>(selected_muons,selected_muons,loose_muon_leg,tight_muon_leg)) != 1){
		return;
	}
	reco::Muon tight_muon = (*tight_muon_leg)[0];

	//Only fill loose lepton information id tight lepton passes trigger
        if(trigger_object_matcher->match_trigger_objects<reco::Muon>(tight_muon,l1_module,max_l1_dR) &&
	   trigger_object_matcher->match_trigger_objects<reco::Muon>(tight_muon,l3_module,max_l3_dR)){
		reco::Muon loose_muon = (*loose_muon_leg)[0];

		if(fabs(loose_muon.eta()) < eta_threshold){
			histos_1D[pt_hname+denom_suffix]->Fill(loose_muon.pt());
		}
		if(loose_muon.pt() > pt_threshold){
			histos_1D[eta_hname+denom_suffix]->Fill(loose_muon.eta());
		}
	
		if(trigger_object_matcher->match_trigger_objects<reco::Muon>(loose_muon,l1_module,max_l1_dR)){	
			if(fabs(loose_muon.eta()) < eta_threshold){
				histos_1D[pt_hname+l1num_suffix]->Fill(loose_muon.pt());
			}
			if(loose_muon.pt() > pt_threshold){
				histos_1D[eta_hname+l1num_suffix]->Fill(loose_muon.eta());
			}  
		
			//Match objects from l3 module to selected loose_muons passing l1.
			if(trigger_object_matcher->match_trigger_objects<reco::Muon>(loose_muon,l3_module,max_l3_dR)){
				if(fabs(loose_muon.eta()) < eta_threshold){
					histos_1D[pt_hname+l3num_suffix]->Fill(loose_muon.pt());
				}
				if(loose_muon.pt() > pt_threshold){
					histos_1D[eta_hname+l3num_suffix]->Fill(loose_muon.eta());
				}
			}
		}
	}

}
}//namespace
