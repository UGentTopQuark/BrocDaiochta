#include "../interface/Jet370EffCalculator.h"

namespace tpg{
	tpg::Jet370EffCalculator::Jet370EffCalculator(TFile *outfile, std::string unversioned_trigger_name) : tpg::HLTEffCalculator(outfile,unversioned_trigger_name)
{
	jet_selector = new PFJetSelector();

	outfile->cd();

	pt_hname = unv_trigger_name+pt_hname;
	eta_hname = unv_trigger_name+eta_hname;

	histos_1D[pt_hname+denom_suffix] = new TH1F((pt_hname+denom_suffix).c_str(),"pt of selected offline PF jets", 600, 0, 600);
	histos_1D[eta_hname+denom_suffix] = new TH1F((eta_hname+denom_suffix).c_str(),"eta of selected offline PF jets", 50, -2.5, 2.5);
	histos_1D[pt_hname+l3num_suffix] = new TH1F((pt_hname+l3num_suffix).c_str(),"pt of selected offline PF jets", 600, 0, 600);
	histos_1D[eta_hname+l3num_suffix] = new TH1F((eta_hname+l3num_suffix).c_str(),"eta of selected offline PF jets", 50, -2.5, 2.5);
	histos_1D[pt_hname+l1num_suffix] = new TH1F((pt_hname+l1num_suffix).c_str(),"pt of selected offline PF jets", 600, 0, 600);
	histos_1D[eta_hname+l1num_suffix] = new TH1F((eta_hname+l1num_suffix).c_str(),"eta of selected offline PF jets", 50, -2.5, 2.5);
}

tpg::Jet370EffCalculator::~Jet370EffCalculator()
{
	if(jet_selector){
		delete jet_selector;
		jet_selector = NULL;
	}
}


void tpg::Jet370EffCalculator::calculate_efficiency()
{
	if(!handle_holder){
		std::cerr<<"Jet370EffCalc: NULL handle_holder"<<std::endl;
		return;
	}

	std::string l1_module = "hltL1sL1SingleJet92";
	if(handle_holder->get_run_number() > 176544){
		l1_module = "hltL1sL1SingleJet128";
	}
	std::string l3_module = "hltSingleJet370Regional";

	//Select good isolated jets. WP80
        double eta_threshold = 2.5; //to fill pt plots
        double pt_threshold = 380.0; //to fill eta plots
        double max_l3_dR = 0.2;//trigger object matching
        double max_l1_dR = 0.3;

	jet_selector->set_handle_holder(handle_holder);
	jet_selector->set_min_pt(90.);
	jet_selector->set_min_ndaughters(1.);
	jet_selector->set_min_chf(0.);
	jet_selector->set_max_nhf(0.99);
	jet_selector->set_max_nemf(0.99);
	jet_selector->set_max_cemf(0.99);
	jet_selector->set_min_cmulti(0.);

	std::vector<reco::PFJet> *selected_jets = jet_selector->get_selected_jets();
	if(selected_jets->size() < 1){return;}		
	
	//Fill denominator with selected jets
	for(std::vector<reco::PFJet>::iterator jet = selected_jets->begin();
	    jet != selected_jets->end();jet++){
		if(fabs(jet->eta()) < eta_threshold){
			histos_1D[pt_hname+denom_suffix]->Fill(jet->pt());
		}
		if(jet->pt() > pt_threshold){
			histos_1D[eta_hname+denom_suffix]->Fill(jet->eta());
		}
	}


	//Match objects from l1 module to selected jets.
	std::vector<reco::PFJet>* matched_l1_jets = trigger_object_matcher->match_trigger_objects<reco::PFJet>(selected_jets,l1_module,max_l1_dR);

	if(matched_l1_jets->size() < 1){ delete matched_l1_jets; return;}
	
	for(std::vector<reco::PFJet>::iterator jet = matched_l1_jets->begin();
	    jet != matched_l1_jets->end();jet++){
		if(fabs(jet->eta()) < eta_threshold){
			histos_1D[pt_hname+l1num_suffix]->Fill(jet->pt());
		}
		if(jet->pt() > pt_threshold){
			histos_1D[eta_hname+l1num_suffix]->Fill(jet->eta());
		}
	}     
	
	//Match objects from l3 module to selected jets passing l1.
	std::vector<reco::PFJet>* matched_l3_jets = trigger_object_matcher->match_trigger_objects(matched_l1_jets,l3_module,max_l3_dR);

	for(std::vector<reco::PFJet>::iterator jet = matched_l3_jets->begin();
	    jet != matched_l3_jets->end();jet++){
		if(fabs(jet->eta()) < eta_threshold){
			histos_1D[pt_hname+l3num_suffix]->Fill(jet->pt());
		}
		if(jet->pt() > pt_threshold){
			histos_1D[eta_hname+l3num_suffix]->Fill(jet->eta());
		}
	}    

}
}
