#include "../interface/Ele65EffCalculator.h"

namespace tpg{
	tpg::Ele65EffCalculator::Ele65EffCalculator(TFile *outfile, std::string unversioned_trigger_name) : tpg::HLTEffCalculator(outfile,unversioned_trigger_name)
{
	e_selector = new ElectronSelector();

	outfile->cd();

	pt_hname = unv_trigger_name+pt_hname;
	eta_hname = unv_trigger_name+eta_hname;

	histos_1D[pt_hname+denom_suffix] = new TH1F((pt_hname+denom_suffix).c_str(),"pt of selected offline electrons", 200, 0, 200);
	histos_1D[eta_hname+denom_suffix] = new TH1F((eta_hname+denom_suffix).c_str(),"eta of selected offline electrons", 50, -2.5, 2.5);
	histos_1D[pt_hname+l3num_suffix] = new TH1F((pt_hname+l3num_suffix).c_str(),"pt of selected offline electrons", 200, 0, 200);
	histos_1D[eta_hname+l3num_suffix] = new TH1F((eta_hname+l3num_suffix).c_str(),"eta of selected offline electrons", 50, -2.5, 2.5);
	histos_1D[pt_hname+l1num_suffix] = new TH1F((pt_hname+l1num_suffix).c_str(),"pt of selected offline electrons", 200, 0, 200);
	histos_1D[eta_hname+l1num_suffix] = new TH1F((eta_hname+l1num_suffix).c_str(),"eta of selected offline electrons", 50, -2.5, 2.5);
}

tpg::Ele65EffCalculator::~Ele65EffCalculator()
{
	if(e_selector){
		delete e_selector;
		e_selector = NULL;
	}
}


void tpg::Ele65EffCalculator::calculate_efficiency()
{
	if(!handle_holder){
		std::cerr<<"Ele65EffCalc: NULL handle_holder"<<std::endl;
		return;
	}

	//Select good isolated electrons. WP80
	e_selector->set_handle_holder(handle_holder);
	e_selector->set_min_pt(5.);

	e_selector->set_max_hadronicOverEmEB(0.04);
	e_selector->set_max_deltaPhiSuperClusterTrackAtVtxEB(0.06);
	e_selector->set_max_deltaEtaSuperClusterTrackAtVtxEB(0.004);
	e_selector->set_max_sigmaIetaIetaEB(0.01);
	e_selector->set_max_ecal_relIsoEB(0.07);
	e_selector->set_max_hcal_relIsoEB(0.10);
	e_selector->set_max_tk_relIsoEB(0.09);
	
	e_selector->set_max_hadronicOverEmEE(0.025);
	e_selector->set_max_deltaPhiSuperClusterTrackAtVtxEE(0.03);
	e_selector->set_max_deltaEtaSuperClusterTrackAtVtxEE(0.007);
	e_selector->set_max_sigmaIetaIetaEE(0.03);
	e_selector->set_max_ecal_relIsoEE(0.05);
	e_selector->set_max_hcal_relIsoEE(0.025);
	e_selector->set_max_tk_relIsoEE(0.04);

	std::vector<reco::GsfElectron> *selected_electrons = e_selector->get_selected_electrons();
	if(selected_electrons->size() < 1){return;}		
	
	//Fill denominator with selected electrons
	for(std::vector<reco::GsfElectron>::iterator electron = selected_electrons->begin();
	    electron != selected_electrons->end();electron++){
		if(fabs(electron->eta()) < 2.5){
			histos_1D[pt_hname+denom_suffix]->Fill(electron->pt());
		}
		if(electron->pt() > 70){
			histos_1D[eta_hname+denom_suffix]->Fill(electron->eta());
		}
	}

	std::string l1_module = "hltL1sL1SingleEG20";
	std::string l3_module = "hltEle65CaloIdVTTrkIdTDphiFilter";


	//Match objects from l1 module to selected electrons.
	std::vector<reco::GsfElectron>* matched_l1_electrons = trigger_object_matcher->match_trigger_objects<reco::GsfElectron>(selected_electrons,l1_module,0.3);

	if(matched_l1_electrons->size() < 1){ delete matched_l1_electrons; return;}
	
	for(std::vector<reco::GsfElectron>::iterator electron = matched_l1_electrons->begin();
	    electron != matched_l1_electrons->end();electron++){
		if(fabs(electron->eta()) < 2.5){
			histos_1D[pt_hname+l1num_suffix]->Fill(electron->pt());
		}
		if(electron->pt() > 70){
			histos_1D[eta_hname+l1num_suffix]->Fill(electron->eta());
		}
	}     
	
	//Match objects from l3 module to selected electrons passing l1.
	std::vector<reco::GsfElectron>* matched_l3_electrons = trigger_object_matcher->match_trigger_objects(matched_l1_electrons,l3_module,0.2);

	for(std::vector<reco::GsfElectron>::iterator electron = matched_l3_electrons->begin();
	    electron != matched_l3_electrons->end();electron++){
		if(fabs(electron->eta()) < 2.5){
			histos_1D[pt_hname+l3num_suffix]->Fill(electron->pt());
		}
		if(electron->pt() > 70){
			histos_1D[eta_hname+l3num_suffix]->Fill(electron->eta());
		}
	}    

}
}
