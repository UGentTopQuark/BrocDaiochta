#include "../interface/TriggerObjectMatcher.h"

template std::vector<reco::Muon>*  tpg::TriggerObjectMatcher::match_trigger_objects<reco::Muon>(std::vector<reco::Muon> *cmssw_objects,std::string module,double max_dR);
template std::vector<reco::GsfElectron>* tpg::TriggerObjectMatcher::match_trigger_objects<reco::GsfElectron>(std::vector<reco::GsfElectron> *cmssw_objects,std::string module,double max_dR);
template std::vector<reco::PFJet>*  tpg::TriggerObjectMatcher::match_trigger_objects<reco::PFJet>(std::vector<reco::PFJet> *cmssw_objects,std::string module,double max_dR);
template bool tpg::TriggerObjectMatcher::match_trigger_objects<reco::Muon>(reco::Muon cmssw_object,std::string module,double max_dR);
template bool tpg::TriggerObjectMatcher::match_trigger_objects<reco::GsfElectron>(reco::GsfElectron cmssw_object,std::string module,double max_dR);
template bool tpg::TriggerObjectMatcher::match_trigger_objects<reco::PFJet>(reco::PFJet cmssw_object,std::string module,double max_dR);

tpg::TriggerObjectMatcher::TriggerObjectMatcher()
{
	handle_holder = NULL;
	hltConfig = NULL;
        trigger_objects = new std::vector<trigger::TriggerObject>();
	
}
tpg::TriggerObjectMatcher::~TriggerObjectMatcher()
{
	if(trigger_objects){
		delete trigger_objects;
		trigger_objects = NULL;
	}
}

void tpg::TriggerObjectMatcher::set_handle_holder(HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	this->hltConfig = handle_holder->get_hltConfig();
	this->aodTriggerEvent = handle_holder->get_aodTriggerEvent();
	this->HLTR = handle_holder->get_HLTR();
}

template <class CMSSWObject>
bool tpg::TriggerObjectMatcher::match_trigger_objects(CMSSWObject cmssw_object,std::string module,double max_dR)
{
	std::vector<CMSSWObject> *cmssw_objects = new std::vector<CMSSWObject>();
	cmssw_objects->push_back(cmssw_object);
	
	bool passed = false;
	std::vector<CMSSWObject> *matched_objects =  match_trigger_objects(cmssw_objects,module,max_dR);
	
	if(matched_objects->size() > 0){ passed = true;}
	
	delete matched_objects; 
	delete cmssw_objects;

	return passed;
}

template <class CMSSWObject>
std::vector<CMSSWObject>* tpg::TriggerObjectMatcher::match_trigger_objects(std::vector<CMSSWObject> *cmssw_objects,std::string module,double max_dR)
{
	std::vector<CMSSWObject> *matched_cmssw_objects = new std::vector<CMSSWObject>();

	std::vector<trigger::TriggerObject> *trigger_objects;
	trigger_objects = get_trigger_objects(module);

	std::vector<int> matched_trig_object_ids;

	for(typename std::vector<CMSSWObject>::iterator cmssw_obj = cmssw_objects->begin();
	    cmssw_obj != cmssw_objects->end();cmssw_obj++){

		double min_dR = -1;
		int matched_trig_object_id = -1;
		int i = 0;
		for(std::vector<trigger::TriggerObject>::iterator trig_obj = trigger_objects->begin();
		    trig_obj != trigger_objects->end();trig_obj++){

			//Skip this trigger object if already matched 
			bool skip_trigger_object = false;
			for(std::vector<int>::iterator matched_trig_id = matched_trig_object_ids.begin();
			    matched_trig_id != matched_trig_object_ids.end();matched_trig_id++){
				if(i == *matched_trig_id){
					skip_trigger_object = true;
					break;
				}
			}
			if(skip_trigger_object){i++; continue;}

			ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > trig;
			trig.SetPxPyPzE(trig_obj->px(),trig_obj->py(),trig_obj->pz(),trig_obj->energy());
			
			double dR = ROOT::Math::VectorUtil::DeltaR(trig,cmssw_obj->p4());
			
			if(min_dR == -1 || dR < min_dR){
				min_dR = dR;
				matched_trig_object_id = i;
			} 

			i++;
		}
		if(min_dR != -1 && min_dR < max_dR){
			matched_cmssw_objects->push_back(*cmssw_obj);
			matched_trig_object_ids.push_back(matched_trig_object_id);
		}
			

	}

	return matched_cmssw_objects;
}

std::vector<trigger::TriggerObject>* tpg::TriggerObjectMatcher::get_trigger_objects(std::string module)
{
	trigger_objects->clear();

	const trigger::TriggerObjectCollection objects = aodTriggerEvent->getObjects();
        //Now use module name to get keys for trigger objects
	//FIXME: Recplace HLT with aodTriggerEvent.process?
        edm::InputTag filterTag = edm::InputTag(module, "", "HLT");

        int filterIndex = aodTriggerEvent->filterIndex(filterTag);
        if ( filterIndex < aodTriggerEvent->sizeFilters() ) {
                //These trigger keys point you to the trigger objects passing this filter in the objects collection
                const trigger::Keys &keys = aodTriggerEvent->filterKeys( filterIndex );
                        for ( size_t i = 0; i < keys.size(); i++ ){
				trigger_objects->push_back(objects[keys[i]]);
			}
        }

	return trigger_objects;
}

