#include "../interface/ObjectSelector.h"

tpg::ObjectSelector::ObjectSelector()
{
	min_pt = -1;
	max_eta = -1;
	
	handle_holder = NULL;
}

tpg::ObjectSelector::~ObjectSelector()
{
}

void tpg::ObjectSelector::set_handle_holder(HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
}
