#include "../interface/PFJetSelector.h"

tpg::PFJetSelector::PFJetSelector()
{
	min_ndaughters= -1;
	min_chf= -1;
	max_nhf= -1;
	max_cemf= -1;
	min_cmulti= -1;
	
	selected_jets = new std::vector<reco::PFJet>();
}

tpg::PFJetSelector::~PFJetSelector()
{
	if(selected_jets){
		delete selected_jets;
		selected_jets = NULL;
	}
}

std::vector<reco::PFJet>* tpg::PFJetSelector::get_selected_jets()
{
	selected_jets->clear();
	if(handle_holder == NULL){
		std::cerr<<"PFJetSelector: NULL handle_holder" << std::endl;
		return selected_jets;
	}

	edm::Handle<edm::View<reco::PFJet> > jets = handle_holder->get_jetHandle();
	for(edm::View<reco::PFJet>::const_iterator jet = jets->begin();
	    jet != jets->end();jet++){

		if(is_good_jet(*jet)){
			selected_jets->push_back(*jet);
		}
	}

	return selected_jets;
}
					 
bool tpg::PFJetSelector::is_good_jet(reco::PFJet jet)
{	
	if(min_pt != -1 && jet.pt() < min_pt){
		return false;
	}
	if(max_eta != -1 && fabs(jet.eta()) > max_eta){
		return false;
	}
	if(min_ndaughters != -1 && jet.numberOfDaughters() < min_ndaughters){
		return false;
	}
	if(min_chf != -1 && jet.chargedHadronEnergyFraction() < min_chf){
		return false;
	}
	if(max_nhf != -1 && jet.neutralHadronEnergyFraction() > max_nhf){
		return false;
	}
	if(max_nemf != -1 && jet.neutralEmEnergyFraction() > max_nemf){
		return false;
	}
	if(max_cemf != -1 && jet.chargedEmEnergyFraction() > max_cemf){
		return false;
	}
	if(min_cmulti != -1 && jet.chargedMultiplicity() < min_cmulti){
		return false;
	}

	
	return true;
}
