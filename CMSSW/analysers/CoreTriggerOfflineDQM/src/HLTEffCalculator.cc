#include "../interface/HLTEffCalculator.h"

tpg::HLTEffCalculator::HLTEffCalculator(TFile *outfile, std::string unversioned_trigger_name)
{
	unv_trigger_name = unversioned_trigger_name;
	trigger_name = "";
	this->outfile = outfile;

	pt_hname = "_offPt"; 
	eta_hname ="_offEta"; 
	denom_suffix = "_All";
	l3num_suffix = "_HLTPass";
	l1num_suffix = "_L1Pass";
	
	handle_holder = NULL;
	trigger_object_matcher = new TriggerObjectMatcher();
}

tpg::HLTEffCalculator::~HLTEffCalculator()
{
	outfile->cd();
	for(std::map<std::string,TH1F*>::iterator histo = histos_1D.begin();
	    histo != histos_1D.end();histo++){
		histo->second->Write();
	}

}

void tpg::HLTEffCalculator::set_handle_holder(HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
        trigger_object_matcher->set_handle_holder(handle_holder);

}

void tpg::HLTEffCalculator::set_versioned_name(std::string name)
{
	trigger_name = name;
}

void tpg::HLTEffCalculator::calculate_efficiency()
{

}
