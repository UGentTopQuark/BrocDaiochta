#include "../interface/CoreTriggerOfflineDQM.h"


CoreTriggerOfflineDQM::CoreTriggerOfflineDQM(const edm::ParameterSet& iConfig):
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  eleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("electronTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  BeamSpotLabel_ (iConfig.getParameter<edm::InputTag> ("BeamSpotTag"))
{
	std::string outfile_name = iConfig.getParameter<std::string>("outfilename");
	outfile = new TFile(outfile_name.c_str(),"RECREATE");

	hltConfig = new HLTConfigProvider();
	handle_holder = new tpg::HandleHolder();

	lumi_block_changed = true;
        hltConfigChanged = true;

	// isomu24 = NULL;
	// ele65 = NULL;
	// mu17ele8 = NULL;
	isomu30tp = NULL;
	isoele65tp = NULL;
	mu8isoele17 = NULL;
	jet370 = NULL;
}

CoreTriggerOfflineDQM::~CoreTriggerOfflineDQM()
{ 
	if(handle_holder){delete handle_holder; handle_holder = NULL;}
	if(hltConfig){ delete hltConfig; hltConfig = NULL; }
	if(isomu30tp){ delete isomu30tp; isomu30tp = NULL; }
	if(isoele65tp){ delete isoele65tp; isoele65tp = NULL; }
	if(mu8isoele17){ delete mu8isoele17; mu8isoele17 = NULL; }
	if(jet370){ delete jet370; jet370 = NULL; }
	//if(isomu24){ delete isomu24; isomu24 = NULL; }
	//if(ele65){ delete ele65; ele65 = NULL; }
	//if(mu17ele8){ delete mu17ele8; mu17ele8 = NULL; }
        outfile->Write();
        outfile->Close();
        if(outfile){
                delete outfile;
                outfile = NULL;
        }

}

void CoreTriggerOfflineDQM::reset_triggers_to_study()
{
	/*** Map of triggers to study to versioned name. 
	 * Always initialise versioned name to -1. 
	 * Program will replace this automatically if
	 * the trigger you want to study is available
	 ***/
	//triggers_to_study["HLT_IsoMu24"] = "-1";//Inclusive method
	triggers_to_study["HLT_IsoMu30_eta2p1"] = "-1";
	triggers_to_study["HLT_Ele65_CaloIdVT_TrkIdT"] = "-1";
	triggers_to_study["HLT_Mu8_Ele17_CaloIdT_CaloIsoVL"] = "-1";
	triggers_to_study["HLT_Jet370"] = "-1";

}

void CoreTriggerOfflineDQM::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	using namespace edm;

       	edm::Handle<edm::View<reco::Muon> > muons;
       	iEvent.getByLabel(muoLabel_,muons);

       	edm::Handle<edm::View<reco::GsfElectron> > electrons;
       	iEvent.getByLabel(eleLabel_,electrons);

       	edm::Handle<edm::View<reco::PFJet> > jets;
       	iEvent.getByLabel(jetLabel_,jets);

       	edm::Handle<reco::BeamSpot> beamSpotHandle;
       	iEvent.getByLabel(BeamSpotLabel_,beamSpotHandle);

       	edm::Handle<edm::View<reco::MET> > metHandle;
       	iEvent.getByLabel(metLabel_,metHandle);

	edm::Handle<edm::TriggerResults> HLTR;
	iEvent.getByLabel("TriggerResults",HLTR);

	edm::Handle<trigger::TriggerEvent> aodTriggerEvent;
	iEvent.getByLabel("hltTriggerSummaryAOD", aodTriggerEvent);

	handle_holder->set_muonHandle(muons);
	handle_holder->set_electronHandle(electrons);
	handle_holder->set_jetHandle(jets);
	handle_holder->set_metHandle(metHandle);
	handle_holder->set_beamSpotHandle(beamSpotHandle);
	handle_holder->set_hltConfig(hltConfig);
	handle_holder->set_aodTriggerEvent(aodTriggerEvent);
	handle_holder->set_HLTR(HLTR);

	if(!HLTR.isValid()){
		std::cout << "WARNING: no valid HLTR" << std::endl;
		return;
	}

	if(lumi_block_changed){
		reset_triggers_to_study();
		hltConfig->init(iEvent.getRun(), iSetup, "HLT", hltConfigChanged);
		lumi_block_changed = false;

		//Loop over unversioned trigger names. Check for each one if 
		//versioned name available in current menu. If yes, save versioned
		//name in triggers to study map 
		std::vector<std::string> valid_triggers = hltConfig->triggerNames();
		for(std::map<std::string,std::string>::iterator trigger_names = triggers_to_study.begin();
		    trigger_names != triggers_to_study.end(); trigger_names++){
			const std::string unv_trigger_name = trigger_names->first;

			std::vector<std::string> v_name_vec = hltConfig->restoreVersion(valid_triggers,unv_trigger_name);

			if(v_name_vec.size() == 0){
				triggers_to_study[unv_trigger_name] = "-1";
				continue; //Trigger not available in current menu. 
			}else{
				triggers_to_study[unv_trigger_name] = v_name_vec[0];
			}
		}
		//Initialise classes for available triggers
		//if class not already initialised
		// if( triggers_to_study.find("HLT_IsoMu24") != triggers_to_study.end() &&!isomu24 && triggers_to_study["HLT_IsoMu24"] != "-1"){
		// 	isomu24 = new tpg::IsoMu24EffCalculator(outfile, "HLT_IsoMu24",triggers_to_study["HLT_IsoMu24"]);
		// }
		// if( triggers_to_study.find("HLT_Mu30") != triggers_to_study.end() &&!mu30 && triggers_to_study["HLT_Mu30"] != "-1"){
		// 	mu30 = new tpg::Mu30EffCalculator(outfile, "HLT_Mu30",triggers_to_study["HLT_Mu30"]);
		// }
		if( triggers_to_study.find("HLT_Ele65_CaloIdVT_TrkIdT") != triggers_to_study.end()  && triggers_to_study["HLT_Ele65_CaloIdVT_TrkIdT"] != "-1"){
			if(!isoele65tp){isoele65tp = new tpg::Ele65TPEffCalculator(outfile, "HLT_Ele65_CaloIdVT_TrkIdT");}
			isoele65tp->set_versioned_name(triggers_to_study["HLT_Ele65_CaloIdVT_TrkIdT"]);
		}
		if( triggers_to_study.find("HLT_IsoMu30_eta2p1") != triggers_to_study.end()  && triggers_to_study["HLT_IsoMu30_eta2p1"] != "-1"){
			if(!isomu30tp){isomu30tp = new tpg::IsoMu30TPEffCalculator(outfile, "HLT_IsoMu30_eta2p1");}
		        isomu30tp->set_versioned_name(triggers_to_study["HLT_IsoMu30_eta2p1"]);
		}
		// if( triggers_to_study.find("HLT_Mu17_Ele8_CaloIdL") != triggers_to_study.end() &&!mu17ele8 && triggers_to_study["HLT_Mu17_Ele8_CaloIdL"] != "-1"){
		// 	mu17ele8 = new tpg::Mu17Ele8EffCalculator(outfile, "HLT_Mu17_Ele8_CaloIdL",triggers_to_study["HLT_Mu17_Ele8_CaloId"]);			
		// }
		if( triggers_to_study.find("HLT_Mu8_Ele17_CaloIdT_CaloIsoVL") != triggers_to_study.end() && triggers_to_study["HLT_Mu8_Ele17_CaloIdT_CaloIsoVL"] != "-1"){
			if(!mu8isoele17){mu8isoele17 = new tpg::Mu8IsoEle17EffCalculator(outfile, "HLT_Mu8_Ele17_CaloIdT_CaloIsoVL");}
			mu8isoele17->set_versioned_name(triggers_to_study["HLT_Mu8_Ele17_CaloIdT_CaloIsoVL"]);
		}
		if( triggers_to_study.find("HLT_Jet370") != triggers_to_study.end() && triggers_to_study["HLT_Jet370"] != "-1"){
			if(!jet370){jet370 = new tpg::Jet370EffCalculator(outfile, "HLT_Jet370");}
			jet370->set_versioned_name(triggers_to_study["HLT_Jet370"]);
		}

	}

	// if(isomu24 && triggers_to_study["HLT_IsoMu24"] != "-1"){
	// 	isomu24->set_handle_holder(handle_holder);
	// 	isomu24->calculate_efficiency();
	// }
	// if(mu30 && triggers_to_study["HLT_Mu30"] != "-1"){
	// 	mu30->set_handle_holder(handle_holder);
	// 	mu30->calculate_efficiency();
	// }
	if(isomu30tp && triggers_to_study["HLT_IsoMu30_eta2p1"] != "-1"){
		isomu30tp->set_handle_holder(handle_holder);
		isomu30tp->calculate_efficiency();
	}
	if(isoele65tp && triggers_to_study["HLT_Ele65_CaloIdVT_TrkIdT"] != "-1"){
		isoele65tp->set_handle_holder(handle_holder);
		isoele65tp->calculate_efficiency();
	}
	if(mu8isoele17 && triggers_to_study["HLT_Mu8_Ele17_CaloIdT_CaloIsoVL"] != "-1"){
		mu8isoele17->set_handle_holder(handle_holder);
		mu8isoele17->calculate_efficiency();
	}
	if(jet370 && triggers_to_study["HLT_Jet370"] != "-1"){
		jet370->set_handle_holder(handle_holder);
		jet370->calculate_efficiency();
	}
	// if(mu17ele8 && triggers_to_study["HLT_Mu17_Ele8_CaloIdL"] != "-1"){
	// 	mu17ele8->set_handle_holder(handle_holder);
	// 	mu17ele8->calculate_efficiency();
	// }
	
	

	// bool accepted = false;
	// for(edm::View<reco::Muon>::const_iterator mu = muons->begin();
	// 	mu != muons->end();
	// 	++mu){
	// 	accepted = match_trigger_summary_objects(*mu);
	// }

	// unsigned int trigger_id( hltConfig->triggerIndex(trigger_name) );
	// bool accepted = HLTR->accept(trigger_id);

}


// ------------ method called once each job just before starting event loop  ------------
void 
CoreTriggerOfflineDQM::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
CoreTriggerOfflineDQM::endJob() 
{
}

// ------------ method called when starting to processes a run  ------------
void 
CoreTriggerOfflineDQM::beginRun(edm::Run const&, edm::EventSetup const&)
{
}

// ------------ method called when ending the processing of a run  ------------
void 
CoreTriggerOfflineDQM::endRun(edm::Run const&, edm::EventSetup const&)
{
}

// ------------ method called when starting to processes a luminosity block  ------------
void 
CoreTriggerOfflineDQM::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
	lumi_block_changed = true;
}

// ------------ method called when ending the processing of a luminosity block  ------------
void 
CoreTriggerOfflineDQM::endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
CoreTriggerOfflineDQM::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(CoreTriggerOfflineDQM);
