#include "../interface/Mu8IsoEle17EffCalculator.h"

namespace tpg{
	tpg::Mu8IsoEle17EffCalculator::Mu8IsoEle17EffCalculator(TFile *outfile, std::string unversioned_trigger_name) : tpg::HLTEffCalculator(outfile,unversioned_trigger_name)
{
	mu_selector = new MuonSelector();
	e_selector = new ElectronSelector();

	outfile->cd();
	histos_1D[unv_trigger_name+"_Muleg1"+pt_hname+denom_suffix] = new TH1F((unv_trigger_name+"_Muleg1"+pt_hname+denom_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[unv_trigger_name+"_Muleg1"+eta_hname+denom_suffix] = new TH1F((unv_trigger_name+"_Muleg1"+eta_hname+denom_suffix).c_str(),"eta of selected offline muons", 48, -2.4, 2.4);
	histos_1D[unv_trigger_name+"_Muleg1"+pt_hname+l3num_suffix] = new TH1F((unv_trigger_name+"_Muleg1"+pt_hname+l3num_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[unv_trigger_name+"_Muleg1"+eta_hname+l3num_suffix] = new TH1F((unv_trigger_name+"_Muleg1"+eta_hname+l3num_suffix).c_str(),"eta of selected offline muons", 48, -2.4, 2.4);

	histos_1D[unv_trigger_name+"_Eleleg2"+pt_hname+denom_suffix] = new TH1F((unv_trigger_name+"_Eleleg2"+pt_hname+denom_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[unv_trigger_name+"_Eleleg2"+eta_hname+denom_suffix] = new TH1F((unv_trigger_name+"_Eleleg2"+eta_hname+denom_suffix).c_str(),"eta of selected offline muons", 50, -2.5, 2.5);
	histos_1D[unv_trigger_name+"_Eleleg2"+pt_hname+l3num_suffix] = new TH1F((unv_trigger_name+"_Eleleg2"+pt_hname+l3num_suffix).c_str(),"pt of selected offline muons", 100, 0, 100);
	histos_1D[unv_trigger_name+"_Eleleg2"+eta_hname+l3num_suffix] = new TH1F((unv_trigger_name+"_Eleleg2"+eta_hname+l3num_suffix).c_str(),"eta of selected offline muons", 50, -2.5, 2.5);
}

tpg::Mu8IsoEle17EffCalculator::~Mu8IsoEle17EffCalculator()
{
	if(mu_selector){
		delete mu_selector;
		mu_selector = NULL;
	}
	if(e_selector){
		delete e_selector;
		e_selector = NULL;
	}
}


void tpg::Mu8IsoEle17EffCalculator::calculate_efficiency()
{
	if(!handle_holder){
		std::cerr<<"Mu8IsoEle17EffCalc: NULL handle_holder"<<std::endl;
		return;
	}
	std::string l1_module = "hltL1sL1MuOpenEG12";
	std::string mul3_module = "hltL1MuOpenEG12L3Filtered8";
	std::string elel3_module = "hltL1NonIsoHLTNonIsoMu8IsoEle17PixelMatchFilter";


	//HLT/L1 efficiency measured. L1 must pass before any histograms filled.
	if(trigger_object_matcher->get_trigger_objects(l1_module)->size() < 1){ return;}


	//Select good  muons
	mu_selector->set_handle_holder(handle_holder);
	mu_selector->set_isGlobalMuon(true);
	mu_selector->set_isTrackerMuon(true);
	mu_selector->set_min_nTrackHits(11.);
	mu_selector->set_min_nPixelHits(1.);
	mu_selector->set_min_nMuonHits(1.);
	mu_selector->set_min_nMatches(2.);
	mu_selector->set_max_normChi2(10.);
	mu_selector->set_max_dxy(0.2);
	mu_selector->set_min_pt(5.0);
	
	std::vector<reco::Muon> *selected_muons = mu_selector->get_selected_muons();

	if(selected_muons->size() < 1){return;}
	
	reco::Muon muon;
	double max_mu_pt = -1;
	for(std::vector<reco::Muon>::iterator mu = selected_muons->begin();
	    mu != selected_muons->end();mu++){
		if(mu->pt() > max_mu_pt){
			max_mu_pt = mu->pt();
			muon = *mu;
		}
	}

	//Fill denominator with selected muon
	if(fabs(muon.eta()) < 2.1){
		histos_1D[unv_trigger_name+"_Muleg1"+pt_hname+denom_suffix]->Fill(muon.pt());
	}
	if(muon.pt() > 15){
		histos_1D[unv_trigger_name+"_Muleg1"+eta_hname+denom_suffix]->Fill(muon.eta());
	}

	//Match objects from l3 muon module to selected muon.
	if(trigger_object_matcher->match_trigger_objects<reco::Muon>(muon,mul3_module,0.3)){
		if(fabs(muon.eta()) < 2.1){
			histos_1D[unv_trigger_name+"_Muleg1"+pt_hname+l3num_suffix]->Fill(muon.pt());
		}
		if(muon.pt() > 15){
			histos_1D[unv_trigger_name+"_Muleg1"+eta_hname+l3num_suffix]->Fill(muon.eta());
		}
	}else{ 
		return;
	}

	//Select good electrons. WP80. https://twiki.cern.ch/twiki/bin/viewauth/CMS/EgammaWorkingPointsv3
	e_selector->set_handle_holder(handle_holder);
	e_selector->set_min_pt(10.0);

	e_selector->set_max_hadronicOverEmEB(0.04);
	e_selector->set_max_deltaPhiSuperClusterTrackAtVtxEB(0.06);
	e_selector->set_max_deltaEtaSuperClusterTrackAtVtxEB(0.004);
	e_selector->set_max_sigmaIetaIetaEB(0.01);
	e_selector->set_max_ecal_relIsoEB(0.07);
	e_selector->set_max_hcal_relIsoEB(0.10);
	e_selector->set_max_tk_relIsoEB(0.09);
	
	e_selector->set_max_hadronicOverEmEE(0.025);
	e_selector->set_max_deltaPhiSuperClusterTrackAtVtxEE(0.03);
	e_selector->set_max_deltaEtaSuperClusterTrackAtVtxEE(0.007);
	e_selector->set_max_sigmaIetaIetaEE(0.03);
	e_selector->set_max_ecal_relIsoEE(0.05);
	e_selector->set_max_hcal_relIsoEE(0.025);
	e_selector->set_max_tk_relIsoEE(0.04);

	std::vector<reco::GsfElectron> *selected_electrons = e_selector->get_selected_electrons();

	if(selected_electrons->size() < 1){return;}
	
	reco::GsfElectron electron;
	double max_e_pt = -1;
	for(std::vector<reco::GsfElectron>::iterator e = selected_electrons->begin();
	    e != selected_electrons->end();e++){
		if(e->pt() > max_e_pt){
			max_e_pt = e->pt();
			electron = *e;
		}
	}

	//Fill denominator with selected electron
	if(fabs(electron.eta()) < 2.5){
		histos_1D[unv_trigger_name+"_Eleleg2"+pt_hname+denom_suffix]->Fill(electron.pt());
	}
	if(electron.pt() > 25){
		histos_1D[unv_trigger_name+"_Eleleg2"+eta_hname+denom_suffix]->Fill(electron.eta());
	}

	//Match objects from l3 electron module to selected electron.
	if(trigger_object_matcher->match_trigger_objects<reco::GsfElectron>(electron,elel3_module,0.3)){
		if(fabs(electron.eta()) < 2.5){
			histos_1D[unv_trigger_name+"_Eleleg2"+pt_hname+l3num_suffix]->Fill(electron.pt());
		}
		if(electron.pt() > 25){
			histos_1D[unv_trigger_name+"_Eleleg2"+eta_hname+l3num_suffix]->Fill(electron.eta());
		}
	}
}
}
