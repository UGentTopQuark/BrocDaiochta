#include "../interface/DiLeptonReconstructor.h"

template int tpg::DiLeptonReconstructor::reconstruct_candidates<reco::GsfElectron>(std::vector<reco::GsfElectron> *loose_leptons, std::vector<reco::GsfElectron> *tight_leptons, std::vector<reco::GsfElectron> *loose_lepton_legs, std::vector<reco::GsfElectron> *tight_lepton_legs);
template int tpg::DiLeptonReconstructor::reconstruct_candidates<reco::Muon>(std::vector<reco::Muon> *loose_leptons, std::vector<reco::Muon> *tight_leptons, std::vector<reco::Muon> *loose_lepton_legs, std::vector<reco::Muon> *tight_lepton_legs);

tpg::DiLeptonReconstructor::DiLeptonReconstructor()
{

	min_mass = -1;
	max_mass = -1;

	allow_equal_charge = false;
}

tpg::DiLeptonReconstructor::~DiLeptonReconstructor()
{
}
void tpg::DiLeptonReconstructor::set_mass_window(double min, double max)
{
	this->min_mass = min;
	this->max_mass = max;
}

void tpg::DiLeptonReconstructor::set_allow_equal_charge(bool allow)
{
	allow_equal_charge = allow;
}


// void DiLeptonReconstructor::reconstruct_all_candidates()
// {
// 	candidates->clear();
// 	loose_electron_legs->clear();
// 	loose_muon_legs->clear();
// 	tight_electron_legs->clear();
// 	tight_muon_legs->clear();
	
// 	if(selected_lepton_id == -1 || selected_lepton_id == 11) reconstruct_candidates<reco::Electron>(loose_electrons, tight_electrons, loose_electron_legs, tight_electron_legs);
// 	if(selected_lepton_id == -1 || selected_lepton_id == 13) reconstruct_candidates<reco::Muon>(loose_muons, tight_muons, loose_muon_legs, tight_muon_legs);
// }

template <class LeptonType>
int tpg::DiLeptonReconstructor::reconstruct_candidates(std::vector<LeptonType> *loose_leptons, std::vector<LeptonType> *tight_leptons, std::vector<LeptonType> *loose_lepton_legs, std::vector<LeptonType> *tight_lepton_legs)
{
	loose_lepton_legs->clear();
	tight_lepton_legs->clear();
	std::vector<ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > > candidates;

	std::vector<bool> assigned_type1;
	std::vector<bool> assigned_type2;
	int nlep1 = 0;
	int nlep2 = 0;

	assigned_type1.assign(tight_leptons->size(), false);
	assigned_type2.assign(loose_leptons->size(), false);
	for(typename std::vector<LeptonType>::iterator lepton1 =
		tight_leptons->begin();
	    lepton1 != tight_leptons->end();
	    ++lepton1)
	{
		nlep2 = 0;
		for(typename std::vector<LeptonType>::iterator lepton2 =
			loose_leptons->begin();
		    lepton2 != loose_leptons->end();
		    ++lepton2)
		{
			// don't use the same lepton twice
			if(lepton1->pt() == lepton2->pt() &&
			   lepton1->eta() == lepton2->eta()){
				++nlep2;
				continue;
			}

			bool already_found_cand = false;
			ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > candidate = (lepton1->p4() + lepton2->p4());

			//	reco::Particle candidate(lepton1->p4() + lepton2->p4());
			for(std::vector<ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > >::iterator 
				    found_cand = candidates.begin(); found_cand != candidates.end(); found_cand++){
				if(found_cand->pt() == candidate.pt() && found_cand->eta() == candidate.eta()){
					already_found_cand = true;
					break;
				}
			}
			if(already_found_cand){
				++nlep2;
				continue;
			}

			//Check if candidate passes cuts set.
			if((allow_equal_charge || lepton1->charge() != lepton2->charge()) &&
			   ((min_mass == -1 || min_mass < candidate.mass()) &&
			    (max_mass == -1 || candidate.mass() < max_mass))&&
				!assigned_type1[nlep1] &&
			        !assigned_type2[nlep2]){
				candidates.push_back(candidate);
				tight_lepton_legs->push_back(*lepton1);
				loose_lepton_legs->push_back(*lepton2);
				assigned_type1[nlep1] = true;
				assigned_type2[nlep2] = true;
			}
			++nlep2;
		}
		++nlep1;
	}
	return candidates.size();
}
