// -*- C++ -*-
//
// Package:    CoreTriggerOfflineDQM
// Class:      CoreTriggerOfflineDQM
// 
/**\class CoreTriggerOfflineDQM CoreTriggerOfflineDQM.cc analysers/CoreTriggerOfflineDQM/src/CoreTriggerOfflineDQM.cc

 Description: Monitors efficiency of specified triggers

 Implementation: 
*/
//
// Original Author:  local user
//         Created:  Mon Aug  1 11:02:19 CEST 2011
// $Id$
//
//

#include <memory>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"

#include "DataFormats/Common/interface/View.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/MuonReco/interface/Muon.h"

#include "TFile.h"

#include "IsoMu24EffCalculator.h"
#include "Ele65EffCalculator.h"
#include "Mu17Ele8EffCalculator.h"
#include "Ele65TPEffCalculator.h"
#include "Mu8IsoEle17EffCalculator.h"
#include "IsoMu30TPEffCalculator.h"
#include "Jet370EffCalculator.h"
#include "HandleHolder.h"



//
// class declaration
//

class CoreTriggerOfflineDQM : public edm::EDAnalyzer {
        public:
	        explicit CoreTriggerOfflineDQM(const edm::ParameterSet&);
	        ~CoreTriggerOfflineDQM();
	
	        static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
	
	
        private:
	        virtual void beginJob() ;
	        virtual void analyze(const edm::Event&, const edm::EventSetup&);
	        virtual void endJob() ;
	        void reset_triggers_to_study();
	 
	        virtual void beginRun(edm::Run const&, edm::EventSetup const&);
	        virtual void endRun(edm::Run const&, edm::EventSetup const&);
	        virtual void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&);
	        virtual void endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&);

                edm::InputTag muoLabel_;
                edm::InputTag eleLabel_;
                edm::InputTag jetLabel_;
                edm::InputTag metLabel_;
                edm::InputTag BeamSpotLabel_;
	
	        std::map<std::string,std::string> triggers_to_study;
                bool lumi_block_changed;
                bool hltConfigChanged;


                TFile *outfile;

	        tpg::HandleHolder *handle_holder;
	        HLTConfigProvider *hltConfig;
	// tpg::IsoMu24EffCalculator *isomu24;
	        // tpg::Ele65EffCalculator *ele65;
	        // tpg::Mu17Ele8EffCalculator *mu17ele8;
	        tpg::Mu8IsoEle17EffCalculator *mu8isoele17;
	        tpg::Ele65TPEffCalculator *isoele65tp;
	        tpg::IsoMu30TPEffCalculator *isomu30tp;
	        tpg::Jet370EffCalculator *jet370;
};
