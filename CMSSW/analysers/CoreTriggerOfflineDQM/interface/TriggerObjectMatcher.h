#ifndef TRIGGEROBJECTMATCHER_H
#define TRIGGEROBJECTMATCHER_H

/**
 * \class TriggerObjectMatcher
 *
 * \brief Templated class, matched input CMSSW object to trigger object.
 * 
 * Finds trigger objects from input module.
 * Matches input reco object collection to found trigger objects
 * using input max_dR.
 * Starting with the highest pt reco object, find closest trigger object.
 * If dR < max_dR , reco object is saved.
 * Double counting of trigger objects or reco objects is not allowed.
 * Returns vector of matched reco objects.
 *
 **/

#include "TLorentzVector.h"
#include "Math/VectorUtil.h"
#include "HandleHolder.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "DataFormats/JetReco/interface/Jet.h"

namespace tpg{
	class TriggerObjectMatcher{
                public:
		        TriggerObjectMatcher();
			~TriggerObjectMatcher();

			void set_handle_holder(HandleHolder *handle_holder);
			/***
			 * This function does the matching.
			 * give as input 1/ the cmssw objects you want to match,
			 * 2/ the trigger module name, from which it will get the trigger objects,
			 * and 3/ the dR you want to use for matching.
			 ***/
			template<class CMSSWObject>
			bool match_trigger_objects(CMSSWObject cmssw_object,std::string trigger_module,double max_dR);
			template<class CMSSWObject>
			std::vector<CMSSWObject>* match_trigger_objects(std::vector<CMSSWObject> *cmssw_objects,std::string trigger_module,double max_dR);

			std::vector<trigger::TriggerObject>* get_trigger_objects(std::string module);
                private:                   
			edm::Handle<trigger::TriggerEvent> aodTriggerEvent;
                        edm::Handle<edm::TriggerResults> HLTR;
			
			HLTConfigProvider *hltConfig;
			tpg::HandleHolder *handle_holder;

			std::vector<trigger::TriggerObject> *trigger_objects;
	};
}
#endif

