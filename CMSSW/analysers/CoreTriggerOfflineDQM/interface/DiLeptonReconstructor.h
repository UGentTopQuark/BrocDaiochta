#ifndef TPG_DILEPTONRECONSTRUCTOR_H
#define TPG_DILEPTONRECONSTRUCTOR_H

#include <vector>
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "HandleHolder.h"

namespace tpg{
	class DiLeptonReconstructor{
	public:
		DiLeptonReconstructor();
		~DiLeptonReconstructor();
		
		void set_handle_holder(tpg::HandleHolder *handle_holder);

		void set_mass_window(double min, double max);
		void set_allow_equal_charge(bool allow);
		template <class LeptonType> int reconstruct_candidates(std::vector<LeptonType> *loose_leptons, std::vector<LeptonType> *tight_leptons, std::vector<LeptonType> *loose_lepton_legs, std::vector<LeptonType> *tight_lepton_legs);
	private:		
		double min_mass;
		double max_mass;
		
		bool allow_equal_charge;
		tpg::HandleHolder *handle_holder;
	};
}

#endif
