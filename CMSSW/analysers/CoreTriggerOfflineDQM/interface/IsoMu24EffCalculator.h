#ifndef ISOMU24EFFCALCULATOR_H
#define ISOMU24EFFCALCULATOR_H
/**
 * \class IsoMu24EffCalculator
 *
 * \brief HLT_IsoMu24 Efficiency for isolated reco muons
 * 
 * Selects good isolated reco muons.  
 * Matches selected muon to IsoMu24 trigger objects (if available)
 * Efficiency = matched muons/selected muons
 *
 **/

#include "HLTEffCalculator.h"

namespace tpg{
	class IsoMu24EffCalculator: public HLTEffCalculator{
                public:
		IsoMu24EffCalculator(TFile *outfile,std::string unversioned_trigger_name);
			~IsoMu24EffCalculator();

			void calculate_efficiency();
			
                private:
			MuonSelector *mu_selector;
	};
}
#endif

