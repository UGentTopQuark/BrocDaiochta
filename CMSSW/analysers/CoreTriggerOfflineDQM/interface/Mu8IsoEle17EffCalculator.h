#ifndef MU8ISOELE17EFFCALCULATOR_H
#define MU8ISOELE17EFFCALCULATOR_H

/**
 * \class Mu8IsoEle17EffCalculator
 *
 * \brief HLT_Mu8_Ele17_CaloIdT_CaloIsoVL/L1 Efficiency
 * 
 * No L1 efficiency is measured.
 * If L1 seed passed select good muon.
 * Use highest pt selected muon for mu leg denominator.
 * L3Mu/L1 efficiency measured
 * If L3 mu passed select good electrons.
 * Use highest pt selected electron for ele leg denominator.
 * Assuming mu and ele uncorrelated L3Ele/L3Mu == L3Ele/L1
 * L3Mu/L1 efficiency should be taken from JetPD.
 * L3Ele/L1 efficiency should be taken from MuPD.
 *
 **/

#include "HLTEffCalculator.h"

namespace tpg{
	class Mu8IsoEle17EffCalculator: public HLTEffCalculator{
                public:
		Mu8IsoEle17EffCalculator(TFile *outfile,std::string unversioned_trigger_name);
			~Mu8IsoEle17EffCalculator();

			void calculate_efficiency();
			
                private:
			MuonSelector *mu_selector;
			ElectronSelector *e_selector;
	};
}
#endif

