#ifndef ISOMU30TPEFFCALCULATOR_H
#define ISOMU30TPEFFCALCULATOR_H
/**
 * \class IsoMu30TPEffCalculator
 *
 * \brief IsoMu30 Efficiency for isolated reco muons
 * 
 * Selects good reco muons.
 * Reconstructs Z mass.  
 * Matches tag muon to IsoMu30 trigger objects (if available)
 * If matched fills probe muon information.
 * Efficiency = matched probe muons/selected probe muons
 *
 **/

#include "HLTEffCalculator.h"

namespace tpg{
	class IsoMu30TPEffCalculator: public HLTEffCalculator{
                public:
		        IsoMu30TPEffCalculator(TFile *outfile,std::string unversioned_trigger_name);
			~IsoMu30TPEffCalculator();

			void calculate_efficiency();
			
                private:
			MuonSelector *mu_selector;
			tpg::DiLeptonReconstructor *dilep_reconstructor;
	};
}
#endif

