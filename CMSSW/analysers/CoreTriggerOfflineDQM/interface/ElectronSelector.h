#ifndef ELECTRONSELECTOR_H
#define ELECTRONSELECTOR_H

/**
 * \class ElectronSelector
 *
 * \brief Applies electrons selection
 * 
 * Fills vector of selected electrons.
 * Selection defined by user.
 *
 **/

#include "ObjectSelector.h"

namespace tpg{
	class ElectronSelector:public ObjectSelector{
                public:
		        ElectronSelector();
			~ElectronSelector();
			
			std::vector<reco::GsfElectron>* get_selected_electrons();

			/***
			    Here you should include any cuts which 
			    are electron specific.
			***/
			inline void set_max_relIso(double val) {max_relIso = val;}

			inline void set_max_dr03TkSumPtEB(double val) {max_dr03TkSumPtEB = val;}
			inline void set_max_dr03EcalRecHitSumEtEB(double val) {max_dr03EcalRecHitSumEtEB = val;}
			inline void set_max_dr03HcalTowerSumEtEB(double val) {max_dr03HcalTowerSumEtEB = val;}
			inline void set_max_hadronicOverEmEB(double val) {max_hadronicOverEmEB = val;}
			inline void set_max_deltaPhiSuperClusterTrackAtVtxEB(double val) {max_deltaPhiSuperClusterTrackAtVtxEB = val;}
			inline void set_max_deltaEtaSuperClusterTrackAtVtxEB(double val) {max_deltaEtaSuperClusterTrackAtVtxEB = val;}
			inline void set_max_sigmaIetaIetaEB(double val) {max_sigmaIetaIetaEB = val;}
			inline void set_min_sigmaIetaIetaSpikesEB(double val) {min_sigmaIetaIetaSpikesEB = val;}
			inline void set_max_ecal_relIsoEB(double val) {max_ecal_relIsoEB = val;}
			inline void set_max_hcal_relIsoEB(double val) {max_hcal_relIsoEB = val;}
			inline void set_max_tk_relIsoEB(double val) {max_tk_relIsoEB = val;}

			inline void set_max_dr03TkSumPtEE(double val) {max_dr03TkSumPtEE = val;}
			inline void set_max_dr03EcalRecHitSumEtEE(double val) {max_dr03EcalRecHitSumEtEE = val;}
			inline void set_max_dr03HcalTowerSumEtEE(double val) {max_dr03HcalTowerSumEtEE = val;}
			inline void set_max_hadronicOverEmEE(double val) {max_hadronicOverEmEE = val;}
			inline void set_max_deltaPhiSuperClusterTrackAtVtxEE(double val) {max_deltaPhiSuperClusterTrackAtVtxEE = val;}
			inline void set_max_deltaEtaSuperClusterTrackAtVtxEE(double val) {max_deltaEtaSuperClusterTrackAtVtxEE = val;}
			inline void set_max_sigmaIetaIetaEE(double val) {max_sigmaIetaIetaEE = val;}
			inline void set_min_sigmaIetaIetaSpikesEE(double val) {min_sigmaIetaIetaSpikesEE = val;}
			inline void set_max_ecal_relIsoEE(double val) {max_ecal_relIsoEE = val;}
			inline void set_max_hcal_relIsoEE(double val) {max_hcal_relIsoEE = val;}
			inline void set_max_tk_relIsoEE(double val) {max_tk_relIsoEE = val;}
			
                private:
			bool is_good_electron(reco::GsfElectron);

			std::vector<reco::GsfElectron> *selected_electrons;
		
			double max_relIso; 

			double max_dr03TkSumPtEB;
			double max_dr03EcalRecHitSumEtEB;
			double max_dr03HcalTowerSumEtEB;
			double max_hadronicOverEmEB;
			double max_deltaPhiSuperClusterTrackAtVtxEB;
			double max_deltaEtaSuperClusterTrackAtVtxEB;
			double max_sigmaIetaIetaEB;
			double min_sigmaIetaIetaSpikesEB;
			double max_ecal_relIsoEB; 
			double max_hcal_relIsoEB; 
			double max_tk_relIsoEB; 

			double max_dr03TkSumPtEE;
			double max_dr03EcalRecHitSumEtEE;
			double max_dr03HcalTowerSumEtEE;
			double max_hadronicOverEmEE;
			double max_deltaPhiSuperClusterTrackAtVtxEE;
			double max_deltaEtaSuperClusterTrackAtVtxEE;
			double max_sigmaIetaIetaEE;
			double min_sigmaIetaIetaSpikesEE;
			double max_ecal_relIsoEE; 
			double max_hcal_relIsoEE; 
			double max_tk_relIsoEE; 


	};
}
#endif

