#ifndef MUONSELECTOR_H
#define MUONSELECTOR_H

/**
 * \class MuonSelector
 *
 * \brief Applies muons selection
 * 
 * Fills vector of selected muons.
 * Selection defined by user.
 *
 **/

#include "ObjectSelector.h"

namespace tpg{
	class MuonSelector:public ObjectSelector{
                public:
		        MuonSelector();
			~MuonSelector();
			
			std::vector<reco::Muon>* get_selected_muons();

			/***
			    Here you should include any cuts which 
			    are muon specific.
			***/
			inline void set_min_nTrackHits(double val) {min_nTrackHits = val;}
			inline void set_min_nPixelHits(double val) {min_nPixelHits = val;}
			inline void set_min_nMuonHits(double val) {min_nMuonHits = val;}
			inline void set_min_nMatches(double val) {min_nMatches = val;}
			inline void set_max_normChi2(double val) {max_normChi2 = val;}
			inline void set_max_dxy(double val) {max_dxy = val;}
			inline void set_max_relIso(double val) {max_relIso = val;}
			inline void set_isGlobalMuon(bool val) {isGlobalMuon = val;}
			inline void set_isTrackerMuon(bool val) {isTrackerMuon = val;}
			
                private:
			bool is_good_muon(reco::Muon);

			std::vector<reco::Muon> *selected_muons;
		
			double min_nTrackHits;
			double min_nPixelHits;
			double min_nMuonHits;
			double min_nMatches;
			double max_normChi2;
			double max_dxy;
			double max_relIso; //< (trackiso+caloiso)/pt
			bool isGlobalMuon;
			bool isTrackerMuon;

	};
}
#endif

