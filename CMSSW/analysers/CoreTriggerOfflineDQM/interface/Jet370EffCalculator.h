#ifndef JET370EFFCALCULATOR_H
#define JET370EFFCALCULATOR_H
/**
 * \class Jet370EffCalculator
 *
 * \brief HLT_Jet370_CaloIdVT_TrkIdT Efficiency for isolated reco electrons
 * 
 * Selects good reco electrons.  
 * Matches selected electron to Jet370 trigger objects (if available)
 * Efficiency = matched electrons/selected electrons
 *
 **/

#include "HLTEffCalculator.h"

namespace tpg{
	class Jet370EffCalculator: public HLTEffCalculator{
                public:
		        Jet370EffCalculator(TFile *outfile,std::string unversioned_trigger_name);
			~Jet370EffCalculator();

			void calculate_efficiency();
			
                private:
			PFJetSelector *jet_selector;
	};
}
#endif

