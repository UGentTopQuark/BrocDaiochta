#ifndef PFJETSELECTOR_H
#define PFJETSELECTOR_H

/**
 * \class JetSelector
 *
 * \brief Applies PF jets selection
 * 
 * Fills vector of selected PF jets.
 * Selection defined by user.
 *
 **/

#include "ObjectSelector.h"

namespace tpg{
	class PFJetSelector:public ObjectSelector{
                public:
		        PFJetSelector();
			~PFJetSelector();
			
			std::vector<reco::PFJet>* get_selected_jets();

			/***
			    Here you should include any cuts which 
			    are jet specific.
			***/
			inline void set_min_ndaughters(double val) {min_ndaughters = val;}
			inline void set_min_chf(double val) {min_chf = val;}
			inline void set_max_nhf(double val) {max_nhf = val;}
			inline void set_max_nemf(double val) {max_nemf = val;}
			inline void set_max_cemf(double val) {max_cemf = val;}
			inline void set_min_cmulti(double val) {min_cmulti = val;}

			
                private:
			bool is_good_jet(reco::PFJet);

			std::vector<reco::PFJet> *selected_jets;
		
		        double min_ndaughters;
			double min_chf;
			double max_nhf;
			double max_nemf;
			double max_cemf;
			double min_cmulti;
	};
}
#endif

