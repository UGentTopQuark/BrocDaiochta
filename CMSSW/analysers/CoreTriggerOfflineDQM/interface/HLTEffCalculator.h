#ifndef HLTEFFCALCULATOR_H
#define HLTEFFCALCULATOR_H

/**
 * \class HLTEffCalculator
 *
 * \brief Parent class for efficiency calculators
 * 
 * Defines functions and classes to be used be all 
 * efficiency calculators.
 *
 **/

#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/METReco/interface/MET.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "MuonSelector.h"
#include "ElectronSelector.h"
#include "PFJetSelector.h"
#include "TriggerObjectMatcher.h"
#include "DiLeptonReconstructor.h"
#include "HandleHolder.h"

#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"

namespace tpg{
	class HLTEffCalculator{
                public:
		        HLTEffCalculator(TFile *outfile, std::string unversioned_trigger_name);
			~HLTEffCalculator();
			
			void set_handle_holder(HandleHolder *handle_holder);
			void set_versioned_name(std::string name);
			virtual void calculate_efficiency();
			
                protected:
			TFile *outfile;

			std::string unv_trigger_name;
			std::string trigger_name;
			std::string pt_hname; 
			std::string eta_hname; 
			std::string denom_suffix;
			std::string l3num_suffix;
			std::string l1num_suffix;
			
			std::map<std::string,TH1F*> histos_1D;
			
			tpg::HandleHolder *handle_holder;
			tpg::TriggerObjectMatcher *trigger_object_matcher;
	};
}
#endif


