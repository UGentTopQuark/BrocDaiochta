#ifndef OBJECTSELECTOR_H
#define OBJECTSELECTOR_H
/**
 * \class ObjectSelector
 *
 * \brief Parent class for object selectors.
 * 
 * Defines functions and classes to be used be all 
 * object selectors.
 *
 **/

#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/METReco/interface/MET.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "HandleHolder.h"



namespace tpg{
	class ObjectSelector{
                public:
		        ObjectSelector();
			~ObjectSelector();
			
			void set_handle_holder(HandleHolder *handle_holder);

			///Here you should include any cuts which are relevant 
			// all objects. Object specific cuts should be defined 
			// in daughter class.
			inline void set_min_pt(double val) {min_pt = val;}
			inline void set_max_eta(double val) {max_eta = val;}
			
                protected:
			double min_pt;
			double max_eta;
			
			tpg::HandleHolder *handle_holder;
	};
}
#endif

