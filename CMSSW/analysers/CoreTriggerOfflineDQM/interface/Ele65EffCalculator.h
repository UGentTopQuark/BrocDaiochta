#ifndef ELE65EFFCALCULATOR_H
#define ELE65EFFCALCULATOR_H
/**
 * \class Ele65EffCalculator
 *
 * \brief HLT_Ele65_CaloIdVT_TrkIdT Efficiency for isolated reco electrons
 * 
 * Selects good reco electrons.  
 * Matches selected electron to Ele65 trigger objects (if available)
 * Efficiency = matched electrons/selected electrons
 *
 **/

#include "HLTEffCalculator.h"

namespace tpg{
	class Ele65EffCalculator: public HLTEffCalculator{
                public:
		        Ele65EffCalculator(TFile *outfile,std::string unversioned_trigger_name);
			~Ele65EffCalculator();

			void calculate_efficiency();
			
                private:
			ElectronSelector *e_selector;
	};
}
#endif

