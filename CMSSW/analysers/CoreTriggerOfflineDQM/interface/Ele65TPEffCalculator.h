#ifndef ELE65TPEFFCALCULATOR_H
#define ELE65TPEFFCALCULATOR_H
/**
 * \class Ele65TPEffCalculator
 *
 * \brief HLT_Ele65_CaloIdVT_TrkIdT Efficiency for isolated reco electrons
 * 
 * Selects good reco electrons.
 * Reconstructs Z mass.  
 * Matches tag electron to Ele65 trigger objects (if available)
 * If matched fills probe electron information.
 * Efficiency = matched probe electrons/selected probe electrons
 *
 **/

#include "HLTEffCalculator.h"

namespace tpg{
	class Ele65TPEffCalculator: public HLTEffCalculator{
                public:
		        Ele65TPEffCalculator(TFile *outfile,std::string unversioned_trigger_name);
			~Ele65TPEffCalculator();

			void calculate_efficiency();
			
                private:
			ElectronSelector *e_selector;
			tpg::DiLeptonReconstructor *dilep_reconstructor;
	};
}
#endif

