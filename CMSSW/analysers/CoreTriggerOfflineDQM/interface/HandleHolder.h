#ifndef HANDLEHOLDER_H
#define HANDLEHOLDER_H

/**
 * \class HandleHolder
 *
 * \brief Holds edm::Handles  
 * 
 * Simplifies passing of handles to 
 * multiple classes
 * 
 *
 **/

#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/JetReco/interface/PFJet.h"
#include "DataFormats/METReco/interface/MET.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/Common/interface/TriggerResults.h"


namespace tpg{
	class HandleHolder{
                public:
		        HandleHolder();
			~HandleHolder();
			
			inline void set_muonHandle(edm::Handle<edm::View<reco::Muon> > muonHandle){this->muonHandle = muonHandle;}
			inline void set_electronHandle(edm::Handle<edm::View<reco::GsfElectron> > electronHandle){this->electronHandle = electronHandle;}
			inline void set_jetHandle(edm::Handle<edm::View<reco::PFJet> > jetHandle){this->jetHandle = jetHandle;}
			inline void set_metHandle(edm::Handle<edm::View<reco::MET> > metHandle){this->metHandle = metHandle;}
			
			inline void set_beamSpotHandle(edm::Handle<reco::BeamSpot> beamSpotHandle){this->beamSpotHandle = beamSpotHandle;}
			
			inline void set_hltConfig(HLTConfigProvider *hltConfig){this->hltConfig = hltConfig;}
			inline void set_aodTriggerEvent(edm::Handle<trigger::TriggerEvent> aodTriggerEvent){this->aodTriggerEvent = aodTriggerEvent;}
			inline void set_HLTR(edm::Handle<edm::TriggerResults> HLTR){this->HLTR = HLTR;}
			inline void set_run_number(double run){run_number = run;}

			inline edm::Handle<edm::View<reco::Muon> > get_muonHandle(){return muonHandle;}
			inline edm::Handle<edm::View<reco::GsfElectron> > get_electronHandle(){return electronHandle;}
			inline edm::Handle<edm::View<reco::PFJet> > get_jetHandle(){return jetHandle;}
			inline edm::Handle<edm::View<reco::MET> > get_metHandle(){return metHandle;}
			
			inline edm::Handle<reco::BeamSpot> get_beamSpotHandle(){return beamSpotHandle;}
			
			inline HLTConfigProvider* get_hltConfig(){return  hltConfig;}
			inline edm::Handle<trigger::TriggerEvent> get_aodTriggerEvent(){return aodTriggerEvent;}
			inline edm::Handle<edm::TriggerResults> get_HLTR(){return HLTR;}
			inline double get_run_number(){return run_number;}
			
                private:
			
			edm::Handle<edm::View<reco::Muon> > muonHandle;
			edm::Handle<edm::View<reco::GsfElectron> > electronHandle;
			edm::Handle<edm::View<reco::PFJet> > jetHandle;
			edm::Handle<edm::View<reco::MET> > metHandle;
			double run_number;

                        edm::Handle<reco::BeamSpot> beamSpotHandle;

                        HLTConfigProvider *hltConfig;
                        edm::Handle<trigger::TriggerEvent> aodTriggerEvent;
                        edm::Handle<edm::TriggerResults> HLTR;


	};
}
#endif
