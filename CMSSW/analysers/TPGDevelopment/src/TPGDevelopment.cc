#include "../interface/TPGDevelopment.h"

TPGDevelopment::TPGDevelopment(const edm::ParameterSet& iConfig)

{
	hltConfig = new HLTConfigProvider();

	set_trigger_menu = "HLT";
	trigger_name = "HLT_IsoMu17_v8";
	lumi_block_changed = true;
}


TPGDevelopment::~TPGDevelopment()
{
	if(hltConfig){ delete hltConfig; hltConfig = NULL; }
}

bool TPGDevelopment::filter(edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	using namespace edm;

	double run = iEvent.run();

	if (run < 163269)
	        trigger_name =  "HLT_IsoMu17_v5";
	else if (run >= 163269 && run < 165071)
	        trigger_name =  "HLT_IsoMu17_v6";
	else if (run >= 165071 && run < 165970)
	        trigger_name =  "HLT_IsoMu17_v8";
	else if (run >= 165970)
	        trigger_name =  "HLT_IsoMu17_v9";

	edm::Handle<edm::TriggerResults> HLTR;
	iEvent.getByLabel("TriggerResults",HLTR);

	iEvent.getByLabel("hltTriggerSummaryAOD", aodTriggerEvent);

/*
	std::vector<std::string> saved_tags; 
	if(lumi_block_changed){
		hltConfig->init(iEvent.getRun(), iSetup, set_trigger_menu, hltConfigChanged);
		lumi_block_changed = false;
		
		std::cout << " -------- " << std::endl;
		std::cout << " Run: " << iEvent.run() << std::endl;
		std::cout << "Trigger menu update. List of available triggers: " << std::endl;
		std::vector<std::string> valid_triggers = hltConfig->triggerNames();
		for(std::vector<std::string>::iterator valid_trigger = valid_triggers.begin();
			valid_trigger != valid_triggers.end();
			++valid_trigger){
			std::cout << *valid_trigger << std::endl;
		}
		std::cout << " -------- " << std::endl;
		std::cout << "getting saved tags..." << std::endl;
		saved_tags = hltConfig->saveTagsModules(trigger_name);

		std::cout << "saved tags for " << trigger_name << ":" << std::endl;
		for(std::vector<std::string>::iterator saved_tag = saved_tags.begin();
			saved_tag != saved_tags.end();
			++saved_tag){
			std::cout << *saved_tag << std::endl;
		}
		std::cout << " -------- " << std::endl;
	}

	if(trigger_name != "-1"){
		if(!HLTR.isValid()){
			std::cout << "WARNING: no valid HLTR" << std::endl;
			return false;
		}
	}

       	edm::Handle<edm::View<reco::Muon> > muons;
       	iEvent.getByLabel("muons",muons);

	bool accepted = false;
	for(edm::View<reco::Muon>::const_iterator mu = muons->begin();
		mu != muons->end();
		++mu){
		accepted = match_trigger_summary_objects(*mu);
	}
*/

	unsigned int trigger_id( hltConfig->triggerIndex(trigger_name) );
	bool accepted = HLTR->accept(trigger_id);

	return accepted;
}

bool TPGDevelopment::match_trigger_summary_objects(reco::Muon mu)
{
	bool found = false;

	std::vector<std::string> module_names = hltConfig->moduleLabels(trigger_name);

	std::vector<trigger::TriggerObject> l1_found;
	std::vector<trigger::TriggerObject> hlt_found;

	std::string l1_module;
	std::string hlt_filter;

	
	//Loop over all modules. Find names corresponding to l1 and hlt
	for(std::vector<std::string>::iterator module = module_names.begin();
	    module != module_names.end(); module++){

		if(hltConfig->moduleType(*module) == "HLTLevel1GTSeed" )
			l1_module = *module;			
		if(!TString(*module).Contains("hltBoolEnd"))
			hlt_filter = *module;
	}

	//fill trigger objects
	l1_found = get_trigger_summary_objects(l1_module);
	hlt_found = get_trigger_summary_objects(hlt_filter);

	//match to hlt
	for(std::vector<trigger::TriggerObject>::iterator hlt_obj = hlt_found.begin();
	    hlt_obj != hlt_found.end();hlt_obj++){

		ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > hlt;
		hlt.SetPxPyPzE(hlt_obj->px(),hlt_obj->py(),hlt_obj->pz(),hlt_obj->energy());

		double dR1 = ROOT::Math::VectorUtil::DeltaR(hlt,mu.p4());
		if(dR1 < 0.2 && mu.pt() < 10.){
			found = true;
			return found;
		}
	}

	return found;
}

std::vector<trigger::TriggerObject> TPGDevelopment::get_trigger_summary_objects(std::string module)
{
	const trigger::TriggerObjectCollection objects = aodTriggerEvent->getObjects();

	std::vector<trigger::TriggerObject> found_objects;

        //Now use module name to get keys for trigger objects
        edm::InputTag filterTag = edm::InputTag(module, "", set_trigger_menu);

        int filterIndex = aodTriggerEvent->filterIndex(filterTag);
        if ( filterIndex < aodTriggerEvent->sizeFilters() ) {
                //These trigger keys point you to the trigger objects passing this filter in the objects collection
                const trigger::Keys &keys = aodTriggerEvent->filterKeys( filterIndex );
                        for ( size_t i = 0; i < keys.size(); i++ ){
				found_objects.push_back(objects[keys[i]]);
			}
        }

	return found_objects;
}



// ------------ method called once each job just before starting event loop  ------------
void 
TPGDevelopment::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
TPGDevelopment::endJob() 
{
}

// ------------ method called when starting to processes a run  ------------
void 
TPGDevelopment::beginRun(edm::Run const&, edm::EventSetup const&)
{
}

// ------------ method called when ending the processing of a run  ------------
void 
TPGDevelopment::endRun(edm::Run const&, edm::EventSetup const&)
{
}

// ------------ method called when starting to processes a luminosity block  ------------
void 
TPGDevelopment::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
	lumi_block_changed = true;
}

// ------------ method called when ending the processing of a luminosity block  ------------
void 
TPGDevelopment::endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
TPGDevelopment::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
}

//define this as a plug-in
DEFINE_FWK_MODULE(TPGDevelopment);
