import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.Services_cff')

process.GlobalTag.globaltag = "GR_R_42_V14::All"

## Options and Output Report
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(100) )

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
        'file:////user/bklein/single_mu_2011Av4.root'
    )
)

process.demo = cms.EDFilter('TPGDevelopment'
)

process.p1 = cms.Path(process.demo)

process.out = cms.OutputModule("PoolOutputModule",
	outputCommands = cms.untracked.vstring('keep *'),
    SelectEvents = cms.untracked.PSet(
    	SelectEvents = cms.vstring('p1')
    ),
    fileName = cms.untracked.string('Wskim.root')
)

process.outpath = cms.EndPath(process.out)
