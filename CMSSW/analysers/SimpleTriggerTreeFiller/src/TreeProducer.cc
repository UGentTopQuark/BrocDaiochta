#include "../interface/TreeProducer.h"

TreeProducer::TreeProducer(TFile *outfile)
{
	directory_name = "lepEffs";
	std::string tree_name = "fitter_tree";

	this->outfile = outfile; 
	outfile->cd();
	outfile->mkdir(directory_name.c_str());
	outfile->Cd(directory_name.c_str());
	tree = new TTree(tree_name.c_str(),tree_name.c_str());

	run =-1;
	lumi=-1;
	event_number=-1;

	reset_variables();
	book_branches();

}

TreeProducer::~TreeProducer()
{
	if(outfile){
		outfile->cd();
		outfile->Cd(directory_name.c_str());
		tree->Write();
	}
}

void TreeProducer::reset_variables()
{

        pt=0;
        mass=0;
        eta=0;
        phi=0;
        abseta=0;
        mindR = 0;
        njets=0;
        reliso=0;
        ecaliso=0;
        hcaliso=0;
        trackiso=0;
        combcaloiso=0;
        npvertices=0;

	chi2=0;
	d0=0;
	nmuon_hits=0;
	npixel_layers=0;
	nmatched_segments=0;

        passing_idx = 0;
        passing_l1 = 0;
        passing_hltwrtl1 = 0;


}

void TreeProducer::book_branches()
{
	tree->Branch("pt",&pt, "pt/F");
	tree->Branch("eta",&eta, "eta/F");
	tree->Branch("phi",&phi, "phi/F");
	tree->Branch("mass",&mass, "mass/F");
	tree->Branch("njets",&njets, "njets/F");
	tree->Branch("abseta",&abseta, "abseta/F");
	tree->Branch("reliso",&reliso, "reliso/F");
	tree->Branch("ecaliso",&ecaliso, "ecaliso/F");
	tree->Branch("hcaliso",&hcaliso, "hcaliso/F");
	tree->Branch("trackiso",&trackiso, "trackiso/F");
	tree->Branch("combcaloiso",&combcaloiso, "combcaloiso/F");
	tree->Branch("mindR",&mindR, "mindR/F");
	tree->Branch("npvertices",&npvertices, "npvertices/F");
	tree->Branch("nhits",&nhits, "nhits/F");
	tree->Branch("chi2",&chi2, "chi2/F");
	tree->Branch("d0",&d0, "d0/F");
	tree->Branch("nmuon_hits",&nmuon_hits, "nmuon_hits/F");
	tree->Branch("npixel_layers",&npixel_layers, "npixel_layers/F");
	tree->Branch("nmatched_segments",&nmatched_segments, "nmatched_segments/F");

	tree->Branch("passing_idx",&passing_idx,"passing_idx/I");
	tree->Branch("passing_l1",&passing_l1,"passing_l1/I");
	tree->Branch("passing_hltwrtl1",&passing_hltwrtl1,"passing_hltwrtl1/I");



}

void TreeProducer::set_summary_results(std::vector<int> results)
{
	if(verbose)std::cout << "TreeProducer: set_summary_results" << std::endl;
	trigger_summary_res = results;
}

void TreeProducer::set_nselected_pvertices(int npv)
{
	nselected_pvertices = npv;
}
void TreeProducer::set_event_information(double r,double l,double e)
{
	run = r;
	lumi = l;
	event_number = e;
}

void TreeProducer::set_beamspot(edm::Handle<reco::BeamSpot> BeamSpot)
{
	this->BeamSpot = BeamSpot;
}


void TreeProducer::fill_branches(reco::Muon mu1,reco::Muon mu2)
{
	if(verbose) std::cout << "TreeProducer: Filling branches " << std::endl;
	int summary_res1 = 0,summary_res2 = 0;
	if(trigger_summary_res.size() == 2){
		summary_res1 = trigger_summary_res[0];
		summary_res2 = trigger_summary_res[1];
		if(verbose) std::cout << "TreeProducer: trigger summary " << summary_res1 << " " << summary_res2 << std::endl;
	}

	if(summary_res1 == 2)
		fill_branches_for_muon(mu2,mu1,summary_res2);

	if(summary_res2 == 2)
		fill_branches_for_muon(mu1,mu2,summary_res1);

	trigger_summary_res.clear();
	run =-1;
	lumi=-1;
	event_number=-1;
}

void TreeProducer::fill_branches_for_muon(reco::Muon mu,reco::Muon other_mu,int summary_res)
{	
	if(verbose) std::cout << "TreeProducer: Filling branches for muon" << std::endl;
	reset_variables();
	
	pt = mu.pt();
	eta = mu.eta();
        phi = mu.phi();
        abseta = std::abs(mu.eta());

        reliso = ((mu.isolationR03().emEt + mu.isolationR03().hadEt + mu.isolationR03().sumPt)/mu.pt());
        ecaliso=mu.isolationR03().emEt;
        hcaliso=mu.isolationR03().hadEt;
        trackiso=mu.isolationR03().sumPt;
        combcaloiso= (mu.isolationR03().emEt + mu.isolationR03().hadEt);

	if(verbose) std::cout << "TreeProducer: filled basic kinematics" << std::endl;
	npvertices = (int32_t) nselected_pvertices;
	reco::TrackRef track = mu.innerTrack();
	nhits = (int32_t) track->numberOfValidHits();
	reco::TrackRef global_track = mu.globalTrack();
	chi2 = global_track->chi2() / global_track->ndof();
	reco::BeamSpot beamSpot=*(BeamSpot);
	math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
	d0 = -1.*track->dxy(point);
	reco::HitPattern global_pattern = global_track->hitPattern();
	nmuon_hits = global_pattern.numberOfValidMuonHits();
	npixel_layers = global_pattern.pixelLayersWithMeasurement();
	nmatched_segments =  mu.numberOfMatches();
	if(verbose) std::cout << "TreeProducer: filled extra kinematics" << std::endl;
	
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > candidate = (mu.p4() + other_mu.p4());
	mass = candidate.mass();
	if(verbose) std::cout << "TreeProducer: filled mass" << std::endl;

	if(summary_res >= 1) passing_l1 = 1;
	else passing_l1 = 0;
	if(summary_res == 2) passing_idx = 1;
	else passing_idx = 0;
	if(passing_l1 && !passing_idx)
		passing_hltwrtl1 = 0;
	else if(passing_l1 && passing_idx)
		passing_hltwrtl1 = 1;
	else
		passing_hltwrtl1 = -1;

        tree->Fill();
}
