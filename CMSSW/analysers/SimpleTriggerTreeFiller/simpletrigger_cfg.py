import FWCore.ParameterSet.Config as cms
import FWCore.Utilities.FileUtils as FileUtils

from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAny_cfi import *
from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAlong_cfi import *
from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorOpposite_cfi import *
from RecoMuon.DetLayers.muonDetLayerGeometry_cfi import *

process = cms.Process("Demo")

process.load("Configuration.StandardSequences.Geometry_cff")
process.load("Configuration.StandardSequences.MagneticField_cff")
process.load("Configuration.StandardSequences.Reconstruction_cff")
process.load("Configuration.StandardSequences.Services_cff")

process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
process.GlobalTag.globaltag = 'GR_R_52_V1::All'
#process.GlobalTag.globaltag = 'START42_V13::All'
process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(100) )


#myLumiRanges = FileUtils.loadListFromFile('jsonfiles/Dec22_rereco_json.txt')
#lumiRanges =  cms.untracked.vstring(*myLumiRanges)

# JSON files in cmssw:
# copy from _CMSSW json file according section for selection:
# eg. https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions10/7TeV/Reprocessing/Cert_139779-140159_7TeV_July16thReReco_Collisions10_COMSSWConfig.txt

#myFileNames = FileUtils.loadListFromFile('filepaths/Dec22_WZSkim_20110211.txt')
#myFileNames = FileUtils.loadListFromFile('filepaths/DYToLL_TuneD6T_WZSkim_20110225.txt')
#input_filenames =  cms.untracked.vstring(*myFileNames)

process.source = cms.Source("PoolSource",
#     fileNames = input_filenames                       
    fileNames = cms.untracked.vstring(
    'rfio:////castor/cern.ch/user/w/walsh/20120207_ZMuSkim_2011B_ReHLT/specialHLT_rereco_trocino-SingleMu_Run2011B_ZMu_5e33_520_RawRecoHlt_v1_97.root',
    'rfio:////castor/cern.ch/user/w/walsh/20120207_ZMuSkim_2011B_ReHLT/specialHLT_rereco_trocino-SingleMu_Run2011B_ZMu_5e33_520_RawRecoHlt_v1_98.root',
    'rfio:////castor/cern.ch/user/w/walsh/20120207_ZMuSkim_2011B_ReHLT/specialHLT_rereco_trocino-SingleMu_Run2011B_ZMu_5e33_520_RawRecoHlt_v1_99.root',
    'rfio:////castor/cern.ch/user/w/walsh/20120207_ZMuSkim_2011B_ReHLT/specialHLT_rereco_trocino-SingleMu_Run2011B_ZMu_5e33_520_RawRecoHlt_v1_100.root',

    )
)

process.produceTrees = cms.EDAnalyzer('SimpleTriggerTreeFiller',
       	muonTag     = cms.untracked.InputTag("muons"),
	jetTag      = cms.untracked.InputTag("ak5CaloJets"),
	metTag      = cms.untracked.InputTag("htMetAK5"),
        primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD','','HLT'),
	HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT'),
                                      
        l1s  = cms.InputTag("hltL1extraParticles"),
        L2Muons_Collection=cms.InputTag("hltL2MuonCandidates"),
        L2Seeds_Collection=cms.InputTag("hltL2MuonSeeds"),
        L3Seeds_Collection=cms.InputTag("hltL3TrajectorySeed"),
        L3TkTracks_Collection=cms.InputTag("hltL3TkTracksFromL2"),
        L3Muons_Collection=cms.InputTag("hltL3MuonCandidates"), 
        BeamSpotTag = cms.InputTag("offlineBeamSpot"),
                                     

    l1matcherConfig = cms.PSet(
        #useTrack = cms.string('global'),
        #useState = cms.string('outermost'),
        useTrack = cms.string("tracker"),  # 'none' to use Candidate P4; or 'tracker', 'muon', 'global'
        useState = cms.string("atVertex"), # 'innermost' and 'outermost' require the TrackExtra
        useSimpleGeometry = cms.bool(True),
    ),
        #associationMap                                      
        SeedMapTag = cms.InputTag( "hltL2Muons" ),

        #set triggername -1 if you don't want to use SummaryAOD                              
	triggername = cms.string("HLT_Mu40_eta2p1_v4"),
                                      
	outfilename = cms.string("/afs/cern.ch/user/w/walsh/scratch0/TEST1.root")
)

#process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange(
#    lumiRanges
#    )


process.p = cms.Path(process.produceTrees)

process.schedule = cms.Schedule(
    process.p
    )
