// system include files
#include <memory>

#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include <map>

#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/METReco/interface/MET.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "Math/VectorUtil.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"

#include <string>


class TreeProducer{
   public:
           TreeProducer(TFile *outfile);
          ~TreeProducer();

	  void fill_branches(reco::Muon mu1,reco::Muon mu2);
	  void set_summary_results(std::vector<int> results);
	  void set_nselected_pvertices(int npv);
	  void set_event_information(double run,double lumi,double event_number);
	  void set_beamspot(edm::Handle<reco::BeamSpot> BeamSpot);
   private:
	  void reset_variables();
	  void book_branches();
	  void fill_branches_for_muon(reco::Muon mu,reco::Muon other_mu,int summary_res);

	  TFile *outfile;
	  TTree* tree;
	  edm::Handle<reco::BeamSpot> BeamSpot;

	  double run;
	  double lumi;
	  double event_number;
	  
	  mutable float pt;
	  mutable float mass;
	  mutable float eta;
	  mutable float phi;
	  mutable int32_t passing_idx;
	  mutable int32_t passing_l1;
	  mutable int32_t passing_hltwrtl1;
	  mutable float abseta;
	  mutable float njets;
	  mutable float npvertices;
	  mutable float nhits;
	  mutable float reliso;
	  mutable float ecaliso;
	  mutable float hcaliso;
	  mutable float trackiso;
	  mutable float combcaloiso;
	  mutable float mindR;
	  mutable float chi2;
	  mutable float d0;
	  mutable float nmuon_hits;
	  mutable float npixel_layers;
	  mutable float nmatched_segments;

	  std::vector<int> trigger_summary_res;
	  int nselected_pvertices;

	  std::string directory_name;

	  static const bool verbose = false;



};
