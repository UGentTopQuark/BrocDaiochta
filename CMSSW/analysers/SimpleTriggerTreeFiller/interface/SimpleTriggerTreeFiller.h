// -*- C++ -*-
//
// Package:    SimpleTriggerTreeFiller
// Class:      SimpleTriggerTreeFiller
// 
/**\class SimpleTriggerTreeFiller SimpleTriggerTreeFiller.cc analysers/SimpleTriggerTreeFiller/src/SimpleTriggerTreeFiller.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  local user
//         Created:  Tue Jan 11 15:45:24 CET 2011
// $Id$
//
//

// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include <SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h>

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "FWCore/ServiceRegistry/interface/Service.h"

#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/Common/interface/TriggerResults.h"


#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/METReco/interface/MET.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

#include <DataFormats/Common/interface/Ref.h>

#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include <map>
#include "TLorentzVector.h"

#include "DataFormats/Common/interface/View.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"


#include "FWCore/Framework/interface/Frameworkfwd.h"

#include "DataFormats/Math/interface/deltaR.h"

#include "DataFormats/Common/interface/ValueMap.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/Common/interface/Ptr.h"

#include "DataFormats/L1Trigger/interface/L1MuonParticleFwd.h"
#include "DataFormats/L1Trigger/interface/L1MuonParticle.h"

#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/RecoCandidate/interface/RecoChargedCandidate.h"
#include "DataFormats/RecoCandidate/interface/RecoChargedCandidateFwd.h"
#include "DataFormats/MuonReco/interface/MuonFwd.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/TrajectorySeed/interface/TrajectorySeed.h"

//new for association map
#include "DataFormats/L1Trigger/interface/L1MuonParticleFwd.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "DataFormats/MuonSeed/interface/L2MuonTrajectorySeedCollection.h"
#include "DataFormats/Common/interface/AssociationMap.h"
#include "DataFormats/Common/interface/OneToMany.h"
#include "DataFormats/Common/interface/Handle.h"
#include "DataFormats/HLTReco/interface/TriggerFilterObjectWithRefs.h"


#include "DataFormats/MuonSeed/interface/L2MuonTrajectorySeed.h"
#include "DataFormats/MuonSeed/interface/L2MuonTrajectorySeedCollection.h"
#include "DataFormats/MuonSeed/interface/L3MuonTrajectorySeed.h"
#include "DataFormats/MuonSeed/interface/L3MuonTrajectorySeedCollection.h"

#include "FWCore/Framework/interface/EDProducer.h"
#include "FWCore/Framework/interface/ESHandle.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "FWCore/MessageService/interface/MessageLogger.h"
//#include "FWCore/ParameterSet/interface/InputTag.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ParameterSet/interface/Registry.h"

#include "CommonTools/Utils/interface/StringCutObjectSelector.h"
#include "HLTriggerOffline/Muon/interface/PropagateToMuon.h"
//#include "MuonAnalysis/MuonAssociators/interface/PropagateToMuon.h"
//tmp#include "TrackingTools/TrajectoryState/interface/TrajectoryStateOnSurface.h"

//tmp#include "TriggerMatcher.h"
#include "TreeProducer.h"
#include "Math/VectorUtil.h"



#include <string>



class SimpleTriggerTreeFiller : public edm::EDAnalyzer {
   public:
      explicit SimpleTriggerTreeFiller(const edm::ParameterSet&);
      ~SimpleTriggerTreeFiller();


   private:
	virtual void beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup);
	virtual bool onRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup);
	void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&);
	virtual void analyze(const edm::Event&, const edm::EventSetup&);
	void initialise(const edm::Event& iEvent, const edm::EventSetup& iSetup);
	
	std::vector<int> match_trigger_summary_objects(reco::Muon mu1,reco::Muon mu2,ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > prop_mu1,ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > prop_mu2);
	std::vector<trigger::TriggerObject> get_trigger_summary_objects(std::string module);
	bool check_trigger_available();
	bool check_event_passed(edm::Handle<edm::View<reco::Muon> > muonHandle, edm::Handle<edm::View<reco::Jet> > jetHandle,edm::Handle<reco::BeamSpot> recoBeamSpotHandle,edm::Handle<std::vector<reco::Vertex> > pvertexHandle);


       // ----------member data ---------------------------
	edm::InputTag muoLabel_;
	edm::InputTag jetLabel_;
	edm::InputTag metLabel_;
	edm::InputTag PriVertexLabel_;
	edm::InputTag hlTriggerResults_;
	edm::InputTag hltAodSummary_;
	edm::InputTag l1Label_;
	edm::InputTag l2MuonLabel_;
	edm::InputTag l2SeedLabel_;
	edm::InputTag l3SeedLabel_;
	edm::InputTag l3TkTrkLabel_;
	edm::InputTag l3MuonLabel_;
	edm::InputTag BeamSpotLabel_;
	edm::ParameterSet l1matcherconf_;
	edm::InputTag SeedMapTagLabel_;
	
	TFile *outfile;
	
	HLTConfigProvider *hltConfig;
        PropagateToMuon *prop_to_l1muon;
	TreeProducer *tree_producer;

	std::string outfile_name;
        std::string auto_trigger_menu;
        std::string set_trigger_menu;
	std::string trigger_name;

	std::vector<reco::Muon> iso_muons;

	std::map<std::string,std::vector<trigger::TriggerObject> > module_trigger_objs;
	typedef edm::AssociationMap<edm::OneToMany<std::vector<L2MuonTrajectorySeed>, std::vector<L2MuonTrajectorySeed> > > SeedMap;

	bool hltConfigChanged;
	bool run_changed;
	bool lumi_block_changed;
	bool first_in_job;
	bool trigger_available;

	int nevents_passed;
	int nevents_failed;
	int nevents_total;
	int nevents_triggered;

	int nmu_prop;
	int nmu_l1p;
	int nmu_l1f;
	int nmu_l2s;
	int nmu_l2m;
	int nmu_l2mf;
	int nmu_l3s;
	int nmu_l3t;
	int nmu_l3tf;
	int nmu_l3m;
	int nmu_l3mf;
	int nmu_hlt;
	int nmu_l1;

	int nselected_pvertices;

	edm::Handle<trigger::TriggerEvent> aodTriggerEvent;
	static const bool verbose = false;

};
