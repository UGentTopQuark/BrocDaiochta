#include "analyzers/TtPreselection/interface/Cuts.h"

Cuts::Cuts(std::string ident)
{
	identifier=ident;

	// initialise cuts
	min_M3=-1;
	min_no_jets=-1;
	min_met=-1;
	min_mu_ht=-1;
	min_e_ht=-1;
	max_nisolated_e=-1;
	max_nisolated_mu=-1;
	max_nisolated_lep=-1;
	min_nisolated_e=-1;
	min_nisolated_mu=-1;
	min_nisolated_lep=-1;
	max_jet_eta=-1;
	max_mu_eta=-1;
	max_e_eta=-1;
	Z_rejection_width=-1;

	cuts_passed_counter=0;
	cuts_not_passed_counter=0;

	already_cut = false;

	jets = NULL;
	isolated_electrons = NULL;
	isolated_muons = NULL;

	min_jet_pt = NULL;

	max_mu_trackiso = NULL;
	max_mu_ecaliso = NULL;
	max_mu_caliso = NULL;
	max_mu_hcaliso = NULL;
	min_mu_relIso = NULL;
	max_mu_hcal_veto_cone = NULL;
	max_mu_ecal_veto_cone = NULL;
	min_mu_dR = NULL;
	min_mu_pt = NULL;
	max_mu_chi2 = NULL;
	max_mu_d0 = NULL;
	min_mu_nHits = NULL;
	mu_electronID = NULL;

	max_e_trackiso = NULL;
	max_e_caliso = NULL;
	max_e_ecaliso = NULL;
	max_e_hcaliso = NULL;
	max_e_hcal_veto_cone = NULL;
	max_e_ecal_veto_cone = NULL;
	min_e_relIso = NULL;
	min_e_dR = NULL;
	min_e_pt = NULL;
	max_e_chi2 = NULL;
	max_e_d0 = NULL;
	min_e_nHits = NULL;
	e_electronID = NULL;
	
	// min_mu_trackmatch = -1;
// 	name_mu_trigger = NULL;
// 	min_e_trackmatch = -1;
// 	name_e_trigger = NULL;
// 	min_jet_trackmatch = -1;
// 	name_jet_trigger = NULL;	
// 	min_trigmatch = -1;
// 	name_trigger = NULL;

	e_selector = new LeptonSelector<pat::Electron>();
	mu_selector = new LeptonSelector<pat::Muon>();
	jet_selector = new JetSelector();

	btag_start = 0.2;
// 	btagB_start = 1;
// 	bcount_eff_start = 1.3;
// 	bcount_pur_start = 1.3;
	for(int i = 0; i < 5; i++){
	  Stmpc[i] = 0;Stm[i]= 0 ;Bumpc[i]= 0;
	  btag_cut[i] = btag_start;
	  btag_start = btag_start + 0.2;
	}

	//counters for triggers
	ngen_events = 0;naccgen_mu15 = 0;ntrigacc_mu15 =0;naccgen_mu11 = 0;ntrigacc_mu11 =0;
	naccgen_e15 = 0;ntrigacc_e15 =0;

}

Cuts::~Cuts()
{
	delete jet_selector;
	jet_selector=NULL;
	delete e_selector;
	e_selector=NULL;
	delete mu_selector;
	mu_selector=NULL;

	if(jets != NULL){
		delete jets;
		jets = NULL;
	}
	if(isolated_electrons != NULL){
		delete isolated_electrons;
		isolated_electrons = NULL;
	}
	if(isolated_muons != NULL){
		delete isolated_muons;
		isolated_muons = NULL;
	}
}

void Cuts::set_handles(edm::Handle<edm::View<pat::Muon> > analyzer_muons,
                           edm::Handle<edm::View<pat::Jet> > analyzer_jets,
                           edm::Handle<edm::View<pat::Electron> > analyzer_electrons,
                           edm::Handle<edm::View<pat::MET> > analyzer_mets)
{
	electrons = analyzer_electrons;
	muons = analyzer_muons;
	uncut_jets = analyzer_jets;
	mets = analyzer_mets;

	if(jets != NULL){
		delete jets;
		jets = NULL;
	}
	if(isolated_electrons != NULL){
		delete isolated_electrons;
		isolated_electrons = NULL;
	}
	isolated_electrons = e_selector->get_leptons(electrons, uncut_jets);

	if(isolated_muons != NULL){
		delete isolated_muons;
		isolated_muons = NULL;
	}
	isolated_muons = mu_selector->get_leptons(muons, uncut_jets);

	// FIXME: for the moment only for electrons
	jets = jet_selector->get_jets(analyzer_jets, isolated_electrons);

	already_cut = false;
}

bool Cuts::cut()
{
	bool cut = false;

	if(already_cut)
		return already_cut_result;
	
	// Call functions to apply cuts set
	if(min_no_jets != -1 ||
	   min_jet_pt->size() > 0 ||
	   max_jet_eta != -1)
		cut = cut || cut_njets();
	if((max_e_trackiso != NULL && max_e_trackiso->size() > 0) ||
	   (max_e_caliso != NULL && max_e_caliso->size() > 0) ||
	   (max_e_ecaliso != NULL && max_e_ecaliso->size() > 0) ||
	   (max_e_hcaliso != NULL && max_e_hcaliso->size() > 0) ||
	   (max_e_hcal_veto_cone != NULL && max_e_hcal_veto_cone->size() > 0) ||
	   (max_e_ecal_veto_cone != NULL && max_e_ecal_veto_cone->size() > 0) ||
	   (min_e_relIso != NULL && min_e_relIso->size() > 0) ||
	   (min_e_dR != NULL && min_e_dR->size() > 0) ||
	   max_nisolated_e != -1 ||
	   min_nisolated_e != -1 ||
	   max_nisolated_lep != -1 ||
	   min_nisolated_lep != -1 ||
	   max_e_eta != -1 ||
	   (max_e_chi2 != NULL && max_e_chi2->size() > 0) ||
	   (max_e_d0 != NULL && max_e_d0->size() > 0) ||
	   (min_e_nHits != NULL && min_e_nHits->size() > 0) ||
	   (e_electronID != NULL && e_electronID->size() > 0) ||
	   (min_e_pt != NULL && min_e_pt->size() > 0)){
		cut = cut || cut_nisolated_electrons();
		cut = cut || cut_nisolated_leptons();
	}
	if((max_e_trackiso != NULL && max_mu_trackiso->size() > 0) ||
	   (max_mu_caliso != NULL && max_mu_caliso->size() > 0) ||
	   (max_mu_ecaliso != NULL && max_mu_ecaliso->size() > 0) ||
	   (max_mu_hcaliso != NULL && max_mu_hcaliso->size() > 0) ||
	   (max_mu_hcal_veto_cone != NULL && max_mu_hcal_veto_cone->size() > 0) ||
	   (max_mu_ecal_veto_cone != NULL && max_mu_ecal_veto_cone->size() > 0) ||
	   (min_mu_relIso != NULL && min_mu_relIso->size() > 0) ||
	   (min_mu_dR != NULL && min_mu_dR->size() > 0) ||
           max_nisolated_mu != -1 ||
           min_nisolated_mu != -1 ||
	   max_nisolated_lep != -1 ||
	   min_nisolated_lep != -1 ||
	   max_mu_eta != -1 ||
	   (max_mu_chi2 != NULL && max_mu_chi2->size() > 0) ||
	   (max_mu_d0 != NULL && max_mu_d0->size() > 0) ||
	   (min_mu_nHits != NULL && min_mu_nHits->size() > 0) ||
	   (mu_electronID != NULL && mu_electronID->size() > 0) ||
	   (min_mu_pt != NULL && min_mu_pt->size() > 0) ){
		cut = cut || cut_nisolated_muons();
		cut = cut || cut_nisolated_leptons();
	}
	if(min_met != -1) cut = cut || cut_met();
	if(min_M3 != -1) cut = cut || cut_M3();
	if(min_mu_ht != -1) cut = cut || cut_mu_ht();
	if(min_e_ht != -1) cut = cut || cut_e_ht();
	if(Z_rejection_width != -1) cut = cut || cut_Zrejection();
	//cut = cut || cut_btag();
	//Should always be last because need to check if event passed selection
//	cut = cut || get_triggerInfo();

	

	// count events that passed cuts and those that were discarded
	if(cut)
		cuts_not_passed_counter++;
	else
		cuts_passed_counter++;

	already_cut = true;
	already_cut_result = cut;
	// Returns false if event passed cuts, true if event should be discarded
	return cut;
}



// Apply cut for min pt and number of jets
bool Cuts::cut_njets()
{
	int njets=jets->size();
//	std::cout << "min_no_jets: " << min_no_jets << std::endl;
//	std::cout << "njets: " << njets << std::endl;
//	std::cout << "min_jet_pt_size: " << min_jet_pt->size() << std::endl;

	if((njets < min_no_jets) || (njets < (int) min_jet_pt->size())){
		//std::cout << "returning true" << std::endl;
		if(min_jet_pt->size() == 1 && (*min_jet_pt)[0] <= 0)
			return false;
		else
			return true;
	}
	else{
		//std::cout << "returning false" << std::endl;
		return false;
	}
}

bool Cuts::cut_M3()
{
  std::vector<pat::Jet>::const_iterator jet_iter = jets->begin();
  int njets = jets->size();

  // if there are less than 3 jets in an event this plot makes no sense
	// FIXME: should it in this case be calculated for just two or one jet?
  if(njets < 3)
    return true;

  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > top_candidate;
  double max_pt=-1;
  int jet_id1=-1,jet_id2=-1, jet_id3=-1;

  // Loop over all 3 jet combinations to see which has highest pt
  for(int i=0;i < njets; ++i){
    for(int j=0;j < njets; ++j){
      for(int k=0;k < njets; ++k){
        if(i != j && i != k && j != k){
          double current_pt = ((*jets)[i].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).pt();

          if(current_pt > max_pt){
            jet_id1=i;
            jet_id2=j;
            jet_id3=k;
            max_pt=current_pt;
          }
        }
      }
    }
  }
  // Get LorentzVector of chosen 3 jet combination
  top_candidate = (*jets)[jet_id1].p4()+(*jets)[jet_id2].p4()+(*jets)[jet_id3].p4();

	if(top_candidate.mass() > min_M3)
		return false;
	else
		return true;
}

bool Cuts::cut_met()
{

	if(mets->size() < 1)
		return true;

        edm::View<pat::MET>::const_iterator met_iter = mets->begin();

	// FIXME: is this correct?
        if(met_iter->et() > min_met)
                return false;
        else
                return true;
}


// Cut on total energy
bool Cuts::cut_mu_ht()
{
	double pt_sum=0;

	// FIXME: distinguish electron & muon HT
	// Sum pt of all jets + one lepton
	for(std::vector<pat::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		pt_sum += jet_iter->pt();
	}

	std::vector<pat::Muon>::const_iterator mu_iter = isolated_muons->begin();

	if(mu_iter != isolated_muons->end())
		pt_sum += mu_iter->pt();

	if(pt_sum < min_mu_ht)
		return true;
	else
		return false;
}

// Cut on total energy
bool Cuts::cut_e_ht()
{
	double pt_sum=0;

	// Sum pt of all jets + one lepton
	for(std::vector<pat::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		pt_sum += jet_iter->pt();
	}

	std::vector<pat::Electron>::const_iterator e_iter = isolated_electrons->begin();

	if(e_iter != isolated_electrons->end())
		pt_sum += e_iter->pt();

	if(pt_sum < min_e_ht)
		return true;
	else
		return false;
}

bool Cuts::cut_Zrejection()
{
	double Z_mass = 91.2;

	if(isolated_electrons->size() > 2){
		for(std::vector<pat::Electron>::iterator electron1 =
			isolated_electrons->begin();
		    electron1 != isolated_electrons->end();
		    ++electron1)
		{
			for(std::vector<pat::Electron>::iterator electron2 =
				isolated_electrons->begin();
			    electron2 != isolated_electrons->end();
			    ++electron2)
			{
				// don't use the same lepton twice
				if(electron1 == electron2)
					continue;

				if(((electron1->p4() + electron2->p4()).mass() -
					Z_mass) < Z_rejection_width)
					return true;
			}
		}
	}

	if(isolated_muons->size() > 2){
		for(std::vector<pat::Muon>::iterator muon1 = isolated_muons->begin();
		    muon1 != isolated_muons->end();
		    ++muon1)
		{
			for(std::vector<pat::Muon>::iterator muon2 =
				isolated_muons->begin();
			    muon2 != isolated_muons->end();
			    ++muon2)
			{
				// don't use the same lepton twice
				if(muon1 == muon2)
					continue;

				if(((muon1->p4() + muon2->p4()).mass() -
					Z_mass) < Z_rejection_width)
					return true;
			}
		}
	}

	return false;
}

// Calculate dR
double Cuts::deltaR(MyLorentzVector p4_1,MyLorentzVector p4_2)
{ 
	  double delta_phi = ROOT::Math::VectorUtil::DeltaPhi(p4_1,p4_2);         
	  double delta_eta = p4_1.eta()-p4_2.eta();        
	  double dR = sqrt(delta_eta * delta_eta + delta_phi * delta_phi);        
	  return dR;
	 
}

// Set max and min number of each lepton allowed
bool Cuts::cut_nisolated_leptons()
{
	int niso_lep = isolated_electrons->size() + isolated_muons->size();

	if(niso_lep == 0 && max_nisolated_lep == 0)
		return false;
	
	if((niso_lep >= min_nisolated_lep && niso_lep <= max_nisolated_lep ) || ((niso_lep >= min_nisolated_lep) && (max_nisolated_lep == -1)) || ((min_nisolated_lep == -1) && (niso_lep <= max_nisolated_lep)) ||  ((min_nisolated_lep == -1) && (max_nisolated_lep == -1)))
		return false;
	else
		return true;		
}


// Set max and min number of each lepton allowed
bool Cuts::cut_nisolated_electrons()
{
	int niso_e = isolated_electrons->size();

	if(niso_e == 0 && max_nisolated_e == 0)
		return false;
	
	if((niso_e >= min_nisolated_e && niso_e <= max_nisolated_e ) || ((niso_e >= min_nisolated_e) && (max_nisolated_e == -1)) || ((min_nisolated_e == -1) && (niso_e <= max_nisolated_e)) ||  ((min_nisolated_e == -1) && (max_nisolated_e == -1)))
		return false;
	else
		return true;		
}

bool Cuts::cut_nisolated_muons()
{
	int niso_mu = isolated_muons->size();

	if(niso_mu == 0 && max_nisolated_mu == 0)
		return false;

	if(((niso_mu >= min_nisolated_mu) && (niso_mu <= max_nisolated_mu)) || ((niso_mu >= min_nisolated_mu) && (max_nisolated_mu == -1)) || ((min_nisolated_mu == -1) && (niso_mu <= max_nisolated_mu)) ||  ((min_nisolated_mu == -1) && (max_nisolated_mu == -1))) 
	  {
		return false;		
	}
	else
	  {
		return true;
	  }
}

//Calculate efficiency and purity for differenet btag cuts
bool Cuts::cut_btag()
{

  

  for(int i =0;i<6;i++)
    {

    
      double bcut = btag_cut[i];
 
      
      for(std::vector<pat::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
	int genparId = 0;
	double bDiscrim = jet_iter->bDiscriminator("jetProbabilityBJetTags") ;
	
	// Get pdgId of matched generated parton 95% CL
	if(jet_iter->genParton() != NULL)
	  genparId = jet_iter->genParton()->pdgId();
	
	if(genparId != 5 && genparId != -5 && bDiscrim > bcut){
	  Bumpc[i]++;
		 }
	if(genparId == 5 || genparId == -5){
	  Stm[i] ++;
	  if(bDiscrim > bcut)
	    Stmpc[i]++;
	}
      }
     
    } 



      return false;
	
}


bool Cuts::get_triggerInfo()
{
	return false;
}


/*
 *	Functions to set cut values
 */

void Cuts::set_min_mu_pt(std::vector<double> *min)
{
	already_cut = false;
	min_mu_pt=min;
	mu_selector->set_min_pt(min);
}

void Cuts::set_min_e_pt(std::vector<double> *min)
{
	already_cut = false;
	min_e_pt=min;
	e_selector->set_min_pt(min);
}

void Cuts::set_min_e_nHits(std::vector<double> *min)
{
	already_cut = false;
	min_e_nHits=min;
	e_selector->set_min_nHits(min);
}

void Cuts::set_max_e_chi2(std::vector<double> *max)
{
	already_cut = false;
	max_e_chi2=max;
	e_selector->set_max_chi2(max);
}

void Cuts::set_max_e_d0(std::vector<double> *max)
{
	already_cut = false;
	max_e_d0=max;
	e_selector->set_max_chi2(max);
}

void Cuts::set_e_electronID(std::vector<double> *eid)
{
	already_cut = false;
	e_electronID=eid;
	e_selector->set_electronID(eid);
}

void Cuts::set_met_cut(double min)
{
	already_cut = false;
	min_met = min;
}

void Cuts::set_min_M3(double min)
{
	already_cut = false;
	min_M3 = min;
}

void Cuts::set_min_njets(int min)
{
	already_cut = false;
	min_no_jets = min;
}

void Cuts::set_min_jet_pt(std::vector<double> *min)
{
	already_cut = false;
	min_jet_pt=min;
	jet_selector->set_min_pt(min);
}

void Cuts::set_max_jet_eta(double max)
{
	already_cut = false;
	jet_selector->set_max_eta(max);
	max_jet_eta=max;
}

void Cuts::set_max_mu_eta(double max)
{
	already_cut = false;
	mu_selector->set_max_eta(max);
	max_mu_eta=max;
}

void Cuts::set_max_e_eta(double max)
{
	already_cut = false;
	e_selector->set_max_eta(max);
	max_e_eta=max;
}

void Cuts::set_min_mu_nHits(std::vector<double> *min)
{
	already_cut = false;
	min_mu_nHits=min;
	mu_selector->set_min_nHits(min);
}

void Cuts::set_max_mu_chi2(std::vector<double> *max)
{
	already_cut = false;
	max_mu_chi2=max;
	mu_selector->set_max_chi2(max);
}

void Cuts::set_max_mu_d0(std::vector<double> *max)
{
	already_cut = false;
	max_mu_d0=max;
	mu_selector->set_max_d0(max);
}

void Cuts::set_mu_electronID(std::vector<double> *eid)
{
	already_cut = false;
	mu_electronID=eid;
	mu_selector->set_electronID(eid);
}

void Cuts::set_max_mu_trackiso(std::vector<double> *max)
{
	already_cut = false;
	max_mu_trackiso=max;
	mu_selector->set_max_trackiso(max);
}

void Cuts::set_max_e_trackiso(std::vector<double> *max)
{
	already_cut = false;
	max_e_trackiso=max;
	e_selector->set_max_trackiso(max);
}
void Cuts::set_max_mu_ecaliso(std::vector<double> *max)
{
	already_cut = false;
	max_mu_ecaliso=max;
	mu_selector->set_max_ecaliso(max);
}
void Cuts::set_max_mu_caliso(std::vector<double> *max)
{
	already_cut = false;
	max_mu_caliso=max;
	mu_selector->set_max_caliso(max);
}

void Cuts::set_max_e_ecaliso(std::vector<double> *max)
{
	already_cut = false;
	max_e_ecaliso=max;
	e_selector->set_max_ecaliso(max);
}

void Cuts::set_max_e_caliso(std::vector<double> *max)
{
	already_cut = false;
	max_e_caliso=max;
	e_selector->set_max_caliso(max);
}

void Cuts::set_max_e_hcal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_e_hcal_veto_cone=max;
	e_selector->set_max_hcal_veto_cone(max);
}

void Cuts::set_max_e_ecal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_e_ecal_veto_cone=max;
	e_selector->set_max_ecal_veto_cone(max);
}

void Cuts::set_max_mu_hcaliso(std::vector<double> *max)
{
	already_cut = false;
	max_mu_hcaliso=max;
	mu_selector->set_max_hcaliso(max);
}

void Cuts::set_max_mu_hcal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_mu_hcal_veto_cone=max;
	mu_selector->set_max_hcal_veto_cone(max);
}

void Cuts::set_max_mu_ecal_veto_cone(std::vector<double> *max)
{
	already_cut = false;
	max_mu_ecal_veto_cone=max;
	mu_selector->set_max_ecal_veto_cone(max);
}

void Cuts::set_max_e_hcaliso(std::vector<double> *max)
{
	already_cut = false;
	max_e_hcaliso=max;
	e_selector->set_max_hcaliso(max);
}

void Cuts::set_min_e_relIso(std::vector<double> *min)
{
	already_cut = false;
	min_e_relIso=min;
	e_selector->set_min_relIso(min);
}

void Cuts::set_min_mu_dR(std::vector<double> *min)
{
	already_cut = false;
	min_mu_dR=min;
	mu_selector->set_min_dR(min);
}

void Cuts::set_min_mu_relIso(std::vector<double> *min)
{
	already_cut = false;
	min_mu_relIso=min;
	mu_selector->set_min_relIso(min);
}

void Cuts::set_min_e_dR(std::vector<double> *min)
{
	already_cut = false;
	min_e_dR=min;
	e_selector->set_min_dR(min);
}

void Cuts::set_min_e_ht(double min)
{
	already_cut = false;
	min_e_ht = min;
}

void Cuts::set_min_mu_ht(double min)
{
	already_cut = false;
	min_mu_ht = min;
}

void Cuts::set_min_nisolated_e(int n)
{
	already_cut = false;
	min_nisolated_e = n;
}

void Cuts::set_min_nisolated_lep(int n)
{
	already_cut = false;
	min_nisolated_lep = n;
}

void Cuts::set_min_nisolated_mu(int n)
{
	already_cut = false;
	min_nisolated_mu = n;
}

void Cuts::set_max_nisolated_e(int n)
{
	already_cut = false;
	max_nisolated_e = n;
}

void Cuts::set_max_nisolated_lep(int n)
{
	already_cut = false;
	max_nisolated_lep = n;
}

void Cuts::set_max_nisolated_mu(int n)
{
	already_cut = false;
	max_nisolated_mu = n;
}

void Cuts::set_Z_rejection_width(double width)
{
	already_cut = false;
	Z_rejection_width = width;
}

// template <class myPATClass>
// void Cuts::set_trigger(int n, std::string trigName)
// {
//   min_trigger = trigName;
//   name_trigmatch_num = n;
// }


/*
 *	get cut information
 */

std::vector<double>* Cuts::get_max_mu_trackiso()
{
	return max_mu_trackiso;
}

std::vector<double>* Cuts::get_max_e_trackiso()
{
	return max_e_trackiso;
}
std::vector<double>* Cuts::get_max_mu_ecaliso()
{
	return max_mu_ecaliso;
}
std::vector<double>* Cuts::get_max_mu_caliso()
{
	return max_mu_caliso;
}

std::vector<double>* Cuts::get_max_e_ecaliso()
{
	return max_e_ecaliso;
}

std::vector<double>* Cuts::get_max_e_caliso()
{
	return max_e_caliso;
}

std::vector<double>* Cuts::get_max_e_hcal_veto_cone()
{
	return max_e_hcal_veto_cone;
}

std::vector<double>* Cuts::get_max_e_ecal_veto_cone()
{
	return max_e_ecal_veto_cone;
}

std::vector<double>* Cuts::get_max_mu_hcaliso()
{
	return max_mu_hcaliso;
}

std::vector<double>* Cuts::get_max_mu_hcal_veto_cone()
{
	return max_mu_hcal_veto_cone;
}

std::vector<double>* Cuts::get_max_mu_ecal_veto_cone()
{
	return max_mu_ecal_veto_cone;
}

std::vector<double>* Cuts::get_max_e_hcaliso()
{
	return max_e_hcaliso;
}

std::vector<double>* Cuts::get_min_e_relIso()
{
	return min_e_relIso;
}


std::vector<double>* Cuts::get_min_mu_dR()
{
	return min_mu_dR;
}

std::vector<double>* Cuts::get_min_e_dR()
{
	return min_e_dR;
}

std::vector<double>* Cuts::get_min_e_nHits()
{
	return min_e_nHits;
}

std::vector<double>* Cuts::get_max_e_chi2()
{
	return max_e_chi2;
}

std::vector<double>* Cuts::get_max_e_d0()
{
	return max_e_d0;
}

std::vector<double>* Cuts::get_e_electronID()
{
	return e_electronID;
}

std::vector<double>* Cuts::get_min_e_pt()
{
	return min_e_pt;
}

std::vector<double>* Cuts::get_min_mu_pt()
{
	return min_mu_pt;
}

std::vector<double>* Cuts::get_min_mu_relIso()
{
	return min_mu_relIso;
}

std::vector<double>* Cuts::get_min_jet_pt()
{
	return min_jet_pt;
}

double Cuts::get_max_jet_eta()
{
	return max_jet_eta;
}

double Cuts::get_max_mu_eta()
{
	return max_mu_eta;
}

double Cuts::get_max_e_eta()
{
	return max_e_eta;
}

std::vector<double>* Cuts::get_min_mu_nHits()
{
	return min_mu_nHits;
}

std::vector<double>* Cuts::get_max_mu_chi2()
{
	return max_mu_chi2;
}

std::vector<double>* Cuts::get_max_mu_d0()
{
	return max_mu_d0;
}

std::vector<double>* Cuts::get_mu_electronID()
{
	return mu_electronID;
}

/*
 *	Print all cuts for Cuts object
 */

void Cuts::print_cuts()
{
	std::cout << "=-----------IMPOSED-CUTS-----------=" << std::endl;
	std::cout << "identifier: " << identifier << std::endl;
	std::cout << "min_no_jets: " << min_no_jets << std::endl;
	print_cuts_vector("min_e_pt", min_e_pt);
	print_cuts_vector("min_mu_pt", min_mu_pt);
	std::cout << "min_met: " << min_met << std::endl;
	std::cout << "min_mu_ht: " << min_mu_ht << std::endl;
	std::cout << "min_e_ht: " << min_e_ht << std::endl;
	std::cout << "max_mu_eta: " << max_mu_eta << std::endl;
	std::cout << "max_e_eta: " << max_e_eta << std::endl;
	std::cout << "max_jet_eta: " << max_jet_eta << std::endl;
	print_cuts_vector("max_mu_trackiso", max_mu_trackiso);
	print_cuts_vector("max_mu_ecaliso", max_mu_ecaliso);
	print_cuts_vector("max_mu_caliso", max_mu_caliso);
	print_cuts_vector("max_mu_hcaliso", max_mu_hcaliso);
	print_cuts_vector("max_mu_hcal_veto_cone", max_mu_hcal_veto_cone);
	print_cuts_vector("max_mu_ecal_veto_cone", max_mu_ecal_veto_cone);
	print_cuts_vector("min_mu_relIso", min_mu_relIso);
	print_cuts_vector("min_mu_dR", min_mu_dR);
	print_cuts_vector("max_mu_d0", max_mu_d0);
	print_cuts_vector("max_mu_chi2", max_mu_chi2);
	print_cuts_vector("min_mu_nHits", min_mu_nHits);
	print_cuts_vector("mu_electronID", mu_electronID);
	print_cuts_vector("max_e_trackiso", max_e_trackiso);
	print_cuts_vector("max_e_ecaliso", max_e_ecaliso);
	print_cuts_vector("max_e_caliso", max_e_caliso);
	print_cuts_vector("max_e_hcal_veto_cone", max_e_hcal_veto_cone);
	print_cuts_vector("max_e_ecal_veto_cone", max_e_ecal_veto_cone);
	print_cuts_vector("max_e_hcaliso", max_e_hcaliso);
	print_cuts_vector("min_e_relIso", min_e_relIso);
	print_cuts_vector("min_e_dR", min_e_dR);
	print_cuts_vector("max_e_d0", max_e_d0);
	print_cuts_vector("max_e_chi2", max_e_chi2);
	print_cuts_vector("min_e_nHits", min_e_nHits);
	print_cuts_vector("e_electronID", e_electronID);
	std::cout << "max_nisolated_e: " << max_nisolated_e << std::endl;
	std::cout << "max_nisolated_mu: " << max_nisolated_mu << std::endl;
	std::cout << "min_nisolated_e: " << min_nisolated_e << std::endl;
	std::cout << "min_nisolated_mu: " << min_nisolated_mu << std::endl;
	std::cout << "Z_rejection_width: " << Z_rejection_width << std::endl;
	print_cuts_vector("min_jet_pt", min_jet_pt);
	std::cout << "cuts_passed: " << cuts_passed_counter << std::endl;
	std::cout << "cuts_not_passed: " << cuts_not_passed_counter << std::endl;
	std::cout << "=----------------------------------=" << std::endl;
}

void Cuts::print_cuts_vector(std::string id, std::vector<double> *cuts)
{
        std::cout << id << ": ";

        if(cuts == NULL || cuts->size() == 0)
                std::cout << "-1" << std::endl;
        else{
                for(std::vector<double>::iterator cut = cuts->begin();
                    cut != cuts->end();
                    ++cut)
                        if(cut != cuts->begin())
                                std::cout << ":" << *cut;
                        else
                                std::cout << *cut;
                std::cout << std::endl;
        }
}
