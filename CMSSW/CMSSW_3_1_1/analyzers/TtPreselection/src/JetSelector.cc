#include "analyzers/TtPreselection/interface/JetSelector.h"

/*************************************************************************
Jets which are in returned vector:
If eta not set; Jets which pass pt cut + all other jets.
If eta set; Jets which pass pt and eta cut + other jets which pass eta cut.

If one of first few jets passes pt cut but not eta it will be discarded and the same cut will be applied to the next pat::Jet  
***************************************************************************/

double const JetSelector::min_lepton_jet_dR=0.5;

template std::vector<pat::Jet>* JetSelector::get_jets(edm::Handle<edm::View<pat::Jet> > uncut_jets, std::vector<pat::Electron>* leptons);
template bool JetSelector::cut_leptons(const pat::Jet *jet_iter, std::vector<pat::Electron>* leptons);

JetSelector::JetSelector()
{
	tools = new Tools();

	cut_jets = NULL;
	min_pt = NULL;
	max_eta = -1;
}

JetSelector::~JetSelector()
{
	delete tools;
	tools = NULL;
}

void JetSelector::set_min_pt(std::vector<double> *min)
{
	min_pt = min;
}

void JetSelector::set_max_eta(double max)
{
	max_eta = max;
}

// Here the functions for any cuts which have been set are called
template <typename myLepton>
std::vector<pat::Jet>* JetSelector::get_jets(edm::Handle<edm::View<pat::Jet> > uncut_jets, std::vector<myLepton>* leptons)
{
	std::vector<pat::Jet> unsorted_jets;
	cut_jets = new std::vector<pat::Jet>();

	PtSorter<const pat::Jet> jet_sorter;

	if(min_pt != NULL && min_pt->size() >1){

		double max_size = min_pt->size();

	std::vector<const pat::Jet*> jets;

	// fill all jets to jets vector
        for(edm::View<pat::Jet>::const_iterator jet_iter = uncut_jets->begin();
	    jet_iter!=uncut_jets->end();
	    ++jet_iter){
		jets.push_back(&(*jet_iter));
	}

		for(unsigned int nasym_cut = 0; nasym_cut < max_size; ++nasym_cut){
        		for(std::vector<const pat::Jet*>::iterator jet_iter =
				jets.begin();
			    jet_iter!=jets.end();
			    ){
        		        double cut_out=false;
			 
				if(min_pt != NULL && min_pt->size() > nasym_cut && (*min_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_pt(*jet_iter, (*min_pt)[nasym_cut]);
				if(max_eta != -1)
					cut_out = cut_out || cut_eta(*jet_iter, max_eta);

				if(leptons != NULL && leptons->size() > 0)
					cut_out = cut_out || cut_leptons(*jet_iter, leptons);

        		        if(!cut_out)
        		        {
        		                unsorted_jets.push_back(*(*jet_iter));
					jet_iter = jets.erase(jet_iter);
        		        }else{
					jet_iter++;
				}
        		}
			if(unsorted_jets.size() < nasym_cut+1){
				for(std::vector<pat::Jet>::const_iterator jet_iter = unsorted_jets.begin();
				    jet_iter!=unsorted_jets.end(); ++jet_iter){
				        jet_sorter.add(&(*jet_iter));
				}
				
				std::vector<const pat::Jet*> sorted_jets = jet_sorter.get_sorted();
				
				for(std::vector<const pat::Jet*>::iterator cut_jet = sorted_jets.begin();
				    cut_jet != sorted_jets.end();
				    ++cut_jet){
				        cut_jets->push_back(**cut_jet);
				}

				return cut_jets;
			}
		}
	}
	else{
        	for(edm::View<pat::Jet>::const_iterator jet_iter = uncut_jets->begin();
		    jet_iter!=uncut_jets->end();
		    ++jet_iter){
        	        double cut_out=false;
		 
			if(min_pt != NULL && min_pt->size() == 1 && (*min_pt)[0] != -1)
				cut_out = cut_out || cut_pt(&(*jet_iter), (*min_pt)[0]);
			if(max_eta != -1)
				cut_out = cut_out || cut_eta(&(*jet_iter), max_eta);
			if(leptons != NULL && leptons->size() > 0)
				cut_out = cut_out || cut_leptons(&(*jet_iter), leptons);

        	        if(!cut_out)
        	        {
        	                unsorted_jets.push_back(*jet_iter);
        	        }
        	}
	}

        for(std::vector<pat::Jet>::const_iterator jet_iter = unsorted_jets.begin();
	    jet_iter!=unsorted_jets.end(); ++jet_iter){
		jet_sorter.add(&(*jet_iter));
        }

        std::vector<const pat::Jet*> sorted_jets = jet_sorter.get_sorted();

	for(std::vector<const pat::Jet*>::iterator cut_jet = sorted_jets.begin();
	    cut_jet != sorted_jets.end();
	    ++cut_jet){
		cut_jets->push_back(**cut_jet);
	}

	return cut_jets; 
}

template <typename myLepton>
bool JetSelector::cut_leptons(const pat::Jet *jet_iter, std::vector<myLepton>* leptons)
{
	for(typename std::vector<myLepton>::iterator lepton_iter = leptons->begin();
		lepton_iter != leptons->end();
		++lepton_iter){
		if(tools->deltaR(jet_iter->p4(), lepton_iter->p4()) < min_lepton_jet_dR)
			return true;
	}

	return false;
}

bool JetSelector::cut_pt(const pat::Jet *jet_iter, double &min_pt)
{
        if(jet_iter->pt() > min_pt)
                return false;
        else
                return true;
}

bool JetSelector::cut_eta(const pat::Jet *jet_iter, double &max_eta)
{
        if(fabs(jet_iter->eta()) < max_eta)
                return false;
        else
                return true;
}
