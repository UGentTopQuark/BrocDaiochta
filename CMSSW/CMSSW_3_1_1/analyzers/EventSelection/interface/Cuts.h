#ifndef CUTS_H
#define CUTS_H

#include "DataFormats/Common/interface/View.h"
#include "FWCore/Framework/interface/Event.h"

#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Lepton.h"
#include "DataFormats/PatCandidates/interface/Jet.h"

#include "analyzers/EventSelection/interface/LeptonSelector.h"
#include "analyzers/EventSelection/interface/JetSelector.h"
#include "analyzers/EventSelection/interface/MassReconstruction.h"
#include "analyzers/EventSelection/interface/METCorrector.h"
#include "analyzers/EventSelection/interface/DoubleCountedLeptonRemover.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Framework/interface/TriggerNames.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"

#include "Math/LorentzVector.h" 
#include "Math/VectorUtil.h" 

//for truth matching TQAF
#include "AnalysisDataFormats/TopObjects/interface/TtSemiLeptonicEvent.h"

#include <map>

class Cuts{
	public:
		Cuts(std::string ident);
		~Cuts();
		void set_handles(edm::Handle<edm::View<pat::Muon> > analyzer_muons,
                           edm::Handle<edm::View<pat::Jet> > analyzer_jets,
                           edm::Handle<edm::View<pat::Electron> > analyzer_electrons,
                           edm::Handle<edm::View<pat::MET> > analyzer_mets,
			   edm::Handle<TtSemiLeptonicEvent> semiLepEvt,
			   edm::Handle<int> hypoClassKeyHandle,
			   edm::Handle<TtGenEvent> genEvt,
			   edm::Handle<edm::TriggerResults> HLTR,
			   edm::Handle<reco::BeamSpot> recoBeamSpotHandle);
		bool cut();

		void set_min_mu_pt(std::vector<double> *min);
		void set_min_e_pt(std::vector<double> *min);
		void set_min_loose_mu_pt(std::vector<double> *min);
		void set_min_loose_e_pt(std::vector<double> *min);
		void set_met_cut(double min);
		void set_min_jet_pt(std::vector<double> *min);
		void set_min_njets(int min);
		void set_max_njets(int max);
		void set_max_ht(double min);
		void set_min_ht(double min);
		void set_min_M3(double min);
		void set_min_mindiffM3(double min);
		void set_min_nisolated_e(int n);
		void set_min_nisolated_mu(int n);
		void set_max_nisolated_e(int n);
		void set_max_nisolated_mu(int n);
		void set_max_nloose_e(int n);
		void set_max_nloose_mu(int n);

		void set_max_e_trackiso(std::vector<double> *max);
		void set_max_e_caliso(std::vector<double> *max);
		void set_max_e_ecaliso(std::vector<double> *max);
		void set_max_e_hcaliso(std::vector<double> *max);
		void set_max_e_hcal_veto_cone(std::vector<double> *max);
		void set_max_e_ecal_veto_cone(std::vector<double> *max);
		void set_min_e_dR(std::vector<double> *min);
		void set_min_e_relIso(std::vector<double> *min);
		void set_min_e_nHits(std::vector<double> *min);
		void set_max_e_chi2(std::vector<double> *max);
		void set_max_e_d0(std::vector<double> *max);
		void set_max_e_d0sig(std::vector<double> *max);
		void set_e_electronID(std::vector<double> *eid);
		void set_max_mu_trackiso(std::vector<double> *max);
		void set_max_mu_caliso(std::vector<double> *max);
		void set_max_mu_ecaliso(std::vector<double> *max);
		void set_max_mu_hcaliso(std::vector<double> *max);
		void set_max_mu_hcal_veto_cone(std::vector<double> *max);
		void set_max_mu_ecal_veto_cone(std::vector<double> *max);
		void set_min_mu_dR(std::vector<double> *min);
		void set_min_mu_relIso(std::vector<double> *min);
		void set_min_mu_nHits(std::vector<double> *min);
		void set_max_mu_chi2(std::vector<double> *max);
		void set_max_mu_d0(std::vector<double> *max);
		void set_max_mu_d0sig(std::vector<double> *max);
		void set_mu_electronID(std::vector<double> *eid);

		void set_max_loose_e_trackiso(std::vector<double> *max);
		void set_max_loose_e_caliso(std::vector<double> *max);
		void set_max_loose_e_ecaliso(std::vector<double> *max);
		void set_max_loose_e_hcaliso(std::vector<double> *max);
		void set_max_loose_e_hcal_veto_cone(std::vector<double> *max);
		void set_max_loose_e_ecal_veto_cone(std::vector<double> *max);
		void set_min_loose_e_dR(std::vector<double> *min);
		void set_min_loose_e_relIso(std::vector<double> *min);
		void set_min_loose_e_nHits(std::vector<double> *min);
		void set_max_loose_e_chi2(std::vector<double> *max);
		void set_max_loose_e_d0(std::vector<double> *max);
		void set_max_loose_e_d0sig(std::vector<double> *max);
		void set_loose_e_electronID(std::vector<double> *eid);
		void set_max_loose_mu_trackiso(std::vector<double> *max);
		void set_max_loose_mu_caliso(std::vector<double> *max);
		void set_max_loose_mu_ecaliso(std::vector<double> *max);
		void set_max_loose_mu_hcaliso(std::vector<double> *max);
		void set_max_loose_mu_hcal_veto_cone(std::vector<double> *max);
		void set_max_loose_mu_ecal_veto_cone(std::vector<double> *max);
		void set_min_loose_mu_dR(std::vector<double> *min);
		void set_min_loose_mu_relIso(std::vector<double> *min);
		void set_min_loose_mu_nHits(std::vector<double> *min);
		void set_max_loose_mu_chi2(std::vector<double> *max);
		void set_max_loose_mu_d0(std::vector<double> *max);
		void set_max_loose_mu_d0sig(std::vector<double> *max);
		void set_loose_mu_electronID(std::vector<double> *eid);

		void set_max_jet_eta(double max);
		void set_JES_factor(double factor);
		void set_max_mu_eta(std::vector<double> *max);
		void set_max_e_eta(std::vector<double> *max);
		void set_max_loose_mu_eta(std::vector<double> *max);
		void set_max_loose_e_eta(std::vector<double> *max);
		void set_mu_type(double type);
		void set_e_type(double type);
		void set_loose_mu_type(double type);
		void set_loose_e_type(double type);
		void set_Z_rejection_width(double width);
 		void set_trigger(std::vector<double> *trigger); 
 		void set_name_btag(double n); 
		void set_min_btag(std::vector<double> *min);
		// cut on chi2 of jet sorting reconstructed mass
 		void set_min_chi2(double min); 
 		void set_max_chi2(double max); 
		
		//for lepton selector and jet selector
		std::vector<double>* get_max_e_trackiso();
		std::vector<double>* get_max_e_caliso();
		std::vector<double>* get_max_e_ecaliso();
		std::vector<double>* get_max_e_hcaliso();
		std::vector<double>* get_max_e_hcal_veto_cone();
		std::vector<double>* get_max_e_ecal_veto_cone();
		std::vector<double>* get_min_e_dR();
		std::vector<double>* get_min_e_relIso();
		std::vector<double>* get_min_e_pt();
		std::vector<double>* get_e_electronID();
		std::vector<double>* get_max_e_d0();
		std::vector<double>* get_max_e_d0sig();
		std::vector<double>* get_max_e_chi2();
		std::vector<double>* get_min_e_nHits();

		std::vector<double>* get_max_mu_trackiso();
		std::vector<double>* get_max_mu_caliso();
		std::vector<double>* get_max_mu_ecaliso();
		std::vector<double>* get_max_mu_hcaliso();
		std::vector<double>* get_max_mu_hcal_veto_cone();
		std::vector<double>* get_max_mu_ecal_veto_cone();
		std::vector<double>* get_min_mu_dR();
		std::vector<double>* get_min_mu_relIso();
		std::vector<double>* get_min_mu_pt();
		std::vector<double>* get_min_jet_pt();
		
		std::vector<double>* get_mu_electronID();
		std::vector<double>* get_max_mu_d0();
		std::vector<double>* get_max_mu_d0sig();
		std::vector<double>* get_max_mu_chi2();
		std::vector<double>* get_min_mu_nHits();
		double get_max_jet_eta();
		double get_JES_factor();
		std::vector<double>* get_max_e_eta();
		std::vector<double>* get_max_mu_eta();
		int get_e_type();
		int get_mu_type();

		//for plot generator
		std::vector<double>* get_min_btag();
		double get_name_btag();
	
		MassReconstruction* get_mass_reco();

		void print_cuts();
		void print_cuts_vector(std::string id, std::vector<double> *cuts);
	
		
	private:

		typedef ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > MyLorentzVector;

		void calculate_ht();
 
		bool cut_M3();
		bool cut_mindiffM3();
		bool cut_chi2();
		bool cut_mu_pt();
		bool cut_e_pt();
		bool cut_njets();
		bool cut_met();
		bool cut_min_ht();
		bool cut_max_ht();
		bool cut_nisolated_electrons();
		bool cut_nisolated_muons();
		bool cut_nloose_electrons();
		bool cut_nloose_muons();
		bool cut_Zrejection();
		bool cut_btag();
		bool cut_hltrigger();
		bool get_triggerInfo();
	
		double deltaR(MyLorentzVector p4_1,MyLorentzVector p4_2);

		edm::Handle<edm::View<pat::Electron> > electrons;
		edm::Handle<edm::View<pat::Jet> > uncut_jets;
//		edm::Handle<edm::View<pat::Jet> > jets;
		std::vector<pat::Jet>* jets;
		edm::Handle<edm::View<pat::Muon> > muons;
		edm::Handle<edm::View<pat::MET> > mets;
		std::vector<pat::MET>* corrected_mets;
		std::vector<pat::MET>* uncorrected_mets;
		std::vector<pat::Muon>* isolated_muons;
		std::vector<pat::Electron>* isolated_electrons;
		std::vector<pat::Muon>* loose_muons;
		std::vector<pat::Electron>* loose_electrons;
		//for truth matching using TQAF:
		edm::Handle<TtGenEvent> genEvt;
		edm::Handle<TtSemiLeptonicEvent> semiLepEvt;
		edm::Handle<int> hypoClassKeyHandle;
		//for trigger information
		edm::Handle<edm::TriggerResults> HLTR;
	

		std::vector<double> *min_jet_pt;

		std::vector<double> *max_mu_trackiso;
		std::vector<double> *max_mu_hcaliso;
		std::vector<double> *max_mu_hcal_veto_cone;
		std::vector<double> *max_mu_ecal_veto_cone;
		std::vector<double> *max_mu_caliso;
		std::vector<double> *max_mu_ecaliso;
		std::vector<double> *min_mu_relIso;
		std::vector<double> *min_mu_dR;
		std::vector<double> *min_mu_pt;
		std::vector<double> *mu_electronID;
		std::vector<double> *min_mu_nHits;
		std::vector<double> *max_mu_d0;
		std::vector<double> *max_mu_d0sig;
		std::vector<double> *max_mu_chi2;

		std::vector<double> *max_e_trackiso;
		std::vector<double> *max_e_hcaliso;
		std::vector<double> *max_e_hcal_veto_cone;
		std::vector<double> *max_e_ecal_veto_cone;
		std::vector<double> *max_e_caliso;
		std::vector<double> *max_e_ecaliso;
		std::vector<double> *min_e_relIso;
		std::vector<double> *min_e_dR;
		std::vector<double> *min_e_pt;
		std::vector<double> *e_electronID;
		std::vector<double> *min_e_nHits;
		std::vector<double> *max_e_d0;
		std::vector<double> *max_e_d0sig;
		std::vector<double> *max_e_chi2;

		std::vector<double> *max_loose_mu_trackiso;
		std::vector<double> *max_loose_mu_hcaliso;
		std::vector<double> *max_loose_mu_hcal_veto_cone;
		std::vector<double> *max_loose_mu_ecal_veto_cone;
		std::vector<double> *max_loose_mu_caliso;
		std::vector<double> *max_loose_mu_ecaliso;
		std::vector<double> *min_loose_mu_relIso;
		std::vector<double> *min_loose_mu_dR;
		std::vector<double> *min_loose_mu_pt;
		std::vector<double> *loose_mu_electronID;
		std::vector<double> *min_loose_mu_nHits;
		std::vector<double> *max_loose_mu_d0;
		std::vector<double> *max_loose_mu_d0sig;
		std::vector<double> *max_loose_mu_chi2;

		std::vector<double> *max_loose_e_trackiso;
		std::vector<double> *max_loose_e_hcaliso;
		std::vector<double> *max_loose_e_hcal_veto_cone;
		std::vector<double> *max_loose_e_ecal_veto_cone;
		std::vector<double> *max_loose_e_caliso;
		std::vector<double> *max_loose_e_ecaliso;
		std::vector<double> *min_loose_e_relIso;
		std::vector<double> *min_loose_e_dR;
		std::vector<double> *min_loose_e_pt;
		std::vector<double> *loose_e_electronID;
		std::vector<double> *min_loose_e_nHits;
		std::vector<double> *max_loose_e_d0;
		std::vector<double> *max_loose_e_d0sig;
		std::vector<double> *max_loose_e_chi2;

		std::vector<double> *trigger;
		std::vector<double> *min_btag;
		
		MassReconstruction *mass_reco;
		METCorrector *METCor;

		// variable to calculate ht only once for the class
		double ht;

		double min_M3;
		double min_mindiffM3;
		double min_met;
		double max_ht;
		double min_ht;
		int min_no_jets;
		int max_no_jets;
		int max_nloose_e;
		int max_nloose_mu;
		int max_nisolated_e;
		int max_nisolated_mu;
		int min_nisolated_e;
		int min_nisolated_mu;
		double JES_factor;
		std::vector<double>* max_mu_eta;
		std::vector<double>* max_e_eta;
		std::vector<double>* max_loose_mu_eta;
		std::vector<double>* max_loose_e_eta;
		double max_jet_eta;
		int e_type;
		int mu_type;
		int loose_e_type;
		int loose_mu_type;
		double Z_rejection_width;
		double num_name_btag;
		double min_chi2;
		double max_chi2;
		std::string name_btag;
  
		//	int min_trigmatch_num;
		//	std::string name_trigger;
		
		int cuts_passed_counter;
		int cuts_not_passed_counter;

	
		float eff, pur;
		float Stmpc,Stm, Bumpc; 

		int ngen_events;int naccgen_mu15;int ntrigacc_mu15;int naccgen_mu11;int ntrigacc_mu11;
		int naccgen_e15;int ntrigacc_e15;


		bool already_cut; // avoid two calls of cut() for the same event
				  // reset for every set_handles() or set_min/max_xyz()
		bool already_cut_result; // value to be returned if cut() called twice
					 // for the same event

		std::string identifier;
		
		LeptonSelector<pat::Electron> *e_selector;
		LeptonSelector<pat::Muon> *mu_selector;
		LeptonSelector<pat::Electron> *loose_e_selector;
		LeptonSelector<pat::Muon> *loose_mu_selector;
		JetSelector *jet_selector;

		DoubleCountedLeptonRemover<pat::Muon> *double_mu_remover;
		DoubleCountedLeptonRemover<pat::Electron> *double_e_remover;
		
		BJetFinder *bjet_finder;
};

#endif
