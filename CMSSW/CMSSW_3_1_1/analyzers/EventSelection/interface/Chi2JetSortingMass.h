#ifndef CHI2JETSORTINGMASS_H
#define CHI2JETSORTINGMASS_H

#include "analyzers/EventSelection/interface/MassReconstructionMethodBTag.h"

class Chi2JetSortingMass: public MassReconstructionMethodBTag{
	public:
		Chi2JetSortingMass();
		~Chi2JetSortingMass();

		void set_tprime_mass(double mass);

		double get_min_chi2();
		double get_min_chi2_1btag();
		double get_min_chi2_2btag();

	private:
		void set_sigmas();
		void reset_values();

		void calculate_mass();
		void calculate_mass_1btag();
		void calculate_mass_2btag();

                //map assigning sigmas for mass Hadt,Lept and HadW.used for chi?? sorting method
		std::map<double,std::map<std::string,double> > mass_sigma;

		double min_chi2;
		double min_chi2_1btag;
		double min_chi2_2btag;

		double tprime_mass;	// tprime mass for mass hypothesis
};

#endif
