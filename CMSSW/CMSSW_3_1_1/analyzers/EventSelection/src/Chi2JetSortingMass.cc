#include "analyzers/EventSelection/interface/Chi2JetSortingMass.h"

Chi2JetSortingMass::Chi2JetSortingMass()
{
	set_sigmas();
}
Chi2JetSortingMass::~Chi2JetSortingMass()
{
}

void Chi2JetSortingMass::set_tprime_mass(double mass)
{
	this->tprime_mass = mass;
}

void Chi2JetSortingMass::reset_values()
{
	hadT_mass = -1;
	hadW_mass = -1;
	lepT_mass = -1;
	lepW_mass = -1;

	hadT_mass_1btag = -1;
	hadW_mass_1btag = -1;
	lepT_mass_1btag = -1;
	lepW_mass_1btag = -1;

	hadT_mass_2btag = -1;
	hadW_mass_2btag = -1;
	lepT_mass_2btag = -1;
	lepW_mass_2btag = -1;

	min_chi2 = -1;
	min_chi2_1btag = -1;
	min_chi2_2btag = -1;

	mass_jet_ids->clear();
	mass_jet_ids_1btag->clear();
	mass_jet_ids_2btag->clear();

	event_is_processed = false;
	event_is_processed_1btag = false;
	event_is_processed_2btag = false;
}

void Chi2JetSortingMass::set_sigmas()
{
          //Map assigning sigmas for tprime mass: map<mass,sigma hadronic t,sigma leptonic t, sigma hadronic W>
	// top hypothesis, numbers from top meeting
//        mass_sigma[175]["hadT"] = 19.2;
//        mass_sigma[175]["lepT"] = 24.2;
//        mass_sigma[175]["hadW"] = 10.5;

	// Sin�ad's estimated sigmas
//        mass_sigma[250]["hadT"] = 53.0;
//        mass_sigma[250]["lepT"] = 60.0;
//        mass_sigma[250]["hadW"] = 18.0;
//        mass_sigma[300]["hadT"] = 56.0;
//        mass_sigma[300]["lepT"] = 71.0;
//        mass_sigma[300]["hadW"]= 17.0;
//        mass_sigma[350]["hadT"] = 57.0;
//        mass_sigma[350]["lepT"]= 82.0;
//        mass_sigma[350]["hadW"] = 16.0;

	// sigmas for (s1) cutset
        // mass_sigma[175]["hadT"] = 22.76;
        // mass_sigma[175]["lepT"] = 37.28;
        // mass_sigma[175]["hadW"] = 14.26;

        // mass_sigma[250]["hadT"] = 31.35;
        // mass_sigma[250]["lepT"] = 36.68;
        // mass_sigma[250]["hadW"] = 13.68;
        // mass_sigma[300]["hadT"] = 32.85;
        // mass_sigma[300]["lepT"] = 58.66;
        // mass_sigma[300]["hadW"]= 12.74;
        // mass_sigma[350]["hadT"] = 41.31;
        // mass_sigma[350]["lepT"]= 68.95;
        // mass_sigma[350]["hadW"] = 11.59;
        // mass_sigma[400]["hadT"] = 39.97;
        // mass_sigma[400]["lepT"]= 71.00;
        // mass_sigma[400]["hadW"] = 11.48;

	// sigmas for (s5) cutset
        mass_sigma[175]["hadT"] = 20.67;
        mass_sigma[175]["lepT"] = 32.45;
        mass_sigma[175]["hadW"] = 12.45;

        mass_sigma[250]["hadT"] = 26.73;
        mass_sigma[250]["lepT"] = 46.40;
        mass_sigma[250]["hadW"] = 11.26;

        mass_sigma[275]["hadT"] = 27.95;
        mass_sigma[275]["lepT"] = 50.95;
        mass_sigma[275]["hadW"] = 10.84;

        mass_sigma[300]["hadT"] = 29.27;
        mass_sigma[300]["lepT"] = 51.47;
        mass_sigma[300]["hadW"] = 10.52;

        mass_sigma[325]["hadT"] = 26.91;
        mass_sigma[325]["lepT"] = 58.93;
        mass_sigma[325]["hadW"] = 11.84;

        mass_sigma[350]["hadT"] = 30.70;
        mass_sigma[350]["lepT"] = 61.81;
        mass_sigma[350]["hadW"] = 8.57;

        mass_sigma[375]["hadT"] = 35.65;
        mass_sigma[375]["lepT"] = 46.78;
        mass_sigma[375]["hadW"] = 10.68;

        mass_sigma[400]["hadT"] = 30.37;
        mass_sigma[400]["lepT"] = 85.65;
        mass_sigma[400]["hadW"] = 10.74;
}


void Chi2JetSortingMass::calculate_mass_1btag()
{

  /******
	 chi² = (Mj1j2 - MW)²/sigma_jj² +(Mj1j2j3 - Mt')²/sigma_jjj² + (MWlj4 - Mt')^²/sigma_munuj²
  ******/
  
  double MW = 80.0,Mt = tprime_mass,sigma_hadt = -1, sigma_lept = -1,sigma_hadW = -1,min_chi2=-1;
  
  if(tprime_mass == -1.0 || tprime_mass == -1 || mass_sigma.find(tprime_mass) == mass_sigma.end()){
	  std::cout <<"no top mass set for chi squared mass MR1: " <<tprime_mass<< std::endl;
    return;
  }
  
  else{
    sigma_hadt = mass_sigma[tprime_mass]["hadT"];
    sigma_lept = mass_sigma[tprime_mass]["lepT"];
    sigma_hadW = mass_sigma[tprime_mass]["hadW"];
  }
  
  if (sigma_hadt == -1 || sigma_lept == -1 || sigma_hadW == -1){
	  std::cout <<"no top mass set for chi squared mass MR2: " <<tprime_mass<< std::endl;
    return;
  }
  
  int njets = jets->size();
  
  if(njets < 4)
    return;
  
  
  int jet_id1= -1,jet_id2=-1,jet_id3=-1,jet_id4=-1;
  int jet_idb=-1;
  double max_btag = -1;
  
	//find most likely bjet
	std::vector<std::pair<int, double> >* bjets = bjet_finder->get_btag_sorted_jets();

	if(bjets->size() < 1)
		return;
	else{
		jet_idb = (*bjets)[0].first;
		max_btag = (*bjets)[0].second;
	}

  for(std::vector<pat::MET>::iterator met = mets->begin();
  	met != mets->end();
	++met){

  	//Now loop over all remaining jets and combine with btag jet in chi eqn to find minimum
  	for(int j=0;j < njets && j < max_njets; ++j){
  	  for(int k=0;k < njets && k < max_njets; ++k){
  	    for(int l=0;l < njets && l < max_njets; ++l){
  	      if(jet_idb != j && jet_idb != k && jet_id1 != l &&
  	         j != k && j != l &&
  	         k != l){
  	        
  	        double hadronic_Mt = ((*jets)[jet_idb].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
  	        double hadronic_MW = ((*jets)[j].p4()+ (*jets)[k].p4()).mass();
  	        
  	        double leptonic_Mt = -1;
  	        if(electrons->begin() != electrons->end()){
  	            leptonic_Mt = (electrons->begin()->p4()+(*jets)[l].p4()+met->p4()).mass();
  	        }
  	        else if(muons->begin() != muons->end()){
  	            leptonic_Mt = (muons->begin()->p4()+(*jets)[l].p4()+met->p4()).mass();
  	        }
  	        
  	        
  	        double chi2 = (((hadronic_MW-MW)*(hadronic_MW-MW))/(sigma_hadW*sigma_hadW))+(((hadronic_Mt-Mt)*(hadronic_Mt-Mt))/(sigma_hadt*sigma_hadt))+(((leptonic_Mt-Mt)*(leptonic_Mt-Mt))/(sigma_lept*sigma_lept));
  	        
  	        if((chi2 < min_chi2) || (min_chi2 == -1)){
  	          jet_id1=jet_idb;
  	          jet_id2=j;
  	          jet_id3=k;
  	          jet_id4=l;
  	          min_chi2= chi2;
  	        }
  	      }
  	    }
  	  }
  	}
  
  	for(int j=0;j < njets && j < max_njets; ++j){
  	  for(int k=0;k < njets && k < max_njets; ++k){
  	    for(int l=0;l < njets && l < max_njets; ++l){
  	      if(jet_idb != j && jet_idb != k && jet_id1 != l &&
  	         j != k && j != l &&
  	         k != l){
  	        
  	        double hadronic_Mt = ((*jets)[l].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
  	        double hadronic_MW = ((*jets)[j].p4()+ (*jets)[k].p4()).mass();
  	        
  	        double leptonic_Mt = -1;
  	        if(electrons->begin() != electrons->end()){
  	            leptonic_Mt = (electrons->begin()->p4()+(*jets)[jet_idb].p4()+met->p4()).mass();
  	        }
  	        else if(muons->begin() != muons->end()){
  	            leptonic_Mt = (muons->begin()->p4()+(*jets)[jet_idb].p4()+met->p4()).mass();
  	        }
  	        
  	        double chi2 = (((hadronic_MW-MW)*(hadronic_MW-MW))/(sigma_hadW*sigma_hadW))+(((hadronic_Mt-Mt)*(hadronic_Mt-Mt))/(sigma_hadt*sigma_hadt))+(((leptonic_Mt-Mt)*(leptonic_Mt-Mt))/(sigma_lept*sigma_lept));
  	        
  	        if((chi2 < min_chi2) || (min_chi2 == -1)){
  	          jet_id1=l;
  	          jet_id2=j;
  	          jet_id3=k;
  	          jet_id4=jet_idb;
  	          min_chi2= chi2;
  	        }
  	      }
  	    }
  	  }
  	}
  }

	mass_jet_ids_1btag->push_back(jet_id1);
	mass_jet_ids_1btag->push_back(jet_id2);
	mass_jet_ids_1btag->push_back(jet_id3);
	mass_jet_ids_1btag->push_back(jet_id4);

	this->min_chi2_1btag = min_chi2;

	hadT_mass_1btag = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);
	hadW_mass_1btag = W_Had_candidate_mass(jet_id2,jet_id3);
	
	lepT_mass_1btag = top_Lep_candidate_mass(jet_id4);
	lepW_mass_1btag = W_Lep_candidate_mass();

	event_is_processed_1btag = true;
}

void Chi2JetSortingMass::calculate_mass_2btag()
{
 /******
       chi² = (Mj1j2 - MW)²/sigma_jj² +(Mj1j2j3 - Mt')²/sigma_jjj² + (MWlj4 - Mt')^²/sigma_munuj²
  ******/


  double MW = 80.0,Mt = tprime_mass,sigma_hadt = -1, sigma_lept = -1,sigma_hadW = -1,min_chi2=-1;

	if(tprime_mass == -1.0 || tprime_mass == -1 || mass_sigma.find(tprime_mass) == mass_sigma.end()){
		std::cout << "first loop fail " << tprime_mass << std::endl;
		std::cout <<"no top mass set for chi squared mass:MR3 " <<tprime_mass<< std::endl;
		return;
	}
	else{
		    sigma_hadt = mass_sigma[tprime_mass]["hadT"];
		    sigma_lept = mass_sigma[tprime_mass]["lepT"];
		    sigma_hadW = mass_sigma[tprime_mass]["hadW"];
	}
		
	if (sigma_hadt == -1 || sigma_lept == -1 || sigma_hadW == -1){
		  std::cout << "second loop fail " << tprime_mass << std::endl;
		  std::cout <<"no top mass set for chi squared mass:MR4 " <<tprime_mass<< std::endl;
		  return;
	}
		
	int njets = jets->size();
		
	if(njets < 4)
		return;

	int jet_id1= -1,jet_id2=-1,jet_id3=-1,jet_id4=-1;
	int jet_idb=-1,jet_idb2=-1;
	double max_btag = -1,max2_btag = -1;

	//find most likely bjet
	std::vector<std::pair<int, double> >* bjets = bjet_finder->get_btag_sorted_jets();

	if(bjets->size() < 2)
		return;
	else{
		jet_idb = (*bjets)[0].first;
		max_btag = (*bjets)[0].second;
		jet_idb2 = (*bjets)[1].first;
		max2_btag = (*bjets)[1].second;
	}

	for(std::vector<pat::MET>::iterator met = mets->begin();
		met != mets->end();
	      ++met){
		//Now loop over all remaining jets and combine with btag jet in chi eqn to find minimum
		  for(int j=0;j < njets && j < max_njets; ++j){
		    for(int k=0;k < njets && k < max_njets; ++k){
		        if(jet_idb != j && jet_idb != k && jet_idb != jet_idb2 &&
		           j != k && j != jet_idb2 &&
		           k != jet_idb2){
		          
		          double hadronic_Mt = ((*jets)[jet_idb].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
		          double hadronic_MW = ((*jets)[j].p4()+ (*jets)[k].p4()).mass();
		          
		          double leptonic_Mt = -1;
		          if(electrons->begin() != electrons->end()){
		      	leptonic_Mt = (electrons->begin()->p4()+(*jets)[jet_idb2].p4()+met->p4()).mass();
		      				  }
		          else if(muons->begin() != muons->end()){
		      	leptonic_Mt = (muons->begin()->p4()+(*jets)[jet_idb2].p4()+met->p4()).mass();
		          }
		          
		          
		          double chi2 = (((hadronic_MW-MW)*(hadronic_MW-MW))/(sigma_hadW*sigma_hadW))+(((hadronic_Mt-Mt)*(hadronic_Mt-Mt))/(sigma_hadt*sigma_hadt))+(((leptonic_Mt-Mt)*(leptonic_Mt-Mt))/(sigma_lept*sigma_lept));
		      		       
		          if((chi2 < min_chi2) || (min_chi2 == -1)){
		            jet_id1=jet_idb;
		            jet_id2=j;
		            jet_id3=k;
		            jet_id4=jet_idb2;
		            min_chi2= chi2;
		          }
		        }
		    }
		  }
	
		  //switch btags and loop again
		  for(int j=0;j < njets && j < max_njets; ++j){
		    for(int k=0;k < njets && k < max_njets; ++k){
		        if(jet_idb != j && jet_idb != k && jet_idb != jet_idb2 &&
		           j != k && j != jet_idb2 &&
		           k != jet_idb2){
		          
		          double hadronic_Mt = ((*jets)[jet_idb2].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
		          double hadronic_MW = ((*jets)[j].p4()+ (*jets)[k].p4()).mass();
		          
		          double leptonic_Mt = -1;
		          if(electrons->begin() != electrons->end()){
		      	leptonic_Mt = (electrons->begin()->p4()+(*jets)[jet_idb].p4()+met->p4()).mass();
		      				  }
		          else if(muons->begin() != muons->end()){
		      	leptonic_Mt = (muons->begin()->p4()+(*jets)[jet_idb].p4()+met->p4()).mass();
		          }
		          
		          
		          double chi2 = (((hadronic_MW-MW)*(hadronic_MW-MW))/(sigma_hadW*sigma_hadW))+(((hadronic_Mt-Mt)*(hadronic_Mt-Mt))/(sigma_hadt*sigma_hadt))+(((leptonic_Mt-Mt)*(leptonic_Mt-Mt))/(sigma_lept*sigma_lept));
		      		       
		          if((chi2 < min_chi2) || (min_chi2 == -1)){
		            jet_id1=jet_idb2;
		            jet_id2=j;
		            jet_id3=k;
		            jet_id4=jet_idb;
		            min_chi2= chi2;
		          }
		        }
		    }
		  }
	}

	mass_jet_ids_2btag->push_back(jet_id1);
	mass_jet_ids_2btag->push_back(jet_id2);
	mass_jet_ids_2btag->push_back(jet_id3);
	mass_jet_ids_2btag->push_back(jet_id4);

	this->min_chi2_2btag = min_chi2;

	hadT_mass_2btag = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);
	hadW_mass_2btag = W_Had_candidate_mass(jet_id2,jet_id3);
	
	lepT_mass_2btag = top_Lep_candidate_mass(jet_id4);
	lepW_mass_2btag = W_Lep_candidate_mass();

	event_is_processed_2btag = true;
}

//Find combination of jets which minimise chi² and use these to plot mass
void Chi2JetSortingMass::calculate_mass()
{

  /******
       chi² = (Mj1j2 - MW)²/sigma_jj² +(Mj1j2j3 - Mt')²/sigma_jjj² + (MWlj4 - Mt')^²/sigma_munuj²
  ******/

	// FIXME: MW inconsistent with plot generator
  double MW = 80.0,Mt = tprime_mass,sigma_hadt = -1, sigma_lept = -1,sigma_hadW = -1,min_chi2=-1;

  if(tprime_mass == -1.0 || tprime_mass == -1 || mass_sigma.find(tprime_mass) == mass_sigma.end()){
    std::cout <<"no top mass set for chi squared mass MR5: " << tprime_mass << std::endl;
    return;
  }
  
  else{
    sigma_hadt = mass_sigma[tprime_mass]["hadT"];
    sigma_lept = mass_sigma[tprime_mass]["lepT"];
    sigma_hadW = mass_sigma[tprime_mass]["hadW"];
  }
    
  if (sigma_hadt == -1 || sigma_lept == -1 || sigma_hadW == -1){
    std::cout <<"no top mass set for chi squared mass MR6: " << tprime_mass << std::endl;
    return;
  }

  int njets = jets->size(); 
  if(njets < 4)
    return;
  
  int jet_id1= -1,jet_id2=-1,jet_id3=-1;
  int jet_id4=-1;
  for(std::vector<pat::MET>::iterator met = mets->begin();
  	met != mets->end();
	++met){
  	for(int i=0;i < njets && i < max_njets; ++i){
  	  for(int j=0;j < njets && j < max_njets; ++j){
  	    for(int k=0;k < njets && k < max_njets; ++k){
  	      for(int l=0;l < njets && l < max_njets; ++l){
  	        if(i != j && i != k && i != l &&
  	           j != k && j != l &&
  	           k != l){
  	          
  	          double hadronic_Mt = ((*jets)[i].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
  	          // FIXME: same jets for Mt and MW
  	          double hadronic_MW = ((*jets)[j].p4()+ (*jets)[k].p4()).mass();
  	          
  	          double leptonic_Mt = -1;
  	          if(electrons->begin() != electrons->end()){
  	            if(mets->begin() != mets->end())
  	      	leptonic_Mt = (electrons->begin()->p4()+(*jets)[l].p4()+mets->begin()->p4()).mass();
  	            else
  	      	leptonic_Mt = (electrons->begin()->p4()+(*jets)[l].p4()).mass();
  	          }
  	          else if(muons->begin() != muons->end()){
  	            if(mets->begin() != mets->end())
  	      	leptonic_Mt = (muons->begin()->p4()+(*jets)[l].p4()+mets->begin()->p4()).mass();
  	            else
  	      	leptonic_Mt = (muons->begin()->p4()+(*jets)[l].p4()).mass();
  	          }
  	          
  	          
  	          double chi2 = (((hadronic_MW-MW)*(hadronic_MW-MW))/(sigma_hadW*sigma_hadW))+(((hadronic_Mt-Mt)*(hadronic_Mt-Mt))/(sigma_hadt*sigma_hadt))+(((leptonic_Mt-Mt)*(leptonic_Mt-Mt))/(sigma_lept*sigma_lept));
  	          
  	          if((chi2 < min_chi2) || (min_chi2 == -1)){
  	            jet_id1=i;
  	            jet_id2=j;
  	            jet_id3=k;
  	            jet_id4=l;
  	            min_chi2= chi2;
  	          }
  	        }
  	      }
  	    }
  	  }
  	}
  }

	mass_jet_ids->push_back(jet_id1);
	mass_jet_ids->push_back(jet_id2);
	mass_jet_ids->push_back(jet_id3);
	mass_jet_ids->push_back(jet_id4);

	this->min_chi2 = min_chi2;

	hadT_mass = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);
	hadW_mass = W_Had_candidate_mass(jet_id2,jet_id3);
	
	lepT_mass = top_Lep_candidate_mass(jet_id4);
	lepW_mass = W_Lep_candidate_mass();

	event_is_processed = true;
}

double Chi2JetSortingMass::get_min_chi2()
{
	if(min_chi2 != -1)
		return min_chi2;
	else{
		calculate_mass();
		return min_chi2;
	}
}

double Chi2JetSortingMass::get_min_chi2_1btag()
{
	if(min_chi2_1btag != -1)
		return min_chi2_1btag;
	else{
		calculate_mass_1btag();
		return min_chi2_1btag;
	}
}

double Chi2JetSortingMass::get_min_chi2_2btag()
{
	if(min_chi2_2btag != -1)
		return min_chi2_2btag;
	else{
		calculate_mass_2btag();
		return min_chi2_2btag;
	}
}
