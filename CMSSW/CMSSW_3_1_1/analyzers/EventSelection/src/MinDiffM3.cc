#include "analyzers/EventSelection/interface/MinDiffM3.h"

void MinDiffM3::calculate_mass()
{
	int jet_id1=-1,jet_id2=-1, jet_id3=-1;
	int njets = jets->size();
	
	// if there are less than 3 jets in an event this plot makes no sense
	if(njets < 4)
	  return;
	
	jet_id1=jet_id2=jet_id3=-1;
	int jet_id4=-1;
	double min_difference = -1;
	for(int i=0;i < njets; ++i){
	  for(int j=0;j < njets; ++j){
	    for(int k=0;k < njets; ++k){
	      for(int l=0;l < njets; ++l){
	        if(i != j && i != k && i != l &&
	           j != k && j != l &&
	           k != l){
	          double hadronic_mass = ((*jets)[i].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
	          double leptonic_mass = -1;
	          if(electrons->begin() != electrons->end()){
	            if(mets->begin() != mets->end())
	      	leptonic_mass = (electrons->begin()->p4()+(*jets)[l].p4()+mets->begin()->p4()).mass();
	            else
	      	leptonic_mass = (electrons->begin()->p4()+(*jets)[l].p4()).mass();
	          }
	          else if(muons->begin() != muons->end()){
	            if(mets->begin() != mets->end())
	      	leptonic_mass = (muons->begin()->p4()+(*jets)[l].p4()+mets->begin()->p4()).mass();
	            else
	      	leptonic_mass = (muons->begin()->p4()+(*jets)[l].p4()).mass();
	          }
	          
	          double mass_difference = fabs(hadronic_mass - leptonic_mass);
	          
	          if((mass_difference < min_difference) || (min_difference == -1)){
	            jet_id1=i;
	            jet_id2=j;
	            jet_id3=k;
	            jet_id4=l;
	            min_difference = mass_difference;
	          }
	        }
	      }
	    }
	  }
	}
	
	mass_jet_ids->push_back(jet_id1);
	mass_jet_ids->push_back(jet_id2);
	mass_jet_ids->push_back(jet_id3);
	mass_jet_ids->push_back(jet_id4);
	
	hadT_mass = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);	
	hadW_mass = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);	
	lepT_mass = top_Lep_candidate_mass(jet_id4);	
	lepW_mass = W_Lep_candidate_mass();

	event_is_processed = true;
}

void MinDiffM3::calculate_mass_1btag()
{
	int njets = jets->size();
	
	if(njets < 4)
	  return;
	
	int jet_id1= -1,jet_id2=-1,jet_id3=-1,jet_id4=-1;
	int jet_idb=-1;
	double max_btag = -1;
	
	//find most likely bjet
	std::vector<std::pair<int, double> >* bjets = bjet_finder->get_btag_sorted_jets();

	if(bjets->size() < 1)
		return;
	else{
		jet_idb = (*bjets)[0].first;
		max_btag = (*bjets)[0].second;
	}

	double min_difference = -1;
	for(int j=0;j < njets; ++j){
	  for(int k=0;k < njets; ++k){
	    for(int l=0;l < njets; ++l){
	      if(jet_idb != j && jet_idb != k && jet_idb != l &&
		 j != k && j != l &&
		 k != l){
		double hadronic_mass = ((*jets)[jet_idb].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
		double leptonic_mass = -1;
		if(electrons->begin() != electrons->end()){
		  if(mets->begin() != mets->end())
		    leptonic_mass = (electrons->begin()->p4()+(*jets)[l].p4()+mets->begin()->p4()).mass();
		  else
		    leptonic_mass = (electrons->begin()->p4()+(*jets)[l].p4()).mass();
		}
		else if(muons->begin() != muons->end()){
		  if(mets->begin() != mets->end()){
		    leptonic_mass = (muons->begin()->p4()+(*jets)[l].p4()+mets->begin()->p4()).mass();
		  }
		  else{
		    leptonic_mass = (muons->begin()->p4()+(*jets)[l].p4()).mass();
		  }
		}
		
		double mass_difference = fabs(hadronic_mass - leptonic_mass);
		
		if((mass_difference < min_difference) || (min_difference == -1)){
		  jet_id1=jet_idb;
		  jet_id2=j;
		  jet_id3=k;
		  jet_id4=l;
		  min_difference = mass_difference;
		}
	      }
	    }
	  }
	}

	for(int j=0;j < njets; ++j){
	  for(int k=0;k < njets; ++k){
	    for(int l=0;l < njets; ++l){
	      if(jet_idb != j && jet_idb != k && jet_idb != l &&
		 j != k && j != l &&
		 k != l){
		double hadronic_mass = ((*jets)[l].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
		double leptonic_mass = -1;
		if(electrons->begin() != electrons->end()){
		  if(mets->begin() != mets->end())
		    leptonic_mass = (electrons->begin()->p4()+(*jets)[jet_idb].p4()+mets->begin()->p4()).mass();
		  else
		    leptonic_mass = (electrons->begin()->p4()+(*jets)[jet_idb].p4()).mass();
		}
		else if(muons->begin() != muons->end()){
		  if(mets->begin() != mets->end())
		    leptonic_mass = (muons->begin()->p4()+(*jets)[jet_idb].p4()+mets->begin()->p4()).mass();
		  else
		    leptonic_mass = (muons->begin()->p4()+(*jets)[jet_idb].p4()).mass();
		}
		
		double mass_difference = fabs(hadronic_mass - leptonic_mass);
		
		if((mass_difference < min_difference) || (min_difference == -1)){
		  jet_id1=l;
		  jet_id2=j;
		  jet_id3=k;
		  jet_id4=jet_idb;
		  min_difference = mass_difference;
		}
	      }
	    }
	  }
	}

	mass_jet_ids_1btag->push_back(jet_id1);
	mass_jet_ids_1btag->push_back(jet_id2);
	mass_jet_ids_1btag->push_back(jet_id3);
	mass_jet_ids_1btag->push_back(jet_id4);

	hadT_mass_1btag = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);
	hadW_mass = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);	
	lepT_mass_1btag = top_Lep_candidate_mass(jet_id4);	
	lepW_mass_1btag = W_Lep_candidate_mass();

	event_is_processed_2btag = true;
}

void MinDiffM3::calculate_mass_2btag()
{
	int njets = jets->size();
	 
	if(njets < 4)
	  return;
	
	int jet_id1= -1,jet_id2=-1,jet_id3=-1,jet_id4=-1;
	int jet_idb=-1,jet_idb2=-1;
	double max_btag = -1,max2_btag = -1;
	
	//find most likely bjet
	std::vector<std::pair<int, double> >* bjets = bjet_finder->get_btag_sorted_jets();

	if(bjets->size() < 2)
		return;
	else{
		jet_idb = (*bjets)[0].first;
		max_btag = (*bjets)[0].second;
		jet_idb2 = (*bjets)[1].first;
		max2_btag = (*bjets)[1].second;
	}

	double min_difference = -1;
	for(int j=0;j < njets; ++j){
	  for(int k=0;k < njets; ++k){
	    if(jet_idb != j && jet_idb != k && jet_idb != jet_idb2 &&
	       j != k && j != jet_idb2 &&
	       k != jet_idb2){
	      double hadronic_mass = ((*jets)[jet_idb].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
	      double leptonic_mass = -1;
	      if(electrons->begin() != electrons->end()){
	        if(mets->begin() != mets->end())
	          leptonic_mass = (electrons->begin()->p4()+(*jets)[jet_idb2].p4()+mets->begin()->p4()).mass();
	        else
	          leptonic_mass = (electrons->begin()->p4()+(*jets)[jet_idb2].p4()).mass();
	      	}
	      	else if(muons->begin() != muons->end()){
	      	  if(mets->begin() != mets->end())
	      	    leptonic_mass = (muons->begin()->p4()+(*jets)[jet_idb2].p4()+mets->begin()->p4()).mass();
	      	  else
	      	    leptonic_mass = (muons->begin()->p4()+(*jets)[jet_idb2].p4()).mass();
	      	}
	      	
	      	double mass_difference = fabs(hadronic_mass - leptonic_mass);
	      	
	      	if((mass_difference < min_difference) || (min_difference == -1)){
	      	  jet_id1=jet_idb;
	      	  jet_id2=j;
	      	  jet_id3=k;
	      	  jet_id4=jet_idb2;
	      	  min_difference = mass_difference;
	      	}
	            }
	          }
	        }
	
	//switch position of btags
	for(int j=0;j < njets; ++j){
	  for(int k=0;k < njets; ++k){
	    if(jet_idb != j && jet_idb != k && jet_idb != jet_idb2 &&
	       j != k && j != jet_idb2 &&
	       k != jet_idb2){
	      double hadronic_mass = ((*jets)[jet_idb2].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
	      double leptonic_mass = -1;
	      if(electrons->begin() != electrons->end()){
	        if(mets->begin() != mets->end())
	          leptonic_mass = (electrons->begin()->p4()+(*jets)[jet_idb].p4()+mets->begin()->p4()).mass();
	        else
	          leptonic_mass = (electrons->begin()->p4()+(*jets)[jet_idb].p4()).mass();
	      	}
	      else if(muons->begin() != muons->end()){
	        if(mets->begin() != mets->end())
	          leptonic_mass = (muons->begin()->p4()+(*jets)[jet_idb].p4()+mets->begin()->p4()).mass();
	        else
	          leptonic_mass = (muons->begin()->p4()+(*jets)[jet_idb].p4()).mass();
	      }
	      	
	      double mass_difference = fabs(hadronic_mass - leptonic_mass);
	      
	      if((mass_difference < min_difference) || (min_difference == -1)){
	        jet_id1=jet_idb2;
	        jet_id2=j;
	        jet_id3=k;
	        jet_id4=jet_idb;
	        min_difference = mass_difference;
	      }
	    }
	  }
	}

	mass_jet_ids_2btag->push_back(jet_id1);
	mass_jet_ids_2btag->push_back(jet_id2);
	mass_jet_ids_2btag->push_back(jet_id3);
	mass_jet_ids_2btag->push_back(jet_id4);

	hadT_mass_2btag = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);
	hadW_mass = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);	
	lepT_mass_2btag = top_Lep_candidate_mass(jet_id4);	
	lepW_mass_2btag = W_Lep_candidate_mass();

	event_is_processed_2btag = true;
}
