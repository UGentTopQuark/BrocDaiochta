#include "analyzers/EventSelection/interface/M3Mass.h"

void M3Mass::calculate_mass()
{
	if(hadT_mass != -1)
		return;

	double max_pt=-1;
	int jet_id1=-1,jet_id2=-1, jet_id3=-1;

	int njets = jets->size();
	
	// if there are less than 3 jets in an event this plot makes no sense
	if(njets < 3)
		return;
	
	// Loop over all 3 jet combinations to see which has highest pt
	for(int i=0;i < njets; ++i){
	  for(int j=0;j < njets; ++j){
	    for(int k=0;k < njets; ++k){
	      if(i != j && i != k && j != k){
	        double current_pt = ((*jets)[i].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).pt();
	
	        if(current_pt > max_pt){
	          jet_id1=i;
	          jet_id2=j;
	          jet_id3=k;
	          max_pt=current_pt;
	        }
	      }
	    }
	  }
	}

	mass_jet_ids->push_back(jet_id1);
	mass_jet_ids->push_back(jet_id2);
	mass_jet_ids->push_back(jet_id3);
	// b-jet from lep. top decay not defined
	mass_jet_ids->push_back(-1);

	hadW_mass = W_Had_candidate_mass(jet_id2, jet_id3, jet_id1);
	hadT_mass = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);

	event_is_processed = true;
}
