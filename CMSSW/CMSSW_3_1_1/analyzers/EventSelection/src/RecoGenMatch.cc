#include "analyzers/EventSelection/interface/RecoGenMatch.h"

double const RecoGenMatch::max_dR_cut = 0.3;
double const RecoGenMatch::sigma_dphi = 0.03;
double const RecoGenMatch::sigma_deta = 0.03;
double const RecoGenMatch::sigma_dpt = 0.16;
// double const RecoGenMatch::sigma_dphi = 1;
// double const RecoGenMatch::sigma_deta = 1;
//  double const RecoGenMatch::sigma_dpt = 10;
double const pi = 3.1416;

RecoGenMatch::RecoGenMatch()
{
	reset_values();
     
	gen_match = NULL;

	gen_match = new GenMatch();

	top_id = gen_match->get_top_id();

	geninfo_available = false;
	/*
	 *	matching_method:
	 *	1 == fast, 2 ==full, 3 ==bid,genParton,
	 *	4 == sort partons for pt and then match dR < 3 (``Francisco's Method'')
	 */
	default_matching_method = 4;
	matching_method = default_matching_method;
	//matching_method = -1;
}

void RecoGenMatch::reset_values()
{
       
	genquark.clear();
	found_id["Wquark1"] = -1;
	found_id["Wquark2"] = -1;
	found_id["hadb"] = -1;
	found_id["lepb"] = -1;

	found_id_no_dRcut["Wquark1"] = -1;
	found_id_no_dRcut["Wquark2"] = -1;
	found_id_no_dRcut["hadb"] = -1;
	found_id_no_dRcut["lepb"] = -1;

	geninfo_available = false;
	ids_filled = false;
	ids_filled_no_dRcut = false;
}



RecoGenMatch::~RecoGenMatch()
{
	if(gen_match != NULL){
		delete gen_match;
		gen_match = NULL;
	}
}

void RecoGenMatch::set_handles(std::vector<pat::Jet>* jets, std::vector<pat::Electron>* electrons, std::vector<pat::Muon>* muons, std::vector<pat::MET> *mets,edm::Handle<reco::GenParticleCollection> genParticles, edm::Handle< edm::View<pat::Muon> > uncut_muons)
{
	reset_values();

	this->jets = jets;
	this->electrons = electrons;
	this->muons = muons;
	this->mets = mets;
	this->genParticles = genParticles;
	this->uncut_muons = uncut_muons;

	
	gen_match->fill_events_from_collection(genParticles);
	get_GenMatch_quarks();
	geninfo_available = is_geninfo_available();

}

bool RecoGenMatch::is_geninfo_available()
{
	if (genquark.empty())
		return false;
	else if(genquark.find("Wquark2") == genquark.end() || genquark.find("Wquark1") == genquark.end() || genquark.find("hadb") == genquark.end() || genquark.find("lepb") == genquark.end())
		return false;
	else
		return true;
}

//matching method. 1 == fast, 2 == full.
void RecoGenMatch::set_matching_method(double method)
{
	if(method != matching_method){
		//these bools are to stop functions being rerun if found_id has already been filled. however they are reset to false if method is changed since would want to rerun function then
		ids_filled = false;
		ids_filled_no_dRcut = false;
	}
	if(method == -1)
		this->matching_method = default_matching_method;
	else	
		this->matching_method = method;

}

//Find the id for reco jets which are closest dR to gen quarks
std::map<std::string,int> RecoGenMatch::get_matched_recojets_id()
{
	if(matching_method == 3){
		get_matched_genPartonjets_id();
	}else if(matching_method == 4){
		pt_sorted_partons_matching();
	}
	
	if(ids_filled)
		return found_id;

	if(!geninfo_available)
		return found_id;

	int njets = jets->size();
	
	if (njets < 4)
		return found_id;

	if(genquark.size() != 4){
		std::cout << "WARNING: exactly 4 quarks not found: "<<genquark.size()<< std::endl;
		return found_id;
	}
	
	std::map<std::string,std::map<int,double> >  quarkjet_dD;
	
	//first fill array with dD
	for(std::map<std::string,reco::GenParticle*>::iterator quark = genquark.begin();quark!=genquark.end();++quark){
		for(int j=0;j<njets;j++){
			
			double current_dphi= (quark->second->phi()-(*jets)[j].phi());
			double current_deta = (quark->second->eta()-(*jets)[j].eta());
			double current_dpt = ((quark->second->pt()- (*jets)[j].pt())/quark->second->pt());
			double current_dD =  ((current_deta*current_deta)/(sigma_deta*sigma_deta)+(current_dphi*current_dphi)/(sigma_dphi*sigma_dphi)+(current_dpt*current_dpt)/(sigma_dpt*sigma_dpt)); 
			quarkjet_dD[quark->first][j] = current_dD;
			
			}
	}
	
	//fast method
	if(matching_method == 1){
		double min_dD = -1;
		int id_min_dD= -1;
		bool used_ids[20] = {false};
		
		//find min_dD recojet for first quark, then second etc. making sure no reco jet is matched twice
		for(std::map<std::string,reco::GenParticle*>::iterator quark = genquark.begin();quark!=genquark.end();++quark){
			for(int i=0;i<njets;i++){
				if(used_ids[i] == false){ 
				
					double current_dD = quarkjet_dD[quark->first][i];
					if(min_dD == -1 || current_dD < min_dD){
						min_dD = current_dD;
						id_min_dD = i;
					}
				}
			}

			//Apply dR cut on min_dD jets
			if(ROOT::Math::VectorUtil::DeltaR(quark->second->p4(),(*jets)[id_min_dD].p4()) <= max_dR_cut){
				found_id[quark->first] = id_min_dD;
				used_ids[id_min_dD] = true;
			}
			else{
				found_id[quark->first] = -1;}


		}

		
		
	}
	//slow method
	else if(matching_method == 2){
		
		double min_sum_dD = -1;
		std::map<std::string,int> reco_id_mindD;

		//Now for all possible 4jet combinations find sum_dD and minimise
		for(int i=0;i<njets;i++){
			for(int j=0;j<njets;j++){
				for(int k=0;k<njets;k++){
					for(int l=0;l<njets;l++){
						if(i != j && i != k && i != l && j!= k && k != l && j != l){

							double dD_array[4] = {0};

// 							dD_array[0] = quarkjet_dD["Wquark1"][i];
// 							dD_array[1] = quarkjet_dD["Wquark2"][j];
// 							dD_array[2] = quarkjet_dD["hadb"][k];
// 							dD_array[3] = quarkjet_dD["lepb"][l];
							
							if(ROOT::Math::VectorUtil::DeltaR(genquark["Wquark1"]->p4(),(*jets)[i].p4()) > max_dR_cut)
								dD_array[0] = quarkjet_dD["Wquark1"][i] + 1000;
							else
								dD_array[0] = quarkjet_dD["Wquark1"][i];

							if(ROOT::Math::VectorUtil::DeltaR(genquark["Wquark2"]->p4(),(*jets)[j].p4()) > max_dR_cut)
								dD_array[1] = quarkjet_dD["Wquark2"][j] + 1000;
							else
								dD_array[1] = quarkjet_dD["Wquark2"][j];

							if(ROOT::Math::VectorUtil::DeltaR(genquark["hadb"]->p4(),(*jets)[k].p4()) > max_dR_cut)
								dD_array[2] = quarkjet_dD["hadb"][k] + 1000;
							else
								dD_array[2] = quarkjet_dD["hadb"][k];

							if(ROOT::Math::VectorUtil::DeltaR(genquark["lepb"]->p4(),(*jets)[l].p4()) > max_dR_cut)
								dD_array[3] = quarkjet_dD["lepb"][l] + 1000;
							else
								dD_array[3] = quarkjet_dD["lepb"][l];

							double current_sum_dD = dD_array[0] + dD_array[1] + dD_array[2] + dD_array[3];
							if(min_sum_dD == -1 || current_sum_dD < min_sum_dD){
								min_sum_dD = current_sum_dD;
								reco_id_mindD["Wquark1"] = i;
								reco_id_mindD["Wquark2"] = j;
								reco_id_mindD["hadb"] = k;
								reco_id_mindD["lepb"] = l;
								
							}


						}
					}
				}
			}
		}

		//min_sum_dD may still contain match which didnt pass dR cut so recheck each 
		for(std::map<std::string,reco::GenParticle*>::iterator quark = genquark.begin();quark!=genquark.end();++quark){

			if(ROOT::Math::VectorUtil::DeltaR(quark->second->p4(),(*jets)[reco_id_mindD[quark->first]].p4()) <= max_dR_cut)
				found_id[quark->first] = reco_id_mindD[quark->first];
			else
				found_id[quark->first] = -1;	


		}
		

	}
	else if(matching_method != 1 && matching_method != 2){
		std::cout <<" WARNING:matching method not set: " << matching_method<< std::endl; 
	}

	ids_filled = true;
	return found_id;

}

//same as above but without any dR cut
std::map<std::string,int> RecoGenMatch::get_recojets_id_no_dRcut()
{
	if(ids_filled_no_dRcut)
		return found_id_no_dRcut;

	if(!geninfo_available)
		return found_id_no_dRcut;

	int njets = jets->size();
	
	if (njets < 4)
		return found_id_no_dRcut;

	if(genquark.size() != 4){
		std::cout << "WARNING: exactly 4 quarks not found: "<<genquark.size()<< std::endl;
		return found_id_no_dRcut;
	}
	
	std::map<std::string,std::map<int,double> >  quarkjet_dD;
	
	//first fill array with dD
	for(std::map<std::string,reco::GenParticle*>::iterator quark = genquark.begin();quark!=genquark.end();++quark){
		for(int j=0;j<njets;j++){
			
			double current_dphi= (quark->second->phi()-(*jets)[j].phi());
			double current_deta = (quark->second->eta()-(*jets)[j].eta());
			double current_dpt = ((quark->second->pt()- (*jets)[j].pt())/quark->second->pt());
			double current_dD =  ((current_deta*current_deta)/(sigma_deta*sigma_deta)+(current_dphi*current_dphi)/(sigma_dphi*sigma_dphi)+(current_dpt*current_dpt)/(sigma_dpt*sigma_dpt));			
			quarkjet_dD[quark->first][j] = current_dD;
			
			}
	}
	
	//fast method
	if(matching_method == 1){
		double min_dD = -1;
		int id_min_dD= -1;
		bool used_ids[20] = {false};
		
		//find min_dD for first quark, then second etc. making sure no reco jet is matched twice
		for(std::map<std::string,reco::GenParticle*>::iterator quark = genquark.begin();quark!=genquark.end();++quark){
			for(int i=0;i<njets;i++){
				if(used_ids[i] == false){ 
				
					double current_dD = quarkjet_dD[quark->first][i];
					if(min_dD == -1 || current_dD < min_dD){
						min_dD = current_dD;
						id_min_dD = i;
					}
				}
			}

			//assign min_dD id as found
			found_id_no_dRcut[quark->first] = id_min_dD;
			used_ids[id_min_dD] = true;


		}

		
		
	}

	//slow method
	else if(matching_method == 2){
		
		double min_sum_dD = -1;
		std::map<std::string,int> reco_id_mindD;

		//Now for all possible 4jet combinations find sum_dD and minimise
		for(int i=0;i<njets;i++){
			for(int j=0;j<njets;j++){
				for(int k=0;k<njets;k++){
					for(int l=0;l<njets;l++){
						if(i != j && i != k && i != l && j!= k && k != l && j != l){

							double current_sum_dD = quarkjet_dD["Wquark1"][i] + quarkjet_dD["Wquark2"][j] + quarkjet_dD["hadb"][k] + quarkjet_dD["lepb"][l];
							if(min_sum_dD == -1 || current_sum_dD < min_sum_dD){
								min_sum_dD = current_sum_dD;
								reco_id_mindD["Wquark1"] = i;
								reco_id_mindD["Wquark2"] = j;
								reco_id_mindD["hadb"] = k;
								reco_id_mindD["lepb"] = l;
								
							}


						}
					}
				}
			}
		}

		//assign ids without any dR cut
		for(std::map<std::string,reco::GenParticle*>::iterator quark = genquark.begin();quark!=genquark.end();++quark){
				found_id_no_dRcut[quark->first] = reco_id_mindD[quark->first];

		}
		

	}
	else if(matching_method != 1 && matching_method != 2){
		std::cout <<" WARNING:matching method not set: " << matching_method<< std::endl; 
	}

	ids_filled_no_dRcut = true;
	return found_id_no_dRcut;

	
}


double RecoGenMatch::min_dR_genquarks()
{
	if(genquark.begin() != genquark.end()){
		
		double min_dR = -1;
		for(std::map<std::string,reco::GenParticle*>::iterator quark = genquark.begin();quark!=genquark.end();++quark){	
			for(std::map<std::string,reco::GenParticle*>::iterator quark2 = genquark.begin();quark2!=genquark.end();++quark2){
				
				if(quark != quark2){
					
					double dR = deltaR(quark->second->p4(),quark2->second->p4());
					if(min_dR == -1 || dR < min_dR)
						min_dR = dR;

				}
			}
		}

		return min_dR;
	}
	else{
		return -1;
	}

}

double RecoGenMatch::dR_Wquarks()
{
	if(genquark.begin() != genquark.end()){
		if(genquark.find("Wquark1") != genquark.end() && genquark.find("Wquark2") != genquark.end()){
			double dR = ROOT::Math::VectorUtil::DeltaR(genquark["Wquark1"]->p4(),genquark["Wquark2"]->p4());
			return dR;
		}
		else return -1;

	}
	else return -1;
}
/********************************************************************************************************************/

double RecoGenMatch::deltaR(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
		    ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2)
{
	double delta_phi = ROOT::Math::VectorUtil::DeltaPhi(p4_1,p4_2);
	double delta_eta = p4_1.eta()-p4_2.eta();
	double dR = sqrt(delta_eta * delta_eta + delta_phi * delta_phi);
	return dR;

}

//Called in constructor. get quarks from GenMatch
void RecoGenMatch::get_GenMatch_quarks()
{

//truth matching  
  if(gen_match != NULL && gen_match->is_ok() && gen_match->get_decay_mode() == 1){

	  //All from hadronic top decay
	  if(!(gen_match->t_decay_is_leptonic())){	
		  genquark["Wquark1"] = gen_match->get_particle("Wplus_dp1");
		  genquark["Wquark2"] = gen_match->get_particle("Wplus_dp2");
		  genquark["hadb"] = gen_match->get_particle("b");
		  genquark["lepb"] = gen_match->get_particle("bbar");
	  }
	  
	  else if(!(gen_match->tbar_decay_is_leptonic())){
		  genquark["Wquark1"]= gen_match->get_particle("Wminus_dp1");
		  genquark["Wquark2"]= gen_match->get_particle("Wminus_dp2");
		  genquark["hadb"]= gen_match->get_particle("bbar");
		  genquark["lepb"]= gen_match->get_particle("b");
		  
	  }
  }
  
}
/*********************************************************************************************************************/

void RecoGenMatch::pt_sorted_partons_matching()
{
	if(ids_filled)
		return;

	if(!geninfo_available)
		return;

	int njets = jets->size();

	if (njets < 1)
		return;

	if(genquark.size() != 4){
		std::cout << "WARNING: exactly 4 quarks not found: "<<genquark.size()<< std::endl;
		return;
	}


	PtSorter<reco::GenParticle> parton_sorter;
	parton_sorter.add(genquark["Wquark1"]);
	parton_sorter.add(genquark["Wquark2"]);
	parton_sorter.add(genquark["hadb"]);
	parton_sorter.add(genquark["lepb"]);
	std::vector<reco::GenParticle*> partons = parton_sorter.get_sorted();
	std::vector<std::pair<reco::GenParticle*, std::string> > indexed_partons;

	// find association between pt-sorted partons and genquark identifier
	for(std::vector<reco::GenParticle*>::iterator parton = partons.begin();
		parton != partons.end();
		++parton){
		for(std::map<std::string, reco::GenParticle*>::iterator quark = genquark.begin();
			quark != genquark.end();
			++quark){
			if((*parton)->eta() == quark->second->eta() &&
			   (*parton)->pt() == quark->second->pt())
				indexed_partons.push_back(std::pair<reco::GenParticle*, std::string>(*parton, quark->first));
		}
	}

	if(indexed_partons.size() < 4){
		std::cout << "WARNING: RecoGenMatch::pt_sorter_partons_matching(): problem indexing particles" << std::endl;
		return;
	}

	std::map<int, bool> already_matched;

	/*
	 *	- run over pt sorted partons
	 *	- check if parton matches jet with dR < 0.3
	 *		- if matches: assign parton, remove jet
	 */
	for(std::vector<std::pair<reco::GenParticle*, std::string> >::iterator parton = indexed_partons.begin();
		parton != indexed_partons.end();
		++parton){
		int jet_id = 0;
		for(std::vector<pat::Jet>::iterator jet = jets->begin();
			jet != jets->end(); ++jet){
			if(jet_id > 4)
				break;
			if(already_matched.find(jet_id) != already_matched.end()){
				++jet_id;
				continue;
			}

			if(ROOT::Math::VectorUtil::DeltaR(parton->first->p4(), jet->p4()) <= max_dR_cut){
				found_id[parton->second] = jet_id;
				already_matched[jet_id] = true;
				break;
			}
			++jet_id;
		}
	}

	ids_filled = true;
}

void RecoGenMatch::get_matched_genPartonjets_id()
{
	if(ids_filled)
		return;

	if(!geninfo_available)
		return;

	int njets = jets->size();
	
	if (njets < 1)
		return;

	if(genquark.size() != 4){
		std::cout << "WARNING: exactly 4 quarks not found: "<<genquark.size()<< std::endl;
		return;
	}

	int Wq1=0,Wq2=0,hb=0,lb=0;
	for(int i=0;i < njets; ++i){
		if((*jets)[i].genParton() == NULL)
			continue;
		
		else if((*jets)[i].genParton()->pt() == genquark["Wquark1"]->pt()){
			found_id["Wquark1"] = i;
			Wq1++;
		}
		else if((*jets)[i].genParton()->pt() == genquark["Wquark2"]->pt()){
			found_id["Wquark2"] = i;
			Wq2++;
		}
		else if((*jets)[i].genParton()->pt() == genquark["hadb"]->pt()){
			found_id["hadb"] = i;
			hb++;
		}
		else if((*jets)[i].genParton()->pt() == genquark["lepb"]->pt()){
			found_id["lepb"] = i;
			lb++;
		}
	}

	if(Wq1 > 1 || Wq2 > 1 || hb>1||lb>1)
		std::cout<<"WARNING: genParton double matching" << Wq1 << Wq2 << hb << lb << std::endl;

	ids_filled = true;
}

//Always check if this vector is empty before using
std::vector<int> RecoGenMatch::get_matched_bjets_id()
{
	int njets = jets->size();
	std::vector<int> found_bjet_id;

	for(int i=0;i < njets; ++i){
		if((*jets)[i].genParton() == NULL)
			continue;

		else if(fabs((*jets)[i].genParton()->pdgId()) == 5)
			found_bjet_id.push_back(i);

	}
	return found_bjet_id;	
}

bool RecoGenMatch::lepton_from_top(int lep_id)
{
	if(!geninfo_available)
		return false;

	
	bool lepton_from_top = false;
	if(muons->size() == 1 && electrons->size() == 0){
		if(muons->begin()->genParticle() != NULL && muons->begin()->genParticle()->mother() != NULL && muons->begin()->genParticle()->mother()->mother() != NULL && muons->begin()->genParticle()->mother()->mother()->mother() != NULL){
			//std::cout << "Lepton " <<muons->begin()->genParticle()->mother()->mother()->pdgId() <<"  " <<muons->begin()->genParticle()->mother()->mother()->mother()->pdgId() << std::endl;
			if(fabs(muons->begin()->genParticle()->mother()->mother()->pdgId()) == 24 && fabs(muons->begin()->genParticle()->mother()->mother()->mother()->pdgId()) == top_id){
				if(lepton_from_top)
					std::cout <<"Error: matched two semi leptonic leptons in event. took first match "<<std::endl;  
				else 
					lepton_from_top = true;
			}
		}
		
	}

	else if(electrons->size() == 1 && muons->size() == 0){
		if(electrons->begin()->genParticle() != NULL && electrons->begin()->genParticle()->mother() != NULL && electrons->begin()->genParticle()->mother()->mother() != NULL && electrons->begin()->genParticle()->mother()->mother()->mother() != NULL){
			if(fabs(electrons->begin()->genParticle()->mother()->mother()->pdgId()) == 24 && fabs(electrons->begin()->genParticle()->mother()->mother()->mother()->pdgId()) == top_id){
				if(lepton_from_top)
					std::cout <<"Error: matched two semi leptonic leptons in event. took first match "<<std::endl;  
				else 
					lepton_from_top = true;
			}
		}
		
	}
	// for now assuming only need this for muons
	else if((*muons)[lep_id].genParticle() != NULL && (*muons)[lep_id].genParticle()->mother() != NULL && (*muons)[lep_id].genParticle()->mother()->mother() != NULL && (*muons)[lep_id].genParticle()->mother()->mother()->mother() != NULL){
		if(fabs((*muons)[lep_id].genParticle()->mother()->mother()->pdgId()) == 24 && fabs((*muons)[lep_id].genParticle()->mother()->mother()->mother()->pdgId()) == top_id){
			lepton_from_top = true;
		}
	}
			
	return lepton_from_top;

}

int RecoGenMatch::lepton_from_top_patmuons()
{
	int i=0,top_mu_i = -1;
	for(edm::View<pat::Muon>::const_iterator uncut_muon = uncut_muons->begin();uncut_muon != uncut_muons->end();++uncut_muon){
		// for now assuming only need this for muons
		if(uncut_muon->genParticle() != NULL && uncut_muon->genParticle()->mother() != NULL && uncut_muon->genParticle()->mother()->mother() != NULL && uncut_muon->genParticle()->mother()->mother()->mother() != NULL){
			if(fabs(uncut_muon->genParticle()->mother()->mother()->pdgId()) == 24 && fabs(uncut_muon->genParticle()->mother()->mother()->mother()->pdgId()) == top_id){
				top_mu_i = i;
			}	
		}
		i++;
	}		
	return top_mu_i;

}
