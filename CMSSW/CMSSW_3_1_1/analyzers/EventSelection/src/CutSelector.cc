#include "analyzers/EventSelection/interface/CutSelector.h"

CutSelector::CutSelector(std::string ident, double mass)
{
        tprime_mass = mass;
	dataset_id = ident;

	//define_cuts_OctX_mu();
	//define_cuts_OctX_e();
	define_cuts_01X09();
	//define_cuts_Francisco();
//		define_preselection_cuts();
	complete_cuts();
	set_cuts();


	ttmu_count = 0,tte_count = 0,ttbg_count = 0;
	//To check if this event has already been called for different
	//cut.Reinitiliased and end of event in plot()
	Event_firstcall = 1;

	edm::Service<TFileService> fs;
	event_counter_histo = fs->make<TH1D>("event_counter", "event counter", 10, -0.5, 9.5);
	
}

CutSelector::~CutSelector()
{
	/*
	 *	delete vector cuts
	 */
	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
	    type_iter != plot_generators.end();
	    ++type_iter)
	{
        	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
        	    pgen_iter != type_iter->second.end();
        	    ++pgen_iter){
			delete pgen_iter->second;
			pgen_iter->second=NULL;
		}
	}

	for(std::map<std::string, std::map<std::string, Cuts*> >::iterator type_iter = cuts.begin();
	    type_iter != cuts.end();
	    ++type_iter)
	{
		for(std::map<std::string,Cuts*>::iterator cuts_iter = type_iter->second.begin();
        	    cuts_iter != type_iter->second.end();
        	    ++cuts_iter){
			cuts_iter->second->print_cuts();
			delete cuts_iter->second;
			cuts_iter->second=NULL;
		}
	}

	for(std::vector<std::vector<double>* >::iterator del_it = cuts_to_be_deleted.begin();
	    del_it != cuts_to_be_deleted.end();
	    ++del_it){
		delete *del_it;
	}

	//Print number of ttbar muon,electron and backgound
	std::cout << "=++++++++++Event-Count:"<< dataset_id <<"++++++++++=" << std::endl;
	std::cout << "ttbar_mu_events: " << ttmu_count << std::endl;
	std::cout << "ttbar_e_events: " << tte_count << std::endl;
	std::cout << "ttbar_bg_events: " << ttbg_count << std::endl;
	std::cout << "=++++++++++++++++++++++++++++++=" << std::endl;
}

void CutSelector::plot()
{

   Event_firstcall = 1;
 	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
 	    type_iter != plot_generators.end();
 	    ++type_iter)
 	{
         	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
         	    pgen_iter != type_iter->second.end();
         	    ++pgen_iter){
 			if(type_iter->first == get_event_type() ||
 			   type_iter->first == ("e_"+get_event_type()) ||
 			   type_iter->first == ("mu_"+get_event_type()) ||
 			   (type_iter->first == "e_background" && get_event_type() == "muon") ||
 			   (type_iter->first == "mu_background" && get_event_type() == "electron") )
 				pgen_iter->second->plot();
				
 		}
 	}
}

std::string CutSelector::get_event_type()
{
	if( genEvt->isTtBar()){
	  if (genEvt->isSemiLeptonic()){

	    if ((genEvt->lepton() != NULL && genEvt->lepton()->pdgId() == 13) || (genEvt->leptonBar() != NULL && genEvt->leptonBar()->pdgId() == -13))
	      {
		if (Event_firstcall == 1){
		  event_counter_histo->Fill(1);
		  ttmu_count++;
		  Event_firstcall = false;
		}
		return "muon";
		
	      }
	    if ((genEvt->lepton() != NULL && genEvt->lepton()->pdgId() == 11) || (genEvt->leptonBar() != NULL && genEvt->leptonBar()->pdgId() == -11))
	      {
		if (Event_firstcall == 1){
		  event_counter_histo->Fill(2);
		  tte_count++;
		  Event_firstcall = false;
		}
		return "electron";
		
	      }	  
	    else
	      {
		if (Event_firstcall == 1){
		  event_counter_histo->Fill(3);
		  ttbg_count++;
		  Event_firstcall = false;
		}
		return "background";
		
	      }
	  }
	  else
	    {
	      if (Event_firstcall == 1){
		event_counter_histo->Fill(3);
		ttbg_count++;
		Event_firstcall = false;
	      }
	      return "background";
	      
	    } 
	}
	else
	  return "background";
}



void CutSelector::set_handles(edm::Handle<edm::View<pat::Muon> > analyzer_muons,
                           edm::Handle<edm::View<pat::Jet> > analyzer_jets,
                           edm::Handle<edm::View<pat::Electron> > analyzer_electrons,
			   edm::Handle<edm::View<pat::MET> > analyzer_mets,
			   edm::Handle<TtGenEvent> genEvt,
			   edm::Handle<TtSemiLeptonicEvent> semiLepEvt,
			   edm::Handle<int> hypoClassKeyHandle,
			   edm::Handle<edm::TriggerResults> HLTR,
			   edm::Handle<reco::GenParticleCollection> genParticles,
			   edm::Handle<reco::BeamSpot> recoBeamSpotHandle)
{
        electrons = analyzer_electrons;
        muons = analyzer_muons;
        jets = analyzer_jets;
        mets = analyzer_mets;
	this->genEvt = genEvt;
	this->semiLepEvt = semiLepEvt;
	this->hypoClassKeyHandle = hypoClassKeyHandle;
	this->HLTR = HLTR;
	this->genParticles = genParticles;

	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
	    type_iter != plot_generators.end();
	    ++type_iter)
	{
        	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
        	    pgen_iter != type_iter->second.end();
        	    ++pgen_iter){
			pgen_iter->second->set_handles(muons, jets, electrons,
			mets, semiLepEvt, hypoClassKeyHandle, genEvt,HLTR,genParticles, recoBeamSpotHandle);
		}
	}
	for(std::map<std::string, std::map<std::string, Cuts*> >::iterator type_iter = cuts.begin();
	    type_iter != cuts.end();
	    ++type_iter)
	{
		for(std::map<std::string,Cuts*>::iterator cuts_iter = type_iter->second.begin();
        	    cuts_iter != type_iter->second.end();
        	    ++cuts_iter){
			cuts_iter->second->set_handles(muons, jets, electrons, mets, semiLepEvt, hypoClassKeyHandle, genEvt,HLTR, recoBeamSpotHandle);
		}
	}
}

/*
 *	For N-1 Plots:
 *	- put ALL cuts to the preselection-loop at the end of define_cuts()
 *	- disable in the upper part of the function the different cuts by 
 *	  setting them to -1
 */

void CutSelector::define_cuts()
{
	/*
	 *	declare cut vectors
	 *	remember to delete them in the destructor!
	 */

	std::vector<double> *jet_cuts = new std::vector<double>();
	jet_cuts->push_back(30);
	jet_cuts->push_back(30);
	jet_cuts->push_back(30);
	jet_cuts->push_back(30);
	cuts_to_be_deleted.push_back(jet_cuts);

	std::vector<double> *e_trackiso = new std::vector<double>();
	e_trackiso->push_back(3);
	e_trackiso->push_back(10);
	cuts_to_be_deleted.push_back(e_trackiso);

	std::vector<double> *e_ecaliso = new std::vector<double>();
	e_ecaliso->push_back(5);
	e_ecaliso->push_back(15);
	cuts_to_be_deleted.push_back(e_ecaliso);

	std::vector<double> *mu_trackiso = new std::vector<double>();
	mu_trackiso->push_back(3);
	mu_trackiso->push_back(10);
	cuts_to_be_deleted.push_back(mu_trackiso);

	std::vector<double> *mu_ecaliso = new std::vector<double>();
	mu_ecaliso->push_back(5);
	mu_ecaliso->push_back(15);
	cuts_to_be_deleted.push_back(mu_ecaliso);


	/*
	 *	cuts for MUONS
	 */
	/*
	cut_defs["muon"]["preselection"]["min_met"] = -1;
	cut_defs["muon"]["preselection"]["max_ht"] = -1;
	cut_defs["muon"]["preselection"]["min_ht"] = -1;
	cut_defs["muon"]["preselection"]["min_ht"] = -1;
	cut_defs["muon"]["preselection"]["max_nisolated_e"] = -1;
	cut_defs["muon"]["preselection"]["max_nisolated_mu"] = 1;
	cut_defs["muon"]["preselection"]["min_nisolated_e"] = -1;
	cut_defs["muon"]["preselection"]["min_nisolated_mu"] = 1;
	cut_defs["muon"]["preselection"]["min_no_jets"] = -1;
	cut_defs["muon"]["preselection"]["max_no_jets"] = -1;
	cut_defs["muon"]["preselection"]["name_btag"] = -1;
	cut_defs["muon"]["preselection"]["min_chi2"] = -1;
	cut_defs["muon"]["preselection"]["max_chi2"] = -1;
	vcuts("muon", "preselection", "min_mu_pt", -1);
	vcuts("muon", "preselection", "max_mu_trackiso", -1);
	vcuts("muon", "preselection", "max_mu_caliso", -1);
	vcuts("muon", "preselection", "max_mu_ecaliso", -1);
	vcuts("muon", "preselection", "max_mu_hcaliso", -1);
	vcuts("muon", "preselection", "max_mu_hcal_veto_cone", -1);
	vcuts("muon", "preselection", "max_mu_ecal_veto_cone", -1);
	vcuts("muon", "preselection", "max_mu_d0", -1);
	vcuts("muon", "preselection", "max_mu_d0sig", -1);
	vcuts("muon", "preselection", "max_mu_chi2", -1);
	vcuts("muon", "preselection", "min_mu_nHits", -1);
	vcuts("muon", "preselection", "mu_electronID", -1);
	vcuts("muon", "preselection", "max_e_hcal_veto_cone", -1);
	vcuts("muon", "preselection", "max_e_ecal_veto_cone", -1);
	vcuts("muon", "preselection", "min_e_pt", -1);
	vcuts("muon", "preselection", "max_e_trackiso", -1);
	vcuts("muon", "preselection", "max_e_caliso", -1);
	vcuts("muon", "preselection", "max_e_ecaliso", -1);
	vcuts("muon", "preselection", "max_e_hcaliso", -1);
	vcuts("muon", "preselection", "min_e_relIso", -1);
	vcuts("muon", "preselection", "max_e_d0", -1);
	vcuts("muon", "preselection", "max_e_d0sig", -1);
	vcuts("muon", "preselection", "max_e_chi2", -1);
	vcuts("muon", "preselection", "min_e_nHits", -1);
	vcuts("muon", "preselection", "e_electronID", -1);
	vcuts("muon", "preselection", "min_mu_dR", -1);
	vcuts("muon", "preselection", "min_mu_relIso", -1);
	vcuts("muon", "preselection", "min_e_dR", -1);
	vcuts("muon", "preselection", "min_jet_pt", 30.0);
	vcuts("muon", "preselection", "trigger", -1);
	vcuts("muon", "preselection", "max_mu_eta", -1);
	cut_defs["muon"]["preselection"]["mu_type"] = -1;
	cut_defs["muon"]["preselection"]["e_type"] = -1;
	cut_defs["muon"]["preselection"]["max_jet_eta"] = -1; 
	cut_defs["muon"]["preselection"]["JES_factor"] = -1; 
	cut_defs["muon"]["preselection"]["Z_rejection_width"] = -1; 
	cut_defs["muon"]["preselection"]["min_M3"] = -1; 
	cut_defs["muon"]["preselection"]["min_mindiffM3"] = -1; 
	*/

	/*
	 *	global cuts for all selections defined so far
	 */
	synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
		type_iter != cut_defs.end();
		++type_iter)
	{
		for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
		{
			set_if_not_set(type_iter->first, set_iter->first, "min_no_jets", 4);
			vset_if_not_set(type_iter->first, set_iter->first, "min_jet_pt", jet_cuts);
			set_if_not_set(type_iter->first, set_iter->first, "max_jet_eta", 2.4);
			set_if_not_set(type_iter->first, set_iter->first, "mu_type", 0);
			vset_if_not_set(type_iter->first, set_iter->first, "max_e_eta", 2.4);
			vset_if_not_set(type_iter->first, set_iter->first, "max_mu_eta", 2.1);
			vset_if_not_set(type_iter->first, set_iter->first, "min_e_pt", 30);
			vset_if_not_set(type_iter->first, set_iter->first, "min_mu_pt", 30);
			vset_if_not_set(type_iter->first, set_iter->first, "min_mu_relIso", 0.0);
			vset_if_not_set(type_iter->first, set_iter->first, "min_e_relIso", 0.0);
			if(type_iter->first == "muon" || type_iter->first == "mu_background"){
				set_if_not_set(type_iter->first, set_iter->first, "max_nisolated_mu", 1.0);
				set_if_not_set(type_iter->first, set_iter->first, "min_nisolated_mu", 1.0);
				set_if_not_set(type_iter->first, set_iter->first, "max_nisolated_e", 0.0);
			}else if(type_iter->first == "electron" || type_iter->first == "e_background"){
				set_if_not_set(type_iter->first, set_iter->first, "max_nisolated_mu", 0.0);
				set_if_not_set(type_iter->first, set_iter->first, "max_nisolated_e", 1.0);
				set_if_not_set(type_iter->first, set_iter->first, "min_nisolated_e", 1.0);
			}
		}
	}

	/*
	 *	cuts for which the global selection cuts are not applied
	 */

	vcuts("muon", "without_cuts", "min_jet_pt", 0.0);
	vcuts("electron", "without_cuts", "min_jet_pt", 0.0);
	vcuts("mu_background", "without_cuts", "min_jet_pt", 0.0);
	vcuts("e_background", "without_cuts", "min_jet_pt", 0.0);
}


// void CutSelector::define_preselection_cuts()
// {
// 	/*
// 	 *	initialise preselection
// 	 */
// 	cut_defs["muon"]["preselection_step00"]["min_nisolated_mu"] = 1.0; 
// 	cut_defs["mu_background"]["preselection_step00"]["min_nisolated_mu"] = 1.0; 
// 	cut_defs["electron"]["preselection_step00"]["min_nisolated_e"] = 1.0; 
// 	cut_defs["e_background"]["preselection_step00"]["min_nisolated_e"] = 1.0; 
// 
// 	std::vector<double> *jet_pt_cuts = new std::vector<double>();
// 	jet_pt_cuts->push_back(65);
// 	jet_pt_cuts->push_back(40);
// 	jet_pt_cuts->push_back(40);
// 	jet_pt_cuts->push_back(40);
// 	cuts_to_be_deleted.push_back(jet_pt_cuts);
// 
// 	/*
// 	 *	set here the number of applied preselection cuts in the lower
// 	 *	part of this function (the initialisation has to be added to
// 	 *	this value, eg.: 1 initialisation + 4 subsequent cuts -> 5
// 	 */
// 	const int MAX_PRESELECTIONS=23;
// 
// 	synchronise_maps();
//         for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
// 		type_iter != cut_defs.end();
// 		++type_iter){
// 		for(int npreselection=1; npreselection < MAX_PRESELECTIONS; ++npreselection){
// 			cut_defs[type_iter->first][get_preselection_id(npreselection)]["dummy"] = -1;
// 			vcut_defs[type_iter->first][get_preselection_id(npreselection)]["dummy"] = NULL;
// 			std::map<std::string,std::map<std::string, double> >::reverse_iterator set_iter=type_iter->second.rbegin();
// 			++set_iter;
// 
// 			for(std::map<std::string, double>::iterator cut_iter = set_iter->second.begin();
// 			    cut_iter != set_iter->second.end();
// 			    ++cut_iter){
// 				std::string cur_set = get_preselection_id(npreselection);
// 				double value = (set_iter->second)[cut_iter->first];
// 				cut_defs[type_iter->first][cur_set][cut_iter->first] = value;
// 			}
// 
// 			std::map<std::string,std::map<std::string, std::vector<double>* > >::reverse_iterator vset_iter=vcut_defs[type_iter->first].rbegin();
// 			++vset_iter;
// 
// 			for(std::map<std::string, std::vector<double>* >::iterator cut_iter = vset_iter->second.begin();
// 			    cut_iter != vset_iter->second.end();
// 			    ++cut_iter){
// 				std::string cur_set = get_preselection_id(npreselection);
// 				std::vector<double> *value = (vset_iter->second)[cut_iter->first];
// 				vcut_defs[type_iter->first][cur_set][cut_iter->first] = value;
// 			}
// 
// 			/*
// 			 *	Put here the subsequent additional cuts
// 			 *	the cuts are then combined with all previous
// 			 *	cuts of this selection
// 			 */
// 			if(type_iter->first == "muon" || type_iter->first == "mu_background"){
// 				switch(npreselection){
// 					case 1:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "max_mu_eta", 2.1); 
// 						break;
// 					case 2: 
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_pt", 10);
// 						break;
// 					case 3:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_pt", 15);
// 						break;
// 					case 4:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						break;
// 					case 5:
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_jet_eta"] = 2.4; 
// 						break;
// 					case 6:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 1; 
// 						break;
// 					case 7:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 1; 
// 						break;
// 					case 8:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 2; 
// 						break;
// 					case 9:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 2; 
// 						break;
// 					case 10:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 11:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 12:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 13:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 4; 
// 						break;
// 					case 14:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 4; 
// 						break;
// 					case 15:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_relIso", 0.6);
// 						break;
// 					case 16:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_relIso", 0.8);
// 						break;
// 					case 17:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_relIso", 0.95);
// 						break;
// 					case 18:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_pt", 30);
// 						break;
// 					case 19:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						break;
// 					case 20:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", jet_pt_cuts);
// 						break;
// 					case 21:
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_nisolated_mu"] = 1.0; 
// 						break;
// 					case 22:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_relIso", 0.95);
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_pt", 30);
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "max_e_eta", 2.4); 
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_nisolated_e"] = 0.0; 
// 						break;
// 				}
// 			}else if(type_iter->first == "electron" || type_iter->first == "e_background"){
// 				switch(npreselection){
// 					case 1:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "max_e_eta", 2.4); 
// 						break;
// 					case 2: 
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_pt", 10);
// 						break;
// 					case 3:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_pt", 15);
// 						break;
// 					case 4:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						break;
// 					case 5:
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_jet_eta"] = 2.4; 
// 						break;
// 					case 6:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 1; 
// 						break;
// 					case 7:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 1; 
// 						break;
// 					case 8:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 2; 
// 						break;
// 					case 9:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 2; 
// 						break;
// 					case 10:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 11:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 12:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 3; 
// 						break;
// 					case 13:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 20);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 4; 
// 						break;
// 					case 14:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 30);
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["min_no_jets"] = 4; 
// 						break;
// 					case 15:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_relIso", 0.6);
// 						break;
// 					case 16:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_relIso", 0.8);
// 						break;
// 					case 17:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_relIso", 0.95);
// 						break;
// 					case 18:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_e_pt", 30);
// 						break;
// 					case 19:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", 40);
// 						break;
// 					case 20:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_jet_pt", jet_pt_cuts);
// 						break;
// 					case 21:
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_nisolated_e"] = 1.0; 
// 						break;
// 					case 22:
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_pt", 30);
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "min_mu_relIso", 0.95);
// 						vcuts(type_iter->first, get_preselection_id(npreselection), "max_mu_eta", 2.1); 
// 						cut_defs[type_iter->first][get_preselection_id(npreselection)]["max_nisolated_mu"] = 0.0; 
// 						break;
// 				}
// 			}
// 			synchronise_maps();
// 		}
// 	}
// }

void CutSelector::define_cuts_OctX_mu()
{

	/************* MUON *****************/


 	// 01_cutset: trigger + global muon
 	vcuts("muon", "01_cutset", "max_mu_d0", -1);
 	vcuts("muon", "01_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["muon"]["01_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["01_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "01_cutset", "max_mu_eta", -1);
 	vcuts("muon", "01_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("muon", "01_cutset", "min_jet_pt", -1);	
 	vcuts("muon", "01_cutset", "min_e_pt", -1);	
 	vcuts("muon", "01_cutset", "min_mu_pt", -1);	
 	vcuts("muon", "01_cutset", "max_mu_trackiso", -1);
 	vcuts("muon", "01_cutset", "max_mu_caliso", -1);
 	vcuts("muon", "01_cutset", "max_e_trackiso", -1);
 	vcuts("muon", "01_cutset", "max_e_caliso", -1);
 	cut_defs["muon"]["01_cutset"]["min_no_jets"] = -1;
 	cut_defs["muon"]["01_cutset"]["mu_type"] = 0;

 	// 02_cutset: 1 iso muon
 	vcuts("muon", "02_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "02_cutset", "max_mu_chi2", 10);
 	vcuts("muon", "02_cutset", "max_mu_d0", 0.2);
 	vcuts("muon", "02_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["02_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["02_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["02_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["02_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "02_cutset", "min_mu_relIso", 0.95);
 	vcuts("muon", "02_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "02_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("muon", "02_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "02_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["02_cutset"]["max_jet_eta"] = -1; 
 	vcuts("muon", "02_cutset", "min_jet_pt", -1);	
 	vcuts("muon", "02_cutset", "min_e_pt", -1);	
 	vcuts("muon", "02_cutset", "min_mu_pt", 20);	
 	cut_defs["muon"]["02_cutset"]["min_no_jets"] = -1;
 	cut_defs["muon"]["02_cutset"]["mu_type"] = 0;

 	// 01_cutset: 4 jets
 	vcuts("muon", "03_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "03_cutset", "max_mu_chi2", 10);
 	vcuts("muon", "03_cutset", "max_mu_d0", 0.2);
 	vcuts("muon", "03_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["03_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["03_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["03_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["03_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "03_cutset", "min_mu_relIso", 0.95);
 	vcuts("muon", "03_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "03_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("muon", "03_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "03_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["03_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "03_cutset", "min_jet_pt", 30);	
 	vcuts("muon", "03_cutset", "min_e_pt", -1);	
 	vcuts("muon", "03_cutset", "min_mu_pt", 20);	
 	cut_defs["muon"]["03_cutset"]["min_no_jets"] = 4;
 	cut_defs["muon"]["03_cutset"]["mu_type"] = 0;

 	// 01_cutset:no loose muon
 	vcuts("muon", "04_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "04_cutset", "max_mu_chi2", 10);
 	vcuts("muon", "04_cutset", "max_mu_d0", 0.2);
 	vcuts("muon", "04_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["04_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["04_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["04_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["04_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "04_cutset", "min_mu_relIso", 0.95);
 	vcuts("muon", "04_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "04_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("muon", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "04_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["04_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "04_cutset", "min_jet_pt", 30);	
 	vcuts("muon", "04_cutset", "min_e_pt", -1);	
 	vcuts("muon", "04_cutset", "min_mu_pt", 20);	
 	cut_defs["muon"]["04_cutset"]["min_no_jets"] = 4;
 	cut_defs["muon"]["04_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["04_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "04_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "04_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "04_cutset", "min_loose_mu_relIso", 0.8);
 	cut_defs["muon"]["04_cutset"]["loose_mu_type"] = 0;

 	// 01_cutset:no loose electron
 	vcuts("muon", "05_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "05_cutset", "max_mu_chi2", 10);
 	vcuts("muon", "05_cutset", "max_mu_d0", 0.2);
 	vcuts("muon", "05_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["05_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["05_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["05_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["05_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "05_cutset", "min_mu_relIso", 0.95);
 	vcuts("muon", "05_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "05_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("muon", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "05_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "05_cutset", "min_jet_pt", 30);	
 	vcuts("muon", "05_cutset", "min_e_pt", -1);	
 	vcuts("muon", "05_cutset", "min_mu_pt", 20);	
 	cut_defs["muon"]["05_cutset"]["min_no_jets"] = 4;
 	cut_defs["muon"]["05_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["05_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "05_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "05_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "05_cutset", "min_loose_mu_relIso", 0.8);
 	cut_defs["muon"]["05_cutset"]["loose_mu_type"] = 0;
  	cut_defs["muon"]["05_cutset"]["max_nloose_e"] = 0;
  	vcuts("muon", "05_cutset", "min_loose_e_pt", 15);	
  	vcuts("muon", "05_cutset", "max_loose_e_eta", 2.5);
  	vcuts("muon", "05_cutset", "min_loose_e_relIso", 0.8);

	/***********MU-BACKGROUND*******/

 	// 01_cutset: trigger + global muon
 	vcuts("mu_background", "01_cutset", "max_mu_d0", -1);
 	vcuts("mu_background", "01_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "01_cutset", "max_mu_eta", -1);
 	vcuts("mu_background", "01_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("mu_background", "01_cutset", "min_jet_pt", -1);	
 	vcuts("mu_background", "01_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "01_cutset", "min_mu_pt", -1);	
 	vcuts("mu_background", "01_cutset", "max_mu_trackiso", -1);
 	vcuts("mu_background", "01_cutset", "max_mu_caliso", -1);
 	vcuts("mu_background", "01_cutset", "max_e_trackiso", -1);
 	vcuts("mu_background", "01_cutset", "max_e_caliso", -1);
 	cut_defs["mu_background"]["01_cutset"]["min_no_jets"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["mu_type"] = 0;

 	// 02_cutset: 1 iso mu_background
 	vcuts("mu_background", "02_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "02_cutset", "max_mu_chi2", 10);
 	vcuts("mu_background", "02_cutset", "max_mu_d0", 0.2);
 	vcuts("mu_background", "02_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["02_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["02_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["02_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["02_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "02_cutset", "min_mu_relIso", 0.95);
 	vcuts("mu_background", "02_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "02_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("mu_background", "02_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "02_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["02_cutset"]["max_jet_eta"] = -1; 
 	vcuts("mu_background", "02_cutset", "min_jet_pt", -1);	
 	vcuts("mu_background", "02_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "02_cutset", "min_mu_pt", 20);	
 	cut_defs["mu_background"]["02_cutset"]["min_no_jets"] = -1;
 	cut_defs["mu_background"]["02_cutset"]["mu_type"] = 0;

 	// 01_cutset: 4 jets
 	vcuts("mu_background", "03_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "03_cutset", "max_mu_chi2", 10);
 	vcuts("mu_background", "03_cutset", "max_mu_d0", 0.2);
 	vcuts("mu_background", "03_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["03_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["03_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["03_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["03_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "03_cutset", "min_mu_relIso", 0.95);
 	vcuts("mu_background", "03_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "03_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("mu_background", "03_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "03_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["03_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "03_cutset", "min_jet_pt", 30);	
 	vcuts("mu_background", "03_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "03_cutset", "min_mu_pt", 20);	
 	cut_defs["mu_background"]["03_cutset"]["min_no_jets"] = 4;
 	cut_defs["mu_background"]["03_cutset"]["mu_type"] = 0;

 	// 01_cutset:no loose muon
 	vcuts("mu_background", "04_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "04_cutset", "max_mu_chi2", 10);
 	vcuts("mu_background", "04_cutset", "max_mu_d0", 0.2);
 	vcuts("mu_background", "04_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["04_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["04_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["04_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["04_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "04_cutset", "min_mu_relIso", 0.95);
 	vcuts("mu_background", "04_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "04_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("mu_background", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "04_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["04_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "04_cutset", "min_jet_pt", 30);	
 	vcuts("mu_background", "04_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "04_cutset", "min_mu_pt", 20);	
 	cut_defs["mu_background"]["04_cutset"]["min_no_jets"] = 4;
 	cut_defs["mu_background"]["04_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["04_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "04_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "04_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "04_cutset", "min_loose_mu_relIso", 0.8);

 	// 01_cutset:no loose electron
 	vcuts("mu_background", "05_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "05_cutset", "max_mu_chi2", 10);
 	vcuts("mu_background", "05_cutset", "max_mu_d0", 0.2);
 	vcuts("mu_background", "05_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["05_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["05_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["05_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["05_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "05_cutset", "min_mu_relIso", 0.95);
 	vcuts("mu_background", "05_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "05_cutset", "max_mu_ecal_veto_cone", 4);
 	vcuts("mu_background", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "05_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "05_cutset", "min_jet_pt", 30);	
 	vcuts("mu_background", "05_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "05_cutset", "min_mu_pt", 20);	
 	cut_defs["mu_background"]["05_cutset"]["min_no_jets"] = 4;
 	cut_defs["mu_background"]["05_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["05_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "05_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "05_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "05_cutset", "min_loose_mu_relIso", 0.8);
 	cut_defs["mu_background"]["05_cutset"]["loose_mu_type"] = 0;
  	cut_defs["mu_background"]["05_cutset"]["max_nloose_e"] = 0;
  	vcuts("mu_background", "05_cutset", "min_loose_e_pt", 15);	
  	vcuts("mu_background", "05_cutset", "max_loose_e_eta", 2.5);
  	vcuts("mu_background", "05_cutset", "min_loose_e_relIso", 0.8);
        /*
         *      cuts for which the global selection cuts are not applied
         */
	vcuts("muon", "without_cuts", "min_jet_pt", 0.0);
	vcuts("mu_background", "without_cuts", "min_jet_pt", 0.0);
}

void CutSelector::define_cuts_OctX_e()
{

	/**********ELECTRON*********/

 	// 01_cutset:trigger + 1 electron 
 	vcuts("electron", "01_cutset", "max_mu_d0", -1);
 	vcuts("electron", "01_cutset", "max_e_d0", -1);
 	cut_defs["electron"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["electron"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["electron"]["01_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["01_cutset"]["min_nisolated_mu"] = -1;
 	vcuts("electron", "01_cutset", "max_mu_eta", -1);
 	vcuts("electron", "01_cutset", "max_e_eta", -1);
 	cut_defs["electron"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("electron", "01_cutset", "min_jet_pt", -1);	
 	vcuts("electron", "01_cutset", "min_e_pt", -1);	
 	vcuts("electron", "01_cutset", "min_mu_pt", -1);
 	cut_defs["electron"]["01_cutset"]["min_no_jets"] = -1;

 	// 02_cutset: exactly 1 electron
 	vcuts("electron", "02_cutset", "max_mu_d0", -1);
 	vcuts("electron", "02_cutset", "max_e_d0", 0.2);
        vcuts("electron", "02_cutset", "e_electronID", 1.0);
 	cut_defs["electron"]["02_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["electron"]["02_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["electron"]["02_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["02_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("electron", "02_cutset", "min_e_relIso", 0.9);
 	vcuts("electron", "02_cutset", "max_mu_eta", -1);
 	vcuts("electron", "02_cutset", "max_e_eta", 2.4);
 	cut_defs["electron"]["02_cutset"]["max_jet_eta"] = -1; 
 	vcuts("electron", "02_cutset", "min_jet_pt", -1);	
 	vcuts("electron", "02_cutset", "min_e_pt", 30);	
 	vcuts("electron", "02_cutset", "min_mu_pt", -1);
 	cut_defs["electron"]["02_cutset"]["min_no_jets"] = -1;

 	// 03_cutset: no muon
 	vcuts("electron", "03_cutset", "min_mu_nHits", 11);
 	vcuts("electron", "03_cutset", "max_mu_chi2", 10);
 	vcuts("electron", "03_cutset", "max_mu_d0", -1);
 	vcuts("electron", "03_cutset", "max_e_d0", 0.2);
        vcuts("electron", "03_cutset", "e_electronID", 1.0);
 	cut_defs["electron"]["03_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["electron"]["03_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["electron"]["03_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["03_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("electron", "03_cutset", "min_e_relIso", 0.9);
  	vcuts("electron", "03_cutset", "min_mu_relIso", 0.95);
 	vcuts("electron", "03_cutset", "max_mu_eta", 2.1);
 	vcuts("electron", "03_cutset", "max_e_eta", 2.4);
 	cut_defs["electron"]["03_cutset"]["max_jet_eta"] = -1; 
 	vcuts("electron", "03_cutset", "min_jet_pt", -1);	
 	vcuts("electron", "03_cutset", "min_e_pt", 30);	
 	vcuts("electron", "03_cutset", "min_mu_pt", 30);
 	cut_defs["electron"]["03_cutset"]["min_no_jets"] = -1;

 	// 01_cutset: jet electron cleaning
 	vcuts("electron", "04_cutset", "min_mu_nHits", 11);
 	vcuts("electron", "04_cutset", "max_mu_chi2", 10);
 	vcuts("electron", "04_cutset", "max_mu_d0", -1);
 	vcuts("electron", "04_cutset", "max_e_d0", 0.2);
        vcuts("electron", "04_cutset", "e_electronID", 1.0);
 	cut_defs["electron"]["04_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["electron"]["04_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["electron"]["04_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["04_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("electron", "04_cutset", "min_e_relIso", 0.9);
  	vcuts("electron", "04_cutset", "min_mu_relIso", 0.95);
 	vcuts("electron", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("electron", "04_cutset", "max_e_eta", 2.4);
 	cut_defs["electron"]["04_cutset"]["max_jet_eta"] = -1; 
 	vcuts("electron", "04_cutset", "min_jet_pt", -1);	
 	vcuts("electron", "04_cutset", "min_e_pt", 30);	
 	vcuts("electron", "04_cutset", "min_mu_pt", 30);
 	cut_defs["electron"]["04_cutset"]["min_no_jets"] = -1;

 	// 01_cutset: 4 jets
 	vcuts("electron", "05_cutset", "min_mu_nHits", 11);
 	vcuts("electron", "05_cutset", "max_mu_chi2", 10);
 	vcuts("electron", "05_cutset", "max_mu_d0", -1);
 	vcuts("electron", "05_cutset", "max_e_d0", 0.2);
        vcuts("electron", "05_cutset", "e_electronID", 1.0);
 	cut_defs["electron"]["05_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["electron"]["05_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["electron"]["05_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["electron"]["05_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("electron", "05_cutset", "min_e_relIso", 0.9);
  	vcuts("electron", "05_cutset", "min_mu_relIso", 0.95);
 	vcuts("electron", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("electron", "05_cutset", "max_e_eta", 2.4);
 	cut_defs["electron"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("electron", "05_cutset", "min_jet_pt", 30);	
 	vcuts("electron", "05_cutset", "min_e_pt", 30);	
 	vcuts("electron", "05_cutset", "min_mu_pt", 30);
 	cut_defs["electron"]["05_cutset"]["min_no_jets"] = 4;

	/***********E-BACKGROUND****************/

 	// 01_cutset:trigger + 1 electron 
 	vcuts("e_background", "01_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "01_cutset", "max_e_d0", -1);
 	cut_defs["e_background"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["e_background"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["e_background"]["01_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["01_cutset"]["min_nisolated_mu"] = -1;
 	vcuts("e_background", "01_cutset", "max_mu_eta", -1);
 	vcuts("e_background", "01_cutset", "max_e_eta", -1);
 	cut_defs["e_background"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("e_background", "01_cutset", "min_jet_pt", -1);	
 	vcuts("e_background", "01_cutset", "min_e_pt", -1);	
 	vcuts("e_background", "01_cutset", "min_mu_pt", -1);
 	cut_defs["e_background"]["01_cutset"]["min_no_jets"] = -1;

 	// 02_cutset: exactly 1 e_background
 	vcuts("e_background", "02_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "02_cutset", "max_e_d0", 0.2);
        vcuts("e_background", "02_cutset", "e_electronID", 1.0);
 	cut_defs["e_background"]["02_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["e_background"]["02_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["e_background"]["02_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["02_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("e_background", "02_cutset", "min_e_relIso", 0.9);
 	vcuts("e_background", "02_cutset", "max_mu_eta", -1);
 	vcuts("e_background", "02_cutset", "max_e_eta", 2.4);
 	cut_defs["e_background"]["02_cutset"]["max_jet_eta"] = -1; 
 	vcuts("e_background", "02_cutset", "min_jet_pt", -1);	
 	vcuts("e_background", "02_cutset", "min_e_pt", 30);	
 	vcuts("e_background", "02_cutset", "min_mu_pt", -1);
 	cut_defs["e_background"]["02_cutset"]["min_no_jets"] = -1;

 	// 03_cutset: no muon
 	vcuts("e_background", "03_cutset", "min_mu_nHits", 11);
 	vcuts("e_background", "03_cutset", "max_mu_chi2", 10);
 	vcuts("e_background", "03_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "03_cutset", "max_e_d0", 0.2);
        vcuts("e_background", "03_cutset", "e_electronID", 1.0);
 	cut_defs["e_background"]["03_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["e_background"]["03_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["e_background"]["03_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["03_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("e_background", "03_cutset", "min_e_relIso", 0.9);
  	vcuts("e_background", "03_cutset", "min_mu_relIso", 0.95);
 	vcuts("e_background", "03_cutset", "max_mu_eta", 2.1);
 	vcuts("e_background", "03_cutset", "max_e_eta", 2.4);
 	cut_defs["e_background"]["03_cutset"]["max_jet_eta"] = -1; 
 	vcuts("e_background", "03_cutset", "min_jet_pt", -1);	
 	vcuts("e_background", "03_cutset", "min_e_pt", 30);	
 	vcuts("e_background", "03_cutset", "min_mu_pt", 30);
 	cut_defs["e_background"]["03_cutset"]["min_no_jets"] = -1;

 	// 01_cutset: jet electron cleaning
 	vcuts("e_background", "04_cutset", "min_mu_nHits", 11);
 	vcuts("e_background", "04_cutset", "max_mu_chi2", 10);
 	vcuts("e_background", "04_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "04_cutset", "max_e_d0", 0.2);
        vcuts("e_background", "04_cutset", "e_electronID", 1.0);
 	cut_defs["e_background"]["04_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["e_background"]["04_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["e_background"]["04_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["04_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("e_background", "04_cutset", "min_e_relIso", 0.9);
  	vcuts("e_background", "04_cutset", "min_mu_relIso", 0.95);
 	vcuts("e_background", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("e_background", "04_cutset", "max_e_eta", 2.4);
 	cut_defs["e_background"]["04_cutset"]["max_jet_eta"] = -1; 
 	vcuts("e_background", "04_cutset", "min_jet_pt", -1);	
 	vcuts("e_background", "04_cutset", "min_e_pt", 30);	
 	vcuts("e_background", "04_cutset", "min_mu_pt", 30);
 	cut_defs["e_background"]["04_cutset"]["min_no_jets"] = -1;

 	// 01_cutset: 4 jets
 	vcuts("e_background", "05_cutset", "min_mu_nHits", 11);
 	vcuts("e_background", "05_cutset", "max_mu_chi2", 10);
 	vcuts("e_background", "05_cutset", "max_mu_d0", -1);
 	vcuts("e_background", "05_cutset", "max_e_d0", 0.2);
        vcuts("e_background", "05_cutset", "e_electronID", 1.0);
 	cut_defs["e_background"]["05_cutset"]["max_nisolated_e"] = 1;
 	cut_defs["e_background"]["05_cutset"]["max_nisolated_mu"] = 0;
 	cut_defs["e_background"]["05_cutset"]["min_nisolated_e"] = 1;
 	cut_defs["e_background"]["05_cutset"]["min_nisolated_mu"] = -1;
  	vcuts("e_background", "05_cutset", "min_e_relIso", 0.9);
  	vcuts("e_background", "05_cutset", "min_mu_relIso", 0.95);
 	vcuts("e_background", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("e_background", "05_cutset", "max_e_eta", 2.4);
 	cut_defs["e_background"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("e_background", "05_cutset", "min_jet_pt", 30);	
 	vcuts("e_background", "05_cutset", "min_e_pt", 30);	
 	vcuts("e_background", "05_cutset", "min_mu_pt", 30);
 	cut_defs["e_background"]["05_cutset"]["min_no_jets"] = 4;
        /*
         *      cuts for which the global selection cuts are not applied
         */
	vcuts("electron", "without_cuts", "min_jet_pt", 0.0);
	vcuts("e_background", "without_cuts", "min_jet_pt", 0.0);

	
}
void CutSelector::define_cuts_01X09()
{
	/*
	 *	declare cut vectors
	 *	remember to delete them in the destructor!
	 */

	std::vector<double> *jetcuts100603030 = new std::vector<double>();
	jetcuts100603030->push_back(100);
	jetcuts100603030->push_back(60);
	jetcuts100603030->push_back(30);
	jetcuts100603030->push_back(30);
	cuts_to_be_deleted.push_back(jetcuts100603030);

	/*
	 *	MUON CUTS
	 */

 	// 05_cutset: preselection
 	vcuts("muon", "01_cutset", "max_mu_d0", -1);
 	vcuts("muon", "01_cutset", "max_e_d0", -1);
 	cut_defs["muon"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["muon"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["muon"]["01_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["01_cutset"]["min_nisolated_mu"] = -1;
 	vcuts("muon", "01_cutset", "max_mu_eta", -1);
 	vcuts("muon", "01_cutset", "max_e_eta", -1);
 	cut_defs["muon"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("muon", "01_cutset", "min_jet_pt", -1);	
 	vcuts("muon", "01_cutset", "min_e_pt", -1);	
 	vcuts("muon", "01_cutset", "min_mu_pt", -1);	
 	vcuts("muon", "01_cutset", "max_mu_trackiso", -1);
 	vcuts("muon", "01_cutset", "max_mu_caliso", -1);
 	vcuts("muon", "01_cutset", "max_e_trackiso", -1);
 	vcuts("muon", "01_cutset", "max_e_caliso", -1);
 	cut_defs["muon"]["01_cutset"]["min_no_jets"] = -1;
 	cut_defs["muon"]["01_cutset"]["mu_type"] = 0;

  	// 02_cutset: > 1 muon, pt>20, eta < 2.1; > 4 jets, pt>30, eta < 2.4
  	vcuts("muon", "02_cutset", "max_mu_d0", -1);
  	vcuts("muon", "02_cutset", "max_e_d0", -1);
  	cut_defs["muon"]["02_cutset"]["max_nisolated_e"] = -1;
  	cut_defs["muon"]["02_cutset"]["max_nisolated_mu"] = -1;
  	cut_defs["muon"]["02_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["02_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "02_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "02_cutset", "max_e_eta", -1);
  	cut_defs["muon"]["02_cutset"]["max_jet_eta"] =2.4; 
  	vcuts("muon", "02_cutset", "min_jet_pt",30.0);	
  	vcuts("muon", "02_cutset", "min_e_pt", -1);	
  	vcuts("muon", "02_cutset", "min_mu_pt", 20.0);	
  	vcuts("muon", "02_cutset", "max_mu_trackiso", -1);
  	vcuts("muon", "02_cutset", "max_mu_caliso", -1);
  	vcuts("muon", "02_cutset", "max_e_trackiso", -1);
  	vcuts("muon", "02_cutset", "max_e_caliso", -1);
  	cut_defs["muon"]["02_cutset"]["min_no_jets"] = 4;
 	cut_defs["muon"]["02_cutset"]["mu_type"] = 0;

  	// 03_cutset: > 1 muon, pt > 30, eta < 2.1
  	vcuts("muon", "03_cutset", "min_jet_pt",30.0);	
  	cut_defs["muon"]["03_cutset"]["min_no_jets"] = 4;
  	vcuts("muon", "03_cutset", "min_mu_pt", 30);	
  	vcuts("muon", "03_cutset", "min_e_pt", -1);	
  	vcuts("muon", "03_cutset", "max_mu_d0", 0.2);
  	vcuts("muon", "03_cutset", "max_e_d0", -1);
  	cut_defs["muon"]["03_cutset"]["max_nisolated_e"] = -1;
  	cut_defs["muon"]["03_cutset"]["max_nisolated_mu"] = -1;
  	cut_defs["muon"]["03_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["03_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("muon", "03_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "03_cutset", "max_e_eta", -1);
  	cut_defs["muon"]["03_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("muon", "03_cutset", "max_mu_trackiso", -1);
  	vcuts("muon", "03_cutset", "max_mu_caliso", -1);
  	vcuts("muon", "03_cutset", "max_e_trackiso", -1);
  	vcuts("muon", "03_cutset", "max_e_caliso", -1);
 	cut_defs["muon"]["03_cutset"]["mu_type"] = 0;

 	// 04_cutset: lepton isolation, exactly 1 lepton
 	vcuts("muon", "04_cutset", "min_jet_pt",30.0);	
 	cut_defs["muon"]["04_cutset"]["min_no_jets"] = 4;
 	vcuts("muon", "04_cutset", "min_mu_pt", 30);	
 	vcuts("muon", "04_cutset", "min_e_pt", 30);	
 	cut_defs["muon"]["04_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["muon"]["04_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["04_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["04_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "04_cutset", "max_e_eta", 2.4);
 	cut_defs["muon"]["04_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "04_cutset", "max_mu_trackiso", 3);
 	vcuts("muon", "04_cutset", "max_mu_caliso", 1);
 	vcuts("muon", "04_cutset", "max_e_trackiso", 5);
 	vcuts("muon", "04_cutset", "max_e_caliso", 10);
 	vcuts("muon", "04_cutset", "max_mu_d0", 0.2);
 	vcuts("muon", "04_cutset", "max_e_d0", 0.2);
 	cut_defs["muon"]["04_cutset"]["mu_type"] = 0;

	// 05_cutset: asymmetric jet cuts
 	vcuts("muon", "05_cutset", "min_jet_pt", jetcuts100603030);
 	cut_defs["muon"]["05_cutset"]["min_no_jets"] = 4;
 	vcuts("muon", "05_cutset", "min_mu_pt", 30);	
 	vcuts("muon", "05_cutset", "min_e_pt", 30);	
 	cut_defs["muon"]["05_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["muon"]["05_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["05_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["05_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "05_cutset", "max_e_eta", 2.4);
 	cut_defs["muon"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "05_cutset", "max_mu_trackiso", 3);
 	vcuts("muon", "05_cutset", "max_mu_caliso", 1);
 	vcuts("muon", "05_cutset", "max_e_trackiso", 5);
 	vcuts("muon", "05_cutset", "max_e_caliso", 10);
 	vcuts("muon", "05_cutset", "max_mu_d0", 0.2);
 	vcuts("muon", "05_cutset", "max_e_d0", 0.2);
 	cut_defs["muon"]["05_cutset"]["mu_type"] = 0;

 	// 06_cutset: asymmetric jet cuts bcut 3 + 11% bjet eff
 	vcuts("muon", "06_cutset", "min_jet_pt", jetcuts100603030);
 	cut_defs["muon"]["06_cutset"]["min_no_jets"] = 4;
 	vcuts("muon", "06_cutset", "min_mu_pt", 30);	
 	vcuts("muon", "06_cutset", "min_e_pt", 30);	
 	cut_defs["muon"]["06_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["muon"]["06_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["06_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["06_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "06_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "06_cutset", "max_e_eta", 2.4);
 	cut_defs["muon"]["06_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "06_cutset", "max_mu_trackiso", 3);
 	vcuts("muon", "06_cutset", "max_mu_caliso", 1);
 	vcuts("muon", "06_cutset", "max_e_trackiso", 5);
 	vcuts("muon", "06_cutset", "max_e_caliso", 10);
 	vcuts("muon", "06_cutset", "max_mu_d0", 0.2);
 	vcuts("muon", "06_cutset", "max_e_d0", 0.2);
 	cut_defs["muon"]["06_cutset"]["name_btag"] = 4; 
 	vcuts("muon", "06_cutset", "min_btag", 1.8);
 	cut_defs["muon"]["06_cutset"]["mu_type"] = 0;

 	// 07_cutset: asymmetric jet cuts bcut 3
 	vcuts("muon", "07_cutset", "min_jet_pt", jetcuts100603030);	
 	cut_defs["muon"]["07_cutset"]["min_no_jets"] = 4;
 	vcuts("muon", "07_cutset", "min_mu_pt", 30);	
 	vcuts("muon", "07_cutset", "min_e_pt", 30);	
 	cut_defs["muon"]["07_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["muon"]["07_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["07_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["07_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "07_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "07_cutset", "max_e_eta", 2.4);
 	cut_defs["muon"]["07_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "07_cutset", "max_mu_trackiso", 3);
 	vcuts("muon", "07_cutset", "max_mu_caliso", 1);
 	vcuts("muon", "07_cutset", "max_e_trackiso", 5);
 	vcuts("muon", "07_cutset", "max_e_caliso", 10);
 	vcuts("muon", "07_cutset", "max_mu_d0", 0.2);
 	vcuts("muon", "07_cutset", "max_e_d0", 0.2);
 	cut_defs["muon"]["07_cutset"]["name_btag"] = 4; 
 	vcuts("muon", "07_cutset", "min_btag", 3);
 	cut_defs["muon"]["07_cutset"]["mu_type"] = 0;

 	// 08_cutset: asymmetric jet cuts bcut 3 - 4.7% lightjet eff
 	vcuts("muon", "08_cutset", "min_jet_pt", jetcuts100603030);	
 	cut_defs["muon"]["08_cutset"]["min_no_jets"] = 4;
 	vcuts("muon", "08_cutset", "min_mu_pt", 30);	
 	vcuts("muon", "08_cutset", "min_e_pt", 30);	
 	cut_defs["muon"]["08_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["muon"]["08_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["muon"]["08_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["08_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "08_cutset", "max_mu_eta", 2.1);
 	vcuts("muon", "08_cutset", "max_e_eta", 2.4);
 	cut_defs["muon"]["08_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("muon", "08_cutset", "max_mu_trackiso", 3);
 	vcuts("muon", "08_cutset", "max_mu_caliso", 1);
 	vcuts("muon", "08_cutset", "max_e_trackiso", 5);
 	vcuts("muon", "08_cutset", "max_e_caliso", 10);
 	vcuts("muon", "08_cutset", "max_mu_d0", 0.2);
 	vcuts("muon", "08_cutset", "max_e_d0", 0.2);
 	cut_defs["muon"]["08_cutset"]["name_btag"] = 4; 
 	vcuts("muon", "08_cutset", "min_btag", 5);
 	cut_defs["muon"]["08_cutset"]["mu_type"] = 0;


 	// 09_cutset: Francisco Yumicevas Selection
 	vcuts("muon", "09_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "09_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "09_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "09_cutset", "min_mu_pt", 20);
  	vcuts("muon", "09_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "09_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["09_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["09_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "09_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "09_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("muon", "09_cutset", "min_e_pt", 30);	
        vcuts("muon", "09_cutset", "e_electronID", 1.0);
        vcuts("muon", "09_cutset", "min_e_relIso", 0.9);
  	vcuts("muon", "09_cutset", "max_e_eta", 2.4);
  	cut_defs["muon"]["09_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("muon", "09_cutset", "min_jet_pt",30.0);	
  	cut_defs["muon"]["09_cutset"]["min_no_jets"] = 4;
  	cut_defs["muon"]["09_cutset"]["max_no_jets"] = 7;
  	cut_defs["muon"]["09_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["muon"]["09_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["muon"]["09_cutset"]["mu_type"] = 0;

         /*
	 *	MUON BACKGROUND CUTS
         */

 	// 01_cutset: preselection + trigger
 	vcuts("mu_background", "01_cutset", "max_mu_d0", -1);
 	vcuts("mu_background", "01_cutset", "max_e_d0", -1);
 	cut_defs["mu_background"]["01_cutset"]["max_nisolated_e"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["max_nisolated_mu"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["min_nisolated_mu"] = -1;
 	vcuts("mu_background", "01_cutset", "max_mu_eta", -1);
 	vcuts("mu_background", "01_cutset", "max_e_eta", -1);
 	cut_defs["mu_background"]["01_cutset"]["max_jet_eta"] = -1; 
 	vcuts("mu_background", "01_cutset", "min_jet_pt", -1);	
 	vcuts("mu_background", "01_cutset", "min_e_pt", -1);	
 	vcuts("mu_background", "01_cutset", "min_mu_pt", -1);	
 	vcuts("mu_background", "01_cutset", "max_mu_trackiso", -1);
 	vcuts("mu_background", "01_cutset", "max_mu_caliso", -1);
 	vcuts("mu_background", "01_cutset", "max_e_trackiso", -1);
 	vcuts("mu_background", "01_cutset", "max_e_caliso", -1);
 	cut_defs["mu_background"]["01_cutset"]["min_no_jets"] = -1;
 	cut_defs["mu_background"]["01_cutset"]["mu_type"] = 0;

  	// 02_cutset: > 1 mu_background, pt>20, eta < 2.0; > 4 jets, pt>30, eta < 2.4
  	vcuts("mu_background", "02_cutset", "max_mu_d0", -1);
  	vcuts("mu_background", "02_cutset", "max_e_d0", -1);
  	cut_defs["mu_background"]["02_cutset"]["max_nisolated_e"] = -1;
  	cut_defs["mu_background"]["02_cutset"]["max_nisolated_mu"] = -1;
  	cut_defs["mu_background"]["02_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["02_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "02_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "02_cutset", "max_e_eta", -1);
  	cut_defs["mu_background"]["02_cutset"]["max_jet_eta"] =2.4; 
  	vcuts("mu_background", "02_cutset", "min_jet_pt",30.0);	
  	vcuts("mu_background", "02_cutset", "min_e_pt", -1);	
  	vcuts("mu_background", "02_cutset", "min_mu_pt", 20.0);	
  	vcuts("mu_background", "02_cutset", "max_mu_trackiso", -1);
  	vcuts("mu_background", "02_cutset", "max_mu_caliso", -1);
  	vcuts("mu_background", "02_cutset", "max_e_trackiso", -1);
  	vcuts("mu_background", "02_cutset", "max_e_caliso", -1);
  	cut_defs["mu_background"]["02_cutset"]["min_no_jets"] = 4;
 	cut_defs["mu_background"]["02_cutset"]["mu_type"] = 0;

  	// 03_cutset: > 1 mu_background, pt > 30, eta < 2.0
  	vcuts("mu_background", "03_cutset", "min_jet_pt",30.0);	
  	cut_defs["mu_background"]["03_cutset"]["min_no_jets"] = 4;
  	vcuts("mu_background", "03_cutset", "min_mu_pt", 30);	
  	vcuts("mu_background", "03_cutset", "min_e_pt", -1);	
  	vcuts("mu_background", "03_cutset", "max_mu_d0", 0.2);
  	vcuts("mu_background", "03_cutset", "max_e_d0", -1);
  	cut_defs["mu_background"]["03_cutset"]["max_nisolated_e"] = -1;
  	cut_defs["mu_background"]["03_cutset"]["max_nisolated_mu"] = -1;
  	cut_defs["mu_background"]["03_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["03_cutset"]["min_nisolated_mu"] = 1;
  	vcuts("mu_background", "03_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "03_cutset", "max_e_eta", -1);
  	cut_defs["mu_background"]["03_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("mu_background", "03_cutset", "max_mu_trackiso", -1);
  	vcuts("mu_background", "03_cutset", "max_mu_caliso", -1);
  	vcuts("mu_background", "03_cutset", "max_e_trackiso", -1);
  	vcuts("mu_background", "03_cutset", "max_e_caliso", -1);
 	cut_defs["mu_background"]["03_cutset"]["mu_type"] = 0;

 	// 04_cutset: lepton isolation, exactly 1 lepton
 	vcuts("mu_background", "04_cutset", "min_jet_pt",30.0);	
 	cut_defs["mu_background"]["04_cutset"]["min_no_jets"] = 4;
 	vcuts("mu_background", "04_cutset", "min_mu_pt", 30);	
 	vcuts("mu_background", "04_cutset", "min_e_pt", 30);	
 	cut_defs["mu_background"]["04_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["mu_background"]["04_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["04_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["04_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "04_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "04_cutset", "max_e_eta", 2.4);
 	cut_defs["mu_background"]["04_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "04_cutset", "max_mu_trackiso", 3);
 	vcuts("mu_background", "04_cutset", "max_mu_caliso", 1);
 	vcuts("mu_background", "04_cutset", "max_e_trackiso", 5);
 	vcuts("mu_background", "04_cutset", "max_e_caliso", 10);
 	vcuts("mu_background", "04_cutset", "max_mu_d0", 0.2);
 	vcuts("mu_background", "04_cutset", "max_e_d0", 0.2);
 	cut_defs["mu_background"]["04_cutset"]["mu_type"] = 0;

	// 05_cutset: asymmetric jet cuts
 	vcuts("mu_background", "05_cutset", "min_jet_pt", jetcuts100603030);
 	cut_defs["mu_background"]["05_cutset"]["min_no_jets"] = 4;
 	vcuts("mu_background", "05_cutset", "min_mu_pt", 30);	
 	vcuts("mu_background", "05_cutset", "min_e_pt", 30);	
 	cut_defs["mu_background"]["05_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["mu_background"]["05_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["05_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["05_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "05_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "05_cutset", "max_e_eta", 2.4);
 	cut_defs["mu_background"]["05_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "05_cutset", "max_mu_trackiso", 3);
 	vcuts("mu_background", "05_cutset", "max_mu_caliso", 1);
 	vcuts("mu_background", "05_cutset", "max_e_trackiso", 5);
 	vcuts("mu_background", "05_cutset", "max_e_caliso", 10);
 	vcuts("mu_background", "05_cutset", "max_mu_d0", 0.2);
 	vcuts("mu_background", "05_cutset", "max_e_d0", 0.2);
 	cut_defs["mu_background"]["05_cutset"]["mu_type"] = 0;

 	// 06_cutset: asymmetric jet cuts bcut 3 + 11% bjet eff
 	vcuts("mu_background", "06_cutset", "min_jet_pt", jetcuts100603030);
 	cut_defs["mu_background"]["06_cutset"]["min_no_jets"] = 4;
 	vcuts("mu_background", "06_cutset", "min_mu_pt", 30);	
 	vcuts("mu_background", "06_cutset", "min_e_pt", 30);	
 	cut_defs["mu_background"]["06_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["mu_background"]["06_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["06_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["06_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "06_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "06_cutset", "max_e_eta", 2.4);
 	cut_defs["mu_background"]["06_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "06_cutset", "max_mu_trackiso", 3);
 	vcuts("mu_background", "06_cutset", "max_mu_caliso", 1);
 	vcuts("mu_background", "06_cutset", "max_e_trackiso", 5);
 	vcuts("mu_background", "06_cutset", "max_e_caliso", 10);
 	vcuts("mu_background", "06_cutset", "max_mu_d0", 0.2);
 	vcuts("mu_background", "06_cutset", "max_e_d0", 0.2);
 	cut_defs["mu_background"]["06_cutset"]["name_btag"] = 4; 
 	vcuts("mu_background", "06_cutset", "min_btag", 1.8);
 	cut_defs["mu_background"]["06_cutset"]["mu_type"] = 0;

 	// 07_cutset: asymmetric jet cuts bcut 3
 	vcuts("mu_background", "07_cutset", "min_jet_pt", jetcuts100603030);	
 	cut_defs["mu_background"]["07_cutset"]["min_no_jets"] = 4;
 	vcuts("mu_background", "07_cutset", "min_mu_pt", 30);	
 	vcuts("mu_background", "07_cutset", "min_e_pt", 30);	
 	cut_defs["mu_background"]["07_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["mu_background"]["07_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["07_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["07_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "07_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "07_cutset", "max_e_eta", 2.4);
 	cut_defs["mu_background"]["07_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "07_cutset", "max_mu_trackiso", 3);
 	vcuts("mu_background", "07_cutset", "max_mu_caliso", 1);
 	vcuts("mu_background", "07_cutset", "max_e_trackiso", 5);
 	vcuts("mu_background", "07_cutset", "max_e_caliso", 10);
 	vcuts("mu_background", "07_cutset", "max_mu_d0", 0.2);
 	vcuts("mu_background", "07_cutset", "max_e_d0", 0.2);
 	cut_defs["mu_background"]["07_cutset"]["name_btag"] = 4; 
 	vcuts("mu_background", "07_cutset", "min_btag", 3);
 	cut_defs["mu_background"]["07_cutset"]["mu_type"] = 0;

 	// 08_cutset: asymmetric jet cuts bcut 3 - 4.7% lightjet eff
 	vcuts("mu_background", "08_cutset", "min_jet_pt", jetcuts100603030);	
 	cut_defs["mu_background"]["08_cutset"]["min_no_jets"] = 4;
 	vcuts("mu_background", "08_cutset", "min_mu_pt", 30);	
 	vcuts("mu_background", "08_cutset", "min_e_pt", 30);	
 	cut_defs["mu_background"]["08_cutset"]["max_nisolated_e"] = 0;
 	cut_defs["mu_background"]["08_cutset"]["max_nisolated_mu"] = 1;
 	cut_defs["mu_background"]["08_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["08_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "08_cutset", "max_mu_eta", 2.1);
 	vcuts("mu_background", "08_cutset", "max_e_eta", 2.4);
 	cut_defs["mu_background"]["08_cutset"]["max_jet_eta"] = 2.4; 
 	vcuts("mu_background", "08_cutset", "max_mu_trackiso", 3);
 	vcuts("mu_background", "08_cutset", "max_mu_caliso", 1);
 	vcuts("mu_background", "08_cutset", "max_e_trackiso", 5);
 	vcuts("mu_background", "08_cutset", "max_e_caliso", 10);
 	vcuts("mu_background", "08_cutset", "max_mu_d0", 0.2);
 	vcuts("mu_background", "08_cutset", "max_e_d0", 0.2);
 	cut_defs["mu_background"]["08_cutset"]["name_btag"] = 4; 
 	vcuts("mu_background", "08_cutset", "min_btag", 5);
 	cut_defs["mu_background"]["08_cutset"]["mu_type"] = 0;


 	// 09_cutset: Francisco Yumicevas Selection
 	vcuts("mu_background", "09_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "09_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "09_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "09_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "09_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "09_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["09_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["09_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "09_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "09_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("mu_background", "09_cutset", "min_e_pt", 30);	
        vcuts("mu_background", "09_cutset", "e_electronID", 1.0);
        vcuts("mu_background", "09_cutset", "min_e_relIso", 0.9);
  	vcuts("mu_background", "09_cutset", "max_e_eta", 2.4);
  	cut_defs["mu_background"]["09_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("mu_background", "09_cutset", "min_jet_pt",30.0);	
  	cut_defs["mu_background"]["09_cutset"]["min_no_jets"] = 4;
  	cut_defs["mu_background"]["09_cutset"]["max_no_jets"] = 7;
  	cut_defs["mu_background"]["09_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["mu_background"]["09_cutset"]["min_nisolated_e"] = -1;
 	cut_defs["mu_background"]["09_cutset"]["mu_type"] = 0;
  /*
          *      ELECTRON CUTS
          */

         /*
          *      ELECTRON BACKGROUND CUTS
          */

        /*
         *      global cuts for all selections defined so far
         */
        synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
                        if(type_iter->first == "muon" || type_iter->first == "mu_background"){
				//vset_if_not_set(type_iter->first, set_iter->first, "trigger", 85);
                        }else if(type_iter->first == "electron" || type_iter->first == "e_background"){
				//vset_if_not_set(type_iter->first, set_iter->first, "trigger", 50);
                        }
                }
        }

        /*
         *      cuts for which the global selection cuts are not applied
         */
	vcuts("muon", "without_cuts", "min_jet_pt", 0.0);
	vcuts("mu_background", "without_cuts", "min_jet_pt", 0.0);
}

std::string CutSelector::get_preselection_id(int npreselection)
{
	Tools tools;

	if(npreselection < 10)
		return "preselection_step0"+tools.stringify(npreselection);
	else
		return "preselection_step"+tools.stringify(npreselection);
}

void CutSelector::set_if_not_set(std::string type, std::string set, std::string cut, double value)
{
	if(cut_defs[type][set].find(cut) == cut_defs[type][set].end()){
		cut_defs[type][set][cut] = value;	
	}
}

void CutSelector::vset_if_not_set(std::string type, std::string set, std::string cut, double value)
{
	if(vcut_defs[type][set].find(cut) == vcut_defs[type][set].end()){
		vcuts(type, set, cut, value);	
	}
}

void CutSelector::vset_if_not_set(std::string type, std::string set, std::string cut, std::vector<double> *value)
{
	if(vcut_defs[type][set].find(cut) == vcut_defs[type][set].end()){
		vcuts(type, set, cut, value);	
	}
}


void CutSelector::synchronise_maps()
{
        for(std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > >::iterator type_iter = vcut_defs.begin();
                type_iter != vcut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, std::vector<double>* > >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			cut_defs[type_iter->first][set_iter->first]["dummy"] = -1;
		}
	}

        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			vcuts(type_iter->first, set_iter->first, "dummy", -1);
		}
	}
}

void CutSelector::vcuts(std::string type, std::string set, std::string cut, double value)
{
	std::vector<double> *cut_vector = new std::vector<double>();
	if(value != -1)
		cut_vector->push_back(value);
	vcut_defs[type][set][cut] = cut_vector;	
	cuts_to_be_deleted.push_back(cut_vector);
}

void CutSelector::vcuts(std::string type, std::string set, std::string cut, std::vector<double> *cut_vector)
{
	vcut_defs[type][set][cut] = cut_vector;
}

void CutSelector::complete_cuts()
{
	std::vector<std::string> all_cuts;
	std::vector<std::string> all_v_cuts;

	all_cuts.push_back("min_met");
	all_cuts.push_back("max_ht");
	all_cuts.push_back("min_ht");
	all_cuts.push_back("min_no_jets");
	all_cuts.push_back("max_no_jets");
	all_cuts.push_back("max_nisolated_e");
	all_cuts.push_back("max_nisolated_mu");
	all_cuts.push_back("min_nisolated_e");
	all_cuts.push_back("min_nisolated_mu");
	all_cuts.push_back("max_nloose_e");
	all_cuts.push_back("max_nloose_mu");
	all_cuts.push_back("max_jet_eta");
	all_cuts.push_back("JES_factor");
	all_cuts.push_back("mu_type");
	all_cuts.push_back("e_type");
	all_cuts.push_back("loose_mu_type");
	all_cuts.push_back("loose_e_type");
	all_cuts.push_back("Z_rejection_width");
	all_cuts.push_back("name_btag");
	all_cuts.push_back("min_M3");
	all_cuts.push_back("min_mindiffM3");
	all_cuts.push_back("min_chi2");
	all_cuts.push_back("max_chi2");

	all_v_cuts.push_back("min_mu_pt");
	all_v_cuts.push_back("max_mu_trackiso");
	all_v_cuts.push_back("max_mu_caliso");
	all_v_cuts.push_back("max_mu_ecaliso");
	all_v_cuts.push_back("max_mu_hcaliso");
	all_v_cuts.push_back("max_mu_hcal_veto_cone");
	all_v_cuts.push_back("max_mu_ecal_veto_cone");
	all_v_cuts.push_back("min_mu_relIso");
	all_v_cuts.push_back("min_mu_dR");
	all_v_cuts.push_back("min_e_pt");
	all_v_cuts.push_back("max_e_trackiso");
	all_v_cuts.push_back("max_e_caliso");
	all_v_cuts.push_back("max_e_ecaliso");
	all_v_cuts.push_back("max_e_hcaliso");
	all_v_cuts.push_back("max_e_hcal_veto_cone");
	all_v_cuts.push_back("max_e_ecal_veto_cone");
	all_v_cuts.push_back("min_e_relIso");
	all_v_cuts.push_back("min_e_dR");
	all_v_cuts.push_back("min_jet_pt");
        all_v_cuts.push_back("max_mu_chi2");
        all_v_cuts.push_back("max_mu_d0");
        all_v_cuts.push_back("max_mu_d0sig");
        all_v_cuts.push_back("min_mu_nHits");
        all_v_cuts.push_back("mu_electronID");
        all_v_cuts.push_back("max_e_chi2");
        all_v_cuts.push_back("max_e_d0");
        all_v_cuts.push_back("max_e_d0sig");
        all_v_cuts.push_back("min_e_nHits");
        all_v_cuts.push_back("e_electronID");
        all_v_cuts.push_back("trigger");
	all_v_cuts.push_back("min_btag");
	all_v_cuts.push_back("max_mu_eta");
	all_v_cuts.push_back("max_e_eta");

	// loose lepton cuts
	all_v_cuts.push_back("min_loose_mu_pt");
	all_v_cuts.push_back("max_loose_mu_trackiso");
	all_v_cuts.push_back("max_loose_mu_caliso");
	all_v_cuts.push_back("max_loose_mu_ecaliso");
	all_v_cuts.push_back("max_loose_mu_hcaliso");
	all_v_cuts.push_back("max_loose_mu_hcal_veto_cone");
	all_v_cuts.push_back("max_loose_mu_ecal_veto_cone");
	all_v_cuts.push_back("min_loose_mu_relIso");
	all_v_cuts.push_back("min_loose_mu_dR");
	all_v_cuts.push_back("min_loose_e_pt");
	all_v_cuts.push_back("max_loose_e_trackiso");
	all_v_cuts.push_back("max_loose_e_caliso");
	all_v_cuts.push_back("max_loose_e_ecaliso");
	all_v_cuts.push_back("max_loose_e_hcaliso");
	all_v_cuts.push_back("max_loose_e_hcal_veto_cone");
	all_v_cuts.push_back("max_loose_e_ecal_veto_cone");
	all_v_cuts.push_back("min_loose_e_relIso");
	all_v_cuts.push_back("min_loose_e_dR");
        all_v_cuts.push_back("max_loose_mu_chi2");
        all_v_cuts.push_back("max_loose_mu_d0");
        all_v_cuts.push_back("max_loose_mu_d0sig");
        all_v_cuts.push_back("min_loose_mu_nHits");
        all_v_cuts.push_back("loose_mu_electronID");
        all_v_cuts.push_back("max_loose_e_chi2");
        all_v_cuts.push_back("max_loose_e_d0");
        all_v_cuts.push_back("max_loose_e_d0sig");
        all_v_cuts.push_back("min_loose_e_nHits");
        all_v_cuts.push_back("loose_e_electronID");
	all_v_cuts.push_back("max_loose_mu_eta");
	all_v_cuts.push_back("max_loose_e_eta");

	synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > >::iterator type_iter = vcut_defs.begin();
                type_iter != vcut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, std::vector<double>* > >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			for(std::vector<std::string>::iterator cut_name = all_v_cuts.begin();
			    cut_name != all_v_cuts.end();
			    ++cut_name){
				if(set_iter->second.find(*cut_name) == set_iter->second.end()){
					vcuts(type_iter->first, set_iter->first, *cut_name, -1);
				}
			}
		}
	}


        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			for(std::vector<std::string>::iterator cut_name = all_cuts.begin();
			    cut_name != all_cuts.end();
			    ++cut_name){
				if(set_iter->second.find(*cut_name) == set_iter->second.end()){
					set_iter->second[*cut_name] = -1;
				}
			}
		}
	}

}

void CutSelector::set_cuts()
{
        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
			std::string full_id = dataset_id+"|"+type_iter->first+"|"+set_iter->first;
			std::string id = dataset_id+"_"+set_iter->first;
			plot_generators[type_iter->first][id] = new PlotGenerator(full_id);
			cuts[type_iter->first][id] = new Cuts(full_id);

			// jet selection cuts
			cuts[type_iter->first][id]->set_JES_factor(cut_defs[type_iter->first][set_iter->first]["JES_factor"]);
			cuts[type_iter->first][id]->set_min_njets((int) cut_defs[type_iter->first][set_iter->first]["min_no_jets"]);
			cuts[type_iter->first][id]->set_max_njets((int) cut_defs[type_iter->first][set_iter->first]["max_no_jets"]);
			cuts[type_iter->first][id]->set_max_jet_eta(cut_defs[type_iter->first][set_iter->first]["max_jet_eta"]);
			cuts[type_iter->first][id]->set_min_jet_pt(vcut_defs[type_iter->first][set_iter->first]["min_jet_pt"]);

			// tight muon selection cuts
			cuts[type_iter->first][id]->set_min_nisolated_mu((int) cut_defs[type_iter->first][set_iter->first]["min_nisolated_mu"]);
			cuts[type_iter->first][id]->set_max_nisolated_mu((int) cut_defs[type_iter->first][set_iter->first]["max_nisolated_mu"]);
			cuts[type_iter->first][id]->set_max_mu_trackiso(vcut_defs[type_iter->first][set_iter->first]["max_mu_trackiso"]);
			cuts[type_iter->first][id]->set_max_mu_ecaliso(vcut_defs[type_iter->first][set_iter->first]["max_mu_ecaliso"]);
			cuts[type_iter->first][id]->set_max_mu_caliso(vcut_defs[type_iter->first][set_iter->first]["max_mu_caliso"]);
			cuts[type_iter->first][id]->set_max_mu_hcaliso(vcut_defs[type_iter->first][set_iter->first]["max_mu_hcaliso"]);
			cuts[type_iter->first][id]->set_max_mu_hcal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_mu_hcal_veto_cone"]);
			cuts[type_iter->first][id]->set_max_mu_ecal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_mu_ecal_veto_cone"]);
			cuts[type_iter->first][id]->set_min_mu_dR(vcut_defs[type_iter->first][set_iter->first]["min_mu_dR"]);
			cuts[type_iter->first][id]->set_min_mu_relIso(vcut_defs[type_iter->first][set_iter->first]["min_mu_relIso"]);
			cuts[type_iter->first][id]->set_min_mu_pt(vcut_defs[type_iter->first][set_iter->first]["min_mu_pt"]);
			cuts[type_iter->first][id]->set_min_mu_nHits(vcut_defs[type_iter->first][set_iter->first]["min_mu_nHits"]);
			cuts[type_iter->first][id]->set_max_mu_d0(vcut_defs[type_iter->first][set_iter->first]["max_mu_d0"]);
			cuts[type_iter->first][id]->set_max_mu_d0sig(vcut_defs[type_iter->first][set_iter->first]["max_mu_d0sig"]);
			cuts[type_iter->first][id]->set_max_mu_chi2(vcut_defs[type_iter->first][set_iter->first]["max_mu_chi2"]);
			cuts[type_iter->first][id]->set_mu_type(cut_defs[type_iter->first][set_iter->first]["mu_type"]);
			cuts[type_iter->first][id]->set_mu_electronID(vcut_defs[type_iter->first][set_iter->first]["mu_electronID"]);
			cuts[type_iter->first][id]->set_max_mu_eta(vcut_defs[type_iter->first][set_iter->first]["max_mu_eta"]);

			// loose muon selection cuts
			cuts[type_iter->first][id]->set_max_nloose_mu((int) cut_defs[type_iter->first][set_iter->first]["max_nloose_mu"]);
			cuts[type_iter->first][id]->set_max_loose_mu_trackiso(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_trackiso"]);
			cuts[type_iter->first][id]->set_max_loose_mu_ecaliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_ecaliso"]);
			cuts[type_iter->first][id]->set_max_loose_mu_caliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_caliso"]);
			cuts[type_iter->first][id]->set_max_loose_mu_hcaliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_hcaliso"]);
			cuts[type_iter->first][id]->set_max_loose_mu_hcal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_hcal_veto_cone"]);
			cuts[type_iter->first][id]->set_max_loose_mu_ecal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_ecal_veto_cone"]);
			cuts[type_iter->first][id]->set_min_loose_mu_dR(vcut_defs[type_iter->first][set_iter->first]["min_loose_mu_dR"]);
			cuts[type_iter->first][id]->set_min_loose_mu_relIso(vcut_defs[type_iter->first][set_iter->first]["min_loose_mu_relIso"]);
			cuts[type_iter->first][id]->set_min_loose_mu_pt(vcut_defs[type_iter->first][set_iter->first]["min_loose_mu_pt"]);
			cuts[type_iter->first][id]->set_min_loose_mu_nHits(vcut_defs[type_iter->first][set_iter->first]["min_loose_mu_nHits"]);
			cuts[type_iter->first][id]->set_max_loose_mu_d0(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_d0"]);
			cuts[type_iter->first][id]->set_max_loose_mu_d0sig(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_d0sig"]);
			cuts[type_iter->first][id]->set_max_loose_mu_chi2(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_chi2"]);
			cuts[type_iter->first][id]->set_loose_mu_type(cut_defs[type_iter->first][set_iter->first]["loose_mu_type"]);
			cuts[type_iter->first][id]->set_loose_mu_electronID(vcut_defs[type_iter->first][set_iter->first]["loose_mu_electronID"]);
			cuts[type_iter->first][id]->set_max_loose_mu_eta(vcut_defs[type_iter->first][set_iter->first]["max_loose_mu_eta"]);

			// tight electron selection cuts
			cuts[type_iter->first][id]->set_min_nisolated_e((int) cut_defs[type_iter->first][set_iter->first]["min_nisolated_e"]);
			cuts[type_iter->first][id]->set_max_nisolated_e((int) cut_defs[type_iter->first][set_iter->first]["max_nisolated_e"]);
			cuts[type_iter->first][id]->set_max_e_trackiso(vcut_defs[type_iter->first][set_iter->first]["max_e_trackiso"]);
			cuts[type_iter->first][id]->set_max_e_ecaliso(vcut_defs[type_iter->first][set_iter->first]["max_e_ecaliso"]);
			cuts[type_iter->first][id]->set_max_e_caliso(vcut_defs[type_iter->first][set_iter->first]["max_e_caliso"]);
			cuts[type_iter->first][id]->set_max_e_hcaliso(vcut_defs[type_iter->first][set_iter->first]["max_e_hcaliso"]);
			cuts[type_iter->first][id]->set_max_e_hcal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_e_hcal_veto_cone"]);
			cuts[type_iter->first][id]->set_max_e_ecal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_e_ecal_veto_cone"]);
			cuts[type_iter->first][id]->set_min_e_dR(vcut_defs[type_iter->first][set_iter->first]["min_e_dR"]);
			cuts[type_iter->first][id]->set_min_e_relIso(vcut_defs[type_iter->first][set_iter->first]["min_e_relIso"]);
			cuts[type_iter->first][id]->set_max_e_eta(vcut_defs[type_iter->first][set_iter->first]["max_e_eta"]);
			cuts[type_iter->first][id]->set_e_type(cut_defs[type_iter->first][set_iter->first]["e_type"]);
			cuts[type_iter->first][id]->set_min_e_pt(vcut_defs[type_iter->first][set_iter->first]["min_e_pt"]);
			cuts[type_iter->first][id]->set_min_e_nHits(vcut_defs[type_iter->first][set_iter->first]["min_e_nHits"]);
			cuts[type_iter->first][id]->set_max_e_d0(vcut_defs[type_iter->first][set_iter->first]["max_e_d0"]);
			cuts[type_iter->first][id]->set_max_e_d0sig(vcut_defs[type_iter->first][set_iter->first]["max_e_d0sig"]);
			cuts[type_iter->first][id]->set_max_e_chi2(vcut_defs[type_iter->first][set_iter->first]["max_e_chi2"]);
			cuts[type_iter->first][id]->set_e_electronID(vcut_defs[type_iter->first][set_iter->first]["e_electronID"]);

			// loose electron selection cuts
			cuts[type_iter->first][id]->set_max_nloose_e((int) cut_defs[type_iter->first][set_iter->first]["max_nloose_e"]);
			cuts[type_iter->first][id]->set_max_loose_e_trackiso(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_trackiso"]);
			cuts[type_iter->first][id]->set_max_loose_e_ecaliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_ecaliso"]);
			cuts[type_iter->first][id]->set_max_loose_e_caliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_caliso"]);
			cuts[type_iter->first][id]->set_max_loose_e_hcaliso(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_hcaliso"]);
			cuts[type_iter->first][id]->set_max_loose_e_hcal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_hcal_veto_cone"]);
			cuts[type_iter->first][id]->set_max_loose_e_ecal_veto_cone(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_ecal_veto_cone"]);
			cuts[type_iter->first][id]->set_min_loose_e_dR(vcut_defs[type_iter->first][set_iter->first]["min_loose_e_dR"]);
			cuts[type_iter->first][id]->set_min_loose_e_relIso(vcut_defs[type_iter->first][set_iter->first]["min_loose_e_relIso"]);
			cuts[type_iter->first][id]->set_max_loose_e_eta(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_eta"]);
			cuts[type_iter->first][id]->set_loose_e_type(cut_defs[type_iter->first][set_iter->first]["loose_e_type"]);
			cuts[type_iter->first][id]->set_min_loose_e_pt(vcut_defs[type_iter->first][set_iter->first]["min_loose_e_pt"]);
			cuts[type_iter->first][id]->set_min_loose_e_nHits(vcut_defs[type_iter->first][set_iter->first]["min_loose_e_nHits"]);
			cuts[type_iter->first][id]->set_max_loose_e_d0(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_d0"]);
			cuts[type_iter->first][id]->set_max_loose_e_d0sig(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_d0sig"]);
			cuts[type_iter->first][id]->set_max_loose_e_chi2(vcut_defs[type_iter->first][set_iter->first]["max_loose_e_chi2"]);
			cuts[type_iter->first][id]->set_loose_e_electronID(vcut_defs[type_iter->first][set_iter->first]["loose_e_electronID"]);

			//other cuts
			cuts[type_iter->first][id]->set_Z_rejection_width(cut_defs[type_iter->first][set_iter->first]["Z_rejection_width"]);
			cuts[type_iter->first][id]->set_min_btag(vcut_defs[type_iter->first][set_iter->first]["min_btag"]);
			cuts[type_iter->first][id]->set_name_btag(cut_defs[type_iter->first][set_iter->first]["name_btag"]);
			cuts[type_iter->first][id]->set_trigger(vcut_defs[type_iter->first][set_iter->first]["trigger"]);
			cuts[type_iter->first][id]->set_min_M3(cut_defs[type_iter->first][set_iter->first]["min_M3"]);
			cuts[type_iter->first][id]->set_min_mindiffM3(cut_defs[type_iter->first][set_iter->first]["min_mindiffM3"]);
			cuts[type_iter->first][id]->set_min_chi2(cut_defs[type_iter->first][set_iter->first]["min_chi2"]);
			cuts[type_iter->first][id]->set_max_chi2(cut_defs[type_iter->first][set_iter->first]["max_chi2"]);
			cuts[type_iter->first][id]->set_met_cut(cut_defs[type_iter->first][set_iter->first]["min_met"]);
			cuts[type_iter->first][id]->set_max_ht(cut_defs[type_iter->first][set_iter->first]["max_ht"]);
			cuts[type_iter->first][id]->set_min_ht(cut_defs[type_iter->first][set_iter->first]["min_ht"]);

			// pass t' mass to plot_generator
			plot_generators[type_iter->first][id]->set_tprime_mass(tprime_mass);

			// initialise plot generator with according cuts
			plot_generators[type_iter->first][id]->apply_cuts(cuts[type_iter->first][id]);
		}
        }
}

void CutSelector::set_tprime_mass(double tprimeMass)
{
	if(tprimeMass > 0)
		tprime_mass = tprimeMass;
}

double CutSelector::get_ht_cut()
{
	if(tprime_mass == 175) return 400;
	if(tprime_mass == 250) return 400;
	if(tprime_mass == 275) return 400;
	if(tprime_mass == 300) return 400;
	if(tprime_mass == 325) return 400;
	if(tprime_mass == 350) return 400;
	if(tprime_mass == 375) return 400;
	if(tprime_mass == 400) return 400;

	std::cerr << "WARNING: no ht cut found but accessed in CutSelector::get_ht_cut()" << std::endl;
	return -1;
}

void CutSelector::define_cuts_Francisco()
{
	/*
	 *	MUON CUTS
	 */

 	// 01_cutset: HLT
// 	vcuts("muon", "01_cutset", "min_mu_nHits", 11);
// 	vcuts("muon", "01_cutset", "max_mu_chi2", 10);
//  	vcuts("muon", "01_cutset", "max_mu_d0sig", 3);
//  	vcuts("muon", "01_cutset", "min_mu_pt", 20);
//  	vcuts("muon", "01_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "01_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["muon"]["01_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["muon"]["01_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("muon", "01_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("muon", "01_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("muon", "01_cutset", "min_e_pt", 30);	
//        vcuts("muon", "01_cutset", "e_electronID", 1.0);
//        vcuts("muon", "01_cutset", "min_e_relIso", 0.9);
//  	vcuts("muon", "01_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["01_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "01_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["01_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["01_cutset"]["max_no_jets"] = 7;
//  	cut_defs["muon"]["01_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["01_cutset"]["min_nisolated_e"] = -1;
 	vcuts("muon", "01_cutset", "trigger", 82);	// added as default
//  	cut_defs["muon"]["01_cutset"]["mu_type"] = 0;

 	// 02_cutset: global muons
// 	vcuts("muon", "02_cutset", "min_mu_nHits", 11);
// 	vcuts("muon", "02_cutset", "max_mu_chi2", 10);
//  	vcuts("muon", "02_cutset", "max_mu_d0sig", 3);
//  	vcuts("muon", "02_cutset", "min_mu_pt", 20);
//  	vcuts("muon", "02_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "02_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["muon"]["02_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["02_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("muon", "02_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("muon", "02_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("muon", "02_cutset", "min_e_pt", 30);	
//        vcuts("muon", "02_cutset", "e_electronID", 1.0);
//        vcuts("muon", "02_cutset", "min_e_relIso", 0.9);
//  	vcuts("muon", "02_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["02_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "02_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["02_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["02_cutset"]["max_no_jets"] = 7;
//  	cut_defs["muon"]["02_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["02_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["02_cutset"]["mu_type"] = 0;

 	// 03_cutset: global muons
// 	vcuts("muon", "03_cutset", "min_mu_nHits", 11);
// 	vcuts("muon", "03_cutset", "max_mu_chi2", 10);
//  	vcuts("muon", "03_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "03_cutset", "min_mu_pt", 20);
  	vcuts("muon", "03_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "03_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["muon"]["03_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["03_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("muon", "03_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("muon", "03_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("muon", "03_cutset", "min_e_pt", 30);	
//        vcuts("muon", "03_cutset", "e_electronID", 1.0);
//        vcuts("muon", "03_cutset", "min_e_relIso", 0.9);
//  	vcuts("muon", "03_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["03_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "03_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["03_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["03_cutset"]["max_no_jets"] = 7;
//  	cut_defs["muon"]["03_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["03_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["03_cutset"]["mu_type"] = 0;

 	// 04_cutset: global muons
	vcuts("muon", "04_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "04_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "04_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "04_cutset", "min_mu_pt", 20);
  	vcuts("muon", "04_cutset", "max_mu_eta", 2.1);
//  	vcuts("muon", "04_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["muon"]["04_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["04_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("muon", "04_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("muon", "04_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("muon", "04_cutset", "min_e_pt", 30);	
//        vcuts("muon", "04_cutset", "e_electronID", 1.0);
//        vcuts("muon", "04_cutset", "min_e_relIso", 0.9);
//  	vcuts("muon", "04_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["04_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "04_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["04_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["04_cutset"]["max_no_jets"] = 7;
//  	cut_defs["muon"]["04_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["04_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["04_cutset"]["mu_type"] = 0;

 	// 05_cutset: global muons
 	vcuts("muon", "05_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "05_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "05_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "05_cutset", "min_mu_pt", 20);
  	vcuts("muon", "05_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "05_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["muon"]["05_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["05_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("muon", "05_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("muon", "05_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("muon", "05_cutset", "min_e_pt", 30);	
//        vcuts("muon", "05_cutset", "e_electronID", 1.0);
//        vcuts("muon", "05_cutset", "min_e_relIso", 0.9);
//  	vcuts("muon", "05_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["05_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "05_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["05_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["05_cutset"]["max_no_jets"] = 7;
//  	cut_defs["muon"]["05_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["05_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["05_cutset"]["mu_type"] = 0;

 	// 06_cutset: global muons
 	vcuts("muon", "06_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "06_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "06_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "06_cutset", "min_mu_pt", 20);
  	vcuts("muon", "06_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "06_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["06_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["06_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("muon", "06_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("muon", "06_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("muon", "06_cutset", "min_e_pt", 30);	
//        vcuts("muon", "06_cutset", "e_electronID", 1.0);
//        vcuts("muon", "06_cutset", "min_e_relIso", 0.9);
//  	vcuts("muon", "06_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["06_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "06_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["06_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["06_cutset"]["max_no_jets"] = 7;
//  	cut_defs["muon"]["06_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["06_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["06_cutset"]["mu_type"] = 0;

 	// 07_cutset: global muons
 	vcuts("muon", "07_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "07_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "07_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "07_cutset", "min_mu_pt", 20);
  	vcuts("muon", "07_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "07_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["07_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["07_cutset"]["min_nisolated_mu"] = 1;
  	cut_defs["muon"]["07_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "07_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "07_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "07_cutset", "min_loose_mu_relIso", 0.8);
// 	vcuts("muon", "07_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("muon", "07_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("muon", "07_cutset", "min_e_pt", 30);	
//        vcuts("muon", "07_cutset", "e_electronID", 1.0);
//        vcuts("muon", "07_cutset", "min_e_relIso", 0.9);
//  	vcuts("muon", "07_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["07_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "07_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["07_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["07_cutset"]["max_no_jets"] = 7;
//  	cut_defs["muon"]["07_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["07_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["07_cutset"]["mu_type"] = 0;

 	// 08_cutset: global muons
 	vcuts("muon", "08_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "08_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "08_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "08_cutset", "min_mu_pt", 20);
  	vcuts("muon", "08_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "08_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["08_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["08_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "08_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "08_cutset", "max_mu_ecal_veto_cone", 4);
  	cut_defs["muon"]["08_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "08_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "08_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "08_cutset", "min_loose_mu_relIso", 0.8);
//  	vcuts("muon", "08_cutset", "min_e_pt", 30);	
//        vcuts("muon", "08_cutset", "e_electronID", 1.0);
//        vcuts("muon", "08_cutset", "min_e_relIso", 0.9);
//  	vcuts("muon", "08_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["08_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "08_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["08_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["08_cutset"]["max_no_jets"] = 7;
//  	cut_defs["muon"]["08_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["muon"]["08_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["08_cutset"]["mu_type"] = 0;

 	// 09_cutset: global muons
 	vcuts("muon", "09_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "09_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "09_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "09_cutset", "min_mu_pt", 20);
  	vcuts("muon", "09_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "09_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["09_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["09_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "09_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "09_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("muon", "09_cutset", "min_e_pt", 20);	
        vcuts("muon", "09_cutset", "e_electronID", 1.0);
        vcuts("muon", "09_cutset", "min_e_relIso", 0.9);
  	vcuts("muon", "09_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["09_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "09_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["09_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["09_cutset"]["max_no_jets"] = 7;
  	cut_defs["muon"]["09_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["muon"]["09_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["09_cutset"]["mu_type"] = 0;
  	cut_defs["muon"]["09_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "09_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "09_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "09_cutset", "min_loose_mu_relIso", 0.8);

 	// 10_cutset: global muons
 	vcuts("muon", "10_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "10_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "10_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "10_cutset", "min_mu_pt", 20);
  	vcuts("muon", "10_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "10_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["10_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["10_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "10_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "10_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("muon", "10_cutset", "min_e_pt", 20);	
        vcuts("muon", "10_cutset", "e_electronID", 1.0);
        vcuts("muon", "10_cutset", "min_e_relIso", 0.9);
  	vcuts("muon", "10_cutset", "max_e_eta", 2.4);
//  	cut_defs["muon"]["10_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("muon", "10_cutset", "min_jet_pt",30.0);	
//  	cut_defs["muon"]["10_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["10_cutset"]["max_no_jets"] = 7;
  	cut_defs["muon"]["10_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["muon"]["10_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["10_cutset"]["mu_type"] = 0;
  	cut_defs["muon"]["10_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "10_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "10_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "10_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["muon"]["10_cutset"]["max_nloose_e"] = 1;
  	vcuts("muon", "10_cutset", "min_loose_e_pt", 15);	
  	vcuts("muon", "10_cutset", "max_loose_e_eta", 2.5);
  	vcuts("muon", "10_cutset", "min_loose_e_relIso", 0.8);

 	// 11_cutset: global muons
 	vcuts("muon", "11_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "11_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "11_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "11_cutset", "min_mu_pt", 20);
  	vcuts("muon", "11_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "11_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["11_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["11_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "11_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "11_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("muon", "11_cutset", "min_e_pt", 20);	
        vcuts("muon", "11_cutset", "e_electronID", 1.0);
        vcuts("muon", "11_cutset", "min_e_relIso", 0.9);
  	vcuts("muon", "11_cutset", "max_e_eta", 2.4);
  	cut_defs["muon"]["11_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("muon", "11_cutset", "min_jet_pt",30.0);	
  	cut_defs["muon"]["11_cutset"]["min_no_jets"] = 1;
  	cut_defs["muon"]["11_cutset"]["max_no_jets"] = 1;
  	cut_defs["muon"]["11_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["muon"]["11_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["11_cutset"]["mu_type"] = 0;
  	cut_defs["muon"]["11_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "11_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "11_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "11_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["muon"]["11_cutset"]["max_nloose_e"] = 1;
  	vcuts("muon", "11_cutset", "min_loose_e_pt", 15);	
  	vcuts("muon", "11_cutset", "max_loose_e_eta", 2.5);
  	vcuts("muon", "11_cutset", "min_loose_e_relIso", 0.8);

 	// 12_cutset: global muons
 	vcuts("muon", "12_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "12_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "12_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "12_cutset", "min_mu_pt", 20);
  	vcuts("muon", "12_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "12_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["12_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["12_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "12_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "12_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("muon", "12_cutset", "min_e_pt", 20);	
        vcuts("muon", "12_cutset", "e_electronID", 1.0);
        vcuts("muon", "12_cutset", "min_e_relIso", 0.9);
  	vcuts("muon", "12_cutset", "max_e_eta", 2.4);
  	cut_defs["muon"]["12_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("muon", "12_cutset", "min_jet_pt",30.0);	
  	cut_defs["muon"]["12_cutset"]["min_no_jets"] = 2;
  	cut_defs["muon"]["12_cutset"]["max_no_jets"] = 2;
  	cut_defs["muon"]["12_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["muon"]["12_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["12_cutset"]["mu_type"] = 0;
  	cut_defs["muon"]["12_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "12_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "12_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "12_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["muon"]["12_cutset"]["max_nloose_e"] = 1;
  	vcuts("muon", "12_cutset", "min_loose_e_pt", 15);	
  	vcuts("muon", "12_cutset", "max_loose_e_eta", 2.5);
  	vcuts("muon", "12_cutset", "min_loose_e_relIso", 0.8);

 	// 13_cutset: global muons
 	vcuts("muon", "13_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "13_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "13_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "13_cutset", "min_mu_pt", 20);
  	vcuts("muon", "13_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "13_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["13_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["13_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "13_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "13_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("muon", "13_cutset", "min_e_pt", 20);	
        vcuts("muon", "13_cutset", "e_electronID", 1.0);
        vcuts("muon", "13_cutset", "min_e_relIso", 0.9);
  	vcuts("muon", "13_cutset", "max_e_eta", 2.4);
  	cut_defs["muon"]["13_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("muon", "13_cutset", "min_jet_pt",30.0);	
  	cut_defs["muon"]["13_cutset"]["min_no_jets"] = 3;
  	cut_defs["muon"]["13_cutset"]["max_no_jets"] = 3;
  	cut_defs["muon"]["13_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["muon"]["13_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["13_cutset"]["mu_type"] = 0;
  	cut_defs["muon"]["13_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "13_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "13_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "13_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["muon"]["13_cutset"]["max_nloose_e"] = 1;
  	vcuts("muon", "13_cutset", "min_loose_e_pt", 15);	
  	vcuts("muon", "13_cutset", "max_loose_e_eta", 2.5);
  	vcuts("muon", "13_cutset", "min_loose_e_relIso", 0.8);

 	// 14_cutset: global muons
 	vcuts("muon", "14_cutset", "min_mu_nHits", 11);
 	vcuts("muon", "14_cutset", "max_mu_chi2", 10);
  	vcuts("muon", "14_cutset", "max_mu_d0sig", 3);
  	vcuts("muon", "14_cutset", "min_mu_pt", 20);
  	vcuts("muon", "14_cutset", "max_mu_eta", 2.1);
  	vcuts("muon", "14_cutset", "min_mu_relIso", 0.95);
  	cut_defs["muon"]["14_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["muon"]["14_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("muon", "14_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("muon", "14_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("muon", "14_cutset", "min_e_pt", 20);	
        vcuts("muon", "14_cutset", "e_electronID", 1.0);
        vcuts("muon", "14_cutset", "min_e_relIso", 0.9);
  	vcuts("muon", "14_cutset", "max_e_eta", 2.4);
  	cut_defs["muon"]["14_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("muon", "14_cutset", "min_jet_pt",30.0);	
  	cut_defs["muon"]["14_cutset"]["min_no_jets"] = 4;
//  	cut_defs["muon"]["14_cutset"]["max_no_jets"] = 7;
  	cut_defs["muon"]["14_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["muon"]["14_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["muon"]["14_cutset"]["mu_type"] = 0;
  	cut_defs["muon"]["14_cutset"]["max_nloose_mu"] = 1;
  	vcuts("muon", "14_cutset", "min_loose_mu_pt", 10);
  	vcuts("muon", "14_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("muon", "14_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["muon"]["14_cutset"]["max_nloose_e"] = 1;
  	vcuts("muon", "14_cutset", "min_loose_e_pt", 15);	
  	vcuts("muon", "14_cutset", "max_loose_e_eta", 2.5);
  	vcuts("muon", "14_cutset", "min_loose_e_relIso", 0.8);

	/*
	 *	MUON BACKGROUND CUTS
	 */

 	// 01_cutset: HLT
// 	vcuts("mu_background", "01_cutset", "min_mu_nHits", 11);
// 	vcuts("mu_background", "01_cutset", "max_mu_chi2", 10);
//  	vcuts("mu_background", "01_cutset", "max_mu_d0sig", 3);
//  	vcuts("mu_background", "01_cutset", "min_mu_pt", 20);
//  	vcuts("mu_background", "01_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "01_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["mu_background"]["01_cutset"]["max_nisolated_mu"] = 1;
//  	cut_defs["mu_background"]["01_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("mu_background", "01_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("mu_background", "01_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("mu_background", "01_cutset", "min_e_pt", 30);	
//        vcuts("mu_background", "01_cutset", "e_electronID", 1.0);
//        vcuts("mu_background", "01_cutset", "min_e_relIso", 0.9);
//  	vcuts("mu_background", "01_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["01_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "01_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["01_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["01_cutset"]["max_no_jets"] = 7;
//  	cut_defs["mu_background"]["01_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["01_cutset"]["min_nisolated_e"] = -1;
 	vcuts("mu_background", "01_cutset", "trigger", 82);	// added as default
//  	cut_defs["mu_background"]["01_cutset"]["mu_type"] = 0;

 	// 02_cutset: global mu_backgrounds
// 	vcuts("mu_background", "02_cutset", "min_mu_nHits", 11);
// 	vcuts("mu_background", "02_cutset", "max_mu_chi2", 10);
//  	vcuts("mu_background", "02_cutset", "max_mu_d0sig", 3);
//  	vcuts("mu_background", "02_cutset", "min_mu_pt", 20);
//  	vcuts("mu_background", "02_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "02_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["mu_background"]["02_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["02_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("mu_background", "02_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("mu_background", "02_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("mu_background", "02_cutset", "min_e_pt", 30);	
//        vcuts("mu_background", "02_cutset", "e_electronID", 1.0);
//        vcuts("mu_background", "02_cutset", "min_e_relIso", 0.9);
//  	vcuts("mu_background", "02_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["02_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "02_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["02_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["02_cutset"]["max_no_jets"] = 7;
//  	cut_defs["mu_background"]["02_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["02_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["02_cutset"]["mu_type"] = 0;

 	// 03_cutset: global mu_backgrounds
// 	vcuts("mu_background", "03_cutset", "min_mu_nHits", 11);
// 	vcuts("mu_background", "03_cutset", "max_mu_chi2", 10);
//  	vcuts("mu_background", "03_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "03_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "03_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "03_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["mu_background"]["03_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["03_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("mu_background", "03_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("mu_background", "03_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("mu_background", "03_cutset", "min_e_pt", 30);	
//        vcuts("mu_background", "03_cutset", "e_electronID", 1.0);
//        vcuts("mu_background", "03_cutset", "min_e_relIso", 0.9);
//  	vcuts("mu_background", "03_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["03_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "03_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["03_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["03_cutset"]["max_no_jets"] = 7;
//  	cut_defs["mu_background"]["03_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["03_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["03_cutset"]["mu_type"] = 0;

 	// 04_cutset: global mu_backgrounds
 	vcuts("mu_background", "04_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "04_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "04_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "04_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "04_cutset", "max_mu_eta", 2.1);
//  	vcuts("mu_background", "04_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["mu_background"]["04_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["04_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("mu_background", "04_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("mu_background", "04_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("mu_background", "04_cutset", "min_e_pt", 30);	
//        vcuts("mu_background", "04_cutset", "e_electronID", 1.0);
//        vcuts("mu_background", "04_cutset", "min_e_relIso", 0.9);
//  	vcuts("mu_background", "04_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["04_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "04_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["04_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["04_cutset"]["max_no_jets"] = 7;
//  	cut_defs["mu_background"]["04_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["04_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["04_cutset"]["mu_type"] = 0;

 	// 05_cutset: global mu_backgrounds
 	vcuts("mu_background", "05_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "05_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "05_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "05_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "05_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "05_cutset", "min_mu_relIso", 0.95);
//  	cut_defs["mu_background"]["05_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["05_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("mu_background", "05_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("mu_background", "05_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("mu_background", "05_cutset", "min_e_pt", 30);	
//        vcuts("mu_background", "05_cutset", "e_electronID", 1.0);
//        vcuts("mu_background", "05_cutset", "min_e_relIso", 0.9);
//  	vcuts("mu_background", "05_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["05_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "05_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["05_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["05_cutset"]["max_no_jets"] = 7;
//  	cut_defs["mu_background"]["05_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["05_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["05_cutset"]["mu_type"] = 0;

 	// 06_cutset: global mu_backgrounds
 	vcuts("mu_background", "06_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "06_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "06_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "06_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "06_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "06_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["06_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["06_cutset"]["min_nisolated_mu"] = 1;
// 	vcuts("mu_background", "06_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("mu_background", "06_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("mu_background", "06_cutset", "min_e_pt", 30);	
//        vcuts("mu_background", "06_cutset", "e_electronID", 1.0);
//        vcuts("mu_background", "06_cutset", "min_e_relIso", 0.9);
//  	vcuts("mu_background", "06_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["06_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "06_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["06_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["06_cutset"]["max_no_jets"] = 7;
//  	cut_defs["mu_background"]["06_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["06_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["06_cutset"]["mu_type"] = 0;

 	// 07_cutset: global mu_backgrounds
 	vcuts("mu_background", "07_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "07_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "07_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "07_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "07_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "07_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["07_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["07_cutset"]["min_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["07_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "07_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "07_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "07_cutset", "min_loose_mu_relIso", 0.8);
// 	vcuts("mu_background", "07_cutset", "max_mu_hcal_veto_cone", 6);
// 	vcuts("mu_background", "07_cutset", "max_mu_ecal_veto_cone", 4);
//  	vcuts("mu_background", "07_cutset", "min_e_pt", 30);	
//        vcuts("mu_background", "07_cutset", "e_electronID", 1.0);
//        vcuts("mu_background", "07_cutset", "min_e_relIso", 0.9);
//  	vcuts("mu_background", "07_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["07_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "07_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["07_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["07_cutset"]["max_no_jets"] = 7;
//  	cut_defs["mu_background"]["07_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["07_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["07_cutset"]["mu_type"] = 0;

 	// 08_cutset: global mu_backgrounds
 	vcuts("mu_background", "08_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "08_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "08_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "08_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "08_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "08_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["08_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["08_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "08_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "08_cutset", "max_mu_ecal_veto_cone", 4);
  	cut_defs["mu_background"]["08_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "08_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "08_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "08_cutset", "min_loose_mu_relIso", 0.8);
//  	vcuts("mu_background", "08_cutset", "min_e_pt", 30);	
//        vcuts("mu_background", "08_cutset", "e_electronID", 1.0);
//        vcuts("mu_background", "08_cutset", "min_e_relIso", 0.9);
//  	vcuts("mu_background", "08_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["08_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "08_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["08_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["08_cutset"]["max_no_jets"] = 7;
//  	cut_defs["mu_background"]["08_cutset"]["max_nisolated_e"] = 0;
//  	cut_defs["mu_background"]["08_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["08_cutset"]["mu_type"] = 0;

 	// 09_cutset: global mu_backgrounds
 	vcuts("mu_background", "09_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "09_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "09_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "09_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "09_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "09_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["09_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["09_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "09_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "09_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("mu_background", "09_cutset", "min_e_pt", 20);	
        vcuts("mu_background", "09_cutset", "e_electronID", 1.0);
        vcuts("mu_background", "09_cutset", "min_e_relIso", 0.9);
  	vcuts("mu_background", "09_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["09_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "09_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["09_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["09_cutset"]["max_no_jets"] = 7;
  	cut_defs["mu_background"]["09_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["mu_background"]["09_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["09_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["09_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "09_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "09_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "09_cutset", "min_loose_mu_relIso", 0.8);

 	// 10_cutset: global mu_backgrounds
 	vcuts("mu_background", "10_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "10_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "10_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "10_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "10_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "10_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["10_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["10_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "10_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "10_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("mu_background", "10_cutset", "min_e_pt", 20);	
        vcuts("mu_background", "10_cutset", "e_electronID", 1.0);
        vcuts("mu_background", "10_cutset", "min_e_relIso", 0.9);
  	vcuts("mu_background", "10_cutset", "max_e_eta", 2.4);
//  	cut_defs["mu_background"]["10_cutset"]["max_jet_eta"] = 2.4; 
//  	vcuts("mu_background", "10_cutset", "min_jet_pt",30.0);	
//  	cut_defs["mu_background"]["10_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["10_cutset"]["max_no_jets"] = 7;
  	cut_defs["mu_background"]["10_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["mu_background"]["10_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["10_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["10_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "10_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "10_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "10_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["mu_background"]["10_cutset"]["max_nloose_e"] = 1;
  	vcuts("mu_background", "10_cutset", "min_loose_e_pt", 15);	
  	vcuts("mu_background", "10_cutset", "max_loose_e_eta", 2.5);
  	vcuts("mu_background", "10_cutset", "min_loose_e_relIso", 0.8);

 	// 11_cutset: global mu_backgrounds
 	vcuts("mu_background", "11_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "11_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "11_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "11_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "11_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "11_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["11_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["11_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "11_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "11_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("mu_background", "11_cutset", "min_e_pt", 20);	
        vcuts("mu_background", "11_cutset", "e_electronID", 1.0);
        vcuts("mu_background", "11_cutset", "min_e_relIso", 0.9);
  	vcuts("mu_background", "11_cutset", "max_e_eta", 2.4);
  	cut_defs["mu_background"]["11_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("mu_background", "11_cutset", "min_jet_pt",30.0);	
  	cut_defs["mu_background"]["11_cutset"]["min_no_jets"] = 1;
  	cut_defs["mu_background"]["11_cutset"]["max_no_jets"] = 1;
  	cut_defs["mu_background"]["11_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["mu_background"]["11_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["11_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["11_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "11_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "11_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "11_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["mu_background"]["11_cutset"]["max_nloose_e"] = 1;
  	vcuts("mu_background", "11_cutset", "min_loose_e_pt", 15);	
  	vcuts("mu_background", "11_cutset", "max_loose_e_eta", 2.5);
  	vcuts("mu_background", "11_cutset", "min_loose_e_relIso", 0.8);

 	// 12_cutset: global mu_backgrounds
 	vcuts("mu_background", "12_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "12_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "12_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "12_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "12_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "12_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["12_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["12_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "12_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "12_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("mu_background", "12_cutset", "min_e_pt", 20);	
        vcuts("mu_background", "12_cutset", "e_electronID", 1.0);
        vcuts("mu_background", "12_cutset", "min_e_relIso", 0.9);
  	vcuts("mu_background", "12_cutset", "max_e_eta", 2.4);
  	cut_defs["mu_background"]["12_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("mu_background", "12_cutset", "min_jet_pt",30.0);	
  	cut_defs["mu_background"]["12_cutset"]["min_no_jets"] = 2;
  	cut_defs["mu_background"]["12_cutset"]["max_no_jets"] = 2;
  	cut_defs["mu_background"]["12_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["mu_background"]["12_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["12_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["12_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "12_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "12_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "12_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["mu_background"]["12_cutset"]["max_nloose_e"] = 1;
  	vcuts("mu_background", "12_cutset", "min_loose_e_pt", 15);	
  	vcuts("mu_background", "12_cutset", "max_loose_e_eta", 2.5);
  	vcuts("mu_background", "12_cutset", "min_loose_e_relIso", 0.8);

 	// 13_cutset: global mu_backgrounds
 	vcuts("mu_background", "13_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "13_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "13_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "13_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "13_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "13_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["13_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["13_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "13_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "13_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("mu_background", "13_cutset", "min_e_pt", 20);	
        vcuts("mu_background", "13_cutset", "e_electronID", 1.0);
        vcuts("mu_background", "13_cutset", "min_e_relIso", 0.9);
  	vcuts("mu_background", "13_cutset", "max_e_eta", 2.4);
  	cut_defs["mu_background"]["13_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("mu_background", "13_cutset", "min_jet_pt",30.0);	
  	cut_defs["mu_background"]["13_cutset"]["min_no_jets"] = 3;
  	cut_defs["mu_background"]["13_cutset"]["max_no_jets"] = 3;
  	cut_defs["mu_background"]["13_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["mu_background"]["13_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["13_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["13_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "13_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "13_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "13_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["mu_background"]["13_cutset"]["max_nloose_e"] = 1;
  	vcuts("mu_background", "13_cutset", "min_loose_e_pt", 15);	
  	vcuts("mu_background", "13_cutset", "max_loose_e_eta", 2.5);
  	vcuts("mu_background", "13_cutset", "min_loose_e_relIso", 0.8);

 	// 14_cutset: global mu_backgrounds
 	vcuts("mu_background", "14_cutset", "min_mu_nHits", 11);
 	vcuts("mu_background", "14_cutset", "max_mu_chi2", 10);
  	vcuts("mu_background", "14_cutset", "max_mu_d0sig", 3);
  	vcuts("mu_background", "14_cutset", "min_mu_pt", 20);
  	vcuts("mu_background", "14_cutset", "max_mu_eta", 2.1);
  	vcuts("mu_background", "14_cutset", "min_mu_relIso", 0.95);
  	cut_defs["mu_background"]["14_cutset"]["max_nisolated_mu"] = 1;
  	cut_defs["mu_background"]["14_cutset"]["min_nisolated_mu"] = 1;
 	vcuts("mu_background", "14_cutset", "max_mu_hcal_veto_cone", 6);
 	vcuts("mu_background", "14_cutset", "max_mu_ecal_veto_cone", 4);
  	vcuts("mu_background", "14_cutset", "min_e_pt", 20);	
        vcuts("mu_background", "14_cutset", "e_electronID", 1.0);
        vcuts("mu_background", "14_cutset", "min_e_relIso", 0.9);
  	vcuts("mu_background", "14_cutset", "max_e_eta", 2.4);
  	cut_defs["mu_background"]["14_cutset"]["max_jet_eta"] = 2.4; 
  	vcuts("mu_background", "14_cutset", "min_jet_pt",30.0);	
  	cut_defs["mu_background"]["14_cutset"]["min_no_jets"] = 4;
//  	cut_defs["mu_background"]["14_cutset"]["max_no_jets"] = 7;
  	cut_defs["mu_background"]["14_cutset"]["max_nisolated_e"] = 0;
  	cut_defs["mu_background"]["14_cutset"]["min_nisolated_e"] = -1;
  	cut_defs["mu_background"]["14_cutset"]["mu_type"] = 0;
  	cut_defs["mu_background"]["14_cutset"]["max_nloose_mu"] = 1;
  	vcuts("mu_background", "14_cutset", "min_loose_mu_pt", 10);
  	vcuts("mu_background", "14_cutset", "max_loose_mu_eta", 2.5);
  	vcuts("mu_background", "14_cutset", "min_loose_mu_relIso", 0.8);
  	cut_defs["mu_background"]["14_cutset"]["max_nloose_e"] = 1;
  	vcuts("mu_background", "14_cutset", "min_loose_e_pt", 15);	
  	vcuts("mu_background", "14_cutset", "max_loose_e_eta", 2.5);
  	vcuts("mu_background", "14_cutset", "min_loose_e_relIso", 0.8);

         /*
          *      ELECTRON CUTS
          */

         /*
          *      ELECTRON BACKGROUND CUTS
          */

        /*
         *      global cuts for all selections defined so far
         */
        synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
                for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
                    set_iter != type_iter->second.end();
                    ++set_iter)
                {
                        if(type_iter->first == "muon" || type_iter->first == "mu_background"){
				vset_if_not_set(type_iter->first, set_iter->first, "trigger", 82);
                        }else if(type_iter->first == "electron" || type_iter->first == "e_background"){
                        }
                }
        }

        /*
         *      cuts for which the global selection cuts are not applied
         */
	vcuts("muon", "without_cuts", "min_jet_pt", 0.0);
	vcuts("mu_background", "without_cuts", "min_jet_pt", 0.0);
}
