#!/usr/bin/perl

# use external config!
#$cmssw_cfg_mask = 'PYTHIA6_TprimeTprimebar_10TeV_RAW2DIGI_RECO.py';
#$crab_cfg_mask = 'crab.cfg';
#$input_dir = '/pnfs/iihe/cms/store/user/bklein/tprime_250/';
#$root_outfile = 'PYTHIA6_TTbar_10TeV_cff_RAW2DIGI_RECO.root';
#$max_files_per_job = 5;
#$storage_prefix = 'dcap:/';

$this_dir = `pwd`;
chomp $this_dir;
$config = 'local_cfg_gen.cfg';
die "can't find config $this_dir/$config" unless(-e $config);
do "$this_dir/$config";

opendir(INDIR, $input_dir);
while(defined($file = readdir(INDIR))){
	next unless($file =~ /\.root/);	
	push(@rootfiles, $file);
}
closedir(INDIR);

$config_counter = 0;
$in_file_counter = 0;

my @files_for_job = ();
foreach my $file(@rootfiles){
	if($in_file_counter >= $max_files_per_job){
		print "creating config...\n";
		create_new_cmssw_cfg(@files_for_job, $config_counter);
		create_new_crab_cfg($config_counter);
		@files_for_job = ();
		$in_file_counter = 0;
		$config_counter++;
	}else{
		push(@files_for_job, $file);
		$in_file_counter++;
	}
}

if($in_file_counter > 0){
	create_new_cmssw_cfg(@files_for_job, $config_counter);
	create_new_crab_cfg($config_counter);
}

sub create_new_crab_cfg
{
	my $config_nr = pop;

	my $cmssw_cfg_string = $config_nr."_".$cmssw_cfg_mask;
	my $outfile_string = $config_nr."_".$root_outfile;

	open(CRAB_CFG, "<$crab_cfg_mask") or die "could not open cmssw config\n";
	my $new_cfg_name = $config_nr."_".$crab_cfg_mask;
	open(CONFIG, ">$new_cfg_name");
	while(defined(my $line = <CRAB_CFG>)){
		$line =~ s/__CMSSW_CONFIG__/$cmssw_cfg_string/;
		$line =~ s/__OUTFILE__/$outfile_string/;
		print CONFIG $line;
	}
	close(CONFIG);
	close(CRAB_CFG);
}

sub create_new_cmssw_cfg
{
	my $config_nr = pop;
	my @files_for_job = @_;

	# prepare files for job string
	my $files_string = '';
	foreach my $file(@files_for_job){
		$files_string .= '\''.$storage_prefix.$input_dir.$file.'\',';
	}
	# remove last comma
	chop $files_string;
	my $outfile_string = '\''.$config_nr."_".$root_outfile.'\'';

	# write out new config based on cmssw_cfg_mask
	open(CMSSW_CFG, "<$cmssw_cfg_mask") or die "could not open cmssw config\n";
	my $new_cfg_name = $config_nr."_".$cmssw_cfg_mask;
	open(CONFIG, ">$new_cfg_name");
	while(defined(my $line = <CMSSW_CFG>)){
#		print "printing line... $line";
		$line =~ s/__FILE_NAMES__/$files_string/;
		$line =~ s/__OUTFILE__/$outfile_string/;
		print CONFIG $line;
	}
	close(CONFIG);
	close(CMSSW_CFG);
}
