# Auto generated configuration file
# using: 
# Revision: 1.77.2.1 
# Source: /cvs_server/repositories/CMSSW/CMSSW/Configuration/PyReleaseValidation/python/ConfigBuilder.py,v 
# with command line options: Reconstruction_2_1_8_IDEAL_V9.py -s RAW2DIGI,RECO --eventcontent RECOSIM --datatier GEN-SIM-RECO --conditions FrontierConditions_GlobalTag,IDEAL_V9::All --no_exec -n 10 --filein gensimraw.root
import FWCore.ParameterSet.Config as cms

process = cms.Process('RECO')

# import of standard configurations
process.load('Configuration/StandardSequences/Services_cff')
process.load('FWCore/MessageService/MessageLogger_cfi')
process.load('Configuration/StandardSequences/MixingNoPileUp_cff')
process.load('Configuration/StandardSequences/GeometryPilot2_cff')
process.load('Configuration/StandardSequences/MagneticField_38T_cff')
process.load('Configuration/StandardSequences/RawToDigi_cff')
process.load('Configuration/StandardSequences/Reconstruction_cff')
process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
process.load('JetMETCorrections/Configuration/L2L3Corrections_iCSA08_S156_cff')
process.load('Configuration/EventContent/EventContent_cff')

process.prefer("L2L3JetCorrectorIcone5")

process.configurationMetadata = cms.untracked.PSet(
    version = cms.untracked.string('$Revision: 1.77.2.1 $'),
    annotation = cms.untracked.string('Reconstruction_2_1_8_IDEAL_V9.py nevts:10'),
    name = cms.untracked.string('PyReleaseValidation')
)
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1)
)
process.options = cms.untracked.PSet(
    Rethrow = cms.untracked.vstring('ProductNotFound')
)
# Input source
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring( __FILE_NAMES__ )
)

process.JETCOREventContent = cms.PSet(
        outputCommands = cms.untracked.vstring('keep  *_L2L3CorJet*_*_*')
	)

process.RECOSIMEventContent.outputCommands.extend(process.JETCOREventContent.outputCommands)

# Output definition
process.output = cms.OutputModule("PoolOutputModule",
    outputCommands = process.RECOSIMEventContent.outputCommands,
    fileName = cms.untracked.string( __OUTFILE__ ),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('GEN-SIM-RECO'),
        filterName = cms.untracked.string('')
    )
)

# Additional output definition

# Other statements
process.GlobalTag.globaltag = 'IDEAL_V9::All'

# Path and EndPath definitions
process.raw2digi_step = cms.Path(process.RawToDigi)
process.reconstruction_step = cms.Path(process.reconstruction)
process.L2L3Icone5_step = cms.Path(process.L2L3CorJetIcone5)
process.L2L3PFIcone5_step = cms.Path(process.L2L3CorJetPFIcone5)
process.L2L3Scone5_step = cms.Path(process.L2L3CorJetScone5)
process.L2L3Scone7_step = cms.Path(process.L2L3CorJetScone7)
process.L2L3kt4_step = cms.Path(process.L2L3CorJetKt4)
process.L2L3kt6_step = cms.Path(process.L2L3CorJetKt6)
process.out_step = cms.EndPath(process.output)

# Schedule definition
process.schedule = cms.Schedule(process.raw2digi_step,process.reconstruction_step, process.L2L3Icone5_step, process.L2L3PFIcone5_step, process.L2L3Scone5_step, process.L2L3Scone7_step, process.L2L3kt4_step, process.L2L3kt6_step, process.out_step)
#process.schedule = cms.Schedule(process.raw2digi_step,process.reconstruction_step, process.out_step)
