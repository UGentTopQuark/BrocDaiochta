import FWCore.ParameterSet.Config as cms

process = cms.Process("EventDisplaySelector")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
      'file:///user/walsh/CMSSW_3_5_6/src/temp/MC_7TeV_gensimreco.root'
    )
)

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.selectEvents = cms.EDFilter('EventSelector',
					# format: run, lumi block, event number
					# consider every event: in run/block/etc:
					# 		-> specify: -1
	#########################################
	# WARNING: 
	# make sure you remove the last comma from the last line of entries
	# in the vector below, right before the closing bracket
	#########################################
      EventID      = cms.vdouble(
      				135521, 210, 23681558,
      				135175, 568, 57384102,
				135528, 582, 109509145,
				135528, 1218, 225247405,
				135575, 649, 108714258,
				135735, 191, 27849377
				      )	# every event in run,
				      			# ignoring lumi block number and event
				      			# number
)

process.out = cms.OutputModule("PoolOutputModule",
    verbose = cms.untracked.bool(True),
    dropMetaDataForDroppedData = cms.untracked.bool(True),                       
    fileName = cms.untracked.string( 'my_outfile.root' )
)

process.p1 = cms.Path(process.selectEvents * process.out)

process.schedule = cms.Schedule(process.p1)

import FWCore.ParameterSet.Config as cms
from IOMC.RandomEngine.RandomServiceHelper import RandomNumberServiceHelper

def customise_for_gc(process):
	try:
		maxevents = __MAX_EVENTS__
		process.maxEvents = cms.untracked.PSet(
			input = cms.untracked.int32(maxevents)
		)
	except:
		pass

	# Dataset related setup
	try:
		tmp = __SKIP_EVENTS__
		process.source = cms.Source("PoolSource",
			skipEvents = cms.untracked.uint32(__SKIP_EVENTS__),
			fileNames = cms.untracked.vstring(__FILE_NAMES__)
		)
		try:
			secondary = __FILE_NAMES2__
			process.source.secondaryFileNames = cms.untracked.vstring(secondary)
		except:
			pass
		try:
			lumirange = [__LUMI_RANGE__]
			process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange(lumirange)
		except:
			pass
	except:
		pass

	# Generator related setup
	try:
		if hasattr(process, "generator"):
			process.source.firstLuminosityBlock = cms.untracked.uint32(1+__MY_JOBID__)
	except:
		pass

	if hasattr(process, "RandomNumberGeneratorService"):
		randSvc = RandomNumberServiceHelper(process.RandomNumberGeneratorService)
		randSvc.populate()

	process.AdaptorConfig = cms.Service("AdaptorConfig",
		enable=cms.untracked.bool(True),
		stats = cms.untracked.bool(True),
	)

	return (process)

process = customise_for_gc(process)

# grid-control: https://ekptrac.physik.uni-karlsruhe.de/trac/grid-control
