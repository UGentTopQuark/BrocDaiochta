#include "../interface/EventSelector.h"

//
// constructors and destructor
//
EventSelector::EventSelector(const edm::ParameterSet& iConfig)
{
   	event_ids = new std::vector<double>(iConfig.getParameter<std::vector<double> >("EventID"));
}

EventSelector::~EventSelector()
{
	if(event_ids){
		delete event_ids;
		event_ids = NULL;
	}
}

// ------------ method called on each new Event  ------------
bool
EventSelector::filter(edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	using namespace edm;

	for(unsigned int i = 0; i < event_ids->size()-2; i = i+3){
		double run = (*event_ids)[i];
		double lumi_block = (*event_ids)[i+1];
		double event_number = (*event_ids)[i+2];
		if((iEvent.run() == run || run == -1)  &&
		   (iEvent.luminosityBlock() == lumi_block || lumi_block == -1) &&
		   (iEvent.id().event() == event_number || event_number == -1))
			return true;	// event is written to file
	}

	return false;	// event is filtered out
}

// ------------ method called once each job just before starting event loop  ------------
void 
EventSelector::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
EventSelector::endJob() {
}

//define this as a plug-in
DEFINE_FWK_MODULE(EventSelector);
