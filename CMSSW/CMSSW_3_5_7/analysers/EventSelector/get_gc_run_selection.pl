#! /usr/bin/perl

my $config = "eventselector_cfg.py";

my $runs = "";
open(CONFIG, "<$config");
while(<CONFIG>){
	next unless(/(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*,?/);
	my $run = $1;
	my $lumi_block = $2;
	
	$runs .= "$run:$lumi_block, ";
}
close(CONFIG);

$runs =~ s/, $//g;
print "\n[CMSSW]\n";
print "lumi filter     = $runs\n";
