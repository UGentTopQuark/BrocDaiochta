// -*- C++ -*-
//
// Package:    EventSelector
// Class:      EventSelector
// 
/**\class EventSelector EventSelector.cc analysers/EventSelector/src/EventSelector.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  local user
//         Created:  Fri Apr 30 13:44:37 CEST 2010
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDFilter.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include <vector>
#include <string>

//
// class declaration
//

class EventSelector : public edm::EDFilter {
   public:
      explicit EventSelector(const edm::ParameterSet&);
      ~EventSelector();

   private:
      virtual void beginJob() ;
      virtual bool filter(edm::Event&, const edm::EventSetup&);
      virtual void endJob() ;

      std::vector<double> *event_ids;
      
      // ----------member data ---------------------------
};
