#include "../interface/METCollection.h"

namespace beag{

template <class CMSSWObject>
beag::METCollection<CMSSWObject>::METCollection(TTree *tree, TFile *outfile, std::string ident) : beag::Collection<CMSSWObject, beag::MET>(tree, outfile, ident)
{
	this->tree->Bronch(ident.c_str(),get_type_string(),&this->beag_objects);
}

template <class CMSSWObject>
beag::METCollection<CMSSWObject>::~METCollection()
{
}

template <class CMSSWObject>
const char* beag::METCollection<CMSSWObject>::get_type_string()
{
	return "std::vector<beag::MET>";
}

template <class CMSSWObject>
void beag::METCollection<CMSSWObject>::fill_special_vars(typename edm::View<CMSSWObject>::const_iterator cmssw_met, beag::MET &beag_met)
{
	beag_met.significance = cmssw_met->significance();
}

template class beag::METCollection<pat::MET>;
template class beag::METCollection<reco::CaloMET>;
template class beag::METCollection<reco::PFMET>;
template class beag::METCollection<reco::MET>;
} // end namespace beag
