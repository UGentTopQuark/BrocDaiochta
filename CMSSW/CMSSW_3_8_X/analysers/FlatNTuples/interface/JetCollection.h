#ifndef BEAG_JETCOLLECTION_H
#define BEAG_JETCOLLECTION_H

#include <DataFormats/Common/interface/Ref.h>
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "Collection.h"
#include "BeagObjects/Jet.h"
#include "FWCore/Framework/interface/Event.h"
#include <JetMETCorrections/Objects/interface/JetCorrector.h>
#include "DataFormats/JetReco/interface/GenJet.h"
#include "../interface/BTagAssociator.h"

namespace beag{
	template<class CMSSWObject>
	class JetCollection: public beag::Collection<CMSSWObject, beag::Jet>{
		public:
			JetCollection(TTree *tree, TFile *outfile, std::string ident);
			~JetCollection();

			inline void set_jetIDHandle(edm::Handle<reco::JetIDValueMap > jetIDHandle){ this->jetIDHandle = jetIDHandle; };
			inline void set_genjetHandle(edm::Handle<edm::View<reco::GenJet> > gen_jetHandle){ this->gen_jetHandle = gen_jetHandle; };
			void set_jetCorrector(std::string mJetCorServiceName, const edm::EventSetup& iSetup);
			virtual void fill_collection(typename edm::Handle<edm::View<CMSSWObject> > handle, const edm::Event& iEvent);
			inline void set_is_mc(bool is_mc){this->is_mc = is_mc;};

		private:
			void fill_corrected_jets(typename edm::Handle<edm::View<CMSSWObject> > handle);
			virtual void fill_special_vars(typename edm::View<CMSSWObject>::const_iterator handle, beag::Jet &beag_obj);
			virtual void fill_common_vars(typename edm::View<CMSSWObject>::const_iterator handle, beag::Jet &beag_obj);
			void correct_jet(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, beag::Jet &beag_obj);
			void write_gen_info(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, beag::Jet &beag_obj);
			void fill_jet_common(typename edm::View<CMSSWObject>::const_iterator cmssw_jet, beag::Jet &beag_jet);
			void fill_jet_id(typename edm::View<CMSSWObject>::const_iterator cmssw_jet, beag::Jet &beag_jet);
			void match_to_genjet(typename edm::View<CMSSWObject>::const_iterator cmssw_obj, beag::Jet &beag_obj);

			virtual const char* get_type_string();

			edm::Handle<reco::JetIDValueMap > jetIDHandle;
			edm::Handle<edm::View<reco::GenJet> > gen_jetHandle;
			JetCorrector *jet_corrector;
			beag::BTagAssociator<CMSSWObject> *btag_associator;

			bool is_mc;
	};
}

#endif
