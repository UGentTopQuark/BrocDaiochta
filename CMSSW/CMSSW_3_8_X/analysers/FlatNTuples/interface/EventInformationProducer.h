#ifndef BEAG_EVENTINFORMATIONPRODUCER_H
#define BEAG_EVENTINFORMATIONPRODUCER_H

#include "TFile.h"
#include "TTree.h"
#include <string>
#include <vector>

#include "BeagObjects/EventInformation.h"
#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h" 
#include <DataFormats/Common/interface/Ref.h>
#include "DataFormats/Common/interface/View.h"

#include "WDecayModeAnalyser.h"

namespace beag{
	class EventInformationProducer{
		public:
			EventInformationProducer(TTree *tree, TFile *outfile, std::string ident);
			~EventInformationProducer();

			void fill_event_information(double run, double lumi_block, double event_number, bool trigger_changed, double cms_energy, double event_weight, bool is_real_data, double rhoFastJet, double rhoFastJetIso);
			void fill_pu_info(edm::Handle<std::vector<PileupSummaryInfo> > pu_info);
			void fill_W_decay_info(edm::Handle<reco::GenParticleCollection> part_collection);
		private:

			TTree *tree;
			TFile *outfile;

			std::vector<beag::EventInformation> *event_info;

			WDecayModeAnalyser *W_decay_info;

			static const bool verbose = false;
	};
}

#endif
