#ifndef CONVREJ2012_H
#define CONVREJ2012_H

#include "RecoEgamma/EgammaTools/interface/ConversionTools.h"
#include "DataFormats/EgammaCandidates/interface/ConversionFwd.h"
#include "DataFormats/EgammaCandidates/interface/Conversion.h"

#include "RecoEgamma/EgammaTools/interface/ConversionFinder.h"
#include "MagneticField/Records/interface/IdealMagneticFieldRecord.h"
#include "MagneticField/Engine/interface/MagneticField.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackExtra.h"
#include <DataFormats/Common/interface/Ref.h>
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/EgammaCandidates/interface/GsfElectron.h"
#include <DataFormats/Scalers/interface/DcsStatus.h>
#include <FWCore/Framework/interface/ESHandle.h>

namespace beag{
	class ConvRej2012{
		public:
			void set_handles(edm::Handle<reco::BeamSpot> beamSpotHandle,
                                        edm::Handle<reco::ConversionCollection> hConversions);
			void next_event(const edm::EventSetup& iSetup, bool is_data);
			void calculate(const reco::GsfElectron *electron);

			inline int pass_conv_veto(){ return passconversionveto; };
		private:
		        edm::Handle<reco::BeamSpot> beamSpotHandle;
                        edm::Handle<reco::ConversionCollection> hConversions;
			edm::Handle<reco::GsfElectronCollection> hElectrons;

			int passconversionveto;
	};
}

#endif
