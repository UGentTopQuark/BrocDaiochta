#ifndef L1EXTRAOBJECTPRODUCER_H
#define L1EXTRAOBJECTPRODUCER_H

#include <vector>
#include <string>
#include <map>
#include "BeagObjects/TriggerObject.h"
#include "L1ExtraCuts.h"
#include <iostream>
#include "DataFormats/L1Trigger/interface/L1MuonParticle.h"
#include "DataFormats/L1Trigger/interface/L1MuonParticleFwd.h"
#include "DataFormats/L1GlobalMuonTrigger/interface/L1MuGMTExtendedCand.h"


namespace beag{
	class L1ExtraObjectProducer{
		public:
			L1ExtraObjectProducer();
			~L1ExtraObjectProducer();
	
			void set_trigger_names(std::vector<std::string> *trigger_names);
			void set_trigger_objects(std::vector<beag::TriggerObject> *trigger_objects);
			void fill_l1_objects(edm::Handle<l1extra::L1MuonParticleCollection> &l1muons);
		private:
			L1ExtraCuts* get_l1_extra_cuts(std::string trigger_name);
			bool pass(const l1extra::L1MuonParticle *l1_cand, L1ExtraCuts *cuts);
			std::vector<beag::TriggerObject> *trigger_objects;
			std::map<std::string, L1ExtraCuts*> trigger_cuts;
			std::vector<std::string> *trigger_names;

			static const bool verbose = false;
	};
}

#endif
