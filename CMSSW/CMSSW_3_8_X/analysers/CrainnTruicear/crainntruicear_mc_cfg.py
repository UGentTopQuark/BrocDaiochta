import FWCore.ParameterSet.Config as cms
import FWCore.Utilities.FileUtils as FileUtils

from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAny_cfi import *
from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAlong_cfi import *
from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorOpposite_cfi import *
from RecoMuon.DetLayers.muonDetLayerGeometry_cfi import *

process = cms.Process("Demo")

process.load("Configuration.StandardSequences.Geometry_cff")
process.load("Configuration.StandardSequences.MagneticField_cff")
process.load("Configuration.StandardSequences.Reconstruction_cff")
process.load("Configuration.StandardSequences.Services_cff")

process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
process.GlobalTag.globaltag = 'START39_V9::All'
process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )


#myFileNames = FileUtils.loadListFromFile('filepaths/DYToLL_TuneD6T_WZSkim_20110225.txt')
myFileNames = FileUtils.loadListFromFile('filepaths/DYToMuMu_Z2_39X_WZSkim_20110321.txt')
input_filenames =  cms.untracked.vstring(*myFileNames)

process.source = cms.Source("PoolSource",
     fileNames = input_filenames                       
)

process.produceTrees = cms.EDAnalyzer('CrainnTruicear',
       	muonTag     = cms.untracked.InputTag("muons"),
	jetTag      = cms.untracked.InputTag("ak5CaloJets"),
	metTag      = cms.untracked.InputTag("htMetAK5"),
        primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD','','HLT'),
	HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT'),
                                      
        l1s  = cms.InputTag("hltL1extraParticles"),
        L2Muons_Collection=cms.InputTag("hltL2MuonCandidates"),
        L2Seeds_Collection=cms.InputTag("hltL2MuonSeeds"),
        L3Seeds_Collection=cms.InputTag("hltL3TrajectorySeed"),
        L3TkTracks_Collection=cms.InputTag("hltL3TkTracksFromL2"),
        L3Muons_Collection=cms.InputTag("hltL3MuonCandidates"), 
        BeamSpotTag = cms.InputTag("hltOfflineBeamSpot"),

    l1matcherConfig = cms.PSet(
        #useTrack = cms.string('global'),
        #useState = cms.string('outermost'),
        useTrack = cms.string("tracker"),  # 'none' to use Candidate P4; or 'tracker', 'muon', 'global'
        useState = cms.string("atVertex"), # 'innermost' and 'outermost' require the TrackExtra
        useSimpleGeometry = cms.bool(True),
    ),
        #associationMap                                      
        SeedMapTag = cms.InputTag( "hltL2Muons" ),
      	triggername = cms.string("HLT_Mu9"),
	outfilename = cms.string("tree_DYToMuMu_Z2_mu15_20110408.root")
)

process.p = cms.Path(process.produceTrees)

process.schedule = cms.Schedule(
    process.p
    )
