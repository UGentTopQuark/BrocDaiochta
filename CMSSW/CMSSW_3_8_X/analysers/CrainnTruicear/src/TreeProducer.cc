#include "../interface/TreeProducer.h"

TreeProducer::TreeProducer(TFile *outfile)
{
	directory_name = "lepEffs";
	std::string tree_name = "fitter_tree";

	this->outfile = outfile; 
	outfile->cd();
	outfile->mkdir(directory_name.c_str());
	outfile->Cd(directory_name.c_str());
	tree = new TTree(tree_name.c_str(),tree_name.c_str());

	mu1_l2_track = NULL;
	mu1_l3_track = NULL;
	mu2_l2_track = NULL;
	mu2_l3_track = NULL;

	run =-1;
	lumi=-1;
	event_number=-1;

	reset_variables();
	book_branches();

}

TreeProducer::~TreeProducer()
{
	if(outfile){
		outfile->cd();
		outfile->Cd(directory_name.c_str());
		tree->Write();
	}

	delete mu1_l2_track;
	delete mu1_l3_track;
	delete mu2_l2_track;
	delete mu2_l3_track;
}

void TreeProducer::reset_variables()
{

        pt=0;
        mass=0;
        eta=0;
        phi=0;
        abseta=0;
        mindR = 0;
        njets=0;
        reliso=0;
        ecaliso=0;
        hcaliso=0;
        trackiso=0;
        combcaloiso=0;
        npvertices=0;

	chi2=0;
	d0=0;
	nmuon_hits=0;
	npixel_layers=0;
	nmatched_segments=0;

	l2trk_nhits=-1;
	l2trk_eta=-99;
	l2trk_pt=-1;
	  
	l3trk_nhits=-1;
	l3trk_eta=-99;
	l3trk_pt=-1;
	l3trk_dxy=-99;

        passing_idx = 0;
        passing_l1 = 0;
        passing_hltwrtl1 = 0;

        passing_prop = 0;
        passing_l1p = 0;
        passing_l1f = 0;
        passing_l2s = 0;
        passing_l2m = 0;
        passing_l2mf = 0;
        passing_l3s = 0;
        passing_l3t = 0;
        passing_l3tf = 0;
        passing_l3m = 0;
        passing_l3mf = 0;

        passing_l1pwrtprop = 0;
        passing_l1fwrtl1p = 0;
        passing_l2swrtl1f = 0;
        passing_l2mwrtl2s = 0;
        passing_l2mfwrtl2m = 0;
        passing_l3swrtl2mf = 0;
        passing_l3twrtl3s = 0;
        passing_l3tfwrtl3t = 0;
        passing_l3mwrtl3tf = 0;
        passing_l3mfwrtl3m = 0;

}

void TreeProducer::book_branches()
{
	tree->Branch("pt",&pt, "pt/F");
	tree->Branch("eta",&eta, "eta/F");
	tree->Branch("phi",&phi, "phi/F");
	tree->Branch("mass",&mass, "mass/F");
	tree->Branch("njets",&njets, "njets/F");
	tree->Branch("abseta",&abseta, "abseta/F");
	tree->Branch("reliso",&reliso, "reliso/F");
	tree->Branch("ecaliso",&ecaliso, "ecaliso/F");
	tree->Branch("hcaliso",&hcaliso, "hcaliso/F");
	tree->Branch("trackiso",&trackiso, "trackiso/F");
	tree->Branch("combcaloiso",&combcaloiso, "combcaloiso/F");
	tree->Branch("mindR",&mindR, "mindR/F");
	tree->Branch("npvertices",&npvertices, "npvertices/F");
	tree->Branch("nhits",&nhits, "nhits/F");
	tree->Branch("chi2",&chi2, "chi2/F");
	tree->Branch("d0",&d0, "d0/F");
	tree->Branch("nmuon_hits",&nmuon_hits, "nmuon_hits/F");
	tree->Branch("npixel_layers",&npixel_layers, "npixel_layers/F");
	tree->Branch("nmatched_segments",&nmatched_segments, "nmatched_segments/F");

	tree->Branch("passing_idx",&passing_idx,"passing_idx/I");
	tree->Branch("passing_l1",&passing_l1,"passing_l1/I");
	tree->Branch("passing_hltwrtl1",&passing_hltwrtl1,"passing_hltwrtl1/I");

	tree->Branch("l2trk_nhits",&l2trk_nhits, "l2trk_nhits/F");
	tree->Branch("l2trk_eta",&l2trk_eta, "l2trk_eta/F");
	tree->Branch("l2trk_pt",&l2trk_pt, "l2trk_pt/F");

	tree->Branch("l3trk_nhits",&l3trk_nhits, "l3trk_nhits/F");
	tree->Branch("l3trk_eta",&l3trk_eta, "l3trk_eta/F");
	tree->Branch("l3trk_pt",&l3trk_pt, "l3trk_pt/F");
	tree->Branch("l3trk_dxy",&l3trk_dxy, "l3trk_dxy/F");

	tree->Branch("passing_prop",&passing_prop,"passing_prop/I");
	tree->Branch("passing_l1p",&passing_l1p,"passing_l1p/I");
	tree->Branch("passing_l1f",&passing_l1f,"passing_l1f/I");
	tree->Branch("passing_l2s",&passing_l2s,"passing_l2s/I");
	tree->Branch("passing_l2m",&passing_l2m,"passing_l2m/I");
	tree->Branch("passing_l2mf",&passing_l2mf,"passing_l2mf/I");
	tree->Branch("passing_l3s",&passing_l3s,"passing_l3s/I");
	tree->Branch("passing_l3t",&passing_l3t,"passing_l3t/I");
	tree->Branch("passing_l3tf",&passing_l3tf,"passing_l3tf/I");
	tree->Branch("passing_l3m",&passing_l3m,"passing_l3m/I");
	tree->Branch("passing_l3mf",&passing_l3mf,"passing_l3mf/I");

	tree->Branch("passing_l1pwrtprop",&passing_l1pwrtprop,"passing_l1pwrtprop/I");
	tree->Branch("passing_l1fwrtl1p",&passing_l1fwrtl1p,"passing_l1fwrtl1p/I");
	tree->Branch("passing_l2swrtl1f",&passing_l2swrtl1f,"passing_l2swrtl1f/I");
	tree->Branch("passing_l2mwrtl2s",&passing_l2mwrtl2s,"passing_l2mwrtl2s/I");
	tree->Branch("passing_l2mfwrtl2m",&passing_l2mfwrtl2m,"passing_l2mfwrtl2m/I");
	tree->Branch("passing_l3swrtl2mf",&passing_l3swrtl2mf,"passing_l3swrtl2mf/I");
	tree->Branch("passing_l3twrtl3s",&passing_l3twrtl3s,"passing_l3twrtl3s/I");
	tree->Branch("passing_l3tfwrtl3t",&passing_l3tfwrtl3t,"passing_l3tfwrtl3t/I");
	tree->Branch("passing_l3mwrtl3tf",&passing_l3mwrtl3tf,"passing_l3mwrtl3tf/I");
	tree->Branch("passing_l3mfwrtl3m",&passing_l3mfwrtl3m,"passing_l3mfwrtl3m/I");
}

void TreeProducer::set_summary_results(std::vector<int> results)
{
	trigger_summary_res = results;
}

void TreeProducer::set_nselected_pvertices(int npv)
{
	nselected_pvertices = npv;
}
void TreeProducer::set_event_information(double r,double l,double e)
{
	run = r;
	lumi = l;
	event_number = e;
}

void TreeProducer::set_l2l3_tracks_matched(std::vector<reco::Track*> mu1_tracks,std::vector<reco::Track*> mu2_tracks,edm::Handle<reco::BeamSpot> BeamSpot)
{
	mu1_l2_track = mu1_tracks[0];
	mu1_l3_track = mu1_tracks[1];
	mu2_l2_track = mu2_tracks[0];
	mu2_l3_track = mu2_tracks[1];
	this->BeamSpot = BeamSpot;
}


void TreeProducer::fill_branches(std::vector<bool> mu1_res,std::vector<bool> mu2_res,reco::Muon mu1,reco::Muon mu2)
{
	int summary_res1 = 0,summary_res2 = 0;
	if(trigger_summary_res.size() == 2){
		summary_res1 = trigger_summary_res[0];
		summary_res2 = trigger_summary_res[1];
	}

	if(mu1_res[10])
		fill_branches_for_muon(mu2_res,mu2,mu1,summary_res2,mu2_l2_track,mu2_l3_track);

	if(mu2_res[10])
		fill_branches_for_muon(mu1_res,mu1,mu2,summary_res1,mu1_l2_track,mu2_l3_track);

	mu1_l2_track = NULL;
	mu1_l3_track = NULL;
	mu2_l2_track = NULL;
	mu2_l3_track = NULL;
	trigger_summary_res.clear();
	run =-1;
	lumi=-1;
	event_number=-1;
}

void TreeProducer::fill_branches_for_muon(std::vector<bool> mu_res,reco::Muon mu,reco::Muon other_mu,int summary_res,reco::Track* l2_trk,reco::Track* l3_trk)
{	
	reset_variables();
	
	pt = mu.pt();
	eta = mu.eta();
        phi = mu.phi();
        abseta = std::abs(mu.eta());

        reliso = ((mu.isolationR03().emEt + mu.isolationR03().hadEt + mu.isolationR03().sumPt)/mu.pt());
        ecaliso=mu.isolationR03().emEt;
        hcaliso=mu.isolationR03().hadEt;
        trackiso=mu.isolationR03().sumPt;
        combcaloiso= (mu.isolationR03().emEt + mu.isolationR03().hadEt);

	npvertices = (int32_t) nselected_pvertices;
	reco::TrackRef track = mu.innerTrack();
	nhits = (int32_t) track->numberOfValidHits();
	reco::TrackRef global_track = mu.globalTrack();
	chi2 = global_track->chi2() / global_track->ndof();
	reco::BeamSpot beamSpot=*(BeamSpot);
	math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());
	d0 = -1.*track->dxy(point);
	reco::HitPattern global_pattern = global_track->hitPattern();
	nmuon_hits = global_pattern.numberOfValidMuonHits();
	npixel_layers = global_pattern.pixelLayersWithMeasurement();
	nmatched_segments =  mu.numberOfMatches();
	
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > candidate = (mu.p4() + other_mu.p4());
	mass = candidate.mass();

	if(l2_trk){
		l2trk_eta = l2_trk->eta();
		l2trk_pt = l2_trk->pt();
		l2trk_nhits = l2_trk->numberOfValidHits();
		if(l2_trk->pt() < 7.0){
			std::cout << "L2 Track low pt: " << std::endl;
			std::cout << std::setprecision (15) << "Event: " << run << ", " << lumi << ", " << event_number << "," << std::endl;
		}
	}
	if(l3_trk){
		l3trk_eta = l3_trk->eta();
		l3trk_pt = l3_trk->pt();
		l3trk_nhits = l3_trk->numberOfValidHits();
		l3trk_dxy = l3_trk->dxy(BeamSpot->position());
	}

	if(summary_res >= 1) passing_l1 = 1;
	else passing_l1 = 0;
	if(summary_res == 2) passing_idx = 1;
	else passing_idx = 0;
	if(passing_l1 && !passing_idx)
		passing_hltwrtl1 = 0;
	else if(passing_l1 && passing_idx)
		passing_hltwrtl1 = 1;
	else
		passing_hltwrtl1 = -1;

	
	passing_prop = (int32_t) mu_res[0];
	passing_l1p  = (int32_t) mu_res[1];
	passing_l1f  = (int32_t) mu_res[2];
	passing_l2s  = (int32_t) mu_res[3];
	passing_l2m  = (int32_t) mu_res[4];
	passing_l2mf = (int32_t) mu_res[5];
	passing_l3s  = (int32_t) mu_res[6];
	passing_l3t  = (int32_t) mu_res[7];
	passing_l3tf = (int32_t) mu_res[8];
	passing_l3m  = (int32_t) mu_res[9];
	passing_l3mf = (int32_t) mu_res[10];

	if(mu_res[0]) passing_l1pwrtprop  = (int32_t) mu_res[1];
	else passing_l1pwrtprop= -1;
	if(mu_res[1]) passing_l1fwrtl1p  = (int32_t) mu_res[2];
	else passing_l1fwrtl1p = -1;
	if(mu_res[2]) passing_l2swrtl1f  = (int32_t) mu_res[3];
	else passing_l2swrtl1f = -1;
	if(mu_res[3]) passing_l2mwrtl2s  = (int32_t) mu_res[4];
	else passing_l2mwrtl2s = -1;
	if(mu_res[4]) passing_l2mfwrtl2m = (int32_t) mu_res[5];
	else passing_l2mfwrtl2m = -1;
	if(mu_res[5]) passing_l3swrtl2mf  = (int32_t) mu_res[6];
	else passing_l3swrtl2mf = -1;
	if(mu_res[6]) passing_l3twrtl3s  = (int32_t) mu_res[7];
	else passing_l3twrtl3s = -1;
	if(mu_res[7]) passing_l3tfwrtl3t = (int32_t) mu_res[8];
	else passing_l3tfwrtl3t = -1;
	if(mu_res[8]) passing_l3mwrtl3tf  = (int32_t) mu_res[9];
	else passing_l3mwrtl3tf = -1;
	if(mu_res[9]) passing_l3mfwrtl3m = (int32_t) mu_res[10];
	else passing_l3mfwrtl3m = -1;

        tree->Fill();
}
