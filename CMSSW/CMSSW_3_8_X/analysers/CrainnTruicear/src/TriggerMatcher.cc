#include "../interface/TriggerMatcher.h"

TriggerMatcher::TriggerMatcher()
{
	l2_track = NULL;
	l3_track = NULL;
}

TriggerMatcher::~TriggerMatcher()
{
}

void TriggerMatcher::set_handles( edm::Handle<l1extra::L1MuonParticleCollection> L1Muons,
				  edm::Handle<L2MuonTrajectorySeedCollection> L2Seeds,
				  edm::Handle<reco::RecoChargedCandidateCollection> L2Muons, 
				  edm::Handle<L3MuonTrajectorySeedCollection> L3Seeds,
				  edm::Handle<reco::TrackCollection> L3TkTracks,
				  edm::Handle<reco::RecoChargedCandidateCollection> L3Muons,
				  edm::Handle<reco::BeamSpot> BeamSpot,
				  edm::Handle<SeedMap> seedMapHandle)
{
	this->L1Muons  = L1Muons;
	this->L2Seeds  = L2Seeds;
	this->L2Muons  = L2Muons;
	this->L3Seeds  = L3Seeds;
	this->L3TkTracks = L3TkTracks;
	this->L3Muons  =  L3Muons;
	this->BeamSpot = BeamSpot;
	this->seedMapHandle = seedMapHandle;

	l1_already_matched.assign(L1Muons->size(), 0);

}
	
std::vector<bool> TriggerMatcher::match_muon(reco::Muon mu,
					     ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > prop_mu)
{
	match_results.clear();
	match_results.resize(11,false);
	match_results[0] = true; //Muon had to be propagated successfully to get here 
	
	//Want to save track candidates for best matched path
	l2_track = NULL;
	l3_track = NULL;
		
	if(!L1Muons.isValid()){
		std::cout << " L1Muon collection not valid " << std::endl;
		return match_results;
	}

	int min_dR_index=-1;
	double min_dR=999;
	l1extra::L1MuonParticleCollection::const_iterator min_dR_iter = L1Muons->begin();

	int current_index = 0;	// index of l1muon in collection
	for(l1extra::L1MuonParticleCollection::const_iterator l1mu = L1Muons->begin();
	    l1mu != L1Muons->end(); ++l1mu){

		const L1MuGMTExtendedCand l1muonCand = l1mu->gmtMuonCand();

		if(l1muonCand.quality() <= 3 || l1mu->pt() < 7 || l1mu->bx() != 0)
			continue; //doesn't correspond to our trigger, try next particle

		double dR = ROOT::Math::VectorUtil::DeltaR(l1mu->p4(),prop_mu);
		if((min_dR_index == -1 || dR < min_dR ) && !l1_already_matched[current_index]){
			min_dR = dR;
			min_dR_index = current_index;
			min_dR_iter = l1mu;
		}
	
		++current_index;	
	}

	if(min_dR < 0.3){
		l1_already_matched[min_dR_index] = 1;
		match_results[1] = true;//Has l1 particle within 0.3
		/*
		 *	FIXME: does this step "1" make any sense? there is a
		 *	particle but clearly not the one belonging to the
		 *	trigger we are studying; apart from the fact that we
		 *	are not studying L1 but HLT losses; in any case, this
		 *	step makes the unique matching incredibly complicated,
		 *	therefore I ignore it for now and always set it to the
		 *	same value as match_results[2]
		 */

		match_results[2] = true;//Has L1 particle passing our filter

		/**************** L2 **********************/
		if(!L2Seeds.isValid()){
			std::cout << " L2 Seed collection not valid " << std::endl;
			return match_results;
		}

		bool matched_full_path = match_muon_l2_l3(mu,min_dR_iter);

		if(matched_full_path)
			return match_results;
	}

	return match_results;	
}

bool TriggerMatcher::match_muon_l2_l3(reco::Muon mu,l1extra::L1MuonParticleCollection::const_iterator l1mu)
{		
	for(L2MuonTrajectorySeedCollection::const_iterator l2seed = L2Seeds->begin();
	    l2seed != L2Seeds->end(); l2seed++){


		l1extra::L1MuonParticleRef l1FromL2Seed = l2seed->l1Particle();
		if (l1FromL2Seed.id() != L1Muons.id()) 
			throw cms::Exception("CorruptData") << "You're using a different L1 collection than the one used by L2 seeds.\n";

		reco::CandidatePtr thisL1 = reco::CandidatePtr(L1Muons, l1mu - L1Muons->begin());
		if(l1FromL2Seed.key() != thisL1.key()){
			if(verbose) std::cout << "Not a corresponding l2 particle" << std::endl;
			continue; //This l2 particle did not come from our l1 particle
		}

		if(verbose) std::cout << "Found L2 Seed " << std::endl;
		match_results[3] = true; //has l2 seed

		if(!(L2Muons.isValid())){
			std::cout << " L2 Muon collection not valid " << std::endl;
			return false;
		}

		for(reco::RecoChargedCandidateCollection::const_iterator l2mu = L2Muons->begin();
		    l2mu != L2Muons->end(); l2mu++) {
			
			//edm::Ref<L2MuonTrajectorySeedCollection> l2museedRef = l2mu->track()->seedRef().castTo<edm::Ref<L2MuonTrajectorySeedCollection> >();
			//if(l2museedRef->l1Particle() != l1FromL2Seed)
			//	continue;

			const edm::RefVector<L2MuonTrajectorySeedCollection>& seeds = (*seedMapHandle)[l2mu->track()->seedRef().castTo<edm::Ref<L2MuonTrajectorySeedCollection> >()];
			bool is_triggered = false;
			for(size_t i=0; i<seeds.size(); i++){
				
				if(seeds[i]->l1Particle()!= l1FromL2Seed) 
					continue;
				else
					is_triggered = true;
				
			}
			if(!is_triggered)
				continue;
			
			match_results[4] = true; //has l2 muon. 

			const reco::Track & l2Trk = *l2mu->track();

			l2_track = const_cast<reco::Track*>(&l2Trk);//Keep track so we can plot vars to see why it didn't pass.

			if(!(fabs(l2Trk.eta()) < 2.5 && l2Trk.pt() > 7.0))// && l2Trk.numberOfValidHits() >= 0))
				continue; //if l2 particle doesn't pass filter it doesn't correspond to Mu9. Try next one 

			match_results[5] = true;//has l2 muon passing filter

			const reco::TrackRef l2Ref = l2mu->track();			

			/***************** L3 *****************/			
			bool matched_full_path = match_muon_l3(mu,l2Ref);

			if(matched_full_path)
				return true;
		}
	}

	//if you get here it means the full path wasn't matched
	return false;
}

bool TriggerMatcher::match_muon_l3(reco::Muon mu,const reco::TrackRef l2Ref)
{
	
	for (L3MuonTrajectorySeedCollection::const_iterator l3seed = L3Seeds->begin();
	     l3seed != L3Seeds->end(); l3seed++){
				
		reco::TrackRef staTrack = l3seed->l2Track();

		if (staTrack != l2Ref) continue;

		match_results[6] = true;//has l3 seed

		if (!L3TkTracks.isValid()){
			std::cout << " L3 TkTracks collection not valid " << std::endl;
			return false;
		}

		for (reco::TrackCollection::const_iterator l3tktrack = L3TkTracks->begin();
		     l3tktrack!= L3TkTracks->end(); l3tktrack++){

			edm::Ref<L3MuonTrajectorySeedCollection> l3seedRef = l3tktrack->seedRef().castTo<edm::Ref<L3MuonTrajectorySeedCollection> >();
			reco::TrackRef staTrack2 = l3seedRef->l2Track();

			if (staTrack2 != l2Ref) continue;

			match_results[7] = true;//has l3 track

			//FIXME: Where is l3 track filtered? Set = true for now
			match_results[8] = true;//has l3 track filtered

			if (!L3Muons.isValid()){
				std::cout << " L3 Muons collection not valid " << std::endl;
				return false;
			}

			for (reco::RecoChargedCandidateCollection::const_iterator l3mu = L3Muons->begin();
			     l3mu != L3Muons->end(); l3mu++) {
				
				edm::Ref<L3MuonTrajectorySeedCollection> l3seedRef2 = l3mu->track()->seedRef().castTo<edm::Ref<L3MuonTrajectorySeedCollection> >();
				reco::TrackRef staTrack3 = l3seedRef2->l2Track();
								
				if (staTrack3 != l2Ref) continue;
				
				match_results[9] = true;//has l3 muon

				const reco::Track &l3Trk = *l3mu->track();
				l3_track = const_cast<reco::Track*>(&l3Trk);//Keep track so we can plot vars to see why it didn't pass.
				double bs_pos = l3Trk.dxy(BeamSpot->position());
				
				if(!(fabs(l3Trk.eta()) < 2.5 && l3Trk.pt() > 15.0 && fabs(bs_pos) <= 2.0))
					//&& l3Trk.numberOfValidHits() >= 0))
					continue; //Doesn't pass trigger selection

				match_results[10] = true;//has l3 muon filtered. i.e It passed the full trigger path
				return true;
			}
		}
	}
	return false;
}
std::vector<reco::Track*> TriggerMatcher::get_l2l3_tracks()
{
	std::vector<reco::Track*> trks;
	trks.push_back(l2_track);
	trks.push_back(l3_track);
	return trks;
}		
