#include "../interface/CrainnTruicear.h"

CrainnTruicear::CrainnTruicear(const edm::ParameterSet& iConfig):
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  PriVertexLabel_(iConfig.getUntrackedParameter<edm::InputTag>("primaryVertexTag")),
  hlTriggerResults_ (iConfig.getParameter<edm::InputTag> ("HLTriggerResults")),
  hltAodSummary_ (iConfig.getParameter<edm::InputTag> ("HLTAodSummary")),
  l1Label_ (iConfig.getParameter<edm::InputTag> ("l1s")),
  l2MuonLabel_ (iConfig.getParameter<edm::InputTag> ("L2Muons_Collection")),
  l2SeedLabel_ (iConfig.getParameter<edm::InputTag> ("L2Seeds_Collection")),
  l3SeedLabel_ (iConfig.getParameter<edm::InputTag> ("L3Seeds_Collection")),
  l3TkTrkLabel_ (iConfig.getParameter<edm::InputTag> ("L3TkTracks_Collection")),
  l3MuonLabel_ (iConfig.getParameter<edm::InputTag> ("L3Muons_Collection")),
  BeamSpotLabel_ (iConfig.getParameter<edm::InputTag> ("BeamSpotTag")),
  l1matcherconf_ (iConfig.getParameter<edm::ParameterSet>("l1matcherConfig")),
  SeedMapTagLabel_ (iConfig.getParameter<edm::InputTag> ("SeedMapTag"))
{
	trigger_name = iConfig.getParameter<std::string>("triggername");
	outfile_name = iConfig.getParameter<std::string>("outfilename");

	hltConfigChanged = true;
	run_changed = true;
	lumi_block_changed = true;
	first_in_job = true;
	trigger_available = false;

   	outfile = NULL;
	hltConfig = NULL;

	set_trigger_menu = "";
	auto_trigger_menu = "";

	outfile = new TFile(outfile_name.c_str(),"RECREATE");

	tree_producer = new TreeProducer(outfile);
	prop_to_l1muon = new PropagateToMuon(l1matcherconf_);
	hltConfig = new HLTConfigProvider();

	nevents_triggered = 0;
	nevents_passed = 0;
	nevents_failed = 0;
	nevents_total = 0;

	nmu_prop = 0;
	nmu_l1p = 0;
	nmu_l1f = 0;
	nmu_l2s = 0;
	nmu_l2m = 0;
	nmu_l2mf = 0;
	nmu_l3s = 0;
	nmu_l3t = 0;
	nmu_l3tf = 0;
	nmu_l3m = 0;
	nmu_l3mf = 0;
	nmu_hlt = 0;
	nmu_l1 = 0;

	nselected_pvertices = 0;
}


CrainnTruicear::~CrainnTruicear()
{
	if(hltConfig){
		delete hltConfig;
		hltConfig = NULL;
	}

	if(tree_producer){
		delete tree_producer;
		tree_producer = NULL;
	}

	if(prop_to_l1muon){
		delete prop_to_l1muon;
		prop_to_l1muon = NULL;
	}

   	outfile->Write();
   	outfile->Close();
   	if(outfile){
   		delete outfile;
		outfile = NULL;
	}

	

	std::cout << "******** Detailed Summary *******" << std::endl;
	std::cout << "N Muons Propagated: " << nmu_prop << std::endl;
	std::cout << "N l1 particle: " << nmu_l1p << std::endl;
	std::cout << "N l1 particle filt: " << nmu_l1f << std::endl;
	std::cout << "N l2 seed: " << nmu_l2s << std::endl;
	std::cout << "N l2 muon: " << nmu_l2m << std::endl;
	std::cout << "N l2 muon filt: " << nmu_l2mf << std::endl;
	std::cout << "N l3 seed: " << nmu_l3s << std::endl;
	std::cout << "N l3 track: " << nmu_l3t << std::endl;
	std::cout << "N l3 track filt: " << nmu_l3tf << std::endl;
	std::cout << "N l3 muon: " << nmu_l3m << std::endl;
	std::cout << "N l3 muon filt: " << nmu_l3mf << std::endl;
	std::cout << "From TriggerSummaryAOD: " << std::endl;
	std::cout << " N L1: " << nmu_l1 << std::endl;
	std::cout << " N HLT: " << nmu_hlt << std::endl;
	std::cout << "************* Summary **********" << std::endl;
	std::cout << "N events triggered: " << nevents_triggered << std::endl;
	std::cout << "N events passed: " << nevents_passed << std::endl;
	std::cout << "N events failed: " << nevents_failed << std::endl;
	std::cout << "N events total: " << nevents_total << std::endl;
	std::cout << "********************************" << std::endl;

 
}

// ------------ method called to for each event  ------------
void CrainnTruicear::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	using namespace edm;
	
        edm::Handle<edm::View<reco::Muon> > muonHandle;
        iEvent.getByLabel(muoLabel_,muonHandle);

        edm::Handle<edm::View<reco::MET> > metHandle;
	iEvent.getByLabel(metLabel_,metHandle);

        edm::Handle<edm::View<reco::Jet> > jetHandle;
        iEvent.getByLabel(jetLabel_,jetHandle);

	edm::Handle<std::vector<reco::Vertex> > pvertexHandle;	
	iEvent.getByLabel(PriVertexLabel_, pvertexHandle);

	edm::Handle<edm::TriggerResults> HLTR;
	iEvent.getByLabel(hlTriggerResults_,HLTR);

	iEvent.getByLabel(hltAodSummary_, aodTriggerEvent);
 
	edm::Handle<l1extra::L1MuonParticleCollection> L1Muons;
	iEvent.getByLabel(l1Label_,L1Muons);
	
	edm::Handle<L2MuonTrajectorySeedCollection> L2Seeds;
	iEvent.getByLabel(l2SeedLabel_,L2Seeds);
	
	edm::Handle<reco::RecoChargedCandidateCollection> L2Muons;
	iEvent.getByLabel(l2MuonLabel_,L2Muons);
	
	edm::Handle<L3MuonTrajectorySeedCollection> L3Seeds;
	iEvent.getByLabel(l3SeedLabel_,L3Seeds);
	
	edm::Handle<reco::TrackCollection> L3TkTracks;
	iEvent.getByLabel(l3TkTrkLabel_,L3TkTracks);
	
	edm::Handle<reco::RecoChargedCandidateCollection> L3Muons;
	iEvent.getByLabel(l3MuonLabel_,L3Muons);
  
	//beam spot
	edm::Handle<reco::BeamSpot> recoBeamSpotHandle;
	iEvent.getByLabel(BeamSpotLabel_,recoBeamSpotHandle);

	edm::Handle<SeedMap> seedMapHandle;
	iEvent.getByLabel(SeedMapTagLabel_, seedMapHandle);
	

	if(first_in_job) initialise(iEvent, iSetup);

	outfile->cd();

	if(lumi_block_changed){
		hltConfig->init(iEvent.getRun(), iSetup, set_trigger_menu, hltConfigChanged);
		lumi_block_changed = false;
	}

	if(trigger_name != "-1"){
		if(!HLTR.isValid()){
			std::cout << "WARNING: no valid HLTR" << std::endl;
			return;
		}
	}

	if(trigger_name != "-1"){
		//Check if trigger is valid
		if(hltConfigChanged || first_in_job){
			trigger_available = check_trigger_available();
			first_in_job = false;
			hltConfigChanged = false;
			if(!trigger_available)
				std::cout << "WARNING: " << trigger_name << " not available. Skipping all events for this job/hltConf" << std::endl;
		}
		
		if(!trigger_available){
			return;
		}
	}

	/******************* Analysis ************************/

	nevents_total++;

	if(trigger_name != "-1"){
		//Check if event passed trigger before checking hlt objects.
		unsigned int trigger_id(hltConfig->triggerIndex(trigger_name) );
		if(!(HLTR->accept(trigger_id)))
			return;

		nevents_triggered++;
	}

	//Check event passes
	bool event_passed = check_event_passed(muonHandle,jetHandle,recoBeamSpotHandle,pvertexHandle);

	if(event_passed){
		nevents_passed++;
		//Names in this vector must correspond to the bools in the trigger_res vectors
		//NOTE: if added or removing a step. change resize in trigger_matcher
		std::vector<std::string> trigger_res_names;
		trigger_res_names.push_back("propagatesToM2");
		trigger_res_names.push_back("hasL1Particle");
		trigger_res_names.push_back("hasL1Filtered");
		trigger_res_names.push_back("hasL2Seed");
		trigger_res_names.push_back("hasL2Muon");
		trigger_res_names.push_back("hasL2MuonFiltered");
		trigger_res_names.push_back("hasL3Seed");
		trigger_res_names.push_back("hasL3Track");
		trigger_res_names.push_back("hasL3TrackFiltered");
		trigger_res_names.push_back("hasL3Muon");
		trigger_res_names.push_back("hasL3MuonFiltered");


		TriggerMatcher* trigger_matcher = new TriggerMatcher();
		//trigger_matcher->set_handles(L1Muons,L2Seeds,L2Muons,L3Seeds,L3TkTracks,L3Muons,recoBeamSpotHandle,seedMapHandle);
		//At this point you have two muons only
		std::vector<bool> mu1_trigger_res, mu2_trigger_res;
		std::vector<reco::Track*> mu1_res_tracks, mu2_res_tracks;
		//Extrapolated muon		
		TrajectoryStateOnSurface prop_mu1_state = prop_to_l1muon->extrapolate(iso_muons[0]);
		ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > prop_mu1;
		if(!prop_mu1_state.isValid()){
			std::cout << "Could not propagate"<< std::endl;
			mu1_trigger_res.resize(trigger_res_names.size(),false);
		}
		else{
			prop_mu1.SetEta(prop_mu1_state.globalPosition().eta());
			prop_mu1.SetPhi(prop_mu1_state.globalPosition().phi());
			//mu1_trigger_res = trigger_matcher->match_muon(iso_muons[0],prop_mu1);
			//mu1_res_tracks = trigger_matcher->get_l2l3_tracks();
		}
		//Extrapolated muon		
		TrajectoryStateOnSurface prop_mu2_state = prop_to_l1muon->extrapolate(iso_muons[1]);
		ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > prop_mu2;
		if(!prop_mu2_state.isValid()){
			std::cout << "Could not propagate"<< std::endl;
			mu2_trigger_res.resize(trigger_res_names.size(),false);
		}
		else{
			prop_mu2.SetEta(prop_mu2_state.globalPosition().eta());
			prop_mu2.SetPhi(prop_mu2_state.globalPosition().phi());
			//mu2_trigger_res = trigger_matcher->match_muon(iso_muons[1],prop_mu2);
			//mu2_res_tracks = trigger_matcher->get_l2l3_tracks();
		}

		//For detailed summary 
		if(mu1_trigger_res[0]) nmu_prop++;  if(mu2_trigger_res[0]) nmu_prop++;
		if(mu1_trigger_res[1]) nmu_l1p++;   if(mu2_trigger_res[1]) nmu_l1p++;
		if(mu1_trigger_res[2]) nmu_l1f++;   if(mu2_trigger_res[2]) nmu_l1f++;
		if(mu1_trigger_res[3]) nmu_l2s++;   if(mu2_trigger_res[3]) nmu_l2s++;
		if(mu1_trigger_res[4]) nmu_l2m++;   if(mu2_trigger_res[4]) nmu_l2m++;
		if(mu1_trigger_res[5]) nmu_l2mf++;  if(mu2_trigger_res[5]) nmu_l2mf++;
		if(mu1_trigger_res[6]) nmu_l3s++;   if(mu2_trigger_res[6]) nmu_l3s++;
		if(mu1_trigger_res[7]) nmu_l3t++;   if(mu2_trigger_res[7]) nmu_l3t++;
		if(mu1_trigger_res[8]) nmu_l3tf++;  if(mu2_trigger_res[8]) nmu_l3tf++;
		if(mu1_trigger_res[9]) nmu_l3m++;   if(mu2_trigger_res[9]) nmu_l3m++;
		if(mu1_trigger_res[10]) nmu_l3mf++; if(mu2_trigger_res[10]) nmu_l3mf++;
		
		
		if(trigger_name != "-1"){
			//Check TriggerSummaryAod for HLT and L1 results.
			if(prop_mu1_state.isValid() && prop_mu2_state.isValid())
				tree_producer->set_summary_results(match_trigger_summary_objects(iso_muons[0],iso_muons[1],prop_mu1,prop_mu2));
		}
		
		if(nselected_pvertices != 0)
			tree_producer->set_nselected_pvertices(nselected_pvertices);
		tree_producer->set_event_information(iEvent.run(),iEvent.luminosityBlock(),iEvent.id().event());
		tree_producer->set_l2l3_tracks_matched(mu1_res_tracks,mu2_res_tracks,recoBeamSpotHandle);
		tree_producer->fill_branches(mu1_trigger_res,mu2_trigger_res,iso_muons[0],iso_muons[1]);
		
		
		delete trigger_matcher;
		trigger_matcher = NULL;
			
	}
	else
		nevents_failed++;


}

bool CrainnTruicear::check_event_passed(edm::Handle<edm::View<reco::Muon> > muonHandle, edm::Handle<edm::View<reco::Jet> > jetHandle,edm::Handle<reco::BeamSpot> recoBeamSpotHandle, 	edm::Handle<std::vector<reco::Vertex> > pvertexHandle)
{
	iso_muons.clear();
	nselected_pvertices = 0;
	
	//cut muons pass selection. 
	//iso_muons in addition are within the Z mass range 
	std::vector<reco::Muon> cut_muons;


	std::vector<reco::Jet> iso_jets;
	//select iso jets, either pf or calo

	bool first_pv_passed = false;
	for(std::vector<reco::Vertex>::const_iterator pv = pvertexHandle->begin();
	    pv != pvertexHandle->end(); pv++){

		if(!pv->isFake() && pv->ndof() > 4 &&
		   fabs(pv->z()) < 24 && pv->position().Rho() < 2.0){
			nselected_pvertices++;
			if(pv == pvertexHandle->begin()){
				first_pv_passed = true;
			}
		}

	}
	//for now applying selection as is done in top group.
	if(!first_pv_passed)
		return false;

	for(edm::View<reco::Muon>::const_iterator mu = muonHandle->begin(); 
	    mu != muonHandle->end();mu++){

		if(!(mu->isGlobalMuon() && mu->isTrackerMuon())) continue;

		if(!(mu->pt() > 20 && fabs(mu->eta()) < 2.1)) continue;

		reco::TrackRef track = mu->innerTrack();
		reco::BeamSpot beamSpot=*(recoBeamSpotHandle);
		math::XYZPoint point(beamSpot.x0(),beamSpot.y0(), beamSpot.z0());

		double d0 = -1.*track->dxy(point);

		reco::TrackRef global_track = mu->globalTrack();
		double chi2 = global_track->chi2() / global_track->ndof();

		double nHits = track->numberOfValidHits();

		if(!(fabs(d0) < 0.02 && chi2 < 10. && nHits >= 11)) continue;


		double relIso = ((mu->isolationR03().emEt + mu->isolationR03().hadEt + mu->isolationR03().sumPt)/mu->pt());

		if(!(relIso < 0.15)) continue;

		reco::HitPattern global_pattern = global_track->hitPattern();

		//In order: nMuonHits, nPixelLayers, nMatchedSegments
		if(!(global_pattern.numberOfValidMuonHits() >= 1 && global_pattern.pixelLayersWithMeasurement() >= 1
		     && mu->numberOfMatches() >= 2)) continue;

		if(!(fabs(mu->vz() - pvertexHandle->begin()->z()) < 1.0))
		   continue;

		//Add dR cut w.r.t iso jet

		cut_muons.push_back(*mu);
	}

	if(cut_muons.size() < 2)
		return false;
	
	//Reconstruct Z candidate
	for(std::vector<reco::Muon>::iterator mu_a = cut_muons.begin(); mu_a != cut_muons.end();mu_a++){

		std::vector<reco::Muon>::iterator next_mu = mu_a + 1;
		
		for(std::vector<reco::Muon>::iterator mu_b = next_mu; mu_b != cut_muons.end();mu_b++){
			
			ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > candidate = (mu_a->p4() + mu_b->p4());
			if(mu_a->charge() != mu_b->charge() &&
			 candidate.mass() > 60. && candidate.mass() < 120.){
				iso_muons.push_back(*mu_a);
				iso_muons.push_back(*mu_b);
			}
		}
		
	}

	if(iso_muons.size() == 2)
		return true;
	else 
		return false;

}

std::vector<int> CrainnTruicear::match_trigger_summary_objects(reco::Muon mu1,reco::Muon mu2,ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > prop_mu1,ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > prop_mu2)
{
	std::vector<int> results;
	results.resize(2,0);

	std::vector<std::string> module_names = hltConfig->moduleLabels(trigger_name);

	std::vector<trigger::TriggerObject> l1_found;
	std::vector<trigger::TriggerObject> hlt_found;

	std::string l1_module;
	std::string hlt_filter;

	
	//Loop over all modules. Find names corresponding to l1 and hlt
	for(std::vector<std::string>::iterator module = module_names.begin();
	    module != module_names.end(); module++){

		if(hltConfig->moduleType(*module) == "HLTLevel1GTSeed" )
			l1_module = *module;			
		if(!TString(*module).Contains("hltBoolEnd"))
			hlt_filter = *module;
	}

	//fill trigger objects
	l1_found = get_trigger_summary_objects(l1_module);
	hlt_found = get_trigger_summary_objects(hlt_filter);

	//Match to l1
	bool found_l1_mu1 = false,found_l1_mu2 = false;
	bool found_hlt_mu1 = false,found_hlt_mu2 = false;
	for(std::vector<trigger::TriggerObject>::iterator l1_obj = l1_found.begin();
	    l1_obj != l1_found.end();l1_obj++){

		ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > l1;
		l1.SetPxPyPzE(l1_obj->px(),l1_obj->py(),l1_obj->pz(),l1_obj->energy());

		double dR1 = ROOT::Math::VectorUtil::DeltaR(l1,prop_mu1);
		double dR2 = ROOT::Math::VectorUtil::DeltaR(l1,prop_mu2);
		
		if(!found_l1_mu1 && (found_l1_mu2 || dR1 < dR2) && dR1 < 0.3)
			found_l1_mu1 = true;

		else if(!found_l1_mu2 && (found_l1_mu1 || dR2 < dR1) && dR2 < 0.3)
			found_l1_mu2 = true;
	}
	//match to hlt
	for(std::vector<trigger::TriggerObject>::iterator hlt_obj = hlt_found.begin();
	    hlt_obj != hlt_found.end();hlt_obj++){

		ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > hlt;
		hlt.SetPxPyPzE(hlt_obj->px(),hlt_obj->py(),hlt_obj->pz(),hlt_obj->energy());

		double dR1 = ROOT::Math::VectorUtil::DeltaR(hlt,mu1.p4());
		double dR2 = ROOT::Math::VectorUtil::DeltaR(hlt,mu2.p4());
		
		if(!found_hlt_mu1 && (found_hlt_mu2 || dR1 < dR2) && dR1 < 0.2)
			found_hlt_mu1 = true;

		else if(!found_hlt_mu2 && (found_hlt_mu1 || dR2 < dR1) && dR2 < 0.2)
			found_hlt_mu2 = true;
	}

	if(found_hlt_mu1){ results[0] = 2; nmu_hlt++; nmu_l1++;}
	else if(found_l1_mu1){ results[0] = 1; nmu_l1++;}

	if(found_hlt_mu2){ results[1] = 2; nmu_hlt++; nmu_l1++;}
	else if(found_l1_mu2){ results[1] = 1; nmu_l1++;}
	
	return results;
}

std::vector<trigger::TriggerObject> CrainnTruicear::get_trigger_summary_objects(std::string module)
{
	const trigger::TriggerObjectCollection objects = aodTriggerEvent->getObjects();

	std::vector<trigger::TriggerObject> found_objects;

        //Now use module name to get keys for trigger objects
        edm::InputTag filterTag = edm::InputTag(module, "", set_trigger_menu);

        int filterIndex = aodTriggerEvent->filterIndex(filterTag);
        if ( filterIndex < aodTriggerEvent->sizeFilters() ) {
                //These trigger keys point you to the trigger objects passing this filter in the objects collection
                const trigger::Keys &keys = aodTriggerEvent->filterKeys( filterIndex );
                        for ( size_t i = 0; i < keys.size(); i++ ){

				found_objects.push_back(objects[keys[i]]);

			}
        }

	return found_objects;

}

bool CrainnTruicear::check_trigger_available()
{
	std::vector<std::string> valid_triggers = hltConfig->triggerNames();

	for(std::vector<std::string>::iterator valid = valid_triggers.begin();
	    valid != valid_triggers.end();valid++){
		if(trigger_name == *valid)
			return true;
	}
	//	std::cout << "Printing all available triggers: " <<std::endl;
	for(std::vector<std::string>::iterator valid = valid_triggers.begin();
	    valid != valid_triggers.end();valid++){
		std::cout << "Available Trigger: " << *valid << std::endl; 

	}
	
	return false;
}

void CrainnTruicear::initialise(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	outfile->cd();

}

void CrainnTruicear::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
	lumi_block_changed = true;
}

bool CrainnTruicear::onRun(edm::Run const &run, edm::EventSetup const &setup)
{
// 	edm::Handle<trigger::TriggerEvent> tmpTriggerEventHLT;
	
// 	const edm::ProcessHistory& processHistory(run.processHistory());
// 	for (edm::ProcessHistory::const_iterator it = processHistory.begin(); it != processHistory.end(); ++it)
// 		{
// 			std::cout << "\t" << it->processName() << std::endl;
	
			
// 		}
			
// 	if(hlTriggerResults_.process() != hltAodSummary_.process()){
// 		std::cout << "Error: Different trigger menus set " << std::endl;
// 		return false;
// 	}

	set_trigger_menu = hlTriggerResults_.process();
	auto_trigger_menu = "";
	
	std::cout << "auto selected menu:" << auto_trigger_menu << std::endl;
	std::cout << "set menu:" << set_trigger_menu << std::endl;

	if (hlTriggerResults_.process() == "")
		return false;
	

	return true;
}

void CrainnTruicear::beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup)
{
	prop_to_l1muon->init(currentEventSetup);

	run_changed = true;
	//if(mu_propagator && propagate_mu_to_station2)
	//mu_propagator->set_event_setup(currentEventSetup);
	
	bool found_menu = onRun(currentRun,currentEventSetup);
	if(!found_menu)
		std::cerr << "ERROR: No trigger menu found in onRun()" << std::endl; 
}
//define this as a plug-in
DEFINE_FWK_MODULE(CrainnTruicear);
