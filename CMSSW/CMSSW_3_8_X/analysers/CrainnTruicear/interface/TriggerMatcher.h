// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include <SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h>

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "FWCore/ServiceRegistry/interface/Service.h"

#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/Common/interface/TriggerResults.h"


#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/METReco/interface/MET.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

#include <DataFormats/Common/interface/Ref.h>

#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include <map>
#include "TLorentzVector.h"

#include "DataFormats/Common/interface/View.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"


#include "FWCore/Framework/interface/Frameworkfwd.h"

#include "DataFormats/Math/interface/deltaR.h"

#include "DataFormats/Common/interface/ValueMap.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/Common/interface/Ptr.h"

#include "DataFormats/L1Trigger/interface/L1MuonParticleFwd.h"
#include "DataFormats/L1Trigger/interface/L1MuonParticle.h"

#include "DataFormats/BeamSpot/interface/BeamSpot.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/RecoCandidate/interface/RecoChargedCandidate.h"
#include "DataFormats/RecoCandidate/interface/RecoChargedCandidateFwd.h"
#include "DataFormats/MuonReco/interface/MuonFwd.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/TrajectorySeed/interface/TrajectorySeed.h"

//new for association map
#include "DataFormats/L1Trigger/interface/L1MuonParticleFwd.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "DataFormats/MuonSeed/interface/L2MuonTrajectorySeedCollection.h"
#include "DataFormats/Common/interface/AssociationMap.h"
#include "DataFormats/Common/interface/OneToMany.h"
#include "DataFormats/Common/interface/Handle.h"
#include "DataFormats/HLTReco/interface/TriggerFilterObjectWithRefs.h"


#include "DataFormats/MuonSeed/interface/L2MuonTrajectorySeed.h"
#include "DataFormats/MuonSeed/interface/L2MuonTrajectorySeedCollection.h"
#include "DataFormats/MuonSeed/interface/L3MuonTrajectorySeed.h"
#include "DataFormats/MuonSeed/interface/L3MuonTrajectorySeedCollection.h"

#include "FWCore/Framework/interface/EDProducer.h"
#include "FWCore/Framework/interface/ESHandle.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "FWCore/MessageService/interface/MessageLogger.h"
//#include "FWCore/ParameterSet/interface/InputTag.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ParameterSet/interface/Registry.h"

#include "CommonTools/Utils/interface/StringCutObjectSelector.h"
#include "HLTriggerOffline/Muon/interface/PropagateToMuon.h"
//#include "MuonAnalysis/MuonAssociators/interface/PropagateToMuon.h"
#include "TrackingTools/TrajectoryState/interface/TrajectoryStateOnSurface.h"
#include "Math/VectorUtil.h"

#include <string>

typedef edm::AssociationMap<edm::OneToMany<std::vector<L2MuonTrajectorySeed>, std::vector<L2MuonTrajectorySeed> > > SeedMap;

class TriggerMatcher{
	public:
		TriggerMatcher();
		~TriggerMatcher();
     
		void set_handles(edm::Handle<l1extra::L1MuonParticleCollection> L1Muons,
		       edm::Handle<L2MuonTrajectorySeedCollection> L2Seeds,
		       edm::Handle<reco::RecoChargedCandidateCollection> L2Muons, 
		       edm::Handle<L3MuonTrajectorySeedCollection> L3Seeds,
		       edm::Handle<reco::TrackCollection> L3TkTracks,
		       edm::Handle<reco::RecoChargedCandidateCollection> L3Muons,
		       edm::Handle<reco::BeamSpot> BeamSpot,
				 edm::Handle<SeedMap> seedMapHandle);

		std::vector<bool> match_muon(reco::Muon muon,ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiE4D<double> > prop_mu);
		
		std::vector<reco::Track*> get_l2l3_tracks();
	private:
	  
		bool match_muon_l2_l3(reco::Muon mu,l1extra::L1MuonParticleCollection::const_iterator l1mu);
		bool match_muon_l3(reco::Muon mu,const reco::TrackRef l2Ref);
		
		
		edm::Handle<l1extra::L1MuonParticleCollection> L1Muons;
		edm::Handle<L2MuonTrajectorySeedCollection> L2Seeds;
		edm::Handle<reco::RecoChargedCandidateCollection> L2Muons; 
		edm::Handle<L3MuonTrajectorySeedCollection> L3Seeds;
		edm::Handle<reco::TrackCollection> L3TkTracks;
		edm::Handle<reco::RecoChargedCandidateCollection> L3Muons;
		edm::Handle<reco::BeamSpot> BeamSpot;
		edm::Handle<SeedMap> seedMapHandle;

		
		std::vector<bool> match_results;

		reco::Track *l2_track;
	        reco::Track *l3_track;

		std::vector<short> l1_already_matched;
		
		static const bool verbose = false;
};
