// system include files
#include <memory>

#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include <map>

#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/METReco/interface/MET.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "Math/VectorUtil.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"

#include <string>


class TreeProducer{
   public:
           TreeProducer(TFile *outfile);
          ~TreeProducer();

	  void fill_branches(std::vector<bool> mu1_res,std::vector<bool> mu2_res,reco::Muon mu1,reco::Muon mu2);
	  void set_summary_results(std::vector<int> results);
	  void set_nselected_pvertices(int npv);
	  void set_event_information(double run,double lumi,double event_number);
	  void set_l2l3_tracks_matched(std::vector<reco::Track*> mu1_tracks,std::vector<reco::Track*> mu2_tracks,edm::Handle<reco::BeamSpot> BeamSpot);
   private:
	  void reset_variables();
	  void book_branches();
	  void fill_branches_for_muon(std::vector<bool> mu_res,reco::Muon mu,reco::Muon other_mu,int summary_res,reco::Track* l2_trk,reco::Track* l3_trk);

	  TFile *outfile;
	  TTree* tree;

	  reco::Track* mu1_l2_track;
	  reco::Track* mu1_l3_track;
	  reco::Track* mu2_l2_track;
	  reco::Track* mu2_l3_track;
	  edm::Handle<reco::BeamSpot> BeamSpot;

	  double run;
	  double lumi;
	  double event_number;
	  
	  mutable float pt;
	  mutable float mass;
	  mutable float eta;
	  mutable float phi;
	  mutable int32_t passing_idx;
	  mutable int32_t passing_l1;
	  mutable int32_t passing_hltwrtl1;
	  mutable float abseta;
	  mutable float njets;
	  mutable float npvertices;
	  mutable float nhits;
	  mutable float reliso;
	  mutable float ecaliso;
	  mutable float hcaliso;
	  mutable float trackiso;
	  mutable float combcaloiso;
	  mutable float mindR;
	  mutable float chi2;
	  mutable float d0;
	  mutable float nmuon_hits;
	  mutable float npixel_layers;
	  mutable float nmatched_segments;

	  mutable float l2trk_nhits;
	  mutable float l2trk_eta;
	  mutable float l2trk_pt;
	  
	  mutable float l3trk_nhits;
	  mutable float l3trk_eta;
	  mutable float l3trk_pt;
	  mutable float l3trk_dxy;
	  
	  mutable int32_t passing_prop;
	  mutable int32_t passing_l1p;
	  mutable int32_t passing_l1f;
	  mutable int32_t passing_l2s;
	  mutable int32_t passing_l2m;
	  mutable int32_t passing_l2mf;
	  mutable int32_t passing_l3s;
	  mutable int32_t passing_l3t;
	  mutable int32_t passing_l3tf;
	  mutable int32_t passing_l3m;
	  mutable int32_t passing_l3mf;

	  mutable int32_t passing_l1pwrtprop;
	  mutable int32_t passing_l1fwrtl1p;
	  mutable int32_t passing_l2swrtl1f;
	  mutable int32_t passing_l2mwrtl2s;
	  mutable int32_t passing_l2mfwrtl2m;
	  mutable int32_t passing_l3swrtl2mf;
	  mutable int32_t passing_l3twrtl3s;
	  mutable int32_t passing_l3tfwrtl3t;
	  mutable int32_t passing_l3mwrtl3tf;
	  mutable int32_t passing_l3mfwrtl3m;

	  std::vector<int> trigger_summary_res;
	  int nselected_pvertices;

	  std::string directory_name;

	  static const bool verbose = false;



};
