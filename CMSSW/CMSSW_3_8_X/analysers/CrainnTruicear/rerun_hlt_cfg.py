import FWCore.ParameterSet.Config as cms

#from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAny_cfi import *
#from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAlong_cfi import *
#from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorOpposite_cfi import *
#from RecoMuon.DetLayers.muonDetLayerGeometry_cfi import *


process = cms.Process("Demo")

process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
process.GlobalTag.globaltag = 'GR_R_39X_V5::All' #CHANGE LATER.MAKES NO SENSE
#process.GlobalTag.globaltag = 'MC_36Y_V6::All' 
process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(1000) )

#import os
#os.system('$CMSSW_RELEASE_BASE/src/HLTrigger/Configuration/test/getHLT.sh --force --offline --data orcoff:/cdaq/physics/Run2010/v2.2/HLT_4\E29_PRE5/V2 MUON')

# import the menu
from HLTrigger.Configuration.OnData_HLT_GRun_newmenuB import *
# remove the prescales
process.PrescaleService.prescaleTable = cms.VPSet()

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
    #'file:////user/walsh/CMSSW_3_6_1_patch4/src/create_pat/ttbar_spring10_reco.root'
    'file:////user/walsh/CMSSW_3_9_7/src/rootfiles/WZMu_Dec22Skim_2010B_raw_reco.root'
    #'file:////user/walsh/CMSSW_3_9_7/src/rootfiles/WZMu_Dec22Skim_2010B_raw_reco.root'#run147196
    ),
    secondaryFileNames = cms.untracked.vstring('file:////user/walsh/CMSSW_3_9_7/src/rootfiles/testsecondaryfile.root')
)

#process.source.secondaryFileNames = cms.untracked.vstring('file:////user/walsh/CMSSW_3_9_7/src/rootfiles/testsecondaryfile.root')

process.filterMuons = cms.EDFilter("CandViewSelector", cut = cms.string('pt > 20. && abs(eta) < 2.4 && isGlobalMuon && isTrackerMuon = 1 && innerTrack.numberOfValidHits > 10 && (globalTrack.chi2/globalTrack.ndof) < 10'), src = cms.InputTag("muons"))
# filter for at least one reco muon with pt>5
process.countMuons = cms.EDFilter("CandViewCountFilter",
                                   minNumber = cms.uint32(2),
                                   maxNumber = cms.uint32(999999),
                                   src = cms.InputTag("filterMuons")
)
process.out = cms.OutputModule("PoolOutputModule",
    #verbose = cms.untracked.bool(True),
    #dropMetaDataForDroppedData = cms.untracked.bool(True),
    fileName = cms.untracked.string('test_secondaryfile_HLTDEBUG.root'),
    dataset = cms.untracked.PSet(
            dataTier = cms.untracked.string('USER'),
            filterName = cms.untracked.string('')
                )
)
   
# remove the endpaths from the default configuration
import FWCore.ParameterSet.DictTypes
process.__dict__['_Process__endpaths'] = FWCore.ParameterSet.DictTypes.SortedKeysDict()                            


# add this path to run muon HLT reconstruction independently from the filters
#process.muonRECO = cms.Path( process.HLTBeginSequence * process.HLTL2muonrecoSequence * process.HLTL2muonisorecoSequence * process.HLTL3muonrecoSequence * process.HLTL3muonisorecoSequence * process.HLTMuTrackJpsiPixelRecoSequence * process.HLTMuTrackJpsiTrackRecoSequence )
#
#process.outpath = cms.EndPath(process.out)
process.muonRECO = cms.Path(
    process.filterMuons *
    process.countMuons *
    (
    #HLT_Mu9
    process.HLTBeginSequenceBPTX *
    process.hltL1sL1SingleMu7 *
    process.hltPreMu9*
    process.hltL1SingleMu7L1Filtered0 *
    process.HLTL2muonrecoSequence *
    process.hltL2Mu7L2Filtered7 *
    process.HLTL3muonrecoSequence *
    process.hltSingleMu9L3Filtered9  *
    process.HLTEndSequence) *
    process.out
    )

process.schedule = cms.Schedule(
    process.HLT_Mu9,
    process.HLT_Mu15_v1,
    process.muonRECO
    )
