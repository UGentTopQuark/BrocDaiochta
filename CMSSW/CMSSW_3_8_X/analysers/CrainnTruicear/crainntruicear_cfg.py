import FWCore.ParameterSet.Config as cms
import FWCore.Utilities.FileUtils as FileUtils

from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAny_cfi import *
from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAlong_cfi import *
from TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorOpposite_cfi import *
from RecoMuon.DetLayers.muonDetLayerGeometry_cfi import *

process = cms.Process("Demo")

process.load("Configuration.StandardSequences.Geometry_cff")
process.load("Configuration.StandardSequences.MagneticField_cff")
process.load("Configuration.StandardSequences.Reconstruction_cff")
process.load("Configuration.StandardSequences.Services_cff")

process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
process.GlobalTag.globaltag = 'GR_H_V25::All'
#process.GlobalTag.globaltag = 'START42_V13::All'
process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(100) )


myLumiRanges = FileUtils.loadListFromFile('jsonfiles/Dec22_rereco_json.txt')
lumiRanges =  cms.untracked.vstring(*myLumiRanges)

# JSON files in cmssw:
# copy from _CMSSW json file according section for selection:
# eg. https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions10/7TeV/Reprocessing/Cert_139779-140159_7TeV_July16thReReco_Collisions10_COMSSWConfig.txt

#myFileNames = FileUtils.loadListFromFile('filepaths/Dec22_WZSkim_20110211.txt')
#myFileNames = FileUtils.loadListFromFile('filepaths/DYToLL_TuneD6T_WZSkim_20110225.txt')
#input_filenames =  cms.untracked.vstring(*myFileNames)

process.source = cms.Source("PoolSource",
#     fileNames = input_filenames                       
    fileNames = cms.untracked.vstring(
    'file:////user/walsh/CMSSW_4_2_8_patch7/src/20120206_MuHLT_ZSkim/outputAForPP_HLT_Zskim_100_1_ks7.root',
    'file:////user/walsh/CMSSW_4_2_8_patch7/src/20120206_MuHLT_ZSkim/outputAForPP_HLT_Zskim_101_1_WKf.root',
    'file:////user/walsh/CMSSW_4_2_8_patch7/src/20120206_MuHLT_ZSkim/outputAForPP_HLT_Zskim_102_1_pBb.root',
    'file:////user/walsh/CMSSW_4_2_8_patch7/src/20120206_MuHLT_ZSkim/outputAForPP_HLT_Zskim_103_1_JG3.root',
    'file:////user/walsh/CMSSW_4_2_8_patch7/src/20120206_MuHLT_ZSkim/outputAForPP_HLT_Zskim_106_1_lGu.root'
    
    )
)

process.produceTrees = cms.EDAnalyzer('CrainnTruicear',
       	muonTag     = cms.untracked.InputTag("muons"),
	jetTag      = cms.untracked.InputTag("ak5CaloJets"),
	metTag      = cms.untracked.InputTag("htMetAK5"),
        primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD','','HLT'),
	HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT'),
                                      
        l1s  = cms.InputTag("hltL1extraParticles"),
        L2Muons_Collection=cms.InputTag("hltL2MuonCandidates"),
        L2Seeds_Collection=cms.InputTag("hltL2MuonSeeds"),
        L3Seeds_Collection=cms.InputTag("hltL3TrajectorySeed"),
        L3TkTracks_Collection=cms.InputTag("hltL3TkTracksFromL2"),
        L3Muons_Collection=cms.InputTag("hltL3MuonCandidates"), 
        BeamSpotTag = cms.InputTag("hltOfflineBeamSpot"),

    l1matcherConfig = cms.PSet(
        #useTrack = cms.string('global'),
        #useState = cms.string('outermost'),
        useTrack = cms.string("tracker"),  # 'none' to use Candidate P4; or 'tracker', 'muon', 'global'
        useState = cms.string("atVertex"), # 'innermost' and 'outermost' require the TrackExtra
        useSimpleGeometry = cms.bool(True),
    ),
        #associationMap                                      
        SeedMapTag = cms.InputTag( "hltL2Muons" ),

        #set triggername -1 if you don't want to use SummaryAOD                              
	triggername = cms.string("HLT_Mu15_v1"),
                                      
	outfilename = cms.string("TEST1.root")
)

#process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange(
#    lumiRanges
#    )


process.p = cms.Path(process.produceTrees)

process.schedule = cms.Schedule(
    process.p
    )
