#include "../interface/Reamhroghnaithe.h"

//
// constructors and destructor
//
Reamhroghnaithe::Reamhroghnaithe(const edm::ParameterSet& iConfig):
  eleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("electronTag")),
  muoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("muonTag")),
  jetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("jetTag")),
  tauLabel_(iConfig.getUntrackedParameter<edm::InputTag>("tauTag")),
  metLabel_(iConfig.getUntrackedParameter<edm::InputTag>("metTag")),
  phoLabel_(iConfig.getUntrackedParameter<edm::InputTag>("photonTag")),
  filteredEleLabel_(iConfig.getUntrackedParameter<edm::InputTag>("filteredElectronTag")),
  filteredMuLabel_(iConfig.getUntrackedParameter<edm::InputTag>("filteredMuonTag")),
  filteredJetLabel_(iConfig.getUntrackedParameter<edm::InputTag>("filteredJetTag")),
  hlTriggerResults_ (iConfig.getParameter<edm::InputTag> ("HLTriggerResults"))
{
	gen_match = NULL;
	if(isMC_)
		gen_match = new GenMatch();

	run_changed = true;

	// electron and muon as signal channels
	signal_ids.push_back(11);
	signal_ids.push_back(13);

	// initialise counters for background
	accepted_counter = 0;
	rejected_counter = 0;

	// initialise counters for signalse
	for(std::vector<int>::iterator signal_id = signal_ids.begin();
	    signal_id != signal_ids.end();
	    ++signal_id){
		signal_accepted_counter[*signal_id] = 0;
		signal_rejected_counter[*signal_id] = 0;
	}

	// if signal dataset (TTbar) distinguish between signal and background
	// TTbar channel
	is_signal_dataset = false;

	trigger_menu = hlTriggerResults_.process();

	cut_on_pat_objects = iConfig.getParameter<bool>("cut_on_pat_objects");
	cut_on_cmssw_selection = iConfig.getParameter<bool>("cut_on_cmssw_selection");

	if((cut_on_pat_objects && cut_on_cmssw_selection)
		|| (!cut_on_pat_objects && !cut_on_cmssw_selection)){
		std::cerr << "WARNING: you have to set either cut_on_pat_objects or cut_on_cmssw_selection but not both at the same time!" << std::endl;
		exit(1);
	}

	min_jet_pt = iConfig.getParameter<double>("min_jet_pt");
	max_jet_eta = iConfig.getParameter<double>("max_jet_eta");
	min_e_pt = iConfig.getParameter<double>("min_e_pt");
	max_e_eta = iConfig.getParameter<double>("max_e_eta");
	min_mu_pt = iConfig.getParameter<double>("min_mu_pt");
	max_mu_eta = iConfig.getParameter<double>("max_mu_eta");
	min_mu_nHits = iConfig.getParameter<double>("min_mu_nHits");
	max_mu_chi2 = iConfig.getParameter<double>("max_mu_chi2");
	global_muon = iConfig.getParameter<double>("global_muon");
	min_nelectrons = iConfig.getParameter<int>("min_nelectrons");
	min_nmuons = iConfig.getParameter<int>("min_nmuons");
	min_nleptons = iConfig.getParameter<int>("min_nleptons");
	min_njets = iConfig.getParameter<int>("min_njets");
}

Reamhroghnaithe::~Reamhroghnaithe()
{
	// print summary about selected and rejected events
	print_summary();

	/*
	 * 	clean up objects
	 */
	if(gen_match){
		delete gen_match;
		gen_match = NULL;
	}
}

// ------------ method called on each new Event  ------------
bool
Reamhroghnaithe::filter(edm::Event& iEvent, const edm::EventSetup& iSetup)
{
	bool cut_out = false;

	isMC_ = !iEvent.isRealData();

	if(cut_on_pat_objects) cut_out |= is_cut_pat_objects(iEvent);
	if(cut_on_cmssw_selection) cut_out |= is_cut_cmssw_selection(iEvent);

	count_events(iEvent, cut_out);

	run_changed = false;
	// return true if event PASSES cuts
	return !cut_out;
}

void Reamhroghnaithe::count_events(edm::Event& iEvent, bool cut_out)
{
        edm::Handle<reco::GenParticleCollection> genParticles;
	if(isMC_){
        	iEvent.getByLabel("genParticles", genParticles);

        	gen_match->fill_events_from_collection(genParticles);
	}

	if(!is_signal_dataset && gen_match != NULL && gen_match->is_ok()){
		is_signal_dataset = true;
	}

	if(is_signal_dataset && gen_match->is_ok() && gen_match->get_decay_mode() == 1){
		bool counted = false;
		for(std::vector<int>::iterator signal_id = signal_ids.begin();
		    signal_id != signal_ids.end();
		    ++signal_id){
			if( (gen_match->t_decay_is_leptonic() && abs(gen_match->get_particle("Wplus_dp1")->pdgId()) == *signal_id ) ||
		  	    (gen_match->tbar_decay_is_leptonic() && abs(gen_match->get_particle("Wminus_dp1")->pdgId()) == *signal_id) )
			{
				if(gen_match->t_decay_is_leptonic() && gen_match->tbar_decay_is_leptonic())
					std::cout << "WARNING: dilepton event" << std::endl;
				if(cut_out)
					signal_rejected_counter[*signal_id]++;
				else
					signal_accepted_counter[*signal_id]++;
				counted = true;
			}
		}
		if(!counted){
			if(cut_out)
				rejected_counter++;
			else
				accepted_counter++;
		}
	}else{
		if(cut_out)
			rejected_counter++;
		else
			accepted_counter++;
	}
}

void Reamhroghnaithe::beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup)
{
        run_changed = true;
}

bool Reamhroghnaithe::is_cut_cmssw_selection(edm::Event& iEvent)
{
        edm::Handle<edm::View<reco::Candidate> > reco_electrons;
        iEvent.getByLabel(filteredEleLabel_,reco_electrons);

        edm::Handle<edm::View<reco::Candidate> > reco_muons;
        iEvent.getByLabel(filteredMuLabel_,reco_muons);

        edm::Handle<edm::View<reco::Candidate> > reco_jets;
        iEvent.getByLabel(filteredJetLabel_,reco_jets);

	bool cut_out = false;

	// count good leptons
	double ngood_leptons = reco_muons->size() + reco_electrons->size();
	if(min_nleptons != -1 && min_nleptons > ngood_leptons)
		cut_out = true;

	// count good jets
	double ngood_jets = reco_jets->size();
	if(min_njets != -1 && min_njets > ngood_jets)
		cut_out = true;
	
	return cut_out;
}

bool Reamhroghnaithe::is_cut_pat_objects(edm::Event& iEvent)
{
        edm::Handle<edm::View<pat::Muon> > muons;
        iEvent.getByLabel(muoLabel_,muons);

        edm::Handle<edm::View<pat::Jet> > jets;
        iEvent.getByLabel(jetLabel_,jets);

        edm::Handle<edm::View<pat::Electron> > electrons;
        iEvent.getByLabel(eleLabel_,electrons);

	/*
	 *	cuts on JETs
	 */
	int ngood_jets=0;
	for(edm::View<pat::Jet>::const_iterator jet = jets->begin();
		jet != jets->end();
		++jet){
		if(
			((min_jet_pt == -1) || (jet->pt() > min_jet_pt)) &&
			((max_jet_eta == -1) || (fabs(jet->eta()) < max_jet_eta))
		)
			++ngood_jets;
	}

	if(min_njets != -1 && min_njets > ngood_jets)
		return true;	// reject event

	/*
	 *	cuts on MUONs
	 */
	int ngood_muons=0;
	for(edm::View<pat::Muon>::const_iterator muon = muons->begin();
		muon != muons->end();
		++muon){
		reco::TrackRef global_track = muon->globalTrack();
		int chi2 = 99999;
		if(!global_track.isNull() && global_track.isAvailable()){
			chi2 = global_track->chi2() / global_track->ndof();

		}
		int nHits = 0;
		reco::TrackRef inner_track = muon->innerTrack();
		if(!inner_track.isNull() && inner_track.isAvailable()){
			nHits = inner_track->numberOfValidHits();
		}

		if(
			((min_mu_pt == -1) || (muon->pt() > min_mu_pt)) &&
			((max_mu_eta == -1) || (fabs(muon->eta()) < max_mu_eta)) &&
			((global_muon == -1) || (muon->isGlobalMuon())) &&
			((min_mu_nHits == -1) || (nHits >= min_mu_nHits)) &&
			((max_mu_chi2 == -1) || (chi2 < max_mu_chi2))
		)
			++ngood_muons;
	}

	if(min_nmuons != -1 && min_nmuons > ngood_muons)
		return true;	// reject event

	/*
	 *	cuts on ELECTRONs
	 */
	int ngood_electrons=0;
	for(edm::View<pat::Electron>::const_iterator electron = electrons->begin();
		electron != electrons->end();
		++electron){
		if(
			((min_e_pt == -1) || (electron->pt() > min_e_pt)) &&
			((max_e_eta == -1) || (fabs(electron->eta()) < max_e_eta))
		)
			++ngood_electrons;
	}

	if(min_nelectrons != -1 && min_nelectrons > ngood_electrons)
		return true;	// reject event

	int ngood_leptons = ngood_electrons+ngood_muons;
	if(min_nleptons != -1 && min_nleptons > ngood_leptons)
		return true;	// reject event

	return false;	// do not cut out / accept event
}

void Reamhroghnaithe::print_summary()
{
	std::cout << "-----------STATISTICS-----------" << std::endl;
	for(std::map<int,int>::iterator signal = signal_accepted_counter.begin();
	    signal != signal_accepted_counter.end();
	    ++signal){
		std::cout << "signal_accepted_" << signal->first << ": " << signal_accepted_counter[signal->first] << std::endl;
		std::cout << "signal_rejected_" << signal->first << ": " << signal_rejected_counter[signal->first] << std::endl;
	}
	std::cout << "accepted: " << accepted_counter << std::endl;
	std::cout << "rejected: " << rejected_counter << std::endl;
	std::cout << "----------END-STATISTICS---------" << std::endl;
}

//define this as a plug-in
DEFINE_FWK_MODULE(Reamhroghnaithe);
