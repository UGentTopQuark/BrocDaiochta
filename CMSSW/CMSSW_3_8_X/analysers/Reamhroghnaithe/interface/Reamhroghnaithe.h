// -*- C++ -*-
//
// Package:    Reamhroghnaithe
// Class:      Reamhroghnaithe
// 
/**\class Reamhroghnaithe Reamhroghnaithe.cc analysers/Reamhroghnaithe/src/Reamhroghnaithe.cc

 Description: <one line class summary>

 Implementation:
     <Notes on implementation>
*/
//
// Original Author:  local user
//         Created:  Wed Apr 22 14:23:46 CEST 2009
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDFilter.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include <DataFormats/EgammaCandidates/interface/GsfElectron.h>
#include <DataFormats/MuonReco/interface/Muon.h>
#include <DataFormats/Candidate/interface/Candidate.h>

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"

#include "DataFormats/Candidate/interface/Candidate.h"

#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

#include "GenMatch.h"

#include "DataFormats/Common/interface/View.h"


//
// class declaration
//

class Reamhroghnaithe : public edm::EDFilter {
   public:
      explicit Reamhroghnaithe(const edm::ParameterSet&);
      ~Reamhroghnaithe();

   private:
	virtual bool filter(edm::Event&, const edm::EventSetup&);
	void beginRun(edm::Run const& currentRun, edm::EventSetup const& currentEventSetup);
	bool is_cut_cmssw_selection(edm::Event& iEvent);
	bool is_cut_pat_objects(edm::Event& iEvent);
	void count_events(edm::Event& iEvent, bool cut_out);
	void print_summary();

        edm::InputTag eleLabel_;
        edm::InputTag muoLabel_;
        edm::InputTag jetLabel_;
        edm::InputTag tauLabel_;
        edm::InputTag metLabel_;
        edm::InputTag phoLabel_;
  	edm::InputTag filteredEleLabel_;
  	edm::InputTag filteredMuLabel_;
  	edm::InputTag filteredJetLabel_;
        edm::InputTag hlTriggerResults_;
      
      // ----------member data ---------------------------
	double is_signal_dataset;

	bool lumi_block_changed;
	bool run_changed;
	std::string trigger_menu;

	GenMatch *gen_match;
	
	std::vector<int> signal_ids;
	bool isMC_;
	bool cut_on_pat_objects;
	bool cut_on_cmssw_selection;

	double min_jet_pt;
	double max_jet_eta;
	double min_e_pt;
	double max_e_eta;
	double min_mu_pt;
	double max_mu_eta;
	double min_mu_nHits;
	double max_mu_chi2;
	int global_muon;
	int min_nelectrons;
	int min_nmuons;
	int min_nleptons;
	int min_njets;
	
	double accepted_counter;
	double rejected_counter;
	std::map<int, int> signal_accepted_counter;
	std::map<int, int> signal_rejected_counter;
};
