import FWCore.ParameterSet.Config as cms
import ConfigParser

process = cms.Process("preselection")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
    #'dcap:///pnfs/iihe/cms/store/user/bklein/skims/Summer10MC_v1/Zjets_job_1423_TopTrigSkim.root'
    #'dcap:///pnfs/iihe/cms/store/user/bklein/skims/Summer10MC_v1/TTbar_job_93_TopTrigSkim.root'
    #'file:///user/bklein/trigger_cmssw/CMSSW_3_8_3/src/analysers/FlatNTuples/TopTrigSkim.root'
#    'file:///user/bklein/trigger_cmssw/CMSSW_3_8_7/src/analysers/FlatNTuples/2010A_segfault_RECO.root'
    'dcap:///pnfs/iihe/cms/store/user/mccartin/AOD/datasets/Fall10/madgraph/TTbar_norm/TTbar-madgraphjob-AOD_12_1_MCE.root'
    )
)

# Config parser that handles the CRAB designated output file
config = ConfigParser.RawConfigParser()
config.read('/user/mccartin/CMSSW/CMSSW_3_8_7_patch2/src/CRAB/crab.cfg')
crab_outfilename = config.get('CMSSW', 'output_file')

##-------------------- Communicate with the DB -----------------------
process.load('Configuration.StandardSequences.Services_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
##-------------------- Import the JEC services -----------------------
process.load('JetMETCorrections.Configuration.DefaultJEC_cff')

######################################################################
###  __          __     _____  _   _ _____ _   _  _____ 
###  \ \        / /\   |  __ \| \ | |_   _| \ | |/ ____|
###   \ \  /\  / /  \  | |__) |  \| | | | |  \| | |  __ 
###    \ \/  \/ / /\ \ |  _  /| . ` | | | | . ` | | |_ |
###     \  /\  / ____ \| | \ \| |\  |_| |_| |\  | |__| |
###      \/  \/_/    \_\_|  \_\_| \_|_____|_| \_|\_____|
###                                                     
### 
###	IMPORTANT: make sure you use the correct global tag!
###	If you use the wrong global tag, among other problems, the jet
###	corrections will be wrong, which makes your production USELESS
######################################################################
process.GlobalTag.globaltag = 'START38_V12::All'

#process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(1000) )

process.MessageLogger = cms.Service("MessageLogger")


process.filterMuons = cms.EDFilter("CandViewSelector", cut = cms.string('pt > 15. && abs(eta) < 2.4 && isGlobalMuon && innerTrack.numberOfValidHits > 8 && (globalTrack.chi2/globalTrack.ndof) < 12'), src = cms.InputTag("muons"))
process.filterElectrons = cms.EDFilter("CandViewSelector", cut = cms.string('pt > 20. && abs(eta) < 2.7'), src = cms.InputTag("gsfElectrons"))
process.filterJets = cms.EDFilter("CandViewSelector", cut = cms.string('pt > 15. && abs(eta) < 2.6'), src = cms.InputTag("ak5PFJetsL2L3"))

##############
###	In this CMSSW analyser there are two different ways of applying cuts for a preselection.
###	1.) create new collections of CMSSW objects according to certain selection, selecting objects form CMSSW collections, eg. gsfElectrons
###		how to use it:
###			-- specify cuts in the lines of fiterMuons, filterElectrons and filterJets
###			-- in the reamhroghnaithe module SELECT cut_on_pat_objects = cms.bool(False), cut_on_cmssw_selection = cms.bool(True),
###			-- define min number of jets, leptons
###			-- in this mode the cuts defined WITHIN the reamhroghnaithe module for min_pt/eta and other quality variables ARE MEANINGLESS,
###			   use the ones in the definition of filterMuons/...
###	2.) first producing PAT objects and then applying the selection cuts on the PAT collections
###		how to use it:
###			-- the collections filterMuons, filterElectrons and filterJets ARE IGNORED
###			-- in the reamhroghnaithe module SELECT cut_on_pat_objects = cms.bool(True), cut_on_cmssw_selection = cms.bool(False),
###			-- define all cuts in the reamhroghnaithe section, only variables that are already there are available
##############

process.reamhroghnaithe = cms.EDFilter("Reamhroghnaithe",
	# define tags
	electronTag = cms.untracked.InputTag("selectedPatElectrons"),
	tauTag      = cms.untracked.InputTag("selectedPatTaus"),
	muonTag     = cms.untracked.InputTag("selectedPatMuons"),
	jetTag      = cms.untracked.InputTag("selectedPatJets"),
	photonTag   = cms.untracked.InputTag("selectedPatPhotons"),
	metTag      = cms.untracked.InputTag("patMETs"),
	filteredElectronTag   = cms.untracked.InputTag("filterElectrons"),
	filteredMuonTag      = cms.untracked.InputTag("filterMuons"),
	filteredJetTag      = cms.untracked.InputTag("filterJets"),
	HLTriggerResults = cms.InputTag( 'TriggerResults','','REDIGI' ),

	# select cut method
	# cut on PAT objects
	cut_on_pat_objects = cms.bool(False),
	# cut externally in CMSSW on quality of leptons and just cut on the
	# number of leptons here
	cut_on_cmssw_selection = cms.bool(True),

	# define cuts
	min_njets = cms.int32(2),
	min_jet_pt = cms.double(30.),
	max_jet_eta = cms.double(2.4),

	min_nelectrons = cms.int32(-1),
	min_e_pt = cms.double(15),
	max_e_eta = cms.double(2.4),

	min_nmuons = cms.int32(-1),
	min_mu_pt = cms.double(15.),
	max_mu_eta = cms.double(2.4),
	min_mu_nHits = cms.double(9),
	max_mu_chi2 = cms.double(12),
	global_muon = cms.double(1),

	min_nleptons = cms.int32(1),
)

process.out = cms.OutputModule("PoolOutputModule",
	fileName = cms.untracked.string(crab_outfilename),
	outputCommands = cms.untracked.vstring('keep *', 
	)
)


process.p1 = cms.Path(process.ak5PFJetsL2L3 * 
			process.filterMuons *
			process.filterElectrons *
			process.filterJets *
			process.reamhroghnaithe * process.out)

process.schedule = cms.Schedule( process.p1 )
