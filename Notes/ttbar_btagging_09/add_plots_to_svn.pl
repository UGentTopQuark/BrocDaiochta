#!/usr/bin/perl

open(FILE, "<TTbar_btag_section.tex") or die "can't open TTbar_btag_section.tex";
@lines = <FILE>;
close(FILE);

foreach$line(@lines){
	if($line !~ m/^\s*\%/ && $line =~ m/\\includegraphics\[.*\]\{(fig\/.*)\}/){
		push(@plots, $1);
	}
}

print "Add following files to svn?\n";
foreach $plot(@plots){
	print "$plot\n";
}
print "Are you sure [N/y]? ";
$answer = <STDIN>;
chomp $answer;

if($answer eq 'y' || $answer eq 'Y'){
	foreach $plot(@plots){
		$command = 'svn add --parents '.$plot;
		print "$command\n";
		system($command);
	}
}
else{
	print "Aborting.\n";
}
