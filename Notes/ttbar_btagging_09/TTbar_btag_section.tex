\documentclass[11pt]{article}
\usepackage{graphicx}
\usepackage{rotating}

\begin{document}

\section{B-tagging}

In this section we examine the feasibility of using
algorithms which have been developed to identify
b-jets in our analysis. Since the decay products of $t\bar{t}$ include two b
quarks the ability to distinguish resultant b-jets becomes an important tool
for identifying this kind of event. We examine the b-tagging algorithms
available in CMS to determine which algorithm is most appropriate for our analysis.
We will require selected events to contain at least one
jet which passes a minimum b-tag requirement. This removes the majority of W+jets and Z+jets
events, which consist mainly of light flavour jets.


\subsection{Algorithms}
A number of algorithms have been developed by CMS for b-jet
identification. These algorithms each return a single number, the
discriminator, which indicates the liklihood of the jet coming from a b
quark. A comparison of
the algorithms can be found in \cite{bjetalgos09}.  
One group of b-tagging algorithms, the soft lepton taggers, require a B hadron
to decay semi-leptonically. Therefore
they can identify only a small fraction of b-jets. We want to work with higher efficiencies within our $t\bar{t}$ sample and so we disregard these algorithms.
The track counting lifetime based taggers  \cite{trackip_algos06} are simple and robust. They order jet
tracks by decreasing impact parameter significance and return the value of a specific track.
The probability algorithms \cite{trackip_algos06} compute the probability that each jet track originated 
from the primary vertex using the tracks signed impact parameter significance.These
probabilities are then combined to form the Jet Probability discriminator. The
Jet B Probability algorithm considers only the four most displaced tracks
in its calculation. 
The Simple Secondary Vertex algorithm \cite{bjetalgos09} is based on the reconstruction of a secondary
vertex. Its efficiency is limited to the probability of finding a vertex in
the presence of weak B hadron decay, which is 60-70\%.
The Combined Secondary Vertex algorithm \cite{CSV_algo06} is a complex
algorithm that combines vertex reconstruction with other variables known to distinguish b-jets from non b-jets.

\begin{figure}[htbp]
  \begin{center}
 \includegraphics[width=0.5\textwidth]{fig/mu/03s4/bjet_vs_nonb_s4.pdf}%
 \includegraphics[width=0.5\textwidth]{fig/ele/06s7/bjet_vs_nonb_s7.pdf}
 \caption{b-jet efficiency vs non-bjet efficiency for the b-tagging
   algorithms. Shown after s4 for the muon channel, left, s7 for the electron channel, right.}  
\label{fig:eff_b_v_non_algos}
  \end{center}
\end{figure}

A b-tag requirement will be applied after selection step
s4 in the muon selection, s7 in the electron selection. Figure~\ref{fig:eff_b_v_non_algos} shows b-jet efficiency versus non b-jet
efficiency for the b-tagging algorithms. The b-jet efficiency $\epsilon$
is defined as $N^{\mathrm{tagged}}_{b\mathrm{-jets}}/N_{b\mathrm{-jets}}$. $N_{b\mathrm{-jets}}$ is the total
number of jets that match to a b quark on parton level using
Monte Carlo truth matching. $N^{\mathrm{tagged}}_{b\mathrm{-jets}}$
is the number b-tagged jets that match to a b quark on parton level using
Monte Carlo truth matching. We focus on an efficiency range of 60 - 75\%. In
this range the results from the different algorithms agree within systematic uncertainty. We exclude the Simple Secondary Vertex algorithm to allow flexibility
in the b-jet efficiency we will examine. Among the remaining algorithms the
track counting algorithms are the most robust. The Track Counting High Efficiency
algorithm is used in this analysis.

\subsection{Applying a $b$-tag Cut}
An effective method for removing the $W + $jets and $Z + $jets
events that remain in the analysis is to identify events which contain no
b-jet. Figure~\ref{fig:bDiscrim_s5} shows the b discriminator distribution for the highest
b-tagged jet in an event for signal and background.
A cut of four supresses the majority of $W + $jets and $Z + $jets events. In
the electron decay channel this corresponds to a b-jet efficiency of $65.5 \pm
0.3\%$ and a mistag rate of $6.8 \pm 0.1\%$, in the muon channel the b-jet
efficiency is $66.2 \pm 0.2\%$ and the mistag rate is $6.9 \pm 0.1\%$.

\begin{figure}[htbp]
  \begin{center}
 \includegraphics[width=0.5\textwidth]{fig/mu/03s4/jet_highest_bDiscrim_cut_HighEff_.pdf}%
 \includegraphics[width=0.5\textwidth]{fig/ele/06s7/jet_highest_bDiscrim_cut_HighEff_.pdf}
\caption{b discriminator distributions from the Track Counting High Efficiency algorithm
  for the jet in each event with the highest discriminator value. Shown after
  s4 for the muon channel, left, s7 for the electron channel, right.}  
\label{fig:bDiscrim_s5}
  \end{center}
\end{figure}


After the b-tag requirement has been applied we examine the percentage of events in which the highest b-tagged jet in the semi-leptonic channel of a $t\bar{t}$ decay is really a b-jet from the
decay. The leading b-tagged jet efficiency, $\epsilon_{\mathrm{lead}\, b}$, is
defined as $N^{leading}_{b\mathrm{-jets}}/N_{\mathrm{events}}$. $N_{\mathrm{events}}$ is
the overall number of events. $N^{leading}_{b\mathrm{-jets}}$
is the number of times the highest b-tagged jet in the event is matched
to a b quark from $t\bar{t}$ decay on parton level using Monte Carlo truth matching.\\ 

\begin{table}[htbp]
  \begin{center}
  \begin{tabular}{|c|ccc|}
\hline
& highest b-tagged jet & 2nd highest b-tagged jet & both \\
\hline
\hline
muon channel & $91.69 \pm 0.19$ & $58.52 \pm 0.33$ & $52.78 \pm 0.34$ \\
electron channel & $87.67 \pm 0.27$ & $50.13 \pm 0.41$ & $44.06 \pm 0.41$ \\
\hline
\end{tabular}
\end{center}
  \caption{Percentage of events in which the highest,$2^{nd}$ highest or both highest and $2^{nd}$
  highest b-tagged jets, identified using Track Counting High Efficiency , are truth matched to a
  b quark on parton level. Shown for events from $t\bar{t}$ decay to the
  semi-leptonic muon and electron channel after selection step s5 and s8 respectively.}
    \label{tab:highest_btag_real}
\end{table}


\subsubsection{Systematic Uncertainty}
The systematic uncertainty on b-jet and light jet
efficiency at $100\mathrm{pb}^{-1}$ is estimated in \cite{bjetsys08} and
\cite{ljetsys08}. The uncertainty on b-jet
efficiency is estimated to be $\pm$ 11\% and the uncertainty on light jet
efficiency is estimated to be $\pm$ 4.7\%

To determine the effect of this uncertainty on our event numbers we produce a plot of b-jet efficiency versus
b discriminator for truth matched b-jets from $t\bar{t}$ decay. Using Figure~\ref{fig:eff_vs_bDiscrim_s5} we determine the
discriminator requirement that would give us the equivalent of $\pm$ 11\% efficiency on the b-tag requirement of four. The effect of the
systematic uncertainty on light jets is studied in the same way 

\begin{figure}[htbp]
  \begin{center}
 \includegraphics[width=0.5\textwidth]{fig/mu/03s4/btagging_eff_s4.pdf}%
 \includegraphics[width=0.5\textwidth]{fig/ele/06s7/btagging_eff_s7.pdf}%
\caption{b-jet efficiency vs. b discriminator. The
  vertical lines show $\epsilon$ after a discriminator requirement of four and
the requirements which give $\pm$ 11\% of the $\epsilon$ at four. Shown after
  s4 for the muon channel, left, s7 for the electron channel, right.}  
\label{fig:eff_vs_bDiscrim_s5}
  \end{center}
\end{figure}

The uncertainty on the b-jet and light jet efficiency is accounted for by applying b-tag requirements of 2.91 and
5.23 in selection step s5 in the muon channel, and requirements of 2.96 and
5.3 in selection step s8 in the electron channel . These correspond to $\pm$ 11\% of the b-jet efficiency at 4.
This b-tag requirement is very effective in supressing background events. 


\bibliographystyle{auto_generated}
\bibliography{auto_generated,my-references}
\end{document}
