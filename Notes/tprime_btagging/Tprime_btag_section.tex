\documentclass[11pt]{article}
\usepackage{graphicx}
\usepackage{rotating}

\begin{document}

\section{B-tagging}

In this section we examine the feasibility of using
algorithms which have been developed to identify
b-jets in our analysis. Since the decay products of $t'\bar{t}'$ include two b
quarks the ability to distinguish resultant b-jets becomes an important tool
for identifying this kind of event. We examine the b-tagging algorithms
available in CMS to determine which algorithm is most appropriate for the two
applications we study.
The first application is to require selected events to contain at least one
jet which passes a minimum b-tag cut. This removes the majority of W+jets and Z+jets
events, which consist mainly of light flavour jets.
The second application is to use b-tagging information to improve the
resolution in mass
reconstruction. After identifying a b-jet we assign its position in the
reconstruction method used to reduce the probability of incorrect jet assignment. 


\subsection{Algorithms}
A number of algorithms have been developed by CMS for b-jet
identification. These algorithms each return a single number, the
discriminator, which indicates the liklihood of the jet coming from a b
quark. A comparison of
the algorithms can be found in \cite{bjetalgos09}.  
One group of b-tagging algorithms, the soft lepton taggers, require a B hadron
to decay semi-leptonically. Therefore
they can identify only a small fraction of b-jets. We rely on high efficiency within our $t'$ sample and so we disregard these algorithms.
The track counting lifetime based taggers  \cite{trackip_algos06} are simple and robust. They order jet
tracks by decreasing impact parameter significance and return the value of a specific track.
The probability algorithms \cite{trackip_algos06} compute the probability that each jet track originated 
from the primary vertex using the tracks signed impact parameter significance.These
probabilities are then combined to form the Jet Probability discriminator. The
Jet B Probability algorithm considers only the four most displaced tracks
in its calculation. 
The Simple Secondary Vertex algorithm \cite{bjetalgos09} is based on the reconstruction of a secondary
vertex. Its efficiency is limited to the probability of finding a vertex in
the presence of weak B hadron decay, which is 60-70\%.
The Combined Secondary Vertex algorithm \cite{CSV_algo06} is a complex
algorithm that combines vertex reconstruction with other variables known to distinguish b-jets from non b-jets.
Our primary concern in choosing an algorithm is how often the highest b-tagged
jet is really a b-jet from semi-leptonic decay of the $t'$. This is vital for mass reconstruction since forcing the wrong jet to a fixed position would distort the
mass distribution. Figure ~\ref{fig:compare_algos_s5} compares the algorithms,
showing the percentage of events in which the highest b-tagged jet in the
semi-leptonic muon channel of a $t'\bar{t}'$ decay is really a b-jet from the
decay.The leading b-tagged jet efficiency, $\epsilon_{\mathrm{lead}\, b}$, is
defined as $N^{leading}_{b\mathrm{-jets}}/N_{\mathrm{events}}$. $N_{\mathrm{events}}$ is
the overall number of events. $N^{leading}_{b\mathrm{-jets}}$
is the number of times the highest b-tagged jet in the event is matched
to a b quark from $t\bar{t}$ decay on parton level using Monte Carlo truth matching .\\ 
The difference between algorithms is not significant enough to
have a noticable effect in mass reconstruction. \\

\begin{figure}[htbp]
  \begin{center}
 \includegraphics[width=0.6\textwidth]{fig/btagging_algos_05.pdf}
\end{center}
  \caption{Percentage of events in which the highest b-tagged
    jet, identified using the algorithms indicated, is truth matched to a
  b quark on parton level. Shown for events from $t'$ decay to the semi-leptonic muon
  channel after selection step s5.}
    \label{fig:compare_algos_s5}
\end{figure}



\begin{figure}[htbp]
  \begin{center}
%\begin{sideways}
 \includegraphics[width=0.6\textwidth]{fig/175/bjet_vs_nonb_05.pdf}
 
%\end{sideways}
\caption{b-jet efficiency vs non-bjet
efficiency for the b-tagging algorithms after selection step s5 using
 events from $t'$ decay to the semi-leptonic muon channel.}  
\label{fig:eff_b_v_non_algos}
  \end{center}
\end{figure}

A b-tag requirement will be applied after selection step
s5. Figure~\ref{fig:eff_b_v_non_algos} shows b-jet efficiency versus non b-jet
efficiency for the b-tagging algorithms. The b-jet efficiency $\epsilon$
is defined as $N^{\mathrm{tagged}}_{b\mathrm{-jets}}/N_{b\mathrm{-jets}}$. $N_{b\mathrm{-jets}}$ is the total
number of jets that match to a b quark on parton level using
Monte Carlo truth matching. $N^{\mathrm{tagged}}_{b\mathrm{-jets}}$
is the number b-tagged jets that match to a b quark on parton level using
Monte Carlo truth matching. We focus on an efficiency range of 60 - 75\%. In
this range the results from the different algorithms agree within systmatic uncertainty. We exclude the Simple Secondary Vertex algorithm to allow flexibility
in the b-jet efficiency we will examine. Of the remaining algorithms the
track counting algorithms are the most robust. The Track Counting High Efficiency
algorithm is used in this analysis.

\subsection{Applying a Btag Cut}
An effective method for removing the $W + $jets and $Z + $jets
events that remain after selection step s5 is to identify events which contain no
b-jet. Figure~\ref{fig:bDiscrim_s5} shows the b discriminator distribution for the highest
b-tagged jet in an event for signal and background.
A cut of five supresses the majority of $W + $jets and $Z + $jets events. 

\begin{figure}[htbp]
  \begin{center}
 \includegraphics[width=0.5\textwidth]{fig/300/signal_muon_05_cutset/jet_highest_bDiscrim_cut_HighEff_.pdf}%
 \includegraphics[width=0.5\textwidth]{fig/300/background_muon_05_cutset/jet_highest_bDiscrim_cut_HighEff_.pdf}
\caption{b discriminator distributions from the Track Counting High Efficiency algortihm after s5
  for the jet in each event with the highest discriminator value. Shown for
  $t'\,(300\,\mathrm{GeV})$ and Standard Model background}  
\label{fig:bDiscrim_s5}
  \end{center}
\end{figure}


\subsubsection{Systematic Uncertainty}
The systematic uncertainty on b-jet and light jet
efficiency at $100\mathrm{pb}^{-1}$ is estimated in \cite{bjetsys08} and
\cite{ljetsys08}. The uncertainty on b-jet
efficiency is $\pm$ 11\% and the uncertainty on light jet efficiency is $\pm$ 4.7\%

To determine the effect of this uncertainty on our event numbers we produce a plot of b-jet efficiency versus
b discriminator for truth matched b-jets from $t\bar{t}$ decay. The
b discriminator and $p_T$ of a jet are not correlated so we expect b-jet
efficiency to be the same for $t\bar{t}$ and $t'\bar{t}'$ events. The $t\bar{t}$ sample is
used to measure the expected effect of the efficiency  uncertainty on the discriminator
requirement to avail of higher event content in the simulated sample. Using Figure~\ref{fig:eff_vs_bDiscrim_s5} we determine the
discriminator requirement that would give us the equivalent of $\pm$ 11\% efficiency on the b-tag requirement of five. The effect of the
systematic uncertainty on light jets is studied in the same way 

\begin{figure}[htbp]
  \begin{center}
 \includegraphics[width=0.5\textwidth]{fig/175/btagging_eff_05.pdf}%
\caption{b-jet efficiency vs. b discriminator for events from
  semi-leptonic muon channel of $t$ decay after selection step s5. The
  vertical lines show $\epsilon$ after a discriminator requirement of five and
the requirements which give $\pm$ 11\% of the $\epsilon$ at five.}  
\label{fig:eff_vs_bDiscrim_s5}
  \end{center}
\end{figure}


A b-tag requirement of 5 retains 63\% of b-jets. The uncertainty on the b-jet
and light jet efficiency is accounted for by applying b-tag requirements of 3.8 and
6.3 in selection step s6. These correspond to $\pm$ 11\% of the b-jet efficiency at 5.
This b-tag requirement is very effective in supressing background events. 

\subsection{B-tagging in Mass Reconstruction}

In this section we examine how b-tagging information can be employed in mass
reconstruction methods. To reconstruct the mass of the $t'\bar{t}'$ quark pair
an assignment of the jets in an event to the partons of the $t'$ quarks is
necessary. The number of possible combinations for the assignment can be
reduced by taking advantage of the fact that in a $t'\bar{t}'$ event there are
two b-jets from the hadronical and leptonical decay of the $t'$ quarks. A
b-tagging algorithm is used to identify the most likely b-jet in the event,the
position of this jet is then fixed in the mass reconstruction method to be
either the b-jet of the hadronically decaying $t'$ quark or the b-jet from the
decay of the leptonically decaying $t'$ quark. 

The selection and reconstruction of the jets from the decay products of the
$t'\bar{t}'$ pair in the CMS detector is not fully efficient.
Table~\ref{tab:bjets_in_event_s6} shows how often the b-jets from the $t'$
decays are available after reconstruction and event selection,
Table~\ref{tab:tagged_is_real_s6} shows how often the jets that give the
highest and second highest b-tag value could be Monte Carlo truth matched to
b-jets from the $t'\bar{t}'$ pair decay. Both tables examine events in the $t'$
semi-leptonic muon decay channel after s6.


\begin{table}[htpb]
\begin{center}
  \begin{tabular}{c|ccccc}
    bjets in event& 175 & 250 & 300 & 350 & 400 \\
    \hline
    2 & $ 67.5 \pm 1.2 $ & $ 80.2 \pm 3 $ & $ 78.4 \pm 2.4 $ & $ 79.8 \pm 2.4 $&$ 78.7 \pm 2 $ \\
    1 &  $ 29.9 \pm 0.8 $&  $ 19 \pm 1.4 $&$ 20.6 \pm 1.2 $ & $ 19.1 \pm 1.2 $&$ 19.7 \pm 1 $ \\
    0 & $ 2.6 \pm 0.2 $ &$ 0.8 \pm 0.3 $ &$ 1 \pm 0.3 $ & $ 1 \pm 0.3 $& $ 1.6 \pm 0.3 $\\
  \end{tabular}
\end{center}
\caption{Percentage of b-jets from top/$t'$ decay after event reconstruction and selection.}
\label{tab:bjets_in_event_s6}
\end{table}

\begin{table}[htpb]
\begin{center}
  \begin{tabular}{c|ccccc}
    Discriminator value & 175 & 250 & 300 & 350 & 400 \\
    \hline
    highest & $ 87.6 \pm 0.5 $ & $ 90.2 \pm 1 $ &$ 89.1 \pm 0.8 $ & $ 88.8 \pm 0.9 $& $ 87.4 \pm 0.8 $\\
    2nd highest &$ 55.3 \pm 0.7 $  & $ 64.4 \pm 1.6 $ &$ 61.5 \pm 1.3 $ & $ 63 \pm 1.3 $&  $ 62.5 \pm 1.1 $\\
    both & $ 47.8 \pm  0.7 $ & $ 58.1 \pm  1.6$ & $ 54.2 \pm  1.3 $& $ 55 \pm 1.3 $& $54.3 \pm  1.1 $ \\    
  \end{tabular}
\end{center}
\caption{How often the highest,$2^{nd}$ highest or both highest and $2^{nd}$
  highest b-tagged jets are really b-jets}
\label{tab:tagged_is_real_s6}
\end{table}

Table~\ref{tab:tagged_is_real_s6} justifies fixing the highest b-tagged jet to
be used as the b-jet in mass reconstruction, either for the hadronic mass or
the leptonic mass. However the identification of the second b-tagged jet is not
efficient enough to achieve an improvement by using this information.

The results of implementing b-tagging in mass reconstruction are examined in
the next section.

\bibliographystyle{auto_generated}
\bibliography{auto_generated,my-references}
\end{document}
