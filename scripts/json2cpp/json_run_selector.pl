#!/usr/bin/perl

use Getopt::Std;
getopts("o:hr:f:");

my $json_file = $ARGV[0];

if($opt_h || !$json_file){
	print <<EOF;
usage: $0 [options] <input json file>
	-h 			print this help
	-f <run file>		file with list of runs
	-o <output file>	json output file
	-r <runs>		comma separated list of runs
EOF
	exit;
}

my $outfile;
$outfile = $opt_o if($opt_o);
my @runs;
@runs = split(/,\s*/, $opt_r) if ($opt_r);

if((not $opt_r) and $opt_f){
	my $read_runs = "";
	open(RUNFILE, "<$opt_f");
	while(my $run = <RUNFILE>){ $read_runs .= $run; }
	close(RUNFILE);
	$read_runs =~ s/\s*//g;
	@runs = split(/,/, $read_runs);
	foreach(@runs){
		unless($_ =~ /^\d+$/){
			print "invalid run: $_, exiting...\n";
		}
	}
}

my %runs_to_process = ();
foreach(@runs){ $runs_to_process{$_} = 1; }

open(JSONIN, "<$json_file");
while(defined(my $line = <JSONIN>)){
	chomp $line;
	$input_line .= $line;
}
close(JSONIN);

my $output_runs;

my @raw_runs = split(/,\s*"/, $input_line);
foreach my $raw_run(@raw_runs){
	$raw_run =~ s/{|}//g;
	$raw_run =~ s/"//g;
	print "WARNING: could not match raw run $raw_run" unless($raw_run =~ m/(\d+):(\s*\[\[.*\]\])/);
	my $nrun = $1;	
	my $lumiblocks = $2;	
	if(defined($runs_to_process{$nrun})){
		$output_runs .= "\"$nrun\":$lumiblocks, ";
	}
}

$output_runs =~ s/^/{/;
$output_runs =~ s/, $/}/;

if($outfile){
	open(OUTFILE, ">$outfile");
	print OUTFILE "$output_runs";
	close(OUTFILE);
}else{
	print "$output_runs\n";
}
