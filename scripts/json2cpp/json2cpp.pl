#!/usr/bin/perl

my @json_files = @ARGV;

my $file = "../src/AnalysisTools/RunSelector.cc";

##############################################################
# comments to place in cpp file:
#        // JSON AUTO FILL BEGIN -- do not edit this comment
#        // JSON AUTO FILL END -- do not edit this comment
##############################################################

my $output_lines = "";

if(@json_files > 0){
	$output_lines .= "\tdouble cur_run_number = evt_info->run();\n";
	$output_lines .= "\tdouble cur_lumi_block = evt_info->lumi_block();\n";
	$output_lines .= "\tdouble MIN=0;\n";
	$output_lines .= "\tdouble MAX=10000000;\n";

	$output_lines .= "\tif(";
	foreach my $json_file (@json_files){
		$output_lines .= get_cpp_lines($json_file);
	}
	$output_lines =~ s/\s*\|\|$/ )/;
	
	$output_lines .= "\t\treturn true;\n\telse\n\t\treturn false;\n";
}

update_file($output_lines, $file);

sub update_file
{
	my ($new_lines, $infile) = @_;

	my $section_begin = "// JSON AUTO FILL BEGIN -- do not edit this comment";
	my $section_end = "// JSON AUTO FILL END -- do not edit this comment";

	open(FH, "+< $infile")                 or die "Opening: $!";
	@file = <FH>;
	my $delete = 0;
	foreach $line(@file){
		if($line =~ m/$section_begin/){
			$delete = 1;
		}elsif($line =~ m/$section_end/){	
			$delete = 0;
			push(@changed_file, "\t$section_begin\n");
			push(@changed_file, $new_lines);
			push(@changed_file, "\t$section_end\n");
		}else{
	        	push(@changed_file, $line) unless($delete);
		}
	}
	seek(FH,0,0)                        or die "Seeking: $!";
	print FH @changed_file                     or die "Printing: $!";
	truncate(FH,tell(FH))               or die "Truncating: $!";
	close(FH)                           or die "Closing: $!";
}

sub get_cpp_lines
{
	my ($json_file) = @_;

	open(JFILE, "<$json_file");
	my $input_line="";
	while(defined(my $line = <JFILE>)){
		chomp $line;
		$input_line .= $line;
	}
	close(JFILE);

	$input_line =~ s/\s+/ /g;
	$input_line =~ s/\[ /\[/g;
	$input_line =~ s/ \]/\]/g;

	my @raw_runs = split(/,\s*"/,$input_line);

	my $lines = "";

	foreach my $raw_run (@raw_runs){
		$raw_run =~ s/{|}//g;
		$raw_run =~ s/"//g;
		print "raw run: $raw_run\n" if($verbose);
		print "WARNING: could not match raw run $raw_run" unless($raw_run =~ m/(\d+):\s*\[\[(.*)\]\]/);
		my $run_number = $1;
		my $lumi_blocks_line = $2;
		print "run: $run_number lumi_blocks: $lumi_blocks_line\n" if($verbose);
		my @lumi_blocks = split(/\],\s*\[/, $lumi_blocks_line);
		my %lumi_block_hash = ();
		foreach my $lumi_block (@lumi_blocks){
			print "lumi_block: $lumi_block\n" if($verbose);
			print "WARNING: could not match lumi block $lumi_block" unless($lumi_block =~ m/(\d+)\s*\,\s*(\d+)/);
			my $lumi_lower_boundary = $1;
			my $lumi_upper_boundary = $2;
			print "lower boundary: $lumi_lower_boundary\n" if($verbose);
			print "upper boundary: $lumi_upper_boundary\n" if($verbose);
			$lumi_block_hash{$lumi_lower_boundary} = $lumi_upper_boundary;
		}
		$lines .= get_cpp_line($run_number, \%lumi_block_hash);
	}
	return $lines;
}

sub get_cpp_line
{
	my ($run_number, $lumi_hash_ref) = @_;

	my $line = "\t( cur_run_number == $run_number && (\n";
	while(my ($lower_boundary, $upper_boundary) = each %{$lumi_hash_ref}){
		$line .= "\t  (cur_lumi_block >= $lower_boundary && cur_lumi_block <= $upper_boundary) ||\n";
	}
	$line =~ s/\s*\|\|\n$/ \) \) ||\n/;
	print "c++ code: \n" if($verbose);
	print "=============\n" if($verbose);
	print $line if($verbose);
	print "=============\n" if($verbose);

	return $line;
}
