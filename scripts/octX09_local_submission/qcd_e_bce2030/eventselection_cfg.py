import FWCore.ParameterSet.Config as cms

process = cms.Process("eventselection")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring( __FILE_NAMES__ )
)

process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttGenEvent_cff")
process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttSemiLepEvtBuilder_cff")

process.load( "HLTrigger.HLTanalyzers.hlTrigReport_cfi" )

process.MessageLogger = cms.Service("MessageLogger")

process.eventselection = cms.EDAnalyzer("EventSelection",
    electronTag = cms.untracked.InputTag("selectedLayer1Electrons"),
    tauTag      = cms.untracked.InputTag("selectedLayer1Taus"),
    muonTag     = cms.untracked.InputTag("selectedLayer1Muons"),
    jetTag      = cms.untracked.InputTag("selectedLayer1Jets"),
    photonTag   = cms.untracked.InputTag("selectedLayer1Photons"),
    metTag      = cms.untracked.InputTag("layer1METs"),
    semiLepTag      = cms.untracked.InputTag("ttSemiLepEvent"),
    hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypGenMatch:Key"),
    HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT' ),                               
    tprimeMass = cms.untracked.double(250.0),
    datasetName = cms.untracked.string("QCDbctoe20to30")
)

process.TFileService = cms.Service("TFileService", fileName = cms.string( __OUTFILE__ ) )

process.p0 = cms.Path(process.makeGenEvt * process.makeTtSemiLepEvent)

process.p1 = cms.Path(process.eventselection)

process.schedule = cms.Schedule( process.p0,
                                 process.p1 )
