#!/usr/bin/perl

my $dirname = $ARGV[0];
my $outfile = $dirname.'/CMSSW_1000.stdout';

# for grid-control: $crab = 0
# for crab	  : $crab = 1
$crab = 1;

my $pattern = '\=\-\-\-\-\-\-';
my $pattern2 = '\=\+\+\+\+\+\+';

my @values_to_average = ();
push(@values_to_average, "b tag efficiency");
push(@values_to_average, "b tag purity");
push(@values_to_average, "b tag eff times purity");
push(@values_to_average, "eff HLT_Mu15");
push(@values_to_average, "eff HLT_Mu11");
push(@values_to_average, "eff HLT_e15_LW_L1R");

my @values_to_sum = ();
push(@values_to_sum, "Nsel_trig HLT_Mu15");
push(@values_to_sum, "Nsel_trig HLT_Mu11");
push(@values_to_sum, "Nsel_trig HLT_e15_LW_L1R");
push(@values_to_sum, "cuts_passed");
push(@values_to_sum, "cuts_not_passed");
push(@values_to_sum, "ttbar_mu_events");
push(@values_to_sum, "ttbar_e_events");
push(@values_to_sum, "ttbar_bg_events");

#######################################
# to use it for preselection recombination use for example:
# my $pattern = '\-\-\-\-\-\-\-\-\-\-\-STATISTICS';
# my $pattern2 = '\=\+\+\+\+\+\+';
# 
# my @values_to_sum = ();
# push(@values_to_sum, "accepted");
# push(@values_to_sum, "rejected");
#######################################

#########################################
#	DO NOT EDIT BEYOND THIS LINE
#########################################

$nfiles = 0;

push(@values_to_sum, @values_to_average);

opendir(DIR, $dirname);
my @files = ();
while((defined(my $file = readdir(DIR)))){
	if($crab){
		next unless($file =~ m/crab_0_\d+/);
		$file = $dirname.'/'.$file.'/res/CMSSW_1.stdout';
	}else{
	        next unless($file =~ m/cmssw_out_\d+.txt/);
	        $file = $dirname.'/'.$file;
	}
	push(@files, $file)if(-e $file);
	$nfiles++;
}

# seperate one of the files and scroll down the handle to the pattern
my $first_file = pop @files;
open(FFILE, "<$first_file");
open(OUTFILE,">$outfile");
while(defined(my $fline = <FFILE>)){
	if($fline =~ m/^$pattern/){
		print OUTFILE $fline;
		last;	
	}
}

# open all other files and save handles
my @fhandles = ();
my $i = 0;
foreach my $file (@files){
	print "file: $file\n";
	open($fhandles[$i], "<$file");
	$i++;
}

# scroll all other files down...
print "adjusting files";
foreach my $handle(@fhandles){
	print ".";
	while(defined(my $line = <$handle>)){
	        last if($line =~ m/^$pattern/);
	}
}
print "\n";

print "combining files";
while(defined(my $fline = <FFILE>)){
	my $cutname, $first_cutvalue;
	if($fline =~ m/^([\w_ ]+):\s+([-\d\.:]+)/ ||
		 $fline =~ m/^([\w_]+):\s+([\w\._]+)/)
	{
		$cutname = $1;
		$first_cutvalue = $2;
	}
	else{
		$cutname = "";
		$first_cutvalue = -1;
	}

	my $line_already_printed=0;

	foreach $val(@values_to_sum){
		if($cutname eq $val){
			my $cuts_passed = $first_cutvalue;
			foreach my $handle(@fhandles){
				my $line = <$handle>;
				$line =~ m/^$val:\s+([\d\.]+)/;
				$cuts_passed += $1 unless($1 == -1);	
			}
			my $average = $cuts_passed / $nfiles;
			my $print_average=0;
			foreach $av_cut (@values_to_average){
				if($av_cut eq $cutname && $first_cutvalue != -1){
					$print_average=1;
				}
			}
							
			print OUTFILE "$cutname: $average\n" if($print_average == 1);
			print OUTFILE "$cutname: $cuts_passed\n" unless($print_average == 1);

			$line_already_printed=1;

			last;
		}
	}

	print OUTFILE $fline unless($line_already_printed);
	unless($line_already_printed){
		foreach my $handle(@fhandles){
			my $tmp = <$handle>;
		}
	}
	print ".";
}

print "\n";

# close all files
foreach my $handle(@fhandles){
	close($handle);
}
close(OUTFILE);
