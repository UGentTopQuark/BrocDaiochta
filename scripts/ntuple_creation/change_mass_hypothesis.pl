#!/usr/bin/perl

open(DATASETSFILE, "<datasets.txt");
$datasets = <DATASETSFILE>;
close(DATASETSFILE);

@datasets = split(/\s+/, $datasets);

die "usage: $0 <hypothesis mass>" unless(@ARGV);

$mass = $ARGV[0];

die "mass not recognised: $mass" unless($mass =~ m/^[\d.]+/);

$i = 0;
foreach $dataset(@datasets){
	open(FH, "+< $dataset/eventselection_cfg.py")                 or die "Opening: $!";
	@cfg_file = <FH>;
	foreach $line(@cfg_file){
		if($line =~ m/tprimeMass\s+=\s+cms\.untracked\.double\(.*\)/){
			$line =~ s/tprimeMass\s+=\s+cms\.untracked\.double\(.*\)/tprimeMass = cms.untracked.double($mass)/;
		}
		push(@changed_file, $line);
	}
	seek(FH,0,0)                        or die "Seeking: $!";
	print FH @changed_file                     or die "Printing: $!";
	truncate(FH,tell(FH))               or die "Truncating: $!";
	close(FH)                           or die "Closing: $!";
	@changed_file=();
}
