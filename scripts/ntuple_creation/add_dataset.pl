#!/usr/bin/perl

$dataset = $ARGV[0];

die "usage: $0 <dataset name>\n" unless($dataset);

`cp -r ttbar $dataset`;
open(TTBAR, "<ttbar/crab.cfg");
open(DS, ">$dataset/crab.cfg");
while(defined($line = <TTBAR>)){
	$line =~ s/ttbar/$dataset/g;
	print DS $line;
}
close(DS);
close(TTBAR);

open(TTBAR, "<ttbar/local_cfg_gen.cfg");
open(DS, ">$dataset/local_cfg_gen.cfg");
while(defined($line = <TTBAR>)){
	$line =~ s/ttbar/$dataset/g;
	print DS $line;
}
close(DS);
close(TTBAR);

`gmkdir.pl /pnfs/iihe/cms/store/user/bklein/ntuples/ntuples_v2/$dataset/`;
`echo "$dataset" >> datasets.txt.all`;
`rm -rf $dataset/.svn`;
