#!/bin/bash

date > submission_started.txt
CURRENT_PATH=`pwd`;
for i in `cat datasets.txt`; do
	cd ${CURRENT_PATH}/${i};
	for j in `ls *_crab.cfg`; do
		echo "creating crab jobs...";
		crab -create -cfg ${j}
	done
	for j in `ls -d crab_0_*`; do
		echo "submitting crab jobs...";
		crab -submit -continue ${j}
	done
done
