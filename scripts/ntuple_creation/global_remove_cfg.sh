#!/bin/bash

for i in `cat datasets.txt`; do
	cd ${i}; rm *.root *_crab.cfg *_create_ntuple_cfg.py *_create_ntuple_cfg.pyc; cd ..;
done
