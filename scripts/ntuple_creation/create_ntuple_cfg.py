import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring( __FILE_NAMES__ )
)

#process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(300) )

process.produceNTuples = cms.EDAnalyzer('FlatNTuples',
    electronTag = cms.untracked.InputTag("selectedLayer1Electrons"),
    tauTag      = cms.untracked.InputTag("selectedLayer1Taus"),
    muonTag     = cms.untracked.InputTag("selectedLayer1Muons"),
    jetTag      = cms.untracked.InputTag("selectedLayer1Jets"),
    photonTag   = cms.untracked.InputTag("selectedLayer1Photons"),
    metTag      = cms.untracked.InputTag("layer1METs"),
      HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT8E29' ),
      TriggerList      = cms.vstring("HLT_Ele15_LW_L1R","HLT_Mu9"),
      TriggerMenu      = cms.string("HLT8E29"),
      BTagAlgorithms	= cms.vstring("trackCountingHighEffBJetTags",
      				      "jetProbabilityBJetTags"),
	MCMatching	= cms.bool(True),
	outfile		= cms.string(__OUTFILE__)
)


process.p1 = cms.Path(process.produceNTuples)

process.schedule = cms.Schedule( process.p1 )
