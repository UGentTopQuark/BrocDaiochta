#!/usr/bin/perl

open(DATASETSFILE, "<datasets.txt.all");
$datasets = <DATASETSFILE>;
close(DATASETSFILE);

@datasets = split(/\s+/, $datasets);

$se_path = '/pnfs/iihe/cms/store/user';

die "usage: $0 <path to pat tuples> [with $se_path/<path to pat tuples>]" unless(@ARGV);

$directory = $ARGV[0];

die "can't identify user-name" unless($directory =~ m/^(\w+)/);
$user = $1;

$i = 0;
foreach $dataset(@datasets){
	open(FH, "+< $dataset/local_cfg_gen.cfg")                 or die "Opening: $!";
	my @file = <FH>;
	foreach my $line(@file){
		print "prosessing line: $line";
		if($line =~ /\$input_dir\s*=\s*\'$se_path/){
			$line =~ s/$se_path\/.*\'/$se_path\/$directory\/$dataset\/\'/;
		}
		push(@changed_file, $line);
	}
	seek(FH,0,0)                        or die "Seeking: $!";
	print FH @changed_file                     or die "Printing: $!";
	truncate(FH,tell(FH))               or die "Truncating: $!";
	close(FH)                           or die "Closing: $!";
	@changed_file=();
}

`echo "$se_path/$outdir" > input_directory.txt`;
