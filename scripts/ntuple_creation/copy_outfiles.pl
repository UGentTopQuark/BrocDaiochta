#!/usr/bin/perl

open(DATASETSFILE, "<datasets.txt");
$datasets = <DATASETSFILE>;
close(DATASETSFILE);

@datasets = split(/\s+/, $datasets);

die "usage: $0 <identifier>" unless(@ARGV);

$outdir = $ARGV[0];

die "dir already exists" if(-e $outdir);

`mkdir $outdir`;

$i = 0;
foreach $dataset(@datasets){
	print "cp $dataset/CMSSW_1000.stdout $outdir/CMSSW_$i.stdout\n";
	`cp $dataset/CMSSW_1000.stdout $outdir/CMSSW_$i.stdout`;
	$i++;
}
