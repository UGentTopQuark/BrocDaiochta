#!/usr/bin/perl

die "$0 should only be executed from within 'screen', remember sourcing the CMSSW/CRAB environment!\n" unless($ENV{'TERM'} eq 'screen');

$this_dir = `pwd`;
chomp $this_dir;
$config = 'submit_all_masses.cfg';
die "can't find config $this_dir/$config" unless(-e $config);
do "$this_dir/$config";

open(LOG, ">submission.log") or die "can't open logfile for wrinting\n";

submit_masses();

close(LOG);

sub submit_masses
{
	foreach $mass(@masses){
		my $mass_dir = '';
		if(defined($identifier)){
			$mass_dir = sprintf("%04d%02d%02d_$mass"."_$identifier",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3]);
		}
		my $tmp_dir = $this_dir;
		$tmp_this_dir =~ s/CMSSW_(\d+)_(\d+)_(\d+)\/src\/.*/CMSSW_\1_\2_\3\/src\//;

		if(defined($identifier) && (-d "$tmp_this_dir/$se_files_out/$mass_dir" || -d "$log_files_out/log_$mass_dir")){
			print "WARNING: output directory $se_files_out/$mass_dir already exists, changing to:\\";
			$mass_dir = sprintf("%04d%02d%02d_%02d%02d_$mass"."_$identifier",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3], (localtime)[2], (localtime)[1]);
			print "$se_files_out/$mass_dir\n";
		}
		if(!defined($identifier)){
			$mass_dir = sprintf("%04d%02d%02d_%02d%02d_$mass",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3], (localtime)[2], (localtime)[1]);
		}

		print "processing mass: $mass\n";
		run_command("making clean", "./make_clean.sh");
		change_datasets($mass);
		run_command("changing mass hypothesis", "./change_mass_hypothesis.pl $mass");
		run_command("removing old files", "./global_remove_cfg.sh");
		run_command("creating new configuration files", "./global_local_cfg_gen.sh");
		run_command("submitting", "./run_all.sh");
		wait_to_complete($mass);
		run_command("getting log files", "./get_all_output.sh");
#		run_command("combining log files", "./combine_all.sh");
#		run_command("copying outfiles", "./copy_outfiles.pl log_$mass_dir");
#		run_command("creating log files out dir", "mkdir -p $log_files_out") unless(-d $log_files_out);
#		run_command("moving outfiles", "mv log_$mass_dir $log_files_out") if($this_dir ne $log_files_out);
#		create_directory($this_dir, "$se_files_out/$mass_dir");
#		run_command("retrieving files from SE", "python LocalGetSEfiles.py $se_files_out/$mass_dir");
		print "=======================\n";
	}
#	run_command("making clean", "./make_clean.sh");
}

sub run_command
{
	my($message, $command) = @_;
	print $message if($message);
	if($command){
		print " ( $command )...";
		@output = `$command 2>&1`;
		print LOG @output;
	}

	print " done."."\n";
}

sub wait_to_complete
{
	my $mass = pop @_;

	$outdir = `cat output_directory.txt`;
	chomp $outdir;
	my ($to_process, $datasets_ref) = count_files_to_process();

	my $percentage = 0;

	print "waiting for output...\n";

	$crab_all_done = check_crab_status();

	my $sleeptime = 30;
	my $crab_status_interval = 1200;
	my $slept_time = 0;

	$|=1;
	
	while(int(0.5+($percentage*100)) < $min_percentage && !$crab_all_done){
		$percentage = get_percentage($outdir, $to_process, $datasets_ref);
		$percent_to_print = int ($percentage*100);

		print "output files on SE ( ";
		foreach $m (@masses){
			if($m == $mass){
				print ">$m< ";
			}else{
				print "$m ";
			}
		}
		print "): $percent_to_print%\r";

		if($slept_time > $crab_status_interval){
			run_command("displaying current time", "date");
			$| = 0;
			$crab_all_done = check_crab_status();
			$slept_time = 0;
			$| = 1;
		}
		$slept_time += $sleeptime;

		sleep $sleeptime;
	}
}

sub check_crab_status()
{
	$command = "./get_all_output.sh";
	print "checking crab status";
	print " ( $command )...\n";
	my @output = `$command 2>&1`;
	print "============\n";
	my %status = ();
	my $overall_jobs = 0;
	foreach my $line(@output){
		if($line =~ m/^1\s+(\w+)\s*$/){
			$status{$1} += 1;
			$overall_jobs++; 
		}
	}
	foreach my $key (keys %status){
		print "$key: ".$status{$key}."\n";
	}
	print "------------\n";
	print "Submitted: $overall_jobs\n";
	print "============\n";

	if($status{"Retrieved"} + $status{"Done"} + $status{"Aborted"} == $overall_jobs){
		return 1;
	}else{
		return 0;
	}
}

# should work, gives same results as manually counting
sub count_files_to_process
{
	open(DATASETSFILE, "<datasets.txt");
	$datasets = <DATASETSFILE>;
	close(DATASETSFILE);

	@datasets = split(/\s+/, $datasets);

	%ds = ();
	foreach $dataset(@datasets){
		chomp $dataset;
		$ds{$dataset} = 1;
	}

	my $file_counter = 0;

	foreach $dataset(@datasets){
		if(-d $dataset){
			opendir(DIR, "./$dataset");
			while(defined(my $file = readdir(DIR))){
				next unless($file =~ m/\d+_crab\.cfg/);
				$file_counter++;
			}
			closedir(DIR);
		}else{
			print "WARNING: one of the dataset directories cannot be found\n";
		}
	}

	return $file_counter, \%ds;
}

sub get_percentage
{
	my ($outdir, $nominal_files, $datasets_ref) = @_;

	my $all_finished_counter = 0;

	opendir(DIR, $outdir);
	while(defined(my $subdir = readdir(DIR))){
		next unless(-d $subdir && defined($datasets_ref->{$subdir}));
		next if($subdir eq '.' || $subdir eq '..');
		my $dir = "$outdir/$subdir";
		my @files = <$dir/*>;
		my $count = @files;

		$all_finished_counter += $count;
	}
	closedir(DIR);

	my $percentage = $all_finished_counter/$nominal_files;

	return $percentage;
}

sub change_datasets
{
	my $mass = pop @_;

	print "changing datasets.txt to $mass mass hypothesis...\n";

	open(DATASETSFILE, "<datasets.txt");
	$datasets = <DATASETSFILE>;
	close(DATASETSFILE);

	my $count = 0;	
	$count++ while $datasets =~ /tprime_\d+/g;
	
	if($count == 1 && -d "tprime_$mass"){
		open(DATASETSFILE, ">datasets.txt");
		$datasets =~ s/tprime_\d+/tprime_$mass/g;
		print DATASETSFILE $datasets;
		close(DATASETSFILE);
	}
}

sub create_directory
{
	my($this_dir, $outdir) = @_;

	$this_dir =~ s/CMSSW_(\d+)_(\d+)_(\d+)\/src\/.*/CMSSW_\1_\2_\3\/src\//;

	`mkdir -p $this_dir/$outdir`;
}
