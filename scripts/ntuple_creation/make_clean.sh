#/bin/bash

CURRENT_PATH=`pwd`;
for i in `cat datasets.txt`; do
	cd ${CURRENT_PATH}/${i};
	for j in `ls -d crab_0_*`; do
		crab -status -continue ${j};
		echo "checking status...";
		crab -getoutput -continue ${j};
		echo "cleaning directories...";
		crab -kill all -continue ${j};
		echo "retrieving all output...";
		rm -r ${j};
	done
	echo "cleaning storage...";
	echo "grm.pl -f /pnfs/iihe/cms/store/user/bklein/ntuples/ntuples_v2/${i}/*";
	grm.pl -f /pnfs/iihe/cms/store/user/bklein/ntuples/ntuples_v2/${i}/*;
done
