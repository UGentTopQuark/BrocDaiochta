#!/bin/bash

DATE=`date --date='7 days ago' +%F`

if [ -e newRuns.txt ]
then
  rm newRuns.txt
fi

if [ -e diff.txt ]
then
  rm diff.txt
fi

#nsls /castor/cern.ch/user/a/aytul/FV_DQM_Root_Files > newRuns.txt
ls ../multicrab_FV_2/DQM_files/ > newRuns.txt

if [ -e oldRuns.txt ]
then
  diff oldRuns.txt newRuns.txt | sed -n '/^>/ p' | sed 's/^.\{2\}//g' > diff.txt
else
  cp newRuns.txt diff.txt
fi

mv newRuns.txt oldRuns.txt
