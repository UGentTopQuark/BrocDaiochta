// This macro provides a function NewSlide 
//
/////////////////////////////////////////
TCanvas* NewSlide(const char* name, 
			      const char* title, 
			      int nx, int ny, float sizeX=600, float sizeY=800) 
{
  // create new canvas with user-defined number of pads

  char canvas_name[120], p1_name[120], p2_name[120], p3_name[120];

  TCanvas* slide;

  if (name ) sprintf(canvas_name,"%s",name);
  else       sprintf(canvas_name,"canv",);

  sprintf(p1_name,"%s_p1",canvas_name);
  sprintf(p2_name,"%s_p2",canvas_name);
  sprintf(p3_name,"%s_p3",canvas_name);

  slide = new TCanvas(canvas_name,canvas_name,0,0,sizeX,sizeY);
	slide->SetFillColor(18);

  TPad *p1 = new TPad(p1_name,p1_name,0.01,0.01,0.99,0.96);

  p1->Divide(nx,ny);
  p1->Draw();
  p1->Range(0,0,1,1);

  slide->cd();

  TPad *p2 = new TPad(p2_name, p2_name,0.7,0.965,0.99,0.995);
  p2->Draw();
  p2->cd();

  time_t t2 = time(0);
  tm* t22 = localtime(&t2);
  TText *text = new TText(0.05,0.3,asctime(t22));
  text->SetTextSize(0.5);
  text->Draw();
  p2->Modified();

  slide->cd();
				// create title pad

  TPad *title_pad = new TPad(p3_name, "title",0.05,0.965,0.69,0.995);
  title_pad->Draw();
  title_pad->cd();

  if (title) {
    TText *text = new TText(0.05,0.3,title);
    text->SetTextSize(0.5);
    text->Draw();
  }
  title_pad->Modified();

  return slide;
}
