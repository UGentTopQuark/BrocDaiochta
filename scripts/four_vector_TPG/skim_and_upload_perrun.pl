#!/usr/bin/perl
use Getopt::Std;

require ConfigReader;

getopts("r:f:c:s:u:i:m:");

#######Accepted Input#############
# s = skim/don't skim
# usage: -s 0 or -s 1. Default = 1,do skim
# u = upload/don't upload
# If u = 0 the program will skim new files but won't create plots
# r = runs:
# usage: one run or run range or run list 
# usage: -r 143567 or -r 145678-156789 or -r 123456,163547,394837
# f = file containing list of skimmed files to run over
# usage: -f file.txt
# c = .C file to be used by runUpload.
# If not defined will use .C auto generator
# usage: -c file.C
# Can also give as input list of skimmed files you want to run over
# usage: skimmed_run775303.root skimmed_run237804.root
# usage: -i example_config.cfg
# usage: -m 0 or -m 1
# Merge the input run range.
#The input files must have been skimmed previously.
#########What program does################
#./getNewDQMRuns.pl #if skimming new files
#./getsummary.pl #if skimming new files
#./plot_macro_generator.pl #if no .C file defined
#./runUpload.pl
#################################
my $config_file = 'example_config.cfg';
$config_file = $opt_i if(defined($opt_i));

my %script_setup = ();
our %configuration = ();
ConfigReader::read_config_file($config_file, \%configuration);
%script_setup = %{$configuration{'configuration'}};

$skims_path = $script_setup{'path_to_skims'};
$ref_C_script =  $script_setup{'reference_script'};
my $skim_path_prefix = "";
$skim_path_prefix = "rfio:" if($skims_path =~ m/castor\/cern.ch/ and !($skims_path =~ m/rfio:/));
#remove .C and use variable later
print "Config file: $config_file \n Path to skims: $skims_path \n Reference .C script: $ref_C_script \n ";

#$uploadlist_file = "uploadlist.txt";
$uploadlist_file = generate_uploadlist_filename();
print "File containing list of files to be uploaded: $uploadlist_file\n";
print "This file will be deleted when the program finishes running. \n";

if(!defined($opt_s)){
    $skim = 1;
}
else{
    $skim = $opt_s;
}

if(!defined($opt_m)){
    $merge = 0;
}
else{
    $merge = $opt_m;
}

if($merge && $skim){
    print "Cannot set merge and skim. Files to be merge must be skimmed previously.";
    print "Disabling skim. If files haven't been skimmed program will fail.";
    $skim = 0;
}

if(!defined($opt_u)){
    $upload = 1;
}
else{
    $upload = $opt_u;
}

my @skimmed_files = ();
fill_list_of_skimmed_files();

if(!defined($opt_c)){
    auto_choose_plots_and_upload();
}
else{
    #if a .C has been given as input will use
    #this file for all skimmed files

    if($merge){
	open(FILE,">$uploadlist_file") or die "Cannot open $uploadlist_file \n";
	print FILE join("\n",@skimmed_files);
	close FILE;
	print "Merge all runs within input run range. \n ./merge_runs.pl -o $uploadlist_file\n";
	system("./merge_runs.pl -o $uploadlist_file");
	open(FILE,"<$uploadlist_file") or die "Cannot open $uploadlist_file";
	 @skimmed_files = <FILE>;
	close FILE;
    }
    
    #This checks if the .png files already exist in the relevant Upload directory and asks for user input in that case. 
    #You can set default_for_preexisting_plots to stop it asking.
    #Then plots are made and uploaded.
    #Takes in run range or txt file listing skimmed files. Open file for explanation of format.
    $skimmed_file_list = join(" ",@skimmed_files);
    print "Running runUpload.pl -c $opt_c -i $config_file $skimmed_file_list \n";
    system("./runUpload.pl -c $opt_c -i $config_file $skimmed_file_list");
    print "Removing uploadlist file $uploadlist_file \n ";
    system("rm $uploadlist_file") if(-e $uploadlist_file);
    print "Done \n";
}

sub fill_list_of_skimmed_files
{
    if($skim){
	#this program lists all dqm files which haven't yet been skimmed and puts then in runlist.txt
	#You need to set the directory of the input dqm files and the primary dataset
	print "Getting list of files to be skimmed\n";
	system("./getNewDQMRuns.pl -i $config_file -o $uploadlist_file");
	
	#this program has not changed, you need to set the directory containing dqm files
	print "Beginning skim of files\n";
	system("./getsummary.pl -i $config_file runlist.txt");

	open(FILE,"<$uploadlist_file") or die "Cannot open $uploadlist_file";
	 @skimmed_files = <FILE>;
	close FILE;

	if(!$upload){
	    print "Files have been skimmed and upload option = 0. Quitting\n";
	    die;
	}
    }
    elsif(!$skim and $upload){
	#need to get list of files to upload somehow
	if(defined($opt_r)){
	    @skimmed_files = get_skim_files_from_runs($opt_r);
	    #print "@skimmed_files \n";
	}
	elsif(defined($opt_f)){
	    @skimmed_files = get_skim_files_from_file($opt_f);
	}
	elsif(@ARGV){
	    @skimmed_files = @ARGV;
	}
	else{
	    print "Error: Cannot determine which files you want to upload. \n";
	    print "Either skim new files or input run range,skimmed file names, or $uploadlist_file \n";
	    die;
	} 
    }
    elsif(!$skim and !$upload){
	    print "Error: You don't want to skim or upload? Quitting. \n";
	    die;
    }
}

sub auto_choose_plots_and_upload
{
    open(FILE,">$uploadlist_file") or die "Cannot open $uploadlist_file \n";
    print FILE join("\n",@skimmed_files);
    close FILE;
    
    if($merge){
	print "Merge all runs within input run range. \n ./merge_runs.pl -o $uploadlist_file \n ";
	system("./merge_runs.pl -o $uploadlist_file");
	open(FILE,"<$uploadlist_file") or die "Cannot open $uploadlist_file";
	 @skimmed_files = <FILE>;
	close FILE;
    }
    
    print "Creating .C files automatically\n";
    print "./plot_macro_generator.pl -i $config_file $uploadlist_file\n";
    system("./plot_macro_generator.pl -i $config_file $uploadlist_file");

    print "Running runUpload.pl for auto generated .C files\n";
    my $use_C_script = $ref_C_script;
    $use_C_script =~ s/.C/___RUN__.C/;
    print "./runUpload.pl -f $uploadlist_file -c $use_C_script -i $config_file\n";
    system("./runUpload.pl -f $uploadlist_file -c $use_C_script -i $config_file");

    if($merge){	
	#There should be only one file in the list. 
	#If there are more than one the command wont work.
	$skimmed_file_list = join("",@skimmed_files);
	if((-e $skimmed_file_list) && ($skimmed_file_list =~ m/to/)){
	    print "Deleting merged file $skimmed_file_list. \n";
	    system("rm $skimmed_file_list");
	}
	else{
	    print "Merged file $skimmed_file_list was not deleted. \n Might want to delete manually.\n";
	}
    }

}
 


sub get_skim_files_from_file
{
    my($infile) = @_;
    @sfiles = ();
    open(FILE,"<$infile");
    while($line = <FILE>){
	push(@sfiles,$1) if($line =~ m/(skimmed.*\.root)/);
	
    }
    close FILE;

    return @sfiles;
}

sub get_skim_files_from_runs
{
    my($runs) = @_;
    @sfiles = ();


    if($runs =~ m/,/){
	foreach $run(split(/\s*,\s*/, $runs)){
	    push(@sfiles,"$skim_path_prefix$skims_path/skimmedFourVector_run$run.root"); 
	}
    }
    elsif($runs =~ m/(\d+)-(\d+)/){
	$min = $1;
	$max = $2;
	if($skims_path =~ m/castor\/cern.ch/){
	    @all_skims = `nsls $skims_path/`;

	}
	else{
	    @all_skims = `ls $skims_path/skimmed*.root`;
	}
	print "Find runs between $min and $max\n";
	foreach $file(@all_skims){
	    if($file =~ /run(\d+)/){
		$run = $1;
		if($run >= $min and $run <= $max){
		    print "To be processed: $skim_path_prefix$skims_path/skimmedFourVector_run$run.root\n";
		    push(@sfiles,"$skim_path_prefix$skims_path/skimmedFourVector_run$run.root"); 
		}
	    }
	}
    }
    else{
#	print "S and U: here: skimmedFourVector_run$runs.root\n";
	if($skims_path =~ m/castor\/cern.ch/){
	    my $exitcode = system("nsls $skims_path/skimmedFourVector_run$runs.root");
	    die "ERROR: failed at listing skimmed files.\n nsls $skims_path/skimmedFourVector_run$runs.root\n Exit code: $exitcode \n" if($exitcode >> 8);
	    push(@sfiles,"$skim_path_prefix$skims_path/skimmedFourVector_run$runs.root"); 

	}
	else{
	    if(-e "$skims_path/skimmedFourVector_run$runs.root"){
		push(@sfiles,"$skims_path/skimmedFourVector_run$runs.root"); 
	    }
	}
    }

    return @sfiles;
}

#Generate the name of the file which will list the root files to be skimmed and/or uploaded.
#Generating names this way allows the program to be run in parallel in the same directory.  
sub generate_uploadlist_filename
{
    my $date = sprintf("%04d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3]);
    my $file_name = $date."_0_uploadlist.txt";
    my $file_to_check = "$file_name";
    if(-e "$file_to_check"){
	$date = sprintf("%04d%02d%02d_%02d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3], (localtime)[2], (localtime)[1], (localtime)[0]);
	$file_name = $date."_uploadlist.txt";
    }
    
    return $file_name;
}
