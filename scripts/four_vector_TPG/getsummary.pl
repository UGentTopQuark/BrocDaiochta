#!/usr/bin/perl
use Getopt::Std;
require ConfigReader;

getopts("i:");
my $config_file = 'example_config.cfg';
$config_file = $opt_i if(defined($opt_i));

my %script_setup = ();
our %configuration = ();
ConfigReader::read_config_file($config_file, \%configuration);
%script_setup = %{$configuration{'configuration'}};

$DQM_path = $script_setup{'path_to_DQM_files'};
$output_skims_path = $script_setup{'path_to_skims'};
$PD =  $script_setup{'primary_dataset'};
$PD_DQMfile =  $script_setup{'DQM_primary_dataset'};

$PD_DQMfile = $PD if($PD_DQMfile eq "");

$default_for_rm_dqm_file_path = "";

$dqm_files = $ARGV[0] if($ARGV[0]);
open(FILE, "<$dqm_files");
while($line = <FILE>){
    if($line =~ m/_R0+([0-9]+)__$PD_DQMfile/){
	$run = $1;
	#in case line contains path, we want only dqm file
	if($line =~ m/(.*\/)?(DQM_V.*root)/){
	    $dqm_file = $2;
	}
 	else{
	    next;
	} 
	my $skim_file = "skimmedFourVector_run$run.root";

	if($DQM_path =~ m/castor\/cern.ch/){
	    $dqm_file = "rfio:$DQM_path/$dqm_file";
	    #system("rfcp $DQM_path/$dqm_file ./$dqm_file");
	}else{
	    print "Copying $DQM_path/$dqm_file to ./$dqm_file \n";
	    system("cp $DQM_path/$dqm_file ./$dqm_file");
	}
	print "Skimming $dqm_file \n";
	system("root -l -b -q skimDirectory.C\\(\\\"$dqm_file\\\",\\\"$run\\\"\\)");

	print "Moving $skim_file to $output_skims_path";
	if($output_skims_path =~ m/castor\/cern.ch/){
	    system("rfcp $skim_file $output_skims_path/$skim_file");
	    system("rm $skim_file");
	}else{
	    sysmte("mv $skim_file $output_skims_path/$skim_file");
	}
	

	if(!($DQM_path =~ m/castor\/cern.ch/)){
	    #If the dqm file is really a path check with user if they really want to delete it
	    #This is in case the $dqm_file might actually be the path to the original file
	    if($default_for_rm_dqm_file_path eq "" and $dqm_file =~ m/\//){
		check_rm_dqm_file($dqm_file);
	    }
	    elsif($default_for_rm_dqm_file_path ne "" and $dqm_file =~ m/\//){
		command_rm_dqm_file($dqm_file,$default_for_rm_dqm_file_path)
		}
	    else{
		system("rm $dqm_file");
	    }
	}
    }
}

sub check_rm_dqm_file
{
    my ($dqm_path) = @_;

    print "Are you sure you want to delete $dqm_path.\n";
    print " [y/n/yr/nr]: ";
    my $command = <STDIN>;
    command_rm_dqm_file($dqm_path,$command);

}

sub command_rm_dqm_file
{
    my ($dqm_path,$command) = @_;
    if($command =~ m/r/){
	$default_for_rm_dqm_file_path = $command;
    }
    if($command =~ m/\s*y\s*/ or $command =~ m/\s*yr\s*/){
	system("rm $dqm_path");
    }
    elsif(!($command =~ m/\s*n\s*/ or $command =~ m/\s*nr\s*/)){
	print "Invalid input $command. Try again. \n";
	$default_for_rm_dqm_file_path = "";
	check_rm_dqm_file($dqm_path);
    }
}
