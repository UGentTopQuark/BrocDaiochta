#!/usr/bin/perl

require RPC::XML;
require RPC::XML::Client;
use Getopt::Std;
use JSON -support_by_pp;

die "usage: $0 <run number> <trigger path>" unless(@ARGV == 2);

$lower_run = $ARGV[0];
$upper_run = $ARGV[0];
$trigger_path = $ARGV[1];

$cli = RPC::XML::Client->new('http://pccmsdqm04.cern.ch/runregistry/xmlrpc');
$resp = $cli->send_request('ReferenceTable.workspaces');

$all_good = "{cmpCastor} = 'GOOD' and {cmpCsc} = 'GOOD' and {cmpDt} = 'GOOD' and {cmpEcal} = 'GOOD' and {cmpEs} = 'GOOD' and {cmpHcal} = 'GOOD' and {cmpHlt} = 'GOOD' and {cmpL1t} = 'GOOD' and {cmpPix} = 'GOOD' and {cmpRpc} = 'GOOD' and {cmpStrip} = 'GOOD'";
@runs = split(/,\s*/, $output);
$resp = $cli->send_request('DataExporter.export','RUN', 'GLOBAL','xml_datasets', "{runNumber} >= $lower_run and {runNumber} <= $upper_run and {groupName} like 'Collisions%' and {datasetName} = '/Global/Online/ALL'");

#print (ref $resp);
$output = $$resp;
print $output;
my %hlt_keys = ();
my $run;
foreach my $line (split(/\n/, $$resp)){
	$run = $1 if($line =~ m/<RUN_NUMBER>(\d+)<\/RUN_NUMBER>/);
	$hlt_keys{$run} = $1 if($line =~ m/<RUN_HLTKEY>(.*)<\/RUN_HLTKEY>/);
}

if(keys %hlt_keys == 0){
	die "No valid run / trigger path combination. Exiting...\n";
}

while(my ($run, $key) = each %hlt_keys){
	print "run: $run hlt_key: $key\n";
	system("edmConfigFromDB --orcoff --format summary.ascii --paths $trigger_path --configName $key");
}
