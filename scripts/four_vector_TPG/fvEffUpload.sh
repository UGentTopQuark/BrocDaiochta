#!/bin/bash
# HLT DQM Web Gallery script
# Author: Christos Lazaridis
# Modified: Will Parker
#
# Expects two arguments: an input directory and a target directory

if [ $# -ne 2 ]
then
  echo ${0} expects exactly two arguments: A source directory and a target directory
  exit 10
fi

if [ ! -d ${1} ]
then
  echo Directory ${1} does not seem to exist!
  exit 5
fi

if [ ! -d ${2} ]
then
  echo Directory ${2} does not seem to exist!
  exit 5
fi

genSubIndex ()
{
  pushd ${1} > /dev/null

  rm -f thumb-*

  RUNNUMBER=`echo ${2} | awk 'BEGIN { FS = "/" } ; { print $NF }'`

  echo Generating plots for ${subDir}

  echo "<html> <head> <title>DQMOffline FourVector Plots</title></head><body> " > index.html
  echo "<h1>DQMOffline FourVector ${subDir} Plots<br /> RUN #${RUNNUMBER:3} </h1> " >> index.html

  for img in indv_*.png ; do
    echo Resizing and generating thumbnail for $img...
    convert -scale 360 $img thumb-$img
    mogrify -scale 1280 $img
    echo "<a href=\"$img\"><img src=\"thumb-$img\"></a> " >> index.html
  done
echo "<br />" >> index.html
  for img in ceff*.png ; do
    echo Preparing slides
    mogrify -scale 1280 $img
    echo "<a href=\"$img\"><Font Size=+2>Download Slide</font></a> " >> index.html
  done

  echo "<br />Webpage generated on: " `date ` >> index.html

  echo "</body></html>" >> index.html

  popd > /dev/null
}

genRunIndex ()
{
  pushd ${1} > /dev/null

  rm -f thumb-*

  RUNNUMBER=`echo ${1} | awk 'BEGIN { FS = "/" } ; { print $NF }'`

  echo Generating links for Run \#${RUNNUMBER:3}

  echo "<html> <head> <title>DQMOffline FourVector Plots</title></head><body> " > index.html
  echo "<h1>DQMOffline FourVector Plots<br /> RUN #${RUNNUMBER:3} </h1> " >> index.html

    #echo Adding Summary...
    #echo "<a href=\"Summary/index.html\"><img src=\"Summary.png\" ><br /></a>" >> index.html
##    cat "Summary/description.txt" >> index.html
    #echo "<br />" >> index.html

    echo Adding Electrons...
    echo "<a href=\"Electrons/index.html\"><img src=\"Electrons.png\" ></a>" >> index.html
#    cat "Electrons/description.txt" >> index.html
#    echo "<br />" >> index.html

    echo Adding Muons...
    echo "<a href=\"Muons/index.html\"><img src=\"Muons.png\" ><br /></a>" >> index.html
 #   cat "Muons/description.txt" >> index.html
    echo "<br />" >> index.html

    echo Adding Photons...
    echo "<a href=\"Photons/index.html\"><img src=\"Photons.png\" ></a>" >> index.html
#    cat "Photons/description.txt" >> index.html
#    echo "<br />" >> index.html

    echo Adding Jets...
    echo "<a href=\"Jets/index.html\"><img src=\"Jets.png\" ><br /></a>" >> index.html
#    cat "Jets/description.txt" >> index.html
    echo "<br />" >> index.html

  echo "<br />Webpage generated on: " `date ` >> index.html

  echo "</body></html>" >> index.html

  popd > /dev/null
}

genMainIndex()
{
  currDir=`pwd`
  pushd ${1} > /dev/null
  

  echo "<html> <head> <title>DQMOffline FourVector Plots</title></head><body> " > index.html

  echo "<h1>DQMOffline FourVector Plots</h1> " >> index.html

  for dir in run* ; do
    echo Adding ${dir}...
    echo "HLT Run #${dir:3} : </a>" >> index.html
    if  [ -d "${dir}/Muons" ]; then
    echo "<a href=\"run${dir:3}/Muons/index.html\">  Muons  </a>" >> index.html
    fi
    if  [ -d "${dir}/Electrons" ]; then
    echo "<a href=\"run${dir:3}/Electrons/index.html\">  Electrons  </a>" >> index.html
    fi
    if  [ -d "${dir}/Photons" ]; then
    echo "<a href=\"run${dir:3}/Photons/index.html\">  Photons  </a>" >> index.html
    fi
    if  [ -d "${dir}/Jets" ]; then
    echo "<a href=\"run${dir:3}/Jets/index.html\">  Jets  </a>" >> index.html
    fi
    echo "<br />" >> index.html
    #echo "<a href=\"run${dir:3}/Muons/index.html\">  Muons  </a> <a href=\"run${dir:3}/Electrons/index.html\">  Electrons  </a><a href=\"run${dir:3}/Photons/index.html\">  Photons  </a><a href=\"run${dir:3}/Jets/index.html\">  Jets  </a><a href=\"run${dir:3}/Summary/index.html\">  Summary  </a><br />" >> index.html
    #echo "<a href=\"run${dir:3}/Muons/index.html\">  Muons  </a> <a href=\"run${dir:3}/Electrons/index.html\">  Electrons  </a><a href=\"run${dir:3}/Photons/index.html\">  Photons  </a><a href=\"run${dir:3}/Jets/index.html\">  Jets  </a><br />" >> index.html

    cat "$dir/description.txt" >> index.html
    echo "<br />" >> index.html
  done

  echo "<br />Webpage generated on: " `date ` >> index.html

  echo "</body></html>" >> index.html

  popd > /dev/null
}

NEWPLOTS=false
echo Executing...
echo `date`
echo Checking for new runs...

for inputDir in `ls -1 ${1}`
do
  if [ "$(ls -A ${1}/${inputDir})" ]
  then
    echo Found ${1}/${inputDir}

#    genRunIndex ${1}/${inputDir}

for subDir in `ls -1 ${1}/${inputDir}`
do
  if [ "$(ls -A ${1}/${inputDir}/${subDir})" ]
  then
    echo Found ${1}/${inputDir}/${subDir}

    genSubIndex ${1}/${inputDir}/${subDir} ${1}/${inputDir}

    NEWPLOTS=true
  fi
done

    echo Moving to ${2}
    #rm -r ${2}/${inputDir}
    mv ${1}/${inputDir} ${2}

    NEWPLOTS=true
  fi
done

if [ ${NEWPLOTS} == "true" ]
then
  genMainIndex ${2}
  echo Updated main index.
fi

echo Done.
