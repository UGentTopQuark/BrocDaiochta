#!/usr/bin/perl

use JSON -support_by_pp;

my $file = "Cert_136033-149442_7TeV_Dec22ReReco_Collisions10_JSON_v3.txt";

open(FILE, "<$file");
my $json_line = <FILE>;
close(FILE);

my $json = new JSON;
#my $json_text = $json->allow_nonref->utf8->relaxed->escape_slash->loose->allow_singlequote->allow_barekey->decode($json_line);
my %json_hash = %{$json->allow_nonref->utf8->relaxed->escape_slash->loose->allow_singlequote->allow_barekey->decode($json_line)};

foreach $key(sort keys %json_hash){
	print "$key\n";
}
