#!/usr/bin/perl

require RPC::XML;
require RPC::XML::Client;
use Getopt::Std;
use JSON -support_by_pp;

getopts("r:");

if($opt_r){
	my @run_range = split(/\s*-\s*/, $opt_r);
	if(@run_range == 2){
		$lower_run = $run_range[0];
		$upper_run = $run_range[1];
	}else{
		die "no valid run range given, exiting...\n";
	}
}else{
	die "please specify run range $0 -r <lower run>-<upper_run>\n";
}

$cli = RPC::XML::Client->new('http://pccmsdqm04.cern.ch/runregistry/xmlrpc');
$resp = $cli->send_request('ReferenceTable.workspaces');

$all_good = "{cmpCastor} = 'GOOD' and {cmpCsc} = 'GOOD' and {cmpDt} = 'GOOD' and {cmpEcal} = 'GOOD' and {cmpEs} = 'GOOD' and {cmpHcal} = 'GOOD' and {cmpHlt} = 'GOOD' and {cmpL1t} = 'GOOD' and {cmpPix} = 'GOOD' and {cmpRpc} = 'GOOD' and {cmpStrip} = 'GOOD'";
$resp = $cli->send_request('RunDatasetTable.export','GLOBAL','csv_run_numbers', "{runNumber} >= $lower_run and {runNumber} <= $upper_run and {groupName} = 'Collisions10' and $all_good");
$output = $$resp;
@runs = split(/,\s*/, $output);
$resp = $cli->send_request('DataExporter.export','RUNLUMISECTION', 'GLOBAL','json', "{runNumber} >= $lower_run and {runNumber} <= $upper_run and {groupName} = 'Collisions10'");

#print (ref $resp);
$output = $$resp;

my $json = new JSON;
my $json_text = $json->allow_nonref->utf8->relaxed->escape_slash->loose->allow_singlequote->allow_barekey->decode($output);

my $good_json = "";
$good_json .= "{";
foreach my $run (@runs){
	$good_json .= "\"$run\":";
	$good_json .= JSON->new->utf8->encode($json_text->{$run});
	$good_json .= ",";
}
chop $good_json;
$good_json .= "}";
if($good_json eq "}"){
	die "WARNING: no valid runs in run range\n";
}
print "$good_json\n";
#print "original: $output\n";

#print $output;
#@runs = split(/,\s*/, $output);
#@runs = split(/,\s*/, $output);
#foreach(@runs){
#	print "$_\n";
#}
