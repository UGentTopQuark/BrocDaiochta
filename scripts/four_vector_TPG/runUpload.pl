#!/usr/bin/perl
use Getopt::Std;
require ConfigReader;

$verbose = 1;
$default_for_preexisting_plots = "or";
$default_for_preuploaded_dirs = "yr"; 


##############Usage####################
#./runUpload.pl path/skimmed_run666678.root
#./runUpload.pl path/skimmed_run775303.root path/skimmed_run237804.root
#./runUpload.pl -r 145678-156789
#./runUpload.pl -r 143567
#./runUpload.pl -r 123456,163547,394837
#./runUpload.pl -f uploadlist.txt #file contains list of path/skimmed.root files 
#Can also give config file as input e.g.
#./runUpload.pl -r 143567 -c fvEffPlotsPublish_v2_static.C
######################################
@skim_files = ();
getopts("r:f:c:i:");

my $config_file = 'example_config.cfg';
$config_file = $opt_i if(defined($opt_i));
print "RunUpload: config file: $config_file \n";

my %script_setup = ();
our %configuration = ();
ConfigReader::read_config_file($config_file, \%configuration);
%script_setup = %{$configuration{'configuration'}};

$skims_path = $script_setup{'path_to_skims'};
$PD =  $script_setup{'primary_dataset'};
my $skim_path_prefix = "";
$skim_path_prefix = "rfio:" if($skims_path =~ m/castor\/cern.ch/ and !($skims_path =~ m/rfio:/));

if(defined($opt_r)){
    @skim_files = get_skim_files_from_runs($opt_r);
    print "@skim_files \n";
}
elsif(defined($opt_f)){
    @skim_files = get_skim_files_from_file($opt_f);
}
elsif(@ARGV){
    @skim_files = @ARGV;
}

foreach my $file_name (@skim_files){

    print "Running over: $file_name \n";
    
    do 'find_triggers.cfg';
    
    my %directories = ();
    
    my $run_number = $1 if($file_name =~ /run(\d+(to\d+)?)/);

    #check if run has already been uploaded
    next if (check_preexisting_dir("~/www/$PD/run$run_number") eq "skip");

    my $fvEff_script = "";
    #if config wasn't given as input check which of the defaults you want to use.
    if(!defined($opt_c)){
	my @split_runs = split(/to/, $run_number);
	
	my $trigger_menu_change = 147196;
	
	if(@split_runs == 1){
	    $fvEff_script = $fvEff_script_old if($run_number < $trigger_menu_change);
	    $fvEff_script = $fvEff_script_new if($run_number >= $trigger_menu_change);
	}elsif(@split_runs == 2){
	    if($split_runs[0] < $trigger_menu_change &&
	       $split_runs[1] < $trigger_menu_change){
		$fvEff_script = $fvEff_script_old;
	    }elsif(
		   ($split_runs[0] > $trigger_menu_change) &&
		   ($split_runs[1] > $trigger_menu_change)){
		$fvEff_script = $fvEff_script_new;
	    }else{ die "ERROR: run range includes trigger menu change: $trigger_menu_change\n"; }
	}else{ die "ERROR: cannot identify run number: $run_number\n"; }
    }
    else{
	print "Option c = $opt_c \n" ;
	$fvEff_script = $opt_c;	
	$fvEff_script =~ s/__RUN__/$run_number/;
    }
    
    my @images = <*.png>;
    if(scalar(@images) > 0){
	print "WARNING: png files exist.Moving to trash_png \n`";
	`mkdir trash_png` if!(-d "trash_png");
	system("mv *.png trash_png/");
    }
    if(!(-e $fvEff_script)){
	 print "Error $fvEff_script does not exist.Exiting\n";
	 die;
    }
    #Merged files are kept locally.
    if($run_number =~ m/to/){
	$skim_path_prefix = "";
	$skims_path = "./";
    }

    print "Running: $skim_path_prefix$skims_path/skimmedFourVector_run$run_number.root using $fvEff_script \n";
    system("root -l -b -q $fvEff_script\\(\\\"$skim_path_prefix$skims_path/skimmedFourVector_run$run_number.root\\\",\\\"$run_number\\\",\\\"DummyPD\\\",0,0\\)");
    #`root -l -b -q $fvEff_script\\(\\"skimmedFourVector_run$run_number.root\\",\\"$run_number\\",\\"DummyPD\\",0,0\\)`;
 
    print "Make upload dir : Upload \n" if($verbose and not -d "Upload");
    `mkdir Upload` if!(-d "Upload");
    print "Make run dir: Upload/run$run_number \n" if($verbose and !(-d "Upload/run$run_number"));
    `mkdir Upload/run$run_number` if!(-d "Upload/run$run_number");
    
   # print "Found Muon $1 \n" if($selected_triggers =~ /(_Mu)/);
   # $directories{"Muons"} = $1 if($selected_triggers =~ /(_Mu)/);
   # $directories{"Photons"} = $1 if($selected_triggers =~ /(_Photon)/);
   # $directories{"Electrons"} = $1 if($selected_triggers =~ /(_Ele)/);
   # $directories{"Jets"} = $1 if($selected_triggers =~ /(_Jet)/);

    #print "Found Muon $1 \n" if($selected_triggers =~ /(_Mu)/);
    $plots_created = join(" ",`ls *.png`);
    $directories{"Muons"} = "Mu" if($plots_created =~ m/Mu/);
    $directories{"Photons"} = "Photon" if($plots_created =~ m/Photon/);
    $directories{"Electrons"} = "Ele" if($plots_created =~ m/Ele/);
    $directories{"Jets"} = "Jet" if($plots_created =~ m/Jet/);
    
    print "Found plots: $plots_created \n";
    
    while(my ($dir,$id) = each %directories){
	#print "dir: $dir id: $id  \n" if($verbose);
	if(!(-d "Upload/run$run_number/$dir")){
	    `mkdir Upload/run$run_number/$dir`;
	}else{
	    check_preexisting_plots("Upload/run$run_number/$dir",$id);
	}
	print "Moving *$id*.png to Upload/run$run_number/$dir \n" if($verbose);
	system("mv *$id*.png Upload/run$run_number/$dir");	
    }

    #Upload directory
    system("./fvEffUpload.sh Upload ~/www/$PD");
    system("fs sa ~/www/$PD/run$run_number webserver:afs read");
}

sub check_preexisting_dir
{
    my ($dir_path) = @_;
    if($dir_path =~ m/~/){
	$dir_path =~ s/~//;    
	$dir_path = $ENV{"HOME"}.$dir_path;
    }
    if(-d $dir_path){
	if($default_for_preuploaded_dirs eq ""){
	    print "Directory $dir_path already exists \n";
	    print "Remove old directory then recreate png files? \n";
	    print "Selecting no will skip this skimmed file. \n";
	    print "Add r to command if you don't want to be asked again. \n";
	    print "[y/n/yr/nr]: ";
	    my $command = <STDIN>;
	    if($command =~ m/y/ and not $command =~ m/n/){
		print "Removing $dir path";
		system("rm -r $dir_path") ;
		$default_for_preuploaded_dirs = $command if($command =~ m/r/);
		return "";
	    }elsif($command =~ m/n/ and not $command =~ m/y/){
		$default_for_preuploaded_dirs = $command if($command =~ m/r/);
		return "skip";
	    }
	    else{ 
		print "Invalid input $command. Try again. \n";
		$default_for_preuploaded_dirs = "";
		check_preexisting_dir($dir_path);
		
	    }
	}
	else{
	    $command = $default_for_preuploaded_dirs;
	    if($command =~ m/y/ and not $command =~ m/n/){
		print "Removing $dir path";
		system("rm -r $dir_path");
		return "";
	    }elsif($command =~ m/n/ and not $command =~ m/y/){
		return "skip";
	    }
	    else{ 
		print "Invalid input $command. Try again. \n";
		$default_for_preuploaded_dirs = "";
		check_preexisting_dir($dir_path);
		
	    }
	}
    }
}


sub check_preexisting_plots
{
	my ($dir_path,$id) = @_;
    #Check if there are png files in the directory
     
    my @images = <$dir_path/*$id*.png>;
    if(scalar(@images) > 0){
	print "$id*.png files exist in $dir_path \n";
	if($default_for_preexisting_plots eq ""){
	    print "What would you like to do:\n";
	    print "Overwrite. Use only new images. [o] \n";
	    print "Append. Add only images not already present. [a] \n";
	    print "Repeat command. Set input as default. Do not ask again. [r] \n";
	    print "[o/a/or/ar]: ";
	    my $command = <STDIN>;
	    command_preexisting_plots($dir_path,$id, $command);
	}
	else{
	    command_preexisting_plots($dir_path,$id, $default_for_preexisting_plots);
	}
    }
    #else{
	#print "No preexisting png files.\n";
    #}

}

sub command_preexisting_plots
{
    my ($dir_path, $id, $command) = @_;
    my $valid_command = 0;
    #print "command : $command";
    $default_for_preexisting_plots = $command if($command =~ /r/ and $default_for_preexisting_plots eq "");
    if($command =~ m/\s*a\s*/ or $command =~ m/\s*ar\s*/){
	`cp $dir_path/*$id*.png ./`;
	$valid_command = true;
    }
    if($command =~ m/\s*o\s*/ or $command =~ m/\s*or\s*/){ 
	`rm $dir_path/*$id*.png`;
	$valid_command = true;
    }
    if(!$valid_command){
	print "ERROR: Invalid Command: $command.Try again. \n "; 
	$default_for_preexisting_plots = "";
	check_preexisting_plots($dir_path, $id);
    }
	
}

sub get_skim_files_from_file
{
    my($infile) = @_;
    @sfiles = ();
    open(FILE,"<$infile");
    while($line = <FILE>){
	push(@sfiles,$1) if($line =~ m/(.*skimmed.*\.root)/);
	
    }
    close FILE;

    return @sfiles;
}

sub get_skim_files_from_runs
{
    my($runs) = @_;
    @sfiles = ();

    if($runs =~ m/,/){
	foreach $run(split(/\s*,\s*/, $runs)){
	    push(@sfiles,"$skim_path_prefix$skims_path/skimmedFourVector_run$run.root"); 
	}
    }
    elsif($runs =~ m/(\d+)-(\d+)/){
	if($skims_path =~ m/castor\/cern.ch/){
	    @all_skims = `nsls $skims_path/skimmed*.root`;

	}
	else{
	    @all_skims = `ls $skims_path/skimmed*.root`;
	}
	$min = $1;
	$max = $2;
	foreach $file(@all_skims){
	    if($file =~ /run(\d+)/){
		$run = $1;
		if($run >= $min and $run <= $max){
		    push(@sfiles,"$skim_path_prefix$file"); 
		}
	    }
	}
    }
    else{
	if($skims_path =~ m/castor\/cern.ch/){
	    my $exitcode = system("nsls $skims_path/skimmedFourVector_run$runs.root");
	    die "ERROR: failed at listing skimmed files." if($exitcode >> 8);
	    push(@sfiles,"$skim_path_prefix$skims_path/skimmedFourVector_run$runs.root"); 

	}
	else{
	    if(-e "$skims_path/skimmedFourVector_run$runs.root"){
		push(@sfiles,"$skim_path_prefix$skims_path/skimmedFourVector_run$runs.root"); 
	    }
	}
    }
	
    return @sfiles;
}
