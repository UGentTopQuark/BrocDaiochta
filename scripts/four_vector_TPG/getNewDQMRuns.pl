#!/usr/bin/perl
use Getopt::Std;
require ConfigReader;

getopts("i:o:");
my $config_file = 'example_config.cfg';
$config_file = $opt_i if(defined($opt_i));
my $uploadlist_file = "uploadlist.txt";
$uploadlist_file = $opt_o if(defined($opt_o));


my %script_setup = ();
our %configuration = ();
ConfigReader::read_config_file($config_file, \%configuration);
%script_setup = %{$configuration{'configuration'}};


$DQM_path = $script_setup{'path_to_DQM_files'};
$output_skims_path = $script_setup{'path_to_skims'};;

$PD = $script_setup{'primary_dataset'};
$PD_DQMfile =  $script_setup{'DQM_primary_dataset'};
$PD_DQMfile = $PD if($PD_DQMfile eq "");

$dqmfile = "dqm_filelist.txt";
$skimfile = "skimmed_filelist.txt";

`rm $dqmfile` if(-e $dqmfile);
`rm $skimfile` if(-e $skimfile);

if($DQM_path =~ m/castor\/cern.ch/){
    `nsls $DQM_path/ > $dqmfile`;
}else{
    `ls $DQM_path/DQM*.root > $dqmfile`; 
}
if($output_skims_path =~ m/castor\/cern.ch/){
    `nsls $output_skims_path/ > $skimfile`;
}else{ 
    `ls $output_skims_path/skimmed*.root > $skimfile`;  
}



%skimmed_runs = ();

open(FILE, "< $skimfile");
while(my $line = <FILE>){
    if($line =~ m/run([0-9]+)/){
	$run = $1;
	#print "Run from skim: $run \n";
	$skimmed_runs{$run} = 1;
    }
}
close FILE;

@files_to_process = ();
@new_skim_files = ();

open(FILE, "< $dqmfile");
while($line = <FILE>){
    if($line =~ m/_R0+([0-9]+)__$PD_DQMfile/){
	$run = $1;
	print "Run from dqm: $run \n";
	print $line;
	unless(exists $skimmed_runs{$run}){
	    if($line =~ m/(DQM_V.*root)/){
		$dqm_file = $1; 	
		print "DQM file : $dqm_file \n";
		push(@files_to_process,$dqm_file);
		if($output_skims_path =~ m/castor\/cern.ch/){
		    push(@new_skim_files,"rfio:$output_skims_path/skimmedFourVector_run$run.root");
		}
		else{
		    push(@new_skim_files,"$output_skims_path/skimmedFourVector_run$run.root");
		}
	    }
	}
    }
}
close FILE;

open(FILE, "> runlist.txt") or die "can't open runlist.txt";
foreach my $line (@files_to_process){
    print "WRITING: $line \n";
    print FILE "$line \n"; 

}
close FILE;

open(FILE, "> $uploadlist_file") or die "can't open $uploadlist_file";
foreach my $line (@new_skim_files){
    print "to be created: $line \n";
    print FILE "$line \n"; 

}
close FILE;
