#!/bin/bash
while read line
do
NUMBER="${line:14:6}"
TYPE=`echo $line | awk 'BEGIN { FS = "__" } ; { print $2 }'`
#root -l -b -q fvEffPlotsPublish_v2.C\(\"skimmedFourVector_run$NUMBER.root\",\"$NUMBER\",\"DummyPD\",0,0\)
root -l -b -q fvEffPlotsPublish_v2_test.C\(\"skimmedFourVector_run$NUMBER.root\",\"$NUMBER\",\"DummyPD\",0,0\)
echo "root -l -b -q fvEffPlotsPublish_v2_test.C\(\"skimmedFourVector_run$NUMBER.root\",\"$NUMBER\",\"DummyPD\",0,0\)"
mkdir Upload
mkdir Upload/run$NUMBER
#mkdir Upload/run$NUMBER/Summary
#mkdir Upload/run$NUMBER/Electrons
mkdir Upload/run$NUMBER/Muons
#mkdir Upload/run$NUMBER/Photons
#mkdir Upload/run$NUMBER/Jets
#mv ceff*.png Upload/run$NUMBER

#mv *ceffSummary*.png Upload/run$NUMBER/Summary
#mv *ceff_Ele*.png Upload/run$NUMBER/Electrons
mv *ceff_Mu*.png Upload/run$NUMBER/Muons
#mv *ceff_Photon*.png Upload/run$NUMBER/Photons
#mv *ceff_Jet*.png Upload/run$NUMBER/Jets
#./fvEffUpload.sh Upload ~/www/$TYPE
#fs sa ~/www/$TYPE/run$NUMBER webserver:afs read
done < $1
