#!/usr/bin/perl -w

use strict;
use Getopt::Std;

our $opt_o;
getopts("o:");
my $file_list="uploadlist.txt";
$file_list = $opt_o if(defined($opt_o));
my $output_file_list=$file_list;

open(LISTFILE, "<$file_list") or die "can't open file: $file_list $!\n";
my @files_to_merge = <LISTFILE>;
close(LISTFILE);

sort @files_to_merge;

#get run numbers of first and last file
my $file = $files_to_merge[0];
$file =~ m/skimmedFourVector_run(\d+).root/;
my $lower_run = $1;
$file = $files_to_merge[@files_to_merge-1];
$file =~ m/skimmedFourVector_run(\d+).root/;
my $upper_run = $1;

my $outfile_name = "skimmedFourVector_run${lower_run}to${upper_run}.root";
print "outfile name: $outfile_name\n";

#remove endline characters after each file
foreach(@files_to_merge){chomp;}

my $files_to_merge_string = join(" ", sort @files_to_merge);

my $command = "hadd -f $outfile_name $files_to_merge_string";
print "$command\n";
`$command`;

open(LISTFILE, ">$output_file_list") or die "can't open file: $output_file_list $!\n";
print LISTFILE $outfile_name;
close(LISTFILE);
