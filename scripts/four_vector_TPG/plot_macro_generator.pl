#!/usr/bin/perl -w

use strict;
use Getopt::Std;
require ConfigReader;

our $opt_i;
getopts("i:");
my $config_file = 'example_config.cfg';
$config_file = $opt_i if(defined($opt_i));
print "Plot macro generator option i $opt_i\n";

die "usage: $0 <skimmed file list>" if(@ARGV != 1);

my $skimmed_files_list = $ARGV[0];

my %available_triggers = ();

my %script_setup = ();

# output mode:
# 0: create one config per run
# 1: combine runs
# (changes format of %available_triggers)
my $output_mode = 0;

our %configuration = ();
print "Plot macro generator config file: $config_file\n";
ConfigReader::read_config_file($config_file, \%configuration);
%script_setup = %{$configuration{'configuration'}};
delete($configuration{'configuration'});

read_available_triggers(\%available_triggers, $skimmed_files_list, $output_mode);
if($output_mode == 0){
	create_root_scripts(\%available_triggers, \%configuration, \%script_setup);
}elsif($output_mode == 1){
	get_prescales(\%available_triggers, \%configuration);
	#create_combined_script(\%available_triggers, \%configuration);
}

sub get_prescales
{
	my ($available_triggers, $configuration) = @_;

	my %triggers_in_run = ();

	# create list of triggers for each run
	foreach my $trigger (keys %$configuration){
		next if($trigger =~ m/global/);
		next unless(defined($available_triggers{$trigger}));
		my $ref_trig = $configuration{$trigger}{'reference_trigger'} ? $configuration{$trigger}{'reference_trigger'} : $configuration{'global'}{'reference_trigger'};
		my $print_mode = $configuration{$trigger}{'print_mode'} ? $configuration{$trigger}{'print_mode'} : $configuration{'global'}{'print_mode'};

		my @files = ();
		# find list of run-files
		foreach my $run (keys %{$available_triggers{$trigger}{$ref_trig}{$print_mode}}){
			push(@{$triggers_in_run{$run}}, $trigger);
		}
	}

	foreach my $run (keys %triggers_in_run){
		print "run: $run\n";
		my $trigger_list = join(",", @{$triggers_in_run{$run}});
		print "$trigger_list\n";
	}
}

sub create_combined_script
{
	my ($available_triggers, $configuration) = @_;
	open(LISTFILE, ">input_list.txt") or die "can't open file: $!";
	foreach my $trigger (keys %configuration){
		next if($trigger =~ m/global/);
		next unless(defined($available_triggers{$trigger}));
		my $ref_trig = $configuration{$trigger}{'reference_trigger'} ? $configuration{$trigger}{'reference_trigger'} : $configuration{'global'}{'reference_trigger'};
		my $print_mode = $configuration{$trigger}{'print_mode'} ? $configuration{$trigger}{'print_mode'} : $configuration{'global'}{'print_mode'};

		my @files = ();
		# find list of run-files
		foreach my $run (keys %{$available_triggers{$trigger}{$ref_trig}{$print_mode}}){
			push(@files, "skimmedFourVector_run${run}.root\n");
		}

		# print nominator and denominator histo for combination root script
		my $reference;
		if($ref_trig eq "-"){ $reference = ""; }
		else{ $reference = $ref_trig; }
		my $histo_name = "${trigger}_wrt_${reference}_offEtOff";
		print LISTFILE "HISTO: $trigger/$histo_name\n";
		foreach(@files){ print LISTFILE $_; }
		$histo_name = "${trigger}_wrt_${reference}_offEtOnOff";
		print LISTFILE "HISTO: $trigger/$histo_name\n";
		foreach(@files){ print LISTFILE $_; }
		$histo_name = "${trigger}_wrt_${reference}_offEtL1Off";
		print LISTFILE "HISTO: $trigger/$histo_name\n";
		foreach(@files){ print LISTFILE $_; }
	}
	close(LISTFILE);
}

sub print_trigger_section
{
	my ($run, $trigger, $ref_trigger, $print_mode, $axis_lable, $outscript, $frame_file_name) = @_;

	open(SEC_FRAME, "<$frame_file_name");
	my @section_frame = <SEC_FRAME>;
	close(SEC_FRAME);
	
	foreach my $line (@section_frame){
		$line =~ s/__TRIGGER__/$trigger/g;
		$line =~ s/__REFERENCE_TRIGGER__/$ref_trigger/g;
		$line =~ s/__AXIS_LABLE__/$axis_lable/g;

		print $outscript $line;
	}
}

sub create_root_scripts
{
	my ($available_triggers, $configuration, $script_setup) = @_;

	open(REF, "<".$script_setup->{'reference_script'});
	my @reference_cfg = <REF>;
	close(REF);
		
	foreach my $run (keys %$available_triggers){
		my $outfile_name = $script_setup->{'reference_script'};
		$outfile_name =~ s/\.C/_${run}.C/;
		print "printing run: $run, outfile: $outfile_name\n";
		open(OUTFILE, ">$outfile_name");

		foreach my $line(@reference_cfg){
			if(not $line =~ /\/\/ AUTO GENERATED SLIDES/){
				my $line_to_print = $line;

				my $initial_name = $script_setup->{'reference_script'};
				$initial_name =~ s/\.C//;
				$line_to_print =~ s/$initial_name/${initial_name}_$run/;

				print OUTFILE $line_to_print;
			}else{
				foreach my $trigger (keys %configuration){
					next if($trigger =~ m/global/);
					my $ref_trig = $configuration{$trigger}{'reference_trigger'} ? $configuration{$trigger}{'reference_trigger'} : $configuration{'global'}{'reference_trigger'};
					my $print_mode = $configuration{$trigger}{'print_mode'} ? $configuration{$trigger}{'print_mode'} : $configuration{'global'}{'print_mode'};
					my $axis_lable = $configuration{$trigger}{'axis_lable'} ? $configuration{$trigger}{'axis_lable'} : $configuration{'global'}{'axis_lable'};
					my $print_absolute = $configuration{$trigger}{'print_absolute'} ? $configuration{$trigger}{'print_absolute'} : $configuration{'global'}{'print_absolute'};
					if(defined($available_triggers{$run}{$trigger}{$ref_trig}{$print_mode})){
						print_trigger_section($run, $trigger, $ref_trig, $print_mode, $axis_lable, \*OUTFILE, $script_setup->{'eff_frame_file'});
						if($print_absolute =~ /true/i){
							print_trigger_section($run, $trigger, $ref_trig, $print_mode, $axis_lable, \*OUTFILE, $script_setup->{'abs_frame_file'});
						}
					}
				}
			}
		}

		close OUTFILE;
	}
}

sub read_available_triggers
{
	my ($available_triggers, $skimmed_files_list, $output_mode) = @_;

	print "Skimmed files list: $skimmed_files_list \n";
	print "root -q -l printContent.C(\"$skimmed_files_list\")\n";
	#my @lines = system("root -q -l printContent.C\\(\\\"$skimmed_files_list\\\"\\)"); 
	my @lines = `root -q -l printContent.C\\(\\"$skimmed_files_list\\"\\)`; 
	print "Finished with printContent\n";

	my $ref_trigger = "";	
	my $XvsY = "";
	my $run_number = "";
	my $trigger = "";
	foreach my $line (@lines){
		next unless($line =~ /^Trigger:/ or $line =~ /^Run: / or $line =~ /^Histo: /);
		chomp($line);
		$run_number = $1 and next if($line =~ /^Run:.*run(\d+(to\d+)?)\.root/);
		$trigger = $1 and next if($line =~ /^Trigger:\s*(.*)/);
		if($line =~ /^Histo:.*_wrt_(.*)_([\d\w]+)$/){
			if($1 eq ""){
				$ref_trigger = "-";
			}else{
				$ref_trigger = $1;
			}
			$XvsY = $2;
		}
		if($output_mode == 0){
			$available_triggers{$run_number}{$trigger}{$ref_trigger}{$XvsY} = 1;
		}elsif($output_mode == 1){
			$available_triggers{$trigger}{$ref_trigger}{$XvsY}{$run_number} = 1;
		}
	}
}
