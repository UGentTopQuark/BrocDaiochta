#!/usr/bin/perl

$file_name = '\"DQM_V0001_R000147221__Mu__Run2010B-v2__FVHarvested.root\"';
@lines = `root -q -l printContent.C\\($file_name\\)`; 

do 'find_triggers.cfg';

$ref_script = "fvEffPlotsPublish_v2.C";
$out_script = "fvEffPlotsPublish_v2_AUTO.C";

%available_triggers = ();

foreach $line (@lines){
	next unless($line =~ /^Trigger:/ or $line =~ /^Run: / or $line =~ /^Histo: /);
	$run_number = $1 and next if($line =~ /^Run:\s*(\d+)/);
	$trigger = $1 and next if($line =~ /^Trigger:\s*(.*)/);
	$ref_trigger = $1 and $XvsY = $2 if($line =~ /^Histo:.*_wrt_(.*)_([\d\w]+)$/);
	$available_triggers{$run_number}{$trigger}{$ref_trigger}{$XvsY} = 1;
}

open(REF, "<$ref_script");
@reference_cfg = <REF>;
close(REF);

open(OUTSCRIPT, ">$out_script");
foreach $line(@reference_cfg){
	if(not $line =~ /\/\/ AUTO GENERATED SLIDES/){
		print OUTSCRIPT $line;
	}else{
		foreach my $run (keys %available_triggers){
			next unless($run =~ /$selected_runs/ and not $run =~ /$vetoed_runs/);
			foreach my $trigger (keys %{$available_triggers{$run}}){
				next unless($trigger =~ /$selected_triggers/ and not $run =~ /$vetoed_triggers/);
				foreach my $ref_trigger (keys %{$available_triggers{$run}{$trigger}}){
					next unless($ref_trigger =~ /$selected_reference_triggers/ and not $ref_trigger =~ /$vetoed_reference_triggers/);
					foreach my $XvsY (keys %{$available_triggers{$run}{$trigger}{$ref_trigger}}){
						next unless($XvsY =~ /$selected_XvsY/ and not $XvsY =~ /$vetoed_XvsY/);
						print "$run $trigger $ref_trigger $XvsY\n";
						my $axis_label = get_axis_lable($trigger);
					}
				}
			}
		}
	}
}
close(OUTSCRIPT);

sub get_axis_lable
{
	my ($trigger) = @_;

	foreach $x_axis_lable (keys %x_axis_lables){
		if($trigger =~ /$x_axis_lable/){
			return $x_axis_lables{$x_axis_lable};
		}
	}

	return "";
}
