#!/usr/bin/perl -w

use strict;

package ConfigReader;

our @EXPORT_OK = qw(read_config_file);

sub read_config_file{
	my ($config_file, $configuration) = @_;
	open(FILE,"<$config_file") or die "could not open config file: $! \n";
	my @lines = <FILE>;
	close(FILE);

	my $trigger_found = 0;
	my @triggers=();
	my $reference_trigger="";
	my $print_mode="";
	my $run_range="";
	foreach my $line (@lines){
		$line =~ s/#.*//;	# remove comments
		$line =~ s/\"//g;	# remove quotes
		$line =~ s/\s+$//g;	# remove whitespaces
		$line =~ s/^\s+//g;	# remove whitespaces
		$line =~ s/\'//g;	# remove quotes
		chomp($line);		# \n

		next if($line eq "");	# ignore empty lines
		$trigger_found = 1 if($line =~ m/\[(.*)\]/);

		next unless($trigger_found);

		if($line =~ m/^\s*\[(.*)\]\s*$/){
			@triggers = split(/\s*,\s*/, $1);
		}elsif($line =~ m/([\w_]+)\s*=\s*(.*)/){	# (parameter)=(value)
			foreach my $trigger (@triggers){
				$configuration->{$trigger}{$1}=$2;
			}
		}else{
			die "ERROR: unrecognised line in config, exiting...: $line";
		}
	}
}

1;
