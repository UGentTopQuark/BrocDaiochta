#!/usr/bin/perl

die "usage: $0 <GC workdirs>" unless(@ARGV > 0);
my @workdirs = @ARGV;

@efficiencies=();
push(@efficiencies, "\n\n");

foreach $workdir(@workdirs){
	run_command("./file_mover.pl $workdir/output", "moving files");
	run_command("./sort_files.pl $workdir/output", "sorting files");
	get_filter_efficiencies("$workdir/output", \@efficiencies);
}

foreach(@efficiencies){
	print;
}

sub get_filter_efficiencies
{
	my ($workdir, $efficiencies) = @_;

	opendir(WORKDIR, $workdir);
	while(defined(my $dir = readdir(WORKDIR))){
		next unless($dir =~ /(.*)_Eire/);
		$dataset = $1;
		push(@$efficiencies, "dataset: $dataset\n");
		push(@$efficiencies, get_efficiency("$workdir/$dir"));
		push(@$efficiencies, "\n");
	}
	closedir(WORKDIR);
}

sub get_efficiency
{
	my ($dirname) = @_;

	# determine if PAT or TtPreselection efficiency
	$ttpreselection = 0;
	opendir(DIR, "$dirname");
	while(defined(my $file = readdir(DIR))){
		next unless($file =~ /cmssw_out.*\.txt/);
		open(FILE, "<$dirname/$file");
		while(<FILE>){
			$ttpreselection = 1 if(/\-\-\-\-\-\-\-\-\-\-\-STATISTICS\-\-\-\-\-\-\-\-\-\-\-/);	
		}
		close(FILE);
		last if($ttpreselection);
	}
	closedir(DIR);

	if($ttpreselection){
		run_command("./file_combiner.pl $dirname", "combining files");
		@output = run_command("./calculate_filter_eff.pl $dirname", "calculating TtPreselection filter efficinecy");
	}else{
		@output = run_command("./pat_filter_eff.pl $dirname", "calculating PAT filter efficiency");
	}

	my $found_total = 0;
	my @efficiency_summary = ();
	foreach $line(@output){
		next unless($found_total or $line =~ /\-\-\- TOTAL \-\-\-/);
		$found_total = 1 and next if($line =~ /\-\-\- TOTAL \-\-\-/);
		push(@efficiency_summary, $line);
	}

	return @efficiency_summary;
}

sub run_command
{
	my ($command, $printout) = @_;
	print "$printout ($command)...\n";
	return `$command`;
}
