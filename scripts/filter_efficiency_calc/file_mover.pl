#!/usr/bin/perl


my $inputdir = $ARGV[0];
my $outputdir = $inputdir;

opendir(DIR, $inputdir);
while(defined(my $subdir = readdir(DIR))){
	next unless($subdir =~ m/job_(\d+)/);
	my $job_nr=$1;
	opendir(SUBDIR, "$inputdir/$subdir");
	print "processing $subdir...\n";
	while(defined(my $file = readdir(SUBDIR))){
		print "processing $file...\n";
		next unless($file =~ m/^cmssw\.log\.gz/);
		`cp $inputdir/$subdir/$file $outputdir/$file`;
		`cd $outputdir; gunzip $file`;
		`mv $outputdir/cmssw.log $outputdir/cmssw_out_$job_nr.txt`;
	}
	close(SUBDIR);
}
close(DIR);
