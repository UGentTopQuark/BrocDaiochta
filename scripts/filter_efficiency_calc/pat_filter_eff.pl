#!/usr/bin/perl

$dirname = $ARGV[0];

die "usage: $0 <dirname>\n" unless(defined($dirname));

$passed = 0;
$overall = 0;

opendir(DIR, $dirname) or die "can't opendir $dirname: $!";
while (defined($file = readdir(DIR))) {
	#next unless($file =~ /CMSSW_x_.*\.stderr/);
	next unless($file =~ /cmssw_out.*\.txt/);
	print "processing $file ...\n";
	my $numbers_ref = get_numbers($dirname, $file);
	$passed += $numbers_ref->{'passed'};
	$overall += $numbers_ref->{'overall'};
}
closedir(DIR);

print "--- TOTAL ---\n";
#print "overall events: $overall\n";
#print "passed events: $passed\n";
print "filter efficiency: ".$passed/$overall."\n";

sub get_numbers
{
	my($dirname, $file) = @_;

	open(FILE, "<$dirname/$file") or die "can't opendir $dirname/$file: $!";
	@content = <FILE>;
	close(FILE);

	my %numbers = ();
	$numbers{'passed'} = 0;
	$numbers{'overall'} = 0;

	$step = 0;
	my $overall = 0;
	my $passed = 0;
	foreach $line(@content){
		if(($step == 0) && $line =~ m/TrigReport ---------- Path\s+Summary ------------/){
			$step = 1;
#			print "step 0...\n";
		}elsif(($step == 1) && $line =~ m/TrigReport\s+Trig\s+Bit#\s+Run\s+Passed\s+Failed\s+Error Name/){
			$step = 2;
#			print "step 1...\n";
		}elsif(($step == 2) && $line =~ m/TrigReport\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+p/){
#			print "step 2...\n";
			$step = 3;
			$numbers{'overall'} = $3;
			$numbers{'passed'} = $4;
			print "--- file: $dirname/$file ---\n";
			print "overall events: ".$numbers{'overall'}."\n";
			print "passed events: ".$numbers{'passed'}."\n";
			print "filter-efficiency: ".$numbers{'passed'}/$numbers{'overall'}."\n";
			print "--- end file: $dirname/$file ---\n";
			last;
		}elsif($step == 1 || $step == 2 || $step == 3){
			$step = -1;	
		}
	}

	return \%numbers;
}
