#!/usr/bin/perl

die "usage: $0 <output dir>" unless(@ARGV > 0);
$output_dir = $ARGV[0];

%directories=();
opendir(OUTDIR, "$output_dir");
while (defined($file = readdir(OUTDIR))) {
	next unless($file =~ /cmssw_out.*\.txt/);
	open(FILE, "<$output_dir/$file");
	$found_dir = "";
	while(<FILE>){
		if(/\/([\w\d\_\-]+)_job_\d+.*\.root/ || /\/store\/mc\/[\w\d\_\-]+\/([\w\d\_\-]+)\//){
			$found_dir = $1."_Eire";
			unless($directories{$found_dir}){
				print "creating directory $output_dir/$found_dir...\n";
				`mkdir $output_dir/$found_dir`;
			}
			$directories{$found_dir} = 1;
			print "mv $output_dir/$file $output_dir/$found_dir...\n";
			last;
		}
	}
	close(FILE);
	`mv $output_dir/$file $output_dir/$found_dir` if($found_dir);
	print "WARNING: could not identify file: $output_dir/$file\n" unless($found_dir);
}
closedir(OUTDIR);
