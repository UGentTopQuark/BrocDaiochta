#!/usr/bin/perl

$dirname = $ARGV[0];

open(FILE, "<$dirname/CMSSW_1000.stdout");
@lines = <FILE>;
close(FILE);

$mu_pass = -1;
$e_pass = -1;
$mu_rej = -1;
$e_rej = -1;
$oth_pass = -1;
$oth_rej = -1;

foreach $line(@lines){
	if($line =~ /signal_accepted_13:\s+(\d+)/){
		$mu_pass = $1;
	}
	if($line =~ /signal_rejected_13:\s+(\d+)/){
		$mu_rej = $1;
	}
	if($line =~ /signal_accepted_11:\s+(\d+)/){
		$e_pass = $1;
	}
	if($line =~ /signal_rejected_11:\s+(\d+)/){
		$e_rej = $1;
	}
	if($line =~ /accepted:\s+(\d+)/){
		$oth_pass = $1;
	}
	if($line =~ /rejected:\s+(\d+)/){
		$oth_rej = $1;
	}
}

print "DATASET: $dirname\n";
print "--- TOTAL ---\n";
if(not($mu_pass == 0 && $mu_rej == 0)){
	$mu_eff = $mu_pass/($mu_pass+$mu_rej);
	$e_eff = $e_pass/($e_pass+$e_rej);
	$mu_backg_eff = ($e_pass+$oth_pass)/($e_pass+$oth_pass+$e_rej+$oth_rej);
	$e_backg_eff = ($mu_pass+$oth_pass)/($mu_pass+$oth_pass+$mu_rej+$oth_rej);
	$backg_eff = ($mu_pass+$e_pass+$oth_pass)/($mu_pass+$e_pass+$oth_pass+$mu_rej+$e_rej+$oth_rej);

	$overall = $mu_pass+$mu_rej+$e_pass+$e_rej+$oth_pass+$oth_rej;
	#print "overall events: $overall\n";

	print "mu filter efficiency: $mu_eff\n";
	print "mu background filter efficiency: $mu_backg_eff\n";
	print "e filter efficiency: $e_eff\n";
	print "e background filter efficiency: $e_backg_eff\n";
	print "ttbar background filter efficiency: $backg_eff\n";
}else{
	$eff = $oth_pass/($oth_pass+$oth_rej);
	$overall = $oth_pass+$oth_rej;
	#print "overall events: $overall\n";
	print "filter efficiency: $eff\n";
}
