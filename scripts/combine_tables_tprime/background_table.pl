#!/usr/bin/perl

%eventnumbers = ();

@masses = (250, 300, 350, 400);

$infile = 'slides.tex';
$infile = $ARGV[0] if($ARGV[0]);
print "processing file $infile\n";
open(FILE, "<$infile");
@file = <FILE>;
close(FILE);

$cur_mass = 0;
foreach $line(@file){
	if($line !~ /^\%/ && $line =~ m/\(other\, (\d+)\)/g){
		$mass = $1;
		print "mass: $1\n";
	}

#	if($mass != 250){
#		print "skipping, mass: $mass\n";
#		next;
#	}

	if($line =~
	m/^\s*([\<\$\\\,\{\}_A-Za-z\+\-\d\(\)]+)\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*/){
		$prefix = $1;
		$error = sqrt($7*$7+$9*$9+$11*$11+$13*$13+$15*$15+$17*$17+$19*$19);
#		print "1: $1 2: $2 3: $3 4: $4 5: $5 6: $6 7: $7 8: $8 9: $9 10: $10\n";
#		print "11: $11 12: $12 13: $13 14: $14 15: $15 16: $16 17: $17 18: $18 19: $19\n";
		$value = $6+$8+$10+$12+$14+$16+$18;
		$string = sprintf "\& \$$value \\pm %.2f\$ \\\\\n", $error;
		$eventnumbers{$mass}{$prefix} = $string;
		$line =~
		s/^\s*([\<\$\\\,\{\}_A-Za-z\+\-\d\(\)]+)\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&/\1 \&/;
		$line =~ s/\\\\/$string/;
		print "$line";
		#print "$1:\t\t$string";
	}elsif($line =~
	m/^\s*([\<\$\\\,\{\}_A-Za-z\+\-\d\(\)]+)\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$\<\s*([\d\.]+)\s*\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*/){
		# for error estimation in QCD

		$prefix = $1;
		$error = sqrt($7*$7+$9*$9+$11*$11+$12*$12+$14*$14+$16*$16+$18*$18);
#		print "1: $1 2: $2 3: $3 4: $4 5: $5 6: $6 7: $7 8: $8 9: $9 10: $10\n";
#		print "11: $11 12: $12 13: $13 14: $14 15: $15 16: $16 17: $17 18: $18 19: $19\n";
		$value = $6+$8+$10+$13+$15+$17;
		$string = sprintf "\& \$$value \\pm %.2f\$ \\\\\n", $error;
		$eventnumbers{$mass}{$prefix} = $string;
		$line =~
		s/^\s*([\<\$\\\,\{\}_A-Za-z\+\-\d\(\)]+)\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&/\1 \&/;
		$line =~ s/\\\\/$string/;
		print "$line";
		#print "$1:\t\t$string";
	}
	if($line =~
	m/^\s*([\<\$\\\,\{\}_A-Za-z\+\-\d\(\)]+)\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*/g)
	{
		$prefix = $1;
		$value = $4+$5+$6+$7+$8+$9+$10;
		$string = sprintf "\& \$$value\$ \\\\\n";
		$eventnumbers{$mass}{$prefix} = $string;
		$line =~
		s/^\s*([\<\$\\\,\{\}_A-Za-z\+\-\d\(\)]+)\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&/\1 \&/;
		$line =~ s/\\\\/$string/;
		print "$line";
		#print "$1:\t\t$string";
	}
}

@cutsets = keys %{ $eventnumbers{350} };
#foreach $cutset(@cutsets){
#	foreach $mass(@masses){
#		while ( my ($key, $value) = each(%eventnumbers) ) {
#			print "valueif($mass == 250);
#	
#		}
#	}
#}
