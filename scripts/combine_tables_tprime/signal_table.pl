#!/usr/bin/perl

%eventnumbers = ();

@masses = (250, 300, 350, 400);
@cutsets =
('s0','s1','s2','s3','s4','s5','s6+','s6','s6-','$H_T\,<\,400\,\mathrm{GeV}$',
'$H_T\,<\,350\,\mathrm{GeV}$', '$H_T\,<\,300\,\mathrm{GeV}$',
'$H_T\,<\,400\,\mathrm{GeV}$b', '$H_T\,<\,350\,\mathrm{GeV}$b',
'$H_T\,<\,300\,\mathrm{GeV}$b', 's3+btag');

$infile = 'slides.tex';
$infile = $ARGV[0] if($ARGV[0]);
open(FILE, "<$infile");
@file = <FILE>;
close(FILE);

$cur_mass = 0;
foreach $line(@file){
	next if($line =~ /^\%/);

	if($line !~ /^\%/ && $line =~ m/\(other\, (\d+)\)/g){
		$mass = $1;
		print "mass: $1\n";
	}

	if($line =~
	m/^\s*([\<\$\\\,\{\}_A-Za-z\+\-\d\(\)]+)\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&/g){
		$prefix = $1;
		$error = sqrt($3*$3+$5*$5);
		$value = $2+$4;
		$string = sprintf "\& \$$value \\pm %.2f\$ ", $error;
		$eventnumbers{$mass}{$prefix} = $string;
		print "$1:\t\t$string\n";
	}elsif($line =~
	m/^\s*([\<\$\\\,\{\}_A-Za-z\+\-\d\(\)]+)\s*\&\s*\$\s*\<\s*([\d\.]+)\s*\$\s*\&\s*\$([\d\.]+)\s*\\pm\s*([\d\.]+)\$\s*\&/g){
		$prefix = $1;
		$error = sqrt($2*$2+$4*$4);
		$value = $3;
		$string = sprintf "\& \$$value \\pm %.2f\$ ", $error;
		$eventnumbers{$mass}{$prefix} = $string;
		print "$1:\t\t$string\n";
	}else{
	#	print "didn't match: $line";
	}
	if($line =~ m/^\s*([a-z\+\-\d\(\)]+)\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&\s*\$?([\d\.]+)\s*\$?\s*\&/g)
	{
		$prefix = $1;
		$error = sqrt($3*$3+$5*$5);
		$value = $2+$3;
		$string = sprintf "\& \$$value\$ ";
		$eventnumbers{$mass}{$prefix} = $string;
		print "$1:\t\t$string\n";
	}
}

foreach $cutset(@cutsets){
	print "$cutset ";
	foreach $mass(@masses){
		print $eventnumbers{$mass}{$cutset};
	}
	print "\\\\\n";
	print "\\hline\n";
}
