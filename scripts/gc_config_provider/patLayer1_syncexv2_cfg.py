# This is an example PAT configuration showing the usage of PAT on minbias data

# Starting with a skeleton process which gets imported with the following line
from PhysicsTools.PatAlgos.patTemplate_cfg import *

from PhysicsTools.PatAlgos.tools.coreTools import *

## global tag for data
process.GlobalTag.globaltag = cms.string('START36_V9::All')

# turn off MC matching for the process
removeMCMatching(process, ['All'])

# add pf met
#from PhysicsTools.PatAlgos.tools.metTools import *
#removeMCMatching(process, ['All'])
#addPfMET(process, 'PF')

from PhysicsTools.PatAlgos.tools.cmsswVersionTools import *

process.load("ElectroWeakAnalysis.WENu.simpleEleIdSequence_cff")

process.patElectronIDs = cms.Sequence(process.simpleEleIdSequence)
process.makePatElectrons = cms.Sequence(process.patElectronIDs*process.patElectronIsolation*process.patElectrons)

process.patElectrons.addElectronID = cms.bool(True)
process.patElectrons.electronIDSources = cms.PSet(
    simpleEleId95relIso= cms.InputTag("simpleEleId95relIso"),
    simpleEleId90relIso= cms.InputTag("simpleEleId90relIso"),
    simpleEleId85relIso= cms.InputTag("simpleEleId85relIso"),
    simpleEleId80relIso= cms.InputTag("simpleEleId80relIso"),
    simpleEleId70relIso= cms.InputTag("simpleEleId70relIso"),
    simpleEleId60relIso= cms.InputTag("simpleEleId60relIso"),
    simpleEleId95cIso= cms.InputTag("simpleEleId95cIso"),
    simpleEleId90cIso= cms.InputTag("simpleEleId90cIso"),
    simpleEleId85cIso= cms.InputTag("simpleEleId85cIso"),
    simpleEleId80cIso= cms.InputTag("simpleEleId80cIso"),
    simpleEleId70cIso= cms.InputTag("simpleEleId70cIso"),
    simpleEleId60cIso= cms.InputTag("simpleEleId60cIso"),
    eidTight = cms.InputTag("eidTight"),
    eidLoose = cms.InputTag("eidLoose"),
    eidRobustTight = cms.InputTag("eidRobustTight"),
    eidRobustLoose = cms.InputTag("eidRobustLoose"),
)

## uncomment this line to run on an 35X input sample
run36xOn35xInput(process)
## uncomment the following lines to add jets from a
## 35X input sample
addJetCollection35X(process,cms.InputTag('ak5CaloJets'),
                 'AK5', 'Calo',
                 doJTA        = True,
                 doBTagging   = False,
                 jetCorrLabel = ('AK5', 'Calo'),
                 doType1MET   = True,
                 doL1Cleaning = True,                 
                 doL1Counters = False,
                 genJetCollection=cms.InputTag("sisCone5GenJets"),
                 doJetID      = True,
                 jetIdLabel   = "ak5"
                 )

##uncomment the following lines to switch the jet
## collection from a 35X input sample
switchJetCollection35X(process,cms.InputTag('ak5CaloJets'),
                 doJTA        = True,
                 doBTagging   = True,
                 #jetCorrLabel = None,
                 jetCorrLabel = ('AK5', 'Calo'),
                 doType1MET   = True,
                 genJetCollection=cms.InputTag("sisCone5GenJets"),
                 doJetID      = True
                 )

process.preselection = cms.EDFilter("TtPreselection",
    electronTag = cms.untracked.InputTag("selectedPatElectrons"),
    tauTag      = cms.untracked.InputTag("selectedPatTaus"),
    muonTag     = cms.untracked.InputTag("selectedPatMuons"),
    jetTag      = cms.untracked.InputTag("selectedPatJets"),
    photonTag   = cms.untracked.InputTag("selectedPatPhotons"),
    metTag      = cms.untracked.InputTag("patMETs"),
    semiLepTag      = cms.untracked.InputTag("ttSemiLepEvent"),
    hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypGenMatch:Key"),
    HLTriggerResults = cms.InputTag( 'TriggerResults','','REDIGI' ),
    datasetName = cms.untracked.string("Preselection")
)


# get the 900 GeV jet corrections
## from PhysicsTools.PatAlgos.tools.jetTools import *
## switchJECSet( process, "Summer09_7TeV_ReReco332")

#process.load('JetMETCorrections.Configuration.DefaultJEC_cff')

# run ak5 gen jets
#from PhysicsTools.PatAlgos.tools.cmsswVersionTools import *
#run33xOnReRecoMC( process, "ak5GenJets")

# Add PF jets
## addJetCollection(process,cms.InputTag('ak5PFJets'),
##                  'AK5', 'PF',
##                  doJTA        = False,
##                  doBTagging   = False,
##                  jetCorrLabel = ('AK5','PF'),
##                  doType1MET   = False,
##                  doL1Cleaning = False,                 
##                  doL1Counters = False,
##                  genJetCollection=cms.InputTag("ak5GenJets"),
##                  doJetID      = False
##                  )

# require physics declared
## process.physDecl = cms.EDFilter("PhysDecl",
##     applyfilter = cms.untracked.bool(True)
## )

## # require scraping filter
## process.scrapingVeto = cms.EDFilter("FilterOutScraping",
##                                     applyfilter = cms.untracked.bool(True),
##                                     debugOn = cms.untracked.bool(False),
##                                     numtrack = cms.untracked.uint32(10),
##                                     thresh = cms.untracked.double(0.2)
##                                     )


## # configure HLT
## process.load('L1TriggerConfig.L1GtConfigProducers.L1GtTriggerMaskTechTrigConfig_cff')
## process.load('HLTrigger/HLTfilters/hltLevel1GTSeed_cfi')
#process.hltLevel1GTSeed.L1TechTriggerSeeding = cms.bool(True)
#process.hltLevel1GTSeed.L1SeedsLogicalExpression = cms.string('0 AND (40 OR 41) AND NOT (36 OR 37 OR 38 OR 39)')

# switch on PAT trigger
#from PhysicsTools.PatAlgos.tools.trigTools import switchOnTrigger
#switchOnTrigger( process )

## process.primaryVertexFilter = cms.EDFilter("GoodVertexFilter",
##                                            vertexCollection = cms.InputTag('offlinePrimaryVertices'),
##                                            minimumNDOF = cms.uint32(4) ,
##                                            maxAbsZ = cms.double(15), 
##                                            maxd0 = cms.double(2) 
##                                            )

# Select jets
#process.selectedPatJets.cut = cms.string('pt > 15. & abs(eta) < 3.0')
#process.countPatJets.minNumber      = 1
#process.selectedPatJetsAK5PF.cut = cms.string('pt > 2 & abs(eta) < 3.0')
process.patMuons.usePV = cms.bool(False)
process.patElectrons.usePV = cms.bool(False)

# Add the files 
readFiles = cms.untracked.vstring()
secFiles = cms.untracked.vstring()

#readFiles.extend( [
#'__FILE_NAMES__'
#        ] );
#process.source.fileNames = readFiles

# let it run

#print
#print "============== Warning =============="
#print "technical trigger filter:    DISABLED"
#print "physics declare bit filter:  DISABLED"
#print "primary vertex filter:       DISABLED"

process.p = cms.Path(
#    process.hltLevel1GTSeed*
#    process.scrapingVeto*
#    process.physDecl*
#    process.primaryVertexFilter*
    process.patDefaultSequence *
    process.preselection
    )

# rename output file
process.out.fileName = cms.untracked.string('TopSkim.root')

# reduce verbosity
#process.MessageLogger.cerr.FwkReport.reportEvery = cms.untracked.int32(1000)

# process all the events
process.source = cms.Source("PoolSource",
 #   fileNames = cms.untracked.vstring(
 #       'file:///user/bklein/default_cmssw/CMSSW_3_5_6/src/grid-control/test.root'
 #   )
    fileNames = cms.untracked.vstring(
   # 'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_1_1.root',
   #                                   'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_2_2.root',
   #                                   'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_3_1.root',
   #                                   'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_4_1.root',
   #                                   'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_5_1.root',
   #                                   'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_6_1.root',
   #                                   'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_7_1.root',
   #                                   'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_8_1.root',
   #                                   'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_9_1.root',
                                      'dcap:////pnfs/iihe/cms/store/user/walsh/sync_ex_20100611/sync_ex_10_1.root'
 #			'file:/user/bklein/trigger_cmssw/CMSSW_3_6_1_patch4/src/analysers/TtPreselection/84872812-1F4B-DF11-8A07-00151796C158.root',
                                      )
)

#process.maxEvents.input = 100
#process.maxEvents.input = -1
process.options.wantSummary = True

from PhysicsTools.PatAlgos.patEventContent_cff import patEventContentNoCleaning
from PhysicsTools.PatAlgos.patEventContent_cff import patExtraAodEventContent
#from PhysicsTools.PatAlgos.patEventContent_cff import patTriggerEventContent
process.out.outputCommands = patEventContentNoCleaning
process.out.outputCommands += patExtraAodEventContent
#process.out.outputCommands += patTriggerEventContent
#process.out.outputCommands += [
#        'keep recoPFCandidates_particleFlow_*_*'
#        ]

import FWCore.ParameterSet.Config as cms
from IOMC.RandomEngine.RandomServiceHelper import RandomNumberServiceHelper

def customise_for_gc(process):
	try:
		maxevents = __MAX_EVENTS__
		process.maxEvents = cms.untracked.PSet(
			input = cms.untracked.int32(maxevents)
		)
	except:
		pass

	# Dataset related setup
	try:
		tmp = __SKIP_EVENTS__
		process.source = cms.Source("PoolSource",
			skipEvents = cms.untracked.uint32(__SKIP_EVENTS__),
			fileNames = cms.untracked.vstring(__FILE_NAMES__)
		)
		try:
			secondary = __FILE_NAMES2__
			process.source.secondaryFileNames = cms.untracked.vstring(secondary)
		except:
			pass
		try:
			lumirange = [__LUMI_RANGE__]
			process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange(lumirange)
		except:
			pass
	except:
		pass

	# Generator related setup
	try:
		if hasattr(process, "generator"):
			process.source.firstLuminosityBlock = cms.untracked.uint32(1+__MY_JOBID__)
	except:
		pass

	if hasattr(process, "RandomNumberGeneratorService"):
		randSvc = RandomNumberServiceHelper(process.RandomNumberGeneratorService)
		randSvc.populate()

	process.AdaptorConfig = cms.Service("AdaptorConfig",
		enable=cms.untracked.bool(True),
		stats = cms.untracked.bool(True),
	)

	return (process)

process = customise_for_gc(process)

# grid-control: https://ekptrac.physik.uni-karlsruhe.de/trac/grid-control
