import FWCore.ParameterSet.Config as cms

#-------------------------------------------------
# test cfg file for tqaflayer1 production from
# fullsim
#-------------------------------------------------
process = cms.Process("SkimPat")

process.load("FWCore.MessageLogger.MessageLogger_cfi")


## global tag for data
#process.GlobalTag.globaltag = cms.string('GR_R_35X_V7A::All')

# process all the events
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
            'dcap:///pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/TopTrig_SkimQCDPt15/QCD_Pt-15_7TeV-pythia6job_56_TopTrigSkim.root'
    )
#    fileNames = cms.untracked.vstring( __FILE_NAMES__ ),
#        skipEvents = cms.untracked.uint32( __SKIP_EVENTS__ )
)

process.countMuons = cms.EDFilter("PATCandViewCountFilter",
                                   minNumber = cms.uint32(1),
                                   maxNumber = cms.uint32(999999),
                                   src = cms.InputTag("filterMuons")
)

process.filterMuons = cms.EDProducer("CandViewSelector", cut = cms.string('pt > 5'), src = cms.InputTag("selectedPatMuons"))

process.out = cms.OutputModule("PoolOutputModule",
    verbose = cms.untracked.bool(True),
        dropMetaDataForDroppedData = cms.untracked.bool(True),
	    fileName = cms.untracked.string('TopTrigSkimPt5.root'),
	        #fileName = cms.untracked.string( __OUTFILE__ ),
		    dataset = cms.untracked.PSet(
		                dataTier = cms.untracked.string('USER'),
				filterName = cms.untracked.string('')
                    )
)

process.p = cms.Path(
	process.filterMuons *
	process.countMuons *
	process.out
)

#process.maxEvents = cms.untracked.PSet(
#	input = cms.untracked.int32(500)
#)

process.schedule = cms.Schedule(process.p)

import FWCore.ParameterSet.Config as cms
from IOMC.RandomEngine.RandomServiceHelper import RandomNumberServiceHelper

def customise_for_gc(process):
	try:
		maxevents = __MAX_EVENTS__
		process.maxEvents = cms.untracked.PSet(
			input = cms.untracked.int32(maxevents)
		)
	except:
		pass

	# Dataset related setup
	try:
		tmp = __SKIP_EVENTS__
		process.source = cms.Source("PoolSource",
			skipEvents = cms.untracked.uint32(__SKIP_EVENTS__),
			fileNames = cms.untracked.vstring(__FILE_NAMES__)
		)
		try:
			secondary = __FILE_NAMES2__
			process.source.secondaryFileNames = cms.untracked.vstring(secondary)
		except:
			pass
		try:
			lumirange = [__LUMI_RANGE__]
			process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange(lumirange)
		except:
			pass
	except:
		pass

	# Generator related setup
	try:
		if hasattr(process, "generator"):
			process.source.firstLuminosityBlock = cms.untracked.uint32(1+__MY_JOBID__)
	except:
		pass

	if hasattr(process, "RandomNumberGeneratorService"):
		randSvc = RandomNumberServiceHelper(process.RandomNumberGeneratorService)
		randSvc.populate()

	process.AdaptorConfig = cms.Service("AdaptorConfig",
		enable=cms.untracked.bool(True),
		stats = cms.untracked.bool(True),
	)

	return (process)

process = customise_for_gc(process)

# grid-control: https://ekptrac.physik.uni-karlsruhe.de/trac/grid-control
