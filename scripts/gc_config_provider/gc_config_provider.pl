#!/usr/bin/perl

my $create_directory = 0; # create output directory on SE?
#my $output_dir_path = 'srm://srm-cms.cern.ch:8443/srm/managerv2?SFN=/castor/cern.ch/user/b/bklein/topcom/trigger/7TeVSkims/muon/';
#my $output_dir_path = 'srm://maite.iihe.ac.be:8443/pnfs/iihe/cms/store/user/bklein/topcom/trigger/7TeVSkims/muon/';
my $output_dir_path = 'srm://maite.iihe.ac.be:8443/pnfs/iihe/cms/store/user/bklein/preselection_spring10_v1/';
my $mkdir_command = 'srmmkdir -2 -debug=true -streams_num=1';

#my $frame = 'skim_pat_gc.conf.frame';
#my $filename_mask = 'skim_PAT___DATASET__.conf';

#my $frame = 'create_pat_gc.conf.frame';
#my $filename_mask = 'create_PAT___DATASET__.conf';
my $frame = 'create_spring10_pat_gc.conf.frame';
my $filename_mask = 'create_PAT_spring10___DATASET__.conf';

my $config = 'gc_config_provider.cfg';
open(CONFIG, $config) or die "can't open config $config\n";
while(defined(my $line = <CONFIG>)){
	$line =~ s/\#.*//g;
	next unless($line =~ m/([\d\w]+)\s+([\/\w\d\-]+)/i);
	my $dataset = $1;
	my $dbsname = $2;
	my $current_filename = $filename_mask;

	# create directory on SE
	if($create_directory){
		my $command = "$mkdir_command $output_dir_path/$dataset";
		print "$command\n";
		system("$command");
	}

	# for Top.Com trigger
	my $run_numbers=""; # run numbers in GC format
	if($dataset =~ /TopTrig_SkimRun(.*)/){
		$run_numbers = $1;
		$run_numbers =~ s/^_//;
		$run_numbers =~ s/_/, /g;
	}

	# create new files and replace placeholders
	$this_directory = `pwd`;
	chomp $this_directory;
	$this_directory =~ s/.*src\///;	# make dir relative to CMSSW ENV
	$current_filename =~ s/__DATASET__/$dataset/g;
	open(OUT, ">$current_filename");
	open(FRAME, "<$frame");
	while(defined(my $frame_line = <FRAME>)){
		$frame_line =~ s/__THIS_DIR__/$this_directory/g;	
		$frame_line =~ s/__RUN_NUMBERS__/$run_numbers/g;	
		$frame_line =~ s/__DATASET__/$dataset/g;	
		$frame_line =~ s/__DBS_PATH__/$dbsname/g;	
		$frame_line =~ s/__SE_OUTPUT_DIR__/$output_dir_path/g;	
		print OUT $frame_line;
	}
	close(FRAME);
	close(OUT);

}
close(CONFIG);
