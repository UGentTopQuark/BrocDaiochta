# import skeleton process
from PhysicsTools.PatAlgos.patTemplate_cfg import *
from PhysicsTools.PatAlgos.tools.coreTools import *

## remove MC matching from the default sequence to make it run on real data
removeMCMatching(process, ['All'])

# filter for at least one reco muon with pt>5
process.countMuons = cms.EDFilter("PATCandViewCountFilter",
                                   minNumber = cms.uint32(1),
                                   maxNumber = cms.uint32(999999),
                                   src = cms.InputTag("filterMuons")
)

process.countElectrons = cms.EDFilter("PATCandViewCountFilter",
                                   minNumber = cms.uint32(1),
                                   maxNumber = cms.uint32(999999),
                                   src = cms.InputTag("filterElectrons")
)

process.filterMuons = cms.EDProducer("CandViewSelector", cut = cms.string('pt > 0.'), src = cms.InputTag("muons"))
process.filterElectrons = cms.EDProducer("CandViewSelector", cut = cms.string('pt > 0'), src = cms.InputTag("gsfElectrons"))

process.patMuons.usePV = False
process.patElectrons.usePV = False

############  require physics declared
process.load('HLTrigger.special.hltPhysicsDeclared_cfi')
process.hltPhysicsDeclared.L1GtReadoutRecordTag = 'gtDigis'

###########   require scraping filter
process.scrapingVeto = cms.EDFilter("FilterOutScraping",
									applyfilter = cms.untracked.bool(True),
									debugOn = cms.untracked.bool(False),
									numtrack = cms.untracked.uint32(10),
									thresh = cms.untracked.double(0.2)
									)

process.primaryVertexFilter = cms.EDFilter("GoodVertexFilter",
										   vertexCollection = cms.InputTag('offlinePrimaryVertices'),
										   minimumNDOF = cms.uint32(4) ,
										   maxAbsZ = cms.double(15),
										   maxd0 = cms.double(2)
										   )







process.p = cms.Path(
	process.scrapingVeto*
	process.hltPhysicsDeclared*
	process.primaryVertexFilter*
	process.filterMuons *
	process.countMuons *
#	process.filterElectrons *
#	process.countElectrons *
	process.patDefaultSequence
)

process.maxEvents.input = -1
#process.maxEvents.input = __MAX_EVENTS__
#process.source.skipEvents = cms.untracked.uint32( __SKIP_EVENTS__ )

#chosenFileNames = [ __FILE_NAMES__ ]

chosenFileNames = ['file:/user/bklein/trigger_cmssw/CMSSW_3_5_4/src/gc_config_provider/JetMETSkim.root',
 'file:/user/bklein/trigger_cmssw/CMSSW_3_5_4/src/gc_config_provider/JetMETSkim2.root',
 'file:/user/bklein/trigger_cmssw/CMSSW_3_5_4/src/gc_config_provider/JetMETSkim3.root',
 'file:/user/bklein/trigger_cmssw/CMSSW_3_5_4/src/gc_config_provider/JetMETSkim4.root',
  ]

outputFileName  = 'TopTrigSkim.root'

process.source.fileNames = cms.untracked.vstring(chosenFileNames)
process.out.fileName = cms.untracked.string(outputFileName)




# we don't need tag infos. as they need a lot of space, just remove them
process.patJets.addBTagInfo = cms.bool(False)

# define the output
process.source.inputCommands = cms.untracked.vstring("keep *", "drop *_MEtoEDMConverter_*_*")
process.out.outputCommands = ['keep *_selectedPatMuons_*_*',
                              'keep *_selectedPatJets_*_*',
                              'keep *_selectedPatElectrons_*_*',
                              'keep *_hltTriggerSummaryAOD_*_*',
                              'keep *_TriggerResults_*_*',
                              'keep *_offlinePrimaryVertices_*_*',
                              'keep *_genParticles_*_*',
                              'keep recoBeamSpot*_*_*_*'
                              ]


import FWCore.ParameterSet.Config as cms
from IOMC.RandomEngine.RandomServiceHelper import RandomNumberServiceHelper

def customise_for_gc(process):
	try:
		maxevents = __MAX_EVENTS__
		process.maxEvents = cms.untracked.PSet(
			input = cms.untracked.int32(maxevents)
		)
	except:
		pass

	# Dataset related setup
	try:
		tmp = __SKIP_EVENTS__
		process.source = cms.Source("PoolSource",
			skipEvents = cms.untracked.uint32(__SKIP_EVENTS__),
			fileNames = cms.untracked.vstring(__FILE_NAMES__)
		)
		try:
			secondary = __FILE_NAMES2__
			process.source.secondaryFileNames = cms.untracked.vstring(secondary)
		except:
			pass
		try:
			lumirange = [__LUMI_RANGE__]
			process.source.lumisToProcess = cms.untracked.VLuminosityBlockRange(lumirange)
		except:
			pass
	except:
		pass

	# Generator related setup
	try:
		if hasattr(process, "generator"):
			process.source.firstLuminosityBlock = cms.untracked.uint32(1+__MY_JOBID__)
	except:
		pass

	if hasattr(process, "RandomNumberGeneratorService"):
		randSvc = RandomNumberServiceHelper(process.RandomNumberGeneratorService)
		randSvc.populate()

	process.AdaptorConfig = cms.Service("AdaptorConfig",
		enable=cms.untracked.bool(True),
		stats = cms.untracked.bool(True),
	)

	return (process)

process = customise_for_gc(process)

# grid-control: https://ekptrac.physik.uni-karlsruhe.de/trac/grid-control
