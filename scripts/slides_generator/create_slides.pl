#!/usr/bin/perl

use Getopt::Std;

getopts("c:");

$config = 'slides.txt';
$outfile = 'slides.tex';
$frame = 'slides.frame';
$combine_evt_sel_dir = '/home/bklein/root/combine_evt_sel';

$config = $opt_c if(defined($opt_c));

my ($sec,$min,$hour,$mday,$mon,$year, $wday,$yday,$isdst) = localtime time;
$year += 1900;
$mon += 1;

my $date_string = "$mday/$mon/$year";

my @files_to_copy = ();
create_slides($config, $outfile, $frame, $date_string, \@files_to_copy);
copy_files(\@files_to_copy);

sub copy_files
{
	my ($files_to_copy_ref) = @_;
	for $file (@$files_to_copy_ref){
		$file =~ m/(.*)\/[\w_\.\d]+$/;
		my $dir = $1;
		print "mkdir -p fig/$dir\n" unless(-d $dir);
		print "cp $combine_evt_sel_dir/$file fig/$file\n";
	}
}

sub create_slides
{
	my ($config, $outfile, $slides_frame, $date_string, $files_to_copy_ref) = @_;
	
	open(FRAME, "<$slides_frame");
	open(SLIDES, ">$outfile");
	open(CONFIG, "<$config") or die "can't open: $config";
	while(defined(my $frame_line = <FRAME>)){
		if($frame_line =~ /--- SLIDES ---/){
			my $frame_title;
			my $previous_slide_exists = 0;
			while(defined($line = <CONFIG>)){
				next if($line !~ /\w|==/);
				if($line =~ /==\s*(.*)/){
					print_slide_end(SLIDES) if($previous_slide_exists);
					$frame_title = $1;
					print_slide_head($frame_title, SLIDES);
					$previous_slide_exists=1;
				}elsif($line =~ /(e|mu)\/(s|c|b)\/s\d+\/[\w\d_]+/){
					my @plots = split(/,/,$line);
					my $nplot = 1;
					my $width = 1./(scalar @plots);
					foreach $plot(@plots){
						$plot =~ s/\s*//g;
						next unless($plot =~ /(e|mu)\/(s|c|b)\/(s\d+)\/([\w\d_]+_)/);
						my $channel;
						my $plotmode;
						my $cutset;
						my $plotname;
						$channel = 'electron' if($1 eq 'e');
						$channel = 'muon' if($1 eq 'mu');

						$plotmode = '' if($2 eq 'c');
						$plotmode = 'signal_' if($2 eq 's');
						$plotmode = 'background_' if($2 eq 'b');

						$cutset = $3;
						$plotname = $4;
						$cutset =~ s/s(\d+)/\1_cutset/;

						my $file_name = $plotmode.$channel."_$cutset/$plotname.pdf";
						push(@$files_to_copy_ref, $file_name);
						print SLIDES "\t\\includegraphics[width=$width\\textwidth]{fig/$file_name}";
						if($nplot >= @plots){
							print SLIDES "\\\\";
						}else{
							print SLIDES "\%";
						}
						print SLIDES "\n";
						$nplot++;
					}
				}
			}
			print_slide_end(SLIDES) if($previous_slide_exists);
		}else{
			$frame_line =~ s/__DATE__/$date_string/g;
			print SLIDES $frame_line;
		}
	}
	close(CONFIG);
	close(SLIDES);
	close(FRAME);
}

sub print_slide_head
{
	my($frame_title, $fh) = @_;
	print $fh <<EOF;
\\begin{frame}\\MyLogo\\frametitle{$frame_title}
\\begin{center}
EOF
}

sub print_slide_end
{
	my($fh) = @_;

	print $fh <<EOF;
\\end{center}
\\end{frame}

EOF
}
