#!/usr/bin/perl

use Digest::Adler32;
use File::Find;
use File::Spec;
use Cwd;

my $user = `whoami`;
chomp $user;

if(@ARGV < 1){
	print "USAGE: $0 <root files to be processed>\n";
	print "eg.: $0 ~/$user/new_ntuples/*.root\n";
	exit;
}

my $lumi_provider_file = "LuminosityProvider/LuminosityProvider.cc";

print "Please enter integrated luminosity for dataset in 1/pb: ";
my $int_lumi = <STDIN>;
chomp $int_lumi;
die "wrong format of integrated luminosity, exiting..." unless($int_lumi =~ /[\d\.]+/);
print "Please enter dbs name for dataset: ";
my $dbs_name = <STDIN>;
chomp $dbs_name;
print "Please enter preselection details about this ntuple production or link to wiki [none]: ";
my $preselection = <STDIN>;
chomp $preselection;
$preselection = "<no preselection applied>" unless($preselection);
print "Please enter any further comments about this ntuple production [none]: ";
my $comments = <STDIN>;
chomp $comments;
$comments = "<no comments>" unless($comments);

# find all files specified
my @files = ();
find {
        by_depth => 1,
        no_chdir => 1,
        wanted   => sub{
		#directories
                if( !-l && -d _){
                } else {	#files
			my $abs_path = File::Spec->rel2abs($_);
			push(@files, $abs_path);
                }
        }
} => @ARGV;

my $identifier = get_identifier(\@files);

# prepare new entry for LuminosityProvider
my @new_entry=();
my $date = sprintf("%02d/%02d/%04d %02d:%02d",((localtime)[3]),((localtime)[4] +1),((localtime)[5]+1900), (localtime)[2], (localtime)[1]);
push(@new_entry, "\t// $user: $date\n");
push(@new_entry, "\t// DBSNAME: $dbs_name\n");
push(@new_entry, "\t// PRESELECTION: $preselection\n");
push(@new_entry, "\t// COMMENT: $comments\n");
push(@new_entry, "\tluminosity[\"$identifier\"] = $int_lumi;\n");
push(@new_entry, "\n");

# ask for confirmation

print "Are you sure you want to make the following changes:\n";
rename_files(\@files, $identifier, 1);
print "Add to LuminosityProvider.cc the following entry:\n";
foreach (@new_entry) { print; }

print "Are you sure? [y/N] : ";
my $continue = <STDIN>;
exit unless($continue =~ /^\s*y(es)?/i);
print "\n";

print "syncing LuminosityProvider: svn up\n";
system("svn up LuminosityProvider");

rename_files(\@files, $identifier, 0);
add_to_LuminosityProvider($lumi_provider_file, $identifier, $int_lumi, \@new_entry);

print <<EOF;

...finished successfully.

##############################################
### DO NOT FORGET TO CHECK IN YOUR CHANGES ###
##############################################
run for example:
svn ci LuminosityProvider/ \
-m "LuminosityProvider: added entry $identifier for dataset $dbs_name"
EOF

sub get_identifier
{
	my ($files_ref) = @_;
	$| = 1;
	print "generating unique identifier... this may take a while...";
	my @checksums = ();
	foreach my $file(@$files_ref){
		my $a32 = Digest::Adler32->new;
		open(FILE, "<$file");
		$a32->addfile(*FILE);
		push(@checksums, hex($a32->hexdigest));
		close(FILE);
	}
	my $xor_checksum = shift(@checksums);
	foreach $sum(@checksums){
		$xor_checksum ^= $sum;
	}
	print " done.\n";

	return unpack("H*", pack("N", $xor_checksum));
}

sub rename_files
{
	my ($files_ref, $ident, $simulate) = @_;
	
	foreach my $file(@$files_ref){
		my $path = "";
		my $filename = "";
		$filename = $file if($file !~ /\//g);

		if($file =~ /(.*\/)([\w\d\_\.]+)$/g){
			$path = $1;	
			$path = "/$path";
			$filename = $2;	
		}
		my ($prefix, $suffix) = split(/\./, $filename);
		print "mv $file $path${prefix}$ident.$suffix\n";
		`mv $file $path${prefix}$ident.$suffix` unless($simulate);
	}
}

sub add_to_LuminosityProvider
{
	my ($lumi_provider_file, $ident, $int_lumi, $new_entry_ref) = @_;
        open(FH, "+< $lumi_provider_file")                 or die "Opening: $!";
        @file = <FH>;

	my @changed_file = ();
        foreach $line(@file){
                if($line =~ m/--- AUTO GENERATED INTEGRATED LUMINOSITIES/){
                	push(@changed_file, @$new_entry_ref);
                }

               	push(@changed_file, $line);
        }
        seek(FH,0,0)                        or die "Seeking: $!";
        print FH @changed_file                     or die "Printing: $!";
        truncate(FH,tell(FH))               or die "Truncating: $!";
        close(FH)                           or die "Closing: $!";
}
