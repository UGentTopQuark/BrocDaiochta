#!/usr/bin/perl

# FIXME: should be parameter
my $modename = 'mu';

my $events = analyse_file($ARGV[0], $modename);
print_output($events, $modename);

sub print_step_output
{
	my ($evt, $passstep, $modename) = @_;

	if($modename eq 'mu'){
		if($passstep == 1){
			print $evt->{'run'}.":".$evt->{'lumisection'}.":".$evt->{'event'}.":".$modename.":".$passstep.":";
			print $evt->{'mu_pt'}.":".$evt->{'mu_eta'}.":".$evt->{'mu_nHits'}.":".$evt->{'mu_d0'}.":";
			print $evt->{'mu_chi2'}.":".$evt->{'mu_evetocone'}.":".$evt->{'mu_hvetocone'}.":".$evt->{'mu_trackiso'}.":";
			print $evt->{'mu_ecaliso'}.":".$evt->{'mu_hcaliso'}.":".$evt->{'mu_relIso'};
			print "\n";
		}elsif($passstep == 2){
			print $evt->{'run'}.":".$evt->{'lumisection'}.":".$evt->{'event'}.":".$modename.":".$passstep.":";
			print $evt->{'njets'};
			print "\n";
		}elsif($passstep == 3){
			print $evt->{'run'}.":".$evt->{'lumisection'}.":".$evt->{'event'}.":".$modename.":".$passstep.":";
			print $evt->{'j0_pt'}.":".$evt->{'j0_eta'}.":";
			print $evt->{'j1_pt'}.":".$evt->{'j1_eta'}.":";
			print $evt->{'j2_pt'}.":".$evt->{'j2_eta'}.":";
			print $evt->{'j3_pt'}.":".$evt->{'j3_eta'};
			print "\n";
		}elsif($passstep == 4){
			print $evt->{'run'}.":".$evt->{'lumisection'}.":".$evt->{'event'}.":".$modename.":".$passstep.":";
			print $evt->{'m3'};
			print "\n";
		}elsif($passstep == 5){
		}
	}elsif($modename eq 'e'){
		if($passstep == 1){
			print $evt->{'run'}.":".$evt->{'lumisection'}.":".$evt->{'event'}.":".$modename.":".$passstep.":";
			print $evt->{'e_pt'}.":".$evt->{'e_pt'}.":".$evt->{'e_phi'}.":".$evt->{'e_d0'}.":";
			print $evt->{'e_trackiso'}.":".$evt->{'e_ecaliso'}.":".$evt->{'e_hcaliso'}.":".$evt->{'e_relIso'};
			print "\n";
		}elsif($passstep == 2){
			print $evt->{'run'}.":".$evt->{'lumisection'}.":".$evt->{'event'}.":".$modename.":".$passstep.":";
			print $evt->{'nmuons'};
			print "\n";
		}elsif($passstep == 3){
			print $evt->{'run'}.":".$evt->{'lumisection'}.":".$evt->{'event'}.":".$modename.":".$passstep.":";
			print $evt->{'njets'};
			print "\n";
		}elsif($passstep == 4){
			print $evt->{'run'}.":".$evt->{'lumisection'}.":".$evt->{'event'}.":".$modename.":".$passstep.":";
			print $evt->{'njets'};
			print "\n";
		}elsif($passstep == 5){
			print $evt->{'run'}.":".$evt->{'lumisection'}.":".$evt->{'event'}.":".$modename.":".$passstep.":";
			print $evt->{'j0_pt'}.":".$evt->{'j0_eta'}.":";
			print $evt->{'j1_pt'}.":".$evt->{'j1_eta'}.":";
			print $evt->{'j2_pt'}.":".$evt->{'j2_eta'}.":";
			print $evt->{'j3_pt'}.":".$evt->{'j3_eta'};
			print "\n";
		}
	}
}

sub print_output
{
	my ($events, $modename) = @_;
	foreach $evt(@$events){
		my $cutset = $evt->{'cutset'};
		if($cutset =~ /(\d+)_cutset/){
			my $passstep = $1;
			$passstep =~ s/^0*//;
			print_step_output($evt, $passstep, $modename);
		}
	}
}

sub analyse_file
{
	my ($infile, $modename) = @_;
	open(INFILE, "<$infile") or die "could not open $infile: $!\n";
	my @file = <INFILE>;
	close(INFILE);

	@events = ();

	my $run = my $event = my $lumisection = -1;
	foreach $line(@file){
		if($line =~ /^Begin processing the (\d+)\w+ record\. Run (\d+), Event (\d+), LumiSection (\d+)/){
			$run = $2;
			$event = $3;
			$lumisection = $4;
		}elsif(((($modename eq "e") && (($line =~ /e_background/) || ($line =~ /electron/))) ||
		        (($modename eq "mu") && (($line =~ /mu_background/) || ($line =~ /muon/)))) &&
			$line =~ /^sync_[\w\d]+\|[\w_]+\|(\d+_\w+)\-/){

			$cutset = $1;
			$line =~ s/^.*$cutset\-\d+ //;
			$line =~ s/:?\s+/:/g;
			my %entry = split(":", $line);
			$entry{'cutset'} = $cutset;
			$entry{'run'} = $run;
			$entry{'event'} = $event;
			$entry{'lumisection'} = $lumisection;
			push(@events, \%entry);
		}
	}

	return \@events;
}
