[muon]
s00:
trigger = 616

min_npvertices = 1
pvertex_min_ndof = 4
pvertex_max_z = 24.0
pvertex_max_rho = 2.0
pvertex_not_fake = 1
identify_events = 1


s01:
min_nisolated_mu = 1
max_nisolated_mu = 1
mu_isPFMuon = 1
mu_type = 0
min_mu_pt = 20.
max_mu_eta = 2.1
max_mu_chi2 = 10
min_mu_nTrackerLayers = 6
min_mu_nMuonHits = 1
max_mu_d0 = 0.02
max_mu_dbeta_PFrelIso = 0.125
min_mu_nPixelHits = 1
max_mu_vz = 1.
min_mu_nMatchedStations = 2
min_mu_dR = 0.3


min_jet_pt = 35
max_jet_eta = 2.5
# PF Additions
min_jet_nconstituents = 1
min_jet_chf = 0.
max_jet_nhf = 0.99
max_jet_nemf = 0.99
max_jet_cemf = 0.99
min_jet_cmulti = 0.


s02:
max_nloose_mu = 1       # the same one as the tight one
loose_mu_type = 0
min_loose_mu_pt = 10.
max_loose_mu_eta = 2.5
max_loose_mu_dbeta_PFrelIso = 0.2

s03:
max_nisolated_e = 0
min_e_et = 20.
max_e_eta = 2.5
max_e_dbeta_PFrelIso = 0.2


s041:
min_no_jets = 1

s042:
min_no_jets = 2

s043:
min_no_jets = 3

s05:
min_no_jets = 4

s06:
name_btag = 7
min_btag = 0.679


