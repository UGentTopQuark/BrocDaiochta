[electron]
s00:
trigger = 608

min_npvertices = 1
pvertex_min_ndof = 4
pvertex_max_z = 24.0
pvertex_max_rho = 2.0
pvertex_not_fake = 1


s01:

min_nisolated_e = 1
max_nisolated_e = 1
min_e_pt = 35.
max_e_eta = 2.5
e_exclude_eta_crack = 1
max_e_d0 = 0.02
max_e_dbeta_PFrelIso = 0.1
identify_events = 1
min_e_mva_id = 0.0
e_conv_veto = 1

min_jet_pt = 35
max_jet_eta = 2.5
# PF Additions
min_jet_nconstituents = 1
min_jet_chf = 0.
max_jet_nhf = 0.99
max_jet_nemf = 0.99
max_jet_cemf = 0.99
min_jet_cmulti = 0.


s02:
max_nisolated_mu = 0
mu_type = 0
min_mu_pt = 10.
max_mu_eta = 2.5
max_mu_dbeta_PFrelIso = 0.2


s03:
max_nloose_e = 1
min_loose_e_pt = 35
max_loose_e_eta = 2.5
loose_e_exclude_eta_crack = 1
min_e_mva_id = 0.0
max_loose_e_dbeta_PFrelIso = 0.2

s041:
max_e_nLostTrackerHits = 0.

;s042:
;e_conv_veto = 1


s051:
min_no_jets = 1

s052:
min_no_jets = 2

s053:
min_no_jets = 3

s05:
min_no_jets = 4

s06:
name_btag = 7
min_btag = 0.679


