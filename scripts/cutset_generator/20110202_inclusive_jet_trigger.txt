[muon]
s01:	
min_npvertices = 1
pvertex_min_ndof = 4
pvertex_max_z = 24
pvertex_max_rho = 2.0
pvertex_not_fake = 1

min_nisolated_mu = 1
max_nisolated_mu = 1
mu_type = 100
min_mu_pt = 20.
max_mu_eta = 2.1

min_jet_mu_dR = 0.1
#min_mu_dR = 0.3

min_jet_pt = 30.
max_jet_eta = 2.4

# PF Additions
min_jet_nconstituents = 1
min_jet_chf = 0.
max_jet_nhf = 0.99
max_jet_nemf = 0.99
max_jet_cemf = 0.99
min_jet_cmulti = 0.

min_mu_nPixelLayers = 1
min_mu_nMatchedSegments = 2
min_mu_nMuonHits = 1
max_mu_relIso = 0.05
max_mu_chi2 = 10.
max_mu_d0 = 0.02
min_mu_nHits = 11
min_mu_pt = 20.
max_mu_eta = 2.1
max_mu_vz = 1.

max_nloose_mu = 1	
loose_mu_type = 0
min_loose_mu_pt = 10.
max_loose_mu_eta = 2.5
max_loose_mu_relIso = 0.2

trigger_study_type = 1
hlt_lep_triggers = 101
max_hlt_dR = 0.2
max_L1_dR = 0.3
max_HLTL1_dR = 0.3
