#!/usr/bin/perl

use Tie::IxHash::Easy;
use Getopt::Std;

getopts("l");

die "usage: $0 [<config>] [-l]\n" if(defined($opt_h));

my $config = 'cuts.txt';
my $cutselector = '../src/EventSelection/CutSelector.cc';

$config = $opt_c if(defined($opt_c));
$config = $ARGV[0] if(!defined($opt_c) && @ARGV>0);

my %allowed_cuts = ();
my %cuts = ();
tie %cuts, "Tie::IxHash::Easy";
get_allowed_cuts("../share/allowed_cuts.txt", \%allowed_cuts,1);
get_allowed_cuts("../share/allowed_vcuts.txt", \%allowed_cuts,2);
if(defined($opt_l)){
	print "possible cut parameters: \n";
	while (my ($parameter, $val) = each %allowed_cuts){
		print "v >$parameter<\n" if($val == 2);
		print "  >$parameter<\n" if($val == 1);
	}
	exit;
}
read_in_config($config, \%cuts);
replace_cuts(\%cuts, \%allowed_cuts, $cutselector, $config);

sub replace_cuts
{
	my ($cuts, $allowed_cuts, $cutselector, $config) = @_;
	open(FH, "+< $cutselector")                 or die "Opening: $!";
	@file = <FH>;
	my $delete = 0;
	my $found_autogen_switched_on = 0;
	foreach $line(@file){
		$found_autogen_switched_on = 1 if($line =~ m/^\s*define_cuts_autogen\(\);/);
		if($line =~ m/---AUTOGENERATED BEGIN---/){
			$delete = 1;
		}elsif($line =~ m/---AUTOGENERATED END---/){	
			$delete = 0;
			my @cuts_to_print = ();
			print_cuts(\%cuts, \%allowed_cuts, \@cuts_to_print);
			push(@changed_file, "\t// ---AUTOGENERATED BEGIN---\n");
			my $config_string = sprintf("\t// %02d/%02d/%04d %02d:%02d CUTSET_GENERATOR configuration: $config\n",((localtime)[3]),((localtime)[4] +1),((localtime)[5]+1900), (localtime)[2], (localtime)[1]);
			push(@changed_file, $config_string);
			push(@changed_file, @cuts_to_print);
			push(@changed_file, "\t// ---AUTOGENERATED END---\n");
		}else{
	        	push(@changed_file, $line) unless($delete);
		}
	}
	seek(FH,0,0)                        or die "Seeking: $!";
	print FH @changed_file                     or die "Printing: $!";
	truncate(FH,tell(FH))               or die "Truncating: $!";
	close(FH)                           or die "Closing: $!";

	print "WARNING: autogen_cuts() not activated in CutSelector()\n" unless($found_autogen_switched_on);
}

sub read_in_config
{
	my ($config, $cuts) = @_;
	open(CONFIG, "$config") or die "can't open $config";
	$mode = '';	# muon / electron
	my $previous_cutset = '';
	my $current_cutset = '';
	my $reading_cuts = 0;
	while(defined($line = <CONFIG>)){
		$line =~ s/#.*$//;	# remove comments
		$line =~ s/;.*$//;	# remove comments
		$line =~ s/\s*$//;	# remove tailing spaces
		next unless($line);	# ignore empty lines

		if($line =~ m/\[(\w+)\]/){
			$current_cutset = '';
			$previous_cutset = '';
			$mode = $1;
		}elsif($line =~ m/(s\d+)\:(.*)$/){
			my $specifier = $2;	# specify which cuts to be inherited from older cutset
			#	first cutset
			if($previous_cutset eq '' && $current_cutset eq ''){
				$current_cutset = $1;
			}elsif($current_cutset ne ''){
				$previous_cutset = $current_cutset;
				$current_cutset = $1;

				if($specifier eq ''){
					# default: take over cuts of previous cutset
					while(my ($cut, $value) = each %{$cuts->{$mode}->{$previous_cutset}}){
						next if($cut eq "identify_events");
						$cuts->{$mode}->{$current_cutset}->{$cut} = $value;
					}
				}elsif($specifier =~ m/(s\d+)/){
					my $inherit_from = $1;
					# take over cuts from cutset $inherit_from
					while(my ($cut, $value) = each %{$cuts->{$mode}->{$inherit_from}}){
						next if($cut eq "identify_events");
						$cuts->{$mode}->{$current_cutset}->{$cut} = $value;
					}
				}elsif($specifier =~ m/X/){
					# don't inherit cuts from any cutset
				}else{
					print "WARNING: unrecognised command: $specifier\n";
				}
			}
		}elsif($line =~ m/\s*([\w\d\_]+)\s*\=\s*(.*)$/){
			$cuts->{$mode}->{$current_cutset}->{$1}=$2;
		}
	}
	close(CONFIG);
}

sub print_cuts
{
	my ($cuts, $allowed_cuts_ref, $everything_to_print) = @_;
	my @cuts_to_print = ();
	my %known_vectors = ();
	#print everything
	while(my ($mode, $tmp) = each %cuts){
		while(my ($cutset, $tmp2) = each %{$cuts{$mode}}){
			push(@cuts_to_print, "\t// step: $cutset\n");
			while(my ($cut, $value) = each %{$cuts{$mode}{$cutset}}){
				my $step = '';
				if($cutset =~ /s(\d+)/){
					$step = "$1_cutset";
				}else{ die "WARNING: wrongly formated step: $cutset, exiting...\n"; };
				if($allowed_cuts_ref->{$cut} == 1){ # scalar cuts
					push(@cuts_to_print, "\tcut_defs[\"$mode\"][\"$step\"][\"$cut\"] = $value;\n");
				}elsif($allowed_cuts_ref->{$cut} == 2){	# vector cuts
					# check if vector filled with different values
					$value =~ s/\s*//;	# remove spaces
					if($value =~ m/:/){
						unless(defined($known_vectors{$value})){ # if a vector like this doesn't exist yet
							my $nvectors = (scalar keys %known_vectors)+1;
							$known_vectors{$value} = "vector_cut_$nvectors";
						}
						push(@cuts_to_print, "\tvcuts(\"$mode\",\"$step\",\"$cut\",".$known_vectors{$value}.");\n");
					}else{	# only one value filled to vector
						push(@cuts_to_print, "\tvcuts(\"$mode\",\"$step\",\"$cut\", $value);\n");
					}
				}else{
					die "WARNING: unrecognised cut: $cut, exiting...\n";
				}
			}
			push(@cuts_to_print, "\n");
		}
	}

	my @vectors_to_print = ();
	# book vectors for vector_cuts
	while(my($values_string, $vector_name) = each %known_vectors){
		my @values = split(/:/, $values_string);
		push(@vectors_to_print, "\tstd::vector<double> *$vector_name = new std::vector<double>();\n");
		foreach my $value(@values){
			push(@vectors_to_print, "\t$vector_name->push_back($value);\n");
		}
		push(@vectors_to_print, "\tcuts_to_be_deleted.push_back($vector_name);\n");
		push(@vectors_to_print, "\n");
	}

	@$everything_to_print = (@vectors_to_print, @cuts_to_print);
}

sub get_allowed_cuts
{
	my ($cutselector, $allowed_cuts_ref, $id) = @_;
	open(CUTSELECTOR, "$cutselector") or die "can't open $cutselector";
	while(defined($line = <CUTSELECTOR>)){
		next unless($line =~ /\w+/);
		$line =~ s/^\s+//;
		$line =~ s/\s+$//;
		chomp $line;
		$allowed_cuts_ref->{$line} = $id;
	}
	close(CUTSELECTOR);
}
