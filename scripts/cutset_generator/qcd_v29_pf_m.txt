[muon]
s00:	
###LOOSE
trigger = 101

min_npvertices = 1
pvertex_min_ndof = 4
pvertex_max_z = 24
pvertex_max_rho = 2.0
pvertex_not_fake = 1
##loose
min_mm_nloose_mu = 1
max_mm_nloose_mu = 1
mm_loose_mu_type = 100
min_mm_loose_mu_pt = 20.
max_mm_loose_mu_eta = 2.1
max_mm_loose_mu_relIso = 0.1
max_mm_loose_mu_chi2 = 10
min_mm_loose_mu_nMuonHits = 1
min_mm_loose_mu_nHits = 11
##########min_jet_mu_dR = 0.1
min_mm_loose_mu_dR = 0.3
max_mm_loose_mu_d0 = 0.02
max_mm_loose_mu_vz = 1.
min_mm_loose_mu_nPixelLayers = 1
min_mm_loose_mu_nMatchedSegments = 2

##tight
min_nisolated_mu = 1
max_nisolated_mu = 0
mu_type = 100
min_mu_pt = 20.
max_mu_eta = 2.1
max_mu_relIso = 0.05
max_mu_chi2 = 10
min_mu_nMuonHits = 1
min_mu_nHits = 11
min_mu_dR = 0.3
max_mu_d0 = 0.02
max_mu_vz = 1.
min_mu_nPixelLayers = 1
min_mu_nMatchedSegments = 2

min_jet_mu_dR = 0.1
min_jet_pt = 30.
max_jet_eta = 2.4
# PF Additions
min_jet_nconstituents = 1
min_jet_chf = 0.
max_jet_nhf = 0.99
max_jet_nemf = 0.99
max_jet_cemf = 0.99
min_jet_cmulti = 0.
 
max_nloose_mu = 1	# the same one as the tight one
loose_mu_type = 0
min_loose_mu_pt = 10.
max_loose_mu_eta = 2.5
max_loose_mu_relIso = 0.2

max_nisolated_e = 0
min_e_et = 15.
max_e_eta = 2.5
max_e_relIso = 0.2

min_no_jets = 4

min_met = 5

s01:	
###TIGHT
trigger = 101

min_npvertices = 1
pvertex_min_ndof = 4
pvertex_max_z = 24
pvertex_max_rho = 2.0
pvertex_not_fake = 1
##loose
min_mm_nloose_mu = 1
max_mm_nloose_mu = 1
mm_loose_mu_type = 100
min_mm_loose_mu_pt = 20.
max_mm_loose_mu_eta = 2.1
max_mm_loose_mu_relIso = 0.1
max_mm_loose_mu_chi2 = 10
min_mm_loose_mu_nMuonHits = 1
min_mm_loose_mu_nHits = 11
##########min_jet_mu_dR = 0.1
min_mm_loose_mu_dR = 0.3
max_mm_loose_mu_d0 = 0.02
max_mm_loose_mu_vz = 1.
min_mm_loose_mu_nPixelLayers = 1
min_mm_loose_mu_nMatchedSegments = 2

##tight
min_nisolated_mu = 1
max_nisolated_mu = 1
mu_type = 100
min_mu_pt = 20.
max_mu_eta = 2.1
max_mu_relIso = 0.05
max_mu_chi2 = 10
min_mu_nMuonHits = 1
min_mu_nHits = 11
min_mu_dR = 0.3
max_mu_d0 = 0.02
max_mu_vz = 1.
min_mu_nPixelLayers = 1
min_mu_nMatchedSegments = 2

min_jet_mu_dR = 0.1
min_jet_pt = 30.
max_jet_eta = 2.4
# PF Additions
min_jet_nconstituents = 1
min_jet_chf = 0.
max_jet_nhf = 0.99
max_jet_nemf = 0.99
max_jet_cemf = 0.99
min_jet_cmulti = 0.
 
max_nloose_mu = 1	# the same one as the tight one
loose_mu_type = 0
min_loose_mu_pt = 10.
max_loose_mu_eta = 2.5
max_loose_mu_relIso = 0.2

max_nisolated_e = 0
min_e_et = 15.
max_e_eta = 2.5
max_e_relIso = 0.2

min_no_jets = 4

min_met = 5
