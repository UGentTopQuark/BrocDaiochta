[muon]

#fill data loose and qcd loose and tight
s00:
trigger = 101 
#PV
min_npvertices = 1
pvertex_min_ndof = 4
pvertex_max_rho = 2.0
pvertex_not_fake = 1
pvertex_max_z = 24
#loose
#min_nloose_mu = 1
#max_nloose_mu = 1
#loose_mu_type = 100
#min_loose_mu_pt = 15
#max_loose_mu_pt = 60
#max_loose_mu_eta = 2.1
#min_loose_mu_nHits = 11
#min_loose_mu_nMuonHits = 1
#min_loose_mu_nPixelHits = 1
#min_loose_mu_nMatchedSegments = 2
#max_loose_mu_chi2 = 10
#max_loose_mu_d0 = 0.02
#max_loose_mu_vz = 1
#min_loose_mu_dR = 0.3 ##
#max_loose_mu_relIso = 0.1
#tight
min_nisolated_mu = 1
max_nisolated_mu = 1
mu_type = 100
min_mu_pt = 15
##max_mu_pt = 60
max_mu_eta = 2.1
min_mu_nHits = 11
min_mu_nMuonHits = 1
min_mu_nPixelHits = 1
min_mu_nMatchedSegments = 2
max_mu_chi2 = 10
max_mu_d0 = 0.02
min_mu_dR = 0.3
max_mu_vz = 1
max_mu_relIso = 0.1
#second lepton veto
min_nisolated_e = -1
max_nisolated_e = 0
e_electronID = 5
min_e_et = 15
max_e_eta = 2.5
max_e_d0 = 0.02
min_e_dR = -1
max_e_relIso = 0.2
#jetmet
min_jet_pt = 30.
max_jet_eta = 2.4
min_jet_emf = 0.01
min_jet_n90Hits = 1
max_jet_fHPD = 0.98
##min_jet_nconstituents = 1
#max_jet_cemf = 0.99
#max_jet_nhf = 0.99
#max_jet_nemf = 0.99
#min_jet_chf = 0
max_met = 5
min_no_jets = 3
##max_no_jets = 2

#fill data tight
s01:
trigger = 101
#PV
min_npvertices = 1
pvertex_min_ndof = 4
pvertex_max_rho = 2.0
pvertex_not_fake = 1
pvertex_max_z = 24
#loose
#min_nloose_mu = 1
#max_nloose_mu = 1
#loose_mu_type = 100
#min_loose_mu_pt = 15
#max_loose_mu_pt = 60
#max_loose_mu_eta = 2.1
#min_loose_mu_nHits = 11
#min_loose_mu_nMuonHits = 1
#min_loose_mu_nPixelHits = 1
#min_loose_mu_nMatchedSegments = 2
#max_loose_mu_chi2 = 10
#max_loose_mu_d0 = 0.02
#max_loose_mu_vz = 1
#min_loose_mu_dR = 0.3 ##
#max_loose_mu_relIso = 0.1
#tight
min_nisolated_mu = 1 
max_nisolated_mu = 1
mu_type = 100
min_mu_pt = 15
##max_mu_pt = 60
max_mu_eta = 2.1
min_mu_nHits = 11
min_mu_nMuonHits = 1
min_mu_nPixelHits = 1
min_mu_nMatchedSegments = 2
max_mu_chi2 = 10
max_mu_d0 = 0.02
min_mu_dR = 0.3
max_mu_vz = 1
max_mu_relIso = 0.05
#second lepton veto
min_nisolated_e = -1
max_nisolated_e = 0
e_electronID = 5
min_e_et = 15
max_e_eta = 2.5
max_e_d0 = 0.02
min_e_dR = -1
max_e_relIso = 0.2
#jetmet
min_jet_pt = 30.
max_jet_eta = 2.4
min_jet_emf = 0.01
min_jet_n90Hits = 1
max_jet_fHPD = 0.98
##min_jet_nconstituents = 1
#max_jet_cemf = 0.99
#max_jet_nhf = 0.99
#max_jet_nemf = 0.99
#min_jet_chf = 0
max_met = 5
min_no_jets = 3
##max_no_jets = 2
