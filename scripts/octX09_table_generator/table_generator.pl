#!/usr/bin/perl
use Tie::IxHash::Easy;

$tex_output_file = 'table.tex';

#$tprime_mass = '250';
$tprime_mass = shift @ARGV;

die "usage: $0 <t' mass> <log-directories>" unless($tprime_mass =~ /\d+/);

$lepton = 'mu';
$process_preselection = 1;
$show_statistic_errors = 0;

#first datasets element considered as signal
#@datasets = ('TTbar_muon', 'TTbar_mu_background', 'Wjets_mu_background', 'Zjets_mu_background', 'QCDmu20_mu_background', 'QCDmu30to50_mu_background', 'QCDmu50to80_mu_background');
#@datasets = ('TTbar_muon', 'TTbar_mu_background', 'Wjets_mu_background', 'Zjets_mu_background', 'QCD');
tie %datasets, "Tie::IxHash::Easy";
if($lepton eq 'mu'){
	$datasets{'Tprime'.$tprime_mass}{'muon'}=1;
	$datasets{'Tprime'.$tprime_mass}{'mu_background'}=1;
#	$datasets{'TTbar'}{'muon'}=1;
	$datasets{'TTbar'}{'mu_background'}=1;
	$datasets{'Wjets'}{'mu_background'}=1;
	$datasets{'Zjets'}{'mu_background'}=1;
	$datasets{'Mupt15'}{'mu_background'}=1;
	$datasets{'TsChan'}{'mu_background'}=1;
	$datasets{'TtChan'}{'mu_background'}=1;
	$datasets{'TtWChan'}{'mu_background'}=1;
}
if($lepton eq 'e'){
	$datasets{'Tprime'}{'electron'}=1;
	$datasets{'Tprime'}{'e_background'}=1;
#	$datasets{'TTbar'}{'electron'}=1;
	$datasets{'TTbar'}{'e_background'}=1;
	$datasets{'Wjets'}{'e_background'}=1;
	$datasets{'Zjets'}{'e_background'}=1;
	$datasets{'QCDpt15'}{'e_background'}=1;
	$datasets{'TsChan'}{'mu_background'}=1;
	$datasets{'TtChan'}{'mu_background'}=1;
	$datasets{'TtWChan'}{'mu_background'}=1;
}

# integrated luminosity in 1/picobarn
$integrated_luminosity = 20;


#######################
##OctX
########################

################
##Muon
################

$cross_section{'TTbar'}=414;
$cross_section{'Wjets'}=8180;
$cross_section{'Zjets'}=1940;
$cross_section{'Mupt15'}=364000;

# = (passed presel/N in SD)*(N in SD/(N in PD/PS))
$filter_efficiency{'TTbar'}=0.592961487383798*(1506/(529750/100));

$filter_efficiency{'Wjets'}=0.00297075867581429*(75065/(2082633/20));
$filter_efficiency{'Zjets'}=0.00376390076988879*(11690/(2000500/100));
$filter_efficiency{'Mupt15'}=0.0852273654699078*(1531609/5204815);

################
##Electron
################
#$cross_section{'TTbar'}=414;
#$cross_section{'Wjets'}=8740;
#$cross_section{'Zjets'}=1940;
#$cross_section{'QCDbctoe20to30'}=192000;
#$cross_section{'QCDbctoe30to80'}=240000;
#$cross_section{'QCDbctoe80to170'}=22800;
#$cross_section{'QCDem20to30'}=3200000;
#$cross_section{'QCDem30to80'}=4700000;
#$cross_section{'QCDem80to170'}=285000;

#$filter_efficiency{'TTbar'}=0.510767364270549*(3297/(529750/100));

#$filter_efficiency{'Wjets'}=0.00271956486962086*(87514/(2158013/20);
#$filter_efficiency{'Zjets'}=0.00404187649085608*(15092/2682355);
#$filter_efficiency{'QCDbctoe20to3'}=0.000318050781259939*(393019/2383833);
#$filter_efficiency{'QCDbctoe30to80'}=0.011427524785401*(769808/2035108);
#$filter_efficiency{'QCDbctoe80to170'}=0.088704674196676*(641522/1038080);
#$filter_efficiency{'QCDem20to30'}=0.000253191658167839*(6062601/33709623);
#$filter_efficiency{'QCDem30to80'}=0.00591902264557805*(8055215/40044873);
#$filter_efficiency{'QCDem80to170'}=0.0586553569902206*(2538711/5735561);


########################################
### filter efficiencies grid submission
########################################
#$filter_efficiency{'TTbar'}=0.287856781096642;
#$filter_efficiency{'Wjets'}=0.;
#$filter_efficiency{'Zjets'}=0.0368144547552413;
#$filter_efficiency{'Mupt15'}=0.;
#$filter_efficiency{'TsChan'}=0.26393866155513;
#$filter_efficiency{'TtChan'}=0.211470586841893;
#$filter_efficiency{'TtWChan'}=0.245734939188869;
## FIXME: fix this for qcd electron background
#$filter_efficiency{'QCDpt15'}=0.;

$dictionary{'TTbar_muon'} = '$t\bar{t}$ ($\mu$-signal)';
$dictionary{'TTbar_mu_background'} = '$t\bar{t}$';
$dictionary{'Mupt15_mu_background'} = 'QCD';
$dictionary{'QCDpt15_e_background'} = 'QCD';
$dictionary{'Tprime_muon'} = '$t\'\bar{t}\'$ ($\mu$, 250)';
$dictionary{'Tprime_mu_background'} = '$t\'\bar{t}\'$ (other, 250)';
$dictionary{'Tprime250_muon'} = '$t\'\bar{t}\'$ ($\mu$, 250)';
$dictionary{'Tprime250_mu_background'} = '$t\'\bar{t}\'$ (other, 250)';
$dictionary{'Tprime275_muon'} = '$t\'\bar{t}\'$ ($\mu$, 275)';
$dictionary{'Tprime275_mu_background'} = '$t\'\bar{t}\'$ (other, 275)';
$dictionary{'Tprime300_muon'} = '$t\'\bar{t}\'$ ($\mu$, 300)';
$dictionary{'Tprime300_mu_background'} = '$t\'\bar{t}\'$ (other, 300)';
$dictionary{'Tprime325_muon'} = '$t\'\bar{t}\'$ ($\mu$, 325)';
$dictionary{'Tprime325_mu_background'} = '$t\'\bar{t}\'$ (other, 325)';
$dictionary{'Tprime350_muon'} = '$t\'\bar{t}\'$ ($\mu$, 350)';
$dictionary{'Tprime350_mu_background'} = '$t\'\bar{t}\'$ (other, 350)';
$dictionary{'Tprime375_muon'} = '$t\'\bar{t}\'$ ($\mu$, 375)';
$dictionary{'Tprime375_mu_background'} = '$t\'\bar{t}\'$ (other, 375)';
$dictionary{'Tprime400_muon'} = '$t\'\bar{t}\'$ ($\mu$, 400)';
$dictionary{'Tprime400_mu_background'} = '$t\'\bar{t}\'$ (other, 400)';
$dictionary{'Tprime250_electron'} = '$t\'\bar{t}\'$ ($e$-signal)';
$dictionary{'Tprime250_e_background'} = '$t\'\bar{t}\'$ (other)';
$dictionary{'Wjets_mu_background'} = '$W\,+\,\mathrm{jets}$';
$dictionary{'Zjets_mu_background'} = '$Z\,+\,\mathrm{jets}$';
$dictionary{'TTbar_electron'} = '$t\bar{t}$ ($e$-signal)';
$dictionary{'TTbar_e_background'} = '$t\bar{t}$ (other)';
$dictionary{'Wjets_e_background'} = '$W\,+\,\mathrm{jets}$';
$dictionary{'Zjets_e_background'} = '$Z\,+\,\mathrm{jets}$';
$dictionary{'TsChan_mu_background'} = 't, s-chan';
$dictionary{'TtChan_mu_background'} = 't, t-chan';
$dictionary{'TtWChan_mu_background'} = 't, tW-chan';
$dictionary{'TsChan_e_background'} = 't, s-chan';
$dictionary{'TtChan_e_background'} = 't, t-chan';
$dictionary{'TtWChan_e_background'} = 't, tW-chan';
$dictionary{'QCD'} = 'QCD';
$dictionary{'min_no_jets'} = '\# jets > ';
$dictionary{'e_electronID'} = '\mathrm{ElectronID}:~';
$dictionary{'min_e_pt'} = 'p_T (e) > ';
$dictionary{'min_mu_pt'} = 'p_T (\mu) > ';
$dictionary{'min_met'} = 'E_T^\mathrm{miss} > ';
$dictionary{'min_mu_ht'} = 'H_T^\mu > ';
$dictionary{'min_e_ht'} = 'H_T^e > ';
$dictionary{'max_e_d0'} = 'd0 (e) < ';
$dictionary{'max_mu_d0'} = 'd0 (\mu) < ';
$dictionary{'max_mu_trackiso'} = 'E_\mathrm{iso}^\mathrm{track}\,(\mu) < ';
$dictionary{'max_e_trackiso'} = 'E_\mathrm{iso}^\mathrm{track}\,(e) < ';
$dictionary{'max_mu_ecaliso'} = 'E_\mathrm{iso}^\mathrm{ecal}\,(\mu) < ';
$dictionary{'max_e_ecaliso'} = 'E_\mathrm{iso}^\mathrm{ecal}\,(e) < ';
$dictionary{'max_mu_caliso'} = 'E_\mathrm{iso}^\mathrm{cal}\,(\mu) < ';
$dictionary{'max_e_caliso'} = 'E_\mathrm{iso}^\mathrm{cal}\,(e) < ';
$dictionary{'max_mu_hcaliso'} = 'E_\mathrm{iso}^\mathrm{hcal}\,(\mu) < ';
$dictionary{'max_e_hcaliso'} = 'E_\mathrm{iso}^\mathrm{hcal}\,(e) < ';
$dictionary{'min_mu_dR'} = '\Delta R\,(\mu,\mathrm{jets}) > ';
$dictionary{'min_e_dR'} = '\Delta R\,(e,\mathrm{jets}) > ';
$dictionary{'max_nisolated_e'} = '\# e \le';
$dictionary{'max_nisolated_mu'} = '\# \mu \le';
$dictionary{'min_nisolated_e'} = '\# e \ge';
$dictionary{'min_nisolated_mu'} = '\# \mu \ge';
$dictionary{'max_e_hcal_veto_cone'} = '\mathrm{Veto~Cone} (e, HCAL) \le';
$dictionary{'max_mu_hcal_veto_cone'} = '\mathrm{Veto~Cone} (\mu, HCAL) \le';
$dictionary{'max_e_ecal_veto_cone'} = '\mathrm{Veto~Cone} (e, ECAL) \le';
$dictionary{'max_mu_ecal_veto_cone'} = '\mathrm{Veto~Cone} (\mu, ECAL) \le';
$dictionary{'min_jet_pt'} = 'p_T^\mathrm{jets} > ';
$dictionary{'min_mu_relIso'} = '\mathrm{rel.~Iso.} (\mu) > ';
$dictionary{'min_e_relIso'} = '\mathrm{rel.~Iso.} (e) > ';
$dictionary{'max_jet_eta'} = '\eta^\mathrm{jets} < ';
$dictionary{'max_mu_eta'} = '\eta\,(\mu) < ';
$dictionary{'max_e_eta'} = '\eta\,(e) < ';
$dictionary{'Z_rejection_width'} = '\mathrm{veto}\,Z < ';
$dictionary{'Preselection'} = '\mathrm{Preselection} ';
$dictionary{'signal_over_background'} = 'S/B';
$dictionary{'purity'} = '$\pi$';
$dictionary{'trigger'} = '\mathrm{Trigger}: ';
$dictionary{'efficiency'} = '$\epsilon$';
$dictionary{'purity_x_efficiency'} = '$\epsilon\cdot\pi$';
$dictionary{'purity_x_efficiency2'} = '$\frac{\epsilon\cdot\pi}{2-\pi}$';

%cumulative_selection = ();
if($process_preselection != 1){
	if($lepton eq 'e'){
		$cumulative_selection{'min_nisolated_e'}=1;
		$cumulative_selection{'max_nisolated_e'}=1;
		$cumulative_selection{'max_nisolated_mu'}=0;
	}
	if($lepton eq 'mu'){
		$cumulative_selection{'max_nisolated_e'}=0;
		$cumulative_selection{'min_nisolated_mu'}=1;
		$cumulative_selection{'max_nisolated_mu'}=1;
	}
#	$cumulative_selection{'min_mu_relIso'}=0.9;
#	$cumulative_selection{'min_e_relIso'}=0.9;

	$cumulative_selection{'min_no_jets'}=4;
#	$cumulative_selection{'min_e_pt'}=30;
#	$cumulative_selection{'min_mu_pt'}=30;
	$cumulative_selection{'max_mu_eta'}=2.1;
	$cumulative_selection{'max_e_eta'}=2.1;
	$cumulative_selection{'max_jet_eta'}=2.4;
#	$cumulative_selection{'min_jet_pt'}=30;
	$cumulative_selection{'max_e_trackiso'}=5;
	$cumulative_selection{'max_e_caliso'}=10;
#	$cumulative_selection{'e_electronID'}=1;
	$cumulative_selection{'max_mu_trackiso'}=3;
	$cumulative_selection{'max_mu_caliso'}=1;
	$cumulative_selection{'trigger'}='85';
#	$cumulative_selection{'max_e_d0'}=0.2;
#	$cumulative_selection{'max_mu_d0'}=0.2;
}

$skip_comparison{'identifier'}=1;
$skip_comparison{'cuts_passed'}=1;
$skip_comparison{'cuts_not_passed'}=1;

#@statistics = ();#'signal_over_background');#, 'purity', 'efficiency', 'purity_x_efficiency', 'purity_x_efficiency2');
@statistics = ('purity_x_efficiency');

$ttbar_counter{'muon'}=0;
$ttbar_counter{'electron'}=0;
$ttbar_counter{'e_background'}=0;
$ttbar_counter{'mu_background'}=0;

$signal_dataset = 'Tprime'.$tprime_mass;
#$signal_dataset = 'TTbar';

#@cutnames = ('xxx','xxx','(s2)','(s3)','(s4)','(s5)','(s6/1)','(s6/2)', 'xxx', 'xxx','xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx',  '(ht300)', '(ht400)', '(ht500)', '(chi1)', '(chi2)', '(chi3)','(chi4)', '(chi5)', '(s5)+Ht400', '(s6/1)+Ht400','xxx');
#@cutnames = ('xxx','xxx','(s2)','(s3)','(s4)','(s5)','(s6/1)','(s6/2)', 'xxx', 'xxx','xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx',  'xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx','xxx', 'xxx', '(s5)+Ht400', '(s6/1)+Ht400', '(s6/2)+Ht400', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx', 'xxx');
#@cutnames = ('s1','s2','s3','s4','s6','s7+','s7','s7-','Ht','500','400','300','500b','400b','300b');
@cutnames = ('s1','s2','s3','s4','xxx','xxx','xxx');

################################################################################
##
##	Main Program
##
################################################################################

%settings = ();
%values = ();
%cuts = ();

@directories = @ARGV;
die "usage: $0 <t' mass> <directory 1> [<directory 2> ...]" if(@ARGV < 1);

%global_map = ();
foreach my $dirname(@directories){
	opendir(DIR, $dirname) or die "can't opendir $dirname: $!";
	while (defined(my $file = readdir(DIR))) {
		next unless($file =~ m/CMSSW_\d+.stdout/);
		analyse_file($dirname, $file);
	}
	closedir(DIR);
}

print_output($tex_output_file);

################################################################################
##
##	Functions
##
################################################################################

sub print_output
{
	my $tex_file = pop;
	
	open(TEX_FILE, ">$tex_file");
	print_header(TEX_FILE);
	print_table(TEX_FILE);	
	print_closing(TEX_FILE);
	close(TEX_FILE);
}

sub print_table
{
	my $fh = pop;

 	my $ndatasets = 0;
	my $already_qcd=0;
	foreach $key (keys %datasets){
		if(not $already_qcd){
			$ndatasets += keys %{$datasets{$key}};
		}
		if($key =~ /QCD/){
			$already_qcd=1;
		}
	}
	my $columns = $ndatasets + 2;

	print $fh "\\begin{sideways}\n";
	print $fh "\\begin{tabular}{";
	for(my $i = 1; $i <= $columns; $i++){
		if ($i == $columns){
			my $nstat_keys = @statistics;
			print "keys statistics = $nstat_keys";
			print "\n";
			if($nstat_keys > 0){
				print $fh "|";
				foreach $stat (@statistics){
					print $fh 'c';
				}
			}
			print $fh "|}\n";
		}elsif ($i == 1){
			print $fh "|l|";
		}else{
			print $fh "c";
		}
	}
	print_headline($fh);
	print_body($fh);
	print $fh "\\end{tabular}\n";
	print $fh "\\end{sideways}\n";
	if(!$process_preselection){
		my $preselection_line = prepare_footnote_preselection();
		print $fh $preselection_line;
	}
}

sub print_body
{
	my $fh = pop;

	print "printing body\n";
	my $datatype = '';
	# find one datatype that is to be considered
	foreach $data(keys %cuts){
		if(defined($datasets{$data})){
			$datatype = $data;
			last;
		}
	}

	my $eventtype = '';
	# find one eventtype that is to be considered
	foreach $event(keys %{$cuts{$datatype}}){
		if(defined($datasets{$datatype}{$event})){
			$eventtype = $event;
			last;
		}
	}

	print_produced_line($fh);

	print "looping over dataset $datatype eventtype $eventtype\n";
	foreach $cutset (sort keys %{$cuts{$datatype}{$eventtype}}){
		print "processing cutset $cutset\n";
		print_line($fh, $datatype, $eventtype, $cutset);
	}
}

sub print_produced_line
{
	my ($fh) = @_;

	print $fh "(s0)";

	my $counter = 1;
	foreach $dataset(keys %datasets){
		my $events = 0;
		if($dataset eq $signal_dataset){
			$events = $integrated_luminosity*$cross_section{$dataset};
			my $signal_fraction = 0;
			if($lepton eq 'mu'){
				$signal_fraction = get_ttbar_fraction("muon");
			}elsif($lepton eq 'e'){
				$signal_fraction = get_ttbar_fraction("electron");
			}else{
				die "ERROR: print_produced_line(): can't find signal lepton\n";
			}
			my $signal = int(0.5+$events*$signal_fraction);
			my $background = int(0.5+$events*(1-$signal_fraction));
			print $fh " & ".$signal." & ".$background;
		}
		else{
			$events = $integrated_luminosity*$cross_section{$dataset};
			print $fh " & ".$events;
		}
		$counter++;
	}

	my $old_counter = $counter;
        my $ndatasets = keys %datasets;
	while($counter < $ndatasets+@statistics){
		print $fh ' & ';
		$counter++;
	}
	print $fh '\\\\'."\n";
	print $fh '\\hline'."\n";	
}

sub print_line
{
	my ($fh, $datatype, $eventtype, $cutset) = @_;

	my @applied_cuts = find_applied_cuts($datatype, $eventtype, $cutset);

	my $counter = 0;
	if(defined(@cutnames)){
		$cutname = shift(@cutnames);
		if($cutname eq 'xxx'){
			return;
		}
		print $fh $cutname.' & ';
	}else{
		foreach my $cut(@applied_cuts){
			if(@applied_cuts == 0){
				print $fh get_formatted_cut($datatype, $eventtype, $cutset, $cut);
			}elsif($counter == @applied_cuts-1){
				print $fh get_formatted_cut($datatype, $eventtype, $cutset, $cut);
				print $fh ' & ';
			}else{
				print $fh get_formatted_cut($datatype, $eventtype, $cutset, $cut);
				print $fh '\\\\';
			}
			$counter++;
		}
	}

	
	my @values_for_row=();
	my @errors_for_row=();
	my @efficiencies=();
	my $qcd_values = 0;
	my $qcd_error = 0;
	my $qcd_all = 0;
	my $processed_qcd = 0;
	foreach $dataset(keys %datasets){
		foreach $evttype (keys %{$datasets{$dataset}}){
			my ($efficiency, $number, $error) = get_number_of_events($dataset, $evttype, $cutset);
			print "number of events: $number\n";
			if($dataset !~ /QCD/){
				if($processed_qcd){
					my $qcd_efficiency = $qcd_values/$qcd_all;
					$qcd_error = sqrt($qcd_error);
					push(@values_for_row, $qcd_values);
					push(@errors_for_row, $qcd_error);
					push(@efficiencies, $qcd_efficiency);
					$processed_qcd = 0;
				}else{
					push(@values_for_row, $number);
					push(@errors_for_row, $error);
					push(@efficiencies, $efficiency);
				}
			}else{
				$qcd_values += $number;	
				$qcd_error += $error * $error;
				if($efficiency != 0){
					$qcd_all += $number/$efficiency;
				}else{
					$qcd_all = -1;
				}
				$processed_qcd=1;
			}
		}
	}
	if($processed_qcd){
		my $qcd_efficiency = $qcd_values/$qcd_all;
		$qcd_error = sqrt($qcd_error);
		push(@values_for_row, $qcd_values);
		push(@errors_for_row, $qcd_error);
		push(@efficiencies, $qcd_efficiency);
		$processed_qcd = 0;
	}

	$counter = 0;
	foreach $value(@values_for_row){
		if($counter == @values_for_row-1){
			if($show_statistic_errors){
				printf $fh "\$%.0f \\pm %.0f\$", $value, $errors_for_row[$counter];
			}else{
				printf $fh "\$%.0f\$", $value, $errors_for_row[$counter];
			}
		}else{
			if($show_statistic_errors){
				printf $fh "\$%.0f \\pm %.0f\$", $value, $errors_for_row[$counter];
			}else{
				printf $fh "\$%.0f\$", $value, $errors_for_row[$counter];
			}
			print $fh ' & ';
		}

		$counter++;
	}

	my $efficiency = $efficiencies[0];

	# for t' sum lepton-signal and other for efficiency
#	if($signal_dataset eq 'Tprime'){
#		$efficiency = ($values_for_row[0]+$values_for_row[1])/(($values_for_row[0]/$efficiencies[0]) + ($values_for_row[1]/$efficiencies[1]));
#		print "calculated efficiency: $efficiency";
#	}

	my @statistic_values = get_statistics(@values_for_row, $efficiency);	

	my $old_counter = $counter;
        my $ndatasets = 0;
        foreach $key (keys %datasets){
                $ndatasets += keys %{$datasets{$key}};
        }  
	while($counter < $ndatasets+@statistics){
		my $array_position = $counter - $old_counter;
		if($array_position < @statistic_values){
			print $fh ' & ';
			printf $fh "%6.3f",$statistic_values[$array_position];
		}else{
			print $fh ' & ';
		}
		$counter++;
	}
	print $fh '\\\\'."\n";
	print $fh '\\hline'."\n";	
}

sub get_statistics
{
	my $efficiency = pop;
	my @row_values = @_;

	my $signal_events = shift @row_values;

	# for t' also t'-other is counted as signal
#	if($signal_dataset eq 'Tprime'){
#		my $signal_events_other = shift @row_values;
#		$signal_events += $signal_events_other;
#	}

	my $background_events = 0;
	foreach $background (@row_values){
		$background_events += $background;
	}

	print "signal: $signal_events, background: $background_events\n";
	
	my @statistic_values=();
	unless($background_events == 0){
		my $signal_over_background = $signal_events / $background_events;
		my $purity = $signal_events / ($signal_events + $background_events);

		foreach $statistic (@statistics){
			if($statistic eq "signal_over_background"){
				push(@statistic_values, $signal_over_background);
			}elsif($statistic eq "purity_x_efficiency"){
				my $purity_x_efficiency = $purity*$efficiency;
				push(@statistic_values, $purity_x_efficiency);
			}elsif($statistic eq "purity_x_efficiency2"){
				my $purity_x_efficiency2 = ($purity*$efficiency)/(2-$purity);
				push(@statistic_values, $purity_x_efficiency2);
			}elsif($statistic eq "purity"){
				push(@statistic_values,$purity);
			}elsif($statistic eq "efficiency"){
				push(@statistic_values,$efficiency);
			}
		}
	}

	return @statistic_values;
}

sub get_number_of_events
{
	my($dataset, $evttype, $cutset) = @_;

	my $nevents_passed_cuts = $cuts{$dataset}{$evttype}{$cutset}{'cuts_passed'};
	my $nevents_not_passed_cuts = $cuts{$dataset}{$evttype}{$cutset}{'cuts_not_passed'};

	# scale events for integrated luminosity
	return scale_events($nevents_not_passed_cuts, $nevents_passed_cuts, $dataset, $evttype);
}

sub scale_events
{
	my ($nevents_not_passed_cuts, $nevents_passed_cuts, $dataset, $evttype) = @_;
	my $sum_events = $nevents_not_passed_cuts + $nevents_passed_cuts;

	return 0 if($sum_events == 0);

	my $scaled_events=-1;
	my $scaled_error=-1;

	print "scaling for dataset $dataset\n";
	
	my $cross_section = -1;
	if(defined($cross_section{$dataset})){
		my $filter_eff = 0;
		if($dataset eq $signal_dataset && $evttype eq 'muon'){
			$filter_eff = $filter_efficiency{$dataset."_muon"};
		}elsif($dataset eq $signal_dataset && $evttype eq 'electron'){
			$filter_eff = $filter_efficiency{$dataset."_electron"};
		}elsif($dataset eq $signal_dataset && $evttype eq 'e_background'){
			$filter_eff = $filter_efficiency{$dataset."_e_background"};
		}elsif($dataset eq $signal_dataset && $evttype eq 'mu_background'){
			$filter_eff = $filter_efficiency{$dataset."_mu_background"};
		}
		else{
			$filter_eff = $filter_efficiency{$dataset};
		}
		$cross_section = $cross_section{$dataset} * $filter_eff;
	}

	if($cross_section != -1){
		my $scaling_factor = $integrated_luminosity * $cross_section;
		$scaled_events = ($scaling_factor / $sum_events) * $nevents_passed_cuts;
		if($nevents_passed_cuts == 0){
			$scaled_error = 0;
		}else{
			$scaled_error = sqrt($nevents_passed_cuts)/$sum_events * $scaling_factor;
		}
	}else{
		$scaled_events = $nevents_passed_cuts;
		$scaled_error = sqrt($cuts_passed);
		print "WARNING: THE EVENTS ARE NOT SCALED FOR THE CROSS SECTION IN $dataset\n";
	}

	my $efficiency = $nevents_passed_cuts / $sum_events;

	if($dataset eq $signal_dataset){
#		my $ttbar_sum = $ttbar_counter{'muon'} + $ttbar_counter{'electron'} + $ttbar_counter{'background'};
#		if($evttype eq 'muon'){
#			$fraction = $ttbar_counter{'muon'}/$ttbar_sum;
#		}elsif($evttype eq 'electron'){
#			$fraction = $ttbar_counter{'electron'}/$ttbar_sum;
#		}elsif($evttype eq 'mu_background'){
#			$fraction = ($ttbar_counter{'electron'} + $ttbar_counter{'background'})/$ttbar_sum;
#		}elsif($evttype eq 'e_background'){
#			$fraction = ($ttbar_counter{'muon'} + $ttbar_counter{'background'})/$ttbar_sum;
#		}

		my $fraction = get_ttbar_fraction($evttype);

		$scaled_events *= $fraction;
		if($cross_section != -1){
			if($nevents_passed_cuts == 0){
				$scaled_error = 0;
			}else{
				my $scaling_factor = $integrated_luminosity * $cross_section;
				$scaled_error = sqrt($nevents_passed_cuts)/($sum_events) * $scaling_factor * $fraction;
			}
		}
	}
	print "$dataset/$evttype: events: $scaled_events, error: $scaled_error\n";

	return ($efficiency, $scaled_events, $scaled_error);
}

sub get_formatted_cut
{
	my ($datatype, $eventtype, $cutset, $cut) = @_;
	
	my $formatted_cut = '';
	if(defined($dictionary{$cut})){
		$formatted_cut = '$'.$dictionary{$cut}." ".$cuts{$datatype}{$eventtype}{$cutset}{$cut}.'$';
	}else{
		$formatted_cut = '$'.$cut." ".$cuts{$datatype}{$eventtype}{$cutset}{$cut}.'$';
	}

	return $formatted_cut;
}

sub find_applied_cuts
{
	my ($datatype, $eventtype, $cutset) = @_;

	my %applied_cuts_hash = %{$cuts{$datatype}{$eventtype}{$cutset}};

	my @applied_cuts = ();
	while( my ($cut, $value) = each %applied_cuts_hash ){
		if($value == -1){
			delete $applied_cuts_hash{$cut};
		}elsif(defined($skip_comparison{$cut})){
			delete $applied_cuts_hash{$cut};
		}elsif(defined($cumulative_selection{$cut}) and
			($cumulative_selection{$cut} eq $applied_cuts_hash{$cut})){
			delete $applied_cuts_hash{$cut};
		}else{
			push(@applied_cuts, $cut);
		}
	}

	push(@applied_cuts, "Preselection") if(@applied_cuts == 0);

	print "applied cuts:\n";
	foreach my $cut(@applied_cuts){
		if($process_preselection){
			$cumulative_selection{$cut} = $cuts{$datatype}{$eventtype}{$cutset}{$cut};
		}
		print $cut.": ".$cuts{$datatype}{$eventtype}{$cutset}{$cut}."\n";
	}
	print "-------------\n";

	return @applied_cuts;
}

sub print_headline
{
	my $fh = pop;

	print $fh "\\hline\n";
	foreach my $datatype (keys %datasets){
		foreach my $evttype (keys %{$datasets{$datatype}}){
			if(defined($dictionary{$datatype.'_'.$evttype})){
				print $fh " & $dictionary{$datatype.'_'.$evttype}";
			}else{
				print $fh " & ".$datatype."_".$evttype;
			}
		}
	}
	foreach my $stat (@statistics){
		if(defined($dictionary{$stat})){
			print $fh " & $dictionary{$stat}";
		}else{
			print $fh " & $stat";
		}
	}
	print $fh "\\\\\n\\hline\n\\hline\n";
}

sub print_header
{
	my $filehandle = pop;
	print $filehandle <<EOF;
\\documentclass[a4paper,10pt]{article}
\\usepackage{rotating}
\\title{t\\bar{t} selection}
\\begin{document}
EOF
}

sub print_closing
{
	my $filehandle = pop;
	print $filehandle <<EOF;
\\end{document}
EOF
}

sub analyse_file
{
	my $file = pop;
	my $dir = pop;
	open(FILE, "<$dir/$file") or die "cannot open $file: $!";

	my $read_cuts=0;
	my $read_ttbar_counter=0;
	my @block = ();
	while (defined (my $line = <FILE>)) {
		if($line =~ m/=-----------IMPOSED-CUTS-----------=/){
			$read_cuts = 1;
		}elsif($line =~ m/=----------------------------------=/)
		{
			analyse_block(@block);
			@block=();
			$read_cuts=0;
		}else{
			push(@block, $line) if($read_cuts);
		}

		if($line =~ m/^=\+{10}Event-Count:$signal_dataset\+{10}=$/)
		{
			$read_ttbar_counter = 1;
		}elsif($line =~ m/^=\+{30}=$/){
			$read_ttbar_counter = 0;
		}
		else{
			if($read_ttbar_counter){
				if($line =~ m/ttbar_mu_events: (\d+)/){
					$ttbar_counter{"muon"} =
					$ttbar_counter{'muon'} + $1;
				}
				if($line =~ m/ttbar_e_events: (\d+)/){
					$ttbar_counter{"electron"} =
					$ttbar_counter{'electron'} + $1;
				}
				if($line =~ m/ttbar_bg_events: (\d+)/){
					$ttbar_counter{"background"} =
					$ttbar_counter{'background'} + $1;
				}
			}
		}
	}	
	close(FILE);
}

sub analyse_block
{
	my @block = @_; 
	my $datatype, $eventtype, $cutset;
	foreach my $blockline(@block){
		@parts = split(/: /, $blockline);
		if(@parts == 2){
			chomp $parts[1];
			if($parts[0] eq 'identifier'){
				($datatype, $eventtype, $cutset) = split(/\|/, $parts[1]);
			}else{
				my $cut = $parts[0];
				if($cut eq 'cuts_passed' ||
					$cut eq 'cuts_not_passed'){
					my $new_value = $cuts{$datatype}{$eventtype}{$cutset}{$cut} + $parts[1];
					$cuts{$datatype}{$eventtype}{$cutset}{$cut} = $new_value;
				}else{
					$cuts{$datatype}{$eventtype}{$cutset}{$cut}=$parts[1];
				}
			}
		}else{
			print "WARNING: could not split $blockline in analyse_block()\n";
		}
	}
}

sub prepare_footnote_preselection
{
	my $footnote = "\\\\\nPreselection: ";
	while(my ($cut, $value) = each %cumulative_selection){
		$footnote .= '$'.$dictionary{$cut}.$value.'$, ';
	}

	$footnote .= "\n\\\\";
	$footnote .= 'S/B: [$t\'$(signal) + $t\'$(other)]/background';
	$footnote .= "\n\\\\";
	$footnote .= 'efficiency \& purity: only $t\'$ (signal) considered as signal, everything else (including $t\'$ (other)) considered as background';
	$footnote .= "\n";

	return $footnote;
}

sub get_ttbar_fraction
{
	my $evttype = shift @_;

	my $ttbar_sum = $ttbar_counter{'muon'} + $ttbar_counter{'electron'} + $ttbar_counter{'background'};
	$ttbar_sum = -1 if($ttbar_sum == 0);
	print "WARNING: ttbarsum == 0 in get_ttbar_fraction\n" if($ttbar_sum == -1);
	if($evttype eq 'muon'){
		$fraction = $ttbar_counter{'muon'}/$ttbar_sum;
	}elsif($evttype eq 'electron'){
		$fraction = $ttbar_counter{'electron'}/$ttbar_sum;
	}elsif($evttype eq 'mu_background'){
		$fraction = ($ttbar_counter{'electron'} + $ttbar_counter{'background'})/$ttbar_sum;
	}elsif($evttype eq 'e_background'){
		$fraction = ($ttbar_counter{'muon'} + $ttbar_counter{'background'})/$ttbar_sum;
	}

	return $fraction;
}
