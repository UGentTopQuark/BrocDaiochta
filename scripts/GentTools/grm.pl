#!/usr/bin/perl

use File::Find;
use File::Spec;

die "usage: $0 [-f] <dir/file to delete>" unless(@ARGV);

$forced_delete=1 if($ARGV[0] eq "-f");

my $se_path = 'srm://maite.iihe.ac.be:8443';
my $srmrm = 'srmrm';
my $srmrmdir = 'srmrmdir';
my $grid_se = '/pnfs/iihe';

print "delete following files:\n";

find {
	by_depth => 1,
	no_chdir => 1,
	wanted   => sub{
		if( !-l && -d _){
			push(@dir_to_del, "$se_path/".File::Spec->rel2abs($_));
			print File::Spec->rel2abs($_)."\n";
		} else {
			push(@file_to_del, "$se_path/".File::Spec->rel2abs($_));
			print File::Spec->rel2abs($_)."\n";
		}
	}
} => @ARGV;

unless($forced_delete){
print "Are you sure? [y/N] ";

$answer = <STDIN>;
chomp $answer;
}

if($forced_delete || $answer eq 'y' || $answer eq 'Y'){
	print "deleting...\n";
	foreach(@file_to_del){
		print "$srmrm $_\n";
		`$srmrm $_`;
	}
	foreach(reverse @dir_to_del){
		print "$srmrmdir $_\n";
		`$srmrmdir $_`;
	}
}else{
	print "Aborted.\n";
}
