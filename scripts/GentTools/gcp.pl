#!/usr/bin/perl

use File::Find;
use File::Spec;
use Cwd;
my $cur_dir = getcwd;

die "usage: $0 <source> <destination>" unless(@ARGV == 2);

my $se_path = 'srm://maite.iihe.ac.be:8443';
my $srmcp = 'srmcp';
my $mkdir = 'mkdir -p';

my $from = $ARGV[0];
my $to = pop @ARGV;

my @to_dirs = ();
my $from_postfix;
if(-d $from){
	my $abs_from = File::Spec->rel2abs($from);
	if($abs_from =~ /.*\/(.*)\/$/){
		$from_postfix = $1;
	}elsif($abs_from =~ /.*\/(.*)$/){
		$from_postfix = $1;
	}
	push(@to_dirs, $from_postfix);
	print "postfix: $from_postfix\n";
}

print "copy following files:\n";

find {
        by_depth => 1,
        no_chdir => 1,
        wanted   => sub{
                if( !-l && -d _){
			my $dir = File::Spec->rel2abs($_);
			$dir = "$se_path/".$dir if(/$grid_se/);
                        push(@from_dirs, $dir);
			$dir =~ /$from(.*)/;
                        push(@to_dirs, $from_postfix."/".$1) if($1);
                        print "from: $dir\n";
                        print "to: $1\n";
                } else {
			my $file = File::Spec->rel2abs($_);
			$file = "$se_path/".$file if(/$grid_se/);
                        push(@from_files, $file);
			my $postfix;
			if(-d $from){
				$file =~ /$from(.*)/;
				$postfix = $1;
			}else{
				$from =~ /.*\/(.*)$/;
				$postfix = $1;
			}
			$to_file = "file:///".File::Spec->rel2abs($to)."/$from_postfix/$postfix";
                       	push(@to_files, $to_file);
                        print "from: $file\n";
                        print "to: $to_file\n";
                }
        }
} => $ARGV[0];

print "destination: $to/$from_postfix\n";
print "Are you sure? [y/N] ";

$answer = <STDIN>;
chomp $answer;

if($answer eq 'y' || $answer eq 'Y'){
	foreach(reverse @to_dirs){
		print "$mkdir $to/$_\n";
		`$mkdir $to/$_`;
	}
	my $i = 0;
	foreach(@from_files){
		print "$srmcp $_ $to_files[$i]\n";
		`$srmcp $_ $to_files[$i]`;
		$i++;
	}
}else{
	        print "Aborted.\n";
}
