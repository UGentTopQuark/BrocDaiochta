#!/usr/bin/perl

die "usage: $0 production.dbs [outdir]" unless(@ARGV > 0);

$infile = $ARGV[0];
die "no production.dbs file: $infile\n" unless(-e $infile and -f $infile);
$outdir = "." unless($ARGV[1]);
$outdir = $ARGV[1] if($ARGV[1]);

open(INFILE, "<$infile") or die "could not open infile: $infile\n";
while(defined(my $line = <INFILE>)){
	push(@header, $line) and next unless($line =~ /(.*)\_job_\d+_.*\.root\s*=\s*(\d+)/);
	push(@{$datasets{$1}}, $line);
	$nevents{$1} += $2;
}
close(INFILE);

s/srm:\/\/maite\.iihe\.ac\.be:8443\//dcap:\/\/\// foreach(@header);

while((my $dataset, my $lines) = each %datasets){
	open(OUTFILE,">$outdir/$dataset.dbs");
	s/events\s*=\s*\d+/events = $nevents{$dataset}/ foreach(@header);
	print OUTFILE @header;
	print OUTFILE foreach(@{$lines});
	close(OUTFILE);
}
