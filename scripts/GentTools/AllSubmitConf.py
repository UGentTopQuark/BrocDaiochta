#! /usr/bin/python
# executes crab -create -submit for all crab*.cfg files in EventSelection

import os, sys, tempfile, shutil, fileinput, string, datetime, re


def ID_dataset(njobs = 0,evnts_per_job = 0):
    
##     dslist = ('/TauolaTTbar/Summer08_IDEAL_V9_PAT_v2/USER','/WJets-madgraph/Summer08_IDEAL_V9_PAT_v4/USER','/ZJets-madgraph/Summer08_IDEAL_V9_PAT_v3/USER','/QCD_BCtoMu_Pt20/Summer08_IDEAL_V9_PAT_v1/USER','/QCD_BCtoMu_Pt30to50/Summer08_IDEAL_V9_PAT_v1/USER','/QCD_BCtoMu_Pt50to80/Summer08_IDEAL_V9_PAT_v1/USER','/TTJets-madgraph/Summer08_IDEAL_V9_PAT_v4/USER','/InclusiveMuPt15/Summer08_IDEAL_V9_PAT_v2/USER')
##     numlist = (100000, 10000000,1000000,10000000,6500000,2500000,2044000,6186841)
#Total number of events in each dataset from
  ##   dslist = ('/WJets-madgraph/Summer08_IDEAL_V9_PAT_v4/USER','/ZJets-madgraph/Summer08_IDEAL_V9_PAT_v3/USER','/TTJets-madgraph/Summer08_IDEAL_V9_PAT_v4/USER','/InclusiveMuPt15/Summer08_IDEAL_V9_PAT_v2/USER')

    #numbers must correspond with dslist defined in Running over conf files
    numlist = (9920335,1151897,2044844,6186841)
    
    sizeDataset = numlist[ds_i]
    print ('found dataset with events', sizeDataset)
    
    #calculate number of events you want to submit
    Num_to_process = int(sizeDataset) * int(percent)/100

    #If running over < 500,000 events there will be 5000 events per job
    evnts_per_job = 50000
    njobs = Num_to_process/evnts_per_job
    if njobs == 0:
        njobs = 1
    #If total number of events > 500,000 will only submit 100 jobs to save time on srmcp unless...
    if njobs > 100:
        #..unless this would make more than 50,000 events per job which might take too long on the grid. In this case there will be 50,000 events per job and however many jobs needed.
        evnts_per_job = Num_to_process/100
        if evnts_per_job > 100000:
            evnts_per_job = 100000
            njobs = Num_to_process/evnts_per_job
        else:
            njobs = 100
        
        
        
    return njobs,evnts_per_job

#Destroy then create proxy
sts = os.system('voms-proxy-destroy')
sts = os.system('voms-proxy-init --voms cms:/cms/becms --hours 99')


#get date and time to append to new directories
date_time = str(datetime.datetime.now())
p = re.compile('(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}).*')
date_time = p.sub(r'\1\2\3_\4\5', date_time, 1)
print date_time

#Get percent of events in dataset you want to run over from user
percent = raw_input('What percent of each dataset would you like to run over ')
## if percent is not >= 1 or percent is not <=100:
##     sys.exit('Error:percent must be between 1 and 100.Program stopped')
    

################################Directories used###################################

#finds path to python file, removes /src and everything after it
pathto_CMSSW,pathafter_src =  os.getcwd().split("/src")
#print pathto_CMSSW, 'and', pathafter_src

#folder containing conf files.temp files will be put here too
folder = str(os.path.join(pathto_CMSSW, 'src/grid-control'))
#Appends folder to list of directories python searches through
sys.path.append(folder)

#folder containing go.py file
folder_go =  str(os.path.join(pathto_CMSSW, 'src/grid-control'))

if 'walsh' in str(folder):
    user = 'walsh'
if 'bklein' in str(folder):
    user = 'bklein'

#Storage element parent directory
se_parentdir = str(('srm://maite.iihe.ac.be:8443/pnfs/iihe/cms/store/user/'+user+'/'))

######################################################################################

###########################Running over .conf file####################################
###Preparation and calculation###


#A conf and py file will be created for every dataset listed here
dslist = ('/WJets-madgraph/Summer08_IDEAL_V9_PAT_v4/USER','/ZJets-madgraph/Summer08_IDEAL_V9_PAT_v3/USER','/TTJets-madgraph/Summer08_IDEAL_V9_PAT_v4/USER','/InclusiveMuPt15/Summer08_IDEAL_V9_PAT_v2/USER')
ds_short = ('Wjets','Zjets','TTbar','Mupt15')

#if numjobs ==0 want to skip dataset
Skip_script = 0
ds_i = 0
size = len(dslist)
#Match .conf file dataset to one of those above
for ds_i in xrange(size):
    Dataset_Name = dslist[ds_i]
    
    
    #calculate number of events 
    numjobs,events_per_job = ID_dataset()
    
    #Skip configuration file if no matching dataset is found
    if numjobs == 0:
        print 'dataset did not match any defined. Skipping this dataset file:', Dataset_Name
        Skip_script = 1
        
    #Skip this loop of dataset number not available     
    if Skip_script == 1:
        continue
    
    #Create lines for jobs and events to be written to tmp file
    Dataset_line = ('dataset = ' + str(Dataset_Name) + '\n')
    Num_evnts_line = ('events per job = ' + str(events_per_job) + '\n')
    Num_jobs_line = ('jobs = ' + str(numjobs)+ '\n')
    In_flight_line = ('in flight = '+str(numjobs)+ '\n')
    print Num_jobs_line

###Create temps and open file###
    
    tmp_prefix =('gtmp_'+ds_short[ds_i]) 
    #generate tmp .conf file to write lines to
    num, abs_path = tempfile.mkstemp(suffix = '.conf',prefix = tmp_prefix, dir = folder, text = True)    
    #print(abs_path)
    
    tmp_prefix_py =('gpy_'+ds_short[ds_i]) 
    #generate tmp .py file to write lines to
    num_py, abs_path_py = tempfile.mkstemp(suffix = '_cfg.py',prefix = tmp_prefix_py, dir = folder, text = True)    
    #print(abs_path) 

    script = 'all.conf'
    path_to_file = os.path.join(folder,script)
    tmp_file = open(abs_path, 'w')
    fr_conf = open(path_to_file)
    #this i is used to check how many instances of jobs are found in file.Only want the second instant
    i = 0
    #i_dir checks for first instance of dir in file
    i_dir = 0


    ###Pass lines from original file to temp###
   
    #Loop through conf file and write all lines to tmp file
    for line in fr_conf:
        
        #Dont modify line if its a comment. Check first character is ;
        if line[0] is ';':
            tmp_file.write(line)
            continue
        
        if 'dir' in line and 'workdir' not in line:
            if i_dir == 0:
                line = ('dir = '+folder+'\n')
                tmp_file.write(line)
                i_dir = i_dir + 1
                continue
            else:
                i_dir = i_dir + 1
                tmp_file.write(line)
                continue
            
        if "workdir" in line:
            
            line = string.rstrip(line)
            line = (line + ds_short[ds_i]+ '_' + date_time+ '\n')
            base_line = os.path.basename(line)
            # first need to create local directory for stdout stderr
            sts = os.system('mkdir '+folder+'/'+base_line)
            tmp_file.write(line)
            continue
        
        if 'jobs'in line:
            #jobs = ? should be second occurence of jobs in .conf file
            if i is not 1:
                tmp_file.write(line)
                i = i+1
                continue
            
            tmp_file.write(Num_jobs_line)
            i = i+1
            continue
        
        if "in flight" in line:
            tmp_file.write(In_flight_line)
            i = i+1
            continue 
        
        
        if 'project area' in line and 'config file' not in line:
            line = ('project area = '+pathto_CMSSW+ '\n')
            tmp_file.write(line)
            continue
        
        if 'dataset' in line: 
            tmp_file.write(Dataset_line)
            continue    
        
        if 'config file' in line:
            line = string.rstrip(line)
            tmp_py_name = os.path.basename(abs_path_py)
            line = (line +tmp_py_name + '\n')
            tmp_file.write(line)
            continue
        
        
        if 'events per job' in line: 
            tmp_file.write(Num_evnts_line)
            continue
        
                
        if 'se output files' in line:
            line = ('se output files = ' + ds_short[ds_i]+ '.root \n')
            tmp_file.write(line)
            continue
        
        
        if "se path" in line:
            
            line = string.rstrip(line)
            line = (line +ds_short[ds_i]+ '_' + date_time+ '\n')
            se_base_line = os.path.basename(line)
            #Need to create SE directory for root files
            sts = os.system('srmmkdir '+se_parentdir+se_base_line)
            tmp_file.write(line)
            continue
        
        else: 
            tmp_file.write(line)
            
            
    fr_conf.close()
    tmp_file.close()

###########################Running over .py file###################################
            
    #.py tmp file was created at same time as .conf file
    script_py = 'all_cfg.py'
    path_to_file_py = os.path.join(folder,script_py)
            
    tmp_file_py = open(abs_path_py, 'w')
    fr_py = open(path_to_file_py)

     #Loop through conf file and write all lines to tmp file
    for line in fr_py:
        
        #Dont modify line if its a comment. Check first character is #
        if line[0] is '#':
            tmp_file_py.write(line)
            continue

        if 'datasetName = cms.untracked.string' in line:
            line = ('datasetName = cms.untracked.string(\"' + ds_short[ds_i]+ '\"), \n')
            tmp_file_py.write(line)
            continue
        
        if 'process.TFileService = cms.Service' in line:
            line = ('process.TFileService = cms.Service(\"TFileService\", fileName = cms.string(\'' + ds_short[ds_i]+ '.root \')) \n')
            tmp_file_py.write(line)
            continue
        

        else: 
            tmp_file_py.write(line)

    fr_py.close()
    tmp_file_py.close()


                    
    #submit job
    sts = os.system(folder_go+'/go.py -i '+ abs_path)
    print abs_path
    #sts = os.system('less '+ abs_path)
    #Don´t remove tmp folder while program running
    #os.remove(abs_path)
                
                
                
                #sts = os.system("crab -cfg "+ abs_path +' -create')
                
               
