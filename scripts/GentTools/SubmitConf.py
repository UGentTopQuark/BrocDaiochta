#! /usr/bin/python
# executes crab -create -submit for all crab*.cfg files in EventSelection

import os, sys, tempfile, shutil, fileinput, string, datetime, re


def ID_dataset(njobs = 0,evnts_per_job = 0):
#Total number of events in each dataset from
    dslist = ('/TauolaTTbar/Summer08_IDEAL_V9_PAT_v2/USER','/WJets-madgraph/Summer08_IDEAL_V9_PAT_v4/USER','/ZJets-madgraph/Summer08_IDEAL_V9_PAT_v3/USER','/QCD_BCtoMu_Pt20/Summer08_IDEAL_V9_PAT_v1/USER','/QCD_BCtoMu_Pt30to50/Summer08_IDEAL_V9_PAT_v1/USER','/QCD_BCtoMu_Pt50to80/Summer08_IDEAL_V9_PAT_v1/USER','/TTJets-madgraph/Summer08_IDEAL_V9_PAT_v4/USER','/InclusiveMuPt15/Summer08_IDEAL_V9_PAT_v2/USER')
    numlist = (100000,9920335,1151897,10000000,6500000,2500000,2044844,6186841)
    i = 0
    size = len(dslist)
    #Match .conf file dataset to one of those above
    for i in xrange(size):
       
        if dslist[i] in line:
            sizeDataset = numlist[i]
            print ('found dataset with events', sizeDataset)

            #calculate number of events you want to submit
            Num_to_process = int(sizeDataset) * int(percent)/100

           #If running over < 500,000 events there will be 5000 events per job
            evnts_per_job = 50000
            njobs = Num_to_process/evnts_per_job
            if njobs == 0:
                njobs = 1
            #If total number of events > 500,000 will only submit 100 jobs to save time on srmcp unless...
            if njobs > 100:
                #..unless this would make more than 50,000 events per job which might take too long on the grid. In this case there will be 50,000 events per job and however many jobs needed.
                evnts_per_job = Num_to_process/100
                if evnts_per_job > 100000:
                    evnts_per_job = 100000
                    njobs = Num_to_process/evnts_per_job
                else:
                    njobs = 100
            else:
                 njobs = njobs

            break

   
    return njobs,evnts_per_job

#Destroy then create proxy
sts = os.system('voms-proxy-destroy')
sts = os.system('voms-proxy-init --voms cms:/cms/becms --hours 99')


#get date and time to append to new directories
date_time = str(datetime.datetime.now())
p = re.compile('(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}).*')
date_time = p.sub(r'\1\2\3_\4\5', date_time, 1)
print date_time

#Get percent of events in dataset you want to run over from user
percent = raw_input('What percent of each dataset would you like to run over ')
## if percent is not >= 1 or percent is not <=100:
##     sys.exit('Error:percent must be between 1 and 100.Program stopped')
    

################################Directories used###################################

#finds path to python file, removes /src and everything after it
pathto_CMSSW,pathafter_src =  os.getcwd().split("/src")
#print pathto_CMSSW, 'and', pathafter_src

#folder containing conf files.temp files will be put here too
folder = str(os.path.join(pathto_CMSSW, 'src/grid-control'))
#Appends folder to list of directories python searches through
sys.path.append(folder)

#folder containing go.py file
folder_go =  str(os.path.join(pathto_CMSSW, 'src/grid-control'))

if 'walsh' in str(folder):
    user = 'walsh'
if 'bklein' in str(folder):
    user = 'bklein'

#Storage element parent directory
se_parentdir = str(('srm://maite.iihe.ac.be:8443/pnfs/iihe/cms/store/user/'+user+'/'))

######################################################################################

for script in os.listdir(folder):
    Skip_script = 0
    #loop through .conf files but not previously creates tmp files
    if script.endswith(".conf") and script.startswith('g') is False and script.endswith('QCD.conf') is False and script.endswith('all.conf') is False:
        
        print "Found a file", script
        path_to_file = os.path.join(folder,script)
                  
        fr = open(path_to_file)
       
       #First loop over file to find dataset and calculate number of events    
        for line in fr:
            #skip comments
            if line[0] is ';':
                continue
    
            numjobs,events_per_job = ID_dataset()
            
            if numjobs != 0:
                break
            #Skip configuration file if no matching dataset is found
            if fr.read() is '' and numjobs == 0:
                print 'Conf file dataset did not match any in python file. Skipping this conf file:', script
                Skip_script = 1

                
        if Skip_script == 1:
            continue

        #Create lines for jobs and events to be written to tmp file
        Num_evnts_line = ('events per job = ' + str(events_per_job) + '\n')
        Num_jobs_line = ('jobs = ' + str(numjobs)+ '\n')
        In_flight_line = ('in flight = '+str(numjobs)+ '\n')
        print Num_jobs_line

        #generate tmp file to write lines to
        num, abs_path = tempfile.mkstemp(suffix = script,prefix = 'gtmp', dir = folder, text = True)    
        #print(abs_path)       

        tmp_file = open(abs_path, 'w')
        fr = open(path_to_file)
        #this i is used to check how many instances of jobs are found in file.Only want the second instant
        i = 0
        #i_dir checks for first instance of dir in file
        i_dir = 0

        #Loop through conf file and write all lines to tmp file
        for line in fr:

            #Dont modify line if its a comment. Check first character is ;
            if line[0] is ';':
                 tmp_file.write(line)
                 continue

            if 'dir' in line and 'workdir' not in line:
                if i_dir == 0:
                    line = ('dir = '+folder+'\n')
                    tmp_file.write(line)
                    i_dir = i_dir + 1
                    continue
                else:
                    i_dir = i_dir + 1
                    tmp_file.write(line)
                    continue
                   
                   
            if 'project area' in line and 'config file' not in line:
                line = ('project area = '+pathto_CMSSW+ '\n')
                tmp_file.write(line)
                continue

            if "in flight" in line:
                tmp_file.write(In_flight_line)
                i = i+1
                continue   

            
            if "workdir" in line:
               
                line = string.rstrip(line)
                line = (line + '_' + date_time+ '\n')
                base_line = os.path.basename(line)
               #first need to create local directory for stdout stderr
                sts = os.system('mkdir '+folder+'/'+base_line)
                tmp_file.write(line)
                continue
                       
            if "se path" in line:
               
                line = string.rstrip(line)
                line = (line + '_' + date_time+ '\n')
                se_base_line = os.path.basename(line)
               #Need to create SE directory for root files
                sts = os.system('srmmkdir '+se_parentdir+se_base_line)
                tmp_file.write(line)
                continue

            if 'jobs'in line:
                #jobs = ? should be second occurence of jobs in .conf file
                if i is not 1:
                    tmp_file.write(line)
                    i = i+1
                    continue
              
                tmp_file.write(Num_jobs_line)
                i = i+1
                continue
            
            if 'events per job' in line: 
                tmp_file.write(Num_evnts_line)
                continue

            else: 
                tmp_file.write(line)

        
        fr.close()
        tmp_file.close()
 
        #submit job
        #sts = os.system(folder_go+'/go.py -i '+ abs_path)
        print abs_path
        #sts = os.system('less '+ abs_path)
        #Don´t remove tmp folder while program running
        #os.remove(abs_path)
                
                
                
                #sts = os.system("crab -cfg "+ abs_path +' -create')
                
               
