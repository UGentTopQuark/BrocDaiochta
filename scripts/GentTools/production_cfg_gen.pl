#!/usr/bin/perl

$cmssw_cfg_mask = 'PYTHIA6_Tprime_MASSGeV_10TeV_GEN_SIM_DIGI_L1_DIGI2RAW_HLT_IDEAL.py';
$root_outfile = 'PYTHIA6_Tprime_MASSGeV_10TeV_GEN_SIM_DIGI_L1_DIGI2RAW_HLT_IDEAL.root';
$crab_cfg_mask = 'crab_MASS.cfg';

@masses = (250, 275, 300, 325, 350, 375, 400);

foreach my $mass (@masses){
	create_new_cmssw_cfg($mass);
	create_new_crab_cfg($mass);
}

sub create_new_crab_cfg
{
	my $mass = pop;

	my $cmssw_cfg_string = $cmssw_cfg_mask;
	$cmssw_cfg_string =~ s/MASS/$mass/g;
	my $new_cfg_name = $crab_cfg_mask;
	$new_cfg_name =~ s/MASS/$mass/g;
	my $outfile_string = $root_outfile;
	$outfile_string =~ s/MASS/$mass/g;

	open(CRAB_CFG, "<$crab_cfg_mask") or die "could not open crab config\n";
	open(CONFIG, ">$new_cfg_name");
	while(defined(my $line = <CRAB_CFG>)){
		$line =~ s/__CMSSW_CONFIG__/$cmssw_cfg_string/;
		$line =~ s/__OUTFILE__/$outfile_string/;
		$line =~ s/__MASS__/$mass/;
		print CONFIG $line;
	}
	close(CONFIG);
	close(CRAB_CFG);
}

sub create_new_cmssw_cfg
{
	my $mass = pop;

	my $cmssw_cfg_string = $cmssw_cfg_mask;
	$cmssw_cfg_string =~ s/MASS/$mass/g;

	my $outfile_string = $root_outfile;
	$outfile_string =~ s/MASS/$mass/g;
	$outfile_string = '\''.$outfile_string.'\'';

	# write out new config based on cmssw_cfg_mask
	open(CMSSW_CFG, "<$cmssw_cfg_mask") or die "could not open cmssw config\n";
	open(CONFIG, ">$cmssw_cfg_string");
	while(defined(my $line = <CMSSW_CFG>)){
		$line =~ s/__OUTFILE__/$outfile_string/;
		$line =~ s/__MASS__/$mass/;
		print CONFIG $line;
	}
	close(CONFIG);
	close(CMSSW_CFG);
}
