#/bin/bash

CURRENT_PATH=`pwd`;
for i in `cat datasets.txt`; do
	cd ${CURRENT_PATH}/${i};
	for j in `ls -d crab_0_*`; do
		echo "checking job status...";
		crab -status -continue ${j}
	done
	for j in `ls -d crab_0_*`; do
		echo "getting job output...";
		crab -getoutput -continue ${j}
	done
done
