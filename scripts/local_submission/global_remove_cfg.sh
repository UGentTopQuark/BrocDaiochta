#!/bin/bash

for i in `cat datasets.txt`; do
	cd ${i}; rm *.root *_crab.cfg *_eventselection_cfg.py *_eventselection_cfg.pyc; cd ..;
done
