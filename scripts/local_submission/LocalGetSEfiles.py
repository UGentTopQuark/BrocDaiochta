#! /usr/bin/python
#Gets files from iihe storage element

#Subprocess needs at least python 2.4. Check if CMSSW loaded
import os, sys, subprocess

hadd_path = '/swmgrs/cmss/slc4_ia32_gcc345/lcg/root/5.18.00a-cms17/bin/hadd'
#hadd_num = os.access(hadd_path,os.X_OK)
hadd_num = subprocess.call('hadd', shell=True)
if hadd_num is not 1:
     sys.exit('Program stopped: hadd not available')


#finds path to python file, removes /src and everything after it
pathto_CMSSW,pathafter_src =  os.getcwd().split("/src")
cp_dir = sys.argv[1]

#Local directory to copy files to
cp_folder = str(pathto_CMSSW+'/src/'+cp_dir)

if 'walsh' in str(cp_folder):
    user = 'walsh'
    nuser = 0
if 'bklein' in str(cp_folder):
    user = 'bklein'
    nuser = 1

# SE path to be searched
folder = str("/pnfs/iihe/cms/store/user/walsh/ttbar_local_submission")
sys.path.append(folder)

for script in os.listdir(cp_folder):
     if script.endswith('*_*_1.root'):
          keep_going = raw_input('Local folder contains *_*_1.root files which will be deleted.Continue?(y/n)')
          if 'y' in keep_going:
               sts = os.system('rm '+cp_folder+'/*_*_1.root')
               break
          if 'n' in keep_going:
               sys.exit('Program stopped: root files in local folder')


#Search all subdirectories of folder for root file
for dirpath,dirnames,filenms in os.walk(folder):
    i = 0
    for script in filenms:
        if script.endswith('_1.root'):
            i = 1
            #print 'Found ',script
            path_to_SEfile = os.path.join(dirpath,script)
            SE_dir_basename = os.path.basename(dirpath)
            #print dirpath
            path_to_cpfile = os.path.join(cp_folder,SE_dir_basename,script)
            path_to_cpfile = os.path.join(cp_folder,script)
            #print path_to_cpfile

            print 'copying ', script
            # cp from SE to local file with '' for tcsh, none for bash
            #if nuser == 0:#code for tcsh shell
             #    sts = os.system("srmcp \'srm://maite.iihe.ac.be:8443/srm/managerv1?SFN=" + path_to_SEfile +"\' \'file:///"+path_to_cpfile+"\'")
            #if nuser == 1:
            sts = os.system("srmcp srm://maite.iihe.ac.be:8443/srm/managerv1?SFN=" + path_to_SEfile +" file:///"+path_to_cpfile)
            #remove root file from SE
            #sts = os.system("srmrm \'srm://maite.iihe.ac.be:8443" + path_to_SEfile+ "\'")

    #If root file was found
    if (i == 1):      

       #Eg. for parent/paretn/ttbar basename is ttbar. hadd output root files will be named basename.root
        SE_dir_basename = os.path.basename(dirpath)
        Added_root = os.path.join(cp_folder,SE_dir_basename)
        print Added_root
    
        sts = os.system("hadd "+Added_root+ ".root "+cp_folder+"/*_*_1.root")
        sys = os.system("rm " +cp_folder+"/*_*_1.root")
       
        
