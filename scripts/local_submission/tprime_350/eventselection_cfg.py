import FWCore.ParameterSet.Config as cms

process = cms.Process("eventselection")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(__MAX_EVENTS__) 
)

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring( __FILE_NAMES__ ),
    skipEvents = cms.untracked.uint32(__SKIP_EVENTS__)
)

process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttGenEvent_cff")
process.load("TopQuarkAnalysis.TopEventProducers.sequences.ttSemiLepEvtBuilder_cff")

process.load( "HLTrigger.HLTanalyzers.hlTrigReport_cfi" )

process.MessageLogger = cms.Service("MessageLogger")

process.eventselection = cms.EDAnalyzer("EventSelection",
    electronTag = cms.untracked.InputTag("selectedLayer1Electrons"),
    tauTag      = cms.untracked.InputTag("selectedLayer1Taus"),
    muonTag     = cms.untracked.InputTag("selectedLayer1Muons"),
    jetTag      = cms.untracked.InputTag("selectedLayer1Jets"),
    photonTag   = cms.untracked.InputTag("selectedLayer1Photons"),
    metTag      = cms.untracked.InputTag("selectedLayer1METs"),
    semiLepTag      = cms.untracked.InputTag("ttSemiLepEvent"),
    hypoClassKey      = cms.untracked.InputTag("ttSemiLepHypGenMatch:Key"),
    HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT' ),                               
    datasetName = cms.untracked.string("Tprime350"),
    tprimeMass = cms.untracked.double(175)
)

process.TFileService = cms.Service("TFileService", fileName = cms.string( __OUTFILE__ ) )

process.p0 = cms.Path(process.makeGenEvt * process.makeTtSemiLepEvent)

process.p1 = cms.Path(process.eventselection)

process.schedule = cms.Schedule( process.p0,
                                 process.p1 )
