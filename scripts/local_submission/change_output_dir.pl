#!/usr/bin/perl

open(DATASETSFILE, "<datasets.txt");
$datasets = <DATASETSFILE>;
close(DATASETSFILE);

@datasets = split(/\s+/, $datasets);

$se_path = '/pnfs/iihe/cms/store/user';

die "usage: $0 <identifier> [with $se_path/<identifier>]" unless(@ARGV);

$outdir = $ARGV[0];

die "can't identify user-name" unless($outdir =~ m/^(\w+)/);
$user = $1;

$i = 0;
foreach $dataset(@datasets){
	open(FH, "+< $dataset/crab.cfg")                 or die "Opening: $!";
	@crab_file = <FH>;
	foreach $line(@crab_file){
		if($line =~ m/storage_path\s+\=/){
			push(@changed_file, "storage_path            = $se_path/$outdir/$dataset/\n");
		}elsif($line =~ m/rfio_server\s+=/){
			push(@changed_file, "rfio_server = /localgrid/$user/\n");
		}else{
			push(@changed_file, $line);
		}
	}
	seek(FH,0,0)                        or die "Seeking: $!";
	print FH @changed_file                     or die "Printing: $!";
	truncate(FH,tell(FH))               or die "Truncating: $!";
	close(FH)                           or die "Closing: $!";
	@changed_file=();
}

open(FH, "+< make_clean.sh")                 or die "Opening: $!";
@file = <FH>;
foreach $line(@file){
	if($line =~ m/\/pnfs\/iihe\/cms\/store\/user\//){
		$line =~ s/\/pnfs\/iihe\/cms\/store\/user\/.*\/\${i}\/\*/\/pnfs\/iihe\/cms\/store\/user\/$outdir\/\$\{i\}\/\*/;
	}
	push(@changed_file, $line);
}
seek(FH,0,0)                        or die "Seeking: $!";
print FH @changed_file                     or die "Printing: $!";
truncate(FH,tell(FH))               or die "Truncating: $!";
close(FH)                           or die "Closing: $!";

@file = ();
@changed_file = ();

open(FH, "+< LocalGetSEfiles.py")                 or die "Opening: $!";
@file = <FH>;
foreach $line(@file){
	if($line =~ m/folder\s=\sstr\(\"\/pnfs\/iihe\/cms\/store\/user/){
		$line =~ s/folder\s=\sstr\(\"\/pnfs\/iihe\/cms\/store\/user\/.*\"\)/folder = str\(\"\/pnfs\/iihe\/cms\/store\/user\/$outdir\")/;
		
	}
	push(@changed_file, $line);
}
seek(FH,0,0)                        or die "Seeking: $!";
print FH @changed_file                     or die "Printing: $!";
truncate(FH,tell(FH))               or die "Truncating: $!";
close(FH)                           or die "Closing: $!";

`echo "$se_path/$outdir" > output_directory.txt`;
