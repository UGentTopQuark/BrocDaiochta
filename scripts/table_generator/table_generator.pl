#!/usr/bin/perl
use Tie::IxHash::Easy;
use Getopt::Std;

getopts("s:c:m:");

$tex_output_file = 'table.tex';

#$tprime_mass = '250';

die "usage: $0 [-c <channel: mu/e> -s <scale events: 0/1> -m <t' mass>] <log-directories>" unless(@ARGV>0);

$lepton = 'mu';
$process_preselection = 1;
my $show_statistic_errors = 1;
my $show_unscaled_events = 0;

my $dont_split_ttbar = 0;

my $tprime_mass = 250;

$tprime_mass = $opt_m if(defined($opt_m));

if(defined($opt_s)){
	if($opt_s != 0){
		$show_unscaled_events = 0;
	}else{
		$show_unscaled_events = 1;
	}
}else{
	$show_unscaled_events = 0;
}

if(defined($opt_c)){
	$lepton = $opt_c;
}

@cutnames = ();
if($lepton eq 'mu'){
	#@cutnames = ('s1','s2','s3', 's4','s5','s6a','s6b','s6c','s7');
	@cutnames = ('$\ge 4j, 0.125$','$=3j, 0.125$','$\ge 3j, 0.125$', '$\ge 4j, 0.22$','$=3j, 0.22$','$\ge 3j, 0.22$', '$\ge 4j, 0.1$','$=3j, 0.1$','$\ge 3j, 0.1$', '$\ge 4j, 0.15$','$=3j, 0.15$','$\ge 3j, 0.15$',);
}else{
	@cutnames = ('xxx', 'Trigger','1 iso. $e$','$\mu$ veto','loose $e$ veto','$\ge 1\,\mathrm{jets}$','$\ge 2\,\mathrm{jets}$','$\ge 3\,\mathrm{jets}$','$\ge 4\,\mathrm{jets}$');
}
#@cutnames = ('presel','$\ge$ 4 jets','$>$ 1 iso e','$=$ 1 iso e','jet clean', 'mu veto','Z rej','btag+', 'btag', 'btag-', 'KA sel','xxx');
#@cutnames = ('s0','s1','s2','s3','FY');


$show_statistic_errors = 0 if($show_unscaled_events);

#first datasets element considered as signal
tie %datasets, "Tie::IxHash::Easy";

if($lepton eq 'mu'){
####	$datasets{'Tprime'.$tprime_mass}{'muon'}=1;
#######	$datasets{'Tprime'.$tprime_mass}{'mu_background'}=1;
	$datasets{'TTbar'}{'muon'}=1;
#	$datasets{'TTbar'}{'mu_background'}=1;
#######	$datasets{'TTbarPy'}{'muon'}=1;
#######	$datasets{'TTbarPy'}{'mu_background'}=1;
#	$datasets{'Wjets'}{'mu_background'}=1;
#	$datasets{'Zjets'}{'mu_background'}=1;
#	$datasets{'MuQCD'}{'mu_background'}=1;
#	$datasets{'TsChan'}{'mu_background'}=1;
#	$datasets{'TtChan'}{'mu_background'}=1;
#	$datasets{'TtWChan'}{'mu_background'}=1;
#	$datasets{'Data'}{'muon'}=1;
}else{
####	$datasets{'Tprime'.$tprime_mass}{'electron'}=1;
####	$datasets{'Tprime'.$tprime_mass}{'e_background'}=1;
	$datasets{'TTbar'}{'electron'}=1;
	$datasets{'TTbar'}{'e_background'}=1;
######	#$datasets{'TTbarPy'}{'electron'}=1;
######	#$datasets{'TTbarPy'}{'e_background'}=1;
	$datasets{'Wjets'}{'e_background'}=1;
	$datasets{'Zjets'}{'e_background'}=1;
	$datasets{'QCDbctoe20to30'}{'e_background'}=1;
	$datasets{'QCDbctoe30to80'}{'e_background'}=1;
	$datasets{'QCDbctoe80to170'}{'e_background'}=1;
	$datasets{'QCDem20to30'}{'e_background'}=1;
	$datasets{'QCDem30to80'}{'e_background'}=1;
	$datasets{'QCDem80to170'}{'e_background'}=1;
#######	$datasets{'Gjets'}{'e_background'}=1;
	$datasets{'TsChan'}{'e_background'}=1;
	$datasets{'TtChan'}{'e_background'}=1;
	$datasets{'TtWChan'}{'e_background'}=1;
	$datasets{'Data'}{'electron'}=1;
}

# integrated luminosity in 1/picobarn
if($lepton eq "e"){
	$integrated_luminosity = 4628.087; #electron channel
}elsif($lepton eq "mu"){
#	$integrated_luminosity = 4944.147; # muon channel
#	$integrated_luminosity = 2267.418; # muon channel
	$integrated_luminosity = 4760.401; # muon channel
}

%cross_section = ();
%filter_efficiency = ();
@ttbar_eff = ();

read_cross_sections(\%cross_section, \%filter_efficiency);

##FIXME: temporary test
#while(my ($key, $val) = each %filter_efficiency){
#	$filter_efficiency{$key} = 1.;
#}

#while(my ($key, $val) = each %cross_section){
#	print "cross_sec: $key: $val\n";
#}
#while(my ($key, $val) = each %filter_efficiency){
#	print "filter_eff: $key: $val\n";
#}

$dictionary{'TTbar_muon'} = '$t\bar{t}$ ($\mu$-signal)';
$dictionary{'TTbar_mu_background'} = '$t\bar{t}$ (other)';
$dictionary{'TTbarPy_muon'} = '$t\bar{t}$ ($\mu$-signal)';
$dictionary{'TTbarPy_mu_background'} = '$t\bar{t}$ (other)';
$dictionary{'Mupt15_mu_background'} = 'QCD';
$dictionary{'MuQCD_mu_background'} = 'QCD';
$dictionary{'Data_mu_background'} = 'Data';
$dictionary{'Data_muon'} = 'Data';
$dictionary{'Data_electron'} = 'Data';
$dictionary{'Data_e_background'} = 'Data';
$dictionary{'QCDbctoe20to30_e_background'} = 'multijet';
$dictionary{'Wjets_mu_background'} = '$W\,+\,\mathrm{jets}$';
$dictionary{'Zjets_mu_background'} = '$Z\,+\,\mathrm{jets}$';
$dictionary{'DY50_mu_background'} = '$Z\,+\,\mathrm{jets}$';
$dictionary{'TTbar_electron'} = '$t\bar{t}$ ($e\,+\,\mathrm{jets}$)';
$dictionary{'TTbar_e_background'} = '$t\bar{t}$ (other)';
$dictionary{'TTbarPy_electron'} = '$t\bar{t}$ ($e$-signal)';
$dictionary{'TTbarPy_e_background'} = '$t\bar{t}$ (other)';
$dictionary{'Wjets_e_background'} = '$W\,+\,\mathrm{jets}$';
$dictionary{'Gjets_e_background'} = '$\gamma\,+\,\mathrm{jets}$';
$dictionary{'DY50_e_background'} = '$Z\,+\,\mathrm{jets}$';
$dictionary{'Zjets_e_background'} = '$Z\,+\,\mathrm{jets}$';
$dictionary{'TsChan_mu_background'} = 't, s-chan';
$dictionary{'TtChan_mu_background'} = 't, t-chan';
$dictionary{'TtWChan_mu_background'} = 't, tW-chan';
$dictionary{'TsChan_e_background'} = 't, s-chan';
$dictionary{'TtChan_e_background'} = 't, t-chan';
$dictionary{'TtWChan_e_background'} = 't, tW-chan';
$dictionary{'QCD'} = 'multijet';
$dictionary{'min_no_jets'} = '\# jets > ';
$dictionary{'e_electronID'} = '\mathrm{ElectronID}:~';
$dictionary{'min_e_pt'} = 'p_T (e) > ';
$dictionary{'min_mu_pt'} = 'p_T (\mu) > ';
$dictionary{'min_met'} = 'E_T^\mathrm{miss} > ';
$dictionary{'min_mu_ht'} = 'H_T^\mu > ';
$dictionary{'min_e_ht'} = 'H_T^e > ';
$dictionary{'max_e_d0'} = 'd0 (e) < ';
$dictionary{'max_mu_d0'} = 'd0 (\mu) < ';
$dictionary{'max_mu_trackiso'} = 'E_\mathrm{iso}^\mathrm{track}\,(\mu) < ';
$dictionary{'max_e_trackiso'} = 'E_\mathrm{iso}^\mathrm{track}\,(e) < ';
$dictionary{'max_mu_ecaliso'} = 'E_\mathrm{iso}^\mathrm{ecal}\,(\mu) < ';
$dictionary{'max_e_ecaliso'} = 'E_\mathrm{iso}^\mathrm{ecal}\,(e) < ';
$dictionary{'max_mu_caliso'} = 'E_\mathrm{iso}^\mathrm{cal}\,(\mu) < ';
$dictionary{'max_e_caliso'} = 'E_\mathrm{iso}^\mathrm{cal}\,(e) < ';
$dictionary{'max_mu_hcaliso'} = 'E_\mathrm{iso}^\mathrm{hcal}\,(\mu) < ';
$dictionary{'max_e_hcaliso'} = 'E_\mathrm{iso}^\mathrm{hcal}\,(e) < ';
$dictionary{'min_mu_dR'} = '\Delta R\,(\mu,\mathrm{jets}) > ';
$dictionary{'min_e_dR'} = '\Delta R\,(e,\mathrm{jets}) > ';
$dictionary{'max_nisolated_e'} = '\# e \le';
$dictionary{'max_nisolated_mu'} = '\# \mu \le';
$dictionary{'min_nisolated_e'} = '\# e \ge';
$dictionary{'min_nisolated_mu'} = '\# \mu \ge';
$dictionary{'max_e_hcal_veto_cone'} = '\mathrm{Veto~Cone} (e, HCAL) \le';
$dictionary{'max_mu_hcal_veto_cone'} = '\mathrm{Veto~Cone} (\mu, HCAL) \le';
$dictionary{'max_e_ecal_veto_cone'} = '\mathrm{Veto~Cone} (e, ECAL) \le';
$dictionary{'max_mu_ecal_veto_cone'} = '\mathrm{Veto~Cone} (\mu, ECAL) \le';
$dictionary{'min_jet_pt'} = 'p_T^\mathrm{jets} > ';
$dictionary{'min_mu_relIso'} = '\mathrm{rel.~Iso.} (\mu) > ';
$dictionary{'min_e_relIso'} = '\mathrm{rel.~Iso.} (e) > ';
$dictionary{'max_jet_eta'} = '\eta^\mathrm{jets} < ';
$dictionary{'max_mu_eta'} = '\eta\,(\mu) < ';
$dictionary{'max_e_eta'} = '\eta\,(e) < ';
$dictionary{'Z_rejection_width'} = '\mathrm{veto}\,Z < ';
$dictionary{'Preselection'} = '\mathrm{Preselection} ';
$dictionary{'signal_over_background'} = 'S/B';
$dictionary{'purity'} = '$\pi$';
$dictionary{'trigger'} = '\mathrm{Trigger}: ';
$dictionary{'efficiency'} = '$\epsilon_\mathrm{sig.}$';
$dictionary{'purity_x_efficiency'} = '$\epsilon_\mathrm{sig.}\cdot\pi$';
$dictionary{'purity_x_efficiency2'} = '$\frac{\epsilon\cdot\pi}{2-\pi}$';

%cumulative_selection = ();
if($process_preselection != 1){
	if($lepton eq 'e'){
		$cumulative_selection{'min_nisolated_e'}=1;
		$cumulative_selection{'max_nisolated_e'}=1;
		$cumulative_selection{'max_nisolated_mu'}=0;
	}
	if($lepton eq 'mu'){
		$cumulative_selection{'max_nisolated_e'}=0;
		$cumulative_selection{'min_nisolated_mu'}=1;
		$cumulative_selection{'max_nisolated_mu'}=1;
	}
#	$cumulative_selection{'min_mu_relIso'}=0.9;
#	$cumulative_selection{'min_e_relIso'}=0.9;

	$cumulative_selection{'min_no_jets'}=4;
#	$cumulative_selection{'min_e_pt'}=30;
#	$cumulative_selection{'min_mu_pt'}=30;
	$cumulative_selection{'max_mu_eta'}=2.1;
	$cumulative_selection{'max_e_eta'}=2.1;
	$cumulative_selection{'max_jet_eta'}=2.4;
#	$cumulative_selection{'min_jet_pt'}=30;
	$cumulative_selection{'max_e_trackiso'}=5;
	$cumulative_selection{'max_e_caliso'}=10;
#	$cumulative_selection{'e_electronID'}=1;
	$cumulative_selection{'max_mu_trackiso'}=3;
	$cumulative_selection{'max_mu_caliso'}=1;
	$cumulative_selection{'trigger'}='85';
#	$cumulative_selection{'max_e_d0'}=0.2;
#	$cumulative_selection{'max_mu_d0'}=0.2;
}

$skip_comparison{'identifier'}=1;
$skip_comparison{'cuts_passed'}=1;
$skip_comparison{'cuts_overall'}=1;

#@statistics = ();#'signal_over_background');#, 'purity', 'efficiency', 'purity_x_efficiency', 'purity_x_efficiency2');
@statistics = ('efficiency', 'purity', 'purity_x_efficiency');
#@statistics = ();

$ttbar_counter{'muon'}=0;
$ttbar_counter{'electron'}=0;
$ttbar_counter{'e_background'}=0;
$ttbar_counter{'mu_background'}=0;

#$signal_dataset = 'Tprime'.$tprime_mass;
$signal_dataset = 'TTbar';

################################################################################
##
##	Main Program
##
################################################################################

%settings = ();
%values = ();
%cuts = ();

@directories = @ARGV;

%global_map = ();
foreach my $dirname(@directories){
	opendir(DIR, $dirname) or die "can't opendir $dirname: $!";
	while (defined(my $file = readdir(DIR))) {
		next unless($file =~ m/CMSSW_\d+.stdout/ || $file =~ m/\.txt/);
		analyse_file($dirname, $file);
	}
	closedir(DIR);
}

print_output($tex_output_file);

################################################################################
##
##	Functions
##
################################################################################

sub print_output
{
	my $tex_file = pop;
	
	open(TEX_FILE, ">$tex_file");
	print_header(TEX_FILE);
	print_table(TEX_FILE);	
	print_closing(TEX_FILE);
	close(TEX_FILE);
}

sub print_table
{
	my $fh = pop;

 	my $ndatasets = 0;
	my $already_qcd=0;
	foreach $key (keys %datasets){
		if(not $already_qcd && $key =~ /QCD/){
			$ndatasets += keys %{$datasets{$key}};
		}
		if($key =~ /QCD/){
			$already_qcd=1;
		}
	}
	my $columns = $ndatasets + 3;

	print $fh "\\begin{sideways}\n";
	print $fh "\\begin{tabular}{";
	for(my $i = 1; $i <= $columns; $i++){
		if ($i == $columns){
			my $nstat_keys = @statistics;
			print "keys statistics = $nstat_keys";
			print "\n";
			if($nstat_keys > 0){
			#	print $fh "|";
				foreach $stat (@statistics){
					print $fh 'c';
				}
			}
			#print $fh "|}\n";
			print $fh "}\n";
		}elsif ($i == 1){
			#print $fh "|p{1.75cm}|";
			print $fh "p{1.75cm}";
		}else{
			# FIXME: to avoid linebreak in tabular
			# print $fh "c";
			print $fh "p{1.75cm}";
		}
	}
	print_headline($fh);
	print_body($fh);
	print $fh "\\bottomrule\n";
	print $fh "\\end{tabular}\n";
	print $fh "\\end{sideways}\n";
	if(!$process_preselection){
		my $preselection_line = prepare_footnote_preselection();
		print $fh $preselection_line;
	}
}

sub print_body
{
	my $fh = pop;

	print "printing body\n";
	my $datatype = '';
	# find one datatype that is to be considered
	foreach $data(keys %cuts){
		if(defined($datasets{$data})){
			$datatype = $data;
			last;
		}
	}

	my $eventtype = '';
	# find one eventtype that is to be considered
	foreach $event(keys %{$cuts{$datatype}}){
		if(defined($datasets{$datatype}{$event})){
			$eventtype = $event;
			last;
		}
	}

	print_produced_line($fh);
	
	print "looping over dataset $datatype eventtype $eventtype\n";
	foreach $cutset (sort keys %{$cuts{$datatype}{$eventtype}}){
		print "processing cutset $cutset\n";
		print_line($fh, $datatype, $eventtype, $cutset);
	}
}

sub print_produced_line
{
	my ($fh) = @_;

	print $fh "Produced";

	my $qcd_events = 0;
	my $just_processed_qcd = 0;
	my $counter = 1;
	foreach $dataset(keys %datasets){
		my $events = 0;
		my $lepton_fullname;
		my $lepton_background_fullname;
		if($lepton eq 'mu'){
			$lepton_fullname = 'muon';
			$lepton_background_fullname = 'mu_background';
		}elsif($lepton eq 'e'){
			$lepton_fullname = 'electron';
			$lepton_background_fullname = 'e_background';
		}else{
			die "ERROR: print_produced_line(): can't find signal lepton\n";
		}

		if($dataset !~ /QCD/){
			if($dataset eq $signal_dataset){
				unless($show_unscaled_events){
					$events = $integrated_luminosity*$cross_section{$dataset};
				}else{
					my @cutsets = sort keys %{$cuts{$dataset}{$lepton_background_fullname}};
					my $cutset = shift @cutsets;
					$events = get_unscaled_number_of_generated_events($dataset, $lepton_fullname, $cutset);
					$events += get_unscaled_number_of_generated_events($dataset, $lepton_background_fullname, $cutset);
				}

				my $signal_fraction = 1.;
				$signal_fraction = get_ttbar_fraction($lepton_fullname) unless($dont_split_ttbar);

				my $signal = int(0.5+$events*$signal_fraction);
				my $background = int(0.5+$events*(1-$signal_fraction));
				printf $fh " & \$%.0f\$ & \$%.0f\$", $signal, $background;
			}elsif($dataset eq 'Data'){
				printf $fh " & ", $events;
			}else{
				if($just_processed_qcd){
					$events = $qcd_events;
					printf $fh " & \$%.0f\$", $events;
				}
				unless($show_unscaled_events){
					$events = $integrated_luminosity*$cross_section{$dataset};
				}else{
					my @cutsets = sort keys %{$cuts{$dataset}{$lepton_background_fullname}};
					my $cutset = shift @cutsets;
					$events = get_unscaled_number_of_generated_events($dataset, $lepton_fullname, $cutset);
					$events += get_unscaled_number_of_generated_events($dataset, $lepton_background_fullname, $cutset);
				}
				printf $fh " & \$%.0f\$", $events;
				$just_processed_qcd = 0;
			}
			$counter++;
		}else{
			unless($show_unscaled_events){
				$events = $integrated_luminosity*$cross_section{$dataset};
			}else{
				my @cutsets = sort keys %{$cuts{$dataset}{$lepton_background_fullname}};
				my $cutset = shift @cutsets;
				$events = get_unscaled_number_of_generated_events($dataset, $lepton_background_fullname, $cutset);
			}
			$qcd_events += $events;
			$just_processed_qcd = 1;
		}
	}

	my $old_counter = $counter;
        my $ndatasets = keys %datasets;
	$old_counter++;	# for "sum" column
	while($counter < $old_counter+@statistics){
		print $fh ' & ';
		$counter++;
	}
	print $fh '\\\\'."\n";
	#print $fh '\\midrule'."\n";	
}


sub print_line
{
	my ($fh, $datatype, $eventtype, $cutset) = @_;

	print "print_line: $cutset\n";
	my @applied_cuts = find_applied_cuts($datatype, $eventtype, $cutset);
	print "found_applied_cuts: $cutset\n";

	my $counter = 0;
	if(defined(@cutnames)){
		$cutname = shift(@cutnames);
		if($cutname eq 'xxx'){
			return;
		}
		print $fh $cutname.' & ';
	}else{
		foreach my $cut(@applied_cuts){
			if(@applied_cuts == 0){
				print $fh get_formatted_cut($datatype, $eventtype, $cutset, $cut);
			}elsif($counter == @applied_cuts-1){
				print $fh get_formatted_cut($datatype, $eventtype, $cutset, $cut);
				print $fh ' & ';
			}else{
				print $fh get_formatted_cut($datatype, $eventtype, $cutset, $cut);
				print $fh '\\\\';
			}
			$counter++;
		}
	}

	
	my @values_for_row=();
	my $mc_sum=0;
	my $mc_sum_err=0;
	my @limits_for_row=();
	my @errors_for_row=();
	my @efficiencies=();
	my $qcd_values = 0;
	my $qcd_error = 0;
	my $qcd_all = 0;
	my $qcd_limit = 1;
	my $processed_qcd = 0;
	print "processing all datasets for $cutset\n";
	foreach $dataset(keys %datasets){
		foreach $evttype (keys %{$datasets{$dataset}}){
			my ($efficiency, $number, $error, $limit) = get_number_of_events($dataset, $evttype, $cutset);
#			print "number of events: $number\n";
			if($dataset !~ /QCD/){
				if($processed_qcd){
					my $qcd_efficiency = $qcd_values/$qcd_all;
					$qcd_error = sqrt($qcd_error);
					push(@values_for_row, $qcd_values);
					push(@limits_for_row, $qcd_limit);
					push(@errors_for_row, $qcd_error);
					push(@efficiencies, $qcd_efficiency);
					$processed_qcd = 0;
					print "final QCD-sum: $qcd_values\n";
					print "---- QCD dataset change ----\n";
					$mc_sum += $qcd_values;
					$mc_sum_err += $qcd_error*$qcd_error;
				}
				push(@values_for_row, $number);
				push(@errors_for_row, $error);
				push(@limits_for_row, $limit);
				push(@efficiencies, $efficiency);

				if($dataset !~ /Data/i){
					$mc_sum += $number;
					$mc_sum_err += $error*$error;
				}
			}else{
				# if all pt hat bins so far have been empty but this one
				# is not empty, ignore everything so far and start with 0
				if($qcd_limit && !$limit){
					$qcd_values = 0;
					$qcd_error = 0;
					print "resetting limit...\n";
				}

				# if this pt hat bin is empty then at least
				# one qcd bin was empty->qcd_limit = false
				$qcd_limit = 0 if(!$limit);
				# if at least one pt hat bin has not been empty and this one
				# is not empty OR if all have been empty and this one is empty
				if((!$qcd_limit && !$limit) || ($qcd_limit && $limit)){
					# add the numbers
					$qcd_values += $number;	
					$qcd_error += $error * $error;
				}else{
					print "not adding dataset limit\n";
				}
				if($efficiency != 0){
					$qcd_all += $number/$efficiency;
				}else{
					$qcd_all = -1;
				}
				print "QCD: $dataset: $number +/- $error\n";
				print "QCD-sum: $qcd_values +/- ".sqrt($qcd_error)."\n";
				$processed_qcd=1;
			}
		}
	}

	if($processed_qcd){
		my $qcd_efficiency = $qcd_values/$qcd_all;
		$qcd_error = sqrt($qcd_error);
		push(@values_for_row, $qcd_values);
		push(@errors_for_row, $qcd_error);
		push(@efficiencies, $qcd_efficiency);
		$processed_qcd = 0;
	}

	$counter = 0;
	foreach $value(@values_for_row){
		my $digits = 0;
		$digits = 1 if($value < 10 || $errors_for_row[$counter] < 10);
		$digits = 2 if($value < 1 || $errors_for_row[$counter] < 1);

		if($counter == @values_for_row-1){
			if($show_statistic_errors){
				if($limits_for_row[$counter]){
				    printf $fh "\$\< %.${digits}f\$", $value, $errors_for_row[$counter];
				}else{
				    printf $fh "\$%.2f \\pm %.${digits}f\$", $value, $errors_for_row[$counter];
				    
				}
			}else{
				if($limits_for_row[$counter]){
					printf $fh "\$\< %.${digits}f \$", $value, $errors_for_row[$counter];
				}else{
				    if($show_unscaled_events){
					printf $fh "\$%.0f\$", $value, $errors_for_row[$counter];
				    }else{
					printf $fh "\$%.${digits}f\$", $value, $errors_for_row[$counter];
				    }
				}
			}
		}else{
			if($show_statistic_errors){
				if($limits_for_row[$counter]){
					printf $fh "(\$\< %.${digits}f \\pm %.${digits}f)\$", $value, $errors_for_row[$counter];
				}else{
					printf $fh "\$%.${digits}f \\pm %.${digits}f\$", $value, $errors_for_row[$counter];
				}
			}else{
				if($limits_for_row[$counter]){
					printf $fh "\$\< %.${digits}f \$", $value, $errors_for_row[$counter];
				}else{
				    if($show_unscaled_events){
					printf $fh "\$%.0f\$", $value, $errors_for_row[$counter];
				    }else{
					printf $fh "\$%.${digits}f\$", $value, $errors_for_row[$counter];
				    }
				}
			}
			print $fh ' & ';
		}

		$counter++;
	}

	$mc_sum_err = sqrt($mc_sum_err);
	printf $fh " & \$%.2f \\pm %.2f\$ ", $mc_sum, $mc_sum_err;

	my $efficiency = $efficiencies[0];

	my @statistic_values = get_statistics(@values_for_row, $efficiency);	

	my $old_counter = $counter;
        my $ndatasets = 0;
        foreach $key (keys %datasets){
                $ndatasets += keys %{$datasets{$key}};
        }  
	while($counter < $old_counter+@statistics){
		my $array_position = $counter - $old_counter;
		if($array_position < @statistic_values){
			print $fh ' & ';
			printf $fh "%6.4f",$statistic_values[$array_position];
		}else{
			print $fh ' & ';
		}
		$counter++;
	}
	print $fh '\\\\'."\n";
	#print $fh '\\midrule'."\n";	
}

sub get_statistics
{
	my $efficiency = pop;
	my @row_values = @_;

	my $signal_events = shift @row_values;
	my $data_events = pop @row_values;

	my $background_events = 0;
	foreach $background (@row_values){
		$background_events += $background;
	}

	print "signal: $signal_events, background: $background_events\n";
	
	my @statistic_values=();
	unless($background_events == 0){
		my $signal_over_background = $signal_events / $background_events;
		my $purity = $signal_events / ($signal_events + $background_events);

		foreach $statistic (@statistics){
			if($statistic eq "signal_over_background"){
				push(@statistic_values, $signal_over_background);
			}elsif($statistic eq "purity_x_efficiency"){
				my $purity_x_efficiency = $purity*$efficiency;
				push(@statistic_values, $purity_x_efficiency);
			}elsif($statistic eq "purity_x_efficiency2"){
				my $purity_x_efficiency2 = ($purity*$efficiency)/(2-$purity);
				push(@statistic_values, $purity_x_efficiency2);
			}elsif($statistic eq "purity"){
				push(@statistic_values,$purity);
			}elsif($statistic eq "efficiency"){
				push(@statistic_values,$efficiency);
			}
		}
	}

	return @statistic_values;
}

sub get_number_of_events
{
	my($dataset, $evttype, $cutset) = @_;

#	print "get_number_of_events for $cutset\n";
	my $nevents_passed_cuts = $cuts{$dataset}{$evttype}{$cutset}{'cuts_passed'};
	my $nevents_overall = $cuts{$dataset}{$evttype}{$cutset}{'cuts_overall'};

	# scale events for integrated luminosity
	return scale_events($nevents_overall, $nevents_passed_cuts, $dataset, $evttype);
}

sub get_unscaled_number_of_generated_events
{
	my($dataset, $evttype, $cutset) = @_;

	my $nevents_passed_cuts = $cuts{$dataset}{$evttype}{$cutset}{'cuts_passed'};
	my $nevents_overall = $cuts{$dataset}{$evttype}{$cutset}{'cuts_overall'};

	my $overall_events = $nevents_overall;

	my $filter_eff = 0;
	if($dataset eq $signal_dataset && $evttype eq 'muon'){
		$filter_eff = $filter_efficiency{$dataset."_muon"};
	}elsif($dataset eq $signal_dataset && $evttype eq 'electron'){
		$filter_eff = $filter_efficiency{$dataset."_electron"};
	}elsif($dataset eq $signal_dataset && $evttype eq 'e_background'){
		$filter_eff = $filter_efficiency{$dataset."_e_background"};
	}elsif($dataset eq $signal_dataset && $evttype eq 'mu_background'){
		$filter_eff = $filter_efficiency{$dataset."_mu_background"};
	}
	else{
		$filter_eff = $filter_efficiency{$dataset};
	}

	$overall_events /= $filter_eff unless($filter_eff == 0);

	return $overall_events;
}

sub scale_events
{
	my ($nevents_overall, $nevents_passed_cuts, $dataset, $evttype) = @_;
	my $sum_events = $nevents_overall;

	return 0 if($sum_events == 0);

	my $scaled_events=-1;
	my $scaled_error=-1;
	my $unscaled_error=-1;

	my $limit = 0;	# if the signal number is 0 we quote a limit
			# -> set number of MC events to 3
	if($nevents_passed_cuts == 0 && $show_unscaled_events != 1){
		$limit = 1;
		$nevents_passed_cuts = 3;
#		print "no passed events found for $dataset\n";
	}

#	print "scaling for dataset $dataset\n";
	
	my $cross_section = -1;
	my $filter_eff = 0;
	if(defined($cross_section{$dataset})){
		if($dataset eq $signal_dataset && $evttype eq 'muon'){
			$filter_eff = $filter_efficiency{$dataset."_muon"};
		}elsif($dataset eq $signal_dataset && $evttype eq 'electron'){
			$filter_eff = $filter_efficiency{$dataset."_electron"};
		}elsif($dataset eq $signal_dataset && $evttype eq 'e_background'){
			$filter_eff = $filter_efficiency{$dataset."_e_background"};
		}elsif($dataset eq $signal_dataset && $evttype eq 'mu_background'){
			$filter_eff = $filter_efficiency{$dataset."_mu_background"};
		}
		else{
			$filter_eff = $filter_efficiency{$dataset};
		}
		$cross_section = $cross_section{$dataset} * $filter_eff;
	}

	if($cross_section != -1){
		my $scaling_factor = $integrated_luminosity * $cross_section;
		$scaled_events = ($scaling_factor / $sum_events) * $nevents_passed_cuts;
#		print "sum_evt: $sum_events passed: $nevents_passed_cuts not_passed: $nevents_not_passed_cuts\n";
		if($nevents_passed_cuts == 0){
			$scaled_error = 0;
		}else{
			$scaled_error = sqrt($nevents_passed_cuts)/$sum_events * $scaling_factor;
		}
	}else{
		$scaled_events = $nevents_passed_cuts;
		$scaled_error = sqrt($cuts_passed);
		print "WARNING: THE EVENTS ARE NOT SCALED FOR THE CROSS SECTION IN $dataset\n";
	}

	my $efficiency = -1;
	if($filter_eff && ($dataset eq $signal_dataset)){
		$efficiency = $nevents_passed_cuts / ($sum_events/$filter_eff);
	}
	elsif($filter_efficiency{$dataset}){
		$efficiency = $nevents_passed_cuts / ($sum_events/$filter_eff);
	}
	else{
		$efficiency = $nevents_passed_cuts / $sum_events;
	}
	push(@ttbar_eff,$efficiency." , ");
	print "$dataset: passed ". $nevents_passed_cuts."\n";
	print "$dataset: overall ". $sum_events."\n";
	print "$dataset: efficiency ". $nevents_passed_cuts/$sum_events ."\n";
	print "$dataset: lumi: ". $integrated_luminosity ."\n";
	print "$dataset: cross section: ". $cross_section ."\n";
	

	if($dataset eq $signal_dataset){
		my $fraction = 1.;
		$fraction = get_ttbar_fraction($evttype) unless($dont_split_ttbar);

		$scaled_events *= $fraction;
		if($cross_section != -1){
			if($nevents_passed_cuts == 0){
				$scaled_error = 0;
			}else{
				my $scaling_factor = $integrated_luminosity * $cross_section;
				$scaled_error = sqrt($nevents_passed_cuts)/($sum_events) * $scaling_factor * $fraction;
				$unscaled_error = sqrt($nevents_passed_cuts)/($sum_events);
				
			}
		}
	}	
#	print "$dataset/$evttype: events: $scaled_events, error: $scaled_error\n";

	if($dataset =~ /Data/i){
		$unscaled_error = sqrt($nevents_passed_cuts);
		return ($efficiency, $nevents_passed_cuts, $unscaled_error, $limit);
	}elsif($show_unscaled_events){
	    $limit = 0;
	    return ($efficiency, $nevents_passed_cuts, $unscaled_error, $limit);
	}else{
	    return ($efficiency, $scaled_events, $scaled_error, $limit);
	}
}

sub get_formatted_cut
{
	my ($datatype, $eventtype, $cutset, $cut) = @_;
	
	my $formatted_cut = '';
	if(defined($dictionary{$cut})){
		$formatted_cut = '$'.$dictionary{$cut}." ".$cuts{$datatype}{$eventtype}{$cutset}{$cut}.'$';
	}else{
		$formatted_cut = '$'.$cut." ".$cuts{$datatype}{$eventtype}{$cutset}{$cut}.'$';
	}

	return $formatted_cut;
}

sub find_applied_cuts
{
	my ($datatype, $eventtype, $cutset) = @_;

	my %applied_cuts_hash = %{$cuts{$datatype}{$eventtype}{$cutset}};

	my @applied_cuts = ();
	while( my ($cut, $value) = each %applied_cuts_hash ){
		if($value == -1){
			delete $applied_cuts_hash{$cut};
		}elsif(defined($skip_comparison{$cut})){
			delete $applied_cuts_hash{$cut};
		}elsif(defined($cumulative_selection{$cut}) and
			($cumulative_selection{$cut} eq $applied_cuts_hash{$cut})){
			delete $applied_cuts_hash{$cut};
		}else{
			push(@applied_cuts, $cut);
		}
	}

	push(@applied_cuts, "Preselection") if(@applied_cuts == 0);

#	print "applied cuts:\n";
	foreach my $cut(@applied_cuts){
		if($process_preselection){
			$cumulative_selection{$cut} = $cuts{$datatype}{$eventtype}{$cutset}{$cut};
		}
		print $cut.": ".$cuts{$datatype}{$eventtype}{$cutset}{$cut}."\n";
	}
#	print "-------------\n";

	return @applied_cuts;
}

sub print_headline
{
	my $fh = pop;

	print $fh "\\toprule\n";
	#print $fh "\\hline\n";
	my $qcd_column_printed = 0;
	print $fh "Selection ";
	foreach my $datatype (keys %datasets){
		foreach my $evttype (keys %{$datasets{$datatype}}){
			# print QCD only once
			unless($qcd_column_printed && $datatype =~ /QCD/){
				if(defined($dictionary{$datatype.'_'.$evttype})){
					print $fh " & $dictionary{$datatype.'_'.$evttype}";
				}else{
					print $fh " & ".$datatype."_".$evttype;
				}
			}
			$qcd_column_printed = 1 if($datatype =~ /QCD/);
		}
	}
	print $fh " & Simulation ";
	foreach my $stat (@statistics){
		if(defined($dictionary{$stat})){
			print $fh " & $dictionary{$stat}";
		}else{
			print $fh " & $stat";
		}
	}
	#print $fh "\\\\\n\\hline\n\\hline\n";
	print $fh "\\\\\n\\midrule\n";
}

sub print_header
{
	my $filehandle = pop;
	print $filehandle <<EOF;
\\documentclass[a4paper,10pt]{article}
\\pagestyle{empty}
\\usepackage{rotating}
\\usepackage{booktabs}
\\usepackage[top=5pt, bottom=5pt, left=5pt, right=5pt]{geometry}
\\title{t\\bar{t} selection}
\\begin{document}
EOF
}

sub print_closing
{
	my $filehandle = pop;
	print $filehandle <<EOF;
\\end{document}
EOF
}

sub analyse_file
{
	my $file = pop;
	my $dir = pop;
	open(FILE, "<$dir/$file") or die "cannot open $file: $!";

	my $read_cuts=0;
	my $read_ttbar_counter=0;
	my @block = ();
	while (defined (my $line = <FILE>)) {
		if($line =~ m/=-----------IMPOSED-CUTS-----------=/){
			$read_cuts = 1;
		}elsif($line =~ m/=----------------------------------=/)
		{
			analyse_block(@block);
			@block=();
			$read_cuts=0;
		}else{
			push(@block, $line) if($read_cuts);
		}
		if($line =~ m/^=\+{10}Event-Count:$signal_dataset\+{10}=$/)
		{
			$read_ttbar_counter = 1;
		}elsif($line =~ m/^=\+{30}=$/){
			$read_ttbar_counter = 0;
		}
		else{
			if($read_ttbar_counter){
				if($line =~ m/ttbar_mu_events: (\d+)/){
					$ttbar_counter{"muon"} =
					$ttbar_counter{'muon'} + $1;
				}
				if($line =~ m/ttbar_e_events: (\d+)/){
					$ttbar_counter{"electron"} =
					$ttbar_counter{'electron'} + $1;
				}
				if($line =~ m/ttbar_bg_events: (\d+)/){
					$ttbar_counter{"background"} =
					$ttbar_counter{'background'} + $1;
				}
			}
		}
	}	
	close(FILE);
}

sub analyse_block
{
	my @block = @_; 
	my $datatype, $eventtype, $cutset;
	foreach my $blockline(@block){
		@parts = split(/: /, $blockline);
		if(@parts == 2){
			chomp $parts[1];
			if($parts[0] eq 'identifier'){
				($datatype, $eventtype, $cutset) = split(/\|/, $parts[1]);
			}else{
				my $cut = $parts[0];
				if($cut eq 'cuts_passed' ||
					$cut eq 'cuts_overall'){
					my $new_value = $cuts{$datatype}{$eventtype}{$cutset}{$cut} + eval($parts[1]);
					$cuts{$datatype}{$eventtype}{$cutset}{$cut} = $new_value;
				}else{
					$cuts{$datatype}{$eventtype}{$cutset}{$cut}=$parts[1];
				}
			}
		}else{
			print "WARNING: could not split $blockline in analyse_block()\n";
		}
	}
}

sub prepare_footnote_preselection
{
	my $footnote = "\\\\\nPreselection: ";
	while(my ($cut, $value) = each %cumulative_selection){
		$footnote .= '$'.$dictionary{$cut}.$value.'$, ';
	}

	$footnote .= "\n\\\\";
	$footnote .= 'S/B: [$t\'$(signal) + $t\'$(other)]/background';
	$footnote .= "\n\\\\";
	$footnote .= 'efficiency \& purity: only $t\'$ (signal) considered as signal, everything else (including $t\'$ (other)) considered as background';
	$footnote .= "\n";

	return $footnote;
}

sub get_ttbar_fraction
{
	my $evttype = shift @_;
	my %fixed_counter = ();
	$fixed_counter{'muon'} = $ttbar_counter{'muon'}/$filter_efficiency{$signal_dataset."_muon"};
	$fixed_counter{'electron'} = $ttbar_counter{'electron'}/$filter_efficiency{$signal_dataset."_electron"};
	if($evttype eq 'electron' || $evttype eq 'e_background'){
		$fixed_counter{'background'} = ($ttbar_counter{'background'}+$ttbar_counter{'muon'})/$filter_efficiency{$signal_dataset."_e_background"};
	}elsif($evttype eq 'muon' || $evttype eq 'mu_background'){
		$fixed_counter{'background'} = ($ttbar_counter{'background'}+$ttbar_counter{'electron'})/$filter_efficiency{$signal_dataset."_mu_background"};
	}

	my $ttbar_sum = $fixed_counter{'muon'} + $fixed_counter{'background'};
	$ttbar_sum = -1 if($ttbar_sum == 0);
	print "WARNING: ttbarsum == 0 in get_ttbar_fraction\n" if($ttbar_sum == -1);
	if($evttype eq 'muon'){
		$fraction = $fixed_counter{'muon'}/$ttbar_sum;
	}elsif($evttype eq 'electron'){
		$fraction = $fixed_counter{'electron'}/$ttbar_sum;
	}elsif($evttype eq 'mu_background'){
		$fraction = ($fixed_counter{'background'})/$ttbar_sum;
	}elsif($evttype eq 'e_background'){
		$fraction = ($fixed_counter{'background'})/$ttbar_sum;
	}
	if($fraction == 1){
	    return $fraction;
	}
	#hardcoded branching ratios
	if($evttype eq 'muon'){
		$fraction = 0.146864425858859;
	}elsif($evttype eq 'electron'){
		$fraction = 0.146864425858859;
	}elsif($evttype eq 'mu_background'){
		$fraction = 1-0.146864425858859;
	}elsif($evttype eq 'e_background'){
		$fraction = 1-0.146864425858859;
	}

	print "SIGNAL FRACTION: $fraction\n";

	return $fraction;
}

sub read_cross_sections
{
	my ($cs_ref, $fe_ref) = @_;
	
	$input_file = "CrossSectionProvider/CrossSectionProvider.cc";

	open(CSFILE, "<$input_file") or die "can't open cs file";
	my $cs_section = 0;
	my $fe_section = 0;
	while(defined($line = <CSFILE>)){
		$cs_section = 1 if($line =~ m/---BEGIN CROSS SECTIONS---/);
		$cs_section = 0 if($line =~ m/---END CROSS SECTIONS---/);
		$fe_section = 1 if($line =~ m/---BEGIN FILTER EFFICIENCIES---/);
		$fe_section = 0 if($line =~ m/---END FILTER EFFICIENCIES---/);
	
		next unless($cs_section or $fe_section);

		if($cs_section){
			if($line =~ m/cross_section\[\"([\d\w_]+)\"\]\s*=\s*([\-\d\.\*\/]+)\s*\;/){
				my $dataset = $1;
				my $cs = $2;
				$cs_ref->{$dataset} = eval($cs);
			}
		}elsif($fe_section){
			if($line =~ m/filter_efficiency\[\"([\d\w_]+)\"\]\s*=\s*([\/\s\-\d\.\*]+)\s*\;/){
				my $dataset = $1;
				my $fe = $2;
				$fe_ref->{$dataset} = eval($fe);
			}
		}
	};
	close(CSFILE);
}

print "{";print @ttbar_eff;print"}\n";
