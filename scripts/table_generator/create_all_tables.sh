#!/bin/bash

PARENT_DIR="/home/bklein/root/flat_selection/gent";
DIRS=("jes_1_00" "jes_0_90" "jes_1_10");
LUMI="200pb";

for DIR in ${DIRS[@]}; do
	./table_generator.pl -c mu -s 0 250 $PARENT_DIR/$DIR
	mv table.tex mu_RAW_${DIR}.tex
	./table_generator.pl -c mu -s 1 250 $PARENT_DIR/$DIR
	mv table.tex mu_${LUMI}_${DIR}.tex
	./table_generator.pl -c e -s 0 250 $PARENT_DIR/$DIR
	mv table.tex e_RAW_${DIR}.tex
	./table_generator.pl -c e -s 1 250 $PARENT_DIR/$DIR
	mv table.tex e_${LUMI}_${DIR}.tex
done
