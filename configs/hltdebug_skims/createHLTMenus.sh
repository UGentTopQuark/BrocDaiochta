#!/bin/bash

DEST=${CMSSW_BASE}/src/hltdebug_skims/HLT/python/

MENU=/cdaq/physics/Run2010/v9.0/HLT/V3
echo "creating menu for MC..."
hltGetConfiguration --cff --process HLT2 --offline --mc orcoff:${MENU} > ${DEST}/priv_HLT_GRun_v9_0_v3_mc_cff.py
echo "creating menu for data..."
hltGetConfiguration --process HLT2 --offline --data orcoff:${MENU} > ${DEST}/priv_HLT_GRun_v9_0_v3_data_cff.py

MENU=/cdaq/physics/Run2010/v9.5/HLT/V1
echo "creating menu for MC..."
hltGetConfiguration --cff --process HLT2 --offline --mc orcoff:${MENU} > ${DEST}/priv_HLT_GRun_v9_5_v1_mc_cff.py
echo "creating menu for data..."
hltGetConfiguration --process HLT2 --offline --data orcoff:${MENU} > ${DEST}/priv_HLT_GRun_v9_5_v1_data_cff.py

echo "done."
