import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
process.GlobalTag.globaltag = '@GLOBALTAG@' 
process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

#import os
#os.system('$CMSSW_RELEASE_BASE/src/HLTrigger/Configuration/test/getHLT.sh --force --offline --data orcoff:/cdaq/physics/Run2010/v2.2/HLT_4\E29_PRE5/V2 MUON')

# import the menu
from HLTrigger.Configuration.@HLTCONF@ import *
# remove the prescales
process.PrescaleService.prescaleTable = cms.VPSet()

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
    #'file:////user/walsh/CMSSW_3_6_1_patch4/src/create_pat/ttbar_spring10_reco.root'
    'file:////user/walsh/CMSSW_3_9_7/src/rootfiles/WZMu_Dec22Skim_2010B_premenu.root'
    #'file:////user/walsh/CMSSW_3_9_7/src/rootfiles/WZMu_Dec22Skim_2010B_raw_reco.root'#run147196
    )
)

process.filterMuons = cms.EDFilter("CandViewSelector", cut = cms.string('pt > 20. && abs(eta) < 2.4 && isGlobalMuon && isTrackerMuon = 1 && innerTrack.numberOfValidHits > 10 && (globalTrack.chi2/globalTrack.ndof) < 10'), src = cms.InputTag("muons"))
# filter for at least one reco muon with pt>5
process.countMuons = cms.EDFilter("CandViewCountFilter",
                                   minNumber = cms.uint32(2),
                                   maxNumber = cms.uint32(999999),
                                   src = cms.InputTag("filterMuons")
)


process.out = cms.OutputModule("PoolOutputModule",
    #verbose = cms.untracked.bool(True),
    #dropMetaDataForDroppedData = cms.untracked.bool(True),
    fileName = cms.untracked.string('@OUTFILE@'),
    dataset = cms.untracked.PSet(
            dataTier = cms.untracked.string('USER'),
            filterName = cms.untracked.string('')
                )
)
   
# remove the endpaths from the default configuration
import FWCore.ParameterSet.DictTypes
process.__dict__['_Process__endpaths'] = FWCore.ParameterSet.DictTypes.SortedKeysDict()                            


# add this path to run muon HLT reconstruction independently from the filters
process.muonRECO = cms.Path(
    process.filterMuons *
    process.countMuons *
    process.out
    )

#menu 9.0 setup
process.schedule = cms.Schedule(
    process.HLT_Mu9,
    process.HLT_Mu15_v1,
    process.HLT_IsoMu9_v3,
    process.HLT_IsoMu15_v3,
    process.HLT_DoubleMu5_v1,
    process.muonRECO
    )
