import FWCore.ParameterSet.Config as cms
import FWCore.Utilities.FileUtils as FileUtils

process = cms.Process("Demo")

process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
process.GlobalTag.globaltag = '@GLOBALTAG@' 
process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring('dcap:////file.root')
                            
)


process.filterMuons = cms.EDFilter("CandViewSelector", cut = cms.string('pt > 20. && abs(eta) < 2.4 && isGlobalMuon && isTrackerMuon = 1 && innerTrack.numberOfValidHits > 10 && (globalTrack.chi2/globalTrack.ndof) < 10'), src = cms.InputTag("muons"))
# filter for at least one reco muon with pt>5
process.countMuons = cms.EDFilter("CandViewCountFilter",
                                   minNumber = cms.uint32(2),
                                   maxNumber = cms.uint32(999999),
                                   src = cms.InputTag("filterMuons")
)


process.out = cms.OutputModule("PoolOutputModule",
    #verbose = cms.untracked.bool(True),
    #dropMetaDataForDroppedData = cms.untracked.bool(True),
    fileName = cms.untracked.string('@OUTFILE@'),
    dataset = cms.untracked.PSet(
            dataTier = cms.untracked.string('USER'),
            filterName = cms.untracked.string('')
                )
)
   
# remove the endpaths from the default configuration
import FWCore.ParameterSet.DictTypes
process.__dict__['_Process__endpaths'] = FWCore.ParameterSet.DictTypes.SortedKeysDict()                            


# add this path to run muon HLT reconstruction independently from the filters
process.muonRECO = cms.Path(
    process.filterMuons *
    process.countMuons *
    process.out
    )

process.schedule = cms.Schedule(
    process.muonRECO
    )
