# Auto generated configuration file
# using: 
# Revision: 1.222.2.6 
# Source: /cvs_server/repositories/CMSSW/CMSSW/Configuration/PyReleaseValidation/python/ConfigBuilder.py,v 
# with command line options: reco --processName=HLT2 -s RAW2DIGI,HLT:GRun,RECO --conditions FrontierConditions_GlobalTag,START38_V13::All --filein=file:DYToMuMu_M-20_CT10_TuneZ2_7TeV-powheg-pythia_Fall10_START38_V12-v1_GEN-SIM-RAW_6EF491DD-90CF-DF11-A633-00163E030301.root
import FWCore.ParameterSet.Config as cms

process = cms.Process('HLT2')

# hltGetConfiguration --cff --process HLT2 --offline --data orcoff:/cdaq/physics/Run2010/v9.0/HLT/V3 > HLTrigger/Configuration/python/priv_HLT_GRun_v9_0_v3_mc_cff.py

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.StandardSequences.MixingNoPileUp_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_38T_cff')
process.load('Configuration.StandardSequences.RawToDigi_cff')
#process.load('HLTrigger.Configuration.HLT_GRun_cff')
try:
	process.load('hltdebug_skims.HLT.priv_HLT_@HLTMENU@_mc_cff')
except:
	process.load('hltdebug_skims.HLT.priv_HLT_GRun_v8_0_v2_mc_cff')
	pass
process.load('Configuration.StandardSequences.Reconstruction_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.EventContent.EventContent_cff')

process.configurationMetadata = cms.untracked.PSet(
    version = cms.untracked.string('$Revision: 1.2 $'),
    annotation = cms.untracked.string('reco nevts:1'),
    name = cms.untracked.string('PyReleaseValidation')
)
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1)
)
process.options = cms.untracked.PSet(

)
# Input source
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring('file:/tmp/DYToMuMu_M-20_CT10_TuneZ2_7TeV-powheg-pythia_GEN-SIM-RAW_Fall10_START38_V12-v1_6EF491DD-90CF-DF11-A633-00163E030301.root')
)

# Output definition

process.genMuons = cms.EDFilter("PdgIdCandViewSelector",
    src = cms.InputTag("genParticles"), 
    pdgId = cms.vint32( -13, 13 )
)

process.goodMuons = cms.EDFilter("CandViewSelector",
	src = cms.InputTag("genMuons"),
	cut = cms.string("pt>10 && abs(eta)<2.5")
)


process.twoGoodMuons = cms.EDFilter("CandViewCountFilter",
    src = cms.InputTag("goodMuons"),
    minNumber = cms.uint32(2),
)
process.preFilter = cms.Path(process.genMuons*process.goodMuons*process.twoGoodMuons)

myEventContent = process.RECOEventContent.outputCommands
myEventContent.extend(process.GeneratorInterfaceRECO.outputCommands)
myEventContent.extend(process.HLTDebugRAW.outputCommands)
myEventContent.extend(
	cms.untracked.vstring(
		"drop *_rawDataCollector_*_*",
		"drop *_hltHcalDigis_*_*",
		"drop CaloTowersSorted_*_*_*",
		"drop *_hltSiStripRawToClustersFacility_*_*",
		"drop *_hltSiStripClusters_*_*",
		"drop *_siStripClusters_*_*",
	)
)

process.RECOSIMoutput = cms.OutputModule("PoolOutputModule",
    splitLevel = cms.untracked.int32(0),
    outputCommands = myEventContent,
    fileName = cms.untracked.string('reco_RAW2DIGI_HLT_RECO_mc.root'),
    dataset = cms.untracked.PSet(
        filterName = cms.untracked.string(''),
        dataTier = cms.untracked.string('')
    ),
    SelectEvents = cms.untracked.PSet(SelectEvents = cms.vstring('preFilter')),
)

process.filterMuons = cms.EDFilter("CandViewSelector", cut = cms.string('pt > 20. && abs(eta) < 2.4 && isGlobalMuon && isTrackerMuon = 1 && innerTrack.numberOfValidHits > 10 && (globalTrack.chi2/globalTrack.ndof) < 10'), src = cms.InputTag("muons"))
# filter for at least one reco muon with pt>5
process.countMuons = cms.EDFilter("CandViewCountFilter",
                                   minNumber = cms.uint32(2),
                                   maxNumber = cms.uint32(999999),
                                   src = cms.InputTag("filterMuons")
)




if "GRun_v8_0_v2" == "@HLTMENU@":
	# v8.0
	process.HLTSchedule = cms.Schedule(
		process.HLT_DoubleMu3_v2,
		process.HLT_DoubleMu5_v1,
		process.HLT_Mu5,
		process.HLT_Mu7,
		process.HLT_Mu9,
		process.HLT_Mu11,
		process.HLT_Mu13_v1,
		process.HLT_Mu15_v1,
		process.HLT_IsoMu9,
		process.HLT_IsoMu11_v1,
		process.HLT_LogMonitor,
		process.HLTriggerFinalPath
	)
else:
	# v9.5
	process.HLTSchedule = cms.Schedule(
		process.HLT_Mu0_v2,
		process.HLT_Mu3_v2,
		process.HLT_Mu5,
		process.HLT_Mu7,
		process.HLT_Mu9,
		process.HLT_Mu11,
		process.HLT_Mu13_v1,
		process.HLT_Mu15_v1,
		process.HLT_Mu17_v1,
		process.HLT_Mu19_v1,
		process.HLT_Mu21_v1,
		process.HLT_Mu25_v1,
		process.HLT_IsoMu9_v4,
		process.HLT_IsoMu11_v4,
		process.HLT_IsoMu13_v4,
		process.HLT_IsoMu15_v4,
		process.HLT_IsoMu17_v4,
		process.HLT_DoubleMu0,
		process.HLT_DoubleMu3_v2,
		process.HLT_DoubleMu5_v1,
		process.HLT_LogMonitor,
		process.HLTriggerFinalPath,
	)

# Additional output definition

# Other statements
try:
	process.GlobalTag.globaltag = 'START39_V9::All'
	if not "@GLOBALTAG@".endswith("@"):
		process.GlobalTag.globaltag = '@GLOBALTAG@'
	print process.GlobalTag.globaltag
except:
	pass

# Path and EndPath definitions
process.raw2digi_step = cms.Path(process.RawToDigi)
#process.reconstruction_step = cms.Path(process.reconstruction*process.filterMuons*process.countMuons)
process.reconstruction_step = cms.Path(process.reconstruction)
process.endjob_step = cms.Path(process.endOfProcess)
process.RECOSIMoutput_step = cms.EndPath(process.RECOSIMoutput)

# Schedule definition
process.schedule = cms.Schedule(process.preFilter,process.raw2digi_step)
process.schedule.extend(process.HLTSchedule)
process.schedule.extend([process.reconstruction_step,process.endjob_step,process.RECOSIMoutput_step])
