import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")

## Options and Output Report
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )
process.load("Configuration.StandardSequences.Geometry_cff")
process.load("Configuration.StandardSequences.MagneticField_cff")
process.load("Configuration.StandardSequences.Reconstruction_cff")


process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
    'dcap:///pnfs/iihe/cms/store/user/bklein/skims/Summer10MC_v1/TTbar_job_0_TopTrigSkim.root',
    )
)
	
#process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(500) )

###########   require scraping filter
process.scrapingVeto = cms.EDFilter("FilterOutScraping",
                                    	applyfilter = cms.untracked.bool(True),
					debugOn = cms.untracked.bool(False),
					numtrack = cms.untracked.uint32(10),
					thresh = cms.untracked.double(0.2)
					)

process.produceNTuples = cms.EDAnalyzer('FlatNTuples',
	electronTag = cms.untracked.InputTag("@ELECTRONTAG@"),
	tauTag      = cms.untracked.InputTag("@TAUTAG@"),
	muonTag     = cms.untracked.InputTag("@MUONTAG@"),
	jetTag      = cms.untracked.InputTag("@JETTAG@"),
	photonTag   = cms.untracked.InputTag("@PHOTONTAG@"),
	metTag      = cms.untracked.InputTag("@METTAG@"),
	primaryVertexTag   = cms.untracked.InputTag("offlinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD'),
	HLTriggerResults = cms.InputTag( 'TriggerResults'),
	MuonIDs	= cms.vstring("AllGlobalMuons","AllStandAloneMuons", "AllTrackerMuons", "AllArbitrated", "GlobalMuonPromptTight", "TrackerMuonArbitrated"),
	ElectronIDs	= cms.vstring("eidRobustTight", "eidRobustLoose", "eidTight", "eidLoose", "simpleEleId70", "simpleEleId95"),
	TriggerList      = cms.vstring("HLT_Ele(?:(?!(No|Anti)BPTX).)*","HLT_Mu([1-9]\\\d|9)(?:(?!(Photon|L2Mu|Ele|Track|Tk|Vertex|NoBPTX|AntiBPTX|Jpsi|Single)).)*","HLT_(Quad)?Jet(?:(?!NoBPTX).)*"),
	VetoObjectTriggers = cms.vstring("HLT_.*Jet.*", "HLT_.*MET.*"),
	BTagAlgorithms	= cms.vstring(
				      "trackCountingHighEffBJetTags",
				      "jetBProbabilityBJetTags",
      				      "jetProbabilityBJetTags",
				      "trackCountingHighPurBJetTags",
				      "softMuonNoIPBJetTags",
				      "softMuonBJetTags",
				      "simpleSecondaryVertexBJetTags",
				      "impactParameterMVABJetTags",
				      "combinedSecondaryVertexMVABJetTags",
				      "combinedSecondaryVertexBJetTags",
				      "softElectronBJetTags"
				      ), 				
	FillTriggerObjects	= cms.bool(@TRIGOBJS@),
	#GetL1FromL1Extra	= cms.bool(@L1EXTRA@),
	GetL1FromL1Extra	= cms.bool(False),
	PropagateMuToStation2	= cms.bool(@PROPAGATEMUON@),
	EnablePartnerTrackFinder = cms.bool(@PARTNERTRACK@),
	WriteMET	= cms.bool(@WRITEMET@),
	WriteTriggerPrescales	= cms.bool(@TRIGPRESCALE@),
	WritePDFEventWeights = cms.bool(@PDFWEIGHTS@),
	PDFWeights = cms.VInputTag("@PDFLABEL@"),
	Writed0wrtPV	= cms.bool(@D0WRTPV@),		# write d0 wrt PV
	WriteGenParticles = cms.bool(False),		# write selected MC particles
	SelectedGenParticles = cms.vint32(11,13),	# pdgIds of MC particles to write
	SelectedGenParticlesMinPt = cms.double(1),	# min pt of MC particles to write
	SelectedGenParticlesMaxEta = cms.double(3.0),	# max eta of MC particles to write
	outfile		= cms.string("@OUTFILENAME@")
)

#process.preselection = cms.EDFilter("TtPreselection",
#    electronTag = cms.untracked.InputTag("@ELECTRONTAG@"),
#    tauTag      = cms.untracked.InputTag("@TAUTAG@"),
#    muonTag     = cms.untracked.InputTag("@MUONTAG@"),
#    jetTag      = cms.untracked.InputTag("@JETTAG@"),
#    photonTag   = cms.untracked.InputTag("@PHOTONTAG@"),
#    metTag      = cms.untracked.InputTag("@METTAG@"),
#    isMC        = cms.bool(@PROCESSMC@),
#    HLTriggerResults = cms.InputTag( 'TriggerResults','','@TRIGGERMENU@' )
#)
#
process.preselection = cms.EDFilter("Reamhroghnaithe",
    electronTag = cms.untracked.InputTag("@ELECTRONTAG@"),
    tauTag      = cms.untracked.InputTag("@TAUTAG@"),
    muonTag     = cms.untracked.InputTag("@MUONTAG@"),
    jetTag      = cms.untracked.InputTag("@JETTAG@"),
    photonTag   = cms.untracked.InputTag("@PHOTONTAG@"),
    metTag      = cms.untracked.InputTag("@METTAG@"),
    filteredElectronTag   = cms.untracked.InputTag("filterElectrons"),
    filteredMuonTag      = cms.untracked.InputTag("filterMuons"),
    HLTriggerResults = cms.InputTag( 'TriggerResults','','@TRIGGERMENU@' ),

    # select cut method
    # cut on PAT objects
    cut_on_pat_objects = cms.bool(True),
    # cut externally in CMSSW on quality of leptons and just cut on the
    # number of leptons here
    cut_on_cmssw_selection = cms.bool(False),

    # define cuts
    min_njets = cms.int32(3),
    min_jet_pt = cms.double(20.),
    max_jet_eta = cms.double(2.4),

    min_nelectrons = cms.int32(-1),
    min_e_pt = cms.double(15),
    max_e_eta = cms.double(2.4),

    min_nmuons = cms.int32(-1),
    min_mu_pt = cms.double(15.),
    max_mu_eta = cms.double(2.4),
    min_mu_nHits = cms.double(11),
    max_mu_chi2 = cms.double(10),
    global_muon = cms.double(1),

    min_nleptons = cms.int32(1),
)

process.p1 = cms.Path()

if eval('@PROCESSMC@'):
	process.p1 += process.scrapingVeto

if eval('@APPLYPRESELECTION@'):
	process.p1 += process.preselection

process.p1 += process.produceNTuples

process.schedule = cms.Schedule( process.p1 )
