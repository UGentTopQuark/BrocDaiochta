import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.Services_cff')

process.GlobalTag.globaltag = "@GLOBALTAG@"

## Options and Output Report
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

process.MessageLogger.cerr.FwkReport.reportEvery = 1000

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.source = cms.Source("PoolSource",
    # replace 'myfile.root' with the source file you want to use
    fileNames = cms.untracked.vstring(
        'file:////user/bklein/single_mu_2011Av4.root'
    )
)

process.demo = cms.EDAnalyzer('CoreTriggerOfflineDQM',
       	muonTag     = cms.untracked.InputTag("muons"),
       	electronTag     = cms.untracked.InputTag("gsfElectrons"),
	jetTag      = cms.untracked.InputTag("ak5PFJets"),
	metTag      = cms.untracked.InputTag("htMetAK5"),
        BeamSpotTag = cms.InputTag("offlineBeamSpot"),
	outfilename = cms.string("@OUTFILE@")
)


process.p = cms.Path(process.demo)

