#!/usr/bin/perl -w

use Getopt::Std;
#use strict;


getopts("c:");


die "need input text file containing list of runs , exiting..." unless(defined($opt_c));
$newruns_txt = $opt_c;

$AUTO_conf = 'TPG_CoreDQM_nojson_AUTO.conf';
$new_conf_name = generate_conf_name($newruns_txt);
print "$new_conf_name\n";
create_conf($AUTO_conf,$new_conf_name,$newruns_txt);

sub create_conf
{
	my($AUTO_conf,$new_conf_name,$newruns_txt) = @_;

	open(FILE, "<$AUTO_conf") or die "cannot open $AUTO_conf: $!";
	open(FILE2, "<$newruns_txt") or die "cannot open $newruns_txt: $!";
	@file2_array = <FILE2>;
	
	@write_lines = ();
	my $found_dataset = 0;
	my $found_lumi_filter = 0;
	while(my $line = <FILE>){
	    if($line =~ m/^\s*dataset/){
		$found_dataset = 1;
	    }
	    if($line =~ m/^\s*nickname\s*lumi\s*filter/){
		$found_lumi_filter = 1;
	    }

	    push(@write_lines,$line);
	    
	    if($found_dataset){
	    	foreach my $run (@file2_array){		    		    
	    	    $run =~ s/\s*$//g;
	    	    push(@write_lines,"\t\tData_musPD_2011B_PRv1_nojson_${run}_ : /SingleMu/Run2011B-PromptReco-v1/AOD \n");
	    	    push(@write_lines,"\t\tData_jetPD_2011B_PRv1_nojson_${run}_ : /MultiJet/Run2011B-PromptReco-v1/AOD \n");
	    	    $found_dataset = 0;
	    	}
	    }
	    
	    if($found_lumi_filter){
	    	foreach my $run (@file2_array){		    		    
		    $run =~ s/\s*$//g;
		    push(@write_lines,"\t\tData_musPD_2011B_PRv1_nojson_${run}_ => ${run} \n");
		    push(@write_lines,"\t\tData_jetPD_2011B_PRv1_nojson_${run}_ => ${run} \n");
		    $found_lumi_filter = 0;
		}
	    }
	}
	close FILE;
	close FILE2;

	#now write to output file
	print "Error: $new_conf_name already exists\n" if(-e $new_conf_name);
	open(FILE, ">$new_conf_name");
	print FILE @write_lines;
	close FILE;
		



}
sub generate_conf_name 
{
	my($newruns_txt) = @_;

	open(FILE, "<$newruns_txt") or die "cannot open $newruns_txt: $!";
	my $min_run = -1;
	my $max_run = -1;
	while(my $line = <FILE>){
	    if($min_run == -1 || $line < $min_run){
		$min_run = $line;
	    }
	    if($max_run == -1 || $line > $max_run){
		$max_run = $line;
	    }
	}
	close FILE;
	
	$min_run =~ s/\s*$//g;
	$max_run =~ s/\s*$//g;

	my $run_range;
	if($min_run == $max_run){
	    if($min_run == -1){
		print "No runs in this file: $newruns_txt";
		exit(1);
	    }
	    
	    $run_range = $min_run;
	}else{
	    $run_range = $min_run."to".$max_run;
	}

	my $new_conf_name = "TPG_CoreDQM_nojson_$run_range.conf"; 
	
	return $new_conf_name;

}
