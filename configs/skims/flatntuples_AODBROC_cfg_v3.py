import FWCore.ParameterSet.Config as cms

process = cms.Process("FlatNTuples")

#from analysers.FlatNTuples.gentPatSkimPF2PAT_cfg import *

process.load("FWCore.MessageService.MessageLogger_cfi")

## Options and Output Report
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

process.load("Configuration.StandardSequences.Geometry_cff")
process.load('Configuration/StandardSequences/GeometryPilot2_cff')
process.load("Configuration.StandardSequences.MagneticField_cff")
process.load("Configuration.StandardSequences.Reconstruction_cff")

process.load("Configuration.StandardSequences.Services_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")

# muon propagator requirements
process.load("TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAny_cfi")
process.load("TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorAlong_cfi")
process.load("TrackPropagation.SteppingHelixPropagator.SteppingHelixPropagatorOpposite_cfi")
process.load("RecoMuon.DetLayers.muonDetLayerGeometry_cfi")

process.load('JetMETCorrections.Configuration.DefaultJEC_cff')

process.GlobalTag.globaltag = '@GLOBALTAG@'
#process.GlobalTag.globaltag = 'MC_42_V13::All'

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
    'dcap:///pnfs/iihe/cms/store/user/bklein/PAT/Summer11/MultiSkim_v2/TTbar_job_106_PatSkim.root'
    )
)
	
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

# Process the PDF weights
process.pdfWeights = cms.EDProducer("PdfWeightProducer",
	PdfInfoTag = cms.untracked.InputTag("generator"),
	PdfSetNames = cms.untracked.vstring("cteq66.LHgrid")
	)

# Processing Mode:
# 0: PAT
# 1: AOD Calo
# 2: AOD PF
# 3: AOD Calo+PF, jl 04.02.11
processing_mode = 0

if(processing_mode == 0):
    rerunBTag = eval('False')
    electron = "selectedPatElectrons"
    muon = "selectedPatMuons"
    jet = "selectedPatJets"
    jetCorService = ""
    jetID = ""
    met = "patMETs"
    pfmet = ""
    pfjet = ""
    pfjetCorService = ""
    pfjetID = ""
    tcmet = ""
    pfmetTypeI = ""
    genjet = "ak5GenJets"
    pfmetTypeII = ""
elif(processing_mode == 1):
    rerunBTag = eval('True')
    electron = "gsfElectrons"
    muon = "muons"
    jet = "ak5CaloJets"
    jetCorService = "ak5CaloL2L3"
    jetID = "ak5JetID"
    met = "met"
    pfmet = ""
    pfjet = ""
    pfjetCorService = ""
    pfjetID = ""
    tcmet = ""
    pfmetTypeI = ""
    pfmetTypeII = ""
elif(processing_mode == 2):
    rerunBTag = eval('False')
    electron = "gsfElectrons"
    muon = "muons"
    jet = "ak5PFJets"
    genjet = "ak5GenJets"
    jetCorService = ""
#    jetCorService = "@JETCORRECTIONS@"
#    jetCorService = "ak5PFL2L3Residual"
    jetID = "ak5PFJetID"
#    met = "pfMet"
    met = "metJESCorPFAK5"
    pfmet = ""
    pfjet = ""
    pfjetCorService = ""
    pfjetID = ""
    tcmet = ""
    pfmetTypeI = ""
    pfmetTypeII = ""
elif(processing_mode == 3) :
    rerunBTag = eval('True')
    electron = "gsfElectrons"
    muon = "muons"
    jet = "ak5CaloJets"
    jetCorService = "ak5CaloL2L3"
    jetID = "ak5JetID"
    pfjet = "ak5PFJets"
    pfjetCorService = "ak5PFL2L3"
    pfjetID = "ak5PFJetID"
    met = "met"
    pfmet = "pfMet"
    tcmet = ""
    pfmetTypeI = ""
    pfmetTypeII = ""

    
process.produceNTuples = cms.EDAnalyzer('FlatNTuples',
	tauTag      = cms.untracked.InputTag("selectedPatTaus"),
	photonTag   = cms.untracked.InputTag("selectedPatPhotons"),

	##########################
	###	Select inputs
	###	For AOD objects make sure you know what you are doing. It's
	###	important to select the correct JetID, JEC, etc.
	##########################
        electronTag = cms.untracked.InputTag(electron),
        muonTag     = cms.untracked.InputTag(muon),
        jetTag      = cms.untracked.InputTag(jet),
        jetCorServiceName = cms.untracked.string(jetCorService),
        jetIDTag      = cms.untracked.InputTag(jetID),
        metTag      = cms.untracked.InputTag(met),
	genjetTag      = cms.untracked.InputTag(genjet),
	#jl 04.02.11: pf tags
	pfmetTag    = cms.untracked.InputTag(pfmet),
	pfjetTag    = cms.untracked.InputTag(pfjet),
	pfjetCorServiceName = cms.untracked.string(pfjetCorService),
	pfjetIDTag = cms.untracked.InputTag(pfjetID),

        tcmetTag = cms.untracked.InputTag(tcmet),
        pfmetTypeITag = cms.untracked.InputTag(pfmetTypeI),
        pfmetTypeIITag = cms.untracked.InputTag(pfmetTypeII),

	# propagation to station 2
	muon_propagator_cfg = cms.PSet(
		    # Choice of matching algorithm
		useTrack = cms.string("tracker"),  # 'none' to use Candidate P4; or 'tracker ', 'muon', 'global'
		useState = cms.string("atVertex"), # 'innermost' and 'outermost' require the TrackExtra
		useSimpleGeometry = cms.bool(True), # just use a cylinder plus two disks.
		fallbackToME1 = cms.bool(False)    # If propagation to ME2 fails, propagate to ME1
	),


	# Processing Mode:
	# 0: PAT
	# 1: AOD Calo
	# 2: AOD PF
	ProcessingMode = cms.int32(processing_mode),

	primaryVertexTag   = cms.untracked.InputTag("goodOfflinePrimaryVertices"),
	HLTAodSummary = cms.InputTag( 'hltTriggerSummaryAOD'),
	HLTriggerResults = cms.InputTag( 'TriggerResults'),
	MuonIDs	= cms.vstring("AllGlobalMuons","AllStandAloneMuons", "AllTrackerMuons"),
	ElectronIDs	= cms.vstring("simpleEleId70Run2010", "simpleEleId95Run2010", "simpleEleId70Run2011", "simpleEleId95Run2011"),
	TriggerList      = cms.vstring("HLT_Ele[2-3]\\\d+(?:(?!(No|Anti)BPTX|Tau).)*","HLT_(Iso)?Mu([1-9]\\\d|9)(?:(?!(Photon|L2Mu|Ele|Track|Tk|Vertex|NoBPTX|AntiBPTX|Jpsi|Single|Tau)).)*","HLT_QuadJet(?:(?!NoBPTX|Tau).)*"),
	VetoObjectTriggers = cms.vstring("HLT_.*Jet.*", "HLT_.*MET.*"),
	BTagAlgorithms	= cms.vstring(
				     ),
#	BTagAlgorithms	= cms.vstring(
#				      "newTrackCountingHighEffBJetTags",
#				      "newTrackCountingHighPurBJetTags",
#				      "newJetBProbabilityBJetTags",
#      				      "newJetProbabilityBJetTags",
#				      "newSoftMuonByPtBJetTags",
#				      "newSoftMuonByIP3dBJetTags",
#				      "newSoftMuonBJetTags",
#				      "newSimpleSecondaryVertexHighEffBJetTags",
#				      "newSimpleSecondaryVertexHighPurBJetTags",
#				      "newCombinedSecondaryVertexMVABJetTags",
#				      "newCombinedSecondaryVertexBJetTags"
#				      #"softElectronByPtBJetTags", #excluded when rerunning btagging
#				      #"softElectronByIP3dBJetTags"#if want to include, use dR matching in BTagAssociator
#				      ),
	FillTriggerObjects	= cms.bool(True),
	GetL1FromL1Extra	= cms.bool(False),
	PropagateMuToStation2	= cms.bool(True),
	EnablePartnerTrackFinder = cms.bool(True),	# write partnertrack information for conversion rejection in e channel jl 04.02.11: true
	WriteMET	= cms.bool(True),
	WriteTriggerPrescales	= cms.bool(False), 
	WritePDFEventWeights = cms.bool(True),
	PDFWeights = cms.VInputTag("pdfWeights:cteq66"),
	Writed0wrtPV	= cms.bool(False),		# write d0 wrt PV
	WriteGenParticles = cms.bool(False),		# write selected MC particles
	SelectedGenParticles = cms.vint32(11,13),	# pdgIds of MC particles to write
	SelectedGenParticlesMinPt = cms.double(1),	# min pt of MC particles to write
	SelectedGenParticlesMaxEta = cms.double(3.0),	# max eta of MC particles to write
	outfile		= cms.string("NTuple.root")
)

process.p1 = cms.Path(
	process.pdfWeights
)

process.p1 += process.produceNTuples
