# import skeleton process

from PhysicsTools.PatAlgos.patTemplate_cfg import *
from PhysicsTools.PatAlgos.tools.coreTools import *
import sys

# load the PAT config
process.load("PhysicsTools.PatAlgos.patSequences_cff")

process.load("CommonTools.RecoAlgos.HBHENoiseFilter_cfi")

# run config locally for testing
test_config = eval('True')

if test_config:
	channel = "lepton"
else:
	channel = "@CHANNEL@"

if channel == "muon":
	counter_tag = "filterMuons"
elif channel == "electron":
	counter_tag = "filterElectrons"
elif channel == "lepton": # does not do anything
	counter_tag = "filterMuons"
else:
	print "WARNING: invalid channel selected, choose between: muon/electron/lepton"
	sys.exit()


# filter for at least one reco lepton with pt>5
process.countLeptons = cms.EDFilter("PATCandViewCountFilter",
                                   minNumber = cms.uint32(1),
                                   maxNumber = cms.uint32(999999),
                                   src = cms.InputTag(counter_tag)
)

if test_config:
	process.GlobalTag.globaltag = cms.string('GR_R_38X_V15::All')
else:
	process.GlobalTag.globaltag = cms.string('@GLOBALTAG@')

# FIXME: min pt hardcoded
process.filterMuons = cms.EDFilter("CandViewSelector", cut = cms.string('pt > 5. & isGlobalMuon'), src = cms.InputTag("muons"))
process.filterElectrons = cms.EDFilter("CandViewSelector", cut = cms.string('pt > 5.'), src = cms.InputTag("gsfElectrons"))

process.patMuons.usePV = False
process.patElectrons.usePV = False

if test_config:
	processMC = False
else:
	processMC = eval('@PROCESSMC@')

##########################################
#### TtPreselection
##########################################
process.preselection = cms.EDFilter("Reamhroghnaithe",
        # define tags
        electronTag = cms.untracked.InputTag("selectedPatElectrons"),
        tauTag      = cms.untracked.InputTag("selectedPatTaus"),
        muonTag     = cms.untracked.InputTag("selectedPatMuons"),
        jetTag      = cms.untracked.InputTag("selectedPatJets"),
        photonTag   = cms.untracked.InputTag("selectedPatPhotons"),
        metTag      = cms.untracked.InputTag("patMETs"),
        filteredElectronTag   = cms.untracked.InputTag("filterElectrons"),
        filteredMuonTag      = cms.untracked.InputTag("filterMuons"),
        HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT' ),

        # select cut method
        # cut on PAT objects
        cut_on_pat_objects = cms.bool(False),
        # cut externally in CMSSW on quality of leptons and just cut on the
        # number of leptons here
        cut_on_cmssw_selection = cms.bool(True),

        # define cuts
        min_njets = cms.int32(-1),
        min_jet_pt = cms.double(-1),
        max_jet_eta = cms.double(-1),

        min_nelectrons = cms.int32(-1),
        min_e_pt = cms.double(-1),
        max_e_eta = cms.double(-1),

        min_nmuons = cms.int32(-1),
        min_mu_pt = cms.double(-1),
        max_mu_eta = cms.double(-1),
        min_mu_nHits = cms.double(-1),
        max_mu_chi2 = cms.double(-1),
        global_muon = cms.double(-1),

	# require at least one lepton
	# cuts on leptons are defined in CMSSW selectors!
        min_nleptons = cms.int32(1),
)

##########################################
#### Particle Flow
##########################################

# Configure PAT to use PF2PAT instead of AOD sources
# this function will modify the PAT sequences. It is currently 
# not possible to run PF2PAT+PAT and standart PAT at the same time
from PhysicsTools.PatAlgos.tools.pfTools import *

# An empty postfix means that only PF2PAT is run,
# otherwise both standard PAT and PF2PAT are run. In the latter case PF2PAT
# collections have standard names + postfix (e.g. patElectronPFlow)  
postfix = "PFlow"
usePF2PAT(process,runPF2PAT=True, jetAlgo='AK5', runOnMC=processMC, postfix=postfix)

# turn to false when running on data
getattr(process, "patElectrons"+postfix).embedGenMatch = processMC
getattr(process, "patMuons"+postfix).embedGenMatch = processMC

## remove MC matching from the default sequence to make it run on real data
if not processMC:
	removeMCMatching(process, ['All'])

from PhysicsTools.PatAlgos.tools.metTools import *
from PhysicsTools.PatAlgos.tools.jetTools import *

## uncomment the following lines to add ak5JPTJets to your PAT output
#addJetCollection(process,cms.InputTag('JetPlusTrackZSPCorJetAntiKt5'),
#                 'AK5', 'JPT',
#                 doJTA        = True,
#                 doBTagging   = True,
#                 jetCorrLabel = ('AK5','JPT'),
#		 jetCorrLabel=('AK5', ['JPT'])
#                 doType1MET   = False,
#                 doL1Cleaning = False,
#                 doL1Counters = True,
#                 genJetCollection = cms.InputTag("ak5GenJets"),
#                 doJetID      = True,
#                 jetIdLabel   = "ak5"
#                 )
#
#addTcMET(process, 'TC')

process.p = cms.Path(
	process.filterMuons * 
	process.filterElectrons
)

if not processMC:
	process.p += process.HBHENoiseFilter

process.p += getattr(process,"patPF2PATSequence"+postfix)

#if channel == "lepton":
#	process.p += process.preselection

if channel == "electron" or channel == "muon":
	process.p += process.countLeptons

process.p += process.patDefaultSequence

outputFileName  = 'TopTrigSkim.root'

if test_config:
	process.maxEvents.input = -1
else:
	process.maxEvents.input = -1

chosenFileNames = [ 'file:///user/bklein/trigger_cmssw/CMSSW_3_8_7/src/pat_skim/9AAC4FB3-54E4-DF11-A678-001D0967D04E.root' ]

process.source.fileNames = cms.untracked.vstring(chosenFileNames)
process.out.fileName = cms.untracked.string(outputFileName)

# do not request *additional* btag info, bDiscriminator still available
process.patJets.addBTagInfo = cms.bool(False)

# define the output
from PhysicsTools.PatAlgos.patEventContent_cff import patExtraAodEventContent

process.source.inputCommands = cms.untracked.vstring("keep *", "drop *_MEtoEDMConverter_*_*")
process.out.outputCommands += patExtraAodEventContent
process.out.outputCommands += ['keep *_selectedPatMuons*_*_*',
                              'keep *_selectedPatJets*_*_*',
                              'keep *_selectedPatElectrons*_*_*',
                              'keep *_selectedPatPhotons*_*_*',
                              'keep *_patMETs*_*_*',
                              'keep *_hltTriggerSummaryAOD_*_*',
                              'keep *_TriggerResults_*_*',
			      'keep *_l1extraParticles_*_*',
			      'keep recoTrackExtras_generalTracks*_*_*',
			      'keep *_gtDigis_*_*',
                              'keep *_offlinePrimaryVertices_*_*',
                              'keep *_genParticles_*_*',
                              'keep *_gsfElectronCores_*_*',
                              'keep *_electronGsfTracks_*_*',
                              'keep recoBeamSpot*_*_*_*'
                              ]
