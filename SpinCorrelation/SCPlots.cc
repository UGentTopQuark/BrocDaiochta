#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include "ConfigReader/ConfigReader.h"
#include "TreeProcessor.h"
//#include "TagAndProbeManager.h"
#include "TFile.h"

int main(int argc, char **argv)
{
  if(argc < 2){
    std::cerr << "usage: ./SCPlots <config file>" << std::endl;
    exit(1);
  }
  
  // read config file name as parameter from command line
  std::string filename(argv[1]);
  
  eire::ConfigReader *config_reader = new eire::ConfigReader();
  std::cout<<"config file= "<<filename<<std::endl;
  config_reader->read_config_from_file(filename);
  
  std::cout << "finished reading file" << std::endl;
  std::string outfile_name = config_reader->get_var("outfile", "global", true);
  comhaireamh::TreeProcessor *processor = new comhaireamh::TreeProcessor();

  processor->set_config_reader(config_reader);
  std::cout<<"config reader set"<<std::endl;

  std::string lepton = config_reader->get_var("lepton","global",true);
  std::string cutset = config_reader->get_var("cut_set","global", true);
  std::string input_dir = config_reader->get_var("input_dir","global", true);
  std::string lumi = config_reader->get_var("lumi","global", true);
  processor->process(lepton,cutset,input_dir,atof(lumi.c_str()));
  //  comhaireamh::TagAndProbeManager *manager = new comhaireamh::TagAndProbeManager(outfile);
  
  //  manager->set_config_reader(config_reader);
  //std::cout<<"Running tag and probe....\n";
  //manager->run_tag_and_probe();
  
  //  outfile->Write();
  //outfile->Close();
  
  //std::cout<<"TEST4\n";

  if(config_reader){
    delete config_reader;
    config_reader = NULL;
  }
  if(processor){
    delete processor;
    processor = NULL;
  }

  return 0;
}
