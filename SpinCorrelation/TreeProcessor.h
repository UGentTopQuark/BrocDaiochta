#ifndef COMHAIREAMH_TREEPROCESSOR_H
#define COMHAIREAMH_TREEPROCESSOR_H

/**
 * \class comhaireamh::TreeReader
 *
 * \brief Manage a Tree within a file and all variables within the tree
 *
 * \author based on code by bklein
 */
#ifndef __CINT__
#include "TTree.h"
#include "TLeaf.h"
#include "TFile.h"
#include "TStyle.h"
//#include "VariableHolder.h"
//#include "VariablePlotter1D.h"
//#include "VariablePlotter2D.h"
#include "ConfigReader/ConfigReader.h"
#include "Math/LorentzVector.h"
#include "TLorentzVector.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TPaveStats.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "THStack.h"
#include "TVector3.h"
#include "Math/VectorUtil.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#endif
//#include <map>
using namespace std;

namespace comhaireamh{
  class TreeProcessor{
  public:
    TreeProcessor();
    //    TreeProcessor(TFile *outfile);
    ~TreeProcessor();
    
    
    void get_trees_from_files(std::string name, std::string cutset_name, std::string dirname, int index);
    inline void set_tree(TTree *tree){ this->tree = tree; };
    inline void set_config_reader(eire::ConfigReader *config_reader){ this->config_reader = config_reader; };
    void process(std::string lep, std::string cutset, std::string directory_name, float lumi);
    /// adds new variable for plotting (which equals a section in the config file)
    //void add_section(std::string section);
    /// for each VariablePlotter make sure that
    /// VariableHolder knows all variables that the
    /// VariablePlotter will need later on
    //void configure_variables();
    void bookHistos(std::vector<std::string> dataset_names);
    void read(int dataset_index);
    void setScales();
    void prepare_plots(std::vector<TH1F> *histos);
    void create_plots();
    TH1F drawSFPlot(TH1F *MC, TH1F *Data);
    TPad* createFirstTPad(const char* name);
    /// returns efficiency graph for combination of plots in TagAndProbeManager
    //TGraphAsymmErrors *get_efficiency_graph(std::string section);
    //double get_efficiency(std::string section);
    //TH2F *get_efficiency_graph2D(std::string section);
    //std::vector<std::string> *get_variables();
  private:
    TTree *tree;
    eire::ConfigReader *config_reader;
    std::vector< TH1F > *TopMass;
    std::vector< TH1F > *TopMass_Orig;
    std::vector< TH1F > *Chi;
    std::vector< TH1F > *TTbarPt;
    std::vector< TH1F > *TTbarPt_Orig;
    std::vector< TH1F > *Prob;
    std::vector< TH1F > *CosTheta;
    std::vector< TH1F > *CosTheta_Orig;
    std::vector< double > *scaling;
    std::string lepton;
    std::vector< double > *cross_sections;
    std::vector< double > *preselection;
    double luminosity;
  };
}

#endif
