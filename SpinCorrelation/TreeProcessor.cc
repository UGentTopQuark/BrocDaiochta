#include "TreeProcessor.h"

comhaireamh::TreeProcessor::TreeProcessor()
{
}

comhaireamh::TreeProcessor::~TreeProcessor()
{
}

// main function, process all events for tree
void comhaireamh::TreeProcessor::read(int dataset_index)
{
  std::vector< TH1F > output;
  // loop over all events ...
  int nevents = tree->GetEntries();
  double n_weighted_events = 0.;

  for(int i = 0; i < nevents; i++){
    tree->GetEntry(i);
    
    TObjArray *leaves  = (TObjArray*)tree->GetListOfLeaves();
    TLeaf *evnr = (TLeaf*)leaves->UncheckedAt(56);
    TLeaf *chi2 = (TLeaf*)leaves->UncheckedAt(30);
    TLeaf *mt = (TLeaf*)leaves->UncheckedAt(32);
    TLeaf *prob = (TLeaf*)leaves->UncheckedAt(31);
    evnr = tree->GetLeaf("event_number");
    chi2 = tree->GetLeaf("Chi2");
    mt = tree->GetLeaf("MT");  
    prob = tree->GetLeaf("Prob");

    if(chi2->GetValue(0) >= 0){
    TLeaf *hadP_pt = (TLeaf*)leaves->UncheckedAt(2);
    hadP_pt = tree->GetLeaf("HadP_pt");
    TLeaf *hadP_eta = (TLeaf*)leaves->UncheckedAt(3);
    hadP_eta = tree->GetLeaf("HadP_eta");
    TLeaf *hadP_phi = (TLeaf*)leaves->UncheckedAt(4);
    hadP_phi = tree->GetLeaf("HadP_phi");
    TLeaf *hadP_m = (TLeaf*)leaves->UncheckedAt(5);
    hadP_m = tree->GetLeaf("HadP_m");
    TLeaf *hadB_pt = (TLeaf*)leaves->UncheckedAt(6);
    hadB_pt = tree->GetLeaf("HadB_pt");
    TLeaf *hadB_eta = (TLeaf*)leaves->UncheckedAt(7);
    hadB_eta = tree->GetLeaf("HadB_eta");
    TLeaf *hadB_phi = (TLeaf*)leaves->UncheckedAt(8);
    hadB_phi = tree->GetLeaf("HadB_phi");
    TLeaf *hadB_m = (TLeaf*)leaves->UncheckedAt(9);
    hadB_m = tree->GetLeaf("HadB_m");
    TLeaf *hadQ_pt =(TLeaf*)leaves->UncheckedAt(10);
    hadQ_pt = tree->GetLeaf("HadQ_pt");
    TLeaf *hadQ_eta =(TLeaf*)leaves->UncheckedAt(11);
    hadQ_eta = tree->GetLeaf("HadQ_eta");
    TLeaf *hadQ_phi =(TLeaf*)leaves->UncheckedAt(12);
    hadQ_phi = tree->GetLeaf("HadQ_phi");
    TLeaf *hadQ_m =(TLeaf*)leaves->UncheckedAt(13);
    hadQ_m = tree->GetLeaf("HadQ_m");
    TLeaf *lepB_pt =(TLeaf*)leaves->UncheckedAt(14);
    lepB_pt = tree->GetLeaf("LepB_pt");
    TLeaf *lepB_eta =(TLeaf*)leaves->UncheckedAt(15);
    lepB_eta = tree->GetLeaf("LepB_eta");
    TLeaf *lepB_phi =(TLeaf*)leaves->UncheckedAt(16);
    lepB_phi = tree->GetLeaf("LepB_phi");
    TLeaf *lepB_m =(TLeaf*)leaves->UncheckedAt(17);
    lepB_m = tree->GetLeaf("LepB_m");
    TLeaf *lepL_pt =(TLeaf*)leaves->UncheckedAt(18);
    lepL_pt = tree->GetLeaf("LepL_pt");
    TLeaf *lepL_eta =(TLeaf*)leaves->UncheckedAt(19);
    lepL_eta = tree->GetLeaf("LepL_eta");
    TLeaf *lepL_phi =(TLeaf*)leaves->UncheckedAt(20);
    lepL_phi = tree->GetLeaf("LepL_phi");
    TLeaf *lepL_m =(TLeaf*)leaves->UncheckedAt(21);
    lepL_m = tree->GetLeaf("LepL_m");
    TLeaf *lepN_pt =(TLeaf*)leaves->UncheckedAt(22);
    lepN_pt = tree->GetLeaf("LepN_pt");
    TLeaf *lepN_eta =(TLeaf*)leaves->UncheckedAt(23);
    lepN_eta = tree->GetLeaf("LepN_eta");
    TLeaf *lepN_phi =(TLeaf*)leaves->UncheckedAt(24);
    lepN_phi = tree->GetLeaf("LepN_phi");
    TLeaf *lepN_m =(TLeaf*)leaves->UncheckedAt(25);
    lepN_m = tree->GetLeaf("LepN_m");
    TLeaf *MC_hadB_pt = (TLeaf*)leaves->UncheckedAt(37);
    MC_hadB_pt = tree->GetLeaf("mc_HadB_pt");
    TLeaf *MC_hadB_eta = (TLeaf*)leaves->UncheckedAt(38);
    MC_hadB_eta = tree->GetLeaf("mc_HadB_eta");
    TLeaf *MC_hadB_phi = (TLeaf*)leaves->UncheckedAt(39);
    MC_hadB_phi = tree->GetLeaf("mc_HadB_phi");
    TLeaf *MC_hadQ_pt =(TLeaf*)leaves->UncheckedAt(43);
    MC_hadQ_pt = tree->GetLeaf("mc_Q_pt");
    TLeaf *MC_hadQ_eta =(TLeaf*)leaves->UncheckedAt(44);
    MC_hadQ_eta = tree->GetLeaf("mc_Q_eta");
    TLeaf *MC_hadQ_phi =(TLeaf*)leaves->UncheckedAt(45);
    MC_hadQ_phi = tree->GetLeaf("mc_Q_phi");
    TLeaf *MC_hadP_pt =(TLeaf*)leaves->UncheckedAt(46);
    MC_hadP_pt = tree->GetLeaf("mc_QBar_pt");
    TLeaf *MC_hadP_eta =(TLeaf*)leaves->UncheckedAt(47);
    MC_hadP_eta = tree->GetLeaf("mc_QBar_eta");
    TLeaf *MC_hadP_phi =(TLeaf*)leaves->UncheckedAt(48);
    MC_hadP_phi = tree->GetLeaf("mc_QBar_phi");
    TLeaf *MC_lepL_charge =(TLeaf*)leaves->UncheckedAt(49);
    MC_lepL_charge = tree->GetLeaf("mc_LepL_charge");
    TLeaf *MC_lepL_pt =(TLeaf*)leaves->UncheckedAt(50);
    MC_lepL_pt = tree->GetLeaf("mc_LepL_pt");
    TLeaf *MC_lepL_eta =(TLeaf*)leaves->UncheckedAt(51);
    MC_lepL_eta = tree->GetLeaf("mc_LepL_eta");
    TLeaf *MC_lepL_phi =(TLeaf*)leaves->UncheckedAt(52);
    MC_lepL_phi = tree->GetLeaf("mc_LepL_phi");
    TLeaf *MC_lepB_pt =(TLeaf*)leaves->UncheckedAt(40);
    MC_lepB_pt = tree->GetLeaf("mc_LepB_pt");
    TLeaf *MC_lepB_eta =(TLeaf*)leaves->UncheckedAt(41);
    MC_lepB_eta = tree->GetLeaf("mc_LepB_eta");
    TLeaf *MC_lepB_phi =(TLeaf*)leaves->UncheckedAt(42);
    MC_lepB_phi = tree->GetLeaf("mc_LepB_phi");
    TLeaf *MC_lepN_pt =(TLeaf*)leaves->UncheckedAt(53);
    MC_lepN_pt = tree->GetLeaf("mc_LepN_pt");
    TLeaf *MC_lepN_eta =(TLeaf*)leaves->UncheckedAt(54);
    MC_lepN_eta = tree->GetLeaf("mc_LepN_eta");
    TLeaf *MC_lepN_phi =(TLeaf*)leaves->UncheckedAt(55);
    MC_lepN_phi = tree->GetLeaf("mc_LepN_phi");

    TLeaf *event_weight =(TLeaf*)leaves->UncheckedAt(58);
    event_weight = tree->GetLeaf("event_weight");

    TLeaf *extraJet_pt =(TLeaf*)leaves->UncheckedAt(59);
    extraJet_pt = tree->GetLeaf("ExtraJet_pt");
    TLeaf *extraJet_eta =(TLeaf*)leaves->UncheckedAt(60);
    extraJet_eta = tree->GetLeaf("ExtraJet_eta");
    TLeaf *extraJet_phi =(TLeaf*)leaves->UncheckedAt(61);
    extraJet_phi = tree->GetLeaf("ExtraJet_phi");

    TLeaf *hadP_pt_orig = (TLeaf*)leaves->UncheckedAt(62);
    hadP_pt_orig = tree->GetLeaf("HadP_pt_orig");
    TLeaf *hadP_eta_orig = (TLeaf*)leaves->UncheckedAt(63);
    hadP_eta_orig = tree->GetLeaf("HadP_eta_orig");
    TLeaf *hadP_phi_orig = (TLeaf*)leaves->UncheckedAt(64);
    hadP_phi_orig = tree->GetLeaf("HadP_phi_orig");
    TLeaf *hadP_m_orig = (TLeaf*)leaves->UncheckedAt(65);
    hadP_m_orig = tree->GetLeaf("HadP_m_orig");
    TLeaf *hadB_pt_orig = (TLeaf*)leaves->UncheckedAt(66);
    hadB_pt_orig = tree->GetLeaf("HadB_pt_orig");
    TLeaf *hadB_eta_orig = (TLeaf*)leaves->UncheckedAt(67);
    hadB_eta_orig = tree->GetLeaf("HadB_eta_orig");
    TLeaf *hadB_phi_orig = (TLeaf*)leaves->UncheckedAt(68);
    hadB_phi_orig = tree->GetLeaf("HadB_phi_orig");
    TLeaf *hadB_m_orig = (TLeaf*)leaves->UncheckedAt(69);
    hadB_m_orig = tree->GetLeaf("HadB_m_orig");
    TLeaf *hadQ_pt_orig =(TLeaf*)leaves->UncheckedAt(70);
    hadQ_pt_orig = tree->GetLeaf("HadQ_pt_orig");
    TLeaf *hadQ_eta_orig =(TLeaf*)leaves->UncheckedAt(71);
    hadQ_eta_orig = tree->GetLeaf("HadQ_eta_orig");
    TLeaf *hadQ_phi_orig =(TLeaf*)leaves->UncheckedAt(72);
    hadQ_phi_orig = tree->GetLeaf("HadQ_phi_orig");
    TLeaf *hadQ_m_orig =(TLeaf*)leaves->UncheckedAt(73);
    hadQ_m_orig = tree->GetLeaf("HadQ_m_orig");
    TLeaf *lepB_pt_orig =(TLeaf*)leaves->UncheckedAt(74);
    lepB_pt_orig = tree->GetLeaf("LepB_pt_orig");
    TLeaf *lepB_eta_orig =(TLeaf*)leaves->UncheckedAt(75);
    lepB_eta_orig = tree->GetLeaf("LepB_eta_orig");
    TLeaf *lepB_phi_orig =(TLeaf*)leaves->UncheckedAt(76);
    lepB_phi_orig = tree->GetLeaf("LepB_phi_orig");
    TLeaf *lepB_m_orig =(TLeaf*)leaves->UncheckedAt(77);
    lepB_m_orig = tree->GetLeaf("LepB_m_orig");
    TLeaf *lepL_pt_orig =(TLeaf*)leaves->UncheckedAt(78);
    lepL_pt_orig = tree->GetLeaf("LepL_pt_orig");
    TLeaf *lepL_eta_orig =(TLeaf*)leaves->UncheckedAt(79);
    lepL_eta_orig = tree->GetLeaf("LepL_eta_orig");
    TLeaf *lepL_phi_orig =(TLeaf*)leaves->UncheckedAt(80);
    lepL_phi_orig = tree->GetLeaf("LepL_phi_orig");
    TLeaf *lepL_m_orig =(TLeaf*)leaves->UncheckedAt(81);
    lepL_m_orig = tree->GetLeaf("LepL_m_orig");
    TLeaf *lepN_pt_orig =(TLeaf*)leaves->UncheckedAt(82);
    lepN_pt_orig = tree->GetLeaf("LepN_pt_orig");
    TLeaf *lepN_eta_orig =(TLeaf*)leaves->UncheckedAt(83);
    lepN_eta_orig = tree->GetLeaf("LepN_eta_orig");
    TLeaf *lepN_phi_orig =(TLeaf*)leaves->UncheckedAt(84);
    lepN_phi_orig = tree->GetLeaf("LepN_phi_orig");
    TLeaf *lepN_m_orig =(TLeaf*)leaves->UncheckedAt(85);
    lepN_m_orig = tree->GetLeaf("LepN_m_orig");

     
    TLorentzVector *h_B = new TLorentzVector();
    h_B->SetPtEtaPhiM(hadB_pt->GetValue(0),hadB_eta->GetValue(0), hadB_phi->GetValue(0), hadB_m->GetValue(0));
    TLorentzVector *l_B = new TLorentzVector();
    l_B->SetPtEtaPhiM(lepB_pt->GetValue(0),lepB_eta->GetValue(0), lepB_phi->GetValue(0), lepB_m->GetValue(0));
    TLorentzVector *Q = new TLorentzVector();
    Q->SetPtEtaPhiM(hadQ_pt->GetValue(0),hadQ_eta->GetValue(0), hadQ_phi->GetValue(0), hadQ_m->GetValue(0));
    TLorentzVector *QB = new TLorentzVector();
    QB->SetPtEtaPhiM(hadP_pt->GetValue(0),hadP_eta->GetValue(0), hadP_phi->GetValue(0), hadP_m->GetValue(0));
    TLorentzVector *lep = new TLorentzVector();
    lep->SetPtEtaPhiM(lepL_pt->GetValue(0),lepL_eta->GetValue(0), lepL_phi->GetValue(0), lepL_m->GetValue(0));
    TLorentzVector *n = new TLorentzVector();
    n->SetPtEtaPhiM(lepN_pt->GetValue(0),lepN_eta->GetValue(0), lepN_phi->GetValue(0), lepN_m->GetValue(0));
    
    TLorentzVector *h_B_Orig = new TLorentzVector();
    h_B_Orig->SetPtEtaPhiM(hadB_pt_orig->GetValue(0),hadB_eta_orig->GetValue(0), hadB_phi_orig->GetValue(0), hadB_m_orig->GetValue(0));
    TLorentzVector *l_B_Orig = new TLorentzVector();
    l_B_Orig->SetPtEtaPhiM(lepB_pt_orig->GetValue(0),lepB_eta_orig->GetValue(0), lepB_phi_orig->GetValue(0), lepB_m_orig->GetValue(0));
    TLorentzVector *Q_Orig = new TLorentzVector();
    Q_Orig->SetPtEtaPhiM(hadQ_pt_orig->GetValue(0),hadQ_eta_orig->GetValue(0), hadQ_phi_orig->GetValue(0), hadQ_m_orig->GetValue(0));
    TLorentzVector *QB_Orig = new TLorentzVector();
    QB_Orig->SetPtEtaPhiM(hadP_pt_orig->GetValue(0),hadP_eta_orig->GetValue(0), hadP_phi_orig->GetValue(0), hadP_m_orig->GetValue(0));
    TLorentzVector *lep_Orig = new TLorentzVector();
    lep_Orig->SetPtEtaPhiM(lepL_pt_orig->GetValue(0),lepL_eta_orig->GetValue(0), lepL_phi_orig->GetValue(0), lepL_m_orig->GetValue(0));
    TLorentzVector *n_Orig = new TLorentzVector();
    n_Orig->SetPtEtaPhiM(lepN_pt_orig->GetValue(0),lepN_eta_orig->GetValue(0), lepN_phi_orig->GetValue(0), lepN_m_orig->GetValue(0));
    
    
    TLorentzVector Whad = *Q + *QB;
    TLorentzVector Wlep = *lep + *n;
    TLorentzVector top_lep;
    TLorentzVector top_had;
    TLorentzVector TTbar;
    
    TLorentzVector Whad_Orig = *Q_Orig + *QB_Orig;
    TLorentzVector Wlep_Orig = *lep_Orig + *n_Orig;
    TLorentzVector top_lep_Orig;
    TLorentzVector top_had_Orig;
    TLorentzVector TTbar_Orig;
    
    top_lep = Wlep + *l_B;
    top_had = Whad + *h_B;
    top_lep_Orig = Wlep_Orig + *l_B_Orig;
    top_had_Orig = Whad_Orig + *h_B_Orig;
    
    TTbar = top_lep + top_had;
    TTbar_Orig = top_lep_Orig + top_had_Orig;
    
    TLorentzVector Lep_boosted = ROOT::Math::VectorUtil::boost(*lep, - top_lep.BoostVector());
    TLorentzVector B_had_boosted = ROOT::Math::VectorUtil::boost(*h_B, - top_had.BoostVector());
    TLorentzVector Lep_orig_boosted = ROOT::Math::VectorUtil::boost(*lep_Orig, - top_lep_Orig.BoostVector());
    TLorentzVector B_had_orig_boosted = ROOT::Math::VectorUtil::boost(*h_B_Orig, - top_had_Orig.BoostVector());
    double coslb(Lep_boosted.Vect().Unit().Dot(B_had_boosted.Vect().Unit()));
    double coslb_Orig(Lep_orig_boosted.Vect().Unit().Dot(B_had_orig_boosted.Vect().Unit()));

    (*TTbarPt)[dataset_index].Fill(TTbar.Pt(),event_weight->GetValue(0));
    (*TopMass)[dataset_index].Fill(mt->GetValue(0), event_weight->GetValue(0));
    (*Chi)[dataset_index].Fill(chi2->GetValue(0), event_weight->GetValue(0));
    (*Prob)[dataset_index].Fill(prob->GetValue(0), event_weight->GetValue(0));
    (*TTbarPt_Orig)[dataset_index].Fill(TTbar_Orig.Pt(), event_weight->GetValue(0));
    (*TopMass_Orig)[dataset_index].Fill((top_lep_Orig.M() + top_had_Orig.M())/2, event_weight->GetValue(0));
    (*CosTheta)[dataset_index].Fill(coslb, event_weight->GetValue(0));
    (*CosTheta_Orig)[dataset_index].Fill(coslb_Orig, event_weight->GetValue(0));

    n_weighted_events += event_weight->GetValue(0);
    }
  }
 
  std::cout<<"number of weighted events = "<<n_weighted_events<<std::endl;
}

void comhaireamh::TreeProcessor::process(std::string lep, std::string cutset, std::string directory_name, float lumi){
  
  luminosity = lumi;

  std::vector<std::string> *Datasets = new std::vector<std::string>();
  scaling = new std::vector<double>();
  cross_sections = new std::vector<double>();
  preselection = new std::vector<double>();
  std::string sig_name;
  lepton = lep;
  if(lepton == "e"){
    sig_name = "electron";
  }
  else{sig_name = "muon";}

  Datasets->push_back("Data_"+sig_name);
  //  Datasets->push_back("WJets_TuneZ2star_"+lepton+"_background");
  Datasets->push_back("WJetsToLNu_TuneZ2Star_"+lepton+"_background");
  Datasets->push_back("DYJetsToLL_"+lepton+"_background");
  Datasets->push_back("TbartWChan_"+lepton+"_background");
  Datasets->push_back("TtWChan_"+lepton+"_background");
  Datasets->push_back("TbarsChan_"+lepton+"_background");
  Datasets->push_back("TsChan_"+lepton+"_background");
  Datasets->push_back("TbartChan_"+lepton+"_background");
  Datasets->push_back("TtChan_"+lepton+"_background");
  Datasets->push_back("TTbar_"+lepton+"_background");
  Datasets->push_back("TTbar_"+sig_name);
  
  for(unsigned int i=0; i < Datasets->size(); i++){
    scaling->push_back(1.);
  }

  bookHistos(*Datasets);
  for(unsigned int i= 0; i < Datasets->size(); i++){
    get_trees_from_files((*Datasets)[i], cutset, directory_name, i);
    std::cout<<"Dataset = "<<(*Datasets)[i]<<std::endl;
    read(i);
  }

 
  float br_fr = 0.14788610200966518;//hardcoded to match table_generator
  //  std::cout<<"top mass electron integral = "<<(*TopMass)[1].Integral("width")<< " e_back integral = "<<(*TopMass)[2].Integral("width")<<std::endl;
  //br_fr = 1./(*scaling)[11]/(1./(*scaling)[11] + 1./(*scaling)[10]);
  std::cout<<"br ratio= "<<br_fr<<std::endl;

  cross_sections->push_back(1);
  //cross_sections->push_back(36257.2);
  cross_sections->push_back(36257.2);//37509);//For WJetsToLNU only
  cross_sections->push_back(3503.71);
  cross_sections->push_back(11.1);
  cross_sections->push_back(11.1);
  cross_sections->push_back(1.76);
  cross_sections->push_back(3.97);
  cross_sections->push_back(30.7);  
  cross_sections->push_back(56.4);
  cross_sections->push_back(245.8*(1. - br_fr));
  cross_sections->push_back(245.8*br_fr);

  preselection->push_back(1);
  //preselection->push_back(1);
  preselection->push_back(1);
  preselection->push_back(1);
  preselection->push_back(1);
  preselection->push_back(1);
  preselection->push_back(1);
  preselection->push_back(1);
  preselection->push_back(1);
  preselection->push_back(1);
  preselection->push_back(1);
  preselection->push_back(1);
  setScales();
  
  prepare_plots(TopMass);
  prepare_plots(TopMass_Orig);
  prepare_plots(Chi);
  prepare_plots(TTbarPt);
  prepare_plots(TTbarPt_Orig);
  prepare_plots(Prob);
  prepare_plots(CosTheta);
  prepare_plots(CosTheta_Orig);
  create_plots();
  
}

void comhaireamh::TreeProcessor::setScales(){

  for(unsigned int i = 0; i < scaling->size(); i++){
    std::cout<<"1/N = "<<(*scaling)[i]<<" cross section = "<<(*cross_sections)[i]<<" lumi = "<<luminosity<<std::endl;
    (*scaling)[i] *= (*cross_sections)[i]*luminosity;
    //    std::cout<<"scaling = "<<(*scaling)[i]<<std::endl;
  }
}

void comhaireamh::TreeProcessor::bookHistos(std::vector< std::string > dataset_names){
  
  TH1F *temp_mass;
  TH1F *temp_mass_orig;
  TH1F *temp_Chi;
  TH1F *temp_TTbarPt;
  TH1F *temp_TTbarPt_orig;
  TH1F *temp_Prob;
  TH1F *temp_CosTheta;
  TH1F *temp_CosTheta_orig;

  TopMass = new std::vector< TH1F >();
  TopMass_Orig = new std::vector< TH1F >();
  Chi = new std::vector< TH1F >();
  TTbarPt = new std::vector< TH1F >();
  TTbarPt_Orig = new std::vector< TH1F >();
  Prob = new std::vector< TH1F >();
  CosTheta = new std::vector < TH1F >();
  CosTheta_Orig = new std::vector < TH1F >();

  for(unsigned int i = 0; i < dataset_names.size(); i++){
    temp_mass = new TH1F(("TopMass_"+(dataset_names)[i]).c_str()," ; m_{top} (GeV/c^{2}); entries",30,100,400);
    temp_mass_orig = new TH1F(("TopMass_Orig_"+(dataset_names)[i]).c_str()," ; m_{top} (GeV/c^{2}); entries",50,100,450);
    temp_Chi = new TH1F(("Chi2_"+(dataset_names)[i]).c_str(),"; #chi^{2}; entries",25,0,80);
    temp_TTbarPt = new TH1F(("TTbarPt_"+(dataset_names)[i]).c_str(),"; p_{t}(t#bar{t}) (GeV); entries",30,0,150);
    temp_TTbarPt_orig = new TH1F(("TTbarPt_Orig_"+(dataset_names)[i]).c_str(),"; p_{t}(t#bar{t}) (GeV); entries",30,0,100);
    temp_Prob = new TH1F(("Prob_"+(dataset_names)[i]).c_str(),"; probability; entries",30,0,1);
    temp_CosTheta = new TH1F(("CosTheta_"+(dataset_names)[i]).c_str(),"; cos#theta_{lb}; entries",10,-1,1);
    temp_CosTheta_orig = new TH1F(("CosTheta_Orig_"+(dataset_names)[i]).c_str(),"; cos#theta_{lb}; entries",10,-1,1);
    TopMass->push_back(*temp_mass);
    TopMass_Orig->push_back(*temp_mass_orig);
    Chi->push_back(*temp_Chi);
    TTbarPt->push_back(*temp_TTbarPt);
    TTbarPt_Orig->push_back(*temp_TTbarPt_orig);
    Prob->push_back(*temp_Prob);
    CosTheta->push_back(*temp_CosTheta);
    CosTheta_Orig->push_back(*temp_CosTheta_orig);
  }
}

void comhaireamh::TreeProcessor::prepare_plots(std::vector<TH1F> *histos){

  (*histos)[10].SetFillColor(kRed-3);
  (*histos)[9].SetFillColor(kYellow-7);
  (*histos)[8].SetFillColor(kWhite+8);
  (*histos)[7].SetFillColor(kWhite+8);
  (*histos)[6].SetFillColor(kWhite+8);
  (*histos)[5].SetFillColor(kWhite+8);
  (*histos)[4].SetFillColor(kWhite+8);
  (*histos)[3].SetFillColor(kWhite+8);
  (*histos)[2].SetFillColor(kCyan-6);
  (*histos)[1].SetFillColor(kBlue-2);
  // (*histos)[1].SetFillColor(kBlue-2);
  (*histos)[0].SetFillColor(kBlack);

  (*histos)[10].SetMarkerColor(kRed-3);
  (*histos)[9].SetMarkerColor(kYellow-7);
  (*histos)[8].SetMarkerColor(kWhite+8);
  (*histos)[7].SetMarkerColor(kWhite+8);
  (*histos)[6].SetMarkerColor(kWhite+8);
  (*histos)[5].SetMarkerColor(kWhite+8);
  (*histos)[4].SetMarkerColor(kWhite+8);
  (*histos)[3].SetMarkerColor(kWhite+8);
  (*histos)[2].SetMarkerColor(kCyan-6);
  (*histos)[1].SetMarkerColor(kBlue-2);
  //(*histos)[1].SetMarkerColor(kBlue-2);
  (*histos)[0].SetMarkerColor(kBlack);

  for(unsigned int i = 0; i < histos->size(); i++){
    (*histos)[i].SetBinContent(1,(*histos)[i].GetBinContent(0) + (*histos)[i].GetBinContent(1));
    (*histos)[i].SetBinContent((*histos)[i].GetNbinsX(),(*histos)[i].GetBinContent((*histos)[i].GetNbinsX()) + (*histos)[i].GetBinContent((*histos)[i].GetNbinsX() + 1));
    (*histos)[i].SetMarkerStyle(20);
    (*histos)[i].SetMarkerSize(0.75);
    (*histos)[i].SetLineColor(kBlack);
     if(i != 0){
      (*histos)[i].Sumw2();
      (*histos)[i].Scale((*scaling)[i]);
    }
    if(i == 1){(*histos)[i].SetLineColor(kBlue-2);}
  }
}

void comhaireamh::TreeProcessor::create_plots()
{

  char temp[100];

  TLegend *leg = new TLegend(0.6773985,0.607473,0.9676871,0.848228,NULL,"brNDC");
  leg->SetFillColor(0);
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  //  leg->SetLineColor(0);
  //leg->SetLineWidth(0);
  if(lepton == "e"){
    leg->AddEntry(&(*TopMass)[10],"t #bar{t} (signal,e)","p");
    leg->AddEntry(&(*TopMass)[9],"t #bar{t} (other,e)","p");
  }
  else{
    leg->AddEntry(&(*TopMass)[10],"t #bar{t} (signal,mu)","p");
    leg->AddEntry(&(*TopMass)[9],"t #bar{t} (other,mu)","p");
  }
  leg->AddEntry(&(*TopMass)[8],"t, #bar{t}, t-channel ","p");
  //  leg->AddEntry(&(*TopMass)[7],"#bar{t}, t-channel","p");
  leg->AddEntry(&(*TopMass)[6],"t, #bar{t}, s-channel ","p");
  //  leg->AddEntry(&(*TopMass)[5],"#bar{t}, s-channel","p");
  leg->AddEntry(&(*TopMass)[4],"t, #bar{t}, tW-channel ","p");
  //  leg->AddEntry(&(*TopMass)[3],"#bar{t}, tW-channel","p");
  leg->AddEntry(&(*TopMass)[2],"DYJetsToLL","p");
  leg->AddEntry(&(*TopMass)[1],"W + Jets","p");//only fill one of the Wjets
  leg->AddEntry(&(*TopMass)[0],"Data","p");

  THStack *TopMassStack = new THStack("Mass","Top Mass of the selected HitFit combination; m_{top} (GeV/c^{2}); entries");
  THStack *TopMassStack_Orig = new THStack("Mass_Orig","Top Mass of the selected HitFit combination; m_{top} (GeV/c^{2}); entries");
  THStack *TTbarPtStack = new THStack("TTbarPt","Top Pair Pt of the selected HitFit combination; p_{t}(t#bar{t}) (GeV); entries");
  THStack *TTbarPtStack_Orig = new THStack("TTbarPt_Orig","Top Pair Pt of the selected HitFit combination; p_{t}(t#bar{t}) (GeV); entries");
  THStack *ChiStack = new THStack("ChiStack","#chi^{2} of the selected HitFit combination; #chi^{2}; entries");
  THStack *ProbStack = new THStack("Prob","Probability of the selected HitFit combination; Probability; entries");
  THStack *CosThetaStack = new THStack("CosTheta","Cos#theta of the selected HitFit combination; cos#theta_{lb}; entries");
  THStack *CosThetaStack_Orig = new THStack("CosTheta_Orig","Cos#theta of the selected HitFit combination; cos#theta_{lb}; entries");
  //histograms for the Kolmogorov-Smirnov Test
  TH1F *TopMassHist;
  TH1F *TopMassHist_Orig;
  TH1F *TTbarPtHist;
  TH1F *TTbarPtHist_Orig;
  TH1F *ChiHist;
  TH1F *ProbHist;
  TH1F *CosThetaHist;
  TH1F *CosThetaHist_Orig;

  for(unsigned int i = 1; i < TopMass->size(); i++)
    {
      std::cout<<"dataset = "<<i<<" integral post scaling = "<<(*TopMass)[i].Integral()<<std::endl;
      if(i == 1){
        TopMassHist = (TH1F*)(*TopMass)[i].Clone("MassHist");
        TopMassHist_Orig = (TH1F*)(*TopMass_Orig)[i].Clone("Mass_OrigHist");
        TTbarPtHist = (TH1F*)(*TTbarPt)[i].Clone("TTbarPtHist");
        TTbarPtHist_Orig = (TH1F*)(*TTbarPt_Orig)[i].Clone("TTbarPtHist_Orig");
        ChiHist = (TH1F*)(*Chi)[i].Clone("ChiHist");
        ProbHist = (TH1F*)(*Prob)[i].Clone("ProbHist");
        CosThetaHist = (TH1F*)(*CosTheta)[i].Clone("CosThetaHist");
        CosThetaHist_Orig = (TH1F*)(*CosTheta_Orig)[i].Clone("CosThetaHist_Orig");
      }
      else{
        TopMassHist->Add(&(*TopMass)[i],1);
        TopMassHist_Orig->Add(&(*TopMass_Orig)[i],1);
        TTbarPtHist->Add(&(*TTbarPt)[i],1);
        TTbarPtHist_Orig->Add(&(*TTbarPt_Orig)[i],1);
        ChiHist->Add(&(*Chi)[i],1);
        ProbHist->Add(&(*Prob)[i],1);
        CosThetaHist->Add(&(*CosTheta)[i],1);
        CosThetaHist_Orig->Add(&(*CosTheta_Orig)[i],1);
      }

    }

  double Integ = TopMassHist->Integral(); //+ TopMassHist->GetBinContent(0) + TopMassHist->GetBinContent(TopMassHist->GetNbinsX() + 1);
  double int_Data = (*TopMass)[0].Integral();// + (*TopMass)[0].GetBinContent(0) + (*TopMass)[0].GetBinContent((*TopMass)[0].GetNbinsX() + 1);
  std::cout<<"MC = "<<Integ<<" Data = "<<int_Data<<std::endl;
  double cor_factor = int_Data/Integ;
  std::cout<<"extra correction factor = "<<cor_factor<<std::endl;


  //rescale
  for(unsigned int i = 1; i < TopMass->size(); i++)
    {
      //      (*TopMass)[i].SetBinContent(1,(*TopMass)[i].GetBinContent(0) + (*TopMass)[i].GetBinContent(1));
      //(*TopMass)[i].SetBinContent((*TopMass)[i].GetNbinsX(),(*TopMass)[i].GetBinContent((*TopMass)[i].GetNbinsX()) + (*TopMass)[i].GetBinContent(1));
      (*TopMass)[i].Scale(cor_factor);
      (*TopMass_Orig)[i].Scale(cor_factor);
      (*TTbarPt)[i].Scale(cor_factor);
      (*TTbarPt_Orig)[i].Scale(cor_factor);
      (*Chi)[i].Scale(cor_factor);
      (*Prob)[i].Scale(cor_factor);
      (*CosTheta)[i].Scale(cor_factor);
      (*CosTheta_Orig)[i].Scale(cor_factor);

      TopMassStack->Add(&(*TopMass)[i]);
      TopMassStack_Orig->Add(&(*TopMass_Orig)[i]);
      TTbarPtStack->Add(&(*TTbarPt)[i]);
      TTbarPtStack_Orig->Add(&(*TTbarPt_Orig)[i]);
      ChiStack->Add(&(*Chi)[i]);
      ProbStack->Add(&(*Prob)[i]);
      CosThetaStack->Add(&(*CosTheta)[i]);
      CosThetaStack_Orig->Add(&(*CosTheta_Orig)[i]);
   }

  double ks = -1;
  TPaveText *KS = new TPaveText(0.7389749,0.9092193,0.938365,0.9605804,"brNDC");
  KS->SetTextColor(kBlack);
  KS->SetFillColor(kWhite);
  KS->SetBorderSize(0);
  KS->SetTextAlign(12);
  KS->SetTextSize(0.03);

  gStyle->SetOptStat(0000);

  TCanvas *c_Mass = new TCanvas();
  c_Mass->cd();
  TPad *c1_Mass = createFirstTPad("c1_Mass");
  TopMassHist->Sumw2();
  TopMassHist->Scale(cor_factor);
  TH1F SF_Mass = drawSFPlot(TopMassHist,&(*TopMass)[0]);
  SF_Mass.Draw("p");
  TLine *l_Mass = new TLine(SF_Mass.GetXaxis()->GetXmin(),1,SF_Mass.GetXaxis()->GetXmax(),1);
  l_Mass->SetLineColor(kRed);
  l_Mass->Draw("same");
  c1_Mass->Modified();
  c1_Mass->Update();
  c_Mass->cd();
  TPad *c2_Mass = new TPad("c2_Mass", "newpad",0.01,0.33,0.99,0.99);
  c2_Mass->SetFillColor(kWhite);
  c2_Mass->Draw();
  c2_Mass->cd();
  ks = TopMassHist->KolmogorovTest(&(*TopMass)[0]);
  TopMassStack->SetMaximum(1.1*(*TopMass)[0].GetMaximum());
  TopMassStack->Draw("HIST");
  (*TopMass)[0].Draw("epsames");
  leg->Draw();
  sprintf(temp, "KS = %.5f", ks);
  KS->AddText(temp);
  KS->Draw();
  c2_Mass->Update();
  c2_Mass->Modified();
  c_Mass->SaveAs("TopMass.C");
  c_Mass->SaveAs("TopMass.png");
  c_Mass->SaveAs("TopMass.pdf");
  delete c_Mass;

  KS->Clear();
  ks = -1;

  TCanvas *c_Mass_Orig = new TCanvas();
  c_Mass_Orig->cd();
  TPad *c1_Mass_Orig = createFirstTPad("c1_Mass_Orig");
  TopMassHist_Orig->Scale(cor_factor);
  TH1F SF_Mass_Orig = drawSFPlot(TopMassHist_Orig,&(*TopMass_Orig)[0]);
  SF_Mass_Orig.Draw("p");
  TLine *l_Mass_Orig = new TLine(SF_Mass_Orig.GetXaxis()->GetXmin(),1,SF_Mass_Orig.GetXaxis()->GetXmax(),1);
  l_Mass_Orig->SetLineColor(kRed);
  l_Mass_Orig->Draw("same");
  c1_Mass_Orig->Modified();
  c1_Mass_Orig->Update();
  c_Mass_Orig->cd();
  TPad *c2_Mass_Orig = new TPad("c2_Mass_Orig", "newpad",0.01,0.33,0.99,0.99);
  c2_Mass_Orig->SetFillColor(kWhite);
  c2_Mass_Orig->Draw();
  c2_Mass_Orig->cd();
  ks = TopMassHist_Orig->KolmogorovTest(&(*TopMass_Orig)[0]);
  TopMassStack_Orig->SetMaximum(1.1*(*TopMass_Orig)[0].GetMaximum());
  TopMassStack_Orig->Draw("HIST");
  (*TopMass_Orig)[0].Draw("epsames");
  leg->Draw();
  sprintf(temp, "KS = %.5f", ks);
  KS->AddText(temp);
  KS->Draw();
  c2_Mass_Orig->Update();
  c2_Mass_Orig->Modified();
  c_Mass_Orig->SaveAs("TopMass_Orig.C");
  c_Mass_Orig->SaveAs("TopMass_Orig.png");
  c_Mass_Orig->SaveAs("TopMass_Orig.pdf");
  delete c_Mass_Orig;

  KS->Clear();
  ks = -1;

  TCanvas *c_TTbarPt = new TCanvas();
  c_TTbarPt->cd();
  TPad *c1_TTbarPt = createFirstTPad("c1_TTbarPt");
  TTbarPtHist->Scale(cor_factor);
  TH1F SF_TTbarPt = drawSFPlot(TTbarPtHist,&(*TTbarPt)[0]);
  SF_TTbarPt.Draw("p");
  TLine *l_TTbarPt = new TLine(SF_TTbarPt.GetXaxis()->GetXmin(),1,SF_TTbarPt.GetXaxis()->GetXmax(),1);
  l_TTbarPt->SetLineColor(kRed);
  l_TTbarPt->Draw("same");
  c1_TTbarPt->Modified();
  c1_TTbarPt->Update();
  c_TTbarPt->cd();
  TPad *c2_TTbarPt = new TPad("c2_TTbarPt", "newpad",0.01,0.33,0.99,0.99);
  c2_TTbarPt->SetFillColor(kWhite);
  c2_TTbarPt->Draw();
  c2_TTbarPt->cd();
  ks = TTbarPtHist->KolmogorovTest(&(*TTbarPt)[0]);
  TTbarPtStack->SetMaximum(1.2*(*TTbarPt)[0].GetMaximum());
  TTbarPtStack->Draw("HIST");
  (*TTbarPt)[0].Draw("epsames");
  leg->Draw();
  sprintf(temp, "KS = %.5f", ks);
  KS->AddText(temp);
  KS->Draw();
  c2_TTbarPt->Update();
  c2_TTbarPt->Modified();
  c_TTbarPt->SaveAs("TTbarPt.C");
  c_TTbarPt->SaveAs("TTbarPt.png");
  c_TTbarPt->SaveAs("TTbarPt.pdf");
  delete c_TTbarPt;

  KS->Clear();
  ks = -1;

  TCanvas *c_TTbarPt_Orig = new TCanvas();
  c_TTbarPt_Orig->cd();
  TPad *c1_TTbarPt_Orig = createFirstTPad("c1_TTbarPt_Orig");
  TTbarPtHist_Orig->Scale(cor_factor);
  TH1F SF_TTbarPt_Orig = drawSFPlot(TTbarPtHist_Orig,&(*TTbarPt_Orig)[0]);
  SF_TTbarPt_Orig.Draw("p");
  TLine *l_TTbarPt_Orig = new TLine(SF_TTbarPt_Orig.GetXaxis()->GetXmin(),1,SF_TTbarPt_Orig.GetXaxis()->GetXmax(),1);
  l_TTbarPt_Orig->SetLineColor(kRed);
  l_TTbarPt_Orig->Draw("same");
  c1_TTbarPt_Orig->Modified();
  c1_TTbarPt_Orig->Update();
  c_TTbarPt_Orig->cd();
  TPad *c2_TTbarPt_Orig = new TPad("c2_TTbarPt_Orig", "newpad",0.01,0.33,0.99,0.99);
  c2_TTbarPt_Orig->SetFillColor(kWhite);
  c2_TTbarPt_Orig->Draw();
  c2_TTbarPt_Orig->cd();
  ks = TTbarPtHist_Orig->KolmogorovTest(&(*TTbarPt_Orig)[0]);
  TTbarPtStack_Orig->SetMaximum(1.2*(*TTbarPt_Orig)[0].GetMaximum());
  TTbarPtStack_Orig->Draw("HIST");
  (*TTbarPt_Orig)[0].Draw("epsames");
  leg->Draw();
  sprintf(temp, "KS = %.5f", ks);
  KS->AddText(temp);
  KS->Draw();
  c2_TTbarPt_Orig->Update();
  c2_TTbarPt_Orig->Modified();
  c_TTbarPt_Orig->SaveAs("TTbarPt_Orig.C");
  c_TTbarPt_Orig->SaveAs("TTbarPt_Orig.png");
  c_TTbarPt_Orig->SaveAs("TTbarPt_Orig.pdf");
  delete c_TTbarPt_Orig;

  KS->Clear();
  ks = -1;

  TCanvas *c_Chi = new TCanvas();
  c_Chi->cd();
  TPad *c1_Chi = createFirstTPad("c1_Chi");
  ChiHist->Scale(cor_factor);
  TH1F SF_Chi = drawSFPlot(ChiHist,&(*Chi)[0]);
  SF_Chi.Draw("p");
  TLine *l_Chi = new TLine(SF_Chi.GetXaxis()->GetXmin(),1,SF_Chi.GetXaxis()->GetXmax(),1);
  l_Chi->SetLineColor(kRed);
  l_Chi->Draw("same");
  c1_Chi->Modified();
  c1_Chi->Update();
  c_Chi->cd();
  TPad *c2_Chi = new TPad("c2_Chi", "newpad",0.01,0.33,0.99,0.99);
  c2_Chi->SetFillColor(kWhite);
  c2_Chi->Draw();
  c2_Chi->cd();
  ks = ChiHist->KolmogorovTest(&(*Chi)[0]);
  ChiStack->SetMaximum(1.1*(*Chi)[0].GetMaximum());
  ChiStack->Draw("HIST");
  (*Chi)[0].Draw("epsames");
  leg->Draw();
  sprintf(temp, "KS = %.5f", ks);
  KS->AddText(temp);
  KS->Draw();
  c2_Chi->Update();
  c2_Chi->Modified();
  c_Chi->SaveAs("Chi.C");
  c_Chi->SaveAs("Chi.png");
  c_Chi->SaveAs("Chi.pdf");
  delete c_Chi;

  KS->Clear();
  ks = -1;

  TCanvas *c_Prob = new TCanvas();
  c_Prob->cd();
  TPad *c1_Prob = createFirstTPad("c1_Prob");
  ProbHist->Scale(cor_factor);
  TH1F SF_Prob = drawSFPlot(ProbHist,&(*Prob)[0]);
  SF_Prob.Draw("p");
  TLine *l_Prob = new TLine(SF_Prob.GetXaxis()->GetXmin(),1,SF_Prob.GetXaxis()->GetXmax(),1);
  l_Prob->SetLineColor(kRed);
  l_Prob->Draw("same");
  c1_Prob->Modified();
  c1_Prob->Update();
  c_Prob->cd();
  TPad *c2_Prob = new TPad("c2_Prob", "newpad",0.01,0.33,0.99,0.99);
  c2_Prob->SetFillColor(kWhite);
  c2_Prob->Draw();
  c2_Prob->cd();
  ks = ProbHist->KolmogorovTest(&(*Prob)[0]);
  ProbStack->Draw("HIST");
  (*Prob)[0].Draw("epsames");
  leg->Draw();
  sprintf(temp, "KS = %.5f", ks);
  KS->AddText(temp);
  KS->Draw();
  c2_Prob->Update();
  c2_Prob->Modified();
  c_Prob->SaveAs("Prob.C");
  c_Prob->SaveAs("Prob.png");
  c_Prob->SaveAs("Prob.pdf");
  delete c_Prob;

  KS->Clear();
  ks = -1;

  TCanvas *c_CosTheta = new TCanvas();
  c_CosTheta->cd();
  TPad *c1_CosTheta = createFirstTPad("c1_CosTheta");
  CosThetaHist->Scale(cor_factor);
  TH1F SF_CosTheta = drawSFPlot(CosThetaHist,&(*CosTheta)[0]);
  SF_CosTheta.Draw("p");
  TLine *l_CosTheta = new TLine(SF_CosTheta.GetXaxis()->GetXmin(),1,SF_CosTheta.GetXaxis()->GetXmax(),1);
  l_CosTheta->SetLineColor(kRed);
  l_CosTheta->Draw("same");
  c1_CosTheta->Modified();
  c1_CosTheta->Update();
  c_CosTheta->cd();
  TPad *c2_CosTheta = new TPad("c2_CosTheta", "newpad",0.01,0.33,0.99,0.99);
  c2_CosTheta->Range(-1.25,-6.798751,1.25,61.18875);
  c2_CosTheta->SetFillColor(kWhite);
  c2_CosTheta->Draw();
  c2_CosTheta->cd();
  ks = CosThetaHist->KolmogorovTest(&(*CosTheta)[0]);
  CosThetaStack->SetMaximum(1.4*(*CosTheta)[0].GetMaximum());
  CosThetaStack->Draw("HIST");
  (*CosTheta)[0].Draw("epsames");
  leg->Draw();
  sprintf(temp, "KS = %.5f", ks);
  KS->AddText(temp);
  KS->Draw();
  c2_CosTheta->Update();
  c2_CosTheta->Modified();
  c_CosTheta->SaveAs("CosTheta.C");
  c_CosTheta->SaveAs("CosTheta.png");
  c_CosTheta->SaveAs("CosTheta.pdf");
  delete c_CosTheta;

  KS->Clear();
  ks = -1;

  TCanvas *c_CosTheta_Orig = new TCanvas();
  c_CosTheta_Orig->cd();
  TPad *c1_CosTheta_Orig = createFirstTPad("c1_CosTheta_Orig");
  CosThetaHist_Orig->Scale(cor_factor);
  TH1F SF_CosTheta_Orig = drawSFPlot(CosThetaHist_Orig,&(*CosTheta_Orig)[0]);
  SF_CosTheta_Orig.Draw("p");
  TLine *l_CosTheta_Orig = new TLine(SF_CosTheta_Orig.GetXaxis()->GetXmin(),1,SF_CosTheta_Orig.GetXaxis()->GetXmax(),1);
  l_CosTheta_Orig->SetLineColor(kRed);
  l_CosTheta_Orig->Draw("same");
  c1_CosTheta_Orig->Modified();
  c1_CosTheta_Orig->Update();
  c_CosTheta_Orig->cd();
  TPad *c2_CosTheta_Orig = new TPad("c2_CosTheta_Orig", "newpad",0.01,0.33,0.99,0.99);
  c2_CosTheta_Orig->SetFillColor(kWhite);
  c2_CosTheta_Orig->Draw();
  c2_CosTheta_Orig->cd();
  ks = CosThetaHist_Orig->KolmogorovTest(&(*CosTheta_Orig)[0]);
  CosThetaStack_Orig->SetMaximum(1.4*(*CosTheta_Orig)[0].GetMaximum());
  CosThetaStack_Orig->Draw("HIST");
  (*CosTheta_Orig)[0].Draw("epsames");
  leg->Draw();
  sprintf(temp, "KS = %.5f", ks);
  KS->AddText(temp);
  KS->Draw();
  c2_CosTheta_Orig->Update();
  c2_CosTheta_Orig->Modified();
  c_CosTheta_Orig->SaveAs("CosTheta_Orig.C");
  c_CosTheta_Orig->SaveAs("CosTheta_Orig.png");
  c_CosTheta_Orig->SaveAs("CosTheta_Orig.pdf");
  delete c_CosTheta_Orig;

  KS->Clear();
  ks = -1;
}

void comhaireamh::TreeProcessor::get_trees_from_files(std::string name, std::string cutset_name, std::string dirname, int index)
{
  std::string hist_name;
  std::string rest;
  if(lepton == "e"){
    hist_name = name.substr(0,name.find("_e"));
    rest = name.substr(name.find("_e")+1,name.size());
  }
  else{
    hist_name = name.substr(0,name.find("_m"));
    rest = name.substr(name.find("_e")+1,name.size());
  }

  std::string Plot_file = dirname+"/"+hist_name+"_plots.root";
  TFile *plot_file = new TFile(Plot_file.c_str(),"OPEN");
  TH1F *counter_histo = (TH1F*) (plot_file->GetDirectory("eventselection")->Get(("event_counter_"+ hist_name+"|"+rest+"|"+cutset_name).c_str()))->Clone("counter_histo");
  (*scaling)[index] = 1./counter_histo->GetBinContent(1);
  plot_file->Close();

  std::string filename_var = dirname+"/HitFit_"+name+"_"+cutset_name+".root";
  std::string tree_name = "HitFitResults";
  std::string dir_name = "HitFit";
  
  TFile *infile = new TFile(filename_var.c_str(), "OPEN");
  set_tree((TTree*) infile->GetDirectory(dir_name.c_str())->Get(tree_name.c_str()));
  
}

TH1F comhaireamh::TreeProcessor::drawSFPlot(TH1F *MC, TH1F *Data) {

  TH1F *SF = new TH1F(*Data);
  SF->SetName("SF_DataOMC");
  SF->Sumw2();
  //  std::cout<<"WIDTH Data integral = "<<SF->Integral("width")<<" MC integral = "<<MC->Integral("width")<<std::endl;
  //std::cout<<"Data integral = "<<SF->Integral()<<" MC integral = "<<MC->Integral()<<std::endl;
  SF->Scale(1./SF->Integral());
  MC->Sumw2();
  MC->Scale(1./MC->Integral());
  SF->Divide(MC);
  SF->SetYTitle("entries");
  //  SF->SetTitle("; m_{top} (GeV/c^2); entries");
  SF->SetMarkerStyle(20);
  SF->SetMarkerSize(0.8);
  SF->SetMarkerColor(kBlack);
  SF->SetLineColor(kBlack);
  SF->GetXaxis()->SetLabelSize(0.08);
  SF->GetYaxis()->SetLabelSize(0.08);
  SF->GetXaxis()->SetTitleSize(0.08);
  SF->GetYaxis()->SetTitleSize(0.08);
  SF->GetYaxis()->SetTitleOffset(0.5);
  if(SF->GetMaximum() > 2){
  SF->GetYaxis()->SetRangeUser(0.,1.2*SF->GetMaximum());
  }
  else{
  SF->GetYaxis()->SetRangeUser(0.,2.);
  }
  return *SF;
}

TPad* comhaireamh::TreeProcessor::createFirstTPad(const char* name){
  //  TPad *c1 = new TPad(name, "newpad",0.005747126,0.03389831,0.987069,0.345339);
  TPad *c1 = new TPad(name, "newpad",0.01,0.01,0.99,0.32);
  c1->Draw();
  c1->cd();
  //  c1->Range(37.3984,-2.971334,523.2724,13.20593);
  //c1->Range(-1.25,-6.798751,1.25,61.18875);
  //  c1->SetLeftMargin(0.1288433);
  //c1->SetRightMargin(0.1508053);
  c1->SetTopMargin(0.1496599);
  c1->SetBottomMargin(0.1836735);
  c1->SetFillColor(kWhite);
  return c1;
}
