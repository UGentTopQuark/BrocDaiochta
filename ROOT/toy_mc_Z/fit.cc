#include <sstream>
#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "tdrstyle.h"
#include <RooGlobalFunc.h>
#include <RooRealVar.h>
#include <RooChi2Var.h>
#include <RooGaussian.h>
#include <RooPlot.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooExponential.h>
#include <RooVoigtian.h>
#include <RooAddPdf.h>
#include <RooFitResult.h>
#include <RooArgusBG.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooLandau.h>
#include <RooMinuit.h>

int main(int argc, char **argv)
{
	using namespace RooFit;
	std::string directory_name = "eventselection";
	std::string full_name = "dilepton_mass_Data|muon|03_cutset";
	setTDRStyle();
	TFile *datafile = new TFile("Data_muPD_plots.root", "OPEN");
	TH1F *datahisto = (TH1F *) datafile->GetDirectory(directory_name.c_str())->Get(full_name.c_str());	

	TFile *outfile = new TFile("output.root", "RECREATE");
	outfile->cd();
        TCanvas *c2 = new TCanvas("fit_results", "canvas", 640, 480);
        c2->cd();
	TH1F *fit_results = new TH1F("fit_results", "Result for pseudo-experiment and fit to pseudoexperiment", 200, 80., 100.);

        TCanvas *c1 = new TCanvas("fit", "canvas", 640, 480);
        c1->cd();

	RooRealVar x("x","dilepton mass [GeV]", 10, 60., 120.);
	RooRealVar m("m","m",91.1876, 60., 120.); 
	RooRealVar g("g","g",2.5, 0., 10.); 
	RooRealVar s("s","s",1, 0., 10.); 

	RooVoigtian voigtian("voigtian", "voigtian", x, m, g, s);

	RooDataHist* data = new RooDataHist("data","data",x,datahisto);

	RooPlot* xframe = x.frame(Name("Z"),Title("Z->mumu"));
	xframe->UseCurrentStyle();
	xframe->SetYTitle("Events");
	data->plotOn(xframe, Binning(20));
//	voigtian.plotOn(xframe,LineColor(kRed),LineStyle(kDashed));
	voigtian.fitTo(*data);
	voigtian.plotOn(xframe,LineColor(kRed),LineStyle(kDashed));
	xframe->Draw();

/*
	int ntoys=10000;

	for(int i = 0; i < ntoys; ++i){
		if(i % 50 == 0) std::cout << "[" << i << "/" << ntoys << "]" << std::endl;
		RooRealVar x("x","x", 10, 60., 120.);
		RooRealVar m("m","m",90.7092, 60., 120.); 
		RooRealVar g("g","g",1.99784, 0., 10.); 
		RooRealVar s("s","s",2.34701, 0., 10.); 

		RooVoigtian voigtian("voigtian", "voigtian", x, m, g, s);

		RooDataSet* pseudodata = voigtian.generate(x, 90);
		//RooDataHist* data = new RooDataHist("data","data",x,histo);

	//	RooPlot* xframe = x.frame(Name("background tail"),Title("background tail"));
	//	xframe->UseCurrentStyle();
	//	xframe->SetYTitle("Events");
	//	pseudodata->plotOn(xframe, Binning(20));
	//	voigtian.plotOn(xframe,LineColor(kRed),LineStyle(kDashed));
		voigtian.fitTo(*pseudodata, PrintLevel(-1));
	//	voigtian.plotOn(xframe,LineColor(kBlue),LineStyle(kDashed));
	//	xframe->Draw();
		fit_results->Fill(m.getVal());
	}
*/

	//std::cout << "mean: " << m.getVal() << " +/- " << m.getError() << std::endl;

	c1->SaveAs("fit.eps");
	c2->cd();
	fit_results->Draw();
	c2->SaveAs("results.eps");
	fit_results->Write();
	outfile->Close();
}
