#include "HistoFitter.h"

HistoFitter::HistoFitter()
{
	histo = NULL;
	tools = new Tools();
}

HistoFitter::~HistoFitter()
{
	delete tools;
	tools = NULL;
}

void HistoFitter::set_histo(TH1F *histo)
{
	this->histo = histo;
}

std::pair<double,double> HistoFitter::fit_iterative_gaussian()
{
        double lower_border=0;
        double upper_border=0;
        double histo_mean = 0;

        //double border_width = histo->GetRMS()*3/2;
        double border_width = histo->GetRMS();

        const int fitruns = 10;

        double x_rand = rand()%100000;
        std::string gaus_ident = "gaus"+tools->stringify(x_rand);

        std::cout << "ident: " << gaus_ident << std::endl;

        TF1 *gaus = new TF1(gaus_ident.c_str(),"gaus",0.,1000.);

        gaus->SetParameter(1,1.);
        gaus->SetParameter(2,border_width);

        for(int j=0; j<fitruns; ++j){
                if(j==0){
                        //histo_mean = histo->GetMean();
                        // FIXME
                        histo_mean = histo->GetMaximumBin() * histo->GetBinWidth(1) - histo->FindBin(0) * histo->GetBinWidth(1);
                        
                        gaus->SetParameter(1,histo_mean);
                        if(verbose){
                                std::cout << "max bin: " << histo->GetMaximumBin() << std::endl;
                                std::cout << "bin_width: " << histo->GetBinWidth(1) << std::endl;
                                std::cout << "0-bin: " << histo->FindBin(0.) << std::endl;
                                std::cout << "x-bins: " << histo->GetNbinsX() << std::endl;
                                std::cout << "Mean: " << gaus->GetParameter(1) << std::endl;
                                std::cout << "RMS/2.: " << gaus->GetParameter(2) << std::endl;
                                std::cout << "-----" << std::endl;
                        }

                        if(verbose)
                                std::cout << "fist iteration." << std::endl;
                }
                else{
                        histo_mean = gaus->GetParameter(1);
                        if(verbose)
                                std::cout << "second iteration." << std::endl;
                }

		border_width = gaus->GetParameter(2)*1.5;

                lower_border = histo_mean - border_width;
                upper_border = histo_mean + border_width;

                if(verbose){
                       std::cout << "histo mean: " << histo_mean << std::endl;
                       std::cout << "lower border: " << lower_border << std::endl;
                       std::cout << "upper border: " << upper_border << std::endl;
                }
                histo->Fit(gaus_ident.c_str(),"","",lower_border,upper_border);
        }

        if(verbose){
                std::cout << "histo mean: " << gaus->GetParameter(1) << std::endl;
                std::cout << "histo sigma: " << gaus->GetParameter(2) << std::endl;
                std::cout << "-----" << std::endl;
        }

	histo->Write();

        // par 1 = mean, par 2 = sigma
        return std::make_pair(gaus->GetParameter(1),gaus->GetParameter(2));
}
