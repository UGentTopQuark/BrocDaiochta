#include "TGraphErrors.h"
#include "Canvas_Holder.h"

int main(int argc, char **argv)
{
std::map<std::string,std::map<int,double> > y_axis_map;
std::map<std::string,std::map<int,double> > y_ERR_map;

double y_axis[5];
double y_ERR[5];
double x_axis[5];
double x_ERR[5];

x_axis[0] = 175;
x_ERR[0] = 0;
x_axis[1] = 250;
x_ERR[1] = 0;
x_axis[2] = 300;
x_ERR[2] = 0;
x_axis[3] = 350;
x_ERR[3] = 0;
x_axis[4] = 400;
x_ERR[4] = 0;

y_axis_map["HighEff"][0] = 81.9447;
y_ERR_map["HighEff"][0] = 0.510509;
y_axis_map["HighPur"][0] = 81.2049;
y_ERR_map["HighPur"][0] = 0.518507;
y_axis_map["SSVertex"][0] = 80.8702;
y_ERR_map["SSVertex"][0] = 0.522024;
y_axis_map["CSVertex"][0] = 87.6519;
y_ERR_map["CSVertex"][0] = 0.436637;
y_axis_map["JetProb"][0] = 86.824;
y_ERR_map["JetProb"][0] = 0.448902;
y_axis_map["JetBProb"][0] = 83.6533;
y_ERR_map["JetBProb"][0] = 0.490791;

y_axis_map["HighEff"][1] = 85.7681;
y_ERR_map["HighEff"][1] = 1.0726;
y_axis_map["HighPur"][1] = 84.4486;
y_ERR_map["HighPur"][1] = 1.11256;
y_axis_map["SSVertex"][1] = 84.9199;
y_ERR_map["SSVertex"][1] = 1.09862;
y_axis_map["CSVertex"][1] = 87.7474;
y_ERR_map["CSVertex"][1] = 1.00664;
y_axis_map["JetProb"][1] = 87.9359;
y_ERR_map["JetProb"][1] = 0.999937;
y_axis_map["JetBProb"][1] = 87.1819;
y_ERR_map["JetBProb"][1] = 1.02628;

y_axis_map["HighEff"][2] = 84.8894;
y_ERR_map["HighEff"][2] = 0.887646;
y_axis_map["HighPur"][2] = 85.9951;
y_ERR_map["HighPur"][2] = 0.860102;
y_axis_map["SSVertex"][2] = 85.9951;
y_ERR_map["SSVertex"][2] = 0.860102;
y_axis_map["CSVertex"][2] = 88.145;
y_ERR_map["CSVertex"][2] = 0.801167;
y_axis_map["JetProb"][2] = 88.2064;
y_ERR_map["JetProb"][2] = 0.799367;
y_axis_map["JetBProb"][2] = 86.7322;
y_ERR_map["JetBProb"][2] = 0.840742;

y_axis_map["HighEff"][3] = 85.4769;
y_ERR_map["HighEff"][3] = 0.874031;
y_axis_map["HighPur"][3] = 85.7846;
y_ERR_map["HighPur"][3] = 0.866278;
y_axis_map["SSVertex"][3] = 86.1538;
y_ERR_map["SSVertex"][3] = 0.856792;
y_axis_map["CSVertex"][3] = 88.0615;
y_ERR_map["CSVertex"][3] = 0.804342;
y_axis_map["JetProb"][3] = 87.3231;
y_ERR_map["JetProb"][3] = 0.825363;
y_axis_map["JetBProb"][3] = 87.2615;
y_ERR_map["JetBProb"][3] = 0.827072;

y_axis_map["HighEff"][4] = 83.8001;
y_ERR_map["HighEff"][4] = 0.764788;
y_axis_map["HighPur"][4] = 83.9724;
y_ERR_map["HighPur"][4] = 0.761491;
y_axis_map["SSVertex"][4] = 84.1448;
y_ERR_map["SSVertex"][4] = 0.758163;
y_axis_map["CSVertex"][4] = 86.2128;
y_ERR_map["CSVertex"][4] = 0.715626;
y_axis_map["JetProb"][4] = 86.6868;
y_ERR_map["JetProb"][4] = 0.705148;
y_axis_map["JetBProb"][4] = 85.782;
y_ERR_map["JetBProb"][4] = 0.724903;



  std::vector<std::pair<std::string,std::string> > btag_algos;
  btag_algos.push_back(std::pair<std::string,std::string>("Track Counting High Pur","HighEff"));
    btag_algos.push_back(std::pair<std::string,std::string>("Track Counting High Eff","HighPur"));
  btag_algos.push_back(std::pair<std::string,std::string>("Jet Probability","JetProb"));
  btag_algos.push_back(std::pair<std::string,std::string>("Jet B Probability","JetBProb"));
  btag_algos.push_back(std::pair<std::string,std::string>("Simple Secondary Vtx","SSVertex"));
  btag_algos.push_back(std::pair<std::string,std::string>("Combined Secondary Vtx","CSVertex"));
  //  btag_algos.push_back(std::pair<std::string,std::string>("CombinedSecondaryVertexMVA","CSVMVA"));
  //  btag_algos.push_back(std::pair<std::string,std::string>("ImpactParameterMVA","IPMVA"));


  std::vector<int> *colours = new std::vector<int>();
  colours->push_back(kBlack);
   colours->push_back(kRed+1);
   colours->push_back(kGreen+2);
   colours->push_back(kBlue);
   colours->push_back(kYellow+1);
   colours->push_back(kMagenta-7);

 CanvasHolder ch;
 std::string drawopt = "*L";

 ch.setTitle("Efficiency of algorithm in choosing real b-jet with highest b-tag");
 
 ch.setTitleX("t' mass [GeV]");
 ch.setTitleY("Efficiency ");
 ch.setCanvasTitle("btagging_algos");

 int step_x_axis = -6;
  for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){

	  std::pair<std::string,std::string> btag = (*btag_algo);
	 std::cout << "algo " << btag.first << " " <<btag.second << std::endl;

	 double x_axis_stepped[5];
  

 	 for(int i = 0;i < 5;i++){
	 	 y_axis[i] = y_axis_map[btag.second][i];
	 	 y_ERR[i] = y_ERR_map[btag.second][i];
	 	 x_axis_stepped[i] = (x_axis[i]+step_x_axis);
		 
 	 }
	 //	 std::cout <<"y ax :"<< y_axis[0] <<" " <<y_axis[1] <<" " <<y_axis[2] <<" " <<y_axis[3] <<" " <<y_axis[4] <<  std::endl;
	 //	 std::cout <<"y err :"<< y_ERR[0] <<" " <<y_ERR[1] << " " <<y_ERR[2] <<" " <<y_ERR[3] <<" " <<y_ERR[4] <<std::endl;

	 TGraphErrors *graph_name = new TGraphErrors(5,x_axis_stepped ,y_axis ,x_ERR ,y_ERR);

	  ch.addGraph(graph_name,"name1",btag.first,drawopt); 
	  ch.setMarkerSizeGraph(1);
	  ch.setLineSizeGraph(2);

	  step_x_axis = step_x_axis+2; 	  
  }


  
  ch.setLineColorsGraph(*colours);
  ch.setOptStat(00000);
  ch.setBordersY(75,100);
  ch.setBordersX(150,425);
  ch.save("eps");

 delete colours;
 colours=NULL;
 
 
}
