#include "TGraphErrors.h"
#include "Canvas_Holder.h"

int main(int argc, char **argv)
{

double ht_A1b[4];
double ht_AERR1b[4];
double ht_a1b[4];
double ht_aERR1b[4];
double ht_x_A1b[4];
double ht_x_a1b[4];
double ht_x_err1b[4];

ht_A1b[0] = 994.711;
ht_AERR1b[0] = 11.222;
ht_a1b[0] = 0.00899581;
ht_aERR1b[0] = 0.000115456;
ht_x_a1b[0] = 0.00942187414859766;
ht_x_A1b[0] = 1059.39;
ht_x_err1b[0] = 0;

ht_A1b[1] = 421.596;
ht_AERR1b[1] = 7.84022;
ht_a1b[1] = 0.0150639;
ht_aERR1b[1] = 0.000293025;
ht_x_a1b[1] = 0.0155029852030485;
ht_x_A1b[1] = 455.328;
ht_x_err1b[1] = 0;

ht_A1b[2] = 259.399;
ht_AERR1b[2] = 5.73105;
ht_a1b[2] = 0.0173271;
ht_aERR1b[2] = 0.000406219;
ht_x_a1b[2] = 0.0170697136670684;
ht_x_A1b[2] = 284.559;
ht_x_err1b[2] = 0;

ht_A1b[3] = 109.975;
ht_AERR1b[3] = 3.80598;
ht_a1b[3] = 0.0184901;
ht_aERR1b[3] = 0.000635676;
ht_x_a1b[3] = 0.0175158624174685;
ht_x_A1b[3] = 126.891;
ht_x_err1b[3] = 0;

TGraphErrors graph_A_ht1b(4, ht_x_A1b, ht_A1b, ht_x_err1b, ht_AERR1b);
TGraphErrors graph_a_ht1b(4, ht_x_a1b, ht_a1b, ht_x_err1b, ht_aERR1b);
double ht_A0b[4];
double ht_AERR0b[4];
double ht_a0b[4];
double ht_aERR0b[4];
double ht_x_A0b[4];
double ht_x_a0b[4];
double ht_x_err0b[4];

ht_A0b[0] = 1778.93;
ht_AERR0b[0] = 22.2862;
ht_a0b[0] = 0.00879806;
ht_aERR0b[0] = 0.000109775;
ht_x_a0b[0] = 0.008916507312025;
ht_x_A0b[0] = 1854.4;
ht_x_err0b[0] = 0;

ht_A0b[1] = 793.099;
ht_AERR0b[1] = 15.4469;
ht_a0b[1] = 0.0141174;
ht_aERR0b[1] = 0.000245784;
ht_x_a0b[1] = 0.0132068727354048;
ht_x_A0b[1] = 841.8;
ht_x_err0b[1] = 0;

ht_A0b[2] = 502.391;
ht_AERR0b[2] = 12.1598;
ht_a0b[2] = 0.0154343;
ht_aERR0b[2] = 0.000320471;
ht_x_a0b[2] = 0.0144596104572573;
ht_x_A0b[2] = 541.008;
ht_x_err0b[2] = 0;

ht_A0b[3] = 237.199;
ht_AERR0b[3] = 9.11069;
ht_a0b[3] = 0.0170706;
ht_aERR0b[3] = 0.000552117;
ht_x_a0b[3] = 0.0153939286944988;
ht_x_A0b[3] = 269.776;
ht_x_err0b[3] = 0;

TGraphErrors graph_A_ht0b(4, ht_x_A0b, ht_A0b, ht_x_err0b, ht_AERR0b);
TGraphErrors graph_a_ht0b(4, ht_x_a0b, ht_a0b, ht_x_err0b, ht_aERR0b);



	std::string parameter = "";
	if(argc > 1){
		parameter = argv[1];
	}else{
		std::cerr << "usage: " << argv[0] << " <parameter, a/A>" << std::endl;
		return -1;
	}

	std::cout << "parameter: " << parameter << std::endl;

	CanvasHolder ch;
	std::string drawopt = "*L";

	if(parameter == "A"){
		ch.setTitle("estimated parameter N_{0} in a #times N_{0} #times exp[-a #times (x-m_{cut})] #times histogram bin width");

		ch.setTitleX("N_{cut}");
		ch.setTitleY("parameter N_{0}");
		ch.setCanvasTitle("background_parameters");
		ch.addGraph(&graph_A_ht0b,"name1","(s3), H_{T} cuts",drawopt);
		ch.addGraph(&graph_A_ht1b,"name1","(s3) + b cut, H_{T} cuts",drawopt);
		ch.setBordersY(00,2600);
		//for chi2
		//ch.setBordersX(50,1700);
		//for Wb
		ch.setBordersX(50,2000);
	}else if(parameter == "a"){
		ch.setTitle("estimated parameter a in a #times N_{0} #times exp[-a #times (x-m_{cut})] #times histogram bin width");
		ch.setTitleX("ln(N_{cut}/N_{up})/(m_{up}-m_{cut})");
		//for chi2 matching
		//ch.setBordersY(0.010,0.06);
		//ch.setBordersX(0.01,0.035);
		//for Wb
		ch.setBordersY(0.006,0.025);
		ch.setBordersX(0.006,0.020);
		ch.setTitleY("parameter a");
		ch.setCanvasTitle("background_parameters");
		ch.addGraph(&graph_a_ht0b,"name1","(s3), H_{T} cuts",drawopt);
		ch.addGraph(&graph_a_ht1b,"name1","(s3) + b cut, H_{T} cuts",drawopt);
	}

	ch.setMarkerSizeGraph(1.5);
   ch.setLineSizeGraph(2);
   ch.setOptStat(00000);
	ch.save("eps");

        return 0;
}
