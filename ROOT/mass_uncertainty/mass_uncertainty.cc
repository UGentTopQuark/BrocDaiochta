#include "TGraphErrors.h"
#include "CanvasHolder/Canvas_Holder.h"
#include <iostream>

int main(int argc, char **argv)
{
	const int npoints = 9;
	double mass[npoints];
	double mass_err[npoints];
	double cs[npoints];
	double cs_err[npoints];

	if(argc < 2){
		std::cout << "usage: " << argv[0] << " <mu_data/e_data/emu_data>" << std::endl;
		return -1;
	}
	std::string mode = argv[1];

	if(mode == "mc_e"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 170.28;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 174.13;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 174.61;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 171.09;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 165.01;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 159.22;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 151.51;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 148.6;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 142.4;
	cs_err[8] = 0;

	} // mc_e

	if(mode == "mc_emu"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 167.18;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 171.5;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 172.04;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 168.1;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 164.86;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 156.25;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 148.75;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 144.52;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 137.41;
	cs_err[8] = 0;

	} // mc_emu

	if(mode == "data_e_eff_only"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 187.01;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 179.86;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 172.81;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 164.74;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 155.48;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 151.16;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 145.77;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 140.93;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 133.47;
	cs_err[8] = 0;

	} // data_e_eff_only

	if(mode == "data_e_shape_only"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 133.45;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 142.39;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 148.52;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 151.37;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 155.48;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 155.04;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 152.49;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 155.41;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 158.35;
	cs_err[8] = 0;

	} // data_e_shape_only

	if(mode == "data_emu_eff_only"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 181.15;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 175.05;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 168.41;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 161.02;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 155.93;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 148.98;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 143.55;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 138.92;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 133.72;
	cs_err[8] = 0;

	} // data_emu_eff_only

	if(mode == "data_emu_shape_only"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 136.03;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 144.32;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 150.85;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 153.36;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 155.93;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 155.07;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 152.6;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 154.41;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 152.69;
	cs_err[8] = 0;

	} // data_emu_shape_only


	if(mode == "data_e"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 160.51;
	cs_err[0] = 4.1;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 164.71;
	cs_err[1] = 3.8;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 165.08;
	cs_err[2] = 3.4;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 160.38;
	cs_err[3] = 3.15;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 155.48;
	cs_err[4] = 2.2;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 150.73;
	cs_err[5] = 2.8;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 142.96;
	cs_err[6] = 2.9;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 140.87;
	cs_err[7] = 3.0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 135.94;
	cs_err[8] = 3.2;

	} // data_e

	if(mode == "data_emu"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 158.04;
	cs_err[0] = 3.9;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 162.02;
	cs_err[1] = 2.8;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 162.93;
	cs_err[2] = 2.5;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 158.37;
	cs_err[3] = 2.7;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 155.93;
	cs_err[4] = 1.8;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 148.16;
	cs_err[5] = 3.0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 140.49;
	cs_err[6] = 5.1;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 137.57;
	cs_err[7] = 3.2;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 130.94;
	cs_err[8] = 7.2;

	} // data_emu

	if(mode == "mc_e_eff_only"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 198.47;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 190.88;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 183.4;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 174.83;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 165.01;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 160.43;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 154.7;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 149.57;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 141.65;
	cs_err[8] = 0;

	} // mc_e_eff_only

	if(mode == "mc_emu_eff_only"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 191.53;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 185.08;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 178.06;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 170.24;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 164.86;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 157.51;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 151.78;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 146.88;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 141.38;
	cs_err[8] = 0;

	} // mc_emu_eff_only

	if(mode == "mc_emu_shape_only"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 143.91;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 152.77;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 159.29;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 162.79;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 164.86;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 163.54;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 161.57;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 162.21;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 160.24;
	cs_err[8] = 0;

	} // mc_emu_shape_only

	if(mode == "mc_e_shape_only"){
	mass[0] = 161.5;
	mass_err[0] = 0;
	cs[0] = 141.58;
	cs_err[0] = 0;

	mass[1] = 163.5;
	mass_err[1] = 0;
	cs[1] = 150.54;
	cs_err[1] = 0;

	mass[2] = 166.5;
	mass_err[2] = 0;
	cs[2] = 157.1;
	cs_err[2] = 0;

	mass[3] = 169.5;
	mass_err[3] = 0;
	cs[3] = 161.48;
	cs_err[3] = 0;

	mass[4] = 172.5;
	mass_err[4] = 0;
	cs[4] = 165.01;
	cs_err[4] = 0;

	mass[5] = 175.5;
	mass_err[5] = 0;
	cs[5] = 163.77;
	cs_err[5] = 0;

	mass[6] = 178.5;
	mass_err[6] = 0;
	cs[6] = 161.61;
	cs_err[6] = 0;

	mass[7] = 181.5;
	mass_err[7] = 0;
	cs[7] = 163.95;
	cs_err[7] = 0;

	mass[8] = 184.5;
	mass_err[8] = 0;
	cs[8] = 165.88;
	cs_err[8] = 0;

	} // mc_e_shape_only




	TGraphErrors graph(npoints, mass, cs, mass_err, cs_err);

	TF1 fctup("lineup","[0]-[1]*x", 172.5, 184.5);
	graph.Fit(&fctup,"","",172.5,184.5);

	TF1 fctdown("linedown","[0]-[1]*x", 161.5, 172.5);
	graph.Fit(&fctdown,"","",161.5,172.5);

	double unc_up = 2.0;
	double unc_down = 0.0;
	double nominal_mass = 172.5;
	double nominal_cs = cs[4];

	std::cout << "------------------------" << std::endl;
	std::cout << "Mass Systematics: " << mode << std::endl;
	std::cout << nominal_cs << " " << fctup.Eval(nominal_mass+unc_up)-nominal_cs << " / " << fctdown.Eval(nominal_mass-unc_down)-nominal_cs << std::endl;
	std::cout << "------------------------" << std::endl;

	CanvasHolder ch;
	std::string drawopt = "LPE1";

	ch.setTitleX("Top Quark Mass (GeV/c^{2})");
	//for chi2 matching
	//ch.setBordersY(0.010,0.06);
	//ch.setBordersX(0.01,0.035);
	//for Wb
	ch.setTitleY("#sigma_{t#bar{t}} (pb)");
	ch.setCanvasTitle("mass_uncertainty_"+mode);
	ch.addGraph(&graph,"name1","measured cross section",drawopt);
	ch.addTF1(&fctup,"higher top mass","");
	ch.addTF1(&fctdown,"lower top mass","");

	ch.addHLine(fctup.Eval(nominal_mass+unc_up));
	ch.addHLine(fctdown.Eval(nominal_mass-unc_down));
	ch.addVLine(nominal_mass+unc_up);
	ch.addVLine(nominal_mass-unc_down);

	ch.setBordersY(125,200);
	ch.setMarkerStylesGraph(8);
	ch.setMarkerSizeGraph(1.0);
   	ch.setLineSizeGraph(2);
   	ch.setOptStat(00000);
	ch.save("pdf");

        return 0;
}
