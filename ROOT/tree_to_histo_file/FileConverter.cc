#include "FileConverter.h"

eire::FileConverter::FileConverter(std::string common_config, std::string config)
{
	read_configurations(common_config, config);
}

eire::FileConverter::~FileConverter()
{
	if(outfile){
		outfile->Write();
		outfile->Close();
	}
}

void eire::FileConverter::read_configurations(std::string common_config, std::string config)
{
	// config for this job
	eire::ConfigReader jcreader; //job config reader
	jcreader.read_config_from_file(config);

	std::string histo_suffix = jcreader.get_var("histo_suffix", "global");
	std::string outfile_name = jcreader.get_var("outfile_name", "global");

	outfile = new TFile(outfile_name.c_str(), "RECREATE");

	// find variables to plot in common config
	eire::ConfigReader creader;
	creader.read_config_from_file(common_config);
	std::vector<std::string> *sections = creader.get_sections();

	for(std::vector<std::string>::iterator section = sections->begin();
		section != sections->end();
		++section){
		int nbins = atoi(creader.get_var("nbins", *section).c_str());
		double xmin = atof(creader.get_var("xmin", *section).c_str());
		double xmax = atof(creader.get_var("xmax", *section).c_str());

		vars.push_back(eire::PlotVariable(*section, nbins, xmin, xmax, outfile, histo_suffix));
	}
}
