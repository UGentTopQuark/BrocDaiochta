#include <iostream>
#include "FileConverter.h"

int main(int argc, char **argv)
{
	std::string common_config = "common.cfg";
	std::string job_config = "job.cfg";

	eire::FileConverter converter(common_config, job_config);

	return 0;
}
