#ifndef EIRE_FILECONVERTER_H
#define EIRE_FILECONVERTER_H

#include "ConfigReader/ConfigReader.h"
#include <string>
#include <vector>
#include "TFile.h"
#include "PlotVariable.h"
#include "TH1F.h"

namespace eire{
	class FileConverter{
		public:
			FileConverter(std::string common_creader, std::string config);
			~FileConverter();

		private:
			void read_configurations(std::string common_config, std::string config);

			std::vector<eire::PlotVariable> vars;
			TFile *outfile;
	};
}

#endif
