#ifndef EIRE_PlotVariable_H
#define EIRE_PlotVariable_H

#include "TH1F.h"
#include "TFile.h"
#include <string>

namespace eire{
	class PlotVariable{
		public:
			PlotVariable(std::string var_name, int nbins, double xmin, double xmax, TFile *outfile, std::string histo_suffix);
			~PlotVariable();
			inline TH1F *histo(){ return histo_; };

		private:
			TH1F *histo_;
	};
}

#endif
