#include "PlotVariable.h"

eire::PlotVariable::PlotVariable(std::string var_name, int nbins, double xmin, double xmax, TFile *outfile, std::string histo_suffix)
{
	outfile->cd();
	histo_ = new TH1F((var_name+histo_suffix).c_str(), (var_name+histo_suffix).c_str(), nbins, xmin, xmax);
}

eire::PlotVariable::~PlotVariable()
{
}
