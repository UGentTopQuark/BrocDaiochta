#ifndef HistoFitter_H
#define HistoFitter_H

#include "TH1F.h"
#include "TF1.h"
#include "Tools.h"

class HistoFitter{
	public:
		HistoFitter();
		~HistoFitter();
		void set_histo(TH1F *histo);
		std::pair<double, double> fit_iterative_gaussian();
	private:
		TH1F *histo;
		Tools *tools;

		static const bool verbose = false;
};

#endif
