#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>
#include "HistoFitter.h"
#include "Tools.h"

int main(int argc, char **argv)
{
	std::string cutset = "09_cutset";

	std::vector<std::string> histos;
	histos.push_back("top_lepmass_RecoGenMatch_");
	histos.push_back("top_hadmass_RecoGenMatch_");
	histos.push_back("W_hadmass_RecoGenMatch_");

	std::vector<double> masses;
	masses.push_back(250);
//	masses.push_back(275);
//	masses.push_back(300);
//	masses.push_back(325);
//	masses.push_back(350);
//	masses.push_back(375);
//	masses.push_back(400);

	HistoFitter *hfitter = new HistoFitter();
	Tools *tools = new Tools();
	TFile *outfile = new TFile("output.root", "RECREATE");

	for(std::vector<double>::iterator mass = masses.begin();
	    mass != masses.end();
	    ++mass){
//		std::string file_name = "tprime_"+tools->stringify(*mass)+".root";
		std::string file_name = "ttbar.root";
		TFile *file = new TFile(file_name.c_str(), "open");
		for(std::vector<std::string>::iterator histo = histos.begin();
		    histo != histos.end();
		    ++histo){
			//std::string suffix = "Tprime"+tools->stringify(*mass)+"|muon|"+cutset;
			std::string suffix = "TTbar|muon|"+cutset;
			std::string histo_name = *histo + suffix;
			TH1F *current_histo = (TH1F *) file->GetDirectory("eventselection")->Get(histo_name.c_str());
			outfile->cd();
			hfitter->set_histo(current_histo);
			std::pair<double, double> fit_result = hfitter->fit_iterative_gaussian();

        		// par 1 = mean, par 2 = sigma
			double mean = fit_result.first;
			double sigma = fit_result.second;
			std::cout << "mass: " << *mass << " histo: " << *histo << " mean: " << mean << " sigma: " << sigma << std::endl;
		}
		std::cout << "----" << std::endl;
	}

	delete hfitter;
	hfitter = NULL;
	delete tools;
	tools = NULL;
	delete outfile;
	outfile = NULL;

	return 0;
}
