#ifndef HISTOCOMBINER_H
#define HISTOCOMBINER_H

#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "Canvas_Holder.h"
#include <sstream>
#include <vector>
#include <map>
#include <string>

class HistoCombiner{
	public:
		HistoCombiner();
		/*
		 *	add_histos() format:
		 *	first parameter: map<filename,vector of postfixes>
		 *	example: files["my_ttbar_root_file.root"].push_back("muon_events");
		 *		 files["my_ttbar_root_file.root"].push_back("electron_events");
		 *		 files["my_Wjets_root_file.root"].push_back("Wjets_events");
		 *	second parameter: names of the histograms with the different paraemters
		 *	example: variables.push_back("muon_pt");
		 *		 variables.push_back("jet_pt");
		 *		 ...
		 */

		// FIXME: what formatting will the postfixes have?
		void add_histos(std::map<std::string,std::vector<std::string> > &files_with_content, std::vector<std::string> &histo_names);
		void combine_histos();
		void write_histos();
		void scale_to_cross_section(double integrated_lumi);
		void set_cross_section(std::string process, double cross_section);
		void set_cutset(std::string cutset);
		void set_sequence_to_plot(std::vector<std::string> *sequence_to_plot);
		void set_colours(std::vector<int> *colour_scheme);
		void set_logscale(std::map<std::string, bool> *histos);
		void set_mass(std::string mass);

	private:
		void calculate_efficiency(std::string datatype, TH1F *histo);

		std::map<std::string,std::map<std::string,TH1F*> > histos;
		std::string getTitleX(std::string id);
		std::string getTitleY(std::string id);
		std::string getTitle(std::string id);

		std::string mass;

		// postfix(process)->cross_section
		std::map<std::string, double> cross_sections;
		std::map<std::string,double> efficiencies;
		std::map<std::string,CanvasHolder*> cholders;
		std::map<std::string, bool> *logscale;
		std::vector<int> *colours;
		std::vector<std::string> *sequence;
		std::string event_counter_histo_name;
		std::string cutset;

		TFile *outfile;
};

#endif
