#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TPaveLabel.h"
#include "TLatex.h"
#include "Canvas_Holder.h"
#include "Tools.h"
#include <sstream>


//Give cutset as arguement.
// Change version(as in python config) and mass (insert 175 for TTbar).
//Add mass reconstruction methods to reco_methods map. 
int main(int argc, char **argv)
{
  std::string d_channel = "muon";
  std::string cutset = argv[2];
  std::string mass = argv[1];
  std::string version = "Tprime"+mass;
  if(mass == "175")
	  version = "TTbar"; 
	  
  //set rebin histo below

  std::map<std::string,double> cross_sections;
  if(d_channel == "muon"){
	  cross_sections["TTbar"] = 375*0.15;
	  cross_sections["Tprime250"] = 35.6*0.15*0.70308358;
	  cross_sections["Tprime275"] = 21.67*0.15*0.7275882;
	  cross_sections["Tprime300"] = 13.65*0.15*0.72491179;
	  cross_sections["Tprime325"] = 8.86*0.15*0.72887521;
	  cross_sections["Tprime350"] = 5.88*0.15*0.74743326;
	  cross_sections["Tprime375"] = 4.03*0.15*0.73504274;
	  cross_sections["Tprime400"] = 2.80*0.15*0.74743326;
  }
  if(d_channel == "electron"){
	  cross_sections["TTbar"] = 375*0.15;
	  cross_sections["Tprime250"] = 35.6*0.15*0.69496503;
	  cross_sections["Tprime275"] = 21.67*0.15*0.70491567;
	  cross_sections["Tprime300"] = 13.65*0.15*0.72061495;
	  cross_sections["Tprime325"] = 8.86*0.15*0.71981808;
	  cross_sections["Tprime350"] = 5.88*0.15*0.73574409;
	  cross_sections["Tprime375"] = 43.03*0.15*0.73002868;
	  cross_sections["Tprime400"] = 2.80*0.15*0.73574409;
  }

  double integrated_lumi = 200;
  double cross_section = cross_sections[version];
  std::map<std::string,double> efficiencies;


  std::string filename_out= (cutset+d_channel+"_topmass.root");
  std::string event_counter_histo_name="event_counter_"+version+"|"+d_channel+"|"+cutset;
  std::string jets_matched_histo_name="top_jets_genmatched_"+version+"|"+d_channel+"|"+cutset; 

   
  TFile *outfile = new TFile(filename_out.c_str(),"recreate");
  TFile *file = NULL;
     //open root file
  if(mass != "175"){
	  std::cout<<"opening file tprime_"<<mass<<".root"<<std::endl;
	  file = new TFile(("tprime_"+mass+".root").c_str(),"open"); 
  }
  else{
	  std::cout<<"opening file ttbar.root"<<std::endl;
	  file = new TFile("ttbar.root","open"); 
  }

  std::vector<std::string> reco_methods;
   reco_methods.push_back("mass_minimisation");
   reco_methods.push_back("mass");
   reco_methods.push_back("Had_chimass");
   reco_methods.push_back("Had_1btag_chimass");
   reco_methods.push_back("Had_2btag_chimass");
   reco_methods.push_back("mass_minimisation_1btag");
   reco_methods.push_back("mass_minimisation_2btag");

  std::vector<int> *colours = new std::vector<int>();
  colours->push_back(kBlack);
  colours->push_back(kGreen+3);
  colours->push_back(kRed-3);
  colours->push_back(kBlue+1);

  CanvasHolder *cholders[reco_methods.size()+1];
  int canvas_count=0;
  for(std::vector<std::string>::iterator reco_method = reco_methods.begin();
      reco_method != reco_methods.end();
      reco_method++)
    {
	    efficiencies.clear();
	    
	    std::vector<std::string> *sequence = new std::vector<std::string>();
	    std::map<std::string,std::string> legend_names;
	    //<histo_name, bin in mass_counter for eff>
	    std::map<std::string, int> histo_names;
	    histo_names["top_"+*reco_method+"_3jetmatched_"+version+"|"+d_channel+"|"+cutset] = 5;
	    sequence->push_back("top_"+*reco_method+"_3jetmatched_"+version+"|"+d_channel+"|"+cutset);
	    legend_names["top_"+*reco_method+"_3jetmatched_"+version+"|"+d_channel+"|"+cutset] ="3 jets genmatched: " ;
	    histo_names["top_"+*reco_method+"_2jetmatched_"+version+"|"+d_channel+"|"+cutset] = 4;
	    sequence->push_back("top_"+*reco_method+"_2jetmatched_"+version+"|"+d_channel+"|"+cutset);
	    legend_names["top_"+*reco_method+"_2jetmatched_"+version+"|"+d_channel+"|"+cutset] ="2 jets genmatched: " ;
	    histo_names["top_"+*reco_method+"_1jetmatched_"+version+"|"+d_channel+"|"+cutset] = 3;
	    sequence->push_back("top_"+*reco_method+"_1jetmatched_"+version+"|"+d_channel+"|"+cutset);
	    legend_names["top_"+*reco_method+"_1jetmatched_"+version+"|"+d_channel+"|"+cutset] ="1 jets genmatched: " ;
	    histo_names["top_"+*reco_method+"_0jetmatched_"+version+"|"+d_channel+"|"+cutset] = 2;
	    sequence->push_back("top_"+*reco_method+"_0jetmatched_"+version+"|"+d_channel+"|"+cutset);
	    legend_names["top_"+*reco_method+"_0jetmatched_"+version+"|"+d_channel+"|"+cutset] ="0 jets genmatched: " ;
	    
	    std::string combined_mass_histo_name = "top_"+*reco_method+"_"+version+"|"+d_channel+"|"+cutset;
// 	    if(*reco_method != "chimass" && *reco_method != "chimass_1btag"  && *reco_method != "chimass_2btag" )
// 		    combined_mass_histo_name="top_"+*reco_method+"_"+version+"|"+d_channel+"|"+cutset;
// 	    else if(*reco_method == "chimass")
// 		    combined_mass_histo_name="top_Had_"+*reco_method+"_"+version+"|"+d_channel+"|"+cutset;
// 	    else if(*reco_method == "chimass_1btag")
// 		    combined_mass_histo_name="top_Had_1btag_chimass_"+version+"|"+d_channel+"|"+cutset;
// 	    else
// 		    combined_mass_histo_name="top_Had_2btag_chimass_"+version+"|"+d_channel+"|"+cutset;
		    
 
	     
	    file->cd();
	    
	    //map<histo name,histo>
	    std::map<std::string,TH1F*> histos;
	    
	    //open histos and calculate efficiency
	    for(std::map<std::string,int>::iterator histo_name = histo_names.begin();
		histo_name != histo_names.end();
		++histo_name)
		    {
			    std::string complete_name = (histo_name->first);
			    outfile->cd();
			    
			    //open all histograms
			    std::cout << "opening histo: " << complete_name.c_str() << std::endl;
			    TH1F *histo = new TH1F(*((TH1F *) file->GetDirectory("eventselection")->Get(complete_name.c_str())));
			    
			    histos[histo_name->first] = histo;
			    //rebin histogram
			    histo->Rebin(2);

				    
			    //calculate efficiency
			    std::cout << "opening histo: " << (combined_mass_histo_name).c_str() << std::endl;
			    TH1F *combined_mass_histo = new TH1F(*((TH1F *) file->GetDirectory("eventselection")->Get((combined_mass_histo_name).c_str())));
			    
			    // efficiency = (entries in histogram) / (entries in combined mass histogram)
			    double eff=0;
			    if(combined_mass_histo->GetEntries() != 0){
				    eff = histo->GetEntries() / combined_mass_histo->GetEntries();
				    efficiencies[histo_name->first]=eff;
			    } 
			    
			    
		    }
	    
	    //scale to cross section
	    for(std::map<std::string,TH1F*>::iterator histo = histos.begin();
		histo != histos.end();
		++histo)
		    {
			    
			    double scale = 1.0;
			    if(histo->second->Integral() != 0){
				    scale = 1.0/histo->second->Integral();

				    histo->second->Scale(scale);    
				    histo->second->Scale(cross_section*integrated_lumi*efficiencies[histo->first]);
				    std::cout << "Scaling Histo " << histo->first << " to " << cross_section * integrated_lumi*efficiencies[histo->first] << std::endl;
				    std::cout << "integrated lumi " << integrated_lumi << std::endl;
				    std::cout << "efficiency " << efficiencies[histo->first] <<std::endl;
				    std::cout << "cross_section " << cross_section << std::endl;
				    
			    }
			    
		    }

  //initialise canvas   
  cholders[canvas_count] = new CanvasHolder();
  std::string combined_title = ("Jets_combined_"+*reco_method);
  cholders[canvas_count]->setCanvasTitle(combined_title);
  
  
	    for(std::vector<std::string>::iterator item = sequence->begin();
	    item != sequence->end();
		++item){
	  
		    for(std::map<std::string,TH1F*>::iterator histo = histos.begin();
			histo != histos.end();
			++ histo){
			    
			    
			    if (*item == histo->first)
				    {
					    //convert eff double to string
					    std::ostringstream temp;
					    int temp_2 = efficiencies[*item]*1000;
					    temp << (double(temp_2)/10);
					    std::string string_eff = temp.str();
					    cholders[canvas_count]->addHistoStacked(histo->second, legend_names[*item]+string_eff+"%");
	
					    cholders[canvas_count]->setOptStat(000000);
					    break;
				    }
		    }
	    }
	    
	    // if(colours != NULL)
	    // cholders[canvas_count]->setLineColors(*colours);
	    cholders[canvas_count]->setLineColors();
	    cholders[canvas_count]->write(outfile);
	    cholders[canvas_count]->save("eps");
	    canvas_count++;
	    
    }
  
  delete file;
  file=NULL;
  delete outfile;
  outfile=NULL;
  delete colours;
  colours=NULL;
   
  return 0;  
    }

