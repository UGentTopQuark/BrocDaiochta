#include "HistoCombiner.h"

HistoCombiner::HistoCombiner()
{
	// where to get information from about the filter efficiency
	// --> important for scaling of datasets
	// event_counter_histo bin 0: one entry for each processed event
	// event_counter_histo bin 1: one entry for each event passing the cuts
	event_counter_histo_name="event_counter_";

	// sequence for the stacked layers
	sequence=NULL;

	colours=NULL;
	logscale=NULL;
	mass = "";

	// output file -> to this file all the combined output histograms will be written
	// FIXME: this should be a parameter
	outfile = new TFile("test.root", "recreate");
}

void HistoCombiner::set_mass(std::string mass)
{
	this->mass = mass;
}

void HistoCombiner::set_colours(std::vector<int> *colour_scheme)
{
	colours = colour_scheme;
}

void HistoCombiner::add_histos(std::map<std::string,std::vector<std::string> > &files_with_content, std::vector<std::string> &histo_names)
{
	// cf. definition in header
	// file_with_content->first: filename
	// file_with_content->second: vector of datasets within file
	// 			--> important because in ttbar.root files
	// 			    3 types of events will be included
	// 			    (ttbar-e, ttbar-mu, ttbar-other)
	for(std::map<std::string, std::vector<std::string> >::iterator file_with_content = files_with_content.begin();
	    file_with_content != files_with_content.end();
	    ++file_with_content)
	{
		// open input file from CMSSW analyser
		TFile *file = new TFile(file_with_content->first.c_str(),"open");

		// loop over vector of names of histograms that shall be read out
		// from the CMSSW root files and shall be combined
		for(std::vector<std::string>::iterator histo_name = histo_names.begin();
		    histo_name != histo_names.end();
		    ++histo_name)
		{
			// loop over the data-type within the root file (ttbar-e, ttbar-mu, ...)
			// this is the postfix within the CMSSW files, eg. njets_muon_ttbar-e
			for(std::vector<std::string>::iterator postfix = file_with_content->second.begin();
			    postfix != file_with_content->second.end();
			    ++postfix)
			{
				// complete histo name = same name for all histos (eg. njets)
				// 			 + postfix (eg. muon_ttbar-e)
				std::string complete_name = (*histo_name)+(*postfix);
				// new histogram shall be created within scope of outfile
				outfile->cd();
				// read histogram from CMSSW root file and write it to
				// histos[][]
				// FIXME: variable for directory
				std::cout << "opening histo: " << complete_name.c_str() << std::endl;
				TH1F *histo = new TH1F(*((TH1F *) file->GetDirectory("eventselection")->Get(complete_name.c_str())));
				histos[*histo_name][*postfix]=histo;	

				// the efficiency is for all histograms of a dataset the same
				// -> calculate it once when processing the first histo
				//    of a dataset
			//	if(postfix==file_with_content->second.begin())
					calculate_efficiency(*postfix, (TH1F *) file->GetDirectory("eventselection")->Get((event_counter_histo_name+*postfix).c_str()));
			}
		}
		delete file;
		file=NULL;
	}
}

void HistoCombiner::calculate_efficiency(std::string datatype, TH1F *histo)
{
	// efficiency = (events passing cut) / (all events)
	// FIXME: is that correct?
	double eff=0;
	if(histo->GetBinContent(1) != 0)
		eff = histo->GetBinContent(2) / histo->GetBinContent(1);
	// save efficiency for dataset centrally
	efficiencies[datatype]=eff;
}

void HistoCombiner::set_cross_section(std::string process, double cross_section)
{
	// cross section for data type
	cross_sections[process] = cross_section;
}

void HistoCombiner::set_sequence_to_plot(std::vector<std::string> *sequence_to_plot)
{
	sequence = sequence_to_plot;
}

void HistoCombiner::scale_to_cross_section(double integrated_lumi)
{
	for(std::map<std::string, std::map<std::string,TH1F*> >::iterator variable = histos.begin();
	    variable != histos.end();
	    ++variable)
	{
		for(std::map<std::string,TH1F*>::iterator histo = variable->second.begin();
		    histo != variable->second.end();
		    ++histo)
		{
			// scale histogram to cross section
			if(cross_sections.find(histo->first) == cross_sections.end())
				std::cerr << "WARNING: cross section not set for: " << histo->first << " ...ignoring... " << std::endl;
			else{
				// normalize histogram
				double scale = 1.0;
				if(histo->second->Integral() != 0)
					scale = 1.0/histo->second->Integral();

				histo->second->Scale(scale);
	
				histo->second->Scale(cross_sections[histo->first]*integrated_lumi*efficiencies[histo->first]);
				std::cout << "Scaling Histo " << histo->first << " to " << cross_sections[histo->first] * integrated_lumi*efficiencies[histo->first] << std::endl;
				std::cout << "integrated lumi " << integrated_lumi << std::endl;
				std::cout << "efficiency " << efficiencies[histo->first] << std::endl;
				std::cout << "cross_section " << cross_sections[histo->first] << std::endl;
			}
		}
	}		
}

void HistoCombiner::set_logscale(std::map<std::string,bool> *histos)
{
	logscale=histos;
}

void HistoCombiner::combine_histos()
{
	// loop over all histograms of the same type (all p_t, all H_T,...) from the different
	// datasets, book for them a CanvasHolder and add them to the CanvasHolder
	for(std::map<std::string,std::map<std::string,TH1F*> >::iterator variable = histos.begin();
	    variable != histos.end();
	    ++variable)
	{
		// variable = p_t, H_t, eta...
		cholders[variable->first] = new CanvasHolder();
		cholders[variable->first]->setCanvasTitle(variable->first);
		cholders[variable->first]->setTitleY(getTitleY(variable->first));
		cholders[variable->first]->setTitleX(getTitleX(variable->first));
		//		cholders[variable->first]->setBordersX(0,10);
		if(logscale != NULL && logscale->find(variable->first) != logscale->end() && (*logscale)[variable->first])
			cholders[variable->first]->setLogY();
		
		if(sequence == NULL){
			// histo: the actual histogram
			for(std::map<std::string, TH1F*>::iterator histo = variable->second.begin();
			    histo != variable->second.end();
			    ++histo)
			{
			//	histo->second->Rebin(5);
				cholders[variable->first]->addHistoStacked(histo->second, getTitle(histo->first));
				cholders[variable->first]->setOptStat(000000);
			}
		}
		else{
			for(std::vector<std::string>::iterator item = sequence->begin();
				item != sequence->end();
				++item){
				cholders[variable->first]->addHistoStacked(variable->second[*item], getTitle(*item));
				cholders[variable->first]->setOptStat(000000);
			}
		}
//		cholders[variable->first]->setBordersY(0.0, histo->second->GetYaxis()->GetXmax() *15);

		if(colours != NULL)
			cholders[variable->first]->setLineColors(*colours);
		cholders[variable->first]->write(outfile);
		cholders[variable->first]->save("eps");
	}
}

std::string HistoCombiner::getTitle(std::string id)
{
	if(id == "Tprime"+mass+"|muon|"+cutset){
		return "t' #bar{t}'(signal, #mu)";
	}
	if(id == "Tprime"+mass+"|mu_background|"+cutset){
		return "t' #bar{t}'(other, #mu)";
	}
	if(id == "TTbar|muon|"+cutset){
		return "t #bar{t}(signal, #mu)";
	}
	//if(id == "TTbar|mu_background|"+cutset){
	//	return "t #bar{t} (other)";
	//}
	if(id == "TTbar|mu_background|"+cutset){
		return "t #bar{t}";
	}
	if(id == "Wjets|mu_background|"+cutset){
		return "W + Jets";
	}
	if(id == "Zjets|mu_background|"+cutset){
		return "Z + Jets";
	}
	if(id == "Mupt15|mu_background|"+cutset){
		return "QCD";
	}

	if(id == "Tprime"+mass+"|electron|"+cutset){
		return "t' #bar{t}'(signal, e)";
	}
	if(id == "Tprime"+mass+"|e_background|"+cutset){
		return "t' #bar{t}'(other, e)";
	}
	if(id == "TTbar|electron|"+cutset){
		return "t #bar{t}(signal, e)";
	}
	//if(id == "TTbar|e_background|"+cutset){
	//	return "t #bar{t}(other)";
	//}
	if(id == "TTbar|e_background|"+cutset){
		return "t #bar{t}";
	}
	if(id == "Wjets|e_background|"+cutset){
		return "W + Jets";
	}
	if(id == "Zjets|e_background|"+cutset){
		return "Z + Jets";
	}
	if(id == "QCDpt15|e_background|"+cutset){
		return "QCD";
	}
	return "unknown";
}

std::string HistoCombiner::getTitleX(std::string id)
{
	if(id == "chi2_wo_btag_")
		return "#chi^{2}/n.d.f.";
	if(id == "unmatch_mu_caloiso_")
		return "caloIso [GeV]";
	if(id == "unmatch_mu_trackiso_")
		return "trackIso [GeV]";
	if(id == "unmatch_e_caloiso_")
		return "caloIso [GeV]";
	if(id == "unmatch_e_trackiso_")
		return "trackIso [GeV]";
	if(id == "electron_MtW_")
		return "M_{T} (W) [GeV]";
	if(id == "electron_eta_")
		return "#eta";
	if(id == "electron_phi_")
		return "#phi";
	if(id == "electron_pt_")
		return "p_{T}";
	if(id == "electron_number_")
		return "number of electrons in event";
	if(id == "muon_MtW_")
		return "M_{T} (W) [GeV]";
	if(id == "muon_eta_")
		return "#eta";
	if(id == "muon_phi_")
		return "#phi";
	if(id == "muon_pt_")
		return "p_{T} [GeV]";
	if(id == "muon_number_")
		return "number of muons in event";
	if(id == "jet1_pt_")
		return "1st jet p_{T} [GeV]";
	if(id == "jet2_pt_")
		return "2nd jet p_{T} [GeV]";
	if(id == "jet3_pt_")
		return "3rd jet p_{T} [GeV]";
	if(id == "jet4_pt_")
		return "4th jet p_{T} [GeV]";
	if(id == "jet_pt_")
		return "all jet p_{T} [GeV]";
	if(id == "jet_eta_")
		return "all jet #eta";
	if(id == "jet_phi_")
		return "all jet #phi";
	if(id == "jet_number_")
		return "number of jets in event";
	if(id == "min_e_dR_")
		return "min dR(e,Jet)";
	if(id == "min_mu_dR_")
		return "min dR(#mu,Jet)";
	if(id == "top_mass_w_METpz_")
		return "reconstructed mass [GeV]";
	if(id == "top_mass_")
		return "reconstructed mass [GeV]";
	if(id == "top_mass_minimisation_")
		return "reconstructed mass [GeV]";
	if(id == "top_mass_minimisation_w_METpz_")
		return "reconstructed mass [GeV]";
	if(id == "W_mass_")
		return "hadronic W mass [GeV]";
	if(id == "Muon_Ht_")
		return "H_{T}^{#mu} [GeV]";
	if(id == "Electron_Ht_")
		return "H_{T}^{e} [GeV]";
	if(id == "Event_MET_")
		return "E_{T}^{miss} [GeV]";
	return "";
}

std::string HistoCombiner::getTitleY(std::string id)
{
	return "entries [200/pb]";
}

void HistoCombiner::write_histos()
{
	delete outfile;
	outfile=NULL;
}

void HistoCombiner::set_cutset(std::string cutset)
{
	this->cutset = cutset;
}
