#ifndef INTEGRALERRORCALCULATOR_H
#define INTEGRALERRORCALCULATOR_H

#include "TF1.h"
#include "TH1F.h"
#include "TRandom3.h"
#include "TFile.h"

class IntegralErrorCalculator{
	public:
		IntegralErrorCalculator();
		~IntegralErrorCalculator();
		void set_function(TF1 *function);
		void set_parameters(std::vector<std::pair<double,double> > *parameters);
		void set_integration_range(double min, double max);
		void set_outfile(TFile *outfile);

		double estimate_uncertainty(int niterations = 1000);
	private:
		double get_integral();
		TF1 *function;
		// parameters for gaussian smearing, vector of different paramter pairs(mean,
		// sigma)
		std::vector<std::pair<double,double> > *parameters;
		double integral_min, integral_max;
		TH1F *integral_values;
		TRandom3 *random_gen;
		TFile *outfile;

		double histo_min, histo_max;
};

#endif
