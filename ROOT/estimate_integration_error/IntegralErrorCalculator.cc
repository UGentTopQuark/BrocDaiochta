#include "IntegralErrorCalculator.h"

IntegralErrorCalculator::IntegralErrorCalculator()
{
	function = NULL;
	parameters = NULL;
	random_gen = new TRandom3();
	integral_values = NULL;
	histo_min = 500.;
	histo_max = 2500.;
}

IntegralErrorCalculator::~IntegralErrorCalculator()
{
	TF1 gauss("gauss","gaus(0)", histo_min, histo_max);
	gauss.SetParameter(0,integral_values->Integral());
	gauss.SetParameter(1,integral_values->GetMean());
	gauss.SetParameter(2,integral_values->GetRMS());
	integral_values->Fit("gauss");
	integral_values->Write();
}

void IntegralErrorCalculator::set_function(TF1 *function)
{
	this->function = function;
}

// parameters for gaussian smearing, vector of different paramter pairs(mean,
// sigma)
void IntegralErrorCalculator::set_parameters(std::vector<std::pair<double,double> >
*parameters)
{
	this->parameters = parameters;
}

void IntegralErrorCalculator::set_integration_range(double min, double max)
{
	this->integral_min = min;
	this->integral_max = max;
}

double IntegralErrorCalculator::estimate_uncertainty(int niterations)
{
	double uncertainty=-1;

	for(int i = 0; i < niterations; ++i){
		double integral = get_integral();
		integral_values->Fill(integral);
	}

	return uncertainty;
}

double IntegralErrorCalculator::get_integral()
{
	int i = 0;
	for(std::vector<std::pair<double,double> >::iterator parameter = parameters->begin();
		parameter != parameters->end();
		++parameter){
		function->SetParameter(i,random_gen->Gaus(parameter->first, parameter->second));
		++i;
	}
	return function->Integral(integral_min, integral_max);
}

void IntegralErrorCalculator::set_outfile(TFile *outfile)
{
	this->outfile = outfile;
	outfile->cd();
	integral_values = new TH1F("integral_values","Results from integration after smearing of function to integrate",500,histo_min,histo_max);
}
