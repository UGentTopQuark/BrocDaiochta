#include <sstream>
#include <iostream>
#include <map>
#include <vector>

#include "TF1.h"

#include "IntegralErrorCalculator.h"

int main(int argc, char **argv)
{
	std::vector<std::pair<double,double> > *parameters = new std::vector<std::pair<double,double> >();
	TFile *outfile = new TFile("outfile.root", "RECREATE");
	/*
	TF1 *function = new TF1("function","landau(0)/0.02",0,200);
	parameters->push_back(std::pair<double,double>(20899.4,77.78));
	parameters->push_back(std::pair<double,double>(0.903148,0.00675413));
	parameters->push_back(std::pair<double,double>(0.356343,0.00379870));

	TF1 *function = new TF1("function","gaus(0)/0.02",0,200);
	parameters->push_back(std::pair<double,double>(3897.57,13.6933));
	parameters->push_back(std::pair<double,double>(0.795526,0.00350338));
	parameters->push_back(std::pair<double,double>(0.368419,0.00286904));
	*/

	IntegralErrorCalculator *error_calc = new IntegralErrorCalculator();


	error_calc->set_outfile(outfile);
	error_calc->set_parameters(parameters);
	error_calc->set_integration_range(0.,0.1);
	error_calc->set_function(function);

	std::cout << "estimating uncertainty..." << std::endl;
	error_calc->estimate_uncertainty(100000);
	std::cout << "writing outfile..." << std::endl;

	delete error_calc;
	error_calc = NULL;
	delete outfile;
	outfile = NULL;

	return 0;
}
