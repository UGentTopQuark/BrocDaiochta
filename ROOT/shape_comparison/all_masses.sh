#!/bin/bash

#for i in 250 275 300 325 350 375 400; do
for i in 250 300 350 400; do
	echo "processing ${i}...";
	./mass_quality ${i} 2> /dev/null | grep width;
	mkdir ${i};
	mv *.eps ${i};
	cd ${i}; for j in `ls *.eps`; do
			epstopdf ${j};
		done
		rm *.eps;
		cd ..;
done
