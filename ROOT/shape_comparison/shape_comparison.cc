#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>
#include "Canvas_Holder.h"
#include "HistoMerger/MergeHistos.h"

int main(int argc, char **argv)
{
	if(argc < 3){
		std::cout << "usage: ./shape_comaprison <e/mu> <s/b/c>\n" << std::endl;
		exit(1);
	}
	/*********************************************
	 *	Configuration
	 ********************************************/
	// mass can be set as command line parameter
	std::string mass = "175";

	std::string dataset = "ttbar";
	std::string e_mu = argv[1];
	std::string d_channel = "electron";
	if(e_mu == "mu")
		d_channel = "muon";

	// what to process? s: signal, b: background
	std::string signal_background = argv[2] ;

	
	if(e_mu != "mu" && e_mu !="e"){
	  std::cout << "Invalid arguments, please choose 'e' or 'mu' \n" << std::endl;
	  exit(1);
        }

	if(signal_background != "s" && signal_background != "b" && signal_background != "c"){
	  std::cout << "Invalid arguments, please choose 's', 'b' or 'c' \n" << std::endl;
	  exit(1);
	}

	// suffix in file_name
	std::vector<std::string> versions;
 	//versions.push_back("_plots");
	//versions.push_back("mcatnlo_plots");
	//versions.push_back("_pythia_plots");
 	//versions.push_back("a0j_plots");
//        versions.push_back("a1j_plots");
 //       versions.push_back("a2j_plots");
  //      versions.push_back("a3j_plots");
   //     versions.push_back("a4j_plots");
 	//versions.push_back("_jet");
 	//versions.push_back("_jes02");
 	//versions.push_back("_jes05");
 	//versions.push_back("_jes10");
 	versions.push_back("_plots_jes090");
 	versions.push_back("_plots_jes100");
 	versions.push_back("_plots_jes110");
	
	std::vector<std::string> cutsets;
	cutsets.push_back("01_cutset");
	//cutsets.push_back("02_cutset");
	//cutsets.push_back("06_cutset");
	//cutsets.push_back("01_cutset");
	//cutsets.push_back("05_cutset");
	//cutsets.push_back("06_cutset");
	//cutsets.push_back("08_cutset");

	// for TLegend, needs the same number of entries as cutsets vector
	std::vector<std::string> identifier;
	//identifier.push_back("no Ht cut,");
	identifier.push_back("");
	//identifier.push_back("02");
	//identifier.push_back("s5 ");
	//identifier.push_back(" s5 +");
	//identifier.push_back(" s5");
	//identifier.push_back(" s5 -");
	//identifier.push_back(" ");

	std::map<std::string, std::string> file_idents;
	//file_idents["_plots"] = " MadGraph 7TeV ";
	//file_idents["mcatnlo_plots"] = " MCatNLO ";
	//file_idents["_pythia_plots"] = " Pythia ";
	//file_idents["_plots_10TeV"] = " 10TeV ";
 	//file_idents["a0j_plots"] = " AlpGen + 0J ";
        //file_idents["a1j_plots"] = " AlpGen + 1J ";
        //file_idents["a2j_plots"] = " AlpGen + 2J ";
        //file_idents["a3j_plots"] = " AlpGen + 3J ";
        //file_idents["a4j_plots"] = " AlpGen + 4J ";
	//file_idents["_jes02"] = " +2% JES";
	//file_idents["_jes05"] = " +5% JES";
	//file_idents["_jes10"] = " +10% JES";
	file_idents["_plots_jes090"] = " -10% JES";
	file_idents["_plots_jes100"] = " nominal JES";
	file_idents["_plots_jes110"] = " +10% JES";



	// for overlaying plots of different dataset jet bins
	std::vector<std::string> dataset_suffix;
	dataset_suffix.push_back("");
	dataset_suffix.push_back("");
	dataset_suffix.push_back("");
	//dataset_suffix.push_back("MCatNLO");
	//dataset_suffix.push_back("Py");
        //dataset_suffix.push_back("A0j");
        //dataset_suffix.push_back("A1j");
        //dataset_suffix.push_back("A2j");
        //dataset_suffix.push_back("A3j");
        //dataset_suffix.push_back("A4j");

	bool normalise_histos = false;


	/*********************************************
	 *	Main Program
	 ********************************************/

	std::map<std::string,std::string> dictionary;
// 	dictionary["top_mass_minimisation_"]="min.diff.M3";
// 	dictionary["top_mass_"]="M3";
// 	dictionary["top_mass_minimisation_w_METpz_"]="min.diff.M3, long. #nu";
// 	dictionary["top_Had_1btag_chimass_"]="had. t', 1 b-tag";
// 	dictionary["top_Lep_1btag_chimass_"]="lep. t', 1 b-tag";
// 	dictionary["top_Had_2btag_chimass_"]="had. t', 2 b-tag";
// 	dictionary["top_Lep_2btag_chimass_"]="lep. t', 2 b-tag";
// 	dictionary["top_Had_chimass_"]="had. t', no b-tag";
// 	dictionary["top_Lep_chimass_"]="lep. t', no b-tag";
// 	dictionary["top_mass_w_METpz_"]="M3, long. #nu";
// 	dictionary["tprime"]="t'";
// 	dictionary["top_hadmass_Wb_0btag_"]="had. t'";
// 	dictionary["top_hadmass_Wb_1btag_"]="had. t', 1 b-tag";

	std::map<std::string,std::vector<std::string> > file_names;

	std::vector<std::string> histo_names;
	//histo_names.push_back("jet_number_");
	histo_names.push_back("jet1_pt_");
	//histo_names.push_back("jet2_pt_");
	//histo_names.push_back("jet3_pt_");
	//histo_names.push_back("jet4_pt_");
	//histo_names.push_back("muon_number_");
	histo_names.push_back("muon_pt_");
	//histo_names.push_back("muon_eta_");
	//histo_names.push_back("muon_d0_");
	//histo_names.push_back("unmatch_mu_trackiso_");
	//histo_names.push_back("unmatch_mu_caloiso_");
	//histo_names.push_back("electron_number_");
	//histo_names.push_back("electron_pt_");
	//histo_names.push_back("electron_eta_");
	//histo_names.push_back("electron_d0_");
	//histo_names.push_back("unmatch_e_trackiso_");
	//histo_names.push_back("unmatch_e_caloiso_");
	//histo_names.push_back("Ht_wo_MET_");
	//histo_names.push_back("top_mass_");
	//histo_names.push_back("jet_highest_bDiscrim_cut_HighEff_");
	//histo_names.push_back("top_hadmass_Wb_0btag_");
	//histo_names.push_back("top_hadmass_Wb_1btag_");
//        histo_names.push_back("jet_Ht_");
//        histo_names.push_back("jet_size_");
	//histo_names.push_back("top_lepmass_Wb_0btag_");
	//histo_names.push_back("top_lepmass_Wb_1btag_");
	//histo_names.push_back("top_mass_minimisation_");
	//histo_names.push_back("top_Had_chimass_");
	//histo_names.push_back("top_Had_1btag_chimass_");
	//histo_names.push_back("top_Had_2btag_chimass_");
	//histo_names.push_back("top_Lep_chimass_");
	//histo_names.push_back("top_Lep_1btag_chimass_");
	//histo_names.push_back("top_Lep_2btag_chimass_");
	//histo_names.push_back("top_mass_w_METpz_");
	//histo_names.push_back("top_mass_minimisation_w_METpz_");
	//histo_names.push_back("top_lepmass_Wb_1btag_");
	//histo_names.push_back("top_mass_minimisation_");
	//histo_names.push_back("top_Had_chimass_");
	//histo_names.push_back("top_Had_1btag_chimass_");
	//histo_names.push_back("top_Had_2btag_chimass_");
 	//histo_names.push_back("top_Lep_chimass_");
 	//histo_names.push_back("top_Lep_1btag_chimass_");
	//histo_names.push_back("top_Lep_2btag_chimass_");
	//histo_names.push_back("top_mass_w_METpz_");
	//histo_names.push_back("top_mass_minimisation_w_METpz_");



	std::vector<CanvasHolder*> objects_to_delete;

	std::vector<int> *colours = new std::vector<int>();  
	colours->push_back(kGreen); 
	colours->push_back(kRed); 
	colours->push_back(kBlack); 
	colours->push_back(kGreen+1); 
	colours->push_back(kBlue); 			
	
	TFile *outfile = new TFile("outfile.root", "RECREATE");

	std::vector<MergeHistos*> histo_merger;
	for(unsigned int i = 0; i < cutsets.size(); ++i){
		histo_merger.push_back(new MergeHistos());
		histo_merger[i]->set_outfile(outfile);
	}
	std::vector<TH1F*> merged_histos;

	for(std::vector<std::string>::iterator histo_name = histo_names.begin();
		histo_name != histo_names.end();
		++histo_name)
	{
		CanvasHolder *cholder = new CanvasHolder;
		objects_to_delete.push_back(cholder);

		for(unsigned int file_nr = 0; file_nr < versions.size(); ++file_nr){
			for(unsigned int i = 0; i < cutsets.size(); ++i){
				std::map<std::string,std::vector<std::string> > file_names;
				if(signal_background == "s" || signal_background == "c"){
				        //file_names["tprime_"+mass+versions[file_nr]+".root"].push_back(*histo_name+"Tprime"+mass+"|"+d_channel+"|"+cutsets[i]);
 					//file_names["tprime_"+mass+versions[file_nr]+".root"].push_back(*histo_name+"Tprime"+mass+"|"+e_mu+"_background|"+cutsets[i]);
					file_names["TTbar"+versions[file_nr]+".root"].push_back(*histo_name+"TTbar"+dataset_suffix[file_nr]+"|"+d_channel+"|"+cutsets[i]);
					file_names["TTbar"+versions[file_nr]+".root"].push_back(*histo_name+"TTbar"+dataset_suffix[file_nr]+"|"+e_mu+"_background|"+cutsets[i]);
				}if(signal_background == "b"|| signal_background == "c"){
					//file_names["ttbar"+versions[file_nr]+".root"].push_back(*histo_name+"TTbar|"+e_mu+"_background|"+cutsets[i]);
					file_names["Wjets"+versions[file_nr]+".root"].push_back(*histo_name+"Wjets|"+e_mu+"_background|"+cutsets[i]);
					file_names["Zjets"+versions[file_nr]+".root"].push_back(*histo_name+"Zjets|"+e_mu+"_background|"+cutsets[i]);
					file_names["TtChan"+versions[file_nr]+".root"].push_back(*histo_name+"TtChan|"+e_mu+"_background|"+cutsets[i]);
					file_names["TsChan"+versions[file_nr]+".root"].push_back(*histo_name+"TsChan|"+e_mu+"_background|"+cutsets[i]);
					file_names["TtWChan"+versions[file_nr]+".root"].push_back(*histo_name+"TtWChan|"+e_mu+"_background|"+cutsets[i]);
					if (e_mu == "mu")
						file_names["Mupt15"+versions[file_nr]+".root"].push_back(*histo_name+"Mupt15|"+e_mu+"_background|"+cutsets[i]);
					else if (e_mu =="e")
						file_names["qcdbcem"+versions[file_nr]+".root"].push_back(*histo_name+"QCDbctoe20to30|"+e_mu+"_background|"+cutsets[i]);
						
				}


				histo_merger[i]->set_integrated_luminosity(200);
				histo_merger[i]->set_cutset(cutsets[i]);
				histo_merger[i]->set_histo_name(*histo_name);
				histo_merger[i]->set_signal_file("TTbar"+versions[file_nr]+".root");
				histo_merger[i]->do_not_scale("QCDbctoe20to30");
				histo_merger[i]->set_file_names(file_names);

				TH1F *histo = histo_merger[i]->combine_histo<TH1F>();

				//const char * hname = histo->GetName();
				//histo = (TH1F *) histo->Rebin(2,hname);
				//std::cout << " NOTE: Rebin set to 2" << std::endl;

				if(normalise_histos)
					histo->Scale(1./histo->Integral());

				merged_histos.push_back(histo);

				std::string legend = dictionary[*histo_name]+identifier[i];
				if(file_idents.find(versions[file_nr]) != file_idents.end()){
					legend += file_idents[versions[file_nr]];
				}

				/********Add fitted line TF1. Line attributes set in canvasholder.h
 				double X_max = histo->GetXaxis()->GetXmax();
 				double mcut = 200;
 				TF1 *myfit = new TF1("myfit","[0]*[1]*exp((x-200)*-1*[1])*10", mcut, X_max);
 				std::cout << "histo integral "<<histo->Integral() << std::endl;
 				myfit->SetParameter(0,150.0);
 				myfit->SetParameter(1,0.01);
 				TH1F *histo_tmp = histo;
 				histo_tmp->Fit("myfit","N","",mcut,X_max);
				//std::cout << "No " <<  myfit->GetParameter(0) << " a "<<  myfit->GetParameter(1) << std::endl;
				********************************/
			       
				cholder->addHisto(histo,legend,"HISTE");
				//if (versions[file_nr] == "_jet")
				//	cholder->addTF1(myfit,"fitted function","");
					
				cholder->setCanvasTitle(*histo_name);
				std::string tmp_dataset = histo->GetTitle();
				//tmp_dataset += ": "+dictionary[dataset]+" ("+mass+" Gev)";
				cholder->setLegTitle("Events @ 200/pb");
				cholder->setTitle("");
				cholder->setLineColors(*colours); 
				cholder->setOptStat(00000);
				//	cholder->setTitleX("mass [GeV/c^{2}]");
				cholder->setTitleY("events/bin");
//				cholder->setBordersY(0.,0.7);

				file_names.clear();
			}
		}
	}

	for(std::vector<CanvasHolder*>::iterator ch = objects_to_delete.begin();
	    ch != objects_to_delete.end();
	    ++ch){
	    	outfile->cd();
	    	(*ch)->save("eps");
	    	(*ch)->write(outfile);
		delete *ch;
	}


	delete colours; 
	colours=NULL; 
	delete outfile;
	outfile = NULL;
}
