#include "TagProbePrinter.h"

using namespace RooFit;

TagProbePrinter::TagProbePrinter()
{
	overall_scale_factor = -1;
}

TagProbePrinter::~TagProbePrinter()
{

}

void TagProbePrinter::print()
{
	if(verbose) std::cout << "In the printer " << std::endl;
	if(print_eff_only_table || print_scale_factor){
		//Create tables for all input directories. in dir eg. pt_bins
		for(std::vector<std::string>::iterator in_dir = input_directories.begin(); in_dir != input_directories.end();
		    in_dir++){
			if(verbose) std::cout << "Reading in workspaces for: " << *in_dir << std::endl;
			read_in_workspaces(*in_dir, true);
			make_efficiency_tables();
		}
	}
	
	if(print_detailed_table){
		for(std::vector<std::string>::iterator file_it = input_files.begin(); file_it != input_files.end(); file_it++){
			
			//Search within all input_dirs under first_dir e.g. MuonEffs/overall/ for sub dirs containing results
			for(std::vector<std::string>::iterator in_dir = input_directories.begin(); in_dir != input_directories.end();
			    in_dir++){
				read_in_workspaces_per_bin(*file_it,*in_dir);
				if(verbose) std::cout << "Detailed table for file: " <<  *file_it << " input directory: " << *in_dir << std::endl;
				make_detailed_table();				
			}
		}
	}
}
void TagProbePrinter::make_detailed_table()
{

	//Get n bins from first dir
	std::vector<std::string> bin_names = get_var_bin_names((per_bin_workspace_map.begin())->first);
	

	std::cout << " \\begin{center} \n \\begin{tabular}{|";
	for(uint i = 0;i < bin_names.size();i++){
		std::cout << " r |";

	}
	std::cout << " c | c | c | c | c | c | c |} \n \\hline " << std::endl;
	for(uint i = 0;i < bin_names.size();i++){
		std::string name = bin_names[i]; 
		std::cout << name.substr(0,name.find("_")) << " & ";
	}
	std::cout << "Efficiency & N Sig Pass & N Sig Fail & N Bkg Pass & N Bkg Fail & Mass Fit Mean & Fit Sigma \\\\ \n \\hline" << std::endl;
	std::string already_printed = "none";
 	for(std::map<std::string, RooWorkspace * >::iterator wrk = per_bin_workspace_map.begin(); wrk != per_bin_workspace_map.end();
 	    wrk++){
		
		bin_names = get_var_bin_names(wrk->first);
		
		RooAbsData* ds  = wrk->second->data("data");
 		if(ds == NULL) std::cout << "ERROR: NULL dataset in make_detailed_table : data" << std::endl; 
		RooAbsPdf* pdf = wrk->second->pdf("simPdf");
		if(pdf == NULL){
			std::cout << " ERROR: NULL pdf for detailed table " << wrk->first << std::endl;
			continue;
		}
		RooArgSet* params = (RooArgSet*) pdf->getParameters(ds);
 		if(params == NULL ) std::cout << "NULL params " << std::endl;
 		RooRealVar* e = (RooRealVar*) params->find("efficiency");	
 		RooRealVar* nsig = (RooRealVar*) params->find("numSignalAll");
 		RooRealVar* nbkgp = (RooRealVar*) params->find("numBackgroundPass");
 		RooRealVar* nbkgf = (RooRealVar*) params->find("numBackgroundFail");
 		RooRealVar *mean = (RooRealVar*) params->find("mean");
 		RooRealVar *sigma = (RooRealVar*) params->find("sigma");

 		if(e == NULL ) std::cout << "NULL efficiency " << std::endl;	
 		if(nsig == NULL ) std::cout << "NULL nsig " << std::endl;
 		if(nbkgp == NULL ) std::cout << "NULL bkgp " << std::endl;
 		if(nbkgf == NULL ) std::cout << "NULL bkgf " << std::endl;
 		if(mean == NULL ) std::cout << "NULL mean " << std::endl;
 		if(sigma == NULL ) std::cout << "NULL sigma " << std::endl;
		
		if(bin_names.size() > 0){
			if(bin_names[0] != already_printed) 
				std::cout << getTitle(bin_names[0]) << " & ";
			else 
				std::cout << " & ";
			for(uint i = 1;i < bin_names.size();i++){
				std::cout << getTitle(bin_names[i]) << " & ";
			}
			already_printed = bin_names[0];
		}
			

		std::cout << setprecision(4);
 		std::cout <<  " & $ " << e->getVal() << " + " << e->getErrorHi() << " " << e->getErrorLo();
		std::cout << setprecision(5);
		std::cout << " $ & $ " <<  nsig->getVal()*e->getVal() <<  " $ & $ " << nsig->getVal()*(1-e->getVal());
		std::cout << " $ & $ " <<  nbkgp->getVal() <<" $ & $ " << nbkgf->getVal();
		std::cout << setprecision(4);
		std::cout << " $ & $ " << mean->getVal() << " \\pm " << mean->getError();
		std::cout << " $ & $ " << sigma->getVal() << " \\pm " << sigma->getError() << " $ \\\\" << std::endl;
 	}

	std::cout << "\\hline \n \\end{tabular} \n \\end{center} " << std::endl;
}

void TagProbePrinter::make_efficiency_tables()
{
	if(verbose) {std::cout << " Printer: Make efficiency tables. " << workspace_map.size() << std::endl;}

 	outfile->cd();
 	for(std::map<std::string,std::map<std::string,RooWorkspace *> >::iterator wrk_ids = workspace_map.begin();
 	    wrk_ids != workspace_map.end(); wrk_ids++){	
		if((wrk_ids->first).find("parent") == std::string::npos) 
			continue;
		if(verbose) std::cout << " processing efficiency plots for: " << wrk_ids->first << std::endl;

		//Extract binned var names from common name
		std::vector<std::string> binned_var_names = get_binned_var_names(wrk_ids->first);
		
		//Read in available vars from first file.
		//This is to check the var exists only. not necessary for plots, only to stop segfault
		RooAbsData* base_ds = NULL;
		if(wrk_ids->second.size() > 0){
			std::map<std::string, RooWorkspace* > tmp_map = wrk_ids->second;
			base_ds = tmp_map.begin()->second->data((efficiency_type+"_efficiency").c_str());
			if(base_ds == NULL)
				std::cout << "ERROR: NULL dataset: " << efficiency_type << "_efficiency" << std::endl; 
			
			const RooArgSet* vars = base_ds->get();
			for(std::vector<std::string>::iterator var_name = binned_var_names.begin(); var_name != binned_var_names.end();
			    var_name++){
 				if(vars->find((*var_name).c_str()) == NULL){
 					std::cout <<"ERROR: no RooRealVar with name: " << *var_name << " exiting efficiency plots " << std::endl;
 					return;
 				}
 				else
					std::cout <<"Found binned variable " << *var_name << std::endl;		
			}			
		}
			
		//Categorise binned variables into 1D,2D
		std::map<std::string,std::string> v2_name_map;
		for(std::vector<std::string>::iterator v1it = binned_var_names.begin(); v1it != binned_var_names.end(); v1it++){
			for(std::vector<std::string>::iterator v2it = binned_var_names.begin(); v2it != binned_var_names.end(); v2it++){
				if(*v2it == *v1it)continue;
				v2_name_map[*v1it] = *v2it;
				print_eff_table2D(wrk_ids->second,*v1it,*v2it, wrk_ids->first);
				if(binned_var_names.size() < 3 ) break;
			}
			if(v2_name_map.find(*v1it) == v2_name_map.end()) 
				print_eff_table1D(wrk_ids->second,*v1it,wrk_ids->first,"");
		       			
		}
 	}

}

void TagProbePrinter::print_eff_table1D(std::map<std::string,RooWorkspace* > workspaces, std::string v1_name, std::string c_id, std::string v2_name)
{
	int prec = 1000000; //precision of output

	std::map<std::string,std::vector<std::string> > table_numbers;
	std::map<std::string,std::vector<double> > scale_factor_numbers;

	std::vector<std::string> files;
	for(std::map<std::string,RooWorkspace *>::iterator wrk = workspaces.begin(); wrk != workspaces.end();wrk++){
		if(wrk->first.find("Data") != std::string::npos)
			files.push_back(wrk->first);
	}
	for(std::map<std::string,RooWorkspace *>::iterator wrk = workspaces.begin(); wrk != workspaces.end();wrk++){
		if(wrk->first.find("Data") == std::string::npos)
			files.push_back(wrk->first);
	}
	

	//map<filename,workspace>
	
	//for(std::map<std::string,RooWorkspace *>::iterator wrk = workspaces.begin(); wrk != workspaces.end();wrk++){
	
	for(std::vector<std::string>::iterator file = files.begin();file != files.end();file++){
	
		std::string filename = *file;
		//std::string filename = wrk->first;
		std::cout << efficiency_type << " efficiency for: " << c_id << " " << filename << " " << v1_name  << std::endl;
		
		RooDataSet* eff = (RooDataSet*) workspaces[*file]->data((efficiency_type+"_efficiency").c_str());
		const RooArgSet* set = eff->get();
		RooRealVar* v1 = (RooRealVar*) set->find((v1_name).c_str());

		//Get v1 bin cats, just to make it easier to identify output
		RooArgSet bin_catv1;
		bin_catv1.addClone( RooBinningCategory((v1_name+"_bins").c_str(), (v1_name+"_bins").c_str(), *v1));
		RooMultiCategory allCatsv1("allCatsv1", "allCatsv1", RooArgSet(bin_catv1));
		RooDataSet myEffv1(*eff);
		myEffv1.addColumn(allCatsv1);

		RooRealVar* e = (RooRealVar*) set->find("efficiency");
		//Loop here to get names of eta bins.should be inline with i but check this
		int i = 0;
		TIterator* catItv1 = allCatsv1.typeIterator();
		for(RooCatType* tv1 = (RooCatType*)catItv1->Next(); tv1!=0; tv1 = (RooCatType*)catItv1->Next() ){
			TString catNamev1 = tv1->GetName();
			if(catNamev1.Contains("NotMapped")) continue;
			catNamev1.ReplaceAll("{","").ReplaceAll("}","").ReplaceAll(";","_&_");
			
			eff->get(i);
			
			std::ostringstream e_val; e_val << fix_precision(e->getVal(),prec); std::string eff_val = e_val.str();
			std::ostringstream e_h; e_h << fix_precision(e->getErrorHi(),prec); std::string eff_h = e_h.str();
			std::ostringstream e_l; e_l << fix_precision(e->getErrorLo(),prec); std::string eff_l = e_l.str();
			
			std::string eff_str;
			if(filename.find("mc") == std::string::npos && filename.find("background") == std::string::npos)
				eff_str = eff_val + " + " + eff_h + " " + eff_l;
			else //Don't want errors for mc if printing the scalefactor
				eff_str = eff_val;
			
			table_numbers[(std::string)catNamev1].push_back(eff_str);

			//Assume first file is data
			if(print_scale_factor){
				if(workspaces.size() > 2){
					std::cout <<"ERROR: Asking for scale factor " << workspaces.size() << " input files. exiting " << std::endl;
					break;
				}
				if( (filename.find("data") != std::string::npos || filename.find("Data") != std::string::npos) ){
					scale_factor_numbers[(std::string)catNamev1].push_back(e->getVal());
					scale_factor_numbers[(std::string)catNamev1].push_back(e->getErrorHi());
					scale_factor_numbers[(std::string)catNamev1].push_back(e->getErrorLo());
				}
// 				else if( (filename.find("data") == std::string::npos && filename.find("Data") == std::string::npos )){
// 					std::cout << "Error: first file not data. Scale factors printed wrong way round" << std::endl;
// 				}
				else 
					scale_factor_numbers[(std::string)catNamev1].push_back(e->getVal());
				
			}
			
			i++;
		}
	}

	//Now print the table in latex format
	for(std::map<std::string,std::vector<std::string> >::iterator cat_v1 = table_numbers.begin(); cat_v1 != table_numbers.end(); cat_v1++){
		if(cat_v1 == table_numbers.begin()){
			std::vector<std::string> tmp_vec = cat_v1->second;
			print_table_start(tmp_vec,v1_name,"");
		}
		
		std::cout << getTitle(cat_v1->first) ;
		
		for(std::vector<std::string>::iterator neff = cat_v1->second.begin(); neff != cat_v1->second.end();neff++){
			
			std::cout << " & $ " << *neff << " $ "; 
		}
		if(print_scale_factor){
			std::vector<double> scale_factors = get_scale_factors(scale_factor_numbers[cat_v1->first]);
			if(getTitle(cat_v1->first) == "abseta_bin0") overall_scale_factor = *scale_factors.begin();
			for(std::vector<double>::iterator fac = scale_factors.begin();fac != scale_factors.end();fac++){
				std::cout << " & $ " << fix_precision(*fac,prec) << " $ ";

			}
		}

		std::cout << " \\\\ " << std::endl;
	}		
	
	std::cout << "\\hline \n \\end{tabular} \n \\end{center} " << std::endl;
	
}

void TagProbePrinter::print_eff_table2D(std::map<std::string,RooWorkspace* > workspaces, std::string v1_name, std::string v2_name, std::string c_id)
{

	int prec = 1000; //precision of output
	std::map<std::string,std::map<std::string,std::vector<std::string> > > table_numbers;
	std::map<std::string,std::map<std::string,std::vector<double> > > scale_factor_numbers;

	//Can't overlay 2D plots so everything done within loop. map<filename,workspace>
	for(std::map<std::string,RooWorkspace *>::iterator wrk = workspaces.begin(); wrk != workspaces.end();wrk++){
		
		std::string filename = wrk->first;
		std::cout << efficiency_type << " efficiency for: " << c_id << " " << filename << " " << v1_name << " " << v2_name << std::endl;
		
		RooDataSet* eff = (RooDataSet*) wrk->second->data((efficiency_type+"_efficiency").c_str());
		const RooArgSet* set = eff->get();
		RooRealVar* v1 = (RooRealVar*) set->find((v1_name).c_str());
		RooRealVar* v2 = (RooRealVar*) set->find((v2_name).c_str());

		//Get v2 bin cat. to loop over
		RooArgSet bin_cat1D;
		bin_cat1D.addClone( RooBinningCategory((v2_name+"_bins").c_str(), (v2_name+"_bins").c_str(), *v2));
		RooMultiCategory allCats1D("allCats1D", "allCats1D", RooArgSet(bin_cat1D));
		RooDataSet myEff(*eff);
		myEff.addColumn(allCats1D);

		//Get v1 bin cats, just to make it easier to identify output
		RooArgSet bin_catv1;
		bin_catv1.addClone( RooBinningCategory((v1_name+"_bins").c_str(), (v1_name+"_bins").c_str(), *v1));
		RooMultiCategory allCatsv1("allCatsv1", "allCatsv1", RooArgSet(bin_catv1));
		RooDataSet myEffv1(*eff);
		myEffv1.addColumn(allCatsv1);

		TIterator* catIt = allCats1D.typeIterator();
		for(RooCatType* t = (RooCatType*)catIt->Next(); t!=0; t = (RooCatType*)catIt->Next() ){
			TString catName = t->GetName();
			if(catName.Contains("NotMapped")) continue;
			catName.ReplaceAll("{","").ReplaceAll("}","").ReplaceAll(";","_&_");
			RooDataSet* eff_bin = (RooDataSet*) myEff.reduce( Cut(TString::Format("allCats1D==%d",t->getVal())) );
			const RooArgSet* set_bin = eff_bin->get();
			RooRealVar* e_bin = (RooRealVar*) set_bin->find("efficiency");
			//Loop here to get names of eta bins.should be inline with i but check this
			int i = 0;
			TIterator* catItv1 = allCatsv1.typeIterator();
			for(RooCatType* tv1 = (RooCatType*)catItv1->Next(); tv1!=0; tv1 = (RooCatType*)catItv1->Next() ){
				TString catNamev1 = tv1->GetName();
				if(catNamev1.Contains("NotMapped")) continue;
				catNamev1.ReplaceAll("{","").ReplaceAll("}","").ReplaceAll(";","_&_");

				eff_bin->get(i);

				std::ostringstream e_val; e_val << fix_precision(e_bin->getVal(),prec); std::string eff_val = e_val.str();
				std::ostringstream e_h; e_h << fix_precision(e_bin->getErrorHi(),prec); std::string eff_h = e_h.str();
				std::ostringstream e_l; e_l << fix_precision(e_bin->getErrorLo(),prec); std::string eff_l = e_l.str();

				std::string eff_str;
				if(filename.find("mc") == std::string::npos && filename.find("_background") == std::string::npos)
					eff_str = eff_val + " + " + eff_h + " " + eff_l;
				else //Don't want errors for mc if printing the scalefactor
					eff_str = eff_val;

				table_numbers[(std::string)catName][(std::string)catNamev1].push_back(eff_str);

				//Assume first file is data
				if(print_scale_factor){
					if(workspaces.size() > 2){
						std::cout <<"ERROR: Asking for scale factor " << workspaces.size() << " input files. exiting " << std::endl;
						break;
					}
					if( wrk == workspaces.begin() && (filename.find("data") != std::string::npos || filename.find("Data") != std::string::npos )){
						scale_factor_numbers[(std::string)catName][(std::string)catNamev1].push_back(e_bin->getVal());
						scale_factor_numbers[(std::string)catName][(std::string)catNamev1].push_back(e_bin->getErrorHi());
						scale_factor_numbers[(std::string)catName][(std::string)catNamev1].push_back(e_bin->getErrorLo());
					}
					else if( wrk == workspaces.begin() && (filename.find("data") == std::string::npos && filename.find("Data") == std::string::npos )){
						std::cout << "Error: first file not data. Scale factors printed wrong way round" << std::endl;
					}
					else
						scale_factor_numbers[(std::string)catName][(std::string)catNamev1].push_back(e_bin->getVal());

				}

				i++;
			}
		}
				
	}

	//Now print the table in latex format
	for(std::map<std::string,std::map<std::string,std::vector<std::string> > >::iterator cat_v2 = table_numbers.begin(); cat_v2 != table_numbers.end(); cat_v2++){
		if(cat_v2 == table_numbers.begin()){
			std::vector<std::string> tmp_vec = (cat_v2->second.begin())->second;
			print_table_start(tmp_vec,v1_name,v2_name);
		}
		
		std::cout << getTitle(cat_v2->first) ;
		
		for(std::map<std::string,std::vector<std::string> >::iterator cat_v1 = cat_v2->second.begin(); cat_v1 != cat_v2->second.end(); cat_v1++){			
			std::cout << " & " << getTitle(cat_v1->first);
			
			for(std::vector<std::string>::iterator neff = cat_v1->second.begin(); neff != cat_v1->second.end();neff++){
			
				std::cout << " & $ " << *neff <<" $ " ; 
			}
			if(print_scale_factor){
				std::vector<double> scale_factors = get_scale_factors(scale_factor_numbers[cat_v2->first][cat_v1->first]);
				for(std::vector<double>::iterator fac = scale_factors.begin();fac != scale_factors.end();fac++){
					std::cout << " & $ " << fix_precision(*fac,prec) << " $ "; 
					
				}
			}
			else
			std::cout << " $ ";
				
			std::cout << " \\\\ " << std::endl;
		}		
	}
	std::cout << "\\hline \n \\end{tabular} \n \\end{center} " << std::endl;
}

std::vector<double> TagProbePrinter::get_scale_factors(std::vector<double> effs)
{
	std::vector<double> factors;

	if(effs.size() != 4){
		std::cout << " ERROR: effs size != 4, size = " << effs.size() << std::endl;
		//	for(std::vector<double>::iterator fac = effs.begin();fac != effs.end();fac++)
		//	std::cout << *fac << std::endl;
		return factors;
	}

	double scale_fac = effs[0]/effs[3];
	double err_h = effs[1]/effs[3];
	double err_l = effs[2]/effs[3];

	factors.push_back(scale_fac);
	factors.push_back(err_h);
	factors.push_back(err_l);

	return factors;
}

void TagProbePrinter::print_table_start(std::vector<std::string> nfiles, std::string v1_name, std::string v2_name)
{
	if(v2_name != "") 
		std::cout << " \\begin{center} \n \\begin{tabular}{| r | r|";
	else 
		std::cout << " \\begin{center} \n \\begin{tabular}{| r |";
		
	for(uint i = 0; i < nfiles.size();i++)
		std::cout << " c | ";
	if(print_scale_factor)
		std::cout << " c |  c |  c | ";
	
	
	std::cout << "} \n \\hline " << std::endl;
	
	if(v2_name != "") 
		std::cout << v2_name << " region & " << v1_name << " region ";
	else 
		std::cout << v1_name << " region ";


	for(uint i = 0; i < nfiles.size();i++)
		std::cout << " & Efficiency ";
	if(print_scale_factor)
		std::cout << " & Scale Factor & Error + & Error - ";
	
	std::cout << " \\\\ \n \\hline" << std::endl;
	
}

double TagProbePrinter::fix_precision(double input, int prec)
{

	int step1 = input*prec;
	double step2 = (double) step1/prec;
	return step2;
}

double TagProbePrinter::get_scale_factor_value()
{
	std::cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
	std::cout<<overall_scale_factor<<"\n";
	std::cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
	return overall_scale_factor;
}
