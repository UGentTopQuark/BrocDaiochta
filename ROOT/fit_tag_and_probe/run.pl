#!/usr/bin/perl -w

use Getopt::Std;
#use strict;


getopts("c:");

$run_cfg = 'run.cfg';
die "can't find $run_cfg, exiting..." unless(-e $run_cfg);

do "$run_cfg";

print "Error define sections" and die if!(defined(@sections));
print "Error define passing ids" and die if!(defined(@passing_ids));
print "Error define datasets" and die if!(defined(@datasets));

$config = $opt_c if(defined($opt_c));
print "Error define config" and die if!(defined($config));
$config_dir = "./" if!(defined($config_dir));

print "Error $config_dir/$config does not exist" and die if(!-e $config_dir."/".$config);

$datadir = "./" if!(defined($datadir));
print "Error Datadir: $datadir does not exist" and die if(!-d $datadir); 
$output_dir = "./" if!(defined($output_dir));
print "Error Ourputdir: $output_dir does not exist" and die if(!-d $output_dir); 
$force_time = 0 if(!defined($force_time));
$force_ident = 0 if(!defined($force_ident));
$identifier = ask_for_ident($identifier) if(!defined($identifier) || $identifier eq "" || $force_ident);
$run_locally = 0 if(!defined($run_locally));




$dir_name = create_directory($identifier,$output_dir,$force_time);
#copy fit_tag_and_probe to output dir
$output_dir = $output_dir."/".$dir_name;

system("make -j2");
`cp fit_tag_and_probe $output_dir/`;

if($run_locally == 0){
    my $submission_scripts_ref = prepare_jobs($run_locally);
    submit_jobs($submission_scripts_ref);
}else{
    my $run_commands_ref = prepare_jobs($run_locally);
    my @run_commands = @$run_commands_ref;
    foreach my $run_cmd(@run_commands){
	print "$run_cmd\n";
	system($run_cmd);
    }

}


sub prepare_jobs
{
    my($run_locally) = @_;

    my @submission_scripts = ();
    my @run_commands = ();
    foreach my $dataset(@datasets){
	
	foreach my $passing_id(@passing_ids){
	    
	    my $i = 0;
	    foreach my $section(@sections){
		my $config_out = create_config($dataset,$passing_id,$i);
		
		if($run_locally == 0){
		    my $submission_script = create_submission_script($output_dir, $config_out, $section);
		    push(@submission_scripts,$submission_script);
		}else{
		    $config_out =~ m/.*\/(.*)\.txt/g;
		    my $ident = $1;
		    my $run_cmd = "$output_dir/fit_tag_and_probe $config_out $section 2>&1 | tee $output_dir/${ident}_summary.txt";
#		    print $run_cmd;
#		    die;
		    push(@run_commands,$run_cmd);

		}
		$i++;
	    }
	}
	
    }
    if($run_locally == 0){
	return \@submission_scripts;
    }else{
	return \@run_commands;
    }
}

sub submit_jobs
{
    my($submission_scripts_ref) = @_;
    my @submission_scripts = @$submission_scripts_ref;

    foreach my $submission_script(@submission_scripts){
	my $command = "qsub -o $output_dir -e $output_dir -q localgrid\@cream01 $submission_script";
	print "$command\n";
	my $jobid = `$command`;
	print $jobid;
    }

}
sub run_local_jobs
{
    my($submission_scripts_ref) = @_;
    my @submission_scripts = @$submission_scripts_ref;

    foreach my $submission_script(@submission_scripts){
	my $command = "qsub -o $output_dir -e $output_dir -q localgrid\@cream01 $submission_script";
	print "$command\n";
	my $jobid = `$command`;
	print $jobid;
    }

}

sub create_config
{
    my($dataset,$passing_id,$postfix) = @_;

    my $id = $dataset."_".$passing_id."_".$postfix;
    my $config_out = $output_dir."/".$id.".txt";
    `cp $config_dir/$config $output_dir/$config` if!(-e $output_dir."/".$config);
    
    open(FILE, "<$config_dir/$config") or die "cannot open $config_dir/$config: $!";;    
    #print "opening $config_dir/$config \n";
    
    @write_lines = ();
    my $found_global = 0;
    my $filled_vars = 0;
    while(my $line = <FILE>){
	next if($line =~ m/^\s*dataset/ || $line =~ m/^\s*output_file/ || $line =~ m/^\s*passing_id/ || $line =~ m/^\s*datadir/);
	$line =~ s/(\#.*)//g;
	if($line =~ m/\s*[global].*/){
	    $found_global = 1; 
	    push(@write_lines,$line);
	    next;
	}

	if($found_global == 1 and $filled_vars == 0){
	    push(@write_lines,"dataset = $dataset \n");
	    push(@write_lines,"output_file = $output_dir/$id.root \n");
	    push(@write_lines,"passing_id = $passing_id \n");
	    push(@write_lines,"datadir = $datadir \n");
	    $filled_vars = 1;
	}
	push(@write_lines,$line);
    }
    close FILE;

    if($found_global == 0){
	    push(@write_lines,"[global]");
	    push(@write_lines,"dataset = $dataset \n");
	    push(@write_lines,"output_file = $output_dir/$id.root \n");
	    push(@write_lines,"passing_id = $passing_id \n");
	    push(@write_lines,"datadir = $datadir \n");
    }
    #now write to output file
    print "Error: $config_out already exists" if(-e $config_out);
    open(FILE, ">$config_out");
    print FILE @write_lines;
    close FILE;

    return $config_out;
}

sub create_submission_script
{
	my($output_dir, $config_name, $section) = @_;
	#print "$config_name \n";
	$config_name =~ m/.*\/(.*)\.txt/g;
#	$config_name =~ m/(.*)\.txt/g;
	my $ident = $1;
	my @output = `pwd`;
	my $here = pop @output;
	chomp $here;
	my $outfile = $output_dir."/sub_$ident.sh";
	open(SUB, ">$outfile") or die "cannot open $outfile: $!";
	print SUB <<EOF;
#!/bin/bash
cd $here
$output_dir/fit_tag_and_probe $config_name $section | tee $output_dir/${ident}_summary.txt
EOF
	close(SUB);
	`chmod 755 $outfile`;
	return $outfile;
}

sub create_directory
{
	my($identifier, $output_dir, $force_time) = @_;

	my $date = sprintf("%04d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3]);
	my $dir_name = $date."_0_$identifier";
	my $dir_to_check = "$output_dir/$dir_name";
	if(-d "$dir_to_check" or $force_time){
		$date = sprintf("%04d%02d%02d_%02d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3], (localtime)[2], (localtime)[1], (localtime)[0]);
		$dir_name = $date."_$identifier";
	}

	system("mkdir -p $output_dir/$dir_name");

	return $dir_name;
}

sub ask_for_ident
{
	my($default_ident) = @_;

	print "Please enter ident for run [$default_ident]:\n";
	my $ident = <STDIN>;

	chomp $ident;

	$ident = $default_ident if($ident !~ /[\d\w]+/);

	return $ident;
}
