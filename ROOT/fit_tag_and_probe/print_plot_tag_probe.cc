#include <sstream>
#include "TFile.h"
#include <iostream>
#include <map>
#include <vector>

#include "TagProbePlotter.h"
#include "TagProbePrinter.h"
#include "ConfigReader/ConfigReader.h"

int main(int argc, char **argv)
{
	if(argv[1]==NULL){
		std::cerr << "usage: ./print_plot_tag_probe <config file>" << std::endl;
		exit(1);
	}

	std::string config = argv[1]; 
	std::string section = "global";
	std::string process_cmd = "";
	eire::ConfigReader *config_reader = new eire::ConfigReader();
	config_reader->read_config_from_file(config);
	std::vector<std::string> fit_output_files;
	std::vector<std::string> overlay = config_reader->get_vec_var("overlay","global");

	for(std::vector<std::string>::iterator variable = overlay.begin(); variable != overlay.end(); ++variable){
		std::string inputdir = config_reader->get_var("inputdir",*variable);
		std::vector<std::string> rootfiles = config_reader->get_vec_var("rootfiles",*variable);
		for (std::vector<std::string>::iterator i =rootfiles.begin(); i != rootfiles.end();  i++){
			fit_output_files.push_back(inputdir+*i);}
	}
	


       	std::string efficiency_type = config_reader->get_var("efficiency_type",section);
	//Output files will be input to tagprobeplotter
	bool print_out = config_reader->get_bool_var("print_out",section);
	bool plot_out = config_reader->get_bool_var("plot_out",section);

	
	/****************** Tag Probe Plotter **********/

	//Directories you want to read into the Plotter
	std::vector<std::string> fit_dirs = config_reader->get_vec_var("fit_dirs",section);
	
	std::map<std::string,int> rebin;
	rebin["mass"] = 2;

	std::vector<int> colours;
	colours.push_back(kBlack);
	colours.push_back(kRed);
	colours.push_back(kBlue);
	

	std::map<std::string,std::vector<int> > cat_colours;
	cat_colours["All"].push_back(kBlue);
	cat_colours["Pass"].push_back(kGreen);
	cat_colours["Fail"].push_back(kRed);
	cat_colours["abseta"] = colours;
	cat_colours["pt"] = colours;
	cat_colours["eta"] = colours;
	cat_colours["phi"] = colours;
	cat_colours["relIso"] = colours;
	cat_colours["njets"] = colours;

	//Add colours according to legend entries when available
	std::map<std::string,int> leg_colours;
	leg_colours["HLT Efficiency"] = kBlack;
	leg_colours["L1 Efficiency"] = kGreen+2;
	leg_colours["HLT/L1 Efficiency"] = kBlue-7;
	leg_colours["HLT Mu9 2010B"] = kBlue+1;
	leg_colours["HLT Mu9 2010A"] = kCyan+2;
	leg_colours["HLT Mu15 2010B"] = kBlack;
	leg_colours["Barrel"] = kMagenta+3;
	leg_colours["Endcap"] = 46;
	leg_colours["Overlap"] = 39;
	leg_colours["Data"] = kBlack;
	leg_colours["Z + Jets"] = kRed;
	leg_colours["Z#rightarrow#mu^{+}#mu^{-}"] = kRed;
	leg_colours["HLT Mu9 MC"] = kRed;
	leg_colours["DY#rightarrowl^{+}l^{-} D6T Madgraph"] = kRed;
	leg_colours["Z#rightarrow#mu^{+}#mu^{-} Z2 Pythia"] = kBlue;
	leg_colours["DY#rightarrow#mu^{+}#mu^{-} Z2 Powheg"] = kGreen;
	leg_colours["DY#rightarrow#mu^{+}#mu^{-} Z2 Pythia"] = kOrange;
	leg_colours["DY#rightarrowl^{+}l^{-}"] = kRed;
	leg_colours["Z#rightarrow#mu^{+}#mu^{-}"] = kRed;
	leg_colours["DY#rightarrow#mu^{+}#mu^{-}"] = kRed;
	leg_colours["DY#rightarrow#mu^{+}#mu^{-}"] = kRed;
	leg_colours["Z/#gamma*#rightarrowl^{+}l^{-}"] = kRed;

	bool print_detailed_table = config_reader->get_bool_var("print_detailed_table",section,true);
	bool print_eff_only_table = config_reader->get_bool_var("print_eff_only_table",section,true);
	bool print_scale_factor = config_reader->get_bool_var("print_scale_factor",section,true);
	bool plot_scale_factor = config_reader->get_bool_var("plot_scale_factor",section,true);
	double overall_scale_factor = -1;

	TagProbePrinter *tp_print = new TagProbePrinter();
	tp_print->set_input_files(fit_output_files);
	tp_print->set_process_cmd(process_cmd);
	tp_print->set_input_directories(fit_dirs);
	tp_print->set_efficiency_type(efficiency_type);
	tp_print->set_print_detailed_table(print_detailed_table);
	tp_print->set_print_eff_only_table(print_eff_only_table);
	tp_print->set_print_scale_factor(print_scale_factor);
	if(print_out) tp_print->print();
	if(plot_scale_factor && plot_out) overall_scale_factor = tp_print->get_scale_factor_value();

	TagProbePlotter *tp_plot = new TagProbePlotter();
	tp_plot->set_input_files(fit_output_files);
	tp_plot->set_process_cmd(process_cmd);
	tp_plot->set_input_directories(fit_dirs);
	tp_plot->set_rebin(rebin);
	tp_plot->set_category_colours(cat_colours);
	tp_plot->set_legend_colours(leg_colours);
	tp_plot->set_efficiency_type(efficiency_type);
	tp_plot->set_print_scale_factor(print_scale_factor);
	if(plot_scale_factor && plot_out) tp_plot->scale_factor_value = overall_scale_factor;
	if(plot_out) tp_plot->plot();

	std::cout << "END PROGRAM" << std::endl;


	delete tp_print;
	tp_print = NULL;
	delete tp_plot;
	tp_plot = NULL;

}
