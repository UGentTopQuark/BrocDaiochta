#ifndef RESULTSMANAGER_H
#define RESULTSMANAGER_H

#include <string>
#include <sstream>
#include <iostream>
#include "TROOT.h"
#include "TFile.h"
#include "TPad.h"
#include "TText.h"
#include "TKey.h"
#include "TCanvas.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TKey.h"
#include "RooWorkspace.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooRealVar.h"
#include "RooFormulaVar.h"
#include "RooAddPdf.h"
#include "RooGlobalFunc.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooFitResult.h"
#include "RooBinning.h"
#include "RooBinningCategory.h"
#include "RooMultiCategory.h"
#include "RooMappedCategory.h"
#include "RooThresholdCategory.h"
#include "Roo1DTable.h"
#include "RooMinuit.h"
#include "RooNLLVar.h"
#include "RooAbsDataStore.h"
#include "RooEfficiency.h"
#include "RooGaussian.h"
#include "RooChebychev.h"
#include "RooProdPdf.h"
#include "RooGenericPdf.h"
#include "RooExtendPdf.h"
#include "RooTrace.h"
#include "RooMsgService.h"
#include "Math/QuantFuncMathCore.h"
#include "CanvasHolder/Canvas_Holder.h"
#include "TGraphAsymmErrors.h"


class ResultsManager{
        public:
	        ResultsManager();
		~ResultsManager();
		void set_input_files(std::vector<std::string> input_files);
		void set_process_cmd(std::string process_cmd);
		void set_input_directories(std::vector<std::string> input_dirs);
		void set_category_colours(std::map<std::string,std::vector<int> > cat_colours);
		void set_legend_colours(std::map<std::string,int> leg_colours);
		void set_efficiency_type(std::string eff_type);

		void set_rebin(std::map<std::string,int> rebin);

		void set_print_detailed_table(bool print = true);
		void set_print_eff_only_table(bool print = true);
		void set_print_scale_factor(bool print= true);


        protected:
		void read_in_workspaces(std::string in_dir,bool parent_only = false);
		void read_in_workspaces_per_bin(std::string filename, std::string in_dir);
		std::vector<std::string> get_binned_var_names(std::string dir_name);
		std::vector<std::string> get_var_bin_names(std::string dir_name);

                std::string getTitle(std::string id);
		
		std::vector<std::string> input_files;
		std::vector<std::string> input_directories;

		std::map<std::string,CanvasHolder* > cholders;
		std::map<std::string,std::map<std::string, RooWorkspace* > > workspace_map;
		std::map<std::string, RooWorkspace* > per_bin_workspace_map;
		std::map<std::string,int> rebin;
		std::map<std::string,int> legend_colours;
		std::map<std::string,std::vector<int> > category_colours;
		std::string process_cmd;
		std::string first_directory;
		std::string efficiency_type;
		bool print_detailed_table;
		bool print_eff_only_table;
		bool print_scale_factor;

		TFile *outfile;

		static const bool verbose = true;
};
#endif
