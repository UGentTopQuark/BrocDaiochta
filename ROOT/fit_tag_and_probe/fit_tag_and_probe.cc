#include <sstream>
#include "TFile.h"
#include <iostream>
#include <map>
#include <vector>

#include "TagProbeFitter.h"
#include "TagProbePlotter.h"
#include "TagProbePrinter.h"
#include "ConfigReader/ConfigReader.h"

int main(int argc, char **argv)
{

	//std::string config = "configs/20101130_muon_test.txt";
	std::string config = argv[1]; 
	std::string section = "global";
	if(argc > 1) section = argv[2]; 

	eire::ConfigReader *config_reader = new eire::ConfigReader();
	config_reader->read_config_from_file(config);

	bool verbose = true;
	
	std::string dataset = config_reader->get_var("dataset",section);
	std::string cutset1 = config_reader->get_var("cutset1",section);
	std::string cutset2 = config_reader->get_var("cutset2",section,false);
	std::string datadir = config_reader->get_var("datadir",section);
 	std::string fit_name = config_reader->get_var("fit_name",section);
 		
	if(datadir == "") datadir = ".";

	std::vector<std::string> input_files;
	input_files.push_back(datadir+"/trigger_tag_and_probe_"+dataset+"_"+cutset1+".root");
	if(cutset2 != "")
		input_files.push_back(datadir+"/trigger_tag_and_probe_"+dataset+"_"+cutset2+".root");

	//passing_idx = hlt path eff.others = passing_l1, passing_hltwrtl1
	std::string passing_id = config_reader->get_var("passing_id",section); 

	//Output files will be input to tagprobeplotter
	std::string output_file = config_reader->get_var("output_file",section); 

	std::string input_directory = "lepEffs";
	std::string input_tree = "fitter_tree;1";
	int nCPU = 3;
	bool save_workspace = true;
	bool float_shape_parameters = true;
	std::vector<std::string> fix_vars; 
	fix_vars.push_back("mean");
	
	//Set start points for fitting
	std::vector<std::string> *pdf_commands = new std::vector<std::string>();
  	//pdf_commands->push_back("Chebychev::backgroundPass(mass, {cPass[0,-1,1], cPass2[0,-1,1]})");
  	//pdf_commands->push_back("Chebychev::backgroundFail(mass, {cFail[0,-1,1], cFail2[0,-1,1]})");	
	//pdf_commands->push_back("Voigtian::signal(mass, mean[90, 80, 100], width[2.495], sigma[3, 1, 20])");
   	pdf_commands->push_back("RooExponential::backgroundPass(mass, cPass[-10,-50,0])");
   	pdf_commands->push_back("RooExponential::backgroundFail(mass, cFail[-10,-20,0])");
 	pdf_commands->push_back("RooVoigtian::signal(mass, mean[91.2, 80.0, 100.0], width[10.3, 0.0, 20.0], sigma[3.0, 0.0, 10.0])");
 	pdf_commands->push_back("efficiency[0.9,0,1]");
 	pdf_commands->push_back("signalFractionInPassing[0.9]");

	//Name of directory containing output of fit(s)
	//NB: must put _bins after binned variable names
// 	std::string eff_1 = "abseta_bins1";
// 	std::string eff_2 = "pt_abseta_bins";
// 	std::string eff_3 = "pt_bins";
// 	std::string eff_4 = "eta_bins";
// 	std::string eff_5 = "relIso_bins";
// 	std::string eff_6 = "njets_bins";
// 	std::string eff_7 = "eta_phi_bins";
// 	std::string eff_8 = "phi_bins";
// 	std::string eff_9 = "mindR_bins";
// 	std::string eff_10 = "npvertices_bins";

	//Variables to be plotted. However the fit should not be binned into different regions of this var
	std::vector<std::string> *unbinned_vars = new std::vector<std::string>();
	unbinned_vars->push_back("mass");
	unbinned_vars->push_back("event_weight");
	//temp for Zmumu
	//unbinned_vars->push_back("pt");

	std::string fine_binned_var = config_reader->get_var("fine_binned_var",section,false);
	/*********** Edit parameterisation here ***************/
	//Vars to be plotted. Also the fit should be split into bins given for these vars
	std::vector<double> pt_v;
	if(fine_binned_var == "pt"){
		pt_v.push_back(5.0);
		pt_v.push_back(9.0);
		pt_v.push_back(12.0);
		pt_v.push_back(15.0);
	}
 	pt_v.push_back(20.0);
 	pt_v.push_back(25.0);
 	pt_v.push_back(30.0);
 	pt_v.push_back(35.0);
 	pt_v.push_back(40.0);
 	pt_v.push_back(45.0);
 	pt_v.push_back(50.0);
 	pt_v.push_back(55.0);
 	pt_v.push_back(60.0);
	pt_v.push_back(80.0);
	pt_v.push_back(100.0);
	pt_v.push_back(150.0);
	std::vector<double> pt_v2;
 	pt_v2.push_back(20.0);
	pt_v2.push_back(200.0);
	std::vector<double> abseta_v;
	if(fine_binned_var == "abseta"){
		abseta_v.push_back(0.0);
		abseta_v.push_back(1.2);
		abseta_v.push_back(2.1);
	}
	else{//One bin only for overall efficiency
		abseta_v.push_back(0);
		abseta_v.push_back(2.1);
	}

	std::vector<std::string> eta_binning = config_reader->get_vec_var("eta_binning",section,true);
	std::vector<double> eta_v;
	for(std::vector<std::string>::iterator bin_str = eta_binning.begin(); 
	    bin_str != eta_binning.end();bin_str++){
		eta_v.push_back(atof(bin_str->c_str()));
	}

	std::vector<std::string> pfreliso_binning = config_reader->get_vec_var("pfreliso_binning",section,true);
	std::vector<double> pfreliso_v;
	for(std::vector<std::string>::iterator bin_str = pfreliso_binning.begin(); 
	    bin_str != pfreliso_binning.end();bin_str++){
		pfreliso_v.push_back(atof(bin_str->c_str()));
	}
					    

// 	std::vector<double> eta_v;
// 	eta_v.push_back(-2.1);
// 	eta_v.push_back(-1.2);
// 	eta_v.push_back(-0.9);
// 	eta_v.push_back(0.0);
// 	eta_v.push_back(0.9);
// 	eta_v.push_back(1.2);
//  	eta_v.push_back(2.1);
	// eta_v.push_back(-2.5);
	// eta_v.push_back(-2.0);
	// eta_v.push_back(-1.5);
	// eta_v.push_back(-1.0);
	// eta_v.push_back(-0.5);
	// eta_v.push_back(0.0);
	// eta_v.push_back(0.5);
	// eta_v.push_back(1.0);
 	// eta_v.push_back(1.5);
	// eta_v.push_back(2.0);
 	// eta_v.push_back(2.5);
	std::vector<double> phi_v;
	if(fine_binned_var == "phi"){
		phi_v.push_back(-3.3);
		phi_v.push_back(-2.5);
		phi_v.push_back(-2.);
		phi_v.push_back(-1.5);
		phi_v.push_back(-1.);
		phi_v.push_back(-0.5);
		phi_v.push_back(0);
		phi_v.push_back(0.5);
		phi_v.push_back(1);
		phi_v.push_back(1.5);
		phi_v.push_back(2.);
		phi_v.push_back(2.5);
		phi_v.push_back(3.3);
	}
	else{
		phi_v.push_back(-3.3);
		phi_v.push_back(-2.);
		phi_v.push_back(-1);
		phi_v.push_back(0.);
		phi_v.push_back(1.);
		phi_v.push_back(2.);
		phi_v.push_back(3.3);
	}
	std::vector<double> relIso_v;
	relIso_v.push_back(0.);
	relIso_v.push_back(0.05);
	relIso_v.push_back(0.1);
	relIso_v.push_back(0.2);
	relIso_v.push_back(0.4);
	relIso_v.push_back(1.);
	std::vector<double> njets_v;
	njets_v.push_back(0.);
	njets_v.push_back(1.);
	njets_v.push_back(2.);
	njets_v.push_back(3.);
	njets_v.push_back(4.);
	njets_v.push_back(8.);
	std::vector<double> mindR_v;
	mindR_v.push_back(0.);
	mindR_v.push_back(0.3);
	mindR_v.push_back(1.0);
	mindR_v.push_back(2.0);
	mindR_v.push_back(3.0);
	mindR_v.push_back(4.0);
	mindR_v.push_back(5.0);
	std::vector<double> npvertices_v;
	npvertices_v.push_back(1.0);
	npvertices_v.push_back(2.0);
	npvertices_v.push_back(3.0);
	npvertices_v.push_back(4.0);
	npvertices_v.push_back(5.0);
	npvertices_v.push_back(6.0);
	npvertices_v.push_back(7.0);
	npvertices_v.push_back(8.0);
	npvertices_v.push_back(9.0);
	npvertices_v.push_back(10.0);
	npvertices_v.push_back(11.0);
	npvertices_v.push_back(12.0);
	npvertices_v.push_back(13.0);
	npvertices_v.push_back(14.0);
	npvertices_v.push_back(15.0);
	npvertices_v.push_back(16.0);
	npvertices_v.push_back(17.0);
	npvertices_v.push_back(18.0);
	npvertices_v.push_back(19.0);
	npvertices_v.push_back(20.0);


	std::map<std::string, std::vector<double> > available_binned_reals;
	available_binned_reals["abseta"] = abseta_v;
	available_binned_reals["pt"] = pt_v;
	available_binned_reals["eta"] = eta_v;
 	available_binned_reals["phi"] = phi_v;
 	available_binned_reals["relIso"] = relIso_v;
 	available_binned_reals["njets"] = njets_v;
 	available_binned_reals["mindR"] = mindR_v;
 	available_binned_reals["npvertices"] = npvertices_v;
	available_binned_reals["pfreliso"] = pfreliso_v;


	int nbinned_reals = atoi(config_reader->get_var("nbinned_reals",section).c_str());
	std::string binned_real1 = config_reader->get_var("binned_real1",section);
	std::string binned_real2 = config_reader->get_var("binned_real2",section, false);
	std::map<std::string, std::vector<double> > binned_reals;
	if(available_binned_reals.find(binned_real1) != available_binned_reals.end())
		binned_reals[binned_real1] = available_binned_reals[binned_real1];
	else{
		std::cerr << "ERROR: binned real " + binned_real1 + " not available. Exiting";
		exit(1);
	}
	if(nbinned_reals == 2){
		if(available_binned_reals.find(binned_real2) != available_binned_reals.end())
			binned_reals[binned_real2] = available_binned_reals[binned_real2];
		else{
			std::cerr << "ERROR: binned real " + binned_real2 + " not available. Exiting";
			exit(1);
		}
	}
	


	std::map<std::string, std::vector<std::string> > binned_categories;

	std::vector<std::string> *bin_to_PDF_map = new std::vector<std::string>();
	bin_to_PDF_map->push_back("gaussPlusLinear");

	//Set these parameters again for MC fit
	//Use same vars and bin2pdf map as fit for data so don't need to redefine

	TagProbeFitter *tp_fit = NULL; 
	if(verbose) std::cout << "Initialising fitter " << std::endl; 
	tp_fit = new TagProbeFitter(input_files,input_directory,input_tree,output_file,nCPU,save_workspace,float_shape_parameters,fix_vars);
	if(verbose) std::cout << "Done Initialising fitter " << std::endl; 

	if(verbose) std::cout << "Adding variables " << std::endl; 
	//Add relevant variables and catagories 
	bool added = true;
	added = added && tp_fit->addVariable("mass","Tag-Probe Mass",60,120, "GeV/c^{2}");
	added = added && tp_fit->addVariable("pt","Probe p_{T}", 0, 100, "GeV/c");
	added = added && tp_fit->addVariable("abseta","Probe #eta", 0, 2.1, "");
	added = added && tp_fit->addVariable("eta","Probe #eta", -2.4, 2.4, "");
	added = added && tp_fit->addVariable("phi","Probe #phi", -3.2, 3.2, "");
	added = added && tp_fit->addVariable("njets","Jet Multiplicity", -0.5, 8.5, "");
	added = added && tp_fit->addVariable("relIso","Probe Relative Isolation", 0, 1, "");
	added = added && tp_fit->addVariable("mindR","dR(Probe,Jet)", 0, 5, "");
	added = added && tp_fit->addVariable("npvertices","Number of Primary Vertices", 0, 9, "");
	added = added && tp_fit->addVariable("pfreliso","PFrelIso", 0, 0.5, "");
	added = added && tp_fit->addVariable("event_weight","Event weight", -1000, 1000, "");
	if(!added)
		std::cerr << "Variables not added properly" << std::endl;
	
	added = true;
	added = added && tp_fit->addCategory(passing_id,"isPassing", "dummy[pass=1,fail=0]");
	if(!added)
		std::cerr << "Catagories not added properly" << std::endl;
	
	//Add parameters needed for fitting
	tp_fit->addPdf("gaussPlusLinear",*pdf_commands);

	tp_fit->setWeightVar("event_weight");
	
	

	/************** Perform Fit *****************/
	std::string a_str = "-1"; //pointless string for output
	std::cout << "Performing " << fit_name << " fit for " << dataset << std::endl;
	a_str = tp_fit->calculateEfficiency(fit_name,passing_id,"pass",*unbinned_vars,binned_reals,binned_categories,*bin_to_PDF_map);

	delete tp_fit;
	tp_fit = NULL;
	delete pdf_commands;	
	pdf_commands = NULL;
	delete unbinned_vars;
	unbinned_vars = NULL;
	delete bin_to_PDF_map;
	bin_to_PDF_map = NULL;

	std::cout << "DONE " << std::endl;
	/****************** Tag Probe Plotter **********/

// 	//Directories you want to read into the Plotter
// 	std::vector<std::string> fit_dirs;
// 	fit_dirs.push_back(eff_dir);

// 	std::map<std::string,int> rebin;
// 	rebin["mass"] = 2;
// 	//rebin["abseta"] = 4;
// 	rebin["pt"] = 4;

// 	std::vector<int> colours;
// 	colours.push_back(kBlack);
// 	colours.push_back(kRed);
// 	colours.push_back(kBlue);
	

// 	std::map<std::string,std::vector<int> > cat_colours;
// 	cat_colours["All"].push_back(kBlue);
// 	cat_colours["Pass"].push_back(kGreen);
// 	cat_colours["Fail"].push_back(kRed);
// 	cat_colours["abseta"] = colours;
// 	cat_colours["pt"] = colours;

// 	TagProbePlotter *tp_plot = new TagProbePlotter();
// 	tp_plot->set_input_files(fit_output_files);
// 	tp_plot->set_process_cmd(process_cmd);
// 	tp_plot->set_input_directories(fit_dirs);
// 	tp_plot->set_rebin(rebin);
// 	tp_plot->set_category_colours(cat_colours);
// 	tp_plot->set_efficiency_type("fit");
// 	//tp_plot->plot();

// 	bool print_detailed_table = false;
// 	bool print_eff_only_table = true;
// 	bool print_scale_factor = true;


// 	TagProbePrinter *tp_print = new TagProbePrinter();
// 	tp_print->set_input_files(fit_output_files);
// 	tp_print->set_process_cmd(process_cmd);
// 	tp_print->set_input_directories(fit_dirs);
// 	tp_print->set_efficiency_type("fit");
// 	tp_print->set_print_detailed_table(print_detailed_table);
// 	tp_print->set_print_eff_only_table(print_eff_only_table);
// 	tp_print->set_print_scale_factor(print_scale_factor);
// 	//tp_print->print();


// 	std::cout << "END PROGRAM" << std::endl;

// 	delete tp_print;
// 	tp_print = NULL;
// 	delete tp_plot;
// 	tp_plot = NULL;


}
