#include "TagProbePlotter.h"

using namespace RooFit;

TagProbePlotter::TagProbePlotter()
{
	plot_1Defficiency_breakdown =false;
	scale_factor_value = -1;
}

TagProbePlotter::~TagProbePlotter()
{

}

void TagProbePlotter::plot()
{
	if(verbose) std::cout << "In the plotter " << std::endl;

	//Create plots for all input directories. in dir eg. pt_bins
	for(std::vector<std::string>::iterator in_dir = input_directories.begin(); in_dir != input_directories.end();
	    in_dir++){

		read_in_workspaces(*in_dir, false);
		//plot_fit_results();
		//plot_distributions();
		make_efficiency_plots();
	}
}

void TagProbePlotter::make_efficiency_plots()
{
 	outfile->cd();
 	for(std::map<std::string,std::map<std::string,RooWorkspace *> >::iterator wrk_ids = workspace_map.begin();
 	    wrk_ids != workspace_map.end(); wrk_ids++){	
		if((wrk_ids->first).find("parent") == std::string::npos)
			continue;
		
		if(verbose) std::cout << " processing efficiency plots for: " << wrk_ids->first << std::endl;		

		//Extract binned var names from common name
		std::vector<std::string> binned_var_names = get_binned_var_names(wrk_ids->first);
		
		//Read in available vars from first file.
		//This is to check the var exists only. not necessary for plots, only to stop segfault
		RooAbsData* base_ds = NULL;
		if(wrk_ids->second.size() > 0){
			std::map<std::string, RooWorkspace* > tmp_map = wrk_ids->second;
			base_ds = tmp_map.begin()->second->data((efficiency_type+"_efficiency").c_str());
			if(base_ds == NULL)
				std::cout << "ERROR: NULL dataset: " << efficiency_type << "_efficiency" << std::endl; 
			
			const RooArgSet* vars = base_ds->get();
			for(std::vector<std::string>::iterator var_name = binned_var_names.begin(); var_name != binned_var_names.end();
			    var_name++){
 				if(vars->find((*var_name).c_str()) == NULL){
 					std::cout <<"ERROR: no RooRealVar with name: " << *var_name << " exiting efficiency plots " << std::endl;
 					return;
 				}
 				else
					std::cout <<"Found binned variable " << *var_name << std::endl;		
			}			
		}
			
		//Categorise binned variables into 1D,2D
		std::map<std::string,std::string> v2_name_map;
		for(std::vector<std::string>::iterator v1it = binned_var_names.begin(); v1it != binned_var_names.end(); v1it++){
			for(std::vector<std::string>::iterator v2it = binned_var_names.begin(); v2it != binned_var_names.end(); v2it++){
				if(*v2it == *v1it)continue;
				v2_name_map[*v1it] = *v2it;
				plot_efficiency2D(wrk_ids->second,*v1it,*v2it, wrk_ids->first);
			}
 			if(v2_name_map.find(*v1it) != v2_name_map.end() && plot_1Defficiency_breakdown) 
 				plot_efficiency1D(wrk_ids->second,*v1it,wrk_ids->first,v2_name_map[*v1it]);
 			else 
 				plot_efficiency1D(wrk_ids->second,*v1it,wrk_ids->first,"");
		       			
		}
 	}

}

void TagProbePlotter::plot_efficiency1D(std::map<std::string,RooWorkspace* > workspaces, std::string var_name, std::string c_id, std::string v2_name)
{
	std::map<std::string,std::map<std::string,RooDataSet* > > file_cat_set;
	std::map<std::string,std::map<std::string,RooDataSet* > > cat_file_set;
	//std::map<std::string,std::map<std::string,RooDataSet* > > file_efftype_set;
	
	for(std::map<std::string,RooWorkspace *>::iterator wrk = workspaces.begin(); wrk != workspaces.end();wrk++){
		
		std::cout << "FILES A: " << wrk->first << std::endl;
		RooDataSet* eff = (RooDataSet*) wrk->second->data((efficiency_type+"_efficiency").c_str());
		if(eff == NULL) std::cerr << "ERROR: No eff results for: " << efficiency_type << std::endl;
		const RooArgSet* set = eff->get();
		if(v2_name != ""){
			RooRealVar* v2 =  (RooRealVar*) set->find((v2_name).c_str());
			//First find binned catagories	
			RooArgSet bin_cat1D_fit;
			bin_cat1D_fit.addClone( RooBinningCategory((v2_name+"_bins").c_str(), (v2_name+"_bins").c_str(), *v2));
			RooMultiCategory allCats1D("allCats1D", "allCats1D", RooArgSet(bin_cat1D_fit));
			RooDataSet myEff(*eff);
			myEff.addColumn(allCats1D);
			TIterator* catIt = allCats1D.typeIterator();
			for(RooCatType* t = (RooCatType*)catIt->Next(); t!=0; t = (RooCatType*)catIt->Next() ){
				TString catName = t->GetName();
				if(catName.Contains("NotMapped")) continue;
				catName.ReplaceAll("{","").ReplaceAll("}","").ReplaceAll(";","_&_");
				RooDataSet* eff_bin = (RooDataSet*) myEff.reduce( Cut(TString::Format("allCats1D==%d",t->getVal())) );
				file_cat_set[wrk->first][(std::string)catName] = eff_bin;
				cat_file_set[(std::string)catName][wrk->first] = eff_bin;
			}
		}
		else{
			file_cat_set[wrk->first]["no_bin"] = eff;
			cat_file_set["no_bin"][wrk->first] = eff;
		}
			
	}

	overlay_efficiency1D(cat_file_set,var_name, c_id);
	overlay_efficiency1D(file_cat_set,var_name, c_id);
	if(print_scale_factor && v2_name == "")
		plot_scale_factor1D(cat_file_set,var_name, c_id);
// 	else if(plot_scale_factor && cat_file_set["no_bin"].size() != 2&& v2_name == "")
// 		std::cout << "WARNING: Can't plot scale factor for: " << file_cat_set.size() << " files." << std::endl;
		
}

void TagProbePlotter::plot_scale_factor1D(std::map<std::string,std::map<std::string,RooDataSet *> > dataset_map, std::string var_name, std::string c_id)
{

	std::map<std::string,RooDataSet* > overlay_set = dataset_map["no_bin"];

	//find the data
	std::string data_entry = "";
	for(std::map<std::string,RooDataSet *>::iterator dataset = overlay_set.begin();dataset != overlay_set.end(); dataset++){
		
		if(dataset->first.find("data") != std::string::npos || dataset->first.find("Data") != std::string::npos){
			if(data_entry != ""){
				std::cout << "ERROR: More than one data root file input. Cannot create scale factor plot" << std::endl;
				return;
			}
			else		
				data_entry = dataset->first;				
		}

      	}

	if(data_entry == ""){
		std::cout << "Error: Can't create scale factor plot. No data entry" << std::endl;
		return;
	}
	
	//Extract the necessary roostuff to have the data efficiency
	const RooArgSet* d_set = overlay_set[data_entry]->get();
	RooRealVar* d_v1 = (RooRealVar*) d_set->find((var_name).c_str());
	RooRealVar* d_e = (RooRealVar*) d_set->find("efficiency");

	//Get v1 bin cats, this allows you to loop over bins
	RooArgSet bin_catv1;
	bin_catv1.addClone( RooBinningCategory((var_name+"_bins").c_str(), (var_name+"_bins").c_str(), *d_v1));
	RooMultiCategory allCatsv1("allCatsv1", "allCatsv1", RooArgSet(bin_catv1));
	TIterator* catItv1 = allCatsv1.typeIterator();
	int nbins = 0;
	for(RooCatType* tv1 = (RooCatType*)catItv1->Next(); tv1!=0; tv1 = (RooCatType*)catItv1->Next() ){
		nbins++;
	}
	std::cout << "Nbins: " << nbins << std::endl;

	//Create plot
	std::string plot_id = "scale_factors_"+var_name+"_"+efficiency_type+"_1Defficiency_"+c_id+"_no_bin";
	cholders[plot_id] = new CanvasHolder();
	cholders[plot_id]->setCanvasTitle(plot_id+"CH");

	RooPlot *frame = d_v1->frame(Name(var_name.c_str()));
	overlay_set[data_entry]->plotOnXY(frame,YVar(*d_e));

	RooHist* rhist = frame->getHist();
	if(rhist == 0)
		std::cout << "ERROR: could not convert to RooHist" << std::endl;

	bool first_graph = true;
	std::vector<int> colours_set;
	for(std::map<std::string,RooDataSet *>::iterator dataset = overlay_set.begin();dataset != overlay_set.end(); dataset++){

		std::string filename = dataset->first;
		if(filename == data_entry)
			continue;

		std::cout << efficiency_type << " efficiency scale factors for: " << c_id << " " << filename << " " << var_name  << std::endl;

		const RooArgSet* set = dataset->second->get();
		RooRealVar* e = (RooRealVar*) set->find("efficiency");
		

// 		Double_t *x_tmp = rhist->GetX();
// 		Double_t *x_h_tmp = rhist->GetEXhigh();
// 		Double_t *x_l_tmp = rhist->GetEXlow();
		Double_t *x = rhist->GetX();
		Double_t *x_h = rhist->GetEXhigh();
		Double_t *x_l = rhist->GetEXlow();
 		double y[nbins];
 		double y_h[nbins];
 		double y_l[nbins];

		std::cout << " X nbins: " <<  rhist->GetXaxis()->GetNbins() << std::endl;
		std::cout << " Y nbins: " << nbins << std::endl;

		int loop_start = 0;
		
		//begin complicated stuff to remove one bin
// 		std::vector<Double_t> x_v,x_h_v,x_l_v;
// 		std::cout << "Size xtmp " << nbins << std::endl;
// 		if(plot_id.find("mindR") != std::string::npos){
// 			loop_start = 1;
// 			for(int i = loop_start; i < nbins;i++){
// 				std::cout << "Push back " << x_tmp[i] << std::endl;
// 				x_v.push_back(x_tmp[i]);
// 				x_h_v.push_back(x_h_tmp[i]);
// 				x_l_v.push_back(x_l_tmp[i]);
// 			}
// 		}
// 		else{
// 			for(int i = loop_start; i < nbins;i++){
// 				x_v.push_back(x_tmp[i]);
// 				x_h_v.push_back(x_h_tmp[i]);
// 				x_l_v.push_back(x_l_tmp[i]);
// 			}

// 		}
// 		Double_t x[x_v.size()],x_h[x_v.size()],x_l[x_v.size()];
// 		std::cout << "V size " << x_v.size() << std::endl;
// 		for(int i = 0;i < x_v.size();i++){
// 			std::cout << "Filling point " << x_v[i] << std::endl;
// 			x[i] = x_v[i]; 
// 			x_h[i] = x_h_v[i]; 
// 			x_l[i] = x_l_v[i]; 
// 		}
		//end complicated stuff to remove one bin
			
		for(int i = loop_start;i < nbins;i++){

			//std::cout << " bin: " << i << std::endl;
			dataset->second->get(i);
			overlay_set[data_entry]->get(i);
			
			y[i] = d_e->getVal()/e->getVal();
			y_h[i] = d_e->getErrorHi()/e->getVal();
			y_l[i] = fabs(d_e->getErrorLo()/e->getVal());
			//std::cout << "Loop: i " << y[i] << " d_e " << d_e->getVal() << " e " << e->getVal() << std::endl;
		}
		//Add colours according to legend title if available
 		std::string legend_title = getTitle(filename);
 		if(legend_colours.find(legend_title) != legend_colours.end())
 			colours_set.push_back(legend_colours[legend_title]);
		

		//std::cout << " Done loop" << std::endl;
		TGraphAsymmErrors * graph = new TGraphAsymmErrors(nbins,x,y,x_l,x_h,y_l,y_h);

		cholders[plot_id]->set_eff_histos(true);
		
		//If you are only plotting one line no point showing legend title
		if(overlay_set.size() == 2)
			legend_title = "";
		
		if(first_graph)
			cholders[plot_id]->addGraph(graph,"",legend_title,"APz");
		else
			cholders[plot_id]->addGraph(graph,"",legend_title,"Pz");


		first_graph = false;

	}

// 		std::vector<int> styles;
// 		styles.push_back(1);
// 		styles.push_back(2);

	//cholders[plot_id]->addHLine(0.966,"",2,kBlue-4,1);
	cholders[plot_id]->addHLine(scale_factor_value,"",2,kBlue+2,9);
	//cholders[plot_id]->addHLine(0.970,"",2,kBlue-4,1);
	cholders[plot_id]->setLineColorsGraph(colours_set);
	cholders[plot_id]->setMarkerColorsGraph(colours_set);
	cholders[plot_id]->setBordersY(0.9, 1.05);
	if(plot_id.find("mindR") != std::string::npos)
		cholders[plot_id]->setBordersX(0.3, 4.);
	if(plot_id.find("pt") != std::string::npos)
		cholders[plot_id]->setBordersX(20., 100.);
	cholders[plot_id]->addHLine(1,"",2);	
	cholders[plot_id]->setOptStat(000000);
	cholders[plot_id]->setTitleY("Scale Factor");
	cholders[plot_id]->setTitleX(frame->GetXaxis()->GetTitle());

	cholders[plot_id]->set_eff_histos(true);
	cholders[plot_id]->setLegendOptions(0.3,0.125,"LowerRight");
	cholders[plot_id]->setLineStylesGraph(1);
	cholders[plot_id]->setLineSizeGraph(1.);
	cholders[plot_id]->setMarkerSizeGraph(1.);
	outfile->cd();
	cholders[plot_id]->save("eps");
	
	delete cholders[plot_id];
	cholders[plot_id] = NULL;
	delete frame;
	frame = NULL;
	
}

void TagProbePlotter::overlay_efficiency1D(std::map<std::string,std::map<std::string,RooDataSet *> > dataset_map, std::string var_name, std::string c_id)
{

	//loop over files to get overlay histos 
	for(std::map<std::string,std::map<std::string,RooDataSet *> >::iterator overlay_set = dataset_map.begin();overlay_set != dataset_map.end(); overlay_set++){

		std::string plot_id = var_name+"_"+efficiency_type+"_1Defficiency_"+c_id+"_"+overlay_set->first;
		if(verbose) std::cout << "Plotting ID: " << plot_id << std::endl;
		
		TCanvas *c = new TCanvas(plot_id.c_str(), "canvas",640,480);
		RooPlot *frame_eff = NULL;

		cholders[plot_id] = new CanvasHolder();
		cholders[plot_id]->setCanvasTitle(plot_id+"CH");

		//Need to collect datasets and e's first so they exist while next plot is overlayed
		std::vector<std::pair<RooDataSet*, RooRealVar* > >dataset_e;
		bool first = true;
		std::vector<std::string> legend_title;
		std::cout << "NDatasets size: " << overlay_set->second.size() << std::endl;
		for(std::map<std::string,RooDataSet *>::iterator dataset = overlay_set->second.begin();dataset != overlay_set->second.end(); dataset++){
			if(verbose) std::cout << "Identifier: " << dataset->first << std::endl;
			RooDataSet* eff_bin = dataset->second;
			const RooArgSet*set = eff_bin->get();
			RooRealVar*e = (RooRealVar*) set->find("efficiency");
			if(e == NULL) std::cerr << " No efficiency set " << std::endl;
			std::pair<RooDataSet*, RooRealVar* > tmp_pair(eff_bin,e);
			dataset_e.push_back(tmp_pair);
			legend_title.push_back(getTitle(dataset->first));
			if(first){
				RooRealVar* var = (RooRealVar*) set->find((var_name).c_str());
				frame_eff = var->frame(Name(var_name.c_str()));
			}
			first = false;
		}

		//Plot efficiencies on frame
		int i = 0;
		std::vector<int> colours_set;
		for(std::vector<std::pair<RooDataSet*, RooRealVar* > >::iterator ds_e_iter = dataset_e.begin(); ds_e_iter != dataset_e.end(); ds_e_iter++){
			
			std::pair<RooDataSet*, RooRealVar* > ds_e = *ds_e_iter;
			RooPlot* tmp_frame = frame_eff->emptyClone(var_name.c_str());
			tmp_frame->SetTitle("");
			tmp_frame->SetYTitle("Efficiency");
			tmp_frame->SetTitleOffset(1.3,"Y");
			tmp_frame->SetLabelSize(0.035,"X");
			tmp_frame->SetLabelSize(0.035,"Y");

			int line_colour = -1;
			//Add colours according to legend title if available
			if(legend_colours.find(legend_title[i]) != legend_colours.end()){
				line_colour = legend_colours[legend_title[i]];
			}
			else if(category_colours.find(var_name) != category_colours.end() && category_colours[var_name].size() > i)
				line_colour = category_colours[var_name][i];
				
			if(line_colour != -1){
				colours_set.push_back(line_colour);
				(ds_e.first)->plotOnXY(tmp_frame,YVar(*(ds_e.second)),MarkerColor(line_colour),LineColor(line_colour));
			       	(ds_e.first)->plotOnXY(frame_eff,YVar(*(ds_e.second)),MarkerColor(line_colour),LineColor(line_colour));
			}
			else
				(ds_e.first)->plotOnXY(tmp_frame,YVar(*(ds_e.second)));

			tmp_frame->SetAxisRange(.6,1.0,"Y");
			if(plot_id.find("mindR") != std::string::npos)
				tmp_frame->SetAxisRange(0.31,3.99,"X");

			std::cout << "Adding plot: " << legend_title[i] << " with colour: " << line_colour << " to " << plot_id << std::endl;
			cholders[plot_id]->addRooPlot(tmp_frame,legend_title[i]);

			i++;

			delete tmp_frame;
			tmp_frame = NULL;
		}
			
//  		c->cd(); frame_eff->Draw();
//  		c->SaveAs((plot_id+".eps").c_str());
//  		c->Write();

		cholders[plot_id]->set_eff_histos(true);
		cholders[plot_id]->setLegendOptions(0.3,0.125,"LowerRight");
		cholders[plot_id]->setLineColors(colours_set);
		cholders[plot_id]->save("eps");

		delete cholders[plot_id];
		cholders[plot_id] = NULL;
		
		delete frame_eff;
		frame_eff = NULL;

		for(std::map<std::string,RooDataSet *>::iterator dataset = overlay_set->second.begin();dataset != overlay_set->second.end(); dataset++){
			dataset->second = NULL;
		}
  		delete c;
  		c = NULL;

	}

}

void TagProbePlotter::plot_efficiency2D(std::map<std::string,RooWorkspace* > workspaces, std::string v1_name, std::string v2_name, std::string c_id)
{
	//Can't overlay 2D plots so everything done within loop
	for(std::map<std::string,RooWorkspace *>::iterator wrk = workspaces.begin(); wrk != workspaces.end();wrk++){

		std::string plot_id = v1_name+"_"+v2_name+"_"+efficiency_type+"_2Defficiency_"+c_id+"_"+wrk->first;
		
		TCanvas *c = new TCanvas(plot_id.c_str(), "canvas",640,480);
		c->SetRightMargin(0.15);
		cholders[plot_id] = new CanvasHolder();
		cholders[plot_id]->setCanvasTitle(plot_id+"CH");
		
		RooDataSet* eff = (RooDataSet*) wrk->second->data((efficiency_type+"_efficiency").c_str());
		const RooArgSet* set = eff->get();
		RooRealVar* e = (RooRealVar*) set->find("efficiency");
		RooRealVar* v1 = (RooRealVar*) set->find((v1_name).c_str());
		RooRealVar* v2 = (RooRealVar*) set->find((v2_name).c_str());

		//Fill the fit efficiency plot	
		TH2F* histo = new TH2F(plot_id.c_str(),"", v1->getBinning().numBins(),v1->getBinning().array(), v2->getBinning().numBins(), v2->getBinning().array());	

		for(int i=0; i < eff->numEntries(); i++){
			eff->get(i);
			histo->SetBinContent(histo->FindBin(v1->getVal(), v2->getVal()), e->getVal());
			histo->SetBinError(histo->FindBin(v1->getVal(), v2->getVal()), (e->getErrorHi()-e->getErrorLo())/2.);
		}	
			
		cholders[plot_id]->addHisto2D(histo,"","COLZtexte");
		cholders[plot_id]->setTitleX(v1->GetTitle());
		cholders[plot_id]->setTitleY(v2->GetTitle());
		cholders[plot_id]->setBordersZ(-0.001,1.001);
		cholders[plot_id]->save("eps");
		

// 		histo->SetOption("colztexte");
// 		gStyle->SetPaintTextFormat(".2f");
// 		histo->GetZaxis()->SetRangeUser(-0.001,1.001);
// 		histo->SetStats(kFALSE);	
// 		c->cd(); histo->Draw();
// 		c->SaveAs((plot_id+".eps").c_str());
// 		c->Write();

		delete histo;
		histo = NULL;
		delete c;
		c = NULL;
 		delete cholders[plot_id];
 		cholders[plot_id] = NULL;
	}
}


void TagProbePlotter::plot_distributions()
{
	std::map<std::string,RooAbsData* > datasets;
	datasets["All"] = NULL;
	datasets["Pass"] = NULL;
	datasets["Fail"] = NULL;
	
	outfile->cd();
	// map = [common name][identifier] = workspace. common name = eg overall_ptbin0. identifier = filename
	for(std::map<std::string,std::map<std::string,RooWorkspace *> >::iterator wrk_ids = workspace_map.begin();
	    wrk_ids != workspace_map.end(); wrk_ids++){

		if((wrk_ids->first).find("parent") != std::string::npos) 
		    continue;

		//Each bin could have diff variable. All files should have same vars in equiv. bins
		std::vector<RooRealVar*> reals;
		RooAbsData* base_ds = NULL;
		//Read in available vars from first file
		if(wrk_ids->second.size() > 0){
			std::map<std::string, RooWorkspace* > tmp_map = wrk_ids->second;
			base_ds = tmp_map.begin()->second->data("data");
			if(base_ds == NULL)
				std::cout << "ERROR: NULL dataset: data" << std::endl; 
			
			const RooArgSet* vars = base_ds->get();
			TIterator* it = vars->createIterator();
			for(RooAbsArg* v = (RooAbsArg*)it->Next(); v!=0; v = (RooAbsArg*)it->Next() ){
				if(!v->InheritsFrom("RooRealVar")) continue;
				std::cout << " VAR NAME:  " << v->GetName() << std::endl;
				reals.push_back((RooRealVar*)v);
			}
			
		}
		//FIXME: verify using reals from base_ds gives same results for second file as fitter
		//loop over vars
		for(uint i=0; i<reals.size(); i++){
			std::string var_name =  reals[i]->GetName();
			//Book empty histo for each var
			//			TH1F* histo = dynamic_cast<TH1F*> (reals[i]->createHistogram(reals[i]->GetName()));
			TH1F* histo = new TH1F (*(dynamic_cast<TH1F*> (reals[i]->createHistogram(reals[i]->GetName()))));
			//TH1F* histo = (TH1F*) reals[i]->createHistogram(reals[i]->GetName());
				
			//Rebin histo
			if(rebin.size() > 0 && rebin.find(var_name) != rebin.end()){
				if(verbose) std::cout << "Rebinning: " << var_name << " by " << rebin[var_name] << std::endl;
				histo->Rebin(rebin[var_name]);
			}		

			//plot dists for events passed, failed and all events
			for(std::map<std::string,RooAbsData* >::iterator ds = datasets.begin();ds != datasets.end();
			    ds++){
				//Create canvas for each var and dataset
				std::string c_id = var_name+"_"+wrk_ids->first+"_"+ds->first;
				cholders[c_id] = new CanvasHolder();
				cholders[c_id]->setCanvasTitle(c_id);

				//finally loop over files to get overlay histos 
				for(std::map<std::string,RooWorkspace *>::iterator wrk = wrk_ids->second.begin(); wrk != wrk_ids->second.end();wrk++){
					
					//Get results of fit from workspace		
					datasets["All"] = wrk->second->data("data");
					if(datasets["All"] == NULL)
						std::cout << "ERROR: NULL dataset: data" << std::endl; 
					datasets["Pass"] = datasets["All"]->reduce(Cut("_efficiencyCategory_==_efficiencyCategory_::Passed")); 
					datasets["Fail"] = datasets["All"]->reduce(Cut("_efficiencyCategory_==_efficiencyCategory_::Failed")); 
										
					histo->Reset(); //Wipe histogram contents
					ds->second->fillHistogram(histo,RooArgList(*(reals[i])));
					
					outfile->cd();
					histo->Write();
 					cholders[c_id]->addHisto(histo, wrk->first,"E1");
					
				}
				if(i < category_colours.size() && category_colours.find(ds->first) != category_colours.end())
					cholders[c_id]->setLineColors(category_colours[ds->first]);
				cholders[c_id]->setOptStat(000000);
				cholders[c_id]->save("eps");

				delete cholders[c_id];
				cholders[c_id] = NULL;
				datasets["All"] = NULL;
				datasets["Pass"] = NULL;
				datasets["Fail"] = NULL;
				
			}
			delete histo;
			histo = NULL;
		}
	}	
	
}

void TagProbePlotter::plot_fit_results()
{
	std::map<std::string,RooAbsData* > datasets;
	datasets["All"] = NULL;
	datasets["Pass"] = NULL;
 	datasets["Fail"] = NULL;
	
	outfile->cd();
	for(std::map<std::string,std::map<std::string,RooWorkspace *> >::iterator wrk_ids = workspace_map.begin();
	    wrk_ids != workspace_map.end(); wrk_ids++){

		if((wrk_ids->first).find("parent") != std::string::npos) 
		    continue;

		std::string c_id = "mass_fit_"+wrk_ids->first;

		TCanvas *c_all = new TCanvas("Mass_fit_all", "canvas",640,480);
		TCanvas *c_pass = new TCanvas("Mass_fit_pass", "canvas",640,480);
		TCanvas *c_fail = new TCanvas("Mass_fit_fail", "canvas",640,480);
		RooPlot* frame_all = NULL;
		RooPlot* frame_pass = NULL;
		RooPlot* frame_fail = NULL;

		//finally loop over files to get overlay histos 
		bool first = true;
		for(std::map<std::string,RooWorkspace *>::iterator wrk = wrk_ids->second.begin(); wrk != wrk_ids->second.end();wrk++){

			//cholder plots not overlayed for now
			std::string plot_all = c_id+wrk->first+"_All";
			std::string plot_pass = c_id+wrk->first+"_Pass";
			std::string plot_fail = c_id+wrk->first+"_Fail";
			cholders[plot_all] = new CanvasHolder();
			cholders[plot_all]->setCanvasTitle(plot_all+"CH");
			cholders[plot_pass] = new CanvasHolder();
			cholders[plot_pass]->setCanvasTitle(plot_pass+"CH");
			cholders[plot_fail] = new CanvasHolder();
			cholders[plot_fail]->setCanvasTitle(plot_fail+"CH");

			RooCategory* efficiencyCategory = wrk->second->cat("_efficiencyCategory_");
			if(efficiencyCategory == NULL) std::cout << "ERROR: NULL category: _efficiencyCategory_" << std::endl;

			//Get results of fit from workspace		
			datasets["All"] = wrk->second->data("data");
			if(datasets["All"] == NULL) std::cout << "ERROR: NULL dataset: data" << std::endl; 
			datasets["Pass"] = datasets["All"]->reduce(Cut("_efficiencyCategory_==_efficiencyCategory_::Passed")); 
			datasets["Fail"] = datasets["All"]->reduce(Cut("_efficiencyCategory_==_efficiencyCategory_::Failed")); 

			RooAbsPdf* pdf = wrk->second->pdf("simPdf");
			if(pdf == NULL){
				std::cout << "ERROR: no pdf for fit plot in " << wrk->first << " " << wrk_ids->first <<  std::endl;
				continue;
			}
			RooArgSet *obs = pdf->getObservables(*datasets["All"]);
			RooRealVar* mass = 0;
			TIterator* it = obs->createIterator();
			for(RooAbsArg* v = (RooAbsArg*)it->Next(); v!=0; v = (RooAbsArg*)it->Next() ){
				if(!v->InheritsFrom("RooRealVar")){
					RooCategory* tmp_cat = (RooCategory*)v;
					std::cout << "TEST CAT: " << std::endl;
					tmp_cat->Print();
					continue;
				}
				mass = (RooRealVar*)v;
				break;
			}
			if(!mass){
				std::cout << "ERROR: no mass " <<std::endl;
				return;
			}
			if(verbose) std::cout << " Making first plot " << std::endl; 
			
			//RooPlot* frame_all = mass->frame(Name("All"),Title("All Probes"),Bins(massBins));
			if(first){
				frame_all = mass->frame(Name("All"),Title("All Probes"));
				frame_pass = mass->frame(Name("Passing"),Title("Passing Probes"));
				frame_fail = mass->frame(Name("Failing"),Title("Failing Probes"));
			}

			datasets["All"]->plotOn(frame_all);
			pdf->plotOn(frame_all, ProjWData(*datasets["All"]), LineColor(kBlue));
			pdf->plotOn(frame_all, ProjWData(*datasets["All"]), LineColor(kBlue), Components("backgroundPass,backgroundFail"), LineStyle(kDashed));
			cholders[plot_all]->addRooPlot(frame_all,"");

			datasets["Pass"]->plotOn(frame_pass);
			pdf->plotOn(frame_pass, Slice(*efficiencyCategory, "Passed"), ProjWData(*datasets["Pass"]), LineColor(kGreen));
			pdf->plotOn(frame_pass, Slice(*efficiencyCategory, "Passed"), ProjWData(*datasets["Pass"]), LineColor(kGreen), Components("backgroundPass"), LineStyle(kDashed));
			cholders[plot_pass]->addRooPlot(frame_pass,"");


			datasets["Fail"]->plotOn(frame_fail);
			pdf->plotOn(frame_fail, Slice(*efficiencyCategory, "Failed"), ProjWData(*datasets["Fail"]), LineColor(kRed));
			pdf->plotOn(frame_fail, Slice(*efficiencyCategory, "Failed"), ProjWData(*datasets["Fail"]), LineColor(kRed), Components("backgroundFail"), LineStyle(kDashed));
			cholders[plot_fail]->addRooPlot(frame_fail,"");
			
			first = false;

			if(frame_all != NULL && frame_pass != NULL && frame_fail != NULL){
				
				cholders[plot_all]->save("eps");
				cholders[plot_pass]->save("eps");
				cholders[plot_fail]->save("eps");
			}
			delete cholders[plot_all];
			cholders[plot_all] = NULL;
			delete cholders[plot_pass];
			cholders[plot_pass] = NULL;
			delete cholders[plot_fail];
			cholders[plot_fail] = NULL;
	

		}
		if(frame_all != NULL && frame_pass != NULL && frame_fail != NULL){
// 			c_all->cd(); frame_all->Draw();
// 			c_all->SaveAs((c_id+"_All.eps").c_str());
// 			c_all->Write();
			
// 			c_pass->cd(); frame_pass->Draw();
// 			c_pass->SaveAs((c_id+"_Pass.eps").c_str());
// 			c_pass->Write();
			
// 			c_fail->cd(); frame_fail->Draw();
// 			c_fail->SaveAs((c_id+"_Fail.eps").c_str());	
// 			c_fail->Write();

		}
		delete c_all;
		c_all = NULL;
		delete c_pass;
		c_pass = NULL;
		delete c_fail;
		c_fail = NULL;

		datasets["All"] = NULL;
		datasets["Pass"] = NULL;
		datasets["Fail"] = NULL;
		
	}
	
}
