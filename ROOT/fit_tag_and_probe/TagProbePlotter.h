#ifndef TAGPROBEPLOTTER_H
#define TAGPROBEPLOTTER_H

#include "ResultsManager.h"


class TagProbePlotter:public ResultsManager{
        public:
	        TagProbePlotter();
	        ~TagProbePlotter();
		void plot();
		double scale_factor_value;
	
        private:

		void plot_fit_results();
		void plot_distributions();
		void make_efficiency_plots();
		void plot_efficiency1D(std::map<std::string,RooWorkspace* > workspaces, std::string var_name, std::string c_id, std::string v2_name = "");
		void overlay_efficiency1D(std::map<std::string,std::map<std::string,RooDataSet *> > dataset_map, std::string var_name, std::string c_id);
		void plot_efficiency2D(std::map<std::string,RooWorkspace* > workspaces, std::string v1_name,std::string v2_name, std::string c_id);
		void plot_scale_factor1D(std::map<std::string,std::map<std::string,RooDataSet *> > dataset_map, std::string var_name, std::string c_id);

		bool plot_1Defficiency_breakdown;

};
#endif 
