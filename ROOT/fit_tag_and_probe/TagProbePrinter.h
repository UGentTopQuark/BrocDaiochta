#ifndef TAGPROBEPRINTER_H
#define TAGPROBEPRINTER_H

#include "ResultsManager.h"


class TagProbePrinter:public ResultsManager{
        public:
	        TagProbePrinter();
	        ~TagProbePrinter();
		void print();
		double get_scale_factor_value();
	
        private:

		void make_efficiency_tables();	      
		void print_eff_table2D(std::map<std::string,RooWorkspace* > workspaces, std::string v1_name,std::string v2_name, std::string c_id);
		void print_eff_table1D(std::map<std::string,RooWorkspace* > workspaces, std::string var_name, std::string c_id, std::string v2_name = "");
		void make_detailed_table();
		void print_table_start(std::vector<std::string> nfiles, std::string v1_name, std::string v2_name);
		std::vector<double> get_scale_factors(std::vector<double> effs);
		double fix_precision(double input, int prec);
		double overall_scale_factor;
};
#endif 
