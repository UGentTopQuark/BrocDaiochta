#include "ResultsManager.h"

ResultsManager::ResultsManager()
{
	process_cmd = "-1"; 
	first_directory = "lepEffs";
	efficiency_type = "fit";
	outfile = new TFile("outfile.root","recreate");
	print_detailed_table = false;
	print_eff_only_table = false;
	print_scale_factor = false;
}

ResultsManager::~ResultsManager()
{
	delete outfile;
	outfile = NULL;
}

void ResultsManager::set_input_files(std::vector<std::string> input_files)
{
	this->input_files = input_files;
}

void ResultsManager::set_process_cmd(std::string process_cmd)
{
	this->process_cmd = process_cmd;
}

void ResultsManager::set_input_directories(std::vector<std::string> input_dirs)
{
	input_directories = input_dirs;
}

void ResultsManager::set_rebin(std::map<std::string,int> rebin)
{
	this->rebin = rebin;
}
void ResultsManager::set_category_colours(std::map<std::string,std::vector<int> > cat_colours)
{
	category_colours = cat_colours;
}
void ResultsManager::set_legend_colours(std::map<std::string,int> leg_colours)
{
	legend_colours = leg_colours;
}
void ResultsManager::set_efficiency_type(std::string eff_type)
{
	efficiency_type = eff_type;
}
void ResultsManager::set_print_detailed_table(bool print)
{
	print_detailed_table = print;
}
void ResultsManager::set_print_eff_only_table(bool print)
{
	print_eff_only_table = print;
}
void ResultsManager::set_print_scale_factor(bool print)
{
	print_scale_factor = print;
}

void ResultsManager::read_in_workspaces(std::string in_dir, bool parent_only)
{
	for(std::map<std::string,std::map<std::string,RooWorkspace *> >::iterator wrk_ids = workspace_map.begin();
	    wrk_ids != workspace_map.end(); wrk_ids++){
		for(std::map<std::string,RooWorkspace *>::iterator wrk = wrk_ids->second.begin(); wrk != wrk_ids->second.end();wrk++){
			if(wrk->second != NULL){
				delete wrk->second;
				wrk->second = NULL;
			}
		}
	}
	workspace_map.clear();
	//Open the input file
	for(std::vector<std::string>::iterator file_it = input_files.begin(); file_it != input_files.end(); file_it++){
		std::cout << "Opening file: " << *file_it << " in dir: "<< in_dir <<  std::endl;
		TFile *infile = new TFile(file_it->c_str(),"open");
		std::cout << "Opened file. " << std::endl;
		infile->cd();
		TKey *key;
		if(verbose) std::cout << "Getting keys from directory: " << first_directory <<"/"<< in_dir << std::endl;
		TIter nextkey(infile->GetDirectory((first_directory+"/"+in_dir).c_str())->GetListOfKeys());
		if(verbose) std::cout << "Got keys " << std::endl;
		
		while ((key = (TKey*)nextkey())){
			if(verbose) std::cout << "processing: " << key->GetName() << std::endl;
			if(verbose) std::cout << "class: " << key->GetClassName() << std::endl;
			
			std::string cur_class = key->GetClassName(); 
			
			//There is a sub dir for each binned results. Each sub dir contains a workspace.
			if(!parent_only && cur_class == "TDirectoryFile"){
				TKey *subkey;
				std::string sub_dir_name = key->GetName();
				std::string sub_dir_path = first_directory+"/"+in_dir+"/"+sub_dir_name;
				if(verbose) std::cout << "   Found sub dir: " << sub_dir_name << std::endl;
				
				//Get list of keys to find the workspace
				TIter subnextkey(infile->GetDirectory(sub_dir_path.c_str())->GetListOfKeys());
				while ((subkey = (TKey*)subnextkey())){
					if(verbose) std::cout << "   processing sub: " << subkey->GetName() << std::endl;
					if(verbose) std::cout << "   class sub: " << subkey->GetClassName() << std::endl;
					
					if((std::string) subkey->GetClassName() == "RooWorkspace"){
						if(verbose) std::cout << "Editing id: " << *file_it << std::endl; 
						int path_end_pos = file_it->find_last_of("/");
						std::string identifier = file_it->substr(path_end_pos+1,(file_it->size()-path_end_pos-6));
						std::string common_name = in_dir+"_"+sub_dir_name;
						if(verbose) std::cout << "   Adding workspace with id: " <<  identifier << ", common name: " << common_name << std::endl; 
						workspace_map[common_name][identifier] = (RooWorkspace*)infile->GetDirectory(sub_dir_path.c_str())->Get(subkey->GetName());
// 						workspace_map[common_name][identifier] = new RooWorkspace(*((RooWorkspace*)infile->GetDirectory(sub_dir_path.c_str())->Get(subkey->GetName())));
					}
					
				}
				
			}
			//Theres one workspace in the parent dir for the efficiency plots
			else if(cur_class == "RooWorkspace"){
				int path_end_pos = file_it->find_last_of("/");
				std::string identifier = file_it->substr(path_end_pos+1,(file_it->size()-path_end_pos-6));
				std::string common_name = in_dir+"_parent";
				if(verbose) std::cout << "   Adding workspace with id: " <<  identifier << ", common name: " << common_name << std::endl; 
				workspace_map[common_name][identifier] = (RooWorkspace*)infile->GetDirectory((first_directory+"/"+in_dir).c_str())->Get(key->GetName());
				//workspace_map[common_name][identifier] = new RooWorkspace(*((RooWorkspace*)infile->GetDirectory((first_directory+"/"+in_dir).c_str())->Get(key->GetName())));
				
				
			}
		}
 		infile->Close();
 		delete infile;
 		infile = NULL;
	}

	
	if(verbose) std::cout << "Setting dataset name = data if title contains data" <<std::endl;
	//In the fitter the dataset name is not set. Need to set it here to access datasets
	// map = [common name][identifier] = workspace. common name = eg overall_ptbin0. identifier = filename
	for(std::map<std::string,std::map<std::string,RooWorkspace *> >::iterator wrk_ids = workspace_map.begin();
	    wrk_ids != workspace_map.end(); wrk_ids++){
		for(std::map<std::string,RooWorkspace *>::iterator wrk = wrk_ids->second.begin(); wrk != wrk_ids->second.end();wrk++){
			
			//if(verbose) wrk->second->Print();
			std::list<RooAbsData*> lst =  wrk->second->allData();
			//Only title = data, need name = data. set here
			for(std::list<RooAbsData* >::iterator it = lst.begin();it != lst.end(); it++){
				if(verbose) std::cout << "dataset names b4 switch: " << (*it)->GetName() << std::endl;
				(*it)->SetName((*it)->GetTitle());
				if(verbose) std::cout << "Available dataset names: " << (*it)->GetName() << std::endl;
				//if(verbose) (*it)->Print(); 
			}
		}
	}
}

void ResultsManager::read_in_workspaces_per_bin(std::string filename,std::string in_dir)
{
	for(std::map<std::string,RooWorkspace *>::iterator wrk = per_bin_workspace_map.begin(); wrk != per_bin_workspace_map.end();wrk++){
		delete wrk->second;
	}
	per_bin_workspace_map.clear();
	
	std::cout << "Opening file for per bin eff: " << filename << std::endl;
	TFile *infile = new TFile(filename.c_str(),"open");
	infile->cd();
	
	TKey *key;
	if(verbose) std::cout << "Getting keys from directory: " << first_directory <<"/"<< in_dir << std::endl;
	TIter nextkey(infile->GetDirectory((first_directory+"/"+in_dir).c_str())->GetListOfKeys());
	if(verbose) std::cout << "Got keys " << std::endl;
	
	while ((key = (TKey*)nextkey())){
		if(verbose) std::cout << "processing: " << key->GetName() << std::endl;
		if(verbose) std::cout << "class: " << key->GetClassName() << std::endl;
		
		std::string cur_class = key->GetClassName(); 
		
		//There is a sub dir for each binned results. Each sub dir contains a workspace.
		if(cur_class == "TDirectoryFile"){
			TKey *subkey;
			std::string sub_dir_name = key->GetName();
			std::string sub_dir_path = first_directory+"/"+in_dir+"/"+sub_dir_name;
			if(verbose) std::cout << "   Found sub dir: " << sub_dir_name << std::endl;
			
			//Get list of keys to find the workspace
			TIter subnextkey(infile->GetDirectory(sub_dir_path.c_str())->GetListOfKeys());
			while ((subkey = (TKey*)subnextkey())){
				if(verbose) std::cout << "   processing sub: " << subkey->GetName() << std::endl;
				if(verbose) std::cout << "   class sub: " << subkey->GetClassName() << std::endl;
				
				if((std::string) subkey->GetClassName() == "RooWorkspace"){
					if(verbose) std::cout << "   Adding workspace for in dir: " <<  in_dir << ", with id: " << sub_dir_name << std::endl; 
					per_bin_workspace_map[sub_dir_name] = new RooWorkspace(*((RooWorkspace*)infile->GetDirectory(sub_dir_path.c_str())->Get(subkey->GetName())));
				}
				
			}			
		}
	}
	delete infile;
	infile = NULL;
		
	if(verbose) std::cout << "Setting dataset name = data if title contains data" <<std::endl;			
	//In the fitter the dataset name is not set. Need to set it here to access datasets
	// map = [sub_dir_name] = workspace. common name = eg overall_ptbin0. identifier = filename
	for(std::map<std::string,RooWorkspace *>::iterator wrk = per_bin_workspace_map.begin(); wrk != per_bin_workspace_map.end();wrk++){
		if(verbose) wrk->second->Print();
		std::list<RooAbsData*> lst =  wrk->second->allData();
		//Only title = data, need name = data. set here
		for(std::list<RooAbsData* >::iterator it = lst.begin();it != lst.end(); it++){
			if(verbose) std::cout << "dataset names b4 switch: " << (*it)->GetName() << std::endl;
			(*it)->SetName((*it)->GetTitle());
			if(verbose) std::cout << "Available dataset names: " << (*it)->GetName() << std::endl;
			if(verbose) (*it)->Print(); 
		}
	}
}

//returns eg. eta, pt
std::vector<std::string>  ResultsManager::get_binned_var_names(std::string dir_name)
{
	std::vector<std::string> binned_vars;

	std::string var_str = dir_name.erase(dir_name.find("_bins"),dir_name.size());
	if(verbose) std::cout <<"Common parent dir = " << var_str << std::endl;
	
	while(var_str.find_first_of("_") != std::string::npos){
		int split_pos = var_str.find_first_of("_");
		binned_vars.push_back(var_str.substr(0,split_pos));
		if(verbose) std::cout << "Pushed bak: " << var_str.substr(0,split_pos) << std::endl;
		var_str = var_str.erase(0,split_pos+1);
		if(verbose) std::cout << " var_str " << var_str << std::endl;
				      
	}
	if(var_str.find("_") == std::string::npos && var_str.size() > 0){
		if(verbose) std::cout << "Pushed bak: " << var_str << std::endl;
		binned_vars.push_back(var_str);
	}
	return binned_vars;

}

//returns eg. eta_bin0, pt_bin4
std::vector<std::string>  ResultsManager::get_var_bin_names(std::string dir_name)
{
	std::vector<std::string> var_bins;

	
	//if(verbose) std::cout <<"Getting bins for: " << dir_name << std::endl;
	
	while(dir_name.find("bin") != std::string::npos){
		int split_pos = dir_name.find("bin");
		var_bins.push_back(dir_name.substr(0,split_pos+4));
		//if(verbose) std::cout << "Pushed bak: " << dir_name.substr(0,split_pos+4) << std::endl;
		if(split_pos+6 != std::string::npos)
			dir_name = dir_name.erase(0,split_pos+6);
		else 
			dir_name = "none";
		//if(verbose) std::cout << " remaining: " << dir_name << std::endl;
				      
	}
	return var_bins;

}

std::string ResultsManager::getTitle(std::string id)
{
//         if(id.find("pt_bin0") != std::string::npos)
// 		return "$\\mu p_{T} 20 - 30$";
//         if(id.find("pt_bin1") != std::string::npos)
// 		return "$\\mu p_{T} 30 - 40$";
//         if(id.find("pt_bin2") != std::string::npos)
// 		return "$\\mu p_{T} 40 - 60$";
//         if(id.find("pt_bin3") != std::string::npos)
// 		return "$\\mu p_{T} > 60$";
        if(id.find("pt_bin0") != std::string::npos)
		return "$20 - 30$";
        if(id.find("pt_bin1") != std::string::npos)
		return "$30 - 40$";
        if(id.find("pt_bin2") != std::string::npos)
		return "$40 - 60$";
        if(id.find("pt_bin3") != std::string::npos)
		return "$> 60$";

	//if(id.find("abseta_bin0") != std::string::npos)
	//		return "Barrel";
	//		//if(id.find("abseta_bin1") != std::string::npos)
	//		return "Overlap";
	//        if(id.find("abseta_bin2") != std::string::npos)
	//		return "Endcap";

//         if(id.find("eta_bin0") != std::string::npos)
// 		return "Endcap";
//         if(id.find("eta_bin1") != std::string::npos)
// 		return "Overlap";
//         if(id.find("eta_bin2") != std::string::npos)
// 		return "Barrel";
//         if(id.find("eta_bin3") != std::string::npos)
// 		return "Barrel";
//         if(id.find("eta_bin4") != std::string::npos)
// 		return "Overlap";
//         if(id.find("eta_bin5") != std::string::npos)
// 		return "Endcap";

	if(process_cmd == "level"){
		if(id.find("passing_l1") != std::string::npos)
			return "L1 Efficiency";
		if(id.find("passing_idx") != std::string::npos)
			return "HLT Efficiency";
		if(id.find("passing_hltwrtl1") != std::string::npos)
			return "HLT/L1 Efficiency";
		else
			return "not_found";
	}
	else if(process_cmd == "time"){
	        if(id.find("mu9_old") != std::string::npos)
			return "2010A";
		if(id.find("mu9_new") != std::string::npos)
			return "2010B";
	}
	else if(process_cmd == "trigger"){
		if(id.find("mu9_old") != std::string::npos)
			return "HLT Mu9 2010A";
		if(id.find("mu9_new") != std::string::npos)
			return "HLT Mu9 2010B";
		if(id.find("Data_muon_mu15") != std::string::npos)
			return "HLT Mu15 2010B";
		if(id.find("Zmumu") != std::string::npos)
			return "HLT Mu9 MC";
		if(id.find("Data_muon_relIso") != std::string::npos)
			return "Comb.Rel.Iso < 0.15";
		if(id.find("Data_muon_") != std::string::npos)
			return "Comb.Rel.Iso < 0.05";
		
	}
	else if(process_cmd == "dm"){
		if(id.find("Zmumu") != std::string::npos)
			return "Z#rightarrow#mu^{+}#mu^{-}";
		if(id.find("mc") != std::string::npos)
			return "Z + Jets";
		if(id.find("Data") != std::string::npos)
			return "Data";
	}
	if(id.find("Zmumu") != std::string::npos)
		return "HLT Mu9 MC";
	if(id.find("Zmumu") != std::string::npos)
		return "Z#rightarrow#mu^{+}#mu^{-}";
	if(id.find("mc") != std::string::npos)
		return "Z + Jets";
	if(id.find("mu9_old") != std::string::npos)
		return "HLT Mu9 2010A";
	if(id.find("mu9_new") != std::string::npos)
		return "HLT Mu9 2010B";
	if(id.find("Data_muon_mu15") != std::string::npos)
		return "HLT Mu15 2010B";
	if(id.find("Data_muon_") != std::string::npos || id.find("Data_electron_") != std::string::npos)
		return "Data";
	if(id.find("DYJets") != std::string::npos || id.find("DY50") != std::string::npos || id.find("Zjets") != std::string::npos)
		return "Z/#gamma*#rightarrowl^{+}l^{-}";
	if(id.find("DYmumu_pow") != std::string::npos)
		return "DY#rightarrow#mu^{+}#mu^{-}";
	if(id.find("DYmumu_py") != std::string::npos || id.find("DYToMuMu") != std::string::npos)
		return "DY#rightarrow#mu^{+}#mu^{-}";
	
	else if(process_cmd == "multimcdata"){
		if(id.find("Zmumu") != std::string::npos)
			return "Z#rightarrow#mu^{+}#mu^{-} Z2 Pythia";
		if(id.find("Data_muon_") != std::string::npos)
			return "Data";
		if(id.find("DYJets") != std::string::npos)
			return "DY#rightarrowl^{+}l^{-} D6T Madgraph";
		if(id.find("DYmumu_pow") != std::string::npos)
			return "DY#rightarrow#mu^{+}#mu^{-} Z2 Powheg";
		if(id.find("DYmumu_py") != std::string::npos)
			return "DY#rightarrow#mu^{+}#mu^{-} Z2 Pythia";

	}
        if(id.find("no_bin") != std::string::npos)
		return "";


	else
		return id;

}


