#include "EventWeightCalculator.h"

eire::EventWeightCalculator::EventWeightCalculator(eire::ConfigReader *config_reader)
{
	this->config_reader = config_reader;
	read_config();
}

eire::EventWeightCalculator::~EventWeightCalculator()
{
}

void eire::EventWeightCalculator::calculate_weights()
{
	TFile ref_file(reference_file.c_str(),"OPEN");
	std::vector<TFile*> files;

	TH1F *histo = (TH1F*) ref_file.GetDirectory(directory.c_str())->Get(histo_names[0].c_str());
	if(histo->Integral() != 0) histo->Scale(1./histo->Integral());

	CanvasHolder all_abs_cholder;
	all_abs_cholder.setCanvasTitle("all_abs");
	all_abs_cholder.setLegendOptions(0.40, 0.14, "UpperRight");
	//all_abs_cholder.setTitleY("weight");
	all_abs_cholder.setOptStat(000000);
	all_abs_cholder.addHisto(histo, "reference", "");


	CanvasHolder all_weight_cholder;
	all_weight_cholder.setCanvasTitle("all_weight");
	all_weight_cholder.setLegendOptions(0.40, 0.14, "UpperRight");
	all_weight_cholder.setTitleY("weight");
	all_weight_cholder.setOptStat(000000);
	//all_weight_cholder.setLogY();

	int nfile = 0;
	for(std::vector<std::string>::iterator file_name = files_to_be_weighted.begin();
		file_name != files_to_be_weighted.end();
		++file_name){

		std::string dataset = *file_name;
		dataset = dataset.substr(0,dataset.find("_plots.root"));

		CanvasHolder weight_cholder;
		weight_cholder.setCanvasTitle(dataset+"_weight");
		weight_cholder.setOptStat(000000);
		weight_cholder.setLegendOptions(0.4, 0.14, "UpperRight");
		weight_cholder.setTitleY("weight");

		files.push_back(new TFile(file_name->c_str(), "OPEN"));

		TH1F *comp_histo = (TH1F*) files[nfile]->GetDirectory(directory.c_str())->Get(histo_names[nfile+1].c_str());
		if(comp_histo->Integral() != 0) comp_histo->Scale(1./comp_histo->Integral());

		TH1F *division = (TH1F*) histo->Clone();
		division->Divide(histo, comp_histo);

		all_abs_cholder.addHisto(comp_histo, dataset, "P");

		weight_cholder.addHisto(division, dataset, "");
		all_weight_cholder.addHisto(division, dataset, "");
		weight_cholder.save("eps");

		print_weights(dataset, division);

		++nfile;
	}

	all_weight_cholder.save("eps");
	all_abs_cholder.save("eps");
}

void eire::EventWeightCalculator::print_weights(std::string dataset, TH1F *weight_histo)
{
	std::cout << "[" << dataset << "]" << std::endl;
	std::cout << "nbins = " << weight_histo->GetNbinsX() << std::endl;
	std::cout << "range = " << weight_histo->GetXaxis()->GetXmin() << ":" << weight_histo->GetXaxis()->GetXmax() << std::endl;
	for(int i = 1; i <= weight_histo->GetNbinsX(); ++i){
		std::cout << i << " = " << weight_histo->GetBinContent(i) << std::endl;
	}
}

void eire::EventWeightCalculator::read_config()
{
	histo_names = config_reader->get_vec_var("histo_names","global",true);
	directory = config_reader->get_var("directory","global",true);
	reference_file = config_reader->get_var("reference_file","global",true);
	files_to_be_weighted = config_reader->get_vec_var("files_to_be_weighted","global",true);
}
