#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "ConfigReader/ConfigReader.h"
#include "EventWeightCalculator.h"

int main(int argc, char **argv)
{
	eire::ConfigReader *config_reader = new eire::ConfigReader();
	config_reader->read_config_from_file(argv[1]);

	eire::EventWeightCalculator evt_weight_calc(config_reader);
	evt_weight_calc.calculate_weights();

	if(config_reader){ delete config_reader; config_reader = NULL; }
}
