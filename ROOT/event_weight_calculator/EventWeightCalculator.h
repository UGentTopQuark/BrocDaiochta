#ifndef EVENTWEIGHTCALCULATOR_H
#define EVENTWEIGHTCALCULATOR_H

#include "TFile.h"
#include "TH1F.h"
#include "TGraphAsymmErrors.h"

#include "ConfigReader/ConfigReader.h"
#include "CanvasHolder/Canvas_Holder.h"

namespace eire{
	class EventWeightCalculator{
		public:
			EventWeightCalculator(eire::ConfigReader *config_reader);
			~EventWeightCalculator();

			void calculate_weights();
			void print_weights(std::string dataset, TH1F *weight_histo);
		private:
			eire::ConfigReader *config_reader;
			void read_config();

			std::string directory;
			std::vector<std::string> histo_names;
			std::string reference_file;
			std::vector<std::string> files_to_be_weighted;
	};
}

#endif
