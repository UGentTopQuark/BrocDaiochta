#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include "Canvas_Holder.h"
#include <sstream>
#include "HistoCombiner.h"

int main(int argc, char **argv)
{
	std::string e_mu = argv[1];
	std::string c_type = argv[2];
	std::string mass = argv[3];
	std::string cutset = argv[4];
	std::string version = "";

	bool electron = true;
	if(e_mu == "e"){
		std::cout << "plotting for electrons..." << std::endl;
		electron = true;
	}
	else{
		std::cout << "plotting for muons..." << std::endl;
		electron = false;
	}

	std::vector<std::string> *sequence_to_plot = new std::vector<std::string>();

	std::map<std::string,std::vector<std::string> > files_with_content;
	if(electron){
		if(c_type == "c" || c_type == "s"){
			std::cout << "plotting signal..." << std::endl;
			files_with_content["ttbar"+version+".root"].push_back("TTbar|e_background|"+cutset);
			sequence_to_plot->push_back("TTbar|e_background|"+cutset);
		}			
		if(c_type == "c" || c_type == "b"){
			std::cout << "plotting background..." << std::endl;
//			files_with_content["qcdpt15"+version+".root"].push_back("QCDpt15|e_background|"+cutset);
//			sequence_to_plot->push_back("QCDpt15|e_background|"+cutset);
			files_with_content["wjets"+version+".root"].push_back("Wjets|e_background|"+cutset);
			sequence_to_plot->push_back("Wjets|e_background|"+cutset);
			files_with_content["zjets"+version+".root"].push_back("Zjets|e_background|"+cutset);
			sequence_to_plot->push_back("Zjets|e_background|"+cutset);

		
		}
	}else{
		if(c_type == "c" || c_type == "s"){
			std::cout << "plotting signal..." << std::endl;

			files_with_content["ttbar_mu"+version+".root"].push_back("TTbar|mu_background|"+cutset);
			sequence_to_plot->push_back("TTbar|mu_background|"+cutset);
			
		}
		if(c_type == "c" || c_type == "b"){
			std::cout << "plotting background..." << std::endl;
			files_with_content["qcd_mu"+version+".root"].push_back("Mupt15|mu_background|"+cutset);
			sequence_to_plot->push_back("Mupt15|mu_background|"+cutset);
			files_with_content["w_mu"+version+".root"].push_back("Wjets|mu_background|"+cutset);
			sequence_to_plot->push_back("Wjets|mu_background|"+cutset);
			files_with_content["z_mu"+version+".root"].push_back("Zjets|mu_background|"+cutset);
			sequence_to_plot->push_back("Zjets|mu_background|"+cutset);
	
	
		}
	}


	std::vector<std::string> histo_names;
	histo_names.push_back("jet_number_");
	//	histo_names.push_back("muon_number_");
		histo_names.push_back("muon_pt_");
// 	histo_names.push_back("muon_d0_");
 	histo_names.push_back("muon_eta_");
//	histo_names.push_back("muon_phi_");
//	histo_names.push_back("electron_number_");
//	histo_names.push_back("electron_pt_");
//	histo_names.push_back("electron_eta_");
//	histo_names.push_back("electron_phi_");
//
	// FIXME: change those lines for new Plot name
//		histo_names.push_back("Ht_");
	//	histo_names.push_back("Ht_wo_MET_");
//	histo_names.push_back("Electron_Ht_");
//		histo_names.push_back("Event_MET_");
// 	histo_names.push_back("pt_hadt_");
// 	histo_names.push_back("pt_lept_");

//	histo_names.push_back("jet_pt_");
//	histo_names.push_back("jet_eta_");
//	histo_names.push_back("jet_phi_");
 	histo_names.push_back("jet1_pt_");
	histo_names.push_back("jet2_pt_");
	histo_names.push_back("jet3_pt_");
        histo_names.push_back("jet4_pt_");
// //
	histo_names.push_back("top_mass_");
//	histo_names.push_back("top_mass_w_METpz_");

//	

//
////	histo_names.push_back("jet_unmatch_btag_");
////	histo_names.push_back("jet_match_btag_");
//	histo_names.push_back("jet1_eta_");
//	histo_names.push_back("jet2_eta_");
//	histo_names.push_back("match_mu_caloiso_");
//	histo_names.push_back("match_mu_trackiso_");
//	histo_names.push_back("match_mu_CombRelIso_");
// 	histo_names.push_back("unmatch_mu_CombRelIso_");
// 	histo_names.push_back("unmatch_mu_caloiso_");
// 	histo_names.push_back("unmatch_mu_trackiso_");
//	histo_names.push_back("match_e_caloiso_");
//	histo_names.push_back("match_e_trackiso_");
// 	histo_names.push_back("unmatch_e_caloiso_");
// 	histo_names.push_back("unmatch_e_trackiso_");
//	histo_names.push_back("match_e_CombRelIso_");
//	histo_names.push_back("unmatch_e_CombRelIso_");


	std::map<std::string,bool> *logscale = new std::map<std::string,bool>();
	(*logscale)["match_mu_caloiso_"] = true;
	(*logscale)["match_mu_trackiso_"] = true;
	(*logscale)["match_mu_CombRelIso_"] = true;
	(*logscale)["unmatch_mu_CombRelIso_"] = true;
	(*logscale)["unmatch_mu_caloiso_"] = true;
	(*logscale)["unmatch_mu_trackiso_"] = true;
	(*logscale)["match_e_caloiso_"] = true;
	(*logscale)["match_e_trackiso_"] = true;
	(*logscale)["unmatch_e_caloiso_"] = true;
	(*logscale)["unmatch_e_trackiso_"] = true;
	(*logscale)["match_e_CombRelIso_"] = true;
	(*logscale)["unmatch_e_CombRelIso_"] = true;
	if(cutset == "01_cutset" || cutset == "02_cutset"){
		(*logscale)["jet1_pt_"] = true;
		(*logscale)["jet2_pt_"] = true;
		(*logscale)["jet3_pt_"] = true;
		(*logscale)["jet4_pt_"] = true;
		(*logscale)["muon_pt_"] = true;
		(*logscale)["muon_d0_"] = true;
		(*logscale)["muon_eta_"] = true;
		(*logscale)["jet1_eta_"] = true;
		(*logscale)["jet2_eta_"] = true;
		(*logscale)["jet3_eta_"] = true;
		(*logscale)["jet4_eta_"] = true;
	}

	std::vector<int> *colours = new std::vector<int>();
	if(c_type == "s" || c_type == "c"){
        	colours->push_back(kRed-3);
        	colours->push_back(kYellow-7);
	}
	if(c_type == "b" || c_type == "c"){
        	colours->push_back(kWhite-1);
        	colours->push_back(kGreen-3);
        	colours->push_back(kCyan-6);
        	colours->push_back(kBlue+1);
        	colours->push_back(kBlue-1);
        	colours->push_back(kBlue-2);
        	colours->push_back(kBlue-3);
	}

	std::cout << "booking HistoCombiner()" << std::endl;
	HistoCombiner *hc = new HistoCombiner();
	hc->set_mass(mass);
	hc->set_logscale(logscale);
	hc->set_cutset(cutset);
	hc->set_sequence_to_plot(sequence_to_plot);
	hc->set_colours(colours);
	hc->set_histo_rebin(3);
	hc->add_histos(files_with_content,histo_names);
	if(electron){
		hc->set_cross_section("TTbar|e_background|"+cutset,414*0.317885795);
		hc->set_cross_section("Wjets|e_background|"+cutset,8740*0.00220573277);
		hc->set_cross_section("Zjets|e_background|"+cutset,1940*0.0000227412106);
		hc->set_cross_section("QCDbctoe20to30|e_background|"+cutset,192000*0.0000524365591);
		hc->set_cross_section("QCDbctoe30to80|e_background|"+cutset,240000*0.00432262072);
		hc->set_cross_section("QCDbctoe80to170|e_background|"+cutset,22800*0.0548185111);
		hc->set_cross_section("QCDem20to30|e_background|"+cutset,3200000*0.0000455359587);
		hc->set_cross_section("QCDem30to80|e_background|"+cutset,4700000*0.00119063931);
		hc->set_cross_section("QCDem80to170|e_background|"+cutset,285000*0.0259624124);
	}else{
// 		hc->set_cross_section("TTbar|mu_background|"+cutset,414*0.592961487383798*(1506/(529750/100)));
// 		hc->set_cross_section("Wjets|mu_background|"+cutset,8180*0.00297075867581429*(75065/(2082633/20)));
// 		hc->set_cross_section("Zjets|mu_background|"+cutset,1940*0.00376390076988879*(11690/(2000500/100)));
// 		// FIXME change e- qcd background cross section
// 		hc->set_cross_section("Mupt15|mu_background|"+cutset,364000*0.0852273654699078*(1531609/5204815));

		hc->set_cross_section("TTbar|mu_background|"+cutset,414*0.16857008);
		hc->set_cross_section("Wjets|mu_background|"+cutset,8180*0.00214151989);
		hc->set_cross_section("Zjets|mu_background|"+cutset,1940*0.00219945014);
		// FIXME change e- qcd background cross section
		hc->set_cross_section("Mupt15|mu_background|"+cutset,364000*0.0250796618);

	}
	hc->scale_to_cross_section(20);
	hc->combine_histos();
	hc->write_histos();

	delete sequence_to_plot;
	sequence_to_plot=NULL;
	delete colours;
	colours=NULL;
	delete logscale;
	logscale = NULL;

	return 0;
}
