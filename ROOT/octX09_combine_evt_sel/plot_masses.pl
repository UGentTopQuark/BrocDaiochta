#!/usr/bin/perl

@masses = (250,300,350,400);
#@masses = (175);
@cutsets = ("01_cutset","02_cutset","03_cutset","04_cutset","05_cutset", "06_cutset","07_cutset","08_cutset","09_cutset");
#@cutsets = ("04_cutset");

foreach $cutset(@cutsets){
	foreach $mass (@masses){
		print "./combine mu s $mass $cutset \n";
		`./combine mu s $mass $cutset`;
		$command = "mkdir signal_muon_$cutset";
		print "$command\n";
		`$command`;
		$command = "mv *.eps signal_muon_$cutset/";
		print "$command\n";
		`$command`;

		print "./combine mu c $mass $cutset \n";
		`./combine mu c $mass $cutset`;
		$command = "mkdir muon_$cutset";
		print "$command\n";
		`$command`;
		$command = "mv *.eps muon_$cutset/";
		print "$command\n";
		`$command`;

		print "./combine mu b $mass $cutset \n";
		`./combine mu b $mass $cutset`;
		$command = "mkdir background_muon_$cutset";
		print "$command\n";
		`$command`;
		$command = "mv *.eps background_muon_$cutset/";
		print "$command\n";
		`$command`;

		#$command = "./convert_to_pdf.sh";
		#print "$command\n";
		#`$command`;

		$command = "mkdir $mass"."_hypo";
		print "$command\n";
		`$command`;
		$command = "mv muon_* $mass"."_hypo";
		print "$command\n";
		`$command`;
		$command = "mv electron_* $mass"."_hypo";
		print "$command\n";
		`$command`;
		$command = "mv signal_* $mass"."_hypo";
		print "$command\n";
		`$command`;
		$command = "mv background_* $mass"."_hypo";
		print "$command\n";
		`$command`;
	}
}
