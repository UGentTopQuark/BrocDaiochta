#!/usr/bin/perl

@cutsets = ();

for($i=1; $i < 10; $i++){
	push(@cutsets,"0".$i."_cutset");
}
for($i=10; $i <= 18; $i++){
	push(@cutsets,$i."_cutset");
}
#push(@cutsets, "04_cutset");

$mass = 175;

foreach $cutset (@cutsets){
	print "./combine mu c $mass $cutset \n";
	print "mkdir muon_$cutset \n";
	print "mv *.eps muon_$cutset \n";
	`./combine mu c $mass $cutset`;
	`mkdir muon_$cutset`;
	`mv *.eps muon_$cutset`;

	print "./combine mu b $mass $cutset \n";
	print "mkdir background_muon_$cutset \n";
	print "mv *.eps background_muon_$cutset \n";
	`./combine mu b $mass $cutset`;
	`mkdir background_muon_$cutset`;
	`mv *.eps background_muon_$cutset`;
	
	print "./combine mu s $mass $cutset \n";
	print "mkdir signal_muon_$cutset \n";
	print "mv *.eps signal_muon_$cutset \n";
	`./combine mu s $mass $cutset`;
	`mkdir signal_muon_$cutset`;
	`mv *.eps signal_muon_$cutset`;

#	print "./combine e c $cutset \n";
#	print "mkdir electron_$cutset \n";
#	print "mv *.eps electron_$cutset \n";
#	`./combine e c $cutset`;
#	`mkdir electron_$cutset`;
#	`mv *.eps electron_$cutset`;

#	print "./combine e s $mass $cutset \n";
#	print "mkdir signal_electron_$cutset \n";
#	print "mv *.eps signal_electron_$cutset \n";
#	`./combine e s $mass $cutset`;
#	`mkdir signal_electron_$cutset`;
#	`mv *.eps signal_electron_$cutset`;
#
#	print "./combine e b  $mass $cutset \n";
#	print "mkdir background_electron_$cutset \n";
#	print "mv *.eps background_electron_$cutset \n";
#	`./combine e b $mass $cutset`;
#	`mkdir background_electron_$cutset`;
#	`mv *.eps background_electron_$cutset`;
}
