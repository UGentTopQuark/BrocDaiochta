#!/usr/bin/perl

$suffix = $ARGV[0];
$dir = '.';

die "Aborting." unless($suffix);

die "no suffix: $suffix" unless($suffix);
print "Add suffix: $suffix [N/y]? ";
$answer = <STDIN>;
chomp $answer;
die "Aborting." unless($answer eq 'y' || $answer eq 'Y');

opendir(DIR, $dir);
while(defined($file = readdir(DIR))){
	next unless ($file =~ /\.root$/);
	@filename = split(/\./, $file);
	$new_filename = $filename[0]."_".$suffix.'.'.$filename[1];
	print "mv $file $new_filename\n";
	`mv $file $new_filename`;
}
closedir(DIR);
