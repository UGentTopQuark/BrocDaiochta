#!/bin/bash
# convert all files from eps to pdf
for i in `ls -d background_* electron_* signal_* muon_*`; do
   cd /home/bklein/root/combine_evt_sel/${i} && for j in `ls *.eps`; do
         epstopdf ${j};
	 done; done; cd /home/bklein/root/combine_evt_sel/
