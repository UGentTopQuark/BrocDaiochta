#include "Logarithmiser.h"

TH1F* Logarithmiser::logarithmise_histo(TH1F *histo)
{
	for(int bin = 0; bin != histo->GetNbinsX(); ++bin){
		double bin_content = histo->GetBinContent(bin);
		if(bin_content != 0)
			histo->SetBinContent(bin, TMath::Log(bin_content));
	}

	return histo;
}
