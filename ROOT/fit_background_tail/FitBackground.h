#ifndef FITBACKGROUND_H
#define FITBACKGROUND_H

#include "TF1.h"
#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"

#include <RooGlobalFunc.h>
#include <RooRealVar.h>
#include <RooChi2Var.h>
#include <RooGaussian.h>
#include <RooPlot.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooExponential.h>
#include <RooAddPdf.h>
#include <RooFitResult.h>
#include <RooArgusBG.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooLandau.h>
#include <RooMinuit.h>

class FitBackground{
	public:
		FitBackground(TFile *outfile);
		void fit(TH1F *histo, std::string cutset);

	private:
		void print_integral(TH1F *histo, std::string cutset);
		void fit_roofit_exp(TH1F *histo, std::string cutset);
		void fit_roofit_straight_line(TH1F *histo, std::string cutset);
		void fit_root_exp(TH1F *histo, std::string cutset);
		void fit_root_straight_line(TH1F *histo, std::string cutset);
		TFile *outfile;

		double mcut;
		double mup;
		double fit_max;
};

#endif
