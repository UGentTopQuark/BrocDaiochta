#ifndef LOGARITHMISER_H
#define LOGARITHMISER_H

#include "TH1F.h"
#include "TMath.h"

class Logarithmiser{
	public:
		TH1F* logarithmise_histo(TH1F *histo);
	private:
};

#endif
