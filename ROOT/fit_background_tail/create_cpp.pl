#!/usr/bin/perl

#	1 = x-axis => ht/chi^2 cuts
#	0 = x-axis => event numbers
$x_cut_value=0;

# JEC * 1.00
#$event_numbers{'03_cutset'}=3472;
#$event_numbers{'15_cutset'}=1989;
#
#$event_numbers{'09_cutset'}=2845;
#$event_numbers{'10_cutset'}=2172;
#$event_numbers{'11_cutset'}=986;
#
#$event_numbers{'12_cutset'}=1604;
#$event_numbers{'13_cutset'}=1199;
#$event_numbers{'14_cutset'}=473;

#############################
## END configuration
#############################

use Tie::IxHash::Easy;

$m3 = "";

@line = ();
while (<>) 
{
	push(@lines, $_) if (/cutset/);
	$m3 = "0b" if(/s3nobtag/);
	$m3 = "1b" if(/s3btag/);
}

$ht{'09_cutset'}=300;
$ht{'10_cutset'}=400;
$ht{'11_cutset'}=500;
$ht{'03_cutset'}=10000 unless($x_cut_value);

$ht{'12_cutset'}=300;
$ht{'13_cutset'}=400;
$ht{'14_cutset'}=500;
$ht{'15_cutset'}=10000 unless($x_cut_value);

tie %ht_A, "Tie::IxHash::Easy";
tie %ht_a, "Tie::IxHash::Easy";
tie %ht_AERR, "Tie::IxHash::Easy";
tie %ht_aERR, "Tie::IxHash::Easy";
tie %chi_A, "Tie::IxHash::Easy";
tie %chi_a, "Tie::IxHash::Easy";
tie %chi_AERR, "Tie::IxHash::Easy";
tie %chi_aERR, "Tie::IxHash::Easy";

foreach $line(@lines){
	if($line =~ /(\d+_cutset) A: ([\-\d.]+) AERR: ([\-\d.]+) a: ([\-\d.]+) aERR: ([\-\d.]+)/){
		if(defined($ht{$1})){
			$ht_A{$1} = $2;
			$ht_AERR{$1} = $3;
			$ht_a{$1} = $4;
			$ht_aERR{$1} = $5;
		}
		if(defined($chi{$1})){
			$chi_A{$1} = $2;
			$chi_AERR{$1} = $3;
			$chi_a{$1} = $4;
			$chi_aERR{$1} = $5;
		}
	}elsif($line =~ /(\d+_cutset) mcut: ([\-\d\.]+) mup: ([\-\d\.]+) N1: ([\-\d\.]+) N2: ([\-\d\.]+) N3: ([\-\d\.]+)/){
		if(defined($ht{$1}) || defined($chi{$1})){
			$mcut{$1} = $2;
			$mup{$1} = $3;
			$N1{$1} = $4;
			$N2{$1} = $5;
			$N3{$1} = $6;
		}
	}
}

if(keys %ht_A){
	print "double ht_A".$m3."[".(keys %ht_A)."];\n";
	print "double ht_AERR".$m3."[".(keys %ht_A)."];\n";
	print "double ht_a".$m3."[".(keys %ht_A)."];\n";
	print "double ht_aERR".$m3."[".(keys %ht_A)."];\n";
	print "double ht_x_A".$m3."[".(keys %ht_A)."];\n";
	print "double ht_x_a".$m3."[".(keys %ht_A)."];\n";
	print "double ht_x_err".$m3."[".(keys %ht_A)."];\n\n";

	my $i = 0;
	foreach $key(keys %ht_A){
		print "ht_A".$m3."[$i] = ".$ht_A{$key}.";\n";
		print "ht_AERR".$m3."[$i] = ".$ht_AERR{$key}.";\n";
		print "ht_a".$m3."[$i] = ".$ht_a{$key}.";\n";
		print "ht_aERR".$m3."[$i] = ".$ht_aERR{$key}.";\n";
		print "ht_x".$m3."[$i] = $ht{$key};\n" if($x_cut_value);
		$x_a = (1.0/($mup{$key}-$mcut{$key}))*log($N2{$key}/$N3{$key});
		print "ht_x_a".$m3."[$i] = $x_a;\n" unless($x_cut_value);
		print "ht_x_A".$m3."[$i] = $N2{$key};\n" unless($x_cut_value);
		print "ht_x_err".$m3."[$i] = 0;\n";
		print "\n";
		$i++;
	}

	print "TGraphErrors graph_A_ht".$m3."(".(keys %ht_A).", ht_x_A".$m3.", ht_A".$m3.", ht_x_err".$m3.", ht_AERR".$m3.");\n";
	print "TGraphErrors graph_a_ht".$m3."(".(keys %ht_A).", ht_x_a".$m3.", ht_a".$m3.", ht_x_err".$m3.", ht_aERR".$m3.");\n";

	print "\n";
}

if(keys %chi_A){
	print "double chi_A".$m3."[".(keys %chi_A)."];\n";
	print "double chi_AERR".$m3."[".(keys %chi_A)."];\n";
	print "double chi_a".$m3."[".(keys %chi_A)."];\n";
	print "double chi_aERR".$m3."[".(keys %chi_A)."];\n";
	print "double chi_x_A".$m3."[".(keys %chi_A)."];\n";
	print "double chi_x_a".$m3."[".(keys %chi_A)."];\n";
	print "double chi_x_err".$m3."[".(keys %chi_A)."];\n\n";

	my $i = 0;
	foreach $key(keys %chi_A){
		print "chi_A".$m3."[$i] = ".$chi_A{$key}.";\n";
		print "chi_AERR".$m3."[$i] = ".$chi_AERR{$key}.";\n";
		print "chi_a".$m3."[$i] = ".$chi_a{$key}.";\n";
		print "chi_aERR".$m3."[$i] = ".$chi_aERR{$key}.";\n";
		print "chi_x".$m3."[$i] = $chi{$key};\n" if($x_cut_value);
		print "chi_x_a".$m3."[$i] = $N1{$key};\n" unless($x_cut_value);
		print "chi_x_A".$m3."[$i] = $N3{$key};\n" unless($x_cut_value);
		print "chi_x_err".$m3."[$i] = 0;\n";
		print "\n";
		$i++;
	}

	print "TGraphErrors graph_A_chi".$m3."(".(keys %chi_A).", chi_x_A".$m3.", chi_A".$m3.", chi_x_err".$m3.", chi_AERR".$m3.");\n";
	print "TGraphErrors graph_a_chi".$m3."(".(keys %chi_A).", chi_x_a".$m3.", chi_a".$m3.", chi_x_err".$m3.", chi_aERR".$m3.");\n";
	print "\n";
}
