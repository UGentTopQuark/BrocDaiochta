#!/usr/bin/perl

#$event_numbers{'03_cutset'}=3472;
#$event_numbers{'15_cutset'}=1989;
#
#$event_numbers{'09_cutset'}=2845;
#$event_numbers{'10_cutset'}=2172;
#$event_numbers{'11_cutset'}=986;
#
#$event_numbers{'12_cutset'}=1604;
#$event_numbers{'13_cutset'}=1199;
#$event_numbers{'14_cutset'}=473;
#
#############################
## END configuration
#############################

use Tie::IxHash::Easy;

$m3 = "";

@line = ();
while (<>) 
{
	push(@lines, $_) if (/cutset/);
	$m3 = "0b" if(/s3nobtag/);
	$m3 = "1b" if(/s3btag/);
}

$ht{'09_cutset'}=300;
$ht{'10_cutset'}=350;
$ht{'11_cutset'}=400;
$ht{'03_cutset'}='\infty';

$ht{'12_cutset'}=300;
$ht{'13_cutset'}=350;
$ht{'14_cutset'}=400;
$ht{'15_cutset'}='\infty';

tie %ht_A, "Tie::IxHash::Easy";
tie %ht_a, "Tie::IxHash::Easy";
tie %ht_AERR, "Tie::IxHash::Easy";
tie %ht_aERR, "Tie::IxHash::Easy";
tie %chi_A, "Tie::IxHash::Easy";
tie %chi_a, "Tie::IxHash::Easy";
tie %chi_AERR, "Tie::IxHash::Easy";
tie %chi_aERR, "Tie::IxHash::Easy";

foreach $line(@lines){
	if($line =~ /(\d+_cutset) A: ([\-\d.]+) AERR: ([\-\d.]+) a: ([\-\d.]+) aERR: ([\-\d.]+)/){
		if(defined($ht{$1})){
			$ht_A{$1} = $2;
			$ht_AERR{$1} = $3;
			$ht_a{$1} = $4;
			$ht_aERR{$1} = $5;
		}
		if(defined($chi{$1})){
			$chi_A{$1} = $2;
			$chi_AERR{$1} = $3;
			$chi_a{$1} = $4;
			$chi_aERR{$1} = $5;
		}
	}elsif($line =~ /(\d+_cutset) mcut: ([\-\d\.]+) mup: ([\-\d\.]+) N1: ([\-\d\.]+) N2: ([\-\d\.]+) N3: ([\-\d\.]+)/){
		if(defined($ht{$1}) || defined($chi{$1})){
			$mcut{$1} = $2;
			$mup{$1} = $3;
			$N1{$1} = $4;
			$N2{$1} = $5;
			$N3{$1} = $6;
		}
	}
}

if(keys %ht_A){
	print <<EOF;
\\tiny{
\\begin{tabular}{c|cccccc}
EOF
	print '$H_T$-cut [(s3)] & $N_\\mathrm{cut}$ & $N_\\mathrm{up}$ & $N_0$ & $a$ & X & A\\\\'."\n" if($m3 eq '0b');
	print '$H_T$-cut [(s3)+b-tag] & $N_\\mathrm{cut}$ & $N_\\mathrm{up}$ & $N_0$ & $a$ & X & A\\\\'."\n" if($m3 eq '1b');
	print '\hline'."\n";
	
	foreach $key(keys %ht_A){
		my $X = (1.0/($mup{$key}-$mcut{$key}))*log($N2{$key}/$N3{$key}); 
		my $A = ($ht_A{$key}*$ht_a{$key})*10; 
		my $AERR = $A*(sqrt(($ht_AERR{$key}/$ht_A{$key})*($ht_AERR{$key}/$ht_A{$key}) + ($ht_aERR{$key}/$ht_a{$key})*($ht_aERR{$key}/$ht_a{$key}))); 
		#print '$'.$ht{$key}.'$ & $'.$N2{$key}.'$ & $'.$ht_A{$key}.' \pm '.$ht_AERR{$key}.'$ & $'.$ht_a{$key}.' \pm '.$ht_aERR{$key}.'$ & $'.$X.'$ \\\\';
		printf "\$ $ht{$key} \$ & \$ %.2f\$ & \$ %.2f\$ & \$ %.2f \\pm %.2f \$ & \$ %.4f \\pm %.4f \$ & \$ %.3f \$ & \$ %.2f \\pm %.2f \$\\\\",
			 $N2{$key}, $N3{$key}, $ht_A{$key}, $ht_AERR{$key}, $ht_a{$key}, $ht_aERR{$key}, $X, $A, $AERR;
		print "\n";
	}

	print <<EOF;
\\end{tabular}
}
EOF

}

if(keys %chi_A){
	print <<EOF;
\\tiny{
\\begin{tabular}{c|cc}
EOF
	print '$\chi^2$-cut & $A$ [min.diff.] & $a$ [min.diff.]\\\\'."\n" unless($m3);
	print '$\chi^2$-cut & $A$ [M3] & $a$ [M3]\\\\'."\n" if($m3);
	print '\hline'."\n";
	
	foreach $key(keys %chi_A){
		print '$'.$chi{$key}.'$ & $'.$chi_A{$key}.' \pm '.$chi_AERR{$key}.'$ & $'.$chi_a{$key}.' \pm '.$chi_aERR{$key}.'$\\\\';
		print "\n";
	}

	print <<EOF;
\\end{tabular}
}
EOF

}
