#include "FitBackground.h"

FitBackground::FitBackground(TFile *outfile)
{
	this->outfile = outfile;

	/*
	 * fit range: mcut to fit_max
	 * mup just for technical reasons defined here but arbitrary
	 */
	mcut = 200.0;
	mup = 300.0;
	fit_max = 500.0;
}

void FitBackground::fit(TH1F *histo, std::string cutset)
{
//	fit_roofit_exp(histo, cutset);
//	fit_roofit_straight_line(histo, cutset);
	fit_root_exp(histo, cutset);
//	fit_root_straight_line(histo, cutset);
	print_integral(histo, cutset);
}

void FitBackground::print_integral(TH1F *histo, std::string cutset)
{
	double max = histo->GetXaxis()->GetXmax();
	double N1 = 0;
	double N2 = 0;
	double N3 = 0;
	N1 = histo->Integral(histo->GetXaxis()->FindBin(mcut), histo->GetXaxis()->FindBin(mup));
	N2 = histo->Integral(histo->GetXaxis()->FindBin(mcut), histo->GetXaxis()->FindBin(max));
	N3 = histo->Integral(histo->GetXaxis()->FindBin(mup), histo->GetXaxis()->FindBin(max));
	std::cout << cutset << " mcut: " << mcut << " mup: " << mup << " N1: " << N1 << " N2: " << N2 << " N3: " << N3 << std::endl;
}

void FitBackground::fit_root_straight_line(TH1F *histo, std::string cutset)
{
	outfile->cd();
        TCanvas *c1 = new TCanvas("fit", "canvas", 640, 480);
        c1->cd();

	TF1 *myfit = new TF1("myfit","[0]+[1]*x", 250, 400);
	myfit->SetParameter(0,1500.0);
	myfit->SetParameter(1,100.0);

	histo->Fit("myfit","","",250.0,400.0);

	std::cout << cutset << " A: " << myfit->GetParameter(0) << " AERR: " << myfit->GetParError(0) << " a: " << myfit->GetParameter(1) << " aERR: " << myfit->GetParError(1) << std::endl;
	
	c1->SaveAs("fit.eps");
	c1->Write();

	histo->Write();
}

void FitBackground::fit_root_exp(TH1F *histo, std::string cutset)
{
	//TEST
	std::cout <<"Int " <<  histo->Integral() << std::endl;
	double max = histo->GetXaxis()->GetXmax();
	outfile->cd();
        TCanvas *c1 = new TCanvas("fit", "canvas", 640, 480);
        c1->cd();

	//FIXME: 10 == bin_width, switch to variable
	TF1 *myfit = new TF1("myfit","[0]*[1]*exp((x-200)*-1*[1])*10", mcut, max);
	//TF1 *myfit = new TF1("myfit","[0]*exp((x-200)*[1])", 200, 500);
//	TF1 *myfit = new TF1("myfit","expo", 0., 1000);
	myfit->SetParameter(0,150.0);
	myfit->SetParameter(1,0.01);
	//myfit->SetParameter(1,100.);

	histo->Fit("myfit","E","",mcut,max);

	myfit->Draw("same");

	std::cout << cutset << " A: " << myfit->GetParameter(0) << " AERR: " << myfit->GetParError(0) << " a: " << myfit->GetParameter(1) << " aERR: " << myfit->GetParError(1) << std::endl;
	
	//histo->GetYaxis()->SetLimits(0., histo->GetXaxis()->GetXmax());
	//double y_max = 300;

	//histo->SetAxisRange(0., y_max, "Y");
	c1->SaveAs("fit.eps");
	c1->Write();

	histo->Write();
}

void FitBackground::fit_roofit_exp(TH1F *histo, std::string cutset)
{
	using namespace RooFit;

	outfile->cd();
        TCanvas *c1 = new TCanvas("fit", "canvas", 640, 480);
        c1->cd();

	RooRealVar x("x","reconstructed mass",0,600);
	RooRealVar A("A","A",1500., 0., 100000.0); 
	RooRealVar a("a","a",-0.01,-5.0,5.0); 

	RooExponential function("function", "function", x, a);
//	RooGenericPdf function("function", "exp(-x*a)", RooArgSet(x, a));
	RooExtendPdf norm_function("norm_function", "norm_function", function, A);

	RooDataHist* data = new RooDataHist("data","data",x,histo);

	norm_function.fitTo(*data,Range(200,500));

	std::cout << cutset << " A: " << A.getVal() << " AERR: " << A.getError() << " a: " << a.getVal() << " aERR: " << a.getError() << std::endl;

	RooPlot* xframe = x.frame(Name("background tail"),Title("background tail"));
	xframe->UseCurrentStyle();
	xframe->SetYTitle("Events");
	data->plotOn(xframe);
	norm_function.plotOn(xframe,LineColor(kRed),LineStyle(kDashed));
	xframe->Draw();
	
	c1->SaveAs("fit.eps");
	c1->Write();

	delete data;

	histo->Write();
}

void FitBackground::fit_roofit_straight_line(TH1F *histo, std::string cutset)
{
	using namespace RooFit;

	outfile->cd();
        TCanvas *c1 = new TCanvas("fit", "canvas", 640, 480);
        c1->cd();

	RooRealVar x("x","reconstructed mass",000,600);
	RooRealVar A("A","A",0.0,10000.0); 
	RooRealVar a("a","a",0.0,100.0); 
	RooRealVar b("b","b",0.0,1000.0); 

	RooGenericPdf function("function", "-x*a+b", RooArgSet(x, a, b));
	RooExtendPdf norm_function("norm_function", "norm_function", function, A);

	RooDataHist* data = new RooDataHist("data","data",x,histo);

	norm_function.fitTo(*data,Range(200,600));

	std::cout << cutset << " A: " << A.getVal() << " AERR: " << A.getError() << " a: " << a.getVal() << " aERR: " << a.getError() << std::endl;

	RooPlot* xframe = x.frame(Name("background tail"),Title("background tail"));
	xframe->UseCurrentStyle();
	xframe->SetYTitle("Events");
	data->plotOn(xframe);
	norm_function.plotOn(xframe,LineColor(kRed),LineStyle(kDashed));
	xframe->Draw();
	
	c1->SaveAs("fit.eps");
	c1->Write();

	delete data;

	histo->Write();
}
