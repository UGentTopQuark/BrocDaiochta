#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "MergeHistos/MergeHistos.h"
#include "FitBackground.h"
#include "Logarithmiser.h"
#include "tdrstyle.h"

int main(int argc, char **argv)
{
	TFile *outfile = new TFile("outfile.root", "RECREATE");
	MergeHistos *mhistos = new MergeHistos(outfile);
	FitBackground *fitter = new FitBackground(outfile);
	Logarithmiser *logarithmiser = new Logarithmiser();
	std::string mass="";
	std::string cutset="";
	if(argc > 2){
		mass = argv[1];
		cutset = argv[2];
	}

	//std::string version="_jes095";
	//	std::string version="_20091026_175_jes_02";
	std::string version="_plots";
	//
	//std::string histo_name="top_mass_minimisation_";
	std::string histo_name="top_hadmass_Wb_0btag_";
	//std::string histo_name="top_Had_chimass_";
	std::cout << std::endl;
	std::cout << histo_name << "END" << std::endl;
	std::string directory_name="";

	std::map<std::string,std::vector<std::string> > file_names;
	//file_names["tprime_"+mass+version+".root"].push_back(histo_name+"Tprime"+mass+"|muon|"+cutset);
	//file_names["tprime_"+mass+version+".root"].push_back(histo_name+"Tprime"+mass+"|mu_background|"+cutset);
	file_names["wjets"+version+".root"].push_back(histo_name+"Wjets|mu_background|"+cutset);
	file_names["zjets"+version+".root"].push_back(histo_name+"Zjets|mu_background|"+cutset);
	file_names["ttbar"+version+".root"].push_back(histo_name+"TTbar|mu_background|"+cutset);
	file_names["qcdmupt15"+version+".root"].push_back(histo_name+"Mupt15|mu_background|"+cutset);
	file_names["t_s_chan"+version+".root"].push_back(histo_name+"TsChan|mu_background|"+cutset);
	file_names["t_t_chan"+version+".root"].push_back(histo_name+"TtChan|mu_background|"+cutset);
	file_names["t_tW_chan"+version+".root"].push_back(histo_name+"TtWChan|mu_background|"+cutset);

	mhistos->set_integrated_luminosity(200);
	mhistos->set_mass(mass);
	mhistos->set_cutset(cutset);
	mhistos->set_histo_name(histo_name);
	mhistos->set_file_names(file_names);

	setTDRStyle();
	//fitter->fit(logarithmiser->logarithmise_histo(mhistos->combine_histo(mass, cutset)), cutset);
	fitter->fit(mhistos->combine_histo<TH1F>(), cutset);
	setTDRStyle();

	delete outfile;
	outfile = NULL;
	delete fitter;
	fitter = NULL;
	delete logarithmiser;
	logarithmiser = NULL;
}
