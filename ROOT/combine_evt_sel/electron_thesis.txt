[global]
lepton_type = electron 		; which channel: muon/electron
cutset = 01_cutset	 	; cuts set
cross_section_set = Fall11sw	; which cross sections to use from CSProvider
ttbar_signal_dataset = TTbar	; name of ttbar dataset
data_dataset = Data		; name of dataset for data
scale_to_data = false
mc_datasets = TTbar:Wjets:Zjets:TtChan:TsChan:TtWChan:QCD
integrated_luminosity = 4628.087
directory = input/20120919_174322_e_nominal_analysis_eta1p5/

[additional_scale_factors]
TTbar = 0.9238
;Wjets = 0.6977
;Zjets = 0.6977
;SingleTop = 1.164
;;QCD = 0.52175095*2.346
;QCD = 1.2240277
QCD = 0.52175095

[edR]
name = min_e_dR_
rebin = 5
xaxis = 0.:3.5
 
[njets]
name = jet_number_
logscale = true
xaxis = 3.5:9.5

[nelectrons]
name = electron_number_

[electron_pt_]
name = electron_pt_
rebin = 2

[chi2_wo_btag_prob_]
name = chi2_wo_btag_prob_
rebin = 5

[chi2_wo_btag_]
name = chi2_wo_btag_
rebin = 5

[electron_abseta_]
name = electron_abseta_
rebin = 5

[electron_abseta_x_charge_]
name = electron_abseta_x_charge_
rebin = 10

[electron_eta_]
name = electron_eta_tf_
rebin = 10
xaxis = -1.5:1.5

[electron_phi_]
name = electron_phi_
; [electron_number_]
; name = electron_number_
; [electron_pt_]
; name = electron_pt_
; rebin = 2
[electron_d0_]
name = electron_d0_
xaxis = -0.02:0.02
  
[Ht_wo_MET_]
name = Ht_wo_MET_
xaxis = 100:900

[Event_MET_]
name = Event_MET_
 
[top_mass_tf_]
name = top_mass_tf_
rebin = 10
xaxis = 0:500
 
[jet_pt_]
name = jet_pt_
rebin = 2
;yaxis = 0:80

[npv_]
name = npv_
xaxis = -0.5:25.5

[pv_z_]
name = pv_z_

[jet_eta_]
name = jet_eta_
[jet_phi_]
name = jet_phi_

[jet1_pt_:jet_pt_]
name = jet1_pt_
[jet2_pt_:jet_pt_]
name = jet2_pt_
[jet3_pt_:jet_pt_]
name = jet3_pt_
[jet4_pt_:jet_pt_]
name = jet4_pt_
[jet1_eta_]
name = jet1_eta_
[jet2_eta_]
name = jet2_eta_
[jet3_eta_]
name = jet3_eta_
[jet4_eta_]
name = jet4_eta_
