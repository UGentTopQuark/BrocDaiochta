[global]
lepton_type = muon 		; which channel: muon/electron
cutset = 01_cutset	 	; cuts set
cross_section_set = Summer11_pfJets_emuisoveryloose	; which cross sections to use from CSProvider
ttbar_signal_dataset = TTbar 	; name of ttbar dataset
data_dataset = Data		; name of dataset for data
scale_to_data = false
mc_datasets = TTbar:Wjets:Zjets:TtChan:TsChan:TtWChan:QCD	; list of MC datasets
integrated_luminosity = 4944.147
directory = 20120312_0_100_fit_template_mu_match_scale_newlumiQCD/
dont_split_ttbar = false

[additional_scale_factors]
TTbar = 1.026
Wjets = 0.9227
Zjets = 0.9227
SingleTop = 1.126
QCD = 5.92668
 
;[PFreliso]
;name = mu_PFrelIso_
;#logscale = true
;xaxis = 0:0.45
;
; [PFreliso005]
; name = mu_PFrelIso0.05_
; logscale = false
; xaxis = 0:0.5
; 
; [PFreliso010]
; name = mu_PFrelIso0.1_
; logscale = false
; xaxis = 0:0.5
; 
; [PFreliso015]
; name = mu_PFrelIso0.15_
; logscale = false
; xaxis = 0:0.5
; 
; [PFreliso020]
; name = mu_PFrelIso0.2_
; logscale = false
; xaxis = 0:0.5
; 
; [PFreliso025]
; name = mu_PFrelIso0.25_
; logscale = false
; xaxis = 0:0.5
; 
; [PFreliso030]
; name = mu_PFrelIso0.3_
; logscale = false
; xaxis = 0:0.5
; 
; [PFreliso035]
; name = mu_PFrelIso0.35_
; logscale = false
; xaxis = 0:0.5
; 
; [PFreliso040]
; name = mu_PFrelIso0.4_
; logscale = false
; xaxis = 0:0.5
; 
; [PFreliso045]
; name = mu_PFrelIso0.45_
; logscale = false
; xaxis = 0:0.5
; 
; [PFreliso050]
; name = mu_PFrelIso0.5_
; logscale = false
; ; xaxis = 0:0.5
; 
; [Mj2nulT]
; name = Mj2nulT_
; 
; [Mevent]
; name = Mevent_
; rebin = 10
; 
; [H3T]
; name = H3T_
; 
; [HlT]
; name = HlT_
; rebin = 10
; 
; [sphericity]
; name = sphericity_
; rebin = 2
; 
; [aplanarity]
; name = aplanarity_

; ; [dphiemet]
; ; name = dphiemet_
; ; rebin = 4
; ; 
; ; [detaje]
; ; name = detaje_
; ; rebin = 2
; ; 
; ; [drje]
; ; name = drje_
; ; rebin = 4

; [dphimumet]
; name = dphimmet_
; rebin = 4
; 
; [detajm]
; name = detajmu_
; rebin = 2
; 
; [drjm]
; name = drjmu_
; rebin = 4

[njets]
name = jet_number_
logscale = true
xaxis = 3.5:9.5

[nmuons]
name = muon_number_

[muon_pt_]
name = muon_pt_
rebin = 2
; [min_mu_dR_]
; name = min_mu_dR_
[muon_d0_]
name = muon_d0_
xaxis = -0.02:0.02
[muon_nHits_]
name = muon_nHits_
[muon_chi2_]
name = muon_chi2_
[muon_eta_]
name = muon_eta_

; [chi2_wo_btag_]
; name = chi2_wo_btag_

[chi2_wo_btag_prob_]
name = chi2_wo_btag_prob_
rebin = 5

[muon_abseta_]
name = muon_abseta_
rebin = 5

[muon_abseta_x_charge_]
name = muon_abseta_x_charge_
rebin = 5

[muon_phi_]
name = muon_phi_
  
[Ht_wo_MET_]
name = Ht_wo_MET_
xaxis = 100:900

[Missing_Ht_]
name = Missing_Ht_
rebin = 5

[Event_MET_]
name = Event_MET_
 
[top_mass_tf_]
name = top_mass_tf_
rebin = 10
;xaxis = 0:500
 
[jet_pt_]
name = jet_pt_
rebin = 2
;yaxis = 0:80

[npv_]
name = npv_

[jet_eta_]
name = jet_eta_
[jet_phi_]
name = jet_phi_

[jet1_pt_:jet_pt_]
name = jet1_pt_
[jet2_pt_:jet_pt_]
name = jet2_pt_
[jet3_pt_:jet_pt_]
name = jet3_pt_
[jet4_pt_:jet_pt_]
name = jet4_pt_
[jet1_eta_]
name = jet1_eta_
[jet2_eta_]
name = jet2_eta_
[jet3_eta_]
name = jet3_eta_
[jet4_eta_]
name = jet4_eta_

; [event_discriminator]
; name = event_discriminator_
; ;yaxis = 0:1200
; rebin = 4
