#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include <sstream>
#include "PlotCombiner/HistoCombiner.h"
#include "BranchingRatio/BranchingRatio.h"
#include "CrossSectionProvider/CrossSectionProvider.h"
#include "ConfigReader/ConfigReader.h"

int main(int argc, char **argv)
{
	if(argc < 2){
		std::cout << "usage: " << argv[0] << " <config file> " << std::endl;
		exit(1);
	}

	std::string config_file(argv[1]);

	eire::ConfigReader *config_reader = new eire::ConfigReader();
	config_reader->read_config_from_file(config_file);
	std::string e_mu = config_reader->get_var("lepton_type", "global", true);
	std::string cutset = config_reader->get_var("cutset", "global", true);
	std::string cross_section_set = config_reader->get_var("cross_section_set", "global", true);
	std::string sig_id = config_reader->get_var("ttbar_signal_dataset", "global", true);
	double integrated_luminosity = atof(config_reader->get_var("integrated_luminosity", "global", true).c_str());
	std::vector<std::string> mc_datasets = config_reader->get_vec_var("mc_datasets", "global", true);
	std::string data_dataset = config_reader->get_var("data_dataset", "global", true);
	std::string directory = config_reader->get_var("directory", "global");
	bool dont_split_ttbar = config_reader->get_bool_var("dont_split_ttbar", "global",false);

	std::vector<std::string> *sections = config_reader->get_sections();

	std::string version = "_plots";

	std::string sig_bars = e_mu;
	std::string bkg_bars = "";
	if(e_mu == "electron"){
		std::cout << "plotting for electrons..." << std::endl;
		bkg_bars = "e_background";
	}else{
		std::cout << "plotting for muons..." << std::endl;
		bkg_bars = "mu_background";
	}

	std::vector<std::string> *sequence_to_plot = new std::vector<std::string>();

	std::map<std::string,std::vector<std::string> > files_with_content;
	// prepare files_with_content
	for(std::vector<std::string>::iterator mc_dataset = mc_datasets.begin();
		mc_dataset != mc_datasets.end();
		++mc_dataset){
		if(*mc_dataset == sig_id){
			files_with_content[directory+*mc_dataset+version+".root"].push_back(*mc_dataset+"|"+sig_bars+"|"+cutset);
			sequence_to_plot->push_back(sig_id+"|"+sig_bars+"|"+cutset);
		}
		if(*mc_dataset != sig_id || !dont_split_ttbar){	
			files_with_content[directory+*mc_dataset+version+".root"].push_back(*mc_dataset+"|"+bkg_bars+"|"+cutset);
			sequence_to_plot->push_back(*mc_dataset+"|"+bkg_bars+"|"+cutset);
		}
	}

	if(data_dataset != ""){
		//WARNING: Data files must always come after MC files.
  		files_with_content[directory+data_dataset+version+".root"].push_back(data_dataset+"|"+sig_bars+"|"+cutset);
  		sequence_to_plot->push_back(data_dataset+"|"+sig_bars+"|"+cutset);
	}

	std::vector<std::string> histo_names;
	std::map<std::string,bool> *logscale = new std::map<std::string,bool>();
	std::map<std::string,int> *rebin = new std::map<std::string,int>();
	std::map<std::string,std::pair<double,double> > *yaxes = new std::map<std::string,std::pair<double, double> >();
	std::map<std::string,std::pair<double,double> > *xaxes = new std::map<std::string,std::pair<double, double> >();

	std::string histo_name_var = "name";
	std::string logscale_var = "logscale";
	std::string rebin_var = "rebin";
	std::string xaxis_var = "xaxis";
	std::string yaxis_var = "yaxis";

	// loop over all sections defining plots
	for(std::vector<std::string>::iterator section = sections->begin();
		section != sections->end();
		++section){
		if(*section == "global" || *section == "additional_scale_factors") continue;	// skip global section

		std::string histo_name = config_reader->get_var(histo_name_var, *section, true);
		histo_names.push_back(histo_name);

		(*logscale)[histo_name] = config_reader->get_bool_var(logscale_var, *section, false);
		std::string rebin_string = config_reader->get_var(rebin_var, *section, false);
		if(rebin_string != ""){
			(*rebin)[histo_name] = atoi(rebin_string.c_str());
		}
		std::vector<std::string> xaxis = config_reader->get_vec_var(xaxis_var, *section, false);
		if(xaxis.size() == 2){
			(*xaxes)[histo_name] = std::pair<double,double>(atof(xaxis[0].c_str()),atof(xaxis[1].c_str()));
		}
		std::vector<std::string> yaxis = config_reader->get_vec_var(yaxis_var, *section, false);
		if(yaxis.size() == 2){
			(*yaxes)[histo_name] = std::pair<double,double>(atof(yaxis[0].c_str()),atof(yaxis[1].c_str()));
		}
	}
       
	std::vector<int> *colours = new std::vector<int>();
	colours->push_back(kRed-3);
	colours->push_back(kYellow-7);
	colours->push_back(kWhite+8);
	colours->push_back(kGreen-3);
	colours->push_back(kCyan-6);
	colours->push_back(kBlue-1);
	colours->push_back(kBlue-2);
//	colours->push_back(kBlue-3);

	//Make sure data is always plotted black
	if(sequence_to_plot->size() < colours->size() && data_dataset != "")
		(*colours)[sequence_to_plot->size() - 1] = kBlack;

	std::cout << "booking HistoCombiner()" << std::endl;
	CrossSectionProvider *cs_prov = new CrossSectionProvider();

	// if we have a TTbar dataset we need to calculate the branching ratio of the signal channel
	double e_BR = 1.;
	double mu_BR = 1.;
	if(sig_id != "" && !dont_split_ttbar){
		TFile *sig_file = new TFile((directory+sig_id+version+".root").c_str(), "OPEN");
		BranchingRatio *br_ratio = new BranchingRatio();
		br_ratio->set_file(sig_file);
		br_ratio->set_analysis(cross_section_set);

		//e_BR = br_ratio->get_branching_ratio_electron();
		//mu_BR = br_ratio->get_branching_ratio_muon();
		e_BR = 0.146864425858859;
		mu_BR = 0.146864425858859;

		std::cout << "Branchin Ratio muons: " << mu_BR << std::endl;
		std::cout << "Branchin Ratio electrons: " << e_BR << std::endl;

		if(sig_file){delete sig_file; sig_file = NULL;}
		if(br_ratio){delete br_ratio; br_ratio = NULL;}
	}

	// invoke HistoCombiner to do the actual combining and plot generation step
	HistoCombiner *hc = new HistoCombiner();
	hc->set_scale_mc_to_data(config_reader->get_bool_var("scale_to_data"));
	hc->set_logscale(logscale);
	hc->set_cutset(cutset);
	hc->set_sequence_to_plot(sequence_to_plot);
	hc->set_colours(colours);	
	hc->set_rebin(rebin);
	hc->set_yaxes(yaxes);
	hc->set_xaxes(xaxes);
	hc->set_luminosity(integrated_luminosity);
	hc->add_plots(files_with_content,histo_names);
	cs_prov->set_analysis(cross_section_set);
	
	//Get additional scale factors if set
	double sf_ttbar=1,sf_wjets=1,sf_zjets=1,sf_single_top=1,sf_qcd = 1;
	std::string ssf_ttbar = config_reader->get_var("TTbar","additional_scale_factors",false);
	if(ssf_ttbar != ""){sf_ttbar = atof(ssf_ttbar.c_str());}
	std::string ssf_wjets = config_reader->get_var("Wjets","additional_scale_factors",false);
	if(ssf_wjets != ""){sf_wjets = atof(ssf_wjets.c_str());}
	std::string ssf_zjets = config_reader->get_var("Zjets","additional_scale_factors",false);
	if(ssf_zjets != ""){sf_zjets = atof(ssf_zjets.c_str());}
	std::string ssf_single_top = config_reader->get_var("SingleTop","additional_scale_factors",false);
	if(ssf_single_top != ""){sf_single_top = atof(ssf_single_top.c_str());}
	std::string ssf_qcd = config_reader->get_var("QCD","additional_scale_factors",false);
	if(ssf_qcd != ""){sf_qcd = atof(ssf_qcd.c_str());}

	if(sig_bars == "electron"){
		hc->set_cross_section("TTbar|"+sig_bars+"|"+cutset,cs_prov->get_cross_section("TTbar")*e_BR*cs_prov->get_filter_efficiency("TTbar_"+sig_bars)*sf_ttbar);
		if(!dont_split_ttbar){
				      hc->set_cross_section("TTbar|"+bkg_bars+"|"+cutset,cs_prov->get_cross_section("TTbar")*(1-e_BR)*cs_prov->get_filter_efficiency("TTbar_"+bkg_bars)*sf_ttbar);
				      }
	}if(sig_bars == "muon"){
		hc->set_cross_section("TTbar|"+sig_bars+"|"+cutset,cs_prov->get_cross_section("TTbar")*mu_BR*cs_prov->get_filter_efficiency("TTbar_"+sig_bars)*sf_ttbar);
		if(!dont_split_ttbar){
			hc->set_cross_section("TTbar|"+bkg_bars+"|"+cutset,cs_prov->get_cross_section("TTbar")*(1-mu_BR)*cs_prov->get_filter_efficiency("TTbar_"+bkg_bars)*sf_ttbar);
		}
	}
	hc->set_cross_section("Wjets|"+bkg_bars+"|"+cutset,cs_prov->get_cross_section("Wjets")*cs_prov->get_filter_efficiency("Wjets")*sf_wjets);
	hc->set_cross_section("Zjets|"+bkg_bars+"|"+cutset,cs_prov->get_cross_section("Zjets")*cs_prov->get_filter_efficiency("Zjets")*sf_zjets);
	hc->set_cross_section("DY50|"+bkg_bars+"|"+cutset,cs_prov->get_cross_section("DY50")*cs_prov->get_filter_efficiency("DY50")*sf_zjets);
	//When using qcdbcem.root set bctoe20to30 to -1. scaling will be skipped
	hc->set_cross_section("QCDbctoe30to80|"+bkg_bars+"|"+cutset,-1.);
	hc->set_cross_section("QCDem30to80|"+bkg_bars+"|"+cutset,-1.);
	hc->set_cross_section("QCDem20to30|"+bkg_bars+"|"+cutset,-1.);
	hc->set_cross_section("QCDbctoe20to30|"+bkg_bars+"|"+cutset,-1.);
	hc->set_cross_section("QCDem80to170|"+bkg_bars+"|"+cutset,-1.);
	hc->set_cross_section("QCDbctoe80to170|"+bkg_bars+"|"+cutset,-1.);
	hc->set_cross_section("MuQCD|mu_background|"+cutset,cs_prov->get_cross_section("MuQCD")*cs_prov->get_filter_efficiency("MuQCD"));
	hc->set_cross_section("TsChan|"+bkg_bars+"|"+cutset,cs_prov->get_cross_section("TsChan")*cs_prov->get_filter_efficiency("TsChan")*sf_single_top);
	hc->set_cross_section("TtChan|"+bkg_bars+"|"+cutset,cs_prov->get_cross_section("TtChan")*cs_prov->get_filter_efficiency("TtChan")*sf_single_top);
	hc->set_cross_section("TtWChan|"+bkg_bars+"|"+cutset,cs_prov->get_cross_section("TtWChan")*cs_prov->get_filter_efficiency("TtWChan")*sf_single_top);
	if(e_mu == "e")
		hc->set_cross_section("QCD|"+bkg_bars+"|"+cutset,sf_qcd/integrated_luminosity);
	else
		hc->set_cross_section("QCD|"+bkg_bars+"|"+cutset,sf_qcd/integrated_luminosity);
	


	hc->scale_to_cross_section();
	hc->combine_histos();
	hc->write_histos();

	// clean up
	if(config_reader){delete config_reader; config_reader = NULL;}
	if(sequence_to_plot){delete sequence_to_plot; sequence_to_plot=NULL;}
	if(colours){delete colours; colours=NULL;}
	if(logscale){delete logscale; logscale = NULL;}
	if(rebin){delete rebin; rebin = NULL;}
	if(yaxes){delete yaxes; yaxes = NULL;}
	if(xaxes){delete xaxes; xaxes = NULL;}
	if(cs_prov){delete cs_prov; cs_prov = NULL;}

	return 0;
}
