#include "../interface/MParticle.h"

void mor::Particle::set_beag_info(beag::Particle &beag_particle)
{
	SetPx(beag_particle.px);
	SetPy(beag_particle.py);
	SetPz(beag_particle.pz);
	SetE(beag_particle.e);

	mc_matched_val = beag_particle.mc_matched;
	from_ttbar_decay = beag_particle.from_ttbar_decay;

	mc_id = beag_particle.mc_id;

	mc_match_p4.SetPx(beag_particle.mc_px);
	mc_match_p4.SetPy(beag_particle.mc_py);
	mc_match_p4.SetPz(beag_particle.mc_pz);
	mc_match_p4.SetE(beag_particle.mc_e);
}
