#include "../interface/MMET.h"

mor::MET::MET(beag::MET &beag_met)
{
	SetPx(beag_met.px);
	SetPy(beag_met.py);
	// no pz component calculated so far
//	SetPz(beag_met.pz);
	SetE(beag_met.e);
}
