#include "../../interface/EventSelection/WbMass.h"

void WbMass::calculate_mass()
{
	if(hadT_mass != -1)
		return;

	int njets = jets->size();

	if(njets < 4)
		return;

	if(max_njets != 4){
		std::cerr << "WbMass::calculate_mass() max_njets != 4... setting to 4." << std::endl;
		max_njets = 4;
	}

	double const W_nominal_mass = 80.0;

	double min_W_diff = -1;
	double curr_W_diff = -1;
	int jet_id1=-1, jet_id2=-1, jet_id3=-1, jet_id4=-1;
	for(int j=0;j < njets && j < max_njets; ++j){
		for(int k=0;k < njets && k < max_njets; ++k){
			if(j == k) continue;
			curr_W_diff = fabs(W_nominal_mass-W_Had_candidate_mass(j, k));
			if((curr_W_diff < min_W_diff) || min_W_diff == -1){
				min_W_diff = curr_W_diff;
				jet_id1 = j;
				jet_id2 = k;
			}
		}
	}

	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > W_Had_candidate;
  
	W_Had_candidate = (*jets)[jet_id1].p4()+(*jets)[jet_id2].p4();	

	/*
	 *	dR assignment method
	 */
	 /*
	// this holds only for 4 jets
	int all_ids[4]={0};
	all_ids[jet_id1] = 1;
	all_ids[jet_id2] = 1;

	int remaining_1 = -1;
	int remaining_2 = -1;

	bool first = true;
	for(int i = 0; i < max_njets; ++i){
		if(all_ids[i] != 1){
			if(first){
				remaining_1 = i;
				first = false;
			}
			else
				remaining_2 = i;
		}
	}

	if(ROOT::Math::VectorUtil::DeltaR(W_Had_candidate, (*jets)[remaining_1].p4()) > ROOT::Math::VectorUtil::DeltaR(W_Had_candidate, (*jets)[remaining_2].p4())){
		jet_id3 = remaining_2;
		jet_id4 = remaining_1;
	}else{
		jet_id3 = remaining_1; 
		jet_id4 = remaining_2;
	}
	*/

	/*
	 *	M3 Method
	 */
	double max_pt = -1;
	
	for(int k=0;k < njets && k < max_njets; ++k){
	        int i = jet_id1;
	        int j = jet_id2;
	
	  if(i != j && i != k && j != k){
	    double current_pt = ((*jets)[i].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).pt();
	
	    if(current_pt > max_pt){
	      jet_id1=i;
	      jet_id2=j;
	      jet_id3=k;
	      max_pt=current_pt;
	    }
	  }
	}
	
	jet_id4 = (0+1+2+3) - (jet_id1+jet_id2+jet_id3);

	hadW_mass = W_Had_candidate.mass();
	hadT_mass = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);
	lepT_mass = top_Lep_candidate_mass(jet_id4);
	
	mass_jet_ids->push_back(jet_id3);
	mass_jet_ids->push_back(jet_id1);
	mass_jet_ids->push_back(jet_id2);
	mass_jet_ids->push_back(jet_id4);

	event_is_processed = true;
}

void WbMass::calculate_mass_1btag()
{
	if(hadT_mass != -1)
		return;

	int njets = jets->size();

	if(njets < 4)
		return;

	if(max_njets != 4){
		std::cerr << "WbMass::calculate_mass() max_njets != 4... setting to 4." << std::endl;
		max_njets = 4;
	}

	//find most likely bjet
	std::vector<std::pair<int, double> >* bjets = bjet_finder->get_btag_sorted_jets();

	int firstbtag = -1;

	if(bjets->size() < 1)
		return;
	else{
		for(std::vector<std::pair<int, double> >::iterator bjet = bjets->begin();
			bjet != bjets->end();
			++bjet){
			if(bjet->first < max_njets){
				firstbtag = bjet->first;
			}
		}
	}

	if(firstbtag == -1)
		return;

	double const W_nominal_mass = 80.0;

	double min_W_diff = -1;
	double curr_W_diff = -1;
	int jet_id1=-1, jet_id2=-1, jet_id3=-1, jet_id4=-1;
	for(int j=0;j < njets && j < max_njets; ++j){
		for(int k=0;k < njets && k < max_njets; ++k){
			if(firstbtag != j && firstbtag != k){
				if(j == k) continue;
				curr_W_diff = fabs(W_nominal_mass-W_Had_candidate_mass(j, k));
				if((curr_W_diff < min_W_diff) || min_W_diff == -1){
					min_W_diff = curr_W_diff;
					jet_id1 = j;
					jet_id2 = k;
				}
			}
		}
	}

	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > W_Had_candidate;
  
	W_Had_candidate = (*jets)[jet_id1].p4()+(*jets)[jet_id2].p4();	

	/*
	 *	dR assignment method
	 */
	// this holds only for max_njets == 4
	// futhermore it's wrong if b-jet not one of 4 leading jets
	/*
	int remaining_jet = (0+1+2+3) - (jet_id1+jet_id2+firstbtag);

	if(ROOT::Math::VectorUtil::DeltaR(W_Had_candidate, (*jets)[firstbtag].p4()) > ROOT::Math::VectorUtil::DeltaR(W_Had_candidate, (*jets)[remaining_jet].p4())){
		jet_id3 = remaining_jet;
		jet_id4 = firstbtag;
	}else{
		jet_id3 = firstbtag; 
		jet_id4 = remaining_jet;
	}
	*/

	/*
	 *	M3 Method
	 */
	double max_pt = -1;
	
	for(int k=0;k < njets && k < max_njets; ++k){
	        int i = jet_id1;
	        int j = jet_id2;
	
	  if(i != j && i != k && j != k){
	    double current_pt = ((*jets)[i].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).pt();
	
	    if(current_pt > max_pt){
	      jet_id1=i;
	      jet_id2=j;
	      jet_id3=k;
	      max_pt=current_pt;
	    }
	  }
	}
	
	jet_id4 = (0+1+2+3) - (jet_id1+jet_id2+jet_id3);

	hadW_mass_1btag = W_Had_candidate.mass();
	hadT_mass_1btag = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);
	lepT_mass_1btag = top_Lep_candidate_mass(jet_id4);
	
	mass_jet_ids_1btag->push_back(jet_id3);
	mass_jet_ids_1btag->push_back(jet_id1);
	mass_jet_ids_1btag->push_back(jet_id2);
	mass_jet_ids_1btag->push_back(jet_id4);

	event_is_processed_1btag = true;
}

void WbMass::calculate_mass_2btag()
{
	if(hadT_mass != -1)
		return;

	int njets = jets->size();

	if(njets < 4)
		return;

	//find most likely bjet
	std::vector<std::pair<int, double> >* bjets = bjet_finder->get_btag_sorted_jets();

	int firstbtag = -1;
	int secondbtag = -1;

	if(bjets->size() < 2)
		return;
	else{
		for(std::vector<std::pair<int, double> >::iterator bjet = bjets->begin();
			bjet != bjets->end();
			++bjet){
			if(bjet->first < max_njets){
				if(firstbtag == -1){
					firstbtag = bjet->first;
				}else if(secondbtag == -1){
					secondbtag = bjet->first;
				}
			}
		}
	}

	if(firstbtag == -1 || secondbtag == -1)
		return;

	double const W_nominal_mass = 80.0;

	double min_W_diff = -1;
	double curr_W_diff = -1;
	int jet_id1=-1, jet_id2=-1, jet_id3=-1, jet_id4=-1;
	for(int j=0;j < njets && j < max_njets; ++j){
		for(int k=0;k < njets && k < max_njets; ++k){
			if(firstbtag != j && firstbtag != k &&
				secondbtag != j && secondbtag != k){
				if(j == k) continue;
				curr_W_diff = fabs(W_nominal_mass-W_Had_candidate_mass(j, k));
				if((curr_W_diff < min_W_diff) || min_W_diff == -1){
					min_W_diff = curr_W_diff;
					jet_id1 = j;
					jet_id2 = k;
				}
			}
		}
	}

	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > W_Had_candidate;
  
	W_Had_candidate = (*jets)[jet_id1].p4()+(*jets)[jet_id2].p4();	

	if(ROOT::Math::VectorUtil::DeltaR(W_Had_candidate, (*jets)[firstbtag].p4()) > ROOT::Math::VectorUtil::DeltaR(W_Had_candidate, (*jets)[secondbtag].p4())){
		jet_id3 = secondbtag;
		jet_id4 = firstbtag;
	}
	else{
		jet_id3 = firstbtag;
		jet_id4 = secondbtag;
	}

	hadW_mass_2btag = W_Had_candidate.mass();
	hadT_mass_2btag = top_Had_candidate_mass(jet_id1,jet_id2,jet_id3);
	lepT_mass_2btag = top_Lep_candidate_mass(jet_id4);
	
	mass_jet_ids_2btag->push_back(firstbtag);
	mass_jet_ids_2btag->push_back(jet_id1);
	mass_jet_ids_2btag->push_back(jet_id2);
	mass_jet_ids_2btag->push_back(secondbtag);

	event_is_processed_2btag = true;
}
