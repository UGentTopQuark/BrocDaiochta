#include "../../interface/EventSelection/BasicObjectPlots.h"

BasicObjectPlots::BasicObjectPlots(std::string ident)
{
	id = ident;

	ht = -1;
}

BasicObjectPlots::~BasicObjectPlots()
{
//	histos1d[("W_jets_assignment"+id).c_str()]->Sumw2();
//	histos1d[("W_jets_assignment"+id).c_str()]->Scale(1.0/histos1d[("W_jets_assignment"+id).c_str()]->Integral());
}

void BasicObjectPlots::plot_all()
{
	plot_jets();
	plot_muons();
	plot_electrons();
	plot_ht();
	plot_dR();
	plot_muon_quality();
	plot_electron_quality();
}

void BasicObjectPlots::set_uncut_jets(std::vector<mor::Jet>* uncut_jets)
{
	this->uncut_jets = uncut_jets;
}

void BasicObjectPlots::set_ht_calc(HtCalculator *ht_calc)
{
	this->ht_calc = ht_calc;
}

void BasicObjectPlots::book_histos()
{
	histos1d[("W_jets_assignment"+id).c_str()]=histo_writer->create_1d(("W_jets_assignment"+id).c_str(),"correctly identified W jets closest to W mass",5,-0.5,4.5);

	histos1d[("jet_number"+id).c_str()]=histo_writer->create_1d(("jet_number"+id).c_str(),"jet multiplicity",10,-0.5,9.5, "Jet Multiplicity");
	histos1d[("muon_number"+id).c_str()]=histo_writer->create_1d(("muon_number"+id).c_str(),"muon multiplicity ",10,-0.5,9.5, "Muon Multiplicity");
	histos1d[("muon_pt"+id).c_str()]=histo_writer->create_1d(("muon_pt"+id).c_str(),"p_{T} of muons in an event",70,0,350, "Muon p_{T} [GeV]");
	histos1d[("muon_eta"+id).c_str()]=histo_writer->create_1d(("muon_eta"+id).c_str(),"eta of muons in an event",40,-6.0,6.0, "Muon #eta");
	histos1d[("muon_phi"+id).c_str()]=histo_writer->create_1d(("muon_phi"+id).c_str(),"phi of muons in an event",40,-4.0,4.0, "Muon #phi");
	histos1d[("muon_d0"+id).c_str()]=histo_writer->create_1d(("muon_d0"+id).c_str(),"d0 of muons in an event",30,-0.3,0.3, "Muon impact parameter d_{0} [cm]");
	histos2d[("muon_d0_vs_phi"+id).c_str()]=histo_writer->create_2d(("muon_d0_vs_phi"+id).c_str(),"d0 vs phi of muons in an event",80,-4,4,80,-0.4,0.4, "Muon #phi", "Muon impact parameter d_{0}");
	histos1d[("muon_nHits"+id).c_str()]=histo_writer->create_1d(("muon_nHits"+id).c_str(),"number of Hits in the tracker of muons in an event",30,-0.5,29.5, "Muon number of tracker hits");
	histos1d[("muon_chi2"+id).c_str()]=histo_writer->create_1d(("muon_chi2"+id).c_str(),"normalised #chi^{2} of muons in an event",50,0.0,50, "Muon global normalised #chi^{2}");
	histos1d[("electron_number"+id).c_str()]=histo_writer->create_1d(("electron_number"+id).c_str(),"electron multiplicity ",10,-0.5,9.5, "Electron Multiplicity");
	histos1d[("electron_pt"+id).c_str()]=histo_writer->create_1d(("electron_pt"+id).c_str(),"p_{T} of electron in an event",70,0,350, "Electron p_{T} [GeV]");
	histos1d[("electron_eta"+id).c_str()]=histo_writer->create_1d(("electron_eta"+id).c_str(),"eta of electron in an event",30,-6.0,6.0, "Electron #eta");
	histos1d[("electron_phi"+id).c_str()]=histo_writer->create_1d(("electron_phi"+id).c_str(),"phi of electrons in an event",50,-4.0,4.0, "Electron #phi");
	histos1d[("electron_d0"+id).c_str()]=histo_writer->create_1d(("electron_d0"+id).c_str(),"d0 of electrons in an event",30,-0.3,0.3, "Electron impact parameter d_{0} [cm]");
	histos2d[("electron_d0_vs_phi"+id).c_str()]=histo_writer->create_2d(("electron_d0_vs_phi"+id).c_str(),"d0 vs phi of electrons in an event",80,-4,4,80,-0.4,0.4, "Electron #phi", "Electon impact parameter d_{0} [cm]");

	histos1d[("Ht"+id).c_str()]=histo_writer->create_1d(("Ht"+id).c_str(),"Ht: Sum pt of all jets and muon and MET",80,0,2000, "H_{T}^{MET} [GeV]");
	histos1d[("Ht_wo_MET"+id).c_str()]=histo_writer->create_1d(("Ht_wo_MET"+id).c_str(),"Ht: Sum pt of all jets and muon",80,0,2000, "H_{T} [GeV]");

	histos1d[("jet_pt"+id).c_str()]=histo_writer->create_1d(("jet_pt"+id).c_str(),"p_{T} of jets in an event",50,0,500, "Jet p_{T} [GeV]");
	histos1d[("jet_eta"+id).c_str()]=histo_writer->create_1d(("jet_eta"+id).c_str(),"eta of jets in an event",15,-2.5,2.5, "Jet #eta");
	histos1d[("jet_phi"+id).c_str()]=histo_writer->create_1d(("jet_phi"+id).c_str(),"phi of jets in an event",50,-4.0,4.0, "Jet #phi");
  	histos1d[("jet1_pt"+id).c_str()]=histo_writer->create_1d(("jet1_pt"+id).c_str(),"p_{T} of leading jet",50,0,500, "Jet 1 p_{T} [GeV]");
	histos1d[("jet1_eta"+id).c_str()]=histo_writer->create_1d(("jet1_eta"+id).c_str(),"eta of leading jet",30,-6.0,6.0, "Jet 1 #eta");
	histos1d[("jet2_pt"+id).c_str()]=histo_writer->create_1d(("jet2_pt"+id).c_str(),"p_{T} of jet 2 ",50,0,500, "Jet 2 p_{T} [GeV]");
	histos1d[("jet2_eta"+id).c_str()]=histo_writer->create_1d(("jet2_eta"+id).c_str(),"eta of jet 2 ",30,-6.0,6.0, "Jet 2 #eta");
	histos1d[("jet3_pt"+id).c_str()]=histo_writer->create_1d(("jet3_pt"+id).c_str(),"p_{T} of jet 3",50,0,500, "Jet 3 p_{T} [GeV]");
	histos1d[("jet3_eta"+id).c_str()]=histo_writer->create_1d(("jet3_eta"+id).c_str(),"eta of jet 3 ",30,-6.0,6.0, "Jet 3 #eta");
	histos1d[("jet4_pt"+id).c_str()]=histo_writer->create_1d(("jet4_pt"+id).c_str(),"p_{T} of jet 4",50,0,500, "Jet 4 p_{T}");
	histos1d[("jet4_eta"+id).c_str()]=histo_writer->create_1d(("jet4_eta"+id).c_str(),"eta of jet 4 ",30,-6.0,6.0, "Jet 4 #eta");

	histos2d[("jet_pt_vs_et"+id).c_str()]=histo_writer->create_2d(("jet_pt_vs_et"+id).c_str(),"pt vs et for all jets",85, 0, 500, 85,0,500, "Jet p_{T} [GeV]", "Jet E_{T} [GeV]");
	histos2d[("jet1_pt_vs_jet2_pt"+id).c_str()]=histo_writer->create_2d(("jet1_pt_vs_jet2_pt"+id).c_str(),"1st jet pt vs 2nd jet pt",85, 0, 500, 85,0,500, "Jet 1 p_{T}", "Jet 2 p_{T}");

	histos1d[("jet_pt_id_truth"+id).c_str()]=histo_writer->create_1d(("jet_pt_id_truth"+id).c_str(),"id of truth matched jets in event",10,-0.5,9.5, "Top decay jets id");

	histos1d[("min_mu_dR"+id).c_str()]=histo_writer->create_1d(("min_mu_dR"+id).c_str(),"deltaR between muon(s) and closest jet",50,0,7,"#Delta R(#mu, closest jet)");

	histos1d[("min_e_dR"+id).c_str()]=histo_writer->create_1d(("min_e_dR"+id).c_str(),"deltaR for electron(s) and closest jet",50,0,7, "#Delta R(e, closest jet)");
	histos1d[("jet_e_dR"+id).c_str()]=histo_writer->create_1d(("jet_e_dR"+id).c_str(),"deltaR for uncut electrons and uncut jet",50,0,7, "#Delta R(e, closest jet) (w/o selection cuts)");

	histos1d[("top_had_jets_genmatched"+id).c_str()]=histo_writer->create_1d(("top_had_jets_genmatched"+id).c_str(),"Jets passing event which are really from semi-lep hadronic decay channel",10,-0.5, 9.5, "Number of jets from hadronic top decay after selection cuts");
	histos1d[("top_jets_genmatched"+id).c_str()]=histo_writer->create_1d(("top_jets_genmatched"+id).c_str(),"Jets passing event which are really from semi-lep decay channel",10,-0.5, 9.5, "Number of jets from t#bar{t} decay after selection cuts");

	//Figuring out whatdR cut should be between gen quark and reco jet, and sigmas for dD equation
	histos1d[("jet_reco_min_dR"+id).c_str()]=histo_writer->create_1d(("jet_reco_min_dR"+id).c_str(),"deltaR between closest reco jets",50,0,7, "#Delta R between closest jets in event");
	histos2d[("jet_delPt_vs_dR"+id).c_str()]=histo_writer->create_2d(("jet_delPt_vs_dR"+id).c_str(),"jet delPt vs dR",210, 0, 7,400, -2,2, "#Delta R(j1, j2)", "#Delta p_{T} (j1, j2)");
	histos2d[("jet_dR_vs_Pt"+id).c_str()]=histo_writer->create_2d(("jet_dR_vs_Pt"+id).c_str(),"jet reco Pt vs gen-reco dR",600,0, 300, 210, 0,7, "p_{T} jet_{reco} [GeV]", "#Delta R(j_{gen}, j_{reco})");
	histos2d[("jet_delPt_vs_dD"+id).c_str()]=histo_writer->create_2d(("jet_delPt_vs_dD"+id).c_str(),"jet delPt vs dD",210, 0, 7,400, -2,2,"#Delta D(j1, j2)", "#Delta p_{T} (j1, j2)");
	histos1d[("jet_dR_genreco"+id).c_str()]=histo_writer->create_1d(("jet_dR_genreco"+id).c_str(),"jet_dR_genreco",50,0,7, "#Delta R(j_{reco}, j_{gen}");
	histos1d[("jet_dPt_genreco"+id).c_str()]=histo_writer->create_1d(("jet_dPt_genreco"+id).c_str(),"jet_dPt_genreco dPt = dpt/pt_gen",100,-2,2, "#Delta p_{t}(j_{reco},j_{gen})");
	histos1d[("jet_deta_genreco"+id).c_str()]=histo_writer->create_1d(("jet_deta_genreco"+id).c_str(),"jet_deta_genreco ",200,-1,1, "#Delta #eta (j_{reco},j_{gen})");
	histos1d[("jet_dphi_genreco"+id).c_str()]=histo_writer->create_1d(("jet_dphi_genreco"+id).c_str(),"jet_dphi_genreco ",200,-1,1, "Delta #phi (j_{reco},j_{gen})");
	histos1d[("dR_Wquarks"+id).c_str()]=histo_writer->create_1d(("dR_Wquarks"+id).c_str(),"deltaR between W quarks",140,0,7, "#Delta R between quarks from W decay");
	histos1d[("min_dR_genquarks"+id).c_str()]=histo_writer->create_1d(("min_dR_genquarks"+id).c_str(),"deltaR between closest two quarks",140,0,7, "#Delta R between closet two quarks");

}

// Plot delta R between muon and closest jet
void BasicObjectPlots::plot_dR()
{
	for(std::vector<mor::Muon>::iterator mu_iter = isolated_muons->begin();
	    mu_iter != isolated_muons->end();
	    mu_iter++)
	{
		double min_dR = -1;
		double min_deta = -1;
		double min_dphi = -1;
		for(std::vector<mor::Jet>::iterator jet_iter = uncut_jets->begin();
		    jet_iter!=uncut_jets->end();
		    ++jet_iter)
			{
				double dR = ROOT::Math::VectorUtil::DeltaR(mu_iter->p4(),jet_iter->p4());
				if ((dR < min_dR) || (min_dR == -1))
					min_dR = dR;
				double deta = fabs(mu_iter->Eta()-jet_iter->Eta());
				if ((deta < min_deta) || (min_deta == -1))
					min_deta = deta;
				double dphi = fabs(ROOT::Math::VectorUtil::DeltaPhi(mu_iter->p4(),jet_iter->p4()));
				if ((dphi < min_dphi) || (min_dphi == -1))
					min_dphi = dphi;
				
			}
		histos1d[("min_mu_dR"+id).c_str()]->Fill(min_dR);
		return;
	}

	for(std::vector<mor::Electron>::iterator e_iter = isolated_electrons->begin();
	    e_iter != isolated_electrons->end();
	    e_iter++)
		{
			double min_dR = -1;
			for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();
			    jet_iter!=jets->end();
			    ++jet_iter)
				{
					double dR = ROOT::Math::VectorUtil::DeltaR(e_iter->p4(),jet_iter->p4());
					if ((dR < min_dR) || (min_dR = -1))
						min_dR = dR;
				}
			histos1d[("min_e_dR"+id).c_str()]->Fill(min_dR);
		}
	
}

void BasicObjectPlots::analyse_recogen_matchedjets()
{
	if(!gen_evt) return;
	
	int num_had_jets=0;
	int num_top_jets=0;
	for(std::vector<mor::Jet>::iterator jet = jets->begin();
		jet != jets->end();
		++jet){
		int decay_prod = jet->ttbar_decay_product();
		if(decay_prod > 0 && decay_prod < 4) ++num_had_jets;
		if(decay_prod > 0 ) ++num_top_jets;
	}

	// is out of 3 had top jets
	histos1d[("top_had_jets_genmatched"+id).c_str()]->Fill(num_had_jets);
	// event contains all 4 jets from top
	histos1d[("top_jets_genmatched"+id).c_str()]->Fill(num_top_jets);
	
	/*
	 *	test hypothesis: 2 jets closest to W mass in event are jets
	 *	that come really from W decay
	 */

	int njets = jets->size();

	if(njets < 2)
		return;

	double min_mass_diff = -1;
	double j1_id = -1;
	double j2_id = -1;
	double W_mass = 80.0;
	for(int i = 0; i < njets; ++i){
		for(int j = i+1; j < njets; ++j){
			if(i != j){
				double curr_diff = ((*jets)[i].p4() + (*jets)[j].p4()).mass() - W_mass;
				if(curr_diff < min_mass_diff || min_mass_diff == -1){
					min_mass_diff = curr_diff;
					j1_id = i;
					j2_id = j;
				}
			}
		}
	}
	
	int ncorrect_Wjets=0;

	if((*jets)[j1_id].ttbar_decay_product()) ++ncorrect_Wjets;
	if((*jets)[j2_id].ttbar_decay_product()) ++ncorrect_Wjets;

	histos1d[("W_jets_assignment"+id).c_str()]->Fill(ncorrect_Wjets);

	/*
	 *	test hypothesis: highest 4 pt jets in event are from top decay
	 */

	for(unsigned int i = 0; i <= jets->size() && i < 4; ++i){
		if((*jets)[i].ttbar_decay_product())
			histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(i);
	}
}

// Plot transverse energy:sum of all jets and the muon pt
void BasicObjectPlots::plot_ht()
{
	if(ht == -1)
		ht_calc->get_ht();
	
	histos1d[("Ht"+id).c_str()]->Fill(ht+corrected_mets->begin()->Pt());
	histos1d[("Ht_wo_MET"+id).c_str()]->Fill(ht);
}

void BasicObjectPlots::plot_muon_quality()
{
	for(std::vector<mor::Muon>::iterator muon_iter = isolated_muons->begin();
		muon_iter != isolated_muons->end();
		++muon_iter){

		double d0 = -1000;
		if(muon_iter->track_available()){
			histos1d[("muon_chi2"+id).c_str()]->Fill(muon_iter->chi2());
			histos1d[("muon_nHits"+id).c_str()]->Fill(muon_iter->nHits());

			d0 = muon_iter->d0();
			histos2d[("muon_d0_vs_phi"+id).c_str()]->Fill(muon_iter->Phi(),d0);
			histos1d[("muon_d0"+id).c_str()]->Fill(d0);
		}
	}
}


void BasicObjectPlots::plot_electron_quality()
{
	for(std::vector<mor::Electron>::iterator electron_iter = isolated_electrons->begin();
		electron_iter != isolated_electrons->end();
		++electron_iter){

		double d0 = -1000;
		if(electron_iter->track_available()){
			d0 = electron_iter->d0();
			histos2d[("electron_d0_vs_phi"+id).c_str()]->Fill(electron_iter->Phi(),d0);
			histos1d[("electron_d0"+id).c_str()]->Fill(d0);
		}
	}
}
//plot pt, eta, and numbet of jets
void BasicObjectPlots::plot_jets()
{
	int njets = jets->size();
	
	if(njets >= 1){
		histos1d[("jet1_pt"+id).c_str()]->Fill((*jets)[0].Pt());
		histos1d[("jet1_eta"+id).c_str()]->Fill((*jets)[0].Eta());
	}

	if(njets >= 2){
		histos1d[("jet2_pt"+id).c_str()]->Fill((*jets)[1].Pt());
		histos1d[("jet2_eta"+id).c_str()]->Fill((*jets)[1].Eta());
	}

	if(njets >= 3){
		histos1d[("jet3_pt"+id).c_str()]->Fill((*jets)[2].Pt());
		histos1d[("jet3_eta"+id).c_str()]->Fill((*jets)[2].Eta());
	}
	if(njets >= 4){
		histos1d[("jet4_pt"+id).c_str()]->Fill((*jets)[3].Pt());
		histos1d[("jet4_eta"+id).c_str()]->Fill((*jets)[3].Eta());
	}
	

	for(std::vector<mor::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		histos2d[("jet_pt_vs_et"+id).c_str()]->Fill(jet_iter->Pt(),jet_iter->Et());
		histos1d[("jet_pt"+id).c_str()]->Fill(jet_iter->Pt());
		histos1d[("jet_eta"+id).c_str()]->Fill(jet_iter->Eta());
		histos1d[("jet_phi"+id).c_str()]->Fill(jet_iter->Phi());
	}
	
	histos1d[("jet_number"+id).c_str()]->Fill(njets);
}

 // Plot eta,pt and number of muons
void BasicObjectPlots::plot_muons()
{
	// Get back vector of muons that passed all cuts in LeptonSelector class
   
	for(std::vector<mor::Muon>::iterator isolated_mu = isolated_muons->begin();
	    isolated_mu!=isolated_muons->end(); 
	    ++isolated_mu){
                histos1d[("muon_pt"+id).c_str()]->Fill(isolated_mu->Pt());
	        histos1d[("muon_eta"+id).c_str()]->Fill(isolated_mu->Eta());
	        histos1d[("muon_phi"+id).c_str()]->Fill(isolated_mu->Phi());
        }
	
	histos1d[("muon_number"+id).c_str()]->Fill(isolated_muons->size());

}

// Plot eta,pt and number of electrons
void BasicObjectPlots::plot_electrons()
{
	// Get back vector of electrons that passed all cuts in LeptonSelector class
	
	for(std::vector<mor::Electron>::iterator isolated_e = isolated_electrons->begin();isolated_e != isolated_electrons->end();++isolated_e){
                histos1d[("electron_pt"+id).c_str()]->Fill(isolated_e->Pt());
	        histos1d[("electron_eta"+id).c_str()]->Fill(isolated_e->Eta());
	        histos1d[("electron_phi"+id).c_str()]->Fill(isolated_e->Phi());
        }

	histos1d[("electron_number"+id).c_str()]->Fill(isolated_electrons->size());

}

