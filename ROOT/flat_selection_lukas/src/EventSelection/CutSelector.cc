#include "../../interface/EventSelection/CutSelector.h"

CutSelector::CutSelector(std::string ident, double mass)
{
        tprime_mass = mass;
	dataset_id = ident;

	define_cuts_autogen();
	complete_cuts();
	set_cuts();

	ttmu_count = 0,tte_count = 0,ttbg_count = 0,data_count=0;
	//To check if this event has already been called for different
	//cut.Reinitiliased and end of event in plot()
	Event_firstcall = true;
}

CutSelector::~CutSelector()
{
	/*
	 *	delete vector cuts
	 */
	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
	    type_iter != plot_generators.end();
	    ++type_iter)
	{
        	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
        	    pgen_iter != type_iter->second.end();
        	    ++pgen_iter){
			delete pgen_iter->second;
			pgen_iter->second=NULL;
		}
	}

	for(std::map<std::string, std::map<std::string, Cuts*> >::iterator type_iter = cuts.begin();
	    type_iter != cuts.end();
	    ++type_iter)
	{
		for(std::map<std::string,Cuts*>::iterator cuts_iter = type_iter->second.begin();
        	    cuts_iter != type_iter->second.end();
        	    ++cuts_iter){
			cuts_iter->second->print_cuts();
			delete cuts_iter->second;
			cuts_iter->second=NULL;
		}
	}

	for(std::vector<std::vector<double>* >::iterator del_it = cuts_to_be_deleted.begin();
	    del_it != cuts_to_be_deleted.end();
	    ++del_it){
		delete *del_it;
	}

	//Print number of ttbar muon,electron and backgound
	std::cout << "=++++++++++Event-Count:"<< dataset_id <<"++++++++++=" << std::endl;
	std::cout << "ttbar_mu_events: " << ttmu_count << std::endl;
	std::cout << "ttbar_e_events: " << tte_count << std::endl;
	std::cout << "ttbar_bg_events: " << ttbg_count << std::endl;
	std::cout << "data_events: " << data_count << std::endl;
	std::cout << "=++++++++++++++++++++++++++++++=" << std::endl;
}

void CutSelector::plot()
{

	Event_firstcall = true;
 	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
 	    type_iter != plot_generators.end();
 	    ++type_iter)
 	{
         	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
         	    pgen_iter != type_iter->second.end();
         	    ++pgen_iter){
 			if((dataset_id == "Data" && get_event_type() != "uncalled")|| 
			   type_iter->first == get_event_type() ||
 			   type_iter->first == ("e_"+get_event_type()) ||
 			   type_iter->first == ("mu_"+get_event_type()) ||
 			   (type_iter->first == "e_background" && get_event_type() == "muon") ||
 			   (type_iter->first == "mu_background" && get_event_type() == "electron") ){
 				pgen_iter->second->next_event();
 				pgen_iter->second->plot();
			}
				
 		}
 	}
}

std::string CutSelector::get_event_type()
{
	if(dataset_id == "Data")
		{
			if(Event_firstcall == 1){
				event_counter_histo->Fill(4);
				data_count++;
				Event_firstcall = false;
			}
				return "unused_string";
		}

	int decay_channel = genEvt->decay_channel();
	if(decay_channel == 2)
		{
			if (Event_firstcall == 1){
				event_counter_histo->Fill(1);
				ttmu_count++;
				Event_firstcall = false;
			}
			return "muon";
		}
	if(decay_channel == 1)
		{
			if (Event_firstcall == 1){
				event_counter_histo->Fill(2);
				tte_count++;
				Event_firstcall = false;
			}
			return "electron";
			
		}	  
	else
		{
			if (Event_firstcall == 1){
				event_counter_histo->Fill(3);
				ttbg_count++;
				Event_firstcall = false;
			}
			return "background";
		}
}

void CutSelector::set_gen_evt(mor::TTbarGenEvent *gen_evt)
{
	this->genEvt = gen_evt;

	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
	    type_iter != plot_generators.end();
	    ++type_iter)
	{
        	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
        	    pgen_iter != type_iter->second.end();
        	    ++pgen_iter){
			pgen_iter->second->set_gen_evt(gen_evt);
		}
	}
}

void CutSelector::set_handles(std::vector<mor::Muon> *muons,
			std::vector<mor::Jet> *jets,
			std::vector<mor::Electron> *electrons,
			std::vector<mor::MET> *mets,
			mor::Trigger *trigger)
{
        this->electrons = electrons;
        this->muons = muons;
        this->jets = jets;
        this->mets = mets;
	this->trigger = trigger;

	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
	    type_iter != plot_generators.end();
	    ++type_iter)
	{
        	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
        	    pgen_iter != type_iter->second.end();
        	    ++pgen_iter){
			pgen_iter->second->set_handles(muons, jets, electrons, mets, trigger);
		}
	}
	for(std::map<std::string, std::map<std::string, Cuts*> >::iterator type_iter = cuts.begin();
	    type_iter != cuts.end();
	    ++type_iter)
	{
		for(std::map<std::string,Cuts*>::iterator cuts_iter = type_iter->second.begin();
        	    cuts_iter != type_iter->second.end();
        	    ++cuts_iter){
			cuts_iter->second->set_handles(muons, jets, electrons, mets, trigger);
		}
	}
}

void CutSelector::define_cuts_autogen()
{
	// ---AUTOGENERATED BEGIN---
	// 23/03/2010 17:10 CUTSET_GENERATOR configuration: 20100308_cuts.txt
	// step: s00
	vcuts("muon","00_cutset","trigger", -1);

	// step: s01
	vcuts("muon","01_cutset","trigger", 80);
	vcuts("muon","01_cutset","min_mu_pt", 20.);
	cut_defs["muon"]["01_cutset"]["min_nisolated_mu"] = 1;
	vcuts("muon","01_cutset","max_mu_eta", 2.1);
	cut_defs["muon"]["01_cutset"]["mu_type"] = 0;
	cut_defs["muon"]["01_cutset"]["min_no_jets"] = 4;
	cut_defs["muon"]["01_cutset"]["max_jet_eta"] = 2.4;
	vcuts("muon","01_cutset","min_jet_pt", 30.);

	// step: s02
	vcuts("muon","02_cutset","trigger", 80);
	vcuts("muon","02_cutset","min_mu_pt", 30.);
	cut_defs["muon"]["02_cutset"]["min_nisolated_mu"] = 1;
	vcuts("muon","02_cutset","max_mu_eta", 2.1);
	cut_defs["muon"]["02_cutset"]["mu_type"] = 0;
	cut_defs["muon"]["02_cutset"]["min_no_jets"] = 4;
	cut_defs["muon"]["02_cutset"]["max_jet_eta"] = 2.4;
	vcuts("muon","02_cutset","min_jet_pt", 30.);
	vcuts("muon","02_cutset","max_mu_d0", 0.02);

	// step: s03
	vcuts("muon","03_cutset","trigger", 80);
	vcuts("muon","03_cutset","min_mu_pt", 30.);
	cut_defs["muon"]["03_cutset"]["min_nisolated_mu"] = 1;
	vcuts("muon","03_cutset","max_mu_eta", 2.1);
	cut_defs["muon"]["03_cutset"]["mu_type"] = 0;
	cut_defs["muon"]["03_cutset"]["min_no_jets"] = 4;
	cut_defs["muon"]["03_cutset"]["max_jet_eta"] = 2.4;
	vcuts("muon","03_cutset","min_jet_pt", 30.);
	vcuts("muon","03_cutset","max_mu_d0", 0.02);
	cut_defs["muon"]["03_cutset"]["max_nisolated_mu"] = 1;
	cut_defs["muon"]["03_cutset"]["max_nisolated_e"] = 0;
	vcuts("muon","03_cutset","max_mu_trackiso", 3);
	vcuts("muon","03_cutset","max_mu_caliso", 1);
	vcuts("muon","03_cutset","max_e_trackiso", 5);
	vcuts("muon","03_cutset","max_e_caliso", 10);
	vcuts("muon","03_cutset","max_e_d0", 0.02);
	vcuts("muon","03_cutset","min_e_pt", 30.);
	vcuts("muon","03_cutset","max_e_eta", 2.4);

	// step: s04
	vcuts("muon","04_cutset","trigger", 80);
	vcuts("muon","04_cutset","min_mu_pt", 30.);
	cut_defs["muon"]["04_cutset"]["min_nisolated_mu"] = 1;
	vcuts("muon","04_cutset","max_mu_eta", 2.1);
	cut_defs["muon"]["04_cutset"]["mu_type"] = 0;
	cut_defs["muon"]["04_cutset"]["min_no_jets"] = 4;
	cut_defs["muon"]["04_cutset"]["max_jet_eta"] = 2.4;
	vcuts("muon","04_cutset","min_jet_pt", 30.);
	vcuts("muon","04_cutset","max_mu_d0", 0.02);
	cut_defs["muon"]["04_cutset"]["max_nisolated_mu"] = 1;
	cut_defs["muon"]["04_cutset"]["max_nisolated_e"] = 0;
	vcuts("muon","04_cutset","max_mu_trackiso", 3);
	vcuts("muon","04_cutset","max_mu_caliso", 1);
	vcuts("muon","04_cutset","max_e_trackiso", 5);
	vcuts("muon","04_cutset","max_e_caliso", 10);
	vcuts("muon","04_cutset","max_e_d0", 0.02);
	vcuts("muon","04_cutset","min_e_pt", 30.);
	vcuts("muon","04_cutset","max_e_eta", 2.4);
	cut_defs["muon"]["04_cutset"]["name_btag"] = 4;
	vcuts("muon","04_cutset","min_btag", 2.91);

	// step: s05
	vcuts("muon","05_cutset","trigger", 80);
	vcuts("muon","05_cutset","min_mu_pt", 30.);
	cut_defs["muon"]["05_cutset"]["min_nisolated_mu"] = 1;
	vcuts("muon","05_cutset","max_mu_eta", 2.1);
	cut_defs["muon"]["05_cutset"]["mu_type"] = 0;
	cut_defs["muon"]["05_cutset"]["min_no_jets"] = 4;
	cut_defs["muon"]["05_cutset"]["max_jet_eta"] = 2.4;
	vcuts("muon","05_cutset","min_jet_pt", 30.);
	vcuts("muon","05_cutset","max_mu_d0", 0.02);
	cut_defs["muon"]["05_cutset"]["max_nisolated_mu"] = 1;
	cut_defs["muon"]["05_cutset"]["max_nisolated_e"] = 0;
	vcuts("muon","05_cutset","max_mu_trackiso", 3);
	vcuts("muon","05_cutset","max_mu_caliso", 1);
	vcuts("muon","05_cutset","max_e_trackiso", 5);
	vcuts("muon","05_cutset","max_e_caliso", 10);
	vcuts("muon","05_cutset","max_e_d0", 0.02);
	vcuts("muon","05_cutset","min_e_pt", 30.);
	vcuts("muon","05_cutset","max_e_eta", 2.4);
	cut_defs["muon"]["05_cutset"]["name_btag"] = 4;
	vcuts("muon","05_cutset","min_btag", 4.);

	// step: s06
	vcuts("muon","06_cutset","trigger", 80);
	vcuts("muon","06_cutset","min_mu_pt", 30.);
	cut_defs["muon"]["06_cutset"]["min_nisolated_mu"] = 1;
	vcuts("muon","06_cutset","max_mu_eta", 2.1);
	cut_defs["muon"]["06_cutset"]["mu_type"] = 0;
	cut_defs["muon"]["06_cutset"]["min_no_jets"] = 4;
	cut_defs["muon"]["06_cutset"]["max_jet_eta"] = 2.4;
	vcuts("muon","06_cutset","min_jet_pt", 30.);
	vcuts("muon","06_cutset","max_mu_d0", 0.02);
	cut_defs["muon"]["06_cutset"]["max_nisolated_mu"] = 1;
	cut_defs["muon"]["06_cutset"]["max_nisolated_e"] = 0;
	vcuts("muon","06_cutset","max_mu_trackiso", 3);
	vcuts("muon","06_cutset","max_mu_caliso", 1);
	vcuts("muon","06_cutset","max_e_trackiso", 5);
	vcuts("muon","06_cutset","max_e_caliso", 10);
	vcuts("muon","06_cutset","max_e_d0", 0.02);
	vcuts("muon","06_cutset","min_e_pt", 30.);
	vcuts("muon","06_cutset","max_e_eta", 2.4);
	cut_defs["muon"]["06_cutset"]["name_btag"] = 4;
	vcuts("muon","06_cutset","min_btag", 5.23);

	// step: s07
	vcuts("muon","07_cutset","trigger", 80);
	vcuts("muon","07_cutset","min_mu_pt", 30.);
	cut_defs["muon"]["07_cutset"]["min_nisolated_mu"] = 1;
	vcuts("muon","07_cutset","max_mu_eta", 2.1);
	cut_defs["muon"]["07_cutset"]["mu_type"] = 0;
	cut_defs["muon"]["07_cutset"]["min_no_jets"] = 4;
	cut_defs["muon"]["07_cutset"]["max_jet_eta"] = 2.4;
	vcuts("muon","07_cutset","min_jet_pt", 30.);
	vcuts("muon","07_cutset","max_mu_d0", -1);
	cut_defs["muon"]["07_cutset"]["max_nisolated_mu"] = 1;
	cut_defs["muon"]["07_cutset"]["max_nisolated_e"] = 0;
	vcuts("muon","07_cutset","max_mu_trackiso", 3);
	vcuts("muon","07_cutset","max_mu_caliso", -1);
	vcuts("muon","07_cutset","max_e_trackiso", 5);
	vcuts("muon","07_cutset","max_e_caliso", 10);
	vcuts("muon","07_cutset","max_e_d0", 0.02);
	vcuts("muon","07_cutset","min_e_pt", 30.);
	vcuts("muon","07_cutset","max_e_eta", 2.4);
	cut_defs["muon"]["07_cutset"]["name_btag"] = 4;
	vcuts("muon","07_cutset","min_btag", 4.);

	// step: s00
	vcuts("electron","00_cutset","trigger", -1);

	// step: s01
	vcuts("electron","01_cutset","trigger", 50);
	cut_defs["electron"]["01_cutset"]["min_no_jets"] = 4;
	vcuts("electron","01_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["01_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["01_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","01_cutset","min_e_pt", 20.);
	vcuts("electron","01_cutset","max_e_eta", 2.4);

	// step: s02
	vcuts("electron","02_cutset","trigger", 50);
	cut_defs["electron"]["02_cutset"]["min_no_jets"] = 4;
	vcuts("electron","02_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["02_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["02_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","02_cutset","min_e_pt", 30.);
	vcuts("electron","02_cutset","max_e_eta", 2.4);
	vcuts("electron","02_cutset","max_e_d0", 0.02);
	vcuts("electron","02_cutset","max_e_trackiso", 1.0);
	vcuts("electron","02_cutset","max_e_caliso", 3.0);

	// step: s03
	vcuts("electron","03_cutset","trigger", 50);
	cut_defs["electron"]["03_cutset"]["min_no_jets"] = 4;
	vcuts("electron","03_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["03_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["03_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","03_cutset","min_e_pt", 30.);
	vcuts("electron","03_cutset","max_e_eta", 2.4);
	vcuts("electron","03_cutset","max_e_d0", 0.02);
	vcuts("electron","03_cutset","max_e_trackiso", 1.0);
	vcuts("electron","03_cutset","max_e_caliso", 3.0);
	cut_defs["electron"]["03_cutset"]["max_nisolated_e"] = 1;

	// step: s04
	vcuts("electron","04_cutset","trigger", 50);
	cut_defs["electron"]["04_cutset"]["min_no_jets"] = 4;
	vcuts("electron","04_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["04_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["04_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","04_cutset","min_e_pt", 30.);
	vcuts("electron","04_cutset","max_e_eta", 2.4);
	vcuts("electron","04_cutset","max_e_d0", 0.02);
	vcuts("electron","04_cutset","max_e_trackiso", 1.0);
	vcuts("electron","04_cutset","max_e_caliso", 3.0);
	cut_defs["electron"]["04_cutset"]["max_nisolated_e"] = 1;
	cut_defs["electron"]["04_cutset"]["min_jet_e_dR"] = 0.3;

	// step: s05
	vcuts("electron","05_cutset","trigger", 50);
	cut_defs["electron"]["05_cutset"]["min_no_jets"] = 4;
	vcuts("electron","05_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["05_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["05_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","05_cutset","min_e_pt", 30.);
	vcuts("electron","05_cutset","max_e_eta", 2.4);
	vcuts("electron","05_cutset","max_e_d0", 0.02);
	vcuts("electron","05_cutset","max_e_trackiso", 1.0);
	vcuts("electron","05_cutset","max_e_caliso", 3.0);
	cut_defs["electron"]["05_cutset"]["max_nisolated_e"] = 1;
	cut_defs["electron"]["05_cutset"]["min_jet_e_dR"] = 0.3;
	cut_defs["electron"]["05_cutset"]["max_nisolated_mu"] = 0;
	vcuts("electron","05_cutset","max_mu_trackiso", 3.0);
	vcuts("electron","05_cutset","max_mu_caliso", 1.0);
	vcuts("electron","05_cutset","max_mu_d0", 0.02);
	vcuts("electron","05_cutset","min_mu_pt", 30.);
	vcuts("electron","05_cutset","max_mu_eta", 2.1);
	cut_defs["electron"]["05_cutset"]["mu_type"] = 0;

	// step: s06
	vcuts("electron","06_cutset","trigger", 50);
	cut_defs["electron"]["06_cutset"]["min_no_jets"] = 4;
	vcuts("electron","06_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["06_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["06_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","06_cutset","min_e_pt", 30.);
	vcuts("electron","06_cutset","max_e_eta", 2.4);
	vcuts("electron","06_cutset","max_e_d0", 0.02);
	vcuts("electron","06_cutset","max_e_trackiso", 1.0);
	vcuts("electron","06_cutset","max_e_caliso", 3.0);
	cut_defs["electron"]["06_cutset"]["max_nisolated_e"] = 1;
	cut_defs["electron"]["06_cutset"]["min_jet_e_dR"] = 0.3;
	cut_defs["electron"]["06_cutset"]["max_nisolated_mu"] = 0;
	vcuts("electron","06_cutset","max_mu_trackiso", 3.0);
	vcuts("electron","06_cutset","max_mu_caliso", 1.0);
	vcuts("electron","06_cutset","max_mu_d0", 0.02);
	vcuts("electron","06_cutset","min_mu_pt", 30.);
	vcuts("electron","06_cutset","max_mu_eta", 2.1);
	cut_defs["electron"]["06_cutset"]["mu_type"] = 0;
	cut_defs["electron"]["06_cutset"]["Z_rejection_width"] = 20.;
	vcuts("electron","06_cutset","max_loose_e_trackiso", 2.0);
	vcuts("electron","06_cutset","max_loose_e_caliso", 5.0);
	vcuts("electron","06_cutset","min_loose_e_pt", 20.0);

	// step: s07
	vcuts("electron","07_cutset","trigger", 50);
	cut_defs["electron"]["07_cutset"]["min_no_jets"] = 4;
	vcuts("electron","07_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["07_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["07_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","07_cutset","min_e_pt", 30.);
	vcuts("electron","07_cutset","max_e_eta", 2.4);
	vcuts("electron","07_cutset","max_e_d0", 0.02);
	vcuts("electron","07_cutset","max_e_trackiso", 1.0);
	vcuts("electron","07_cutset","max_e_caliso", 3.0);
	cut_defs["electron"]["07_cutset"]["max_nisolated_e"] = 1;
	cut_defs["electron"]["07_cutset"]["min_jet_e_dR"] = 0.3;
	cut_defs["electron"]["07_cutset"]["max_nisolated_mu"] = 0;
	vcuts("electron","07_cutset","max_mu_trackiso", 3.0);
	vcuts("electron","07_cutset","max_mu_caliso", 1.0);
	vcuts("electron","07_cutset","max_mu_d0", 0.02);
	vcuts("electron","07_cutset","min_mu_pt", 30.);
	vcuts("electron","07_cutset","max_mu_eta", 2.1);
	cut_defs["electron"]["07_cutset"]["mu_type"] = 0;
	cut_defs["electron"]["07_cutset"]["Z_rejection_width"] = 20.;
	vcuts("electron","07_cutset","max_loose_e_trackiso", 2.0);
	vcuts("electron","07_cutset","max_loose_e_caliso", 5.0);
	vcuts("electron","07_cutset","min_loose_e_pt", 20.0);
	cut_defs["electron"]["07_cutset"]["name_btag"] = 4;
	vcuts("electron","07_cutset","min_btag", 2.96);

	// step: s08
	vcuts("electron","08_cutset","trigger", 50);
	cut_defs["electron"]["08_cutset"]["min_no_jets"] = 4;
	vcuts("electron","08_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["08_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["08_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","08_cutset","min_e_pt", 30.);
	vcuts("electron","08_cutset","max_e_eta", 2.4);
	vcuts("electron","08_cutset","max_e_d0", 0.02);
	vcuts("electron","08_cutset","max_e_trackiso", 1.0);
	vcuts("electron","08_cutset","max_e_caliso", 3.0);
	cut_defs["electron"]["08_cutset"]["max_nisolated_e"] = 1;
	cut_defs["electron"]["08_cutset"]["min_jet_e_dR"] = 0.3;
	cut_defs["electron"]["08_cutset"]["max_nisolated_mu"] = 0;
	vcuts("electron","08_cutset","max_mu_trackiso", 3.0);
	vcuts("electron","08_cutset","max_mu_caliso", 1.0);
	vcuts("electron","08_cutset","max_mu_d0", 0.02);
	vcuts("electron","08_cutset","min_mu_pt", 30.);
	vcuts("electron","08_cutset","max_mu_eta", 2.1);
	cut_defs["electron"]["08_cutset"]["mu_type"] = 0;
	cut_defs["electron"]["08_cutset"]["Z_rejection_width"] = 20.;
	vcuts("electron","08_cutset","max_loose_e_trackiso", 2.0);
	vcuts("electron","08_cutset","max_loose_e_caliso", 5.0);
	vcuts("electron","08_cutset","min_loose_e_pt", 20.0);
	cut_defs["electron"]["08_cutset"]["name_btag"] = 4;
	vcuts("electron","08_cutset","min_btag", 4);

	// step: s09
	vcuts("electron","09_cutset","trigger", 50);
	cut_defs["electron"]["09_cutset"]["min_no_jets"] = 4;
	vcuts("electron","09_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["09_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["09_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","09_cutset","min_e_pt", 30.);
	vcuts("electron","09_cutset","max_e_eta", 2.4);
	vcuts("electron","09_cutset","max_e_d0", 0.02);
	vcuts("electron","09_cutset","max_e_trackiso", 1.0);
	vcuts("electron","09_cutset","max_e_caliso", 3.0);
	cut_defs["electron"]["09_cutset"]["max_nisolated_e"] = 1;
	cut_defs["electron"]["09_cutset"]["min_jet_e_dR"] = 0.3;
	cut_defs["electron"]["09_cutset"]["max_nisolated_mu"] = 0;
	vcuts("electron","09_cutset","max_mu_trackiso", 3.0);
	vcuts("electron","09_cutset","max_mu_caliso", 1.0);
	vcuts("electron","09_cutset","max_mu_d0", 0.02);
	vcuts("electron","09_cutset","min_mu_pt", 30.);
	vcuts("electron","09_cutset","max_mu_eta", 2.1);
	cut_defs["electron"]["09_cutset"]["mu_type"] = 0;
	cut_defs["electron"]["09_cutset"]["Z_rejection_width"] = 20.;
	vcuts("electron","09_cutset","max_loose_e_trackiso", 2.0);
	vcuts("electron","09_cutset","max_loose_e_caliso", 5.0);
	vcuts("electron","09_cutset","min_loose_e_pt", 20.0);
	cut_defs["electron"]["09_cutset"]["name_btag"] = 4;
	vcuts("electron","09_cutset","min_btag", 5.23);

	// step: s10
	vcuts("electron","10_cutset","trigger", 50);
	cut_defs["electron"]["10_cutset"]["min_no_jets"] = 4;
	vcuts("electron","10_cutset","min_jet_pt", 30.);
	cut_defs["electron"]["10_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["10_cutset"]["min_nisolated_e"] = 1;
	vcuts("electron","10_cutset","min_e_pt", 30.);
	vcuts("electron","10_cutset","max_e_eta", 2.4);
	vcuts("electron","10_cutset","max_e_d0", -1);
	vcuts("electron","10_cutset","max_e_trackiso", 1.0);
	vcuts("electron","10_cutset","max_e_caliso", -1);
	cut_defs["electron"]["10_cutset"]["max_nisolated_e"] = 1;
	cut_defs["electron"]["10_cutset"]["min_jet_e_dR"] = 0.3;
	cut_defs["electron"]["10_cutset"]["max_nisolated_mu"] = 0;
	vcuts("electron","10_cutset","max_mu_trackiso", 3.0);
	vcuts("electron","10_cutset","max_mu_caliso", 1.0);
	vcuts("electron","10_cutset","max_mu_d0", 0.02);
	vcuts("electron","10_cutset","min_mu_pt", 30.);
	vcuts("electron","10_cutset","max_mu_eta", 2.1);
	cut_defs["electron"]["10_cutset"]["mu_type"] = 0;
	cut_defs["electron"]["10_cutset"]["Z_rejection_width"] = 20.;
	vcuts("electron","10_cutset","max_loose_e_trackiso", 2.0);
	vcuts("electron","10_cutset","max_loose_e_caliso", 5.0);
	vcuts("electron","10_cutset","min_loose_e_pt", 20.0);
	cut_defs["electron"]["10_cutset"]["name_btag"] = 4;
	vcuts("electron","10_cutset","min_btag", 4);

	// ---AUTOGENERATED END---

        /*
         *      global cuts for all selections defined so far
         */
        synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
		for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
			{
				if(type_iter->first == "muon" || type_iter->first == "mu_background"){
				}else if(type_iter->first == "electron" || type_iter->first == "e_background"){
				}
			}
	}
}

void CutSelector::set_if_not_set(std::string type, std::string set, std::string cut, double value)
{
	if(cut_defs[type][set].find(cut) == cut_defs[type][set].end()){
		cut_defs[type][set][cut] = value;	
	}
}

void CutSelector::vset_if_not_set(std::string type, std::string set, std::string cut, double value)
{
	if(vcut_defs[type][set].find(cut) == vcut_defs[type][set].end()){
		vcuts(type, set, cut, value);	
	}
}

void CutSelector::vset_if_not_set(std::string type, std::string set, std::string cut, std::vector<double> *value)
{
	if(vcut_defs[type][set].find(cut) == vcut_defs[type][set].end()){
		vcuts(type, set, cut, value);	
	}
}

//In case a cutset contains only vcuts and not cut_defs this stops program from crashing
void CutSelector::synchronise_maps()
{
        for(std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > >::iterator type_iter = vcut_defs.begin();
                type_iter != vcut_defs.end();
                ++type_iter)
        {
		for(std::map<std::string,std::map<std::string, std::vector<double>* > >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
			{
				cut_defs[type_iter->first][set_iter->first]["dummy"] = -1;
			}
		
	}
	
        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
	    type_iter != cut_defs.end();
	    ++type_iter)
		{
			for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
			    set_iter != type_iter->second.end();
			    ++set_iter)
				{
					vcuts(type_iter->first, set_iter->first, "dummy", -1);
				}
			
		}
}

void CutSelector::vcuts(std::string type, std::string set, std::string cut, double value)
{
	std::vector<double> *cut_vector = new std::vector<double>();
	if(value != -1)
		cut_vector->push_back(value);
	vcut_defs[type][set][cut] = cut_vector;	
	cuts_to_be_deleted.push_back(cut_vector);
}

void CutSelector::vcuts(std::string type, std::string set, std::string cut, std::vector<double> *cut_vector)
{
	vcut_defs[type][set][cut] = cut_vector;
}

void CutSelector::complete_cuts()
{
	std::vector<std::string> all_cuts;
	std::vector<std::string> all_v_cuts;

	//	---ALLOWED CUTS BEGIN---

	all_cuts.push_back("min_met");
	all_cuts.push_back("max_met");
	all_cuts.push_back("max_ht");
	all_cuts.push_back("min_ht");
	all_cuts.push_back("min_no_jets");
	all_cuts.push_back("max_no_jets");
	all_cuts.push_back("min_nisolated_lep");
	all_cuts.push_back("max_nisolated_e");
	all_cuts.push_back("max_nisolated_mu");
	all_cuts.push_back("min_nisolated_e");
	all_cuts.push_back("min_nisolated_mu");
	all_cuts.push_back("max_nloose_e");
	all_cuts.push_back("max_nloose_mu");
	all_cuts.push_back("max_jet_eta");
	all_cuts.push_back("min_jet_e_dR");
	all_cuts.push_back("JES_factor");
	all_cuts.push_back("mu_type");
	all_cuts.push_back("e_type");
	all_cuts.push_back("loose_mu_type");
	all_cuts.push_back("loose_e_type");
	all_cuts.push_back("Z_rejection_width");
	all_cuts.push_back("name_btag");
	all_cuts.push_back("min_M3");
	all_cuts.push_back("min_mindiffM3");
	all_cuts.push_back("min_chi2");
	all_cuts.push_back("max_chi2");

	all_v_cuts.push_back("min_mu_pt");
	all_v_cuts.push_back("min_mu_et");
	all_v_cuts.push_back("max_mu_trackiso");
	all_v_cuts.push_back("max_mu_caliso");
	all_v_cuts.push_back("max_mu_ecaliso");
	all_v_cuts.push_back("max_mu_hcaliso");
	all_v_cuts.push_back("max_mu_hcal_veto_cone");
	all_v_cuts.push_back("max_mu_ecal_veto_cone");
	all_v_cuts.push_back("min_mu_relIso");
	all_v_cuts.push_back("min_mu_dR");
	all_v_cuts.push_back("min_e_pt");
	all_v_cuts.push_back("min_e_et");
	all_v_cuts.push_back("max_e_trackiso");
	all_v_cuts.push_back("max_e_caliso");
	all_v_cuts.push_back("max_e_ecaliso");
	all_v_cuts.push_back("max_e_hcaliso");
	all_v_cuts.push_back("max_e_hcal_veto_cone");
	all_v_cuts.push_back("max_e_ecal_veto_cone");
	all_v_cuts.push_back("min_e_relIso");
	all_v_cuts.push_back("min_e_dR");
	all_v_cuts.push_back("min_jet_pt");
        all_v_cuts.push_back("max_mu_chi2");
        all_v_cuts.push_back("max_mu_d0");
        all_v_cuts.push_back("max_mu_d0sig");
        all_v_cuts.push_back("min_mu_nHits");
        all_v_cuts.push_back("mu_electronID");
        all_v_cuts.push_back("max_e_chi2");
        all_v_cuts.push_back("max_e_d0");
        all_v_cuts.push_back("max_e_d0sig");
        all_v_cuts.push_back("min_e_nHits");
        all_v_cuts.push_back("e_electronID");
        all_v_cuts.push_back("trigger");
	all_v_cuts.push_back("min_btag");
	all_v_cuts.push_back("max_mu_eta");
	all_v_cuts.push_back("max_e_eta");

	// loose lepton cuts
	all_v_cuts.push_back("min_loose_mu_pt");
	all_v_cuts.push_back("min_loose_mu_et");
	all_v_cuts.push_back("max_loose_mu_trackiso");
	all_v_cuts.push_back("max_loose_mu_caliso");
	all_v_cuts.push_back("max_loose_mu_ecaliso");
	all_v_cuts.push_back("max_loose_mu_hcaliso");
	all_v_cuts.push_back("max_loose_mu_hcal_veto_cone");
	all_v_cuts.push_back("max_loose_mu_ecal_veto_cone");
	all_v_cuts.push_back("min_loose_mu_relIso");
	all_v_cuts.push_back("min_loose_mu_dR");
	all_v_cuts.push_back("min_loose_e_pt");
	all_v_cuts.push_back("min_loose_e_et");
	all_v_cuts.push_back("max_loose_e_trackiso");
	all_v_cuts.push_back("max_loose_e_caliso");
	all_v_cuts.push_back("max_loose_e_ecaliso");
	all_v_cuts.push_back("max_loose_e_hcaliso");
	all_v_cuts.push_back("max_loose_e_hcal_veto_cone");
	all_v_cuts.push_back("max_loose_e_ecal_veto_cone");
	all_v_cuts.push_back("min_loose_e_relIso");
	all_v_cuts.push_back("min_loose_e_dR");
        all_v_cuts.push_back("max_loose_mu_chi2");
        all_v_cuts.push_back("max_loose_mu_d0");
        all_v_cuts.push_back("max_loose_mu_d0sig");
        all_v_cuts.push_back("min_loose_mu_nHits");
        all_v_cuts.push_back("loose_mu_electronID");
        all_v_cuts.push_back("max_loose_e_chi2");
        all_v_cuts.push_back("max_loose_e_d0");
        all_v_cuts.push_back("max_loose_e_d0sig");
        all_v_cuts.push_back("min_loose_e_nHits");
        all_v_cuts.push_back("loose_e_electronID");
	all_v_cuts.push_back("max_loose_mu_eta");
	all_v_cuts.push_back("max_loose_e_eta");

	//	---ALLOWED CUTS END---
	
	synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > >::iterator type_iter = vcut_defs.begin();
                type_iter != vcut_defs.end();
                ++type_iter)
        {
		for(std::map<std::string,std::map<std::string, std::vector<double>* > >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
			{
				for(std::vector<std::string>::iterator cut_name = all_v_cuts.begin();
				    cut_name != all_v_cuts.end();
				    ++cut_name){
					if(set_iter->second.find(*cut_name) == set_iter->second.end()){
						vcuts(type_iter->first, set_iter->first, *cut_name, -1);
					}
				}
			}
		
	}
	
	
        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
		
		for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
			{
				for(std::vector<std::string>::iterator cut_name = all_cuts.begin();
				    cut_name != all_cuts.end();
				    ++cut_name){
					if(set_iter->second.find(*cut_name) == set_iter->second.end()){
						set_iter->second[*cut_name] = -1;
					}
				}
			}
		
	}
}

//type_iter->first = muon/electron, set_iter->first = ??_cutset
void CutSelector::set_cuts()
{
        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
		
		//If no _background cuts set, and not running over Data, set _background cuts = signal cuts
		std::vector<std::string> event_types = get_event_types(type_iter->first);
			
		for(std::vector<std::string>::iterator event_type_iter=event_types.begin();event_type_iter != event_types.end();++event_type_iter)
			{
				for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
				    set_iter != type_iter->second.end();
				    ++set_iter)
					{
						std::string full_id = dataset_id+"|"+*event_type_iter+"|"+set_iter->first;
						std::string id = dataset_id+"_"+set_iter->first;
						plot_generators[*event_type_iter][id] = new PlotGenerator(full_id);
						cuts[*event_type_iter][id] = new Cuts(full_id);

						//Pass map with all cuts to Cuts.cc
						cuts[*event_type_iter][id]->set_all_cuts(set_iter->second);
						cuts[*event_type_iter][id]->set_all_vcuts(vcut_defs[type_iter->first][set_iter->first]);
						
						// pass t' mass to plot_generator
						plot_generators[*event_type_iter][id]->set_tprime_mass(tprime_mass);
						
						// initialise plot generator with according cuts
						plot_generators[*event_type_iter][id]->apply_cuts(cuts[*event_type_iter][id]);
					}
			}
	}
}
	
void CutSelector::set_histo_writer(HistoWriter *histo_writer)
{
	this->histo_writer = histo_writer;
	event_counter_histo = histo_writer->create_1d("event_counter", "event counter", 10, -0.5, 9.5);

	for(std::map<std::string, std::map<std::string, PlotGenerator*> >::iterator type_iter = plot_generators.begin();
	    type_iter != plot_generators.end();
	    ++type_iter)
	{
        	for(std::map<std::string,PlotGenerator*>::iterator pgen_iter = type_iter->second.begin();
        	    pgen_iter != type_iter->second.end();
        	    ++pgen_iter){
			pgen_iter->second->set_histo_writer(histo_writer);
		}
	}
}

//Returns what goes between the | | in histo_names and defines for what channels cuts should be set
std::vector<std::string> CutSelector::get_event_types(std::string event_type)
{
	std::vector<std::string> event_types;
	//If no _background cuts set, and not running over Data, set _background cuts = signal cuts
	if(dataset_id != "Data" && event_type == "muon" && cut_defs.find("mu_background") == cut_defs.end())
		{ 
			event_types.push_back(event_type);
			event_types.push_back("mu_background");
		}
	else if(dataset_id != "Data" && event_type == "electron" && cut_defs.find("e_background") == cut_defs.end())
		{
			event_types.push_back(event_type);
			event_types.push_back("e_background");
		}
	else 
		event_types.push_back(event_type);
       
	return event_types;

}
