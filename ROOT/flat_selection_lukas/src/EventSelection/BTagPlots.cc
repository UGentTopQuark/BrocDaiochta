#include "../../interface/EventSelection/BTagPlots.h"

BTagPlots::BTagPlots(std::string ident)
{
	id = ident;

	gen_evt = NULL;
	bjet_finder = NULL;
}

BTagPlots::~BTagPlots()
{
}

void BTagPlots::plot_all()
{
//	plot_MCmethod_comparison(); // Compare MC matching methods to find best one
	//the following three use highest and second highest btagged jets, for selecting correct jet for mass reco
	analyse_btag_algorithms();      // matches all b-jets
	plot_bDiscriminator();          // matches only b-jets from semi-leptonic top decay
	plot_bDiscriminator_allbjets(); // matches all b-jets
	//this function is to plot the difference between signal btag value and background in order to apply cut
	plot_bDiscriminator_find_btagcut();
}

void BTagPlots::set_gen_evt(mor::TTbarGenEvent *gen_match)
{
	this->gen_evt = gen_match;
}

void BTagPlots::set_bjet_finder(BJetFinder *bjet_finder)
{
	this->bjet_finder = bjet_finder;
}

void BTagPlots::book_histos()
{
	// plots for comparing bid and recogen information, ie for comparing MC matching in recogenmatch with another method
	histos1d[("bjets_in_event_MC1"+id).c_str()]=histo_writer->create_1d(("bjets_in_event_MC1"+id).c_str(),"bjets in event method 1",5,-0.5,3.5, "b-jet multiplicity" );
	histos1d[("jets_in_event_MC1"+id).c_str()]=histo_writer->create_1d(("jets_in_event_MC1"+id).c_str(),"jets in event method 1",8,-0.5,6.5, "jet multiplicity" );
	histos1d[("bjets_in_event_MC2"+id).c_str()]=histo_writer->create_1d(("bjets_in_event_MC2"+id).c_str(),"bjets found by matching method 2",4,-0.5,3.5, "b-jet multiplicity" );
	histos1d[("jets_in_event_MC2"+id).c_str()]=histo_writer->create_1d(("jets_in_event_MC2"+id).c_str(),"jets found by matching method 2",7,-0.5,6.5, "jet multiplicity" );
	histos1d[("bjets_in_event_MCboth"+id).c_str()]=histo_writer->create_1d(("bjets_in_event_MCboth"+id).c_str(),"bjets found where 2 methods agree",4,-0.5,3.5, "b-jet multiplicity" );
	histos1d[("jets_in_event_MCboth"+id).c_str()]=histo_writer->create_1d(("jets_in_event_MCboth"+id).c_str(),"jets found where 2 methods agree",7,-0.5,6.5, "jet multiplicity" );

	histos1d[("bjets_MCmethods_compare"+id).c_str()]=histo_writer->create_1d(("bjets_MCmethods_compare"+id).c_str(),"bjets:2=MCagree,1=MC1b,-1=MC2b,-2diffb's,-3MC1bMC2W,-4MC2bMC1W ",7,-4.5,2.5);
	histos1d[("jets_MCmethods_compare"+id).c_str()]=histo_writer->create_1d(("jets_MCmethods_compare"+id).c_str(),"jets:2=MC agree,1=MC1only,-1=MC2only,-2=diffpartons",5,-2.5,2.5);
	histos2d[("dPt_vs_dR_bjet_MCmethods_agree"+id).c_str()]=histo_writer->create_2d(("dPt_vs_dR_bjet_MCmethods_agree"+id).c_str(),"bjets: dPt vs dR both MC's match",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str()]=histo_writer->create_2d(("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str(),"bjets: dPt vs dR MC1 only matched",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str()]=histo_writer->create_2d(("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str(),"bjets: dPt vs dR MC2 only matched",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_jet_MCmethods_agree"+id).c_str()]=histo_writer->create_2d(("dPt_vs_dR_jet_MCmethods_agree"+id).c_str(),"dPt vs dR both MC's match",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_jet_MC1_disagrees"+id).c_str()]=histo_writer->create_2d(("dPt_vs_dR_jet_MC1_disagrees"+id).c_str(),"dPt vs dR MC1 only matched",210, 0, 7,800, -4,4);
	histos2d[("dPt_vs_dR_jet_MC2_disagrees"+id).c_str()]=histo_writer->create_2d(("dPt_vs_dR_jet_MC2_disagrees"+id).c_str(),"dPt vs dR MC2 only matched",210, 0, 7,800, -4,4);
	histos2d[("jet_MCnumber_when_disagree"+id).c_str()]=histo_writer->create_2d(("jet_MCnumber_when_disagree"+id).c_str(),"1=hadb,2=lepb,3=Wq1,4=Wq2",160, 0.5, 4.5,160, 0.5,4.5);

	//Btag...
	histos1d[("bjet_pt_order"+id).c_str()]=histo_writer->create_1d(("bjet_pt_order"+id).c_str(),"MC matched bjet has #th highest pt in event",11,-0.5,10.5);
	histos1d[("bjets_in_event"+id).c_str()]=histo_writer->create_1d(("bjets_in_event"+id).c_str(),"How many bjets from top decay in event",4,-0.5,3.5, "b-jet multiplicity" );
	histos1d[("bjets_in_event_allbjets"+id).c_str()]=histo_writer->create_1d(("bjets_in_event_allbjets"+id).c_str(),"How many bjets from any decay in event",6,-0.5,5.5, "b-jet multiplicity" );
	
	//book btag plots for more than one algorithm if necessary
	std::vector<std::pair<std::string,std::string> > btag_algos;
	btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighEffBJetTags","_HighEff"));
	btag_algos.push_back(std::pair<std::string,std::string>("jetProbabilityBJetTags","_JetProb"));
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighPurBJetTags","_HighPur"));
	btag_algos.push_back(std::pair<std::string,std::string>("softMuonNoIPBJetTags","_SoftMuNoIP"));
	btag_algos.push_back(std::pair<std::string,std::string>("softMuonBJetTags","_SoftMu"));
	btag_algos.push_back(std::pair<std::string,std::string>("simpleSecondaryVertexBJetTags","_SSVertex"));
	btag_algos.push_back(std::pair<std::string,std::string>("impactParameterMVABJetTags","_IPMVA"));
	btag_algos.push_back(std::pair<std::string,std::string>("combinedSecondaryVertexMVABJetTags","_CSVertexMVA"));
	btag_algos.push_back(std::pair<std::string,std::string>("combinedSecondaryVertexBJetTags","_CSVertex"));
	btag_algos.push_back(std::pair<std::string,std::string>("softElectronBJetTags","_SoftEle"));
	//IF adding an algorithm don´t forget to set nbins,minX and maxX below.

	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){
		std::pair<std::string,std::string> btag = (*btag_algo);
	
		//bin ranges when plotting btag value different for each algorithm
		int nbins = 35;
		double min_x = 0,max_x = 35;	
		if(btag.first == "trackCountingHighEffBJetTags")        {nbins = 50; min_x = 0;max_x = 50; }
		if(btag.first == "jetBProbabilityBJetTags")             {nbins = 50; min_x = 0;max_x = 10; }
		if(btag.first == "jetProbabilityBJetTags")              {nbins = 15; min_x = 0;max_x = 3; }
		if(btag.first == "trackCountingHighPurBJetTags")        {nbins = 50; min_x = 0;max_x = 50; }
		if(btag.first == "softMuonNoIPBJetTags")                {nbins = 40; min_x = -1;max_x = 1; }
		if(btag.first == "softMuonBJetTags")                    {nbins = 40; min_x = -1;max_x = 1; }
		if(btag.first == "simpleSecondaryVertexBJetTags")       {nbins = 40; min_x = -2;max_x = 8; }
		if(btag.first == "impactParameterMVABJetTags")          {nbins = 40; min_x = 0;max_x = 1; }
		if(btag.first == "combinedSecondaryVertexMVABJetTags")  {nbins = 40; min_x = 0;max_x = 1; }
		if(btag.first == "combinedSecondaryVertexBJetTags")     {nbins = 40; min_x = 0;max_x = 1; }
		if(btag.first == "softElectronBJetTags")                {nbins = 40; min_x = -1;max_x = 1; }
		

		//Do full analysis for these two algorithms, only short analysis for the others
		if(btag.first == "trackCountingHighEffBJetTags"){
			
			//btag plots using genParton id (identifies all bjets not just those from semileptonic decay
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_sorted_werent_found_bid"+btag.second+id).c_str(),(btag.second+": -1=2nd found,0=1st found, 1=1st not found,2=2nd not found").c_str(),4,-1.5,2.5);
			
			histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_highest_is_real_bid"+btag.second+id).c_str(),(btag.second+":1 matched b, 0 unmatched b").c_str(),2,-0.5,1.5);
			histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_second_highest_is_real_bid"+btag.second+id).c_str(),(btag.second+":1 matched b id, 0 unmatched b id").c_str(),2,-0.5,1.5);
			//pt
			histos1d[("bjet_highest_pt_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_highest_pt_bid"+btag.second+id).c_str(),(btag.second+":pt for most likely bjet matched id").c_str(),52,0,260,"p_{T} [GeV]");	
			histos1d[("nonbjet_highest_pt_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_highest_pt_bid"+btag.second+id).c_str(),(btag.second+":pt for most likely bjet unmatched id").c_str(),52,0,260,"p_{T} [GeV]");	
			histos1d[("bjet_second_highest_pt_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_second_highest_pt_bid"+btag.second+id).c_str(),(btag.second+":pt for second most likely bjet matched id").c_str(),52,0,260,"p_{T} [GeV]");
			histos1d[("nonbjet_second_highest_pt_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_second_highest_pt_bid"+btag.second+id).c_str(),(btag.second+":pt for second most likely bjet unmatched id").c_str(),52,0,260,"p_{T} [GeV]");
			//eta
			histos1d[("bjet_highest_eta_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_highest_eta_bid"+btag.second+id).c_str(),(btag.second+":eta for most likely bjet matched id").c_str(),60,-3,3, "#eta");	
			histos1d[("nonbjet_highest_eta_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_highest_eta_bid"+btag.second+id).c_str(),(btag.second+":eta for most likely bjet unmatched id").c_str(),60,-3,3, "#eta");	
			histos1d[("bjet_second_highest_eta_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_second_highest_eta_bid"+btag.second+id).c_str(),(btag.second+":eta for second most likely bjet matched id").c_str(),60,-3,3, "#eta");
			histos1d[("nonbjet_second_highest_eta_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_second_highest_eta_bid"+btag.second+id).c_str(),(btag.second+":eta for second most likely bjet unmatched id").c_str(),60,-3,3, "#eta");
			//btag
			histos1d[("bjet_highest_bDiscrim_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_highest_bDiscrim_bid"+btag.second+id).c_str(),(btag.second+":btag value for most likely bjet matched id").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("nonbjet_highest_bDiscrim_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_highest_bDiscrim_bid"+btag.second+id).c_str(),(btag.second+":btag value for most likely bjet unmatched id").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");	
			histos1d[("bjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_second_highest_bDiscrim_bid"+btag.second+id).c_str(),(btag.second+":btag value for second most likely bjet matched id").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("nonbjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_second_highest_bDiscrim_bid"+btag.second+id).c_str(),(btag.second+":btag value for second most likely bjet unmatched id").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			
			//btag plots using reco gen match 
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_highest_is_real"+btag.second+id).c_str(),(btag.second+":1 matched b, 0 unmatched b").c_str(),2,-0.5,1.5);
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_second_highest_is_real"+btag.second+id).c_str(),(btag.second+":1 matched b, 0 unmatched b").c_str(),2,-0.5,1.5);
			histos1d[("bjets_highest_is_real_highpt"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_highest_is_real_highpt"+btag.second+id).c_str(),(btag.second+":1 matched b, 0 unmatched b (top 4 pt jets)").c_str(),2,-0.5,1.5);
			histos1d[("bjets_second_highest_is_real_highpt"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_second_highest_is_real_highpt"+btag.second+id).c_str(),(btag.second+":1 matched b, 0 unmatched b (top 4 pt jets)").c_str(),2,-0.5,1.5);
			histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_two_highest_are_real"+btag.second+id).c_str(),(btag.second+":3 both matched , 2 2nd only, 1 1st only, 0 none").c_str(),4,-0.5,3.5);
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_sorted_werent_found"+btag.second+id).c_str(),(btag.second+": -1=2nd found,0=1st found, 1=1st not found,2=2nd not found").c_str(),4,-1.5,2.5);
			histos1d[("bjets_highest_identify"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_highest_identify"+btag.second+id).c_str(),(btag.second+":1 hadronic b, 0 leptonic b").c_str(),2,-0.5,1.5);
			histos1d[("bjets_second_highest_identify"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_second_highest_identify"+btag.second+id).c_str(),(btag.second+":1 hadronic b, 0 leptonic b").c_str(),2,-0.5,1.5);
			//pt
			histos1d[("bjet_highest_pt"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_highest_pt"+btag.second+id).c_str(),(btag.second+":pt for most likely bjet matched").c_str(),52,0,260,"p_{T} [GeV]");	
			histos1d[("nonbjet_highest_pt"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_highest_pt"+btag.second+id).c_str(),(btag.second+":pt for most likely bjet unmatched").c_str(),52,0,260,"p_{T} [GeV]");	
			histos1d[("bjet_second_highest_pt"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_second_highest_pt"+btag.second+id).c_str(),(btag.second+":pt for second most likely bjet matched").c_str(),52,0,260,"p_{T} [GeV]");
			histos1d[("nonbjet_second_highest_pt"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_second_highest_pt"+btag.second+id).c_str(),(btag.second+":pt for second most likely bjet unmatched").c_str(),52,0,260,"p_{T} [GeV]");	
			//eta
			histos1d[("bjet_highest_eta"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_highest_eta"+btag.second+id).c_str(),(btag.second+":eta for most likely bjet matched").c_str(),60,-3,3, "#eta");	
			histos1d[("nonbjet_highest_eta"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_highest_eta"+btag.second+id).c_str(),(btag.second+":eta for most likely bjet unmatched").c_str(),60,-3,3, "#eta");	
			histos1d[("bjet_second_highest_eta"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_second_highest_eta"+btag.second+id).c_str(),(btag.second+":eta for second most likely bjet matched").c_str(),60,-3,3, "#eta");
			histos1d[("nonbjet_second_highest_eta"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_second_highest_eta"+btag.second+id).c_str(),(btag.second+":eta for second most likely bjet unmatched").c_str(),60,-3,3, "#eta");
			//dR
			histos1d[("dR_highest_bjet_Wjj"+btag.second+id).c_str()]=histo_writer->create_1d(("dR_highest_bjet_Wjj"+btag.second+id).c_str(),(btag.second+":dR between highest btagged jet and genmatched Wjj direction").c_str(),50,0,7);
			histos1d[("dR_second_highest_bjet_Wjj"+btag.second+id).c_str()]=histo_writer->create_1d(("dR_second_highest_bjet_Wjj"+btag.second+id).c_str(),(btag.second+":dR between second highest btagged jet and genmatched Wjj direction").c_str(),50,0,7);
			histos1d[("dR_highest_bjet_muon"+btag.second+id).c_str()]=histo_writer->create_1d(("dR_highest_bjet_muon"+btag.second+id).c_str(),(btag.second+":dR between highest btagged jet and muon direction").c_str(),50,0,7);
			histos1d[("dR_second_highest_bjet_muon"+btag.second+id).c_str()]=histo_writer->create_1d(("dR_second_highest_bjet_muon"+btag.second+id).c_str(),(btag.second+":dR between second highest btagged jet and muon direction").c_str(),50,0,7);
			//btag
			histos1d[("bjet_bDiscrim"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for b jets matched").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("nonbjet_bDiscrim"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for non b jets unmatched").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("bjet_highest_bDiscrim"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_highest_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for most likely bjet matched").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");	
			histos1d[("nonbjet_highest_bDiscrim"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_highest_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for most likely bjet unmatched").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");	
			histos1d[("bjet_second_highest_bDiscrim"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_second_highest_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for second most likely bjet matched").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("nonbjet_second_highest_bDiscrim"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_second_highest_bDiscrim"+btag.second+id).c_str(),(btag.second+":btag value for second most likely bjet unmatched").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("bjet_bDiscrim_hadb"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_bDiscrim_hadb"+btag.second+id).c_str(),(btag.second+":btag value for hadronic bjets").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("bjet_bDiscrim_lepb"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_bDiscrim_lepb"+btag.second+id).c_str(),(btag.second+":btag value for leptonic bjets").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			
			//for plot_bDiscrim_find_btagcut
			histos1d[("bjet_efficiency_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_efficiency_cut"+btag.second+id).c_str(),(btag.second+": 1= tagged bjet, 0=untagged bjet").c_str(),2,-0.5,1.5);
			histos1d[("bjet_passed_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_passed_cut"+btag.second+id).c_str(),(btag.second+": 1= tagged bjet, 0=mistag").c_str(),2,-0.5,1.5);
			histos1d[("bjet_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for b jets").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("cjet_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("cjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for c jets").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("lightjet_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("lightjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for light jets").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for non b jets").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("bjet_pt_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_pt_cut"+btag.second+id).c_str(),(btag.second+":pt of b jets").c_str(),52,0,260,"p_{T} [GeV]");
			histos1d[("nonbjet_pt_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_pt_cut"+btag.second+id).c_str(),(btag.second+":pt of non b jets").c_str(),52,0,260,"p_{T} [GeV]");
			histos1d[("bjet_eta_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_eta_cut"+btag.second+id).c_str(),(btag.second+":eta of b jets").c_str(),50,-2.5,2.5, "#eta");
			histos1d[("nonbjet_eta_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_eta_cut"+btag.second+id).c_str(),(btag.second+":eta of non b jets").c_str(),50,-2.5,2.5, "#eta");
			histos2d[("bjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_2d(("bjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":eta of b jets vs bDiscrim").c_str(),nbins,min_x,max_x,50,-2.5,2.5);
			histos2d[("nonbjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_2d(("nonbjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":eta of non b jets vs bDiscrim").c_str(),nbins,min_x,max_x,50,-2.5,2.5);
			histos2d[("bjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_2d(("bjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":pt of b jets vs bDiscrim").c_str(),nbins,min_x,max_x,52,0,260);
			histos2d[("nonbjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_2d(("nonbjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":pt of non b jets vs bDiscrim").c_str(),nbins,min_x,max_x,52,0,260);
			histos1d[("bjet_event_passed_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_event_passed_cut"+btag.second+id).c_str(),(btag.second+":how often does event pass btag cut").c_str(),2,-0.5,1.5);
			
			histos1d[("jets_btagged_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("jets_btagged_cut"+btag.second+id).c_str(),(btag.second+":number of jets btagged in this event").c_str(),11,-0.5,10.5);
			histos1d[("jet_highest_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("jet_highest_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":highest bDiscrim jet in event").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("jet_second_highest_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("jet_second_highest_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":second highest bDiscrim jet in event").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("jet_highest_pt_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("jet_highest_pt_cut"+btag.second+id).c_str(),(btag.second+":pt of highest bDiscrim jet").c_str(),52,0,260,"p_{T} [GeV]");
			histos1d[("jet_second_highest_pt_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("jet_second_highest_pt_cut"+btag.second+id).c_str(),(btag.second+":pt of second highest bDiscrim jet").c_str(),52,0,260,"p_{T} [GeV]");
			histos1d[("jet_highest_eta_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("jet_highest_eta_cut"+btag.second+id).c_str(),(btag.second+":eta of highest bDiscrim  jet").c_str(),50,-2.5,2.5, "#eta");
			histos1d[("jet_second_highest_eta_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("jet_second_highest_eta_cut"+btag.second+id).c_str(),(btag.second+":eta of second highest bDiscrim  jet").c_str(),50,-2.5,2.5, "#eta");
			histos2d[("jet_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_2d(("jet_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":highest bDiscrim jet pt vs bDiscrim").c_str(),nbins,min_x,max_x,52,0,260);
			histos2d[("jet_second_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_2d(("jet_second_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":second highest bDiscrim jet pt vs bDiscrim").c_str(),nbins,min_x,max_x,52,0,260);
			
			// plots to test hypothesis if only considering first 4 b-jets would make sense
			histos2d[("1st_b_pt_vs_4th_jet_pt"+btag.second+id).c_str()]=histo_writer->create_2d(("1st_b_pt_vs_4th_jet_pt"+btag.second+id).c_str(),"pt 1st btagged jet vs pt 4th pt jet",150, 0, 500, 150,0,500);
			histos2d[("2nd_b_pt_vs_4th_jet_pt"+btag.second+id).c_str()]=histo_writer->create_2d(("2nd_b_pt_vs_4th_jet_pt"+btag.second+id).c_str(),"pt 2nd btagged jet vs pt 4th pt jet",150, 0, 500, 150,0,500);
		}
		//less detailed comparison of btag algorithms (efficiency in chosing real bjet with highest btag)
		else{
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_highest_is_real"+btag.second+id).c_str(),"1 matched b, 0 unmatched b",2,-0.5,1.5);
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]=histo_writer->create_1d(("bjets_second_highest_is_real"+btag.second+id).c_str(),"1 matched b, 0 unmatched b",2,-0.5,1.5);
			histos1d[("bjet_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("bjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for b jets").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
			histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]=histo_writer->create_1d(("nonbjet_bDiscrim_cut"+btag.second+id).c_str(),(btag.second+":btag value for non b jets").c_str(),nbins,min_x,max_x, "bTag Discriminator Value");
		}

	}
}

/***********************************************Btagging*********************************************************/
//Plot value returned by bDiscrim for real b jets and non bjets, for two jets with highest btag value in event (using genParton to see if real bjet from any decay)
void BTagPlots::plot_bDiscriminator_allbjets()
{
	int njets = jets->size();
	if (njets < 2)
		return;

	if(gen_evt == NULL)
		return;

	std::vector<std::pair<std::string,std::string> > btag_algos;
	//btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighEffBJetTags","_HighEff"));

	//now get back the ids for all bjets in event (genParton =5)
	std::vector<int> matched_bjet_ids;
	for(unsigned int i = 0; i < jets->size(); ++i){
		if((*jets)[i].from_ttbar() && abs((*jets)[i].mc_match_id()) == 5) matched_bjet_ids.push_back(i);
	}

	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){
		std::pair<std::string,std::string> btag = (*btag_algo);
		
		int  max_b_id = -1,max2_b_id = -1;

		//get back highest and second highest btagged ids for this btagging algorithm. from BJetFinder
		std::vector<double> set_min_btag (1,-1000);
		bjet_finder->set_min_btag_value(set_min_btag);	
		std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(btag.first);
		if(btag_ids_this_algo->size() > 0)
			max_b_id = (*btag_ids_this_algo)[0].first;
		if(btag_ids_this_algo->size() > 1)
			max2_b_id = (*btag_ids_this_algo)[1].first;


		//If no bjets found in event fill plots for when highest and second highest are non-bjets
		if (matched_bjet_ids.empty() == true){	
			if(max_b_id != -1){
				histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("bjets_in_event_allbjets"+id).c_str()]->Fill(0);
				histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("nonbjet_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());}
			else if(max_b_id == -1){
				histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(1);}
			
			if(max2_b_id != -1){
				histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(-1);
				histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_second_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("nonbjet_second_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());}
			else if(max2_b_id == -1){
				histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(2);}
			continue;
		}

		histos1d[("bjets_in_event_allbjets"+id).c_str()]->Fill(matched_bjet_ids.size());

		std::vector<int>::iterator max_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),max_b_id);
		std::vector<int>::iterator max2_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),max2_b_id);
		
		//truth match highest 
		if(max_b_id != -1){
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(0);
			if(max_is_b != matched_bjet_ids.end()){
				histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("bjet_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("bjet_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());
			}
			else{
				histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("nonbjet_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());}
		}
		else if (max_b_id == -1){
			histos1d[("bjets_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(1);}	
		
	       
		//now examine second highest btagged, if id != -1 && highest jet info is available (because will only be using second highest if highest is also good)
		if(max2_b_id != -1 && max_b_id != -1){
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(-1);
			if(max2_is_b != matched_bjet_ids.end()){
				histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("bjet_second_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("bjet_second_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());
			}
			else {
				histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_second_highest_bDiscrim_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_second_highest_pt_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("nonbjet_second_highest_eta_bid"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());}
		}
		else if (max2_b_id == -1){
			histos1d[("bjets_second_highest_is_real_bid"+btag.second+id).c_str()]->Fill(0);
			histos1d[("bjets_sorted_werent_found_bid"+btag.second+id).c_str()]->Fill(2);}		

		// check how often 5th or lower pt jet is 1st or 2nd b-jet
		if(max_b_id != -1 && jets->size() > 4)
			histos2d[("1st_b_pt_vs_4th_jet_pt"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt(), (*jets)[3].pt());
		if(max2_b_id != -1 && jets->size() > 4)
			histos2d[("2nd_b_pt_vs_4th_jet_pt"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt(), (*jets)[3].pt());
	}
}

//Plot value returned by bDiscrim for real b jets and non bjets, for two jets with highest btag value in event (using RecoGenMatch to see if real bjet from semileptonic decay)
void BTagPlots::plot_bDiscriminator()
{
	if(gen_evt == NULL)
		return;
	if(bjet_finder == NULL)
		return;

	int njets = jets->size();
	if ( njets < 2)
		return;
	
	int matched_jet_ids_hadb = -1;
	int matched_jet_ids_lepb = -1;
	int matched_jet_ids_Wquark1 = -1;
	int matched_jet_ids_Wquark2 = -1;

	for(unsigned int i = 0; i < jets->size(); ++i){
		if(matched_jet_ids_Wquark1 == -1 && equal_p4(gen_evt->q()->p4(), (*jets)[i].mc_p4())) matched_jet_ids_Wquark1 = i;
		else if(matched_jet_ids_Wquark2 == -1 && equal_p4(gen_evt->qbar()->p4(), (*jets)[i].mc_p4())) matched_jet_ids_Wquark2 = i;
		else if(matched_jet_ids_hadb == -1 && equal_p4(gen_evt->hadB()->p4(), (*jets)[i].mc_p4())) matched_jet_ids_hadb = i;
		else if(matched_jet_ids_lepb == -1 && equal_p4(gen_evt->lepB()->p4(), (*jets)[i].mc_p4())) matched_jet_ids_lepb = i;
	}

	//number of bjets available after reconstruction & selection
	int num_b_jets = 0;
	if(matched_jet_ids_hadb != -1){
		++num_b_jets;			
	}
	if(matched_jet_ids_lepb != -1){
		++num_b_jets;
	}
	histos1d[("bjets_in_event"+id).c_str()]->Fill(num_b_jets);


	std::vector<std::pair<std::string,std::string> > btag_algos;
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighEffBJetTags","_HighEff"));
        //btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));

	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){
		std::pair<std::string,std::string> btag = (*btag_algo);

		//bDiscrim of genmatched jets
		if(matched_jet_ids_hadb != -1){
			histos1d[("bjet_bDiscrim_hadb"+btag.second+id).c_str()]->Fill((*jets)[matched_jet_ids_hadb].bDiscriminator(btag.first));		
			histos1d[("bjet_pt_order"+id).c_str()]->Fill(matched_jet_ids_hadb);}
		if(matched_jet_ids_lepb != -1){
			histos1d[("bjet_bDiscrim_lepb"+btag.second+id).c_str()]->Fill((*jets)[matched_jet_ids_lepb].bDiscriminator(btag.first));		
			histos1d[("bjet_pt_order"+id).c_str()]->Fill(matched_jet_ids_lepb);}
					
		int max_b_id = -1,max2_b_id = -1;	
		//get back highest and second highest btagged ids for this btagging algorithm. from BJetFinder
		std::vector<double> set_min_btag (1,-1000);
		bjet_finder->set_min_btag_value(set_min_btag);
		std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(btag.first);
		if(btag_ids_this_algo->size() >= 1)
			max_b_id = (*btag_ids_this_algo)[0].first;
		if(btag_ids_this_algo->size() >= 2)
			max2_b_id = (*btag_ids_this_algo)[1].first;
				
		if(max_b_id != -1){	
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]->Fill(0);	
			if(matched_jet_ids_hadb == max_b_id || matched_jet_ids_lepb == max_b_id ){
				histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_highest_bDiscrim"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("bjet_highest_pt"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("bjet_highest_eta"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());
				if(matched_jet_ids_hadb == max_b_id)
					histos1d[("bjets_highest_identify"+btag.second+id).c_str()]->Fill(1);			
				else if(matched_jet_ids_lepb == max_b_id)
					histos1d[("bjets_highest_identify"+btag.second+id).c_str()]->Fill(0);}
			
			else{
				histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_highest_bDiscrim"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_highest_pt"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].pt());
				histos1d[("nonbjet_highest_eta"+btag.second+id).c_str()]->Fill((*jets)[max_b_id].eta());}
			
		}
		else if(max_b_id == -1){
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(0);	
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]->Fill(1);}
			

		if(max2_b_id != -1 && max_b_id != -1){
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]->Fill(-1);
			if(matched_jet_ids_hadb == max2_b_id || matched_jet_ids_lepb == max2_b_id ){
				histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_second_highest_bDiscrim"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("bjet_second_highest_pt"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("bjet_second_highest_eta"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());
				if(matched_jet_ids_hadb == max2_b_id)
					histos1d[("bjets_second_highest_identify"+btag.second+id).c_str()]->Fill(1);			
				else if(matched_jet_ids_lepb == max2_b_id)
					histos1d[("bjets_second_highest_identify"+btag.second+id).c_str()]->Fill(0);}
			else{
				histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_second_highest_bDiscrim"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].bDiscriminator(btag.first));
				histos1d[("nonbjet_second_highest_pt"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].pt());
				histos1d[("nonbjet_second_highest_eta"+btag.second+id).c_str()]->Fill((*jets)[max2_b_id].eta());}
		}
		else if(max2_b_id == -1){
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(0);
			histos1d[("bjets_sorted_werent_found"+btag.second+id).c_str()]->Fill(2);}

		//how often are both found.
		if(max2_b_id != -1 && max_b_id != -1 &&(matched_jet_ids_hadb == max2_b_id || matched_jet_ids_lepb == max2_b_id) && (matched_jet_ids_hadb == max_b_id || matched_jet_ids_lepb == max_b_id ))
				histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]->Fill(3);
		else if(max_b_id != -1 && (matched_jet_ids_hadb == max_b_id || matched_jet_ids_lepb == max_b_id ))
				histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]->Fill(1);
		else if(max2_b_id != -1 && (matched_jet_ids_hadb == max2_b_id || matched_jet_ids_lepb == max2_b_id ))
				histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]->Fill(2);
		else
				histos1d[("bjets_two_highest_are_real"+btag.second+id).c_str()]->Fill(0);
		
			

		//plot btag value for all jets
		for(int i=0;i < njets; ++i){
			double bDiscrim = (*jets)[i].bDiscriminator(btag.first) ;
			if(matched_jet_ids_hadb == i || matched_jet_ids_lepb == i){
				histos1d[("bjet_bDiscrim"+btag.second+id).c_str()]->Fill(bDiscrim);	
			}
			else{
				histos1d[("nonbjet_bDiscrim"+btag.second+id).c_str()]->Fill(bDiscrim);
			}
		}
	
		//dR of highest and second highest btagged jets vs. genmatched W
		if(max_b_id != -1 && max2_b_id != -1){
			if(matched_jet_ids_Wquark1 != -1 && matched_jet_ids_Wquark2 != -1){
				double maxb_Wjj = ROOT::Math::VectorUtil::DeltaR((*jets)[max_b_id].p4(),((*jets)[matched_jet_ids_Wquark1].p4()+(*jets)[matched_jet_ids_Wquark2].p4()));
				double max2b_Wjj = ROOT::Math::VectorUtil::DeltaR((*jets)[max2_b_id].p4(),((*jets)[matched_jet_ids_Wquark1].p4()+(*jets)[matched_jet_ids_Wquark2].p4()));
				histos1d[("dR_highest_bjet_Wjj"+btag.second+id).c_str()]->Fill(maxb_Wjj);
				histos1d[("dR_second_highest_bjet_Wjj"+btag.second+id).c_str()]->Fill(max2b_Wjj);				
			}			
			// 		//dR b´s and leptons
			if((*isolated_muons).size() > 0){
				double maxb_muon = ROOT::Math::VectorUtil::DeltaR((*jets)[max_b_id].p4(),(*isolated_muons)[0].p4());
				double max2b_muon = ROOT::Math::VectorUtil::DeltaR((*jets)[max2_b_id].p4(),(*isolated_muons)[0].p4());
				histos1d[("dR_highest_bjet_muon"+btag.second+id).c_str()]->Fill(maxb_muon);
				histos1d[("dR_second_highest_bjet_muon"+btag.second+id).c_str()]->Fill(max2b_muon);
			}
		}

		//Examine same things only for top 4 pt jets
		bjet_finder->set_min_btag_value(set_min_btag);
		bjet_finder->set_max_considered_jets(4);
		btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(btag.first);
		if(btag_ids_this_algo->size() >= 1)
			max_b_id = (*btag_ids_this_algo)[0].first;
		if(btag_ids_this_algo->size() >= 2)
			max2_b_id = (*btag_ids_this_algo)[1].first;
				
		if(max_b_id != -1){		
			if(matched_jet_ids_hadb == max_b_id || matched_jet_ids_lepb == max_b_id ){
				histos1d[("bjets_highest_is_real_highpt"+btag.second+id).c_str()]->Fill(1);}
			
			else{
				histos1d[("bjets_highest_is_real_highpt"+btag.second+id).c_str()]->Fill(0);}
			
		}
		else if(max_b_id == -1){
			histos1d[("bjets_highest_is_real_highpt"+btag.second+id).c_str()]->Fill(0);}
			

		if(max2_b_id != -1 && max_b_id != -1){
			if(matched_jet_ids_hadb == max2_b_id || matched_jet_ids_lepb == max2_b_id ){
				histos1d[("bjets_second_highest_is_real_highpt"+btag.second+id).c_str()]->Fill(1);}
			else{
				histos1d[("bjets_second_highest_is_real_highpt"+btag.second+id).c_str()]->Fill(0);}
		}
		else if(max2_b_id == -1){
			histos1d[("bjets_second_highest_is_real_highpt"+btag.second+id).c_str()]->Fill(0);}

		bjet_finder->set_max_considered_jets(-1);

	}
}

bool BTagPlots::equal_p4(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2)
{
	if(p4_1.pt() == p4_2.pt() && p4_1.eta() == p4_2.eta())
		return true;
	else
		return false;
}

//Matching to all bjets in order to use for background also
void BTagPlots::analyse_btag_algorithms()
{
	if(gen_evt == NULL)
		return;
	int njets = jets->size(), max_b_id = -1,max2_b_id = -1;
	if(njets < 2)
		return;
	if(bjet_finder == NULL)
		return;
		
	//	std::map<std::string,int> matched_jet_ids = reco_gen_match->get_matched_recojets_id(); //min_dD with dR cut	
	//now get back the ids for all bjets in event (genParton =5)
	std::vector<int> matched_bjet_ids;
	for(unsigned int i = 0; i < jets->size(); ++i){
		if((*jets)[i].from_ttbar() && abs((*jets)[i].mc_match_id()) == 5) matched_bjet_ids.push_back(i);
	}

	std::vector<std::pair<std::string,std::string> > btag_algos;
	btag_algos.push_back(std::pair<std::string,std::string>("jetProbabilityBJetTags","_JetProb"));
	btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighPurBJetTags","_HighPur"));
	btag_algos.push_back(std::pair<std::string,std::string>("softMuonNoIPBJetTags","_SoftMuNoIP"));
	btag_algos.push_back(std::pair<std::string,std::string>("softMuonBJetTags","_SoftMu"));
	btag_algos.push_back(std::pair<std::string,std::string>("simpleSecondaryVertexBJetTags","_SSVertex"));
	btag_algos.push_back(std::pair<std::string,std::string>("impactParameterMVABJetTags","_IPMVA"));
	btag_algos.push_back(std::pair<std::string,std::string>("combinedSecondaryVertexMVABJetTags","_CSVertexMVA"));
	btag_algos.push_back(std::pair<std::string,std::string>("combinedSecondaryVertexBJetTags","_CSVertex"));
	btag_algos.push_back(std::pair<std::string,std::string>("softElectronBJetTags","_SoftEle"));
	
	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){

		std::pair<std::string,std::string> btag = (*btag_algo);	
		max_b_id = -1;max2_b_id = -1;
		
		//If no bjets found in event fill plots for when highest and second highest are non-bjets
		if (matched_bjet_ids.empty() == true){
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(0);
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(0);
			for (int i = 0;i < njets;i++){

				double bDiscrim = (*jets)[i].bDiscriminator(btag.first);				
				histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
			}
			continue;
		}
		
		//get back highest and second highest btagged ids for this btagging algorithm. from BJetFinder
		std::vector<double> set_min_btag (1,-1000);
		bjet_finder->set_min_btag_value(set_min_btag);		
		std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(btag.first);
		if(btag_ids_this_algo->size() >= 1)
			max_b_id = (*btag_ids_this_algo)[0].first;
		if(btag_ids_this_algo->size() >= 2)
			max2_b_id = (*btag_ids_this_algo)[1].first;
		
		std::vector<int>::iterator max_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),max_b_id);
		std::vector<int>::iterator max2_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),max2_b_id);			
		
		if(max_b_id != -1 && max_is_b != matched_bjet_ids.end()){
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(1);}
		else{
			histos1d[("bjets_highest_is_real"+btag.second+id).c_str()]->Fill(0);}
		
		if(max2_b_id != -1 && max2_is_b != matched_bjet_ids.end()){
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(1);}
		else{
			histos1d[("bjets_second_highest_is_real"+btag.second+id).c_str()]->Fill(0);}
		
		for (int i = 0;i < njets;i++){
			std::vector<int>::iterator i_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),i);

			double bDiscrim = (*jets)[i].bDiscriminator(btag.first);

			if(i_is_b != matched_bjet_ids.end())
				histos1d[("bjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
			else
				histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
		}		
	}	
	btag_algos.clear();
}

void BTagPlots::plot_bDiscriminator_find_btagcut()
{
	int njets = jets->size();
	if ( njets < 2)
		return;
	if(gen_evt == NULL)
		return;
	
	
	//Matching all bjets
	std::vector<std::pair<std::string,std::string> > btag_algos;
	btag_algos.push_back(std::pair<std::string,std::string>("trackCountingHighEffBJetTags","_HighEff"));
	//btag_algos.push_back(std::pair<std::string,std::string>("jetBProbabilityBJetTags","_JetBProb"));

	int def_min_btag = -1000; //default value if vector not set
	if(min_btag->size() < 1)
		min_btag->push_back(def_min_btag);

	//get back the ids for all bjets in event (genParton =5)
	std::vector<int> matched_bjet_ids;
	for(unsigned int i = 0; i < jets->size(); ++i){
		if((*jets)[i].from_ttbar() && abs((*jets)[i].mc_match_id()) == 5) matched_bjet_ids.push_back(i);
	}

	for(std::vector<std::pair<std::string,std::string> >::iterator btag_algo = btag_algos.begin();btag_algo != btag_algos.end();++btag_algo){

		std::pair<std::string,std::string> btag = (*btag_algo);	


		//get back vector of jet(id,btag) in order of decreasing btag
		bjet_finder->set_min_btag_value(*min_btag);	
		std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(btag.first);
	
		double jets_passed_btag = btag_ids_this_algo->size();
		histos1d[("jets_btagged_cut"+btag.second+id).c_str()]->Fill(jets_passed_btag);
		
		double bDiscrim = -1 ,pt = -1,eta = -1;
		if (jets_passed_btag > 0){
			bDiscrim =(*btag_ids_this_algo)[0].second;
			pt = (*jets)[(*btag_ids_this_algo)[0].first].pt();
			eta = (*jets)[(*btag_ids_this_algo)[0].first].eta();
			histos1d[("jet_highest_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
			histos1d[("jet_highest_pt_cut"+btag.second+id).c_str()]->Fill(pt);
			histos2d[("jet_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,pt);
			histos1d[("jet_highest_eta_cut"+btag.second+id).c_str()]->Fill(eta);}
		if (jets_passed_btag > 1){
			bDiscrim =(*btag_ids_this_algo)[1].second;
			pt = (*jets)[(*btag_ids_this_algo)[1].first].pt();
			eta = (*jets)[(*btag_ids_this_algo)[1].first].eta();
			histos1d[("jet_second_highest_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
			histos1d[("jet_second_highest_pt_cut"+btag.second+id).c_str()]->Fill(pt);
			histos2d[("jet_second_highest_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,pt);
			histos1d[("jet_second_highest_eta_cut"+btag.second+id).c_str()]->Fill(eta);}
			
		int bjets_tagged = 0;
		for (std::vector<std::pair<int,double> >::iterator btag_ids_iter = btag_ids_this_algo->begin();btag_ids_iter != btag_ids_this_algo->end();btag_ids_iter++){	
			
			//check if jet id is in the matched bjets vector
			std::vector<int>::iterator jet_is_b = find(matched_bjet_ids.begin(),matched_bjet_ids.end(),(*btag_ids_iter).first);
			bDiscrim = (*btag_ids_iter).second;
			//std::cout << " int " << (*btag_ids_iter).first << " bDiscrim " << (*btag_ids_iter).second << std::endl;
		
			if(jet_is_b != matched_bjet_ids.end()){
				bjets_tagged++;
				histos1d[("bjet_efficiency_cut"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_passed_cut"+btag.second+id).c_str()]->Fill(1);
				histos1d[("bjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
				histos1d[("bjet_pt_cut"+btag.second+id).c_str()]->Fill((*jets)[(*btag_ids_iter).first].pt());
				histos2d[("bjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,(*jets)[(*btag_ids_iter).first].pt());
				histos1d[("bjet_eta_cut"+btag.second+id).c_str()]->Fill((*jets)[(*btag_ids_iter).first].eta());
				histos2d[("bjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,(*jets)[(*btag_ids_iter).first].eta());
			}
			else{
				histos1d[("bjet_passed_cut"+btag.second+id).c_str()]->Fill(0);
				histos1d[("nonbjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
				histos1d[("nonbjet_pt_cut"+btag.second+id).c_str()]->Fill((*jets)[(*btag_ids_iter).first].pt());
				histos2d[("nonbjet_pt_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,(*jets)[(*btag_ids_iter).first].pt());
				histos1d[("nonbjet_eta_cut"+btag.second+id).c_str()]->Fill((*jets)[(*btag_ids_iter).first].eta());
				histos2d[("nonbjet_eta_vs_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim,(*jets)[(*btag_ids_iter).first].eta());

				//To replicate mistag efficiency from AN-2007/048. e mistag = Nlight,tagged/Nlight,taggable. taggable = has >=2 tracks.
				/*
				if((*jets)[(*btag_ids_iter).first].associatedTracks().size() >= 2 && (*jets)[(*btag_ids_iter).first].genParton() != NULL && fabs((*jets)[(*btag_ids_iter).first].genParton()->pdgId()) == 4){
					histos1d[("cjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
				}
				else if((*jets)[(*btag_ids_iter).first].associatedTracks().size() >= 2 && (*jets)[(*btag_ids_iter).first].genParton() != NULL && fabs((*jets)[(*btag_ids_iter).first].genParton()->pdgId()) != 4){
					histos1d[("lightjet_bDiscrim_cut"+btag.second+id).c_str()]->Fill(bDiscrim);
				}
				*/
			}

		}
		
		int size_bjets_not_tagged = (matched_bjet_ids.size()-bjets_tagged);
		for (int i=0;i < size_bjets_not_tagged;i++)
			histos1d[("bjet_efficiency_cut"+btag.second+id).c_str()]->Fill(0);
		
	}
	if((*min_btag)[0] == def_min_btag)
		min_btag->clear();
}

/******************************not currently used***************************************/
				     

/*
//Examines difference between two matching methods.
void BTagPlots::plot_MCmethod_comparison()
{
	int njets = jets->size();
	if ( njets < 1)
		return;
	if(gen_evt == NULL)
		return;	
	
	reco_gen_match->set_matching_method(4);
	std::map<std::string,int> matched_jet_ids = reco_gen_match->get_matched_recojets_id(); 
	reco_gen_match->set_matching_method(3);
	std::map<std::string,int> matched_jet_ids2 = reco_gen_match->get_matched_recojets_id();
	reco_gen_match->set_matching_method(-1);
	std::map<std::string,reco::GenParticle*> genquark;

	//truth matching  
	if(gen_match != NULL && gen_match->is_ok() && gen_match->get_decay_mode() == 1){
		
		//All from hadronic top decay
		if(!(gen_match->t_decay_is_leptonic())){	
			genquark["Wquark1"] = gen_match->get_particle("Wplus_dp1");
			genquark["Wquark2"] = gen_match->get_particle("Wplus_dp2");
			genquark["hadb"] = gen_match->get_particle("b");
			genquark["lepb"] = gen_match->get_particle("bbar");
		}
		
		else if(!(gen_match->tbar_decay_is_leptonic())){
			genquark["Wquark1"]= gen_match->get_particle("Wminus_dp1");
			genquark["Wquark2"]= gen_match->get_particle("Wminus_dp2");
			genquark["hadb"]= gen_match->get_particle("bbar");
			genquark["lepb"]= gen_match->get_particle("b");
		  
		}
	}


	int num_MC1_bjets =0,num_MC2_bjets = 0, num_agreed_bjets = 0, num_MC1_jets =0,num_MC2_jets = 0, num_agreed_jets = 0;
	//for counting number of bjets in event when genInfo is available matched by recogenmatch
	for(int i=0;i < njets; ++i){
		std::string genmatch_MC1 = "-1",genmatch_MC2 = "-1";
		int num_MC1 = -1,num_MC2 = -1;
		if(matched_jet_ids_hadb == i){ genmatch_MC1 = "hadb"; num_MC1 = 1;}
		if(matched_jet_ids_lepb == i){ genmatch_MC1 = "lepb"; num_MC1 = 2;}
		if(matched_jet_ids_Wquark1 == i){ genmatch_MC1 = "Wquark1"; num_MC1 = 3;}
		if(matched_jet_ids_Wquark2 == i){ genmatch_MC1 = "Wquark2"; num_MC1 = 4;}

		if(matched_jet_ids2["hadb"] == i){ genmatch_MC2 = "hadb"; num_MC2 = 1;}
		if(matched_jet_ids2["lepb"] == i){ genmatch_MC2 = "lepb"; num_MC2 = 2;}
		if(matched_jet_ids2["Wquark1"] == i){ genmatch_MC2 = "Wquark1"; num_MC2 = 3;}
		if(matched_jet_ids2["Wquark2"] == i){ genmatch_MC2 = "Wquark2"; num_MC2 = 4;}
		

		//calculate dR vs dPT. for closest jet-parton match
		double dR_MC1= 0,dR_MC2= 0,dPt_MC1 = 0,dPt_MC2 = 0 ;
		if(genmatch_MC1 != "-1"){
			dR_MC1 = ROOT::Math::VectorUtil::DeltaR(genquark[genmatch_MC1]->p4(),(*jets)[i].p4());
			dPt_MC1 = (((*jets)[i].pt()- genquark[genmatch_MC1]->pt())/genquark[genmatch_MC1]->pt());}
		if(genmatch_MC2 != "-1"){
			dR_MC2 = ROOT::Math::VectorUtil::DeltaR(genquark[genmatch_MC2]->p4(),(*jets)[i].p4());
			dPt_MC2 = (((*jets)[i].pt()- genquark[genmatch_MC2]->pt())/genquark[genmatch_MC2]->pt());}
		
		if(genmatch_MC1 == genmatch_MC2 && genmatch_MC1 != "-1"){
			++num_MC1_jets; 
			++num_MC2_jets;
			++num_agreed_jets;
			histos1d[("jets_MCmethods_compare"+id).c_str()]->Fill(2);
			histos2d[("dPt_vs_dR_jet_MCmethods_agree"+id).c_str()]->Fill(dR_MC1,dPt_MC1);
			if(genmatch_MC1 == "hadb" || genmatch_MC1 == "lepb" ){
				++num_MC1_bjets; 
				++num_MC2_bjets;
				++num_agreed_bjets;
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(2);
				histos2d[("dPt_vs_dR_jet_MCmethods_agree"+id).c_str()]->Fill(dR_MC1,dPt_MC1);}
			
		}
		else if(genmatch_MC1 != "-1" && genmatch_MC2 == "-1"){
			++num_MC1_jets; 
			histos2d[("dPt_vs_dR_jet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);
			histos1d[("jets_MCmethods_compare"+id).c_str()]->Fill(1);
			if(matched_jet_ids_hadb == i || matched_jet_ids_lepb == i){
				++num_MC1_bjets; 
				histos2d[("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(1);}
		}

		else if(genmatch_MC2 != "-1" && genmatch_MC1 == "-1"){
			++num_MC2_jets; 
			histos2d[("dPt_vs_dR_jet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);
			histos1d[("jets_MCmethods_compare"+id).c_str()]->Fill(-1);
			if(matched_jet_ids2["hadb"] == i||matched_jet_ids2["lepb"] == i ){
				++num_MC2_bjets; 
				histos2d[("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(-1);}
			
		}
		//If both found partons but not the same one
		else if(genmatch_MC2 != "-1" && genmatch_MC1 != "-1"){
			histos2d[("dPt_vs_dR_jet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);
			histos2d[("dPt_vs_dR_jet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);
			histos1d[("jets_MCmethods_compare"+id).c_str()]->Fill(-2);

			if((genmatch_MC1 == "hadb" || genmatch_MC1 == "lepb")&& (genmatch_MC2 == "hadb" || genmatch_MC2 == "lepb")){
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(-2);
				histos2d[("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);
				histos2d[("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);}
			else if((genmatch_MC1 == "hadb" || genmatch_MC1 == "lepb")&& !(genmatch_MC2 == "hadb" || genmatch_MC2 == "lepb")){
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(-3);
				histos2d[("dPt_vs_dR_bjet_MC1_disagrees"+id).c_str()]->Fill(dR_MC1,dPt_MC1);}
			else if(!(genmatch_MC1 == "hadb" || genmatch_MC1 == "lepb")&& (genmatch_MC2 == "hadb" || genmatch_MC2 == "lepb")){
				histos1d[("bjets_MCmethods_compare"+id).c_str()]->Fill(-4);
				histos2d[("dPt_vs_dR_bjet_MC2_disagrees"+id).c_str()]->Fill(dR_MC2,dPt_MC2);}


			histos2d[("jet_MCnumber_when_disagree"+id).c_str()]->Fill(num_MC1,num_MC2);

			
		}

			
	}
	
	histos1d[("bjets_in_event_MC1"+id).c_str()]->Fill(num_MC1_bjets);	
	histos1d[("bjets_in_event_MC2"+id).c_str()]->Fill(num_MC2_bjets);	
	histos1d[("bjets_in_event_MCboth"+id).c_str()]->Fill(num_agreed_bjets);

	histos1d[("jets_in_event_MC1"+id).c_str()]->Fill(num_MC1_jets);	
	histos1d[("jets_in_event_MC2"+id).c_str()]->Fill(num_MC2_jets);	
	histos1d[("jets_in_event_MCboth"+id).c_str()]->Fill(num_agreed_jets);
}
*/

void BTagPlots::set_btag_cuts(std::vector<double> *min,double n)
{
	min_btag= min;

	if (n == 1) name_btag = "jetBProbabilityBJetTags";
	else if (n == 2) name_btag = "jetProbabilityBJetTags";
	else if (n == 3) name_btag = "trackCountingHighPurBJetTags";
	else if (n == 4) name_btag = "trackCountingHighEffBJetTags";
	else name_btag = "-1";
	
}
