#include "../../interface/EventSelection/METPlots.h"

METPlots::METPlots(std::string ident)
{
	id = ident;
}

METPlots::~METPlots()
{
}

void METPlots::set_met_handle(std::vector<mor::MET> *analyser_mets)
{
	mets = analyser_mets;
}

void METPlots::plot_all()
{
	plot_MET_pz();
	plot_MET_MtW();
}

int METPlots::get_pt_bin(double neutrino_pt)
{
	double min_pt = 0;
	double max_pt = 200.;
	double bin_width = (max_pt - min_pt)/((double) PT_BINS);

	int bin = (int) ((neutrino_pt-min_pt)/bin_width);

	if(bin < PT_BINS)
		return bin;
	else
		return -1;
}

int METPlots::get_pz_bin(double neutrino_pz)
{
	double min_pz = 0;
	double max_pz = 200.;
	double bin_width = (max_pz - min_pz)/((double) PT_BINS);

	int bin = (int) ((neutrino_pz-min_pz)/bin_width);

	if((bin > 0) && (bin < PT_BINS))
		return bin;
	else
		return -1;
}

void METPlots::plot_MET_pz()
{
	if(gen_evt == NULL || gen_evt->decay_channel() == 0)
		return;

	if(isolated_muons->size() < 1)
		return;

	mor::Particle *neutrino = gen_evt->neutrino();

	mor::MET corrected_MET = *(corrected_mets->begin());
	histos1d[("dpz_corMET_neutrino"+id).c_str()]->Fill(neutrino->pz() - corrected_MET.pz());
	histos1d[("dR_corMET_neutrino"+id).c_str()]->Fill(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), corrected_MET.p4()));
	histos1d[("neutrino_pz"+id).c_str()]->Fill(neutrino->pz());
	histos1d[("neutrino_pt"+id).c_str()]->Fill(neutrino->pt());
	histos1d[("dpt_neutrino_MET"+id).c_str()]->Fill(neutrino->pt() - corrected_MET.pt());

	// how often do neutrino and MET p4 match?
//	if(ROOT::Math::VectorUtil::DeltaR(neutrino->p4(), corrected_MET.p4()) < 0.4){
//		histos1d[("match_MET_neutrino"+id).c_str()]->Fill(1);
//	}else{
//		histos1d[("match_MET_neutrino"+id).c_str()]->Fill(0);
//	}

	histos2d[("pt_neutrino_vs_MET"+id).c_str()]->Fill(neutrino->pt(), corrected_MET.pt());
	histos2d[("pz_neutrino_vs_MET"+id).c_str()]->Fill(neutrino->pz(), corrected_MET.pz());

	double npx = neutrino->px();
	double npy = neutrino->py();
	double nmet_pt = npx*npx+npy*npy;
        mor::MET neutrino_MET;
        neutrino_MET.SetPxPyPzE(npx,npy,0.,nmet_pt);

	MyMEzCalculator MEzCal;
	MEzCal.SetMET(neutrino_MET);
	mor::Lepton muon;
	muon.SetPxPyPzE(isolated_muons->begin()->px(), isolated_muons->begin()->py(), isolated_muons->begin()->pz(), isolated_muons->begin()->e());
	MEzCal.SetLepton(*(isolated_muons->begin()));

	double neutrinoMET_recolep_pz = MEzCal.Calculate();

	histos2d[("pz_neutrinoMET_recolep_vs_neutrino"+id).c_str()]->Fill(neutrino->pz(), neutrinoMET_recolep_pz);

	histos1d[("dpz_neutrino_recolep"+id).c_str()]->Fill(neutrino->pz() - neutrinoMET_recolep_pz);

	mor::Particle *gen_lepton = gen_evt->lepton();

	histos2d[("pz_neutrino_gen_lep"+id).c_str()]->Fill(neutrino->pz(), gen_lepton->pz());

//	Tools tool;
//	int pt_bin = get_pt_bin(neutrino->pt());
//	if(pt_bin != -1){
//		histos1d[("MET_pt_bin"+tool.stringify(pt_bin)+id).c_str()]->Fill((neutrino->pt() - corrected_MET.pt())/neutrino->pz());
//	}
//	int pz_bin = get_pz_bin(fabs(neutrino->pz()));
}

// Plot leptonic transverse mass and MET 
void METPlots::plot_MET_MtW()
{
	if(mets->size() < 1)
		return;

	// Loop to get total MET, should only be one iteration of loop.
	double e_MtW=0.0,mu_MtW=0.0;
	for(std::vector<mor::MET>::iterator met_iter = corrected_mets->begin();
	    met_iter!=corrected_mets->end();
	    ++met_iter){
		
		// Check for at least one lepton before calculating Mt	
		if(isolated_muons->size() > 0){
	   		mu_MtW = ((*isolated_muons)[0].p4()+met_iter->p4()).Mt(); 
	   		histos1d[("muon_MtW"+id).c_str()]->Fill(mu_MtW);
		}
		       
		if(isolated_electrons->size() > 0){
	   		e_MtW = ((*isolated_electrons)[0].p4()+met_iter->p4()).Mt();
	   		histos1d[("electron_MtW"+id).c_str()]->Fill(e_MtW); 
		}
	}
	histos1d[("Event_MET"+id).c_str()]->Fill(corrected_mets->begin()->Et());
	histos1d[("Event_MET_pt"+id).c_str()]->Fill(corrected_mets->begin()->pt());	
}

void METPlots::book_histos()
{
	// METpz plots
//	histos1d[("dpz_MET_neutrino"+id).c_str()]=histo_writer->create_1d(("dpz_MET_neutrino"+id).c_str(),"delta p_z between MET and generator neutrino",100,-150,150);
	histos1d[("dR_uncorMET_neutrino"+id).c_str()]=histo_writer->create_1d(("dR_uncorMET_neutrino"+id).c_str(),"delta R between MET and generator neutrino",30,0,8, "dR(MET, #nu)");
	histos1d[("dpz_neutrino_recolep"+id).c_str()]=histo_writer->create_1d(("dpz_neutrino_recolep"+id).c_str(),"delta p_z between neutrino-MET-pz from reco lepton and generator neutrino",100,-150,150, "dp_{z}(neutrino, ch.lepton)");
	histos1d[("dpz_corMET_neutrino"+id).c_str()]=histo_writer->create_1d(("dpz_corMET_neutrino"+id).c_str(),"delta p_{z} between MET and generator neutrino",100,-200,200,"dp_{z}(MET, ch.lepton) [GeV]");
	histos1d[("dR_corMET_neutrino"+id).c_str()]=histo_writer->create_1d(("dR_corMET_neutrino"+id).c_str(),"delta R between MET and generator neutrino",30,0,8, "dR(MET, #nu)");
	histos1d[("neutrino_pz"+id).c_str()]=histo_writer->create_1d(("neutrino_pz"+id).c_str(),"p_{z} of generator neutrino",100,-200,200, "p_{z}(#nu) [GeV]");
	histos1d[("neutrino_pt"+id).c_str()]=histo_writer->create_1d(("neutrino_pt"+id).c_str(),"p_{T} of generator neutrino",40,0,400, "p_{T}(#nu) [GeV]");
	histos1d[("dpt_neutrino_MET"+id).c_str()]=histo_writer->create_1d(("dpt_neutrino_MET"+id).c_str(),"dp_{T}(generator neutrino, MET)",100,-200,200, "dp_{T}(#nu, MET) [GeV]");

	histos1d[("Event_MET"+id).c_str()]=histo_writer->create_1d(("Event_MET"+id).c_str(),"missing Et in event",40,0,400, "MET [GeV]");
	histos1d[("Event_MET_pt"+id).c_str()]=histo_writer->create_1d(("Event_MET_pt"+id).c_str(),"Neutrino pt in event",40,0,400, "MET [GeV]");
	histos1d[("muon_MtW"+id).c_str()]=histo_writer->create_1d(("muon_MtW"+id).c_str(),"Leptonic Transverse Mass with muon",30,0, 200, "M_{T}(W) [GeV]");
	histos1d[("electron_MtW"+id).c_str()]=histo_writer->create_1d(("electron_MtW"+id).c_str(),"Leptonic Transverse Mass with electron",30,0, 200, "M_{T}(W) [GeV]");

//	// 1 = matched, 0 = unmatched dR < 0.4
//	histos1d[("match_MET_neutrino"+id).c_str()]=histo_writer->create_1d(("match_MET_neutrino"+id).c_str(),"dR(nu, MET) < 0.4) 0 = unmatched, 1 = matched",5,-0.5,4.5);
//	histos1d[("match_MET_neutrino_uncorMET"+id).c_str()]=histo_writer->create_1d(("match_MET_neutrino_uncorMET"+id).c_str(),"dR(nu, MET) < 0.4) 0 = unmatched, 1 = matched",5,-0.5,4.5);

	// neutrino vs MET
	histos2d[("pz_neutrinoMET_recolep_vs_neutrino"+id).c_str()]=histo_writer->create_2d(("pz_neutrinoMET_recolep_vs_neutrino"+id).c_str(),"correlation between neutrino pz and ``neutrino-MET''-pz",150, -300, 300, 150,-300,300, "p_{z} #nu [GeV]", "p_{z} MET [GeV]");
	histos2d[("pt_neutrino_vs_MET"+id).c_str()]=histo_writer->create_2d(("pt_neutrino_vs_MET"+id).c_str(),"correlation between neutrino pt and MET",150, 0, 300, 150,0,300, "p_{T} #nu [GeV]", "MET [GeV]");
	histos2d[("pz_neutrino_vs_MET"+id).c_str()]=histo_writer->create_2d(("pz_neutrino_vs_MET"+id).c_str(),"correlation between neutrino pz and pz component of MET",150, -300, 300, 150,-300,300, "p_{z} #nu [GeV]", "p_{z} MET [GeV]");

	histos2d[("pz_neutrino_gen_lep"+id).c_str()]=histo_writer->create_2d(("pz_neutrino_gen_lep"+id).c_str(),"correlation between neutrino pz and generator charged lepton pz",150, -300, 300, 150,-300,300, "p_{z} #nu [GeV]", "p_{z} charged lepton [GeV]");

//	Tools tool;
//	for(int bin = 0; bin < PT_BINS; ++bin){
//		histos1d[("MET_pt_bin"+tool.stringify(bin)+id).c_str()]=histo_writer->create_1d(("MET_pt_bin"+tool.stringify(bin)+id).c_str(),"MET resolution in pt bin",100,-3.,3.);
//	}
}
