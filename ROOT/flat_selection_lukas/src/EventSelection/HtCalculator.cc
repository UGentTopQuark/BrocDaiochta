#include "../../interface/EventSelection/HtCalculator.h"

HtCalculator::HtCalculator()
{
	ht = -1;

	jets = NULL;
	isolated_electrons = NULL;
	isolated_muons = NULL;
	corrected_mets = NULL;
}

HtCalculator::~HtCalculator()
{
}

void HtCalculator::set_handles(std::vector<mor::Jet>* jets,
		 std::vector<mor::Electron>* isolated_electrons,
		 std::vector<mor::Muon>* isolated_muons,
		 std::vector<mor::MET>* corrected_mets)
{
	this->jets = jets;
	this->isolated_electrons = isolated_electrons;
	this->isolated_muons = isolated_muons;
	this->corrected_mets = corrected_mets;

	ht = -1;
}

double HtCalculator::get_ht()
{
	if(ht != -1)
		return ht;
	else
		return calculate_ht();
}

double HtCalculator::calculate_ht()
{
  	double pt_sum=0;

	for(std::vector<mor::Jet>::iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		pt_sum += jet_iter->Pt();
	}

	for(std::vector<mor::Muon>::iterator mu_iter = isolated_muons->begin();
		mu_iter != isolated_muons->end();
		++mu_iter)
		pt_sum += mu_iter->Pt();

	for(std::vector<mor::Electron>::iterator e_iter = isolated_electrons->begin();
		e_iter != isolated_electrons->end();
		++e_iter)
		pt_sum += e_iter->Pt();

	//pt_sum += corrected_mets->begin()->pt();
	ht = pt_sum;

	return ht;
}
