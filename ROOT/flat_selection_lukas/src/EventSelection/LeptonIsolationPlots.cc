#include "../../interface/EventSelection/LeptonIsolationPlots.h"

LeptonIsolationPlots::LeptonIsolationPlots(std::string ident)
{
	id = ident;

	gen_evt = NULL;
}

LeptonIsolationPlots::~LeptonIsolationPlots()
{
}

void LeptonIsolationPlots::set_gen_evt(mor::TTbarGenEvent *gen_evt)
{
	this->gen_evt = gen_evt;
}

void LeptonIsolationPlots::plot_all()
{
	matchleptons_plotiso_plotveto();
}

void LeptonIsolationPlots::book_histos()
{
	//isolation plots
	histos1d[("match_mu_trackiso"+id).c_str()]=histo_writer->create_1d(("match_mu_trackiso"+id).c_str(),"matched Muon Track Iso",120,0,60, "TrackIso [GeV]");
	histos1d[("match_mu_hcaliso"+id).c_str()]=histo_writer->create_1d(("match_mu_hcaliso"+id).c_str(),"matched Muon Hcal Iso",120,0,60, "HCAL Iso [GeV]");
	histos1d[("match_mu_ecaliso"+id).c_str()]=histo_writer->create_1d(("match_mu_ecaliso"+id).c_str(),"matched Muon Ecal Iso",120,0,60, "ECAL Iso [GeV]");
	histos1d[("match_mu_caloiso"+id).c_str()]=histo_writer->create_1d(("match_mu_caloiso"+id).c_str(),"matched Muon CalIso",120,0,60, "CaloIso [GeV]");
	histos1d[("match_mu_sumiso"+id).c_str()]=histo_writer->create_1d(("match_mu_sumiso"+id).c_str(),"matched Muon Ecal+Hcal+trackIso",70,0,140, "SumIso [GeV]");
	histos1d[("match_mu_CombRelIso"+id).c_str()]=histo_writer->create_1d(("match_mu_CombRelIso"+id).c_str(),"matched Muon CombRelIso",75,0,1.5, "comb.rel.Iso.");
	histos1d[("match_e_trackiso"+id).c_str()]=histo_writer->create_1d(("match_e_trackiso"+id).c_str(),"matched Electron Track Iso",120,0,60, "TrackIso [GeV]");
	histos1d[("match_e_hcaliso"+id).c_str()]=histo_writer->create_1d(("match_e_hcaliso"+id).c_str(),"matched Electron Hcal Iso",120,0,60, "HCAL Iso [GeV]");
	histos1d[("match_e_ecaliso"+id).c_str()]=histo_writer->create_1d(("match_e_ecaliso"+id).c_str(),"matched Electron Ecal Iso",120,0,60, "ECAL Iso [GeV]");
	histos1d[("match_e_caloiso"+id).c_str()]=histo_writer->create_1d(("match_e_caloiso"+id).c_str(),"matched Electron CalIso",120,0,60, "CaloIso [GeV]");
	histos1d[("match_e_sumiso"+id).c_str()]=histo_writer->create_1d(("match_e_sumiso"+id).c_str(),"matched Electron SumIso",70,0,140, "SumIso [GeV]");
	histos1d[("match_e_CombRelIso"+id).c_str()]=histo_writer->create_1d(("match_e_CombRelIso"+id).c_str(),"matched Electron CombRelIso",75,0,1.5, "comb.rel.Iso.");
	histos1d[("unmatch_mu_trackiso"+id).c_str()]=histo_writer->create_1d(("unmatch_mu_trackiso"+id).c_str(),"unmatched Muon TrackIso",120,0,60, "TrackIso [GeV]");
	histos1d[("unmatch_mu_hcaliso"+id).c_str()]=histo_writer->create_1d(("unmatch_mu_hcaliso"+id).c_str(),"unmatched Muon Hcal Iso",120,0,60, "HCAL Iso [GeV]");
	histos1d[("unmatch_mu_ecaliso"+id).c_str()]=histo_writer->create_1d(("unmatch_mu_ecaliso"+id).c_str(),"unmatched Muon Ecal Iso",120,0,60, "ECAL Iso [GeV]");
	histos1d[("unmatch_mu_caloiso"+id).c_str()]=histo_writer->create_1d(("unmatch_mu_caloiso"+id).c_str(),"unmatched Muon CaloIso",120,0,60, "CaloIso [GeV]");
	histos1d[("unmatch_mu_sumiso"+id).c_str()]=histo_writer->create_1d(("unmatch_mu_sumiso"+id).c_str(),"unmatched Muon Ecal+Hcal+trackIso",70,0,140, "SumIso [GeV]");
	histos1d[("unmatch_mu_CombRelIso"+id).c_str()]=histo_writer->create_1d(("unmatch_mu_CombRelIso"+id).c_str(),"unmatched Muon CombRelIso",75,0,1.5, "comb.rel.Iso.");
	histos1d[("unmatch_e_trackiso"+id).c_str()]=histo_writer->create_1d(("unmatch_e_trackiso"+id).c_str(),"unmatched Electron Track Iso",120,0,60, "TrackIso [GeV]");
	histos1d[("unmatch_e_hcaliso"+id).c_str()]=histo_writer->create_1d(("unmatch_e_hcaliso"+id).c_str(),"unmatched Electron Hcal Iso",120,0,60, "HCAL Iso [GeV]");
	histos1d[("unmatch_e_ecaliso"+id).c_str()]=histo_writer->create_1d(("unmatch_e_ecaliso"+id).c_str(),"unmatched Electron Ecal Iso",120,0,60, "ECAL Iso [GeV]");
	histos1d[("unmatch_e_caloiso"+id).c_str()]=histo_writer->create_1d(("unmatch_e_caloiso"+id).c_str(),"unmatched Electron CaloIso",120,0,60, "CaloIso [GeV]");
	histos1d[("unmatch_e_sumiso"+id).c_str()]=histo_writer->create_1d(("unmatch_e_sumiso"+id).c_str(),"unmatched Electron SumIso",70,0,140, "SumIso [GeV]");
	histos1d[("unmatch_e_CombRelIso"+id).c_str()]=histo_writer->create_1d(("unmatch_e_CombRelIso"+id).c_str(),"unmatched Electron CombRelIso",75,0,1.5, "comb.rel.Iso.");
	//vetocone plots
	histos1d[("match_mu_veto_hcalEnergy"+id).c_str()]=histo_writer->create_1d(("match_mu_veto_hcalEnergy"+id).c_str(),"matched muon vetocone(size 0.1) hcalEnergy",50,0,100, "HCAL VetoCone [GeV]");
	histos1d[("match_mu_veto_ecalEnergy"+id).c_str()]=histo_writer->create_1d(("match_mu_veto_ecalEnergy"+id).c_str(),"matched muon vetocone(size 0.07) ecalEnergy",50,0,100, "ECAL VetoCone [GeV]");
	histos1d[("match_e_veto_ecalEnergy"+id).c_str()]=histo_writer->create_1d(("match_e_veto_ecalEnergy"+id).c_str(),"matched electron vetocone(size 0.07) ecalEnergy",50,0,100, "ECAL VetoCone [GeV]");
	histos1d[("match_e_veto_hcalEnergy"+id).c_str()]=histo_writer->create_1d(("match_e_veto_hcalEnergy"+id).c_str(),"matched electron vetocone(size(0.1) hcalEnergy",50,0,100, "HCAL VetoCone [GeV]");
	histos1d[("unmatch_mu_veto_hcalEnergy"+id).c_str()]=histo_writer->create_1d(("unmatch_mu_veto_hcalEnergy"+id).c_str(),"unmatched muon vetocone(size 0.1) hcalEnergy",50,0,100, "HCAL VetoCone [GeV]");
	histos1d[("unmatch_mu_veto_ecalEnergy"+id).c_str()]=histo_writer->create_1d(("unmatch_mu_veto_ecalEnergy"+id).c_str(),"unmatched muon vetocone(size 0.07) ecalEnergy",50,0,100, "ECAL VetoCone [GeV]");
	histos1d[("unmatch_e_veto_ecalEnergy"+id).c_str()]=histo_writer->create_1d(("unmatch_e_veto_ecalEnergy"+id).c_str(),"unmatched electron vetocone(size 0.07) ecalEnergy",50,0,100, "ECAL VetoCone [GeV]");
	histos1d[("unmatch_e_veto_hcalEnergy"+id).c_str()]=histo_writer->create_1d(("unmatch_e_veto_hcalEnergy"+id).c_str(),"unmatched electron vetocone(size(0.1) hcalEnergy",50,0,100, "HCAL VetoCone [GeV]");
	
	histos2d[("match_e_caloiso_vs_trackiso"+id).c_str()]=histo_writer->create_2d(("match_e_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60, "TrackIso [GeV]", "CaloIso [GeV");
	histos2d[("unmatch_e_caloiso_vs_trackiso"+id).c_str()]=histo_writer->create_2d(("unmatch_e_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60, "TrackIso [GeV]", "CaloIso [GeV");
	histos2d[("match_mu_caloiso_vs_trackiso"+id).c_str()]=histo_writer->create_2d(("match_mu_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60, "TrackIso [GeV]", "CaloIso [GeV");
	histos2d[("unmatch_mu_caloiso_vs_trackiso"+id).c_str()]=histo_writer->create_2d(("unmatch_mu_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60, "TrackIso [GeV]", "CaloIso [GeV");
	histos2d[("match_e_veto_hcal_vs_veto_ecal"+id).c_str()]=histo_writer->create_2d(("match_e_veto_hcal_vs_veto_ecal"+id).c_str(),"matched electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100, "ECAL VetoCone [GeV]", "HCAL VetoCone [GeV]");
	histos2d[("unmatch_e_veto_hcal_vs_veto_ecal"+id).c_str()]=histo_writer->create_2d(("unmatch_e_veto_hcal_vs_veto_ecal"+id).c_str(),"unmatched electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100, "ECAL VetoCone [GeV]", "HCAL VetoCone [GeV]");
	histos2d[("match_mu_veto_hcal_vs_veto_ecal"+id).c_str()]=histo_writer->create_2d(("match_mu_veto_hcal_vs_veto_ecal"+id).c_str(),"matched electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100, "ECAL VetoCone [GeV]", "HCAL VetoCone [GeV]");
	histos2d[("unmatch_mu_veto_hcal_vs_veto_ecal"+id).c_str()]=histo_writer->create_2d(("unmatch_mu_veto_hcal_vs_veto_ecal"+id).c_str(),"unmatched electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100, "ECAL VetoCone [GeV]", "HCAL VetoCone [GeV]");
}

//plot isolation and vetocone(vetocone to replace dR cut) for semilep and non semilep events
void LeptonIsolationPlots::matchleptons_plotiso_plotveto()
{
	
	//	if(reco_gen_match == NULL || !reco_gen_match->is_geninfo_available())
	//	return;
	
	
	//	if(!(genEvt->isTtBar() && genEvt->isSemiLeptonic()))
	//	{ 
	for(std::vector<mor::Muon>::iterator muon = isolated_muons->begin();
	    muon != isolated_muons->end();
	    ++muon){
		
		histos1d[("unmatch_mu_hcaliso"+id).c_str()]->Fill(muon->hcalIso());
		histos1d[("unmatch_mu_ecaliso"+id).c_str()]->Fill(muon->ecalIso());
		histos1d[("unmatch_mu_trackiso"+id).c_str()]->Fill(muon->trackIso());
		histos1d[("unmatch_mu_caloiso"+id).c_str()]->Fill(muon->caloIso());
		double unmatch_mu_sumIso = (muon->trackIso()+muon->hcalIso()+muon->ecalIso());
		double unmatch_mu_CombRelIso = (unmatch_mu_sumIso)/(muon->pt());
		histos1d[("unmatch_mu_CombRelIso"+id).c_str()]->Fill(unmatch_mu_CombRelIso);
		histos1d[("unmatch_mu_sumiso"+id).c_str()]->Fill(unmatch_mu_sumIso);
		histos1d[("unmatch_mu_veto_hcalEnergy"+id).c_str()]->Fill(muon->hcal_vcone());
		histos1d[("unmatch_mu_veto_ecalEnergy"+id).c_str()]->Fill(muon->ecal_vcone()); 
		
		// 2d plots lepton isolation to see correlations
		histos2d[("unmatch_mu_caloiso_vs_trackiso"+id).c_str()]->Fill(muon->trackIso(), muon->caloIso());
		histos2d[("unmatch_mu_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill(muon->ecal_vcone(), muon->hcal_vcone());
	}
	
	for(std::vector<mor::Electron>::iterator electron = isolated_electrons->begin();electron != isolated_electrons->end();++electron){
		
		histos1d[("unmatch_e_hcaliso"+id).c_str()]->Fill(electron->hcalIso());
		histos1d[("unmatch_e_ecaliso"+id).c_str()]->Fill(electron->ecalIso());
		histos1d[("unmatch_e_trackiso"+id).c_str()]->Fill(electron->trackIso());
		histos1d[("unmatch_e_caloiso"+id).c_str()]->Fill(electron->caloIso());
		double unmatch_e_sumIso = (electron->trackIso()+electron->hcalIso()+electron->ecalIso());
		double unmatch_e_CombRelIso = (unmatch_e_sumIso)/(electron->pt());
		histos1d[("unmatch_e_CombRelIso"+id).c_str()]->Fill(unmatch_e_CombRelIso);
		histos1d[("unmatch_e_sumiso"+id).c_str()]->Fill(unmatch_e_sumIso);
		histos1d[("unmatch_e_veto_hcalEnergy"+id).c_str()]->Fill(electron->hcal_vcone());
		histos1d[("unmatch_e_veto_ecalEnergy"+id).c_str()]->Fill(electron->ecal_vcone());
		
		// 2d plots lepton isolation to see correlations
		histos2d[("unmatch_e_caloiso_vs_trackiso"+id).c_str()]->Fill(electron->trackIso(), electron->caloIso());
		histos2d[("unmatch_e_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill(electron->ecal_vcone(), electron->hcal_vcone());
	}
	
	return; 
	
// 		}
// 	else{
	
	
// 	int max_nmuons = isolated_muons->size(),max_nelectrons = isolated_electrons->size();	
	
// 	// For each candidate loop over all pat leptons and check if pt and eta match
// 	for(int i = 0;i < max_nmuons; i++){
		
// 		//    double dR = ROOT::Math::VectorUtil::DeltaR(muon->p4(),genEvt->singleLepton()->p4());
// 		if(reco_gen_match->lepton_from_top(i,"muon")){
// 			histos1d[("match_mu_hcaliso"+id).c_str()]->Fill((*isolated_muons)[i].hcalIso());
// 			histos1d[("match_mu_ecaliso"+id).c_str()]->Fill((*isolated_muons)[i].ecalIso());
// 			histos1d[("match_mu_trackiso"+id).c_str()]->Fill((*isolated_muons)[i].trackIso());
// 			histos1d[("match_mu_caloiso"+id).c_str()]->Fill((*isolated_muons)[i].caloIso());	
// 			double match_mu_sumIso = ((*isolated_muons)[i].trackIso()+(*isolated_muons)[i].hcalIso()+(*isolated_muons)[i].ecalIso());
// 			double match_mu_CombRelIso = (match_mu_sumIso)/((*isolated_muons)[i].pt());
// 			histos1d[("match_mu_CombRelIso"+id).c_str()]->Fill(match_mu_CombRelIso);
// 			histos1d[("match_mu_sumiso"+id).c_str()]->Fill(match_mu_sumIso);
// 			histos1d[("match_mu_veto_hcalEnergy"+id).c_str()]->Fill((*isolated_muons)[i].hcal_vcone());
// 			histos1d[("match_mu_veto_ecalEnergy"+id).c_str()]->Fill((*isolated_muons)[i].ecal_vcone()); 

// 			// 2d plots lepton isolation to see correlations
// 			histos2d[("match_mu_caloiso_vs_trackiso"+id).c_str()]->Fill((*isolated_muons)[i].trackIso(), (*isolated_muons)[i].caloIso());
// 			histos2d[("match_mu_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill((*isolated_muons)[i].ecal_vcone(), (*isolated_muons)[i].hcal_vcone());
// 		}
// 		else{
// 			histos1d[("unmatch_mu_hcaliso"+id).c_str()]->Fill((*isolated_muons)[i].hcalIso());
// 			histos1d[("unmatch_mu_ecaliso"+id).c_str()]->Fill((*isolated_muons)[i].ecalIso());
// 			histos1d[("unmatch_mu_trackiso"+id).c_str()]->Fill((*isolated_muons)[i].trackIso());
// 			histos1d[("unmatch_mu_caloiso"+id).c_str()]->Fill((*isolated_muons)[i].caloIso());
// 			double unmatch_mu_sumIso = ((*isolated_muons)[i].trackIso()+(*isolated_muons)[i].hcalIso()+(*isolated_muons)[i].ecalIso());
// 			double unmatch_mu_CombRelIso = (unmatch_mu_sumIso)/((*isolated_muons)[i].pt());
// 			histos1d[("unmatch_mu_CombRelIso"+id).c_str()]->Fill(unmatch_mu_CombRelIso);
// 			histos1d[("unmatch_mu_sumiso"+id).c_str()]->Fill(unmatch_mu_sumIso);
// 			histos1d[("unmatch_mu_veto_hcalEnergy"+id).c_str()]->Fill((*isolated_muons)[i].hcal_vcone());
// 			histos1d[("unmatch_mu_veto_ecalEnergy"+id).c_str()]->Fill((*isolated_muons)[i].ecal_vcone()); 

// 			// 2d plots lepton isolation to see correlations
// 			histos2d[("unmatch_mu_caloiso_vs_trackiso"+id).c_str()]->Fill((*isolated_muons)[i].trackIso(), (*isolated_muons)[i].caloIso());
// 			histos2d[("unmatch_mu_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill((*isolated_muons)[i].ecal_vcone(), (*isolated_muons)[i].hcal_vcone());
// 		}
// 	}
	
// 	//fill matched amd unmatched semileptonic electron
// 	for(int i = 0;i < max_nelectrons; i++){
		
// 		if(reco_gen_match->lepton_from_top(i,"electron")){
			
// 			histos1d[("match_e_hcaliso"+id).c_str()]->Fill((*isolated_electrons)[i].hcalIso());
// 			histos1d[("match_e_ecaliso"+id).c_str()]->Fill((*isolated_electrons)[i].ecalIso());
// 			histos1d[("match_e_trackiso"+id).c_str()]->Fill((*isolated_electrons)[i].trackIso());
// 			histos1d[("match_e_caloiso"+id).c_str()]->Fill((*isolated_electrons)[i].caloIso());
// 			double match_e_sumIso = ((*isolated_electrons)[i].trackIso()+(*isolated_electrons)[i].hcalIso()+(*isolated_electrons)[i].ecalIso());
// 			double match_e_CombRelIso = (match_e_sumIso)/((*isolated_electrons)[i].pt());
// 			histos1d[("match_e_CombRelIso"+id).c_str()]->Fill(match_e_CombRelIso);
// 			histos1d[("match_e_sumiso"+id).c_str()]->Fill(match_e_sumIso);
// 			histos1d[("match_e_veto_hcalEnergy"+id).c_str()]->Fill((*isolated_electrons)[i].hcal_vcone());
// 			histos1d[("match_e_veto_ecalEnergy"+id).c_str()]->Fill((*isolated_electrons)[i].ecal_vcone());

// 			// 2d plots lepton isolation to see correlations
// 			histos2d[("match_e_caloiso_vs_trackiso"+id).c_str()]->Fill((*isolated_electrons)[i].trackIso(), (*isolated_electrons)[i].caloIso());
// 			histos2d[("match_e_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill((*isolated_electrons)[i].ecal_vcone(), (*isolated_electrons)[i].hcal_vcone());
// 		}
// 		else{
// 			histos1d[("unmatch_e_hcaliso"+id).c_str()]->Fill((*isolated_electrons)[i].hcalIso());
// 			histos1d[("unmatch_e_ecaliso"+id).c_str()]->Fill((*isolated_electrons)[i].ecalIso());
// 			histos1d[("unmatch_e_trackiso"+id).c_str()]->Fill((*isolated_electrons)[i].trackIso());
// 			histos1d[("unmatch_e_caloiso"+id).c_str()]->Fill((*isolated_electrons)[i].caloIso());
// 			double unmatch_e_sumIso = ((*isolated_electrons)[i].trackIso()+(*isolated_electrons)[i].hcalIso()+(*isolated_electrons)[i].ecalIso());
// 			double unmatch_e_CombRelIso = (unmatch_e_sumIso)/((*isolated_electrons)[i].pt());
// 			histos1d[("unmatch_e_CombRelIso"+id).c_str()]->Fill(unmatch_e_CombRelIso);
// 			histos1d[("unmatch_e_sumiso"+id).c_str()]->Fill(unmatch_e_sumIso);
// 			histos1d[("unmatch_e_veto_hcalEnergy"+id).c_str()]->Fill((*isolated_electrons)[i].hcal_vcone());
// 			histos1d[("unmatch_e_veto_ecalEnergy"+id).c_str()]->Fill((*isolated_electrons)[i].ecal_vcone());
				
// 			// 2d plots lepton isolation to see correlations
// 			histos2d[("unmatch_e_caloiso_vs_trackiso"+id).c_str()]->Fill((*isolated_electrons)[i].trackIso(), (*isolated_electrons)[i].caloIso());
// 			histos2d[("unmatch_e_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill((*isolated_electrons)[i].ecal_vcone(), (*isolated_electrons)[i].hcal_vcone());
// 		}
// 	}
// 	}
}
