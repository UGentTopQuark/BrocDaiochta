#include "../../interface/EventSelection/PlotGenerator.h" 

PlotGenerator::PlotGenerator(std::string ident)
{
	id = "_"+ident;

	jets=NULL;
	isolated_muons = NULL;
	isolated_electrons = NULL;
	all_electrons = NULL;
	corrected_mets = NULL;
	uncorrected_mets = NULL;

	// objects for generator truth information

        cuts=NULL;
	mass_reco = NULL;	// mass reco from cuts class used
	ht_calc = NULL;

	bjet_finder = new BJetFinder();
	mass_jet_match = new MassJetMatch();

	tprime_mass = -1;

	/*
	 *	book plot classes
	 */
	met_plots = new METPlots(id);

	abcd_plots = new ABCDMethodPlots(id);

	trigger_plots = new TriggerPlots(id);

	lept_iso_plots = new LeptonIsolationPlots(id);

	xplusnjets_plots = new XPlusNJetsPlots(id);

	btag_plots = new BTagPlots(id);
	btag_plots->set_bjet_finder(bjet_finder);

	mass_plots = new MassPlots(id);
	mass_plots->set_bjet_finder(bjet_finder);
	mass_plots->set_mass_jet_match(mass_jet_match);

	mva_prod = new MVAInputProducer();
	mva_prod->set_bjet_finder(bjet_finder);
	mva_prod->set_ident(id);

	basic_plots = new BasicObjectPlots(id);
}

PlotGenerator::~PlotGenerator()
{
	if(mva_prod){
		delete mva_prod;
		mva_prod = NULL;
	}

	if(trigger_plots){
		delete trigger_plots;
		trigger_plots = NULL;
	}

	if(btag_plots){
		delete btag_plots;
		btag_plots = NULL;
	}

	if(basic_plots){
		delete basic_plots;
		basic_plots = NULL;
	}

	if(mass_plots){
		delete mass_plots;
		mass_plots = NULL;
	}

	if(lept_iso_plots){
		delete lept_iso_plots;
		lept_iso_plots = NULL;
	}
	
	if(met_plots != NULL){
		delete met_plots;
		met_plots = NULL;
	}

	if(abcd_plots){
		delete abcd_plots;
		abcd_plots = NULL;
	}

	if(xplusnjets_plots){
		delete xplusnjets_plots;
		xplusnjets_plots = NULL;
	}

	if(mass_jet_match != NULL){
		delete mass_jet_match;
		mass_jet_match = NULL;
	}

	if(bjet_finder != NULL){
		delete bjet_finder;
		bjet_finder = NULL;	
	}
}

void PlotGenerator::set_handles(std::vector<mor::Muon> *muons,
			std::vector<mor::Jet> *jets,
			std::vector<mor::Electron> *electrons,
			std::vector<mor::MET> *mets,
			mor::Trigger *trigger)
{
        this->electrons = electrons;
        this->muons = muons;
        this->uncut_jets = jets;
        this->mets = mets;
	this->HLTR = trigger;
}

void PlotGenerator::next_event()
{
	if(cuts != NULL) cuts->next_event();

	ht = -1;

        isolated_electrons = cuts->get_isolated_electrons();

        all_electrons = cuts->get_all_electrons();

        isolated_muons = cuts->get_isolated_muons();

	jets = cuts->get_jets();

	corrected_mets = cuts->get_corrected_mets();

	mass_jet_match->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets,gen_evt,muons,electrons);
	bjet_finder->set_handles(jets);

	/*
	 *	setting handles for plot classes
	 */
	met_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	met_plots->set_met_handle(mets);

	abcd_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);

	xplusnjets_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);

	trigger_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	trigger_plots->set_muons(muons);
	trigger_plots->set_trigger(HLTR);
	trigger_plots->set_gen_evt(gen_evt);

	lept_iso_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	lept_iso_plots->set_gen_evt(gen_evt);

	btag_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	btag_plots->set_gen_evt(gen_evt);

	mass_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	mass_plots->set_gen_evt(gen_evt);

	basic_plots->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	basic_plots->set_uncut_jets(uncut_jets);

	mva_prod->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
}

void PlotGenerator::plot()
{
	/*
	 *	Plots with applied cuts
	 */
	// plot if cuts are applied (cuts != NULL) and event passes cut
	// cirteria or if cuts are not applied (cuts == NULL)

	if((cuts != NULL && cuts->cut() == false)
	   || cuts == NULL){

		/*
		 *	plot individual plot classes
		 */

		if(plot_met) met_plots->plot();
		if(plot_abcd) abcd_plots->plot();
		if(plot_xplusnjets) xplusnjets_plots->plot();
		if(plot_trigger) trigger_plots->plot();
		if(plot_lept_iso) lept_iso_plots->plot();
		if(plot_btag) btag_plots->plot();
		if(plot_mass) mass_plots->plot();
		if(plot_kinematics) basic_plots->plot();

		//mva_prod->print_MVA_input();
	
		histos1d[("event_counter"+id).c_str()]->Fill(1);
	}

	/*
	 *	Plots created even for events that don't pass the cuts
	 */
	histos1d[("event_counter"+id).c_str()]->Fill(0);
}

//Book all the histograms
void PlotGenerator::book_histos()
{
	/*
	 *	1D Histos
	 */

	// 0 = all events, 1 = events passing cuts
	histos1d[("event_counter"+id).c_str()]=histo_writer->create_1d(("event_counter"+id).c_str(),"processed events",5,-0.5,4.5);


	/*
	 *=========================================================================================
	 */

	/*
	 *	2D Histos
	 */
}

void PlotGenerator::apply_cuts(Cuts *mycuts)
{
	cuts = mycuts;
	btag_plots->set_btag_cuts(cuts->get_min_btag(),cuts->get_name_btag());		
	mass_reco = cuts->get_mass_reco();
	ht_calc = cuts->get_ht_calc();
	mass_plots->set_ht_calc(ht_calc);
	mass_plots->set_mass_reco(mass_reco);
	basic_plots->set_ht_calc(ht_calc);
	mass_reco->set_tprime_mass(tprime_mass);
	mass_jet_match->set_mass_reco(mass_reco);
	mva_prod->set_mass_reco(mass_reco);
}

void PlotGenerator::deactivate_cuts()
{
	cuts = NULL;
}

void PlotGenerator::set_tprime_mass(double mass)
{
	tprime_mass = mass;
}

void PlotGenerator::set_histo_writer(HistoWriter *histo_writer)
{
	this->histo_writer = histo_writer;

	if(plot_met) met_plots->set_histo_writer(histo_writer);
	if(plot_mass) mass_plots->set_histo_writer(histo_writer);
	if(plot_kinematics) basic_plots->set_histo_writer(histo_writer);
	if(plot_abcd) abcd_plots->set_histo_writer(histo_writer);
	if(plot_xplusnjets) xplusnjets_plots->set_histo_writer(histo_writer);
	if(plot_btag) btag_plots->set_histo_writer(histo_writer);
	if(plot_lept_iso) lept_iso_plots->set_histo_writer(histo_writer);
	if(plot_trigger) trigger_plots->set_histo_writer(histo_writer);

	// book histos by means of cmssw file service
	book_histos();
}

void PlotGenerator::set_gen_evt(mor::TTbarGenEvent *gen_evt)
{
	this->gen_evt = gen_evt;

	met_plots->set_gen_evt(gen_evt);
	btag_plots->set_gen_evt(gen_evt);
}
