#include "../../interface/EventSelection/Plots.h"

Plots::Plots()
{
	histos_booked = false;
}

Plots::~Plots()
{
}

void Plots::book_histos()
{
}

void Plots::prepare_objects()
{
}

void Plots::set_handles(std::vector<mor::Jet>* jets,
		 std::vector<mor::Electron>* isolated_electrons,
		 std::vector<mor::Muon>* isolated_muons,
		 std::vector<mor::MET>* corrected_mets)
{
	this->jets = jets;
	this->isolated_electrons = isolated_electrons;
	this->isolated_muons = isolated_muons;
	this->corrected_mets = corrected_mets;
}

void Plots::plot()
{
	if(!histos_booked){
//		book_histos();
		histos_booked = true;
	}
	plot_all();
}

void Plots::set_histo_writer(HistoWriter *histo_writer)
{
	this->histo_writer = histo_writer;
	book_histos();
}

void Plots::set_gen_evt(mor::TTbarGenEvent *gen_evt)
{
	this->gen_evt = gen_evt;
}
