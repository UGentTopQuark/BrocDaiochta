#include "../../interface/EventSelection/LeptonSelector.h"

template LeptonSelector<mor::Electron>::LeptonSelector();
template LeptonSelector<mor::Electron>::~LeptonSelector();
template LeptonSelector<mor::Muon>::LeptonSelector();
template LeptonSelector<mor::Muon>::~LeptonSelector();

template void LeptonSelector<mor::Electron>::set_max_trackiso(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_max_ecaliso(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_max_caliso(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_max_hcaliso(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_max_hcal_veto_cone(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_max_ecal_veto_cone(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_min_dR(std::vector<double> *min);
template void LeptonSelector<mor::Electron>::set_min_pt(std::vector<double> *min);
template void LeptonSelector<mor::Electron>::set_min_et(std::vector<double> *min);
template void LeptonSelector<mor::Electron>::set_min_relIso(std::vector<double> *min);
template void LeptonSelector<mor::Electron>::set_max_eta(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_electronID(std::vector<double> *eid);
template void LeptonSelector<mor::Electron>::set_max_d0(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_max_d0sig(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_max_chi2(std::vector<double> *max);
template void LeptonSelector<mor::Electron>::set_min_nHits(std::vector<double> *min);
template void LeptonSelector<mor::Electron>::set_lepton_type(int lepton_type);

template void LeptonSelector<mor::Muon>::set_max_trackiso(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_max_ecaliso(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_max_hcaliso(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_max_caliso(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_max_hcal_veto_cone(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_max_ecal_veto_cone(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_min_dR(std::vector<double> *min);
template void LeptonSelector<mor::Muon>::set_min_pt(std::vector<double> *min);
template void LeptonSelector<mor::Muon>::set_min_et(std::vector<double> *min);
template void LeptonSelector<mor::Muon>::set_min_relIso(std::vector<double> *min);
template void LeptonSelector<mor::Muon>::set_max_eta(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_electronID(std::vector<double> *eid);
template void LeptonSelector<mor::Muon>::set_max_d0(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_max_d0sig(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_max_chi2(std::vector<double> *max);
template void LeptonSelector<mor::Muon>::set_min_nHits(std::vector<double> *min);
template void LeptonSelector<mor::Muon>::set_lepton_type(int lepton_type);

template bool LeptonSelector<mor::Electron>::cut_trackiso(mor::Electron *lepton_iter, double &max_trackiso);
template bool LeptonSelector<mor::Electron>::cut_ecaliso(mor::Electron *lepton_iter, double &max_ecaliso);
template bool LeptonSelector<mor::Electron>::cut_caliso(mor::Electron *lepton_iter, double &max_caliso);
template bool LeptonSelector<mor::Electron>::cut_hcaliso(mor::Electron *lepton_iter, double &max_hcaliso);
template bool LeptonSelector<mor::Electron>::cut_hcal_veto_cone(mor::Electron *lepton_iter, double &max_hcal_veto_cone);
template bool LeptonSelector<mor::Electron>::cut_ecal_veto_cone(mor::Electron *lepton_iter, double &max_ecal_veto_cone);
template bool LeptonSelector<mor::Electron>::cut_dR(mor::Electron *lepton_iter, double &min_dR);
template bool LeptonSelector<mor::Electron>::cut_pt(mor::Electron *lepton_iter, double &min_pt);
template bool LeptonSelector<mor::Electron>::cut_et(mor::Electron *lepton_iter, double &min_et);
template bool LeptonSelector<mor::Electron>::cut_relIso(mor::Electron *lepton_iter, double &min_relIso);
template bool LeptonSelector<mor::Electron>::cut_nHits(mor::Electron *lepton_iter, double &min_nHits);
template bool LeptonSelector<mor::Electron>::cut_d0(mor::Electron *lepton_iter, double &max_d0);
template bool LeptonSelector<mor::Electron>::cut_d0sig(mor::Electron *lepton_iter, double &max_d0sig);

template bool LeptonSelector<mor::Muon>::cut_trackiso(mor::Muon *lepton_iter, double &max_trackiso);
template bool LeptonSelector<mor::Muon>::cut_ecaliso(mor::Muon *lepton_iter, double &max_ecaliso);
template bool LeptonSelector<mor::Muon>::cut_caliso(mor::Muon *lepton_iter, double &max_caliso);
template bool LeptonSelector<mor::Muon>::cut_hcaliso(mor::Muon *lepton_iter, double &max_hcaliso);
template bool LeptonSelector<mor::Muon>::cut_hcal_veto_cone(mor::Muon *lepton_iter, double &max_hcal_veto_cone);
template bool LeptonSelector<mor::Muon>::cut_ecal_veto_cone(mor::Muon *lepton_iter, double &max_ecal_veto_cone);
template bool LeptonSelector<mor::Muon>::cut_dR(mor::Muon *lepton_iter, double &min_dR);
template bool LeptonSelector<mor::Muon>::cut_pt(mor::Muon *lepton_iter, double &min_pt);
template bool LeptonSelector<mor::Muon>::cut_et(mor::Muon *lepton_iter, double &min_et);
template bool LeptonSelector<mor::Muon>::cut_d0(mor::Muon *lepton_iter, double &max_d0);
template bool LeptonSelector<mor::Muon>::cut_d0sig(mor::Muon *lepton_iter, double &max_d0sig);
//template bool LeptonSelector<mor::Muon>::cut_eta(mor::Muon *lepton_iter, double &max_eta);
template bool LeptonSelector<mor::Muon>::cut_relIso(mor::Muon *lepton_iter, double &min_relIso);
//template bool LeptonSelector<mor::Muon>::cut_chi2(mor::Muon *lepton_iter, double &max_chi2);
//template bool LeptonSelector<mor::Muon>::cut_nHits(mor::Muon *lepton_iter, double &min_nHits);

template std::vector<mor::Muon>* LeptonSelector<mor::Muon>::get_leptons(std::vector<mor::Muon> *leptons, std::vector<mor::Jet> *myjets);
template std::vector<mor::Electron>* LeptonSelector<mor::Electron>::get_leptons(std::vector<mor::Electron> *leptons, std::vector<mor::Jet> *myjets);

template <class myLepton>
LeptonSelector<myLepton>::LeptonSelector()
{
	max_trackiso = NULL;
	max_caliso = NULL;
	max_hcaliso = NULL;
	max_ecaliso = NULL;
	max_ecal_veto_cone = NULL;
	max_hcal_veto_cone = NULL;
	min_dR = NULL;
	min_pt = NULL;
	min_et = NULL;
	min_relIso = NULL;
	electronID = NULL;
	max_chi2 = NULL;
	max_d0 = NULL;
	max_d0sig = NULL;
	min_nHits = NULL;
	lepton_type = -1;
	max_eta = NULL;

	isolated_leptons = NULL;
}

template <class myLepton>
LeptonSelector<myLepton>::~LeptonSelector()
{
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_trackiso(std::vector<double> *max)
{
	max_trackiso = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_ecaliso(std::vector<double> *max)
{
	max_ecaliso = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_caliso(std::vector<double> *max)
{
	max_caliso = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_hcaliso(std::vector<double> *max)
{
	max_hcaliso = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_hcal_veto_cone(std::vector<double> *max)
{
	max_hcal_veto_cone = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_ecal_veto_cone(std::vector<double> *max)
{
	max_ecal_veto_cone = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_dR(std::vector<double> *min)
{
	min_dR = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_pt(std::vector<double> *min)
{
	min_pt = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_et(std::vector<double> *min)
{
	min_et = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_relIso(std::vector<double> *min)
{
	min_relIso = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_eta(std::vector<double> *max)
{
	max_eta = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_lepton_type(int type)
{
	lepton_type = type;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_electronID(std::vector<double> *eid)
{
	electronID = eid;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_min_nHits(std::vector<double> *min)
{
	min_nHits = min;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_d0(std::vector<double> *max)
{
	max_d0 = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_d0sig(std::vector<double> *max)
{
	max_d0sig = max;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_max_chi2(std::vector<double> *max)
{
	max_chi2 = max;
}

// Here the functions for any cuts which have been set are called
template <class myLepton>
std::vector<myLepton>* LeptonSelector<myLepton>::get_leptons(std::vector<myLepton> *leptons, std::vector<mor::Jet> *myjets)
{
	bool force_triggered_lepton=cut_e_trigger || cut_mu_trigger;

	jets = myjets;
        isolated_leptons = new std::vector<myLepton>();

	if((max_trackiso != NULL && max_trackiso->size() > 1) ||
	   (max_ecaliso != NULL && max_ecaliso->size() > 1) ||
	   (max_caliso != NULL && max_caliso->size() > 1) ||
	   (max_hcaliso != NULL && max_hcaliso->size() > 1) ||
	   (max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > 1) ||
	   (max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > 1) ||
	   (min_dR != NULL && min_dR->size() > 1) ||
	   (min_relIso != NULL && min_relIso->size() > 1) ||
	   (min_pt != NULL && min_pt->size() >1) ||
	   (min_et != NULL && min_et->size() >1) ||
	   (max_d0 != NULL && max_d0->size() >1) ||
	   (max_eta != NULL && max_eta->size() >1) ||
	   (max_d0sig != NULL && max_d0sig->size() >1) ||
	   (max_chi2 != NULL && max_chi2->size() >1) ||
	   (min_nHits != NULL && min_nHits->size() >1) ||
	   (electronID != NULL && electronID->size() >1)){

		// copy of leptons to be able to delete the ones that pass the
		// currently tightest cut
		std::vector<myLepton*> tmp_leptons;
		for(typename std::vector<myLepton>::iterator lepton_iter = leptons->begin(); lepton_iter!=leptons->end();
		++lepton_iter){
			tmp_leptons.push_back(&(*lepton_iter));
		}

		double max_size = max_trackiso->size();
		if(max_ecaliso != NULL && max_ecaliso->size() > max_size) max_size = max_ecaliso->size();
		if(max_caliso != NULL && max_caliso->size() > max_size) max_size = max_caliso->size();
		if(max_hcaliso != NULL && max_hcaliso->size() > max_size) max_size = max_hcaliso->size();
		if(max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > max_size) max_size = max_hcal_veto_cone->size();
		if(max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > max_size) max_size = max_ecal_veto_cone->size();
		if(min_dR != NULL && min_dR->size() > max_size) max_size = min_dR->size();
		if(min_relIso != NULL && min_relIso->size() > max_size) max_size = min_relIso->size();
		if(min_pt != NULL && min_pt->size() > max_size) max_size = min_pt->size();
		if(min_et != NULL && min_et->size() > max_size) max_size = min_et->size();
	   	if(electronID != NULL && electronID->size() > max_size) max_size = electronID->size();
	   	if(max_chi2 != NULL && max_chi2->size() > max_size) max_size = max_chi2->size();
	   	if(max_d0 != NULL && max_d0->size() > max_size) max_size = max_d0->size();
	   	if(max_d0sig != NULL && max_d0sig->size() > max_size) max_size = max_d0sig->size();
	   	if(min_nHits != NULL && min_nHits->size() > max_size) max_size = min_nHits->size();
	   	if(max_eta != NULL && max_eta->size() > max_size) max_size = max_eta->size();

		for(unsigned int nasym_cut = 0; nasym_cut < max_size; ++nasym_cut){
        		typename std::vector<myLepton*>::iterator lepton_iter;
        		for(lepton_iter = tmp_leptons.begin();
			lepton_iter!=tmp_leptons.end();){
        		        double cut_out=false;
			 
        		        if(max_trackiso != NULL && max_trackiso->size() > nasym_cut && (*max_trackiso)[nasym_cut] != -1)
					cut_out = cut_out || cut_trackiso(*lepton_iter, (*max_trackiso)[nasym_cut]);
        		        if(max_ecaliso != NULL && max_ecaliso->size() > nasym_cut && (*max_ecaliso)[nasym_cut] != -1)
					cut_out = cut_out || cut_ecaliso(*lepton_iter, (*max_ecaliso)[nasym_cut]);
        		        if(max_caliso != NULL && max_caliso->size() > nasym_cut && (*max_caliso)[nasym_cut] != -1)
					cut_out = cut_out || cut_caliso(*lepton_iter, (*max_caliso)[nasym_cut]);
	       		        if(max_hcaliso != NULL && max_hcaliso->size() > nasym_cut && (*max_hcaliso)[nasym_cut] != -1)
					cut_out = cut_out || cut_hcaliso(*lepton_iter,  (*max_hcaliso)[nasym_cut]);
	       		        if(max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > nasym_cut && (*max_hcal_veto_cone)[nasym_cut] != -1)
					cut_out = cut_out || cut_hcal_veto_cone(*lepton_iter,  (*max_hcal_veto_cone)[nasym_cut]);
	       		        if(max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > nasym_cut && (*max_ecal_veto_cone)[nasym_cut] != -1)
					cut_out = cut_out || cut_ecal_veto_cone(*lepton_iter,  (*max_ecal_veto_cone)[nasym_cut]);
        		        if(min_dR != NULL && min_dR->size() > nasym_cut && (*min_dR)[nasym_cut] != -1)
					cut_out = cut_out || cut_dR(*lepton_iter,  (*min_dR)[nasym_cut]);
        		        if(min_relIso != NULL && min_relIso->size() > nasym_cut && (*min_relIso)[nasym_cut] != -1)
					cut_out = cut_out || cut_relIso(*lepton_iter,  (*min_relIso)[nasym_cut]);
				if(min_pt != NULL && min_pt->size() > nasym_cut && (*min_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_pt(*lepton_iter, (*min_pt)[nasym_cut]);
				if(min_et != NULL && min_et->size() > nasym_cut && (*min_et)[nasym_cut] != -1)
					cut_out = cut_out || cut_et(*lepton_iter, (*min_et)[nasym_cut]);
				if(electronID != NULL && electronID->size() > nasym_cut && (*electronID)[nasym_cut] != -1)
					cut_out = cut_out || cut_electronID(*lepton_iter, (*electronID)[nasym_cut]);
				if(max_chi2 != NULL && max_chi2->size() > nasym_cut && (*max_chi2)[nasym_cut] != -1)
					cut_out = cut_out || cut_chi2(*lepton_iter, (*max_chi2)[nasym_cut]);
				if(max_d0 != NULL && max_d0->size() > nasym_cut && (*max_d0)[nasym_cut] != -1)
					cut_out = cut_out || cut_d0(*lepton_iter, (*max_d0)[nasym_cut]);
				if(max_d0sig != NULL && max_d0sig->size() > nasym_cut && (*max_d0sig)[nasym_cut] != -1)
					cut_out = cut_out || cut_d0sig(*lepton_iter, (*max_d0sig)[nasym_cut]);
				if(min_nHits != NULL && min_nHits->size() > nasym_cut && (*min_nHits)[nasym_cut] != -1)
					cut_out = cut_out || cut_nHits(*lepton_iter, (*min_nHits)[nasym_cut]);
				if(max_eta != NULL && max_eta->size() > nasym_cut && (*max_eta)[nasym_cut] != -1)
					cut_out = cut_out || cut_eta(*lepton_iter, (*max_eta)[nasym_cut]);
				if(lepton_type != -1)
					cut_out = cut_out || cut_lepton_type(*lepton_iter, lepton_type);
				if(force_triggered_lepton)
					cut_out = cut_out || cut_trigger(*lepton_iter);

        		        if(!cut_out)
        		        {
        		                isolated_leptons->push_back(**lepton_iter);
					lepton_iter=tmp_leptons.erase(lepton_iter);
        		        }else{
					lepton_iter++;
				}
        		}

			if(isolated_leptons->size() < nasym_cut+1)
				return isolated_leptons;
		}
	}
	else{
        	typename std::vector<myLepton>::iterator lepton_iter;
        	for(lepton_iter = leptons->begin(); lepton_iter!=leptons->end(); ++lepton_iter){
        	        double cut_out=false;
		 
        	        if(max_trackiso != NULL && max_trackiso->size() > 0 && (*max_trackiso)[0] != -1)
				cut_out = cut_out || cut_trackiso(&(*lepton_iter), (*max_trackiso)[0]);
        	        if(max_ecaliso != NULL && max_ecaliso->size() > 0 && (*max_ecaliso)[0] != -1)
				cut_out = cut_out || cut_ecaliso(&(*lepton_iter), (*max_ecaliso)[0]);
        	        if(max_caliso != NULL && max_caliso->size() > 0 && (*max_caliso)[0] != -1)
				cut_out = cut_out || cut_caliso(&(*lepton_iter), (*max_caliso)[0]);
        	        if(max_hcaliso != NULL && max_hcaliso->size() > 0 && (*max_hcaliso)[0] != -1)
				cut_out = cut_out || cut_hcaliso(&(*lepton_iter), (*max_hcaliso)[0]);
        	        if(max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > 0 && (*max_hcal_veto_cone)[0] != -1)
				cut_out = cut_out || cut_hcal_veto_cone(&(*lepton_iter), (*max_hcal_veto_cone)[0]);
        	        if(max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > 0 && (*max_ecal_veto_cone)[0] != -1)
				cut_out = cut_out || cut_ecal_veto_cone(&(*lepton_iter), (*max_ecal_veto_cone)[0]);
        	        if(min_dR != NULL && min_dR->size() > 0 && (*min_dR)[0] != -1)
				cut_out = cut_out || cut_dR(&(*lepton_iter), (*min_dR)[0]);
        	        if(min_relIso != NULL && min_relIso->size() > 0 && (*min_relIso)[0] != -1)
				cut_out = cut_out || cut_relIso(&(*lepton_iter), (*min_relIso)[0]);
			if(min_pt != NULL && min_pt->size() > 0 && (*min_pt)[0] != -1)
				cut_out = cut_out || cut_pt(&(*lepton_iter), (*min_pt)[0]);
			if(min_et != NULL && min_et->size() > 0 && (*min_et)[0] != -1)
				cut_out = cut_out || cut_et(&(*lepton_iter), (*min_et)[0]);
			if(electronID != NULL && electronID->size() > 0 && (*electronID)[0] != -1)
				cut_out = cut_out || cut_electronID(&(*lepton_iter), (*electronID)[0]);
        	        if(min_nHits != NULL && min_nHits->size() > 0 && (*min_nHits)[0] != -1)
				cut_out = cut_out || cut_nHits(&(*lepton_iter), (*min_nHits)[0]);
        	        if(max_chi2 != NULL && max_chi2->size() > 0 && (*max_chi2)[0] != -1)
				cut_out = cut_out || cut_chi2(&(*lepton_iter), (*max_chi2)[0]);
        	        if(max_d0 != NULL && max_d0->size() > 0 && (*max_d0)[0] != -1)
				cut_out = cut_out || cut_d0(&(*lepton_iter), (*max_d0)[0]);
        	        if(max_d0sig != NULL && max_d0sig->size() > 0 && (*max_d0sig)[0] != -1)
				cut_out = cut_out || cut_d0sig(&(*lepton_iter), (*max_d0sig)[0]);
			if(max_eta != NULL && max_eta->size() > 0 && (*max_eta)[0] != -1)
				cut_out = cut_out || cut_eta(&(*lepton_iter), (*max_eta)[0]);
			if(lepton_type != -1)
				cut_out = cut_out || cut_lepton_type(&(*lepton_iter), lepton_type);
			if(force_triggered_lepton)
				cut_out = cut_out || cut_trigger(&(*lepton_iter));

        	        if(!cut_out)
        	        {
        	                isolated_leptons->push_back(*lepton_iter);
        	        }
        	}
	}

	if(verbose)
		std::cout << "isolated leptons in event: " << isolated_leptons->size()  << std::endl;
 
        return isolated_leptons;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_chi2(myLepton *lepton_iter, double &max_chi2)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_chi2(), dropping ALL LEPTONS" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_chi2(mor::Muon *lepton_iter, double &max_chi2)
{
	if(lepton_iter->chi2() < max_chi2){
		return false;
	}
	else{
		return true;
	}
}

template <>
bool LeptonSelector<mor::Electron>::cut_chi2(mor::Electron *lepton_iter, double &max_chi2)
{
	std::cerr << "WARNING: electrons have no chi2 value in LeptonSelector::cut_chi2()" << std::endl;
	return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_d0(myLepton *lepton_iter, double &max_d0)
{
	double d0 = lepton_iter->d0();

	if(fabs(d0) < max_d0){
		if(verbose){
			std::cout << "cut d0: " << fabs(d0) << " max_d0: " << max_d0 << std::endl;
			std::cout << "cut false." << std::endl;
		}
		return false;
	}
	else{
		if(verbose){
			std::cout << "cut d0: " << fabs(d0) << " max_d0: " << max_d0 << std::endl;
			std::cout << "cut true." << std::endl;
		}

		return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_d0sig(myLepton *lepton_iter, double &max_d0sig)
{
	double d0sig = -1000;

	if(lepton_iter->track_available()){
			double d0 = lepton_iter->d0();
			double d0sigma = lepton_iter->d0_sigma();
			d0sig = d0/d0sigma;
	}else{
		std::cerr << "WARNING: LeptonSelector: muon track not available" << std::endl;
		return true;
	}

	if(fabs(d0sig) < max_d0sig){
		if(verbose){
			std::cout << "cut d0sig: " << fabs(d0sig) << " max_d0sig: " << max_d0sig << std::endl;
			std::cout << "cut false." << std::endl;
		}
		return false;
	}
	else{
		if(verbose){
			std::cout << "cut d0sig: " << fabs(d0sig) << " max_d0sig: " << max_d0sig << std::endl;
			std::cout << "cut true." << std::endl;
		}
		return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_trigger(myLepton *lepton_iter)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_trigger()" << std::endl;
		return true;
}

template <>
bool LeptonSelector<mor::Electron>::cut_trigger(mor::Electron *lepton_iter)
{
	std::string electron_trigger = "HLT_Ele15_LW_L1R";

	if(lepton_iter->triggered(electron_trigger) || !cut_e_trigger)
		return false;
	else
		return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_trigger(mor::Muon *lepton_iter)
{
	std::string muon_trigger = "HLT_Mu15";

	if(lepton_iter->triggered(muon_trigger) || !cut_mu_trigger)
		return false;
	else
		return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nHits(myLepton *lepton_iter, double &min_nHits)
{
	std::cerr << "WARNING: lepton type in LeptonSelector::cut_nHits() has no nHits information" << std::endl;
        return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_nHits(mor::Muon *lepton_iter, double &min_nHits)
{
        if(lepton_iter->nHits() >= min_nHits){
		if(verbose){
			std::cout << "cut nhits: " << (lepton_iter->nHits()) << " min_nHits: " << min_nHits << std::endl;
			std::cout << "cut false." << std::endl;
		}
                return false;
	}
        else{
		if(verbose){
			std::cout << "cut nhits: " << (lepton_iter->nHits()) << " min_nHits: " << min_nHits << std::endl;
			std::cout << "cut true." << std::endl;
		}
                return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_electronID(myLepton *lepton_iter, double &electronID)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_electronID()" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_electronID(mor::Muon *lepton_iter, double &electronID)
{
	std::cerr << "WARNING: electonID set for muons" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Electron>::cut_electronID(mor::Electron *lepton_iter, double &electronID)
{
	if(lepton_iter->lepton_id_passed())
		return false;
	else
		return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_lepton_type(myLepton *lepton_iter, int &lepton_type)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_lepton_type()" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_lepton_type(mor::Muon *lepton_iter, int &lepton_type)
{
	bool cut_out = false;
	switch(lepton_type){
		case 0:
			if(!lepton_iter->lepton_id_passed())
				cut_out = true;
			break;
		default:
			cut_out = true;
			std::cerr << "WARNING: lepton type is not valid in LeptonSelector::cut_lepton_type()" << std::endl;
			break;
	}

	return cut_out;
}

template <>
bool LeptonSelector<mor::Electron>::cut_lepton_type(mor::Electron *lepton_iter, int &lepton_type)
{
	std::cerr << "WARNING: lepton_type unknown for electrons: FILTERING ALL ELECTRONS" << std::endl;
	return true;
}


template <typename myLepton>
bool LeptonSelector<myLepton>::cut_trackiso(myLepton *lepton_iter, double &max_trackiso)
{
        if(lepton_iter->trackIso()<max_trackiso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_ecal_veto_cone(myLepton *lepton_iter, double &max_ecal_veto_cone)
{
        if(lepton_iter->ecal_vcone() < max_ecal_veto_cone)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_hcal_veto_cone(myLepton *lepton_iter, double &max_hcal_veto_cone)
{
        if(lepton_iter->hcal_vcone() < max_hcal_veto_cone)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_ecaliso(myLepton *lepton_iter, double &max_ecaliso)
{
        if(lepton_iter->ecalIso() < max_ecaliso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_caliso(myLepton *lepton_iter, double &max_caliso)
{
        if(lepton_iter->ecalIso()+lepton_iter->hcalIso() < max_caliso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_eta(myLepton *lepton_iter, double &max_eta)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_eta(), dropping ALL LEPTONS" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_eta(mor::Muon *lepton_iter, double &max_eta)
{
        if(fabs(lepton_iter->Eta()) < max_eta){
		if(verbose){
			std::cout << "cut eta: " << fabs(lepton_iter->Eta()) << " max_eta: " << max_eta << std::endl;
			std::cout << "cut false." << std::endl;
		}
                return false;
	}
        else{
		if(verbose){
			std::cout << "cut eta: " << fabs(lepton_iter->Eta()) << " max_eta: " << max_eta << std::endl;
			std::cout << "cut true." << std::endl;
		}
                return true;
	}
}

template <>
bool LeptonSelector<mor::Electron>::cut_eta(mor::Electron *lepton_iter, double &max_eta)
{
	double eta = lepton_iter->Eta();
	// exclude transition region between barrel and endcap for electrons
        if((fabs(eta) < max_eta) && !(fabs(eta > 1.442) && fabs(eta < 1.560))){
		if(verbose){
			std::cout << "cut eta: " << fabs(lepton_iter->Eta()) << " max_eta: " << max_eta << std::endl;
			std::cout << "cut false." << std::endl;
		}
                return false;
	}
        else{
		if(verbose){
			std::cout << "cut eta: " << fabs(lepton_iter->Eta()) << " max_eta: " << max_eta << std::endl;
			std::cout << "cut true." << std::endl;
		}
                return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_hcaliso(myLepton *lepton_iter, double &max_hcaliso)
{
        if(lepton_iter->hcalIso() < max_hcaliso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_pt(myLepton *lepton_iter, double &min_pt)
{
        if(lepton_iter->Pt() > min_pt){
		if(verbose){
			std::cout << "cut pt: " << fabs(lepton_iter->Pt()) << " min_pt: " << min_pt << std::endl;
			std::cout << "cut false." << std::endl;
		}
                return false;
	}
        else{
		if(verbose){
			std::cout << "cut pt: " << fabs(lepton_iter->Pt()) << " min_pt: " << min_pt << std::endl;
			std::cout << "cut true." << std::endl;
		}
                return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_et(myLepton *lepton_iter, double &min_et)
{
        if(lepton_iter->Et() > min_et){
		if(verbose){
			std::cout << "cut et: " << fabs(lepton_iter->Et()) << " min_et: " << min_et << std::endl;
			std::cout << "cut false." << std::endl;
		}
                return false;
	}
        else{
		if(verbose){
			std::cout << "cut et: " << fabs(lepton_iter->Et()) << " min_et: " << min_et << std::endl;
			std::cout << "cut true." << std::endl;
		}
                return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_dR(myLepton *lepton_iter, double &min_dR)
{
        for(std::vector<mor::Jet>::iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter)
          {
            double dR = ROOT::Math::VectorUtil::DeltaR(*lepton_iter,*jet_iter);

            if(dR < min_dR)
              return true;
          }

        return false;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_relIso(myLepton *lepton_iter, double &min_relIso)
{
	double relIso=0;
	relIso = lepton_iter->Pt()/(lepton_iter->Pt() + lepton_iter->ecalIso()
		 + lepton_iter->hcalIso() + lepton_iter->trackIso());

	if(relIso <= min_relIso){
		if(verbose){
			std::cout << "cut relIso: " << relIso << " min_relIso: " << min_relIso << std::endl;
			std::cout << "cut true." << std::endl;
		}
		return true;
	}
	else{
		if(verbose){
			std::cout << "cut relIso: " << relIso << " min_relIso: " << min_relIso << std::endl;
			std::cout << "cut false." << std::endl;
		}
        	return false;
	}
}
