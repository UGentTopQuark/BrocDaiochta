#include "../../interface/EventSelection/JetSelector.h"

/*************************************************************************
Jets which are in returned vector:
If eta not set; Jets which pass pt cut + all other jets.
If eta set; Jets which pass pt and eta cut + other jets which pass eta cut.

If one of first few jets passes pt cut but not eta it will be discarded and the same cut will be applied to the next mor::Jet  
***************************************************************************/

template std::vector<mor::Jet>* JetSelector::get_jets(std::vector<mor::Jet> *uncut_jets, std::vector<mor::Electron> *leptons);
template bool JetSelector::cut_leptons(mor::Jet *jet_iter, std::vector<mor::Electron> *leptons);

JetSelector::JetSelector()
{
	tools = new Tools();

	cut_jets = NULL;
	min_pt = NULL;
	max_eta = -1;
	min_e_dR = -1;
	JES_scaling_factor = -1;
}

JetSelector::~JetSelector()
{
	delete tools;
	tools = NULL;
}

void JetSelector::set_min_pt(std::vector<double> *min)
{
	min_pt = min;
}

void JetSelector::set_max_eta(double max)
{
	max_eta = max;
}

void JetSelector::set_min_e_dR(double min)
{
	min_e_dR = min;
}

void JetSelector::change_JES(double scaling_factor)
{
	JES_scaling_factor = scaling_factor;
}

// Here the functions for any cuts which have been set are called
template <typename myLepton>
std::vector<mor::Jet>* JetSelector::get_jets(std::vector<mor::Jet> *uncut_jets, std::vector<myLepton> *leptons)
{
	std::vector<mor::Jet> unsorted_jets;
	cut_jets = new std::vector<mor::Jet>();

	PtSorter<mor::Jet> jet_sorter;
	std::vector<mor::Jet*> jets;

	// fill all jets to jets vector
	for(std::vector<mor::Jet>::iterator jet_iter = uncut_jets->begin();
	    jet_iter!=uncut_jets->end();
	    ++jet_iter){
	  
	  	mor::Jet *tmp_jet = new mor::Jet(*jet_iter);
		//Apply JES smearing
		if(JES_scaling_factor > 0){
			double px, py, pz, e;
			px = jet_iter->Px() * JES_scaling_factor;
			py = jet_iter->Py() * JES_scaling_factor;
			pz = jet_iter->Pz() * JES_scaling_factor;
			e = sqrt(px*px + py*py + pz*pz);
			tmp_jet->SetPxPyPzE(px, py, pz, e);
		}
		jets.push_back(tmp_jet);
	}
	
	if(min_pt != NULL && min_pt->size() >1){
	  
		double max_size = min_pt->size();
		
		for(unsigned int nasym_cut = 0; nasym_cut < max_size; ++nasym_cut){
			for(std::vector<mor::Jet*>::iterator jet_iter =
			jets.begin();
			jet_iter!=jets.end();
			){
				double cut_out=false;
				     
				if(min_pt != NULL && min_pt->size() > nasym_cut && (*min_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_pt(*jet_iter, (*min_pt)[nasym_cut]);
				if(max_eta != -1)
					cut_out = cut_out || cut_eta(*jet_iter, max_eta);
				     
				if(leptons->size() > 0 && min_e_dR != -1)
					cut_out = cut_out || cut_leptons(*jet_iter, leptons);
				    
				if(!cut_out)
				{
					unsorted_jets.push_back(*(*jet_iter));
					delete *jet_iter;
					jet_iter = jets.erase(jet_iter);
				}else{
					jet_iter++;
				}
			}
	    

			// this *is* important, don't delete it -- again
			if(unsorted_jets.size() < nasym_cut+1){ 
				for(std::vector<mor::Jet>::iterator jet_iter = unsorted_jets.begin(); 
				    jet_iter!=unsorted_jets.end(); ++jet_iter){ 
				        jet_sorter.add(&(*jet_iter)); 
				} 
				 
				std::vector<mor::Jet*> sorted_jets = jet_sorter.get_sorted(); 
				 
				for(std::vector<mor::Jet*>::iterator cut_jet = sorted_jets.begin(); 
				    cut_jet != sorted_jets.end(); 
				    ++cut_jet){ 
				        cut_jets->push_back(**cut_jet); 
				} 
				
				for(std::vector<mor::Jet*>::iterator jet = jets.begin(); 
				    jet != jets.end(); 
				    ++jet){ 
				  delete *jet; 
				} 
				return cut_jets; 
			}
		}
	}
	else{
	  
        	for(std::vector<mor::Jet*>::iterator jet_iter = jets.begin();
		    jet_iter!=jets.end();
		    ++jet_iter){
        	        double cut_out=false;
		 
			if(min_pt != NULL && min_pt->size() == 1 && (*min_pt)[0] != -1)
				cut_out = cut_out || cut_pt(*jet_iter, (*min_pt)[0]);
			if(max_eta != -1)
				cut_out = cut_out || cut_eta(*jet_iter, max_eta);
			if(leptons->size() > 0 && min_e_dR != -1)
				cut_out = cut_out || cut_leptons(*jet_iter, leptons);

        	        if(!cut_out)
        	        {
        	                unsorted_jets.push_back(**jet_iter);
        	        }
        	}
	}

        for(std::vector<mor::Jet>::iterator jet_iter = unsorted_jets.begin();
	    jet_iter!=unsorted_jets.end(); ++jet_iter){
		jet_sorter.add(&(*jet_iter));
        }

        std::vector<mor::Jet*> sorted_jets = jet_sorter.get_sorted();

	for(std::vector<mor::Jet*>::iterator cut_jet = sorted_jets.begin();
	    cut_jet != sorted_jets.end();
	    ++cut_jet){
		cut_jets->push_back(**cut_jet);
	}


	for(std::vector<mor::Jet*>::iterator jet = jets.begin();
		    jet != jets.end();
		    ++jet){
		  delete *jet;
		}

	return cut_jets; 
}

template <typename myLepton>
bool JetSelector::cut_leptons(mor::Jet *jet_iter, std::vector<myLepton> *leptons)
{
	for(typename std::vector<myLepton>::iterator lepton_iter = leptons->begin();
		lepton_iter != leptons->end();
		++lepton_iter){
		if(ROOT::Math::VectorUtil::DeltaR(*dynamic_cast<ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> >*>(jet_iter), *dynamic_cast<ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> >*>(&(*lepton_iter))) < min_e_dR)
			return true;
	}

	return false;
}

bool JetSelector::cut_pt(mor::Jet *jet_iter, double &min_pt)
{
        if(jet_iter->Pt() > min_pt)
                return false;
        else
                return true;
}

bool JetSelector::cut_eta(mor::Jet *jet_iter, double &max_eta)
{
        if(fabs(jet_iter->Eta()) < max_eta)
                return false;
        else
                return true;
}
