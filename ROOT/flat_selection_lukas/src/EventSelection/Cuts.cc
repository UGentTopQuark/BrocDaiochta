#include "../../interface/EventSelection/Cuts.h"

Cuts::Cuts(std::string ident)
{
	identifier=ident;

	// initialise cuts
	ht = -1;

	min_M3=-1;
	min_mindiffM3=-1;
	min_met=-1;
	max_met=-1;
	max_ht=-1;
	min_ht=-1;
	min_no_jets=-1;
	max_no_jets=-1;
	min_nisolated_lep=-1;
	max_nisolated_e=-1;
	max_nisolated_mu=-1;
	min_nisolated_e=-1;
	min_nisolated_mu=-1;
	min_jet_e_dR=-1;
	max_jet_eta=-1;
	e_type=-1;
	mu_type=-1;
	Z_rejection_width=-1;
	num_name_btag = -1;
	min_chi2 = -1;
	max_chi2 = -1;
	name_btag = "-1";

	cuts_passed_counter=0;
	cuts_not_passed_counter=0;

	already_cut = false;


	jets = NULL;
	all_electrons = NULL;
	isolated_electrons = NULL;
	loose_electrons = NULL;
	isolated_muons = NULL;
	loose_muons = NULL;
	corrected_mets = NULL;
	uncorrected_mets = NULL;

	min_jet_pt = NULL;

	max_mu_trackiso = NULL;
	max_mu_ecaliso = NULL;
	max_mu_caliso = NULL;
	max_mu_hcaliso = NULL;
	min_mu_relIso = NULL;
	max_mu_hcal_veto_cone = NULL;
	max_mu_ecal_veto_cone = NULL;
	min_mu_dR = NULL;
	min_mu_pt = NULL;
	min_mu_et = NULL;
	max_mu_chi2 = NULL;
	max_mu_d0 = NULL;
	max_mu_d0sig = NULL;
	min_mu_nHits = NULL;
	mu_electronID = NULL;
	max_mu_eta=NULL;
	max_e_eta=NULL;

	max_e_trackiso = NULL;
	max_e_caliso = NULL;
	max_e_ecaliso = NULL;
	max_e_hcaliso = NULL;
	max_e_hcal_veto_cone = NULL;
	max_e_ecal_veto_cone = NULL;
	min_e_relIso = NULL;
	min_e_dR = NULL;
	min_e_pt = NULL;
	min_e_et = NULL;
	max_e_chi2 = NULL;
	max_e_d0 = NULL;
	max_e_d0sig = NULL;
	min_e_nHits = NULL;
	e_electronID = NULL;

	max_loose_mu_trackiso = NULL;
	max_loose_mu_ecaliso = NULL;
	max_loose_mu_caliso = NULL;
	max_loose_mu_hcaliso = NULL;
	min_loose_mu_relIso = NULL;
	max_loose_mu_hcal_veto_cone = NULL;
	max_loose_mu_ecal_veto_cone = NULL;
	min_loose_mu_dR = NULL;
	min_loose_mu_pt = NULL;
	min_loose_mu_et = NULL;
	max_loose_mu_chi2 = NULL;
	max_loose_mu_d0 = NULL;
	max_loose_mu_d0sig = NULL;
	min_loose_mu_nHits = NULL;
	loose_mu_electronID = NULL;
	max_loose_mu_eta=NULL;
	max_loose_e_eta=NULL;

	max_loose_e_trackiso = NULL;
	max_loose_e_caliso = NULL;
	max_loose_e_ecaliso = NULL;
	max_loose_e_hcaliso = NULL;
	max_loose_e_hcal_veto_cone = NULL;
	max_loose_e_ecal_veto_cone = NULL;
	min_loose_e_relIso = NULL;
	min_loose_e_dR = NULL;
	min_loose_e_pt = NULL;
	min_loose_e_et = NULL;
	max_loose_e_chi2 = NULL;
	max_loose_e_d0 = NULL;
	max_loose_e_d0sig = NULL;
	min_loose_e_nHits = NULL;
	loose_e_electronID = NULL;

	min_btag = NULL;
	trigger = NULL;

	//double_mu_remover = NULL;
	//double_e_remover = NULL;

	bjet_finder = new BJetFinder();
	all_e_selector = new LeptonSelector<mor::Electron>();
	e_selector = new LeptonSelector<mor::Electron>();
	mu_selector = new LeptonSelector<mor::Muon>();
	loose_e_selector = new LeptonSelector<mor::Electron>();
	loose_mu_selector = new LeptonSelector<mor::Muon>();
	jet_selector = new JetSelector();
	METCor = new METCorrector();
	mass_reco = new MassReconstruction();

//	double_mu_remover = new DoubleCountedLeptonRemover<mor::Muon>();
//	double_e_remover = new DoubleCountedLeptonRemover<mor::Electron>();

	ht_calc = new HtCalculator();

	// FIXME those vectors are never deleted
	std::vector<double> *e_eta = new std::vector<double>();
	e_eta->push_back(2.4);
	all_e_selector->set_max_eta(e_eta);
	std::vector<double> *e_pt = new std::vector<double>();
	e_pt->push_back(30.0);
	all_e_selector->set_min_et(e_pt);
	std::vector<double> *e_d0 = new std::vector<double>();
	e_d0->push_back(0.02);
	all_e_selector->set_max_d0(e_d0);
}

Cuts::~Cuts()
{
	if(jet_selector){
		delete jet_selector;
		jet_selector=NULL;
	}
	if(e_selector){
		delete e_selector;
		e_selector=NULL;
	}
	if(all_e_selector){
		delete all_e_selector;
		all_e_selector=NULL;
	}
	if(mu_selector){
		delete mu_selector;
		mu_selector=NULL;
	}
	if(loose_e_selector){
		delete loose_e_selector;
		loose_e_selector=NULL;
	}
	if(loose_mu_selector){
		delete loose_mu_selector;
		loose_mu_selector=NULL;
	}

//	delete double_mu_remover;
//	double_mu_remover = NULL;
//	delete double_e_remover;
//	double_e_remover = NULL;

	if(jets != NULL){
		delete jets;
		jets = NULL;
	}
	if(isolated_electrons != NULL){
		delete isolated_electrons;
		isolated_electrons = NULL;
	}
	if(all_electrons != NULL){
		delete all_electrons;
		all_electrons = NULL;
	}
	if(isolated_muons != NULL){
		delete isolated_muons;
		isolated_muons = NULL;
	}

	if(loose_electrons != NULL){
		delete loose_electrons;
		loose_electrons = NULL;
	}
	if(loose_muons != NULL){
		delete loose_muons;
		loose_muons = NULL;
	}

	if(mass_reco){
		delete mass_reco;
		mass_reco = NULL;
	}

	if(bjet_finder){
		delete bjet_finder;
		bjet_finder = NULL;
	}

}

void Cuts::set_gen_evt(mor::TTbarGenEvent *gen_evt)
{
	this->genEvt = gen_evt;
}

void Cuts::set_handles(std::vector<mor::Muon> *muons,
			std::vector<mor::Jet> *jets,
			std::vector<mor::Electron> *electrons,
			std::vector<mor::MET> *mets,
			mor::Trigger *trigger)
{
        this->electrons = electrons;
        this->muons = muons;
        this->uncut_jets = jets;
        this->mets = mets;
	this->HLTR = trigger;
}

void Cuts::next_event()
{
	ht = -1;

	if(isolated_electrons != NULL){
		delete isolated_electrons;
		isolated_electrons = NULL;
	}
	isolated_electrons = e_selector->get_leptons(electrons, uncut_jets);

	if(all_electrons != NULL){
		delete all_electrons;
		all_electrons = NULL;
	}

	all_electrons = all_e_selector->get_leptons(electrons, uncut_jets);

	if(isolated_muons != NULL){
		delete isolated_muons;
		isolated_muons = NULL;
	}
	isolated_muons = mu_selector->get_leptons(muons, uncut_jets);

	if(loose_electrons != NULL){
		delete loose_electrons;
		loose_electrons = NULL;
	}
	loose_electrons = loose_e_selector->get_leptons(electrons, uncut_jets);

	if(loose_muons != NULL){
		delete loose_muons;
		loose_muons = NULL;
	}
	loose_muons = loose_mu_selector->get_leptons(muons, uncut_jets);

	// remove leptons that appear in loose as well as in tight lepton selection
//	double_mu_remover->set_loose_leptons(loose_muons);
//	double_mu_remover->set_tight_leptons(isolated_muons);
//	double_mu_remover->clean_loose_leptons();
//
//	double_e_remover->set_loose_leptons(loose_electrons);
//	double_e_remover->set_tight_leptons(isolated_electrons);
//	double_e_remover->clean_loose_leptons();

	if(jets){
		delete jets;
		jets = NULL;
	}

	jets = jet_selector->get_jets(uncut_jets, all_electrons);

        if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
                METCor->set_handles(mets, dynamic_cast<mor::Lepton*>(&(*isolated_muons->begin())));
                corrected_mets = METCor->get_MET();
        }
        else if(isolated_electrons->size() == 1 && isolated_muons->size() == 0){
                METCor->set_handles(mets, dynamic_cast<mor::Lepton*>(&(*isolated_electrons->begin())));
                corrected_mets = METCor->get_MET();
        }else{
                METCor->set_handles(mets);
                corrected_mets = METCor->get_uncorrected_MET();
	}

        uncorrected_mets = METCor->get_uncorrected_MET();

	mass_reco->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);
	ht_calc->set_handles(jets, isolated_electrons, isolated_muons, corrected_mets);

	already_cut = false;
	already_cut_result = false;
	
	bjet_finder->set_handles(jets);
}


bool Cuts::cut()
{
	bool cut = false;

	if(already_cut)
		return already_cut_result;

	// Call functions to apply cuts set
	if(trigger != NULL) cut = cut || cut_hltrigger();
	if(min_no_jets != -1 ||
	   max_no_jets != -1 ||
	   min_jet_pt->size() > 0 ||
	   max_jet_eta != -1)
		cut = cut || cut_njets();
	if((max_e_trackiso != NULL && max_e_trackiso->size() > 0) ||
	   (max_e_caliso != NULL && max_e_caliso->size() > 0) ||
	   (max_e_ecaliso != NULL && max_e_ecaliso->size() > 0) ||
	   (max_e_hcaliso != NULL && max_e_hcaliso->size() > 0) ||
	   (max_e_hcal_veto_cone != NULL && max_e_hcal_veto_cone->size() > 0) ||
	   (max_e_ecal_veto_cone != NULL && max_e_ecal_veto_cone->size() > 0) ||
	   (min_e_relIso != NULL && min_e_relIso->size() > 0) ||
	   (min_e_dR != NULL && min_e_dR->size() > 0) ||
	   max_nisolated_e != -1 ||
	   min_nisolated_e != -1 ||
	   max_e_eta != NULL ||
	   e_type != -1 ||
	   (max_e_chi2 != NULL && max_e_chi2->size() > 0) ||
	   (max_e_d0 != NULL && max_e_d0->size() > 0) ||
	   (max_e_d0sig != NULL && max_e_d0sig->size() > 0) ||
	   (min_e_nHits != NULL && min_e_nHits->size() > 0) ||
	   (e_electronID != NULL && e_electronID->size() > 0) ||
	   (min_e_et != NULL && min_e_et->size() > 0) ||
	   (min_e_pt != NULL && min_e_pt->size() > 0))
		cut = cut || cut_nisolated_electrons();
	if((max_loose_e_trackiso != NULL && max_loose_e_trackiso->size() > 0) ||
	   (max_loose_e_caliso != NULL && max_loose_e_caliso->size() > 0) ||
	   (max_loose_e_ecaliso != NULL && max_loose_e_ecaliso->size() > 0) ||
	   (max_loose_e_hcaliso != NULL && max_loose_e_hcaliso->size() > 0) ||
	   (max_loose_e_hcal_veto_cone != NULL && max_loose_e_hcal_veto_cone->size() > 0) ||
	   (max_loose_e_ecal_veto_cone != NULL && max_loose_e_ecal_veto_cone->size() > 0) ||
	   (min_loose_e_relIso != NULL && min_loose_e_relIso->size() > 0) ||
	   (min_loose_e_dR != NULL && min_loose_e_dR->size() > 0) ||
	   max_nloose_e != -1 ||
	   max_loose_e_eta != NULL ||
	   loose_e_type != -1 ||
	   (max_loose_e_chi2 != NULL && max_loose_e_chi2->size() > 0) ||
	   (max_loose_e_d0 != NULL && max_loose_e_d0->size() > 0) ||
	   (max_loose_e_d0sig != NULL && max_loose_e_d0sig->size() > 0) ||
	   (min_loose_e_nHits != NULL && min_loose_e_nHits->size() > 0) ||
	   (loose_e_electronID != NULL && loose_e_electronID->size() > 0) ||
	   (min_loose_e_et != NULL && min_loose_e_et->size() > 0) ||
	   (min_loose_e_pt != NULL && min_loose_e_pt->size() > 0))
		cut = cut || cut_nloose_electrons();
	if((max_loose_mu_trackiso != NULL && max_loose_mu_trackiso->size() > 0) ||
	   (max_loose_mu_caliso != NULL && max_loose_mu_caliso->size() > 0) ||
	   (max_loose_mu_ecaliso != NULL && max_loose_mu_ecaliso->size() > 0) ||
	   (max_loose_mu_hcaliso != NULL && max_loose_mu_hcaliso->size() > 0) ||
	   (max_loose_mu_hcal_veto_cone != NULL && max_loose_mu_hcal_veto_cone->size() > 0) ||
	   (max_loose_mu_ecal_veto_cone != NULL && max_loose_mu_ecal_veto_cone->size() > 0) ||
	   (min_loose_mu_relIso != NULL && min_loose_mu_relIso->size() > 0) ||
	   (min_loose_mu_dR != NULL && min_loose_mu_dR->size() > 0) ||
	   max_nloose_mu != -1 ||
	   max_loose_mu_eta != NULL ||
	   loose_mu_type != -1 ||
	   (max_loose_mu_chi2 != NULL && max_loose_mu_chi2->size() > 0) ||
	   (max_loose_mu_d0 != NULL && max_loose_mu_d0->size() > 0) ||
	   (max_loose_mu_d0sig != NULL && max_loose_mu_d0sig->size() > 0) ||
	   (min_loose_mu_nHits != NULL && min_loose_mu_nHits->size() > 0) ||
	   (loose_mu_electronID != NULL && loose_mu_electronID->size() > 0) ||
	   (min_loose_mu_et != NULL && min_loose_mu_et->size() > 0) ||
	   (min_loose_mu_pt != NULL && min_loose_mu_pt->size() > 0))
		cut = cut || cut_nloose_muons();
	if((max_e_trackiso != NULL && max_mu_trackiso->size() > 0) ||
	   (max_mu_caliso != NULL && max_mu_caliso->size() > 0) ||
	   (max_mu_ecaliso != NULL && max_mu_ecaliso->size() > 0) ||
	   (max_mu_hcaliso != NULL && max_mu_hcaliso->size() > 0) ||
	   (max_mu_hcal_veto_cone != NULL && max_mu_hcal_veto_cone->size() > 0) ||
	   (max_mu_ecal_veto_cone != NULL && max_mu_ecal_veto_cone->size() > 0) ||
	   (min_mu_relIso != NULL && min_mu_relIso->size() > 0) ||
	   (min_mu_dR != NULL && min_mu_dR->size() > 0) ||
           max_nisolated_mu != -1 ||
           min_nisolated_mu != -1 ||
	   max_mu_eta != NULL ||
	   mu_type != -1 ||
	   (max_mu_chi2 != NULL && max_mu_chi2->size() > 0) ||
	   (max_mu_d0 != NULL && max_mu_d0->size() > 0) ||
	   (max_mu_d0sig != NULL && max_mu_d0sig->size() > 0) ||
	   (min_mu_nHits != NULL && min_mu_nHits->size() > 0) ||
	   (mu_electronID != NULL && mu_electronID->size() > 0) ||
	   (min_mu_et != NULL && min_mu_et->size() > 0) ||
	   (min_mu_pt != NULL && min_mu_pt->size() > 0) )
		cut = cut || cut_nisolated_muons();
	if(min_nisolated_lep != -1) cut = cut || cut_nisolated_leptons();
	if(min_met != -1 || max_met != -1) cut = cut || cut_met();
//	if(min_M3 != -1) cut = cut || cut_M3();
//	if(min_mindiffM3 != -1) cut = cut || cut_mindiffM3();
	if(max_ht != -1) cut = cut || cut_max_ht();
	if(min_ht != -1) cut = cut || cut_min_ht();
	if(Z_rejection_width != -1) cut = cut || cut_Zrejection();
	if(min_btag != NULL && name_btag != "-1") cut = cut || cut_btag();
//	if(min_chi2 != -1 || max_chi2 != -1) cut = cut || cut_chi2();

	/*
	 * INFO: gettriggerInfo() removed in version 1044
	 */

	// count events that passed cuts and those that were discarded
	if(cut)
		cuts_not_passed_counter++;
	else
		cuts_passed_counter++;

	already_cut = true;
	already_cut_result = cut;
	// Returns false if event passed cuts, true if event should be discarded
	return cut;
}

// Apply cut for min pt and number of jets
bool Cuts::cut_njets()
{
	int njets=jets->size();
	if(njets < min_no_jets || (njets > max_no_jets && max_no_jets >= 0)){
		if(min_jet_pt->size() == 1 && (*min_jet_pt)[0] <= 0)
			return false;
		else
			return true;
	}
	else
		return false;
}

bool Cuts::cut_mindiffM3()
{
	if(mass_reco->calculate_min_diff_M3() > min_mindiffM3)
		return false;
	else
		return true;
}

bool Cuts::cut_M3()
{
	if(mass_reco->calculate_M3() > min_M3)
		return false;
	else
		return true;
}

bool Cuts::cut_met()
{

	if(mets->size() < 1)
		return true;

        std::vector<mor::MET>::const_iterator met_iter = mets->begin();

        if((min_met == -1 || met_iter->Et() > min_met) && (met_iter->Et() < max_met || max_met == -1))
                return false;
        else
                return true;
}

bool Cuts::cut_chi2()
{
	if((min_chi2 != -1 && mass_reco->get_chi2() < min_chi2) || (max_chi2 != -1 && mass_reco->get_chi2() > max_chi2))
		return true;
	else
		return false;
}

// Cut on total transverse energy
bool Cuts::cut_min_ht()
{
	if(ht == -1)
		ht_calc->get_ht();

	// Sum pt of all jets + one lepton

	if(ht < min_ht)
		return true;
	else
		return false;
}

// Cut on total energy (at the moment only max_ht cut)
bool Cuts::cut_max_ht()
{
	if(ht == -1)
		ht_calc->get_ht();

	if(ht > max_ht)
		return true;
	else
		return false;
}

bool Cuts::cut_Zrejection()
{
	double Z_mass = 91.2;

	if(isolated_electrons->size() == 1 && loose_electrons->size() >= 1){
		for(std::vector<mor::Electron>::iterator electron1 =
			isolated_electrons->begin();
		    electron1 != isolated_electrons->end();
		    ++electron1)
		{
			for(std::vector<mor::Electron>::iterator electron2 =
				loose_electrons->begin();
			    electron2 != loose_electrons->end();
			    ++electron2)
			{
				// don't use the same lepton twice
				if(electron1->Pt() == electron2->Pt() &&
					electron1->Eta() == electron2->Eta())
					continue;

				if((electron1->charge() != electron2->charge()) && (((electron1->p4() + electron2->p4()).mass() - Z_mass) < Z_rejection_width))
					return true;
			}
		}
	}

	if(isolated_muons->size() == 1 && loose_muons->size() >= 1){
		for(std::vector<mor::Muon>::iterator muon1 = isolated_muons->begin();
		    muon1 != isolated_muons->end();
		    ++muon1)
		{
			for(std::vector<mor::Muon>::iterator muon2 =
				loose_muons->begin();
			    muon2 != loose_muons->end();
			    ++muon2)
			{
				// don't use the same lepton twice
				if(muon1->Pt() == muon2->Pt() &&
					muon1->Eta() == muon2->Eta())
					continue;

				if((muon1->charge() != muon2->charge()) && (((muon1->p4() + muon2->p4()).mass() - Z_mass) < Z_rejection_width))
					return true;
			}
		}
	}

	return false;
}

bool Cuts::cut_nisolated_leptons()
{
	int niso_lep = isolated_electrons->size() + isolated_muons->size();
		
	if((niso_lep >= min_nisolated_lep) || (min_nisolated_lep == -1))
		return false;
	else
		return true;   		
}

// Set max and min number of each lepton allowed
bool Cuts::cut_nisolated_electrons()
{
	int niso_e = isolated_electrons->size();

	if(niso_e == 0 && max_nisolated_e == 0)
		return false;
	
	if((niso_e >= min_nisolated_e && niso_e <= max_nisolated_e ) || ((niso_e >= min_nisolated_e) && (max_nisolated_e == -1)) || ((min_nisolated_e == -1) && (niso_e <= max_nisolated_e)) ||  ((min_nisolated_e == -1) && (max_nisolated_e == -1)))
		return false;
	else
		return true;		
}

bool Cuts::cut_nloose_electrons()
{
	int nloose_e = loose_electrons->size();

	if(nloose_e == 0 && max_nloose_e == 0)
		return false;
	
	if((nloose_e <= max_nloose_e ) || (max_nloose_e == -1))
		return false;
	else
		return true;		
}

bool Cuts::cut_nloose_muons()
{
	int nloose_mu = loose_muons->size();

	if(nloose_mu == 0 && max_nloose_mu == 0)
		return false;
	
	if((nloose_mu <= max_nloose_mu ) || (max_nloose_mu == -1))
		return false;
	else
		return true;		
}

bool Cuts::cut_nisolated_muons()
{
	int niso_mu = isolated_muons->size();

	if(niso_mu == 0 && max_nisolated_mu == 0)
		return false;

	if(((niso_mu >= min_nisolated_mu) && (niso_mu <= max_nisolated_mu)) || ((niso_mu >= min_nisolated_mu) && (max_nisolated_mu == -1)) || ((min_nisolated_mu == -1) && (niso_mu <= max_nisolated_mu)) ||  ((min_nisolated_mu == -1) && (max_nisolated_mu == -1))) 
	  {
		return false;		
	}
	else
	  {
		return true;
	  }
}

//Calculate efficiency and purity for differenet btag cuts
bool Cuts::cut_btag()
{

	//get back vector of jet(id,btag) in order of decreasing btag
	bjet_finder->set_min_btag_value(*min_btag);		
	std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets(name_btag);

/*
	std::cout << "Min btag cut_btag(): " << (*min_btag)[0] << std::endl;
	std::cout << "btag_ids_this_algo->size(): " << btag_ids_this_algo->size() << std::endl;
	std::cout << "min_btag->size(): " << min_btag->size() << std::endl;

	if(btag_ids_this_algo->size() > 0) std::cout << "max_bjet: " << (*btag_ids_this_algo)[0].second << std::endl;
*/
	
	if(btag_ids_this_algo->size() >= min_btag->size())
		return false;
	else
		return true;
 
}

//Apply trigger cuts
bool Cuts::cut_hltrigger()
{
	// FIXME: this needs to be cleand up
	std::map<int, std::string> trigger_dict;
	trigger_dict[80]="HLT_Mu9";
	trigger_dict[83]="HLT_Mu11";
	trigger_dict[85]="HLT_Mu15";
	trigger_dict[50]="HLT_Ele15_LW_L1R";

	for(std::vector<double>::iterator trig_iter = trigger->begin(); trig_iter!=trigger->end(); ++ trig_iter){ 
		if(!HLTR->triggered(trigger_dict[(int) *trig_iter])) {
					//HLT_QuadJet30 = 24, //HLT_Mu11 = 83,//HLT_Mu15 = 85,  //HLT_Ele_15_LW_L1R = 50
		return true;
		}
	}

	return false;
}

void Cuts::set_all_vcuts(std::map<std::string,std::vector<double>*> &cuts_set)
{
	already_cut = false;

	min_mu_pt = cuts_set["min_mu_pt"];
	min_mu_et = cuts_set["min_mu_et"];
	max_mu_eta = cuts_set["max_mu_eta"];
	min_mu_nHits = cuts_set["min_mu_nHits"];
	max_mu_chi2 = cuts_set["max_mu_chi2"];
	max_mu_d0 = cuts_set["max_mu_d0"];
	max_mu_d0sig = cuts_set["max_mu_d0sig"];
	max_mu_trackiso = cuts_set["max_mu_trackiso"];
	max_mu_ecaliso = cuts_set["max_mu_ecaliso"];
	max_mu_caliso = cuts_set["max_mu_caliso"];
	max_mu_hcaliso = cuts_set["max_mu_hcaliso"];
	max_mu_hcal_veto_cone = cuts_set["max_mu_hcal_veto_cone"];
	max_mu_ecal_veto_cone = cuts_set["max_mu_ecal_veto_cone"];
	min_mu_dR = cuts_set["min_mu_dR"];
	min_mu_relIso = cuts_set["min_mu_relIso"];
	mu_electronID = cuts_set["mu_electronID"];

	min_loose_mu_pt = cuts_set["min_loose_mu_pt"];
	min_loose_mu_et = cuts_set["min_loose_mu_et"];
	max_loose_mu_eta = cuts_set["max_loose_mu_eta"];
	min_loose_mu_nHits = cuts_set["min_loose_mu_nHits"];
	max_loose_mu_chi2 = cuts_set["max_loose_mu_chi2"];
	max_loose_mu_d0 = cuts_set["max_loose_mu_d0"];
	max_loose_mu_d0sig = cuts_set["max_loose_mu_d0sig"];
	max_loose_mu_trackiso= cuts_set["max_loose_mu_trackiso"];
	max_loose_mu_ecaliso = cuts_set["max_loose_mu_ecaliso"];
	max_loose_mu_caliso = cuts_set["max_loose_mu_caliso"];
	max_loose_mu_hcaliso = cuts_set["max_loose_mu_hcaliso"];
	max_loose_mu_hcal_veto_cone = cuts_set["max_loose_mu_hcal_veto_cone"];
	max_loose_mu_ecal_veto_cone = cuts_set["max_loose_mu_ecal_veto_cone"];
	min_loose_mu_dR = cuts_set["min_loose_mu_dR"];
	min_loose_mu_relIso = cuts_set["min_loose_mu_relIso"];
	loose_mu_electronID = cuts_set["loose_mu_electronID"];

	min_e_pt = cuts_set["min_e_pt"];
	min_e_et = cuts_set["min_e_et"];
	min_e_nHits = cuts_set["min_e_nHit"];
	max_e_chi2 = cuts_set["max_e_chi2"];
	max_e_d0 = cuts_set["max_e_d0"];
	max_e_d0sig = cuts_set["max_e_d0sig"];
	e_electronID = cuts_set["e_electronID"];
	max_e_eta = cuts_set["max_e_eta"];
	max_e_trackiso = cuts_set["max_e_trackiso"];
	max_e_ecaliso = cuts_set["max_e_ecaliso"];
	max_e_caliso = cuts_set["max_e_caliso"];
	max_e_hcal_veto_cone = cuts_set["max_e_hcal_veto_cone"];
	max_e_ecal_veto_cone = cuts_set["max_e_ecal_veto_cone"];
	max_e_hcaliso = cuts_set["max_e_hcaliso"];
	min_e_relIso = cuts_set["min_e_relIso"];
	min_e_dR = cuts_set["min_e_dR"];

	min_loose_e_pt = cuts_set["min_loose_e_pt"];
	min_loose_e_et = cuts_set["min_loose_e_et"];
	min_loose_e_nHits = cuts_set["min_loose_e_nHits"];
	max_loose_e_chi2 = cuts_set["max_loose_e_chi2"];
	max_loose_e_d0 = cuts_set["max_loose_e_d0"];
	max_loose_e_d0sig = cuts_set["max_loose_e_d0sig"];
	loose_e_electronID = cuts_set["loose_e_electronID"];
	max_loose_e_eta = cuts_set["max_loose_e_eta"];
	max_loose_e_trackiso = cuts_set["max_loose_e_trackiso"];
	max_loose_e_ecaliso = cuts_set["max_loose_e_ecaliso"];
	max_loose_e_caliso = cuts_set["max_loose_e_caliso"];
	max_loose_e_hcal_veto_cone = cuts_set["max_loose_e_hcal_veto_cone"];
	max_loose_e_ecal_veto_cone = cuts_set["max_loose_e_ecal_veto_cone"];
	max_loose_e_hcaliso = cuts_set["max_loose_e_hcaliso"];
	min_loose_e_relIso = cuts_set["min_loose_e_relIso"];
	min_loose_e_dR = cuts_set["min_loose_e_dR"];

	min_jet_pt = cuts_set["min_jet_pt"];
	trigger = cuts_set["trigger"];

	mu_selector->set_min_pt(cuts_set["min_mu_pt"]);
	mu_selector->set_min_et(cuts_set["min_mu_et"]);
	mu_selector->set_max_eta(cuts_set["max_mu_eta"]);
	mu_selector->set_min_nHits(cuts_set["min_mu_nHits"]);
	mu_selector->set_max_chi2(cuts_set["max_mu_chi2"]);
	mu_selector->set_max_d0(cuts_set["max_mu_d0"]);
	mu_selector->set_max_d0sig(cuts_set["max_mu_d0sig"]);
	mu_selector->set_electronID(cuts_set["mu_electronID"]);
	mu_selector->set_max_trackiso(cuts_set["max_mu_trackiso"]);
	mu_selector->set_max_ecaliso(cuts_set["max_mu_ecaliso"]);
	mu_selector->set_max_caliso(cuts_set["max_mu_caliso"]);
	mu_selector->set_max_hcaliso(cuts_set["max_mu_hcaliso"]);
	mu_selector->set_max_hcal_veto_cone(cuts_set["max_mu_hcal_veto_cone"]);
	mu_selector->set_max_ecal_veto_cone(cuts_set["max_mu_ecal_veto_cone"]);
	mu_selector->set_min_dR(cuts_set["min_mu_dR"]);
	mu_selector->set_min_relIso(cuts_set["min_mu_relIso"]);


	loose_mu_selector->set_min_pt(cuts_set["min_loose_mu_pt"]);
	loose_mu_selector->set_min_et(cuts_set["min_loose_mu_et"]);
	loose_mu_selector->set_max_eta(cuts_set["max_loose_mu_eta"]);
	loose_mu_selector->set_min_nHits(cuts_set["min_loose_mu_nHits"]);
	loose_mu_selector->set_max_chi2(cuts_set["max_loose_mu_chi2"]);
	loose_mu_selector->set_max_d0(cuts_set["max_loose_mu_d0"]);
	loose_mu_selector->set_max_d0sig(cuts_set["max_loose_mu_d0sig"]);
	loose_mu_selector->set_electronID(cuts_set["loose_mu_electronID"]);
	loose_mu_selector->set_max_trackiso(cuts_set["max_loose_mu_trackiso"]);
	loose_mu_selector->set_max_ecaliso(cuts_set["max_loose_mu_ecaliso"]);
	loose_mu_selector->set_max_caliso(cuts_set["max_loose_mu_caliso"]);
	loose_mu_selector->set_max_hcaliso(cuts_set["max_loose_mu_hcaliso"]);
	loose_mu_selector->set_max_hcal_veto_cone(cuts_set["max_loose_mu_hcal_veto_cone"]);
	loose_mu_selector->set_max_ecal_veto_cone(cuts_set["max_loose_mu_ecal_veto_cone"]);
	loose_mu_selector->set_min_dR(cuts_set["min_loose_mu_dR"]);
	loose_mu_selector->set_min_relIso(cuts_set["min_loose_mu_relIso"]);

	e_selector->set_min_pt(cuts_set["min_e_pt"]);
	e_selector->set_min_et(cuts_set["min_e_et"]);
	e_selector->set_min_nHits(cuts_set["min_e_nHits"]);
	e_selector->set_max_chi2(cuts_set["max_e_chi2"]);
	e_selector->set_max_d0(cuts_set["max_e_d0"]);
	e_selector->set_max_d0sig(cuts_set["max_e_d0sig"]);
	e_selector->set_electronID(cuts_set["e_electronID"]);
	e_selector->set_max_eta(cuts_set["max_e_eta"]);
	e_selector->set_max_trackiso(cuts_set["max_e_trackiso"]);
	e_selector->set_max_ecaliso(cuts_set["max_e_ecaliso"]);
	e_selector->set_max_caliso(cuts_set["max_e_caliso"]);
	e_selector->set_max_hcal_veto_cone(cuts_set["max_e_hcal_veto_cone"]);
	e_selector->set_max_ecal_veto_cone(cuts_set["max_e_ecal_veto_cone"]);
	e_selector->set_max_hcaliso(cuts_set["max_e_hcaliso"]);
	e_selector->set_min_relIso(cuts_set["min_e_relIso"]);
	e_selector->set_min_dR(cuts_set["min_e_dR"]);

	loose_e_selector->set_min_pt(cuts_set["min_loose_e_pt"]);
	loose_e_selector->set_min_et(cuts_set["min_loose_e_et"]);
	loose_e_selector->set_min_nHits(cuts_set["min_loose_e_nHits"]);
	loose_e_selector->set_max_chi2(cuts_set["max_loose_e_chi2"]);
	loose_e_selector->set_max_d0(cuts_set["max_loose_e_d0"]);
	loose_e_selector->set_max_d0sig(cuts_set["max_loose_e_d0sig"]);
	loose_e_selector->set_electronID(cuts_set["loose_e_electronID"]);
	loose_e_selector->set_max_eta(cuts_set["max_loose_e_eta"]);
	loose_e_selector->set_max_trackiso(cuts_set["max_loose_e_trackiso"]);
	loose_e_selector->set_max_ecaliso(cuts_set["max_loose_e_ecaliso"]);
	loose_e_selector->set_max_caliso(cuts_set["max_loose_e_caliso"]);
	loose_e_selector->set_max_hcal_veto_cone(cuts_set["max_loose_e_hcal_veto_cone"]);
	loose_e_selector->set_max_ecal_veto_cone(cuts_set["max_loose_e_ecal_veto_cone"]);
	loose_e_selector->set_max_hcaliso(cuts_set["max_loose_e_hcaliso"]);
	loose_e_selector->set_min_relIso(cuts_set["min_loose_e_relIso"]);
	loose_e_selector->set_min_dR(cuts_set["min_loose_e_dR"]);

	min_btag = cuts_set["min_btag"];

	jet_selector->set_min_pt(cuts_set["min_jet_pt"]);
}

void Cuts::set_all_cuts(std::map<std::string,double> &cuts_set)
{
	already_cut = false;

	mu_type = (int) cuts_set["mu_type"];

	loose_mu_type = (int) cuts_set["loose_mu_type"];

	e_type = (int) cuts_set["e_type"];
	loose_e_type = (int) cuts_set["loose_e_type"];

	min_no_jets = cuts_set["min_no_jets"];
	max_no_jets = cuts_set["max_no_jets"];
	JES_factor = cuts_set["JES_factor"];
	max_jet_eta = cuts_set["max_jet_eta"];
	min_jet_e_dR = cuts_set["min_jet_e_dR"];

	min_nisolated_lep = cuts_set["min_nisolated_lep"];
	min_nisolated_e = cuts_set["min_nisolated_e"];
	min_nisolated_mu = cuts_set["min_nisolated_mu"];
	max_nisolated_e = cuts_set["max_nisolated_e"];
	max_nisolated_mu = cuts_set["max_nisolated_mu"];
	max_nloose_mu = cuts_set["max_nloose_mu"];
	max_nloose_e = cuts_set["max_nloose_e"];

	min_met = cuts_set["min_met"];
	max_met = cuts_set["max_met"];
	min_M3 = cuts_set["min_M3"];
	min_mindiffM3 = cuts_set["min_mindiffM3"];
	min_ht = cuts_set["min_ht"];
	max_ht = cuts_set["max_ht"];
	Z_rejection_width = cuts_set["Z_rejection_width"];
	min_chi2 = cuts_set["min_chi2"];
	
	num_name_btag = cuts_set["name_btag"];
	if (num_name_btag == 1) name_btag = "jetBProbabilityBJetTags";
	else if (num_name_btag == 2) name_btag = "jetProbabilityBJetTags";
	else if (num_name_btag == 3) name_btag = "trackCountingHighPurBJetTags";
	else if (num_name_btag == 4) name_btag = "trackCountingHighEffBJetTags";
	else name_btag = "-1";

	mu_selector->set_lepton_type((int) cuts_set["mu_type"]);

	loose_mu_selector->set_lepton_type((int) cuts_set["loose_mu_type"]);

	e_selector->set_lepton_type((int) cuts_set["e_type"]);
	loose_e_selector->set_lepton_type((int) cuts_set["loose_e_type"]);

	jet_selector->change_JES(cuts_set["JES_factor"]);
	jet_selector->set_max_eta(cuts_set["max_jet_eta"]);
	jet_selector->set_min_e_dR(cuts_set["min_jet_e_dR"]);
}


/*
 *	get cut information
 */

std::vector<double>* Cuts::get_min_btag()
{
	return min_btag;
}

double Cuts::get_name_btag()
{
	return num_name_btag;
}

MassReconstruction* Cuts::get_mass_reco()
{
	return mass_reco;
}

HtCalculator* Cuts::get_ht_calc()
{
	return ht_calc;
}

/*
 *	Print all cuts for Cuts object
 */

void Cuts::print_cuts()
{
	// FIXME: store cuts_set from CutSelector in Cuts and loop over all cuts
	// to print them...
	// FIXME: loose lepton cuts should also be printed
	std::cout << "=-----------IMPOSED-CUTS-----------=" << std::endl;
	std::cout << "identifier: " << identifier << std::endl;
	std::cout << "min_no_jets: " << min_no_jets << std::endl;
	print_cuts_vector("min_e_pt", min_e_pt);
	print_cuts_vector("min_mu_pt", min_mu_pt);
	print_cuts_vector("min_e_et", min_e_et);
	print_cuts_vector("min_mu_et", min_mu_et);
	std::cout << "min_met: " << min_met << std::endl;
	std::cout << "max_met: " << max_met << std::endl;
	std::cout << "max_ht: " << max_ht << std::endl;
	std::cout << "min_ht: " << min_ht << std::endl;
	print_cuts_vector("max_mu_eta", max_mu_eta);
	print_cuts_vector("max_e_eta", max_e_eta);
	std::cout << "mu_type: " << mu_type << std::endl;
	std::cout << "e_type: " << e_type << std::endl;
	std::cout << "max_jet_eta: " << max_jet_eta << std::endl;
	std::cout << "min_jet_e_dR: " << min_jet_e_dR << std::endl;
	print_cuts_vector("max_mu_trackiso", max_mu_trackiso);
	print_cuts_vector("max_mu_ecaliso", max_mu_ecaliso);
	print_cuts_vector("max_mu_caliso", max_mu_caliso);
	print_cuts_vector("max_mu_hcaliso", max_mu_hcaliso);
	print_cuts_vector("max_mu_hcal_veto_cone", max_mu_hcal_veto_cone);
	print_cuts_vector("max_mu_ecal_veto_cone", max_mu_ecal_veto_cone);
	print_cuts_vector("min_mu_relIso", min_mu_relIso);
	print_cuts_vector("min_mu_dR", min_mu_dR);
	print_cuts_vector("max_mu_d0", max_mu_d0);
	print_cuts_vector("max_mu_d0sig", max_mu_d0sig);
	print_cuts_vector("max_mu_chi2", max_mu_chi2);
	print_cuts_vector("min_mu_nHits", min_mu_nHits);
	print_cuts_vector("mu_electronID", mu_electronID);
	print_cuts_vector("max_e_trackiso", max_e_trackiso);
	print_cuts_vector("max_e_ecaliso", max_e_ecaliso);
	print_cuts_vector("max_e_caliso", max_e_caliso);
	print_cuts_vector("max_e_hcal_veto_cone", max_e_hcal_veto_cone);
	print_cuts_vector("max_e_ecal_veto_cone", max_e_ecal_veto_cone);
	print_cuts_vector("max_e_hcaliso", max_e_hcaliso);
	print_cuts_vector("min_e_relIso", min_e_relIso);
	print_cuts_vector("min_e_dR", min_e_dR);
	print_cuts_vector("max_e_d0", max_e_d0);
	print_cuts_vector("max_e_d0sig", max_e_d0sig);
	print_cuts_vector("max_e_chi2", max_e_chi2);
	print_cuts_vector("min_e_nHits", min_e_nHits);
	print_cuts_vector("e_electronID", e_electronID);
	std::cout << "max_nloose_e: " << max_nloose_e << std::endl;
	std::cout << "max_nloose_mu: " << max_nloose_mu << std::endl;
	std::cout << "min_nisolated_lep: " << min_nisolated_lep << std::endl;
	std::cout << "max_nisolated_e: " << max_nisolated_e << std::endl;
	std::cout << "max_nisolated_mu: " << max_nisolated_mu << std::endl;
	std::cout << "min_nisolated_e: " << min_nisolated_e << std::endl;
	std::cout << "min_nisolated_mu: " << min_nisolated_mu << std::endl;
	std::cout << "min_M3: " << min_M3 << std::endl;
	std::cout << "min_mindiffM3: " << min_mindiffM3 << std::endl;
	std::cout << "min_chi2: " << min_chi2 << std::endl;
	print_cuts_vector("trigger", trigger);	
	std::cout << "name_btag: " << name_btag << std::endl;
	print_cuts_vector("min_btag", min_btag);
	print_cuts_vector("min_jet_pt", min_jet_pt);
	std::cout << "cuts_passed: " << cuts_passed_counter << std::endl;
	std::cout << "cuts_not_passed: " << cuts_not_passed_counter << std::endl;
	std::cout << "=----------------------------------=" << std::endl;
}

void Cuts::print_cuts_vector(std::string id, std::vector<double> *cuts)
{
        std::cout << id << ": ";

        if(cuts == NULL || cuts->size() == 0)
                std::cout << "-1" << std::endl;
        else{
                for(std::vector<double>::iterator cut = cuts->begin();
                    cut != cuts->end();
                    ++cut)
                        if(cut != cuts->begin())
                                std::cout << ":" << *cut;
                        else
                                std::cout << *cut;
                std::cout << std::endl;
        }
}
