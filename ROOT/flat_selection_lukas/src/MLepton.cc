#include "../interface/MLepton.h"

bool mor::Lepton::triggered(std::string trigger_name)
{
	if(trigger.find(trigger_name) != trigger.end()){
		return trigger[trigger_name];
	}else{
		std::cerr << "WARNING: mor::Lepton::triggered(): trying to read out trigger information that was not in ntuple: " << trigger_name << std::endl;
		return false;
	}
}

void mor::Lepton::set_beag_info(beag::Lepton &beag_lepton)
{
	ecal_iso = beag_lepton.ecal_iso;
	hcal_iso = beag_lepton.hcal_iso;
	track_iso = beag_lepton.track_iso;

	d0_val = beag_lepton.d0;
	d0_sigma_val = beag_lepton.d0_sigma;

	hcal_vcone_val = beag_lepton.hcal_vcone;
	ecal_vcone_val = beag_lepton.ecal_vcone;

	charge_val = beag_lepton.charge;
	lepton_id = beag_lepton.lepton_id;
	track_available_val = beag_lepton.track_available;

	trigger = beag_lepton.trigger;

	SetPx(beag_lepton.px);
	SetPy(beag_lepton.py);
	SetPz(beag_lepton.pz);
	SetE(beag_lepton.e);

	mc_matched_val = beag_lepton.mc_matched;

	if(mc_matched_val){
		mc_match_p4.SetPx(beag_lepton.mc_px);
		mc_match_p4.SetPy(beag_lepton.mc_py);
		mc_match_p4.SetPz(beag_lepton.mc_pz);
		mc_match_p4.SetE(beag_lepton.mc_e);
		mc_id = beag_lepton.mc_id;
	}

	from_ttbar_decay = beag_lepton.from_ttbar_decay;
}
