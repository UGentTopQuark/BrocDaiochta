#include "../interface/Analyser.h"

template void Analyser::prepare_mor_collections<beag::Jet, mor::Jet>(std::vector<beag::Jet> *beag_objs, std::vector<mor::Jet> *mor_objs);
template void Analyser::prepare_mor_collections<beag::Muon, mor::Muon>(std::vector<beag::Muon> *beag_objs, std::vector<mor::Muon> *mor_objs);
template void Analyser::prepare_mor_collections<beag::Electron, mor::Electron>(std::vector<beag::Electron> *beag_objs, std::vector<mor::Electron> *mor_objs);
template void Analyser::prepare_mor_collections<beag::MET, mor::MET>(std::vector<beag::MET> *beag_objs, std::vector<mor::MET> *mor_objs);

Analyser::Analyser(std::string dataset_name)
{
	process_mc = false;	// assume by default that no MC information is available

        evt_runner = new EventRunner();
	beag_jets = new std::vector<beag::Jet>();
	beag_electrons = new std::vector<beag::Electron>();
	beag_muons = new std::vector<beag::Muon>();
	beag_mets = new std::vector<beag::MET>();
	beag_triggers = new std::vector<beag::Trigger>();

	jets = new std::vector<mor::Jet>();
	electrons = new std::vector<mor::Electron>();
	muons = new std::vector<mor::Muon>();
	mets = new std::vector<mor::MET>();

	beag_gen_evts = NULL;
	beag_triggers = NULL;
	gen_evt = NULL;

	trigger = new mor::Trigger();

	identifier = dataset_name;

	cut_selector = new CutSelector(identifier);
	cut_selector->set_handles(muons, jets, electrons, mets, trigger);
     	cut_selector->set_gen_evt(NULL); 

	histo_writer = new HistoWriter();
        
        event_number = 0;
        max_events = -1;
}

Analyser::~Analyser()
{
	outfile->cd();

	if(cut_selector){
		delete cut_selector;
		cut_selector = NULL;
	}

	if(histo_writer){
		delete histo_writer;
		histo_writer = NULL;
	}
}

void Analyser::set_file_names(std::vector<std::string> *file_names)
{
        evt_runner->set_file_names(file_names);
}

void Analyser::set_outfile(TFile *outfile)
{
        this->outfile = outfile;
	histo_writer->set_outfile(outfile);
	cut_selector->set_histo_writer(histo_writer);
}

void Analyser::process_monte_carlo(bool process_mc)
{
	this->process_mc = process_mc;

	if(process_mc) beag_gen_evts = new std::vector<beag::TTbarGenEvent>();
	if(process_mc) gen_evt = new mor::TTbarGenEvent();
	if(process_mc)
		cut_selector->set_gen_evt(gen_evt);
	else
      		cut_selector->set_gen_evt(NULL); 
}

void Analyser::set_max_events(int max_events)
{
	this->max_events = max_events;
}

void Analyser::analyse()
{
	while(evt_runner->has_next() && (event_number < max_events || max_events == -1)){
                change_event();
		// analysis code here
	
		cut_selector->plot();
	}
}

void Analyser::change_event()
{
        event_number = evt_runner->current_event();
        if(event_number % 5000 == 0)
                std::cout << identifier << " processing event: " << event_number << std::endl;
        if(evt_runner->end_of_file()){
                std::cout << identifier << " assigning collections..." << std::endl;
                evt_runner->assign_collection<beag::Jet>(beag_jets, "jets");
                evt_runner->assign_collection<beag::Electron>(beag_electrons, "electrons");
                evt_runner->assign_collection<beag::Muon>(beag_muons, "muons");
                evt_runner->assign_collection<beag::MET>(beag_mets, "mets");
                evt_runner->assign_collection<beag::Trigger>(beag_triggers, "trigger");
                if(process_mc) evt_runner->assign_collection<beag::TTbarGenEvent>(beag_gen_evts, "gen_evt");
        }
        
        evt_runner->next_event();

	prepare_mor_collections(beag_jets, jets);
	prepare_mor_collections(beag_muons, muons);
	prepare_mor_collections(beag_electrons, electrons);
	prepare_mor_collections(beag_mets, mets);

	if(process_mc) gen_evt->set_beag_info(*beag_gen_evts->begin());

	trigger->set_beag_info(*beag_triggers->begin());
}

template <class beag_type, class mor_type>
void Analyser::prepare_mor_collections(typename std::vector<beag_type> *beag_objs, typename std::vector<mor_type> *mor_objs)
{
	// remove all mor objects from the previous event
	mor_objs->clear();

	// for each beag object in the .root file
	for(typename std::vector<beag_type>::iterator beag_obj = beag_objs->begin();
		beag_obj != beag_objs->end();
		++beag_obj){
		// create an according mor object
		mor_type mor_obj(*beag_obj);
		// add it to the vector of mor objects
		mor_objs->push_back(mor_obj);
	}
}
