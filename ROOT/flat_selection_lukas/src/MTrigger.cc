#include "../interface/MTrigger.h"

void mor::Trigger::set_beag_info(beag::Trigger &beag_trigger)
{
	triggered_list.clear();
	triggered_list.insert(beag_trigger.triggered.begin(), beag_trigger.triggered.end());
}

bool mor::Trigger::trigger_available(std::string trigger)
{
	if(triggered_list.find(trigger) != triggered_list.end())
		return true;
	else
		return false;
}

bool mor::Trigger::triggered(std::string trigger)
{

	if(triggered_list.find(trigger) != triggered_list.end()){
		return triggered_list[trigger];
	}
	else{
		std::cerr << "WARNING: mor::Trigger::triggered(): trigger not available: " << trigger << std::endl;
		return false;
	}
}
