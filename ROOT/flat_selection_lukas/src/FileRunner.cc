#include "../interface/FileRunner.h"

FileRunner::FileRunner()
{
        infile = NULL;
        tree = NULL;
}

FileRunner::~FileRunner()
{
        if(infile){
                delete infile;
                infile = NULL;
        }
        if(tree){
                delete tree;
                tree = NULL;
        }
}

void FileRunner::set_file_names(std::vector<std::string> *file_names)
{
        this->file_names = file_names;
        current_file = file_names->begin();
}

void FileRunner::cd_infile()
{
        infile->cd();
}

TTree* FileRunner::get_next_tree()
{
        tree=NULL;
        if(current_file != file_names->end()){
                if(tree){
                        delete tree;
                        tree = NULL;
                }
                if(infile){
                        infile->Close();
                        delete infile;
                        infile = NULL;
                }
                if(verbose) std::cout << "processing file: " << *current_file << std::endl;
	        infile = new TFile(current_file->c_str(), "OPEN");
                infile->cd();

	        tree = (TTree*) infile->Get("tree");
        }

        ++current_file;

        return tree;
}

bool FileRunner::has_next()
{
        if(current_file != file_names->end())
                return true;
        else
                return false;
}
