#include "TFile.h"
#include <vector>
#include <sstream>
#include <iostream>
//#include <string>
#include "../interface/Analyser.h"

int main(int argc, char **argv)
{
	if(argc <= 2){
		std::cerr << "usage: ./flat_selection <dataset> <filenames>" << std::endl;
		return 1;
	}
	
        int max_events = -1;
	std::string version = "_plots";

	std::map<std::string,std::string> dataset_outfile;
	dataset_outfile["Data"] = "data"+version+".root";
	dataset_outfile["TTbar"] = "ttbar"+version+".root";
	dataset_outfile["TTbarPy"] = "ttbar_pythia"+version+".root";
	dataset_outfile["Wjets"] = "wjets"+version+".root";
	dataset_outfile["Zjets"] = "zjets"+version+".root";
	dataset_outfile["TsChan"] = "t_s_chan"+version+".root";
	dataset_outfile["TtChan"] = "t_t_chan"+version+".root";
	dataset_outfile["TtWChan"] = "t_tW_chan"+version+".root";
	dataset_outfile["Mupt15"] = "qcdmupt15"+version+".root";
	dataset_outfile["QCDem20to30"] = "qcdem20to30"+version+".root";
	dataset_outfile["QCDem30to80"] = "qcdem30to80"+version+".root";
	dataset_outfile["QCDem80to170"] = "qcdem80to170"+version+".root";
	dataset_outfile["QCDbctoe20to30"] = "qcdbctoe20to30"+version+".root";
	dataset_outfile["QCDbctoe30to80"] = "qcdbctoe30to80"+version+".root";
	dataset_outfile["QCDbctoe80to170"] = "qcdbctoe80to170"+version+".root";
	dataset_outfile["TTbarA0j"] = "ttbara0j"+version+".root";
	dataset_outfile["TTbarA1j"] = "ttbara1j"+version+".root";
	dataset_outfile["TTbarA2j"] = "ttbara2j"+version+".root";
	dataset_outfile["TTbarA3j"] = "ttbara3j"+version+".root";
	dataset_outfile["TTbarA4j"] = "ttbara4j"+version+".root";
	dataset_outfile["TTbarMCatNLO"] = "ttbarmcatnlo"+version+".root";

	//Check first argument not rootfile and assign outfile name
	std::string dataset_name = argv[1];
	int is_rootfile = dataset_name.find(".root");
	if(is_rootfile != int(std::string::npos)){
		std::cout << "ERROR: No dataset input.Arg1 = " << dataset_name << std::endl;
		return 0;
	}
	if(dataset_outfile.find(dataset_name) == dataset_outfile.end()){
		std::cout << "ERROR: No outfile for this dataset: " << dataset_name << std::endl;
		return 0;
	}
	std::string outfile_name = dataset_outfile[dataset_name];

	TFile *outfile = new TFile(outfile_name.c_str(), "RECREATE");
        outfile->cd();

        std::vector<std::string> *file_names = new std::vector<std::string>();
        std::cout << "files that will be processed:";
        for(int i = 2; i < argc; ++i){
                std::cout << " " << argv[i];
                file_names->push_back(argv[i]);
        }
        std::cout << std::endl;

        Analyser *analyser = new Analyser(dataset_name);
        analyser->set_file_names(file_names);
        analyser->set_max_events(max_events);
        analyser->set_outfile(outfile);
	if(dataset_name == "Data")
		analyser->process_monte_carlo(false);
	else
		analyser->process_monte_carlo(true);

        analyser->analyse();

        delete analyser;
        analyser = NULL;

	return 0;
}
