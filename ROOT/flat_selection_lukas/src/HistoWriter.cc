#include "../interface/HistoWriter.h"

HistoWriter::HistoWriter()
{
}

HistoWriter::~HistoWriter()
{
	outfile->cd();
	outfile->mkdir("eventselection");
	outfile->Cd("eventselection");
	for(std::vector<TH1F*>::iterator histo = histos1d.begin();
		histo != histos1d.end();
		++histo){
		(*histo)->Write();
	}

	for(std::vector<TH2F*>::iterator histo = histos2d.begin();
		histo != histos2d.end();
		++histo){
		(*histo)->Write();
	}
}

TH1F* HistoWriter::create_1d(std::string identifier,
				std::string title,
				int nbins_x, double min_x, double max_x, std::string x_title, std::string y_title)
{
	outfile->cd();
	TH1F* histo = new TH1F(identifier.c_str(), title.c_str(), nbins_x, min_x, max_x);
	histo->SetXTitle(x_title.c_str());
	histo->SetYTitle(y_title.c_str());
	histos1d.push_back(histo);

	return histo;
}

TH2F* HistoWriter::create_2d(std::string identifier,
				std::string title,
				int nbins_x, double min_x, double max_x,
				int nbins_y, double min_y, double max_y,
				std::string x_title, std::string y_title)
{
	outfile->cd();
	TH2F* histo = new TH2F(identifier.c_str(), title.c_str(), 
			       nbins_x, min_x, max_x,
			       nbins_y, min_y, max_y);
	histo->SetXTitle(x_title.c_str());
	histo->SetYTitle(y_title.c_str());
	histos2d.push_back(histo);

	return histo;
}

void HistoWriter::set_outfile(TFile *outfile)
{
	this->outfile = outfile;
}
