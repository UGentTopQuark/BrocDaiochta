#!/bin/bash

for i in Jet Electron Muon MET Trigger TTbarGenEvent Particle Lepton; do
	rootcint -f ${i}Dict.cc -c ../interface/${i}.h ../interface/Headers.h ../interface/${i}LinkDef.h
done
