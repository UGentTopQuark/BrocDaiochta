#include "../interface/MJet.h"

mor::Jet::Jet(beag::Jet &beag_jet)
{
	SetPx(beag_jet.px);
	SetPy(beag_jet.py);
	SetPz(beag_jet.pz);
	SetE(beag_jet.e);

	mc_matched_val = beag_jet.mc_matched;

	if(mc_matched_val){
		mc_match_p4.SetPx(beag_jet.mc_px);
		mc_match_p4.SetPy(beag_jet.mc_py);
		mc_match_p4.SetPz(beag_jet.mc_pz);
		mc_match_p4.SetE(beag_jet.mc_e);
		mc_id = beag_jet.mc_id;
	}

	ttbar_decay_product_val = beag_jet.ttbar_decay_product;
	from_ttbar_decay = beag_jet.from_ttbar_decay;

	btags.clear();
	btags.insert(beag_jet.btags.begin(), beag_jet.btags.end());
}

void mor::Jet::set_beag_info(beag::Jet &beag_jet)
{
	SetPx(beag_jet.px);
	SetPy(beag_jet.py);
	SetPz(beag_jet.pz);
	SetE(beag_jet.e);

	mc_matched_val = beag_jet.mc_matched;

	if(mc_matched_val){
		mc_match_p4.SetPx(beag_jet.mc_px);
		mc_match_p4.SetPy(beag_jet.mc_py);
		mc_match_p4.SetPz(beag_jet.mc_pz);
		mc_match_p4.SetE(beag_jet.mc_e);
		mc_id = beag_jet.mc_id;
	}

	ttbar_decay_product_val = beag_jet.ttbar_decay_product;
	from_ttbar_decay = beag_jet.from_ttbar_decay;

	btags.insert(beag_jet.btags.begin(), beag_jet.btags.end());
}

double mor::Jet::bDiscriminator(std::string btag_algo)
{
	if(btags.find(btag_algo) != btags.end()){
		return btags[btag_algo];
	}else
		return -1000;
}

bool mor::Jet::bDiscriminator_available(std::string btag_algo)
{
	if(btags.find(btag_algo) != btags.end())
		return true;
	else
		return false;
}
