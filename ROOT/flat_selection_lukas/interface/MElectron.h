#ifndef MOR_ELECTRON_H
#define MOR_ELECTRON_H

#include "MLepton.h"
#include "Electron.h"

namespace mor{
	class Electron : public mor::Lepton{
		public:
			Electron(beag::Electron &beag_electron);
	};
}

#endif
