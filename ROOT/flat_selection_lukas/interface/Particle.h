#ifndef BEAG_PARTICLE_H
#define BEAG_PARTICLE_H

#include "Rtypes.h"

namespace beag{
	class Particle{
		public:
			Particle():mc_matched(false),from_ttbar_decay(false){};
			virtual ~Particle(){};
			double px;
			double py;
			double pz;
			double e;

			bool mc_matched;	// flag if monte carlo truth matched

			bool from_ttbar_decay;	// if particle is from ttbar decay

			double mc_px;
			double mc_py;
			double mc_pz;
			double mc_e;
			double mc_id;		// id of monte carlo truth matched parton

		ClassDef(Particle, 1);
	};
}

#endif
