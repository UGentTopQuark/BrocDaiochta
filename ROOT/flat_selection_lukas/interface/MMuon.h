#ifndef MOR_MUON_H
#define MOR_MUON_H

#include "MLepton.h"
#include "Muon.h"

namespace mor{
	class Muon : public mor::Lepton{
		public:
			Muon(beag::Muon &beag_lepton);
			Muon(){};
			virtual void set_beag_info(beag::Muon &beag_lepton);
			double chi2();		// normalised chi2
			double nHits();
		private:
			double chi2_val;
			double nHits_val;
	};

	inline double Muon::chi2() { return chi2_val; }
	inline double Muon::nHits() { return nHits_val; }
}

#endif
