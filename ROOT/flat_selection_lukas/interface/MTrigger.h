#ifndef MOR_TRIGGER_H
#define MOR_TRIGGER_H

#include <map>
#include <string>
#include <iostream>
#include "Trigger.h"

namespace mor{
	class Trigger{
		public:
			bool trigger_available(std::string trigger);
			bool triggered(std::string trigger);
			void set_beag_info(beag::Trigger &beag_trigger);
		private:
			std::map<std::string,bool> triggered_list;
	};
}

#endif
