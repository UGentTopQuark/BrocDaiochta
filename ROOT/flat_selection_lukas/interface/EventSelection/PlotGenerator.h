#ifndef PLOTGENERATOR_H
#define PLOTGENERATOR_H

#include "../MMET.h"
#include "../MElectron.h"
#include "../MMuon.h"
#include "../MJet.h"
#include "../MTTbarGenEvent.h"

#include "Cuts.h"
#include "MassReconstruction.h"
#include "METCorrector.h"
#include "MassJetMatch.h"
#include "METPlots.h"
#include "TriggerPlots.h"
#include "LeptonIsolationPlots.h"
#include "BTagPlots.h"
#include "MassPlots.h"
#include "ABCDMethodPlots.h"
#include "BasicObjectPlots.h"
#include "XPlusNJetsPlots.h"
#include "MVAInputProducer.h"

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include "TMath.h"

#include "TH1F.h"
#include "TH2F.h"
#include <map>
#include "TGraph.h"

#include "../HistoWriter.h"

class PlotGenerator{
	public:
		PlotGenerator(std::string ident);
		~PlotGenerator();
                void set_handles(std::vector<mor::Muon> *muons,
				std::vector<mor::Jet> *jets,
				std::vector<mor::Electron> *electrons,
				std::vector<mor::MET> *mets,
				mor::Trigger *trigger);
		void next_event();
		void plot();
		void apply_cuts(Cuts *mycuts);
		void deactivate_cuts();
		void set_tprime_mass(double mass);
		void set_histo_writer(HistoWriter *histo_writer);
		void set_gen_evt(mor::TTbarGenEvent *gen_evt);

	private:
		bool static compare_btag(const std::pair<int,double> p1, const std::pair<int,double> p2);
		void book_histos();

		std::vector<mor::Jet>* jets;
		std::vector<mor::Electron>* isolated_electrons;
		std::vector<mor::Electron>* all_electrons;
		std::vector<mor::Muon>* isolated_muons;
		std::vector<mor::MET>* corrected_mets;
		std::vector<mor::MET>* uncorrected_mets;

		// collections w/o selection as they are in file
		std::vector<mor::Electron> *electrons;
		std::vector<mor::Jet> *uncut_jets;
		std::vector<mor::Muon> *muons;
		std::vector<mor::MET> *mets;
		mor::TTbarGenEvent *gen_evt;
		mor::Trigger *HLTR;

		std::map<std::string,TH1F*> histos1d;
		std::map<std::string,TH2F*> histos2d;
		std::map<std::string,TGraph*> Tgraphs;

		Cuts *cuts;
		MassReconstruction *mass_reco;
                METCorrector *METCor;
		MassJetMatch *mass_jet_match;
		BJetFinder *bjet_finder;
		HtCalculator *ht_calc;

		MVAInputProducer *mva_prod;

		// plot classes
		METPlots *met_plots;
		MassPlots *mass_plots;
		BasicObjectPlots *basic_plots;
		TriggerPlots *trigger_plots;
		LeptonIsolationPlots *lept_iso_plots;
		BTagPlots *btag_plots;
		ABCDMethodPlots *abcd_plots;
		XPlusNJetsPlots *xplusnjets_plots;

		// to avoid calculating H_T several times... this should be solved properly one day
		double ht;

		double tprime_mass;

		static const bool plot_met = true;
		static const bool plot_mass = false;
		static const bool plot_kinematics = true;
		static const bool plot_trigger = false;
		static const bool plot_lept_iso = true;
		static const bool plot_btag = false;
		static const bool plot_abcd = false;
		static const bool plot_xplusnjets = false;

		HistoWriter *histo_writer;

		// to distinguish the plots of multiple instances of the class
		// (muon-channel, electron-channel...)
		std::string id;
};

#endif
