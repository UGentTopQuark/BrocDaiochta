#ifndef XPLUSNJETSPLOTS_H
#define XPLUSNJETSPLOTS_H

#include "Plots.h"

class XPlusNJetsPlots: public Plots{
	public:
		XPlusNJetsPlots(std::string ident);
		~XPlusNJetsPlots();

	private:
		virtual void plot_all();
		virtual void book_histos();

		// functions for plotting different variables
		void plot_jets();
};

#endif
