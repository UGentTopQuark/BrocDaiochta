#ifndef MyMEzCalculator_h
#define MyMEzCalculator_h

/**_________________________________________________________________
   class:   MEzCalculator.h

 author: Francisco Yumiceva, Fermilab (yumiceva@fnal.gov)

 version $Id: MEzCalculator.h,v 1.3.4.1 2009/02/18 21:29:37 yumiceva Exp $

________________________________________________________________**/

#include "../MLepton.h"
#include "../MMET.h"

#include "TLorentzVector.h"

class MyMEzCalculator {
  
 public:
  /// constructor
  MyMEzCalculator();
  /// destructor
  ~MyMEzCalculator();
  /// Set MET
  void SetMET(mor::MET& MET) { MET_ = MET; } ;
  /// Set lepton
  void SetLepton(mor::Lepton& lepton, bool isMuon = true) {
	  lepton_ = lepton;
	  isMuon_ = isMuon;
  };
  /// Calculate MEz
  /// options to choose roots from quadratic equation:
  /// type = 0 : if real roots, pick the one nearest to
  ///                     the lepton Pz except when the Pz so chosen
  ///                     is greater than 300 GeV in which case pick
  ///                     the most central root.
  /// type = 1 (default): if real roots, choose the one closest to the lepton Pz
  ///           if complex roots, use only the real part.
  /// type = 2: if real roots, choose the most central solution.
  ///           if complex roots, use only the real part.
  double Calculate(int type = 1);
  /// check for complex root
  bool IsComplex() const { return isComplex_; };
  /// verbose
  void Print() {
	  std::cout << " METzCalculator: pxmu = " << lepton_.Px() << " pzmu= " << lepton_.Pz() << std::endl;
	  std::cout << " METzCalculator: pxnu = " << MET_.Px() << " pynu= " << MET_.Py() << std::endl;
  }

  double get_solution1();
  double get_solution2();

 private:

	double solution1;
	double solution2;
  bool isComplex_;
  mor::Lepton lepton_;
  mor::MET MET_;
  bool isMuon_;
};

#endif
