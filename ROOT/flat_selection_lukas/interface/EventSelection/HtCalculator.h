#ifndef HTCALCULATOR_H
#define HTCALCULATOR_H

#include "../MMET.h"
#include "../MElectron.h"
#include "../MMuon.h"
#include "../MJet.h"

class HtCalculator{
	public:
		HtCalculator();
		~HtCalculator();

		void set_handles(std::vector<mor::Jet>* jets,
				 std::vector<mor::Electron>* isolated_electrons,
				 std::vector<mor::Muon>* isolated_muons,
				 std::vector<mor::MET>* corrected_mets);
		double get_ht();

	private:
		double calculate_ht();

		std::vector<mor::Jet>* jets;
		std::vector<mor::Electron>* isolated_electrons;
		std::vector<mor::Muon>* isolated_muons;
		std::vector<mor::MET>* corrected_mets;

		double ht;
};

#endif
