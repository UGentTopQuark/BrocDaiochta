#ifndef BJETFINDER_H
#define BJETFINDER_H

#include "../MJet.h"
#include <vector>
#include <string>

class BJetFinder{
	public:
		BJetFinder();
		~BJetFinder();
		
		std::vector<std::pair<int, double> >* get_btag_sorted_jets(std::string btag_algo="trackCountingHighEffBJetTags");

		void set_handles(std::vector<mor::Jet>* jets);
		void set_min_btag_value(std::vector<double> min_btag_value);
		void set_max_considered_jets(int njets);

	private:
		bool static compare_btag(const std::pair<int,double> p1, const std::pair<int,double> p2);
		void sort_btagged_jets(std::string btag_algo="trackCountingHighEffBJetTags");
		void cut_sorted_jets();

		std::vector<std::pair<int, double> > *btag_values;
		std::vector<mor::Jet>* jets;

		bool sorted; // btagged jets already sorted for this event?
		std::map<std::string,std::vector<double> > old_min_btag_value;
		std::vector<double> min_btag;
		int max_njets;
		//1st and 2nd highest ids per algorithm
		std::map<std::string,std::vector<std::pair<int, double> >* > btag_algo_ids;
};

#endif
