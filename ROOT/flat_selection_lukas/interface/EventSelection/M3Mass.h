#ifndef M3MASS_H
#define M3MASS_H

#include "MassReconstructionMethod.h"

class M3Mass: public MassReconstructionMethod {
	public:

	protected:
		void calculate_mass();
};

#endif
