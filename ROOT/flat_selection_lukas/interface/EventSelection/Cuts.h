#ifndef CUTS_H
#define CUTS_H

#include "LeptonSelector.h"
#include "JetSelector.h"
#include "MassReconstruction.h"
#include "METCorrector.h"
#include "BJetFinder.h"
//#include "analysers/EventSelection/interface/DoubleCountedLeptonRemover.h"

#include "../MMuon.h"
#include "../MElectron.h"
#include "../MJet.h"
#include "../MMET.h"
#include "../MTrigger.h"
#include "../MTTbarGenEvent.h"

#include "HtCalculator.h"

#include "Math/LorentzVector.h" 
#include "Math/VectorUtil.h" 

#include <map>

class Cuts{
	public:
		Cuts(std::string ident);
		~Cuts();
                void set_handles(std::vector<mor::Muon> *muons,
				std::vector<mor::Jet> *jets,
				std::vector<mor::Electron> *electrons,
				std::vector<mor::MET> *mets,
				mor::Trigger *trigger);
		void set_gen_evt(mor::TTbarGenEvent *genEvt);
		bool cut();
		void next_event();

		void set_all_cuts(std::map<std::string,double> &cuts_set);
		void set_all_vcuts(std::map<std::string,std::vector<double>*> &cuts_set);

		//for plot generator
		std::vector<double>* get_min_btag();
		double get_name_btag();
	
		MassReconstruction* get_mass_reco();
		HtCalculator* get_ht_calc();

		inline std::vector<mor::Electron>* get_isolated_electrons(){ return isolated_electrons; };
		inline std::vector<mor::Muon>* get_isolated_muons(){ return isolated_muons; };
		inline std::vector<mor::MET>* get_corrected_mets(){ return corrected_mets; };
		inline std::vector<mor::Jet>* get_jets(){ return jets; };
		inline std::vector<mor::Electron>* get_all_electrons(){ return all_electrons; };

		void print_cuts();
		void print_cuts_vector(std::string id, std::vector<double> *cuts);
		
	private:

		typedef ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > MyLorentzVector;

		bool cut_M3();
		bool cut_mindiffM3();
		bool cut_chi2();
		bool cut_mu_pt();
		bool cut_e_pt();
		bool cut_njets();
		bool cut_met();
		bool cut_min_ht();
		bool cut_max_ht();
		bool cut_nisolated_leptons();
		bool cut_nisolated_electrons();
		bool cut_nisolated_muons();
		bool cut_nloose_electrons();
		bool cut_nloose_muons();
		bool cut_Zrejection();
		bool cut_btag();
		bool cut_hltrigger();
	
		std::vector<mor::Electron> *electrons;
		std::vector<mor::Jet> *uncut_jets;
		std::vector<mor::Jet> *jets;
		std::vector<mor::Muon> *muons;
		std::vector<mor::MET> *mets;
		std::vector<mor::MET>* corrected_mets;
		std::vector<mor::MET>* uncorrected_mets;
		std::vector<mor::Muon>* isolated_muons;
		std::vector<mor::Electron>* isolated_electrons;
		std::vector<mor::Electron>* all_electrons;
		std::vector<mor::Muon>* loose_muons;
		std::vector<mor::Electron>* loose_electrons;

		mor::TTbarGenEvent *genEvt;

		//for trigger information
		mor::Trigger *HLTR;
	

		std::vector<double> *min_jet_pt;

		std::vector<double> *max_mu_trackiso;
		std::vector<double> *max_mu_hcaliso;
		std::vector<double> *max_mu_hcal_veto_cone;
		std::vector<double> *max_mu_ecal_veto_cone;
		std::vector<double> *max_mu_caliso;
		std::vector<double> *max_mu_ecaliso;
		std::vector<double> *min_mu_relIso;
		std::vector<double> *min_mu_dR;
		std::vector<double> *min_mu_pt;
		std::vector<double> *min_mu_et;
		std::vector<double> *mu_electronID;
		std::vector<double> *min_mu_nHits;
		std::vector<double> *max_mu_d0;
		std::vector<double> *max_mu_d0sig;
		std::vector<double> *max_mu_chi2;

		std::vector<double> *max_e_trackiso;
		std::vector<double> *max_e_hcaliso;
		std::vector<double> *max_e_hcal_veto_cone;
		std::vector<double> *max_e_ecal_veto_cone;
		std::vector<double> *max_e_caliso;
		std::vector<double> *max_e_ecaliso;
		std::vector<double> *min_e_relIso;
		std::vector<double> *min_e_dR;
		std::vector<double> *min_e_pt;
		std::vector<double> *min_e_et;
		std::vector<double> *e_electronID;
		std::vector<double> *min_e_nHits;
		std::vector<double> *max_e_d0;
		std::vector<double> *max_e_d0sig;
		std::vector<double> *max_e_chi2;

		std::vector<double> *max_loose_mu_trackiso;
		std::vector<double> *max_loose_mu_hcaliso;
		std::vector<double> *max_loose_mu_hcal_veto_cone;
		std::vector<double> *max_loose_mu_ecal_veto_cone;
		std::vector<double> *max_loose_mu_caliso;
		std::vector<double> *max_loose_mu_ecaliso;
		std::vector<double> *min_loose_mu_relIso;
		std::vector<double> *min_loose_mu_dR;
		std::vector<double> *min_loose_mu_pt;
		std::vector<double> *min_loose_mu_et;
		std::vector<double> *loose_mu_electronID;
		std::vector<double> *min_loose_mu_nHits;
		std::vector<double> *max_loose_mu_d0;
		std::vector<double> *max_loose_mu_d0sig;
		std::vector<double> *max_loose_mu_chi2;

		std::vector<double> *max_loose_e_trackiso;
		std::vector<double> *max_loose_e_hcaliso;
		std::vector<double> *max_loose_e_hcal_veto_cone;
		std::vector<double> *max_loose_e_ecal_veto_cone;
		std::vector<double> *max_loose_e_caliso;
		std::vector<double> *max_loose_e_ecaliso;
		std::vector<double> *min_loose_e_relIso;
		std::vector<double> *min_loose_e_dR;
		std::vector<double> *min_loose_e_pt;
		std::vector<double> *min_loose_e_et;
		std::vector<double> *loose_e_electronID;
		std::vector<double> *min_loose_e_nHits;
		std::vector<double> *max_loose_e_d0;
		std::vector<double> *max_loose_e_d0sig;
		std::vector<double> *max_loose_e_chi2;

		std::vector<double> *trigger;
		std::vector<double> *min_btag;
		
		MassReconstruction *mass_reco;
		METCorrector *METCor;

		// variable to calculate ht only once for the class
		double ht;

		double min_M3;
		double min_mindiffM3;
		double min_met;
		double max_met;
		double max_ht;
		double min_ht;
		int min_no_jets;
		int max_no_jets;
		int max_nloose_e;
		int max_nloose_mu;
		int min_nisolated_lep;
		int max_nisolated_e;
		int max_nisolated_mu;
		int min_nisolated_e;
		int min_nisolated_mu;
		double JES_factor;
		std::vector<double>* max_mu_eta;
		std::vector<double>* max_e_eta;
		std::vector<double>* max_loose_mu_eta;
		std::vector<double>* max_loose_e_eta;
		double max_jet_eta;
		double min_jet_e_dR;
		int e_type;
		int mu_type;
		int loose_e_type;
		int loose_mu_type;
		double Z_rejection_width;
		double num_name_btag;
		double min_chi2;
		double max_chi2;
		std::string name_btag;
  
		//	int min_trigmatch_num;
		//	std::string name_trigger;
		
		int cuts_passed_counter;
		int cuts_not_passed_counter;

		bool already_cut; // avoid two calls of cut() for the same event
				  // reset for every set_handles() or set_min/max_xyz()
		bool already_cut_result; // value to be returned if cut() called twice
					 // for the same event

		std::string identifier;
		
		LeptonSelector<mor::Electron> *e_selector;
		LeptonSelector<mor::Electron> *all_e_selector;
		LeptonSelector<mor::Muon> *mu_selector;
		LeptonSelector<mor::Electron> *loose_e_selector;
		LeptonSelector<mor::Muon> *loose_mu_selector;
		JetSelector *jet_selector;

//		DoubleCountedLeptonRemover<pat::Muon> *double_mu_remover;
//		DoubleCountedLeptonRemover<pat::Electron> *double_e_remover;
		
		BJetFinder *bjet_finder;
		HtCalculator *ht_calc;
};

#endif
