#ifndef LEPTONSELECTOR_H
#define LEPTONSELECTOR_H

#include "../MElectron.h"
#include "../MMuon.h"
#include "../MLepton.h"
#include "../MJet.h"

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

template <class myLepton>
class LeptonSelector{
	public:
		LeptonSelector();
		~LeptonSelector();
                void set_max_trackiso(std::vector<double> *max);
                void set_max_caliso(std::vector<double> *max);
                void set_max_ecaliso(std::vector<double> *max);
                void set_max_hcaliso(std::vector<double> *max);
                void set_max_hcal_veto_cone(std::vector<double> *max);
                void set_max_ecal_veto_cone(std::vector<double> *max);
                void set_min_dR(std::vector<double> *min);
		void set_min_pt(std::vector<double> *min);
		void set_min_et(std::vector<double> *min);
		void set_min_relIso(std::vector<double> *min);
		void set_max_eta(std::vector<double> *max);

		/*
		 *	electron IDs:
		 *	0: "eidRobustLoose";
		 *	1: "eidRobustTight";
		 *	2: "eidLoose";
		 *	3: "eidTight";
		 */
		void set_electronID(std::vector<double> *eid);
		void set_min_nHits(std::vector<double> *min);
		void set_max_d0(std::vector<double> *max);
		void set_max_d0sig(std::vector<double> *max);
		void set_max_chi2(std::vector<double> *max);

		/*
		 *	muons:
		 *	0: isGlobalMuon()
		 */
		void set_lepton_type(int lepton_type);
		std::vector<myLepton>* get_leptons(std::vector<myLepton> *leptons, std::vector<mor::Jet> *myjets);
		
	private:
                std::vector<double> *max_trackiso;
                std::vector<double> *max_caliso;
                std::vector<double> *max_ecaliso;
                std::vector<double> *max_hcaliso;
                std::vector<double> *max_hcal_veto_cone;
                std::vector<double> *max_ecal_veto_cone;
		std::vector<double> *min_dR;
		std::vector<double> *min_pt;
		std::vector<double> *min_et;
		std::vector<double> *min_relIso;
		std::vector<double> *electronID;
		std::vector<double> *min_nHits;
		std::vector<double> *max_d0;
		std::vector<double> *max_d0sig;
		std::vector<double> *max_chi2;
		std::vector<double> *max_eta;
		std::vector<myLepton> *isolated_leptons;
		int lepton_type;

		static bool const cut_e_trigger;
		static bool const cut_mu_trigger;

                bool cut_trackiso(myLepton *lepton_iter, double &max_trackiso);
                bool cut_caliso(myLepton *lepton_iter, double &max_caliso);
                bool cut_ecaliso(myLepton *lepton_iter, double &max_ecaliso);
                bool cut_hcaliso(myLepton *lepton_iter, double &max_hcaliso);
                bool cut_hcal_veto_cone(myLepton *lepton_iter, double &max_hcal_veto_cone);
                bool cut_ecal_veto_cone(myLepton *lepton_iter, double &max_ecal_veto_cone);
                bool cut_dR(myLepton *lepton_iter, double &min_dR);
		bool cut_pt(myLepton *lepton_iter, double &min_pt);
		bool cut_et(myLepton *lepton_iter, double &min_et);
		bool cut_eta(myLepton *lepton_iter, double &max_eta);
		bool cut_relIso(myLepton *lepton_iter, double &min_relIso);
		bool cut_electronID(myLepton *lepton_iter, double &electron_id);
		bool cut_lepton_type(myLepton *lepton_iter, int &lepton_type);
		bool cut_chi2(myLepton *lepton_iter, double &max_chi2);
		bool cut_d0(myLepton *lepton_iter, double &max_d0);
		bool cut_d0sig(myLepton *lepton_iter, double &max_d0sig);
		bool cut_nHits(myLepton *lepton_iter, double &min_nHits);
		bool cut_trigger(myLepton *lepton_iter);

		double deltaR(ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_1,
			      ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p4_2);

		std::vector<mor::Jet> *jets;
		static bool const verbose = false;
};

template<class myLepton> bool const LeptonSelector<myLepton>::cut_e_trigger = false; 	//	match electrons to trigger electrons
template<class myLepton> bool const LeptonSelector<myLepton>::cut_mu_trigger = false;	//	match muons to trigger muons

#endif
