#ifndef PLOTS_H
#define PLOTS_H

#include "../MMET.h"
#include "../MElectron.h"
#include "../MMuon.h"
#include "../MJet.h"

#include <iostream>
#include <map>
#include "TH1F.h"
#include "TH2F.h"

#include "../HistoWriter.h"
#include "../MTTbarGenEvent.h"

class Plots{
	public:
		Plots();
		virtual ~Plots();
		void set_handles(std::vector<mor::Jet>* jets,
				 std::vector<mor::Electron>* isolated_electrons,
				 std::vector<mor::Muon>* isolated_muons,
				 std::vector<mor::MET>* corrected_mets);
		void plot();
		void set_histo_writer(HistoWriter *histo_writer);
		void set_gen_evt(mor::TTbarGenEvent *gen_evt);
	protected:
		std::string id;
                std::map<std::string,TH1F*> histos1d;
                std::map<std::string,TH2F*> histos2d;

		virtual void prepare_objects();
		virtual void book_histos();
		virtual void plot_all()=0;

		std::vector<mor::Jet>* jets;
		std::vector<mor::Electron>* isolated_electrons;
		std::vector<mor::Muon>* isolated_muons;
		std::vector<mor::MET>* corrected_mets;

		bool histos_booked;

		HistoWriter *histo_writer;
		mor::TTbarGenEvent *gen_evt;
};

#endif
