#ifndef JETSELECTOR_H
#define JETSELECTOR_H

#include "../MJet.h"
#include "../MElectron.h"

#include "PtSorter.h"
#include "Tools.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class JetSelector{
	public:
		JetSelector();
		~JetSelector();
		void set_min_pt(std::vector<double> *min);
		void set_max_eta(double max);
		void set_min_e_dR(double min);
		void change_JES(double scaling_factor);

		template <typename myLepton>
		std::vector<mor::Jet>* get_jets(std::vector<mor::Jet> *uncut_jets, std::vector<myLepton> *leptons);

	private:
		double max_eta;
		double min_e_dR;
		double JES_scaling_factor;

		// needed for dR calculation
		Tools *tools;

		std::vector<double> *min_pt;

		std::vector<mor::Jet> *cut_jets;

		bool cut_pt(mor::Jet *jet_iter, double &min_pt);
		bool cut_eta(mor::Jet *jet_iter, double &max_eta);
		template <typename myLepton>
		bool cut_leptons(mor::Jet *jet_iter, std::vector<myLepton> *leptons);
};
#endif
