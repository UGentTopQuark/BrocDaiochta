#ifndef MASSPLOTS_H
#define MASSPLOTS_H

#include "Plots.h"
#include "MassReconstruction.h"
#include "MassJetMatch.h"
#include "HtCalculator.h"
#include "BJetFinder.h"
#include "TMath.h"

class MassPlots : public Plots{
	public:
		MassPlots(std::string ident);
		~MassPlots();

		void set_mass_reco(MassReconstruction *mass_reco);
		void set_bjet_finder(BJetFinder *bjet_finder);
		void set_ht_calc(HtCalculator *ht_calc);
		void set_gen_evt_handle(mor::TTbarGenEvent *gen_evt);
		void set_mass_jet_match(MassJetMatch *mass_jet_match);

	private:
		void book_histos();

		virtual void plot_all();
		void plot_masstop();
		void plot_massjetmatch_top();
		void plot_Wb_mass();
		void plot_recogenmatch_top();
		void plot_Kinfit_top();
		void plot_minmass_1btag_top();
		void plot_minmass_2btag_top();
		void plot_genmass_top();
		void plot_chimass_top();
		void plot_chimass_1btag_top();
		void plot_chimass_2btag_top();

   		void plot_massW(double nominal_massW);

		MassReconstruction *mass_reco;
		MassJetMatch *mass_jet_match;
		BJetFinder *bjet_finder;

		HtCalculator *ht_calc;

		mor::TTbarGenEvent *gen_evt;

		double ht;
};

#endif
