#ifndef CUTSELECTOR_H
#define CUTSELECTOR_H

#include "PlotGenerator.h"
#include "Cuts.h"
#include "Tools.h"

#include "../MMuon.h"
#include "../MElectron.h"
#include "../MJet.h"
#include "../MMET.h"
#include "../MTrigger.h"
#include "../MTTbarGenEvent.h"

#include <string>
#include <map>
#include <vector>
#include "TH1F.h"
#include "../HistoWriter.h"

class CutSelector{
	public:
		CutSelector(std::string ident, double mass=175.0);
		~CutSelector();
                void set_handles(std::vector<mor::Muon> *muons,
				std::vector<mor::Jet> *jets,
				std::vector<mor::Electron> *electrons,
				std::vector<mor::MET> *mets,
				mor::Trigger *trigger);
		void set_gen_evt(mor::TTbarGenEvent *genEvt);

		void plot();
		void set_tprime_mass(double tprimeMass);
		void set_histo_writer(HistoWriter *histo_writer);

	private:
		std::vector<std::vector<double>* > cuts_to_be_deleted;

		std::string get_event_type();
		void set_cuts();
		void complete_cuts();
		void define_cuts_autogen();
		std::vector<std::string> get_event_types(std::string event_type);

		void synchronise_maps();
		void vcuts(std::string type, std::string set, std::string cut, std::vector<double> *cut_vector);
		void vcuts(std::string type, std::string set, std::string cut, double value);
		void set_if_not_set(std::string type, std::string set, std::string cut, double value);
		void vset_if_not_set(std::string type, std::string set, std::string cut, double value);
		void vset_if_not_set(std::string type, std::string set, std::string cut, std::vector<double>* value);

		std::map<std::string, std::map<std::string,PlotGenerator*> > plot_generators;
		std::map<std::string, std::map<std::string,Cuts*> > cuts;

		std::map<std::string,std::map<std::string,std::map<std::string,double> > > cut_defs;
		std::map<std::string,std::map<std::string,std::map<std::string,std::vector<double>* > > > vcut_defs;

		std::string dataset_id;

                std::vector<mor::Electron> *electrons;
                std::vector<mor::Jet> *jets;
                std::vector<mor::Muon> *muons;
                std::vector<mor::MET> *mets;

		//for truth matching
		mor::TTbarGenEvent *genEvt;

		mor::Trigger *trigger;

		/*
		 *	count different event types:
		 *	muon: 1
		 *	electron: 2
		 *	background: 3
		 */
		TH1F *event_counter_histo;

		HistoWriter *histo_writer;
		
		int data_count;
		int ttmu_count;
		int tte_count;
		int ttbg_count;
		double tprime_mass;
		bool Event_firstcall;
};

#endif
