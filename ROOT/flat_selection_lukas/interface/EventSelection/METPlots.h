#ifndef METPLOTS_H
#define METPLOTS_H

#include "Plots.h"
#include "METCorrector.h"
#include "MyMEzCalculator.h"
#include "../MMET.h"
#include "Tools.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class METPlots : public Plots{
	public:
		METPlots(std::string ident);
		~METPlots();
		void set_met_handle(std::vector<mor::MET> *analyser_mets);
	protected:
		virtual void plot_all();
		void plot_MET_pz();
		void plot_MET_MtW();
		int get_pt_bin(double neutrino_pt);
		int get_pz_bin(double neutrino_pz);
		virtual void book_histos();
		std::vector<mor::MET> *method0_mets;
		std::vector<mor::MET> *method1_mets;
		std::vector<mor::MET> *method2_mets;
		std::vector<mor::MET> *uncorrected_mets;

        	METCorrector *METCor_method0;
        	METCorrector *METCor_method1;
        	METCorrector *METCor_method2;
		std::vector<mor::MET> *mets;

		static const int PT_BINS=10;	// compare MET and nu pt in x pt-bins
};

#endif
