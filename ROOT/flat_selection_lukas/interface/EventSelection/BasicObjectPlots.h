#ifndef BASICOBJECTPLOTS_H
#define BASICOBJECTPLOTS_H

#include "Plots.h"
#include "Math/VectorUtil.h"
#include "HtCalculator.h"

class BasicObjectPlots : public Plots{
	public:
		BasicObjectPlots(std::string ident);
		~BasicObjectPlots();

		void set_uncut_jets(std::vector<mor::Jet>* uncut_jets);
		void set_ht_calc(HtCalculator *ht_calc);
		
	private:
		virtual void plot_all();
		virtual void book_histos();

		void plot_jets();
		void plot_muons();
		void plot_electrons();
		void plot_ht();
		void plot_electron_quality();
		void plot_muon_quality();
		void plot_dR();
		void get_triggereff();
		void analyse_recogen_matchedjets();

		std::vector<mor::Jet>* uncut_jets;

		HtCalculator *ht_calc;
		double ht;
};

#endif
