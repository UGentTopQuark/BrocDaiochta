#ifndef METCORRECTOR_H
#define METCORRECTOR_H

#include "../MMET.h"
#include "../MLepton.h"
#include "../MMuon.h"
#include "../MElectron.h"

#include "MyMEzCalculator.h"

class METCorrector{
	public:
		METCorrector();
		~METCorrector();
		void set_handles(std::vector<mor::MET> *mets_handle, mor::Lepton *lepton=NULL, bool is_muon=true);
		std::vector<mor::MET>* get_MET();
		std::vector<mor::MET>* get_uncorrected_MET();
		bool is_corrected();
		void set_correction_method(int method);
	private:
		void correct_MET();
		void prepare_uncorrected_MET();

		mor::Lepton *lepton;
		std::vector<mor::MET>* mets_handle;
		std::vector<mor::MET>* mets;
		std::vector<mor::MET>* uncorrected_mets;
		MyMEzCalculator *MEzCal;
		bool is_muon;
		bool corrected;
		int correction_method;
		static const bool verbose = 0;
};

#endif
