#ifndef LEPTONISOLATIONPLOTS_H
#define LEPTONISOLATIONPLOTS_H

#include "Plots.h"

class LeptonIsolationPlots : public Plots {
	public:
		LeptonIsolationPlots(std::string ident);
		~LeptonIsolationPlots();

		void set_gen_evt(mor::TTbarGenEvent *gen_evt);
	private:
		virtual void plot_all();
		void book_histos();
		void matchleptons_plotiso_plotveto();

		mor::TTbarGenEvent *gen_evt;
};

#endif
