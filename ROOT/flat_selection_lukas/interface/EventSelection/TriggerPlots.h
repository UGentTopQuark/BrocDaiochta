#ifndef TRIGGERPLOTS_H
#define TRIGGERPLOTS_H

#include "Plots.h"
#include "Tools.h"
#include "../MTrigger.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"


class TriggerPlots : public Plots{
	public:
		TriggerPlots(std::string ident);
		~TriggerPlots();
		void set_gen_evt(mor::TTbarGenEvent *gen_evt);
		void set_trigger(mor::Trigger *HLTR);
		void set_muons(std::vector<mor::Muon> *muons);

	private:
		virtual void plot_all();
		void book_histos();
		void plot_trigger();
		void plot_trigger_muons();

		mor::Trigger *HLTR;
		std::vector<mor::Muon> *muons;
		mor::TTbarGenEvent *gen_evt;
};

#endif
