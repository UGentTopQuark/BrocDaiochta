#ifndef ABCDMETHODPLOTS_H
#define ABCDMETHODPLOTS_H

#include "Plots.h"

class ABCDMethodPlots : public Plots{
	public:
		ABCDMethodPlots(std::string ident);
		~ABCDMethodPlots();
	
	private:
		void plot_all();
		void book_histos();
		void plot_leptIso_electron();
		void plot_leptIso_muon();
		void plot_d0sig_vs_relIso();
};

#endif
