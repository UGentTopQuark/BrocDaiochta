#ifndef MOR_LEPTON_H
#define MOR_LEPTON_H

#include "MParticle.h"
#include "Lepton.h"
#include <map>
#include <iostream>

namespace mor{
	class Lepton : public Particle{
		public:
			double ecalIso();
			double hcalIso();
			double caloIso();
			double trackIso();
			double relIso();

			double hcal_vcone();
			double ecal_vcone();

			double d0();
			double d0_sigma();

			int charge();

			bool lepton_id_passed();	// global muon, eID robust tight
			bool track_available();

			bool triggered(std::string trigger_name);

			virtual void set_beag_info(beag::Lepton &beag_lepton);
		protected:
			double ecal_iso;	
			double hcal_iso;	
			double track_iso;	

			double hcal_vcone_val;
			double ecal_vcone_val;

			double d0_val;
			double d0_sigma_val;

			signed char charge_val;

			bool lepton_id;
			bool track_available_val;

			std::map<std::string,bool> trigger;	// Trigger bits -> lepton triggered by certain trigger:
								// true / false
	};

	inline double Lepton::ecalIso() { return ecal_iso; }
	inline double Lepton::caloIso() { return ecal_iso+hcal_iso; }
	inline double Lepton::hcalIso() { return hcal_iso; }
	inline double Lepton::trackIso() { return track_iso; }
	inline double Lepton::relIso() { return 1./(1.+track_iso/this->Pt()+(ecal_iso+hcal_iso)/this->Et()); }

	inline double Lepton::hcal_vcone() { return hcal_vcone_val; }
	inline double Lepton::ecal_vcone() { return ecal_vcone_val; }

	inline double Lepton::d0() { return d0_val; }
	inline double Lepton::d0_sigma() { return d0_sigma_val; }

	inline int Lepton::charge() { return charge_val; }

	inline bool Lepton::lepton_id_passed() { return lepton_id; }
	inline bool Lepton::track_available() { return track_available_val; }
}

#endif
