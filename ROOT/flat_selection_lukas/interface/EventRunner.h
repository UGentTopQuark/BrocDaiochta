#ifndef EVENTRUNNER_H
#define EVENTRUNNER_H

#include "TBranch.h"
#include "TTree.h"
#include "FileRunner.h"
#include "Jet.h"
#include "Electron.h"
#include "Muon.h"
#include "MET.h"
#include "Trigger.h"
#include "TTbarGenEvent.h"

class EventRunner{
        public:
                EventRunner();
                ~EventRunner();
                void next_event();
                template <class beagObj>
                void assign_collection(typename std::vector<beagObj> *&collection, std::string branch_name);
                bool has_next();
                int current_event();
                bool end_of_file();
                void set_file_names(std::vector<std::string> *file_names);
        private:
                TTree *tree;
                FileRunner *file_runner;
                int current_file_event;
                int current_file_max_events;
                int overall_current_event;

                static const bool verbose = false;
                bool new_file;
};

#endif
