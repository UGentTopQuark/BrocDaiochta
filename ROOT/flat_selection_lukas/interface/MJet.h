#ifndef MOR_JET_H
#define MOR_JET_H

#include "MParticle.h"
#include "Jet.h"
#include <map>

namespace mor{
	class Jet : public Particle{
		public:
			Jet(beag::Jet &beag_jet);
			Jet(){};
			double bDiscriminator(std::string btag_algo);
			bool bDiscriminator_available(std::string btag_algo);
			int ttbar_decay_product();
			void set_beag_info(beag::Jet &beag_jet);
		private:
			std::map<std::string, double> btags;	// algo - bDiscriminator
			/*
			 *	ttbar_decay_product:
			 *	is jet from quark from ttbar decay
			 *	possible values:
			 *	1: quark jet
			 *	2: anti-quark jet
			 *	3: hadronically decaying top b-jet
			 *	4: leptonically decaying top b-jet
			 */
			int ttbar_decay_product_val;
	};

	inline int mor::Jet::ttbar_decay_product() { return ttbar_decay_product_val; }
}

#endif
