#ifndef FILERUNNER_H
#define FILERUNNER_H

#include <vector>
#include "TFile.h"
#include "TTree.h"
#include <iostream>

class FileRunner{
        public:
                FileRunner();
                ~FileRunner();
                void set_file_names(std::vector<std::string> *file_names);
                bool has_next();
                TTree* get_next_tree();
                void cd_infile();
        private:
                std::vector<std::string> *file_names;
                TFile *infile;
                std::vector<std::string>::iterator current_file;
                TTree *tree;
                static const bool verbose = true;
};

#endif
