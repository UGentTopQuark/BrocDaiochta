#ifndef ANALYSER_H
#define ANALYSER_H

#include <vector>
#include <iostream>
#include "../interface/Jet.h"
#include "../interface/Electron.h"
#include "../interface/Muon.h"
#include "../interface/MET.h"
#include "../interface/MJet.h"
#include "../interface/MElectron.h"
#include "../interface/MMuon.h"
#include "../interface/MMET.h"
#include "../interface/Trigger.h"
#include "../interface/TTbarGenEvent.h"
#include "../interface/MTTbarGenEvent.h"
#include "../interface/MTrigger.h"
#include "../interface/EventRunner.h"
#include "../interface/EventSelection/CutSelector.h"
#include "HistoWriter.h"

#include "TFile.h"

class Analyser{
        public:
	Analyser(std::string dataset_name);
                ~Analyser();
                void set_file_names(std::vector<std::string> *file_names);
                void set_max_events(int max_events);
                void set_outfile(TFile *outfile);
		void process_monte_carlo(bool process_mc);
                void analyse();
        private:
                void change_event();

		template <class beag_type, class mor_type>
		void prepare_mor_collections(typename std::vector<beag_type> *beag_objs, typename std::vector<mor_type> *mor_objs);

                EventRunner *evt_runner;
                std::vector<beag::Jet> *beag_jets;
                std::vector<beag::Electron> *beag_electrons;
                std::vector<beag::Muon> *beag_muons;
                std::vector<beag::MET> *beag_mets;
                std::vector<beag::Trigger> *beag_triggers;
                std::vector<beag::TTbarGenEvent> *beag_gen_evts;

                std::vector<mor::Jet> *jets;
                std::vector<mor::Electron> *electrons;
                std::vector<mor::Muon> *muons;
                std::vector<mor::MET> *mets;

		mor::TTbarGenEvent *gen_evt;
		mor::Trigger *trigger;

		CutSelector *cut_selector;

                TFile *outfile;

		HistoWriter *histo_writer;

                int event_number;
                int max_events;
		
		bool process_mc;

		std::string identifier;
};

#endif
