#ifndef HISTOWRITER_H
#define HISTOWRITER_H

#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"

class HistoWriter{
	public:
		HistoWriter();
		~HistoWriter();
		void set_outfile(TFile *outfile);
		TH1F* create_1d(std::string identifier,
				std::string title,
				int nbins_x, double min_x, double max_x,
				std::string x_title="", std::string y_title="Events");
		TH2F* create_2d(std::string identifier,
				std::string title,
				int nbins_x, double min_x, double max_x,
				int nbins_y, double min_y, double max_y,
				std::string x_title="", std::string y_title="");
	private:
		std::vector<TH1F*> histos1d;
		std::vector<TH2F*> histos2d;

		TFile *outfile;
};

#endif
