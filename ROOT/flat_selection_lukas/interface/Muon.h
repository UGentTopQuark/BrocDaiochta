#ifndef BEAG_MUON_H
#define BEAG_MUON_H

#include "Lepton.h"
#include "Rtypes.h"

namespace beag{
	class Muon : public beag::Lepton{
		public:
			virtual ~Muon(){};

			double chi2;
			double nHits;

		ClassDef(Muon, 1);
	};
}

#endif
