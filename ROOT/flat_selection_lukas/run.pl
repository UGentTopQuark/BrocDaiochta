#!/usr/bin/perl

use threads;
use Getopt::Std;
use Thread::Semaphore;

getopts("j:s:hm:i:c:y");

my @datasets = ();

$run_cfg = 'run.cfg';
die "can't find $run_cfg, exiting..." unless(-e $run_cfg);

do "$run_cfg";

my $cutselector = "src/EventSelection/CutSelector.cc";		# where to find CutSelector.cc
my $flat_sel_out_dir = ".";					# directory where flat_selection writes all outputs to
my $cut_gen_dir = "cutset_generator";				# where is cutset_generator?
my $user_id = `whoami`;
chomp $user_id;
my $computer_id = `hostname`;
chomp $computer_id;
my $log = "$user_id\_$computer_id\_logfile.log";				# specify logfile

$nthreads = $opt_j if(defined($opt_j));

if(defined($opt_c)){
	$cuts_definition = $opt_c;
	$set_cuts = 1;
}

if(defined($opt_h)){
	print <<EOF;
usage: $0 [-h] [-i identifier] [-j <# processes>] [-m <message>] [-s <sample of datasets>] [-c <cutset_definition>] [-y]
	-c	set cuts according to config file (relative to cutset_generator/ directory)
	-h	show this help
	-i	run identifier, on output directory
	-j	number of processes of flat_selection that run in parallel
	-m	message/comment about submission for log-file
	-s	name of dataset sample, eg. AG for alpgen samples
	-y	assume yes on questions
EOF
	exit;
}

$comment = $opt_m if(defined($opt_m));

$identifier = $opt_i if(defined($opt_i));

if(defined($opt_s)){
	if($opt_s eq 'MG'){
		@datasets = ("TTbar"
			     ,"Mupt15"
			     ,"QCDem20to30"
			     ,"QCDem30to80"
			     ,"QCDem80to170"
			     ,"QCDbctoe20to30"
			     ,"QCDbctoe30to80"
			     ,"QCDbctoe80to170"
			     ,"Wjets"
			     ,"Zjets"
			     ,"TsChan"
			     ,"TtChan"
			     ,"TtWChan"
			     );
	}elsif($opt_s eq 'MGMu'){
		@datasets = ("TTbar"
			     ,"Mupt15"
			     ,"Wjets"
			     ,"Zjets"
			     ,"TsChan"
			     ,"TtChan"
			     ,"TtWChan"
			     );
	}elsif($opt_s eq 'MGE'){
		@datasets = ("TTbar"
			     ,"QCDem20to30"
			     ,"QCDem30to80"
			     ,"QCDem80to170"
			     ,"QCDbctoe20to30"
			     ,"QCDbctoe30to80"
			     ,"QCDbctoe80to170"
			     ,"Wjets"
			     ,"Zjets"
			     ,"TsChan"
			     ,"TtChan"
			     ,"TtWChan"
			     );
	}elsif($opt_s eq 'AG'){
		@datasets = (
			     "TTbarA0j"
			     ,"TTbarA1j"
			     ,"TTbarA2j"
			     ,"TTbarA3j"
			     ,"TTbarA4j"
			     );
	}elsif($opt_s eq 'TEST'){
		@datasets = ("TsChan");
	}else{
		die "couldn't find sample: $opt_s";
	}
}else{
	if(@ARGV){
		@datasets = @ARGV;
	}else{
		@datasets = ("TTbar"
			     ,"Mupt15"
			     ,"QCDem20to30"
			     ,"QCDem30to80"
			     ,"QCDem80to170"
			     ,"QCDbctoe20to30"
			     ,"QCDbctoe30to80"
			     ,"QCDbctoe80to170"
			     ,"Wjets"
			     ,"Zjets"
			     ,"TsChan"
			     ,"TtChan"
			     ,"TtWChan"
			     );
	}
}

my $allowed_datasets = ();

$allowed_datasets{"TTbarA0j"} = 1;
$allowed_datasets{"TTbarA1j"} = 1;
$allowed_datasets{"TTbarA2j"} = 1;
$allowed_datasets{"TTbarA3j"} = 1;
$allowed_datasets{"TTbarA4j"} = 1;
$allowed_datasets{"TTbar"} = 1;
$allowed_datasets{"Wjets"} = 1;
$allowed_datasets{"Zjets"} = 1;
$allowed_datasets{"TsChan"} = 1;
$allowed_datasets{"TtChan"} = 1;
$allowed_datasets{"TtWChan"} = 1;
$allowed_datasets{"Mupt15"} = 1;
$allowed_datasets{"QCDem20to30"} = 1;
$allowed_datasets{"QCDem30to80"} = 1;
$allowed_datasets{"QCDem80to170"} = 1;
$allowed_datasets{"QCDbctoe20to30"} = 1;
$allowed_datasets{"QCDbctoe30to80"} = 1;
$allowed_datasets{"QCDbctoe80to170"} = 1;
$allowed_datasets{"TTbarMCatNLO"} = 1;

my $filenames = ();
$filenames{"TsChan"} = "t_s_chan";
$filenames{"TtWChan"} = "t_tW_chan";
$filenames{"TtChan"} = "t_t_chan";
$filenames{"Mupt15"} = "qcdmu15";
$filenames{"TTbarA0j"} = "ttbar_alpg_0jets";
$filenames{"TTbarA1j"} = "ttbar_alpg_1jets";
$filenames{"TTbarA2j"} = "ttbar_alpg_2jets";
$filenames{"TTbarA3j"} = "ttbar_alpg_3jets";
$filenames{"TTbarA4j"} = "ttbar_alpg_4jets";
$filenames{"TTbarPy"} = "ttbar_pythia_wo_preselection";

confirm_datasets(\@datasets, $opt_y);
set_config($cuts_definition, $cut_gen_dir) if($set_cuts);
run_make($nthreads);
run_flat_selection(\@datasets, $nthreads, \@filenames, $datadir);
write_versioning_information($cutselector, $identifier, $output_dir, $flat_sel_out_dir, $cut_gen_dir, $log, $comment);

sub get_datafiles_and_run
{
	my ($datadir, $filenames_ref, $dataset, $semaphore) = @_;

	$semaphore->down();

	my @filenames = @$filenames_ref;

	my $filenames = "";
	opendir(DIR, $datadir);
	while(defined(my $file = readdir(DIR))){
	        next unless($file =~ m/\.root/ && $file !~ m/plots/i);
		my $dataset_filename = $filenames{$dataset};
	       	next if($file !~ m/$dataset(\_1)?\.root/i && (!defined($filenames{$dataset}) ||
			(defined($filenames{$dataset}) && $file !~ m/$dataset_filename(\_1)?\.root/)));
		$filenames .= " $datadir/$file";
	}
	closedir(DIR);
	
	print "datafiles to process: $filenames\n";
	
	system("time ./flat_selection $dataset $filenames | tee $dataset.txt");
	$semaphore->up();
}

sub set_config
{
	my ($config, $cutset_gen_dir) = @_;

	my $cutset_gen_executable = "./change_cuts.pl -c $config";
	
	print "setting cuts according to $config...\n";	
	die "cuts definition file $cutset_gen_dir/$config does not exist, exiting...\n" unless(-e "$cutset_gen_dir/$config"); 
	system ("cd $cutset_gen_dir/ && $cutset_gen_executable && cd -");
}

sub run_make
{
	my ($nthreads) = @_;
	my $make = "make -j $nthreads";
	print "running make...\n";
	system("$make");
}

sub write_versioning_information
{
	my ($cutselector, $identifier, $output_dir, $flat_sel_out_dir, $cut_gen_dir,
		$log, $comment) = @_;

	my $configuration;
	open(CUTSELECTOR, "<$cutselector") or die "can't open CutSelector.cc: $cutselector\n";
	while(defined($line = <CUTSELECTOR>)){
		if($line =~ m/CUTSET_GENERATOR configuration: (.*)/){
			$configuration = $1;
			last;
		}
	}
	close(CUTSELECTOR);

	my $dir_name = create_directory($identifier, $output_dir);

	system("mv $flat_sel_out_dir/*.root $output_dir/$dir_name");
	system("mv $flat_sel_out_dir/*.txt $output_dir/$dir_name");
	system("cp $cut_gen_dir/$configuration $output_dir/$dir_name");
	system("svnversion > $output_dir/$dir_name/svnversion");
	
	$comment = "---" unless($comment);
	my $log_line = "$dir_name\t$configuration\t$comment\n";
	my $heading = "run\t\t\tconfiguration\t\tcomment";
	system("echo '$heading' > $log") unless(-e $log);

	open(LOG, "+< $log") or die "can't open log: $log\n";
	my @log = <LOG>;

	push(@log, $log_line);

        seek(LOG,0,0)                        or die "Seeking: $!";
        print LOG @log                     or die "Printing: $!";
        truncate(LOG,tell(LOG))               or die "Truncating: $!";
        close(LOG)                           or die "Closing: $!";
}

sub create_directory
{
	my ($identifier, $output_dir) = @_;

	my $date = sprintf("%04d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3]);
	my $dir_name = $date."_$identifier";
	$date = sprintf("%04d%02d%02d_%02d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3], (localtime)[2], (localtime)[1], (localtime)[0]) if(-d "$output_dir/$dir_name");
	$dir_name = $date."_$identifier";
	system("mkdir -p $output_dir/$dir_name");

	return $dir_name;
}

sub confirm_datasets
{
	my ($datasets_ref, $confirmation) = @_;
	print "Process the following datasets?\n";
	foreach $dataset (@$datasets_ref){
		print " - $dataset\n";
	}

	my $confirmation = "";
	if(defined($opt_y)){
		$confirmation = "yes";
	}else{
		print "[y/N]: ";
		$confirmation = <STDIN>;
	}
	if($confirmation !~ /y(es)?/i){
		exit;	
	}
}

sub run_flat_selection
{
	my ($dataset_ref, $nthreads, $filenames_ref, $datadir) = @_;
	my @datasets = @$dataset_ref;
	my @threads = ();
	my @commands = ();
	
	my $i=0;
	my $semaphore = Thread::Semaphore->new($nthreads);
	foreach my $dataset (@datasets){
		my $thread = threads->create('get_datafiles_and_run', $datadir, $filenames_ref, $dataset, $semaphore);
		push(@threads, $thread);
	}
	
	foreach my $thread (@threads){
		$thread->join();
	}
}
