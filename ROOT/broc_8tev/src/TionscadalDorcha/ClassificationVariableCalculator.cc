#include "../../interface/TionscadalDorcha/ClassificationVariableCalculator.h"

template double broc::ClassificationVariableCalculator::get_dphilmet(std::vector<mor::Electron> *leptons);
template double broc::ClassificationVariableCalculator::get_detajl(std::vector<mor::Electron> *leptons, std::vector<mor::Jet> *jets);
template double broc::ClassificationVariableCalculator::get_drjl(std::vector<mor::Electron> *leptons, std::vector<mor::Jet> *jets);

template double broc::ClassificationVariableCalculator::get_dphilmet(std::vector<mor::Muon> *leptons);
template double broc::ClassificationVariableCalculator::get_detajl(std::vector<mor::Muon> *leptons, std::vector<mor::Jet> *jets);
template double broc::ClassificationVariableCalculator::get_drjl(std::vector<mor::Muon> *leptons, std::vector<mor::Jet> *jets);

broc::ClassificationVariableCalculator::ClassificationVariableCalculator(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;

	drjl=-1;
	detajl=-1;
}

broc::ClassificationVariableCalculator::~ClassificationVariableCalculator()
{
}

template <class LEPTON>
double broc::ClassificationVariableCalculator::get_detajl(std::vector<LEPTON> *leptons, std::vector<mor::Jet> *jets)
{
	calculate_lepton_jet_distance(leptons, jets);
	return detajl;
}

template <class LEPTON>
double broc::ClassificationVariableCalculator::get_drjl(std::vector<LEPTON> *leptons, std::vector<mor::Jet> *jets)
{
	calculate_lepton_jet_distance(leptons, jets);
	return drjl;
}

template <class LEPTON>
void broc::ClassificationVariableCalculator::calculate_lepton_jet_distance(std::vector<LEPTON> *leptons, std::vector<mor::Jet> *jets)
{
	drjl=-1;
	detajl=-1;
        for(typename std::vector<LEPTON>::iterator lep = leptons->begin();
            lep != leptons->end();
            lep++)
        {
                for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();
                    jet_iter!=jets->end();
                    ++jet_iter)
                {
                	double dR = ROOT::Math::VectorUtil::DeltaR(lep->p4(),jet_iter->p4());
                	if ((dR < drjl) || (drjl = -1))
                	        drjl = dR;
                	double deta = fabs(lep->Eta()-jet_iter->Eta());
                	if ((deta < detajl) || (detajl == -1))
                		detajl = deta;
                }
        }
}

template <class LEPTON>
double broc::ClassificationVariableCalculator::get_dphilmet(std::vector<LEPTON> *leptons)
{
	std::vector<mor::MET> *mets = handle_holder->get_selected_mets();

	double dphilmet = -1;
	if(leptons->size() > 0){
		dphilmet = ROOT::Math::VectorUtil::DeltaPhi(leptons->begin()->p4(), mets->begin()->p4());
	}

	return dphilmet;
}
