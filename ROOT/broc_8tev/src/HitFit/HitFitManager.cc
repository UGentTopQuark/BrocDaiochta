#include "../../interface/HitFit/HitFitManager.h"


hitfit::HitFitManager::HitFitManager(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
  eire::ConfigReader *config_reader = handle_holder->get_config_reader();
  std::string output_dir = config_reader->get_var("output_directory");
  std::string suffix = config_reader->get_var("outfile_suffix");
  hitfit_producer = NULL;
  ntuple_file = NULL;

  std::string filename = output_dir+"/"+"HitFit_"+handle_holder->get_ident()+suffix+".root";
  while(filename.find("|") != std::string::npos)
	  filename.replace(filename.find("|"),1,"_");
  
  std::string directory_name = "HitFit";
  hitfit_producer = new hitfit::HitFitProducer(handle_holder);	  
  
  ntuple_file = new TFile(filename.c_str(), "RECREATE");
  if(directory_name != ""){
	  ntuple_file->mkdir(directory_name.c_str());
	  ntuple_file->Cd(directory_name.c_str());
  }

  
  hitfit_producer->set_outfile(ntuple_file, directory_name);
  hitfit_producer->create_tree("HitFitResults");
  hitfit_producer->set_handles(handle_holder); 
  hitfit_producer->book_branches();

}

hitfit::HitFitManager::~HitFitManager()
{
	if(hitfit_producer){ delete hitfit_producer; hitfit_producer = NULL; }

        if(ntuple_file){
	  ntuple_file->Write();
	  ntuple_file->Close();
	  delete ntuple_file;
	  ntuple_file = NULL;
        }

}

void hitfit::HitFitManager::fill_trees()
{
	hitfit_producer->fill_branches();  
}
