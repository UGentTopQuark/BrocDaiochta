#include "../../interface/HitFit/HitFitProducer.h"

using namespace hitfit;
const Int_t HitFitProducer::maxNCombStatic;
HitFitProducer::HitFitProducer(eire::HandleHolder *handle_holder)
{
	
	eire::ConfigReader *config_reader = handle_holder->get_config_reader();
	eire::ConfigReader *hitfit_config_reader = new eire::ConfigReader();
	std::string hitfit_config_file = config_reader->get_var("hitfit_config_file", "global", false);
	hitfit_config_reader->read_config_from_file(hitfit_config_file, true);
	
	this->gen_evt = handle_holder->get_ttbar_gen_evt();
	this->handle_holder = handle_holder;
	this->event_information = handle_holder->get_event_information();

	// Configuration parameters taken from specified hitfit_config_file
	// This is taken from the default config of the broc

	maxNJets_             =  atoi(hitfit_config_reader->get_var("maxNJets", "mass_studies", false).c_str()); // int   //4
	maxNComb_             =  atoi(hitfit_config_reader->get_var("maxNComb", "mass_studies",false).c_str()); // int   //1
	bTagAlgo_             =  hitfit_config_reader->get_var("bTagAlgo","mass_studies", false);  // string
	minBTagValueBJet_     =  atof(hitfit_config_reader->get_var("minBDiscBJets","mass_studies", false).c_str()); // double
	maxBTagValueNonBJet_  =  atof(hitfit_config_reader->get_var("maxBDiscLightJets","mass_studies", false).c_str()); //double
	useBTag_              =  hitfit_config_reader->get_bool_var("useBTagging","mass_studies", false); // bool // false
	mW_                   =  atof(hitfit_config_reader->get_var("mW","mass_studies", false).c_str()); // double // 80.4
	mTop_                 =  atof(hitfit_config_reader->get_var("mTop","mass_studies", false).c_str()); // double // 172
	jetCorrectionLevel_   =  hitfit_config_reader->get_var("jetCorrectionLevel","mass_studies", false); // string  // L3
	jes_                  =  atof(hitfit_config_reader->get_var("jes","mass_studies", false).c_str()); // double  // 1? //
	jesB_                 =  atof(hitfit_config_reader->get_var("jesB","mass_studies", false).c_str()); // double // 1? //
                                                                                                                                                                                        

	hitfitDefault_ =  hitfit_config_reader->get_var("hitfitDefault","mass_studies", false);
	hitfitElectronResolution_ = hitfit_config_reader->get_var("hitfitElectronResolution","mass_studies", false);
	hitfitMuonResolution_ = hitfit_config_reader->get_var("hitfitMuonResolution","mass_studies", false);
	hitfitUdscJetResolution_ = hitfit_config_reader->get_var("hitfitUdscResolution","mass_studies", false);
	hitfitBJetResolution_ = hitfit_config_reader->get_var("hitfitBJetResolution","mass_studies", false);
	hitfitMETResolution_ = hitfit_config_reader->get_var("hitfitMETResolution","mass_studies", false);


	// The following four initializers instantiate the translator between PAT objects                                                                                               
	// and HitFit objects using the ASCII text files which contains the resolutions.                                                                                                
                                                                                                                                                                                           
	electronTranslator_ = hitfit::LeptonTranslatorBase<mor::Electron>(hitfitElectronResolution_);
	muonTranslator_     = hitfit::LeptonTranslatorBase<mor::Muon>(hitfitMuonResolution_);
	jetTranslator_      = hitfit::JetTranslatorBase<mor::Jet>(hitfitUdscJetResolution_, hitfitBJetResolution_, jetCorrectionLevel_, jes_, jesB_);
	metTranslator_      = hitfit::METTranslatorBase<mor::MET>(hitfitMETResolution_);

	// Declare which jets/leptons to use	

	jets = handle_holder->get_tight_jets();
	muons = handle_holder->get_tight_muons();
	electrons = handle_holder->get_tight_electrons();
	mets = handle_holder->get_mets();
	gen_evt = handle_holder->get_ttbar_gen_evt();

	// Create an instance of RunHitFit and initialize it.
                
	HitFit = new BrocHitFit(electronTranslator_,
				       muonTranslator_,
				       jetTranslator_,
				       metTranslator_,
				       hitfitDefault_,
				       mW_,
				       mW_,
				       mTop_);

	// Default hitfit cuts on jets (in addition to broc selection)
	
	maxEtaMu_  = 2.4;
	maxEtaEle_ = 2.5;
	maxEtaJet_ = 3.0;
	

	std::cout << std::endl;
	std::cout << "+++++++++++++     HitFitProducer    ++++++++++++" << std::endl;
	std::cout << " the following additional cuts are applied on   " << std::endl; 
	std::cout << " selected leptons and jets by the HitFitProducer" << std::endl; 
	std::cout << " |eta(muons    )| <= " << maxEtaMu_  << "       " << std::endl;
	std::cout << " |eta(electrons)| <= " << maxEtaEle_ << "       " << std::endl;
	std::cout << " |eta(jets     )| <= " << maxEtaJet_ << "       " << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

}


HitFitProducer::~HitFitProducer()
{
}

void HitFitProducer::book_branches()
{
	// Book the branches inside the tree (defined in the HitFitManager)

	tree->Branch("nSol",&fitResult.nSol, "nSol/i");
	tree->Branch("maxSol",&maxNComb_, "maxNComb_/i");

	tree->Branch("HadP_pt",fitResult.HadP_pt, "HadP_pt[maxNComb_]/D");
	tree->Branch("HadP_eta",fitResult.HadP_eta, "HadP_eta[maxNComb_]/D");
	tree->Branch("HadP_phi",fitResult.HadP_phi, "HadP_phi[maxNComb_]/D");
	tree->Branch("HadP_m",fitResult.HadP_m, "HadP_m[maxNComb_]/D");

	tree->Branch("HadB_pt",fitResult.HadB_pt, "HadB_pt[maxNComb_]/D");
	tree->Branch("HadB_eta",fitResult.HadB_eta, "HadB_eta[maxNComb_]/D");
	tree->Branch("HadB_phi",fitResult.HadB_phi, "HadB_phi[maxNComb_]/D");
	tree->Branch("HadB_m",fitResult.HadB_m, "HadB_m[maxNComb_]/D");

	tree->Branch("HadQ_pt",fitResult.HadQ_pt, "HadQ_pt[maxNComb_]/D");
	tree->Branch("HadQ_eta",fitResult.HadQ_eta, "HadQ_eta[maxNComb_]/D");
	tree->Branch("HadQ_phi",fitResult.HadQ_phi, "HadQ_phi[maxNComb_]/D");
	tree->Branch("HadQ_m",fitResult.HadQ_m, "HadQ_m[maxNComb_]/D");

	tree->Branch("LepB_pt",fitResult.LepB_pt, "LepB_pt[maxNComb_]/D");
	tree->Branch("LepB_eta",fitResult.LepB_eta, "LepB_eta[maxNComb_]/D");
	tree->Branch("LepB_phi",fitResult.LepB_phi, "LepB_phi[maxNComb_]/D");
	tree->Branch("LepB_m",fitResult.LepB_m, "LepB_m[maxNComb_]/D");

	tree->Branch("LepL_pt",fitResult.LepL_pt, "LepL_pt[maxNComb_]/D");
	tree->Branch("LepL_eta",fitResult.LepL_eta, "LepL_eta[maxNComb_]/D");
	tree->Branch("LepL_phi",fitResult.LepL_phi, "LepL_phi[maxNComb_]/D");
	tree->Branch("LepL_m",fitResult.LepL_m, "LepL_m[maxNComb_]/D");

	tree->Branch("LepN_pt",fitResult.LepN_pt, "LepN_pt[maxNComb_]/D");
	tree->Branch("LepN_eta",fitResult.LepN_eta, "LepN_eta[maxNComb_]/D");
	tree->Branch("LepN_phi",fitResult.LepN_phi, "LepN_phi[maxNComb_]/D");
	tree->Branch("LepN_m",fitResult.LepN_m, "LepN_m[maxNComb_]/D");

	tree->Branch("Combi",fitResult.Combi, "*Combi[4][maxNComb_]/I");
	tree->Branch("Chi2",fitResult.Chi2, "Chi2[maxNComb_]/D");
	tree->Branch("Prob",fitResult.Prob, "Prob[maxNComb_]/D");
	tree->Branch("MT",fitResult.MT, "MT[maxNComb_]/D");
	tree->Branch("SigMT",fitResult.SigMT, "SigMT[maxNComb_]/D");
	tree->Branch("Status",fitResult.Status, "Status[maxNComb_]/I");

	tree->Branch("mc_trueCombi",trueCombi, "trueCombi[4]/I");
	tree->Branch("mc_JetPermutation_match",fitResult.matched, "matched[maxNComb_]/I");
	
	tree->Branch("mc_HadB_pt",&mc_hadB_pt, "mc_hadB_pt/D");
	tree->Branch("mc_HadB_eta",&mc_hadB_eta, "mc_hadB_eta/D");
	tree->Branch("mc_HadB_phi",&mc_hadB_phi, "mc_hadB_phi/D");

	tree->Branch("mc_LepB_pt",&mc_lepB_pt, "mc_lepB_pt/D");
	tree->Branch("mc_LepB_eta",&mc_lepB_eta, "mc_lepB_eta/D");
	tree->Branch("mc_LepB_phi",&mc_lepB_phi, "mc_lepB_phi/D");

	tree->Branch("mc_Q_pt",&mc_q_pt, "mc_q_pt/D");
	tree->Branch("mc_Q_eta",&mc_q_eta, "mc_q_eta/D");
	tree->Branch("mc_Q_phi",&mc_q_phi, "mc_q_phi/D");

	tree->Branch("mc_QBar_pt",&mc_qbar_pt, "mc_qbar_pt/D");
	tree->Branch("mc_QBar_eta",&mc_qbar_eta, "mc_qbar_eta/D");
	tree->Branch("mc_QBar_phi",&mc_qbar_phi, "mc_qbar_phi/D");
	
}


void HitFitProducer::fill_branches()
{

  // Initial selection: Skip events with no appropriate 
  // lepton candidate or empty MET or less jets than partons
 
  const unsigned int nPartons = 4;


  // Clear the internal state
  HitFit->clear();

  // Add lepton into HitFit
  bool foundLepton = false;

  if(!muons->empty()){
	  for(unsigned iLep=0; iLep<(*muons).size() && !foundLepton; ++iLep) {
		  if(std::abs((*muons)[iLep].eta()) <= maxEtaMu_) {
			  mor::Muon muon(*(dynamic_cast<const mor::Muon*>(&((*muons)[0]))));
			  HitFit->AddLepton(muon);
			  foundLepton = true;
		  }
	  }
  }
  else if(!electrons->empty()){
	  for(unsigned iLep=0; iLep<(*electrons).size() && !foundLepton; ++iLep) {
		  if(std::abs((*electrons)[iLep].eta()) <= maxEtaEle_) {
			  mor::Electron electron(*(dynamic_cast<const mor::Electron*>(&((*electrons)[0]))));
			  HitFit->AddLepton(electron);
			  foundLepton = true;
		  }
	  }
  }

  // Add jets into HitFit
  int nJetsFound = 0;
  for(unsigned iJet=0; iJet<(*jets).size() && nJetsFound!=maxNJets_; ++iJet) {
    if(std::abs((*jets)[iJet].eta()) <= maxEtaJet_) {
      HitFit->AddJet((*jets)[iJet]);
      nJetsFound++;
    }
  }

  // Add missing transverse energy into HitFit
  if(!mets->empty())
    HitFit->SetMet((*mets)[0]);

  
  if( !foundLepton || mets->empty() || (unsigned)nJetsFound<nPartons ) {
	  for(Int_t i=0; i < maxNComb_; ++i) {
		  fitResult.HadB_pt[i] = -1;
		  fitResult.HadB_eta[i] = -1;
		  fitResult.HadB_phi[i] = -1;
		  fitResult.HadB_m[i] = -1;
		  
		  fitResult.HadP_pt[i] = -1;
		  fitResult.HadP_eta[i] = -1;
		  fitResult.HadP_phi[i] = -1;
		  fitResult.HadP_m[i] = -1;
		  
		  fitResult.HadQ_pt[i] = -1;
		  fitResult.HadQ_eta[i] = -1;
		  fitResult.HadQ_phi[i] = -1;
		  fitResult.HadQ_m[i] = -1;
		  
		  fitResult.LepB_pt[i] = -1;
		  fitResult.LepB_eta[i] = -1;
		  fitResult.LepB_phi[i] = -1;
		  fitResult.LepB_m[i] = -1;
		  
		  fitResult.LepL_pt[i] = -1;
		  fitResult.LepL_eta[i] = -1;
		  fitResult.LepL_phi[i] = -1;
		  fitResult.LepL_m[i] = -1;
		  
		  fitResult.LepN_pt[i] = -1;
		  fitResult.LepN_eta[i] = -1;
		  fitResult.LepN_phi[i] = -1;
		  fitResult.LepN_m[i] = -1;
		  
		  fitResult.MT[i] = -1;
		  fitResult.SigMT[i] = -1;
		  fitResult.Prob[i] = -1;
		  fitResult.Chi2[i] = -1;
		  fitResult.Status[i] = -1;

		  fitResult.matched[i] = -1;
	  }
	  tree->Fill();
   }

   
  std::list<FitResult> FitResultList;
 
  //
  // BEGIN DECLARATION OF VARIABLES FROM KINEMATIC FIT
  //

  // In this part are variables from the
  // kinematic fit procedure

  // Number of all permutations of the event
  size_t nHitFit    = 0 ;

  // Number of jets in the event
  size_t nHitFitJet = 0 ;

  // Results of the fit for all jet permutations of the event
  std::vector<Fit_Result> hitFitResult;

  //
  // R U N   H I T F I T
  //
  // Run the kinematic fit and get how many permutations are possible
  // in the fit

  nHitFit         = HitFit->FitAllPermutation();
  
  //
  // BEGIN PART WHICH EXTRACTS INFORMATION FROM HITFIT
  //

  // Get the number of jets
  nHitFitJet = HitFit->GetUnfittedEvent()[0].njets();

  // Get the fit results for all permutations
  hitFitResult = HitFit->GetFitAllPermutation();

  // Loop over all permutations and extract the information
  for (size_t fit = 0 ; fit != nHitFit ; ++fit) {

      // Get the event after the fit
      Lepjets_Event fittedEvent = hitFitResult[fit].ev();
      
      /*
        Get jet permutation according to TQAF convention
        11 : leptonic b
        12 : hadronic b
        13 : hadronic W
        14 : hadronic W
      */
      std::vector<int> hitCombi(4);
      for (size_t jet = 0 ; jet != nHitFitJet ; ++jet) {
          int jet_type = fittedEvent.jet(jet).type();
          
          switch(jet_type) {
            case 11: hitCombi[LepB     ] = jet;
              break;
            case 12: hitCombi[HadB     ] = jet;
              break;
            case 13: hitCombi[LightQ   ] = jet;
              break;
            case 14: hitCombi[LightQBar] = jet;
              break;
          }
      }

      // Store the kinematic quantities in the corresponding containers.
     
      Lepjets_Event_Jet hadP_ = fittedEvent.jet(hitCombi[hitfit::LightQ   ]);
      Lepjets_Event_Jet hadQ_ = fittedEvent.jet(hitCombi[hitfit::LightQBar]);
      Lepjets_Event_Jet hadB_ = fittedEvent.jet(hitCombi[hitfit::HadB     ]);
      Lepjets_Event_Jet lepB_ = fittedEvent.jet(hitCombi[hitfit::LepB     ]);
      Lepjets_Event_Lep lepL_ = fittedEvent.lep(0);

      // Current b-tag selection is hard-coded to look at the CSV algo only, using the 0.679 working point.
       
      if (   hitFitResult[fit].chisq() > 0    // only take into account converged fits 
          && (!useBTag_ || (   useBTag_       // use btag information if chosen
			    && jets->at(hitCombi[hitfit::LightQ   ]).bDiscriminator("combinedSecondaryVertexBJetTags") < 0.679 // find label for b-tagged jet in broc
                            && jets->at(hitCombi[hitfit::LightQBar]).bDiscriminator("combinedSecondaryVertexBJetTags") < 0.679
			       && ( jets->at(hitCombi[hitfit::HadB     ]).bDiscriminator("combinedSecondaryVertexBJetTags") >= 0.679
                            || jets->at(hitCombi[hitfit::LepB     ]).bDiscriminator("combinedSecondaryVertexBJetTags") >= 0.679
			       ))
	      )
	      ) { 
	      /*
      std::cout << "Q " << jets->at(hitCombi[hitfit::LightQ   ]).bDiscriminator("combinedSecondaryVertexBJetTags") << std::endl;
      std::cout << "QBar " << jets->at(hitCombi[hitfit::LightQBar]).bDiscriminator("combinedSecondaryVertexBJetTags") << std::endl;
      std::cout << "HadB " << jets->at(hitCombi[hitfit::HadB     ]).bDiscriminator("combinedSecondaryVertexBJetTags") << std::endl;
      std::cout << "LepB " << jets->at(hitCombi[hitfit::LepB     ]).bDiscriminator("combinedSecondaryVertexBJetTags")  << std::endl;

      // Previous HitFit code that selects on the b-tagging algo's bDiscriminator

      if (   hitFitResult[fit].chisq() > 0    // only take into account converged fits 
          && (!useBTag_ || (   useBTag_       // use btag information if chosen
			    && jets->at(hitCombi[hitfit::LightQ   ]).bDiscriminator(bTagAlgo_) < maxBTagValueNonBJet_ // find label for b-tagged jet in broc
                            && jets->at(hitCombi[hitfit::LightQBar]).bDiscriminator(bTagAlgo_) < maxBTagValueNonBJet_
                            && jets->at(hitCombi[hitfit::HadB     ]).bDiscriminator(bTagAlgo_) > minBTagValueBJet_
                            && jets->at(hitCombi[hitfit::LepB     ]).bDiscriminator(bTagAlgo_) > minBTagValueBJet_
			       )
	      )
	      ) { 
	      */

	      FitResult result;
	      result.Status = 0;
	      result.Chi2 = hitFitResult[fit].chisq();
	      result.Prob = exp(-1.0*(hitFitResult[fit].chisq())/2.0);
	      result.MT   = hitFitResult[fit].mt();
	      result.SigMT= hitFitResult[fit].sigmt();
	      result.HadB = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(hadB_.p().perp(),hadB_.p().pseudoRapidity(),hadB_.p().phi(),hadB_.p().m()));
	      result.HadP = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(hadP_.p().perp(),hadP_.p().pseudoRapidity(),hadP_.p().phi(),hadP_.p().m()));
	      result.HadQ = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(hadQ_.p().perp(),hadQ_.p().pseudoRapidity(),hadQ_.p().phi(),hadQ_.p().m()));
	      result.LepB = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(lepB_.p().perp(),lepB_.p().pseudoRapidity(),lepB_.p().phi(),lepB_.p().m()));
	      result.LepL = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(lepL_.p().perp(),lepL_.p().pseudoRapidity(),lepL_.p().phi(),lepL_.p().m()));
	      result.LepN = mor::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >(fittedEvent.met().perp(), fittedEvent.met().pseudoRapidity(),
												      fittedEvent.met().phi(), fittedEvent.met().m()));
	      result.JetCombi = hitCombi;
	      
	      FitResultList.push_back(result);
      }

  }


  // sort results w.r.t. chi2 values
  FitResultList.sort();
  
  // -----------------------------------------------------
  // feed out result
  // starting with the JetComb having the smallest chi2
  // -----------------------------------------------------

  fitResult.nSol = 0;

  // Set the default of the trueCombi array entires to be -1 (needs fixing)

  for (unsigned int i=0; i<4; ++i) {
       trueCombi[i] = -1;
   }

  // Fill the quark information from the TTBarGenEvent MC class
 
  mc_hadB_pt = ((*gen_evt).hadB()->p4().pt());
  mc_lepB_pt = ((*gen_evt).lepB()->p4().pt());
  mc_q_pt = ((*gen_evt).q()->p4().pt());
  mc_qbar_pt = ((*gen_evt).qbar()->p4().pt());

  mc_hadB_eta = ((*gen_evt).hadB()->p4().eta());
  mc_lepB_eta = ((*gen_evt).lepB()->p4().eta());
  mc_q_eta = ((*gen_evt).q()->p4().eta());
  mc_qbar_eta = ((*gen_evt).qbar()->p4().eta());

  mc_hadB_phi = ((*gen_evt).hadB()->p4().phi());
  mc_lepB_phi = ((*gen_evt).lepB()->p4().phi());
  mc_q_phi = ((*gen_evt).q()->p4().phi());
  mc_qbar_phi = ((*gen_evt).qbar()->p4().phi());
 

  // Fill the trueCombi array with the MC information inside the jet class

  for (unsigned int i=0; i<(*jets).size(); ++i) {
		  int jetRole((*jets).at(i).ttbar_decay_product());
		  switch (jetRole) {
		          case 1:
				  trueCombi[LightQ] = (i);
				  break;
		          case 2:
				  trueCombi[LightQBar] = (i);
				  break;
		          case 3:
				  trueCombi[HadB] = (i);
				  break;
		          case 4:
				  trueCombi[LepB] = (i);
				  break;
	                  default:
				  break;
		  }
		  
  }

  // Set the branches equal to -1 if the fit failed for the event, fill with fitted values otherwise.

  if( ((unsigned)FitResultList.size())<1 ) { // in case no fit results were stored in the list (all fits aborted)
	  if(!useBTag_){
		  std::cout << " No fit solutions found for event! " << std::endl;
	  }
	  for(Int_t i=0; i < maxNComb_; ++i) {
		  fitResult.HadB_pt[i] = -1;
		  fitResult.HadB_eta[i] = -1;
		  fitResult.HadB_phi[i] = -1;
		  fitResult.HadB_m[i] = -1;
		  
		  fitResult.HadP_pt[i] = -1;
		  fitResult.HadP_eta[i] = -1;
		  fitResult.HadP_phi[i] = -1;
		  fitResult.HadP_m[i] = -1;
		  
		  fitResult.HadQ_pt[i] = -1;
		  fitResult.HadQ_eta[i] = -1;
		  fitResult.HadQ_phi[i] = -1;
		  fitResult.HadQ_m[i] = -1;
		  
		  fitResult.LepB_pt[i] = -1;
		  fitResult.LepB_eta[i] = -1;
		  fitResult.LepB_phi[i] = -1;
		  fitResult.LepB_m[i] = -1;
		  
		  fitResult.LepL_pt[i] = -1;
		  fitResult.LepL_eta[i] = -1;
		  fitResult.LepL_phi[i] = -1;
		  fitResult.LepL_m[i] = -1;
		  
		  fitResult.LepN_pt[i] = -1;
		  fitResult.LepN_eta[i] = -1;
		  fitResult.LepN_phi[i] = -1;
		  fitResult.LepN_m[i] = -1;
		  
		  fitResult.MT[i] = -1;
		  fitResult.SigMT[i] = -1;
		  fitResult.Prob[i] = -1;
		  fitResult.Chi2[i] = -1;
		  fitResult.Status[i] = -1;

		  fitResult.matched[i] = -1;
	  }	  
  }
  else{
	  unsigned int i = 0;
	  for(std::list<FitResult>::const_iterator result = FitResultList.begin(); result != FitResultList.end(); ++result) {
		  if(fitResult.nSol >=  maxNComb_) break;
		  fitResult.nSol++;
		  
		  fitResult.HadB_pt[i] = result->HadB.pt();
		  fitResult.HadB_eta[i] = result->HadB.eta();
		  fitResult.HadB_phi[i] = result->HadB.phi();
		  fitResult.HadB_m[i] = result->HadB.mt();
		  
		  fitResult.HadP_pt[i] = result->HadP.pt();
		  fitResult.HadP_eta[i] = result->HadP.eta();
		  fitResult.HadP_phi[i] = result->HadP.phi();
		  fitResult.HadP_m[i] = result->HadP.mt();
		  
		  fitResult.HadQ_pt[i] = result->HadQ.pt();
		  fitResult.HadQ_eta[i] = result->HadQ.eta();
		  fitResult.HadQ_phi[i] = result->HadQ.phi();
		  fitResult.HadQ_m[i] = result->HadQ.mt();
		  
		  fitResult.LepB_pt[i] = result->LepB.pt();
		  fitResult.LepB_eta[i] = result->LepB.eta();
		  fitResult.LepB_phi[i] = result->LepB.phi();
		  fitResult.LepB_m[i] = result->LepB.mt();
		  
		  fitResult.LepL_pt[i] = result->LepL.pt();
		  fitResult.LepL_eta[i] = result->LepL.eta();
		  fitResult.LepL_phi[i] = result->LepL.phi();
		  fitResult.LepL_m[i] = result->LepL.mt();
		  
		  fitResult.LepN_pt[i] = result->LepN.pt();
		  fitResult.LepN_eta[i] = result->LepN.eta();
		  fitResult.LepN_phi[i] = result->LepN.phi();
		  fitResult.LepN_m[i] = result->LepN.mt();
		  
		  fitResult.MT[i] = result->MT;
		  fitResult.SigMT[i] = result->SigMT;
		  fitResult.Prob[i] = result->Prob;
		  fitResult.Chi2[i] = result->Chi2;
		  fitResult.Status[i] = result->Status;
		  
		  for (unsigned int j=0; j<result->JetCombi.size(); ++j ){
			  fitResult.Combi[j][i] = result->JetCombi.at(j);
		  }

		  fitResult.matched[i] = 0;
		  
		  // Check to see if parton matching was successful for the event
		  if((trueCombi[0] == fitResult.Combi[0][i] || trueCombi[0] == fitResult.Combi[1][i]) && (trueCombi[1] == fitResult.Combi[1][i] || trueCombi[1] == fitResult.Combi[0][i] ) && trueCombi[2] == fitResult.Combi[2][i] && trueCombi[3] == fitResult.Combi[3][i] ){
			  fitResult.matched[i] = 1; //matched;
		  } ++i;

	  }// for(std::list<FitResult>::const_iterator result = FitResultList.begin(); result != FitResultList.end(); ++result) 
  }
  
  tree->Fill();
}
  

