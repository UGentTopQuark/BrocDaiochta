 //
// $Id: MorElectronHitFitTranslator.cc,v 1.8 2010/08/06 22:02:52 haryo Exp $
//

/**
    @file MorElectronHitFitTranslator.cc

    @brief Specialization of template class LeptonTranslatorBase in the
    package HitFit for mor::Electron.

    @author Haryo Sumowidagdo <Suharyo.Sumowidagdo@cern.ch>

    @par Created
    Sat Jun 27 17:49:06 2009 UTC

    @version $Id: MorElectronHitFitTranslator.cc,v 1.8 2010/08/06 22:02:52 haryo Exp $
 */


#include "../../../interface/HitFit/Core/LeptonTranslatorBase.hpp"
#include "../../../interface/MorObjects/MElectron.h"

namespace hitfit {

template<>
LeptonTranslatorBase<mor::Electron>::LeptonTranslatorBase()
{

} // LeptonTranslatorBase<mor::Electron>::LeptonTranslatorBase()


template<>
LeptonTranslatorBase<mor::Electron>::LeptonTranslatorBase(const std::string& ifile)
{

    std::string resolution_filename;

    if (ifile.empty()) {
	    std::cout << " No resolution function found for electrons!" << std::endl;
    } else {
        resolution_filename = ifile ;
    }

    resolution_ = EtaDepResolution(resolution_filename);

} // LeptonTranslatorBase<mor::Electron>::LeptonTranslatorBase(const std::string& ifile)


template<>
LeptonTranslatorBase<mor::Electron>::~LeptonTranslatorBase()
{
}


template<>
Lepjets_Event_Lep
LeptonTranslatorBase<mor::Electron>::operator()(const mor::Electron& lepton,
                                                int type /* = hitfit::lepton_label */,
                                                bool useObjEmbRes /* = false */)
{

    Fourvec p(lepton.px(),lepton.py(),lepton.pz(),lepton.energy());

    //double            electron_eta  =  lepton.supercluster_eta();
    double            electron_eta  =  (*(const_cast<mor::Electron*>(&lepton))).supercluster_eta();
    Vector_Resolution electron_resolution = resolution_.GetResolution(electron_eta);

    Lepjets_Event_Lep electron(p,
                               electron_label,
                               electron_resolution);
    return electron;

} // Lepjets_Event_Lep LeptonTranslatorBase<mor::Electron>::operator()


template<>
const EtaDepResolution&
LeptonTranslatorBase<mor::Electron>::resolution() const
{
    return resolution_;
}


template<>
bool
LeptonTranslatorBase<mor::Electron>::CheckEta(const mor::Electron& lepton) const
{
    double            electron_eta        = (*(const_cast<mor::Electron*>(&lepton))).supercluster_eta();
    return resolution_.CheckEta(electron_eta);
}


} // namespace hitfit
