//
//     $Id: PatMuonHitFitTranslator.cc,v 1.8 2010/08/06 22:03:03 haryo Exp $
//
/**
    @file PatMuonHitFitTranslator.cc

    @brief Specialization of template class LeptonTranslatorBase in the
    package HitFit for pat::Muon.

    @author Haryo Sumowidagdo <Suharyo.Sumowidagdo@cern.ch>

    @par Created
    Sat Jun 27 17:49:15 2009 UTC

    @version $Id: PatMuonHitFitTranslator.cc,v 1.8 2010/08/06 22:03:03 haryo Exp $
 */


#include "../../../interface/HitFit/Core/LeptonTranslatorBase.hpp"
#include "../../../interface/MorObjects/MMuon.h"

namespace hitfit {


template<>
LeptonTranslatorBase<mor::Muon>::LeptonTranslatorBase()
{
}

template<>
LeptonTranslatorBase<mor::Muon>::LeptonTranslatorBase(const std::string& ifile)
{
	std::string resolution_filename;
    if (ifile.empty()) {
	    std::cout << " No resolution function found for muons!" << std::endl;
    } else {
        resolution_filename = ifile ;
    }

    resolution_ = EtaDepResolution(resolution_filename);

}


template<>
LeptonTranslatorBase<mor::Muon>::~LeptonTranslatorBase()
{
}


template<>
Lepjets_Event_Lep
LeptonTranslatorBase<mor::Muon>::operator()(const mor::Muon& lepton,
                                            int type /*= hitfit::lepton_label */,
                                            bool useObjEmbRes /* = false */)
{

    Fourvec p(lepton.px(),lepton.py(),lepton.pz(),lepton.energy());

    double            muon_eta        = lepton.eta();
    Vector_Resolution muon_resolution = resolution_.GetResolution(muon_eta);

    Lepjets_Event_Lep muon(p,
                           muon_label,
                           muon_resolution);
    return muon;

}


template<>
const EtaDepResolution&
LeptonTranslatorBase<mor::Muon>::resolution() const
{
    return resolution_;
}


template<>
bool
LeptonTranslatorBase<mor::Muon>::CheckEta(const mor::Muon& lepton) const
{
    return resolution_.CheckEta(lepton.eta());
}


}
