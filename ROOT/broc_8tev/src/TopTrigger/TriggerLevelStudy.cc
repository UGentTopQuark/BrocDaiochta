#include "../../interface/TopTrigger/TriggerLevelStudy.h"

template void truicear::TriggerLevelStudy<mor::Muon, mor::TriggerObject, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::plot();
template void truicear::TriggerLevelStudy<mor::Muon, mor::TriggerObject, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::set_handles(std::vector<mor::Muon> *leptons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<mor::Jet> *jets, mor::EventInformation *event_information);
template void truicear::TriggerLevelStudy<mor::Muon, mor::TriggerObject, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::set_histo_writer(HistoWriter *histo_writer);
template void truicear::TriggerLevelStudy<mor::Muon, mor::TriggerObject, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::set_matching_criteria(truicear::MatchingCriteria *matching_criteria);
template truicear::TriggerLevelStudy<mor::Muon, mor::TriggerObject, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::TriggerLevelStudy(std::string id, std::string trigger_name, std::string trigger_level,std::string l1seed_name);
template truicear::TriggerLevelStudy<mor::Muon, mor::TriggerObject, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::~TriggerLevelStudy();
template std::vector<mor::TriggerObject>* truicear::TriggerLevelStudy<mor::Muon, mor::TriggerObject, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::get_matched_trigger_objects();
template std::vector<mor::Muon>* truicear::TriggerLevelStudy<mor::Muon, mor::TriggerObject, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::get_matched_reco_objects();

template void truicear::TriggerLevelStudy<mor::Electron, mor::TriggerObject, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::plot();
template void truicear::TriggerLevelStudy<mor::Electron, mor::TriggerObject, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::set_handles(std::vector<mor::Electron> *leptons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<mor::Jet> *jets, mor::EventInformation *event_information);
template void truicear::TriggerLevelStudy<mor::Electron, mor::TriggerObject, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::set_histo_writer(HistoWriter *histo_writer);
template void truicear::TriggerLevelStudy<mor::Electron, mor::TriggerObject, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::set_matching_criteria(truicear::MatchingCriteria *matching_criteria);
template truicear::TriggerLevelStudy<mor::Electron, mor::TriggerObject, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::TriggerLevelStudy(std::string id, std::string trigger_name, std::string trigger_level,std::string l1seed_name);
template truicear::TriggerLevelStudy<mor::Electron, mor::TriggerObject, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::~TriggerLevelStudy();
template std::vector<mor::TriggerObject>* truicear::TriggerLevelStudy<mor::Electron, mor::TriggerObject, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::get_matched_trigger_objects();
template std::vector<mor::Electron>* truicear::TriggerLevelStudy<mor::Electron, mor::TriggerObject, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::get_matched_reco_objects();

//template void truicear::TriggerLevelStudy<mor::TriggerObject, mor::TriggerObject, truicear::BasicTriggerObjectPlots, truicear::MatchedTriggerObjectPlots>::plot();
template void truicear::TriggerLevelStudy<mor::TriggerObject, mor::TriggerObject, truicear::BasicTriggerObjectPlots, truicear::MatchedTriggerObjectPlots>::set_handles(std::vector<mor::TriggerObject> *leptons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<mor::Jet> *jets, mor::EventInformation *event_information);
template void truicear::TriggerLevelStudy<mor::TriggerObject, mor::TriggerObject, truicear::BasicTriggerObjectPlots, truicear::MatchedTriggerObjectPlots>::set_histo_writer(HistoWriter *histo_writer);
template void truicear::TriggerLevelStudy<mor::TriggerObject, mor::TriggerObject, truicear::BasicTriggerObjectPlots, truicear::MatchedTriggerObjectPlots>::set_matching_criteria(truicear::MatchingCriteria *matching_criteria);
template truicear::TriggerLevelStudy<mor::TriggerObject, mor::TriggerObject, truicear::BasicTriggerObjectPlots, truicear::MatchedTriggerObjectPlots>::TriggerLevelStudy(std::string id, std::string trigger_name, std::string trigger_level,std::string l1seed_name);
template truicear::TriggerLevelStudy<mor::TriggerObject, mor::TriggerObject, truicear::BasicTriggerObjectPlots, truicear::MatchedTriggerObjectPlots>::~TriggerLevelStudy();
template std::vector<mor::TriggerObject>* truicear::TriggerLevelStudy<mor::TriggerObject, mor::TriggerObject, truicear::BasicTriggerObjectPlots, truicear::MatchedTriggerObjectPlots>::get_matched_trigger_objects();
template std::vector<mor::TriggerObject>* truicear::TriggerLevelStudy<mor::TriggerObject, mor::TriggerObject, truicear::BasicTriggerObjectPlots, truicear::MatchedTriggerObjectPlots>::get_matched_reco_objects();

template <typename LeptonType, typename MatchedLeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
truicear::TriggerLevelStudy<LeptonType, MatchedLeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::TriggerLevelStudy(std::string id, std::string trigger_name, std::string trigger_level,std::string l1seed_name)
{
	this->trigger_level = trigger_level;
	this->trigger_name = trigger_name;
	this->l1seed_name = l1seed_name;

	trigger_objects = NULL;
	leptons = NULL;
	jets = NULL;
	list_of_matches = NULL;

	std::string trigger_id = trigger_name+"_"+trigger_level;

	trigger_matcher = new truicear::TriggerMatcher<LeptonType, MatchedLeptonType>();

	std::string extra_id = "";
	if(trigger_level == "HLTvsL1Seed")
		extra_id = "_HLTvsL1Seed";

	basic_lep_plots = new BasicLeptonPlotsType();
	unmatched_basic_lep_plots = new BasicLeptonPlotsType();
	unmatched_basic_lep_plots->set_id(id, trigger_level+"_unmatched."+trigger_name+extra_id+"_");
	matched_lep_plots = new MatchedLeptonPlotsType();
	basic_jet_plots = new truicear::BasicJetPlots();
	basic_lep_plots->set_id(id, trigger_level+"_matched."+trigger_name+extra_id+"_");
	matched_lep_plots->set_id(id, trigger_level+"_matched."+trigger_name+extra_id+"_");
	matched_lep_plots->set_trigger_matcher(trigger_matcher);
	basic_jet_plots->set_id(id, trigger_level+"_matched."+trigger_name+extra_id+"_");

	basic_trig_obj_plots = new truicear::BasicTriggerObjectPlots();
	matched_trig_obj_plots = new truicear::BasicTriggerObjectPlots();
	unmatched_trig_obj_plots = new truicear::BasicTriggerObjectPlots();
	basic_trig_obj_plots->set_id(id, "All."+trigger_id+extra_id+"_");
	matched_trig_obj_plots->set_id(id, "RECO_matched."+trigger_id+extra_id+"_");
	unmatched_trig_obj_plots->set_id(id, "RECO_unmatched."+trigger_id+extra_id+"_");
	
	eire::HandleHolder dummy_hh;
	std::cerr << "ERROR: TriggerLevelStudy currently no longer supported! Propagate HandleHolder to here. Exiting..." << std::endl;
	exit(1);

	trigger_obj_selector = new truicear::TriggerObjectSelector(&dummy_hh);
	int module = -1;
	if(trigger_level == "HLT"){
		module = 0;
		trigger_obj_selector->set_trigger_name(trigger_name, module);
	}
	if(trigger_level == "L1Seed"){
		module = 1;
		trigger_obj_selector->set_trigger_name(trigger_name, module);
	}

	nobj_found = 0;
	nobj_match = 0;
	nlep_found = 0;
}

template <typename LeptonType, typename MatchedLeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
truicear::TriggerLevelStudy<LeptonType, MatchedLeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::~TriggerLevelStudy()
{
	std::cout << "nlep " << trigger_level << " " << trigger_name << " found: " << nobj_found << std::endl;
	std::cout << "nlep " << trigger_level << " " << trigger_name << " matched: " << nobj_match << std::endl;
	double efficiency = nobj_match/nlep_found;
		
	std::cout << trigger_level << " " << trigger_name << " Efficiency: " << efficiency << " +/- " << sqrt((efficiency*(1-efficiency))/nlep_found) << std::endl;
	if(basic_lep_plots){
		delete basic_lep_plots;
		basic_lep_plots = NULL;
	}
	if(unmatched_basic_lep_plots){
		delete unmatched_basic_lep_plots;
		unmatched_basic_lep_plots = NULL;
	}
	if(matched_lep_plots){
		delete matched_lep_plots;
		matched_lep_plots = NULL;
	}
	if(basic_jet_plots){
		delete basic_jet_plots;
		basic_jet_plots = NULL;
	}
	if(trigger_obj_selector){
		delete trigger_obj_selector;
		trigger_obj_selector = NULL;
	}

	if(basic_trig_obj_plots){
		delete basic_trig_obj_plots;
		basic_trig_obj_plots = NULL;
	}
	if(matched_trig_obj_plots){
		delete matched_trig_obj_plots;
		matched_trig_obj_plots = NULL;
	}
	if(unmatched_trig_obj_plots){
		delete unmatched_trig_obj_plots;
		unmatched_trig_obj_plots = NULL;
	}
}

template <typename LeptonType, typename MatchedLeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerLevelStudy<LeptonType, MatchedLeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::set_handles(std::vector<LeptonType> *leptons, std::vector<MatchedLeptonType> *trigger_objects, std::vector<mor::Jet> *jets, mor::EventInformation *event_information)
{
	this->leptons = leptons;
	this->trigger_objects = trigger_objects;
	this->jets = jets;
	this->event_information = event_information;
}

template <typename LeptonType, typename MatchedLeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerLevelStudy<LeptonType, MatchedLeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::set_histo_writer(HistoWriter *histo_writer)
{
	this->histo_writer = histo_writer;
	basic_lep_plots->set_histo_writer(histo_writer);
	unmatched_basic_lep_plots->set_histo_writer(histo_writer);
	matched_lep_plots->set_histo_writer(histo_writer);
	basic_jet_plots->set_histo_writer(histo_writer);
	basic_trig_obj_plots->set_histo_writer(histo_writer);
	matched_trig_obj_plots->set_histo_writer(histo_writer);
	unmatched_trig_obj_plots->set_histo_writer(histo_writer);
}

template <typename LeptonType, typename MatchedLeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerLevelStudy<LeptonType, MatchedLeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::plot()
{
	prepare_objects();
	nobj_found += trigger_objects->size();		// so far unmatched, that is all trigger objects
	nlep_found += leptons->size();			// count leptons, or l1 trigger objects for HLTvsL1Seed
	std::vector<LeptonType> *matched_leptons = trigger_matcher->match(leptons, trigger_objects);

	nobj_match += matched_leptons->size();	// matched trigger objects
	if(matched_leptons->size() > trigger_objects->size()){
		std::cout << "WARNING" << std::endl;
		std::cout << "matched: " << matched_leptons->size() << std::endl;
		std::cout << "obj: " << trigger_objects->size() << std::endl;
	}
	if(matched_leptons->size() > 0){
		basic_lep_plots->plot(matched_leptons, jets, event_information);
		matched_lep_plots->plot(leptons, trigger_objects, trigger_matcher->get_match_results());
		basic_jet_plots->plot(jets);
	}
	unmatched_basic_lep_plots->plot(trigger_matcher->get_type1_not_matched(), jets, event_information);

	basic_trig_obj_plots->plot(trigger_objects, jets, event_information);
	matched_trig_obj_plots->plot(trigger_matcher->get_type2_matched(), jets, event_information);
	unmatched_trig_obj_plots->plot(trigger_matcher->get_type2_not_matched(), jets, event_information);
}

template <typename LeptonType, typename MatchedLeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerLevelStudy<LeptonType, MatchedLeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::set_matching_criteria(truicear::MatchingCriteria *matching_criteria)
{
	trigger_matcher->set_max_dR(matching_criteria->get_maxdR());
	trigger_matcher->set_max_dEta(matching_criteria->get_maxdEta());
	trigger_matcher->set_max_dPt(matching_criteria->get_maxdPt());
	trigger_matcher->set_matching_method(matching_criteria->get_matching_method());

	trigger_obj_selector->set_min_pt(matching_criteria->get_minPt());
	trigger_obj_selector->set_max_eta(matching_criteria->get_maxEta());
}

template <typename LeptonType, typename MatchedLeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerLevelStudy<LeptonType, MatchedLeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::prepare_objects()
{
	// select trigger objects
	if(trigger_level != "HLTvsL1Seed")
		trigger_objects = trigger_obj_selector->get_selected_objects(trigger_objects);
}

namespace truicear{
	template <>
	void truicear::TriggerLevelStudy<mor::TriggerObject, mor::TriggerObject, truicear::BasicTriggerObjectPlots, truicear::MatchedTriggerObjectPlots>::prepare_objects()
	{
		// do nothing
		// for hlt to l1 comparison trigger objects are already selected
	}
}

template <class LeptonType, class MatchedLeptonType, class BasicLeptonPlotsType, class MatchedLeptonPlotsType>
std::vector<MatchedLeptonType>* truicear::TriggerLevelStudy<LeptonType, MatchedLeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::get_matched_trigger_objects()
{
	return trigger_matcher->get_type2_matched();
}

template <class LeptonType, class MatchedLeptonType, class BasicLeptonPlotsType, class MatchedLeptonPlotsType>
std::vector<LeptonType>* truicear::TriggerLevelStudy<LeptonType, MatchedLeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::get_matched_reco_objects()
{
	return trigger_matcher->get_type1_matched();
}
