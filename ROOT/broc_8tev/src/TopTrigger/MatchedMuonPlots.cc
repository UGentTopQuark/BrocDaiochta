#include "../../interface/TopTrigger/MatchedMuonPlots.h"

truicear::MatchedMuonPlots::MatchedMuonPlots()
{
	trigger_matcher = NULL;
}

truicear::MatchedMuonPlots::~MatchedMuonPlots()
{
}

void truicear::MatchedMuonPlots::book_histos()
{
	histos1d[pre+"dR"+id]=histo_writer->create_1d(pre+"dR"+id,"dR reco muon to trigger object "+pre,50,0,0.5, "dR");
	histos1d[pre+"min_match_diff"+id]=histo_writer->create_1d(pre+"min_match_diff"+id,"min diff in matching variable between reco muon and trigger object "+pre,400,0,4.0, "dD");
	histos1d[pre+"dEta"+id]=histo_writer->create_1d(pre+"dEta"+id,"dEta reco muon to trigger object "+pre,60,-3.0,3.0, "dEta");
	histos1d[pre+"dPhi"+id]=histo_writer->create_1d(pre+"dPhi"+id,"dPhi reco muon to trigger object "+pre,66,-3.3,3.3, "dPhi");
	histos1d[pre+"dPtRel"+id]=histo_writer->create_1d(pre+"dPtRel"+id,"dPtRel reco muon to trigger object "+pre,20, -0.5,0.5, "dPtRel");
	histos2d[pre+"pt_vs_dPt"+id]=histo_writer->create_2d(pre+"pt_vs_dPt"+id,"pt vs dPt reco muon to trigger object "+pre,20, 0,2,50,0,100," dp_{T}", "p_{T}");
	histos2d[pre+"pt_vs_dPtRel"+id]=histo_writer->create_2d(pre+"pt_vs_dPtRel"+id,"pt vs dPtRel reco muon to trigger object "+pre,20, 0,2,40,0,1," dp_{T}", "p_{T}");

	histos2d[pre+"dR_vs_dPtRel"+id]=histo_writer->create_2d(pre+"dR_vs_dPtRel"+id,"dR vs dPtRel reco muon to trigger object "+pre,50, 0,0.5,40,0,1," dp_{T}", "dR");
	histos2d[pre+"dEta_vs_dPtRel"+id]=histo_writer->create_2d(pre+"dEta_vs_dPtRel"+id,"dEta vs dPtRel reco muon to trigger object "+pre,60, 0,3.0,40,0,1," dp_{T}", "Delta #eta");
	histos2d[pre+"dPhi_vs_dPtRel"+id]=histo_writer->create_2d(pre+"dPhi_vs_dPtRel"+id,"dPhi vs dPtRel reco muon to trigger object "+pre,66, 0,3.3,40,0,1," dp_{T}", "#Delta R");

	histos2d[pre+"dEta_vs_eta"+id]=histo_writer->create_2d(pre+"dEta_vs_Eta"+id,"dEta_vs_Eta muons "+pre,20,-2.4,2.4, 500, 0.0, 1.0, "#eta", "#Delta #eta");
	histos2d[pre+"dR_vs_eta"+id]=histo_writer->create_2d(pre+"dR_vs_Eta"+id,"dR_vs_Eta muons "+pre,20,-2.4,2.4, 500, 0.0, 1.0, "#eta", "#Delta R");
}

void truicear::MatchedMuonPlots::set_trigger_matcher(truicear::TriggerMatcher<mor::Muon, mor::TriggerObject> *trigger_matcher)
{
	this->trigger_matcher = trigger_matcher;
}

void truicear::MatchedMuonPlots::plot(std::vector<mor::Muon> *reco_muons, std::vector<mor::TriggerObject> *trigger_objects,std::vector<std::pair<int, int> >* match_results)
{
	for(std::vector<std::pair<int,int> >::iterator match = match_results->begin();match != match_results->end();match++){
		std::pair<int,int> match_id = *match;
		
		double dR = ROOT::Math::VectorUtil::DeltaR((*reco_muons)[match_id.first].p4(),(*trigger_objects)[match_id.second].p4());
		double dEta = (*reco_muons)[match_id.first].Eta() - (*trigger_objects)[match_id.second].Eta();
		double dPhi = (*reco_muons)[match_id.first].Phi() - (*trigger_objects)[match_id.second].Phi();
		double dPt = (*reco_muons)[match_id.first].Pt() - (*trigger_objects)[match_id.second].Pt();
		double dPtRel = dPt/(*trigger_objects)[match_id.second].Pt();

		histos1d[pre+"dR"+id]->Fill(dR);
		histos1d[pre+"dEta"+id]->Fill(dEta);
		histos1d[pre+"dPhi"+id]->Fill(dPhi);
		histos1d[pre+"dPtRel"+id]->Fill(dPtRel);
		histos2d[pre+"pt_vs_dPt"+id]->Fill(dPt,(*reco_muons)[match_id.first].Pt());
		histos2d[pre+"pt_vs_dPtRel"+id]->Fill(fabs(dPtRel),(*reco_muons)[match_id.first].Pt());
		histos2d[pre+"dR_vs_dPtRel"+id]->Fill(fabs(dPtRel),fabs(dR));
		histos2d[pre+"dEta_vs_dPtRel"+id]->Fill(fabs(dPtRel),fabs(dEta));
		histos2d[pre+"dPhi_vs_dPtRel"+id]->Fill(fabs(dPtRel),fabs(dPhi));

	} 

	for(std::vector<mor::Muon>::iterator reco_muon = reco_muons->begin();
		reco_muon != reco_muons->end();
		++reco_muon){
		if(trigger_matcher){
			// for HLT dmin is dR, for L1 dEta
			double dmin = trigger_matcher->get_smallest_match_diff();
			if(dmin != -1) histos2d[pre+"dEta_vs_eta"+id]->Fill(reco_muon->eta(), dmin);
			if(dmin != -1) histos2d[pre+"dR_vs_eta"+id]->Fill(reco_muon->eta(), dmin);
			if(dmin != -1) histos1d[pre+"min_match_diff"+id]->Fill(dmin);
		}
	}
}
