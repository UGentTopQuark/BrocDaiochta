#include "../../interface/TopTrigger/BasicMuonPlots.h"

truicear::BasicMuonPlots::BasicMuonPlots()
{

}

truicear::BasicMuonPlots::~BasicMuonPlots()
{
}


void truicear::BasicMuonPlots::plot(std::vector<mor::Muon> *reco_muons, std::vector<mor::Jet> *jets, mor::EventInformation *event_information)
{
	int nmu_barrel = 0,nmu_overlap = 0,nmu_endcap = 0;
	int nmu_pt20to30 = 0,nmu_pt30to40 = 0,nmu_pt40plus = 0;
	for(std::vector<mor::Muon>::iterator reco_muon = reco_muons->begin();
		reco_muon != reco_muons->end();
		++reco_muon){


		double mu_pt = reco_muon->Pt();
		double mu_eta = reco_muon->Eta();
		histos1d[pre+"overall_efficiency"+id]->Fill(1);
		histos1d[pre+"pt"+id]->Fill(mu_pt);
		histos1d[pre+"pt_fine"+id]->Fill(mu_pt);
		histos1d[pre+"eta"+id]->Fill(mu_eta);
		histos1d[pre+"eta_fine"+id]->Fill(mu_eta);
		histos1d[pre+"phi"+id]->Fill(reco_muon->Phi());
		histos1d[pre+"charge"+id]->Fill(reco_muon->charge());
		histos1d[pre+"d0"+id]->Fill(reco_muon->d0());
		histos1d[pre+"z0"+id]->Fill(reco_muon->z0());
		histos1d[pre+"z0_pv"+id]->Fill(reco_muon->z0_pv());
		histos1d[pre+"nHits"+id]->Fill(reco_muon->nHits());
		histos1d[pre+"chi2"+id]->Fill(reco_muon->chi2());
		histos1d[pre+"relIso"+id]->Fill(reco_muon->relIso());
		histos1d[pre+"nMuonHits"+id]->Fill(reco_muon->nMuonHits());
		histos1d[pre+"nStripHits"+id]->Fill(reco_muon->nStripHits());
		histos1d[pre+"nPixelLayers"+id]->Fill(reco_muon->nPixelLayers());
		histos1d[pre+"nLostTrackerHits"+id]->Fill(reco_muon->nLostTrackerHits());
		histos1d[pre+"nStations"+id]->Fill((reco_muon->nDTstations()+reco_muon->nCSCstations()));
		histos1d[pre+"nPixelHits"+id]->Fill(reco_muon->nPixelHits());
		histos2d[pre+"eta_vs_phi"+id]->Fill(reco_muon->Phi(),reco_muon->eta());
		histos2d[pre+"pt_vs_eta"+id]->Fill(reco_muon->eta(),reco_muon->pt());

		histos1d[pre+"eff_vs_run"+id]->Fill(event_information->run());

		//find closest jet
		double min_jet_dR = -1;
		for(std::vector<mor::Jet>::iterator jet = jets->begin(); jet != jets->end(); jet++){
			double dR = ROOT::Math::VectorUtil::DeltaR(*reco_muon,*jet);
			if(dR < min_jet_dR || min_jet_dR == -1)
				min_jet_dR = dR;
		}
		if(min_jet_dR != -1)
			histos1d[pre+"dR_jet_muon"+id]->Fill(min_jet_dR);

		//Fill plots for different eta regions
		// important guy in TPG meeting said it's 0.9
		if(fabs(mu_eta) < 0.9){
			nmu_barrel++;
			histos1d[pre+"overall_efficiency"+id]->Fill(2);
			histos1d[pre+"pt_barrel"+id]->Fill(mu_pt);
			histos1d[pre+"pt_barrel_fixedbins"+id]->Fill(mu_pt);
			histos1d[pre+"phi_barrel"+id]->Fill(reco_muon->Phi());
			histos1d[pre+"d0_barrel"+id]->Fill(reco_muon->d0());
			histos1d[pre+"nHits_barrel"+id]->Fill(reco_muon->nHits());
			histos1d[pre+"chi2_barrel"+id]->Fill(reco_muon->chi2());
			histos1d[pre+"relIso_barrel"+id]->Fill(reco_muon->relIso());
		}
		else if (0.9 <= fabs(mu_eta) && fabs(mu_eta) < 1.2){
			nmu_overlap++;
			histos1d[pre+"overall_efficiency"+id]->Fill(3);
			histos1d[pre+"pt_overlap"+id]->Fill(mu_pt);
			histos1d[pre+"pt_overlap_fixedbins"+id]->Fill(mu_pt);
			histos1d[pre+"phi_overlap"+id]->Fill(reco_muon->Phi());
			histos1d[pre+"d0_overlap"+id]->Fill(reco_muon->d0());
			histos1d[pre+"nHits_overlap"+id]->Fill(reco_muon->nHits());
			histos1d[pre+"chi2_overlap"+id]->Fill(reco_muon->chi2());
			histos1d[pre+"relIso_overlap"+id]->Fill(reco_muon->relIso());
		}
		else if (1.2 <= fabs(mu_eta)){
			nmu_endcap++;
			histos1d[pre+"overall_efficiency"+id]->Fill(4);
			histos1d[pre+"pt_endcap"+id]->Fill(mu_pt);
			histos1d[pre+"pt_endcap_fixedbins"+id]->Fill(mu_pt);
			histos1d[pre+"phi_endcap"+id]->Fill(reco_muon->Phi());
			histos1d[pre+"d0_endcap"+id]->Fill(reco_muon->d0());
			histos1d[pre+"nHits_endcap"+id]->Fill(reco_muon->nHits());
			histos1d[pre+"chi2_endcap"+id]->Fill(reco_muon->chi2());
			histos1d[pre+"relIso_endcap"+id]->Fill(reco_muon->relIso());
		}
		//Fill plots for different pt regions
		if(20 < mu_pt && mu_pt <= 30){			
			nmu_pt20to30++;
			histos1d[pre+"overall_efficiency"+id]->Fill(5);
			histos1d[pre+"eta_pt20to30"+id]->Fill(mu_eta);
			histos1d[pre+"phi_pt20to30"+id]->Fill(reco_muon->Phi());
			histos1d[pre+"d0_pt20to30"+id]->Fill(reco_muon->d0());
			histos1d[pre+"nHits_pt20to30"+id]->Fill(reco_muon->nHits());
			histos1d[pre+"chi2_pt20to30"+id]->Fill(reco_muon->chi2());
			histos1d[pre+"relIso_pt20to30"+id]->Fill(reco_muon->relIso());
		}
		if(30. < mu_pt && mu_pt <= 40){
			nmu_pt30to40++;
			histos1d[pre+"overall_efficiency"+id]->Fill(6);
			histos1d[pre+"eta_pt30to40"+id]->Fill(mu_eta);
			histos1d[pre+"phi_pt30to40"+id]->Fill(reco_muon->Phi());
			histos1d[pre+"d0_pt30to40"+id]->Fill(reco_muon->d0());
			histos1d[pre+"nHits_pt30to40"+id]->Fill(reco_muon->nHits());
			histos1d[pre+"chi2_pt30to40"+id]->Fill(reco_muon->chi2());
			histos1d[pre+"relIso_pt30to40"+id]->Fill(reco_muon->relIso());
		}
		else if(40 < mu_pt){
			nmu_pt40plus++;
			histos1d[pre+"overall_efficiency"+id]->Fill(7);
			histos1d[pre+"eta_pt40plus"+id]->Fill(mu_eta);
			histos1d[pre+"phi_pt40plus"+id]->Fill(reco_muon->Phi());
			histos1d[pre+"d0_pt40plus"+id]->Fill(reco_muon->d0());
			histos1d[pre+"nHits_pt40plus"+id]->Fill(reco_muon->nHits());
			histos1d[pre+"chi2_pt40plus"+id]->Fill(reco_muon->chi2());
			histos1d[pre+"relIso_pt40plus"+id]->Fill(reco_muon->relIso());
		}
	}

	if(reco_muons->size() > 0) histos1d[pre+"number"+id]->Fill(reco_muons->size());
	if(nmu_barrel > 0) histos1d[pre+"number_barrel"+id]->Fill(nmu_barrel);
	if(nmu_overlap > 0) histos1d[pre+"number_overlap"+id]->Fill(nmu_overlap);
	if(nmu_endcap > 0) histos1d[pre+"number_endcap"+id]->Fill(nmu_endcap);
	if(nmu_pt20to30 > 0) histos1d[pre+"number_pt20to30"+id]->Fill(nmu_pt20to30);
	if(nmu_pt30to40 > 0) histos1d[pre+"number_pt30to40"+id]->Fill(nmu_pt30to40);
	if(nmu_pt40plus > 0) histos1d[pre+"number_pt40plus"+id]->Fill(nmu_pt40plus);
}

void truicear::BasicMuonPlots::book_histos()
{

	int nptBins = 9;
	float  ptBins[100] = { 0,5,9,12,15,20,30,40,60,100 };
	int netaBins = 5 ;
	float  etaBins[100] = { -2.1, -1.2, -0.9, 0.9, 1.2, 2.1};
	int nrelIsoBins = 5;
	float  relIsoBins[100] = { 0, 0.05, 0.1, 0.2, 0.4, 1 };
	int nfixedptBins = 5;
	float  fixedptBins[100] = { 0, 10, 20, 30, 40, 100 };
	int nphiBins = 6;
	float  phiBins[100] = { -3.3, -2, -1, 0, 1, 2, 3.3 };

	histos1d[pre+"overall_efficiency"+id]=histo_writer->create_1d(pre+"overall_efficiency"+id,"overall efficiency "+pre,7,0.5,7.5, "Overall Efficiency in different eta pt regions");	
	
	histos1d[pre+"number"+id]=histo_writer->create_1d(pre+"number"+id,"muon multiplicity "+pre,10,-0.5,9.5, "Muon Multiplicity");
	histos1d[pre+"pt"+id]=histo_writer->create_1d(pre+"pt"+id,"p_{t} muons "+pre,nptBins,ptBins, "p_{t} [GeV]");
	histos1d[pre+"pt_fine"+id]=histo_writer->create_1d(pre+"pt_fine"+id,"p_{t} muons "+pre,50,0.,25., "p_{t} [GeV]");
	histos1d[pre+"eta"+id]=histo_writer->create_1d(pre+"eta"+id,"#eta muons "+pre,netaBins,etaBins, "#eta");
	histos1d[pre+"eta_fine"+id]=histo_writer->create_1d(pre+"eta_fine"+id,"#eta muons "+pre,30,-2.5,2.5, "#eta");
	histos1d[pre+"phi"+id]=histo_writer->create_1d(pre+"phi"+id,"#phi muons "+pre,nphiBins,phiBins, "#phi");
	histos1d[pre+"charge"+id]=histo_writer->create_1d(pre+"charge"+id,"muon charge "+pre,2,-1.5,1.5, "charge");
	histos1d[pre+"d0"+id]=histo_writer->create_1d(pre+"d0"+id,"d0 muons "+pre,15,-0.3,0.3, "d0");
	histos1d[pre+"z0"+id]=histo_writer->create_1d(pre+"z0"+id,"z0 muons "+pre,40,-20.,20., "z0");
	histos1d[pre+"z0_pv"+id]=histo_writer->create_1d(pre+"z0_pv"+id,"z0 muons w.r.t primary vertex "+pre,15,-0.6,0.6, "z0");
	histos1d[pre+"nHits"+id]=histo_writer->create_1d(pre+"nHits"+id,"nHits muons "+pre,30,-0.5,29.5, "nHits");
	histos1d[pre+"chi2"+id]=histo_writer->create_1d(pre+"chi2"+id,"#chi^{2}/n.d.f. muons "+pre,15,0.0,30, "#chi^{2}/n.d.f.");
	histos1d[pre+"relIso"+id]=histo_writer->create_1d(pre+"relIso"+id,"relIso muons "+pre,nrelIsoBins, relIsoBins, "Relative Isolation");
	histos1d[pre+"nMuonHits"+id]=histo_writer->create_1d(pre+"nMuonHits"+id,"nMuonHits "+pre,20,-0.5,19.5, "nValidMuonHits");
	histos1d[pre+"nStripHits"+id]=histo_writer->create_1d(pre+"nStripHits"+id,"nStripHits  "+pre,20,-0.5,29.5, "nStripValidHits");
	histos1d[pre+"nPixelLayers"+id]=histo_writer->create_1d(pre+"nPixelLayers"+id,"nPixelLayers with measurement  "+pre,10,-0.5,9.5, "nPixelLayers with Measurement");
	histos1d[pre+"nStations"+id]=histo_writer->create_1d(pre+"nStations"+id,"nStations DT OR CSC "+pre,10,-0.5,9.5, "nStations DT OR CSC");
	histos1d[pre+"nLostTrackerHits"+id]=histo_writer->create_1d(pre+"nLostTrackerHits"+id,"nLostTrackerHits muons "+pre,10,-0.5,9.5, "nMissingTrackerHits");
	histos1d[pre+"nPixelHits"+id]=histo_writer->create_1d(pre+"nPixelHits"+id,"nPixelHits "+pre,20,-0.5,19.5, "nPixelHits");
	histos2d[pre+"eta_vs_phi"+id]=histo_writer->create_2d(pre+"eta_vs_phi"+id,"#eta vs #phi muons "+pre,36,-3.3,3.3,30,-3.0,3.0, "#phi","#eta");
	histos2d[pre+"pt_vs_eta"+id]=histo_writer->create_2d(pre+"pt_vs_eta"+id,"p_{T} vs #eta muons "+pre,30,-3.0,3.0,100,-0,50.0, "#eta", "p_{T}");
	histos1d[pre+"dR_jet_muon"+id]=histo_writer->create_1d(pre+"dR_jet_muon"+id,"dR between muon and closest jet "+pre,80,0.,8, "dR");

	// efficiency over time
	histos1d[pre+"eff_vs_run"+id]=histo_writer->create_1d(pre+"eff_vs_run"+id,"Efficiency over run numer "+pre,80000,100000.5,180000.5, "Run Number");
	
	std::vector<std::string> eta_regions;
	eta_regions.push_back("barrel");
	eta_regions.push_back("overlap");
	eta_regions.push_back("endcap");

	for(std::vector<std::string>::iterator eta_region = eta_regions.begin();eta_region != eta_regions.end();eta_region++){

		histos1d[pre+"number_"+*eta_region+""+id]=histo_writer->create_1d(pre+"number_"+*eta_region+""+id,"muon multiplicity "+*eta_region+" "+pre,10,-0.5,9.5, "Muon Multiplicity");
		histos1d[pre+"pt_"+*eta_region+""+id]=histo_writer->create_1d(pre+"pt_"+*eta_region+""+id,"p_{t} "+*eta_region+" muons "+pre,nptBins,ptBins, "p_{t} [GeV]");
		histos1d[pre+"pt_"+*eta_region+"_fixedbins"+id]=histo_writer->create_1d(pre+"pt_"+*eta_region+"_fixedbins"+id,"p_{t} "+*eta_region+" muons "+pre,nfixedptBins,fixedptBins, "p_{t} [GeV]");
		histos1d[pre+"phi_"+*eta_region+""+id]=histo_writer->create_1d(pre+"phi_"+*eta_region+""+id,"#phi "+*eta_region+" muons "+pre,18,-3.3,3.3, "#phi");
		histos1d[pre+"d0_"+*eta_region+""+id]=histo_writer->create_1d(pre+"d0_"+*eta_region+""+id,"d0  "+*eta_region+" muons "+pre,15,-0.3,0.3, "d0");
		histos1d[pre+"nHits_"+*eta_region+""+id]=histo_writer->create_1d(pre+"nHits_"+*eta_region+""+id,"nHits "+*eta_region+" muons "+pre,30,-0.5,29.5, "nHits");
		histos1d[pre+"chi2_"+*eta_region+""+id]=histo_writer->create_1d(pre+"chi2_"+*eta_region+""+id,"#chi^{2}/n.d.f. "+*eta_region+" muons "+pre,15,0.0,30, "#chi^{2}/n.d.f.");
		histos1d[pre+"relIso_"+*eta_region+""+id]=histo_writer->create_1d(pre+"relIso_"+*eta_region+""+id,"relIso "+*eta_region+" muons "+pre,nrelIsoBins, relIsoBins, "Relative Isolation");
	}
	
	std::vector<std::string> pt_regions;
	pt_regions.push_back("pt20to30");
	pt_regions.push_back("pt30to40");
	pt_regions.push_back("pt40plus");

	for(std::vector<std::string>::iterator pt_region = pt_regions.begin();pt_region != pt_regions.end();pt_region++){

		histos1d[pre+"number_"+*pt_region+""+id]=histo_writer->create_1d(pre+"number_"+*pt_region+""+id,"muon multiplicity "+*pt_region+" "+pre,10,-0.5,9.5, "Muon Multiplicity");
		histos1d[pre+"eta_"+*pt_region+""+id]=histo_writer->create_1d(pre+"eta_"+*pt_region+""+id,"#eta "+*pt_region+" muons "+pre,netaBins,etaBins, "#eta");
		histos1d[pre+"phi_"+*pt_region+""+id]=histo_writer->create_1d(pre+"phi_"+*pt_region+""+id,"#phi "+*pt_region+" muons "+pre,18,-3.3,3.3, "#phi");
		histos1d[pre+"d0_"+*pt_region+""+id]=histo_writer->create_1d(pre+"d0_"+*pt_region+""+id,"d0  "+*pt_region+" muons "+pre,15,-0.3,0.3, "d0");
		histos1d[pre+"nHits_"+*pt_region+""+id]=histo_writer->create_1d(pre+"nHits_"+*pt_region+""+id,"nHits "+*pt_region+" muons "+pre,30,-0.5,29.5, "nHits");
		histos1d[pre+"chi2_"+*pt_region+""+id]=histo_writer->create_1d(pre+"chi2_"+*pt_region+""+id,"#chi^{2}/n.d.f. "+*pt_region+" muons "+pre,15,0.0,30, "#chi^{2}/n.d.f.");
		histos1d[pre+"relIso_"+*pt_region+""+id]=histo_writer->create_1d(pre+"relIso_"+*pt_region+""+id,"relIso "+*pt_region+" muons "+pre,nrelIsoBins, relIsoBins, "Relative Isolation");
	}
}
