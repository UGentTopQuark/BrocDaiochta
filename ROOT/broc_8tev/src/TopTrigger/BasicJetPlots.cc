#include "../../interface/TopTrigger/BasicJetPlots.h"

truicear::BasicJetPlots::BasicJetPlots()
{
}

truicear::BasicJetPlots::~BasicJetPlots()
{
}

void truicear::BasicJetPlots::book_histos()
{
	histos1d[pre+"jet_number"+id]=histo_writer->create_1d(pre+"jet_number"+id,"Jet Multiplicity "+pre,10,-0.5,9.5, "Jet Multiplicity");
	histos1d[pre+"jet1_pt"+id]=histo_writer->create_1d(pre+"jet1_pt"+id,"leading jet p_{t} "+pre,60,0,100, "p_{t} [GeV]");
	histos1d[pre+"jet1_eta"+id]=histo_writer->create_1d(pre+"jet1_eta"+id,"leading jet #eta  "+pre,60,-3.0,3.0, "#eta");
	histos1d[pre+"jet1_phi"+id]=histo_writer->create_1d(pre+"jet1_phi"+id,"leading jet #phi "+pre,60,-3.3,3.3, "#phi");
	histos1d[pre+"jet2_pt"+id]=histo_writer->create_1d(pre+"jet2_pt"+id,"second jet p_{t} "+pre,60,0,100, "p_{t} [GeV]");
	histos1d[pre+"jet2_eta"+id]=histo_writer->create_1d(pre+"jet2_eta"+id,"second jet #eta  "+pre,60,-3.0,3.0, "#eta");
	histos1d[pre+"jet2_phi"+id]=histo_writer->create_1d(pre+"jet2_phi"+id,"second jet #phi "+pre,60,-3.3,3.3, "#phi");
	histos1d[pre+"jet3_pt"+id]=histo_writer->create_1d(pre+"jet3_pt"+id,"third jet p_{t} "+pre,60,0,100, "p_{t} [GeV]");
	histos1d[pre+"jet3_eta"+id]=histo_writer->create_1d(pre+"jet3_eta"+id,"third jet #eta  "+pre,60,-3.0,3.0, "#eta");
	histos1d[pre+"jet3_phi"+id]=histo_writer->create_1d(pre+"jet3_phi"+id,"third jet #phi "+pre,60,-3.3,3.3, "#phi");
	histos1d[pre+"jet4_pt"+id]=histo_writer->create_1d(pre+"jet4_pt"+id,"fourth jet p_{t} "+pre,60,0,100, "p_{t} [GeV]");
	histos1d[pre+"jet4_eta"+id]=histo_writer->create_1d(pre+"jet4_eta"+id,"fourth jet #eta  "+pre,60,-3.0,3.0, "#eta");
	histos1d[pre+"jet4_phi"+id]=histo_writer->create_1d(pre+"jet4_phi"+id,"fourth jet #phi "+pre,60,-3.3,3.3, "#phi");

}

void truicear::BasicJetPlots::plot(std::vector<mor::Jet> *jets)
{
	int njets = jets->size();
	histos1d[pre+"jet_number"+id]->Fill(njets);

	if(njets > 0){
		histos1d[pre+"jet1_pt"+id]->Fill((*jets)[0].pt());
		histos1d[pre+"jet1_eta"+id]->Fill((*jets)[0].eta());
		histos1d[pre+"jet1_phi"+id]->Fill((*jets)[0].phi());
	}
	if(njets > 1){
		histos1d[pre+"jet2_pt"+id]->Fill((*jets)[1].pt());
		histos1d[pre+"jet2_eta"+id]->Fill((*jets)[1].eta());
		histos1d[pre+"jet2_phi"+id]->Fill((*jets)[1].phi());
	}
	if(njets > 2){
		histos1d[pre+"jet3_pt"+id]->Fill((*jets)[2].pt());
		histos1d[pre+"jet3_eta"+id]->Fill((*jets)[2].eta());
		histos1d[pre+"jet3_phi"+id]->Fill((*jets)[2].phi());
	}
	if(njets > 3){
		histos1d[pre+"jet4_pt"+id]->Fill((*jets)[3].pt());
		histos1d[pre+"jet4_eta"+id]->Fill((*jets)[3].eta());
		histos1d[pre+"jet4_phi"+id]->Fill((*jets)[3].phi());
	}
		
	
	//for(std::vector<mor::Jet>::iterator jet = jets->begin();
	//jet != jets->end();
	//++jet){
	//}
}
