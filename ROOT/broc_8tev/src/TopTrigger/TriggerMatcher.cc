#include "../../interface/TopTrigger/TriggerMatcher.h"

template <class ParType1, class ParType2>
truicear::TriggerMatcher<ParType1, ParType2>::TriggerMatcher()
{
	match_results = new std::vector<std::pair<int, int> >();

	matching_method = 0;	// default dR matching

	max_dR = -1;		// max value to match two particle for dR
	max_dEta = -1;
	smallest_match_diff = -1;	// the smallest distance between two objects in an event
	max_dPt = -1;

	smallest_match_diffs = new std::vector<double>();
	smallest_match_diffs_no_double_matching = new std::vector<double>();

	type1_matched = new std::vector<ParType1>();
	type1_unmatched = new std::vector<ParType1>();
	type2_matched = new std::vector<ParType2>();
	type2_unmatched = new std::vector<ParType2>();
}

template <class ParType1, class ParType2>
truicear::TriggerMatcher<ParType1, ParType2>::~TriggerMatcher()
{
	if(smallest_match_diffs){delete smallest_match_diffs; smallest_match_diffs = NULL;}
	if(smallest_match_diffs_no_double_matching){delete smallest_match_diffs_no_double_matching; smallest_match_diffs_no_double_matching = NULL;}
	if(match_results){ delete match_results; match_results = NULL; }
	if(type1_matched){ delete type1_matched; type1_matched = NULL; }
	if(type1_unmatched){ delete type1_unmatched; type1_unmatched = NULL; }
	if(type2_unmatched){ delete type2_unmatched; type2_unmatched = NULL; }
	if(type2_matched){ delete type2_matched; type2_matched = NULL; }
}

template <class ParType1, class ParType2>
void truicear::TriggerMatcher<ParType1, ParType2>::set_matching_method(int selected_method)
{
	this->matching_method = selected_method;
}

template <class ParType1, class ParType2>
void truicear::TriggerMatcher<ParType1, ParType2>::set_max_dR(double dR)
{
	this->max_dR = dR;
}

template <class ParType1, class ParType2>
void truicear::TriggerMatcher<ParType1, ParType2>::set_max_dEta(double dEta)
{
	this->max_dEta = dEta;
}

template <class ParType1, class ParType2>
void truicear::TriggerMatcher<ParType1, ParType2>::set_max_dPt(double dPt)
{
	this->max_dPt = dPt;
}

template <class ParType1, class ParType2>
double truicear::TriggerMatcher<ParType1, ParType2>::get_smallest_match_diff()
{
	return smallest_match_diff;
}

template <class ParType1, class ParType2>
std::vector<ParType1>* truicear::TriggerMatcher<ParType1, ParType2>::get_type1_matched()
{
	return type1_matched;
}

template <class ParType1, class ParType2>
std::vector<ParType2>* truicear::TriggerMatcher<ParType1, ParType2>::get_type2_matched()
{
	return type2_matched;
}

template <class ParType1, class ParType2>
std::vector<ParType1>* truicear::TriggerMatcher<ParType1, ParType2>::get_type1_not_matched()
{
	return type1_unmatched;
}

template <class ParType1, class ParType2>
std::vector<ParType2>* truicear::TriggerMatcher<ParType1, ParType2>::get_type2_not_matched()
{
	return type2_unmatched;
}

template <class ParType1, class ParType2>
std::vector<ParType1>* truicear::TriggerMatcher<ParType1, ParType2>::match(std::vector<ParType1> *type1_particles, std::vector<ParType2> *type2_particles)
{
	// clear information from previous matching
	match_results->clear();
	type1_matched->clear();
	type1_unmatched->clear();
	type2_matched->clear();
	type2_unmatched->clear();

	// initialise counters for match results
	int npart1=0;
	int npart2=0;

	std::vector<short> type1_already_matched;
	std::vector<short> type2_already_matched;
	type1_already_matched.assign(type1_particles->size(),0);
	type2_already_matched.assign(type2_particles->size(),0);

	smallest_match_diffs->clear();
	smallest_match_diffs_no_double_matching->clear();

/*	// FIXME: just for electron high eta test
	bool verbose = false;
	if(fabs((*type1_particles)[0].eta()) > 2.1 || fabs((*type1_particles)[1].eta()) > 2.1){
		verbose = true;
		std::cout << "eta particle 0: " << (*type1_particles)[0].eta() << std::endl;
		std::cout << "eta particle 1: " << (*type1_particles)[1].eta() << std::endl;
	}
	*/

	if(verbose) std::cout << "#reco lepton: " << type1_particles->size() << std::endl;
	if(verbose) std::cout << "#trigger objects: " << type2_particles->size() << std::endl;

	// match the particles of the two collections
	for(typename std::vector<ParType1>::iterator part1 = type1_particles->begin();
		part1 != type1_particles->end();
		++part1){

		double smallest_match_diff_accept_unmatch = -1;	// find the closest match even if this is no accepted match
		double smallest_match_diff_accept_unmatch_no_double_matching = -1;	// find the closest match even if this is no accepted match
		
		// reset type 2 counter
		npart2 = 0;
		int smallest_diff_part1=-1;
		int smallest_diff_part2=-1;
		smallest_match_diff = -1;
		bool found_match = false;
		for(typename std::vector<ParType2>::iterator part2 = type2_particles->begin();
		    part2 != type2_particles->end();
		    ++part2){
			
		    	bool matched = false;
			double match_diff = -1;

			double tmp_dR = -1;
			double tmp_dpt = -1;

			// select a matching method, eg dR, dpt, deta, etc
			switch(matching_method){
				case 0:
					match_diff = match_dR(&(*part1), &(*part2));
					if(match_diff < max_dR) matched = true;
					break;
				case 1:
					match_diff = match_dEta((*part1).eta(),(*part2).eta());
					if(match_diff < max_dEta) matched = true;
					break;
				case 2:
					tmp_dR = match_dR(&(*part1), &(*part2));
					tmp_dpt = match_dPt(&(*part1), &(*part2));
					match_diff = tmp_dR;
					if(tmp_dR < max_dR && tmp_dpt < max_dPt) matched = true;
					break;
				case 3:
					match_diff = match_dR_propagated(&(*part1), &(*part2));
					if(match_diff < max_dR) matched = true;
					break;
				case 4:
					tmp_dR = match_dR_propagated(&(*part1), &(*part2));
					tmp_dpt = match_dPt(&(*part1), &(*part2));
					match_diff = tmp_dR;
					if(tmp_dR < max_dR && tmp_dpt < max_dPt) matched = true;
					break;
				default:
					match_diff = match_dR(&(*part1), &(*part2));
					if(match_diff < max_dR) matched = true;
					break;
			};

			if(verbose) std::cout << "current match diff (maxdR = " << max_dR << "): " << match_diff << std::endl;
			if(verbose && type1_already_matched[npart1] == 1) std::cout << "reco lepton already matched" << std::endl;
			if(verbose && type2_already_matched[npart2] == 1) std::cout << "trigger object already matched" << std::endl;
			if(match_diff != -1 && (match_diff < smallest_match_diff || smallest_match_diff == -1) && (type1_already_matched[npart1] != 1) && (type2_already_matched[npart2] != 1)){
				smallest_match_diff = match_diff;
				smallest_diff_part1 = npart1;
				smallest_diff_part2 = npart2;
				if(matched) found_match = true;
				if(verbose && matched) std::cout << "found match." << std::endl;
			}

			if(match_diff != -1 && ((match_diff < smallest_match_diff_accept_unmatch) || smallest_match_diff_accept_unmatch == -1)){
				smallest_match_diff_accept_unmatch = match_diff;
			}

			if(match_diff != -1 && ((match_diff < smallest_match_diff_accept_unmatch_no_double_matching) || smallest_match_diff_accept_unmatch_no_double_matching == -1) && (type1_already_matched[npart1] != 1) && (type2_already_matched[npart2] != 1)){
				smallest_match_diff_accept_unmatch_no_double_matching = match_diff;
			}

			++npart2;
		}

		smallest_match_diffs->push_back(smallest_match_diff_accept_unmatch);
		smallest_match_diffs_no_double_matching->push_back(smallest_match_diff_accept_unmatch_no_double_matching);

		if(smallest_diff_part1 != -1 && smallest_diff_part2 != -1 && found_match){
				type1_matched->push_back((*type1_particles)[smallest_diff_part1]);
				type2_matched->push_back((*type2_particles)[smallest_diff_part2]);
				type1_already_matched[smallest_diff_part1] = 1;
				type2_already_matched[smallest_diff_part2] = 1;
				match_results->push_back(std::pair<int,int>(smallest_diff_part1,smallest_diff_part2));
				if(verbose) std::cout << "assigning match: " << smallest_diff_part1 << " -- " << smallest_diff_part2 << std::endl;
		}
		++npart1;
	}

	// check which particles were not matched and fill them to unmatch vectors
	for(unsigned int npart1 = 0; npart1 < type1_particles->size(); ++npart1){
		if(!type1_already_matched[npart1])
			type1_unmatched->push_back((*type1_particles)[npart1]);
	}
	for(unsigned int npart2 = 0; npart2 < type2_particles->size(); ++npart2){
		if(!type2_already_matched[npart2])
			type2_unmatched->push_back((*type2_particles)[npart2]);
	}

	if(verbose) std::cout << "number of reco matches: " << type1_matched->size() << std::endl;
	if(verbose) std::cout << " --- " << std::endl;
	
	return type1_matched;
}

template <class ParType1, class ParType2>
double truicear::TriggerMatcher<ParType1, ParType2>::match_dR(ParType1 *part1, ParType2 *part2)
{
	double dR = ROOT::Math::VectorUtil::DeltaR(*part1, *part2);
	return dR;
}

namespace truicear{
	template <class ParType1, class ParType2>
	double truicear::TriggerMatcher<ParType1, ParType2>::match_dR_propagated(ParType1 *part1, ParType2 *part2)
	{
		std::cerr << "WARNING: TriggerMatcher::match_dR_propagated() call for invalid template type" << std::endl;
		return 999;
	}

	template <>
	double truicear::TriggerMatcher<mor::Electron, mor::TriggerObject>::match_dR_propagated(mor::Electron *part1, mor::TriggerObject *part2)
	{
		ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > propagated_part1(*part1);
		propagated_part1.SetEta(part1->supercluster_eta());
		propagated_part1.SetPhi(part1->supercluster_phi());

		double dR = ROOT::Math::VectorUtil::DeltaR(propagated_part1, *part2);
		return dR;
	}
	
	template <>
	double truicear::TriggerMatcher<mor::Muon, mor::TriggerObject>::match_dR_propagated(mor::Muon *part1, mor::TriggerObject *part2)
	{
		ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > propagated_part1(*part1);
		propagated_part1.SetEta(part1->station2_eta());
		propagated_part1.SetPhi(part1->station2_phi());

		double dR = ROOT::Math::VectorUtil::DeltaR(propagated_part1, *part2);
		return dR;
	}
}

template <class ParType1, class ParType2>
double truicear::TriggerMatcher<ParType1, ParType2>::match_dPt(ParType1 *part1, ParType2 *part2)
{
	double dpt = std::abs(part1->pt() - part2->pt())/part2->pt();
	return dpt;
}

template <class ParType1, class ParType2>
double truicear::TriggerMatcher<ParType1, ParType2>::match_dEta(double part1_eta, double part2_eta)
{
	double dEta = std::abs(part1_eta - part2_eta);
	return dEta;
}

template <class ParType1, class ParType2>
std::vector<std::pair<int, int> >* truicear::TriggerMatcher<ParType1, ParType2>::get_match_results()
{
	return match_results;
}

template class truicear::TriggerMatcher<mor::TriggerObject, mor::TriggerObject>;
template class truicear::TriggerMatcher<mor::Muon, mor::TriggerObject>;
template class truicear::TriggerMatcher<mor::Electron, mor::TriggerObject>;
template class truicear::TriggerMatcher<mor::Jet, mor::TriggerObject>;
template class truicear::TriggerMatcher<mor::Particle, mor::TriggerObject>;
