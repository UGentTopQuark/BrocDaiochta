#include "../../interface/EventSelection/MassReconstructionMethod.h"

void MassReconstructionMethod::reset_values()
{
	hadT_mass = -1;
	hadW_mass = -1;
	lepT_mass = -1;
	lepW_mass = -1;

	max_njets = 1000000;

	event_is_processed = false;

	mass_jet_ids->clear();
}

MassReconstructionMethod::MassReconstructionMethod()
{
	book_id_vectors();

	reset_values();

	event_is_processed = false;
}

MassReconstructionMethod::~MassReconstructionMethod()
{
	free_id_vectors();
}

void MassReconstructionMethod::book_id_vectors()
{
	mass_jet_ids = new std::vector<int>();
}

void MassReconstructionMethod::free_id_vectors()
{
	delete mass_jet_ids;
	mass_jet_ids = NULL;
}

void MassReconstructionMethod::set_handles(std::vector<mor::Jet>* jets, std::vector<mor::Electron>* electrons, std::vector<mor::Muon>* muons, std::vector<mor::MET> *mets)
{
	reset_values();

	this->jets = jets;
	this->electrons = electrons;
	this->muons = muons;
	this->mets = mets;
}

void MassReconstructionMethod::next_event()
{
	reset_values();
}

void MassReconstructionMethod::calculate_mass()
{
	// should be defined in derived classes
}

bool MassReconstructionMethod::is_processed()
{
	return event_is_processed;
}

//Check if relevant functions have been called and return calculated values
double MassReconstructionMethod::get_hadT_mass()
{
	if(hadT_mass != -1)
		return hadT_mass;
	else{
		calculate_mass();
		return hadT_mass;
	}
}

double MassReconstructionMethod::get_lepT_mass()
{
	if(lepT_mass != -1)
		return lepT_mass;
	else{
		calculate_mass();
		return lepT_mass;
	}
}

double MassReconstructionMethod::get_hadW_mass()
{
	if(hadW_mass != -1)
		return hadW_mass;
	else{
		calculate_mass();
		return hadW_mass;
	}
}

double MassReconstructionMethod::get_lepW_mass()
{
	if(lepW_mass != -1)
		return lepW_mass;
	else{
		calculate_mass();
		return lepW_mass;
	}
}

std::vector<int>* MassReconstructionMethod::get_mass_jet_ids()
{
	if(mass_jet_ids->size() > 0)
		return mass_jet_ids;
	else{
		calculate_mass();
		return mass_jet_ids;
	}
}

//called from calculate_* ,these functions return mass found using jets selected
double MassReconstructionMethod::top_Had_candidate_mass(int jet_id1,int jet_id2,int jet_id3)
{
	if(jet_id1 == -1 || jet_id2 == -1 || jet_id3 == -1){
		//std::cerr << "WARNING: invalid jet_id in MassReconstructionMethod::top_Had_candidate_mass()" << std::endl;
		//std::cout << "Info: njets = " << jets->size() << ", id1 = " << jet_id1 << ", id2 = " << jet_id2 << ", id3 = " << jet_id3 << std::endl;
		return -1;
	}

	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > top_Had_candidate;
	top_Had_candidate = (*jets)[jet_id1].p4()+(*jets)[jet_id2].p4()+(*jets)[jet_id3].p4();
	return top_Had_candidate.mass();

}

double MassReconstructionMethod::top_Lep_candidate_mass(int jet_id4)
{
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > top_Lep_candidate;
	
	if(electrons->begin() != electrons->end() && mets->begin() != mets->end()){
	  top_Lep_candidate = electrons->begin()->p4()+(*jets)[jet_id4].p4()+mets->begin()->p4();
	}
	else if(muons->begin() != muons->end() && mets->begin() != mets->end()){
	  top_Lep_candidate  = muons->begin()->p4()+(*jets)[jet_id4].p4()+mets->begin()->p4();  
	}
	else 
	  return -1;
	
	return top_Lep_candidate.mass();
}

double MassReconstructionMethod::W_Had_candidate_mass(int jet_id2, int jet_id3, int jet_id1)
{
	if(jet_id2 == -1 || jet_id3 == -1){
	      std::cout << "WARNING: in W_Had_candidate_mass() invalid jet_id" << std::endl;
	      return -1;
	}

	if(jet_id1 == -1){
		ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > W_Had_candidate;
		W_Had_candidate = (*jets)[jet_id2].p4()+(*jets)[jet_id3].p4();
		
		return W_Had_candidate.mass();
	}else{ // find best two jets out of 3
		double m12 = ((*jets)[jet_id1].p4()+(*jets)[jet_id2].p4()).mass();
		double m23 = ((*jets)[jet_id2].p4()+(*jets)[jet_id3].p4()).mass();
		double m13 = ((*jets)[jet_id1].p4()+(*jets)[jet_id3].p4()).mass();

		if((m12 < m23) && (m12 < m13))
			return m12;
		else if((m23 < m12) && (m23 < m13))
			return m23;
		else
			return m13;
	}
}

double MassReconstructionMethod::W_Lep_candidate_mass(mor::MET *met)
{

	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > W_Lep_candidate;

	if(met == NULL){
		if(electrons->begin() != electrons->end() && mets->begin() != mets->end()){
			W_Lep_candidate = electrons->begin()->p4()+mets->begin()->p4();
		}
		else if(muons->begin() != muons->end() && mets->begin() != mets->end()){
			W_Lep_candidate = muons->begin()->p4()+mets->begin()->p4();   
		}
		else 
			return -1;
	}else{
		if(electrons->begin() != electrons->end() && mets->begin() != mets->end()){
			W_Lep_candidate = electrons->begin()->p4()+met->p4();
		}
		else if(muons->begin() != muons->end() && mets->begin() != mets->end()){
			W_Lep_candidate = muons->begin()->p4()+met->p4();   
		}
		else 
			return -1;
	}

	return W_Lep_candidate.mass();
}

void MassReconstructionMethod::set_max_njets(int max)
{
	max_njets = max;
}
