#include "../../interface/EventSelection/MassReconstruction.h"

MassReconstruction::MassReconstruction(eire::HandleHolder *handle_holder)
{
	m3_mass = new M3Mass();
	min_diff_mass = new MinDiffM3();
	chi2_mass = new Chi2JetSortingMass();
	chi2_mass->set_tprime_mass(172.1);
	Wb_mass = new WbMass();

	b_jet_finder = new BJetFinder();
	b_jet_finder_max_4jets = new BJetFinder();
	b_jet_finder_max_4jets->set_max_considered_jets(4);
	b_jet_finder_max_5jets = new BJetFinder();
	b_jet_finder_max_5jets->set_max_considered_jets(4);

	set_handles(handle_holder);
	/* some b-tag algorithms:
	 *	"jetBProbabilityBJetTags"
	 *	"jetProbabilityBJetTags"
	 *	"trackCountingHighPurBJetTags"
	 *	"trackCountingHighEffBJetTags"
	 */
	std::string btag_algo = "trackCountingHighEffBJetTags";
	//std::string btag_algo = "jetBProbabilityBJetTags";

	std::vector<double> set_min_btag (1,-1000);
	b_jet_finder->set_min_btag_value(set_min_btag);
	b_jet_finder_max_4jets->set_min_btag_value(set_min_btag);
	b_jet_finder_max_5jets->set_min_btag_value(set_min_btag);


	chi2_mass->set_bjet_finder(b_jet_finder_max_5jets);
	chi2_mass->set_max_njets(5);
	min_diff_mass->set_bjet_finder(b_jet_finder);
	Wb_mass->set_bjet_finder(b_jet_finder_max_4jets);
	Wb_mass->set_max_njets(4);
}

MassReconstruction::~MassReconstruction()
{
	delete b_jet_finder;
	b_jet_finder = NULL;
	delete b_jet_finder_max_4jets;
	b_jet_finder_max_4jets = NULL;
	delete b_jet_finder_max_5jets;
	b_jet_finder_max_5jets = NULL;
 	if(m3_mass){
		delete m3_mass;
		m3_mass = NULL;
	}
	if(min_diff_mass){
		delete min_diff_mass;
		min_diff_mass = NULL;
	}
	if(chi2_mass){
		delete chi2_mass;
		chi2_mass = NULL;
	}
	if(Wb_mass){
		delete Wb_mass;
		Wb_mass = NULL;
	}

	delete b_jet_finder;
	b_jet_finder = NULL;
}

void MassReconstruction::set_tprime_mass(double mass)
{
	chi2_mass->set_tprime_mass(mass);
}

void MassReconstruction::next_event()
{
	m3_mass->next_event();
	min_diff_mass->next_event();
	chi2_mass->next_event();
	Wb_mass->next_event();
}

void MassReconstruction::set_handles(eire::HandleHolder *handle_holder)
{
	this->jets = handle_holder->get_tight_jets();
	this->electrons = handle_holder->get_tight_electrons();
	this->muons = handle_holder->get_tight_muons();
	this->mets = handle_holder->get_selected_mets();

	b_jet_finder->set_handles(handle_holder);
	b_jet_finder_max_4jets->set_handles(handle_holder);
	b_jet_finder_max_5jets->set_handles(handle_holder);

	m3_mass->set_handles(jets, electrons, muons, mets);
	min_diff_mass->set_handles(jets, electrons, muons, mets);
	chi2_mass->set_handles(jets, electrons, muons, mets);
	Wb_mass->set_handles(jets, electrons, muons, mets);
}

double MassReconstruction::calculate_M3()
{
	return m3_mass->get_hadT_mass();
}

double MassReconstruction::calculate_Wb_top()
{
	return Wb_mass->get_hadT_mass_2btag();
}

double MassReconstruction::calculate_Wb_lepTmass_top_2btag()
{
	return Wb_mass->get_lepT_mass_2btag();
}

double MassReconstruction::calculate_Wb_hadTmass_top_1btag()
{
	return Wb_mass->get_hadT_mass_1btag();
}

double MassReconstruction::calculate_Wb_hadWmass_1btag()
{
	return Wb_mass->get_hadW_mass_1btag();
}

double MassReconstruction::calculate_Wb_lepTmass_top_1btag()
{
	return Wb_mass->get_lepT_mass_1btag();
}

double MassReconstruction::calculate_Wb_hadTmass_top_0btag()
{
	return Wb_mass->get_hadT_mass();
}

double MassReconstruction::calculate_Wb_hadWmass_0btag()
{
	return Wb_mass->get_hadW_mass();
}

double MassReconstruction::calculate_Wb_lepTmass_top_0btag()
{
	return Wb_mass->get_lepT_mass();
}

double MassReconstruction::calculate_Wb_dR_W_bjet1()
{
	std::vector<int>* ids = Wb_mass->get_mass_jet_ids_2btag();

	if(ids->size() != 4)
		return -1;
	
	if((*ids)[1] == -1 || (*ids)[2] == -1 || (*ids)[0] == -1)
		return -1;

	ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > W_Had_candidate = (*jets)[(*ids)[1]].p4() + (*jets)[(*ids)[2]].p4();
	return ROOT::Math::VectorUtil::DeltaR(W_Had_candidate, (*jets)[(*ids)[0]].p4());
}

double MassReconstruction::calculate_Wb_dR_W_bjet2()
{
	std::vector<int>* ids = Wb_mass->get_mass_jet_ids_2btag();

	if(ids->size() != 4)
		return -1;
	
	if((*ids)[1] == -1 || (*ids)[2] == -1 || (*ids)[3] == -1)
		return -1;

	ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > W_Had_candidate = (*jets)[(*ids)[1]].p4() + (*jets)[(*ids)[2]].p4();
	return ROOT::Math::VectorUtil::DeltaR(W_Had_candidate, (*jets)[(*ids)[3]].p4());
}

double MassReconstruction::calculate_Wb_W_mass()
{
	return Wb_mass->get_hadW_mass_2btag();
}

double MassReconstruction::calculate_min_diff_M3()
{
	return min_diff_mass->get_hadT_mass();
}

double MassReconstruction::calculate_min_diff_lepTmass()
{
	return min_diff_mass->get_lepT_mass();
}

double MassReconstruction::calculate_minmass_1btag_top()
{
	return min_diff_mass->get_hadT_mass_1btag();
}

double MassReconstruction::calculate_minmass_2btag_top()
{
	return min_diff_mass->get_hadT_mass_2btag();
}

double MassReconstruction::calculate_chimass_1btag_top()
{
	return chi2_mass->get_hadT_mass_1btag(); 
}

double MassReconstruction::calculate_chimass_2btag_top()
{
	return chi2_mass->get_hadT_mass_2btag(); 
}

// Plot the invariant mass of the two jets which best corresponds to the nominal value for the W mass, set in plot()
double MassReconstruction::calculate_massW(double nominal_massW)
{
	int njets = jets->size();
	double current_dmass = 0.0;
	double min_dmass = -1;
	
	if(njets < 2)
	  return -1;	
	
	// Loop over all 2 jet combinations to see which has mass  closest to nominal_massW
	int jet_id2=-1,jet_id3=-1;
	for(int k=0;k < njets; ++k){
	  for(int j=0;j < njets; ++j){
	    if(k != j){
	      double current_mass = ((*jets)[k].p4()+ (*jets)[j].p4()).mass();
	      
	      current_dmass = fabs(current_mass - nominal_massW);
	      
	      if(min_dmass == -1){
	        jet_id3=k;
	        jet_id2=j;
	        min_dmass=current_dmass;
	      }	
	      else if(current_dmass < min_dmass){
	        jet_id3=k;
	        jet_id2=j;
	        min_dmass=current_dmass;
	        
	      }
	    }
	  }
	}
	
	return ((*jets)[jet_id2].p4() + (*jets)[jet_id3].p4()).mass();
}

double MassReconstruction::calculate_chihadTmass()
{
	return chi2_mass->get_hadT_mass();
}

double MassReconstruction::calculate_chihadWmass()
{
	return chi2_mass->get_hadW_mass();
}

double MassReconstruction::calculate_chilepTmass()
{
	return chi2_mass->get_lepT_mass();
}

double MassReconstruction::calculate_chilepWmass()
{
	return chi2_mass->get_lepW_mass();
}

double MassReconstruction::calculate_chihadTmass_1btag()
{
	return chi2_mass->get_hadT_mass_1btag();
}

double MassReconstruction::calculate_chihadWmass_1btag()
{
	return chi2_mass->get_hadW_mass_1btag();
}

double MassReconstruction::calculate_chilepTmass_1btag()
{
	return chi2_mass->get_lepT_mass_1btag();
}


double MassReconstruction::calculate_chilepWmass_1btag()
{
	return chi2_mass->get_lepW_mass_1btag();
}

double MassReconstruction::calculate_chihadTmass_2btag()
{
	return chi2_mass->get_hadT_mass_2btag();
}

double MassReconstruction::calculate_chihadWmass_2btag()
{
	return chi2_mass->get_hadW_mass_2btag();
}

double MassReconstruction::calculate_chilepTmass_2btag()
{
	return chi2_mass->get_lepT_mass_2btag();
}

double MassReconstruction::calculate_chilepWmass_2btag()
{
	return chi2_mass->get_lepW_mass_2btag();
}

//Find combination of jets which minimise chi² and use these to plot mass
double MassReconstruction::calculate_chimass()
{
	return chi2_mass->get_hadT_mass();
}

double MassReconstruction::get_chi2()
{
	return chi2_mass->get_min_chi2();
}

double MassReconstruction::get_chi2_1btag()
{
	return chi2_mass->get_min_chi2_1btag();
}

double MassReconstruction::get_chi2_2btag()
{
	return chi2_mass->get_min_chi2_2btag();
}

std::vector<int>* MassReconstruction::get_ids_chi2()
{
	return chi2_mass->get_mass_jet_ids();
}

std::vector<int>* MassReconstruction::get_ids_chi2_1btag()
{
	return chi2_mass->get_mass_jet_ids_1btag();
}

std::vector<int>* MassReconstruction::get_ids_chi2_2btag()
{
	return chi2_mass->get_mass_jet_ids_2btag();
}

std::vector<int>* MassReconstruction::get_ids_m3()
{
	return m3_mass->get_mass_jet_ids();
}

std::vector<int>* MassReconstruction::get_ids_min_diff_m3()
{
	return min_diff_mass->get_mass_jet_ids();
}

std::vector<int>* MassReconstruction::get_ids_min_diff_m3_1btag()
{
	return min_diff_mass->get_mass_jet_ids_1btag();
}

std::vector<int>* MassReconstruction::get_ids_min_diff_m3_2btag()
{
	return min_diff_mass->get_mass_jet_ids_2btag();
}

std::vector<int>* MassReconstruction::get_ids_Wb()
{
	return Wb_mass->get_mass_jet_ids_2btag();
}
