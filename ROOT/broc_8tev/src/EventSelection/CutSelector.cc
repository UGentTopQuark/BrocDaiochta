#include "../../interface/EventSelection/CutSelector.h"

CutSelector::CutSelector(eire::HandleHolder *handle_holder, eire::ConfigReader *config_reader)
{
	this->config_reader = config_reader;
	this->handle_holder = handle_holder;
	this->pdf_weight_prov = handle_holder->get_pdf_weight_provider();
	dataset_id = handle_holder->get_ident();
	this->genEvt = handle_holder->get_ttbar_gen_evt();

	event_counter_histo = handle_holder->get_histo_writer()->create_1d("event_counter", "event counter", 10, -0.5, 9.5);

	ttmu_count = 0,tte_count = 0,ttbg_count = 0,data_count=0;
	//To check if this event has already been called for different
	//cut.Reinitiliased and end of event in plot()
	Event_firstcall = true;
	process_all_pdf_sets = false;	// set in Analyser.cc, don't set it here
	pdf_set = "";			// which pdf set to process, set in Analyser.cc
	is_ttbar = false;
	is_data = false;
	dont_split_ttbar =  config_reader->get_bool_var("dont_split_ttbar","global",false);

	allowed_cuts_file = config_reader->get_var("allowed_cuts_file","global",true);
	allowed_vcuts_file = config_reader->get_var("allowed_vcuts_file","global",true);
	

	if(dataset_id == "Data")
		is_data = true;
	else if(dataset_id.find("TTbar") != std::string::npos)
		is_ttbar = true;
}

CutSelector::~CutSelector()
{
	/*
	 *	delete vector cuts
	 */
	for(std::map<std::string, std::map<std::string, broc::SelectionStep*> >::iterator type_iter = selection_steps.begin();
	    type_iter != selection_steps.end();
	    ++type_iter)
	{
        	for(std::map<std::string,broc::SelectionStep*>::iterator pgen_iter = type_iter->second.begin();
        	    pgen_iter != type_iter->second.end();
        	    ++pgen_iter){
			delete pgen_iter->second;
			pgen_iter->second=NULL;
		}
	}

	for(std::vector<broc::CutsSet*>::iterator cuts_set = cuts_sets.begin();
		cuts_set != cuts_sets.end();
		++cuts_set){
		delete *cuts_set;
	}

	for(std::vector<eire::HandleHolder*>::iterator hholder = handle_holders.begin();
		hholder != handle_holders.end();
		++hholder){
		delete *hholder;
	}

	//Print number of ttbar muon,electron and backgound
	std::cout << "=++++++++++Event-Count:"<< dataset_id <<"++++++++++=" << std::endl;
	std::cout << "ttbar_mu_events: " << std::setprecision(20) << ttmu_count << std::endl;
	std::cout << "ttbar_e_events: " << std::setprecision(20) << tte_count << std::endl;
	std::cout << "ttbar_bg_events: " << std::setprecision(20) << ttbg_count << std::endl;
	std::cout << "data_events: " << std::setprecision(20) << data_count << std::endl;
	std::cout << "=++++++++++++++++++++++++++++++=" << std::endl;
}

void CutSelector::set_all_cuts()
{
	define_cuts_autogen();
	if(process_all_pdf_sets) add_pdf_cutsets();	
	complete_cuts();
	set_cuts();
}

void CutSelector::plot()
{
	Event_firstcall = true;
 	for(std::map<std::string, std::map<std::string, broc::SelectionStep*> >::iterator type_iter = selection_steps.begin();
 	    type_iter != selection_steps.end();
 	    ++type_iter)
 	{
         	for(std::map<std::string,broc::SelectionStep*>::iterator pgen_iter = type_iter->second.begin();
         	    pgen_iter != type_iter->second.end();
         	    ++pgen_iter){
 			if((is_data && get_event_type() != "uncalled")|| 
			    (genEvt == NULL && get_event_type() != "uncalled")|| 
			   type_iter->first == get_event_type() ||
 			   type_iter->first == ("e_"+get_event_type()) ||
 			   type_iter->first == ("mu_"+get_event_type()) ||
 			   (type_iter->first == "e_background" && get_event_type() == "muon") ||
 			   (type_iter->first == "mu_background" && get_event_type() == "electron") ||
			   (dont_split_ttbar && get_event_type(type_iter->first) == "signal")){
 				pgen_iter->second->next_event();
			}
				
 		}
 	}
}

std::string CutSelector::get_event_type(std::string type_iter)
{
	if(is_data || genEvt == NULL)
		{
			if(!is_data && genEvt == NULL && Event_firstcall && data_count == 0 )
				std::cout << "***WARNING***: running over " << dataset_id << " without any genevt information " << std::endl;

			if(Event_firstcall == 1){
				event_counter_histo->Fill(4);
				data_count++;
				Event_firstcall = false;
			}
 
			return "unused_string";
		}
	else if(is_ttbar && !dont_split_ttbar){
		int decay_channel = genEvt->decay_channel();
		
		if(decay_channel == 2)
			{
				if (Event_firstcall == 1){
					event_counter_histo->Fill(1);
					ttmu_count++;
					Event_firstcall = false;
				}
				return "muon";
			}
		if(decay_channel == 1)
			{
				if (Event_firstcall == 1){
					event_counter_histo->Fill(2);
					tte_count++;
					Event_firstcall = false;
				}
				return "electron";
				
			}	  
		else
			{
				if (Event_firstcall == 1){
					event_counter_histo->Fill(3);
					ttbg_count++;
					Event_firstcall = false;
				}
				return "background";
			}
	}
	else if(is_ttbar && dont_split_ttbar){
		if(type_iter == "-1"){
			return "skip";
		}
		else if(type_iter == "muon"){
			if(Event_firstcall == 1){
					event_counter_histo->Fill(1);
					ttmu_count++;
					Event_firstcall = false;
				}
			return "signal";
		}
		else if(type_iter == "electron"){
			if(Event_firstcall == 1){
				event_counter_histo->Fill(2);
				tte_count++;
				Event_firstcall = false;
			}
			return "signal";
		}
		else{
			std::cerr << "***ERROR***: CutSelector: dont_split_ttbar is true but type_iter is neither muon nor electron: " << type_iter <<std::endl;
			exit(1);
		}
		
		
	}
	else
			{
				if (Event_firstcall == 1){
					event_counter_histo->Fill(3);
					ttbg_count++;
					Event_firstcall = false;
				}
				return "background";
			}
}
	
void CutSelector::define_cuts_autogen()
{
	// ---AUTOGENERATED BEGIN---
	// 04/04/2012 14:25 CUTSET_GENERATOR configuration: Jet_3_leg2012.txt
	// step: s01
	cut_defs["electron"]["01_cutset"]["hlt_reference_trigger"] = 1002;
	cut_defs["electron"]["01_cutset"]["hlt_cross_trigger"] = 1003;
	cut_defs["electron"]["01_cutset"]["min_npvertices"] = 1;
	cut_defs["electron"]["01_cutset"]["pvertex_min_ndof"] = 4;
	cut_defs["electron"]["01_cutset"]["pvertex_max_rho"] = 2.0;
	cut_defs["electron"]["01_cutset"]["pvertex_not_fake"] = 1;
	cut_defs["electron"]["01_cutset"]["pvertex_max_z"] = 24.0;
	cut_defs["electron"]["01_cutset"]["min_nisolated_e"] = 1;
	cut_defs["electron"]["01_cutset"]["max_nisolated_e"] = 1;
	vcuts("electron","01_cutset","max_e_vz", 1.);
	vcuts("electron","01_cutset","min_e_et", 25.);
	vcuts("electron","01_cutset","max_e_eta", 1.5);
	cut_defs["electron"]["01_cutset"]["e_exclude_eta_crack"] = 1;
	vcuts("electron","01_cutset","max_e_d0", 0.02);
	vcuts("electron","01_cutset","e_electronID", 20);
	vcuts("electron","01_cutset","max_e_PFrelIso", 0.1);
	cut_defs["electron"]["01_cutset"]["min_nisolated_mu"] = 0;
	cut_defs["electron"]["01_cutset"]["max_nisolated_mu"] = 0;
	cut_defs["electron"]["01_cutset"]["mu_type"] = 0;
	vcuts("electron","01_cutset","min_mu_pt", 20.);
	vcuts("electron","01_cutset","max_mu_eta", 2.5);
	vcuts("electron","01_cutset","max_mu_PFrelIso", 0.2);
	cut_defs["electron"]["01_cutset"]["max_nloose_e"] = 1;
	vcuts("electron","01_cutset","min_loose_e_et", 20);
	vcuts("electron","01_cutset","max_loose_e_eta", 2.5);
	cut_defs["electron"]["01_cutset"]["loose_e_exclude_eta_crack"] = 1;
	vcuts("electron","01_cutset","loose_e_electronID", 16);
	vcuts("electron","01_cutset","max_loose_e_PFrelIso", 0.2);
	vcuts("electron","01_cutset","max_e_nLostTrackerHits", 0.);
	cut_defs["electron"]["01_cutset"]["max_e_dcot"] = 0.02;
	cut_defs["electron"]["01_cutset"]["max_e_dist"] = 0.02;
	vcuts("electron","01_cutset","min_e_dR", 0.3);
	vcuts("electron","01_cutset","min_jet_pt", 30);
	cut_defs["electron"]["01_cutset"]["max_jet_eta"] = 2.4;
	cut_defs["electron"]["01_cutset"]["min_jet_nconstituents"] = 1;
	cut_defs["electron"]["01_cutset"]["min_jet_chf"] = 0.;
	cut_defs["electron"]["01_cutset"]["max_jet_nhf"] = 0.99;
	cut_defs["electron"]["01_cutset"]["max_jet_nemf"] = 0.99;
	cut_defs["electron"]["01_cutset"]["max_jet_cemf"] = 0.99;
	cut_defs["electron"]["01_cutset"]["min_jet_cmulti"] = 0.;
	cut_defs["electron"]["01_cutset"]["min_no_jets"] = 3;

	// ---AUTOGENERATED END---

        /*
         *      global cuts for all selections defined so far
         */
        synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
		for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
			{
				if(type_iter->first == "muon" || type_iter->first == "mu_background"){
				}else if(type_iter->first == "electron" || type_iter->first == "e_background"){
				}
			}
	}
}

void CutSelector::set_if_not_set(std::string type, std::string set, std::string cut, double value)
{
	if(cut_defs[type][set].find(cut) == cut_defs[type][set].end()){
		cut_defs[type][set][cut] = value;	
	}
}

void CutSelector::vset_if_not_set(std::string type, std::string set, std::string cut, double value)
{
	if(vcut_defs[type][set].find(cut) == vcut_defs[type][set].end()){
		vcuts(type, set, cut, value);	
	}
}

void CutSelector::vset_if_not_set(std::string type, std::string set, std::string cut, std::vector<double> *value)
{
	if(vcut_defs[type][set].find(cut) == vcut_defs[type][set].end()){
		vcuts(type, set, cut, value);	
	}
}

//In case a cutset contains only vcuts and not cut_defs this stops program from crashing
void CutSelector::synchronise_maps()
{
        for(std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > >::iterator type_iter = vcut_defs.begin();
                type_iter != vcut_defs.end();
                ++type_iter)
        {
		for(std::map<std::string,std::map<std::string, std::vector<double>* > >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
		{
			cut_defs[type_iter->first][set_iter->first]["dummy"] = -1;
		}
	}
	
        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
	    type_iter != cut_defs.end();
	    ++type_iter)
	{
	  for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
		{
			vcuts(type_iter->first, set_iter->first, "dummy", -1);
		}
	}
}

void CutSelector::vcuts(std::string type, std::string set, std::string cut, double value)
{
	std::vector<double> *cut_vector = new std::vector<double>();
	if(value != -1)
		cut_vector->push_back(value);
	vcut_defs[type][set][cut] = cut_vector;	
	cuts_to_be_deleted.push_back(cut_vector);
}

void CutSelector::vcuts(std::string type, std::string set, std::string cut, std::vector<double> *cut_vector)
{
	vcut_defs[type][set][cut] = cut_vector;
}

void CutSelector::complete_cuts()
{
	eire::ConfigReader allowed_cuts_reader;
	allowed_cuts_reader.read_list_from_file(allowed_cuts_file);
	std::vector<std::string> all_cuts(*(allowed_cuts_reader.get_list()));
	allowed_cuts_reader.read_list_from_file(allowed_vcuts_file);
	std::vector<std::string> all_v_cuts(*(allowed_cuts_reader.get_list()));

	synchronise_maps();

        for(std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > >::iterator type_iter = vcut_defs.begin();
                type_iter != vcut_defs.end();
                ++type_iter)
        {
		for(std::map<std::string,std::map<std::string, std::vector<double>* > >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
		{
			for(std::vector<std::string>::iterator cut_name = all_v_cuts.begin();
			    cut_name != all_v_cuts.end();
			    ++cut_name){
				if(set_iter->second.find(*cut_name) == set_iter->second.end()){
					vcuts(type_iter->first, set_iter->first, *cut_name, -1);
				}
			}
		}
		
	}
	
	
        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
                type_iter != cut_defs.end();
                ++type_iter)
        {
		
		for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
			{
				for(std::vector<std::string>::iterator cut_name = all_cuts.begin();
				    cut_name != all_cuts.end();
				    ++cut_name){
					if(set_iter->second.find(*cut_name) == set_iter->second.end()){
						set_iter->second[*cut_name] = -1;
					}
				}
			}
		
	}
}

//type_iter->first = muon/electron, set_iter->first = ??_cutset
void CutSelector::set_cuts()
{
        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = cut_defs.begin();
	    type_iter != cut_defs.end();
	    ++type_iter){
		
		//If no _background cuts set, and not running over Data, set _background cuts = signal cuts
		std::vector<std::string> event_types = get_event_types(type_iter->first);			
		for(std::vector<std::string>::iterator event_type_iter=event_types.begin();event_type_iter != event_types.end();++event_type_iter){
			for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
			    set_iter != type_iter->second.end();
			    ++set_iter){
				
				std::string full_id = dataset_id+"|"+*event_type_iter+"|"+set_iter->first;
				std::string id = dataset_id+"_"+set_iter->first;

				// create cuts set object and fill map to it
				broc::CutsSet *new_cuts_set = new broc::CutsSet();
				new_cuts_set->set_cuts(set_iter->second);
				new_cuts_set->set_cuts(vcut_defs[type_iter->first][set_iter->first]);
				cuts_sets.push_back(new_cuts_set);

				eire::HandleHolder *new_holder = new eire::HandleHolder(*(this->handle_holder));
				new_holder->set_ident(full_id);
				new_holder->set_cuts_set(new_cuts_set);
				handle_holders.push_back(new_holder);	// keep track of the copies and delete them in destructor

				selection_steps[*event_type_iter][id] = new broc::SelectionStep(new_cuts_set, new_holder);
			}
		}
	}
}
	
//Returns what goes between the | | in histo_names and defines for what channels cuts should be set
std::vector<std::string> CutSelector::get_event_types(std::string event_type)
{
	std::vector<std::string> event_types;
	//If no _background cuts set, and not running over Data, set _background cuts = signal cuts
	if(!is_data){
		if(!is_ttbar){
			if(event_type == "muon")
				event_types.push_back("mu_background");
			else
				event_types.push_back("e_background");
			
		}
		else if(!dont_split_ttbar && event_type == "muon" && cut_defs.find("mu_background") == cut_defs.end()){ 
			event_types.push_back(event_type);
			event_types.push_back("mu_background");
		}
		else if(!dont_split_ttbar && event_type == "electron" && cut_defs.find("e_background") == cut_defs.end()){
			event_types.push_back(event_type);
				event_types.push_back("e_background");
		}
		else 
			event_types.push_back(event_type);
	}
	else 
		event_types.push_back(event_type);
	
	return event_types;

}

void CutSelector::add_pdf_cutsets()
{
	//loop over all cutsets add add extra cutsets for each pdf weight available
	std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > > tmp_vcut_defs = vcut_defs;
        for(std::map<std::string, std::map<std::string,std::map<std::string, std::vector<double>* > > >::iterator type_iter = tmp_vcut_defs.begin();
                type_iter != tmp_vcut_defs.end();
                ++type_iter)
        {

		for(std::map<std::string,std::map<std::string, std::vector<double>* > >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
			{
				set_pdf_cutsets(type_iter->first, set_iter->first,set_iter->second);
			}
		
	}

	std::map<std::string, std::map<std::string,std::map<std::string, double> > > tmp_cut_defs = cut_defs;
        for(std::map<std::string, std::map<std::string,std::map<std::string, double> > >::iterator type_iter = tmp_cut_defs.begin();
                type_iter != tmp_cut_defs.end();
                ++type_iter)
        {

		for(std::map<std::string,std::map<std::string, double> >::iterator set_iter=type_iter->second.begin();
		    set_iter != type_iter->second.end();
		    ++set_iter)
			{
			
				set_pdf_cutsets(type_iter->first, set_iter->first,set_iter->second);	
			}
		
	}
}

//Returns vec<cutsets<map<cut_name,cut val> >
void CutSelector::set_pdf_cutsets(std::string type, std::string cutset,std::map<std::string, std::vector<double>* > cuts_set)
{	
	if(pdf_weight_prov != NULL){
	        int npdfs = pdf_weight_prov->get_nevt_weights(pdf_set);
		for(int i = 0;i < npdfs;i++){
			
			std::map<std::string,std::vector<double>* > cut_map = cuts_set;
			
			//convert double to string
			std::stringstream i_str;
			i_str  << i;
			std::string cutset_name;
			if(i < 10)
				cutset_name = cutset+"_0"+i_str.str();
			else 
				cutset_name = cutset+"_"+i_str.str(); 

			vcut_defs[type][cutset_name] = cut_map;
		}
	}
	else if(pdf_weight_prov == NULL)
		std::cout << "ERROR: unable to weight events. pdf_weight_prov == NULL " << std::endl;

}


//Returns vec<cutsets<map<cut_name,cut val> >
void CutSelector::set_pdf_cutsets(std::string type, std::string cutset,std::map<std::string, double> cuts_set)
{
	
	if(pdf_weight_prov != NULL){
	        int npdfs = pdf_weight_prov->get_nevt_weights(pdf_set);
		for(int i = 0;i < npdfs;i++){
			
			std::map<std::string,double> cut_map = cuts_set;
			cut_map["pdf_weight_pos"] = i;	
	
			//convert double to string
			std::stringstream i_str;
			i_str  << i;
			std::string cutset_name;
			if(i < 10)
				cutset_name = cutset+"_0"+i_str.str();
			else 
				cutset_name = cutset+"_"+i_str.str(); 

			cut_defs[type][cutset_name] = cut_map;
		}
	}
	else if(pdf_weight_prov == NULL)
		std::cout << "ERROR: unable to weight events. pdf_weight_prov == NULL " << std::endl;

}

void CutSelector::set_process_all_pdf_sets(bool process_all_pdf_sets, std::string pdf_set)
{
	this->process_all_pdf_sets = process_all_pdf_sets;
	this->pdf_set = pdf_set;
}
