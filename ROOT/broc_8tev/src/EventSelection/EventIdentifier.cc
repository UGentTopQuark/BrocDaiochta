#include "../../interface/EventSelection/EventIdentifier.h"

EventIdentifier::EventIdentifier(std::string ident)
{
	id = ident;

	mass_reco = NULL;
	jets = NULL;
	isolated_electrons = NULL;
	isolated_muons = NULL;
	corrected_mets = NULL;
	event_information = NULL;
	handle_holder = NULL;
}
EventIdentifier::EventIdentifier(eire::HandleHolder *handle_holder)
{
        id=handle_holder->get_ident();
	this->handle_holder = handle_holder;
	//Initialise when/if you need them.
	mass_reco = NULL;
	jets = NULL;
	isolated_electrons = NULL;
	isolated_muons = NULL;
	corrected_mets = NULL;
	event_information = NULL;
	handle_holder = NULL;
}

EventIdentifier::~EventIdentifier()
{
}

void EventIdentifier::set_handles(std::vector<mor::Jet>* jets,
		 		  std::vector<mor::Electron>* isolated_electrons,
		 		  std::vector<mor::Muon>* isolated_muons,
				  std::vector<mor::Electron>* loose_electrons,
				  std::vector<mor::Muon>* loose_muons,
		 		  std::vector<mor::MET>* corrected_mets,
		 		  std::vector<mor::PrimaryVertex>* pvertices,
				  mor::EventInformation *event_information)
{
	this->jets = jets;
	this->isolated_electrons = isolated_electrons;
	this->isolated_muons = isolated_muons;
	this->loose_electrons = loose_electrons;
	this->loose_muons = loose_muons;
	this->corrected_mets = corrected_mets;
	this->pvertices = pvertices;
	this->event_information = event_information;
}

void EventIdentifier::print_event_id()
{
	std::cout 
		<< event_information->run() << ","
		<< std::setprecision(10) << event_information->event_number() << ","
		<< event_information->lumi_block()
		<< std::endl;
}

void EventIdentifier::print()
{
	event_information = handle_holder->get_event_information();
	std::vector<mor::Electron> *electrons = handle_holder->get_electrons();
	
	/*
        std::cout << "run: " << handle_holder->get_event_information()->run() << " lumi: " << handle_hol\
der->get_event_information()->lumi_block() << " event: " << std::setprecision(10) << handle_holder->get_\
event_information()->event_number() << std::endl;

	if(event_information->event_number() == 11982324){
		jets = handle_holder->get_tight_jets();
		print_jets();
		// std::cout << "loose" <<std::endl;
		// loose_electrons = handle_holder->get_loose_electrons();
		// for(std::vector<mor::Electron>::iterator le = loose_electrons->begin();
		//     le != loose_electrons->end();
		//     ++le){				     
		// 	std::cout << "Et: " << le->Et() << " eta: " << le->Eta(); 
		// 	double relIso = (le->chargedHadronIso() + le->neutralHadronIso() + le->photonIso())/le->Pt();
		// 	std::cout << " PFrelIso: " << relIso << std::endl;
		// }
		// std::cout << "iso" << std::endl;
		// isolated_electrons = handle_holder->get_tight_electrons();
		// for(std::vector<mor::Electron>::iterator te = isolated_electrons->begin();
		//     te != isolated_electrons->end();
		//     ++te){				     
		// 	std::cout << "Et: " << te->Et() << " eta: " << te->Eta(); 
		// 	double relIso = (te->chargedHadronIso() + te->neutralHadronIso() + te->photonIso())/te->Pt();
		// 	std::cout << " PFrelIso: " << relIso << std::endl;
		// }
	}
	*/

//	print_event_id();
// 	if(event_information->event_number() == 123616234 ||
// 		event_information->event_number() == 137395924 ||
// 		event_information->event_number() == 284857877 ||
// 		event_information->event_number() == 173677912 ||
// 		event_information->event_number() == 96350597){
// 		print_muons();		
// 	}
	///	std::cout << event_information->run() << ":" << event_information->event_number() << ":" << event_information->lumi_block()  << ":" << std::setprecision(8) << isolated_muons->begin()->Pt() << std::endl;
//	std::cout << "PASSED: " << std::setprecision(20) <<  event_information->run() << ":" << event_information->event_number() << ":" << event_information->lumi_block()  << std::endl;
	//std::cout << std::setprecision(8) <<  event_information->run() << ":" << event_information->event_number() << ":" << event_information->lumi_block()  << ":" << isolated_electrons->begin()->Pt() << ":" <<isolated_electrons->begin()->nLostTrackerHits() << std::endl;
//	std::cout << event_information->run() << ":" << event_information->event_number() << ":" << event_information->lumi_block()  << ":" << std::setprecision(8) << isolated_muons->begin()->Pt() << ":" <<  loose_muons->begin()->Pt()  << ":" << isolated_muons->begin()->charge() << ":" << loose_muons->begin()->charge() <<  std::endl;
	//print_event_id();
	/*
	if(event_information->event_number() == 397643021 && event_information->run() == 143833 &&
		event_information->lumi_block() == 377){
		std::cout << "nvertices: " << pvertices->size() << std::endl;
		for(std::vector<mor::PrimaryVertex>::iterator vertex = pvertices->begin();
			vertex != pvertices->end();
			++vertex){
			std::cout << "vertex z: " << vertex->z() << std::endl;
			std::cout << "vertex rho: " << vertex->rho() << std::endl;
			std::cout << "is fake: " << vertex->is_fake() << std::endl;
			std::cout << "ndof: " << vertex->ndof() << std::endl;
		}
	}
	*/

	/*	if(event_information->event_number() == 408148 || event_information->event_number() == 408617 ||
		event_information->event_number() == 468236 || event_information->event_number() == 468387 || 
		event_information->event_number() == 468666 || event_information->event_number() == 901849 ||
		event_information->event_number() == 901994 || event_information->event_number() == 928652 ||
		event_information->event_number() == 1906737){
		if(event_information->event_number() == 1906737) std::cout << "TWO ONLY" << std::endl;
		//print_muons(); 
		//print_jets();

*/

		//std::cout << "N electrons: " << isolated_electrons->size() << std::endl;
		for(std::vector<mor::Electron>::iterator electron = electrons->begin();
		    electron != electrons->end();
		    ++electron){

			/*
			std::cout << "electron eta: " << electron->eta() << std::endl;
			std::cout << "electron sc eta: " << electron->supercluster_eta() << std::endl;
			std::cout << "electron phi: " << electron->phi() << std::endl;
			std::cout << "electron sc phi: " << electron->supercluster_phi() << std::endl;
			*/
		    
			if(electron->lepton_id_passed("eidHyperTight1MC") != electron->lepton_id_passed("eidHyperTight4MC")){
				std::cout << "HYPERTIGHT1 != HYPERTIGHT4" << std::endl;
				print_event_id();
				++one_ne_four;
			}

			if(electron->lepton_id_passed("eidHyperTight1MC") && !electron->lepton_id_passed("eidHyperTight4MC"))
				++one_not_four;

			if(!electron->lepton_id_passed("eidHyperTight1MC") && electron->lepton_id_passed("eidHyperTight4MC"))
				++four_not_one;

		//	if(electron->Et() < 30. ||
		//	   (electron->eta() > 2.5 || (electron->supercluster_eta() < 1.5660 && electron->supercluster_eta() > 1.4442)) ||
		//	   electron->d0() > 0.02 ||
		//	   !electron->lepton_id_passed("simpleEleId70") ||
		//	   (electron->trackIso() + electron->ecalIso() + electron->hcalIso())/electron->Et() > 0.1
		//	   ){
		//	   	std::cout << "WRONG ELECTRON FOUND" << std::endl;
		//	   
		//	std::cout << "ELECTRON FOUND" << std::endl;
		//	std::cout << "pt: " << electron->Pt() << std::endl;
		//	std::cout << "et: " << electron->Et() << " eta: " << electron->eta() << " phi: " << electron->phi() << std::endl;
		//	std::cout << "d0: " << electron->d0() << " relIso: " << (electron->ecalIso() + electron->hcalIso() + electron->trackIso())/electron->Et() << std::endl;
		//	std::cout << "eID: " << electron->lepton_id_passed("simpleEleId70") << std::endl;
		}
		/*

		std::cout << "---" << std::endl;
		}*/

// 		std::cout << "ELECTRONS FOUND " << isolated_electrons->size() << std::endl;
// 		for(std::vector<mor::Electron>::iterator electron = isolated_electrons->begin();
// 		    electron != isolated_electrons->end();
// 		    ++electron){
// 			if( electron->nLostTrackerHits() != 0){
// 			std::cout << "pt: " << electron->Pt() << std::endl;
// 			std::cout << "et: " << electron->Et() << " eta: " << electron->eta() << " phi: " << electron->phi() << std::endl;
// 			std::cout << "d0: " << electron->d0() << " relIso: " << (electron->ecalIso() + electron->hcalIso() + electron->trackIso())/electron->Et() << std::endl;
// 			std::cout << "eID: " << electron->lepton_id_passed("simpleEleId70") << std::endl;
// 			std::cout << "nlosthits: " << electron->nLostTrackerHits() << std::endl;
// 			}
// 		}
// 		std::cout << "---" << std::endl;
	/*
	*/
	//}

	//print_mass();
//	if(corrected_mets->begin() != corrected_mets->end()){
//		if(corrected_mets->begin()->Et() > 200.){
//		if(muon->relIso() < 0.05){
/*
		if(event_information->run() == 140124)// && (event_information->event_number() == 625751913 || event_information->event_number() == 624975349)){
*/
//		if(event_information->run() == 140126 && event_information->event_number() == 581556327)
			
			//}
//		}
//	}
//		}
//	}


// 	double Z_mass = 90;
// 	double Z_width = 30;
// 	if(isolated_muons->size() >= 1 && loose_muons->size() >= 1){
// 		for(std::vector<mor::Muon>::iterator muon1 = isolated_muons->begin();
// 		    muon1 != isolated_muons->end();
// 		    ++muon1)
// 		{
// 			for(std::vector<mor::Muon>::iterator muon2 =
// 				loose_muons->begin();
// 			    muon2 != loose_muons->end();
// 			    ++muon2)
// 			{
// 				// don't use the same lepton twice
// 				if(muon1->Pt() == muon2->Pt() &&
// 					muon1->Eta() == muon2->Eta())
// 					continue;

// 				double curr_Z_mass = (muon1->p4() + muon2->p4()).mass();
// 				if((muon1->charge() != muon2->charge()) && (fabs(curr_Z_mass - Z_mass) < Z_width)){
// 					double curr_Z_pt = (muon1->p4() + muon2->p4()).pt();
// 					double curr_Z_eta = (muon1->p4() + muon2->p4()).eta();
// 					double curr_Z_phi = (muon1->p4() + muon2->p4()).phi();
// 					/*
// 					printf("* * %9.0f * %9.0f * %9.0f * %9.6f * %9.6f * %9.6f * %9.6f * %9.6f * %9.6f *\n",
// 						event_information->run(),
// 						event_information->lumi_block(),
// 						event_information->event_number(),
// 						curr_Z_mass,
// 						curr_Z_pt,
// 						muon1->eta(),
// 						muon2->eta(),
// 						muon1->pt(),
// 						muon2->pt());
// 					*/
// 					printf("%9.f %7.f %12.f %8.3f %8.3f %8.3f %8.3f\n",
// 						event_information->run(),
// 						event_information->lumi_block(),
// 						event_information->event_number(),
// 						curr_Z_mass,
// 						curr_Z_pt,
// 						curr_Z_eta,
// 						curr_Z_phi);

// 					return;

// 				}
// 			}
// 		}
// 	}
	
}


void EventIdentifier::print_muons()
{
	std::cout << "N muons: " << isolated_muons->size() << std::endl;
	int nmuon = 0;
	for(std::vector<mor::Muon>::iterator muon = isolated_muons->begin();
	    muon != isolated_muons->end();
	    ++muon){
		std::cout << "muon " << nmuon << std::endl;
		std::cout << "pt: " << muon->pt() << " eta: " << muon->eta() << " phi: " << muon->phi() << std::endl;
		std::cout << "chi2: " << muon->chi2() << " nHits: " << muon->nHits() << " nMuonHits: " << muon->nMuonHits() << std::endl;
		std::cout << "d0: " << muon->d0() << " relIso: " << (muon->ecalIso() + muon->hcalIso() + muon->trackIso())/muon->Pt() << std::endl;
		std::cout << "layerswithmeasurement: " << muon->nPixelLayers() << " matchedsegments: " << muon->nMatchedSegments() << std::endl;
		for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();
		    jet_iter != jets->end();
			++jet_iter)
		{
			std::cout << "dR: " << ROOT::Math::VectorUtil::DeltaR(muon->p4(), jet_iter->p4()) << std::endl;
		}
		std::cout << "pvz - vz: " << (*pvertices)[0].z() - muon->vz() << std::endl;
		std::cout << "globalmuon: " << muon->lepton_id_passed("AllGlobalMuons") << std::endl;
		std::cout << "trackermuon: " << muon->lepton_id_passed("AllTrackerMuons") << std::endl;
		std::cout << "--------------------------" << std::endl;
		++nmuon;
	}
	std::cout << "========================" << std::endl;

}

void EventIdentifier::print_jets()
{
	std::cout << "N jets: " << jets->size() << std::endl;
	for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();
	    jet_iter != jets->end();
		++jet_iter)
	{
		std::cout << "pt: " << jet_iter->pt() << " eta: " << jet_iter->eta() << " phi: " << jet_iter->phi() << std::endl;
		//	std::cout << "emf: " << jet_iter->emf() << " n90Hits: " << jet_iter->n90Hits() << " fHPD: " << jet_iter->fHPD() << std::endl;
		std::cout << "nDaughters: " << jet_iter->nconstituents() <<" chf: " << jet_iter->chf();
		std::cout << " nhf: " << jet_iter->nhf() << " nemf: " << jet_iter->nemf();
		std::cout << " cemf: " << jet_iter->cemf() << " cmulti: " << jet_iter->cmulti() <<std::endl;
	}

}
void EventIdentifier::print_mass()
{
	if(mass_reco == NULL){
		std::cerr << "WARNING: no mass rco object found in EventIdentifier::plot_mass()" << std::endl;
		return;
	}
	double m3 = mass_reco->calculate_M3();
	std::cout << "M3 mass: " << m3 << std::endl;

}

void EventIdentifier::set_mass_reco(MassReconstruction *mass_reco)
{
	this->mass_reco = mass_reco;
}
