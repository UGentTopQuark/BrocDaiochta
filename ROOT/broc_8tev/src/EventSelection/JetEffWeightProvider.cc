#include "../../interface/EventSelection/JetEffWeightProvider.h"

broc::JetEffWeightProvider::JetEffWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	jets = handle_holder->get_tight_jets();
}

broc::JetEffWeightProvider::~JetEffWeightProvider()
{
}

double broc::JetEffWeightProvider::get_weight()
{
	double value = -999;
	double weight = 1.;
	//if(jets->size() >= 3)
	//	value = (*jets)[2].pt();

	if(do_not_reweight)
		weight = 1.;
	else{
		// FIXME: reweight only for the last jet
		if(jets->size() < 1) return 1.;
		int max_njet = 3;
		if(jets->size() < 3) max_njet = jets->size();
		// FIXME: reweight only for the last jet: njet = MAX_NJET;
		for(int njet = max_njet; njet < max_njet; ++njet){	// reweight for the first 3 jets
			value = (*jets)[njet].pt();
			if(value == -999.)
				weight = 1.;
			else{
				if(value > 45.)	// ignore jets with pt > 45 GeV
					weight *= 1.;
				else{
					if(value < 30.)
						weight *= 0.;
					else if(value < 32.5)
						weight *= 0.867;
					else if(value < 35.)
						weight *= 0.936;
					else if(value < 37.5)
						weight *= 0.974;
					else
						weight *= 0.992;
				}
			}
		}
	}

	return weight;
}
