#include "../../interface/EventSelection/JetSelector.h"

/*************************************************************************
Jets which are in returned vector:
If eta not set; Jets which pass pt cut + all other jets.
If eta set; Jets which pass pt and eta cut + other jets which pass eta cut.

If one of first few jets passes pt cut but not eta it will be discarded and the same cut will be applied to the next mor::Jet  
***************************************************************************/

template bool JetSelector::cut_leptons(mor::Jet *jet_iter, std::vector<mor::Electron> *leptons, double min_dR);
template bool JetSelector::cut_leptons(mor::Jet *jet_iter, std::vector<mor::Muon> *leptons, double min_dR);

JetSelector::JetSelector(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;

	cut_jets = NULL;
	min_pt = NULL;
	max_pt = NULL;
	max_eta = -1;
	min_e_dR = -1;
	min_mu_dR = -1;
	max_fHPD = -1;
	min_n90Hits = -1;
	min_emf = -1;
	min_nconstituents = -1;	// number of constituents
	min_chf = -1;		// charged hardron energy fraction
	max_nhf = -1;		// neutral hardon energy fraction
	max_nemf = -1;		// neutral em energy fraction
	max_cemf = -1;		// charged em energy fraction
	min_cmulti = -1;	// charged multiplicity
	study_jes_unc = -1;
	cut_jets = new std::vector<mor::Jet>();
	jecUnc = NULL;
	jer_corrector = new broc::JERCorrector();

	suppress_JER_bias_correction = false;

	if(handle_holder->get_ident().find("Data") != std::string::npos)
		is_real_data = true;
	else
		is_real_data = false;
	//is_real_data = handle_holder->get_event_information()->is_real_data();
}

JetSelector::~JetSelector()
{
	if(jecUnc){ delete jecUnc; jecUnc = NULL; }
	if(cut_jets){ delete cut_jets; cut_jets = NULL; }
	if(jer_corrector){ delete jer_corrector; jer_corrector = NULL; }
}

void JetSelector::initialise_jec_uncertainty(eire::ConfigReader *config_reader)
{
	suppress_JER_bias_correction = config_reader->get_bool_var("suppress_JER_bias_correction","global",false);
	std::string JEC_uncertainty_file = config_reader->get_var("JEC_uncertainties","global",true);
	std::string JEC_dir = config_reader->get_var("JEC_dir","global",true);
	std::string full_path = JEC_dir+"/"+JEC_uncertainty_file;
	if(JEC_uncertainty_file.size() > 0){
		jecUnc = new JetCorrectionUncertainty(full_path);
	}
}

void JetSelector::set_cuts_set(broc::CutsSet *cuts_set)
{
	max_eta = cuts_set->get_cut_value("max_jet_eta");
	min_pt = cuts_set->get_vcut_value("min_jet_pt");
	max_pt = cuts_set->get_vcut_value("max_jet_pt");
	min_e_dR = cuts_set->get_cut_value("min_jet_e_dR");
	min_mu_dR = cuts_set->get_cut_value("min_jet_mu_dR");

	study_jes_unc = cuts_set->get_cut_value("study_jes_unc");
	study_jer_unc = cuts_set->get_cut_value("study_jer_unc");
	
	// calo & jpt jets
	max_fHPD = cuts_set->get_cut_value("max_jet_fHPD");
	min_n90Hits = cuts_set->get_cut_value("min_jet_n90Hits");
	min_emf = cuts_set->get_cut_value("min_jet_emf");

	// pf jets
	min_nconstituents = cuts_set->get_cut_value("min_jet_nconstituents");	// number of constituents
	min_chf = cuts_set->get_cut_value("min_jet_chf");		// charged hardron energy fraction
	max_nhf = cuts_set->get_cut_value("max_jet_nhf");		// neutral hardon energy fraction
	max_nemf = cuts_set->get_cut_value("max_jet_nemf");		// neutral em energy fraction
	max_cemf = cuts_set->get_cut_value("max_jet_cemf");		// charged em energy fraction
	min_cmulti = cuts_set->get_cut_value("min_jet_cmulti");	// charged multiplicity
}

// Here the functions for any cuts which have been set are called
std::vector<mor::Jet>* JetSelector::get_jets(std::vector<mor::Jet> *uncut_jets, std::vector<mor::Muon> *muons, std::vector<mor::Electron> *electrons)
{
	cut_jets->clear();
	std::vector<mor::Jet> unsorted_jets;

	PtSorter<mor::Jet> jet_sorter;
	std::vector<mor::Jet*> jets;

	// fill all jets to jets vector
	for(std::vector<mor::Jet>::iterator jet_iter = uncut_jets->begin();
	    jet_iter!=uncut_jets->end();
	    ++jet_iter){
	 
	  	mor::Jet *tmp_jet = new mor::Jet(*jet_iter);

		// do JER smearing and bias correction if necessary
		if(!is_real_data && !suppress_JER_bias_correction) jer_corrector->correctJER(tmp_jet, study_jer_unc);

		if(study_jes_unc != 0 && study_jes_unc != -1){
			jecUnc->setJetEta(tmp_jet->eta());
			jecUnc->setJetPt(tmp_jet->pt());
			double unc = 0;
			if(study_jes_unc == 1){
				unc = jecUnc->getUncertainty(false);
			}else if(study_jes_unc == 2){
				unc = jecUnc->getUncertainty(true);
			}

			// cf. recommendations TopPAG
			// https://twiki.cern.ch/twiki/bin/viewauth/CMS/TopLeptonPlusJets2010Systematics
			//unc = sqrt(unc * unc + 0.053 * 0.053);

			double pt_scale_unc = 0;
			if(study_jes_unc == 2){
				pt_scale_unc = 1. + unc;
			}else if(study_jes_unc == 1){
				pt_scale_unc = 1. - unc;
			}

			tmp_jet->SetPt(jet_iter->Pt());
			tmp_jet->SetEta(jet_iter->eta());
			tmp_jet->SetPhi(jet_iter->Phi());
			tmp_jet->SetM(jet_iter->mass());
			*tmp_jet *= pt_scale_unc;
		}

		jets.push_back(tmp_jet);
	}

	if((min_pt != NULL && min_pt->size() >1) ||
		(max_pt != NULL && max_pt->size() > 1)){
	  
		double max_size = min_pt->size();
		
		for(unsigned int nasym_cut = 0; nasym_cut < max_size; ++nasym_cut){
			for(std::vector<mor::Jet*>::iterator jet_iter =
			jets.begin();
			jet_iter!=jets.end();
			){
				double cut_out=false;
				     
				if(min_pt != NULL && min_pt->size() > nasym_cut && (*min_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_pt(*jet_iter, (*min_pt)[nasym_cut]);
				if(max_pt != NULL && max_pt->size() > nasym_cut && (*max_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_max_pt(*jet_iter, (*max_pt)[nasym_cut]);
				if(max_eta != -1)
					cut_out = cut_out || cut_eta(*jet_iter, max_eta);
				if(electrons->size() > 0 && min_e_dR != -1)
					cut_out = cut_out || cut_leptons(*jet_iter, electrons, min_e_dR);
				if(muons->size() > 0 && min_mu_dR != -1)
					cut_out = cut_out || cut_leptons(*jet_iter, muons, min_mu_dR);
				if(max_fHPD != -1) cut_out = cut_out || cut_max_fHPD(*jet_iter,max_fHPD);
				if(min_n90Hits != -1) cut_out = cut_out || cut_min_n90Hits(*jet_iter, min_n90Hits);
				if(min_emf != -1) cut_out = cut_out || cut_min_emf(*jet_iter, min_emf);
				if(min_nconstituents != -1) cut_out = cut_out || cut_min_nconstituents(*jet_iter, min_nconstituents);
				if(min_chf != -1) cut_out = cut_out || cut_min_chf(*jet_iter, min_chf);
				if(max_nhf != -1) cut_out = cut_out || cut_max_nhf(*jet_iter, max_nhf);
				if(max_nemf != -1) cut_out = cut_out || cut_max_nemf(*jet_iter, max_nemf);
				if(max_cemf != -1) cut_out = cut_out || cut_max_cemf(*jet_iter, max_cemf);
				if(min_cmulti != -1) cut_out = cut_out || cut_min_cmulti(*jet_iter, min_cmulti);
				    
				if(!cut_out)
				{
					unsorted_jets.push_back(*(*jet_iter));
					delete *jet_iter;
					jet_iter = jets.erase(jet_iter);
				}else{
					jet_iter++;
				}
			}
	    

			// this *is* important, don't delete it -- again
			if(unsorted_jets.size() < nasym_cut+1){ 
				for(std::vector<mor::Jet>::iterator jet_iter = unsorted_jets.begin(); 
				    jet_iter!=unsorted_jets.end(); ++jet_iter){ 
				        jet_sorter.add(&(*jet_iter)); 
				} 
				 
				std::vector<mor::Jet*> sorted_jets = jet_sorter.get_sorted(); 
				 
				for(std::vector<mor::Jet*>::iterator cut_jet = sorted_jets.begin(); 
				    cut_jet != sorted_jets.end(); 
				    ++cut_jet){ 
				        cut_jets->push_back(**cut_jet); 
				} 
				
				for(std::vector<mor::Jet*>::iterator jet = jets.begin(); 
				    jet != jets.end(); 
				    ++jet){ 
				  delete *jet; 
				} 
				return cut_jets; 
			}
		}
	}
	else{
	  
        	for(std::vector<mor::Jet*>::iterator jet_iter = jets.begin();
		    jet_iter!=jets.end();
		    ++jet_iter){
        	        double cut_out=false;
		 
			if(min_pt != NULL && min_pt->size() == 1 && (*min_pt)[0] != -1)
				cut_out = cut_out || cut_pt(*jet_iter, (*min_pt)[0]);
			if(max_pt != NULL && max_pt->size() == 1 && (*max_pt)[0] != -1)
				cut_out = cut_out || cut_max_pt(*jet_iter, (*max_pt)[0]);
			if(max_eta != -1)
				cut_out = cut_out || cut_eta(*jet_iter, max_eta);
			if(electrons->size() > 0 && min_e_dR != -1)
				cut_out = cut_out || cut_leptons(*jet_iter, electrons, min_e_dR);
			if(muons->size() > 0 && min_mu_dR != -1)
				cut_out = cut_out || cut_leptons(*jet_iter, muons, min_mu_dR);
				     
			if(max_fHPD != -1)
				cut_out = cut_out || cut_max_fHPD(*jet_iter,max_fHPD);
			
			if(min_n90Hits != -1)
				cut_out = cut_out || cut_min_n90Hits(*jet_iter, min_n90Hits);
			
			if(min_emf != -1)
				cut_out = cut_out || cut_min_emf(*jet_iter, min_emf);
			if(min_nconstituents != -1) cut_out = cut_out || cut_min_nconstituents(*jet_iter, min_nconstituents);
			if(min_chf != -1) cut_out = cut_out || cut_min_chf(*jet_iter, min_chf);
			if(max_nhf != -1) cut_out = cut_out || cut_max_nhf(*jet_iter, max_nhf);
			if(max_nemf != -1) cut_out = cut_out || cut_max_nemf(*jet_iter, max_nemf);
			if(max_cemf != -1) cut_out = cut_out || cut_max_cemf(*jet_iter, max_cemf);
			if(min_cmulti != -1) cut_out = cut_out || cut_min_cmulti(*jet_iter, min_cmulti);

        	        if(!cut_out)
        	        {
        	                unsorted_jets.push_back(**jet_iter);
        	        }
        	}
	}

        for(std::vector<mor::Jet>::iterator jet_iter = unsorted_jets.begin();
	    jet_iter!=unsorted_jets.end(); ++jet_iter){
		jet_sorter.add(&(*jet_iter));
        }

        std::vector<mor::Jet*> sorted_jets = jet_sorter.get_sorted();

	for(std::vector<mor::Jet*>::iterator cut_jet = sorted_jets.begin();
	    cut_jet != sorted_jets.end();
	    ++cut_jet){
		cut_jets->push_back(**cut_jet);
	}


	for(std::vector<mor::Jet*>::iterator jet = jets.begin();
		    jet != jets.end();
		    ++jet){
		  delete *jet;
		}

	return cut_jets; 
}

template <typename myLepton>
bool JetSelector::cut_leptons(mor::Jet *jet_iter, std::vector<myLepton> *leptons, double min_dR)
{
	for(typename std::vector<myLepton>::iterator lepton_iter = leptons->begin();
		lepton_iter != leptons->end();
		++lepton_iter){
		if(ROOT::Math::VectorUtil::DeltaR(*dynamic_cast<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >*>(jet_iter), *dynamic_cast<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >*>(&(*lepton_iter))) < min_dR)
			return true;
	}

	return false;
}

bool JetSelector::cut_min_cmulti(mor::Jet *jet_iter, double &min_cmulti)
{
	if(verbose) std::cout << "cmulti: " << jet_iter->cmulti() << " min cmulti: " << min_cmulti << std::endl;
        if(jet_iter->cmulti() > min_cmulti)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_cemf(mor::Jet *jet_iter, double &max_cemf)
{
	if(verbose) std::cout << "cemf: " << jet_iter->cemf() << " max cemf: " << max_cemf << std::endl;
        if(jet_iter->cemf() < max_cemf)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_nemf(mor::Jet *jet_iter, double &max_nemf)
{
	if(verbose) std::cout << "nemf: " << jet_iter->nemf() << " max nemf: " << max_nemf << std::endl;
        if(jet_iter->nemf() < max_nemf)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_nhf(mor::Jet *jet_iter, double &max_nhf)
{
	if(verbose) std::cout << "nhf: " << jet_iter->nhf() << " max nhf: " << max_nhf << std::endl;
        if(jet_iter->nhf() < max_nhf)
                return false;
        else
                return true;
}

bool JetSelector::cut_min_chf(mor::Jet *jet_iter, double &min_chf)
{
	if(verbose) std::cout << "chf: " << jet_iter->chf() << " min chf: " << min_chf << std::endl;
        if(jet_iter->chf() > min_chf)
                return false;
        else
                return true;
}

bool JetSelector::cut_min_nconstituents(mor::Jet *jet_iter, double &min_nconstituents)
{
	if(verbose) std::cout << "nconstituents: " << jet_iter->nconstituents() << " min nconstituents: " << min_nconstituents << std::endl;
        if(jet_iter->nconstituents() > min_nconstituents)
                return false;
        else
                return true;
}


bool JetSelector::cut_pt(mor::Jet *jet_iter, double &min_pt)
{
        if(jet_iter->Pt() > min_pt)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_pt(mor::Jet *jet_iter, double &max_pt)
{
        if(jet_iter->Pt() < max_pt)
                return false;
        else
                return true;
}

bool JetSelector::cut_eta(mor::Jet *jet_iter, double &max_eta)
{
        if(fabs(jet_iter->Eta()) < max_eta)
                return false;
        else
                return true;
}

bool JetSelector::cut_max_fHPD(mor::Jet *jet_iter, double &max_fHPD)
{

	/*
	 * to be in sync we allow jets with an electromagnetic fraction of 1.
	 * this results in a fHPD value of -1 what we accept for the moment
	 */
        //if(jet_iter->fHPD() >= 0 && jet_iter->fHPD() < max_fHPD)
        if(jet_iter->fHPD() < max_fHPD)
                return false;
        else
                return true;
}

bool JetSelector::cut_min_n90Hits(mor::Jet *jet_iter, double &min_n90Hits)
{
        if(jet_iter->n90Hits() >= min_n90Hits)
                return false;
        else
                return true;
}

bool JetSelector::cut_min_emf(mor::Jet *jet_iter, double &min_emf)
{
        if(jet_iter->emf() > min_emf)
                return false;
        else
                return true;
}
