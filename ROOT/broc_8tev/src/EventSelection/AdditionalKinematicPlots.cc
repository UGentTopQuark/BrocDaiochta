#include "../../interface/EventSelection/AdditionalKinematicPlots.h"

AdditionalKinematicPlots::AdditionalKinematicPlots(eire::HandleHolder *handle_holder)
{
	id = "_"+handle_holder->get_ident();
	set_handles(handle_holder);

	evt_shape = handle_holder->services()->evt_shape();
	var_calc = new broc::ClassificationVariableCalculator(handle_holder);
}

AdditionalKinematicPlots::~AdditionalKinematicPlots()
{
	if(var_calc){ delete var_calc; var_calc = NULL; }
}

void AdditionalKinematicPlots::plot_all()
{
        std::vector<mor::Muon> *isomu = handle_holder->get_tight_muons();
        std::vector<mor::Electron> *isoe = handle_holder->get_tight_electrons();

        std::vector<mor::Jet>  *jets = handle_holder->get_tight_jets();

	double drjm     = var_calc->get_drjl<mor::Muon>(isomu, jets);
	double detajm   = var_calc->get_detajl<mor::Muon>(isomu, jets);
	double dphimmet = var_calc->get_dphilmet<mor::Muon>(isomu);

	double drje     = var_calc->get_drjl<mor::Electron>(isoe, jets);
	double detaje   = var_calc->get_detajl<mor::Electron>(isoe, jets);
	double dphiemet = var_calc->get_dphilmet<mor::Electron>(isoe);

	evt_shape->calculate_variables();

	double aplanarity = evt_shape->get_aplanarity();
	double sphericity = evt_shape->get_sphericity();
	double HlT = evt_shape->get_HlT();
	double Mevent = evt_shape->get_Mevent();
	double H3T = evt_shape->get_H3T();
	double Mj2nulT = evt_shape->get_Mj2nulT();

	histos1d[("drjmu"+id).c_str()]->Fill(drjm);
	histos1d[("detajmu"+id).c_str()]->Fill(detajm);
	histos1d[("dphimmet"+id).c_str()]->Fill(dphimmet);
	histos1d[("dphiemet"+id).c_str()]->Fill(dphiemet);
	histos1d[("drje"+id).c_str()]->Fill(drje);
	histos1d[("detaje"+id).c_str()]->Fill(detaje);

	histos1d[("aplanarity"+id).c_str()]->Fill(aplanarity);
	histos1d[("sphericity"+id).c_str()]->Fill(sphericity);
	histos1d[("HlT"+id).c_str()]->Fill(HlT);
	histos1d[("H3T"+id).c_str()]->Fill(H3T);
	//histos1d[("MjetT"+id).c_str()]->Fill(MjetT);
	histos1d[("Mevent"+id).c_str()]->Fill(Mevent);
	histos1d[("Mj2nulT"+id).c_str()]->Fill(Mj2nulT);
}

void AdditionalKinematicPlots::book_histos()
{
	histos1d[("drjmu"+id).c_str()]=histo_writer->create_1d(("drjmu"+id).c_str(),"dr(j,mu)",100,0.,7.0, "#Delta R(j,mu)");
	histos1d[("detajmu"+id).c_str()]=histo_writer->create_1d(("detajmu"+id).c_str(),"deta(jet,mu)",100,0.,2.5, "#Delta #eta(jet,mu)");
	histos1d[("dphimmet"+id).c_str()]=histo_writer->create_1d(("dphimmet"+id).c_str(),"dphi(met,mu)",100,0.,4.0, "#Delta #phi(met,mu)");
	histos1d[("drje"+id).c_str()]=histo_writer->create_1d(("drje"+id).c_str(),"dr(j,e)",100,0.,7.0, "dR(j,e)");
	histos1d[("detaje"+id).c_str()]=histo_writer->create_1d(("detaje"+id).c_str(),"deta(jet,e)",100,0.,2.5, "#Delta #eta(jet,e)");
	histos1d[("dphiemet"+id).c_str()]=histo_writer->create_1d(("dphiemet"+id).c_str(),"dphi(met,e)",100,0.,4.0, "#Delta #phi(met,e)");
	histos1d[("aplanarity"+id).c_str()]=histo_writer->create_1d(("aplanarity"+id).c_str(),"event aplanarity",50,0.,0.5, "Aplanarity");
	histos1d[("sphericity"+id).c_str()]=histo_writer->create_1d(("sphericity"+id).c_str(),"event sphericity",50,0.,1, "Sphericity");
	histos1d[("HlT"+id).c_str()]=histo_writer->create_1d(("HlT"+id).c_str(),"event HlT",200,0.,1500., "H^{l}_{T} [GeV]");
	histos1d[("H3T"+id).c_str()]=histo_writer->create_1d(("H3T"+id).c_str(),"event H3T",50,0.,500., "H^{3}_{T} [GeV]");
//	histos1d[("MjetT"+id).c_str()]=histo_writer->create_1d(("MjetT"+id).c_str(),"event MjetT",50,0.,500., "M^{jet}_T");
	histos1d[("Mevent"+id).c_str()]=histo_writer->create_1d(("Mevent"+id).c_str(),"event Mevent",200,0.,1500., "M_{event}");
	histos1d[("Mj2nulT"+id).c_str()]=histo_writer->create_1d(("Mj2nulT"+id).c_str(),"event Mj2nulT",50,0.,500., "M^{j_{2} #nu l}_{T}");
}
