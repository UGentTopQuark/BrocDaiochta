#include "../../interface/EventSelection/CollectionSkimmer.h"

broc::CollectionSkimmer::CollectionSkimmer(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	this->cuts_set = cuts_set;
	this->handle_holder = handle_holder;

	mm_loose_e_selector = new LeptonSelector<mor::Electron>();
	mm_loose_mu_selector = new LeptonSelector<mor::Muon>();
	e_selector = new LeptonSelector<mor::Electron>();
	mu_selector = new LeptonSelector<mor::Muon>();
	jet_uncleaned_mu_selector = new LeptonSelector<mor::Muon>();
	jet_uncleaned_e_selector = new LeptonSelector<mor::Electron>();
	loose_e_selector = new LeptonSelector<mor::Electron>();
	loose_mu_selector = new LeptonSelector<mor::Muon>();
	jet_selector = new JetSelector(handle_holder);
	jet_selector->initialise_jec_uncertainty(handle_holder->get_config_reader());
	METCor = new METCorrector();

	tight_primary_vertices = new std::vector<mor::PrimaryVertex>();
	handle_holder->set_tight_primary_vertices(tight_primary_vertices);

	do_met_recalc = handle_holder->get_config_reader()->get_bool_var("recompute_met", "global", true);

	pvertex_max_z = -1;
	pvertex_min_ndof = -1;
	pvertex_max_rho = -1;
	pvertex_not_fake = -1;

	set_cuts_sets(cuts_set);

	dummy_vec = NULL;
}

broc::CollectionSkimmer::~CollectionSkimmer()
{
	if(jet_selector){ delete jet_selector; jet_selector=NULL; }
	if(e_selector){ delete e_selector; e_selector=NULL; }
	if(mm_loose_e_selector){ delete mm_loose_e_selector; mm_loose_e_selector=NULL; }
        if(mm_loose_mu_selector){ delete mm_loose_mu_selector; mm_loose_mu_selector=NULL; }
	if(mu_selector){ delete mu_selector; mu_selector=NULL; }
	if(loose_e_selector){ delete loose_e_selector; loose_e_selector=NULL; }
	if(loose_mu_selector){ delete loose_mu_selector; loose_mu_selector=NULL; }
	if(dummy_vec){ delete dummy_vec; dummy_vec = NULL; }
	if(tight_primary_vertices){ delete tight_primary_vertices; tight_primary_vertices = NULL; }
}

void broc::CollectionSkimmer::set_cuts_sets(broc::CutsSet *cuts_set)
{
	// loose electron collection for mm
	mm_loose_e_selector->set_cuts_set(cuts_set,"e","mm_loose_");
	//loose muon collection for mm
	mm_loose_mu_selector->set_cuts_set(cuts_set,"mu","mm_loose_");

	CutsSet jet_uncleaned_cuts_set(*cuts_set);
	dummy_vec = new std::vector<double>();
	dummy_vec->push_back(-1.);
	jet_uncleaned_cuts_set.set_cut("min_mu_dR", dummy_vec);
	jet_uncleaned_cuts_set.set_cut("min_e_dR", dummy_vec);
	jet_uncleaned_mu_selector->set_cuts_set(&jet_uncleaned_cuts_set, "mu", "");
	jet_uncleaned_e_selector->set_cuts_set(&jet_uncleaned_cuts_set, "e", "");

	e_selector->set_cuts_set(cuts_set, "e", "");
	mu_selector->set_cuts_set(cuts_set, "mu", "");

	loose_e_selector->set_cuts_set(cuts_set, "e", "loose_");
	loose_mu_selector->set_cuts_set(cuts_set, "mu", "loose_");

	jet_selector->set_cuts_set(cuts_set);

	pvertex_max_z = cuts_set->get_cut_value("pvertex_max_z");
	pvertex_min_ndof = cuts_set->get_cut_value("pvertex_min_ndof");
	pvertex_max_rho = cuts_set->get_cut_value("pvertex_max_rho");
	pvertex_not_fake = cuts_set->get_cut_value("pvertex_not_fake");
}

void broc::CollectionSkimmer::skim()
{
	std::vector<mor::PrimaryVertex>* pvertices = handle_holder->get_primary_vertices();
	std::vector<mor::Muon> *muons = handle_holder->get_muons();
	std::vector<mor::Electron> *electrons = handle_holder->get_electrons();
	std::vector<mor::Jet> *uncut_jets = handle_holder->get_jets();
	std::vector<mor::MET> *uncorrected_mets = handle_holder->get_mets();

	mm_loose_e_selector->set_primary_vertices(pvertices);
	mm_loose_mu_selector->set_primary_vertices(pvertices);
	loose_e_selector->set_primary_vertices(pvertices);
	loose_mu_selector->set_primary_vertices(pvertices);	
	e_selector->set_primary_vertices(pvertices);
	mu_selector->set_primary_vertices(pvertices);

	METCor->set_handles(handle_holder->get_mets());
	
	jet_uncleaned_mu_selector->set_primary_vertices(pvertices);	
	jet_uncleaned_e_selector->set_primary_vertices(pvertices);	
	
	double min_jet_e_dR = cuts_set->get_cut_value("min_jet_e_dR");
	double min_jet_mu_dR = cuts_set->get_cut_value("min_jet_mu_dR");
	double min_e_dR = -1;
	double min_mu_dR = -1;
	if(cuts_set->get_vcut_value("min_e_dR") != NULL && cuts_set->get_vcut_value("min_e_dR")->size() > 0)
		min_e_dR = (*(cuts_set->get_vcut_value("min_e_dR")))[0];
	if(cuts_set->get_vcut_value("min_mu_dR") != NULL && cuts_set->get_vcut_value("min_mu_dR")->size() > 0)
		min_mu_dR = (*(cuts_set->get_vcut_value("min_mu_dR")))[0];

	std::vector<mor::Jet> *jets = NULL;;
	if(min_jet_e_dR == -1 && min_jet_mu_dR == -1){
		jets = jet_selector->get_jets(uncut_jets,muons, electrons);	// e and mu not used in this usecase
		handle_holder->set_tight_electrons(e_selector->get_leptons(electrons, jets, uncorrected_mets)); //jl 20.01.11: dr cut wrt selected jets
		handle_holder->set_tight_muons(mu_selector->get_leptons(muons, jets, uncorrected_mets)); //jl 20.01.11: dr cut wrt selected jets
	}else{
		std::vector<mor::Muon> *jet_uncleaned_muons = jet_uncleaned_mu_selector->get_leptons(muons, uncut_jets, uncorrected_mets);
		std::vector<mor::Electron> *jet_uncleaned_electrons = jet_uncleaned_e_selector->get_leptons(electrons, uncut_jets, uncorrected_mets);//e

		jets = jet_selector->get_jets(uncut_jets,jet_uncleaned_muons, jet_uncleaned_electrons);
		handle_holder->set_tight_electrons(e_selector->get_leptons(electrons, jets, uncorrected_mets));//e
		handle_holder->set_tight_muons(mu_selector->get_leptons(muons, jets, uncorrected_mets));
	}

	handle_holder->set_tight_jets(jets);

	handle_holder->set_loose_muons(loose_mu_selector->get_leptons(muons, jets, uncorrected_mets));
	handle_holder->set_loose_electrons(loose_e_selector->get_leptons(electrons, jets,uncorrected_mets));
	handle_holder->set_loose_mm_electrons(mm_loose_e_selector->get_leptons(electrons, jets, uncorrected_mets));
	handle_holder->set_loose_mm_muons(mm_loose_mu_selector->get_leptons(muons, jets, uncorrected_mets)); //jl 15.03.11 for MM

	std::vector<mor::Muon> *tight_muons = handle_holder->get_tight_muons();
	std::vector<mor::Electron> *tight_electrons = handle_holder->get_tight_electrons();

        if(tight_muons->size() == 1 && tight_electrons->size() == 0){
                METCor->set_handles(uncorrected_mets, dynamic_cast<mor::Lepton*>(&(*tight_muons->begin())));
        }
        else if(tight_electrons->size() == 1 && tight_muons->size() == 0){
                METCor->set_handles(uncorrected_mets, dynamic_cast<mor::Lepton*>(&(*tight_electrons->begin())));
        }else{
                METCor->set_handles(uncorrected_mets);
	}

        handle_holder->set_corrected_mets(METCor->get_corrected_MET());

	if(do_met_recalc) handle_holder->set_selected_mets(handle_holder->get_corrected_mets());
	else 		  handle_holder->set_selected_mets(handle_holder->get_mets());

	tight_primary_vertices->clear();
	if(pvertices->size() > 0){
		for(std::vector<mor::PrimaryVertex>::iterator vertex = pvertices->begin();
			vertex != pvertices->end();
			++vertex){
			if(((pvertex_max_z == -1) || pvertex_max_z > fabs(vertex->z())) &&
			   ((pvertex_min_ndof == -1) || pvertex_min_ndof < vertex->ndof()) &&
			   ((pvertex_max_rho == -1) || pvertex_max_rho > vertex->rho()) &&
			   ((pvertex_not_fake == -1) || pvertex_not_fake == !vertex->is_fake())){
				tight_primary_vertices->push_back(*vertex);
			}
		}
	}
}
