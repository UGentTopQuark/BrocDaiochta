#include "../../interface/EventSelection/LeptonSelector.h"

template <class myLepton>
LeptonSelector<myLepton>::LeptonSelector()
{
	max_trackiso = NULL;
	max_caliso = NULL;
	max_ecaliso = NULL;
	max_hcaliso = NULL;
	max_ecal_veto_cone = NULL;
	max_hcal_veto_cone = NULL;
	min_dR = NULL;
	max_dR = NULL;
	min_pt = NULL;
	max_pt = NULL;
	min_et = NULL;
	max_et = NULL;
	min_relIso = NULL;
	max_relIso = NULL;
	max_PFrelIso = NULL;
	min_PFrelIso = NULL;
	electronID = NULL;
	min_nHits = NULL;
	max_d0 = NULL;
	min_d0 = NULL;
	max_vz = NULL;
	max_d0sig = NULL;
	max_chi2 = NULL;
	min_nMuonHits = NULL;
	min_nStripHits = NULL;
	min_nPixelHits = NULL;
	min_nPixelLayers = NULL;
	min_nStations = NULL;
	min_nMatchedSegments = NULL;
	max_nLostTrackerHits = NULL;
	lepton_type = -1;
	min_eta = NULL;
	max_eta = NULL;
	//jl 04.11.10: dphi lep/met cut
	min_dphi = NULL;
	max_dphi = NULL;
	lep_match_trigger = -1;

	max_dcot = -1;
	max_dist = -1;
	
	primary_vertices = NULL;

	exclude_eta_crack = 0;
	lepton_charge = -1;
	lepton_scale = -1;
	conesize = "0.3";
	iso_cone = 0.3;

        isolated_leptons = new std::vector<myLepton>();

	conversion_identifier = new eire::ConversionIdentifier();
}

template <class myLepton>
LeptonSelector<myLepton>::~LeptonSelector()
{
	if(isolated_leptons){ delete isolated_leptons; isolated_leptons = NULL; }
	if(conversion_identifier){ delete conversion_identifier; conversion_identifier = NULL; }
}

template <class myLepton>
void LeptonSelector<myLepton>::set_primary_vertices(std::vector<mor::PrimaryVertex> *primary_vertices)
{
	this->primary_vertices = primary_vertices;
}

template <class myLepton>
void LeptonSelector<myLepton>::set_cuts_set(broc::CutsSet *cuts_set, std::string lep, std::string type)
{
	max_trackiso = cuts_set->get_vcut_value("max_"+type+lep+"_trackiso");
	max_caliso = cuts_set->get_vcut_value("max_"+type+lep+"_caliso");
	max_ecaliso = cuts_set->get_vcut_value("max_"+type+lep+"_ecaliso");
	max_hcaliso = cuts_set->get_vcut_value("max_"+type+lep+"_hcaliso");
	max_hcal_veto_cone = cuts_set->get_vcut_value("max_"+type+lep+"_hcal_veto_cone");
	max_ecal_veto_cone = cuts_set->get_vcut_value("max_"+type+lep+"_ecal_veto_cone");
	min_dR = cuts_set->get_vcut_value("min_"+type+lep+"_dR");
	max_dR = cuts_set->get_vcut_value("max_"+type+lep+"_dR");
	min_pt = cuts_set->get_vcut_value("min_"+type+lep+"_pt");
	max_pt = cuts_set->get_vcut_value("max_"+type+lep+"_pt");
	min_relIso = cuts_set->get_vcut_value("min_"+type+lep+"_relIso");
	max_relIso = cuts_set->get_vcut_value("max_"+type+lep+"_relIso");
	max_PFrelIso = cuts_set->get_vcut_value("max_"+type+lep+"_PFrelIso");
	min_PFrelIso = cuts_set->get_vcut_value("min_"+type+lep+"_PFrelIso");
	electronID = cuts_set->get_vcut_value(type+lep+"_electronID");
	min_nHits = cuts_set->get_vcut_value("min_"+type+lep+"_nHits");
	max_d0 = cuts_set->get_vcut_value("max_"+type+lep+"_d0");
	min_d0 = cuts_set->get_vcut_value("min_"+type+lep+"_d0");
	max_vz = cuts_set->get_vcut_value("max_"+type+lep+"_vz");
	max_d0sig = cuts_set->get_vcut_value("max_"+type+lep+"_d0sig");
	max_chi2 = cuts_set->get_vcut_value("max_"+type+lep+"_chi2");
	min_eta = cuts_set->get_vcut_value("min_"+type+lep+"_eta");
	max_eta = cuts_set->get_vcut_value("max_"+type+lep+"_eta");
	//jl 04.11.10: dphi lep/met cut
	min_dphi = cuts_set->get_vcut_value("min_"+type+lep+"_dphilepmet");
	max_dphi = cuts_set->get_vcut_value("max_"+type+lep+"_dphilepmet");
	if(lep == "mu"){
		min_nMuonHits = cuts_set->get_vcut_value("min_"+type+lep+"_nMuonHits");
		min_nStripHits = cuts_set->get_vcut_value("min_"+type+lep+"_nStripHits");
		min_nPixelHits = cuts_set->get_vcut_value("min_"+type+lep+"_nPixelHits");
		min_nPixelLayers = cuts_set->get_vcut_value("min_"+type+lep+"_nPixelLayers");
		min_nStations = cuts_set->get_vcut_value("min_"+type+lep+"_nStations");
		min_nMatchedSegments = cuts_set->get_vcut_value("min_"+type+lep+"_nMatchedSegments");
	}
	if(lep == "e"){
		exclude_eta_crack = cuts_set->get_cut_value(type+lep+"_exclude_eta_crack");
		min_et = cuts_set->get_vcut_value("min_"+type+lep+"_et");
		max_et = cuts_set->get_vcut_value("max_"+type+lep+"_et");
		max_dcot = cuts_set->get_cut_value("max_"+type+lep+"_dcot");
		max_dist = cuts_set->get_cut_value("max_"+type+lep+"_dist");
		conversion_identifier->set_cuts(cuts_set, type+lep);
	}
	max_nLostTrackerHits = cuts_set->get_vcut_value("max_"+type+lep+"_nLostTrackerHits");
	lepton_type = (int) cuts_set->get_cut_value(type+lep+"_type");
	lepton_charge = (int) cuts_set->get_cut_value(type+lep+"_charge");
	lep_match_trigger = cuts_set->get_cut_value(type+lep+"_match_trigger");
	lepton_scale = cuts_set->get_cut_value(type+lep+"_lepton_scale");
	iso_cone = cuts_set->get_cut_value(type+lep+"_isocone");

	// default value for iso_cone
	if(iso_cone <= 0) iso_cone = 0.3;
	Tools tools;
	conesize = tools.stringify(iso_cone);
}

// Here the functions for any cuts which have been set are called
template <class myLepton>
std::vector<myLepton>* LeptonSelector<myLepton>::get_leptons(std::vector<myLepton> *leptons, std::vector<mor::Jet> *myjets, std::vector<mor::MET> *mymet) //jl 04.11.10	
{
	jets = myjets;
	isolated_leptons->clear();

	// copy of leptons for systematics
	std::vector<myLepton*> tmp_leptons;
	for(typename std::vector<myLepton>::iterator lepton_iter = leptons->begin(); lepton_iter!=leptons->end();
	++lepton_iter){
		tmp_leptons.push_back(&(*lepton_iter));
	}

	if(lepton_scale != -1){
		change_lepton_scale(tmp_leptons);
	}

	if((max_trackiso != NULL && max_trackiso->size() > 1) ||
	   (max_ecaliso != NULL && max_ecaliso->size() > 1) ||
	   (max_caliso != NULL && max_caliso->size() > 1) ||
	   (max_hcaliso != NULL && max_hcaliso->size() > 1) ||
	   (max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > 1) ||
	   (max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > 1) ||
	   (min_dR != NULL && min_dR->size() > 1) ||
	   (max_dR != NULL && max_dR->size() > 1) ||
	   (min_relIso != NULL && min_relIso->size() > 1) ||
	   (max_relIso != NULL && max_relIso->size() > 1) ||
	   (max_PFrelIso != NULL && max_PFrelIso->size() > 1) ||
	   (min_PFrelIso != NULL && min_PFrelIso->size() > 1) ||
	   (min_pt != NULL && min_pt->size() >1) ||
	   (max_pt != NULL && max_pt->size() >1) ||
	   (min_et != NULL && min_et->size() >1) ||
	   (max_et != NULL && max_et->size() >1) ||
	   (max_d0 != NULL && max_d0->size() >1) ||
	   (min_d0 != NULL && min_d0->size() >1) ||
	   (max_vz != NULL && max_vz->size() >1) ||
	   (min_eta != NULL && min_eta->size() >1) ||
	   (max_eta != NULL && max_eta->size() >1) ||
	   (min_dphi != NULL && min_dphi->size() >1) || //jl 04.11.10
	   (max_dphi != NULL && max_dphi->size() >1) || //jl 04.11.10
	   (max_d0sig != NULL && max_d0sig->size() >1) ||
	   (max_chi2 != NULL && max_chi2->size() >1) ||
	   (min_nHits != NULL && min_nHits->size() >1) ||
	   (min_nMuonHits != NULL && min_nMuonHits->size() > 0) ||
	   (min_nStripHits != NULL && min_nStripHits->size() > 0) ||
	   (min_nPixelHits != NULL && min_nPixelHits->size() > 0) ||
	   (min_nPixelLayers != NULL && min_nPixelLayers->size() > 0) ||
	   (min_nStations != NULL && min_nStations->size() > 0) ||
	   (min_nMatchedSegments != NULL && min_nMatchedSegments->size() > 0) ||
	   (max_nLostTrackerHits != NULL && max_nLostTrackerHits->size() > 0) ||
	   (electronID != NULL && electronID->size() >1)){

		double max_size = max_trackiso->size();
		if(max_ecaliso != NULL && max_ecaliso->size() > max_size) max_size = max_ecaliso->size();
		if(max_caliso != NULL && max_caliso->size() > max_size) max_size = max_caliso->size();
		if(max_hcaliso != NULL && max_hcaliso->size() > max_size) max_size = max_hcaliso->size();
		if(max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > max_size) max_size = max_hcal_veto_cone->size();
		if(max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > max_size) max_size = max_ecal_veto_cone->size();
		if(min_dR != NULL && min_dR->size() > max_size) max_size = min_dR->size();
		if(max_dR != NULL && max_dR->size() > max_size) max_size = max_dR->size();
		if(min_relIso != NULL && min_relIso->size() > max_size) max_size = min_relIso->size();
		if(max_relIso != NULL && max_relIso->size() > max_size) max_size = max_relIso->size();
		if(max_PFrelIso != NULL && max_PFrelIso->size() > max_size) max_size = max_PFrelIso->size();
		if(min_PFrelIso != NULL && min_PFrelIso->size() > max_size) max_size = min_PFrelIso->size();
		if(min_pt != NULL && min_pt->size() > max_size) max_size = min_pt->size();
		if(max_pt != NULL && max_pt->size() > max_size) max_size = max_pt->size();
		if(min_et != NULL && min_et->size() > max_size) max_size = min_et->size();
		if(max_et != NULL && max_et->size() > max_size) max_size = max_et->size();
	   	if(electronID != NULL && electronID->size() > max_size) max_size = electronID->size();
	   	if(max_chi2 != NULL && max_chi2->size() > max_size) max_size = max_chi2->size();
	   	if(max_d0 != NULL && max_d0->size() > max_size) max_size = max_d0->size();
	   	if(min_d0 != NULL && min_d0->size() > max_size) max_size = min_d0->size();
	   	if(max_vz != NULL && max_vz->size() > max_size) max_size = max_vz->size();
	   	if(max_d0sig != NULL && max_d0sig->size() > max_size) max_size = max_d0sig->size();
	   	if(min_nHits != NULL && min_nHits->size() > max_size) max_size = min_nHits->size();
	   	if(min_eta != NULL && min_eta->size() > max_size) max_size = min_eta->size();
		if(max_eta != NULL && max_eta->size() > max_size) max_size = max_eta->size();
		if(min_dphi != NULL && min_dphi->size() > max_size) max_size = min_dphi->size(); //jl 04.11.10
		if(max_dphi != NULL && max_dphi->size() > max_size) max_size = max_dphi->size(); //jl 04.11.10
	   	if(min_nMuonHits != NULL && min_nMuonHits->size() > max_size) max_size = min_nMuonHits->size();
	   	if(min_nStripHits != NULL && min_nStripHits->size() > max_size) max_size = min_nStripHits->size();
	   	if(min_nPixelHits != NULL && min_nPixelHits->size() > max_size) max_size = min_nPixelHits->size();
	   	if(min_nPixelLayers != NULL && min_nPixelLayers->size() > max_size) max_size = min_nPixelLayers->size();
	   	if(min_nStations != NULL && min_nStations->size() > max_size) max_size = min_nStations->size();
	   	if(min_nMatchedSegments != NULL && min_nMatchedSegments->size() > max_size) max_size = min_nMatchedSegments->size();
	   	if(max_nLostTrackerHits != NULL && max_nLostTrackerHits->size() > max_size) max_size = max_nLostTrackerHits->size();

		for(unsigned int nasym_cut = 0; nasym_cut < max_size; ++nasym_cut){
        		typename std::vector<myLepton*>::iterator lepton_iter;
        		for(lepton_iter = tmp_leptons.begin();
			lepton_iter!=tmp_leptons.end();){
        		        double cut_out=false;
			 
        		        if(max_trackiso != NULL && max_trackiso->size() > nasym_cut && (*max_trackiso)[nasym_cut] != -1)
					cut_out = cut_out || cut_trackiso(*lepton_iter, (*max_trackiso)[nasym_cut]);
        		        if(max_ecaliso != NULL && max_ecaliso->size() > nasym_cut && (*max_ecaliso)[nasym_cut] != -1)
					cut_out = cut_out || cut_ecaliso(*lepton_iter, (*max_ecaliso)[nasym_cut]);
        		        if(max_caliso != NULL && max_caliso->size() > nasym_cut && (*max_caliso)[nasym_cut] != -1)
					cut_out = cut_out || cut_caliso(*lepton_iter, (*max_caliso)[nasym_cut]);
	       		        if(max_hcaliso != NULL && max_hcaliso->size() > nasym_cut && (*max_hcaliso)[nasym_cut] != -1)
					cut_out = cut_out || cut_hcaliso(*lepton_iter,  (*max_hcaliso)[nasym_cut]);
	       		        if(max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > nasym_cut && (*max_hcal_veto_cone)[nasym_cut] != -1)
					cut_out = cut_out || cut_hcal_veto_cone(*lepton_iter,  (*max_hcal_veto_cone)[nasym_cut]);
	       		        if(max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > nasym_cut && (*max_ecal_veto_cone)[nasym_cut] != -1)
					cut_out = cut_out || cut_ecal_veto_cone(*lepton_iter,  (*max_ecal_veto_cone)[nasym_cut]);
        		        if(min_dR != NULL && min_dR->size() > nasym_cut && (*min_dR)[nasym_cut] != -1)
					cut_out = cut_out || cut_dR(*lepton_iter,  (*min_dR)[nasym_cut]);
        		        if(max_dR != NULL && max_dR->size() > nasym_cut && (*max_dR)[nasym_cut] != -1)
					cut_out = cut_out || cut_max_dR(*lepton_iter,  (*max_dR)[nasym_cut]);
        		        if(min_relIso != NULL && min_relIso->size() > nasym_cut && (*min_relIso)[nasym_cut] != -1)
					cut_out = cut_out || cut_inverse_relIso(*lepton_iter,  (*min_relIso)[nasym_cut]);
        		        if(max_relIso != NULL && max_relIso->size() > nasym_cut && (*max_relIso)[nasym_cut] != -1)
					cut_out = cut_out || cut_relIso(*lepton_iter,  (*max_relIso)[nasym_cut]);
        		        if(max_PFrelIso != NULL && max_PFrelIso->size() > nasym_cut && (*max_PFrelIso)[nasym_cut] != -1)
					cut_out = cut_out || cut_PFrelIso(*lepton_iter,  (*max_PFrelIso)[nasym_cut]);		
        		        if(min_PFrelIso != NULL && min_PFrelIso->size() > nasym_cut && (*min_PFrelIso)[nasym_cut] != -1)
					cut_out = cut_out || cut_min_PFrelIso(*lepton_iter,  (*min_PFrelIso)[nasym_cut]);				
				if(min_pt != NULL && min_pt->size() > nasym_cut && (*min_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_pt(*lepton_iter, (*min_pt)[nasym_cut]);
				if(max_pt != NULL && max_pt->size() > nasym_cut && (*max_pt)[nasym_cut] != -1)
					cut_out = cut_out || cut_max_pt(*lepton_iter, (*max_pt)[nasym_cut]);
				if(min_et != NULL && min_et->size() > nasym_cut && (*min_et)[nasym_cut] != -1)
					cut_out = cut_out || cut_et(*lepton_iter, (*min_et)[nasym_cut]);
				if(max_et != NULL && max_et->size() > nasym_cut && (*max_et)[nasym_cut] != -1)
					cut_out = cut_out || cut_max_et(*lepton_iter, (*max_et)[nasym_cut]);
				if(electronID != NULL && electronID->size() > nasym_cut && (*electronID)[nasym_cut] != -1)
					cut_out = cut_out || cut_electronID(*lepton_iter, (*electronID)[nasym_cut]);
				if(max_chi2 != NULL && max_chi2->size() > nasym_cut && (*max_chi2)[nasym_cut] != -1)
					cut_out = cut_out || cut_chi2(*lepton_iter, (*max_chi2)[nasym_cut]);
				if(max_d0 != NULL && max_d0->size() > nasym_cut && (*max_d0)[nasym_cut] != -1)
					cut_out = cut_out || cut_d0(*lepton_iter, (*max_d0)[nasym_cut]);
				if(min_d0 != NULL && min_d0->size() > nasym_cut && (*min_d0)[nasym_cut] != -1)
					cut_out = cut_out || cut_min_d0(*lepton_iter, (*min_d0)[nasym_cut]);
				if(max_vz != NULL && max_vz->size() > nasym_cut && (*max_vz)[nasym_cut] != -1)
					cut_out = cut_out || cut_vz(*lepton_iter, (*max_vz)[nasym_cut]);
				if(max_d0sig != NULL && max_d0sig->size() > nasym_cut && (*max_d0sig)[nasym_cut] != -1)
					cut_out = cut_out || cut_d0sig(*lepton_iter, (*max_d0sig)[nasym_cut]);
				if(min_nHits != NULL && min_nHits->size() > nasym_cut && (*min_nHits)[nasym_cut] != -1)
					cut_out = cut_out || cut_nHits(*lepton_iter, (*min_nHits)[nasym_cut]);
				if(min_nMuonHits != NULL && min_nMuonHits->size() > nasym_cut && (*min_nMuonHits)[nasym_cut] != -1)
					cut_out = cut_out || cut_nMuonHits(*lepton_iter, (*min_nMuonHits)[nasym_cut]);
				if(min_nStripHits != NULL && min_nStripHits->size() > nasym_cut && (*min_nStripHits)[nasym_cut] != -1)
					cut_out = cut_out || cut_nStripHits(*lepton_iter, (*min_nStripHits)[nasym_cut]);
				if(min_nPixelHits != NULL && min_nPixelHits->size() > nasym_cut && (*min_nPixelHits)[nasym_cut] != -1)
					cut_out = cut_out || cut_nPixelHits(*lepton_iter, (*min_nPixelHits)[nasym_cut]);
				if(min_nPixelLayers != NULL && min_nPixelLayers->size() > nasym_cut && (*min_nPixelLayers)[nasym_cut] != -1)
					cut_out = cut_out || cut_nPixelLayers(*lepton_iter, (*min_nPixelLayers)[nasym_cut]);
				if(min_nStations != NULL && min_nStations->size() > nasym_cut && (*min_nStations)[nasym_cut] != -1)
					cut_out = cut_out || cut_nStations(*lepton_iter, (*min_nStations)[nasym_cut]);
				if(min_nMatchedSegments != NULL && min_nMatchedSegments->size() > nasym_cut && (*min_nMatchedSegments)[nasym_cut] != -1)
					cut_out = cut_out || cut_nMatchedSegments(*lepton_iter, (*min_nMatchedSegments)[nasym_cut]);
				if(max_nLostTrackerHits != NULL && max_nLostTrackerHits->size() > nasym_cut && (*max_nLostTrackerHits)[nasym_cut] != -1)
					cut_out = cut_out || cut_nLostTrackerHits(*lepton_iter, (*max_nLostTrackerHits)[nasym_cut]);
				if(min_eta != NULL && min_eta->size() > nasym_cut && (*min_eta)[nasym_cut] != -1)
					cut_out = cut_out || cut_eta(*lepton_iter, (*min_eta)[nasym_cut], false);
				if(max_eta != NULL && max_eta->size() > nasym_cut && (*max_eta)[nasym_cut] != -1)
					cut_out = cut_out || cut_eta(*lepton_iter, (*max_eta)[nasym_cut], true);
				//jl 04.11.10: dphi lep/met
				if(min_dphi != NULL && min_dphi->size() > nasym_cut && (*min_dphi)[nasym_cut] != -1)
					cut_out = cut_out || cut_min_dphi_lepmet(*lepton_iter, (*min_dphi)[nasym_cut], mymet);
				if(max_dphi != NULL && max_dphi->size() > nasym_cut && (*max_dphi)[nasym_cut] != -1)
					cut_out = cut_out || cut_max_dphi_lepmet(*lepton_iter, (*max_dphi)[nasym_cut], mymet);
				if(lepton_type != -1)
					cut_out = cut_out || cut_lepton_type(*lepton_iter, lepton_type);
				if(lepton_charge != -1)
					cut_out = cut_out || cut_lepton_charge(*lepton_iter, lepton_charge);
				//if(max_dcot != -1 || max_dist != -1 || (max_nLostTrackerHits != NULL && max_nLostTrackerHits->size() > nasym_cut))
			//		cut_out = cut_out || cut_conv_rej(*lepton_iter);
				if(lep_match_trigger != -1)
					cut_out = cut_out || cut_trigger(*lepton_iter);

				cut_out = cut_out || cut_triangle(*lepton_iter, mymet); //jl 22.04.11: triangle cut

        		        if(!cut_out)
        		        {
        		                isolated_leptons->push_back(**lepton_iter);
					lepton_iter=tmp_leptons.erase(lepton_iter);
        		        }else{
					lepton_iter++;
				}
        		}

			if(isolated_leptons->size() < nasym_cut+1)
				return isolated_leptons;
		}
	}
	else{
        	typename std::vector<myLepton*>::iterator lepton_iter;
        	for(lepton_iter = tmp_leptons.begin(); lepton_iter!=tmp_leptons.end(); ++lepton_iter){
        	        double cut_out=false;
		 
        	        if(max_trackiso != NULL && max_trackiso->size() > 0 && (*max_trackiso)[0] != -1)
				cut_out = cut_out || cut_trackiso(*lepton_iter, (*max_trackiso)[0]);
        	        if(max_ecaliso != NULL && max_ecaliso->size() > 0 && (*max_ecaliso)[0] != -1)
				cut_out = cut_out || cut_ecaliso(*lepton_iter, (*max_ecaliso)[0]);
        	        if(max_caliso != NULL && max_caliso->size() > 0 && (*max_caliso)[0] != -1)
				cut_out = cut_out || cut_caliso(*lepton_iter, (*max_caliso)[0]);
        	        if(max_hcaliso != NULL && max_hcaliso->size() > 0 && (*max_hcaliso)[0] != -1)
				cut_out = cut_out || cut_hcaliso(*lepton_iter, (*max_hcaliso)[0]);
        	        if(max_hcal_veto_cone != NULL && max_hcal_veto_cone->size() > 0 && (*max_hcal_veto_cone)[0] != -1)
				cut_out = cut_out || cut_hcal_veto_cone(*lepton_iter, (*max_hcal_veto_cone)[0]);
        	        if(max_ecal_veto_cone != NULL && max_ecal_veto_cone->size() > 0 && (*max_ecal_veto_cone)[0] != -1)
				cut_out = cut_out || cut_ecal_veto_cone(*lepton_iter, (*max_ecal_veto_cone)[0]);
        	        if(min_dR != NULL && min_dR->size() > 0 && (*min_dR)[0] != -1)
				cut_out = cut_out || cut_dR(*lepton_iter, (*min_dR)[0]);
        	        if(max_dR != NULL && max_dR->size() > 0 && (*max_dR)[0] != -1)
				cut_out = cut_out || cut_max_dR(*lepton_iter, (*max_dR)[0]);
        	        if(min_relIso != NULL && min_relIso->size() > 0 && (*min_relIso)[0] != -1)
				cut_out = cut_out || cut_inverse_relIso(*lepton_iter, (*min_relIso)[0]);
        	        if(max_relIso != NULL && max_relIso->size() > 0 && (*max_relIso)[0] != -1)
				cut_out = cut_out || cut_relIso(*lepton_iter, (*max_relIso)[0]);
        	        if(max_PFrelIso != NULL && max_PFrelIso->size() > 0 && (*max_PFrelIso)[0] != -1)
				cut_out = cut_out || cut_PFrelIso(*lepton_iter, (*max_PFrelIso)[0]);
        	        if(min_PFrelIso != NULL && min_PFrelIso->size() > 0 && (*min_PFrelIso)[0] != -1)
				cut_out = cut_out || cut_min_PFrelIso(*lepton_iter, (*min_PFrelIso)[0]);
			if(min_pt != NULL && min_pt->size() > 0 && (*min_pt)[0] != -1)
				cut_out = cut_out || cut_pt(*lepton_iter, (*min_pt)[0]);
			if(max_pt != NULL && max_pt->size() > 0 && (*max_pt)[0] != -1)
				cut_out = cut_out || cut_max_pt(*lepton_iter, (*max_pt)[0]);
			if(min_et != NULL && min_et->size() > 0 && (*min_et)[0] != -1)
				cut_out = cut_out || cut_et(*lepton_iter, (*min_et)[0]);
			if(max_et != NULL && max_et->size() > 0 && (*max_et)[0] != -1)
				cut_out = cut_out || cut_max_et(*lepton_iter, (*max_et)[0]);
			if(electronID != NULL && electronID->size() > 0 && (*electronID)[0] != -1)
				cut_out = cut_out || cut_electronID(*lepton_iter, (*electronID)[0]);
        	        if(min_nHits != NULL && min_nHits->size() > 0 && (*min_nHits)[0] != -1)
				cut_out = cut_out || cut_nHits(*lepton_iter, (*min_nHits)[0]);
        	        if(min_nMuonHits != NULL && min_nMuonHits->size() > 0 && (*min_nMuonHits)[0] != -1)
				cut_out = cut_out || cut_nMuonHits(*lepton_iter, (*min_nMuonHits)[0]);
        	        if(min_nStripHits != NULL && min_nStripHits->size() > 0 && (*min_nStripHits)[0] != -1)
				cut_out = cut_out || cut_nStripHits(*lepton_iter, (*min_nStripHits)[0]);
        	        if(min_nPixelHits != NULL && min_nPixelHits->size() > 0 && (*min_nPixelHits)[0] != -1)
				cut_out = cut_out || cut_nPixelHits(*lepton_iter, (*min_nPixelHits)[0]);
        	        if(min_nPixelLayers != NULL && min_nPixelLayers->size() > 0 && (*min_nPixelLayers)[0] != -1)
				cut_out = cut_out || cut_nPixelLayers(*lepton_iter, (*min_nPixelLayers)[0]);
        	        if(min_nStations != NULL && min_nStations->size() > 0 && (*min_nStations)[0] != -1)
				cut_out = cut_out || cut_nStations(*lepton_iter, (*min_nStations)[0]);
        	        if(min_nMatchedSegments != NULL && min_nMatchedSegments->size() > 0 && (*min_nMatchedSegments)[0] != -1)
				cut_out = cut_out || cut_nMatchedSegments(*lepton_iter, (*min_nMatchedSegments)[0]);
        	        if(max_nLostTrackerHits != NULL && max_nLostTrackerHits->size() > 0 && (*max_nLostTrackerHits)[0] != -1)
				cut_out = cut_out || cut_nLostTrackerHits(*lepton_iter, (*max_nLostTrackerHits)[0]);
        	        if(max_chi2 != NULL && max_chi2->size() > 0 && (*max_chi2)[0] != -1)
				cut_out = cut_out || cut_chi2(*lepton_iter, (*max_chi2)[0]);
        	        if(max_d0 != NULL && max_d0->size() > 0 && (*max_d0)[0] != -1)
				cut_out = cut_out || cut_d0(*lepton_iter, (*max_d0)[0]);
        	        if(min_d0 != NULL && min_d0->size() > 0 && (*min_d0)[0] != -1)
				cut_out = cut_out || cut_min_d0(*lepton_iter, (*min_d0)[0]);
        	        if(max_vz != NULL && max_vz->size() > 0 && (*max_vz)[0] != -1)
				cut_out = cut_out || cut_vz(*lepton_iter, (*max_vz)[0]);
        	        if(max_d0sig != NULL && max_d0sig->size() > 0 && (*max_d0sig)[0] != -1)
				cut_out = cut_out || cut_d0sig(*lepton_iter, (*max_d0sig)[0]);
			if(min_eta != NULL && min_eta->size() > 0 && (*min_eta)[0] != -1)
				cut_out = cut_out || cut_eta(*lepton_iter, (*min_eta)[0], false);
			if(max_eta != NULL && max_eta->size() > 0 && (*max_eta)[0] != -1)
				cut_out = cut_out || cut_eta(*lepton_iter, (*max_eta)[0], true);
			//jl 04.11.10: dphi lep/met
			if(min_dphi != NULL && min_dphi->size() > 0 && (*min_dphi)[0] != -1)
				cut_out = cut_out || cut_min_dphi_lepmet(*lepton_iter, (*min_dphi)[0], mymet);
			if(max_dphi != NULL && max_dphi->size() > 0 && (*max_dphi)[0] != -1)
				cut_out = cut_out || cut_max_dphi_lepmet(*lepton_iter, (*max_dphi)[0], mymet);
			if(lepton_type != -1)
				cut_out = cut_out || cut_lepton_type(*lepton_iter, lepton_type);
			if(lepton_charge != -1)
				cut_out = cut_out || cut_lepton_charge(*lepton_iter, lepton_charge);
//			if(max_dcot != -1 || max_dist != -1 || (max_nLostTrackerHits != NULL && max_nLostTrackerHits->size() > 0 && (*max_nLostTrackerHits)[0] != -1))
//				cut_out = cut_out || cut_conv_rej(*lepton_iter);
			if(lep_match_trigger != -1)
				cut_out = cut_out || cut_trigger(*lepton_iter);

			cut_out = cut_out || cut_triangle(*lepton_iter, mymet); //jl 22.04.11: triangle cut

        	        if(!cut_out)
        	        {
        	                isolated_leptons->push_back(**lepton_iter);
        	        }
        	}
	}

	if(verbose)
		std::cout << "isolated leptons in event: " << isolated_leptons->size()  << std::endl;

        return isolated_leptons;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_chi2(myLepton *lepton_iter, double &max_chi2)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_chi2(), dropping ALL LEPTONS" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_chi2(mor::Muon *lepton_iter, double &max_chi2)
{
	if(lepton_iter->chi2() < max_chi2){
		return false;
	}
	else{
		return true;
	}
}

template <>
bool LeptonSelector<mor::Electron>::cut_chi2(mor::Electron *lepton_iter, double &max_chi2)
{
	std::cerr << "WARNING: electrons have no chi2 value in LeptonSelector::cut_chi2()" << std::endl;
	return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_d0(myLepton *lepton_iter, double &max_d0)
{
	double d0 = lepton_iter->d0();

	if(fabs(d0) < max_d0)
		return false;
	else
		return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_min_d0(myLepton *lepton_iter, double &min_d0)
{
	double d0 = lepton_iter->d0();

	if(fabs(d0) > min_d0)
		return false;
	else
		return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_vz(myLepton *lepton_iter, double &max_vz)
{
	if(!primary_vertices || primary_vertices->size() < 1){
		std::cerr << "WARNING: LeptonSelector<myLepton>::cut_vz(): no primary vertex found" << std::endl;
		if(primary_vertices) std::cerr << " Size vertex vector: " << primary_vertices->size() << std::endl; 
		return true;
	}

	double vz = fabs(lepton_iter->vz() - primary_vertices->begin()->z());

	if(fabs(vz) < max_vz)
		return false;
	else
		return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_d0sig(myLepton *lepton_iter, double &max_d0sig)
{
	double d0sig = -1000;

	if(lepton_iter->track_available()){
			double d0 = lepton_iter->d0();
			double d0sigma = lepton_iter->d0_sigma();
			d0sig = d0/d0sigma;
	}else{
		std::cerr << "WARNING: LeptonSelector: muon track not available" << std::endl;
		return true;
	}

	if(fabs(d0sig) < max_d0sig)
		return false;
	else
		return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_trigger(myLepton *lepton_iter)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_trigger()" << std::endl;
		return true;
}

template <>
bool LeptonSelector<mor::Electron>::cut_trigger(mor::Electron *lepton_iter)
{
	eire::TriggerNameProvider trig_name_prov;

	if(lepton_iter->triggered(trig_name_prov.hlt_name(int(lep_match_trigger))) || (lep_match_trigger == -1))
		return false;
	else
		return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_trigger(mor::Muon *lepton_iter)
{
	eire::TriggerNameProvider trig_name_prov;

	if(lepton_iter->triggered(trig_name_prov.hlt_name(int(lep_match_trigger))) || (lep_match_trigger == -1))
		return false;
	else
		return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nHits(myLepton *lepton_iter, double &min_nHits)
{
	std::cerr << "WARNING: lepton type in LeptonSelector::cut_nHits() has no nHits information" << std::endl;
        return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_nHits(mor::Muon *lepton_iter, double &min_nHits)
{
        if(lepton_iter->nHits() >= min_nHits)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nMuonHits(myLepton *lepton_iter, double &min_nMuonHits)
{
	std::cerr << "WARNING: lepton type in LeptonSelector::cut_nHits() has no nHits information" << std::endl;
        return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_nMuonHits(mor::Muon *lepton_iter, double &min_nMuonHits)
{
        if(lepton_iter->nMuonHits() >= min_nMuonHits){
                return false;
	}
        else{
                return true;
	}
}


template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nStripHits(myLepton *lepton_iter, double &min_nStripHits)
{
	std::cerr << "WARNING: lepton type in LeptonSelector::cut_nHits() has no nHits information" << std::endl;
        return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_nStripHits(mor::Muon *lepton_iter, double &min_nStripHits)
{
        if(lepton_iter->nStripHits() >= min_nStripHits){
                return false;
	}
        else{
                return true;
	}
}


template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nPixelHits(myLepton *lepton_iter, double &min_nPixelHits)
{
	std::cerr << "WARNING: lepton type in LeptonSelector::cut_nHits() has no nHits information" << std::endl;
        return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_nPixelHits(mor::Muon *lepton_iter, double &min_nPixelHits)
{
        if(lepton_iter->nPixelHits() >= min_nPixelHits){
                return false;
	}
        else{
                return true;
	}
}


template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nPixelLayers(myLepton *lepton_iter, double &min_nPixelLayers)
{
	std::cerr << "WARNING: lepton type in LeptonSelector::cut_nHits() has no nHits information" << std::endl;
        return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_nPixelLayers(mor::Muon *lepton_iter, double &min_nPixelLayers)
{
        if(lepton_iter->nPixelLayers() >= min_nPixelLayers){
                return false;
	}
        else{
                return true;
	}
}


template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nStations(myLepton *lepton_iter, double &min_nStations)
{
	std::cerr << "WARNING: lepton type in LeptonSelector::cut_nHits() has no nHits information" << std::endl;
        return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_nStations(mor::Muon *lepton_iter, double &min_nStations)
{
	double nstations = lepton_iter->nDTstations() + lepton_iter->nCSCstations();
        if(nstations >= min_nStations){
                return false;
	}
        else{
                return true;
	}

}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nMatchedSegments(myLepton *lepton_iter, double &min_nMatchedSegments)
{
	std::cerr << "WARNING: lepton type in LeptonSelector::cut_nHits() has no nHits information" << std::endl;
        return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_nMatchedSegments(mor::Muon *lepton_iter, double &min_nMatchedSegments)
{
	double nMatchedSegments = lepton_iter->nMatchedSegments();
        if(nMatchedSegments >= min_nMatchedSegments){
                return false;
	}
        else{
                return true;
	}
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_nLostTrackerHits(myLepton *lepton_iter, double &max_nLostTrackerHits)
{
	/*
	 * FIXME:
	 * accept all Electron from conversion rejection here and reject them
	 * in ConversionIdentifier to be able to change lepton definition in
	 * different steps of the selection as required by top ref selection v4
	 */
	return false;
	/*
        if(lepton_iter->nLostTrackerHits() <= max_nLostTrackerHits){
                return false;
	}
        else{
                return true;
	}
	*/
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_electronID(myLepton *lepton_iter, double &electronID)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_electronID()" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_electronID(mor::Muon *lepton_iter, double &electronID)
{
	std::cerr << "WARNING: electonID set for muons" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Electron>::cut_electronID(mor::Electron *lepton_iter, double &electronID)
{
	bool cut_out = false;
	if(electronID == 1 && lepton_iter->lepton_id_passed("eidRobustTight"))
		cut_out =  false;
	else if(electronID == 2 && lepton_iter->lepton_id_passed("eidRobustLoose"))
		cut_out =  false;
	else if(electronID == 3 && lepton_iter->lepton_id_passed("simpleEleId70Run2011"))
		cut_out =  false;
	else if (electronID == 4 && lepton_iter->lepton_id_passed("eidLoose"))
		cut_out =  false;//jl 01.09.10: add looser EMID criterion
	else if (electronID == 5 && lepton_iter->lepton_id_passed("simpleEleId95Run2011"))// "simpleEleId70cIso"))
		cut_out =  false;
	else if (electronID == 6 && lepton_iter->lepton_id_passed("eidVeryLooseMC"))
		cut_out =  false;
	else if (electronID == 7 && lepton_iter->lepton_id_passed("eidLooseMC"))
		cut_out =  false;
	else if (electronID == 8 && lepton_iter->lepton_id_passed("eidMediumMC"))
		cut_out =  false;
	else if (electronID == 9 && lepton_iter->lepton_id_passed("eidTightMC"))
		cut_out =  false;
	else if (electronID == 10 && lepton_iter->lepton_id_passed("eidSuperTightMC"))
		cut_out =  false;
	else if (electronID == 11 && lepton_iter->lepton_id_passed("eidHyperTight1MC"))
		cut_out =  false;
	else if (electronID == 12 && lepton_iter->lepton_id_passed("eidHyperTight2MC"))
		cut_out =  false;
	else if (electronID == 13 && lepton_iter->lepton_id_passed("eidHyperTight3MC"))
		cut_out =  false;
	else if (electronID == 14 && lepton_iter->lepton_id_passed("eidHyperTight4MC"))
		cut_out =  false;
	else if (electronID == 15 && lepton_iter->lepton_id_passed("eidVeryLooseMC_eID_only"))
		cut_out =  false;
	else if (electronID == 16 && lepton_iter->lepton_id_passed("eidLooseMC_eID_only"))
		cut_out =  false;
	else if (electronID == 17 && lepton_iter->lepton_id_passed("eidMediumMC_eID_only"))
		cut_out =  false;
	else if (electronID == 18 && lepton_iter->lepton_id_passed("eidTightMC_eID_only"))
		cut_out =  false;
	else if (electronID == 19 && lepton_iter->lepton_id_passed("eidSuperTightMC_eID_only"))
		cut_out =  false;
	else if (electronID == 20 && lepton_iter->lepton_id_passed("eidHyperTight1MC_eID_only"))
		cut_out =  false;
	else if (electronID == 21 && lepton_iter->lepton_id_passed("eidHyperTight2MC_eID_only"))
		cut_out =  false;
	else if (electronID == 22 && lepton_iter->lepton_id_passed("eidHyperTight3MC_eID_only"))
		cut_out =  false;
	else if (electronID == 23 && lepton_iter->lepton_id_passed("eidHyperTight4MC_eID_only"))
		cut_out =  false;
	else if(electronID > 23){//jl 01.09.10: this was 3
		cut_out = true;
		std::cerr << "WARNING: electronID is not valid in LeptonSelector::cut_electronID(): " << electronID << std::endl;
	}
	else cut_out = true;
	
	return cut_out;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_lepton_type(myLepton *lepton_iter, int &lepton_type)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_lepton_type()" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_lepton_type(mor::Muon *lepton_iter, int &lepton_type)
{
	bool cut_out = false;
	switch(lepton_type){
		case 0:
			if(!lepton_iter->lepton_id_passed("AllGlobalMuons"))
				cut_out = true;
			break;
		case 1:
			if(!lepton_iter->lepton_id_passed("AllTrackerMuons"))
				cut_out = true;
			break;
		case 2:
			if(!lepton_iter->lepton_id_passed("AllStandAloneMuons"))
				cut_out = true;
			break;
		case 100:
			if(!(lepton_iter->lepton_id_passed("AllGlobalMuons") && lepton_iter->lepton_id_passed("AllTrackerMuons")))
				cut_out = true;
			break;
		case 101:
			if(!(lepton_iter->lepton_id_passed("AllGlobalMuons") && lepton_iter->lepton_id_passed("AllTrackerMuons") &&  lepton_iter->lepton_id_passed("AllStandAloneMuons")))
				cut_out = true;
			break;
		default:
			cut_out = true;
			std::cerr << "WARNING: lepton type is not valid in LeptonSelector::cut_lepton_type()" << std::endl;
			break;
	}

	return cut_out;
}

template <>
bool LeptonSelector<mor::Electron>::cut_lepton_type(mor::Electron *lepton_iter, int &lepton_type)
{
	std::cerr << "WARNING: lepton_type " << lepton_type << " unknown for electrons: FILTERING ALL ELECTRONS" << std::endl;
	return true;
}


template <typename myLepton>
bool LeptonSelector<myLepton>::cut_trackiso(myLepton *lepton_iter, double &max_trackiso)
{
        if(lepton_iter->trackIso()<max_trackiso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_ecal_veto_cone(myLepton *lepton_iter, double &max_ecal_veto_cone)
{
        if(lepton_iter->ecal_vcone() < max_ecal_veto_cone)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_hcal_veto_cone(myLepton *lepton_iter, double &max_hcal_veto_cone)
{
        if(lepton_iter->hcal_vcone() < max_hcal_veto_cone)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_ecaliso(myLepton *lepton_iter, double &max_ecaliso)
{
        if(lepton_iter->ecalIso() < max_ecaliso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_caliso(myLepton *lepton_iter, double &max_caliso)
{
        if(lepton_iter->ecalIso()+lepton_iter->hcalIso() < max_caliso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_eta(myLepton *lepton_iter, double &limit_eta, bool max_cut)
{
	std::cerr << "WARNING: couldn't find type in LeptonSelector::cut_eta(), dropping ALL LEPTONS" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_eta(mor::Muon *lepton_iter, double &limit_eta, bool max_cut)
{
        if(max_cut && fabs(lepton_iter->Eta()) < limit_eta)
                return false;
        else if(!max_cut && fabs(lepton_iter->Eta()) >= limit_eta)
                return false;
        else
                return true;
}

template <>
bool LeptonSelector<mor::Electron>::cut_eta(mor::Electron *lepton_iter, double &limit_eta, bool max_cut)
{
	/*
	 * as described in reference selection: take eta of super cluster to exclude crack in detector
	 */
	double sc_eta = lepton_iter->supercluster_eta();
	double eta = lepton_iter->eta();

	if(exclude_eta_crack == -1) exclude_eta_crack = 0;
	// exclude transition region between barrel and endcap for electrons
        if(max_cut && (fabs(eta) < limit_eta) && (!exclude_eta_crack || (exclude_eta_crack && !(fabs(sc_eta) > 1.4442 && fabs(sc_eta) < 1.5660))))
                return false;
        else if(!max_cut && (fabs(eta) >= limit_eta) && (!exclude_eta_crack || (exclude_eta_crack && !(fabs(sc_eta) > 1.4442 && fabs(sc_eta) < 1.5660))))
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_hcaliso(myLepton *lepton_iter, double &max_hcaliso)
{
        if(lepton_iter->hcalIso() < max_hcaliso)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_pt(myLepton *lepton_iter, double &min_pt)
{
        if(lepton_iter->Pt() > min_pt)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_max_pt(myLepton *lepton_iter, double &max_pt)
{
        if(lepton_iter->Pt() < max_pt)
                return false;
        else
                return true;
}


template <typename myLepton>
bool LeptonSelector<myLepton>::cut_et(myLepton *lepton_iter, double &min_et)
{
        if(lepton_iter->Et() > min_et)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_max_et(myLepton *lepton_iter, double &max_et)
{
        if(lepton_iter->Et() < max_et)
                return false;
        else
                return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_conv_rej(myLepton *lepton_iter)
{
	std::cerr << "WARNING: LeptonSelector::cut_conv_rej(): conv_rej not available for this type of lepton" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Electron>::cut_conv_rej(mor::Electron *lepton_iter)
{
	if(conversion_identifier->from_conversion(&(*lepton_iter)))
		return false;
	else
		return true;
	
	/*
	 * FIXME:
	 * accept all Electron from conversion rejection here and reject them
	 * in ConversionIdentifier to be able to change lepton definition in
	 * different steps of the selection as required by top ref selection v4
	return false;
	 */
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_lepton_charge(myLepton *lepton_iter, int &charge)
{
	switch((int) charge){
		case 1:
			if(lepton_iter->charge() == 1)
				return false;
			break;
		case 2:
			if(lepton_iter->charge() == -1)
				return false;
			break;
		default:
			std::cerr << "LeptonSelector<myLepton>::cut_lepton_charge: invalid charge: " << charge << std::endl;
			break;
	};
	return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_dR(myLepton *lepton_iter, double &min_dR)
{
        for(std::vector<mor::Jet>::iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter)
          {
            double dR = ROOT::Math::VectorUtil::DeltaR(*lepton_iter,*jet_iter);
	      if(verbose) std::cout << "dR(jet, mu): " << dR << " min_dR: " << min_dR << std::endl;
            if(dR < min_dR)
              return true;
          }
        return false;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_max_dR(myLepton *lepton_iter, double &max_dR)
{
        for(std::vector<mor::Jet>::iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter)
          {
            double dR = ROOT::Math::VectorUtil::DeltaR(*lepton_iter,*jet_iter);
	      if(verbose) std::cout << "dR(jet, mu): " << dR << " max_dR: " << max_dR << std::endl;
            if(dR < max_dR)
              return false;
          }
	//std::cout << "drop" << std::endl;
        return true;
}
	
template <typename myLepton>
bool LeptonSelector<myLepton>::cut_inverse_relIso(myLepton *lepton_iter, double &min_relIso)
{
	double relIso=0;
	//relIso = lepton_iter->Pt()/(lepton_iter->Pt() + lepton_iter->ecalIso()
	//			    + lepton_iter->hcalIso() + lepton_iter->trackIso());
	 relIso = (lepton_iter->ecalIso() + lepton_iter->hcalIso() + lepton_iter->trackIso())/lepton_iter->Pt(); //reflect right reliso
	
	std::cout << " WARNING: Cutting on min Inverse relIso > " << min_relIso << ". Max relIso is recommended"<< std::endl;
	if(relIso <= min_relIso)
		return true;
	else
		return false;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_relIso(myLepton *lepton_iter, double &min_relIso)
{
	std::cerr << "LeptonSelector<myLepton>::cut_relIso(): wrong lepton type" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_relIso(mor::Muon *lepton_iter, double &min_relIso)
{
	/*
	 * change of definition of relIso... therefore min_relIso should be called max_relIso
	 */
	double relIso=0;
	relIso = (lepton_iter->ecalIso() + lepton_iter->hcalIso() + lepton_iter->trackIso())/lepton_iter->Pt();

	if(relIso > min_relIso)
		return true;
	else
        	return false;
}

template <>
bool LeptonSelector<mor::Electron>::cut_relIso(mor::Electron *lepton_iter, double &min_relIso)
{
	double relIso=0;
	relIso = (lepton_iter->ecalIso() + lepton_iter->hcalIso() + lepton_iter->trackIso())/lepton_iter->Et();

	if(relIso > min_relIso)
		return true;
	else
        	return false;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_PFrelIso(myLepton *lepton_iter, double &max_relIso)
{
	std::cerr << "LeptonSelector<myLepton>::cut_relIso(): wrong lepton type" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_PFrelIso(mor::Muon *lepton_iter, double &max_relIso)
{
	/*
	 * change of definition of relIso... therefore min_relIso should be called max_relIso
	 */
	double relIso  = lepton_iter->PFrelIso();
	//relIso = (lepton_iter->chargedHadronIso(conesize) + lepton_iter->neutralHadronIso(conesize) + lepton_iter->photonIso(conesize))/lepton_iter->Pt();

	if(relIso > max_relIso)
		return true;
	else
        	return false;
}

template <>
bool LeptonSelector<mor::Electron>::cut_PFrelIso(mor::Electron *lepton_iter, double &max_relIso)
{
	double relIso = lepton_iter->PFrelIso();
	//relIso = (lepton_iter->chargedHadronIso(conesize) + lepton_iter->neutralHadronIso(conesize) + lepton_iter->photonIso(conesize))/lepton_iter->Pt();

	if(relIso > max_relIso)
		return true;
	else
        	return false;
}

//jl 16.09.11: antireliso for pfreliso
template <typename myLepton>
bool LeptonSelector<myLepton>::cut_min_PFrelIso(myLepton *lepton_iter, double &min_relIso)
{
	std::cerr << "LeptonSelector<myLepton>::cut_min_relIso(): wrong lepton type" << std::endl;
	return true;
}

template <>
bool LeptonSelector<mor::Muon>::cut_min_PFrelIso(mor::Muon *lepton_iter, double &min_relIso)
{
	/*
	 * change of definition of relIso... therefore min_relIso should be called max_relIso
	 */
	double relIso = lepton_iter->PFrelIso();
	//relIso = (lepton_iter->chargedHadronIso(conesize) + lepton_iter->neutralHadronIso(conesize) + lepton_iter->photonIso(conesize))/lepton_iter->Pt();

	if(relIso < min_relIso)
		return true;
	else
        	return false;
}

template <>
bool LeptonSelector<mor::Electron>::cut_min_PFrelIso(mor::Electron *lepton_iter, double &min_relIso)
{
	double relIso = lepton_iter->PFrelIso();
	//relIso = (lepton_iter->chargedHadronIso(conesize) + lepton_iter->neutralHadronIso(conesize) + lepton_iter->photonIso(conesize))/lepton_iter->Pt();

	if(relIso < min_relIso)
		return true;
	else
        	return false;
}

//jl 04.11.10: dphi lep/met
template <typename myLepton>
bool LeptonSelector<myLepton>::cut_min_dphi_lepmet(myLepton *lepton_iter, double &min_dphi, std::vector<mor::MET> *mymet)
{
	double dphi = 0.;
//	for(std::vector<mor::MET>::iterator met_iter = met->begin();met_iter!=met->end();++met_iter)
//	{
		dphi = lepton_iter->Phi() - (*mymet)[0].Phi();
		if (dphi>TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
		if (dphi<-TMath::Pi()) dphi = 2*TMath::Pi() + dphi;       
		dphi = fabs(dphi);
//	}
	if(dphi>min_dphi)
		return false;
	else    
		return true;
}

template <typename myLepton>
bool LeptonSelector<myLepton>::cut_max_dphi_lepmet(myLepton *lepton_iter, double &max_dphi, std::vector<mor::MET> *mymet)
{       
	double dphi = 0.;
//	for(std::vector<mor::MET>::iterator met_iter = met->begin();met_iter!=met->end();++met_iter)
//	{
		dphi = lepton_iter->Phi() - (*mymet)[0].Phi();
		if (dphi>TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
		if (dphi<-TMath::Pi()) dphi = 2*TMath::Pi() + dphi;
		dphi = fabs(dphi);
//	}
	if(dphi<max_dphi)
		return false;
	else    
		return true;
}

//jl 21.04.11: triangle cut
template <typename myLepton> 
		bool LeptonSelector<myLepton>::cut_triangle(myLepton *lepton_iter, std::vector<mor::MET> *mymet)
{
        double mw     = -1.;
	double trival = -2.;
        trival = 115 - 0.5 * (*mymet)[0].Et();
	mw = ( lepton_iter->p4() + (*mymet)[0].p4() ).Mt();
//	if(mw > trival)
                return false; //mW above threshold, do not remove the event
//	else
//		return true;
}

template <class myLepton>
void LeptonSelector<myLepton>::change_lepton_scale(std::vector<myLepton*> &leptons)
{
}

template <>
void LeptonSelector<mor::Electron>::change_lepton_scale(std::vector<mor::Electron*> &leptons)
{
	for(std::vector<mor::Electron*>::iterator lepton = leptons.begin();
		lepton != leptons.end();
		++lepton){
		double px, py, pz, e;
		double scale = 1.;
		if(lepton_scale == 1){
			if(fabs((*lepton)->supercluster_eta()) > 1.5660)
				scale = 0.975;
			else
				scale = 1.;
		}else if(lepton_scale == 2){
			if(fabs((*lepton)->supercluster_eta()) > 1.5660)
				scale = 1.025;
			else
				scale = 1.;
		}
		px = (*lepton)->Px() * scale;
		py = (*lepton)->Py() * scale;
		pz = (*lepton)->Pz() * scale;
		e = (*lepton)->E() * scale;
		
		(*lepton)->SetPxPyPzE(px, py, pz, e);
	}
}

template class LeptonSelector<mor::Muon>;
template class LeptonSelector<mor::Electron>;
