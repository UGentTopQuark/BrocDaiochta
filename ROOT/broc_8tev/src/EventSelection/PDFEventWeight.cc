#include "../../interface/EventSelection/PDFEventWeight.h"

broc::PDFEventWeight::PDFEventWeight(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	pdf_weight_prov = handle_holder->get_pdf_weight_provider();
	pdf_weight_pos = (int) cuts_set->get_cut_value("pdf_weight_pos");
	pdf_name = handle_holder->get_config_reader()->get_var("pdf_name", "event_weights", false);
	apply_pdf_weights = handle_holder->get_config_reader()->get_bool_var("pdf_weights","event_weights",false);
	suppress_pdf_offset_correction = handle_holder->get_config_reader()->get_bool_var("suppress_pdf_offset_correction","event_weights",false);
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;


	if(pdf_name == ""){
		std::cout << "INFO: PDFEventWeight pdf_name in config not set (assuming you want: cteq66), IGNORING pdf event weights..." << std::endl;
		pdf_name = "cteq66";
		pdf_weight_pos = -1;
	}

	event_weight = 1.;
}

broc::PDFEventWeight::~PDFEventWeight()
{
}

double broc::PDFEventWeight::get_weight()
{
	event_weight = 1.;
	if(do_not_reweight) return 1.;

	if(apply_pdf_weights){
		if(pdf_weight_pos != -1) event_weight = pdf_weight_prov->get_event_weight(pdf_name, pdf_weight_pos);
	}else if(!suppress_pdf_offset_correction){
		event_weight = pdf_weight_prov->get_event_weight(pdf_name, 0);	// reweight from cteq6l to cteq66
	}

	return event_weight;
}
