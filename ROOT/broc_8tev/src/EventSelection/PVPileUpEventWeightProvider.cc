#include "../../interface/EventSelection/PVPileUpEventWeightProvider.h"

broc::PVPileUpEventWeightProvider::PVPileUpEventWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder): broc::EventWeightProvider(weight_file, handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	pvertices = handle_holder->get_tight_primary_vertices();
}

broc::PVPileUpEventWeightProvider::~PVPileUpEventWeightProvider()
{
}

double broc::PVPileUpEventWeightProvider::get_value_for_event()
{
	// for now reweighting as function of primary vertices in the event
	return pvertices->size();
}

double broc::PVPileUpEventWeightProvider::get_weight()
{
	double value = get_value_for_event();
	if(do_not_reweight)
		return 1.;
	else
		return weights[find_bin(value)];
}
