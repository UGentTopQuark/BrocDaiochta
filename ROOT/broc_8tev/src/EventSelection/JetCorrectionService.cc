#include "../../interface/EventSelection/JetCorrectionService.h"

eire::JetCorrectionService::JetCorrectionService()
{
	JEC = NULL;
	corrected_jets = new std::vector<mor::Jet>();
}

eire::JetCorrectionService::~JetCorrectionService()
{
	if(corrected_jets){
		delete corrected_jets;
		corrected_jets = NULL;
	}
	if(JEC){
		delete JEC;
		JEC = NULL;
	}
}

void eire::JetCorrectionService::initialise(eire::ConfigReader *config_reader)
{
	std::vector<std::string> jet_corrections = config_reader->get_vec_var("JEC","global",true);
	std::string JEC_dir = config_reader->get_var("JEC_dir","global",true);
	if(jet_corrections.size() > 0){
		for(std::vector<std::string>::iterator jet_correction = jet_corrections.begin();
			jet_correction != jet_corrections.end();
			++jet_correction){
			jec_parameters.push_back(JetCorrectorParameters(JEC_dir+*jet_correction));
		}

		JEC = new FactorizedJetCorrector(jec_parameters);
	}
}

void eire::JetCorrectionService::correct_jets(std::vector<mor::Jet> *uncorrected_jets, std::vector<mor::Jet> *corrected_jets, double rho)
{
	corrected_jets->clear();
	for(std::vector<mor::Jet>::iterator jet_iter = uncorrected_jets->begin();
	    jet_iter!=uncorrected_jets->end();
	    ++jet_iter){
	  
	  	mor::Jet corr_jet(*jet_iter);

		if(JEC != NULL){
			JEC->setJetEta(corr_jet.eta());
			JEC->setJetPt(corr_jet.pt());
			JEC->setRho(rho);
			JEC->setJetA(jet_iter->jet_area());	// for AK5 jets
			double corr = JEC->getCorrection();

			corr_jet.SetPt(jet_iter->Pt());
			corr_jet.SetEta(jet_iter->eta());
			corr_jet.SetPhi(jet_iter->Phi());
			corr_jet.SetM(jet_iter->mass());
			corr_jet *= corr;
		}
		/*
		if(jet_iter->pt() > 30.){
			std::cout << "jet area: " << jet_iter->jet_area() << std::endl;
			std::cout << "uncorrected jet pt: " << jet_iter->pt() << std::endl;
			std::cout << "corrected jet pt: " << corr_jet.pt() << std::endl;
//			std::cout << "uncorrected jet eta: " << jet_iter->eta() << std::endl;
//			std::cout << "corrected jet eta: " << corr_jet.eta() << std::endl;
			std::cout << "---" << std::endl;
		*/
		//}
		corrected_jets->push_back(corr_jet);
	}
}
