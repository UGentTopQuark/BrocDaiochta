#include "../../interface/EventSelection/SpinPlots.h"

SpinPlots::SpinPlots(std::string ident, bool gen_evt_info)
{
  id = ident;
  plot_gen_evt_info = gen_evt_info;
}

SpinPlots::~SpinPlots()
{
  this->ttbar = false;
  this->decay = NULL;
  this->lepton = NULL;
  this->quark = NULL;
  this->quark_bar = NULL;
  this->Top_had = NULL;
  this->Top_lep = NULL;
  this->B_had = NULL;
  this->B_lep = NULL;
  this->W_had = NULL;
  this->W_lep = NULL;
  this->evt_info = NULL;
  this->plot_gen_evt_info = false;
}

void SpinPlots::plot_all()
{
  initialise();
  select_and_plot();
  //  plot_ZMFPhi();
  //  plot_ZMFPhi();
  //plot_deltaPhi();
  //plot_deltaPhiInvMassConstraint();
}

void SpinPlots::initialise()
{
  this->ttbar = gen_evt->is_ttbar();
  this->decay = gen_evt->decay_channel();
  this->lepton = gen_evt->reco_lepton();
  //this->quark = gen_evt->q_jet();
  //  this->quark_bar = gen_evt->qbar_jet();
  this->quark = gen_evt->q();
  this->quark_bar = gen_evt->qbar();
  this->Top_had = gen_evt->hadT();
  this->Top_lep = gen_evt->lepT();
  //this->B_had = gen_evt->hadB_jet();
  //this->B_lep = gen_evt->lepB_jet();
  this->B_had = gen_evt->hadB();
  this->B_lep = gen_evt->lepB();
  this->W_had = gen_evt->hadW();
  this->W_lep = gen_evt->lepW();
  this->evt_info = handle_holder->get_event_information();
}

void SpinPlots::book_histos()
{
  histos1d[("downtaggingeff"+id).c_str()]=histo_writer->create_1d(("downtaggingeff"+id).c_str(),"efficiency of downtagging",3,0.,3.,"Lowest Energy - Closest to B - both");
  histos1d[("downtaggingpur"+id).c_str()]=histo_writer->create_1d(("downtaggingpur"+id).c_str(),"purity of downtagging",4,-1,3.,"Lowest Energy - Closest to B - both");

  histos1d[("coslb"+id).c_str()]=histo_writer->create_1d(("coslb"+id).c_str(),"cos phi between lepton and b-type quark in their top rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosdb"+id).c_str()]=histo_writer->create_1d(("cosdb"+id).c_str(),"cos phi between down and b-type quark in their top rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosld"+id).c_str()]=histo_writer->create_1d(("cosld"+id).c_str(),"cos phi between lepton and d-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosldZMF"+id).c_str()]=histo_writer->create_1d(("cosldZMF"+id).c_str(),"cos phi between lepton and d-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos1d[("coslu"+id).c_str()]=histo_writer->create_1d(("coslu"+id).c_str(),"cos phi between lepton and u-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos1d[("coslW"+id).c_str()]=histo_writer->create_1d(("coslW"+id).c_str(),"cos phi between lepton and hadronic W in top parent rest frame",50,-1,1,"cos delta phi");
  histos2d[("cosltdt"+id).c_str()]=histo_writer->create_2d(("cosltdt"+id).c_str(),"cos phi between lepton and t and d-type quark and t quark in top rest frame",50,-1,1,50,-1,1,"cos lt","cos dt");
  histos2d[("cosltbt"+id).c_str()]=histo_writer->create_2d(("cosltbt"+id).c_str(),"cos phi between lepton and t and b-type quark and t quark in top rest frame",50,-1,1,50,-1,1,"cos lt","cos bt");

  histos1d[("coslb_M"+id).c_str()]=histo_writer->create_1d(("coslb_M"+id).c_str(),"cos phi between lepton and b-type quark in their top rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosdb_M"+id).c_str()]=histo_writer->create_1d(("cosdb_M"+id).c_str(),"cos phi between down and b-type quark in their top rest frame",50,-1,1,"cos delta phi");
  histos1d[("cosld_M"+id).c_str()]=histo_writer->create_1d(("cosld_M"+id).c_str(),"cos phi between lepton and d-type quark in top parent rest frame",50,-1,1,"cos delta phi");
 histos1d[("cosldZMF_M"+id).c_str()]=histo_writer->create_1d(("cosldZMF_M"+id).c_str(),"cos phi between lepton and d-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos1d[("coslu_M"+id).c_str()]=histo_writer->create_1d(("coslu_M"+id).c_str(),"cos phi between lepton and u-type quark in top parent rest frame",50,-1,1,"cos delta phi");
  histos2d[("cosltdt_M"+id).c_str()]=histo_writer->create_2d(("cosltdt_M"+id).c_str(),"cos phi between lepton and t and d-type quark and t quark in top rest frame",50,-1,1,50,-1,1,"cos lt","cos dt");
  histos2d[("cosltbt_M"+id).c_str()]=histo_writer->create_2d(("cosltbt_M"+id).c_str(),"cos phi between lepton and t and b-type quark and t quark in top rest frame",50,-1,1,50,-1,1,"cos lt","cos bt");

  histos1d[("phi_l"+id).c_str()]=histo_writer->create_1d(("phi_l"+id).c_str(),"phi of lepton",50,-3.14,3.14,"phi_l");
  histos1d[("phi_top"+id).c_str()]=histo_writer->create_1d(("phi_top"+id).c_str(),"phi of top",50,-3.14,3.14,"phi_t");
  histos1d[("phi_lToprest"+id).c_str()]=histo_writer->create_1d(("phi_lToprest"+id).c_str(),"phi of lepton in top rest frame",50,-3.14,3.14,"phi_lToprest");
  histos1d[("phi_d"+id).c_str()]=histo_writer->create_1d(("phi_d"+id).c_str(),"phi of down",50,-3.14,3.14,"phi_d");
  histos1d[("phi_dToprest"+id).c_str()]=histo_writer->create_1d(("phi_dToprest"+id).c_str(),"phi of down in top rest frame",50,-3.14,3.14,"phi_d");

  histos1d[("eta_l"+id).c_str()]=histo_writer->create_1d(("eta_l"+id).c_str(),"eta of lepton ",50,-3.14,3.14,"eta_l");
  histos1d[("eta_top"+id).c_str()]=histo_writer->create_1d(("eta_top"+id).c_str(),"eta of top",50,-3.14,3.14,"eta_top");
  histos1d[("eta_lToprest"+id).c_str()]=histo_writer->create_1d(("eta_lToprest"+id).c_str(),"eta of lepton in top rest frame",50,-3.14,3.14,"eta_lToprest");
  histos1d[("eta_d"+id).c_str()]=histo_writer->create_1d(("eta_d"+id).c_str(),"eta of down",50,-3.14,3.14,"eta_d");
  histos1d[("eta_dToprest"+id).c_str()]=histo_writer->create_1d(("eta_dToprest"+id).c_str(),"eta of down in top rest frame",50,-3.14,3.14,"eta_d");
}

void SpinPlots::print_info(double x1, double x2, double x3, double x4, double x5){
  std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
  std::cout<<"%%%%%%Printing info Spin Correlations%%%%%%"<<std::endl;
  std::cout<<std::setprecision(10)<<"%%Event number: "<<evt_info->event_number()<<"  lumi block: "<<evt_info->lumi_block()<<"  run: "<<evt_info->run()<<std::endl;
  std::cout<<"%%hadronic top: pt= "<<Top_had->p4().Pt()<<" phi= "<<Top_had->p4().Phi()<<" eta= "<<Top_had->p4().Eta()<<"vs mc_matched= "<<Top_had->mc_matched()<<" pt= "<<Top_had->mc_p4().Pt()<<" phi= "<<Top_had->mc_p4().Phi()<<" eta= "<<Top_had->mc_p4().Eta()<<std::endl;
  std::cout<<"%%leptonic top: pt= "<<Top_lep->p4().Pt()<<" phi= "<<Top_lep->p4().Phi()<<" eta= "<<Top_lep->p4().Eta()<<"vs mc_matched= "<<Top_lep->mc_matched()<<" pt= "<<Top_lep->mc_p4().Pt()<<" phi= "<<Top_lep->mc_p4().Phi()<<" eta= "<<Top_lep->mc_p4().Eta()<<std::endl;
  std::cout<<"%%hadronic W: pt= "<<W_had->p4().Pt()<<" phi= "<<W_had->p4().Phi()<<" eta= "<<W_had->p4().Eta()<<"vs mc_matched= "<<W_had->mc_matched()<<" pt= "<<W_had->mc_p4().Pt()<<" phi= "<<W_had->mc_p4().Phi()<<" eta= "<<W_had->mc_p4().Eta()<<std::endl;
 std::cout<<"%%leptonic W: pt= "<<W_lep->p4().Pt()<<" phi= "<<W_lep->p4().Phi()<<" eta= "<<W_lep->p4().Eta()<<"vs mc_matched= "<<W_lep->mc_matched()<<" pt= "<<W_lep->mc_p4().Pt()<<" phi= "<<W_lep->mc_p4().Phi()<<" eta= "<<W_lep->mc_p4().Eta()<<std::endl;
 std::cout<<"%%hadronic B: pt= "<<B_had->p4().Pt()<<" phi= "<<B_had->p4().Phi()<<" eta= "<<B_had->p4().Eta()<<"vs mc_matched= "<<B_had->mc_matched()<<" pt= "<<B_had\
   ->mc_p4().Pt()<<" phi= "<<B_had->mc_p4().Phi()<<" eta= "<<B_had->mc_p4().Eta()<<std::endl;
 std::cout<<"%%leptonic B: pt= "<<B_lep->p4().Pt()<<" phi= "<<B_lep->p4().Phi()<<" eta= "<<B_lep->p4().Eta()<<"vs mc_matched= "<<B_lep->mc_matched()<<" pt= "<<B_lep->mc_p4().Pt()<<" phi= "<<B_lep->mc_p4().Phi()<<" eta= "<<B_lep->mc_p4().Eta()<<std::endl;
 std::cout<<"%%lepton: pt= "<<lepton->p4().Pt()<<" phi= "<<lepton->p4().Phi()<<" eta= "<<lepton->p4().Eta()<<"vs mc_matched= "<<lepton->mc_matched()<<" pt= "<<lepton->mc_p4().Pt()<<" phi= "<<lepton->mc_p4().Phi()<<" eta= "<<lepton->mc_p4().Eta()<<std::endl;
 std::cout<<"%%decay quark: pt= "<<quark->p4().Pt()<<" phi= "<<quark->p4().Phi()<<" eta= "<<quark->p4().Eta()<<"vs mc_matched= "<<quark->mc_matched()<<" pt= "<<quark->mc_p4().Pt()<<" phi= "<<quark->mc_p4().Phi()<<" eta= "<<quark->mc_p4().Eta()<<std::endl;
 std::cout<<"%%decay anti-quark: pt= "<<quark_bar->p4().Pt()<<" phi= "<<quark_bar->p4().Phi()<<" eta= "<<quark_bar->p4().Eta()<<"vs mc_matched= "<<quark_bar->mc_matched()<<" pt= "<<quark_bar->mc_p4().Pt()<<" phi= "<<quark_bar->mc_p4().Phi()<<" eta= "<<quark_bar->mc_p4().Eta()<<std::endl;
 std::cout<<"%%%%%%%1D variables%%%%%%%%%%%%%%%"<<std::endl;
 std::cout<<"coslb= "<<x1<<std::endl;
 std::cout<<"cosdb= "<<x2<<std::endl;
 std::cout<<"cosld= "<<x3<<std::endl;
 std::cout<<"coslu= "<<x4<<std::endl;
 std::cout<<"coslW= "<<x5<<std::endl;
 std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
 std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
}

void SpinPlots::select_and_plot()
{
  //check if it is a semi-leptonic decay
  if(!ttbar || (decay != 1 && decay != 2)){return;}


  //boost all the particles to the parent top rest frame
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > Lep = ToTopRestFrame(lepton, true);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > T_h = ToTopPairRestFrame(Top_had);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > T_l = ToTopPairRestFrame(Top_lep);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > B_h = ToTopRestFrame(B_had, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > B_l = ToTopRestFrame(B_lep, true);

  //boost all the particles to the ttbar pair rest frame
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > LepTTbar = ToTopPairRestFrame(lepton);

  mor::Particle* Down = ChooseD();
  mor::Particle* U;
  if(Down->mc_match_id() == -3 || Down->mc_match_id() == -1){U = quark;}
  else{U = quark_bar;}
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > DownT = ToTopRestFrame(Down, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > UT = ToTopRestFrame(U, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > DownTpair = ToTopPairRestFrame(Down);

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > WT = ToTopRestFrame(W_had, false);

  //checkplots
  histos1d[("phi_l"+id).c_str()]->Fill(lepton->p4().Phi());
  histos1d[("phi_top"+id).c_str()]->Fill(Top_had->p4().Phi());
  histos1d[("phi_top"+id).c_str()]->Fill(Top_lep->p4().Phi());
  histos1d[("eta_top"+id).c_str()]->Fill(Top_had->p4().Eta());
  histos1d[("eta_top"+id).c_str()]->Fill(Top_lep->p4().Eta());
  histos1d[("eta_l"+id).c_str()]->Fill(lepton->p4().Eta());
  histos1d[("phi_lToprest"+id).c_str()]->Fill(Lep.Phi());
  histos1d[("eta_lToprest"+id).c_str()]->Fill(Lep.Eta());
  histos1d[("phi_d"+id).c_str()]->Fill(Down->p4().Phi());
  histos1d[("eta_d"+id).c_str()]->Fill(Down->p4().Eta());
  histos1d[("phi_dToprest"+id).c_str()]->Fill(DownT.Phi());
  histos1d[("eta_dToprest"+id).c_str()]->Fill(DownT.Eta());

  //in helicity frame
  double x1 = plot1D(Lep,B_h,"coslb");
  double x2 = plot1D(DownT, B_l,"cosdb");
  double x3 = plot1D(Lep, DownT,"cosld");
  double x4 = plot1D(Lep, UT, "coslu");
  double x5 = plot1D(Lep, WT, "coslW");
  plot2D(Lep,T_l,DownT,T_h,"cosltdt");

  if(plot_gen_evt_info){print_info(x1,x2,x3,x4,x5);}

  plot2D(lepton->p4(),Top_lep->p4(),B_had->p4(),Top_had->p4(),"cosltbt");

  //in ZMF
  double x = plot1D(LepTTbar, DownTpair,"cosldZMF");

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > topPairCmf( Top_had->p4() + Top_lep->p4() );
  if(topPairCmf.M() > 400){return;}
  //if invariant mass cut is passed
  //in helicity frame
  x= plot1D(Lep,B_h,"coslb_M");
  x= plot1D(DownT, B_l,"cosdb_M");
  x= plot1D(Lep, DownT,"cosld_M");
  plot2D(Lep,T_l,DownT,T_h,"cosltdt_M");

  plot2D(Lep,T_l,B_h,T_h,"cosltbt_M");

  //in ZMF
  x= plot1D(LepTTbar, DownTpair,"cosldZMF_M");

}

mor::Particle* SpinPlots::ChooseD()
{
  mor::Particle* D;
  mor::Particle* U;

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > QT = ToTopRestFrame(quark, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > QT_bar = ToTopRestFrame(quark_bar, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > QW = ToWRestFrame(quark, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > QW_bar = ToWRestFrame(quark_bar, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > BW = ToWRestFrame(B_had, false);
  double dRQ = ROOT::Math::VectorUtil::DeltaR(QW, BW);
  double dRQ_bar = ROOT::Math::VectorUtil::DeltaR(QW_bar, BW);
  if(QT.E() < QT_bar.E()){
    if(quark->mc_match_id() == 3 || quark->mc_match_id() == 1){ histos1d[("downtaggingpur"+id).c_str()]->Fill(0.5);}
    if(dRQ < dRQ_bar){
      histos1d[("downtaggingpur"+id).c_str()]->Fill(-0.5);
      if(quark->mc_match_id() == 3 || quark->mc_match_id() == 1){ histos1d[("downtaggingpur"+id).c_str()]->Fill(2.5);}
    }
  }
  else{
    if(quark_bar->mc_match_id() == (-3) || quark_bar->mc_match_id() == (-1)){histos1d[("downtaggingpur"+id).c_str()]->Fill(0.5);}
    if(dRQ_bar < dRQ){
      histos1d[("downtaggingpur"+id).c_str()]->Fill(-0.5);
      if(quark_bar->mc_match_id() == (-3) || quark_bar->mc_match_id() == (-1)){ histos1d[("downtaggingpur"+id).c_str()]->Fill(2.5);}
    }
  }
  if(dRQ < dRQ_bar){
    if(quark->mc_match_id() == 3 || quark->mc_match_id() == 1){ histos1d[("downtaggingpur"+id).c_str()]->Fill(1.5);}
  }
  else{
      if(quark_bar->mc_match_id() == (-3) || quark_bar->mc_match_id() == (-1)){ histos1d[("downtaggingpur"+id).c_str()]->Fill(1.5);}
  }

  if(quark->mc_match_id() == 3 || quark->mc_match_id() == 1){D = quark; U = quark_bar;}
  else{D = quark_bar; U = quark;}
  
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > DT = ToTopRestFrame(D, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > UT = ToTopRestFrame(U, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > DW = ToWRestFrame(D, false);
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > UW = ToWRestFrame(U, false);
  if(DT.E() < UT.E()){histos1d[("downtaggingeff"+id).c_str()]->Fill(0.5);}
  //  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > BW = ToWRestFrame(B_had, false);
  double dRD = ROOT::Math::VectorUtil::DeltaR(DW, BW);
  double dRU = ROOT::Math::VectorUtil::DeltaR(UW, BW);
  if(dRD < dRU){histos1d[("downtaggingeff"+id).c_str()]->Fill(1.5);}
  if((dRD < dRU) && (DT.E() < UT.E())){histos1d[("downtaggingeff"+id).c_str()]->Fill(2.5);}
  return D;

}

double SpinPlots::plot1D(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part1, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part2, std::string histname)
{
  double Cos = part1.Vect().Unit().Dot(part2.Vect().Unit());
  histos1d[(histname+id).c_str()]->Fill(Cos);
  return Cos;
}

void SpinPlots::plot2D(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part1, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part2, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part3, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > part4, std::string histname)
{
  double Cos1 = part1.Vect().Unit().Dot(part2.Vect().Unit());
  double Cos2 = part3.Vect().Unit().Dot(part4.Vect().Unit());
  histos2d[(histname+id).c_str()]->Fill(Cos1,Cos2);
}

ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > SpinPlots::ToZeroMom(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > particle0, ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > particle1)
{
  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p0(particle0.Px(), particle0.Py(), particle0.Pz(), particle0.E());
  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p1(particle1.Px(), particle1.Py(), particle1.Pz(), particle1.E());
 
  double mass, gamma, beta[3], bdotp;
  mass = sqrt(fabs(p0.E()*p0.E()-p0.Px()*p0.Px()-p0.Py()*p0.Py()-p0.Pz()*p0.Pz()));
  gamma = p0.E()/mass;
  bdotp = 0.0;
  beta[0]= p0.Px()/p0.E();
  beta[1]= p0.Py()/p0.E();
  beta[2]= p0.Pz()/p0.E();
  bdotp = p1.Px()*beta[0] + p1.Py()*beta[1] + p1.Pz()*beta[2]; 

  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > p_out(p1.Px()+gamma*beta[0]*(gamma/(gamma+1.)*bdotp-p1.E()),p1.Py()+gamma*beta[1]*(gamma/(gamma+1.)*bdotp-p1.E()),p1.Pz()+gamma*beta[2]*(gamma/(gamma+1.)*bdotp-p1.E()), gamma*(p1.E()-bdotp));

  return p_out;
}

ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > SpinPlots::ToWRestFrame(mor::Particle * particle, bool Leptonic)
{
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  ParticleTRest;
  if(Leptonic){ParticleTRest = ROOT::Math::VectorUtil::boost( particle->p4(), W_lep->p4().BoostToCM() ) ;}
  else{ParticleTRest = ROOT::Math::VectorUtil::boost( particle->p4(), W_had->p4().BoostToCM() ) ;}

  return ParticleTRest;
}

ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > SpinPlots::ToTopRestFrame(mor::Particle * particle, bool Leptonic)
{
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  ParticleTRest;
  if(Leptonic){ParticleTRest = ROOT::Math::VectorUtil::boost( particle->p4(), Top_lep->p4().BoostToCM() ) ;}
  else{ParticleTRest = ROOT::Math::VectorUtil::boost( particle->p4(), Top_had->p4().BoostToCM() ) ;}

  return ParticleTRest;
}

ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > SpinPlots::ToTopPairRestFrame(mor::Particle * particle)
{
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > topPairCmf( Top_had->p4() + Top_lep->p4() );
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > tParticleCmf( ROOT::Math::VectorUtil::boost( particle->p4(), topPairCmf.BoostToCM() ) );

  return tParticleCmf;
}

/*
//void SpinPlots::plot_ZMFPhi()
//{
//  if(!ttbar || (decay != 1 && decay != 2)){return;}
// //  double Phi_l = 10.;
//  //double Phi_d = 10.;
  //double Phi_low = 10.;
  int id_low = 0;
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > topPairCmf( Top_had->p4() + Top_lep->p4() );

  std::cout<<"%%%%%%%%%%%%%%%%%Particle Properties%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
  std::cout<<"%%%Before boosting: Top_l pt= "<<Top_lep->p4().Pt()<<" Top_h pt= "<<Top_had->p4().Pt()<<std::endl;
  std::cout<<"%%%Before boosting: Top_l phi= "<<Top_lep->p4().Phi()<<" Top_h phi= "<<Top_had->p4().Phi()<<std::endl;
  std::cout<<"%%%Before boosting: Top_l eta= "<<Top_lep->p4().Eta()<<" Top_h eta= "<<Top_had->p4().Eta()<<std::endl;
  std::cout<<"%%%Before boosting: Top Pair Pt = "<<topPairCmf.Pt()<<std::endl;
  std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
  
  
  // boost particle 4-vectors to TT CMF                                                                                                                                       
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > tLeptonicCmf( ROOT::Math::VectorUtil::boost( Top_lep->p4(), topPairCmf.BoostToCM() ) );
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > tHadronicCmf( ROOT::Math::VectorUtil::boost( Top_had->p4(), topPairCmf.BoostToCM() ) );
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > lLeptonicCmf( ROOT::Math::VectorUtil::boost( lepton->p4(), topPairCmf.BoostToCM() ) );
  // ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > bHadronicCmf( ROOT::Math::VectorUtil::boost( gen_evt->hadronicDecayB()->p4(), topPairCmf.BoostToCM() ) );
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > qHadronicCmf( ROOT::Math::VectorUtil::boost( quark->p4(), topPairCmf.BoostToCM() ) );
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > q_bHadronicCmf( ROOT::Math::VectorUtil::boost( quark_bar->p4(), topPairCmf.BoostToCM() ) );
  //  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > beamCmf(ROOT::Math::VectorUtil::boost( ( gen_evt->initialPartons() )[0].p4(), topPairCmf.BoostToCM() ) );

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > tPairCmf( ROOT::Math::VectorUtil::boost( topPairCmf, topPairCmf.BoostToCM() ) );
  std::cout<<"%%%After boosting: Top_l pt= "<<tLeptonicCmf.Pt()<<" Top_h pt= "<<tHadronicCmf.Pt()<<std::endl;
  std::cout<<"%%%After boosting: Top_l phi= "<<tLeptonicCmf.Phi()<<" Top_h phi= "<<tHadronicCmf.Phi()<<std::endl;
  std::cout<<"%%%After boosting: Top_l eta= "<<tLeptonicCmf.Eta()<<" Top_h eta= "<<tHadronicCmf.Eta()<<std::endl;
  std::cout<<"%%%Before boosting: Top Pair Pt = "<<tPairCmf.Pt()<<std::endl;

  // build spin basis unit vectors          
  //reco::Particle::Vector beamBeamCmf( beamCmf.Vect().Unit() );

  // boost 4-vectors to T(BAR) rest frames is restframe of single top, not necessary                                                                                            
  //  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  lLeptonicTRest( ROOT::Math::VectorUtil::boost( lLeptonicCmf, tLeptonicCmf.BoostToCM() ) );
  //  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  bHadronicTRest( ROOT::Math::VectorUtil::boost( bHadronicCmf, tHadronicCmf.BoostToCM() ) );
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  qHadronicTRest( ROOT::Math::VectorUtil::boost( qHadronicCmf, tHadronicCmf.BoostToCM() ) );
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  q_bHadronicTRest( ROOT::Math::VectorUtil::boost( q_bHadronicCmf, tHadronicCmf.BoostToCM() ) );

  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > q;

    ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  qHadronicCMF2;
  if(qHadronicTRest.energy() < q_bHadronicTRest.energy()){
    qHadronicCMF2 = qHadronicCmf;
    id_low = quark->mc_match_id();
    q = quark->p4();
  }
  else{
    qHadronicCMF2 = q_bHadronicCmf;
    id_low = quark_bar->mc_match_id();
    q = quark_bar->p4();
  }
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%TEST%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > l(lepton->p4());

  
  //the lowest energy should be the down
  bool hasD = false;
  ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >  q_dHadronicTRest;
  if(quark->mc_match_id() == 1 || quark->mc_match_id() == 3){ q_dHadronicTRest = qHadronicCmf; hasD = true;}
  else if(quark_bar->mc_match_id() == -1 || quark->mc_match_id() == -3){q_dHadronicTRest = q_bHadronicCmf; hasD = true;}
  

  // extract particle directions in                                                                                                                        
  double cosPhiLQ(lLeptonicCmf.Vect().Unit().Dot(qHadronicCMF2.Vect().Unit()));
  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > LZMF(ToZeroMom(topPairCmf,l));
  ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > QZMF(ToZeroMom(topPairCmf,q));
  // std::cout<<"lLeptonicCmf= "<<lLeptonicCmf.E()<<" qHadronicCmf2= "<<qHadronicCMF2.E()<<std::endl;
  //std::cout<<"LZMF= "<<LZMF.Pt()<<" QZMF= "<<QZMF.Pt()<<std::endl;
  double cosPhiLQZMF(LZMF.Vect().Unit().Dot(QZMF.Vect().Unit()));
  
  //  std::cout<<"cosPhiLQ= "<<cosPhiLQ<<" cosPhiLQZMF= "<<cosPhiLQZMF<<std::endl;
  double cosPhiLD= 10.;
  if(hasD){cosPhiLD= (lLeptonicCmf.Vect().Unit().Dot(q_dHadronicTRest.Vect().Unit()));}
  double AngleTTbar(ROOT::Math::VectorUtil::Angle(tLeptonicCmf, tHadronicCmf));
  histos1d[("AngleTTbar"+id).c_str()]->Fill(AngleTTbar);

  histos1d[("cosPhiZMF_LE"+id).c_str()]->Fill(cosPhiLQ);
  histos1d[("MC_id_ZMF_LE"+id).c_str()]->Fill(fabs(id_low));
  if(hasD){  histos1d[("cosPhiZMF"+id).c_str()]->Fill(cosPhiLD);}
  if(topPairCmf.M() < 400){
    histos1d[("cosPhiZMFInvMass_LE"+id).c_str()]->Fill(cosPhiLQ);
    if(hasD){  histos1d[("cosPhiZMFInvMass"+id).c_str()]->Fill(cosPhiLD);}
  }
}

void SpinPlots::plot_deltaPhi()
{
  //  bool ttbar = gen_evt->is_ttbar();
  //char decay = gen_evt->decay_channel();
  //mor::Particle* lepton;
  //mor::Jet* quark;
  //mor::Jet* quark_bar;
  double Phi_q = 10.;
  double Phi_l = 10.;
  double Phi_qMax = 10.;
  double MaxEn = 0.;
  //select ttbar events, semi-leptonic
  if(ttbar && (decay == 1 || decay == 2)){
    //ask for the only lepton
    //    lepton = gen_evt->lepton();   
    //    Phi_l = lepton->phi();
    //if(Phi_l > TMath::Pi()){Phi_l = -(2*TMath::Pi() - Phi_l);}
*/    
    //ask for the quarks
    //quark = gen_evt->q_jet();
    //quark_bar = gen_evt->qbar_jet();
    
    //check the identity
    //if(quark->mc_match_id() == 2 || quark->mc_match_id() == 4){if(quark->pt()>MaxEn){Phi_qMax = quark->phi(); MaxEn = quark->pt(); if(Phi_qMax > TMath::Pi()){Phi_qMax = -(2*TMath::Pi() - Phi_qMax);}}}
    //these are the good down-types
    //if(quark->mc_match_id() == 1 || quark->mc_match_id() == 3){
    //Phi_q = quark->phi();
    //if(Phi_q > TMath::Pi()){Phi_q = -(2*TMath::Pi() - Phi_q);}
    //if(quark->pt()>MaxEn){Phi_qMax = Phi_q; MaxEn = quark->pt();}
    //    }
    //check the identity of anti-quark
    //if(quark_bar->mc_match_id() == -2 || quark_bar->mc_match_id() == -4){if(quark_bar->pt()>MaxEn){Phi_qMax = quark_bar->phi(); MaxEn = quark_bar->pt(); if(Phi_qMax > TMath::Pi()){Phi_qMax = -(2*TMath::Pi() - Phi_qMax);}}}
    //these are the good down-types
    //if(quark_bar->mc_match_id() == -1 || quark_bar->mc_match_id() == -3){
    //Phi_q = quark_bar->phi();
    //if(Phi_q > TMath::Pi()){Phi_q = -(2*TMath::Pi() - Phi_q);}
    //if(quark_bar->pt()>MaxEn){Phi_qMax = Phi_q; MaxEn = quark_bar->pt();}
    // }
    
    //take the differences
    //double delta = fabs(Phi_l - Phi_q);
    //double deltaMaxEn = fabs(Phi_l - Phi_qMax);
    //check if they are in correct range, else shift
    //if(delta >= TMath::Pi()){delta = 2*TMath::Pi()-delta;}
    //if(deltaMaxEn >= TMath::Pi()){deltaMaxEn = 2*TMath::Pi()-deltaMaxEn;}
    //if these values are filled (not necessarily so due to matching inefficiency)
    //if(Phi_l != 10 && Phi_q != 10){
    //histos1d[("deltaPhi"+id).c_str()]->Fill(delta);
      //  if(Phi_qMax != 10){
      //histos1d[("deltaPhiMaxEn"+id).c_str()]->Fill(deltaMaxEn);}
      //histos1d[("Phi_lepton"+id).c_str()]->Fill(Phi_l);
      //histos1d[("Phi_quark"+id).c_str()]->Fill(Phi_q);
      //    }
      //}
  
      //}



      //void SpinPlots::plot_deltaPhiInvMassConstraint()
      //{
  //  bool ttbar = gen_evt->is_ttbar();
  //char decay = gen_evt->decay_channel();
  //mor::Particle* lepton;
  //mor::Jet* quark;
  //mor::Jet* quark_bar;
  // double Phi_q = 10.;
  //  double Phi_l = 10.;
  //double Phi_qMax = 0.;
  //double MaxEn = 0.;
  //need the top-quarks
  //  mor::Particle* T;
  //mor::Particle* Tl;
  //ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > FourMomTTbar;
  //only select ttbar, semi-leptonic
  //if(ttbar && (decay == 1 || decay == 2)){
    //    Top_had = gen_evt->hadT();
    //Top_lep = gen_evt->lepT();
    //lepton =gen_evt->lepton();
    //Phi_l = lepton->phi();
    //if(Phi_l > TMath::Pi()){Phi_l = -(2*TMath::Pi() - Phi_l);}
    //quark = gen_evt->q_jet();
    //quark_bar = gen_evt->qbar_jet();
    
    //    if(quark->mc_match_id() == 2 || quark->mc_match_id() == 4){if(quark->pt()>MaxEn){Phi_qMax = quark->phi(); MaxEn = quark->pt(); if(Phi_qMax > TMath::Pi()){Phi_qMax = -(2*TMath::Pi() - Phi_qMax);}}}
    //these are the down-type
    //if(quark->mc_match_id() == 1 || quark->mc_match_id() == 3){
    //Phi_q = quark->phi(); 
    //if(Phi_q > TMath::Pi()){Phi_q = -(2*TMath::Pi() - Phi_q);}
    //if(quark->pt()>MaxEn){Phi_qMax = Phi_q; MaxEn = quark->pt();}
    //    }    
    //if(quark_bar->mc_match_id() == -2 || quark_bar->mc_match_id() == -4){if(quark_bar->pt()>MaxEn){Phi_qMax = quark_bar->phi(); MaxEn = quark_bar->pt(); if(Phi_qMax > TMath::Pi()){Phi_qMax = -(2*TMath::Pi() - Phi_qMax);}}}
    //these are the anti-down types
    //if(quark_bar->mc_match_id() == -1 || quark_bar->mc_match_id() == -3){
    //Phi_q = quark_bar->phi(); 
    //if(Phi_q > TMath::Pi()){Phi_q = -(2*TMath::Pi() - Phi_q);}
    //if(quark_bar->pt()>MaxEn){Phi_qMax = Phi_q; MaxEn = quark_bar->pt();}
    //    }    

    //    FourMomTTbar = Top_had->p4() + Top_lep->p4();
    //if(FourMomTTbar.M() < 400 && Phi_l != 10 && Phi_q != 10){
    //double delta = fabs(Phi_l - Phi_q);
    //double deltaMaxEn = fabs(Phi_l - Phi_qMax);
    //if(delta >= TMath::Pi()){delta = 2*TMath::Pi()-delta;}
    //if(deltaMaxEn >= TMath::Pi()){deltaMaxEn = 2*TMath::Pi()-deltaMaxEn;}
    //histos1d[("deltaPhiInvMassConstraint"+id).c_str()]->Fill(delta);
    //if(Phi_qMax != 10){
    //histos1d[("deltaPhiInvMassConstraintMaxEn"+id).c_str()]->Fill(deltaMaxEn);}
    //    }
    
    //  }
  
    //}
//  
