#include "../../interface/EventSelection/HtCalculator.h"

HtCalculator::HtCalculator(eire::HandleHolder *handle_holder)
{
	ht = -1;
	missing_ht = -1;

	this->jets = handle_holder->get_tight_jets();
	this->isolated_electrons = handle_holder->get_tight_electrons();
	this->isolated_muons = handle_holder->get_tight_muons();
	this->corrected_mets = handle_holder->get_selected_mets();
	this->handle_holder = handle_holder;
}

HtCalculator::~HtCalculator()
{
}

double HtCalculator::get_ht()
{
	if(ht != -1)
		return ht;
	else
		return calculate_ht();
}

double HtCalculator::get_missing_ht()
{
	if(missing_ht != -1)
		return missing_ht;
	else
		return calculate_missing_ht();
}

double HtCalculator::calculate_ht()
{
  	double pt_sum=0;

	for(std::vector<mor::Jet>::iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		pt_sum += jet_iter->Pt();
	}

	for(std::vector<mor::Muon>::iterator mu_iter = isolated_muons->begin();
		mu_iter != isolated_muons->end();
		++mu_iter)
		pt_sum += mu_iter->Pt();

	for(std::vector<mor::Electron>::iterator e_iter = isolated_electrons->begin();
		e_iter != isolated_electrons->end();
		++e_iter)
		pt_sum += e_iter->Pt();

	//pt_sum += corrected_mets->begin()->pt();
	ht = pt_sum;

	return ht;
}

double HtCalculator::calculate_missing_ht()
{
	ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > sum;

	for(std::vector<mor::Jet>::iterator jet_iter = handle_holder->get_jets()->begin(); jet_iter!=handle_holder->get_jets()->end(); ++jet_iter){
		sum += jet_iter->p4();
	}

	for(std::vector<mor::Muon>::iterator mu_iter = handle_holder->get_muons()->begin();
		mu_iter != handle_holder->get_muons()->end();
		++mu_iter)
		sum += mu_iter->p4();

	for(std::vector<mor::Electron>::iterator e_iter = handle_holder->get_electrons()->begin();
		e_iter != handle_holder->get_electrons()->end();
		++e_iter)
		sum += e_iter->p4();

	missing_ht = sum.pt();

	return missing_ht;
}
