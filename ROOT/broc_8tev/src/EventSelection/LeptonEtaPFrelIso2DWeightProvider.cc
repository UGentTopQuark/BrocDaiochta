#include "../../interface/EventSelection/LeptonEtaPFrelIso2DWeightProvider.h"

broc::LeptonEtaPFrelIso2DWeightProvider::LeptonEtaPFrelIso2DWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder): broc::LeptonEfficiencyWeightProvider(weight_file, handle_holder)
{
}

broc::LeptonEtaPFrelIso2DWeightProvider::~LeptonEtaPFrelIso2DWeightProvider()
{
}

double broc::LeptonEtaPFrelIso2DWeightProvider::get_value_for_event()
{
	double lepton_eta=-999;
	if(process_muons && muons->size() > 0){
		lepton_eta = muons->begin()->eta();
	}else if(process_electrons && electrons->size() > 0){
		lepton_eta = electrons->begin()->eta();
	}

	return lepton_eta;
}

double broc::LeptonEtaPFrelIso2DWeightProvider::get_2nd_value_for_event()
{
        std::string conesize = "0.3";
        double lepton_relIso=-999;
        if(process_muons && muons->size() > 0){
                lepton_relIso = (muons->begin()->chargedHadronIso(conesize) + muons->begin()->neutralHadro\
nIso(conesize) + muons->begin()->photonIso(conesize))/muons->begin()->Pt();
        }else if(process_electrons && electrons->size() > 0){
                lepton_relIso = (electrons->begin()->chargedHadronIso(conesize) + electrons->begin()->neut\
ralHadronIso(conesize) + electrons->begin()->photonIso(conesize))/electrons->begin()->Pt();
        }

        return lepton_relIso;

}
