#include "../../interface/EventSelection/ETriggerEffWeightProvider.h"

broc::ETriggerEffWeightProvider::ETriggerEffWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	electrons = handle_holder->get_tight_electrons();

	double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_e_trigger_unc");
	
	up = false;
	down = false;

	if(study_unc == 1){ down = true; }
	else if(study_unc == 2){ up = true; }
}

broc::ETriggerEffWeightProvider::~ETriggerEffWeightProvider()
{
}

double broc::ETriggerEffWeightProvider::get_weight()
{
	double weight = 1.;

	if(do_not_reweight)
		weight = 1.;
	else{
		if(electrons->size() >= 1){
			std::vector<mor::Electron>::iterator e = electrons->begin();
			double eta = e->supercluster_eta();
			double abseta = std::abs(eta);
			double pt = e->pt();
			double ud = 0.; // up-down switch for systematics
			if(up) ud = 1.;
			else if(down) ud = -1.;
			if(eta > 0){ 
				if(abseta < 1.0){
					if(pt > 45.){ weight = 0.953+ud*0.003; }
					else if(pt < 30.){ weight = 0.; }
					else if(pt < 35.){ weight = 0.85+ud*0.03; }
					else if(pt < 40.){ weight = 0.93+ud*0.02; }
					else { weight = 0.92+ud*0.02; }
				}else if(abseta < 1.5){
					if(pt > 45.){ weight = 0.92+ud*0.01; }
					else if(pt < 30.){ weight = 0.; }
					else if(pt < 35.){ weight = 0.93+ud*0.04; }
					else if(pt < 40.){ weight = 0.77+ud*0.06; }
					else { weight = 0.83+ud*0.03; }
				}else{
					if(pt > 45.){ weight = 0.90+ud*0.01; }
					else if(pt < 30.){ weight = 0.; }
					else if(pt < 35.){ weight = 0.68+ud*0.07; }
					else if(pt < 40.){ weight = 0.85+ud*0.05; }
					else { weight = 0.97+ud*0.03; }
				}
			}else{
				if(abseta < 1.0){
					if(pt > 45.){ weight = 0.952+ud*0.003; }
					else if(pt < 30.){ weight = 0.; }
					else if(pt < 35.){ weight = 0.93+ud*0.03; }
					else if(pt < 40.){ weight = 0.92+ud*0.03; }
					else { weight = 0.90+ud*0.02; }
				}else if(abseta < 1.5){
					if(pt > 45.){ weight = 0.92+ud*0.01; }
					else if(pt < 30.){ weight = 0.; }
					else if(pt < 35.){ weight = 0.84+ud*0.05; }
					else if(pt < 40.){ weight = 0.80+ud*0.06; }
					else { weight = 0.88+ud*0.03; }
				}else{
					if(pt > 45.){ weight = 0.88+ud*0.01; }
					else if(pt < 30.){ weight = 0.; }
					else if(pt < 35.){ weight = 0.67+ud*0.07; }
					else if(pt < 40.){ weight = 0.81+ud*0.07; }
					else { weight = 0.83+ud*0.05; }
				}
			}
		}
	}

	return weight;
}
