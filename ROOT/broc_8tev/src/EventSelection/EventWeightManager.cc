#include "../../interface/EventSelection/EventWeightManager.h"

broc::EventWeightManager::EventWeightManager(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;

	pdf_event_weight = NULL;
	pu_event_weight = NULL;
	lep_abseta_event_weight = NULL;
	lep_eta_event_weight = NULL;
	lep_pt_event_weight = NULL;
	lep_pfreliso_event_weight = NULL;
	lep_eta_pfreliso_2D_event_weight = NULL;
	jet_event_weight = NULL;
	e_trig_event_weight = NULL;

	eire::ConfigReader *config_reader = handle_holder->get_config_reader();
	if(config_reader->get_bool_var("pu_weights","event_weights",false)){
		//std::string weights_file = config_reader->get_var("pu_weights_file","event_weights",true);
		std::string weights_file = "";
		pu_event_weight = new broc::PileUpEventWeightProvider(weights_file, handle_holder);
	}
	pdf_event_weight = new broc::PDFEventWeight(cuts_set, handle_holder);
	if(config_reader->get_bool_var("lep_eff_weights","event_weights",false)){
		std::string abseta_weights_file = config_reader->get_var("abseta_weights_file","event_weights",false);
		std::string eta_weights_file = config_reader->get_var("eta_weights_file","event_weights",false);
		std::string pt_weights_file = config_reader->get_var("pt_weights_file","event_weights",false);
		std::string pfreliso_weights_file = config_reader->get_var("pfreliso_weights_file","event_weights",false);
		std::string eta_pfreliso_2D_weights_file = config_reader->get_var("eta_pfreliso_2D_weights_file","event_weights",false);
		if(abseta_weights_file == "" && eta_weights_file == "" && pfreliso_weights_file == "" && eta_pfreliso_2D_weights_file == ""){
			std::cerr << "ERROR:EventWeightManager: lep_eff_weights is true in config. Must provide a weights file.";
			std::cerr << " Define abseta_weights_file, eta_weights_file,eta_pfreliso_2D_weights_file or pfreliso_weights_file. " << std::endl;
			exit(1);
		}else if(abseta_weights_file != "" && eta_weights_file != ""){
			std::cerr << "ERROR:EventWeightManager: Cannot reweight by abseta and eta. Disable one weights file in config.";
			exit(1);
		}
		if(abseta_weights_file != ""){lep_abseta_event_weight = new broc::LeptonAbsEtaWeightProvider(abseta_weights_file,handle_holder);}
		if(eta_weights_file != ""){lep_eta_event_weight = new broc::LeptonEtaWeightProvider(eta_weights_file,handle_holder);}
		if(pt_weights_file != ""){lep_pt_event_weight = new broc::LeptonPtWeightProvider(pt_weights_file,handle_holder);}
		if(pfreliso_weights_file != ""){lep_pfreliso_event_weight = new broc::LeptonPFrelIsoWeightProvider(pfreliso_weights_file,handle_holder);}
		if(eta_pfreliso_2D_weights_file != ""){
lep_eta_pfreliso_2D_event_weight = new broc::LeptonEtaPFrelIso2DWeightProvider(eta_pfreliso_2D_weights_file,handle_holder);}
	}
	if(config_reader->get_bool_var("jet_eff_weights","event_weights",false)){
		jet_event_weight = new broc::JetEffWeightProvider(handle_holder);
	}
	if(config_reader->get_bool_var("e_trigger_eff_weights","event_weights",false)){
		e_trig_event_weight = new broc::ETriggerEffWeightProvider(handle_holder);
	}
}

broc::EventWeightManager::~EventWeightManager()
{
	if(pdf_event_weight){delete pdf_event_weight; pdf_event_weight=NULL;}
	if(pu_event_weight){delete pu_event_weight; pu_event_weight=NULL;}
	if(lep_abseta_event_weight){delete lep_abseta_event_weight; lep_abseta_event_weight=NULL;}
	if(lep_eta_event_weight){delete lep_eta_event_weight; lep_eta_event_weight=NULL;}
	if(lep_pt_event_weight){delete lep_pt_event_weight; lep_pt_event_weight=NULL;}
	if(lep_pfreliso_event_weight){delete lep_pfreliso_event_weight; lep_pfreliso_event_weight=NULL;}
	if(lep_eta_pfreliso_2D_event_weight){delete lep_eta_pfreliso_2D_event_weight; lep_eta_pfreliso_2D_event_weight=NULL;}
	if(jet_event_weight){ delete jet_event_weight; jet_event_weight = NULL; }
	if(e_trig_event_weight){ delete e_trig_event_weight; e_trig_event_weight = NULL; }
}

double broc::EventWeightManager::get_all_events_weight()
{
	double weight = 1.;
	// MC generator event weights
	weight *= abs(handle_holder->get_event_information()->event_weight())/handle_holder->get_event_information()->event_weight();
	// fix branching ratio to NLO in ttbar events
	weight *= get_W_BR_correction_weight();
	weight *= get_W_scale_correction_weight();

	weight *= pdf_event_weight->get_weight();

	if(pu_event_weight) weight *= pu_event_weight->get_weight();

	handle_holder->set_event_weight(weight);
	handle_holder->set_failing_event_weight(weight);

	return weight;
}

double broc::EventWeightManager::get_selected_events_weight()
{
	double weight = handle_holder->get_event_weight();
	if(lep_abseta_event_weight) weight *= lep_abseta_event_weight->get_weight();
	if(lep_eta_event_weight) weight *= lep_eta_event_weight->get_weight();
	if(lep_pt_event_weight) weight *= lep_pt_event_weight->get_weight();
	if(lep_pfreliso_event_weight) weight *= lep_pfreliso_event_weight->get_weight();
	if(lep_eta_pfreliso_2D_event_weight) weight *= lep_eta_pfreliso_2D_event_weight->get_weight();
	if(jet_event_weight) weight *= jet_event_weight->get_weight();
	if(e_trig_event_weight) weight *= e_trig_event_weight->get_weight();

	handle_holder->set_event_weight(weight);

	return weight;
}


double broc::EventWeightManager::get_W_scale_correction_weight()
{
	double decay_mode = handle_holder->get_event_information()->W_decay_mode();
	if(decay_mode == 0){
		return 1.;
	}
	else{
		// https://hypernews.cern.ch/HyperNews/CMS/get/top-crosssection/60/1/1/1/1/1/1/1/1.html
		if(decay_mode == 1){
			return 3;
		}else if(decay_mode == 2){
			return 0.75;
		}else if(decay_mode == 3){
			return 0.75;
		}else{
			return 1.;
		}
	}
}

// in the MC production of top events the wrong W branching ratio is assumed,
// cf.
// https://twiki.cern.ch/twiki/bin/viewauth/CMS/TopLeptonPlusJets2010Systematics
// "W Branching Fraction in some madgraph samples"
double broc::EventWeightManager::get_W_BR_correction_weight()
{
	if(handle_holder->get_ttbar_gen_evt() && handle_holder->get_ttbar_gen_evt()->decay_channel() != 0){
		double ttbar_decay = handle_holder->get_ttbar_gen_evt()->decay_channel();
		if(ttbar_decay == 1 ||	// semi lep e
		   ttbar_decay == 2 ||	// semi lep mu
		   ttbar_decay == 6){	// semi lep tau
			return (0.108*9)*(0.676*1.5);
		}else if(ttbar_decay == 4){
			return (0.108*9)*(0.108*9);
		}else if(ttbar_decay == 5){
			return (0.676*1.5)*(0.676*1.5);
		}else{
			std::cerr << "ERROR: broc::EventWeightManager::get_W_BR_correction_weight(): invalid decay mode: " << ttbar_decay << std::endl;
			std::cerr << "Exiting..." << std::endl;
			exit(1);
		}
	}

	return 1.;
}
