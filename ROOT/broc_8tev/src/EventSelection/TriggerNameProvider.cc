#include "../../interface/EventSelection/TriggerNameProvider.h"

mor::EventInformation *eire::TriggerNameProvider::event_information = NULL;

eire::TriggerNameProvider::TriggerNameProvider()
{
        hlt_names[80]="HLT_Mu9";
        hlt_names[86]="HLT_IsoMu9";
        hlt_names[90]="HLT_Mu15";
        hlt_names[91]="HLT_Mu20";
        hlt_names[92]="HLT_Mu24";
        hlt_names[93]="HLT_IsoMu15";
        hlt_names[94]="HLT_IsoMu17";
        hlt_names[95]="HLT_IsoMu24";
        hlt_names[96]="HLT_IsoMu17";
        hlt_names[97]="HLT_Mu21";
        hlt_names[98]="HLT_Mu30";
        hlt_names[50]="HLT_Ele32_WP70";
        hlt_names[49]="HLT_Photon15_L1R";
        hlt_names[51]= "HLT_Photon10_L1R";
        hlt_names[52]="HLT_Ele25_CaloIdL_CaloIsoVL_TrkIdVL_TrkIsoVL";
        hlt_names[53]="HLT_Ele32_CaloIdVL_CaloIsoVL_TrkIdVL_TrkIsoVL";
        hlt_names[54]="HLT_Ele32_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT";

	// hlt_names[100]="HLT_IsoMu17_v5";	// Summer 11 MC, 2011 Data
	// //hlt_names[100]="HLT_IsoMu17_v4"; //Spring 11 MC. 2011 Data. 
        // hlt_names[101]="HLT_Mu9";//For Top Fall10. Lowest threshold available.
        // hlt_names[102]="HLT_Mu15_v2"; //For Top (not Fall10) . Lowest threshold available.
	// hlt_names[103]="HLT_IsoMu9_v4";
	// hlt_names[104]="HLT_Mu15_v1";
	// hlt_names[105]="HLT_Mu30_v1"; //Summer11 MC. 2011 Data
	// hlt_names[106]="HLT_IsoMu15_v4";
	// hlt_names[107]="HLT_IsoMu17_v5"; //Summer11 MC. 2011 Data
	// hlt_names[108]="HLT_IsoMu24_v5"; //Not tested on MC. 2011 Data

	hlt_names[100]="HLT_IsoMu17";	// Summer 11 MC, 2011 Data
        hlt_names[101]="HLT_Mu9";//For Top Fall10. Lowest threshold available.
        hlt_names[102]="HLT_Mu15"; //For Top (not Fall10) . Lowest threshold available.
	hlt_names[103]="HLT_IsoMu9";
	hlt_names[104]="HLT_Mu15";
	hlt_names[105]="HLT_Mu30"; //Summer11 MC. 2011 Data
	hlt_names[106]="HLT_IsoMu15";
	hlt_names[107]="HLT_IsoMu17"; //Summer11 MC. 2011 Data
	hlt_names[108]="HLT_IsoMu24"; // 2011 Data
	hlt_names[109]="HLT_IsoMu30"; // 2011 Data, changes to _eta2p1 
	hlt_names[110]="HLT_IsoMu40"; // 2011 Data, changes to _eta2p1 
	hlt_names[200]="HLT_Mu17_TriCentralJet30"; // 2011 Data

	hlt_names[300]="HLT_Mu40"; //Summer11 MC. 2011 Data

	hlt_names[1] = "HLT_Jet15U";
	hlt_names[2] = "HLT_DiJetAve15U_8E29";
	hlt_names[3] = "HLT_FwdJet20U";
	hlt_names[4] = "HLT_Jet30U";
	hlt_names[5] = "HLT_Jet50U";
	hlt_names[6] = "HLT_DiJetAve30U_8E29";
	hlt_names[7] = "HLT_QuadJet15U";
	hlt_names[8] = "HLT_MET45";
	hlt_names[9] = "HLT_MET100";
	hlt_names[10] = "HLT_HT100U";
	hlt_names[11] = "HLT_SingleLooseIsoTau20";
	hlt_names[12] = "HLT_DoubleLooseIsoTau15";
	hlt_names[13] = "HLT_DoubleJet15U_ForwardBackward";
	hlt_names[14] = "HLT_BTagMu_Jet10U";
	hlt_names[15] = "HLT_BTagIP_Jet50U";
	hlt_names[16] = "HLT_StoppedHSCP_8E29";

	//2012 Triggers
	hlt_names[601] = "HLT_MET200_HBHENoiseCleaned";
	hlt_names[602] = "HLT_MET200";
	hlt_names[603] = "HLT_MET120_HBHENoiseCleaned";
	hlt_names[604] = "HLT_Ele27_WP80";
	//	hlt_names[605] = "HLT_Ele27_WP70";
	//	hlt_names[605] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT";
	hlt_names[606] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_CentralPFJet30";
	hlt_names[607] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_DiCentralPFJet30";
	hlt_names[608] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralPFJet30";
	hlt_names[609] = "HLT_Ele27_WP80_CentralPFJet30_CentralPFJet25_PFMHT20";
	hlt_names[610] = "HLT_Ele27_WP80_CentralPFJet30_CentralPFJet25";
	hlt_names[611] = "HLT_QuadPFJet75_55_35_20_BTagCSV_VBF";
	hlt_names[612] = "HLT_IsoMu20_eta2p1";
	hlt_names[613] = "HLT_IsoMu24_eta2p1";
	hlt_names[614] = "HLT_IsoMu20_eta2p1_CentralPFJet30";
	hlt_names[615] = "HLT_IsoMu20_eta2p1_DiCentralPFJet30";
	hlt_names[616] = "HLT_IsoMu20_eta2p1_TriCentralPFJet30";
	hlt_names[617] = "HLT_IsoMu20_eta2p1_CentralPFNoPUJet30";
	hlt_names[618] = "HLT_IsoMu20_eta2p1_DiCentralPFNoPUJet30";
	hlt_names[619] = "HLT_IsoMu20_eta2p1_TriCentralPFNoPUJet30";
	hlt_names[620] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_CentralPFNoPUJet30";
	hlt_names[621] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_DiCentralPFNoPUJet30";
	hlt_names[622] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralPFNoPUJet30";

	hlt_names[120] ="HLT_Mu13_Mu8";                                 //DiMuon Trigger
        hlt_names[150] ="HLT_Ele32_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT";	// Spring11
        hlt_names[151] ="HLT_Ele25_CaloIdVT_TrkIdT_CentralTriJet30";	// used in data and available in Fall10MC
        //hlt_names[152] ="HLT_Ele25_CaloIdVT_TrkIdT_CentralTriJet30";	// used in data and available in Fall10MC
        hlt_names[152] ="PASS";	// used in data and available in Fall10MC

        hlt_names[153] ="HLT_Ele25_CaloIdVT_TrkIdT_CentralJet30";	// used in data and available in Fall10MC
	//        hlt_names[154] ="HLT_Ele25_CaloIdVT_TrkIdT_CentralDiJet30";	// used in data and available in Fall10MC
	//hlt_names[154] ="HLT_Ele25_CaloIdVT_TrkIdT_CentralTriJet30";
//	hlt_names[154] ="HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralJet30"; // Fall11
	hlt_names[154] ="PASS"; // Fall11

        hlt_names[155] ="HLT_QuadJet40";
        hlt_names[156] ="HLT_QuadJet70";

	hlt_names[157] ="PASS"; // Fall11

        //hlt_names[50]="HLT_Ele15_LW_L1R";

        hlt_names[999]="PASS";

	// CROSS TRIGGER SECTION
        hlt_names[1001] ="HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_CentralJet30";	// used in data and available in Fall10MC
        hlt_names[1002] ="HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_DiCentralJet30";	// used in data and available in Fall10MC
        hlt_names[1003] ="HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralJet30";	// used in data and available in Fall10MC
        hlt_names[1004] ="HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_QuadCentralJet30";	// used in data and available in Fall10MC

	hlt_names[1021] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_CentralPFJet30";
	hlt_names[1022] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_DiCentralPFJet30";
	hlt_names[1013] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralJet30";
	hlt_names[1023] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralPFJet30";
	hlt_names[1014] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_QuadCentralJet30";
	hlt_names[1024] = "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_QuadCentralPFJet30";

	hlt_names[2001] ="HLT_IsoMu17_CentralJet30";	// used in data and available in Fall10MC
        hlt_names[2002] ="HLT_IsoMu17_CentralDiJet30";	// used in data and available in Fall10MC
        hlt_names[2003] ="HLT_IsoMu17_CentralTriJet30";	// used in data and available in Fall10MC
        hlt_names[2004] ="HLT_IsoMu17_CentralQuadJet30";// used in data and available in Fall10MC

	hlt_names[2011] ="HLT_IsoMu17_CentralJet30";	// used in data and available in Fall10MC
        hlt_names[2012] ="HLT_IsoMu17_DiCentralJet30";	// used in data and available in Fall10MC
        hlt_names[2013] ="HLT_IsoMu17_TriCentralJet30";	// used in data and available in Fall10MC
        hlt_names[2014] ="HLT_IsoMu17_QuadCentralJet30";// used in data and available in Fall10MC

	hlt_names[2021] ="HLT_IsoMu17_CentralPFJet30";	// used in data and available in Fall10MC
        hlt_names[2022] ="HLT_IsoMu17_DiCentralPFJet30";// used in data and available in Fall10MC
        hlt_names[2023] ="HLT_IsoMu17_TriCentralPFJet30";// used in data and available in Fall10MC
        hlt_names[2024] ="HLT_IsoMu17_QuadCentralPFJet30";// used in data and available in Fall10MC
}

eire::TriggerNameProvider::~TriggerNameProvider()
{
}

std::string eire::TriggerNameProvider::hlt_name(int id)
{
	if(event_information == NULL){
		std::cerr << "ERROR: No event information in TrigNameMap. " << std::endl;
		exit(1);
		return "";
	}
	if(event_information->is_real_data()){
		int run = (int) event_information->run();
		if(id == 101 || id == 102 || id == 100){
			if(run < 147196)
				return "HLT_Mu9";
			else if(run < 150000) 
				return "HLT_Mu15_v1";
			else 
				return "HLT_IsoMu17";
		}else if(id == 150){ // HLT Electron triggers
			if(run < 140041)
				return "HLT_Ele10_LW_L1R";
			else if(run >= 140041 && run <= 143962)
				return "HLT_Ele15_SW_L1R";
			else if(run >= 143963 && run <= 146427)
				return "HLT_Ele15_SW_CaloEleId_L1R";
			else if(run >= 146428 && run <= 147116)
				return "HLT_Ele17_SW_CaloEleId_L1R";
			else if(run >= 147117 && run <= 148818)
				return "HLT_Ele17_SW_TightEleId_L1R";
			else if((run >= 148819) && (run < 157945))
				return "HLT_Ele22_SW_TighterEleId_L1R";
			else if((run >= 157945) && (run < 164000))
				return "HLT_Ele27_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT";
		}
		else if(id == 103){//Iso Mu 9
			if(run >= 146315 && run < 165071)
				return "HLT_IsoMu9";
			else
				return "HLT_IsoMu17";
		}
		else if (id == 90 || id == 104 || id == 105){
			if(run < 147196)
				return "HLT_Mu9";
			else if(run < 150000) 
				return "HLT_Mu15";
			else
				return "HLT_Mu30";
		}
		else if (id == 108){//Mu trigger for 2011 AN
			if(run >= 173236){
				return "HLT_IsoMu24_eta2p1";}
			//else use default HLT_IsoMu30
		}
		else if (id == 109){//Mu trigger for 2011 AN
			if(run >= 173236){
				return "HLT_IsoMu30_eta2p1";}
			//else use default HLT_IsoMu30
		}
		else if (id == 300){
			if(run >= 173236){
				return "HLT_Mu40_eta2p1";}
			//else use default HLT_IsoMu30
		}
		else if (id == 110){
			if(run >= 173236){
				return "HLT_IsoMu40_eta2p1";}
			//else use default HLT_IsoMu40
		}
		else if (id == 200){
			return "HLT_IsoMu17_TriCentralJet30";
		}
		else if(id == 151){	// ElectronHad triggers
			return "HLT_Ele25_CaloIdVT_TrkIdT_TriCentralJet30";
		}else if(id == 152){	// ElectronHad triggers
			if(run <= 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralJet30";
			else if(run > 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralPFJet30";
		}else if(id == 153){	// ElectronHad triggers
			return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_CentralJet30";
		}else if(id == 154){    // ElectronHad triggers                                                                                                                 
		  if(run <= 178420)
		    return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralJet30";
		  else if(run > 178420)
		    return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralPFJet30";
		}else if(id == 157){
			if(run <= 178420)
				return "HLT_Ele25_CaloIdVT_TrkIdT_TriCentralJet30";
			else if(run > 178420)
				return "HLT_Ele25_CaloIdVT_TrkIdT_TriCentralPFJet30";
		}else if(id == 1001){	// ElectronHad triggers
			if(run < 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_CentralJet30";
			else if(run >= 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_CentralPFJet30";
		}else if(id == 1002){	// ElectronHad triggers
			if(run < 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_DiCentralJet30";
			else if(run >= 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_DiCentralPFJet30";
		}else if(id == 1003){	// ElectronHad triggers
			if(run < 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralJet30";
			else if(run >= 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralPFJet30";
		}else if(id == 1004){	// ElectronHad triggers
			if(run < 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_QuadCentralJet30";
			else if(run >= 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_QuadCentralPFJet30";
		}else if(id == 1013){	// ElectronHad triggers
			if(run < 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralJet30";
        	}else if(id == 1023){
	        	if(run >= 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_TriCentralPFJet30";
		}else if(id == 1014){	// ElectronHad triggers
			if(run < 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_QuadCentralJet30";
        	}else if(id == 1024){
	        	if(run >= 178420)
				return "HLT_Ele25_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_QuadCentralPFJet30";
		}else if(id == 2003){	// MuonHad triggers
			if(run < 178420)
				return "HLT_IsoMu17_TriCentralJet30";
			else if(run >= 178420)
				return "HLT_IsoMu17_TriCentralPFJet30";
		}else if(id == 2013){	// MuonHad triggers
			if(run < 178420)
				return "HLT_IsoMu17_TriCentralJet30";
        	}else if(id == 2023){
	        	if(run >= 178420)
				return "HLT_IsoMu17_TriCentralPFJet30";

		}
	}
	// if this is not the case, continue with normal trigger
	// lookup...
	if(hlt_names.find(id) != hlt_names.end()){
		return hlt_names[id];
	}
	else{
		std::cout << "WARNING: eire::TriggerNameProvider::get_hlt_name(): invalid trigger id: " << id << std::endl;
		exit(1);
		return "";
	}
}

void eire::TriggerNameProvider::set_event_information(mor::EventInformation *event_info)
{
	event_information = event_info;
}
