#include "../../interface/EventSelection/PileUpEventWeightProvider.h"

broc::PileUpEventWeightProvider::PileUpEventWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

//	std::string data_pu_file = handle_holder->get_config_reader()->get_var("data_pu_file", "event_weights", true);
//	std::string mc_pu_file = handle_holder->get_config_reader()->get_var("mc_pu_file", "event_weights", true);

	std::string weights_file_3d = handle_holder->get_config_reader()->get_var("3d_pu_weights_file", "event_weights", true);

	systematic_shift = handle_holder->get_cuts_set()->get_cut_value("pu_systematic_shift");
	if(systematic_shift == 1){
		weights_file_3d = handle_holder->get_config_reader()->get_var("3d_pu_weights_file_sys_down", "event_weights", true);

		if(weights_file_3d == ""){
			std::cerr << "ERROR: 3d_pu_weights_file_sys_down not defined and systematics study enabled, exiting..." << std::endl;
			exit(1);
		}
	}else if(systematic_shift == 2){
		weights_file_3d = handle_holder->get_config_reader()->get_var("3d_pu_weights_file_sys_up", "event_weights", true);

		if(weights_file_3d == ""){
			std::cerr << "ERROR: 3d_pu_weights_file_sys_up not defined and systematics study enabled, exiting..." << std::endl;
			exit(1);
		}
	}



	pu_info = handle_holder->get_event_information()->pu_info();
//	reweighter = new reweight::LumiReWeighting(mc_pu_file, data_pu_file, "pileup", "pileup");
	reweighter = new edm::Lumi3DReWeighting();
	reweighter->weight3D_init(weights_file_3d);

//	poisson_mean_shifter = NULL;
//	if(systematic_shift != -1 && systematic_shift != 0){
//		poisson_mean_shifter = new reweight::PoissonMeanShifter(systematic_shift);
//	}
}

broc::PileUpEventWeightProvider::~PileUpEventWeightProvider()
{
	if(reweighter){ delete reweighter; reweighter = NULL; }
}

double broc::PileUpEventWeightProvider::get_weight()
{
	if(do_not_reweight) return 1.;

	int nm1 = -1; int n0 = -1; int np1 = -1;
	for(std::vector<std::pair<int, double> >::iterator PVI = pu_info->begin(); PVI != pu_info->end(); ++PVI) {
	
	   int BX = PVI->first;
	
	   if(BX == -1) { 
	     nm1 = (int) PVI->second;
	   }
	   if(BX == 0) { 
	     n0 = (int) PVI->second;
	   }
	   if(BX == 1) { 
	     np1 = (int) PVI->second;
	   }
	
	}
	
	double MyWeight3D = reweighter->weight3D( nm1,n0,np1);

//	if(systematic_shift != -1 && systematic_shift != 0){
//		MyWeight3D *= poisson_mean_shifter->ShiftWeight(ave_nvtx);
//	}

	return MyWeight3D;

/*
	int npv = 0;
	float sum_nvtx = 0;

	for(std::vector<std::pair<int, double> >::iterator pu = pu_info->begin();
		pu != pu_info->end();
		++pu){
		npv = (int) pu->second;

		sum_nvtx += float(npv);
	}

	float ave_nvtx = sum_nvtx/3.;

	double weight = reweighter->ITweight3BX(ave_nvtx);

	if(systematic_shift != -1 && systematic_shift != 0){
		weight *= poisson_mean_shifter->ShiftWeight(ave_nvtx);
	}

	return weight;
*/
}
