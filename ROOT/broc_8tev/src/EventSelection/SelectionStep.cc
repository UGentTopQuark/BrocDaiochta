#include "../../interface/EventSelection/SelectionStep.h"

broc::SelectionStep::SelectionStep(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	this->cuts_set = cuts_set;
	this->handle_holder = handle_holder;

	plot_generator = NULL;
	event_classifier = NULL;
	collection_skimmer = NULL;
	central_services = NULL;
	tag_and_probe = NULL;
	btag_system8 = NULL;
	cross_trigger_study = NULL;
	hitfit_study = NULL;
	cuts = NULL;

	std::string id = "_"+handle_holder->get_ident();
	event_counter_histo = handle_holder->get_histo_writer()->create_1d(("event_counter"+id).c_str(),"processed events",5,-0.5,4.5);

	read_config();
	book_modules();
}

broc::SelectionStep::~SelectionStep()
{
	if(weight_manager){delete weight_manager; weight_manager = NULL;}
	if(central_services){delete central_services; central_services = NULL;}
	if(plot_generator){delete plot_generator; plot_generator = NULL;}
	if(collection_skimmer){delete collection_skimmer; collection_skimmer = NULL;}
	if(event_classifier){delete event_classifier; event_classifier = NULL;}
	if(cuts){delete cuts; cuts = NULL;}
	if(tag_and_probe){delete tag_and_probe; tag_and_probe = NULL;}
	if(btag_system8){delete btag_system8; btag_system8 = NULL;}
	if(cross_trigger_study){delete cross_trigger_study; cross_trigger_study = NULL;}
	if(hitfit_study){delete hitfit_study; hitfit_study = NULL;}
}

void broc::SelectionStep::next_event()
{
	eire::TriggerNameProvider::set_event_information(handle_holder->get_event_information());
	// prepare object collections according to cuts
	// they are automatically updated in handle_holder
	collection_skimmer->skim();
	central_services->next_event();

	// calculate global event weight for current event as product of single sub-weights
	double event_weight = weight_manager->get_all_events_weight();
	event_counter_histo->Fill(0.,event_weight); // fill 0 always
	event_weight = weight_manager->get_selected_events_weight();//Includes weights applied to unselected
	if(cuts->cut()) return; // stop here if event does not pass cuts
	event_counter_histo->Fill(1.,event_weight); // fill 1 only if event passed cuts

	// set weights for plots
	eire::TH1F::set_global_weight(event_weight);
	eire::TH2F::set_global_weight(event_weight);

	/*
	 * if modules enabled, execute them
	 */
	if(enable_plot_module){ plot_generator->plot(); }
	if(enable_tag_and_probe_module){ tag_and_probe->fill_trees(); }
	if(enable_btag_system8_module){ btag_system8->fill_trees(); }
	if(enable_mva_module){ event_classifier->run(); }

	if(enable_cross_trigger_module){ cross_trigger_study->fill_trees(); }
	if(enable_kinematic_fit_module){ hitfit_study->fill_trees(); }
}

void broc::SelectionStep::book_modules()
{
	collection_skimmer = new broc::CollectionSkimmer(cuts_set, handle_holder);
	collection_skimmer->skim();	// skim once to initialise collections

	/*
	 * book modules
	 */
	central_services = new broc::CentralServices(cuts_set, handle_holder);

	cuts = new broc::Cuts(cuts_set, handle_holder);
	weight_manager = new broc::EventWeightManager(cuts_set, handle_holder);
	if(enable_mva_module){
		event_classifier = new tionscadaldorcha::ClassificationManager(handle_holder->get_config_reader(), handle_holder);
		event_classifier->initialise();
	}
	if(enable_plot_module) plot_generator = new broc::PlotGenerator(handle_holder);
	if(enable_tag_and_probe_module) tag_and_probe = new clibisfiosraigh::TagAndProbe(cuts_set, handle_holder);
	if(enable_btag_system8_module) btag_system8 = new bclib::BTagStudies(cuts_set, handle_holder);
	if(enable_cross_trigger_module) cross_trigger_study = new broc::CrossTriggerEfficiencyManager(cuts_set, handle_holder);
	if(enable_kinematic_fit_module) hitfit_study = new hitfit::HitFitManager(cuts_set, handle_holder);
}

void broc::SelectionStep::read_config()
{
	eire::ConfigReader *config_reader = handle_holder->get_config_reader();
	enable_mva_module = config_reader->get_bool_var("enable_mva_module","global",false);
	enable_plot_module = config_reader->get_bool_var("enable_plots_module","global",false);
	enable_tag_and_probe_module = config_reader->get_bool_var("enable_tag_and_probe_module","global",false);
	enable_btag_system8_module = config_reader->get_bool_var("enable_btag_system8_module","global",false);
	enable_cross_trigger_module = config_reader->get_bool_var("enable_cross_trigger_module","global",false);
	enable_kinematic_fit_module = config_reader->get_bool_var("enable_kinematic_fit_module", "global", false);
}
