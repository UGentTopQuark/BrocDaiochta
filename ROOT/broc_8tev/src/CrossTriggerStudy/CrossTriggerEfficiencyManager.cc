#include "../../interface/CrossTriggerStudy/CrossTriggerEfficiencyManager.h"


broc::CrossTriggerEfficiencyManager::CrossTriggerEfficiencyManager(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
  eire::ConfigReader *config_reader = handle_holder->get_config_reader();
  std::string output_dir = config_reader->get_var("output_directory");
  std::string suffix = config_reader->get_var("outfile_suffix");
  jet_eff_est = NULL;
  ele_eff_est = NULL;
  mu_eff_est = NULL;
  tot_eff_mu_est = NULL;
  tot_eff_el_est = NULL;
  ntuple_file = NULL;
  enable_electron = config_reader->get_bool_var("enable_electron","cross_trigger",false);
  enable_jets = config_reader->get_bool_var("enable_jets","cross_trigger",false);
  enable_muon = config_reader->get_bool_var("enable_muon","cross_trigger",false);
  enable_total = config_reader->get_bool_var("enable_total","cross_trigger",false);
  enable_el_leg = config_reader->get_bool_var("enable_el_leg","cross_trigger",false);
  enable_mu_leg = config_reader->get_bool_var("enable_mu_leg","cross_trigger",false);
  enable_jet_leg = config_reader->get_bool_var("enable_jet_leg","cross_trigger",false);
  enable_total_leg = config_reader->get_bool_var("enable_total_leg","cross_trigger",false);
  jet_one = config_reader->get_bool_var("jet_one","cross_trigger",false);
  jet_two = config_reader->get_bool_var("jet_two","cross_trigger",false);
  jet_three = config_reader->get_bool_var("jet_three","cross_trigger",false);

  if (enable_jets){
	  std::string filename = output_dir+"/"+"JetCrossTrigger_"+handle_holder->get_ident()+suffix+".root";
	  while(filename.find("|") != std::string::npos)
		  filename.replace(filename.find("|"),1,"_");
	  
	  std::string directory_name = "JetEffs";

	  jet_eff_est = new broc::JetEfficiencyEstimator(handle_holder);
	  ntuple_file = new TFile(filename.c_str(), "RECREATE");
	  if(directory_name != ""){
		  ntuple_file->mkdir(directory_name.c_str());
		  ntuple_file->Cd(directory_name.c_str());
	  }
 
	  jet_eff_est->set_outfile(ntuple_file, directory_name);
	  jet_eff_est->create_tree("cross_trig");
	  jet_eff_est->book_branches();
	  
  }
  if (enable_electron){
	  std::string filename = output_dir+"/"+"EleCrossTrigger_"+handle_holder->get_ident()+suffix+".root";
	  while(filename.find("|") != std::string::npos)
		  filename.replace(filename.find("|"),1,"_");
	  
	  std::string directory_name = "EleEffs";
	  
	  ntuple_file = new TFile(filename.c_str(), "RECREATE");
	  if(directory_name != ""){
		  ntuple_file->mkdir(directory_name.c_str());
		  ntuple_file->Cd(directory_name.c_str());
	  }
 
	  ele_eff_est = new broc::EleEfficiencyEstimator(handle_holder);
	  ele_eff_est->set_outfile(ntuple_file, directory_name);
	  ele_eff_est->create_tree("cross_trig");
	  ele_eff_est->set_handles(handle_holder); 
	  ele_eff_est->book_branches();
  }
  if (enable_muon){
	  std::string filename = output_dir+"/"+"MuCrossTrigger_"+handle_holder->get_ident()+suffix+".root";
	  while(filename.find("|") != std::string::npos)
		  filename.replace(filename.find("|"),1,"_");
	  
	  std::string directory_name = "MuEffs";
	  
	  ntuple_file = new TFile(filename.c_str(), "RECREATE");
	  if(directory_name != ""){
		  ntuple_file->mkdir(directory_name.c_str());
		  ntuple_file->Cd(directory_name.c_str());
	  }
 
	  mu_eff_est = new broc::MuEfficiencyEstimator(handle_holder);
	  mu_eff_est->set_outfile(ntuple_file, directory_name);
	  mu_eff_est->create_tree("cross_trig");
	  mu_eff_est->set_handles(handle_holder); 
	  mu_eff_est->book_branches();
  }
  if (enable_total){
    std::string filename = output_dir+"/"+"TotalCrossTrigger_"+handle_holder->get_ident()+suffix+".root";
    while(filename.find("|") != std::string::npos)
      filename.replace(filename.find("|"),1,"_");

    std::string directory_name = "TotEffs";

    ntuple_file = new TFile(filename.c_str(), "RECREATE");
    if(directory_name != ""){
      ntuple_file->mkdir(directory_name.c_str());
      ntuple_file->Cd(directory_name.c_str());
    }

    if(enable_mu_leg){
    tot_eff_mu_est = new broc::TotalCrossTrigEfficiencyEstimator<mor::Muon>(handle_holder, enable_el_leg, enable_mu_leg, enable_jet_leg, enable_total_leg, jet_one, jet_two, jet_three);
    tot_eff_mu_est->set_outfile(ntuple_file, directory_name);
    tot_eff_mu_est->create_tree("cross_trig");
    tot_eff_mu_est->set_handles(handle_holder);
    tot_eff_mu_est->book_branches();
    }
    if(enable_el_leg || enable_jet_leg){
      std::cout<<"in electron!!! crosstriggermanager"<<std::endl;
      tot_eff_el_est = new broc::TotalCrossTrigEfficiencyEstimator<mor::Electron>(handle_holder, enable_el_leg, enable_mu_leg, enable_jet_leg, enable_total_leg, jet_one, jet_two, jet_three);
      tot_eff_el_est->set_outfile(ntuple_file, directory_name);
      tot_eff_el_est->create_tree("cross_trig");
      tot_eff_el_est->set_handles(handle_holder);
      tot_eff_el_est->book_branches();
    }

  }

}

broc::CrossTriggerEfficiencyManager::~CrossTriggerEfficiencyManager()
{
	if(jet_eff_est){ delete jet_eff_est; jet_eff_est = NULL; }
	if(ele_eff_est){ delete ele_eff_est; ele_eff_est = NULL; }
	if(mu_eff_est){delete mu_eff_est; mu_eff_est = NULL;}
	if(tot_eff_mu_est){delete tot_eff_mu_est; tot_eff_mu_est = NULL;}
	if(tot_eff_el_est){delete tot_eff_el_est; tot_eff_el_est = NULL;}
	if(ntuple_file){
	  ntuple_file->Write();
	  ntuple_file->Close();
	  delete ntuple_file;
	  ntuple_file = NULL;
        }
}

void broc::CrossTriggerEfficiencyManager::fill_trees()
{
  if (enable_electron){
	  ele_eff_est->fill_branches();  
  }
  if(enable_muon){
	  mu_eff_est->fill_branches();
  }
  if (enable_jets){
	  //jet_eff_est->fill_plots(); // Add back in when supported in config
	  jet_eff_est->fill_branches();
  }
  if(enable_total){
    if(enable_mu_leg){
      tot_eff_mu_est->fill_branches();
    }
    if(enable_el_leg || enable_jet_leg || enable_total_leg){
      tot_eff_el_est->fill_branches();
    }
  }
}
