#include "../../interface/CrossTriggerStudy/EleEfficiencyEstimator.h"

broc::EleEfficiencyEstimator::EleEfficiencyEstimator(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	this->primary_vertices = handle_holder->get_tight_primary_vertices();
	this->event_information = handle_holder->get_event_information();
	this->histo_writer = handle_holder->get_histo_writer();

	jet_trigger_matcher = new truicear::TriggerMatcher<mor::Jet, mor::TriggerObject>();
	electron_trigger_matcher = new truicear::TriggerMatcher<mor::Electron, mor::TriggerObject>();

	trig_name_prov = new eire::TriggerNameProvider();
	trigger_id = (int) handle_holder->get_cuts_set()->get_cut_value("hlt_cross_trigger");                                                                              
	ref_trigger = (int) handle_holder->get_cuts_set()->get_cut_value("hlt_reference_trigger");    
	this->lepton = handle_holder->get_tight_electrons();

    	obj_selector = new truicear::TriggerObjectSelector(handle_holder);
	
	pt=0;
	eta=0;
	phi=0;
	abseta=0;
	sc_eta=0;	// eta super cluster (electrons only)
	sc_abseta=0;	// eta super cluster (electrons only)
	sc_phi=0;
	mindR = 0;
	njets=0;
	reliso=0;
	pfreliso=0;
	ecaliso=0;
	hcaliso=0;
	trackiso=0;
	combcaloiso=0;
	npvertices=0;
	run=0;
	lumi_section=0;
	passing_ref = -1;
	passing_cross_trigger = -1;
	event_weight = 1;

	jets = NULL;

	set_handles(handle_holder);
}

broc::EleEfficiencyEstimator::~EleEfficiencyEstimator()
{
	if(trig_name_prov){ delete trig_name_prov; trig_name_prov = NULL; }
    	if(obj_selector){ delete obj_selector; obj_selector = NULL; }
	if(jet_trigger_matcher){
	  delete jet_trigger_matcher;
	  jet_trigger_matcher = NULL;
        }
	if(electron_trigger_matcher){
	  delete electron_trigger_matcher;
	  electron_trigger_matcher = NULL;
        }

}

void broc::EleEfficiencyEstimator::book_branches()
{
	tree->Branch("pt",&pt, "pt/F");
	tree->Branch("eta",&eta, "eta/F");
	tree->Branch("sc_eta",&sc_eta, "sc_eta/F");
	tree->Branch("sc_abseta",&sc_abseta, "sc_abseta/F");
	tree->Branch("phi",&phi, "phi/F");
	tree->Branch("njets",&njets, "njets/F");
	tree->Branch("abseta",&abseta, "abseta/F");
	tree->Branch("reliso",&reliso, "reliso/F");
	tree->Branch("pfreliso",&pfreliso, "pfreliso/F");
	tree->Branch("ecaliso",&ecaliso, "ecaliso/F");
	tree->Branch("hcaliso",&hcaliso, "hcaliso/F");
	tree->Branch("trackiso",&trackiso, "trackiso/F");
	tree->Branch("combcaloiso",&combcaloiso, "combcaloiso/F");
	tree->Branch("run",&run, "run/F");
	tree->Branch("lumi_section",&lumi_section, "lumi_section/F");
	tree->Branch("mindR",&mindR, "mindR/F");
	tree->Branch("npvertices",&npvertices, "npvertices/F");
	tree->Branch("event_weight",&event_weight, "event_weight/F");
	tree->Branch("passing_ref",&passing_ref, "passing_ref/I");
	tree->Branch("passing_cross_trigger",&passing_cross_trigger, "passing_cross_trigger/I");
}

void broc::EleEfficiencyEstimator::fill_branches()
{
  //set the method used for the trigger matching (matching in dR) and set the value of the dR cut
  double max_trig_dR = 0.1;
  jet_trigger_matcher->set_max_dR(max_trig_dR);
  electron_trigger_matcher->set_max_dR(max_trig_dR);
  jet_trigger_matcher->set_matching_method(0);
  electron_trigger_matcher->set_matching_method(0);

  //get the only electron in the event
  lepton->clear();
  lepton->push_back((*electrons)[0]);
  
  mor::Electron probe_lepton = (*lepton)[0];

  //get the names of the triggers: trigger_id is the cross-trigger and ref_trigger is the reference trigger
  std::string trigger_name = trig_name_prov->hlt_name(trigger_id);                                                                                                        
  std::string ref_trigger_name = trig_name_prov->hlt_name(ref_trigger);                                                                                                   
  // check if event passed reference trigger and cross trigger                                                                                                                   
  bool passed_ref = handle_holder->get_trigger()->triggered(ref_trigger_name);
  bool passed_cross = handle_holder->get_trigger()->triggered(trigger_name); 

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%REFERENCE TRIGGER%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if(!passed_ref){return;} //if the event doesn't pass the reference trigger, exit the method 
    
  //get the trigger object-selector for the reference trigger objects
  obj_selector = new truicear::TriggerObjectSelector(handle_holder);
  obj_selector->set_trigger_name(ref_trigger_name);

  //get the reference trigger objects and check if there are at least three central jets
  std::vector<mor::TriggerObject> *sel_objects = obj_selector->get_selected_objects(handle_holder->get_trigger_objects());
  int n_centraljets = 0;
  for(unsigned int i= 0; i < sel_objects->size(); i++){
    if(std::fabs((*sel_objects)[i].eta()) < 2.6){n_centraljets++;}
  }
  
  if(n_centraljets < 3){return;}

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%CROSS-TRIGGER%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  //get the trigger object-selector for the cross-trigger objects: 2 for the jets and 1 for the electrons
  truicear::TriggerObjectSelector *trigObj_crosstrig = new truicear::TriggerObjectSelector(handle_holder);
  trigObj_crosstrig->set_trigger_name(trigger_name,2);
  truicear::TriggerObjectSelector *trigObj_crosstrig_el = new truicear::TriggerObjectSelector(handle_holder);
  trigObj_crosstrig_el->set_trigger_name(trigger_name,1);

  //select the jet and electron trigger objects of the cross-trigger
  std::vector<mor::TriggerObject> *trig_objects = trigObj_crosstrig->get_selected_objects(handle_holder->get_trigger_objects());
  std::vector<mor::TriggerObject> *trig_objects_el = trigObj_crosstrig_el->get_selected_objects(handle_holder->get_trigger_objects());

  //get the Electron that is matched to a trigger object and get the trigger objects that are matched to that electron
  std::vector<mor::Electron> *matched_electrons = electron_trigger_matcher->match(lepton, trig_objects_el);
  std::vector<mor::TriggerObject> *matched_trigger_objects = electron_trigger_matcher->get_type2_matched();

  //check the separation between the jet-trigger objects and the electron-matched trigger objects
  //we do not want one of the jets to fake an electron
  double mindR = 20;
  for(unsigned int i = 0; i < trig_objects->size(); i++){
    double dR = ROOT::Math::VectorUtil::DeltaR((*trig_objects)[i], (*matched_trigger_objects)[0]);
    if(dR <= mindR){mindR = dR;}
  }
  if(mindR < 0.1){return;}//if the jet is too close to an electron, throw away the event

  //delete all the trigger object selectors per event
  delete trigObj_crosstrig;
  trigObj_crosstrig = NULL;
  delete trigObj_crosstrig_el;
  trigObj_crosstrig_el = NULL;
  
  //at this point the tree can be filled, we know now that the hadronic part of the cross-trigger is satisfied, but if the event passes the cross-trigger, but cannot match an electron to the trigger object, we artificially set the event as failing the cross trigger
  if(matched_electrons->size() < 1){passed_cross = false;}
  fill_probe_branch(probe_lepton,passed_ref, passed_cross);

}

void broc::EleEfficiencyEstimator::fill_probe_branch(mor::Electron &probe_lepton,bool ref, bool cross)
{
	pt = probe_lepton.pt();
	eta = probe_lepton.eta();
	sc_eta = probe_lepton.supercluster_eta();
	sc_abseta = std::abs(probe_lepton.supercluster_eta());
	phi = probe_lepton.phi();
	abseta = std::abs(probe_lepton.eta());
	reliso = probe_lepton.relIso();
	pfreliso = probe_lepton.PFrelIso();
	ecaliso=probe_lepton.ecalIso();
	hcaliso=probe_lepton.hcalIso();
	trackiso=probe_lepton.trackIso();
	combcaloiso=probe_lepton.caloIso();
	run = event_information->run();
	lumi_section = event_information->lumi_block();
	njets = jets->size();
	npvertices   = primary_vertices->size();
	mindR = min_lep_jet_dR(&probe_lepton);
	event_weight = handle_holder->get_event_weight();
	if(ref){
		passing_ref = 1;
		if(cross){
			passing_cross_trigger = 1;
		}
		else{
			passing_cross_trigger = 0;
		}
	}
	else{passing_ref = 0;
	  passing_cross_trigger = -1;
	}
	tree->Fill();
}

double broc::EleEfficiencyEstimator::min_lep_jet_dR(mor::Electron *lepton)
{
	double min_dR = -1;
	for(std::vector<mor::Jet>::iterator jet = jets->begin();
		jet != jets->end();
		++jet){
		double dR = ROOT::Math::VectorUtil::DeltaR(*jet, *lepton);
		if(min_dR == -1 || dR < min_dR){
			min_dR = dR;
		}
	}

	return min_dR;
}

