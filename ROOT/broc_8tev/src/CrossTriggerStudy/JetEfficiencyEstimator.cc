#include "../../interface/CrossTriggerStudy/JetEfficiencyEstimator.h"

broc::JetEfficiencyEstimator::JetEfficiencyEstimator(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	this->primary_vertices = handle_holder->get_tight_primary_vertices();
	this->event_information = handle_holder->get_event_information();
	this->prescale = handle_holder->get_prescale_provider();


	trig_name_prov = new eire::TriggerNameProvider();
	trigObj_crosstrig = new truicear::TriggerObjectSelector(handle_holder);
	jet_trigger_matcher = new truicear::TriggerMatcher<mor::Jet, mor::TriggerObject>();
  	jet_trigger_matcher->set_matching_method(0);
	double max_trig_dR = 0.1;
	jet_trigger_matcher->set_max_dR(max_trig_dR);

	//	jet = new std::vector<mor::Jet>();
	jet_id = -1;

	// get trigger name
	id = handle_holder->get_ident();
	jets = handle_holder->get_tight_jets();
	trigger_id = (int) handle_holder->get_cuts_set()->get_cut_value("hlt_cross_trigger");
	// get number of jet for current trigger
	jet_id = (unsigned int) handle_holder->get_cuts_set()->get_cut_value("cross_trigger_jetID");
	jet_id = jet_id-1;


	jet_pt=0;
	jet_eta=0;
	jet_phi=0;
	jet_abseta=0;

	njets=0;
	npvertices=0;
	run=0;
	lumi_section=0;
	passing_cross_trigger = -1;
	event_weight = 1;

	prescale_factor = 1;
}

broc::JetEfficiencyEstimator::~JetEfficiencyEstimator()
{
	if(trig_name_prov){ delete trig_name_prov; trig_name_prov = NULL; }
	if(trigObj_crosstrig){ delete trigObj_crosstrig; trigObj_crosstrig = NULL; }
	if(jet_trigger_matcher){ delete jet_trigger_matcher; jet_trigger_matcher = NULL; }
}

void broc::JetEfficiencyEstimator::book_branches()
{
	tree->Branch("jet_pt",&jet_pt, "jet_pt/F");
 	tree->Branch("jet_eta",&jet_eta, "jet_eta/F");
	tree->Branch("jet_phi",&jet_phi, "jet_phi/F");
	tree->Branch("jet_abseta",&jet_abseta, "jet_abseta/F");

	tree->Branch("njets",&njets, "njets/F");
	tree->Branch("run",&run, "run/F");
	tree->Branch("lumi_section",&lumi_section, "lumi_section/F");
	tree->Branch("npvertices",&npvertices, "npvertices/F");
	tree->Branch("event_weight",&event_weight, "event_weight/F");
	tree->Branch("prescale_factor",&prescale_factor, "prescale_factor/F");
	tree->Branch("passing_cross_trigger",&passing_cross_trigger, "passing_cross_trigger/I");
}

void broc::JetEfficiencyEstimator::fill_branches()
{
	std::string trigger_name = trig_name_prov->hlt_name(trigger_id);
        // check if trigger passed event                                

	trigObj_crosstrig->set_trigger_name(trigger_name);
	trig_objects = trigObj_crosstrig->get_selected_objects(handle_holder->get_trigger_objects());
	matched_jets = jet_trigger_matcher->match(jets, trig_objects);
        
        bool passed_trigger = handle_holder->get_trigger()->triggered(trigger_name);
        // fill Nth jet to pass/fail histo                                                                                                                                                                                                
//	std::cout << "matched jets size = " << matched_jets->size() << " || jet id = " << jet_id << std::endl;
//	std::cout << "jets size = " << jets->size() << " || jet id = " << jet_id << std::endl;
/*
        if(matched_jets->size() > (jet_id) && passed_trigger){
		jet_pt       = (*matched_jets)[jet_id].pt();
		jet_eta      = (*matched_jets)[jet_id].eta();
		jet_phi      = (*matched_jets)[jet_id].phi();
		jet_abseta   = std::abs((*matched_jets)[jet_id].eta());
		npvertices   = primary_vertices->size();
		njets        = (*matched_jets).size();
		run          = event_information->run();
		lumi_section = event_information->lumi_block();
		event_weight = handle_holder->get_event_weight();
		passing_cross_trigger = 1;	// passed trigger && matched

		prescale_factor = prescale->prescale(trigger_name);
		tree->Fill();
	}
	else*/ if(jets->size() > (jet_id)){
		jet_pt       = (*jets)[jet_id].pt();
		jet_eta      = (*jets)[jet_id].eta();
		jet_phi      = (*jets)[jet_id].phi();
		jet_abseta   = std::abs((*jets)[jet_id].eta());
		npvertices   = primary_vertices->size();
		njets        = (*jets).size();
		run          = event_information->run();
		lumi_section = event_information->lumi_block();
		event_weight = handle_holder->get_event_weight();
		//passing_cross_trigger = 0;	// didn't match -> fail
		passing_cross_trigger = passed_trigger;	// didn't match -> fail
//		std::cout << "trigger name " << trigger_name << std::endl;
		prescale_factor = prescale->prescale(trigger_name);
		tree->Fill();
	}
}
