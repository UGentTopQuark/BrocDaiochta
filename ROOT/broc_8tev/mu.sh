#!/bin/bash

if  [ "$1" = ''  -o  "$2" = ''  -o  "$3" = ''  -o  "$4" = '' ] 
then
	echo
	echo './mu.sh <vers> <cutfile> <json> <process>'
	echo
	exit 1
fi

vers="$1"
fname="$2"
json="$3"
process="$4"
ls ../input_383_calopftc/${process}*.root -1 | sed "s/\n/\ /g" > ${process}_input.txt

cp share/default_config_sed.txt .
cat default_config_sed.txt | sed "s/PROCESS/$process/g" > ${process}_config.txt

cd cutset_generator/ ; ./change_cuts.pl  ${fname}_m.txt ; cd .. ; cd json2cpp ; ./json2cpp.pl ${json} ; cd .. ; make
./broc_draiochta ${process}_config.txt >& ${vers}-${process}.txt
mv ${process}_plots.root ${vers}-${process}.root

