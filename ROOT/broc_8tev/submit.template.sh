#!/bin/bash

#variables
vers="VERSION"
cutfil="CUTFIL"
sample="SAMPLE"
script="SCRIPT"
dest="VERSION-SAMPLE-PROCESS"
indir="MYINDIR"
pathtorel="MYREL"
boost="MYBOOST"
subpath="SUBPATH"

h="HEAD"
t="TAIL"

#setup 
source $VO_CMS_SW_DIR/cmsset_default.sh
cd $pathtorel
eval `scramv1 runtime -sh`
export CPLUS_INCLUDE_PATH=$boost/include:${CPLUS_INCLUDE_PATH}
export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:~/CMS/ttAnalysis/broc_draiochta/share/
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$boost/lib
export LIBRARY_PATH=$boost/lib:${LIBRARY_PATH}
cd $subpath

#create directory for job to run in and copy necessary files
mkdir $dest
cp $vers-flat-$sample $dest/.
cp $vers-flat-${sample}_wc $dest/.
cp $script.sh $dest/.
cp default_config_sed.txt $dest/.
cd $dest

#run the job
./$script.sh $vers $sample $h $t $indir

#then clean after ourselves
rm $script.sh
rm $vers-flat-$sample 
####gzip *.txt

