#include <iostream>

#include "TCut.h"
#include "TFile.h"
#include "TSystem.h"
#include "TTree.h"

#include "TMVAGui.C"
#include "TMVA/Tools.h"
#include "TMVA/Factory.h"
#include "TPluginManager.h"





// ---------------------------------------------------------------


void TrainTMVA_newtest()
{
   // explicit loading of the shared libTMVA is done in TMVAlogon.C, defined in .rootrc
   // if you use your private .rootrc, or run from a different directory, please copy the 
   // corresponding lines from .rootrc

   // methods to be processed can be given as an argument; use format:
   //
   // mylinux~> root -l TMVAnalysis.C\(\"myMethod1,myMethod2,myMethod3\"\)
   //
   // if you like to use a method via the plugin mechanism, we recommend using
   // 
   // mylinux~> root -l TMVAnalysis.C\(\"P_myMethod\"\)
   // (an example is given for using the BDT as plugin (see below),
   // but of course the real application is when you write your own
   // method based)
   //

  
   std::cout << "Start TMVAnalysis" << std::endl
             << "=================" << std::endl
             << std::endl;
   std::cout << "Running all standard methods may take about 10 minutes of running..." << std::endl;

   // Create a new root output file.
   TString outfileName( "TMVA.root" );
   TFile* outputFile = TFile::Open( outfileName, "RECREATE" );

   // Create the factory object. Later you can choose the methods
   // whose performance you'd like to investigate. The factory will
   // then run the performance analysis for you.
   //
   // The first argument is the base of the name of all the
   // weightfiles in the directory weight/ 
   //
   // The second argument is the output file for the training results
   // All TMVA output can be suppressed by removing the "!" (not) in 
   // front of the "Silent" argument in the option string
   TMVA::Factory *factory = new TMVA::Factory( "TrainTMVA", outputFile, 
                                               Form("!V:!Silent:%sColor", gROOT->IsBatch()?"!":"") );

   // If you wish to modify default settings 
   // (please check "src/Config.h" to see all available global options)
   //    (TMVA::gConfig().GetVariablePlotting()).fTimesRMS = 8.0;
   //    (TMVA::gConfig().GetIONames()).fWeightFileDir = "myWeightDirectory";






   factory->AddVariable("m3",'F');
   factory->AddVariable("HlT",'F');
   factory->AddVariable("mu1_pt",'F');
   factory->AddVariable("mu1_eta",'F');
   factory->AddVariable("dphimmet",'F');
   factory->AddVariable("detajm", 'F');
   factory->AddVariable("drjm", 'F');
   factory->AddVariable("jet2_pt", 'F');
   factory->AddVariable("njets", 'I');

//   factory->AddVariable("j1_bid_bl",'F');
//   factory->AddVariable("j1_bid_bc",'F');
//   factory->AddVariable("j1_bid_bb",'F');
//
//   factory->AddVariable("j2_bid_bl",'F');
//   factory->AddVariable("j2_bid_bc",'F');
//   factory->AddVariable("j2_bid_bb",'F');
 





//   factory->SetWeightExpression("weight");

   char tmpchar[500];




   sprintf(tmpchar,"Signal_train.root");
   TFile *sigtrainfile = new TFile(tmpchar);		      

   sprintf(tmpchar,"Signal_validate.root");
   TFile *sigtestfile = new TFile(tmpchar);		      

   sprintf(tmpchar,"Background_train.root");
   TFile *bkgtrainfile = new TFile(tmpchar);		  

   sprintf(tmpchar,"Background_validate.root");
   TFile *bkgtestfile = new TFile(tmpchar);		  



   if (!sigtrainfile || !bkgtrainfile || !sigtestfile || !bkgtestfile) {
     std::cout << "ERROR: could not open data file" << std::endl;
     exit(1);
   }


   TTree *sigtree;
   TTree *bkgtree;

   TTree *sigtraintree;
   TTree *bkgtraintree;

   TTree *sigtesttree;
   TTree *bkgtesttree;

   sigtraintree = (TTree*)sigtrainfile->Get("tree");
   bkgtraintree = (TTree*)bkgtrainfile->Get("tree");
   sigtesttree  = (TTree*)sigtestfile->Get("tree");
   bkgtesttree  = (TTree*)bkgtestfile->Get("tree");
   
   // global event weights per tree (see below for setting event-wise weights)
   Double_t signalWeight          = 1.0;
   Double_t backgroundWeight      = 1.0;
   Double_t signalTrainWeight     = 1.0;
   Double_t backgroundTrainWeight = 1.0;
   Double_t signalTestWeight      = 1.0;
   Double_t backgroundTestWeight  = 1.0;
   
   // ====== register trees ====================================================
   //
   // the following method is the prefered one:
   // you can add an arbitrary number of signal or background trees

//   factory->AddSignalTree    ( sigtree,     signalWeight     );
//   factory->AddBackgroundTree( bkgtree,     backgroundWeight );
   



   // To give different trees for training and testing, do as follows:
   factory->AddSignalTree( sigtraintree,  signalTrainWeight, "Train" );
   factory->AddSignalTree( sigtesttree,   signalTestWeight,  "Test" );
   
   factory->AddBackgroundTree( bkgtraintree, backgroundTrainWeight, "Train" );
   factory->AddBackgroundTree( bkgtesttree,  backgroundTestWeight,  "Test" );



   // Apply additional cuts on the signal and background samples (can be different)

   TCut mycuts = ""; // for example: TCut mycuts = "abs(var1)<0.5 && abs(var2-0.5)<1";
   TCut mycutb = ""; // for example: TCut mycutb = "abs(var1)<0.5";


//   TCut mycuts = "j1j2_m>80. && j1j2_m<120."; // for example: TCut mycuts = "abs(var1)<0.5 && abs(var2-0.5)<1";
//   TCut mycutb = "j1j2_m>80. && j1j2_m<120."; // for example: TCut mycutb = "abs(var1)<0.5";



   // tell the factory to use all remaining events in the trees after training for testing:
   factory->PrepareTrainingAndTestTree( mycuts, mycutb,
//                                        "SplitMode=Random:NormMode=NumEvents:!V" );
                                        "SplitMode=Random:NormMode=EqualNumEvents:V" );



   char name[200];
   sprintf(name,"RF");

//   char name1[200];
//   sprintf(name1,"BDTG_3n_%s%s",Mass,DTST);
//
//   char name2[200];
//   sprintf(name2,"BDTG_5n_%s%s",Mass,DTST);
//
//   char name3[200];
//   sprintf(name3,"BDTG_10n_%s%s",Mass,DTST);
//
//   char nametest[200];
//   sprintf(nametest,"BDTG_15n_%s%s",Mass,DTST);


   //   cout<<"*********************************"<<name<<endl;
   //   cout<<"*********************************"<<name1<<endl;
   //   cout<<"*********************************"<<name2<<endl;
   //   cout<<"*********************************"<<name3<<endl;



//       factory->BookMethod( TMVA::Types::kBDT,name, "!H:V:NTrees=1000:BoostType=Grad:Shrinkage=0.10:UseBaggedGrad:GradBaggingFraction=0.6:SeparationType=GiniIndex:nCuts=20:PruneMethod=CostComplexity:PruneStrength=50:NNodesMax=3:IgnoreNegWeightsInTraining=True" ); ////SET HERE METHOD AND PARAMETER
	factory->BookMethod( TMVA::Types::kBDT,name, "!H:V:NTrees=100:BoostType=Bagging:SeparationType=GiniIndex:nCuts=20:PruneMethod=CostComplexity:PruneStrength=-1:UseRandomisedTrees=kTrue:UseNvars=9:nEventsMin=700");


   // --------------------------------------------------------------------------------------------------

   // ---- Now you can tell the factory to train, test, and evaluate the MVAs

   // Train MVAs using the set of training events
   factory->TrainAllMethods();

   // ---- Evaluate all MVAs using the set of test events
   factory->TestAllMethods();

   // ----- Evaluate and compare performance of all configured MVAs
   factory->EvaluateAllMethods();    

   // --------------------------------------------------------------
   
   // Save the output
   outputFile->Close();

   std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;
   std::cout << "==> TMVAnalysis is done!" << std::endl;      

   //delete mlist;
   delete factory;

   // Launch the GUI for the root macros
   if (!gROOT->IsBatch()) TMVAGui( outfileName );
}
