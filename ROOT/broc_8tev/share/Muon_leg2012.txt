[global]
dataset = PROCESS
input_files = PROCESS_input.txt
output_directory = .
max_events = -1
object_type = 0
recompute_met = false
allowed_cuts_file = share/allowed_cuts.txt
allowed_vcuts_file = share/allowed_vcuts.txt
hitfit_config_file = share/hitfit_study.txt
suppress_JER_bias_correction = false		# don't do JER bias correction for jets
JEC_dir = share/JEC/
JEC = 
JEC_uncertainties = 
#JEC = GR_R_38X_V15_AK5PF_L2Relative.txt:GR_R_38X_V15_AK5PF_L3Absolute.txt:GR_R_38X_V15_AK5PF_L2L3Residual.txt
#JEC_uncertainties = GR_R_38X_V15_AK5PF_Uncertainty.txt
outfile_suffix =
dont_split_ttbar = true #All ttbar is ttbar|muon|. i.e. not split into mu and mu_background. default = false.

apply_dataset_name_mapping = false # allow mapping of dataset name to histo identifiers, only relevant for systematics
dataset_mapping_file = share/dataset_mapping.txt
process_trigger_objects = true
process_prescales = false

enable_event_weights = true
enable_mva_module = false
enable_tag_and_probe_module = false
enable_btag_system8_module = false
enable_plots_module = true
enable_cross_trigger_module = true
enable_kinematic_fit_module = false

[MVA]
method_name = BDT
weights_file = TrainTMVA_RF.weights.txt
classify_events = true
create_tree = false

[tag_and_probe]
fill_electron_trigger_trees = false
fill_muon_trigger_trees = false

[cross_trigger]
enable_electron = false
enable_jets = false
enable_muon = false
enable_total = true
enable_el_leg = false
enable_mu_leg = true
enable_jet_leg = false
enable_total_leg = false
jet_one = false
jet_two = false
jet_three = false

[plots]
plot_met = false
plot_mass = false
plot_kinematics = true
plot_trigger = false
plot_lept_iso = false
plot_btag = false
plot_abcd = false
plot_xplusnjets = false
plot_mm = false
plot_spin = false
plot_gen_evt_info = false ;can only be set to true if also plot_spin is true

[event_weights]
pdf_name = cteq66
pdf_weights = false
pu_weights = true
3d_pu_weights_file = share/Weight3D_1p0.root
suppress_pdf_offset_correction = true
