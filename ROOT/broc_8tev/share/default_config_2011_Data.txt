[global]
dataset = Data
input_files = Data_input_files.txt
output_directory = .
max_events = -1
object_type = 0 #0 = calo or default, 1 = pf
recompute_met = false
allowed_cuts_file = share/allowed_cuts.txt
allowed_vcuts_file = share/allowed_vcuts.txt
JEC_dir = share/JEC/
JEC =GR_R_42_V19_AK5PFchs_L1FastJet.txt:GR_R_42_V19_AK5PFchs_L2Relative.txt:GR_R_42_V19_AK5PFchs_L3Absolute.txt:GR_R_42_V19_AK5PFchs_L2L3Residual.txt
JEC_uncertainties = 

enable_mva_module = false
enable_tag_and_probe_module = false
enable_plots_module = true

produce_tree = false

[tag_and_probe]
fill_electron_trigger_trees = false
fill_muon_trigger_trees = true

[plots]
plot_met = false
plot_mass = true
plot_kinematics = true
plot_trigger = false
plot_lept_iso = true
plot_btag = false
plot_abcd = false
plot_xplusnjets = false
plot_mm = false

[event_weights]
pdf_weights = false
lep_eff_weights = false
process_electron_efficiencies = false
process_muon_efficiencies = false
lep_eff_weights_file = share/abseta_scale_factors_muon.txt
pu_weights = false
data_pu_file = share/Pileup_2011_EPS_8_jul.root
mc_pu_file = share/MC_PU_distribution.root



[MVA]
;method_name = MLP
method_name = NeuroBayes
weights_file = /localgrid/bklein/analysis/broc_draiochta/share/MVA/weights/TMVAClassification_NeuroBayes.weights.xml
classify_events = true
create_tree = false

