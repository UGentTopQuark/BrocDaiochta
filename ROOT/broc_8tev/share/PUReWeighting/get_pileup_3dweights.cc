#include <vector>
#include "../PhysicsTools/Utilities/interface/Lumi3DReWeighting.h"

int main(int argc, char **argv)
{
	if(argc < 4){
		std::cout << "usage: " << argv[0] << " <mc file> <data_file> <scale>" << std::endl;
		exit(1);
	}

	edm::Lumi3DReWeighting *reweighter = new edm::Lumi3DReWeighting(argv[1], argv[2], "pileup", "pileup");
	reweighter->weight3D_init(atof(argv[3]));

	if(reweighter){ delete reweighter; reweighter = NULL; }

	return 0;
}
