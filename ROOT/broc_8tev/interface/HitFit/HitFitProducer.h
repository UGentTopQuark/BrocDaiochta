#ifndef HITFIT_HITFITPRODUCER_H
#define HITFIT_HITFITPRODUCER_H

// needs more objects!
#include <string>
#include "Core/RunHitFit.hpp"
#include "Core/JetTranslatorBase.hpp"
#include "Core/LeptonTranslatorBase.hpp"
#include "Core/METTranslatorBase.hpp"

#include "../TagAndProbe/TreeProducer.h"
#include "../EventSelection/HandleHolder.h"
#include "../MorObjects/MTTbarGenEvent.h"
#include "../BeagObjects/Jet.h"

typedef hitfit::RunHitFit<mor::Electron,mor::Muon,mor::Jet,mor::MET> BrocHitFit;

namespace hitfit{
	/// semi-leptonic parton enum used to define the order 
	/// in the vector for lepton and jet combinatorics
	enum { LightQ, LightQBar, HadB, LepB, Lepton };

	class HitFitProducer : public clibisfiosraigh::TreeProducer{
		public:
			HitFitProducer(eire::HandleHolder *handle_holder);
			~HitFitProducer();

			virtual void book_branches();
			virtual void fill_branches();

		private:
			eire::HandleHolder *handle_holder;
			mor::TTbarGenEvent *gen_evt;

			mor::EventInformation *event_information;

			BrocHitFit *HitFit;
			
			Int_t maxNComb_;
			static const Int_t maxNCombStatic = 120;

			struct FitResult {
				int Status;
				double Chi2;
				double Prob;
				double MT;
				double SigMT;

				mor::Particle HadB;
				mor::Particle HadP;
				mor::Particle HadQ;
				mor::Particle LepB;
				mor::Particle LepL;
				mor::Particle LepN;

				std::vector<int> JetCombi;
				bool operator< (const FitResult& rhs) { return Chi2 < rhs.Chi2; };
			};

			struct TTGenEvent {
				mor::Particle HadB;
				mor::Particle HadP;
				mor::Particle HadQ;
				mor::Particle LepB;
				mor::Particle LepL;
				mor::Particle LepN;

				std::vector<int> JetCombi;
			};

			//			maxNComb_ =  atoi(hitfit_config_reader->get_var("maxNComb", "mass_studies",false).c_str()); // int   //1 
	
			struct FitResultContainer {
				//Int_t trueCombi[4][maxNCombStatic];
				Int_t Combi[4][maxNCombStatic];
				Int_t Status[maxNCombStatic];
				Double_t Chi2[maxNCombStatic];
				Double_t Prob[maxNCombStatic];
				Double_t MT[maxNCombStatic];
				Double_t SigMT[maxNCombStatic];

				Int_t nSol;
				Double_t HadB_pt[maxNCombStatic];
				Double_t HadB_eta[maxNCombStatic];
				Double_t HadB_phi[maxNCombStatic];
				Double_t HadB_m[maxNCombStatic];

				Double_t HadP_pt[maxNCombStatic];
				Double_t HadP_eta[maxNCombStatic];
				Double_t HadP_phi[maxNCombStatic];
				Double_t HadP_m[maxNCombStatic];

				Double_t HadQ_pt[maxNCombStatic];
				Double_t HadQ_eta[maxNCombStatic];
				Double_t HadQ_phi[maxNCombStatic];
				Double_t HadQ_m[maxNCombStatic];

				Double_t LepB_pt[maxNCombStatic];
				Double_t LepB_eta[maxNCombStatic];
				Double_t LepB_phi[maxNCombStatic];
				Double_t LepB_m[maxNCombStatic];

				Double_t LepL_pt[maxNCombStatic];
				Double_t LepL_eta[maxNCombStatic];
				Double_t LepL_phi[maxNCombStatic];
				Double_t LepL_m[maxNCombStatic];

				Double_t LepN_pt[maxNCombStatic];
				Double_t LepN_eta[maxNCombStatic];
				Double_t LepN_phi[maxNCombStatic];
				Double_t LepN_m[maxNCombStatic];

				Int_t matched[maxNCombStatic];

			}fitResult;

			/// maximal number of jets (-1 possible to indicate 'all')                                                                                                                                                                     
			int maxNJets_;

			/// maximum eta value for muons, needed to limited range in which resolutions are provided                                                                                                                                     
			double maxEtaMu_;
			/// maximum eta value for electrons, needed to limited range in which resolutions are provided                                                                                                                                 
			double maxEtaEle_;
			/// maximum eta value for jets, needed to limited range in which resolutions are provided                                                                                                                                      
			double maxEtaJet_;

			/// input tag for b-tagging algorithm                                                                                                                                                                                          
			std::string bTagAlgo_;
			/// min value of bTag for a b-jet                                                                                                                                                                                              
			double minBTagValueBJet_;
			/// max value of bTag for a non-b-jet                                                                                                                                                                                          
			double maxBTagValueNonBJet_;
			/// switch to tell whether to use b-tagging or not                                                                                                                                                                             
			bool useBTag_;

			/// constraints                                                                                                                                                                                                                
			double mW_;
			double mTop_;

			/// jet correction level                                                                                                                                                                                                       
			std::string jetCorrectionLevel_;

			/// jet energy scale                                                                                                                                                                                                           
			double jes_;
			double jesB_;

			// mc truth information

			double mc_hadB_pt;
			double mc_lepB_pt;
			double mc_q_pt;
			double mc_qbar_pt;

			double mc_hadB_eta;
			double mc_lepB_eta;
			double mc_q_eta;
			double mc_qbar_eta;

			double mc_hadB_phi;
			double mc_lepB_phi;
			double mc_q_phi;
			double mc_qbar_phi;


			LeptonTranslatorBase<mor::Electron> electronTranslator_;
			LeptonTranslatorBase<mor::Muon>     muonTranslator_;
			JetTranslatorBase<mor::Jet>         jetTranslator_;
			METTranslatorBase<mor::MET>         metTranslator_;

			std::string hitfitDefault_;
			std::string hitfitElectronResolution_;
			std::string hitfitMuonResolution_;
			std::string hitfitUdscJetResolution_;
			std::string hitfitBJetResolution_;
			std::string hitfitMETResolution_;

			std::vector<mor::Particle> *pPartonsHadP;
			std::vector<mor::Particle> *pPartonsHadQ;
			std::vector<mor::Particle> *pPartonsHadB;
			std::vector<mor::Particle> *pPartonsLepB;
			std::vector<mor::Particle> *pLeptons;
			std::vector<mor::Particle> *pNeutrinos;

			std::vector<std::vector<int> > *pCombi;
			std::vector<double> *pChi2;
			std::vector<double> *pProb;
			std::vector<double> *pMT;
			std::vector<double> *pSigMT;
			std::vector<int> *pStatus;
			
			std::vector<mor::Jet> *jets;
			std::vector<beag::Jet> *beagjets;
			std::vector<mor::Muon> *muons;
			std::vector<mor::Electron> *electrons;
			std::vector<mor::MET> *mets;

		        Int_t trueCombi[4];
	};
}

#endif
