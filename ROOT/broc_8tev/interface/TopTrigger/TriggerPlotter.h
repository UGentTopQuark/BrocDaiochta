#ifndef TRIUCEARTRIGGERPLOTTER_H
#define TRIUCEARTRIGGERPLOTTER_H

#include "../AnalysisTools/HistoWriter.h"
#include "TH1F.h"
#include "TH2F.h"

namespace truicear{
	class TriggerPlotter{
		public:
			TriggerPlotter(HistoWriter *histo_writer, std::string name, int nbinsx, double minx, double maxx);
			~TriggerPlotter();

			fill(double value, bool pass);
		private:
			HistoWriter *histo_writer;
			TH1F *pass_1d;
			TH1F *all_1d;
	};
}

#endif
