#ifndef MATCHEDELECTRONPLOTS_H
#define MATCHEDELECTRONPLOTS_H

#include "TriggerPlot.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MTriggerObject.h"
#include "TriggerMatcher.h"

namespace truicear{
	class MatchedElectronPlots: public TriggerPlot{
		public:
			MatchedElectronPlots();
			~MatchedElectronPlots();

			void plot(std::vector<mor::Electron> *reco_electrons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<std::pair<int, int> >* match_results);
			void set_trigger_matcher(truicear::TriggerMatcher<mor::Electron, mor::TriggerObject> *trigger_matcher);
		private:
			virtual void book_histos();

			truicear::TriggerMatcher<mor::Electron, mor::TriggerObject> *trigger_matcher;
	};
}

#endif
