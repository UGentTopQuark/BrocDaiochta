#ifndef TRIGGERMATCHER_H
#define TRIGGERMATCHER_H

/** \class truicear::TriggerMatcher
 * 
 * \brief Implements matching of trigger objects to offline letpons
 *
 * The class implements the matching of trigger objects to offline leptons.
 * Offline lepton propagation to the different geometry of the L1 system is
 * supported as well as various mathching criteria.
 * 
 * \author klein
 */

#include <vector>

#include "../MorObjects/MJet.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MTriggerObject.h"
#include "Math/VectorUtil.h"

namespace truicear{
	template <class ParType1, class ParType2>
	class TriggerMatcher{
		public:
			TriggerMatcher();
			~TriggerMatcher();

			// match particles and return vector of pair of matched particles of collection 1 and 2
			std::vector<ParType1>* match(std::vector<ParType1> *type1_particles, std::vector<ParType2> *type2_particles);
			std::vector<std::pair<int, int> >* get_match_results();
			void set_matching_method(int selected_method);

			void set_max_dR(double dR);
			void set_max_dEta(double dEta);
			void set_max_dPt(double dPt);

			double get_smallest_match_diff();
			std::vector<double> *get_smallest_match_diffs(){return smallest_match_diffs;};
			std::vector<double> *get_smallest_match_diffs_no_double_matching(){return smallest_match_diffs_no_double_matching;};
			std::vector<ParType1>* get_type1_matched();
			std::vector<ParType2>* get_type2_matched();
			std::vector<ParType1>* get_type1_not_matched();
			std::vector<ParType2>* get_type2_not_matched();
		private:
			double match_dR(ParType1 *type1_particles, ParType2 *type2_particles);
			double match_dR_propagated(ParType1 *type1_particles, ParType2 *type2_particles);
			double match_dPt(ParType1 *type1_particles, ParType2 *type2_particles);
			double match_dEta(double type1_eta, double type2_eta);

			std::vector<std::pair<int, int> > *match_results;	// assignment between type 1 
											// and type 2 particles
			std::vector<ParType1>* type1_matched;
			std::vector<ParType1>* type1_unmatched;
			std::vector<ParType2>* type2_matched;
			std::vector<ParType2>* type2_unmatched;

			double max_dR;		// max value to match two particle for dR
			double max_dEta;		// max value to match two particle for dR
			double max_dPt;		// max value to match two particle for dPt

			int matching_method;	// matching method to be used (dR, dpt, deta...)

			double smallest_match_diff;	// the smallest distance between two objects in an event
			std::vector<double> *smallest_match_diffs;	// list of smallest differences for the various leptons
			std::vector<double> *smallest_match_diffs_no_double_matching;	// list of smallest differences for the various leptons

			static const bool verbose = false;
	};
}

#endif
