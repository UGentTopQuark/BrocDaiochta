#ifndef TRUICEARTRIGGERPLOT_H
#define TRUICEARTRIGGERPLOT_H

#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MTriggerObject.h"

#include <iostream>
#include <map>
#include "../AnalysisTools/ETH1F.h"
#include "../AnalysisTools/ETH2F.h"

#include "../AnalysisTools/HistoWriter.h"

namespace truicear{
	class TriggerPlot{
		public:
			TriggerPlot();
			virtual ~TriggerPlot();
			void set_histo_writer(HistoWriter *histo_writer);
			void set_id(std::string ident, std::string trigger_name);
		protected:
			std::string id;
			std::string pre;	// a prefix for trigger names, eg. HLT_Mu9.pt_TTbar|...
	                std::map<std::string,eire::TH1F*> histos1d;
	                std::map<std::string,eire::TH2F*> histos2d;
	
			virtual void book_histos()=0;
	
			bool histos_booked;
	
			HistoWriter *histo_writer;
	};
}

#endif
