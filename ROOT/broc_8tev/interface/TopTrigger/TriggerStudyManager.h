#ifndef TRIGGERSTUDYMANAGER_H
#define TRIGGERSTUDYMANAGER_H

#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MEventInformation.h"
#include "../MorObjects/MTrigger.h"
#include "../EventSelection/CutsSet.h"
#include "../EventSelection/TriggerNameProvider.h"

#include "BasicMuonPlots.h"
#include "BasicElectronPlots.h"
#include "BasicJetPlots.h"
#include "MatchedMuonPlots.h"
#include "MatchedElectronPlots.h"
#include "TriggerObjectSelector.h"
#include "TopComSyncEx.h"
#include "MatchingCriteria.h"
#include "TriggerStudy.h"

#include "../AnalysisTools/HistoWriter.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TriggerMatcher.h"
#include <map>
#include "math.h"

namespace truicear{
	class TriggerStudyManager{
		public:
		        TriggerStudyManager(std::string ident);
			~TriggerStudyManager();
			
			void set_handles( std::vector<mor::Jet> *jets,
					  std::vector<mor::Electron> *electrons,
					  std::vector<mor::Muon> *muons,
					  std::vector<mor::MET> *mets,
					  mor::EventInformation *event_information,
					  std::vector<mor::TriggerObject> *trigger_objects,
					  mor::Trigger *HLTR);

			void set_histo_writer(HistoWriter *histo_writer);
			void set_trigger_study_cuts(broc::CutsSet *cuts_set);

			void study_triggers();
			
	        private:
			void book_plot_objects();

			std::string id;
			HistoWriter *histo_writer;
			
			std::vector<mor::Jet>* jets;
			std::vector<mor::Electron>* isolated_electrons;
			std::vector<mor::Muon>* isolated_muons;
			std::vector<mor::MET>* corrected_mets;
			mor::EventInformation *event_information;
			std::vector<mor::TriggerObject>* trigger_objects;

			truicear::BasicMuonPlots *all_basic_mu_plots;
			truicear::BasicElectronPlots *all_basic_e_plots;
			truicear::BasicJetPlots *all_basic_jet_plots;
			truicear::TopComSyncEx *sync_ex;

			std::vector<double> *min_hlt_pt;
			std::vector<double> *min_hlt_eta;
			std::vector<double> *max_hlt_eta;
			std::vector<double> *max_hlt_dR;
			std::vector<double> *max_hlt_dPt;
			std::vector<double> *max_hlt_dEta;
			std::vector<double> *max_L1_dR;
			std::vector<double> *max_L1_dPt;
			std::vector<double> *max_L1_dEta;
			std::vector<double> *max_HLTL1_dR;
			std::vector<double> *max_HLTL1_dPt;
			std::vector<double> *max_HLTL1_dEta;
			std::vector<double> *hlt_lep_triggers;
			double trigger_study_type;

			mor::Trigger *HLTR;

			//counters for event info
			int nlep_found;
			std::vector<double> nl1seed_found;
			std::vector<double> nhlt_obj_found;
			std::vector<double> nlep_l1seed_match;
			std::vector<double> nlep_l1seed_hlt_match;
			std::vector<double> nlep_hlt_match;

			bool do_sync;

			std::vector<truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>* > electron_trigger_studies;
			std::vector<truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>* > muon_trigger_studies;
			std::vector<std::map<std::string, truicear::MatchingCriteria*>* > matching_criteria_to_delete;	// delete in destructor
	};
}

#endif
