#ifndef tionscadaldorcha_CLASSIFICATIONMANAGER_H
#define tionscadaldorcha_CLASSIFICATIONMANAGER_H

/** \class tionscadaldorcha::ClassificationManager
 * 
 * \brief Controls various MVA analysers and enables the user to generate
 * classification training trees as well to classify events.
 *
 * At the moment this class supports only the classification with TMVA. The
 * training trees generated are simple ROOT trees and can therefore in
 * principle also be used in other MVA packages.
 * 
 * \author klein
 */

#include "../EventSelection/HandleHolder.h"
#include "EventShapeVariableCalculator.h"
#include "../../src/ConfigReader/ConfigReader.h"
#include "EventClassifier.h"
#include "TionscadalDorchaTreeProducer.h"
#include "../EventSelection/CentralServices.h"
//jl 10.06.11: for MM propagation
#include "../../interface/EventSelection/QCDMMPlots.h"

namespace tionscadaldorcha{
	class ClassificationManager{
		public:
			ClassificationManager(eire::ConfigReader *config_reader, eire::HandleHolder *handle_holder);
			~ClassificationManager();
			inline void set_handles(eire::HandleHolder *handle_holder){this->handle_holder = handle_holder;};
			void run();
			void initialise();

			inline double get_discriminator(){ return discriminator; }
		private:
			eire::HandleHolder *handle_holder;
			tionscadaldorcha::EventShapeVariableCalculator *evt_shape;
			tionscadaldorcha::EventClassifier *evt_classifier;
			tionscadaldorcha::TionscadalDorchaTreeProducer *tree_producer;
			TFile *flat_tree_outfile;

			bool classify_events;
			bool create_tree;

			double discriminator;
			eire::TH1F *discriminator_plot;
			std::string id;
			//jl 10.06.11: propagate MM to this class
			QCDMMPlots * mm;
			double mmweight;
			eire::TH1F * QCD_discriminator_plot;
	};
}

#endif
