#ifndef broc_CLASSIFICATIONVARIABLECALCULATOR_H
#define broc_CLASSIFICATIONVARIABLECALCULATOR_H

#include "../EventSelection/HandleHolder.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

namespace broc{
	class ClassificationVariableCalculator{
		public:
			ClassificationVariableCalculator(eire::HandleHolder *handle_holder);
			~ClassificationVariableCalculator();
			template<class LEPTON>
			double get_drjl(std::vector<LEPTON> *leptons, std::vector<mor::Jet> *jets);
			template<class LEPTON>
			double get_detajl(std::vector<LEPTON> *leptons, std::vector<mor::Jet> *jets);
			template <class LEPTON>
			double get_dphilmet(std::vector<LEPTON> *leptons);
		private:
			template<class LEPTON>
			void calculate_lepton_jet_distance(std::vector<LEPTON> *leptons, std::vector<mor::Jet> *jets);

			eire::HandleHolder *handle_holder;
                	double drjl;
                	double detajl;
	};
}

#endif
