#ifndef BROC_JETEFFWEIGHTPROVIDER_H
#define BROC_JETEFFWEIGHTPROVIDER_H

/** \class broc::LeptonEfficiencyWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on efficiency studies of lepton in event.
 *
 * \author klein
 */

#include "../../src/ConfigReader/ConfigReader.h"
#include "HandleHolder.h"
#include "EventWeightProvider.h"
#include "../MorObjects/MJet.h"
#include <string>

namespace broc{
	class JetEffWeightProvider{
		public:
			JetEffWeightProvider(eire::HandleHolder *handle_holder);
			~JetEffWeightProvider();
			double get_weight();
		protected:
			bool do_not_reweight;

			std::vector<mor::Jet> *jets;
	};
}

#endif
