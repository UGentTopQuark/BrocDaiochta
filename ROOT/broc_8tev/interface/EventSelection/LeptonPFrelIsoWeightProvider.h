#ifndef BROC_LEPPFRELISOEVENTWEIGHTPROVIDER_H
#define BROC_LEPPFRELISOEVENTWEIGHTPROVIDER_H

/** \class broc::LeptonPFrelIsoWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on abseta of lepton in event.
 *
 * \author klein
 */

#include "LeptonEfficiencyWeightProvider.h"

namespace broc{
	class LeptonPFrelIsoWeightProvider : public LeptonEfficiencyWeightProvider{
		public:
			LeptonPFrelIsoWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder);
			~LeptonPFrelIsoWeightProvider();
		protected:
			virtual double get_value_for_event();
	};
}

#endif
