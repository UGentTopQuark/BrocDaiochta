#ifndef M3MASS_H
#define M3MASS_H

/**
 * \class M3Mass
 * 
 * \brief Simple M3 mass reconstruction class.
 *
 * The class performs a mass reconstruction according to the M3 definition:
 * Search in all three jet combinations in the event for that comination of jet
 * for which the transverse momentum of the four vector sum of the three jets
 * is maximised.
 *
 * \authors walsh, klein
 *
 */


#include "MassReconstructionMethod.h"

class M3Mass: public MassReconstructionMethod {
	public:

	protected:
		void calculate_mass();
};

#endif
