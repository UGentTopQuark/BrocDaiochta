#ifndef JETSELECTOR_H
#define JETSELECTOR_H

#include "../MorObjects/MJet.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "CutsSet.h"

#include "PtSorter.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include "../../share/CondFormats/JetMETObjects/interface/JetCorrectionUncertainty.h"
#include "../../src/ConfigReader/ConfigReader.h"
#include "JERCorrector.h"
#include "HandleHolder.h"

class JetSelector{
	public:
		JetSelector(eire::HandleHolder *handle_holder);
		~JetSelector();

		void set_cuts_set(broc::CutsSet *cuts_set);

		std::vector<mor::Jet>* get_jets(std::vector<mor::Jet> *uncut_jets, std::vector<mor::Muon> *muons, std::vector<mor::Electron> *electrons);
		void initialise_jec_uncertainty(eire::ConfigReader *config_reader);

	private:
		double max_eta;
		double min_e_dR;
		double min_mu_dR;
		double max_fHPD;
		double min_n90Hits;
		double min_emf;

		double min_nconstituents;	// number of constituents
		double min_chf;		// charged hardron energy fraction
		double max_nhf;		// neutral hardon energy fraction
		double max_nemf;		// neutral em energy fraction
		double max_cemf;		// charged em energy fraction
		double min_cmulti;	// charged multiplicity

		/*
		 *	study_jes_unc:
		 *	0: disable
		 *	1: scale down
		 *	2: scale up
		 */
		double study_jes_unc;
		double study_jer_unc;

		std::vector<double> *min_pt;
		std::vector<double> *max_pt;

		std::vector<mor::Jet> *cut_jets;

		JetCorrectionUncertainty *jecUnc;
		broc::JERCorrector *jer_corrector;

		static const bool verbose = false;
	
		bool cut_pt(mor::Jet *jet_iter, double &min_pt);
		bool cut_max_pt(mor::Jet *jet_iter, double &max_pt);
		bool cut_eta(mor::Jet *jet_iter, double &max_eta);
		bool cut_max_fHPD(mor::Jet *jet_iter, double &max_fHPD);
		bool cut_min_n90Hits(mor::Jet *jet_iter, double &min_n90Hits);
		bool cut_min_emf(mor::Jet *jet_iter, double &min_emf);
		template <typename myLepton>
		bool cut_leptons(mor::Jet *jet_iter, std::vector<myLepton> *leptons, double min_dR);

		// PF quality cuts
		bool cut_min_nconstituents(mor::Jet *jet_iter, double &min_nconstituents);
		bool cut_min_chf(mor::Jet *jet_iter, double &min_chf);
		bool cut_max_nhf(mor::Jet *jet_iter, double &max_nhf);
		bool cut_max_nemf(mor::Jet *jet_iter, double &max_nemf);
		bool cut_max_cemf(mor::Jet *jet_iter, double &max_cemf);
		bool cut_min_cmulti(mor::Jet *jet_iter, double &min_cmulti);

		bool is_real_data;
		bool suppress_JER_bias_correction;

		eire::HandleHolder *handle_holder;
};
#endif
