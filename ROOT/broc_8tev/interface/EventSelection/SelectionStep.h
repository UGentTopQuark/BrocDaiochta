#ifndef BROC_SELECTIONSTEP_H
#define BROC_SELECTIONSTEP_H

/**
 * \class broc::SelectionStep
 * 
 * \brief Represents one selection step and manages all information/modules per selection step.
 *
 * Selection steps are booked in CutSelector. Each selection step performs an
 * object selection and enables several modules for different studies.
 *
 * \author klein
 */

#include "CutsSet.h"
#include "HandleHolder.h"
#include "Cuts.h"
#include "PlotGenerator.h"
#include "CollectionSkimmer.h"
#include "EventWeightManager.h"
#include "../TagAndProbe/TagAndProbe.h"
#include "../BTagStudies/BTagStudies.h"
#include "CentralServices.h"
#include "../CrossTriggerStudy/CrossTriggerEfficiencyManager.h"
#include "../HitFit/HitFitManager.h"

namespace broc{
	class SelectionStep{
		public:
			SelectionStep(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			~SelectionStep();

			void next_event();
		private:
			void book_modules(); //< Create for all activated modules a new object.
			void read_config(); //< Read all necessary parameters from configuration file.

			broc::Cuts *cuts;
			broc::CutsSet *cuts_set;
			eire::HandleHolder *handle_holder;
			broc::CollectionSkimmer *collection_skimmer;

			broc::PlotGenerator *plot_generator;
			tionscadaldorcha::ClassificationManager *event_classifier;
			broc::EventWeightManager *weight_manager;
			clibisfiosraigh::TagAndProbe *tag_and_probe;
			bclib::BTagStudies *btag_system8;
			broc::CentralServices *central_services;
			broc::CrossTriggerEfficiencyManager *cross_trigger_study;
			hitfit::HitFitManager *hitfit_study;

			TH1F *event_counter_histo;

			bool enable_mva_module;
			bool enable_plot_module;
			bool enable_tag_and_probe_module;
			bool enable_btag_system8_module;
			bool enable_cross_trigger_module;
			bool enable_kinematic_fit_module;
	};
}

#endif
