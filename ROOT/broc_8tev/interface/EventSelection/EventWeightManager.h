#ifndef BROC_EVENTWEIGHTMANAGER_H
#define BROC_EVENTWEIGHTMANAGER_H

/**
 * \class broc::EventWeightManager
 * 
 * \brief Calculates for each event a certain event weight.
 *
 * For each type of event weight (pdf weighs, mc weights, pile up weights, etc)
 * a new module should be defined. This module should inherit from
 * broc::EventWeight to guarantee a common interface. broc::EventWeightManager
 * executes then all broc::EventWeight modules and calculates a global weight
 * for the event.
 *
 * \author klein
 */

#include "HandleHolder.h"
#include "CutsSet.h"
#include "PDFEventWeight.h"
#include "PileUpEventWeightProvider.h"
#include "PVPileUpEventWeightProvider.h"
#include "LeptonAbsEtaWeightProvider.h"
#include "LeptonEtaWeightProvider.h"
#include "LeptonPtWeightProvider.h"
#include "LeptonPFrelIsoWeightProvider.h"
#include "LeptonEtaPFrelIso2DWeightProvider.h"
#include "JetEffWeightProvider.h"
#include "ETriggerEffWeightProvider.h"
#include "HandleHolder.h"
#include "../../src/ConfigReader/ConfigReader.h"

namespace broc{
	class EventWeightManager{
		public:
			EventWeightManager(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			~EventWeightManager();

			double get_all_events_weight();//< Does not include lepton scale factors.
			double get_selected_events_weight();//< Includes weights applied to all events. Must be called after all_events_weight
		private:
			double get_W_BR_correction_weight();
			double get_W_scale_correction_weight();

			broc::PDFEventWeight *pdf_event_weight;
			broc::PileUpEventWeightProvider *pu_event_weight;
			broc::LeptonAbsEtaWeightProvider *lep_abseta_event_weight;
			broc::LeptonEtaWeightProvider *lep_eta_event_weight;
			broc::LeptonPtWeightProvider *lep_pt_event_weight;
			broc::LeptonPFrelIsoWeightProvider *lep_pfreliso_event_weight;
			broc::LeptonEtaPFrelIso2DWeightProvider *lep_eta_pfreliso_2D_event_weight;
			broc::JetEffWeightProvider *jet_event_weight;
			broc::ETriggerEffWeightProvider *e_trig_event_weight;
			eire::HandleHolder *handle_holder;
	};
}

#endif
