#ifndef QCDMMPLOTS_H
#define QCDMMPLOTS_H

#include "Plots.h"
#include "Math/VectorUtil.h"
#include "HtCalculator.h"
#include "MassReconstruction.h"
#include "HandleHolder.h"

class QCDMMPlots : public Plots{
	public:
		QCDMMPlots(eire::HandleHolder *handle_holder, bool bookmyhists);
		~QCDMMPlots();
		double ComputeMMWeights(); //jl 10.06.11: make it double	
		void set_uncut_jets(std::vector<mor::Jet>* uncut_jets);
		void set_ht_calc(HtCalculator *ht_calc);void 
		set_mass_reco(MassReconstruction *mass_reco); //jl 17.12.10
                void set_PV(std::vector<mor::PrimaryVertex> *pvert); //jl 23.12.10
		
	private:
		
		void bookhistos();
		virtual void plot_all();
		
		void plot_QCDjets();
		void plot_QCDmuons();
		void plot_QCDelectrons();
		void plot_QCDht();
		void plot_QCDelectron_quality();
		void plot_QCDmuon_quality();
		void plot_QCDdR();
		void plot_QCDiso();
		void plot_QCDMET_MtW();
		void plot_top();
		void plot_PV(); //jl 23.12.10
		void GetLiteralValues(double pt, double dphi);//jl added 06.10.10, added dphi 05.11.10
		
		std::vector<mor::Jet>* uncut_jets;

		std::vector<mor::PrimaryVertex> *pv;//jl 23.12.10
		
		bool isData, isMuon;

		MassReconstruction *mass_reco;	
		HtCalculator *ht_calc;
		double ht;
		double epsQCD, epsSignal;
		double weight, w1, w2;
		//jl added 06.10.10
		static const bool literal = false;
};

#endif
