#ifndef BTAGPTRELPLOTS_H
#define BTAGPTRELPLOTS_H

/**
 * \class BTagPTrelPlots
 *
 * \brief Creates plots of pTrel for measurement of b-tagging efficiency
 *
 * The class finds a muon within a jet. Creates templates for different 
 * jet flavours (on MC) and fills  pTrel distribution.
 * Then created subset where jet is btagged with loose, medium or tight 
 * working point.
 * Output plots should be fed into fit_templates to measure b-tagging efficiency.
 *
 * \authors walsh
 */


#include "Plots.h"
#include "LeptonSelector.h"
#include "HandleHolder.h"
#include "CutsSet.h"


#include "../MorObjects/MTTbarGenEvent.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include "Math/Functions.h"
#include "TMath.h"
#include "TVector3.h"

class BTagPTRelPlots : public Plots{
	public:
	BTagPTRelPlots(std::string ident);
		~BTagPTRelPlots();

		
	private:
		virtual void book_histos();
		virtual void plot_all();

		bool select_muon_and_jet();
		void calculate_pt_rel();
		void plot_pt_rel_btagged();
		void plot_misc();

		void plot_pt_rel(std::string working_point = "",std::string algo = "");
		void plot_pt_rel_flavour(std::string wp = "",std::string flavour = "", std::string algo = "");
		void plot_bDiscrim(double bDiscrim,std::string algo = "");
		void plot_bDiscrim_flavour(double bDiscrim,std::string flavour = "",std::string algo = "");
		void plot_jet_mu_vars_flavour(std::string flavour = "");

		mor::Muon found_mu;
		mor::Jet found_jet;
		double pTrel;
		double unoff_pTrel;
		std::string id;

		std::vector<std::string> algo_ids;

		LeptonSelector<mor::Muon> *bjet_mu_selector;
};

#endif
