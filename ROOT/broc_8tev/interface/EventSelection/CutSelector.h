#ifndef CUTSELECTOR_H
#define CUTSELECTOR_H

/**
 * \class CutSelector
 * 
 * \brief Reads in user defined cuts for various selection steps and executes the analysis according to these selection steps.
 *
 * The class reads in a list of selection steps with each defined by a set of
 * cuts. For each of those sets of cuts the analysis is then performed (event
 * selection, generation of plots, execution of various modules, ...).
 * 
 * \authors walsh, klein
 */


#include "PlotGenerator.h"
#include "Cuts.h"
#include "SelectionStep.h"
#include "Tools.h"

#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MTrigger.h"
#include "../MorObjects/MTTbarGenEvent.h"
#include "../MorObjects/MPrimaryVertex.h"
#include "../MorObjects/MEventInformation.h"
#include "../MorObjects/MParticle.h"

#include <string>
#include <sstream>
#include <map>
#include <vector>
#include "TH1F.h"
#include "../AnalysisTools/HistoWriter.h"
#include "../AnalysisTools/PDFWeightProvider.h"

#include "../../src/ConfigReader/ConfigReader.h"

class CutSelector{
	public:
		CutSelector(eire::HandleHolder *handle_holder, eire::ConfigReader *config_reader);
		~CutSelector();

		void plot();
		void set_process_all_pdf_sets(bool process_all_pdf_sets, std::string pdf_set);
		void set_all_cuts();

	private:
		std::vector<std::vector<double>* > cuts_to_be_deleted;

		bool process_all_pdf_sets;		// clone cutssets for all pdf sets and weights
		std::string pdf_set;
		eire::PDFWeightProvider *pdf_weight_prov;

		eire::ConfigReader *config_reader;

		std::string get_event_type(std::string type_iter = "-1");
		void set_cuts();
		void add_pdf_cutsets();
		void complete_cuts();
		void define_cuts_autogen();
		std::vector<std::string> get_event_types(std::string event_type);
		void set_pdf_cutsets(std::string type, std::string cutset,std::map<std::string, double> cuts_set);
		void set_pdf_cutsets(std::string type, std::string cutset,std::map<std::string, std::vector<double>* > cuts_set);

		void synchronise_maps();
		void vcuts(std::string type, std::string set, std::string cut, std::vector<double> *cut_vector);
		void vcuts(std::string type, std::string set, std::string cut, double value);
		void set_if_not_set(std::string type, std::string set, std::string cut, double value);
		void vset_if_not_set(std::string type, std::string set, std::string cut, double value);
		void vset_if_not_set(std::string type, std::string set, std::string cut, std::vector<double>* value);

		std::map<std::string, std::map<std::string,broc::SelectionStep*> > selection_steps;

		std::vector<broc::CutsSet*> cuts_sets;

		std::map<std::string,std::map<std::string,std::map<std::string,double> > > cut_defs;
		std::map<std::string,std::map<std::string,std::map<std::string,std::vector<double>* > > > vcut_defs;

		std::string dataset_id;

		//for truth matching
		mor::TTbarGenEvent *genEvt;

		/*
		 *	count different event types:
		 *	muon: 1
		 *	electron: 2
		 *	background: 3
		 */
		TH1F *event_counter_histo;

		HistoWriter *histo_writer;

		std::string allowed_cuts_file;
		std::string allowed_vcuts_file;

		std::vector<eire::HandleHolder*> handle_holders;

		eire::HandleHolder *handle_holder;
		
		int data_count;
		int ttmu_count;
		int tte_count;
		int ttbg_count;
		double tprime_mass;
		bool Event_firstcall;
		bool is_ttbar;
		bool is_data;
		bool dont_split_ttbar;
};

#endif
