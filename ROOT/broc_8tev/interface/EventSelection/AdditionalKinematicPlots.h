#ifndef ADDITIONALKINEMAITCPLOTS_H
#define ADDITIONALKINEMAITCPLOTS_H

#include "Plots.h"
#include "../MorObjects/MPrimaryVertex.h"

#include "Math/VectorUtil.h"
#include "HtCalculator.h"
#include "CutsSet.h"
#include "HandleHolder.h"
#include "CentralServices.h"

class AdditionalKinematicPlots : public Plots{
	public:
		AdditionalKinematicPlots(eire::HandleHolder *handle_holder);
		~AdditionalKinematicPlots();

	private:
		virtual void plot_all();
		virtual void book_histos();

		tionscadaldorcha::EventShapeVariableCalculator *evt_shape;
		broc::ClassificationVariableCalculator *var_calc;
};

#endif
