#ifndef BROC_COLLECTIONSKIMMER_H
#define BROC_COLLECTIONSKIMMER_H

/**
 * \class broc::CollectionSkimmer
 * 
 * \brief Select collections of good jets/muons/etc for a given selection step.
 *
 * Selects for each collection out of all objects that are in the event those
 * objects which pass specified selection cuts.
 *
 * \author klein
 */

#include "CutsSet.h"
#include "HandleHolder.h"

#include "LeptonSelector.h"
#include "JetSelector.h"
#include "METCorrector.h"

//#include "analysers/EventSelection/interface/DoubleCountedLeptonRemover.h"


namespace broc{
	class CollectionSkimmer{
		public:
			CollectionSkimmer(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			~CollectionSkimmer();
			void set_cuts_sets(broc::CutsSet *cuts_set);

			void skim();
		private:
			std::vector<double> *dummy_vec; /// temporary vector to force initialisation to -1 for certain cuts

			eire::HandleHolder *handle_holder;

			LeptonSelector<mor::Electron> *e_selector;
			LeptonSelector<mor::Electron> *jet_uncleaned_e_selector;
			LeptonSelector<mor::Electron> *mm_loose_e_selector;
			LeptonSelector<mor::Muon> *mu_selector;
			LeptonSelector<mor::Muon> *jet_uncleaned_mu_selector;
			LeptonSelector<mor::Electron> *loose_e_selector;
			LeptonSelector<mor::Muon> *loose_mu_selector;
			LeptonSelector<mor::Muon> *mm_loose_mu_selector;
			JetSelector *jet_selector;

			bool do_met_recalc;

			double pvertex_max_z;
			double pvertex_min_ndof;
			double pvertex_max_rho;
			double pvertex_not_fake;

			broc::CutsSet *cuts_set;
			std::vector<mor::PrimaryVertex> *tight_primary_vertices;

			METCorrector *METCor;
	};
}

#endif
