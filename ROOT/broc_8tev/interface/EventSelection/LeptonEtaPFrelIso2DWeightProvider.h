#ifndef BROC_LEPETAPFRELISO2DEVENTWEIGHTPROVIDER_H
#define BROC_LEPETAPFRELISO2DEVENTWEIGHTPROVIDER_H

/** \class broc::LeptonEtaPFrelIso2DWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on eta and PFrelIso of lepton in event.
 *
 * \author klein
 */

#include "LeptonEfficiencyWeightProvider.h"

namespace broc{
	class LeptonEtaPFrelIso2DWeightProvider : public LeptonEfficiencyWeightProvider{
		public:
			LeptonEtaPFrelIso2DWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder);
			~LeptonEtaPFrelIso2DWeightProvider();
		protected:
			virtual double get_value_for_event();
			virtual double get_2nd_value_for_event();
	};
}

#endif
