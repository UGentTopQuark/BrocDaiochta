#ifndef BROC_CENTRAL_SERVICES_H
#define BROC_CENTRAL_SERVICES_H

#include "CutsSet.h"
#include "MassReconstruction.h"
#include "HtCalculator.h"
#include "DiLeptonReconstructor.h"
#include "../TionscadalDorcha/EventShapeVariableCalculator.h"
#include "../TionscadalDorcha/EventClassifier.h"

/** \class broc::CentralServices
 * 
 * \brief Holds time critical calculation methods.
 *
 * To avoid re-calculating complicated quantities the according modules can be
 * accessed centrally through handle holder.
 * 
 * \author klein
 */

// forwards declaration needed because handle holder uses also central services
namespace eire{
	class HandleHolder;
}

namespace tionscadaldorcha{
	class EventClassifier;
}

namespace broc{
	class CentralServices{
		public:
			CentralServices(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			~CentralServices();
			inline HtCalculator *ht_calc(){ return ht_calc_; };
			inline MassReconstruction *mass_reco(){ return mass_reco_;};
			inline DiLeptonReconstructor *dilepton_reco(){ return dilepton_reco_;};
			inline tionscadaldorcha::EventShapeVariableCalculator *evt_shape(){ return evt_shape_;};
			inline tionscadaldorcha::EventClassifier *event_classifier(){ return event_classifier_;};

			void next_event();
		private:
			void book();

			eire::HandleHolder *handle_holder;

			HtCalculator *ht_calc_;
			MassReconstruction *mass_reco_;
			DiLeptonReconstructor *dilepton_reco_;
			tionscadaldorcha::EventShapeVariableCalculator *evt_shape_;
			tionscadaldorcha::EventClassifier *event_classifier_;
	};
}

#endif
