#ifndef PLOTGENERATOR_H
#define PLOTGENERATOR_H

/**
 * \class PlotGenerator
 * 
 * \brief Different submodules are activated here and has the role of the main class for one SelectionStep
 *
 * For each selection step this is bascially the main class. The different
 * sub-modules as TagAndProbe, TriggerStudyManager, etc are activated and
 * deactivated here and booked and managed accordingly.
 * 
 * \warning The classes PlotGenerator and Cuts will in the near future be
 * reorganised. Their role will be restricted to managing plots and applying
 * cuts respectively but object selection and activation as well as booking of
 * sub-modules will be moved to new classes CollectionSelector and
 * SelectionStep
 *
 * \authors walsh, klein
 */

#include "../MorObjects/MMET.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MTrigger.h"
#include "../MorObjects/MTTbarGenEvent.h"
#include "../MorObjects/MPrimaryVertex.h"
#include "../MorObjects/MEventInformation.h"
#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MParticle.h"

#include "MassReconstruction.h"
#include "MassJetMatch.h"
#include "METPlots.h"
#include "SpinPlots.h"
#include "TriggerPlots.h"
#include "LeptonIsolationPlots.h"
#include "BTagPlots.h"
#include "BTagPTRelPlots.h"
#include "MassPlots.h"
#include "ABCDMethodPlots.h"
#include "BasicObjectPlots.h"
#include "XPlusNJetsPlots.h"
#include "CutsSet.h"
#include "TriggerNameProvider.h"
#include "SystematicPlots.h"
#include "QCDMMPlots.h"
#include "AdditionalKinematicPlots.h"

#include <map>

#include "../AnalysisTools/PDFWeightProvider.h"

#include "../../src/ConfigReader/ConfigReader.h"
#include "../EventSelection/HandleHolder.h"
#include "CentralServices.h"

namespace broc{
	class PlotGenerator{
		public:
			PlotGenerator(eire::HandleHolder *handle_holder);
			~PlotGenerator();
			void plot();
	
		private:
			void set_handles(eire::HandleHolder *handle_holder);
	
			eire::ConfigReader *config_reader;
	
			MassReconstruction *mass_reco;
			MassJetMatch *mass_jet_match;
			BJetFinder *bjet_finder;
	
			// plot classes
			SpinPlots *spin_plots;
			METPlots *met_plots;
			MassPlots *mass_plots;
			BasicObjectPlots *basic_plots;
			TriggerPlots *trigger_plots;
			LeptonIsolationPlots *lept_iso_plots;
			BTagPlots *btag_plots;
			BTagPTRelPlots *btag_ptrel_plots;
			ABCDMethodPlots *abcd_plots;
			XPlusNJetsPlots *xplusnjets_plots;
			SystematicPlots *systematic_plots;
			AdditionalKinematicPlots *add_kin_plots;
			broc::CutsSet *cuts_set;
	
			eire::HandleHolder *handle_holder;
	
			QCDMMPlots *mm_plots; //jl 15.07.10
	
			int pdf_weight_pos;
	
			bool plot_spin;
			bool plot_gen_evt_info;
			bool plot_met;
			bool plot_mass;
			bool plot_kinematics;
			bool plot_trigger;
			bool plot_lept_iso;
			bool plot_btag;
			bool plot_ptrel_btag;
			bool plot_abcd;
			bool plot_xplusnjets;
			bool plot_mm;
			bool plot_systematics;
			bool plot_add_kin;
	
			std::string pdf_name;
	
			// to distinguish the plots of multiple instances of the class
			// (muon-channel, electron-channel...)
			std::string id;
			//jl 20.04.11: get measured met everywhere we want to
			std::vector<mor::MET>* my_mets;
			bool do_met_recalc;
	};
}

#endif
