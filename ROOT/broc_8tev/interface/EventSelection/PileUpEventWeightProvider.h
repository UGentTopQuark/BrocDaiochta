#ifndef BROC_PILEUPEVENTWEIGHTPROVIDER_H
#define BROC_PILEUPEVENTWEIGHTPROVIDER_H

/**
 * \class broc::PileUpEventWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up distribution.
 *
 * \author klein
 */

#include "../../src/ConfigReader/ConfigReader.h"
#include "HandleHolder.h"
#include "EventWeightProvider.h"
#include <string>
//#include "../../share/PhysicsTools/Utilities/interface/LumiReweightingStandAlone.h"
#include "../../share/PhysicsTools/Utilities/interface/Lumi3DReWeighting.h"

// Pile up systematics: https://hypernews.cern.ch/HyperNews/CMS/get/physics-validation/1479/3/1/1.html

namespace broc{
	class PileUpEventWeightProvider{
		public:
			PileUpEventWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder);
			~PileUpEventWeightProvider();
			double get_weight();
		protected:
			bool do_not_reweight;
			double systematic_shift;

			edm::Lumi3DReWeighting *reweighter;
			//reweight::PoissonMeanShifter *poisson_mean_shifter;
			std::vector<std::pair<int, double> >* pu_info;
	};
}

#endif
