#ifndef BROC_CROSSTRIGGEREFFICIENCYMANAGER_H
#define BROC_CROSSTRIGGEREFFICIENCYMANAGER_H

#include "JetEfficiencyEstimator.h"
#include "EleEfficiencyEstimator.h"
#include "MuEfficiencyEstimator.h"
#include "TotalCrossTrigEfficiencyEstimator.h"
#include "TFile.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MTrigger.h"
#include "../EventSelection/CutsSet.h"
#include "../EventSelection/HandleHolder.h"


namespace broc{
	class CrossTriggerEfficiencyManager{
		public:
                	  CrossTriggerEfficiencyManager(broc::CutsSet  *cuts_set, eire::HandleHolder *handle_holder);
			~CrossTriggerEfficiencyManager();

			void fill_trees();
		private:
			TFile *ntuple_file;
			broc::JetEfficiencyEstimator *jet_eff_est;
		 	broc::EleEfficiencyEstimator *ele_eff_est;
			broc::MuEfficiencyEstimator *mu_eff_est;
			broc::TotalCrossTrigEfficiencyEstimator<mor::Electron> *tot_eff_el_est;
			broc::TotalCrossTrigEfficiencyEstimator<mor::Muon> *tot_eff_mu_est;
			bool enable_electron;
			bool enable_jets;
			bool enable_muon;
			bool enable_total;

			bool enable_el_leg;
			bool enable_mu_leg;
			bool enable_jet_leg;
			bool enable_total_leg;
			bool jet_one;
			bool jet_two;
			bool jet_three;

	};
}

#endif
