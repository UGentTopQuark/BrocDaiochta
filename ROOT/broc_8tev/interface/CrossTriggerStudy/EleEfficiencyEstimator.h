#ifndef ELEEFFICIENCYESTIMATOR_H
#define ELEEFFICIENCYESTIMATOR_H

#include "../EventSelection/Plots.h"
#include "../EventSelection/Tools.h"
#include "../TagAndProbe/TreeProducer.h"
#include "../EventSelection/LeptonSelector.h"
#include "../EventSelection/TriggerNameProvider.h"

#include "../TopTrigger/TriggerMatcher.h"
#include "../TopTrigger/TriggerObjectSelector.h"
#include "../MorObjects/MPrimaryVertex.h"
#include "../MorObjects/MJet.h"
#include "../EventSelection/CentralServices.h"

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

namespace broc{
	class EleEfficiencyEstimator: public clibisfiosraigh::TreeProducer{
		public:
			EleEfficiencyEstimator(eire::HandleHolder *handle_holder);
			~EleEfficiencyEstimator();

			virtual void book_branches();
			virtual void fill_branches();

		private:
			double min_lep_jet_dR(mor::Electron *electron);
			void fill_probe_branch(mor::Electron &probe_lepton, bool ref, bool cross);
			std::vector<mor::Electron> *lepton;
			std::vector<mor::Jet> *all_jets;
			std::vector<mor::PrimaryVertex> *primary_vertices;
			eire::TriggerNameProvider *trig_name_prov;
			truicear::TriggerMatcher<mor::Jet, mor::TriggerObject> *jet_trigger_matcher;
			truicear::TriggerMatcher<mor::Electron, mor::TriggerObject> *electron_trigger_matcher;

			mutable float pt;
			mutable float eta;
			mutable float sc_eta;
			mutable float sc_abseta;
			mutable float sc_phi;
			mutable float phi;
			mutable int32_t passing_ref;
			mutable int32_t passing_cross_trigger;
			mutable float abseta;
			mutable float njets;
			mutable float npvertices;
			mutable float reliso;
			mutable float pfreliso;
			mutable float ecaliso;
			mutable float hcaliso;
			mutable float trackiso;
			mutable float combcaloiso;
			mutable float mindR;
			mutable float run;
			mutable float lumi_section;
			mutable float event_weight;


			mor::EventInformation *event_information;
			truicear::TriggerObjectSelector *obj_selector;

			int trigger_id;
			int ref_trigger;

			bool passed_ref;
			bool passed_cross;

			HistoWriter *histo_writer;

	};
}

#endif
