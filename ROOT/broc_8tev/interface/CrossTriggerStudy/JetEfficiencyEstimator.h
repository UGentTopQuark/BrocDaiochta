#ifndef BROC_JETEFFICIENCYESTIMATOR_H
#define BROC_JETEFFICIENCYESTIMATOR_H

#include <string>
#include "../TagAndProbe/TreeProducer.h"
#include "../MorObjects/MJet.h"
#include "../EventSelection/HandleHolder.h"
#include "../AnalysisTools/ETH1F.h"
#include "../EventSelection/TriggerNameProvider.h"
#include "../AnalysisTools/HistoWriter.h"
#include "../TopTrigger/TriggerMatcher.h"
#include "../TopTrigger/TriggerObjectSelector.h"

namespace broc{
	class JetEfficiencyEstimator: public clibisfiosraigh::TreeProducer{
		public:
			JetEfficiencyEstimator(eire::HandleHolder *handle_holder);
			~JetEfficiencyEstimator();

			virtual void book_branches();
			virtual void fill_branches();

		private:
			eire::HandleHolder *handle_holder;

			unsigned int jet_id;
			int trigger_id;
			eire::TriggerNameProvider *trig_name_prov;
			truicear::TriggerObjectSelector *trigObj_crosstrig;
			truicear::TriggerMatcher<mor::Jet, mor::TriggerObject> *jet_trigger_matcher;

			std::string id;

			std::vector<mor::Jet> *jets;
			std::vector<mor::PrimaryVertex> *primary_vertices;
			std::vector<mor::TriggerObject> *trig_objects;
			std::vector<mor::Jet> *matched_jets;

			mutable float jet_pt;
			mutable float jet_eta;
			mutable float jet_phi;
			mutable int32_t passing_cross_trigger;
			mutable float jet_abseta;
			mutable float njets;
			mutable float npvertices;
			mutable float run;
			mutable float lumi_section;
			mutable float event_weight;
			mutable float prescale_factor;

			broc::PrescaleProvider *prescale;
			mor::EventInformation *event_information;

			bool passed_cross;

	};
}

#endif
