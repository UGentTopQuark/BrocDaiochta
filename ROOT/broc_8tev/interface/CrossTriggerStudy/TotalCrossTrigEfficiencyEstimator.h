#ifndef TOTALEFFICIENCYESTIMATOR_H
#define TOTALEFFICIENCYESTIMATOR_H

#include "../EventSelection/Plots.h"
#include "../EventSelection/Tools.h"
#include "../TagAndProbe/TreeProducer.h"
#include "../EventSelection/LeptonSelector.h"
#include "../EventSelection/TriggerNameProvider.h"

#include "../TopTrigger/TriggerMatcher.h"
#include "../TopTrigger/TriggerObjectSelector.h"
#include "../MorObjects/MPrimaryVertex.h"
#include "../MorObjects/MJet.h"
#include "../EventSelection/CentralServices.h"

#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

namespace broc{
  template <class ParType1>
	class TotalCrossTrigEfficiencyEstimator: public clibisfiosraigh::TreeProducer{
		public:
    TotalCrossTrigEfficiencyEstimator(eire::HandleHolder *handle_holder, bool el, bool mu, bool jet, bool tot, bool one, bool two, bool three);
			~TotalCrossTrigEfficiencyEstimator();

			virtual void book_branches();
			virtual void fill_branches();

		private:
			double min_lep_jet_dR(ParType1 *lepton);
			void fill_probe_branch(ParType1 &probe_lepton, bool ref, bool cross, bool cross_with_matching);
			std::vector<ParType1>* get_leptons();
			std::vector<ParType1> *lepton;
			std::vector<mor::Jet> *all_jets;
			std::vector<mor::PrimaryVertex> *primary_vertices;
			eire::TriggerNameProvider *trig_name_prov;
			truicear::TriggerMatcher<mor::Jet, mor::TriggerObject> *jet_trigger_matcher;
			truicear::TriggerMatcher<ParType1, mor::TriggerObject> *electron_trigger_matcher;
			
			bool enable_el_leg;
			bool enable_mu_leg;
			bool enable_jet_leg;
			bool jet_one;
			bool jet_two;
			bool jet_three;
			bool enable_total_leg;

			mutable float lep_pt;
			mutable float lep_eta;
			mutable float lep_phi;
			mutable float jet1_pt;
			mutable float jet1_eta;
			mutable float jet1_phi;
			mutable float jet2_pt;
			mutable float jet2_eta;
			mutable float jet2_phi;
			mutable float jet3_pt;
			mutable float jet3_eta;
			mutable float jet3_phi;
			mutable float jet4_pt;
			mutable float jet4_eta;
			mutable float jet4_phi;			
			mutable int32_t passing_ref;
			mutable int32_t passing_cross_trigger;
			mutable int32_t passing_cross_trigger_with_matching;
			mutable float njets;
			mutable float npvertices;
			mutable float run;
			mutable float lumi_section;
			mutable float event_weight;

			mor::EventInformation *event_information;
			truicear::TriggerObjectSelector *obj_selector;

			int trigger_id;
			int ref_trigger;

			bool passed_ref;
			bool passed_cross;

			HistoWriter *histo_writer;

	};
}

#endif
