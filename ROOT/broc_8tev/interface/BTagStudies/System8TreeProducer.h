#ifndef BCLIB_SYSTEM8TREEPRODUCER_H
#define BCLIB_SYSTEM8TREEPRODUCER_H

/***
 * \class System8TreeProducer
 *
 * \brief Outputs tree for b-tagging studies with System 8
 *
 * Fills probe jet information for selected Z->bb events.
 *
 * \author walsh
 ***/

#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/CutsSet.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"

#include "Math/VectorUtil.h"
#include "TFile.h"
#include "TTree.h"



namespace bclib{
	class System8TreeProducer{
		public:
		System8TreeProducer(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
		~System8TreeProducer();
                        void set_handles();

                        void set_outfile(TFile *ntuple_file, std::string directory_name="");
                        void create_tree(std::string tree_name);
			void book_branches();
			void fill_branches();


		private:
			broc::CutsSet *cuts_set;
			eire::HandleHolder *handle_holder;

                        std::vector<mor::Muon> *muons;
                        std::vector<mor::Jet> *jets;

                        mutable float pt;
                        mutable float eta;
                        mutable float phi;
                        mutable float TCHE;
                        mutable float mu_pt_rel;
                        mutable float mu_dR;

                        mutable float away_jet_pt;
                        mutable float away_jet_eta;
                        mutable float away_jet_phi;
                        mutable float away_jet_TCHE;
                        mutable float away_jet_mu_pt_rel;
                        mutable float away_jet_mu_dR;

                        mutable float dR_mu_jet_away_jet;
                        mutable float njets;

                        std::string directory_name;


			TFile *outfile;
                        TTree *tree;

	};
}

#endif
