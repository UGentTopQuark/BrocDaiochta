#!/bin/sh




EXE=/work/kiki-clued0/jbrown/TMVAtest/test/TrainTMVA_newtest.C
#EXE=/work/kiki-clued0/jbrown/TMVAtest/test/TrainTMVA_vjets.C
#EXE=/work/kiki-clued0/jbrown/TMVAtest/test/TrainTMVA_tt.C
#EXE=/work/kiki-clued0/jbrown/TMVAtest/test/TrainTMVA_stop.C

macro=`echo $EXE | awk -F '/' '{print $NF}'`



##p20
if [ "$1" = "p20el2j" ]
    then
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2100/
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2110/

#this one is "default" (last version)
    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2180/

    echo ${INPUT}

elif [ "$1" = "p20el3j" ]
    then
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2101/
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2111/

#this one is "default" (last version)
    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2181/

    echo ${INPUT}

elif [ "$1" = "p20mu2j" ]
    then
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2102/
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2114/
    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2184/
    echo ${INPUT}

elif [ "$1" = "p20mu3j" ]
    then
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2103/
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2115/
    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2185/
    echo ${INPUT}

##p17
elif [ "$1" = "p17el2j" ]
    then
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2104/
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2112/
    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2182/
    echo ${INPUT}

elif [ "$1" = "p17el3j" ]
    then
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2105/
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2113/
    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/je2183/
    echo ${INPUT}

elif [ "$1" = "p17mu2j" ]
    then
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2106/
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2116/
    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2186/
    echo ${INPUT}

elif [ "$1" = "p17mu3j" ]
    then
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2107/
#    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2117/
    INPUT=/work/kiki-clued0/jbrown/p2113_vjets_3.5.0_v1_EMID.NEWJER/wh_cafe/logs/rootfiles/jm2187/
    echo ${INPUT}

fi



NNdirver=$2

TRAINDIR=/work/kiki-clued0/jbrown/TMVAtest/LogTMVATraining_TMVA4



SUBMITDIR=/work/kiki-clued0/jbrown/TMVAtest/test
#launcher=submitFit.perl
launcher=submitWrapper.perl


echo ""


if [ $# -ne 2 ]
then
    echo "1st argument = Specify the channel : <p17/p20><el/mu><2j/3j> , for example p17mu3j"
    echo "2nd argument = Specify the output directory name specified in TRAINDIR internal variable"
    exit 1
fi


cd $TRAINDIR

testdir=${NNdirver}

if [ ! -d ${testdir} ] 
    then
    echo "Training version $testdir doen't exists , creating it"
    mkdir ${TRAINDIR}/${testdir}
else
    echo " ${testdir} exists, I WILL NOT OVERWRITE IT"
    exit 1
fi



cd ${NNdirver}

#for btag in ST DT  ; do
for btag in  DT  ; do

    mkdir -p ${btag}
    cd ${btag}
#    for mass in  100 105 110 115 120 125 130 135 140 145 150 ; do
    for mass in   115  ; do

	mkdir -p ${mass}
	cd ${mass}
	where=`pwd`


	
	methodname=${mass}${btag}

	echo "training BDT : "${btag}" "${mass}


	argtrain=${NNdirver}-${mass}${btag}


	echo =====================================
	echo
	echo '${EXE}' ${EXE}
	echo '${argtrain}' ${argtrain} 
	echo '${where}'  ${where} 
	echo '${btag}' ${btag} 
	echo '${mass}' ${mass} 
	echo '${methodname}' ${methodname}
	echo 'channel' $1
	echo
	echo =====================================
	

	cp ${SUBMITDIR}/Wrapper.C.newtemplate ${SUBMITDIR}/Wrapper.C

#	sed -i "s#MYTMVAMACRO#${EXE}#g" ${SUBMITDIR}/Wrapper.C
	sed -i "s#MYTMVAMACRO#${macro}#g" ${SUBMITDIR}/Wrapper.C
	sed -i "s#MASS#${mass}#g" ${SUBMITDIR}/Wrapper.C
	sed -i "s#BTAG#${btag}#g" ${SUBMITDIR}/Wrapper.C
	sed -i "s#BASEDIR#${INPUT}#g" ${SUBMITDIR}/Wrapper.C
	sed -i "s#CHANNEL#${1}#g" ${SUBMITDIR}/Wrapper.C


	cp  ${SUBMITDIR}/Wrapper.C .
	cp ${EXE} .
	ln -s /work/kiki-clued0/jbrown/TMVAtest/test/TMVAGui.C  .
	ln -s /work/kiki-clued0/jbrown/TMVAtest/test/tmvaglob.C .

#	${SUBMITDIR}/${launcher} Wrapper.C ${argtrain} ${where} ${mass} ${btag}
	

##        ## just for test
	root -l -b -q Wrapper.C


	echo"" 

	cd ..
    done
    cd ..
done
cd ..

