#!/usr/bin/perl

use threads;
use Getopt::Std;
use Thread::Semaphore;

getopts("j:");

die "usage: $0 <output_directory>\n" unless(@ARGV > 0);

$output_directory = $ARGV[0];
$output_directory .= "/" unless($output_directory =~ m/\/$/);

$nthreads = defined($opt_j) ? $opt_j : 8;

$summary_files = get_files("(.*)_\\d+_summary.txt", $output_directory);
$root_files = get_files("(.*)_plots_\\d+\\.root", $output_directory);
$tree_files = get_files("trigger_tag_and_probe_(.*)_\\d+\\.root", $output_directory);
$elecrosstrigger_tree_files = get_files("EleCrossTrigger_(.*)_\\d+\\.root", $output_directory);
$totalcrosstrigger_tree_files = get_files("TotalCrossTrigger_(.*)_\\d+\\.root", $output_directory);
$jetcrosstrigger_tree_files = get_files("JetCrossTrigger_(.*)_\\d+\\.root", $output_directory);
$hitfit_tree_files = get_files("HitFit_(.*)_\\d+\\.root", $output_directory);
$btag_tree_files = get_files("btag_system8_(.*)_\\d+\\.root", $output_directory);
$flat_tree_files = get_files("flat_tree_(.*)_\\d+\\.root", $output_directory);

print "Are you sure you want to HARVEST [no]?\n";
my $answer = <STDIN>;
exit if($answer !~ /y|Y/);

my @threads = ();

my $semaphore = Thread::Semaphore->new($nthreads);
while(my ($dataset, $files) = each %{$summary_files}){
	if(@{$root_files->{"$dataset"}} > 0){
		my $root_file_list = join(" $output_directory", @{$root_files->{"$dataset"}});
		my $root_file_list = $output_directory.$root_file_list;
		my $outfile = "${dataset}_plots.root";
		$outfile =~ s/Data_.*_plots\.root/Data_plots.root/ if($outfile =~ m/Data.*_plots\.root/);

		my $thread = threads->create('do_hadd', $output_directory, $dataset, $outfile, $root_file_list, $semaphore);
		push(@threads, $thread);
	}
}

foreach my $type (keys %$tree_files){
	if(@{$tree_files->{"$type"}} > 0){
		my $root_file_list = join(" $output_directory", @{$tree_files->{"$type"}});
		my $root_file_list = $output_directory.$root_file_list;
		my $outfile = "trigger_tag_and_probe_${type}.root";

		my $thread = threads->create('do_hadd', $output_directory, $type, $outfile, $root_file_list, $semaphore);
		push(@threads, $thread);
	}
}

foreach my $type (keys %$elecrosstrigger_tree_files){
    if(@{$elecrosstrigger_tree_files->{"$type"}} > 0){
	my $root_file_list = join(" $output_directory", @{$elecrosstrigger_tree_files->{"$type"}});
	my $root_file_list = $output_directory.$root_file_list;
	my $outfile = "EleCrossTrigger_${type}.root";

	my $thread = threads->create('do_hadd', $output_directory, $type, $outfile, $root_file_list, $semaphore);
	push(@threads, $thread);
    }
}

foreach my $type (keys %$totalcrosstrigger_tree_files){
    if(@{$totalcrosstrigger_tree_files->{"$type"}} > 0){
        my $root_file_list = join(" $output_directory", @{$totalcrosstrigger_tree_files->{"$type"}});
        my $root_file_list = $output_directory.$root_file_list;
        my $outfile = "TotalCrossTrigger_${type}.root";

        my $thread = threads->create('do_hadd', $output_directory, $type, $outfile, $root_file_list, $semaphore);
        push(@threads, $thread);
    }
}

foreach my $type (keys %$jetcrosstrigger_tree_files){
    if(@{$jetcrosstrigger_tree_files->{"$type"}} > 0){
	my $root_file_list = join(" $output_directory", @{$jetcrosstrigger_tree_files->{"$type"}});
	my $root_file_list = $output_directory.$root_file_list;
	my $outfile = "JetCrossTrigger_${type}.root";

	my $thread = threads->create('do_hadd', $output_directory, $type, $outfile, $root_file_list, $semaphore);
	push(@threads, $thread);
    }
}

foreach my $type (keys %$hitfit_tree_files){
    if(@{$hitfit_tree_files->{"$type"}} > 0){
	my $root_file_list = join(" $output_directory", @{$hitfit_tree_files->{"$type"}});
	my $root_file_list = $output_directory.$root_file_list;
	my $outfile = "HitFit_${type}.root";

	my $thread = threads->create('do_hadd', $output_directory, $type, $outfile, $root_file_list, $semaphore);
	push(@threads, $thread);
    }
}

foreach my $type (keys %$btag_tree_files){
	if(@{$btag_tree_files->{"$type"}} > 0){
		my $root_file_list = join(" $output_directory", @{$btag_tree_files->{"$type"}});
		my $root_file_list = $output_directory.$root_file_list;
		my $outfile = "btag_system8_${type}.root";

		my $thread = threads->create('do_hadd', $output_directory, $type, $outfile, $root_file_list, $semaphore);
		push(@threads, $thread);
	}
}

foreach my $type (keys %$flat_tree_files){
	if(@{$flat_tree_files->{"$type"}} > 0){
		my $root_file_list = join(" $output_directory", @{$flat_tree_files->{"$type"}});
		my $root_file_list = $output_directory.$root_file_list;
		my $outfile = "flat_tree_${type}.root";

		my $thread = threads->create('do_hadd', $output_directory, $type, $outfile, $root_file_list, $semaphore);
		push(@threads, $thread);
	}
}

while(my ($dataset, $files) = each %{$summary_files}){
	my $outfile = "${dataset}.txt";

	my $thread = threads->create('combine_files', $output_directory, $outfile, $dataset);
	push(@threads, $thread);
}

foreach my $thread (@threads){
	$thread->join();
}

print "Cleanup output directory [no]?\n";
my $answer = <STDIN>;
if($answer =~ /y|Y/){
	while(my ($dataset, $files) = each %{$summary_files}){
		foreach $file (@{$files}){
			`rm $output_directory/$file`;
		}
	}
	while(my ($dataset, $files) = each %{$root_files}){
		foreach $file (@{$files}){
			`rm $output_directory/$file`;
		}
	}
	while(my ($dataset, $files) = each %{$tree_files}){
		foreach $file (@{$files}){
			`rm $output_directory/$file`;
		}
	}
	while(my ($dataset, $files) = each %{$elecrosstrigger_tree_files}){
	    foreach $file (@{$files}){
		`rm $output_directory/$file`;
	    }
	}
	while(my ($dataset, $files) = each %{$totalcrosstrigger_tree_files}){
                foreach $file (@{$files}){
	        `rm $output_directory/$file`;
                }
            }
	while(my ($dataset, $files) = each %{$jetcrosstrigger_tree_files}){
	    foreach $file (@{$files}){
		`rm $output_directory/$file`;
	    }
	}
	while(my ($dataset, $files) = each %{$hitfit_tree_files}){
	    foreach $file (@{$files}){
		`rm $output_directory/$file`;
	    }
	}
	while(my ($dataset, $files) = each %{$btag_tree_files}){
		foreach $file (@{$files}){
			`rm $output_directory/$file`;
		}
	}
	while(my ($dataset, $files) = each %{$flat_tree_files}){
		foreach $file (@{$files}){
			`rm $output_directory/$file`;
		}
	}
	`rm $output_directory/submission*.sh`;
	`rm $output_directory/*input_files_*.txt`;
	`rm $output_directory/*input_remote_files_*.txt`;
	`rm $output_directory/*.cfg`;
	`rm $output_directory/broc_draiochta`;
	`rm $output_directory/*_job_ids.txt`;
	`rm $output_directory/*.ER`;
	`rm $output_directory/*.OU`;
}

sub do_hadd
{
	my ($output_directory, $dataset, $outfile, $root_file_list, $semaphore) = @_;

	$semaphore->down();
	my $command = "hadd -f $output_directory/$outfile $root_file_list";
	print "$command\n";
	`$command`;
	$semaphore->up();
}

sub get_files
{
	my ($regex, $directory) = @_;

	my %files = ();

	opendir(DIR, $directory) or die "can't open directory: $directory $!";
	while(defined(my $file = readdir(DIR))){
		push(@{$files{$1}}, $file) if($file =~ /$regex/);	
	}
	closedir(DIR);

	return \%files;
}

sub combine_files
{
	my ($dirname, $outfile, $dataset) = @_;
	
	my $pattern = '\=\-\-\-\-\-\-';
	my $pattern2 = '\=\+\+\+\+\+\+';
	
	my @values_to_average = ();
	push(@values_to_average, "b tag efficiency");
	push(@values_to_average, "b tag purity");
	push(@values_to_average, "b tag eff times purity");
	push(@values_to_average, "eff HLT_Mu15");
	push(@values_to_average, "eff HLT_Mu11");
	push(@values_to_average, "eff HLT_e15_LW_L1R");
	
	my @values_to_sum = ();
	push(@values_to_sum, "Nsel_trig HLT_Mu15");
	push(@values_to_sum, "Nsel_trig HLT_Mu11");
	push(@values_to_sum, "Nsel_trig HLT_e15_LW_L1R");
	push(@values_to_sum, "cuts_passed");
	push(@values_to_sum, "cuts_not_passed");
	push(@values_to_sum, "ttbar_mu_events");
	push(@values_to_sum, "ttbar_e_events");
	push(@values_to_sum, "ttbar_bg_events");
	push(@values_to_sum, "data_events");
	
	#########################################
	#	DO NOT EDIT BEYOND THIS LINE
	#########################################
	
	$nfiles = 0;
	
	push(@values_to_sum, @values_to_average);
	
	opendir(DIR, $dirname);
	my @files = ();
	while((defined(my $file = readdir(DIR)))){
		next unless($file =~ m/${dataset}_\d+_summary\.txt/);
		$file = $dirname.'/'.$file;
		push(@files, $file)if(-e $file);
		$nfiles++;
	}
	
	# seperate one of the files and scroll down the handle to the pattern
	my $first_file = pop @files;
	open(FFILE, "<$first_file");
	open(OUTFILE,">$output_directory/$outfile");
	while(defined(my $fline = <FFILE>)){
		if($fline =~ m/^$pattern/){
			print OUTFILE $fline;
			last;	
		}
	}
	
	# open all other files and save handles
	my @fhandles = ();
	my $i = 0;
	foreach my $file (@files){
		print "file: $file\n";
		open($fhandles[$i], "<$file");
		$i++;
	}
	
	# scroll all other files down...
	print "adjusting files";
	foreach my $handle(@fhandles){
		print ".";
		while(defined(my $line = <$handle>)){
		        last if($line =~ m/^$pattern/);
		}
	}
	print "\n";
	
	print "combining files";
	while(defined(my $fline = <FFILE>)){
		my $cutname, $first_cutvalue;
		if($fline =~ m/^([\w_ ]+):\s+([-\d\.:]+)/ ||
			 $fline =~ m/^([\w_]+):\s+([\w\._]+)/)
		{
			$cutname = $1;
			$first_cutvalue = $2;
		}
		else{
			$cutname = "";
			$first_cutvalue = -1;
		}
	
		my $line_already_printed=0;
	
		foreach $val(@values_to_sum){
			if($cutname eq $val){
				my $cuts_passed = $first_cutvalue;
				foreach my $handle(@fhandles){
					my $line = <$handle>;
					$line =~ m/^$val:\s+([\d\.]+)/;
					$cuts_passed += $1 unless($1 == -1);	
				}
				my $average = $cuts_passed / $nfiles;
				my $print_average=0;
				foreach $av_cut (@values_to_average){
					if($av_cut eq $cutname && $first_cutvalue != -1){
						$print_average=1;
					}
				}
								
				print OUTFILE "$cutname: $average\n" if($print_average == 1);
				print OUTFILE "$cutname: $cuts_passed\n" unless($print_average == 1);
	
				$line_already_printed=1;
	
				last;
			}
		}
	
		print OUTFILE $fline unless($line_already_printed);
		unless($line_already_printed){
			foreach my $handle(@fhandles){
				my $tmp = <$handle>;
			}
		}
		print ".";
	}
	
	print "\n";
	
	# close all files
	foreach my $handle(@fhandles){
		close($handle);
	}
	close(OUTFILE);
}
