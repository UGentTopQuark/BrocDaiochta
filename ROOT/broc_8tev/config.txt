[global]
dataset = TTbar
input_files = input.txt
output_directory = .
max_events = 10000
#set object_type to 0 to get collection "jets" and "mets"
#set to 1 to get "pfjets" and "pfTypeImet"
#1 for 2010, 0 for 2011
object_type = 0
#recompute_met: True to assume mW and calculate pz(nu), false
recompute_met = false
#list of cuts the code recognises
allowed_cuts_file = share/allowed_cuts.txt
allowed_vcuts_file = share/allowed_vcuts.txt
JEC_dir = share/JEC/
#leave the following two empty not to apply JEC on the fly share/CondFormats/JetMETObjects/data/
JEC = START42_V17_AK5PFchs_L1FastJet.txt:START42_V17_AK5PFchs_L2Relative.txt:START42_V17_AK5PFchs_L3Absolute.txt
JEC_uncertainties = START42_V17_AK5PFchs_Uncertainty.txt
outfile_suffix =  

enable_mva_module = true
enable_tag_and_probe_module = false
enable_plots_module = true

[plots]
plot_met = true
plot_mass = true
plot_kinematics = true
plot_trigger = false
plot_lept_iso = true
plot_btag = false
plot_abcd = false
plot_xplusnjets = false
plot_mm = false


#[MVA]
#method_name = BDT
#weights_file = VERS-TrainTMVA_RF.weights.txt
#classify_events = true
#create_tree = false
[MVA]
method_name = NeuroBayes
weights_file=weights/TMVAClassification_NeuroBayes_electron.weights.xml
classify_events = false
create_tree = false

[event_weights]
pdf_weights = false
pu_weights = true 
3d_pu_weights_file = share/Weight3D_1p0.root
3d_pu_weights_file_sys_up = share/Weight3D_1p08.root
3d_pu_weights_file_sys_down = share/Weight3D_0p92.root
lep_eff_weights = true
process_electron_efficiencies = false
process_muon_efficiencies = true
jet_eff_weights = false
eta_weights_file = share/eta_scale_factors_muon_idx.txt
pt_weights_file = share/pt_scale_factors_muon_id.txt
