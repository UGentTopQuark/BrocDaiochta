#!/bin/bash

#---------------------------------------------
#		SET VARIABLES
era="2012" #2010 or 2011pre/post
runwhat="both" #data for data, mc for mc, both for both
pbspath="/localgrid/jlellouc/jobs_broc-draiochta" #whence jobs are submitted
pathtorel="/localgrid/jlellouc/CMS/prod/CMSSW_5_0_1_patch3/" #path to release on localgrid for setups
boost="/localgrid/jlellouc/boost_145/" #path where the BOOST libray is located
##boost="/localgrid/bklein/software/boost_1_44/lib"
runQCDMC="0" #run over the QCD MC? Not needed if using the MM
walltime="00:30:00"
#---------------------------------------------


if  [ "$1" = ''  -o  "$2" = ''  -o  "$3" = ''  -o  "$4" = '' ] 
then
	echo
	echo './Run.sh <channel> <version> <action> <filename>'
	echo
	exit 1
fi

cha="$1"
ver="$2"
act="$3"
fil="$4"

if [ "$era" = '2010' ]
then
	indirm="/localgrid/jlellouc/jobs_broc-draiochta/input_387_met" #where mc files are located
	if [ "$cha" = 'e' ] 
	then
		indird="/localgrid/jlellouc/jobs_broc-draiochta/input_387_met/ELECTRON_v2" #where files are located
		json="/localgrid/jlellouc/jobs_broc-draiochta/input_387_met/JSON/Data_EM.json"
		nd="66"  #number of data jobs for electron
		nw="49" #nW for nn studies
		nz="9" #number of DY50 jobs
		nt="24"  #number of tt jobs
		nqcd="7" #number of muon QCD jobs
	else
		indird="/localgrid/jlellouc/jobs_broc-draiochta/input_387_met/MUON_v2" #where files are located
		json="/localgrid/jlellouc/jobs_broc-draiochta/input_387_met/JSON/Data_mu.json"
                nd="45"  #number of data jobs for muon
                nw="49" #nW for nn studies
                nz="9" #number of DY50 jobs
                nt="24"  #number of tt jobs
                nqcd="7" #number of muon QCD jobs
	fi
elif [ "$era" = '2011' ]
then
        if [ "$cha" = 'e' ]
        then
		indird="/nasmount/Gent/bklein/ntuples/20111022_eHad_v7"
		indirm="/nasmount/Gent/bklein/ntuples/20111022_Summer11_MultiSkim_v11"
		json="/user/jlellouc/CMS/ttAnalysis/broc_8tev/json2cpp/good_2011_eHad.json"
	        nd="63"  #number of data jobs for electron
        	nw="64" #nW for nn studies
	        nz="61" #number of DY50 jobs
        	nt="8"  #number of tt jobs
		ngj="10" #Gamma+jets
	else
		indird="/nasmount/Gent/bklein/ntuples/20111022_singlemu_v6"
		indirm="/nasmount/Gent/bklein/ntuples/20111022_Summer11_MultiSkim_v11"
		json="/user/jlellouc/CMS/ttAnalysis/broc_8tev/json2cpp/20111111_single_mu_full_2011.txt"
		nd="426"
		nw="30"
		nz="60"
		nt="19"
		nqcd="259"
	fi
elif [ "$era" = '2012' ]
then
        if [ "$cha" = 'e' ]
        then 
                indird="/nasmount/Gent/mccartin/ntuples/TTJets_Summer12/"
                indirm="/nasmount/Gent/mccartin/ntuples/TTJets_Summer12/"
                json=""
                nd="63"  #number of data jobs for electron
                nw="64" #nW for nn studies
                nz="61" #number of DY50 jobs
                nt="10"  #number of tt jobs
                ngj="10" #Gamma+jets
        else
                indird="/nasmount/Gent/mccartin/ntuples/TTJets_Summer12/"
                indirm="/nasmount/Gent/mccartin/ntuples/TTJets_Summer12/"
                json=""
                nd="426"
                nw="30"
                nz="60"
                nt="19"
                nqcd="259"
        fi 
else
	echo
	echo 'Era '$era' does not exist, please choose 2010, 2011 or 2012'
	echo
	exit 1
fi


echo
echo "--------------------------RUN.SH--------------------------"
echo 'Era             '$era''
echo 'Channel         '$cha''
echo 'Version         '$ver''
echo 'Action          '$act''
echo 'Cut file        '$fil''
echo 'Process         '$runwhat''
echo 'Data files in   '$indird''
echo 'MC files in     '$indirm''
echo 'JSON file       '$json''
echo 'Submit from     '$pbspath''
echo 'Release info in '$pathtorel''
echo 'BOOST lib in    '$boost''
echo 'RunQCDMC        '$runQCDMC''
#echo 'Objects type    '$objtype''
#echo 'MET type        '$met''
echo 'Data  jobs      '$nd''
echo 'Wjets jobs      '$nw''
echo 'DY50  jobs      '$nz''
echo 'TTbar jobs      '$nt''
echo 'MuQCD jobs      '$nqcd''
echo "----------------------------------------------------------"  

do_fake_e()
{
# fake
cp cutset_generator/${fil}_e_low.txt ../output/$ver-${fil}_e_low.txt
cp cutset_generator/${fil}_e_wj.txt ../output/$ver-${fil}_e_wj.txt
cd cutset_generator/ ; ./change_cuts.pl ${fil}_e_low.txt ; cd .. 
cd json2cpp/ ; ./json2cpp.pl $json ; cd .. ; make -j
mv broc_draiochta $ver-flat-Data
# eff
cd cutset_generator/ ; ./change_cuts.pl  ${fil}_e_wj.txt ; cd .. 
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make -j 
mv broc_draiochta $ver-flat-Wjets
# W/Z contamination
cd cutset_generator/ ; ./change_cuts.pl  ${fil}_e_low.txt ; cd ..  
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make -j 
mv broc_draiochta $ver-flat-Wjets_wc
cd cutset_generator/ ; ./change_cuts.pl  ${fil}_e_low.txt ; cd ..  
cd json2cpp/ ; ./json2cpp.pl; cd .. ; make -j 
mv broc_draiochta $ver-flat-Zjets_wc
# move
mv $ver-flat* $pbspath/.
cp pbs_scripts/*.sh $pbspath/.
cp -r share $pbspath/$ver-share
cp weights_use/TrainTMVA_RF.weights.txt $pbspath/$ver-TrainTMVA_RF.weights.txt
cp -r weights $pbspath/$ver-weights
ndatafil=`ls $indird/Data_* | wc -l`
fperj=`echo 'scale=0;'$ndatafil' / '$nd'' | bc`
remainder=`echo 'scale=0; '$ndatafil' % '$nd'' | bc`	
nwfil=`ls $indirm/W* | wc -l`
fperjw=`echo 'scale=0;'$nwfil' / '$nw'' | bc`
remainw=`echo 'scale=0; '$nwfil' % '$nw'' | bc`
nzfil=`ls $indirm/Z* | wc -l`
fperjz=`echo 'scale=0;'$nzfil' / '$nz'' | bc`
remainz=`echo 'scale=0; '$nzfil' % '$nz'' | bc`
echo
echo 'There are '$ndatafil' data files and you requested '$nd' jobs; fperj is '$fperj' and the remainder is '$remainder' '
echo 'There are '$nwfil' W files and you requested '$nw' jobs; fperj is '$fperjw' and the remainder is '$remainw' '
echo 'There are '$nzfil' Z files and you requested '$nz' jobs; fperj is '$fperjz' and the remainder is '$remainz' '
echo
cd $pbspath
end=`echo ''$nd' + 1' | bc`
h="$fperj"
for (( c=1 ; c<=$end ; c++ ))
do
	if [ "$c" == "1" ]
	then
		t="999"
	elif [ "$c" == "$end" ]
	then
		t="$remainder"
		h="-0"
	else
		t="$fperj"
	fi
	cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Data/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/fake$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/ | sed s/ISDATA/1/ | sed 's|MYINDIR|'$indird'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_fake$c.sh
	qsub -e $ver-fake$c.err -o $ver-fake$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_fake$c.sh ;  sleep 0.05
	h=`echo ''$h' + '$fperj'' | bc`
done
end=`echo ''$nw' + 1' | bc`
h="$fperjw"
for (( c=1 ; c<=$end ; c++ ))
do
        if [ "$c" == "1" ]
        then
                t="999"
        elif [ "$c" == "$end" ]
        then
                t="$remainw"
                h="-0"
        else
                t="$fperjw"
        fi
        cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Wjets/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/eff$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_eff$c.sh
        qsub -e $ver-eff$c.err -o $ver-eff$c.out  -l walltime=$walltime -q localgrid@cream01 s_${ver}_eff$c.sh ;  sleep 0.05
        cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Wjets/    | sed s/SCRIPT/muwc_pbs/ | sed s/PROCESS/wconta$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_wconta$c.sh
        qsub -e $ver-wconta$c.err -o $ver-wconta$c.out  -l walltime=$walltime -q localgrid@cream01 s_${ver}_wconta$c.sh ;  sleep 0.05
        h=`echo ''$h' + '$fperjw'' | bc`
done
end=`echo ''$nz' + 1' | bc`
h="$fperjz" 
for (( c=1 ; c<=$end ; c++ ))
do 
        if [ "$c" == "1" ]
        then 
                t="999"
        elif [ "$c" == "$end" ]
        then 
                t="$remainz"
                h="-0"
        else
                t="$fperjz"
        fi 
        cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Zjets/    | sed s/SCRIPT/muwc_pbs/ | sed s/PROCESS/zconta$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_zconta$c.sh
        qsub -e $ver-zconta$c.err -o $ver-zconta$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_zconta$c.sh ;  sleep 0.05
        h=`echo ''$h' + '$fperjz'' | bc`
done
}

do_run_e()
{
#because of json junk we need to have one executable per job
cd cutset_generator/ ; ./change_cuts.pl  ${fil}_e.txt ; cd ..
cp cutset_generator/${fil}_e.txt ../output/$ver-${fil}_e.txt
./create_exe_el.sh $ver $json
mv $ver-flat* $pbspath/.
cp pbs_scripts/*.sh $pbspath/.
cp -r share $pbspath/$ver-share
cp weights_use/TrainTMVA_RF.weights.txt $pbspath/$ver-TrainTMVA_RF.weights.txt
cp -r weights $pbspath/$ver-weights
ndatafil=`ls $indird/Data_* | wc -l`
fperj=`echo 'scale=0;'$ndatafil' / '$nd'' | bc`
remainder=`echo 'scale=0; '$ndatafil' % '$nd'' | bc`	
nwfil=`ls $indirm/W* | wc -l`
fperjw=`echo 'scale=0;'$nwfil' / '$nw'' | bc`
remainw=`echo 'scale=0; '$nwfil' % '$nw'' | bc`
nzfil=`ls $indirm/Zjet* | wc -l`
fperjz=`echo 'scale=0;'$nzfil' / '$nz'' | bc`
remainz=`echo 'scale=0; '$nzfil' % '$nz'' | bc`
nttfil=`ls $indirm/TT* | wc -l`
fperjtt=`echo 'scale=0;'$nttfil' / '$nt'' | bc`
remaintt=`echo 'scale=0; '$nttfil' % '$nt'' | bc`
ngjfil=`ls $indirm/GJ* | wc -l`
fperjgj=`echo 'scale=0;'$ngjfil' / '$ngj'' | bc`
remaingj=`echo 'scale=0; '$ngjfil' % '$ngj'' | bc`
#qcd
nemeafil=`ls $indirm/EQCD_EM_20* | wc -l`
fperjemea=`echo 'scale=0;'$nemeafil' / 100' | bc`
remainemea=`echo 'scale=0; '$nemeafil' % 100' | bc`
nemebfil=`ls $indirm/EQCD_EM_30* | wc -l`
fperjemeb=`echo 'scale=0;'$nemebfil' / 200' | bc`
remainemeb=`echo 'scale=0; '$nemebfil' % 200' | bc`
nemecfil=`ls $indirm/EQCD_EM_80* | wc -l`
fperjemec=`echo 'scale=0;'$nemecfil' / 50' | bc`
remainemec=`echo 'scale=0; '$nemecfil' % 50' | bc`

nbcafil=`ls $indirm/EQCD_BC2E_20* | wc -l`
fperjbca=`echo 'scale=0;'$nbcafil' / 17' | bc`
remainbca=`echo 'scale=0; '$nbcafil' % 17' | bc`
nbcbfil=`ls $indirm/EQCD_BC2E_30* | wc -l`
fperjbcb=`echo 'scale=0;'$nbcbfil' / 16' | bc`
remainbcb=`echo 'scale=0; '$nbcbfil' % 16' | bc`
nbccfil=`ls $indirm/EQCD_BC2E_80* | wc -l`
fperjbcc=`echo 'scale=0;'$nbccfil' / 6' | bc`
remainbcc=`echo 'scale=0; '$nbccfil' % 6' | bc`

echo
echo 'There are '$ndatafil' data files and you requested '$nd' jobs; fperj is '$fperj' and the remainder is '$remainder' '
echo 'There are '$nwfil' W files and you requested '$nw' jobs; fperj is '$fperjw' and the remainder is '$remainw' '
echo 'There are '$nzfil' Z files and you requested '$nz' jobs; fperj is '$fperjz' and the remainder is '$remainz' '
echo 'There are '$ngjfil' gamma files and you requested '$ngj' jobs; fperj is '$fperjgj' and the remainder is '$remaingj' '
echo 'There are '$nttfil' tt files and you requested '$nt' jobs; fperj is '$fperjtt' and the remainder is '$remaintt' '
echo 'There are '$nemeafil' QCD_EM_20* files and you requested 100 jobs; fperj is '$fperjemea' and the remainder is '$remainemea' '
echo 'There are '$nemebfil' QCD_EM_30* files and you requested 200 jobs; fperj is '$fperjemeb' and the remainder is '$remainemeb' '
echo 'There are '$nemecfil' QCD_EM_80* files and you requested 50 jobs; fperj is '$fperjemec' and the remainder is '$remainemec' '
echo 'There are '$nbcafil' QCD_BC_20* files and you requested 17 jobs; fperj is '$fperjbca' and the remainder is '$remainbca' ' 
echo 'There are '$nbcbfil' QCD_BC_30* files and you requested 16 jobs; fperj is '$fperjbcb' and the remainder is '$remainbcb' ' 
echo 'There are '$nbccfil' QCD_BC_80* files and you requested 6 jobs; fperj is '$fperjbcc' and the remainder is '$remainbcc' ' 
echo
cd $pbspath
#DATA
if [ $runwhat = 'both' -o $runwhat = 'data' ]
then
	end=`echo ''$nd' + 1' | bc`
	h="$fperj"
	for (( c=1 ; c<=$end ; c++ ))
	do
		if [ "$c" == "1" ]
		then
			t="999"
		elif [ "$c" == "$end" ]
		then
			t="$remainder"
			h="-0"
		else
			t="$fperj"
		fi
		cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Data/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/datae$c/ |  sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/   | sed s/ISDATA/1/ | sed 's|MYINDIR|'$indird'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|' > s_${ver}_Datae$c.sh
		qsub -e $ver-data$c.err -o $ver-data$c.out  -l walltime=$walltime -q localgrid@cream01 s_${ver}_Datae$c.sh ;  sleep 0.05
		h=`echo ''$h' + '$fperj'' | bc`
	done
fi
#MC
if [ $runwhat = 'both' -o $runwhat = 'mc' ]
then
      	end=`echo ''$nw' + 1' | bc`
      	h="$fperjw"
      	for (( c=1 ; c<=$end ; c++ ))
     	do
                if [ "$c" == "1" ]
        	then
                	t="999"
                elif [ "$c" == "$end" ]
                then
	                t="$remainw"
        	        h="-0"
                else
                        t="$fperjw"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Wjets/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/wje$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_wje$c.sh
                qsub -e $ver-wj$c.err -o $ver-wj$c.out  -l walltime=$walltime -q localgrid@cream01 s_${ver}_wje$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjw'' | bc`
        done
	end=`echo ''$nt' + 1' | bc`
	h="$fperjtt"
	for (( c=1 ; c<=$end ; c++ ))
        do	
                if [ "$c" == "1" ]
                then
                        t="999"
                elif [ "$c" == "$end" ]
                then
                        t="$remaintt"
                        h="-0"
                else
                        t="$fperjtt"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/TTbar/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/tte$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_tte$c.sh
                qsub -e $ver-tt$c.err -o $ver-tt$c.out  -l walltime=$walltime -q localgrid@cream01 s_${ver}_tte$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjtt'' | bc`
        done
        end=`echo ''$nz' + 1' | bc`
        h="$fperjz"
        for (( c=1 ; c<=$end ; c++ ))
        do
                if [ "$c" == "1" ]
                then
                        t="999"
                elif [ "$c" == "$end" ]
                then
                        t="$remainz"
                        h="-0"
                else
                        t="$fperjz"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Zjets/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/zje$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_zje$c.sh
                qsub -e $ver-zj$c.err -o $ver-zj$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_zje$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjz'' | bc`
        done
        end=`echo ''$ngj' + 1' | bc`
        h="$fperjgj"
        for (( c=1 ; c<=$end ; c++ ))
        do 
                if [ "$c" == "1" ]
                then 
                        t="999"
                elif [ "$c" == "$end" ]
                then 
                        t="$remaingj"
                        h="-0"
                else
                        t="$fperjgj"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Gjets/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/gje$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_gje$c.sh
                qsub -e $ver-gj$c.err -o $ver-gj$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_gje$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjgj'' | bc`
        done


	cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/TsChan/  | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/stse/    | sed -e 's/HEAD/\-0/' | sed s/TAIL/999/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_stse.sh
	cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/TtChan/  | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/stte/    | sed -e 's/HEAD/\-0/' | sed s/TAIL/999/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_stte.sh
	cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/TtWChan/ | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/sttwe/   | sed -e 's/HEAD/\-0/' | sed s/TAIL/999/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_sttwe.sh

       	qsub -e $ver-sts.err -o $ver-sts.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_stse.sh ;  sleep 0.05
        qsub -e $ver-stt.err -o $ver-stt.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_stte.sh ;  sleep 0.05
       	qsub -e $ver-sttw.err -o $ver-sttw.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_sttwe.sh 
       	
       	if [ "$runQCDMC" == "1" ]
       	then
       	end=`echo '100 + 1' | bc`
        h="$fperjemea"
        for (( c=1 ; c<=$end ; c++ ))
        do  
                if [ "$c" == "1" ]
                then
                        t="999"
                elif [ "$c" == "$end" ]
                then  
                        t="$remainemea"
                        h="-0"
                else
                        t="$fperjemea"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/EQCD_EM_2030/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/em_2030$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_em2030$c.sh
                qsub -e $ver-em2030$c.err -o $ver-em2030$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_em2030$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjemea'' | bc`
        done	

        end=`echo '200 + 1' | bc`
        h="$fperjemeb"
        for (( c=1 ; c<=$end ; c++ ))
        do   
                if [ "$c" == "1" ]
                then 
                        t="999"
                elif [ "$c" == "$end" ]
                then  
                        t="$remainemeb"
                        h="-0" 
                else 
                        t="$fperjemeb"
                fi 
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/EQCD_EM_3080/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/em_3080$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_em3080$c.sh
                qsub -e $ver-em3080$c.err -o $ver-em3080$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_em3080$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjemeb'' | bc`
        done  

        end=`echo '50 + 1' | bc`
        h="$fperjemec"
        for (( c=1 ; c<=$end ; c++ ))
        do   
                if [ "$c" == "1" ]
                then 
                        t="999"
                elif [ "$c" == "$end" ]
                then  
                        t="$remainemec"
                        h="-0" 
                else 
                        t="$fperjemec"
                fi 
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/EQCD_EM_80170/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/em_80170$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_em80170$c.sh
                qsub -e $ver-em80170$c.err -o $ver-em80170$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_em80170$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjemec'' | bc`
        done  
#
        end=`echo '17 + 1' | bc`
        h="$fperjbca"
        for (( c=1 ; c<=$end ; c++ ))
        do   
                if [ "$c" == "1" ]
                then 
                        t="999"
                elif [ "$c" == "$end" ]
                then  
                        t="$remainbca"
                        h="-0" 
                else 
                        t="$fperjbca"
                fi 
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/EQCD_BC2E_2030/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/bc_2030$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_bc2030$c.sh
                qsub -e $ver-bc2030$c.err -o $ver-bc2030$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_bc2030$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjbca'' | bc`
        done  

        end=`echo '16 + 1' | bc`
        h="$fperjbcb"
        for (( c=1 ; c<=$end ; c++ ))
        do
                if [ "$c" == "1" ]
                then
                        t="999"
                elif [ "$c" == "$end" ]
                then
                        t="$remainbcb"
                        h="-0"
                else
                        t="$fperjbcb"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/EQCD_BC2E_3080/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/bc_3080$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_bc3080$c.sh
                qsub -e $ver-bc3080$c.err -o $ver-bc3080$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_bc3080$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjbcb'' | bc`
        done   

        end=`echo '6 + 1' | bc`
        h="$fperjbcc"
        for (( c=1 ; c<=$end ; c++ ))
        do    
                if [ "$c" == "1" ]
                then
                        t="999"
                elif [ "$c" == "$end" ]
                then
                        t="$remainbcc"
                        h="-0"
                else
                        t="$fperjbcc"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/EQCD_BC2E_80170/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/bc_80170$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_bc80170$c.sh
                qsub -e $ver-bc80170$c.err -o $ver-bc80170$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_bc80170$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjbcc'' | bc`
        done 

     	fi
fi 
cd -
}

do_fake_m()
{
cp cutset_generator/${fil}_m_low.txt ../output/$ver-${fil}_m_low.txt
cp cutset_generator/${fil}_m_wj.txt ../output/$ver-${fil}_m_wj.txt
# fake
cd cutset_generator/ ; ./change_cuts.pl  ${fil}_m_low.txt ; cd .. 
cd json2cpp/ ; ./json2cpp.pl $json ; cd .. ; make -j
mv broc_draiochta $ver-flat-Data
# eff
cd cutset_generator/ ; ./change_cuts.pl  ${fil}_m_wj.txt ; cd .. 
cd json2cpp/ ; ./json2cpp.pl ;cd .. ; make -j 
mv broc_draiochta $ver-flat-Wjets
# W/Z contamination
cd cutset_generator/ ; ./change_cuts.pl  ${fil}_m_low.txt ; cd ..  
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make -j
mv broc_draiochta $ver-flat-Wjets_wc
cd cutset_generator/ ; ./change_cuts.pl  ${fil}_m_low.txt ; cd ..  
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make -j 
mv broc_draiochta $ver-flat-Zjets_wc
# move
mv $ver-flat* $pbspath/.
cp pbs_scripts/*.sh $pbspath/.
cp weights_use/TrainTMVA_RF.weights.txt $pbspath/$ver-TrainTMVA_RF.weights.txt
cp -r weights $pbspath/$ver-weights
cp -r share $pbspath/$ver-share
ndatafil=`ls $indird/Data* | wc -l`
fperj=`echo 'scale=0;'$ndatafil' / '$nd'' | bc`
remainder=`echo 'scale=0; '$ndatafil' % '$nd'' | bc`	
nwfil=`ls $indirm/W* | wc -l`
fperjw=`echo 'scale=0;'$nwfil' / '$nw'' | bc`
remainw=`echo 'scale=0; '$nwfil' % '$nw'' | bc`
nzfil=`ls $indirm/Z* | wc -l`
fperjz=`echo 'scale=0;'$nzfil' / '$nz'' | bc`
remainz=`echo 'scale=0; '$nzfil' % '$nz'' | bc`
echo
echo 'There are '$ndatafil' data files and you requested '$nd' jobs; fperj is '$fperj' and the remainder is '$remainder' '
echo 'There are '$nwfil' W files and you requested '$nw' jobs; fperj is '$fperjw' and the remainder is '$remainw' '
echo 'There are '$nzfil' Z files and you requested '$nz' jobs; fperj is '$fperjz' and the remainder is '$remainz' '
echo
cd $pbspath
end=`echo ''$nd' + 1' | bc`
h="$fperj"
for (( c=1 ; c<=$end ; c++ ))
do
	if [ "$c" == "1" ]
	then
		t="999"
	elif [ "$c" == "$end" ]
	then
		t="$remainder"
		h="-0"
	else
		t="$fperj"
	fi
	cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Data/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/fake$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/1/ | sed 's|MYINDIR|'$indird'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_fake$c.sh
	qsub -e $ver-fake$c.err -o $ver-fake$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_fake$c.sh ;  sleep 0.05
	h=`echo ''$h' + '$fperj'' | bc`
done
end=`echo ''$nw' + 1' | bc`
h="$fperjw"
for (( c=1 ; c<=$end ; c++ ))
do
        if [ "$c" == "1" ]
        then
                t="999"
        elif [ "$c" == "$end" ]
        then
                t="$remainw"
                h="-0"
        else
                t="$fperjw"
        fi
        cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Wjets/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/eff$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_eff$c.sh
        qsub -e $ver-eff$c.err -o $ver-eff$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_eff$c.sh ;  sleep 0.05
        cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Wjets/    | sed s/SCRIPT/muwc_pbs/ | sed s/PROCESS/wconta$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_wconta$c.sh
        qsub -e $ver-wconta$c.err -o $ver-wconta$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_wconta$c.sh ;  sleep 0.05
        h=`echo ''$h' + '$fperjw'' | bc`
done
end=`echo ''$nz' + 1' | bc`
h="$fperjz"
for (( c=1 ; c<=$end ; c++ ))
do
        if [ "$c" == "1" ]
        then
                t="999"
        elif [ "$c" == "$end" ]
        then
                t="$remainz"
                h="-0"
        else
                t="$fperjz"
        fi
        cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Zjets/    | sed s/SCRIPT/muwc_pbs/ | sed s/PROCESS/zconta$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_zjm$c.sh
        qsub -e $ver-zj$c.err -o $ver-zj$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_zjm$c.sh ;  sleep 0.05
        h=`echo ''$h' + '$fperjz'' | bc`
done
}

do_run_m()
{
cp cutset_generator/${fil}_m.txt ../output/$ver-${fil}_m.txt
#because of json junk we need to have one executable per job
cd cutset_generator/ ; ./change_cuts.pl  ${fil}_m.txt ; cd ..
./create_exe_mu.sh $ver $json
mv $ver-flat* $pbspath/.
cp pbs_scripts/*.sh $pbspath/.
cp -r share $pbspath/$ver-share
cp weights_use/TrainTMVA_RF.weights.txt $pbspath/$ver-TrainTMVA_RF.weights.txt
cp -r weights $pbspath/$ver-weights
ndatafil=`ls $indird/Data* | wc -l`
fperj=`echo 'scale=0;'$ndatafil' / '$nd'' | bc`
remainder=`echo 'scale=0; '$ndatafil' % '$nd'' | bc`	
nwfil=`ls $indirm/W* | wc -l`
fperjw=`echo 'scale=0;'$nwfil' / '$nw'' | bc`
remainw=`echo 'scale=0; '$nwfil' % '$nw'' | bc`	
nzfil=`ls $indirm/Z* | wc -l`
fperjz=`echo 'scale=0;'$nzfil' / '$nz'' | bc`
remainz=`echo 'scale=0; '$nzfil' % '$nz'' | bc`
nttfil=`ls $indirm/TT* | wc -l`
fperjtt=`echo 'scale=0;'$nttfil' / '$nt'' | bc`
remaintt=`echo 'scale=0; '$nttfil' % '$nt'' | bc`
nqcdfil=`ls $indirm/MuQCD* | wc -l`
fperjqcd=`echo 'scale=0;'$nqcdfil' / '$nqcd'' | bc`
remainqcd=`echo 'scale=0; '$nqcdfil' % '$nqcd'' | bc`
echo
echo 'There are '$ndatafil' data files and you requested '$nd' jobs; fperj is '$fperj' and the remainder is '$remainder' '
echo 'There are '$nwfil' W files and you requested '$nw' jobs; fperj is '$fperjw' and the remainder is '$remainw' '
echo 'There are '$nttfil' tt files and you requested '$nt' jobs; fperj is '$fperjtt' and the remainder is '$remaintt' '
echo 'There are '$nqcdfil' MuQCD files and you requested '$nqcd' jobs; fperj is '$fperjqcd' and the remainder is '$remainqcd' '
echo 'There are '$nzfil' DY50 files and you requested '$nz' jobs; fperj is '$fperjz' and the remainder is '$remainz' '
echo
cd $pbspath
#DATA
if [ $runwhat = 'both' -o $runwhat = 'data' ]
then
	end=`echo ''$nd' + 1' | bc`
	h="$fperj"
	for (( c=1 ; c<=$end ; c++ ))
	do
		if [ "$c" == "1" ]
		then
			t="999"
		elif [ "$c" == "$end" ]
		then
		t="$remainder"
		h="-0"
		else
			t="$fperj"
		fi
		cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Data/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/datam$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/1/ | sed 's|MYINDIR|'$indird'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_Datam$c.sh
		qsub -e $ver-data$c.err -o $ver-data$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_Datam$c.sh ;  sleep 0.05
		h=`echo ''$h' + '$fperj'' | bc`
	done
fi
#MC
if [ $runwhat = 'both' -o $runwhat = 'mc' ]
then		
      	end=`echo ''$nw' + 1' | bc`
      	h="$fperjw"
      	for (( c=1 ; c<=$end ; c++ ))
     	do
                if [ "$c" == "1" ]
        	then
                	t="999"
                elif [ "$c" == "$end" ]
                then
	                t="$remainw"
        	        h="-0"
                else
                        t="$fperjw"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Wjets/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/wjm$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_wjm$c.sh
                qsub -e $ver-wj$c.err -o $ver-wj$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_wjm$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjw'' | bc`
        done	
	end=`echo ''$nt' + 1' | bc`
	h="$fperjtt"
	for (( c=1 ; c<=$end ; c++ ))
        do	
                if [ "$c" == "1" ]
                then
                        t="999"
                elif [ "$c" == "$end" ]
                then
                        t="$remaintt"
                        h="-0"
                else
                        t="$fperjtt"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/TTbar/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/ttm$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_ttm$c.sh
                qsub -e $ver-tt$c.err -o $ver-tt$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_ttm$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjtt'' | bc`
        done	
        end=`echo ''$nz' + 1' | bc`
        h="$fperjz"
        for (( c=1 ; c<=$end ; c++ ))
        do
                if [ "$c" == "1" ]
                then
                        t="999"
                elif [ "$c" == "$end" ]
                then
                        t="$remainz"
                        h="-0"
                else
                        t="$fperjz"
                fi
                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/Zjets/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/zjm$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_zjm$c.sh
                qsub -e $ver-zjm$c.err -o $ver-zjm$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_zjm$c.sh ;  sleep 0.05
                h=`echo ''$h' + '$fperjz'' | bc`
        done
	cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/TsChan/  | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/stsm/    | sed -e 's/HEAD/\-0/' | sed s/TAIL/999/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_stsm.sh
	cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/TtChan/  | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/sttm/    | sed -e 's/HEAD/\-0/' | sed s/TAIL/999/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_sttm.sh
	cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/TtWChan/ | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/sttwm/   | sed -e 's/HEAD/\-0/' | sed s/TAIL/999/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_sttwm.sh

        qsub -e $ver-sts.err -o $ver-sts.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_stsm.sh ;  sleep 0.05
	qsub -e $ver-stt.err -o $ver-stt.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_sttm.sh ;  sleep 0.05
        qsub -e $ver-sttw.err -o $ver-sttw.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_sttwm.sh ;  sleep 0.05
	
	       	if [ "$runQCDMC" == "1" ]
	       	then
        		end=`echo ''$nqcd' + 1' | bc`
		        h="$fperjqcd"
		        for (( c=1 ; c<=$end ; c++ ))
		        do
		                if [ "$c" == "1" ]
                		then
		                        t="999"
		                elif [ "$c" == "$end" ]
		                then
		                        t="$remainqcd"
		                        h="-0"
		                else
		                        t="$fperjqcd"
		                fi
		                cat submit.template.sh | sed s/VERSION/$ver/ | sed s/CUTFIL/$fil/ | sed s/SAMPLE/MuQCD/    | sed s/SCRIPT/mu_pbs/ | sed s/PROCESS/MuQCD$c/  | sed -e 's/HEAD/'$h'/'  | sed s/TAIL/$t/  | sed s/ISDATA/0/ | sed 's|MYINDIR|'$indirm'|'  | sed 's|MYREL|'$pathtorel'|' | sed  's|MYBOOST|'$boost'|' | sed  's|SUBPATH|'$pbspath'|'> s_${ver}_muqcd$c.sh
		                qsub -e $ver-muqcd$c.err -o $ver-muqcd$c.out -l walltime=$walltime -q localgrid@cream01 s_${ver}_muqcd$c.sh ;  sleep 0.05
		                h=`echo ''$h' + '$fperjqcd'' | bc`
		        done
       		
       		fi
fi
cd -
}

die_act()
{
echo
echo "action is either fake or run"
echo
exit 1
}

die_chan()
{
echo
echo "channel is either e or m"
exit 1
echo
}

if [ "$cha" = 'e' ] 
then
	if [ "$act" = 'fake' ] 
	then
		do_fake_e
	elif [ "$act" = 'run' ] 
	then
		do_run_e
	else
		die_act
	fi


elif [ $cha = 'm' ] 
then
        if [ $act = 'fake' ] 
	then
		do_fake_m
        elif [ $act = 'run' ] 
	then
		do_run_m
        else
        	die_act
        fi


else
	die_chan
fi

