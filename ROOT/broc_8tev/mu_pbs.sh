#!/bin/bash

if  [ "$1" = ''  -o  "$2" = ''  -o  "$3" = ''  -o  "$4" = '' -o "$5" = '' ] 
then
	echo
	echo './mu.sh <vers> <process> <head> <tail> <indir>'
	echo
	exit 1
fi

vers="$1"
process="$2"
h="$3"
t="$4"
indir="$5"
ls $indir/${process}*.root -1 | sed "s/\n/\ /g" | head --lines=${h} | tail -${t} > ${process}_input.txt

cat default_config_sed.txt | sed "s/PROCESS/$process/g" > ${process}_config.txt 

./${vers}-flat-${process} ${process}_config.txt >& ${vers}-${process}.txt
mv ${process}_plots.root ${vers}-${process}.root

