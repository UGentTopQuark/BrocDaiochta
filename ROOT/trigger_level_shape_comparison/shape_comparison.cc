#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>
#include "CanvasHolder/Canvas_Holder.h"

int main(int argc, char **argv)
{
	if(argc < 2){
		std::cout << "usage: ./shape_comaprison <infile>\n" << std::endl;
		exit(1);
	}

	std::string cutset = "01_cutset";
	std::string event_type = "muon";
	std::string dataset = "Data";

	// trigger Level
	std::vector<std::pair<std::string, std::string> > versions;
 	versions.push_back(std::pair<std::string, std::string>("HLT_Mu5_L1Seed.", "trig_obj_"));
 	versions.push_back(std::pair<std::string, std::string>("HLT_Mu5_HLT.", "trig_obj_"));
 	versions.push_back(std::pair<std::string, std::string>("All.", ""));

	TFile *infile = new TFile(argv[1], "OPEN");
	
	// for TLegend, needs the same number of entries as cutsets vector
	std::vector<std::string> identifier;
	identifier.push_back("L1");
	identifier.push_back("HLT");
	identifier.push_back("RECO");

	bool normalise_histos = false;

	/*********************************************
	 *	Main Program
	 ********************************************/

	std::vector<std::string> histo_names;
	histo_names.push_back("eta_");
	histo_names.push_back("pt_");
	histo_names.push_back("phi_");

	std::vector<CanvasHolder*> objects_to_delete;

	std::vector<int> *colours = new std::vector<int>();  
	colours->push_back(kGreen); 
	colours->push_back(kRed); 
	colours->push_back(kYellow+2); 
	colours->push_back(kGreen+1); 
	colours->push_back(kBlue-5); 			
	colours->push_back(kBlue+2);
	colours->push_back(kBlack);
	
	TFile *outfile = new TFile("outfile.root", "RECREATE");

	for(std::vector<std::string>::iterator histo_name = histo_names.begin();
		histo_name != histo_names.end();
		++histo_name)
	{
		CanvasHolder *cholder = new CanvasHolder;
		objects_to_delete.push_back(cholder);

		for(unsigned int trigger_level = 0; trigger_level < versions.size();
			++trigger_level){

			std::string full_name = versions[trigger_level].first+*histo_name+versions[trigger_level].second
						+ dataset + "|" + event_type + "|" + cutset;
			std::cout << "full_name: " << full_name << std::endl;
			TH1F* histo = (TH1F*) infile->GetDirectory("eventselection")->Get(full_name.c_str());

			std::cout << "Here" << std::endl;
			std::cout << "histo " << histo << std::endl;

			if(normalise_histos)
				histo->Scale(1./histo->Integral());

			cholder->addHisto(histo,identifier[trigger_level],"HISTE0");
				
			cholder->setCanvasTitle(*histo_name);
			cholder->setLegTitle("Level");
			cholder->setTitle("");
			cholder->setLineColors(*colours); 
			cholder->setOptStat(00000);
			cholder->setTitleY("number of Muons");
		}
	}

	for(std::vector<CanvasHolder*>::iterator ch = objects_to_delete.begin();
	    ch != objects_to_delete.end();
	    ++ch){
	    	outfile->cd();
	    	(*ch)->save("eps");
	    	(*ch)->write(outfile);
		delete *ch;
	}


	delete colours; 
	colours=NULL; 
	delete outfile;
	outfile = NULL;
}
