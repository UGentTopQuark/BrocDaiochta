#ifndef FEISTEOIR_TEMPATE_H
#define FEISTEOIR_TEMPATE_H

#include "TFile.h"
#include "TH1F.h"

namespace feisteoir{
	class Template{
		public:
			Template(std::string file_name, std::string dataset_name, std::string histo_name, std::string directory_name);
			~Template();

			inline TH1F* get_histo(){ return histo; };
			inline TFile* get_file(){ return input_file; };
			inline std::string get_dataset_name(){ return dataset_name; };
		private:
			std::string dataset_name;
			TFile *input_file;
			TH1F *histo;
	};
}

#endif
