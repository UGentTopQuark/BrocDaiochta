#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "TemplateFitter.h"
#include "ConfigReader/ConfigReader.h"

int main(int argc, char **argv)
{
	if(argc < 2){
		std::cout << "usage: " << argv[0] << " <config file>" << std::endl;
		exit(1);
	}
	std::string config_file_name(argv[1]);

	eire::ConfigReader *config_reader = new eire::ConfigReader();
	config_reader->read_config_from_file(config_file_name);

	feisteoir::TemplateFitter *template_fitter = new feisteoir::TemplateFitter(config_reader);
	template_fitter->fit();
}
