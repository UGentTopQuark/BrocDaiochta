#include "Template.h"

feisteoir::Template::Template(std::string file_name, std::string dataset_name, std::string histo_name, std::string directory_name)
{
	input_file = new TFile(file_name.c_str(), "OPEN");
	histo = (TH1F*) input_file->GetDirectory(directory_name.c_str())->Get(histo_name.c_str());

	this->dataset_name = dataset_name;
}

feisteoir::Template::~Template()
{
	if(input_file){
		delete input_file;
		input_file = NULL;
	}
}
