#include "TemplateFitter.h"

feisteoir::TemplateFitter::TemplateFitter(eire::ConfigReader *config_reader)
{
	toy_mode = false;

	data = NULL;

	histo_scaler = new eire::HistoScaler();

	process_config(config_reader);
}

feisteoir::TemplateFitter::~TemplateFitter()
{
	if(histo_scaler){delete histo_scaler; histo_scaler = NULL;}
}

void feisteoir::TemplateFitter::fit()
{
        std::vector<int> *colours = new std::vector<int>();
        colours->push_back(kRed-3);
        colours->push_back(kYellow-7);
        colours->push_back(kWhite+8);
        colours->push_back(kGreen-3);
/*
        colours->push_back(kCyan-6);
        colours->push_back(kBlue-1);
        colours->push_back(kBlue-2);
        colours->push_back(kBlue-3);
*/
	CanvasHolder cholder;

	TObjArray *mc = new TObjArray(templates.size());
	int nparameter = 0;
	for(std::map<std::string, feisteoir::Template*>::iterator temp = templates.begin();
		temp != templates.end();
		++temp){
		TH1F *histo = temp->second->get_histo();
		histo->Rebin(rebin);
		mc->Add(histo);
		parameter_names[temp->first] = nparameter;
		++nparameter;
	}

	if(toy_mode == false){
		data->get_histo()->Rebin(rebin);

		TFractionFitter* fit = new TFractionFitter(data->get_histo(), mc);

		// don't allow negative contributions
		for(int i = 0; i < nparameter; ++i){
			fit->Constrain(i, 0., 1.);
		}

		Int_t status = fit->Fit();               // perform the fit
		std::cout << "fit status: " << status << std::endl;
		if (status == 0) {                       // check on fit status
			TH1F* result = (TH1F*) fit->GetPlot();
			std::cout << "data integral: " << data->get_histo()->Integral() << std::endl;
			std::cout << "result integral: " << result->Integral() << std::endl;
			double par, error;
			for(std::vector<std::string>::iterator dataset = sequence_to_plot.begin();
				dataset != sequence_to_plot.end();
				++dataset){
				int parameter_id = parameter_names[*dataset];
				fit->GetResult(parameter_id,par,error);
				std::cout << *dataset << ": " << par << "+/-" << error << std::endl;
				TH1F* frac = (TH1F*) fit->GetMCPrediction(parameter_id);
				frac->Scale(result->Integral()*par/frac->Integral());
				std::cout << *dataset << " (after fit): " << frac->Integral() << std::endl;
				cholder.addHistoStacked(frac, *dataset, "HIST");
			}
		}
		cholder.addHisto(data->get_histo(), "data", "E1");
	}

	cholder.setLineColors(*colours);
	cholder.setOptStat(000000);
	cholder.save("test.eps");
}

void feisteoir::TemplateFitter::process_config(eire::ConfigReader *config_reader)
{
	std::string template_identifier = "template";
	std::string data_identifier = "data";

	std::string file_var = "file_name";
	std::string histo_var = "histo_name";
	std::string dir_var = "directory_name";
	std::string dataset_var = "dataset_name";
	std::string toy_var = "toy_mode";

	histo_scaler->set_cross_section_set(config_reader->get_var("cross_section_set", "global", true));
	histo_scaler->set_integrated_luminosity(atof(config_reader->get_var("integrated_luminosity", "global", true).c_str()));

	sequence_to_plot = config_reader->get_vec_var("sequence_to_plot", "global", true);

	std::string rebin_str = config_reader->get_var("rebin", "global");
	if(rebin_str != ""){
		rebin = atoi(rebin_str.c_str());
	}else{
		rebin = 1;
	}

	std::vector<std::string> *sections = config_reader->get_sections();
	for(std::vector<std::string>::iterator section = sections->begin();
		section != sections->end();
		++section){
		if(*section == "global") continue; // ignore global section

		if(section->find(template_identifier) != std::string::npos){
			std::string file_name = config_reader->get_var(file_var, *section, true);
			std::string histo_name = config_reader->get_var(histo_var, *section, true);
			std::string dir_name = config_reader->get_var(dir_var, *section, true);
			std::string dataset_name = config_reader->get_var(dataset_var, *section, true);
			feisteoir::Template* new_template = new feisteoir::Template(file_name, dataset_name, histo_name, dir_name);
			new_template->get_histo()->Sumw2();
			std::cout << "dataset: " << dataset_name << " integral before scaling: " << new_template->get_histo()->Integral() << std::endl;
			histo_scaler->scale_histo<TH1F>(new_template->get_histo(), dir_name, new_template->get_file());
			std::cout << "dataset: " << dataset_name << " integral after scaling: " << new_template->get_histo()->Integral() << std::endl;
			templates[dataset_name] = new_template;
		}

		if(section->find(data_identifier) != std::string::npos){
			toy_mode = config_reader->get_bool_var(toy_var, *section, true);

			if(!toy_mode){
				std::string file_name = config_reader->get_var(file_var, *section, true);
				std::string histo_name = config_reader->get_var(histo_var, *section, true);
				std::string dir_name = config_reader->get_var(dir_var, *section, true);
				std::string dataset_name = config_reader->get_var(dataset_var, *section, true);
				feisteoir::Template* new_template = new feisteoir::Template(file_name, dataset_name, histo_name, dir_name);
				data = new_template;
			}
		}
	}
}
