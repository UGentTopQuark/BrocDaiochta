#ifndef FEISTEOIR_TEMPLATEFITTER_H
#define FEISTEOIR_TEMPLATEFITTER_H

#include "TFractionFitter.h"
#include "ConfigReader/ConfigReader.h"
#include "Template.h"
#include "CanvasHolder/Canvas_Holder.h"
#include "HistoScaler/HistoScaler.h"

namespace feisteoir{
	class TemplateFitter{
		public:
			TemplateFitter(eire::ConfigReader *config_reader);
			~TemplateFitter();

			void fit();
		private:
			void process_config(eire::ConfigReader *config_reader);

			// dataset name, Template
			std::map<std::string, feisteoir::Template*> templates;
			std::map<std::string, int> parameter_names;
			feisteoir::Template* data;

			std::vector<std::string> sequence_to_plot;

			eire::HistoScaler *histo_scaler;
			int rebin;

			bool toy_mode;
			int ntoy_experiments;
	};
}

#endif
