#include "../../interface/MorObjects/MParticle.h"

mor::Particle::Particle()
{
	mc_matched_val = -1;
	from_ttbar_decay = false;
	charge_val = 0;
	mc_id = 0;
}

mor::Particle::Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>  > p4) : ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double>  > (p4)
{
}

mor::Particle::Particle(beag::Particle &beag_particle)
{
	set_beag_info(beag_particle);
}

mor::Particle::~Particle()
{
}

void mor::Particle::set_beag_info(beag::Particle &beag_particle)
{
	SetEta(beag_particle.eta);
	SetPhi(beag_particle.phi);
	SetM(beag_particle.mass);
	SetPt(beag_particle.pt);

	mc_matched_val = beag_particle.mc_matched;
	from_ttbar_decay = beag_particle.from_ttbar_decay;

	charge_val = beag_particle.charge;

	if(mc_matched_val){
		mc_match_p4.SetEta(beag_particle.mc_eta);
		mc_match_p4.SetPhi(beag_particle.mc_phi);
		mc_match_p4.SetM(beag_particle.mc_mass);
		mc_match_p4.SetPt(beag_particle.mc_pt);
		mc_id = beag_particle.mc_id;
	}
	if(beag_particle.mc_id != 0){
	mc_id = beag_particle.mc_id;
	}
}
