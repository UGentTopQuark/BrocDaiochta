#include "../../interface/MorObjects/MTrigger.h"

int mor::Trigger::error_counter = 0;

void mor::Trigger::set_beag_info(beag::Trigger &beag_trigger)
{
	triggered_val = beag_trigger.triggered;
}

bool mor::Trigger::trigger_available(std::string trigger)
{
	if(trigger_names.find(trigger) != trigger_names.end())
		return true;
	else
		return false;
}

bool mor::Trigger::triggered(std::string trigger)
{
	if(trigger == "PASS") return true;

	if(trigger_names.find(trigger) != trigger_names.end()){
		if(verbose){
			std::cout << "trigger name: " << trigger << std::endl;
			for(std::vector<bool>::iterator bit = triggered_val.begin();
				bit != triggered_val.end();
				++bit){
				std::cout << *bit;
			}
			std::cout << std::endl;
		}
		return triggered_val[trigger_names[trigger]];
		//return triggered_val[trigger_names[trigger]];
	}
	else{
		if(error_counter == 0 || error_counter % 100000 == 0){
			std::cerr << "WARNING: mor::Trigger::triggered(): trigger not available: " << trigger << " (message encountered " << error_counter << " times) " << std::endl;
			if(error_counter == 0){
				std::cout << "Available ids: " <<std::endl;
				for(std::map<std::string,  unsigned long long>::iterator id = trigger_names.begin();id != trigger_names.end();
				    id++){
					std::cout << id->first << std::endl;
				}
				
			}
		}
		++error_counter;
		//exit(1);	// no point in keep on running if the trigger is not there, results won't make any sense anyway
		return false;
	}
}

void mor::Trigger::set_trigger_names(std::vector<std::string> *trigger_names_vec)
{
	trigger_names.clear();
	unsigned long long ntrigger = 0;
	for(std::vector<std::string>::iterator name = trigger_names_vec->begin();
		name != trigger_names_vec->end();
		++name){
		trigger_names[*name] = ntrigger;
		++ntrigger;
	}
}
