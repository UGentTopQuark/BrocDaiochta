#include "../../interface/MorObjects/MTTbarGenEvent.h"

mor::TTbarGenEvent::TTbarGenEvent(eire::JetCorrectionService *jec)
{
	this->jec = jec;
}

mor::TTbarGenEvent::~TTbarGenEvent()
{
	jec = 0;
}

void mor::TTbarGenEvent::set_beag_info(beag::TTbarGenEvent &beag_gen_evt, double rho)
{
	hadT_p.set_beag_info(beag_gen_evt.hadT);
	hadB_p.set_beag_info(beag_gen_evt.hadB);
	hadW_p.set_beag_info(beag_gen_evt.hadW);
	q_p.set_beag_info(beag_gen_evt.q);
	qbar_p.set_beag_info(beag_gen_evt.qbar);

	lepT_p.set_beag_info(beag_gen_evt.lepT);
	lepB_p.set_beag_info(beag_gen_evt.lepB);
	lepW_p.set_beag_info(beag_gen_evt.lepW);
	lep_p.set_beag_info(beag_gen_evt.lep);
	neu_p.set_beag_info(beag_gen_evt.neu);

	decay_channel_val = beag_gen_evt.decay_channel;

	hadB_matched_val = beag_gen_evt.hadB_matched;
	lepB_matched_val = beag_gen_evt.lepB_matched;
	q_matched_val = beag_gen_evt.q_matched;
	qbar_matched_val = beag_gen_evt.qbar_matched;

	if(hadB_matched_val) hadB_jet_j.set_beag_info(beag_gen_evt.hadB_jet);
	else{ beag::Jet tmp; hadB_jet_j.set_beag_info(tmp); }
	if(lepB_matched_val) lepB_jet_j.set_beag_info(beag_gen_evt.lepB_jet);
	else{ beag::Jet tmp; lepB_jet_j.set_beag_info(tmp); }
	if(q_matched_val) q_jet_j.set_beag_info(beag_gen_evt.q_jet);
	else{ beag::Jet tmp; q_jet_j.set_beag_info(tmp); }
	if(qbar_matched_val) qbar_jet_j.set_beag_info(beag_gen_evt.qbar_jet);
	else{ beag::Jet tmp; qbar_jet_j.set_beag_info(tmp); }

	if(jec){
		hadB_jet_j *= jec->get_correction(&hadB_jet_j, rho);
		lepB_jet_j *= jec->get_correction(&lepB_jet_j, rho);
		q_jet_j *= jec->get_correction(&q_jet_j, rho);
		qbar_jet_j *= jec->get_correction(&qbar_jet_j, rho);
	}

	lep_matched_val = beag_gen_evt.lep_matched;
	if(lep_matched_val) reco_lep.set_beag_info(beag_gen_evt.reco_lep);
	else{ beag::Lepton tmp; reco_lep.set_beag_info(tmp); }
}

void mor::TTbarGenEvent::set_beag_info()
{
	beag::TTbarGenEvent dummy;
	set_beag_info(dummy);
}
