#include "../../interface/AnalysisTools/RunSelector.h"

RunSelector::RunSelector()
{
	accept_all_events = false;
}

RunSelector::~RunSelector()
{
}

void RunSelector::set_event_information(mor::EventInformation *evt_info)
{
	this->evt_info = evt_info;
}

bool RunSelector::is_good()
{
	if(accept_all_events) return true;
	// JSON AUTO FILL BEGIN -- do not edit this comment
	double cur_run_number = evt_info->run();
	double cur_lumi_block = evt_info->lumi_block();
	double MIN=0;
	double MAX=10000000;
	if(	( cur_run_number == 165993 && (
	  (cur_lumi_block >= 879 && cur_lumi_block <= 1660) ||
	  (cur_lumi_block >= 71 && cur_lumi_block <= 873) ||
	  (cur_lumi_block >= 1665 && cur_lumi_block <= 1697) ) ) ||
	( cur_run_number == 166011 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 81) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 83) ) ) ||
	( cur_run_number == 166033 && (
	  (cur_lumi_block >= 713 && cur_lumi_block <= 1233) ||
	  (cur_lumi_block >= 613 && cur_lumi_block <= 707) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 330) ||
	  (cur_lumi_block >= 35 && cur_lumi_block <= 53) ||
	  (cur_lumi_block >= 360 && cur_lumi_block <= 444) ||
	  (cur_lumi_block >= 450 && cur_lumi_block <= 606) ||
	  (cur_lumi_block >= 336 && cur_lumi_block <= 355) ) ) ||
	( cur_run_number == 166034 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 234 && cur_lumi_block <= 307) ||
	  (cur_lumi_block >= 115 && cur_lumi_block <= 228) ) ) ||
	( cur_run_number == 166049 && (
	  (cur_lumi_block >= 88 && cur_lumi_block <= 236) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 86) ||
	  (cur_lumi_block >= 242 && cur_lumi_block <= 674) ) ) ||
	( cur_run_number == 166149 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ) ) ||
	( cur_run_number == 166150 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 99) ||
	  (cur_lumi_block >= 101 && cur_lumi_block <= 116) ) ) ||
	( cur_run_number == 166161 && (
	  (cur_lumi_block >= 38 && cur_lumi_block <= 120) ||
	  (cur_lumi_block >= 122 && cur_lumi_block <= 123) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 126) ) ) ||
	( cur_run_number == 166163 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ||
	  (cur_lumi_block >= 14 && cur_lumi_block <= 33) ) ) ||
	( cur_run_number == 166164 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 32) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 40) ) ) ||
	( cur_run_number == 166346 && (
	  (cur_lumi_block >= 212 && cur_lumi_block <= 215) ||
	  (cur_lumi_block >= 48 && cur_lumi_block <= 210) ) ) ||
	( cur_run_number == 166374 && (
	  (cur_lumi_block >= 46 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 188) ) ) ||
	( cur_run_number == 166380 && (
	  (cur_lumi_block >= 715 && cur_lumi_block <= 1400) ||
	  (cur_lumi_block >= 373 && cur_lumi_block <= 711) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 367) ||
	  (cur_lumi_block >= 1406 && cur_lumi_block <= 1809) ) ) ||
	( cur_run_number == 166408 && (
	  (cur_lumi_block >= 953 && cur_lumi_block <= 1236) ||
	  (cur_lumi_block >= 67 && cur_lumi_block <= 283) ||
	  (cur_lumi_block >= 291 && cur_lumi_block <= 947) ) ) ||
	( cur_run_number == 166429 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 89) ) ) ||
	( cur_run_number == 166438 && (
	  (cur_lumi_block >= 87 && cur_lumi_block <= 856) ||
	  (cur_lumi_block >= 32 && cur_lumi_block <= 85) ||
	  (cur_lumi_block >= 858 && cur_lumi_block <= 866) ) ) ||
	( cur_run_number == 166462 && (
	  (cur_lumi_block >= 323 && cur_lumi_block <= 526) ||
	  (cur_lumi_block >= 108 && cur_lumi_block <= 317) ||
	  (cur_lumi_block >= 78 && cur_lumi_block <= 102) ) ) ||
	( cur_run_number == 166486 && (
	  (cur_lumi_block >= 97 && cur_lumi_block <= 174) ||
	  (cur_lumi_block >= 80 && cur_lumi_block <= 95) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 75) ) ) ||
	( cur_run_number == 166502 && (
	  (cur_lumi_block >= 83 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 43 && cur_lumi_block <= 78) ) ) ||
	( cur_run_number == 166512 && (
	  (cur_lumi_block >= 1873 && cur_lumi_block <= 1874) ||
	  (cur_lumi_block >= 1868 && cur_lumi_block <= 1868) ||
	  (cur_lumi_block >= 491 && cur_lumi_block <= 605) ||
	  (cur_lumi_block >= 42 && cur_lumi_block <= 430) ||
	  (cur_lumi_block >= 1821 && cur_lumi_block <= 1862) ||
	  (cur_lumi_block >= 432 && cur_lumi_block <= 487) ||
	  (cur_lumi_block >= 1870 && cur_lumi_block <= 1871) ||
	  (cur_lumi_block >= 611 && cur_lumi_block <= 1279) ||
	  (cur_lumi_block >= 1281 && cur_lumi_block <= 1818) ) ) ||
	( cur_run_number == 166514 && (
	  (cur_lumi_block >= 460 && cur_lumi_block <= 464) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 455) ) ) ||
	( cur_run_number == 166530 && (
	  (cur_lumi_block >= 43 && cur_lumi_block <= 63) ) ) ||
	( cur_run_number == 166554 && (
	  (cur_lumi_block >= 597 && cur_lumi_block <= 730) ||
	  (cur_lumi_block >= 320 && cur_lumi_block <= 595) ||
	  (cur_lumi_block >= 46 && cur_lumi_block <= 218) ||
	  (cur_lumi_block >= 736 && cur_lumi_block <= 736) ||
	  (cur_lumi_block >= 224 && cur_lumi_block <= 287) ||
	  (cur_lumi_block >= 732 && cur_lumi_block <= 734) ||
	  (cur_lumi_block >= 290 && cur_lumi_block <= 317) ) ) ||
	( cur_run_number == 166563 && (
	  (cur_lumi_block >= 492 && cur_lumi_block <= 748) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 276) ) ) ||
	( cur_run_number == 166565 && (
	  (cur_lumi_block >= 469 && cur_lumi_block <= 898) ||
	  (cur_lumi_block >= 316 && cur_lumi_block <= 467) ||
	  (cur_lumi_block >= 153 && cur_lumi_block <= 312) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 147) ) ) ||
	( cur_run_number == 166699 && (
	  (cur_lumi_block >= 240 && cur_lumi_block <= 415) ||
	  (cur_lumi_block >= 681 && cur_lumi_block <= 912) ||
	  (cur_lumi_block >= 421 && cur_lumi_block <= 477) ||
	  (cur_lumi_block >= 483 && cur_lumi_block <= 677) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 234) ) ) ||
	( cur_run_number == 166701 && (
	  (cur_lumi_block >= 712 && cur_lumi_block <= 724) ||
	  (cur_lumi_block >= 783 && cur_lumi_block <= 792) ||
	  (cur_lumi_block >= 731 && cur_lumi_block <= 757) ||
	  (cur_lumi_block >= 681 && cur_lumi_block <= 705) ||
	  (cur_lumi_block >= 324 && cur_lumi_block <= 506) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 13) ||
	  (cur_lumi_block >= 764 && cur_lumi_block <= 777) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 319) ||
	  (cur_lumi_block >= 557 && cur_lumi_block <= 672) ||
	  (cur_lumi_block >= 513 && cur_lumi_block <= 551) ) ) ||
	( cur_run_number == 166763 && (
	  (cur_lumi_block >= 174 && cur_lumi_block <= 650) ||
	  (cur_lumi_block >= 46 && cur_lumi_block <= 168) ) ) ||
	( cur_run_number == 166781 && (
	  (cur_lumi_block >= 236 && cur_lumi_block <= 253) ||
	  (cur_lumi_block >= 115 && cur_lumi_block <= 115) ||
	  (cur_lumi_block >= 41 && cur_lumi_block <= 111) ||
	  (cur_lumi_block >= 255 && cur_lumi_block <= 382) ||
	  (cur_lumi_block >= 117 && cur_lumi_block <= 233) ) ) ||
	( cur_run_number == 166782 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 569) ) ) ||
	( cur_run_number == 166784 && (
	  (cur_lumi_block >= 281 && cur_lumi_block <= 365) ||
	  (cur_lumi_block >= 119 && cur_lumi_block <= 276) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 114) ) ) ||
	( cur_run_number == 166787 && (
	  (cur_lumi_block >= 60 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 132 && cur_lumi_block <= 364) ) ) ||
	( cur_run_number == 166839 && (
	  (cur_lumi_block >= 299 && cur_lumi_block <= 302) ||
	  (cur_lumi_block >= 178 && cur_lumi_block <= 297) ||
	  (cur_lumi_block >= 43 && cur_lumi_block <= 173) ) ) ||
	( cur_run_number == 166841 && (
	  (cur_lumi_block >= 998 && cur_lumi_block <= 1015) ||
	  (cur_lumi_block >= 882 && cur_lumi_block <= 977) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 845) ||
	  (cur_lumi_block >= 984 && cur_lumi_block <= 984) ||
	  (cur_lumi_block >= 851 && cur_lumi_block <= 876) ||
	  (cur_lumi_block >= 988 && cur_lumi_block <= 992) ) ) ||
	( cur_run_number == 166842 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 170) ) ) ||
	( cur_run_number == 166859 && (
	  (cur_lumi_block >= 421 && cur_lumi_block <= 421) ||
	  (cur_lumi_block >= 423 && cur_lumi_block <= 423) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 418) ) ) ||
	( cur_run_number == 166860 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 21) ) ) ||
	( cur_run_number == 166861 && (
	  (cur_lumi_block >= 8 && cur_lumi_block <= 13) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 6) ) ) ||
	( cur_run_number == 166864 && (
	  (cur_lumi_block >= 102 && cur_lumi_block <= 119) ||
	  (cur_lumi_block >= 249 && cur_lumi_block <= 307) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 99) ||
	  (cur_lumi_block >= 367 && cur_lumi_block <= 374) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 29) ||
	  (cur_lumi_block >= 125 && cur_lumi_block <= 247) ||
	  (cur_lumi_block >= 378 && cur_lumi_block <= 454) ||
	  (cur_lumi_block >= 311 && cur_lumi_block <= 365) ||
	  (cur_lumi_block >= 31 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 478 && cur_lumi_block <= 537) ) ) ||
	( cur_run_number == 166888 && (
	  (cur_lumi_block >= 398 && cur_lumi_block <= 470) ||
	  (cur_lumi_block >= 156 && cur_lumi_block <= 394) ||
	  (cur_lumi_block >= 93 && cur_lumi_block <= 154) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 90) ) ) ||
	( cur_run_number == 166889 && (
	  (cur_lumi_block >= 79 && cur_lumi_block <= 228) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 73) ) ) ||
	( cur_run_number == 166890 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 441) ) ) ||
	( cur_run_number == 166894 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 190) ) ) ||
	( cur_run_number == 166895 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 66) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 597) ||
	  (cur_lumi_block >= 599 && cur_lumi_block <= 603) ) ) ||
	( cur_run_number == 166911 && (
	  (cur_lumi_block >= 58 && cur_lumi_block <= 76) ||
	  (cur_lumi_block >= 81 && cur_lumi_block <= 104) ) ) ||
	( cur_run_number == 166922 && (
	  (cur_lumi_block >= 752 && cur_lumi_block <= 769) ||
	  (cur_lumi_block >= 773 && cur_lumi_block <= 773) ||
	  (cur_lumi_block >= 41 && cur_lumi_block <= 105) ||
	  (cur_lumi_block >= 345 && cur_lumi_block <= 418) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 39) ||
	  (cur_lumi_block >= 423 && cur_lumi_block <= 747) ||
	  (cur_lumi_block >= 110 && cur_lumi_block <= 340) ) ) ||
	( cur_run_number == 166923 && (
	  (cur_lumi_block >= 389 && cur_lumi_block <= 470) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 382) ) ) ||
	( cur_run_number == 166946 && (
	  (cur_lumi_block >= 75 && cur_lumi_block <= 201) ||
	  (cur_lumi_block >= 41 && cur_lumi_block <= 72) ) ) ||
	( cur_run_number == 166950 && (
	  (cur_lumi_block >= 883 && cur_lumi_block <= 950) ||
	  (cur_lumi_block >= 36 && cur_lumi_block <= 210) ||
	  (cur_lumi_block >= 956 && cur_lumi_block <= 1012) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 31) ||
	  (cur_lumi_block >= 1327 && cur_lumi_block <= 1345) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 216 && cur_lumi_block <= 877) ||
	  (cur_lumi_block >= 1347 && cur_lumi_block <= 1438) ||
	  (cur_lumi_block >= 1018 && cur_lumi_block <= 1321) ) ) ||
	( cur_run_number == 166960 && (
	  (cur_lumi_block >= 143 && cur_lumi_block <= 166) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 137) ) ) ||
	( cur_run_number == 166966 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 238) ) ) ||
	( cur_run_number == 166967 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 220) ) ) ||
	( cur_run_number == 167039 && (
	  (cur_lumi_block >= 20 && cur_lumi_block <= 92) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 228) ) ) ||
	( cur_run_number == 167041 && (
	  (cur_lumi_block >= 339 && cur_lumi_block <= 391) ||
	  (cur_lumi_block >= 396 && cur_lumi_block <= 462) ||
	  (cur_lumi_block >= 467 && cur_lumi_block <= 663) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 336) ) ) ||
	( cur_run_number == 167043 && (
	  (cur_lumi_block >= 130 && cur_lumi_block <= 235) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 125) ) ) ||
	( cur_run_number == 167078 && (
	  (cur_lumi_block >= 40 && cur_lumi_block <= 174) ) ) ||
	( cur_run_number == 167098 && (
	  (cur_lumi_block >= 167 && cur_lumi_block <= 406) ||
	  (cur_lumi_block >= 448 && cur_lumi_block <= 461) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 162) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 90) ) ) ||
	( cur_run_number == 167102 && (
	  (cur_lumi_block >= 323 && cur_lumi_block <= 430) ||
	  (cur_lumi_block >= 48 && cur_lumi_block <= 233) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 235 && cur_lumi_block <= 317) ) ) ||
	( cur_run_number == 167103 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 94) ) ) ||
	( cur_run_number == 167151 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 42) ) ) ||
	( cur_run_number == 167281 && (
	  (cur_lumi_block >= 146 && cur_lumi_block <= 315) ||
	  (cur_lumi_block >= 317 && cur_lumi_block <= 593) ||
	  (cur_lumi_block >= 18 && cur_lumi_block <= 140) ) ) ||
	( cur_run_number == 167282 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 441) ) ) ||
	( cur_run_number == 167284 && (
	  (cur_lumi_block >= 476 && cur_lumi_block <= 1157) ||
	  (cur_lumi_block >= 1633 && cur_lumi_block <= 1644) ||
	  (cur_lumi_block >= 356 && cur_lumi_block <= 395) ||
	  (cur_lumi_block >= 1160 && cur_lumi_block <= 1628) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 315) ||
	  (cur_lumi_block >= 320 && cur_lumi_block <= 346) ||
	  (cur_lumi_block >= 399 && cur_lumi_block <= 474) ) ) ||
	( cur_run_number == 167551 && (
	  (cur_lumi_block >= 56 && cur_lumi_block <= 190) ||
	  (cur_lumi_block >= 196 && cur_lumi_block <= 471) ) ) ||
	( cur_run_number == 167673 && (
	  (cur_lumi_block >= 309 && cur_lumi_block <= 418) ||
	  (cur_lumi_block >= 210 && cur_lumi_block <= 236) ||
	  (cur_lumi_block >= 239 && cur_lumi_block <= 305) ||
	  (cur_lumi_block >= 423 && cur_lumi_block <= 447) ) ) ||
	( cur_run_number == 167674 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 345) ) ) ||
	( cur_run_number == 167675 && (
	  (cur_lumi_block >= 811 && cur_lumi_block <= 1062) ||
	  (cur_lumi_block >= 721 && cur_lumi_block <= 725) ||
	  (cur_lumi_block >= 710 && cur_lumi_block <= 712) ||
	  (cur_lumi_block >= 690 && cur_lumi_block <= 707) ||
	  (cur_lumi_block >= 762 && cur_lumi_block <= 770) ||
	  (cur_lumi_block >= 740 && cur_lumi_block <= 741) ||
	  (cur_lumi_block >= 793 && cur_lumi_block <= 797) ||
	  (cur_lumi_block >= 748 && cur_lumi_block <= 758) ||
	  (cur_lumi_block >= 715 && cur_lumi_block <= 716) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 129) ||
	  (cur_lumi_block >= 774 && cur_lumi_block <= 787) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 299) ||
	  (cur_lumi_block >= 719 && cur_lumi_block <= 719) ||
	  (cur_lumi_block >= 301 && cur_lumi_block <= 617) ) ) ||
	( cur_run_number == 167676 && (
	  (cur_lumi_block >= 289 && cur_lumi_block <= 450) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 278) ) ) ||
	( cur_run_number == 167740 && (
	  (cur_lumi_block >= 170 && cur_lumi_block <= 173) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 126) ||
	  (cur_lumi_block >= 132 && cur_lumi_block <= 168) ) ) ||
	( cur_run_number == 167746 && (
	  (cur_lumi_block >= 56 && cur_lumi_block <= 384) ) ) ||
	( cur_run_number == 167754 && (
	  (cur_lumi_block >= 62 && cur_lumi_block <= 103) ) ) ||
	( cur_run_number == 167784 && (
	  (cur_lumi_block >= 51 && cur_lumi_block <= 67) ) ) ||
	( cur_run_number == 167786 && (
	  (cur_lumi_block >= 11 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 81 && cur_lumi_block <= 176) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ) ) ||
	( cur_run_number == 167807 && (
	  (cur_lumi_block >= 1444 && cur_lumi_block <= 1842) ||
	  (cur_lumi_block >= 210 && cur_lumi_block <= 482) ||
	  (cur_lumi_block >= 484 && cur_lumi_block <= 558) ||
	  (cur_lumi_block >= 878 && cur_lumi_block <= 1441) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 159) ||
	  (cur_lumi_block >= 560 && cur_lumi_block <= 872) ||
	  (cur_lumi_block >= 178 && cur_lumi_block <= 204) ) ) ||
	( cur_run_number == 167830 && (
	  (cur_lumi_block >= 590 && cur_lumi_block <= 828) ||
	  (cur_lumi_block >= 442 && cur_lumi_block <= 587) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 437) ||
	  (cur_lumi_block >= 834 && cur_lumi_block <= 1242) ) ) ||
	( cur_run_number == 167898 && (
	  (cur_lumi_block >= 1057 && cur_lumi_block <= 1295) ||
	  (cur_lumi_block >= 1013 && cur_lumi_block <= 1053) ||
	  (cur_lumi_block >= 621 && cur_lumi_block <= 995) ||
	  (cur_lumi_block >= 108 && cur_lumi_block <= 619) ||
	  (cur_lumi_block >= 1001 && cur_lumi_block <= 1010) ||
	  (cur_lumi_block >= 1298 && cur_lumi_block <= 1762) ) ) ||
	( cur_run_number == 167913 && (
	  (cur_lumi_block >= 128 && cur_lumi_block <= 432) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 126) ) ) ||
	( cur_run_number == 170826 && (
	  (cur_lumi_block >= 50 && cur_lumi_block <= 122) ||
	  (cur_lumi_block >= 248 && cur_lumi_block <= 308) ||
	  (cur_lumi_block >= 139 && cur_lumi_block <= 243) ) ) ||
	( cur_run_number == 170842 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 102 && cur_lumi_block <= 331) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 27) ) ) ||
	( cur_run_number == 170854 && (
	  (cur_lumi_block >= 475 && cur_lumi_block <= 578) ||
	  (cur_lumi_block >= 420 && cur_lumi_block <= 470) ||
	  (cur_lumi_block >= 341 && cur_lumi_block <= 414) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 336) ) ) ||
	( cur_run_number == 170876 && (
	  (cur_lumi_block >= 518 && cur_lumi_block <= 518) ||
	  (cur_lumi_block >= 115 && cur_lumi_block <= 295) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 110) ||
	  (cur_lumi_block >= 301 && cur_lumi_block <= 516) ) ) ||
	( cur_run_number == 170896 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 212) ) ) ||
	( cur_run_number == 170899 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 84) ) ) ||
	( cur_run_number == 170901 && (
	  (cur_lumi_block >= 159 && cur_lumi_block <= 200) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 153) ) ) ||
	( cur_run_number == 171050 && (
	  (cur_lumi_block >= 384 && cur_lumi_block <= 423) ||
	  (cur_lumi_block >= 371 && cur_lumi_block <= 379) ||
	  (cur_lumi_block >= 342 && cur_lumi_block <= 369) ||
	  (cur_lumi_block >= 471 && cur_lumi_block <= 648) ||
	  (cur_lumi_block >= 80 && cur_lumi_block <= 337) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 74) ||
	  (cur_lumi_block >= 427 && cur_lumi_block <= 467) ) ) ||
	( cur_run_number == 171091 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 135) ) ) ||
	( cur_run_number == 171098 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 8) ) ) ||
	( cur_run_number == 171102 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 19) ) ) ||
	( cur_run_number == 171106 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 288) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 27) ) ) ||
	( cur_run_number == 171117 && (
	  (cur_lumi_block >= 80 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 78) ) ) ||
	( cur_run_number == 171156 && (
	  (cur_lumi_block >= 111 && cur_lumi_block <= 686) ||
	  (cur_lumi_block >= 42 && cur_lumi_block <= 106) ||
	  (cur_lumi_block >= 688 && cur_lumi_block <= 692) ) ) ||
	( cur_run_number == 171178 && (
	  (cur_lumi_block >= 563 && cur_lumi_block <= 787) ||
	  (cur_lumi_block >= 812 && cur_lumi_block <= 1043) ||
	  (cur_lumi_block >= 210 && cur_lumi_block <= 551) ||
	  (cur_lumi_block >= 97 && cur_lumi_block <= 205) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 92) ||
	  (cur_lumi_block >= 556 && cur_lumi_block <= 557) ||
	  (cur_lumi_block >= 792 && cur_lumi_block <= 810) ) ) ||
	( cur_run_number == 171219 && (
	  (cur_lumi_block >= 153 && cur_lumi_block <= 162) ||
	  (cur_lumi_block >= 48 && cur_lumi_block <= 151) ) ) ||
	( cur_run_number == 171274 && (
	  (cur_lumi_block >= 88 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 143) ) ) ||
	( cur_run_number == 171282 && (
	  (cur_lumi_block >= 104 && cur_lumi_block <= 134) ||
	  (cur_lumi_block >= 14 && cur_lumi_block <= 99) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 171) ) ) ||
	( cur_run_number == 171315 && (
	  (cur_lumi_block >= 53 && cur_lumi_block <= 225) ) ) ||
	( cur_run_number == 171369 && (
	  (cur_lumi_block >= 42 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 136 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 161) ) ) ||
	( cur_run_number == 171446 && (
	  (cur_lumi_block >= 58 && cur_lumi_block <= 144) ) ) ||
	( cur_run_number == 171484 && (
	  (cur_lumi_block >= 207 && cur_lumi_block <= 370) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 202) ||
	  (cur_lumi_block >= 377 && cur_lumi_block <= 432) ) ) ||
	( cur_run_number == 171578 && (
	  (cur_lumi_block >= 578 && cur_lumi_block <= 816) ||
	  (cur_lumi_block >= 156 && cur_lumi_block <= 174) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 150) ||
	  (cur_lumi_block >= 353 && cur_lumi_block <= 480) ||
	  (cur_lumi_block >= 821 && cur_lumi_block <= 974) ||
	  (cur_lumi_block >= 320 && cur_lumi_block <= 347) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 314) ||
	  (cur_lumi_block >= 487 && cur_lumi_block <= 572) ) ) ||
	( cur_run_number == 171812 && (
	  (cur_lumi_block >= 59 && cur_lumi_block <= 296) ||
	  (cur_lumi_block >= 301 && cur_lumi_block <= 438) ) ) ||
	( cur_run_number == 171876 && (
	  (cur_lumi_block >= 368 && cur_lumi_block <= 391) ||
	  (cur_lumi_block >= 397 && cur_lumi_block <= 533) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 362) ) ) ||
	( cur_run_number == 171880 && (
	  (cur_lumi_block >= 19 && cur_lumi_block <= 202) ) ) ||
	( cur_run_number == 171895 && (
	  (cur_lumi_block >= 34 && cur_lumi_block <= 34) ) ) ||
	( cur_run_number == 171897 && (
	  (cur_lumi_block >= 517 && cur_lumi_block <= 542) ||
	  (cur_lumi_block >= 442 && cur_lumi_block <= 511) ||
	  (cur_lumi_block >= 205 && cur_lumi_block <= 437) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 23) ) ) ||
	( cur_run_number == 171921 && (
	  (cur_lumi_block >= 51 && cur_lumi_block <= 141) ) ) ||
	( cur_run_number == 171926 && (
	  (cur_lumi_block >= 161 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 155) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 49) ||
	  (cur_lumi_block >= 177 && cur_lumi_block <= 264) ) ) ||
	( cur_run_number == 172014 && (
	  (cur_lumi_block >= 149 && cur_lumi_block <= 243) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 143) ) ) ||
	( cur_run_number == 172024 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 46) ) ) ||
	( cur_run_number == 172033 && (
	  (cur_lumi_block >= 71 && cur_lumi_block <= 277) ||
	  (cur_lumi_block >= 282 && cur_lumi_block <= 375) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 65) ||
	  (cur_lumi_block >= 380 && cur_lumi_block <= 473) ||
	  (cur_lumi_block >= 478 && cur_lumi_block <= 749) ) ) ||
	( cur_run_number == 172163 && (
	  (cur_lumi_block >= 36 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 115 && cur_lumi_block <= 469) ) ) ||
	( cur_run_number == 172208 && (
	  (cur_lumi_block >= 61 && cur_lumi_block <= 199) ) ) ||
	( cur_run_number == 172252 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 54) ) ) ||
	( cur_run_number == 172254 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 35) ) ) ||
	( cur_run_number == 172255 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 40) ) ) ||
	( cur_run_number == 172268 && (
	  (cur_lumi_block >= 56 && cur_lumi_block <= 169) ) ) ||
	( cur_run_number == 172286 && (
	  (cur_lumi_block >= 184 && cur_lumi_block <= 216) ||
	  (cur_lumi_block >= 52 && cur_lumi_block <= 177) ) ) ||
	( cur_run_number == 172389 && (
	  (cur_lumi_block >= 434 && cur_lumi_block <= 460) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 144) ||
	  (cur_lumi_block >= 150 && cur_lumi_block <= 428) ) ) ||
	( cur_run_number == 172399 && (
	  (cur_lumi_block >= 57 && cur_lumi_block <= 226) ) ) ||
	( cur_run_number == 172400 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 495) ||
	  (cur_lumi_block >= 696 && cur_lumi_block <= 704) ||
	  (cur_lumi_block >= 500 && cur_lumi_block <= 690) ) ) ||
	( cur_run_number == 172401 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ||
	  (cur_lumi_block >= 5 && cur_lumi_block <= 148) ) ) ||
	( cur_run_number == 172411 && (
	  (cur_lumi_block >= 85 && cur_lumi_block <= 349) ) ) ||
	( cur_run_number == 172478 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 110) ) ) ||
	( cur_run_number == 172619 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 77) ) ) ||
	( cur_run_number == 172620 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 495) ) ) ||
	( cur_run_number == 172630 && (
	  (cur_lumi_block >= 36 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 139 && cur_lumi_block <= 160) ||
	  (cur_lumi_block >= 68 && cur_lumi_block <= 134) ) ) ||
	( cur_run_number == 172635 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 18) ||
	  (cur_lumi_block >= 24 && cur_lumi_block <= 268) ) ) ||
	( cur_run_number == 172778 && (
	  (cur_lumi_block >= 48 && cur_lumi_block <= 97) ) ) ||
	( cur_run_number == 172791 && (
	  (cur_lumi_block >= 721 && cur_lumi_block <= 1295) ||
	  (cur_lumi_block >= 571 && cur_lumi_block <= 715) ||
	  (cur_lumi_block >= 418 && cur_lumi_block <= 569) ||
	  (cur_lumi_block >= 1542 && cur_lumi_block <= 1645) ||
	  (cur_lumi_block >= 1300 && cur_lumi_block <= 1536) ||
	  (cur_lumi_block >= 1649 && cur_lumi_block <= 1658) ||
	  (cur_lumi_block >= 65 && cur_lumi_block <= 413) ) ) ||
	( cur_run_number == 172798 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 31) ) ) ||
	( cur_run_number == 172799 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 367) ) ) ||
	( cur_run_number == 172801 && (
	  (cur_lumi_block >= 863 && cur_lumi_block <= 909) ||
	  (cur_lumi_block >= 839 && cur_lumi_block <= 861) ||
	  (cur_lumi_block >= 911 && cur_lumi_block <= 1139) ||
	  (cur_lumi_block >= 681 && cur_lumi_block <= 750) ||
	  (cur_lumi_block >= 819 && cur_lumi_block <= 837) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 679) ||
	  (cur_lumi_block >= 768 && cur_lumi_block <= 815) ||
	  (cur_lumi_block >= 753 && cur_lumi_block <= 766) ) ) ||
	( cur_run_number == 172802 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 629) ||
	  (cur_lumi_block >= 634 && cur_lumi_block <= 784) ) ) ||
	( cur_run_number == 172819 && (
	  (cur_lumi_block >= 57 && cur_lumi_block <= 87) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 254) ) ) ||
	( cur_run_number == 172822 && (
	  (cur_lumi_block >= 667 && cur_lumi_block <= 832) ||
	  (cur_lumi_block >= 1102 && cur_lumi_block <= 1112) ||
	  (cur_lumi_block >= 358 && cur_lumi_block <= 662) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 1118 && cur_lumi_block <= 2121) ||
	  (cur_lumi_block >= 2127 && cur_lumi_block <= 2333) ||
	  (cur_lumi_block >= 837 && cur_lumi_block <= 1096) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 353) ) ) ||
	( cur_run_number == 172824 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 54) ) ) ||
	( cur_run_number == 172847 && (
	  (cur_lumi_block >= 62 && cur_lumi_block <= 131) ) ) ||
	( cur_run_number == 172865 && (
	  (cur_lumi_block >= 36 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 157 && cur_lumi_block <= 382) ) ) ||
	( cur_run_number == 172868 && (
	  (cur_lumi_block >= 714 && cur_lumi_block <= 909) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 29) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 708) ||
	  (cur_lumi_block >= 912 && cur_lumi_block <= 1829) ||
	  (cur_lumi_block >= 1835 && cur_lumi_block <= 1970) ) ) ||
	( cur_run_number == 172949 && (
	  (cur_lumi_block >= 161 && cur_lumi_block <= 263) ||
	  (cur_lumi_block >= 276 && cur_lumi_block <= 892) ||
	  (cur_lumi_block >= 933 && cur_lumi_block <= 1287) ||
	  (cur_lumi_block >= 269 && cur_lumi_block <= 274) ||
	  (cur_lumi_block >= 898 && cur_lumi_block <= 928) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 159) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 127) ) ) ||
	( cur_run_number == 172951 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 52) ) ) ||
	( cur_run_number == 172952 && (
	  (cur_lumi_block >= 1169 && cur_lumi_block <= 1562) ||
	  (cur_lumi_block >= 197 && cur_lumi_block <= 670) ||
	  (cur_lumi_block >= 675 && cur_lumi_block <= 797) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 195) ||
	  (cur_lumi_block >= 799 && cur_lumi_block <= 1164) ) ) ||
	( cur_run_number == 172992 && (
	  (cur_lumi_block >= 516 && cur_lumi_block <= 713) ||
	  (cur_lumi_block >= 752 && cur_lumi_block <= 874) ||
	  (cur_lumi_block >= 505 && cur_lumi_block <= 511) ||
	  (cur_lumi_block >= 880 && cur_lumi_block <= 946) ) ) ||
	( cur_run_number == 172999 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 71) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 294) ) ) ||
	( cur_run_number == 173198 && (
	  (cur_lumi_block >= 488 && cur_lumi_block <= 768) ||
	  (cur_lumi_block >= 770 && cur_lumi_block <= 823) ||
	  (cur_lumi_block >= 362 && cur_lumi_block <= 485) ||
	  (cur_lumi_block >= 324 && cur_lumi_block <= 356) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 322) ) ) ||
	( cur_run_number == 173236 && (
	  (cur_lumi_block >= 128 && cur_lumi_block <= 231) ) ) ||
	( cur_run_number == 173240 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 124) ) ) ||
	( cur_run_number == 173241 && (
	  (cur_lumi_block >= 92 && cur_lumi_block <= 381) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 85) ||
	  (cur_lumi_block >= 388 && cur_lumi_block <= 759) ) ) ||
	( cur_run_number == 173243 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 89) ) ) ||
	( cur_run_number == 173380 && (
	  (cur_lumi_block >= 217 && cur_lumi_block <= 217) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 204) ||
	  (cur_lumi_block >= 209 && cur_lumi_block <= 209) ) ) ||
	( cur_run_number == 173381 && (
	  (cur_lumi_block >= 139 && cur_lumi_block <= 221) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 226) ||
	  (cur_lumi_block >= 231 && cur_lumi_block <= 294) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 134) ) ) ||
	( cur_run_number == 173389 && (
	  (cur_lumi_block >= 276 && cur_lumi_block <= 653) ||
	  (cur_lumi_block >= 230 && cur_lumi_block <= 262) ||
	  (cur_lumi_block >= 111 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 174 && cur_lumi_block <= 224) ||
	  (cur_lumi_block >= 18 && cur_lumi_block <= 106) ) ) ||
	( cur_run_number == 173406 && (
	  (cur_lumi_block >= 37 && cur_lumi_block <= 286) ) ) ||
	( cur_run_number == 173430 && (
	  (cur_lumi_block >= 72 && cur_lumi_block <= 163) ) ) ||
	( cur_run_number == 173438 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 56) ) ) ||
	( cur_run_number == 173439 && (
	  (cur_lumi_block >= 280 && cur_lumi_block <= 313) ||
	  (cur_lumi_block >= 472 && cur_lumi_block <= 756) ||
	  (cur_lumi_block >= 319 && cur_lumi_block <= 467) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 274) ) ) ||
	( cur_run_number == 173657 && (
	  (cur_lumi_block >= 59 && cur_lumi_block <= 93) ) ) ||
	( cur_run_number == 173658 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 110) ) ) ||
	( cur_run_number == 173659 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 61) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 317) ) ) ||
	( cur_run_number == 173660 && (
	  (cur_lumi_block >= 359 && cur_lumi_block <= 362) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 81) ||
	  (cur_lumi_block >= 169 && cur_lumi_block <= 357) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 164) ) ) ||
	( cur_run_number == 173661 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 56) ||
	  (cur_lumi_block >= 65 && cur_lumi_block <= 125) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 63) ) ) ||
	( cur_run_number == 173663 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 71) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 120) ) ) ||
	( cur_run_number == 173664 && (
	  (cur_lumi_block >= 5 && cur_lumi_block <= 16) ) ) ||
	( cur_run_number == 173692 && (
	  (cur_lumi_block >= 149 && cur_lumi_block <= 468) ||
	  (cur_lumi_block >= 50 && cur_lumi_block <= 143) ||
	  (cur_lumi_block >= 1824 && cur_lumi_block <= 2361) ||
	  (cur_lumi_block >= 1168 && cur_lumi_block <= 1321) ||
	  (cur_lumi_block >= 1326 && cur_lumi_block <= 1361) ||
	  (cur_lumi_block >= 1389 && cur_lumi_block <= 1431) ||
	  (cur_lumi_block >= 474 && cur_lumi_block <= 1123) ||
	  (cur_lumi_block >= 2366 && cur_lumi_block <= 2710) ||
	  (cur_lumi_block >= 1129 && cur_lumi_block <= 1162) ||
	  (cur_lumi_block >= 1366 && cur_lumi_block <= 1383) ||
	  (cur_lumi_block >= 2715 && cur_lumi_block <= 2754) ||
	  (cur_lumi_block >= 1436 && cur_lumi_block <= 1818) ) ) ||
	( cur_run_number == 175860 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 82) ) ) ||
	( cur_run_number == 175863 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 75) ) ) ||
	( cur_run_number == 175865 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 58) ) ) ||
	( cur_run_number == 175866 && (
	  (cur_lumi_block >= 356 && cur_lumi_block <= 511) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 353) ) ) ||
	( cur_run_number == 175872 && (
	  (cur_lumi_block >= 60 && cur_lumi_block <= 90) ) ) ||
	( cur_run_number == 175873 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 32) ) ) ||
	( cur_run_number == 175874 && (
	  (cur_lumi_block >= 36 && cur_lumi_block <= 108) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 33) ) ) ||
	( cur_run_number == 175877 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 69) ) ) ||
	( cur_run_number == 175881 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 160) ) ) ||
	( cur_run_number == 175886 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 280) ) ) ||
	( cur_run_number == 175887 && (
	  (cur_lumi_block >= 21 && cur_lumi_block <= 138) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 18) ) ) ||
	( cur_run_number == 175888 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 424) ) ) ||
	( cur_run_number == 175906 && (
	  (cur_lumi_block >= 79 && cur_lumi_block <= 191) ) ) ||
	( cur_run_number == 175910 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ) ) ||
	( cur_run_number == 175921 && (
	  (cur_lumi_block >= 64 && cur_lumi_block <= 428) ) ) ||
	( cur_run_number == 175973 && (
	  (cur_lumi_block >= 186 && cur_lumi_block <= 268) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 183) ) ) ||
	( cur_run_number == 175974 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 124) ) ) ||
	( cur_run_number == 175975 && (
	  (cur_lumi_block >= 104 && cur_lumi_block <= 390) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 90) ||
	  (cur_lumi_block >= 93 && cur_lumi_block <= 101) ) ) ||
	( cur_run_number == 175976 && (
	  (cur_lumi_block >= 36 && cur_lumi_block <= 51) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 33) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 102) ) ) ||
	( cur_run_number == 175990 && (
	  (cur_lumi_block >= 56 && cur_lumi_block <= 164) ) ) ||
	( cur_run_number == 176023 && (
	  (cur_lumi_block >= 44 && cur_lumi_block <= 129) ) ) ||
	( cur_run_number == 176161 && (
	  (cur_lumi_block >= 6 && cur_lumi_block <= 7) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 23) ||
	  (cur_lumi_block >= 13 && cur_lumi_block <= 14) ) ) ||
	( cur_run_number == 176163 && (
	  (cur_lumi_block >= 7 && cur_lumi_block <= 63) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ) ) ||
	( cur_run_number == 176165 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 29) ) ) ||
	( cur_run_number == 176167 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 5) ) ) ||
	( cur_run_number == 176169 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 140) ) ) ||
	( cur_run_number == 176201 && (
	  (cur_lumi_block >= 67 && cur_lumi_block <= 263) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 63) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 58) ||
	  (cur_lumi_block >= 52 && cur_lumi_block <= 52) ||
	  (cur_lumi_block >= 265 && cur_lumi_block <= 570) ||
	  (cur_lumi_block >= 573 && cur_lumi_block <= 656) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 55) ) ) ||
	( cur_run_number == 176202 && (
	  (cur_lumi_block >= 182 && cur_lumi_block <= 264) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 179) ) ) ||
	( cur_run_number == 176206 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 101) ) ) ||
	( cur_run_number == 176207 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 230) ) ) ||
	( cur_run_number == 176286 && (
	  (cur_lumi_block >= 250 && cur_lumi_block <= 252) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 247) ||
	  (cur_lumi_block >= 254 && cur_lumi_block <= 486) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 224) ) ) ||
	( cur_run_number == 176289 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 43) ||
	  (cur_lumi_block >= 46 && cur_lumi_block <= 75) ) ) ||
	( cur_run_number == 176304 && (
	  (cur_lumi_block >= 425 && cur_lumi_block <= 516) ||
	  (cur_lumi_block >= 135 && cur_lumi_block <= 422) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 132) ||
	  (cur_lumi_block >= 69 && cur_lumi_block <= 69) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 73) ) ) ||
	( cur_run_number == 176308 && (
	  (cur_lumi_block >= 48 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 45) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 293) ) ) ||
	( cur_run_number == 176309 && (
	  (cur_lumi_block >= 630 && cur_lumi_block <= 1177) ||
	  (cur_lumi_block >= 470 && cur_lumi_block <= 512) ||
	  (cur_lumi_block >= 1180 && cur_lumi_block <= 1641) ||
	  (cur_lumi_block >= 356 && cur_lumi_block <= 467) ||
	  (cur_lumi_block >= 87 && cur_lumi_block <= 122) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 514 && cur_lumi_block <= 627) ||
	  (cur_lumi_block >= 125 && cur_lumi_block <= 354) ) ) ||
	( cur_run_number == 176467 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 85) ||
	  (cur_lumi_block >= 88 && cur_lumi_block <= 164) ) ) ||
	( cur_run_number == 176468 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 124) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 210) ) ) ||
	( cur_run_number == 176469 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 5) ) ) ||
	( cur_run_number == 176545 && (
	  (cur_lumi_block >= 5 && cur_lumi_block <= 5) ) ) ||
	( cur_run_number == 176547 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 154 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 184) ) ) ||
	( cur_run_number == 176548 && (
	  (cur_lumi_block >= 577 && cur_lumi_block <= 1024) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 387) ||
	  (cur_lumi_block >= 390 && cur_lumi_block <= 487) ||
	  (cur_lumi_block >= 489 && cur_lumi_block <= 494) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 496 && cur_lumi_block <= 575) ) ) ||
	( cur_run_number == 176697 && (
	  (cur_lumi_block >= 50 && cur_lumi_block <= 66) ||
	  (cur_lumi_block >= 68 && cur_lumi_block <= 344) ) ) ||
	( cur_run_number == 176701 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 145) ) ) ||
	( cur_run_number == 176702 && (
	  (cur_lumi_block >= 6 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 287 && cur_lumi_block <= 291) ||
	  (cur_lumi_block >= 801 && cur_lumi_block <= 839) ||
	  (cur_lumi_block >= 726 && cur_lumi_block <= 794) ||
	  (cur_lumi_block >= 622 && cur_lumi_block <= 684) ||
	  (cur_lumi_block >= 796 && cur_lumi_block <= 799) ||
	  (cur_lumi_block >= 353 && cur_lumi_block <= 599) ||
	  (cur_lumi_block >= 294 && cur_lumi_block <= 351) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ||
	  (cur_lumi_block >= 45 && cur_lumi_block <= 103) ||
	  (cur_lumi_block >= 686 && cur_lumi_block <= 723) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 284) ) ) ||
	( cur_run_number == 176765 && (
	  (cur_lumi_block >= 84 && cur_lumi_block <= 141) ) ) ||
	( cur_run_number == 176771 && (
	  (cur_lumi_block >= 57 && cur_lumi_block <= 60) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 156) ) ) ||
	( cur_run_number == 176795 && (
	  (cur_lumi_block >= 38 && cur_lumi_block <= 105) ) ) ||
	( cur_run_number == 176796 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 24) ||
	  (cur_lumi_block >= 31 && cur_lumi_block <= 100) ) ) ||
	( cur_run_number == 176797 && (
	  (cur_lumi_block >= 203 && cur_lumi_block <= 237) ||
	  (cur_lumi_block >= 33 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 95 && cur_lumi_block <= 131) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 26) ||
	  (cur_lumi_block >= 244 && cur_lumi_block <= 252) ||
	  (cur_lumi_block >= 136 && cur_lumi_block <= 196) ) ) ||
	( cur_run_number == 176799 && (
	  (cur_lumi_block >= 38 && cur_lumi_block <= 238) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 27) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 35) ) ) ||
	( cur_run_number == 176801 && (
	  (cur_lumi_block >= 95 && cur_lumi_block <= 173) ||
	  (cur_lumi_block >= 176 && cur_lumi_block <= 177) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 92) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 249) ) ) ||
	( cur_run_number == 176805 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 97) ) ) ||
	( cur_run_number == 176807 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 74) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 255) ||
	  (cur_lumi_block >= 257 && cur_lumi_block <= 461) ) ) ||
	( cur_run_number == 176841 && (
	  (cur_lumi_block >= 238 && cur_lumi_block <= 241) ||
	  (cur_lumi_block >= 159 && cur_lumi_block <= 182) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 235) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 157) ) ) ||
	( cur_run_number == 176844 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 110) ) ) ||
	( cur_run_number == 176848 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 25) ) ) ||
	( cur_run_number == 176850 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 301) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 29) ) ) ||
	( cur_run_number == 176860 && (
	  (cur_lumi_block >= 273 && cur_lumi_block <= 335) ||
	  (cur_lumi_block >= 81 && cur_lumi_block <= 271) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 78) ||
	  (cur_lumi_block >= 338 && cur_lumi_block <= 450) ) ) ||
	( cur_run_number == 176868 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 495) ) ) ||
	( cur_run_number == 176885 && (
	  (cur_lumi_block >= 67 && cur_lumi_block <= 152) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 235) ) ) ||
	( cur_run_number == 176886 && (
	  (cur_lumi_block >= 516 && cur_lumi_block <= 678) ||
	  (cur_lumi_block >= 418 && cur_lumi_block <= 425) ||
	  (cur_lumi_block >= 502 && cur_lumi_block <= 503) ||
	  (cur_lumi_block >= 509 && cur_lumi_block <= 514) ||
	  (cur_lumi_block >= 505 && cur_lumi_block <= 505) ||
	  (cur_lumi_block >= 427 && cur_lumi_block <= 429) ||
	  (cur_lumi_block >= 431 && cur_lumi_block <= 499) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 335) ||
	  (cur_lumi_block >= 337 && cur_lumi_block <= 416) ||
	  (cur_lumi_block >= 507 && cur_lumi_block <= 507) ) ) ||
	( cur_run_number == 176889 && (
	  (cur_lumi_block >= 83 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 81) ) ) ||
	( cur_run_number == 176928 && (
	  (cur_lumi_block >= 102 && cur_lumi_block <= 121) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 99) ) ) ||
	( cur_run_number == 176929 && (
	  (cur_lumi_block >= 221 && cur_lumi_block <= 320) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 59) ||
	  (cur_lumi_block >= 201 && cur_lumi_block <= 218) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 198) ) ) ||
	( cur_run_number == 176933 && (
	  (cur_lumi_block >= 418 && cur_lumi_block <= 438) ||
	  (cur_lumi_block >= 361 && cur_lumi_block <= 415) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 71) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 358) ) ) ||
	( cur_run_number == 176959 && (
	  (cur_lumi_block >= 17 && cur_lumi_block <= 39) ) ) ||
	( cur_run_number == 176982 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 10) ) ) ||
	( cur_run_number == 177053 && (
	  (cur_lumi_block >= 74 && cur_lumi_block <= 87) ||
	  (cur_lumi_block >= 90 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 712 && cur_lumi_block <= 740) ||
	  (cur_lumi_block >= 385 && cur_lumi_block <= 427) ||
	  (cur_lumi_block >= 429 && cur_lumi_block <= 570) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 217) ||
	  (cur_lumi_block >= 219 && cur_lumi_block <= 258) ||
	  (cur_lumi_block >= 260 && cur_lumi_block <= 380) ||
	  (cur_lumi_block >= 573 && cur_lumi_block <= 709) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 72) ) ) ||
	( cur_run_number == 177074 && (
	  (cur_lumi_block >= 123 && cur_lumi_block <= 468) ||
	  (cur_lumi_block >= 470 && cur_lumi_block <= 554) ||
	  (cur_lumi_block >= 52 && cur_lumi_block <= 120) ||
	  (cur_lumi_block >= 737 && cur_lumi_block <= 769) ||
	  (cur_lumi_block >= 556 && cur_lumi_block <= 735) ) ) ||
	( cur_run_number == 177088 && (
	  (cur_lumi_block >= 85 && cur_lumi_block <= 95) ||
	  (cur_lumi_block >= 57 && cur_lumi_block <= 83) ) ) ||
	( cur_run_number == 177095 && (
	  (cur_lumi_block >= 49 && cur_lumi_block <= 111) ||
	  (cur_lumi_block >= 113 && cur_lumi_block <= 204) ) ) ||
	( cur_run_number == 177096 && (
	  (cur_lumi_block >= 17 && cur_lumi_block <= 190) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 14) ) ) ||
	( cur_run_number == 177131 && (
	  (cur_lumi_block >= 49 && cur_lumi_block <= 139) ) ) ||
	( cur_run_number == 177138 && (
	  (cur_lumi_block >= 59 && cur_lumi_block <= 114) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 57) ) ) ||
	( cur_run_number == 177139 && (
	  (cur_lumi_block >= 252 && cur_lumi_block <= 335) ||
	  (cur_lumi_block >= 208 && cur_lumi_block <= 249) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 206) ||
	  (cur_lumi_block >= 337 && cur_lumi_block <= 375) ||
	  (cur_lumi_block >= 378 && cur_lumi_block <= 627) ) ) ||
	( cur_run_number == 177140 && (
	  (cur_lumi_block >= 108 && cur_lumi_block <= 492) ||
	  (cur_lumi_block >= 495 && cur_lumi_block <= 557) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 106) ) ) ||
	( cur_run_number == 177141 && (
	  (cur_lumi_block >= 59 && cur_lumi_block <= 154) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 494 && cur_lumi_block <= 628) ||
	  (cur_lumi_block >= 157 && cur_lumi_block <= 288) ||
	  (cur_lumi_block >= 290 && cur_lumi_block <= 492) ) ) ||
	( cur_run_number == 177183 && (
	  (cur_lumi_block >= 202 && cur_lumi_block <= 219) ||
	  (cur_lumi_block >= 4 && cur_lumi_block <= 74) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 199) ) ) ||
	( cur_run_number == 177184 && (
	  (cur_lumi_block >= 15 && cur_lumi_block <= 41) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 12) ) ) ||
	( cur_run_number == 177201 && (
	  (cur_lumi_block >= 519 && cur_lumi_block <= 603) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 212) ||
	  (cur_lumi_block >= 68 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 606 && cur_lumi_block <= 621) ||
	  (cur_lumi_block >= 215 && cur_lumi_block <= 334) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 65) ||
	  (cur_lumi_block >= 623 && cur_lumi_block <= 799) ||
	  (cur_lumi_block >= 337 && cur_lumi_block <= 516) ) ) ||
	( cur_run_number == 177222 && (
	  (cur_lumi_block >= 50 && cur_lumi_block <= 342) ) ) ||
	( cur_run_number == 177317 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 60) ) ) ||
	( cur_run_number == 177318 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 81) ) ) ||
	( cur_run_number == 177319 && (
	  (cur_lumi_block >= 289 && cur_lumi_block <= 351) ||
	  (cur_lumi_block >= 227 && cur_lumi_block <= 286) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 354 && cur_lumi_block <= 368) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 224) ) ) ||
	( cur_run_number == 177449 && (
	  (cur_lumi_block >= 83 && cur_lumi_block <= 234) ||
	  (cur_lumi_block >= 236 && cur_lumi_block <= 392) ||
	  (cur_lumi_block >= 394 && cur_lumi_block <= 434) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 80) ) ) ||
	( cur_run_number == 177452 && (
	  (cur_lumi_block >= 158 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 156) ) ) ||
	( cur_run_number == 177718 && (
	  (cur_lumi_block >= 53 && cur_lumi_block <= 62) ||
	  (cur_lumi_block >= 659 && cur_lumi_block <= 697) ||
	  (cur_lumi_block >= 64 && cur_lumi_block <= 482) ||
	  (cur_lumi_block >= 703 && cur_lumi_block <= 703) ||
	  (cur_lumi_block >= 706 && cur_lumi_block <= 1572) ||
	  (cur_lumi_block >= 699 && cur_lumi_block <= 701) ||
	  (cur_lumi_block >= 1575 && cur_lumi_block <= 1635) ||
	  (cur_lumi_block >= 485 && cur_lumi_block <= 656) ) ) ||
	( cur_run_number == 177719 && (
	  (cur_lumi_block >= 197 && cur_lumi_block <= 694) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 194) ||
	  (cur_lumi_block >= 696 && cur_lumi_block <= 704) ) ) ||
	( cur_run_number == 177730 && (
	  (cur_lumi_block >= 237 && cur_lumi_block <= 448) ||
	  (cur_lumi_block >= 2268 && cur_lumi_block <= 2303) ||
	  (cur_lumi_block >= 704 && cur_lumi_block <= 773) ||
	  (cur_lumi_block >= 933 && cur_lumi_block <= 1470) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 234) ||
	  (cur_lumi_block >= 451 && cur_lumi_block <= 701) ||
	  (cur_lumi_block >= 1473 && cur_lumi_block <= 1763) ||
	  (cur_lumi_block >= 1766 && cur_lumi_block <= 1956) ||
	  (cur_lumi_block >= 776 && cur_lumi_block <= 931) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 1959 && cur_lumi_block <= 2266) ) ) ||
	( cur_run_number == 177776 && (
	  (cur_lumi_block >= 73 && cur_lumi_block <= 108) ) ) ||
	( cur_run_number == 177782 && (
	  (cur_lumi_block >= 78 && cur_lumi_block <= 226) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 75) ) ) ||
	( cur_run_number == 177783 && (
	  (cur_lumi_block >= 237 && cur_lumi_block <= 363) ||
	  (cur_lumi_block >= 97 && cur_lumi_block <= 176) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 94) ||
	  (cur_lumi_block >= 178 && cur_lumi_block <= 234) ) ) ||
	( cur_run_number == 177788 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 46) ) ) ||
	( cur_run_number == 177789 && (
	  (cur_lumi_block >= 84 && cur_lumi_block <= 86) ||
	  (cur_lumi_block >= 41 && cur_lumi_block <= 82) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 38) ) ) ||
	( cur_run_number == 177790 && (
	  (cur_lumi_block >= 191 && cur_lumi_block <= 715) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 189) ) ) ||
	( cur_run_number == 177791 && (
	  (cur_lumi_block >= 137 && cur_lumi_block <= 198) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 27) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 109) ) ) ||
	( cur_run_number == 177875 && (
	  (cur_lumi_block >= 370 && cur_lumi_block <= 448) ||
	  (cur_lumi_block >= 217 && cur_lumi_block <= 367) ||
	  (cur_lumi_block >= 71 && cur_lumi_block <= 214) ||
	  (cur_lumi_block >= 451 && cur_lumi_block <= 589) ) ) ||
	( cur_run_number == 177878 && (
	  (cur_lumi_block >= 839 && cur_lumi_block <= 849) ||
	  (cur_lumi_block >= 143 && cur_lumi_block <= 492) ||
	  (cur_lumi_block >= 495 && cur_lumi_block <= 837) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 138) ) ) ||
	( cur_run_number == 178098 && (
	  (cur_lumi_block >= 251 && cur_lumi_block <= 549) ||
	  (cur_lumi_block >= 155 && cur_lumi_block <= 179) ||
	  (cur_lumi_block >= 181 && cur_lumi_block <= 249) ||
	  (cur_lumi_block >= 679 && cur_lumi_block <= 707) ||
	  (cur_lumi_block >= 579 && cur_lumi_block <= 619) ||
	  (cur_lumi_block >= 122 && cur_lumi_block <= 142) ||
	  (cur_lumi_block >= 622 && cur_lumi_block <= 677) ||
	  (cur_lumi_block >= 551 && cur_lumi_block <= 576) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 152) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 120) ) ) ||
	( cur_run_number == 178099 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 192) ) ) ||
	( cur_run_number == 178100 && (
	  (cur_lumi_block >= 1117 && cur_lumi_block <= 1613) ||
	  (cur_lumi_block >= 166 && cur_lumi_block <= 511) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 40) ||
	  (cur_lumi_block >= 514 && cur_lumi_block <= 529) ||
	  (cur_lumi_block >= 532 && cur_lumi_block <= 1114) ||
	  (cur_lumi_block >= 43 && cur_lumi_block <= 163) ) ) ||
	( cur_run_number == 178101 && (
	  (cur_lumi_block >= 64 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 61) ) ) ||
	( cur_run_number == 178102 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 13) ) ) ||
	( cur_run_number == 178110 && (
	  (cur_lumi_block >= 252 && cur_lumi_block <= 396) ||
	  (cur_lumi_block >= 199 && cur_lumi_block <= 246) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 133) ||
	  (cur_lumi_block >= 135 && cur_lumi_block <= 197) ||
	  (cur_lumi_block >= 401 && cur_lumi_block <= 468) ) ) ||
	( cur_run_number == 178116 && (
	  (cur_lumi_block >= 122 && cur_lumi_block <= 615) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 120) ) ) ||
	( cur_run_number == 178151 && (
	  (cur_lumi_block >= 47 && cur_lumi_block <= 122) ) ) ||
	( cur_run_number == 178160 && (
	  (cur_lumi_block >= 458 && cur_lumi_block <= 534) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 456) ) ) ||
	( cur_run_number == 178162 && (
	  (cur_lumi_block >= 149 && cur_lumi_block <= 175) ||
	  (cur_lumi_block >= 7 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 146) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ) ) ||
	( cur_run_number == 178365 && (
	  (cur_lumi_block >= 241 && cur_lumi_block <= 380) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 238) ||
	  (cur_lumi_block >= 560 && cur_lumi_block <= 660) ||
	  (cur_lumi_block >= 1053 && cur_lumi_block <= 1059) ||
	  (cur_lumi_block >= 382 && cur_lumi_block <= 558) ||
	  (cur_lumi_block >= 43 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 663 && cur_lumi_block <= 1050) ) ) ||
	( cur_run_number == 178367 && (
	  (cur_lumi_block >= 713 && cur_lumi_block <= 737) ||
	  (cur_lumi_block >= 369 && cur_lumi_block <= 392) ||
	  (cur_lumi_block >= 138 && cur_lumi_block <= 161) ||
	  (cur_lumi_block >= 266 && cur_lumi_block <= 316) ||
	  (cur_lumi_block >= 163 && cur_lumi_block <= 264) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 135) ||
	  (cur_lumi_block >= 560 && cur_lumi_block <= 711) ||
	  (cur_lumi_block >= 395 && cur_lumi_block <= 548) ||
	  (cur_lumi_block >= 318 && cur_lumi_block <= 366) ) ) ||
	( cur_run_number == 178380 && (
	  (cur_lumi_block >= 84 && cur_lumi_block <= 195) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 81) ) ) ||
	( cur_run_number == 178420 && (
	  (cur_lumi_block >= 63 && cur_lumi_block <= 70) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 61) ) ) ||
	( cur_run_number == 178421 && (
	  (cur_lumi_block >= 588 && cur_lumi_block <= 742) ||
	  (cur_lumi_block >= 323 && cur_lumi_block <= 512) ||
	  (cur_lumi_block >= 295 && cur_lumi_block <= 320) ||
	  (cur_lumi_block >= 917 && cur_lumi_block <= 1207) ||
	  (cur_lumi_block >= 268 && cur_lumi_block <= 293) ||
	  (cur_lumi_block >= 744 && cur_lumi_block <= 756) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 135) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 137 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 3) ||
	  (cur_lumi_block >= 759 && cur_lumi_block <= 915) ||
	  (cur_lumi_block >= 514 && cur_lumi_block <= 586) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 265) ) ) ||
	( cur_run_number == 178424 && (
	  (cur_lumi_block >= 538 && cur_lumi_block <= 594) ||
	  (cur_lumi_block >= 597 && cur_lumi_block <= 837) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 536) ) ) ||
	( cur_run_number == 178479 && (
	  (cur_lumi_block >= 578 && cur_lumi_block <= 985) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 356 && cur_lumi_block <= 576) ||
	  (cur_lumi_block >= 132 && cur_lumi_block <= 245) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 129) ||
	  (cur_lumi_block >= 988 && cur_lumi_block <= 1004) ||
	  (cur_lumi_block >= 247 && cur_lumi_block <= 354) ) ) ||
	( cur_run_number == 178703 && (
	  (cur_lumi_block >= 43 && cur_lumi_block <= 183) ) ) ||
	( cur_run_number == 178708 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 374) ) ) ||
	( cur_run_number == 178712 && (
	  (cur_lumi_block >= 199 && cur_lumi_block <= 333) ||
	  (cur_lumi_block >= 336 && cur_lumi_block <= 394) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 197) ) ) ||
	( cur_run_number == 178724 && (
	  (cur_lumi_block >= 3 && cur_lumi_block <= 261) ) ) ||
	( cur_run_number == 178731 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 208) ) ) ||
	( cur_run_number == 178738 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 143) ||
	  (cur_lumi_block >= 145 && cur_lumi_block <= 252) ) ) ||
	( cur_run_number == 178786 && (
	  (cur_lumi_block >= 123 && cur_lumi_block <= 135) ||
	  (cur_lumi_block >= 120 && cur_lumi_block <= 120) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 67) ||
	  (cur_lumi_block >= 70 && cur_lumi_block <= 117) ||
	  (cur_lumi_block >= 138 && cur_lumi_block <= 202) ||
	  (cur_lumi_block >= 220 && cur_lumi_block <= 227) ||
	  (cur_lumi_block >= 205 && cur_lumi_block <= 218) ) ) ||
	( cur_run_number == 178803 && (
	  (cur_lumi_block >= 319 && cur_lumi_block <= 457) ||
	  (cur_lumi_block >= 49 && cur_lumi_block <= 115) ||
	  (cur_lumi_block >= 117 && cur_lumi_block <= 316) ) ) ||
	( cur_run_number == 178840 && (
	  (cur_lumi_block >= 339 && cur_lumi_block <= 341) ||
	  (cur_lumi_block >= 122 && cur_lumi_block <= 336) ||
	  (cur_lumi_block >= 569 && cur_lumi_block <= 597) ||
	  (cur_lumi_block >= 48 && cur_lumi_block <= 119) ||
	  (cur_lumi_block >= 343 && cur_lumi_block <= 566) ) ) ||
	( cur_run_number == 178854 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 134) ) ) ||
	( cur_run_number == 178866 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 424) ) ) ||
	( cur_run_number == 178871 && (
	  (cur_lumi_block >= 159 && cur_lumi_block <= 380) ||
	  (cur_lumi_block >= 67 && cur_lumi_block <= 71) ||
	  (cur_lumi_block >= 468 && cur_lumi_block <= 1181) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 382 && cur_lumi_block <= 466) ||
	  (cur_lumi_block >= 1184 && cur_lumi_block <= 1273) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 156) ) ) ||
	( cur_run_number == 178920 && (
	  (cur_lumi_block >= 53 && cur_lumi_block <= 291) ) ) ||
	( cur_run_number == 178970 && (
	  (cur_lumi_block >= 52 && cur_lumi_block <= 242) ||
	  (cur_lumi_block >= 244 && cur_lumi_block <= 581) ) ) ||
	( cur_run_number == 178985 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 81) ) ) ||
	( cur_run_number == 179411 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 112) ) ) ||
	( cur_run_number == 179434 && (
	  (cur_lumi_block >= 21 && cur_lumi_block <= 289) ||
	  (cur_lumi_block >= 291 && cur_lumi_block <= 388) ||
	  (cur_lumi_block >= 390 && cur_lumi_block <= 607) ||
	  (cur_lumi_block >= 609 && cur_lumi_block <= 748) ) ) ||
	( cur_run_number == 179452 && (
	  (cur_lumi_block >= 833 && cur_lumi_block <= 849) ||
	  (cur_lumi_block >= 124 && cur_lumi_block <= 158) ||
	  (cur_lumi_block >= 518 && cur_lumi_block <= 830) ||
	  (cur_lumi_block >= 160 && cur_lumi_block <= 276) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 108) ||
	  (cur_lumi_block >= 1118 && cur_lumi_block <= 1233) ||
	  (cur_lumi_block >= 851 && cur_lumi_block <= 1116) ||
	  (cur_lumi_block >= 278 && cur_lumi_block <= 289) ||
	  (cur_lumi_block >= 292 && cur_lumi_block <= 515) ||
	  (cur_lumi_block >= 110 && cur_lumi_block <= 121) ) ) ||
	( cur_run_number == 179476 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 16) ||
	  (cur_lumi_block >= 19 && cur_lumi_block <= 207) ) ) ||
	( cur_run_number == 179497 && (
	  (cur_lumi_block >= 283 && cur_lumi_block <= 402) ||
	  (cur_lumi_block >= 134 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 118 && cur_lumi_block <= 131) ||
	  (cur_lumi_block >= 277 && cur_lumi_block <= 280) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 274) ||
	  (cur_lumi_block >= 405 && cur_lumi_block <= 433) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 116) ) ) ||
	( cur_run_number == 179547 && (
	  (cur_lumi_block >= 75 && cur_lumi_block <= 156) ||
	  (cur_lumi_block >= 158 && cur_lumi_block <= 162) ||
	  (cur_lumi_block >= 265 && cur_lumi_block <= 338) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 262) ) ) ||
	( cur_run_number == 179558 && (
	  (cur_lumi_block >= 75 && cur_lumi_block <= 134) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 52) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 64) ) ) ||
	( cur_run_number == 179563 && (
	  (cur_lumi_block >= 443 && cur_lumi_block <= 631) ||
	  (cur_lumi_block >= 1141 && cur_lumi_block <= 1186) ||
	  (cur_lumi_block >= 970 && cur_lumi_block <= 1085) ||
	  (cur_lumi_block >= 915 && cur_lumi_block <= 968) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 440) ||
	  (cur_lumi_block >= 633 && cur_lumi_block <= 704) ||
	  (cur_lumi_block >= 724 && cur_lumi_block <= 912) ||
	  (cur_lumi_block >= 707 && cur_lumi_block <= 722) ||
	  (cur_lumi_block >= 1087 && cur_lumi_block <= 1138) ) ) ||
	( cur_run_number == 179889 && (
	  (cur_lumi_block >= 466 && cur_lumi_block <= 497) ||
	  (cur_lumi_block >= 142 && cur_lumi_block <= 448) ||
	  (cur_lumi_block >= 70 && cur_lumi_block <= 139) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 68) ||
	  (cur_lumi_block >= 450 && cur_lumi_block <= 463) ||
	  (cur_lumi_block >= 500 && cur_lumi_block <= 534) ) ) ||
	( cur_run_number == 179959 && (
	  (cur_lumi_block >= 69 && cur_lumi_block <= 86) ) ) ||
	( cur_run_number == 179977 && (
	  (cur_lumi_block >= 53 && cur_lumi_block <= 114) ||
	  (cur_lumi_block >= 116 && cur_lumi_block <= 136) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 51) ) ) ||
	( cur_run_number == 180072 && (
	  (cur_lumi_block >= 49 && cur_lumi_block <= 102) ) ) ||
	( cur_run_number == 180076 && (
	  (cur_lumi_block >= 184 && cur_lumi_block <= 356) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 182) ) ) ||
	( cur_run_number == 180093 && (
	  (cur_lumi_block >= 295 && cur_lumi_block <= 346) ||
	  (cur_lumi_block >= 263 && cur_lumi_block <= 292) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 260) ) ) ||
	( cur_run_number == 180241 && (
	  (cur_lumi_block >= 80 && cur_lumi_block <= 252) ||
	  (cur_lumi_block >= 78 && cur_lumi_block <= 78) ||
	  (cur_lumi_block >= 331 && cur_lumi_block <= 351) ||
	  (cur_lumi_block >= 353 && cur_lumi_block <= 416) ||
	  (cur_lumi_block >= 255 && cur_lumi_block <= 328) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 74) ) ) ||
	( cur_run_number == 180250 && (
	  (cur_lumi_block >= 84 && cur_lumi_block <= 259) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 82) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 3) ||
	  (cur_lumi_block >= 429 && cur_lumi_block <= 539) ||
	  (cur_lumi_block >= 262 && cur_lumi_block <= 426) ) ) ||
	( cur_run_number == 180252 && (
	  (cur_lumi_block >= 52 && cur_lumi_block <= 79) ||
	  (cur_lumi_block >= 42 && cur_lumi_block <= 49) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 40) ) ) )
		return true;
	else
		return false;
	// JSON AUTO FILL END -- do not edit this comment
	
	return true;
}

void RunSelector::accept_all(bool accept)
{
	this->accept_all_events = accept;
}
