#include "../../interface/NTupleReader/FileRunner.h"

FileRunner::FileRunner()
{
        infile = NULL;
        tree = NULL;
}

FileRunner::~FileRunner()
{
        if(infile){
                delete infile;
                infile = NULL;
        }
        if(tree){
                delete tree;
                tree = NULL;
        }
}

void FileRunner::set_file_names(std::vector<std::string> *file_names)
{
        this->file_names = file_names;
        current_file = file_names->begin();
}

void FileRunner::cd_infile()
{
        infile->cd();
}

TTree* FileRunner::get_next_tree(std::string tree_name, bool change_file)
{
        tree=NULL;
        if((change_file && current_file != file_names->end()) ||
		(!change_file && --current_file != file_names->end())){
		if(change_file){
                	if(infile){
                	        infile->Close();
                	        delete infile;
                	        infile = NULL;
                	}
                	if(verbose) std::cout << "processing file: " << *current_file << std::endl;
	        	//infile = new TDCacheFile(current_file->c_str(), "OPEN");
	        	infile = new TFile(current_file->c_str(), "OPEN");
		}
               	infile->cd();

	        tree = (TTree*) infile->Get(tree_name.c_str());
		tree->SetCacheSize(10000000);
//		tree->AddBranchToCache("*");
        }

        ++current_file;

        return tree;
}

bool FileRunner::has_next()
{
        if(current_file != file_names->end())
                return true;
        else
                return false;
}
