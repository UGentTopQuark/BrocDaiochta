#ifndef MOR_PRIMARYVERTEX_H
#define MOR_PRIMARYVERTEX_H

#include "TLorentzVector.h"
#include "../BeagObjects/PrimaryVertex.h"
#include "Math/LorentzVector.h"

namespace mor{
	class PrimaryVertex{
		public:
			PrimaryVertex(){};
			PrimaryVertex(beag::PrimaryVertex &beag_primary_vetex);
			~PrimaryVertex(){};
			void set_beag_info(beag::PrimaryVertex &beag_primary_vertex);
			double z();
			double rho();
			double ndof();
			bool is_fake();
		private:
			double z_val;
			double rho_val;
			bool is_fake_val;
			double ndof_val;
	};
	
	inline double PrimaryVertex::z() { return z_val; }
	inline double PrimaryVertex::rho() { return rho_val; }
	inline double PrimaryVertex::ndof() { return ndof_val; }
	inline bool PrimaryVertex::is_fake() { return is_fake_val; }
}

#endif
