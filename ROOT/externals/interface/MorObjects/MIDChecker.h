#ifndef MORIDCHECKER_H
#define MORIDCHECKER_H

#include <vector>
#include <map>
#include <string>

#include <iostream>

namespace mor{
	class IDChecker{
		public:
			IDChecker();
			~IDChecker();

			bool id_passed(std::string ID_name, unsigned long long lepton_id);
			void set_id_vector(std::vector<std::string> *names_vec);
			double id_value(std::string ID_name, std::vector<double> *id_values);
			std::vector<double> id_value(std::string ID_name, std::vector<std::vector<double> > *id_values);
			bool id_exists(std::string ID_name);
		private:
			std::map<std::string, unsigned long long> id_map;

			const static bool verbose = false;
	};
}

#endif
