#ifndef MOR_MET_H
#define MOR_MET_H

#include "MParticle.h"
#include "../BeagObjects/MET.h"

namespace mor{
	class MET: public Particle{
		public:
			MET(beag::MET &beag_met);
			inline MET(){};
		private:
	};
}

#endif
