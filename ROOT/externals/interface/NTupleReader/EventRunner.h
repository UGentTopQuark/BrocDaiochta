#ifndef EVENTRUNNER_H
#define EVENTRUNNER_H

/**
 * \class EventRunner
 * 
 * \brief Handle the read in of single events from ntuples ROOT files.
 *
 * The class is responsible for the reading of single events from ntuple root
 * files, keeps track of the current event and switches between events. It
 * ensures that there are not more events read in than are available in a
 * certain root file. It employs FileRunner to allow to switch between events
 * over single file boundaries.
 * 
 * \authors klein
 */


#include "TBranch.h"
#include "TTree.h"
#include "FileRunner.h"
#include "../BeagObjects/TriggerObject.h"
#include "../BeagObjects/Jet.h"
#include "../BeagObjects/Electron.h"
#include "../BeagObjects/Muon.h"
#include "../BeagObjects/MET.h"
#include "../BeagObjects/Trigger.h"
#include "../BeagObjects/TTbarGenEvent.h"
#include "../BeagObjects/PrimaryVertex.h"
#include "../BeagObjects/EventInformation.h"

class EventRunner{
        public:
                EventRunner();
                ~EventRunner();
                void next_event();
                template <class beagObj>
                void assign_collection(typename std::vector<beagObj> *&collection, std::string branch_name);
                bool has_next();
                int current_event();
                bool end_of_file();
                void set_file_names(std::vector<std::string> *file_names);
		FileRunner* get_file_runner();

		inline bool empty_file(){ return empty_file_; };
        private:
                TTree *tree;
                FileRunner *file_runner;
                int current_file_event;
                int current_file_max_events;
                int overall_current_event;
		bool empty_file_;

                static const bool verbose = false;
                bool new_file;
};

#endif
