#ifndef BEAG_MUON_H
#define BEAG_MUON_H

#include "Lepton.h"
#include "Rtypes.h"

namespace beag{
	class Muon : public beag::Lepton{
		public:
	                Muon():chi2(99), ndf(0), global_chi2(99), nHits(0),nMuonHits(0),
			nTrackerHits(0),nTrackerLayers(0),nStripHits(0),nLostStripHits(0),
			nPixelHits(0),nPixelLayers(0),nDTstations(0),nCSCstations(0),
			nMatchedSegments(0),nMatchedStations(0), isPFMuon(0),
			station2_eta(0), station2_phi(0), trackerChi2(99){}
		        virtual ~Muon(){};

			double chi2;	// this is the global normalised chi2
			double ndf;
			double global_chi2;	// not normalised
			double nHits;
			double nMuonHits;
			double nTrackerHits;
			double nTrackerLayers;
			double nStripHits;
			double nLostStripHits;
			double nPixelHits;
			double nPixelLayers;
			double nDTstations;
			double nCSCstations;
			double nMatchedSegments;
			double nMatchedStations;
			double isPFMuon;

			double station2_eta;
			double station2_phi;
			double trackerChi2;

		ClassDef(Muon, 1);
	};
}

#endif
