#ifndef BEAG_LEPTON_H
#define BEAG_LEPTON_H

#include "Particle.h"
#include <map>
#include <string>

#include "Rtypes.h"

namespace beag{
	class Lepton : public Particle{
		public:
			Lepton():ecal_iso(99),hcal_iso(99),track_iso(99),
				 hcal_vcone(99),ecal_vcone(99), d0(99), d0_sigma(99),
				 d0_pv(99), d0_sigma_pv(99),
			         lepton_id(0),track_available(0),
				nLostTrackerHits(99),nPixelHits(0), z0(99),z0_pv(99),vz(99),
				photonIso(0), neutralHadronIso(0),
				chargedHadronIso(0){};
			virtual ~Lepton(){};
			double ecal_iso;	
			double hcal_iso;	
			double track_iso;	

			double hcal_vcone;
			double ecal_vcone;

			double d0;		//< d0 wrt offline beamspot
			double d0_sigma;	//< error d0 wrt offline beamspot

			double d0_pv;		//< d0 wrt primary vertex
			double d0_sigma_pv;	//< error d0 wrt primary vertex

			unsigned long long lepton_id;
			int track_available;

			double nLostTrackerHits;
			double nPixelHits;

			double z0;
			double z0_pv;

			double vz;

			unsigned long long trigger;	//< Trigger bits -> lepton triggered by certain trigger: true / false

			double photonIso;
			double neutralHadronIso;
			double chargedHadronIso;
			double puChargedHadronIso;

			std::vector<double> photonIsos; //<Isolation values for different cone sizes
			std::vector<double> neutralHadronIsos;
			std::vector<double> chargedHadronIsos;

		ClassDef(Lepton, 1);
	};
}

#endif
