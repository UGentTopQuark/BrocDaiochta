#ifndef BEAG_TRIGGER_H
#define BEAG_TRIGGER_H

#include <map>
#include <string>
#include <vector>

#include "Rtypes.h"

namespace beag{
	class Trigger{
		public:
			virtual ~Trigger(){};

			std::vector<bool> triggered;
			std::vector<double> prescales;


		ClassDef(Trigger, 1);
	};
}

#endif
