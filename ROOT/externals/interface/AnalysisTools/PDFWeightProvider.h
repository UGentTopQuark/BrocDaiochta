#ifndef EIRE_PDF_WEIGHT_PROVIDER_H
#define EIRE_PDF_WEIGHT_PROVIDER_H

/** \class eire::PDFWeightProvider
 * 
 * \brief Handles vectors of pdf weights and returns weights belonging to a certain set.
 *
 * Usually there are ~20-40 pdfs available for pdf systematic studies.
 * PDFWeightProvider allows to access per event the weight for a certain pdf
 * set.
 *
 * \author klein
 */

#include "../NTupleReader/MIDChecker.h"

namespace eire{
	class PDFWeightProvider{
		public:
			double get_event_weight(std::string pdf_name, unsigned int npdf);
			std::vector<double> get_event_weight(std::string pdf_name);
			void set_pdf_set_provider(mor::IDChecker *pdf_set_provider);
			void set_pdf_evt_weights(std::vector<std::vector<double> > *pdf_evt_weights);
			unsigned int get_nevt_weights(std::string pdf_name);
		private:
			mor::IDChecker *pdf_set_provider;
			std::vector<std::vector<double> > *pdf_evt_weights;
	};
}

#endif
