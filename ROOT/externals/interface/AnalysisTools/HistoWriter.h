#ifndef HISTOWRITER_H
#define HISTOWRITER_H

/**
 * \class HistoWriter
 * 
 * \brief Allows the bookkeeping of histograms throughout the code, including
 * memory management and writing to disk.
 *
 * There is only one single HistoWriter object necessary throughout the code.
 * It keeps track of booked histograms for various SelectionSteps, provides
 * functions to book those histograms in an convenient way and writes them at
 * the end of the program to an output root file and frees the memory.
 * 
 * \authors klein
 */


#include "TFile.h"
#include "ETH1D.h"
#include "ETH2D.h"
#include <vector>

class HistoWriter{
	public:
		HistoWriter();
		~HistoWriter();
		void set_outfile(TFile *outfile);
		eire::TH1D* create_1d(std::string identifier,
				std::string title,
				int nbins_x, double min_x, double max_x,
				std::string x_title="", std::string y_title="Events/bin");
		eire::TH1D* create_1d(std::string identifier,
				std::string title,
				int nbins_x, float var_bins[100],
				std::string x_title="", std::string y_title="Events/bin");
		eire::TH2D* create_2d(std::string identifier,
				std::string title,
				int nbins_x, double min_x, double max_x,
				int nbins_y, double min_y, double max_y,
				std::string x_title="", std::string y_title="");
	private:
		std::vector<eire::TH1D*> histos1d;
		std::vector<eire::TH2D*> histos2d;
		//jl 26.08.10: re-normalise QCD histograms
		std::string hname;

		TFile *outfile;
};

#endif
