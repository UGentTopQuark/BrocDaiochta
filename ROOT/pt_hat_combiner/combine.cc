#include "TFile.h"
#include "TDirectory.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TKey.h"
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include "HistoMerger/MergeHistos.h"

int main(int argc, char **argv)
{
	//NOTE: if version != _plots have to change manually in histocombiner name of output file
	std::string version = "_plots";
	//std::string version = "";

	//TFile *outfile = new TFile(("QCDMuPt"+version+".root").c_str(), "recreate");
	TFile *outfile = new TFile(("QCDbcem"+version+".root").c_str(), "recreate");
	std::string directory_name = "eventselection";
	outfile->mkdir(directory_name.c_str());

	std::vector<std::string> cutsets;
 	//cutsets.push_back("00_cutset");
	cutsets.push_back("01_cutset");
    	cutsets.push_back("02_cutset");
     	cutsets.push_back("03_cutset");
     	cutsets.push_back("04_cutset");
    	//cutsets.push_back("07_cutset");
//    	cutsets.push_back("073_cutset");
	//  	cutsets.push_back("08_cutset");
	//   	cutsets.push_back("09_cutset");
   	//cutsets.push_back("10_cutset");
	
	std::map<std::string, std::string> files;
 	files["QCDem20to30"+version+".root"]="QCDem20to30";
 	files["QCDem30to80"+version+".root"]="QCDem30to80";
 	files["QCDem80to170"+version+".root"]="QCDem80to170";
 	files["QCDbctoe20to30"+version+".root"]="QCDbctoe20to30";
 	files["QCDbctoe30to80"+version+".root"]="QCDbctoe30to80";
 	files["QCDbctoe80to170"+version+".root"]="QCDbctoe80to170";
// 	files["QCDMuPt20to30"+version+".root"]="QCDMuPt20to30";
//	files["QCDMuPt30to50"+version+".root"]="QCDMuPt30to50";
//	files["QCDMuPt50to80"+version+".root"]="QCDMuPt50to80";
	
	std::vector<std::string> file_names;
	for(std::map<std::string, std::string>::iterator file = files.begin(); file != files.end(); file++)
		file_names.push_back(file->first);

	//Leave this = 1 so that files can be scaled to the correct lumi in other programs
	double int_lumi = 4628.087;

	std::string first_file = *(file_names.begin());
	TFile *file = new TFile(first_file.c_str(),"open");
	
	TKey *key;

	std::vector<std::string> histo_names;
	std::vector<std::string> histo_names2d;

	std::map<std::string,bool> already_there_1d;
	std::map<std::string,bool> already_there_2d;

	std::cout << "searching for keys" << std::endl;
	TIter nextkey(file->GetDirectory("eventselection")->GetListOfKeys());
	while ((key = (TKey*)nextkey())){
	//	std::cout << "processing: " << key->GetName() << std::endl;
	//	std::cout << "class: " << key->GetClassName() << std::endl;

		std::string histo_name = key->GetName();
		if((std::string) key->GetClassName() == "TH1D"){
			std::cout << "histo name: " << histo_name << std::endl;
			std::string cleaned_name = histo_name.substr(0,histo_name.find("_"+files[first_file]));
			//std::cout << cleaned_name << std::endl;
			if(already_there_1d.find(cleaned_name) == already_there_1d.end())
				histo_names.push_back(cleaned_name);
			already_there_1d[cleaned_name] = true;
		}
		if((std::string) key->GetClassName() == "TH2D"){
			std::string cleaned_name = histo_name.substr(0,histo_name.find("_"+files[first_file]));
	//		std::cout << cleaned_name << std::endl;
			if(already_there_2d.find(cleaned_name) == already_there_2d.end())
				histo_names2d.push_back(cleaned_name);
			already_there_2d[cleaned_name] = true;
		}
	}
	delete file;

	std::cout << "booking histocombiner" << std::endl;
	MergeHistos *hm = new MergeHistos();
	std::cout << "setting outfile" << std::endl;
	hm->set_outfile(outfile);
	std::cout << "adding histos" << std::endl;
	hm->set_integrated_luminosity(int_lumi);
	std::cout << "setting cross section" << std::endl;
	hm->set_analysis("Fall11sw");

	std::cout << "setting vfile_names" << std::endl;
	hm->set_vfile_names(file_names);
	hm->set_vhisto_names(histo_names);
	hm->set_cutsets(cutsets);
	hm->set_dataset_ids(files);
	hm->set_output_directory("eventselection");
	std::cout << "running combine all..." << std::endl;
	hm->combine_all_histos<TH1D>(false);
	//	CrossSectionProvider cs_prov;
// 	if(version != "_plots_10TeV"){
// 		std::cout << " Setting default cross sections" << std::endl;
// 		for(std::map<std::string,std::string>::iterator dataset = files.begin();
// 		    dataset != files.end();
// 		    ++dataset){
// 			hm->set_cross_section(dataset->second,cs_prov.get_cross_section(dataset->second)*
// 					      cs_prov.get_filter_efficiency(dataset->second));
// 		}
// 	}
// 	else if (version == "_plots_10TeV"){
// 		std::cout << "Setting 10TeV cross sections. For 1D plots" << std::endl;
// 		hm->set_cross_section("QCDem20to30",3200000*0.0002600160);
// 		hm->set_cross_section("QCDem30to80",4700000*0.00703883596280863);
// 		hm->set_cross_section("QCDem80to170",285000*0.0386703858280173);
// 		hm->set_cross_section("QCDbctoe20to30",192000*0.0004758968);
// 		hm->set_cross_section("QCDbctoe30to80",240000*0.010286577);
// 		hm->set_cross_section("QCDbctoe80to170",22800*0.075459828);
// 	}

	std::cout << "adding histos" << std::endl;
	//        hm->add_histos(files, histo_names, cutsets);
	std::cout << "combining histos" << std::endl;
//        hm->write_histos();

	std::cout << "booking histocombiner 2d" << std::endl;
	MergeHistos *hm2d = new MergeHistos();
	std::cout << "setting outfile 2d" << std::endl;
	hm2d->set_outfile(outfile);
	hm2d->set_integrated_luminosity(int_lumi);
	std::cout << "setting cross section 2d" << std::endl;
	hm2d->set_analysis("Fall11sw");

	hm2d->set_vfile_names(file_names);
	hm2d->set_vhisto_names(histo_names2d);
	hm2d->set_cutsets(cutsets);
	hm2d->set_dataset_ids(files);
	hm2d->combine_all_histos<TH2D>(false);
// 	for(std::map<std::string,std::string>::iterator dataset = files.begin();
// 		dataset != files.end();
// 		++dataset){
//         	hm2d->set_cross_section(dataset->second,cs_prov.get_cross_section(dataset->second)*
// 						cs_prov.get_filter_efficiency(dataset->second));
// 	}

	std::cout << "adding histos 2d" << std::endl;
        //hm2d->add_histos(files, histo_names2d, cutsets);
	std::cout << "combining histos 2d" << std::endl;
//        hm2d->write_histos();
	std::cout << "writing output" << std::endl;

	outfile->Write();
	outfile->Close();

	return 0;
}
