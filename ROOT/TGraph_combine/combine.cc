#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TPaveLabel.h"
#include "TLatex.h"
#include "Canvas_Holder.h"
#include <sstream>


//Will fail if no input string
int main(int argc, char **argv)
{
  std::string db_channel = argv[1];
  std::string d_channel = "none";
  if (db_channel == "mu")
    d_channel = "muon";
  else if (db_channel == "e")
    d_channel = "electron";

  std::string cutset = argv[2];
  std::string version = ("_0511_1500");
  std::string filename_out= (cutset+d_channel+"_btag.root");
  std::string btag_algos[2] = {"_ce","B"};
  std::vector<std::string> *back_file = new std::vector<std::string>();
  back_file->push_back("wjets"+version+".root");
  back_file->push_back("zjets"+version+".root");
  //back_file->push_back("mupt15"+version+".root");
  back_file->push_back("qcdpt15"+version+".root");
  //back_file->push_back("ttbar"+version+".root");
  std::vector<std::string> *back_DS = new std::vector<std::string>();
  back_DS->push_back("Wjets");
  back_DS->push_back("Zjets");
  //back_DS->push_back("Mupt15");
  back_DS->push_back("QCDpt15");
  //back_DS->push_back("TTbar");
  TFile *file_out = new TFile(filename_out.c_str(),"recreate");   
  CanvasHolder *cholders[2];

  //initialise canvas   
  for(int l = 0;l < 2; l++){
    
    cholders[l] = new CanvasHolder();
    
  }
       
       
  //open root file
  TFile *filet = new TFile(("tprime"+version+".root").c_str(),"open");
  
  
   
  //loop over btag algorithms
  for(int l = 0;l < 2; l++){	   
    
    std::string complete_name_Stmpc= ("jet_btag"+btag_algos[l]+"_evtpass_Tprime|"+d_channel+"|"+cutset);
    //std::cout <<"jet_btag"<<btag_algos[l]<<"_evtpass_Tprime|"<<d_channel<<"|"<<cutset<<std::endl;
    std::string complete_name_Stm = ("jet_btag"+btag_algos[l]+"_all_Tprime|"+d_channel+"|"+cutset);

    TH1F *hist_e = NULL;
    TH1F *hist_p = NULL;
    TH1F *hist_StmpcBumpc = NULL;
    TH1F *hist_Bumpc = NULL;
    
    
    //initialize outfile histgrams with different bins for different btag algot=rithms
    if(l == 1){
      hist_e = new TH1F(("hist_e"+btag_algos[l]).c_str(),"BtagB eff",20,0.25,10.25);
      hist_p = new TH1F(("hist_p"+btag_algos[l]).c_str(),"BtagB purity",20,0.25,10.25);
      hist_StmpcBumpc = new TH1F(("hist_SpcBpc"+btag_algos[l]+"|"+d_channel+"|").c_str(),"BtagB SpcBpc",20,0.25,10.25);
      hist_Bumpc = new TH1F(("hist_Bumpc"+btag_algos[l]+"|"+d_channel+"|").c_str(),"BtagB Bumpc",20,0.25,10.25);}
    else if( l == 0 ){
      hist_e = new TH1F(("hist_e"+btag_algos[l]).c_str(),"Btag eff",25,0.5,25.5);
      hist_p = new TH1F(("hist_p"+btag_algos[l]).c_str(),"Btag purity",25,0.5,25.5);
      hist_StmpcBumpc = new TH1F(("hist_SpcBpc"+btag_algos[l]+"|"+d_channel+"|").c_str(),"Btag ce SpcBpc",20,0.25,10.25);
      hist_Bumpc = new TH1F(("hist_Bumpc"+btag_algos[l]+"|"+d_channel+"|").c_str(),"Btag ce Bpc",20,0.25,10.25);}
    
    //Get signal btag files
    TH1F *hist_Stm = ((TH1F *) filet->GetDirectory("eventselection")->Get(complete_name_Stm.c_str()));
    TH1F *hist_Stmpc = ((TH1F *) filet->GetDirectory("eventselection")->Get(complete_name_Stmpc.c_str()));
    

    //loop over and add btag evtpass histogram for each background file
    //std::vector<std::string>::iterator back_DS_iter = back_DS->begin();
    int i=0;
    for (std::vector<std::string>::iterator DS_iter = back_file->begin(); DS_iter != back_file->end(); ++DS_iter){
      
      
      TFile *fileb = new TFile(DS_iter->c_str(),"open");
      std::string complete_name_aBumpc= ("jet_btag"+btag_algos[l]+"_evtpass_"+(*back_DS)[i]+"|"+db_channel+"_background|"+cutset);
      TH1F *hist_aBumpc = ((TH1F *) fileb->GetDirectory("eventselection")->Get(complete_name_aBumpc.c_str()));
      
      if (DS_iter == back_file->begin())
	hist_Bumpc = hist_aBumpc;
      else
	hist_Bumpc->Add(hist_aBumpc,hist_Bumpc);

      ++i;
    }
    

    file_out->cd();
    hist_StmpcBumpc->Add(hist_Stmpc,hist_Bumpc);
	      
    //Calculate eff and purity plots
    hist_e->Divide(hist_Stmpc, hist_Stm);
    hist_p->Divide(hist_Stmpc,hist_StmpcBumpc);
    
    //Create eff vs purity TGraph
    TGraph *hist_ep = new TGraph(50);
    //TPaveLabel *myLabel = new TPaveLabel();
    // hist_ep->SetName("btag_eff_vs_purity");	   
    int k=0,numbins = hist_e->GetNbinsX(); 
    double cutB =0.5,cut_ce=1;
    //15 = number of cuts looped over, set in Plotgenerator
    for(k=1;k<16; k++)
      {	
	
	if (l==1){
	  std::cout << cutB <<"  " << hist_p->GetBinContent(k)<<"  "<<hist_e->GetBinContent(k) <<"  "<< std::endl;
	  cutB = cutB+0.5;
	}
	else if (l==0){
	  std::cout << cut_ce <<"  " << hist_p->GetBinContent(k)<<"  "<<hist_e->GetBinContent(k) <<"  "<< std::endl;
	  cut_ce = cut_ce + 1;
	}
	
	//if (hist_e->GetBinContent(k) != 0 && hist_p->GetBinContent(k) != 0)
	hist_ep->SetPoint(k,hist_p->GetBinContent(k),hist_e->GetBinContent(k));
      }
    
    //Draw eff vs pur on canvas
    //hist_ep->Draw(("jet_eff_vs_pur_btag_"+Type_DS[i]+"|"+decay_channel[j]+"|"+cutset).c_str());  
      
      cholders[l]->addGraph(hist_ep,(""+d_channel).c_str(),(""+d_channel).c_str(),"*");
      cholders[l]->setCanvasTitle("jet_eff_vs_pur_btag"+btag_algos[l]+d_channel);
      cholders[l]->setTitle("jet_eff_vs_pur_btag"+btag_algos[l]);
      cholders[l]->setTitleY("efficiency");
      cholders[l]->setTitleX("purity");
      cholders[l]->setBordersY(0,1);
      cholders[l]->setBordersX(0,1);
      cholders[l]->addHLine(1.0);
      cholders[l]->setOptStat(000000);
      cholders[l]->setMarkerSizeGraph(1.2);
	    
      cholders[l]->write(file_out);
      cholders[l]->save("eps");
	  
      //file_out->Write(hist_ep,("jet_eff_vs_pur_btag_"+Type_DS[i]+"|"+decay_channel[j]+"|"+cutset).c_str());
  
  }   
  file_out->Write();

  delete back_file;
  back_file=NULL;
  delete back_DS;
  back_DS=NULL;
  
   
  return 0;  
}

     /*********************** 
     Stm = signal truth matched = real b jet
     Stmpc = signal truth matched passed cut
     Bumpc = non b jet
     eff = Stm/Stmpc
     pur = Stmpc/(Stmpc+Bumpc)
     ***********************/
