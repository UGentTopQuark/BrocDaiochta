#include <iostream>
#include "TKey.h"
#include "TFile.h"
#include "TDirectory.h"
#include <string>
#include <vector>
#include "ConfigReader/ConfigReader.h"
#include "BranchingRatio/BranchingRatio.h"
#include "HistoNameParser/HistoNameParser.h"

int main(int argc, char **argv)
{
	if(argc < 2){
		std::cerr << "usage: " << argv[0] << " <config>" << std::endl;
		return 1;
	}

	eire::ConfigReader *config_reader = new eire::ConfigReader();
	config_reader->read_config_from_file(argv[1]);

	std::string data_dataset = config_reader->get_var("data_dataset");
	std::string ttbar_dataset = config_reader->get_var("ttbar_dataset");
	std::vector<std::string> other_datasets = config_reader->get_vec_var("other_signal_datasets");
	double lumi = atof(config_reader->get_var("luminosity").c_str());
	std::string directory = config_reader->get_var("input_directory");
	directory += "/";

	// find all histo names to process
	TKey *key = NULL;
	TFile *data_file = new TFile((directory+data_dataset+"_plots.root").c_str(), "OPEN");
        TIter nextkey(data_file->GetDirectory("eventselection")->GetListOfKeys());
	bool channel_identified = false;
	HistoNameParser hnp;
	std::string channel = "";
	std::map<std::string,bool> histo_names;
	std::map<std::string,bool> cutssets;
        while ((key = (TKey*)nextkey())){
                std::string histo_name = key->GetName();
		if(!channel_identified){	// find if e or mu channel
			hnp.set_histo_name(histo_name);
			channel = hnp.event_type();
			channel_identified = true;
		}
                if((std::string) key->GetClassName() == "TH1F"){
                        std::string cleaned_name = histo_name.substr(0,histo_name.find("_"+data_dataset));
			std::string cutsset = hnp.cutset();
			hnp.set_histo_name(histo_name);
			if(cutsset != ""){
				cutssets[cutsset] = true;
                        	histo_names[cleaned_name] = true;
			}
                }
        }

	for(std::map<std::string, bool>::iterator histo = histo_names.begin();
		histo != histo_names.end();
		++histo){
		std::cout << "histo name: " << histo->first << std::endl;
	}
	for(std::map<std::string, bool>::iterator histo = cutssets.begin();
		histo != cutssets.end();
		++histo){
		std::cout << "cutsset: " << histo->first << std::endl;
	}

	// // find TTbar BR
	// BranchingRatio br_calc;
	// br_calc.set_file(directory+ttbar_dataset+"_plots.root");
	// double br = 1.;
	// if(channel == "electron"){
	// 	br = br_calc.get_branching_ratio_electron();
	// }else{
	// 	br = br_calc.get_branching_ratio_muon();
	// }

        delete data_file;
	return 0;
}
