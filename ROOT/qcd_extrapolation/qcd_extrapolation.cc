#include "TGraphErrors.h"
#include "CanvasHolder/Canvas_Holder.h"
#include <iostream>

int main(int argc, char **argv)
{
	const int nslices = 3;
	double relIso[nslices]= {0.424363, 0.60103, 1.0325};
	double relIsoError[nslices] = {0., 0., 0.};
	double cs[nslices] = {155.39, 157.83, 160.01};
	double csError[nslices] = {3.6, 3.5, 3.4};

	double mean_first_slice = 0.0346;

	TGraphErrors graph(nslices, relIso, cs, relIsoError, csError);

	TF1 fct("line","[0]-[1]*x", 0., 2.5);
	graph.Fit(&fct,"","",0.,2.5);

	double extrapolated_cs = fct.Eval(mean_first_slice);
	std::cout << "cross section value extrapolated to first slice: " << extrapolated_cs << std::endl;
	double exrelIso[1] = {mean_first_slice};
	double exrelIsoError[1] = {0.};
	double excs[1] = {extrapolated_cs};
	double excsError[1] = {0.};
	TGraphErrors extrapolated_cross_section(1, exrelIso, excs, exrelIsoError, excsError);

	CanvasHolder ch;
	std::string drawopt = "PE1";

	ch.setTitleX("Relative isolation");

	ch.setTitleY("#sigma_{t#bar{t}} (pb)");
	ch.setCanvasTitle("qcd_extrapolation");
	ch.addGraph(&graph,"name1","measured",drawopt);
	ch.addGraph(&extrapolated_cross_section,"name1","extrapolated",drawopt);
	ch.addTF1(&fct,"linear fit","");

	ch.setBordersY(125,200);
	ch.setBordersX(0.0,1.2);
	ch.setMarkerStylesGraph(20);
	ch.setMarkerSizeGraph(1.2);
   	ch.setLineSizeGraph(2);
   	ch.setOptStat(00000);
	ch.save("pdf");

        return 0;
}
