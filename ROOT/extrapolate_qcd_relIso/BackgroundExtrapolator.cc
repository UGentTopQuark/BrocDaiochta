#include "BackgroundExtrapolator.h"

BackgroundExtrapolator::BackgroundExtrapolator()
{
	outfile = NULL;
}

BackgroundExtrapolator::~BackgroundExtrapolator()
{
}

void BackgroundExtrapolator::set_signal_histo(TH1F *histo)
{
	this->signal_histo = histo;
}

void BackgroundExtrapolator::set_background_histo(TH1F *histo)
{
	this->background_histo = histo;
}

void BackgroundExtrapolator::set_outfile(TFile *file)
{
	this->outfile = file;
}

void BackgroundExtrapolator::extrapolate_to_signal_region()
{
	TH1F *combined_histo = new TH1F(*background_histo);
	combined_histo->Add(signal_histo);
	fit_root_landau(combined_histo);	
	std::cout << "root histo integral: " << background_histo->Integral() << std::endl;
}

void BackgroundExtrapolator::fit_roofit_landau(TH1F *histo)
{
	using namespace RooFit;

	outfile->cd();
        TCanvas *c1 = new TCanvas("fit", "canvas", 640, 480);
        c1->cd();

	RooRealVar x("x","relIso",0,1.);
	RooRealVar mean("mean","mean",0.5, 0., 1.0); 
	RooRealVar sigma("sigma","sigma",0.5,0.0,1.0); 
	RooRealVar A("A","A",1500., 0., 100000.0);

	RooLandau norm_landau("norm_landau", "norm_landau", x, mean, sigma);
	RooExtendPdf landau("landau", "landau", norm_landau, A);

	RooDataHist* data = new RooDataHist("data","data",x,histo);

	landau.fitTo(*data,Range(0.2,1.0));

//	std::cout << cutset << " A: " << A.getVal() << " AERR: " << A.getError() << " a: " << a.getVal() << " aERR: " << a.getError() << std::endl;

	RooPlot* xframe = x.frame(Name("extrapolation"),Title("Extrapolation of background events to signal region"));
	xframe->UseCurrentStyle();
	xframe->SetYTitle("Events");
	data->plotOn(xframe);
	landau.plotOn(xframe,LineColor(kRed),LineStyle(kDashed),Range(0.2,1.0));
	landau.plotOn(xframe,LineColor(kGreen),LineStyle(kDashed),Range(0.1,0.2));
	landau.plotOn(xframe,LineColor(kBlue),LineStyle(kDashed),Range(0.,0.1));

	std::cout << "raw: " << landau.getVal() << std::endl;
	std::cout << "normalisation: " << A.getVal() << std::endl;
	std::cout << "bin width: " << histo->GetBinWidth(1) << std::endl;

	x.setRange("signal",0.,1.0);
	RooAbsReal *integral = landau.createIntegral(x,Range("signal"));
	//regValue(integral->getVal(),"rf110_gx_Int[x|signal]_Norm[x]") ;
	std::cout << "integral: " << integral->getVal()*A.getVal() << std::endl;
	xframe->Draw();
	
	c1->SaveAs("fit.eps");
	c1->Write();

	delete data;

	histo->Write();
}

void BackgroundExtrapolator::fit_root_landau(TH1F *histo)
{
	outfile->cd();

	double fit_min=0.15;
	double fit_max=1.0;
	double sig_min=0.0;
	double sig_max=0.1;

	CanvasHolder cholder;

	TF1 *background_region = new TF1("background","landau(0)",fit_min,fit_max);
	//TF1 *background_region = new TF1("background","[0]+[1]*x+[2]*x*x+[3]*x*x*x",fit_min,fit_max);
	//TF1 *background_region = new TF1("background","[0]+[1]*cos([2]*x+[3])",fit_min,fit_max);
	//TF1 *background_region = new TF1("background","gaus(0)",fit_min,fit_max);
	background_region->SetRange(fit_min, fit_max);

	//background->SetParameter(0,histo->Integral());
	background_region->SetParameter(0,18000);
	background_region->SetParameter(1,0.9);
	background_region->SetParameter(2,0.4);
	histo->Fit(background_region, "r+");

	std::cout << "integral: " << background_region->Integral(0,0.1)/histo->GetBinWidth(0) << std::endl;
	std::cout << "background histo integral: " << background_histo->Integral(1,5) << " +/- " << sqrt(background_histo->Integral(1,5))  << std::endl;
	std::cout << "histo binwidth: " << histo->GetBinWidth(0) << std::endl;

	TF1 *intermediate_region = new TF1(*background_region);
	TF1 *signal_region = new TF1(*background_region);

	intermediate_region->SetRange(sig_max, fit_min);
	signal_region->SetRange(sig_min, sig_max);

	std::vector<int> *colours = new std::vector<int>();
	colours->push_back(kRed-3);
//	colours->push_back(kYellow-7);
	colours->push_back(kWhite-1);
//	colours->push_back(kGreen-3);
//	colours->push_back(kCyan-6);
//	colours->push_back(kBlue-1);
//	colours->push_back(kBlue-2);
//	colours->push_back(kBlue-3);

//	cholder.setDrawOpt("LF2");
	cholder.addHistoStacked(signal_histo,"t#bar{t} signal","");
	cholder.addHistoStacked(background_histo,"QCD background","");
	cholder.setLineColors(*colours);
	cholder.addTF1(background_region,"fit region", "");
	cholder.addTF1(intermediate_region,"intermediate region", "");
	cholder.addTF1(signal_region,"signal region", "");

	cholder.setLineColors(*colours);
	cholder.setOptStat(000000);
	cholder.setTitle("");
	cholder.setTitleX("rel.Iso");
	cholder.setTitleY("events @ 200/pb");
	cholder.setBordersX(0,fit_max);
	cholder.setBordersY(0,6000);
	cholder.setLegDraw(true);
	cholder.write(outfile);
	cholder.save("extrapolation.eps");
}
