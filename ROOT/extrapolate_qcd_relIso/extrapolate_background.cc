#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "MergeHistos.h"
#include "BackgroundExtrapolator.h"
#include "tdrstyle.h"

int main(int argc, char **argv)
{
	std::string histo_name = "unmatch_mu_CombRelIso_";
	//std::string histo_name = "unmatch_e_CombRelIso_";
	std::string cutset = "01_cutset";
	std::string version = "_plots";

	TFile *outfile = new TFile("outfile.root", "RECREATE");
	MergeHistos *mhistos = new MergeHistos(outfile);
	BackgroundExtrapolator *extrapolator = new BackgroundExtrapolator();
	extrapolator->set_outfile(outfile);

	std::map<std::string,std::vector<std::string> > signal_file_names;
	signal_file_names["ttbar"+version+".root"].push_back(histo_name+"TTbar|muon|"+cutset);
	//signal_file_names["ttbar"+version+".root"].push_back(histo_name+"TTbar|electron|"+cutset);

	std::map<std::string,std::vector<std::string> > background_file_names;
	//background_file_names["wjets"+version+".root"].push_back(histo_name+"Wjets|mu_background|"+cutset);
	//background_file_names["zjets"+version+".root"].push_back(histo_name+"Zjets|mu_background|"+cutset);
	background_file_names["qcdmupt15"+version+".root"].push_back(histo_name+"Mupt15|mu_background|"+cutset);
        //background_file_names["qcdbctoe20to30"+version+".root"].push_back(histo_name+"QCDbctoe20to30|e_background|"+cutset);
        //background_file_names["qcdbctoe30to80"+version+".root"].push_back(histo_name+"QCDbctoe30to80|e_background|"+cutset);
        //background_file_names["qcdbctoe80to170"+version+".root"].push_back(histo_name+"QCDbctoe80to170|e_background|"+cutset);
        //background_file_names["qcdem20to30"+version+".root"].push_back(histo_name+"QCDem20to30|e_background|"+cutset);
        //background_file_names["qcdem30to80"+version+".root"].push_back(histo_name+"QCDem30to80|e_background|"+cutset);
        //background_file_names["qcdem80to170"+version+".root"].push_back(histo_name+"QCDem80to170|e_background|"+cutset);
	//background_file_names["t_s_chan"+version+".root"].push_back(histo_name+"TsChan|mu_background|"+cutset);
	//background_file_names["t_t_chan"+version+".root"].push_back(histo_name+"TtChan|mu_background|"+cutset);
	//background_file_names["t_tW_chan"+version+".root"].push_back(histo_name+"TtWChan|mu_background|"+cutset);

	mhistos->set_integrated_luminosity(200.0);
	mhistos->set_cutset(cutset);


	mhistos->set_file_names(signal_file_names);
	extrapolator->set_signal_histo(mhistos->combine_histo<TH1F>(histo_name));
	mhistos->set_file_names(background_file_names);
	extrapolator->set_background_histo(mhistos->combine_histo<TH1F>(histo_name));

	extrapolator->extrapolate_to_signal_region();

	delete outfile;
	outfile = NULL;
	delete extrapolator;
	extrapolator = NULL;
}
