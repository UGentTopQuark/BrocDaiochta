#ifndef MERGEHISTOS_H
#define MERGEHISTOS_H

#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

class MergeHistos{
	public:
		MergeHistos(TFile *outfile);
		template <class HistoClass>
		HistoClass* combine_histo(std::string histo_name);
		void set_file_names(std::map<std::string,std::vector<std::string> > &file_names);
		void set_cutset(std::string cutset);
		void set_integrated_luminosity(double integrated_luminosity);

	private:
		TFile *outfile;
		std::string cutset;
		std::map<std::string,std::vector<std::string> > file_names;

		double integrated_luminosity;
};
#endif
