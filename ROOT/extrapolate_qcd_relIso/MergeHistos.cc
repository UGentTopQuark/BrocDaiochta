#include "MergeHistos.h"

template TH1D* MergeHistos::combine_histo<TH1D>(std::string histo_name);
template TH2D* MergeHistos::combine_histo<TH2D>(std::string histo_name);
template TH1F* MergeHistos::combine_histo<TH1F>(std::string histo_name);
template TH2F* MergeHistos::combine_histo<TH2F>(std::string histo_name);

MergeHistos::MergeHistos(TFile *outfile)
{
	this->outfile = outfile;
	integrated_luminosity = -1;
}

void MergeHistos::set_integrated_luminosity(double integrated_luminosity)
{
	this->integrated_luminosity = integrated_luminosity;
}

void MergeHistos::set_file_names(std::map<std::string,std::vector<std::string> > &file_names)
{
	this->file_names = file_names;
}

void MergeHistos::set_cutset(std::string cutset)
{
	this->cutset = cutset;
}

template <class HistoClass>
HistoClass* MergeHistos::combine_histo(std::string histo_name)
{
	std::string directory_name="eventselection";

	std::cout << "scaling for histo " << histo_name << " cutset " << cutset << std::endl;

	std::map<std::string,double> cross_section;
	cross_section[histo_name+"Tprime250|muon|"+cutset]=35.6*0.15;
	cross_section[histo_name+"Tprime250|mu_background|"+cutset]=35.6*0.85;
	cross_section[histo_name+"Tprime275|muon|"+cutset]=21.67*0.15;
	cross_section[histo_name+"Tprime275|mu_background|"+cutset]=21.67*0.85;
	cross_section[histo_name+"Tprime300|muon|"+cutset]=13.65*0.15;
	cross_section[histo_name+"Tprime300|mu_background|"+cutset]=13.65*0.85;
	cross_section[histo_name+"Tprime325|muon|"+cutset]=8.86*0.15;
	cross_section[histo_name+"Tprime325|mu_background|"+cutset]=8.86*0.85;
	cross_section[histo_name+"Tprime350|muon|"+cutset]=5.88*0.15;
	cross_section[histo_name+"Tprime350|mu_background|"+cutset]=5.88*0.85;
	cross_section[histo_name+"Tprime375|muon|"+cutset]=4.03*0.15;
	cross_section[histo_name+"Tprime375|mu_background|"+cutset]=4.03*0.85;
	cross_section[histo_name+"Tprime400|muon|"+cutset]=2.80*0.15;
	cross_section[histo_name+"Tprime400|mu_background|"+cutset]=2.80*0.85;
	cross_section[histo_name+"TTbar|muon|"+cutset]=375*0.15;
	cross_section[histo_name+"TTbar|mu_background|"+cutset]=375*0.85;
	cross_section[histo_name+"TTbarTrigger|muon|"+cutset]=375*0.15;
	cross_section[histo_name+"TTbarTrigger|mu_background|"+cutset]=375*0.85;
	cross_section[histo_name+"Zjets|mu_background|"+cutset]=3540;
	cross_section[histo_name+"Wjets|mu_background|"+cutset]=35550;
	cross_section[histo_name+"Mupt15|mu_background|"+cutset]=509100000*0.000239;
	cross_section[histo_name+"TtChan|mu_background|"+cutset]=27.3;
	cross_section[histo_name+"TtWChan|mu_background|"+cutset]=63.6;
	cross_section[histo_name+"TsChan|mu_background|"+cutset]=1.66;

	cross_section[histo_name+"TTbar|electron|"+cutset]=375*0.15;
	cross_section[histo_name+"TTbar|e_background|"+cutset]=375*0.85;
	cross_section[histo_name+"TTbarTrigger|electron|"+cutset]=375*0.15;
	cross_section[histo_name+"TTbarTrigger|e_background|"+cutset]=375*0.85;
	cross_section[histo_name+"Zjets|e_background|"+cutset]=3540;
	cross_section[histo_name+"Wjets|e_background|"+cutset]=35550;
	cross_section[histo_name+"Mupt15|e_background|"+cutset]=509100000*0.000239;
	cross_section[histo_name+"TtChan|e_background|"+cutset]=27.3;
	cross_section[histo_name+"TtWChan|e_background|"+cutset]=63.6;
	cross_section[histo_name+"TsChan|e_background|"+cutset]=1.66;
	cross_section[histo_name+"QCDbctoe20to30|e_background|"+cutset]=400000000 * 0.00048;
	cross_section[histo_name+"QCDbctoe30to80|e_background|"+cutset]=100000000 * 0.0024;
	cross_section[histo_name+"QCDbctoe80to170|e_background|"+cutset]=1900000 * 0.012;
	cross_section[histo_name+"QCDem20to30|e_background|"+cutset]=400000000 * 0.0080;
	cross_section[histo_name+"QCDem30to80|e_background|"+cutset]=100000000 * 0.047;
	cross_section[histo_name+"QCDem80to170|e_background|"+cutset]=1900000 * 0.15;

	std::map<std::string,double> filter_efficiencies;
	filter_efficiencies[histo_name+"TTbar|muon|"+cutset]=0.617077991754673;
	filter_efficiencies[histo_name+"TTbar|mu_background|"+cutset]=0.384405643925796 ;
	filter_efficiencies[histo_name+"TTbarTrigger|muon|"+cutset]=0.68207041;
	filter_efficiencies[histo_name+"TTbarTrigger|mu_background|"+cutset]=0.471051079;
	filter_efficiencies[histo_name+"Tprime250|muon|"+cutset]=0.53157895;
	filter_efficiencies[histo_name+"Tprime250|mu_background|"+cutset]=0.53157895;
	filter_efficiencies[histo_name+"Tprime275|muon|"+cutset]=0.55;
	filter_efficiencies[histo_name+"Tprime275|mu_background|"+cutset]=0.55;
	filter_efficiencies[histo_name+"Tprime300|muon|"+cutset]=0.56668;
	filter_efficiencies[histo_name+"Tprime300|mu_background|"+cutset]=0.56668;
	filter_efficiencies[histo_name+"Tprime325|muon|"+cutset]=0.574458;
	filter_efficiencies[histo_name+"Tprime325|mu_background|"+cutset]=0.574458;
	filter_efficiencies[histo_name+"Tprime350|muon|"+cutset]=0.5943;
	filter_efficiencies[histo_name+"Tprime350|mu_background|"+cutset]=0.5943;
	filter_efficiencies[histo_name+"Tprime375|muon|"+cutset]=0.59487487;
	filter_efficiencies[histo_name+"Tprime375|mu_background|"+cutset]=0.59487487;
	filter_efficiencies[histo_name+"Tprime400|muon|"+cutset]=0.60180723;
	filter_efficiencies[histo_name+"Tprime400|mu_background|"+cutset]=0.60180723;
	filter_efficiencies[histo_name+"Zjets|mu_background|"+cutset]=0.0076099354;
	filter_efficiencies[histo_name+"Wjets|mu_background|"+cutset]=0.0034905503;
	filter_efficiencies[histo_name+"Mupt15|mu_background|"+cutset]=0.10212446;
	filter_efficiencies[histo_name+"TtChan|mu_background|"+cutset]=0.088775394;
	filter_efficiencies[histo_name+"TsChan|mu_background|"+cutset]=0.11759313;
	filter_efficiencies[histo_name+"TtWChan|mu_background|"+cutset]=0.28042331;

	filter_efficiencies[histo_name+"TTbar|electron|"+cutset]= 0.60370888417633 ;
	filter_efficiencies[histo_name+"TTbar|e_background|"+cutset]=0.386677654168383 ;
	filter_efficiencies[histo_name+"TTbarTrigger|electron|"+cutset]=0.632092652;
	filter_efficiencies[histo_name+"TTbarTrigger|e_background|"+cutset]=0.479672568;
	filter_efficiencies[histo_name+"Zjets|e_background|"+cutset]=0.0076099354;
	filter_efficiencies[histo_name+"Wjets|e_background|"+cutset]=0.0034905503;
	filter_efficiencies[histo_name+"Mupt15|e_background|"+cutset]=0.10212446;
	filter_efficiencies[histo_name+"TtChan|e_background|"+cutset]=0.088775394;
	filter_efficiencies[histo_name+"TsChan|e_background|"+cutset]=0.11759313;
	filter_efficiencies[histo_name+"TtWChan|e_background|"+cutset]=0.28042331;
	filter_efficiencies[histo_name+"QCDbctoe20to30|e_background|"+cutset]=0.0004758968;
	filter_efficiencies[histo_name+"QCDbctoe30to80|e_background|"+cutset]=0.010286577;
	filter_efficiencies[histo_name+"QCDbctoe80to170|e_background|"+cutset]=0.075459828;
	filter_efficiencies[histo_name+"QCDem20to30|e_background|"+cutset]=0.000261562103295226;
	filter_efficiencies[histo_name+"QCDem30to80|e_background|"+cutset]=0.00703883596280863;
	filter_efficiencies[histo_name+"QCDem80to170|e_background|"+cutset]=0.0386703858280173;

	std::map<std::string,std::string> event_counters;
	event_counters[histo_name+"Tprime250|muon|"+cutset]="event_counter_Tprime250|muon|"+cutset;
	event_counters[histo_name+"Tprime250|mu_background|"+cutset]="event_counter_Tprime250|mu_background|"+cutset;
	event_counters[histo_name+"Tprime300|muon|"+cutset]="event_counter_Tprime300|muon|"+cutset;
	event_counters[histo_name+"Tprime300|mu_background|"+cutset]="event_counter_Tprime300|mu_background|"+cutset;
	event_counters[histo_name+"Tprime350|muon|"+cutset]="event_counter_Tprime350|muon|"+cutset;
	event_counters[histo_name+"Tprime350|mu_background|"+cutset]="event_counter_Tprime350|mu_background|"+cutset;
	event_counters[histo_name+"Tprime400|muon|"+cutset]="event_counter_Tprime400|muon|"+cutset;
	event_counters[histo_name+"Tprime400|mu_background|"+cutset]="event_counter_Tprime400|mu_background|"+cutset;
	event_counters[histo_name+"TTbar|muon|"+cutset]="event_counter_TTbar|muon|"+cutset;
	event_counters[histo_name+"TTbar|mu_background|"+cutset]="event_counter_TTbar|mu_background|"+cutset;
	event_counters[histo_name+"TTbarTrigger|muon|"+cutset]="event_counter_TTbarTrigger|muon|"+cutset;
	event_counters[histo_name+"TTbarTrigger|mu_background|"+cutset]="event_counter_TTbarTrigger|mu_background|"+cutset;
	event_counters[histo_name+"Zjets|mu_background|"+cutset]="event_counter_Zjets|mu_background|"+cutset;
	event_counters[histo_name+"Mupt15|mu_background|"+cutset]="event_counter_Mupt15|mu_background|"+cutset;
	event_counters[histo_name+"Wjets|mu_background|"+cutset]="event_counter_Wjets|mu_background|"+cutset;
	event_counters[histo_name+"TtChan|mu_background|"+cutset]="event_counter_TtChan|mu_background|"+cutset;
	event_counters[histo_name+"TsChan|mu_background|"+cutset]="event_counter_TsChan|mu_background|"+cutset;
	event_counters[histo_name+"TtWChan|mu_background|"+cutset]="event_counter_TtWChan|mu_background|"+cutset;

	event_counters[histo_name+"TTbar|electron|"+cutset]="event_counter_TTbar|electron|"+cutset;
	event_counters[histo_name+"TTbar|e_background|"+cutset]="event_counter_TTbar|e_background|"+cutset;
	event_counters[histo_name+"TTbarTrigger|electron|"+cutset]="event_counter_TTbarTrigger|electron|"+cutset;
	event_counters[histo_name+"TTbarTrigger|e_background|"+cutset]="event_counter_TTbarTrigger|e_background|"+cutset;
	event_counters[histo_name+"Zjets|e_background|"+cutset]="event_counter_Zjets|e_background|"+cutset;
	event_counters[histo_name+"Mupt15|e_background|"+cutset]="event_counter_Mupt15|e_background|"+cutset;
	event_counters[histo_name+"Wjets|e_background|"+cutset]="event_counter_Wjets|e_background|"+cutset;
	event_counters[histo_name+"TtChan|e_background|"+cutset]="event_counter_TtChan|e_background|"+cutset;
	event_counters[histo_name+"TsChan|e_background|"+cutset]="event_counter_TsChan|e_background|"+cutset;
	event_counters[histo_name+"TtWChan|e_background|"+cutset]="event_counter_TtWChan|e_background|"+cutset;
	event_counters[histo_name+"QCDbctoe20to30|e_background|"+cutset]="event_counter_QCDbctoe20to30|e_background|"+cutset;
	event_counters[histo_name+"QCDbctoe30to80|e_background|"+cutset]="event_counter_QCDbctoe30to80|e_background|"+cutset;
	event_counters[histo_name+"QCDbctoe80to170|e_background|"+cutset]="event_counter_QCDbctoe80to170|e_background|"+cutset;
	event_counters[histo_name+"QCDem20to30|e_background|"+cutset]="event_counter_QCDem20to30|e_background|"+cutset;
	event_counters[histo_name+"QCDem30to80|e_background|"+cutset]="event_counter_QCDem30to80|e_background|"+cutset;
	event_counters[histo_name+"QCDem80to170|e_background|"+cutset]="event_counter_QCDem80to170|e_background|"+cutset;

	double event_counter=0;
	
	outfile->cd();

	HistoClass *combined_histo=NULL;
	/*
	 * combine the backgrounds
	 */
	bool first = true;
	for(std::map<std::string,std::vector<std::string> >::iterator file_name = file_names.begin();
		file_name != file_names.end();
		++file_name){
		TFile *file = new TFile(file_name->first.c_str(), "open");
		std::cout << "opened " << file_name->first <<std::endl;
		for(unsigned int file_nr = 0; file_nr < file_name->second.size();
			++file_nr){
			
			// create a copy of the first histo.
			if(first){
				outfile->cd();
				std::cout << "opening " << directory_name << "/" << file_name->second[file_nr] <<std::endl;
				combined_histo = new HistoClass(*((HistoClass *) file->GetDirectory(directory_name.c_str())->Get(file_name->second[file_nr].c_str())));

//				combined_histo->Sumw2();

				if(combined_histo->GetEntries() != 0)
					first = false;
				else{
					std::cout << file_name->second[file_nr] <<" was empty " <<std::endl;
					continue;
				}

				if(cross_section.find(file_name->second[file_nr]) != cross_section.end()){
					HistoClass *eff_histo = (HistoClass *) file->GetDirectory(directory_name.c_str())->Get(event_counters[file_name->second[file_nr].c_str()].c_str());

					double efficiency =0;
					if(eff_histo->GetBinContent(1) != 0)
						efficiency = 1 / eff_histo->GetBinContent(1);
     
					double histo_entries = combined_histo->GetEntries();
					double scaling_factor = cross_section[file_name->second[file_nr]]*integrated_luminosity*efficiency*histo_entries;
					
					if(filter_efficiencies.find(file_name->second[file_nr]) != filter_efficiencies.end()){
						scaling_factor *= filter_efficiencies[file_name->second[file_nr]];
					}

					event_counter = cross_section[file_name->second[file_nr]]*integrated_luminosity;

					if(filter_efficiencies[file_name->second[file_nr]] != -1)
					{
						combined_histo->Scale(1.0/combined_histo->Integral());
						combined_histo->Scale(scaling_factor);
					}
				}else
					std::cerr << "WARNING: MergeHistos:: no cross section found for " << file_name->second[file_nr] << std::endl;
			}
			else{ 
			
				HistoClass *histo_to_add = (HistoClass *) file->GetDirectory(directory_name.c_str())->Get(file_name->second[file_nr].c_str()); 
//				histo_to_add->Sumw2();

				HistoClass *eff_histo = (HistoClass *) file->GetDirectory(directory_name.c_str())->Get(event_counters[file_name->second[file_nr].c_str()].c_str());

				double efficiency =0;
				if(eff_histo->GetBinContent(1) != 0)
					efficiency = 1 / eff_histo->GetBinContent(1);

				double histo_entries = histo_to_add->GetEntries();
				double scaling_factor = integrated_luminosity*efficiency*histo_entries;

				if(cross_section.find(file_name->second[file_nr]) != cross_section.end()){
					scaling_factor *= cross_section[file_name->second[file_nr]];
				}else
					std::cerr << "WARNING: MergeHistos:: no cross section found for " << file_name->second[file_nr] << std::endl;

				if(filter_efficiencies.find(file_name->second[file_nr]) != filter_efficiencies.end()){
					scaling_factor *= filter_efficiencies[file_name->second[file_nr]];
				}

				event_counter += cross_section[file_name->second[file_nr]]*integrated_luminosity;

				if(filter_efficiencies[file_name->second[file_nr]] != -1)
				{
					histo_to_add->Scale(1.0/histo_to_add->Integral());
					histo_to_add->Scale(scaling_factor);
				}
				if(scaling_factor != 0)
					combined_histo->Add(histo_to_add);
			}
		}

		delete file;
		file = NULL;
	}

	return combined_histo;
}

