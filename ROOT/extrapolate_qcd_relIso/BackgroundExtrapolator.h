#ifndef BACKGROUNDEXTRAPOLATOR_H
#define BACKGROUNDEXTRAPOLATOR_H

#include "TH1F.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TF1.h"
#include "Canvas_Holder.h"

#include <RooGlobalFunc.h>
#include <RooRealVar.h>
#include <RooAbsReal.h>
#include <RooChi2Var.h>
#include <RooGaussian.h>
#include <RooPlot.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooExponential.h>
#include <RooAddPdf.h>
#include <RooFitResult.h>
#include <RooArgusBG.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooLandau.h>
#include <RooMinuit.h>


class BackgroundExtrapolator{
	public:
		BackgroundExtrapolator();
		~BackgroundExtrapolator();
		void set_signal_histo(TH1F *signal_histo);
		void set_background_histo(TH1F *background_histo);
		void set_outfile(TFile *file);

		void extrapolate_to_signal_region();
	private:
		void fit_roofit_landau(TH1F *histo);
		void fit_root_landau(TH1F *histo);

		TH1F *signal_histo;
		TH1F *background_histo;
		TFile *outfile;
};

#endif
