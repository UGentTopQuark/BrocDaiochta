#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"

#include "TMVA/Factory.h"
#include "TMVA/Tools.h"

int main(int argc, char **argv)
{
	TString outfileName( "TMVA.root" );
	TFile* outputFile = TFile::Open( outfileName, "RECREATE" );
	TMVA::Factory *factory = new TMVA::Factory( "TMVAClassification", outputFile, "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" );
//	factory->AddVariable("sphericity", "sphericity", "", 'F');
	factory->AddVariable("aplanarity", "aplanarity", "", 'F');
//	factory->AddVariable("HlT", "HlT", "[GeV]", 'F');
	factory->AddVariable("H3T", "H3T", "[GeV]", 'F');
	factory->AddVariable("Mevent", "M_{event}", "", 'F');
	factory->AddVariable("Mj2nulT", "M^{j2 #nu l}", "", 'F');
	factory->AddVariable("m3", "M3", "", 'F');
//	factory->AddVariable("HT", "HT", "", 'F');
//	factory->AddVariable("MET", "MET", "", 'F');
//	factory->AddVariable("drje", "dr(j,e)", "", 'F');
//	factory->AddVariable("detaje", "deta(j,e)", "", 'F');
//	factory->AddVariable("dphiemet", "dphi(met,e)", "", 'F');
//	factory->AddVariable("e1_eta", "e eta", "", 'F');
//	factory->AddVariable("e1_pt", "e pt", "", 'F');
//	factory->AddVariable("jet1_pt", "j1 pt", "", 'F');
	factory->AddVariable("jet2_pt", "j2 pt", "", 'F');
//	factory->AddVariable("jet3_pt", "j3 pt", "", 'F');
	factory->AddVariable("jet4_pt", "j4 pt", "", 'F');
//	factory->AddVariable("jet1_eta", "j1 eta", "", 'F');
//	factory->AddVariable("jet2_eta", "j2 eta", "", 'F');
//	factory->AddVariable("jet3_eta", "j3 eta", "", 'F');
//	factory->AddVariable("jet4_eta", "j4 eta", "", 'F');
//	factory->AddVariable("njets", "njets", "", 'F');
	factory->AddVariable("drjm", "dr(j,mu)", "", 'F');
	factory->AddVariable("detajm", "deta(j,mu)", "", 'F');
	factory->AddVariable("dphimmet", "dphi(met,mu)", "", 'F');
	factory->AddVariable("mu1_eta", "mu eta", "", 'F');
//	factory->AddVariable("mu1_pt", "mu pt", "", 'F');

/*
	TFile *signal_input = TFile::Open( "flat_tree_TTbar_electron_01_cutset.root" );
	TFile *ttbar_other_input = TFile::Open( "flat_tree_TTbar_e_background_01_cutset.root" );
	TFile *wjets_input = TFile::Open( "flat_tree_Wjets_e_background_01_cutset.root" );
	TFile *zjets_input = TFile::Open( "flat_tree_DY50_e_background_01_cutset.root" );
	TFile *t_t_chan_input = TFile::Open( "flat_tree_TtChan_e_background_01_cutset.root" );
	TFile *t_s_chan_input = TFile::Open( "flat_tree_TsChan_e_background_01_cutset.root" );
	TFile *t_tW_chan_input = TFile::Open( "flat_tree_TtWChan_e_background_01_cutset.root" );
	TFile *eqcd_bc2e_2030_input = TFile::Open( "flat_tree_QCDbctoe20to30_e_background_01_cutset.root" );
	TFile *eqcd_bc2e_3080_input = TFile::Open( "flat_tree_QCDbctoe30to80_e_background_01_cutset.root" );
	TFile *eqcd_bc2e_80170_input = TFile::Open( "flat_tree_QCDbctoe80to170_e_background_01_cutset.root" );
	TFile *eqcd_em_2030_input = TFile::Open( "flat_tree_QCDem20to30_e_background_01_cutset.root" );
	TFile *eqcd_em_3080_input = TFile::Open( "flat_tree_QCDem30to80_e_background_01_cutset.root" );
	TFile *eqcd_em_80170_input = TFile::Open( "flat_tree_QCDem80to170_e_background_01_cutset.root" );

	TTree *signal = (TTree*) signal_input->Get("tree");
	TTree *ttbar_other_tree = (TTree*) ttbar_other_input->Get("tree");
	TTree *wjets_tree = (TTree*) wjets_input->Get("tree");
	TTree *zjets_tree = (TTree*) zjets_input->Get("tree");
	TTree *t_t_chan_tree = (TTree*) t_t_chan_input->Get("tree");
	TTree *t_s_chan_tree = (TTree*) t_s_chan_input->Get("tree");
	TTree *t_tW_chan_tree = (TTree*) t_tW_chan_input->Get("tree");
	TTree *eqcd_bc2e_2030_tree = (TTree*) eqcd_bc2e_2030_input->Get("tree");
	TTree *eqcd_bc2e_3080_tree = (TTree*) eqcd_bc2e_3080_input->Get("tree");
	TTree *eqcd_bc2e_80170_tree = (TTree*) eqcd_bc2e_80170_input->Get("tree");
	TTree *eqcd_em_2030_tree = (TTree*) eqcd_em_2030_input->Get("tree");
	TTree *eqcd_em_3080_tree = (TTree*) eqcd_em_3080_input->Get("tree");
	TTree *eqcd_em_80170_tree = (TTree*) eqcd_em_80170_input->Get("tree");

	factory->AddSignalTree(signal);
	factory->AddBackgroundTree(ttbar_other_tree, 0.04);
	factory->AddBackgroundTree(wjets_tree,0.54);
	factory->AddBackgroundTree(zjets_tree,0.11);
	factory->AddBackgroundTree(t_t_chan_tree,0.013);
	factory->AddBackgroundTree(t_s_chan_tree,0.0025);
	factory->AddBackgroundTree(t_tW_chan_tree,0.019);
	//std::cout << "adding eqcd bc2e 2030" << std::endl;
//	factory->AddBackgroundTree(eqcd_bc2e_2030_tree,0.28);
	//std::cout << "adding eqcd bc2e 3080" << std::endl;
//	factory->AddBackgroundTree(eqcd_bc2e_3080_tree,0.28);
	std::cout << "adding eqcd bc2e 80170" << std::endl;
	factory->AddBackgroundTree(eqcd_bc2e_80170_tree,0.28);
	//std::cout << "adding eqcd em 2030" << std::endl;
//	factory->AddBackgroundTree(eqcd_em_2030_tree,0.28);
	std::cout << "adding eqcd em 3080" << std::endl;
	factory->AddBackgroundTree(eqcd_em_3080_tree,0.28);
	std::cout << "adding eqcd em 80170" << std::endl;
	factory->AddBackgroundTree(eqcd_em_80170_tree,0.28);
*/

	TFile *signal_input = TFile::Open( "flat_tree_TTbar_muon_03_cutset.root" );
	TFile *ttbar_other_input = TFile::Open( "flat_tree_TTbar_mu_background_03_cutset.root" );
	TFile *wjets_input = TFile::Open( "flat_tree_Wjets_mu_background_03_cutset.root" );
	TFile *zjets_input = TFile::Open( "flat_tree_Zjets_mu_background_03_cutset.root" );
	TFile *t_t_chan_input = TFile::Open( "flat_tree_TtChan_mu_background_03_cutset.root" );
	TFile *t_s_chan_input = TFile::Open( "flat_tree_TsChan_mu_background_03_cutset.root" );
	TFile *t_tW_chan_input = TFile::Open( "flat_tree_TtWChan_mu_background_03_cutset.root" );
	TFile *muqcd_input = TFile::Open( "flat_tree_MuQCD_mu_background_03_cutset.root" );

	TTree *signal = (TTree*) signal_input->Get("tree");
	TTree *ttbar_other_tree = (TTree*) ttbar_other_input->Get("tree");
	TTree *wjets_tree = (TTree*) wjets_input->Get("tree");
	TTree *zjets_tree = (TTree*) zjets_input->Get("tree");
	TTree *t_t_chan_tree = (TTree*) t_t_chan_input->Get("tree");
	TTree *t_s_chan_tree = (TTree*) t_s_chan_input->Get("tree");
	TTree *t_tW_chan_tree = (TTree*) t_tW_chan_input->Get("tree");
	TTree *muqcd_tree = (TTree*) muqcd_input->Get("tree");

	factory->AddSignalTree(signal);
	factory->AddBackgroundTree(ttbar_other_tree, 0.054);
	factory->AddBackgroundTree(wjets_tree,0.775);
	factory->AddBackgroundTree(zjets_tree,0.063);
	factory->AddBackgroundTree(t_t_chan_tree,0.0045);
	factory->AddBackgroundTree(t_s_chan_tree,0.001);
	factory->AddBackgroundTree(t_tW_chan_tree,0.014);
	factory->AddBackgroundTree(muqcd_tree,0.0876);

	factory->SetWeightExpression("weight");
/*
	// 2010 data, 3 jets
	factory->AddSignalTree(signal, 23.7);
	factory->AddBackgroundTree(ttbar_other_tree, 4.4);
	factory->AddBackgroundTree(wjets_tree,38.4);
	factory->AddBackgroundTree(zjets_tree,6.7);
	factory->AddBackgroundTree(t_t_chan_tree,1.4);
	factory->AddBackgroundTree(t_s_chan_tree,0.2);
	factory->AddBackgroundTree(t_tW_chan_tree,1.2);
	//std::cout << "adding eqcd bc2e 2030" << std::endl;
	//factory->AddBackgroundTree(eqcd_bc2e_2030_tree);
	//std::cout << "adding eqcd bc2e 3080" << std::endl;
	//factory->AddBackgroundTree(eqcd_bc2e_3080_tree);
	std::cout << "adding eqcd bc2e 80170" << std::endl;
	factory->AddBackgroundTree(eqcd_bc2e_80170_tree,24.5);
	//std::cout << "adding eqcd em 2030" << std::endl;
	//factory->AddBackgroundTree(eqcd_em_2030_tree);
	std::cout << "adding eqcd em 3080" << std::endl;
	factory->AddBackgroundTree(eqcd_em_3080_tree,24.5);
	std::cout << "adding eqcd em 80170" << std::endl;
	factory->AddBackgroundTree(eqcd_em_80170_tree,24.5);
*/

	TCut mycuts = ""; // for example: TCut mycuts = "abs(var1)<0.5 && abs(var2-0.5)<1";
	TCut mycutb = ""; // for example: TCut mycutb = "abs(var1)<0.5";
	
	// Tell the factory how to use the training and testing events
	//
	// If no numbers of events are given, half of the events in the tree are used 
	// for training, and the other half for testing:
	//    factory->PrepareTrainingAndTestTree( mycut, "SplitMode=random:!V" );
	// To also specify the number of testing events, use:
	//    factory->PrepareTrainingAndTestTree( mycut,
	//                                         "NSigTrain=3000:NBkgTrain=3000:NSigTest=3000:NBkgTest=3000:SplitMode=Random:!V" );
	factory->PrepareTrainingAndTestTree( mycuts, mycutb,
                                        "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=NumEvents:!V" );

	//factory->BookMethod( TMVA::Types::kMLP, "MLP", "H:!V:NeuronType=tanh:VarTransform=N:NCycles=600:HiddenLayers=N+5:TestRate=5" );
	//factory->BookMethod( TMVA::Types::kMLP, "MLP", "H:!V:NeuronType=tanh:VarTransform=D,G,N:NCycles=100:HiddenLayers=N+5:TestRate=5:TrainingMethod=BFGS" );
	//factory->BookMethod( TMVA::Types::kMLP, "MLP_nopp", "H:!V:NeuronType=tanh:NCycles=100:HiddenLayers=N+5:TestRate=5:TrainingMethod=BFGS" );
	//factory->BookMethod( TMVA::Types::kPlugins, "NeuroBayes1", "H:V:NTrainingIter=0:TrainingMethod=BFGS:Preprocessing=622:NBIndiPreproFlagList=14,14,14,14,14,14,14,14:ShapeTreat=DIAG" );
	//factory->BookMethod( TMVA::Types::kPlugins, "NeuroBayes2", "H:V:NTrainingIter=100:TrainingMethod=BFGS:Preprocessing=612:NBIndiPreproFlagList=14,14,14,14,14,14,14,14:ShapeTreat=OFF" );
	factory->BookMethod( TMVA::Types::kPlugins, "NeuroBayes2", "H:V:NTrainingIter=100:TrainingMethod=BFGS:Preprocessing=612:NBIndiPreproFlagList=14,14,14,14,14,14,14,14,14,14,14:ShapeTreat=OFF" );
	//factory->BookMethod( TMVA::Types::kPlugins, "NeuroBayes3", "H:V:NTrainingIter=100:TrainingMethod=BFGS:Preprocessing=622:NBIndiPreproFlagList=14,14,14,14,14,14,14,14:ShapeTreat=DIAG" );
	//factory->BookMethod( TMVA::Types::kBDT,"BDT", "!H:V:NTrees=100:BoostType=Bagging:SeparationType=GiniIndex:nCuts=20:PruneMethod=CostComplexity:PruneStrength=-1:UseRandomisedTrees=kTrue:UseNvars=9:nEventsMin=700");
	//factory->BookMethod( TMVA::Types::kSVM, "SVM", "Gamma=0.25:Tol=0.001:VarTransform=Norm" );
	//factory->BookMethod( TMVA::Types::kBDT, "BDT",
	//                           "!H:!V:NTrees=850:nEventsMin=150:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:SeparationType=GiniIndex:nCuts=20:PruneMethod=NoPruning" );

	factory->TrainAllMethods();
	factory->TestAllMethods();
	factory->EvaluateAllMethods();

	outputFile->Close();
	delete factory;
}
