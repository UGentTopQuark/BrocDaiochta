#ifndef CORRELATIONCHECKER_H
#define CORRELATIONCHECKER_H

#include "TFile.h"
#include "TH2F.h"
#include "TH2F.h"
#include <iostream>

#include "Canvas_Holder.h"
#include "Tools.h"

class CorrelationChecker{
	public:
		CorrelationChecker(std::string ident);
		CorrelationChecker(std::string ident, TFile *outfile);
		~CorrelationChecker();
		void common_constructor(std::string ident);
		void set_outfile(TFile *outfile);
		void check_correlations(TH2F *histo);
		void rebin_projection_x(int nrebin);
		void rebin_projection_y(int nrebin);
	private:
		void plot_marginal_distributions();
		double calculate_correlation_coeff_error(double r, double Nevents); // estimator for corr. coeff.: r

		std::string ident;
		TH2F *histo;
		TFile *outfile;
		Tools *tool;

		int rebin_proj_x;
		int rebin_proj_y;

		const static int MAX_NINTERVALS=3;
		const static bool verbose = true;
};

#endif
