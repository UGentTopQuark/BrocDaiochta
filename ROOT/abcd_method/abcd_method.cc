#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "MergeHistos/MergeHistos.h"
#include "MatrixMethod.h"

int main(int argc, char **argv)
{
	TFile *outfile = new TFile("outfile.root", "RECREATE");

	std::string cutset = "01_cutset";
	std::string version = "_plots";

	/*
	 * define ABCD regions
	 * A = 1, B = 2, ...
	 */
	MatrixMethod *abcd_method = new MatrixMethod();
/*
	std::string histo_name = "isolation_vs_met_muon_";
	// met vs trackiso
	abcd_method->define_region(0,0.,2.,40.,80.);
	abcd_method->define_region(1,5.,40.,40.,80.);
	abcd_method->define_region(2,0.,2.,10.,30.);
	abcd_method->define_region(3,5.,40.,10.,30.);
*/

	std::string histo_name = "d0sig_vs_comb_rel_iso_muon_";
	abcd_method->define_region(0,0.95,1.0,0.0,3.0);
	abcd_method->define_region(1,0.0,0.7,0.0,3.0);
	abcd_method->define_region(2,0.95,1.0,5.0,10.0);
	abcd_method->define_region(3,0.0,0.7,5.0,10.0);
/*
*/
/*
	std::string histo_name = "d0_vs_calo_iso_muon_";
	abcd_method->define_region(0,0.0,1.,0.0,0.02);
	abcd_method->define_region(1,5.0,40.0,0.0,0.02);
	abcd_method->define_region(2,0.0,1.0,0.025,0.15);
	abcd_method->define_region(3,5.0,40.0,0.025,0.15);
*/

/*
	std::string histo_name = "d0_vs_calo_iso_electron_";
	abcd_method->define_region(0,0.0,5.,0.0,0.02);
	abcd_method->define_region(1,10.0,40.0,0.0,0.02);
	abcd_method->define_region(2,0.0,5.0,0.025,0.15);
	abcd_method->define_region(3,10.0,40.0,0.025,0.15);
*/

	std::map<std::string,std::vector<std::string> > signal_file_names;
        signal_file_names["ttbar"+version+".root"].push_back(histo_name+"TTbar|muon|"+cutset);
//        signal_file_names["ttbar"+version+".root"].push_back(histo_name+"TTbar|electron|"+cutset);

	std::map<std::string,std::vector<std::string> > background_file_names;
//        background_file_names["ttbar"+version+".root"].push_back(histo_name+"TTbar|mu_background|"+cutset);
//        background_file_names["wjets"+version+".root"].push_back(histo_name+"Wjets|mu_background|"+cutset);
//        background_file_names["zjets"+version+".root"].push_back(histo_name+"Zjets|mu_background|"+cutset);
        background_file_names["qcdmupt15"+version+".root"].push_back(histo_name+"Mupt15|mu_background|"+cutset);
/*
        background_file_names["qcdbctoe20to30"+version+".root"].push_back(histo_name+"QCDbctoe20to30|e_background|"+cutset);
        background_file_names["qcdbctoe30to80"+version+".root"].push_back(histo_name+"QCDbctoe30to80|e_background|"+cutset);
        background_file_names["qcdbctoe80to170"+version+".root"].push_back(histo_name+"QCDbctoe80to170|e_background|"+cutset);
        background_file_names["qcdem20to30"+version+".root"].push_back(histo_name+"QCDem20to30|e_background|"+cutset);
        background_file_names["qcdem30to80"+version+".root"].push_back(histo_name+"QCDem30to80|e_background|"+cutset);
        background_file_names["qcdem80to170"+version+".root"].push_back(histo_name+"QCDem80to170|e_background|"+cutset);
*/
//        background_file_names["t_s_chan"+version+".root"].push_back(histo_name+"TsChan|mu_background|"+cutset);
//        background_file_names["t_t_chan"+version+".root"].push_back(histo_name+"TtChan|mu_background|"+cutset);
//        background_file_names["t_tW_chan"+version+".root"].push_back(histo_name+"TtWChan|mu_background|"+cutset);

	std::cout << "setting outfile..." << std::endl;
	abcd_method->set_outfile(outfile);

	std::cout << "setting histo name..." << std::endl;
	abcd_method->set_histo_name(histo_name);
	std::cout << "setting cutset..." << std::endl;
	abcd_method->set_cutset(cutset);
	std::cout << "setting integrated luminosity..." << std::endl;
	abcd_method->set_integrated_luminosity(200.0);
	std::cout << "setting signal file names..." << std::endl;
	abcd_method->set_signal_file_names(signal_file_names);
	std::cout << "setting background file names..." << std::endl;
	abcd_method->set_background_file_names(background_file_names);
	std::cout << "estimating entries in A: " << std::endl;
	abcd_method->get_nbackground_events();
	std::cout << "checking stability of method:" << std::endl;
	abcd_method->check_stability(1);

	delete abcd_method;
	abcd_method = NULL;
	delete outfile;
	outfile = NULL;

	return 0;
}
