#include "MatrixMethod.h"

MatrixMethod::MatrixMethod()
{
	signal_histo = NULL;
	background_histo = NULL;
	combined_histo = NULL;

	histo_name = "";

	// initialise arrays
	for(int i = 0; i < NREGIONS; ++i){
		x_min[i] = -1;
		x_max[i] = -1;
		y_min[i] = -1;
		y_max[i] = -1;
		nominal_x_min[i] = -1;
		nominal_x_max[i] = -1;
		nominal_y_min[i] = -1;
		nominal_y_max[i] = -1;
	}

	signal_region = 1;
	outfile = NULL; 

	histo_merger = NULL;

	tool = new Tools();
}

MatrixMethod::~MatrixMethod()
{
	if(histo_merger){
		delete histo_merger;
		histo_merger = NULL;
	}
}
 
void MatrixMethod::define_region(int region, double min_x, double max_x, double min_y, double max_y)
{
	nominal_x_min[region] = min_x;
	nominal_x_max[region] = max_x;
	nominal_y_min[region] = min_y;
	nominal_y_max[region] = max_y;
}

void MatrixMethod::set_signal_region(int region)
{
	signal_region = region;
}

void MatrixMethod::check_stability(int nintervals)
{
	double variation_B_D = -1;
	double variation_C_D = -1;

	/*
	 *	ASSUMPTION:
	 *		- A/B share same Y-coordinates
	 *		- C/D share same Y-coordinates
	 *		- A/C share same X-coordinates
	 *		- D/B share same Y-coordinates
	 *
	 */

	/*			  |	     |
	 *			  | var_B_D  |
	 * 	-------------------          ------------------
	 *	|		  |	     |		      |
	 *	|	C	  |	     |	D	      |
	 *	|		  |	     |		      |
	 *	|		  |	     |		      |
	 *-----	-------------------	     ------------------
	 *variation_C_D	  	     		      
	 *-----	-------------------	     ------------------
	 *	|		  |	     |		      |
	 *	|	A	  |	     |	B	      |
	 *	|		  |	     |		      |
	 *	|		  |	     |		      |
	 * 	-------------------          ------------------
	 */
	//	if A higher min value than max value of B
	if(fabs(nominal_x_min[0] - nominal_x_max[1]) < fabs(nominal_x_max[0] - nominal_x_min[1])){
		variation_B_D = fabs(nominal_x_min[0] - nominal_x_max[1]);
	}else{// else (B higher min value than max value of A
		variation_B_D = fabs(nominal_x_max[0] - nominal_x_min[1]);
	}

	//	if the space between region A and B is larger than the half
	//	width of region B, use half width of region B instead
	if(variation_B_D > 0.5*fabs(nominal_x_max[1] - nominal_x_min[1])){
		variation_B_D = 0.5*fabs(nominal_x_max[1] - nominal_x_min[1]);
	}

	//	if A higher min value than max value of C
	if(fabs(nominal_y_min[0] - nominal_x_max[2]) < fabs(nominal_x_max[0] - nominal_y_min[2])){
		variation_C_D = fabs(nominal_y_min[0] - nominal_x_max[2]);
	}else{// else (C higher min value than max value of A
		variation_C_D = fabs(nominal_y_max[0] - nominal_y_min[2]);
	}

	//	if the space between region A and C is larger than the half
	//	width of region C, use half width of region C instead
	if(variation_C_D > 0.5*fabs(nominal_y_max[2] - nominal_y_min[2])){
		variation_C_D = 0.5*fabs(nominal_y_max[2] - nominal_y_min[2]);
	}

	std::cout << "variation B/D: " << variation_B_D << std::endl;
	std::cout << "variation C/D: " << variation_C_D << std::endl;

	bool A_higher_than_B=false;
	bool A_higher_than_C=false;
	// is A to the right of B?
	if(nominal_x_min[0] > nominal_x_min[1]) A_higher_than_B = true;
	// is A above C?
	if(nominal_y_min[0] > nominal_y_min[2]) A_higher_than_C = true;

	CanvasHolder cholder;
	double x_value[nintervals*2+1][nintervals*2+1];
	double y_value[nintervals*2+1][nintervals*2+1];
	double x_value_err[nintervals*2+1][nintervals*2+1];
	double y_value_err[nintervals*2+1][nintervals*2+1];

	reset_regions();
	estimate nominal_estimate = estimate_nbackground_events();
	double nominal_estimate_x = nominal_estimate.estimate;

	int outer_interval=0;
	double true_value = -1;
	/*
	 *	process all combinations of border changes
	 */
	for(double change_B_D = -variation_B_D; change_B_D <= variation_B_D;
		change_B_D += (variation_B_D/(double) nintervals)){
		int interval=0;

		for(double change_C_D = -variation_C_D; change_C_D <= variation_C_D;
			change_C_D += (variation_C_D/(double) nintervals)){
			// reset values to nominal values
			reset_regions();
			//	change common border-line of B/D
			if(A_higher_than_B){
				x_max[1] = nominal_x_max[1] + change_B_D;
				x_max[3] = nominal_x_max[3] + change_B_D;
			}else{
				x_min[1] = nominal_x_min[1] + change_B_D;
				x_min[3] = nominal_x_min[3] + change_B_D;
			}

			//	change common border-line of C/D
			if(A_higher_than_C){
				y_max[2] = nominal_y_max[2] + change_C_D;
				y_max[3] = nominal_y_max[3] + change_C_D;
				x_value[outer_interval][interval] = y_max[2];
			}else{
				y_min[2] = nominal_y_min[2] + change_C_D;
				y_min[3] = nominal_y_min[3] + change_C_D;
				x_value[outer_interval][interval] = y_min[2];
			}

			estimate my_estimate = estimate_nbackground_events();
			if(my_estimate.true_value == 0) std::cerr << "WARNING: check_stability(): estimate true value equals 0" << std::endl;

			true_value = my_estimate.true_value;
			// calculate relative difference between estimated
			// value and true value
			y_value[outer_interval][interval] = (my_estimate.estimate-my_estimate.true_value)/my_estimate.true_value;
			x_value_err[outer_interval][interval] = 0;

			/*
			 * calculate errors
			 */
			
//			y_value_err[outer_interval][interval] = my_estimate.error/my_estimate.true_value;
			/*
			 *	show error on difference between nominal value
			 *	of estimate (without changed borders) and the
			 *	current value with changed borders
			 */
			double diff_nominal_current_value = fabs(nominal_estimate_x - my_estimate.estimate);
			std::cout << "diff nominal - current value: " << diff_nominal_current_value << " +/- " << sqrt(diff_nominal_current_value) << std::endl;
			std::cout << "nominal estimate: " << nominal_estimate_x << std::endl;
			std::cout << "current estimate: " << my_estimate.estimate << std::endl;
			std::cout << "current true value: " << my_estimate.true_value << std::endl;
			y_value_err[outer_interval][interval] = (1./diff_nominal_current_value*sqrt(diff_nominal_current_value));

			interval++;
		}
		TGraphErrors *my_graph = new TGraphErrors(nintervals*2+1,x_value[outer_interval],y_value[outer_interval],x_value_err[outer_interval],y_value_err[outer_interval]);
		std::string leg_entry = background_histo->GetXaxis()->GetTitle();
		//	common border-line of B/D
		if(A_higher_than_B){
			double my_xmax = nominal_x_max[1] + change_B_D;
			leg_entry += " "+tool->stringify(nominal_x_min[1])+"-"+tool->stringify(my_xmax);
		}else{
			double my_xmin = nominal_x_min[1] + change_B_D;
			leg_entry += " "+tool->stringify(my_xmin)+"-"+tool->stringify(nominal_x_max[1]);
		}

		cholder.addGraph(my_graph,leg_entry,leg_entry,"*L");

		outer_interval++;
	}
	cholder.setOptStat(000000);
	cholder.setLegDraw(true);
	cholder.setMarkerSizeGraph(1.0);
	cholder.addHLine(0.0);
	cholder.setTitleX(background_histo->GetYaxis()->GetTitle());
	cholder.setTitleY("|N_{true}-N_{est}|/N_{true}");
	cholder.setTitle("");
	cholder.write(outfile);
	cholder.save("stability.eps");
}

double MatrixMethod::get_nbackground_events()
{
	reset_regions();

	return estimate_nbackground_events(true).estimate;
}

MatrixMethod::estimate MatrixMethod::estimate_nbackground_events(bool check_correlations)
{
	if(!signal_histo || !background_histo){
		std::cerr << "WARNING: MatrixMethod::estimate_nbackground_events(): histogram not set" << std::endl;
		estimate empty;
		empty.true_value=-1;
		empty.estimate=-1;
		empty.error=-1;
		return empty;
	}
	if(!outfile){
		std::cerr << "WARNING: MatrixMethod::estimate_nbackground_events(): outfile not set" << std::endl;
		estimate empty;
		empty.true_value=-1;
		empty.estimate=-1;
		empty.error=-1;
		return empty;
	}

	// combine background_files and signal_files
	combined_histo = new TH2F(*background_histo);
	combined_histo->Add(signal_histo);

	double nestimated_background=-1;

	double err_nevt_A=-1;
	double err_nevt_B=-1;
	double err_nevt_C=-1;
	double err_nevt_D=-1;

	// calculate number of events in different regions
	double neventsA = combined_histo->IntegralAndError(combined_histo->GetXaxis()->FindBin(x_min[0]),
						   combined_histo->GetXaxis()->FindBin(x_max[0]),
						   combined_histo->GetYaxis()->FindBin(y_min[0]),
						   combined_histo->GetYaxis()->FindBin(y_max[0]),
						   err_nevt_A);
	double neventsB = combined_histo->IntegralAndError(combined_histo->GetXaxis()->FindBin(x_min[1]),
						   combined_histo->GetXaxis()->FindBin(x_max[1]),
						   combined_histo->GetYaxis()->FindBin(y_min[1]),
						   combined_histo->GetYaxis()->FindBin(y_max[1]),
						   err_nevt_B);
	double neventsC = combined_histo->IntegralAndError(combined_histo->GetXaxis()->FindBin(x_min[2]),
						   combined_histo->GetXaxis()->FindBin(x_max[2]),
						   combined_histo->GetYaxis()->FindBin(y_min[2]),
						   combined_histo->GetYaxis()->FindBin(y_max[2]),
						   err_nevt_C);
	double neventsD = combined_histo->IntegralAndError(combined_histo->GetXaxis()->FindBin(x_min[3]),
						   combined_histo->GetXaxis()->FindBin(x_max[3]),
						   combined_histo->GetYaxis()->FindBin(y_min[3]),
						   combined_histo->GetYaxis()->FindBin(y_max[3]),
						   err_nevt_D);
	double nevents_signalA = signal_histo->Integral(combined_histo->GetXaxis()->FindBin(x_min[0]),
						   combined_histo->GetXaxis()->FindBin(x_max[0]),
						   combined_histo->GetYaxis()->FindBin(y_min[0]),
						   combined_histo->GetYaxis()->FindBin(y_max[0]));
	double nevents_backgroundA = background_histo->Integral(combined_histo->GetXaxis()->FindBin(x_min[0]),
						   combined_histo->GetXaxis()->FindBin(x_max[0]),
						   combined_histo->GetYaxis()->FindBin(y_min[0]),
						   combined_histo->GetYaxis()->FindBin(y_max[0]));

	// estimate number of events with matrix method A = B*C/D
	if(neventsD){
		nestimated_background = neventsB * neventsC / neventsD;
	}else{
		std::cerr << "WARNING: nevents in region D equals 0" << std::endl;
	}

	double error_nestimated_background = sqrt((neventsC/neventsD)*(neventsC/neventsD)*err_nevt_B*err_nevt_B +
					(neventsB*neventsB)/(neventsD*neventsD)*err_nevt_C*err_nevt_C +
					(neventsC*neventsB*neventsC*neventsB)/(neventsD*neventsD*neventsD*neventsD)*
					err_nevt_D*err_nevt_D);

	std::cout << "nevents region A: " << neventsA << std::endl;
	std::cout << "nevents region B: " << neventsB << std::endl;
	std::cout << "nevents region C: " << neventsC << std::endl;
	std::cout << "nevents region D: " << neventsD << std::endl;
	std::cout << "--------------------------" << std::endl;
	std::cout << "true # signal events region A: " << nevents_signalA << std::endl;
	std::cout << "true # background events region A: " << nevents_backgroundA << std::endl;
	std::cout << "estimated # background events in region A: " << nestimated_background << " +/- " << error_nestimated_background << std::endl;
	
	CorrelationChecker backg_correlations("background", outfile);

	if(check_correlations) backg_correlations.check_correlations(background_histo);
	
	CanvasHolder cholder;

	// add signal and background to canvas
	cholder.addHisto(background_histo, "QCD", "");
	cholder.addHisto(signal_histo, "t#bar{t}", "");

	// add lines to point out the different regions
	for(int i = 0; i < NREGIONS; ++i){
		cholder.addLine(x_min[i], x_max[i], y_min[i], y_min[i], "", 3);
		cholder.addLine(x_min[i], x_max[i], y_max[i], y_max[i], "", 3);
		cholder.addLine(x_min[i], x_min[i], y_min[i], y_max[i], "", 3);
		cholder.addLine(x_max[i], x_max[i], y_min[i], y_max[i], "", 3);
	}

	cholder.setOptStat(000000);
	cholder.setLegDraw(false);
	cholder.write(outfile);
	cholder.save("abcd_matrix.eps");

	estimate my_estimate;
	my_estimate.true_value = nevents_backgroundA;
	my_estimate.estimate = nestimated_background;
	my_estimate.error = error_nestimated_background;

	return my_estimate;
}

void MatrixMethod::set_outfile(TFile *outfile)
{
	this->outfile = outfile;
	histo_merger = new MergeHistos(outfile);
}

void MatrixMethod::set_cutset(std::string cutset)
{
	histo_merger->set_cutset(cutset);
}

void MatrixMethod::set_signal_file_names(std::map<std::string,std::vector<std::string> > &file_names)
{
	signal_file_names = file_names;
	histo_merger->set_file_names(file_names);
	histo_merger->set_histo_name(histo_name);
	histo_merger->set_cross_section();
	signal_histo = histo_merger->combine_histo<TH2F>();
}

void MatrixMethod::set_background_file_names(std::map<std::string,std::vector<std::string> > &file_names)
{
	background_file_names = file_names;
	histo_merger->set_file_names(file_names);
	histo_merger->set_histo_name(histo_name);
	histo_merger->set_cross_section();
	background_histo = histo_merger->combine_histo<TH2F>();
}

void MatrixMethod::set_histo_name(std::string histo_name)
{
	this->histo_name = histo_name;
}

void MatrixMethod::set_integrated_luminosity(double integrated_luminosity)
{
	histo_merger->set_integrated_luminosity(integrated_luminosity);
}

void MatrixMethod::reset_regions()
{
	for(int i = 0; i < NREGIONS; ++i){
		x_min[i] = nominal_x_min[i];
		x_max[i] = nominal_x_max[i];
		y_min[i] = nominal_y_min[i];
		y_max[i] = nominal_y_max[i];
	}
}
