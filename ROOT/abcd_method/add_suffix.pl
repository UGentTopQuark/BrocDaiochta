#!/usr/bin/perl

$suffix = $ARGV[0];
$dir = '.';

opendir(DIR, $dir);
while(defined($file = readdir(DIR))){
	next unless ($file =~ /\.root$/);
	@filename = split(/\./, $file);
	$new_filename = $filename[0]."_".$suffix.'.'.$filename[1];
	print "mv $file $new_filename\n";
	`mv $file $new_filename`;
}
closedir(DIR);
