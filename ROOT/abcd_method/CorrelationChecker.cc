#include "CorrelationChecker.h"

CorrelationChecker::CorrelationChecker(std::string ident)
{
	common_constructor(ident);
}

CorrelationChecker::CorrelationChecker(std::string ident, TFile *outfile)
{
	this->outfile = outfile;
	common_constructor(ident);
}

void CorrelationChecker::common_constructor(std::string ident)
{
	this->ident = ident;
	histo = NULL;
	tool = new Tools();

	rebin_proj_x = -1;
	rebin_proj_y = -1;
}

CorrelationChecker::~CorrelationChecker()
{
	if(tool){
		delete tool;
		tool = NULL;
	}
}

void CorrelationChecker::check_correlations(TH2F *histo)
{
	this->histo = histo;

	plot_marginal_distributions();

	double r = histo->GetCorrelationFactor();	// correlation factor
	double r_error = calculate_correlation_coeff_error(r, histo->GetEntries());	// correlation factor
	std::cout << "--------------------------" << std::endl;
	std::cout << "correlation-factor in " << ident << ": " << r << " +/- " << r_error << std::endl;
	std::cout << "--------------------------" << std::endl;
}

void CorrelationChecker::set_outfile(TFile *outfile)
{
	this->outfile = outfile;
}

double CorrelationChecker::calculate_correlation_coeff_error(double r, double Nevents) // estimator for corr. coeff.: r
{
	/*
	 *	Fisher transformation:
	 *	z = tanh^(-1) r = 1/2 log (1+r)/(1-r)
	 *	E[z] = 1/2 log (1+rho)/(1-rho) + rho/(2(n-1))
	 *	V[z] = 1/(n-3)
	 */

	double z = 1./2.*log((1+r)/(1-r));
	double Vz = 1./(Nevents-3);
	double sigma_z = sqrt(Vz);
	double z_plus = z + sigma_z;

	double r_plus = fabs(fabs(r) - fabs(tanh(z_plus)));

	//std::cout << "z = " << z << " z_plus = " << z_plus << " z_minus = " << z_minus << std::endl;
	//std::cout << "r = " << r << " r_plus = " << r_plus << " r_minus = " << r_minus << std::endl;

	return r_plus;
}

void CorrelationChecker::plot_marginal_distributions()
{
	CanvasHolder cholder_x;
	CanvasHolder cholder_y;
	int max_bin_x=histo->GetNbinsX();
	int max_bin_y=histo->GetNbinsY();
	int interval_width_x = max_bin_x/MAX_NINTERVALS;	// bins per interval
	int interval_width_y = max_bin_y/MAX_NINTERVALS;	// bins per interval

	if(verbose) std::cout << "nintervals: " << MAX_NINTERVALS << std::endl;
	if(verbose) std::cout << "nbins x: " << max_bin_x << std::endl;
	if(verbose) std::cout << "nbins y: " << max_bin_y << std::endl;
	if(verbose) std::cout << "interval width x: " << interval_width_x << std::endl;
	if(verbose) std::cout << "interval width y: " << interval_width_y << std::endl;

	// plot marginal distributions independent of X
	for(int i = 0; i < MAX_NINTERVALS; ++i){
		TH1D *projection = histo->ProjectionY((ident+"_projx").c_str(),i*interval_width_x,(i+1)*interval_width_x);
		if(rebin_proj_y != -1) projection->Rebin(rebin_proj_y);
		projection->Scale(1./projection->Integral());
		std::string title = histo->GetXaxis()->GetTitle();
		title += " "+tool->stringify(histo->GetBinCenter(i*interval_width_x+1)-histo->GetBinWidth((i)*interval_width_x+1)/2.) + "-" + tool->stringify(histo->GetBinCenter((i+1)*interval_width_x)+histo->GetBinWidth((i+1)*interval_width_x)/2.);
		cholder_x.addHisto(projection,title,"");
	}

	cholder_x.setOptStat(000000);
	cholder_x.setTitle("");
	cholder_x.write(outfile);
	cholder_x.save(ident+"_projection_x.eps");

	// plot marginal distributions independent of Y
	for(int i = 0; i < MAX_NINTERVALS; ++i){
		TH1D *projection = histo->ProjectionX((ident+"_projy").c_str(),(i+1)*interval_width_y,(i+2)*interval_width_y);
		if(rebin_proj_x != -1) projection->Rebin(rebin_proj_x);
		projection->Scale(1./projection->Integral());
		std::string title = histo->GetYaxis()->GetTitle();
		title += " "+tool->stringify(histo->GetYaxis()->GetBinCenter((i)*interval_width_y+1)-histo->GetYaxis()->GetBinWidth(((i)*interval_width_x)+1)/2.) + "-" + tool->stringify(histo->GetYaxis()->GetBinCenter((i+1)*interval_width_y)+histo->GetYaxis()->GetBinWidth((i+1)*interval_width_y)/2.);
		cholder_y.addHisto(projection,title,"");
	}

	cholder_y.setOptStat(000000);
	cholder_y.setTitle("");
	cholder_y.write(outfile);
	cholder_y.save(ident+"_projection_y.eps");
}

void CorrelationChecker::rebin_projection_x(int nrebin)
{
	rebin_proj_x = nrebin;
}

void CorrelationChecker::rebin_projection_y(int nrebin)
{
	rebin_proj_y = nrebin;
}
