#ifndef __CANVAS_HOLDER__
#define __CANVAS_HOLDER__

#include <iostream>
#include <iomanip>

#include <map>
#include <vector>
#include <string>
#include <algorithm>


#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TFile.h>
#include <TColor.h>
#include <TClass.h>
#include <TLine.h>

#include <TStyle.h>
#include "ExtHisto.h"

//need vector which can be initialized like a c-array: ;)
template <class T>
class myVector:public std::vector<T> {
 public:
  myVector(int size,const T *obj) {
    for (int i=0;i<size;++i){
      push_back(obj[i]);
    }
  }
};

class CanvasHolder{
  
  class Diff_Histos {
  private:
    //there will be no private objects...
  public:
    Diff_Histos():eHistA(0),eHistB(0),use(true){}
      Diff_Histos(ExtHisto const &a,ExtHisto const &b):eHistA(a),eHistB(b),use(true){}
	ExtHisto eHistA;
	ExtHisto eHistB;
	bool use;
  };
 
 public:
  
  
  template <typename T>
    class SwitchVal {
  public:
    SwitchVal() {
      theState=false;  //da meckert der compiler - uninitialisiertusw... 
    }
    SwitchVal(T a) {
      theValue = a;
      theState = true;  //set sollte set machen... hmm
    }
    T getValue() {
      if(!theState) {
	std::cerr<<"Error: Variable not set!"<<std::endl;
      }
      return theValue;
    }
    
    inline void set(const T &val) {
      theValue = val;
      theState = true;
    }

    inline void set() {
      theState = true;
    }

    inline void unset() {
      theState=false;
    }
    
    bool getState() {
      return theState;
    }
    
  private:
    T theValue;
    bool theState;
  };
  
  CanvasHolder();
  CanvasHolder(const std::vector<std::string> &mitems,
	       const std::vector<std::string> &blacklist,
	       const std::vector<TH1*> &inhist);

  CanvasHolder(const std::vector<std::string> &mitems,
	       const std::vector<std::string> &blacklist,
	       const std::map<std::string,TGraph*> &inmap);
  


  CanvasHolder(const CanvasHolder &inCan);
  ~CanvasHolder(){
    cleanExtHistos(ehistos);
    if (!theLeg) delete theLeg; 
    if (!theCan) delete theCan;
    if (!theStyle) delete theStyle;
  }
  std::string getRnd();
  void addHisto(TH1* histo, const std::string &legentry,std::string drawopt = "");
  void addHistoStacked(TH1* histo, const std::string &legentry,std::string drawopt = "");
  void add(const CanvasHolder &B);
  void show();
  void addHistoFormated(TH1* histo, const std::string &legentry,std::string drawopt= "");
  void setStandardValues();
  
  void addGraph(TGraph* inGraph, std::string inName, std::string inLegName, std::string inDrawOpt);
  void addGraphFormated(TGraph* inGraph, std::string inName, std::string inLegName, std::string inDrawOpt);
  
  bool draw(std::string opt = "");
  void write(TFile *file, std::string opt = "");
  void save(const std::string format, std::string opt = "");
  // -------------------------


  void cleanLegend(const std::map<std::string,std::string> &repmap);
  
  void divideHisto(const std::string &search);
  void divideHisto(const ExtHisto &a);
  void exchangeAction(char action,CanvasHolder &B,const std::vector<std::string> &NoDiffIn);
  void divide(CanvasHolder &B,const std::vector<std::string> &NoDiff){
    exchangeAction('d',B,NoDiff);
  }
  void multiply(CanvasHolder &B,const std::vector<std::string> &NoDiff){
    exchangeAction('m',B,NoDiff);
  };
  
  void setLineColors(const std::vector<int> &vcolor);
  void setLineColors(const int &incolor);
  void setLineColors();
  void setLineStyles(const std::vector<int> &vstyle);
  void setLineStyles(const int &instyle);
  void setLineStyles();
  void getExtHistos(std::vector<ExtHisto> &HistVec) const ;

  
  void setOptStat(const Int_t &opt){theOptStat=opt;}
  void setOptTitle(const Int_t &opt){theOptTitle=opt;}
  
  void setLegend(const std::vector<std::string> &inLeg);
  void setLegDraw(bool gna=true){theDrawLegend=gna;}
  void setLegPos(double x1,double y1,double x2,double y2) {theLegX1=x1;theLegX2=x2;theLegY1=y1;theLegY2=y2;}
  void setLegTitle(const std::string &inLegTitle) {theLegTitle.set(inLegTitle);};
  void setLegNCol(const int inLegNCol) {theLegNCol.set(inLegNCol);};
  void setLegMargin(const double inLegMargin) {theLegMargin.set(inLegMargin);};

  void setLegDrawSymbol(std::string inLegDrawSymbol) { theLegDrawSymbol = inLegDrawSymbol;}

  void setLogX(){theLogX=true;}
  void setLogY(){theLogY=true;}
  void setLogZ(){theLogZ=true;}
    
  void setTitle(std::string title){theTitle.set(title);}
  void setCanvasTitle(std::string title) {theCanvasTitle=title;};
  void addToCanvasTitle(std::string title) {theCanvasTitle=theCanvasTitle+title;};

  void setTitleX(const std::string &titx){theTitleX.set(titx);}
  void setTitleY(const std::string &tity){theTitleY.set(tity);}
  void setTitleZ(const std::string &titz){theTitleZ.set(titz);}
  void setBinning(const std::string &bintype){
    std::vector<double> binning;
    if (bintype=="QCD"){
      fillQcdBinning(binning);
    }
    else{
      std::cerr<<"No Such Binning: "<<bintype<<std::endl;
      return;
    }
    
    doNewBinning(binning);
  }
  void setBinning(const std::vector<double> &binning){
    doNewBinning(binning);
  }
    
  void setYperBinWidth(bool b=true);
  void setCancelOutBigErr(bool b=true){if (b) doTheCancelOutBigErr();}
  

  double getBorderYMin(){
    return theMinY.getValue();
  }

  double getBorderYMax(){
    return theMaxY.getValue();
  }

  double getBorderXMin(){
    return theMinX.getValue();
  }

  double getBorderXMax(){
    return theMaxX.getValue();
  }

  void setBordersX(const double min,const double max){
    theMinX.set(min);
    theMaxX.set(max);
  }

  void setBordersY(const double min,const double max){
    theMinY.set(min);
    theMaxY.set(max);
  }


  std::string getHistoType() const {return theHistoType;}

  void normalizeHistos();
  void scaleHistos(const double &factor){doScaleHistos(factor);}

 
  void setError_1_N(const double &n){doSetError_1_N(n);}

  void addHLine(const double& inPos,
		const std::string &inLegEntry = "",
		const int &inWidth = 1,
		const int &inColor = 1, 
		const int &inStyle = 1) {
    theLines.push_back(extLine(inPos,"H",inLegEntry,inWidth,inColor,inStyle));
  }

  void addVLine(const double& inPos,
		const std::string &inLegEntry = "",
		const int &inWidth = 1,
		const int &inColor = 1, 
		const int &inStyle = 1) {
    theLines.push_back(extLine(inPos,"V",inLegEntry,inWidth,inColor,inStyle));
  }

  void addLine(const double& xminpos,
  		const double& xmaxpos,
		const double& yminpos,
		const double& ymaxpos,
		const std::string &inLegEntry = "",
		const int &inWidth = 1,
		const int &inColor = 1, 
		const int &inStyle = 1) {
    theLines.push_back(extLine(xminpos,xmaxpos,yminpos,ymaxpos,"F",inLegEntry,inWidth,inColor,inStyle));
  }

  
  void setLineStylesGraph(const int &instyle);
  void setLineColorsGraph(const int &instyle);
  void setLineSizeGraph(const int &insize);
  
  void setMarkerStylesGraph(const int &instyle);
  void setMarkerColorsGraph(const int &instyle);
  void setDrawOptGraph(std::string instring);
  void setMarkerSizeGraph(const double &instyle);
  
  void setFont(const int &inFont) {theFont.set(inFont);};
  void setYaxisOff(const double &inYaxisOff) {theYaxisOff = inYaxisOff;};
  void setXaxisOff(const double &inXaxisOff) {theXaxisOff = inXaxisOff;};

  void setLegW(const double &inVal) {theLegW = inVal;};
  void setLegH(const double &inVal) {theLegH = inVal;};
  void addLegW(const double &inVal) {theLegW = theLegW + inVal;};
  void addLegH(const double &inVal) {theLegH = theLegH + inVal;};

  

  void setLegPos(std::string inPos) {theLegPos.set(inPos);};
  void makeLegPos( const std::string &inVal);


  void addToCanleftmargin(const double &inVal) {theCanleftmargin+=inVal;};
  void addToCanrightmargin(const double &inVal) {theCanrightmargin+=inVal;};
  void addToCantopmargin(const double &inVal) {theCantopmargin+=inVal;};
  void addToCanbottommargin(const double &inVal) {theCanbottommargin+=inVal;};

  void setDrawOpt(const std::string &inDrawOpt) {theDrawOpt = inDrawOpt;};


  void addTF1(TF1* inTF1, std::string inLegName, std::string inDrawOpt) {
    extTF1 tmp(inTF1,inLegName,inDrawOpt);
    tmp.theLineColor = getNextColor();
    tmp.theLineStyle = getNextStyle();
    theTF1Vec.push_back(tmp);
  }
  
 private:
  void setDefaultStyle();
  void doSetError_1_N(const double &n);

  bool mitmatch(const std::string &hina,const std::vector<std::string> &items,const std::vector<std::string> &blacklist);

  std::vector<ExtHisto> ehistos;
  void doNewBinning(const std::vector<double> &binning);
  void reBinHisto(TH1D *&Histo,const std::vector<double> &binning);
  void doScaleHistos(const double &wert);
  void doYperBinWidth();
  void doTheCancelOutBigErr();


  void fillQcdBinning(std::vector<double> &binnings);
 

  // Canvas
  TCanvas *theCan;
  TStyle *theStyle;
  std::string theMatchString;
  std::string theCanvasTitle;
  bool theLogX,theLogY,theLogZ;
  Int_t theOptStat,theOptTitle;
  double theCanleftmargin, theCanrightmargin, theCantopmargin,theCanbottommargin;

  // Legend
  TLegend *theLeg;
  double theLegX1,theLegY1,theLegX2,theLegY2;
  bool theDrawLegend;
  SwitchVal<std::string> theLegTitle;
  SwitchVal<int> theLegNCol;
  SwitchVal<double> theLegMargin;
  std::string theLegDrawSymbol;
  double theLegW, theLegH;
  SwitchVal<std::string> theLegPos;


  // Histogramms
  SwitchVal<double> theMinX,theMaxX;
  SwitchVal<double> theMinY,theMaxY;
  SwitchVal<std::string> theTitle,theTitleX,theTitleY,theTitleZ;
  std::string theHistoType;
  bool theXaxisDec, theYaxisDec,theDoYperBinWidth, theCancelOutBigErr, theYperBinWidth;
  double theYaxisOff,theXaxisOff;
  std::string theDrawOpt;


  bool theUserSetXRange, theUserSetYRange;

  // Color and Style
  std::vector<int> fillColors;
  SwitchVal<int> theFont;
  int colorsPa[22];


  // Lines

//  std::vector<TLine> theLines;
//  std::map<TLine*,std::string> theLines;
  std::vector<extLine> theHLines;
  std::vector<extLine> theVLines;

   std::vector<extLine> theLines;
   


  std::vector<extGraph> theGraphVec;

  std::vector<extTF1> theTF1Vec;


  void doNormalizeHisto(TH1 *hist);
  
  void formatHistos();
  void formatHisto(TH1* inHisto);
  void setLineColor();

  int theNextColor; 
  int theNextStyle; 
  int getNextColor();
  int getNextStyle();


  void prepareCanvas();
  void drawPad(TVirtualPad *a,std::string PadDrawOpt="");
  void cleanExtHistos(std::vector <ExtHisto>  &cand);
  std::string doCutOut(const std::string &instring,
		       const std::vector<std::string> &cutIt);
  std::string doStringRep(const std::string &instring, 
			  const std::string &cutIt,
			  const std::string &rep);
  
  std::string doStringRep(std::string instring, 
			  const std::map< std::string , std::string > &repmap);

  


};

#endif
