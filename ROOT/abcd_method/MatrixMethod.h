#ifndef MATRIXMETHOD_H
#define MATRIXMETHOD_H

#include "TH2D.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include <iostream>

#include "Canvas_Holder.h"
#include "MergeHistos/MergeHistos.h"
#include "CorrelationChecker.h"
#include "Tools.h"

class MatrixMethod{
	public:
		MatrixMethod();
		~MatrixMethod();

		void set_histo(TH2D *histo);
		/*
		 * regions:
		 * 1 = A
		 * 2 = B
		 * 3 = C
		 * 4 = D
		 */
		void define_region(int region, double min_x, double max_x, double min_y, double max_y);
		void set_signal_region(int region);
		void set_outfile(TFile *outfile);
		void set_signal_file_names(std::map<std::string,std::vector<std::string> > &file_names);
		void set_background_file_names(std::map<std::string,std::vector<std::string> > &file_names);
		void set_cutset(std::string cutset);
		void set_histo_name(std::string histo_name);
		double get_nbackground_events();
		void check_stability(int nintervals);
		void set_integrated_luminosity(double integrated_luminosity);

	private:
		struct estimate{
			double true_value;
			double estimate;
			double error;
		};
		estimate estimate_nbackground_events(bool check_correlations=false);
		void reset_regions();

		static const int NREGIONS=4;

		double x_min[NREGIONS];
		double x_max[NREGIONS];
		double y_min[NREGIONS];
		double y_max[NREGIONS];

		double nominal_x_min[NREGIONS];
		double nominal_x_max[NREGIONS];
		double nominal_y_min[NREGIONS];
		double nominal_y_max[NREGIONS];

		int signal_region;

		std::map<std::string,std::vector<std::string> > signal_file_names;
		std::map<std::string,std::vector<std::string> > background_file_names;

		std::string histo_name;
		TH2F *signal_histo;
		TH2F *background_histo;
		TH2F *combined_histo;
		TFile *outfile;
		MergeHistos *histo_merger;

		Tools *tool;
};

#endif
