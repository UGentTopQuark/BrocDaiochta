#include "CalculateEff.h"

template <class EffObj>
CalculateEff<EffObj>::CalculateEff(TFile *outfile)
{

	this->outfile = outfile;
	sig_mhistos = new MergeHistos();
	sig_mhistos->set_outfile(outfile);
	bg_mhistos = new MergeHistos();
	bg_mhistos->set_outfile(outfile);
	eff = new EffObj(outfile);
	//will crash if not set
	e_mu = "";
	d_channel = "";
	cutset = "";
	version = "";
	mass = "";	
	calc_method = "";
	analysis = "";
	luminosity = -1;
	input_directory = "";
	
	//dont need to be set
	eff_prefix = "";
	cut_to_set = -1;
}

template <class EffObj>
CalculateEff<EffObj>::~CalculateEff()
{
	if(sig_mhistos){ delete sig_mhistos; sig_mhistos = NULL; } 
	if(bg_mhistos){ delete bg_mhistos; bg_mhistos = NULL; }
}

template <typename EffObj>
void  CalculateEff<EffObj>::calculator()
{

	for(std::vector<std::string>::iterator histo_name = histo_names->begin();histo_name !=histo_names->end();++histo_name){


		for(std::vector<std::string>::iterator hsym = symmetric_cuts->begin();
		    hsym != symmetric_cuts->end(); hsym++){
			if(*histo_name == *hsym){
				eff->set_symmetric_cut(true);
			}
		}
		for(std::vector<std::string>::iterator hmax = max_requirements->begin();
		    hmax != max_requirements->end(); hmax++){
			if(*histo_name == *hmax){
				eff->set_max_requirement(true);
			}
		}
		
		eff->set_cut_for_systematics(cut_to_set);
		if(*histo_name == "bjet_bDiscrim_cut_HighEff_")
			eff->set_error_on_eff(11);		
		else if(*histo_name == "lightjet_bDiscrim_cut_HighEff_")
			eff->set_error_on_eff(4.7);
		
		eff->set_histo_rebin(1);
		eff->set_calc_method(calc_method);
		
		std::cout << "WARNING: signal set as |muon| only. |mu_background| ignored " <<std::endl;
		std::cout << "preparing signal: " << *histo_name << std::endl;
		std::map<std::string,std::vector<std::string> > sig_file_names;
		if (mass == "175" ){
			sig_file_names["TTbar"+version+".root"].push_back(*histo_name+"TTbar|"+d_channel+"|"+cutset);
		}
		else if(mass == "175t"){
			sig_file_names["TTbar_wo_trigger"+version+".root"].push_back(*histo_name+"TTbarTrigger|"+d_channel+"|"+cutset);
		}
		else{
			sig_file_names["Tprime_"+mass+version+".root"].push_back(*histo_name+"Tprime"+mass+"|"+d_channel+"|"+cutset);
		}
		sig_mhistos->set_signal_file(sig_file_names.begin()->first);
		if(input_directory != ""){
			sig_mhistos->set_input_file_directory(input_directory);
		}
		sig_mhistos->set_file_names(sig_file_names);
		sig_mhistos->set_integrated_luminosity(luminosity);	
		sig_mhistos->set_analysis(analysis);
		sig_mhistos->set_histo_name(*histo_name);
		if(mass == "175t")
			sig_mhistos->do_not_scale("TTbarTrigger");
		sig_file_names.clear();
		
		std::cout << "preparing background: " << *histo_name << std::endl;
		std::map<std::string,std::vector<std::string> > bg_file_names;
		if(eff_prefix != "un" || (eff_prefix == "non" && *histo_name == "bjet_bDiscrim_cut_HighEff_"))
		{
			if(!background_file_names){
				std::cerr << "Error: CalcEff: need background file names" << std::endl;
				exit(1);
			}
			for(std::vector<std::string>::iterator file_name = background_file_names->begin();
			    file_name != background_file_names->end(); file_name++){
				bg_file_names[*file_name+version+".root"].push_back(eff_prefix+*histo_name+*file_name+"|"+e_mu+"_background|"+cutset);
			}
		}
		else if(eff_prefix == "un" && mass == "175t") //WARNING: not set up to accept more than one file
			bg_file_names["ttbar_wo_trigger"+version+".root"].push_back(eff_prefix+*histo_name+"TTbarTrigger|"+d_channel+"|"+cutset);

		bg_mhistos->set_file_names(bg_file_names);
		if(input_directory != ""){
			bg_mhistos->set_input_file_directory(input_directory);
		}
		bg_mhistos->set_integrated_luminosity(luminosity);	
		bg_mhistos->set_analysis(analysis);
		bg_mhistos->set_histo_name(eff_prefix+*histo_name);
		if(mass == "175t")
			bg_mhistos->do_not_scale("TTbarTrigger");
		if(bg_file_names.find("qcdbcem"+version+".root") != bg_file_names.end())
			bg_mhistos->do_not_scale("QCDbctoe20bto30");			
		bg_file_names.clear();
		
		std::cout << "adding signal *histo" << std::endl;
	
		eff->set_signal_histo(sig_mhistos->combine_histo<TH1F>());
		std::cout << "adding background histo" << std::endl;
		eff->set_background_histo(bg_mhistos->combine_histo<TH1F>());
		std::cout << "calculating purity and efficiency" << std::endl;
		eff->calculate();
	}
}

template <class EffObj>
void CalculateEff<EffObj>::set_e_mu(std::string e_mu,std::string d_channel)
{
	this->e_mu = e_mu;
	this->d_channel = d_channel;
}
template <class EffObj>
void CalculateEff<EffObj>::set_mass(std::string mass )
{
	this->mass = mass;
}
template <class EffObj>
void CalculateEff<EffObj>::set_cutset(std::string cutset)
{
	this->cutset = cutset;

}
template <class EffObj>
void CalculateEff<EffObj>::set_version(std::string version)
{
	this->version = version;

}
template <class EffObj>
void  CalculateEff<EffObj>::set_histo_names(std::vector<std::string> *histo_names)
{
	this->histo_names = histo_names;
}
template <class EffObj>
void  CalculateEff<EffObj>::set_symmetric_cuts(std::vector<std::string> *symmetric_cuts)
{
	this->symmetric_cuts = symmetric_cuts;
}
template <class EffObj>
void  CalculateEff<EffObj>::set_max_requirements(std::vector<std::string> *max_requirements)
{
	this->max_requirements = max_requirements;
}
template <class EffObj>
void  CalculateEff<EffObj>::set_calc_method(std::string method)
{
	calc_method = method;
}

template <class EffObj>
void  CalculateEff<EffObj>::set_eff_prefix(std::string prefix)
{
	eff_prefix = prefix;
}
template <class EffObj>
void  CalculateEff<EffObj>::set_cut_for_systematics(double cut)
{
	cut_to_set = cut;
}

template class CalculateEff<BTagEfficiency>;
template class CalculateEff<TriggerEfficiency>;
template class CalculateEff<EfficiencyXPurity>;
