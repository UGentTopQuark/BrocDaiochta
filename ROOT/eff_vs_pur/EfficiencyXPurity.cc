#include "EfficiencyXPurity.h"

EfficiencyXPurity::EfficiencyXPurity(TFile *outfile)
{
	this->outfile = outfile;
}

EfficiencyXPurity::~EfficiencyXPurity()
{
}

void EfficiencyXPurity::calculate()
{
	if(background_histo == NULL || signal_histo == NULL)
		return;

	// calculate number of expected TTbar events
	double ttbar_cs = cs_prov->get_cross_section("TTbar");
	double nttbar_produced = ttbar_cs * luminosity;
	nttbar_produced *= 0.147780073566693;	// only semi-leptonic channel

	outfile->cd();

	const int nbins = signal_histo->GetNbinsX();
	double eff_y_axis[nbins];
	double eff_y_ERRl[nbins];
	double eff_y_ERRh[nbins];
	double pur_y_axis[nbins];
	double pur_y_ERRl[nbins];
	double pur_y_ERRh[nbins];
	double purxeff_y_axis[nbins];
	double purxeff_y_ERRl[nbins];
	double purxeff_y_ERRh[nbins];

	double x_axis[nbins];
	double x_ERRl[nbins];
	double x_ERRh[nbins];
	
	int max_i=0;
	if(symmetric_cut)
		max_i =  (int) ((signal_histo->GetNbinsX()/2.0)+0.5);
	else
		max_i = signal_histo->GetNbinsX();


	signal_histo->Sumw2();
	background_histo->Sumw2();

	double max_purxeff = -1,max_purxeff_errl = -1,max_purxeff_errh = -1;
	double max_pur = -1,max_pur_errl = -1,max_pur_errh = -1;
	double max_eff = -1,max_eff_errl = -1,max_eff_errh = -1;
	double eff_at_max_purxeff = -1,eff_at_max_purxeff_errl = -1,eff_at_max_purxeff_errh = -1;
	double pur_at_max_purxeff = -1,pur_at_max_purxeff_errl = -1,pur_at_max_purxeff_errh = -1;
	double optimal_cut_purxeff = -1,optimal_cut_pur = -1,optimal_cut_eff = -1;
	//FIXME: Calculate cut for +/- optimal purxeff error
	for(int i = 1; i <= max_i; ++i){

		x_axis[i-1] = signal_histo->GetBinLowEdge(i);
		x_ERRl[i-1] = 0;
		x_ERRh[i-1] = 0;

		double sig_sum=0,sig_err = 0;
		double bg_sum=0,bg_err = 0;
		int max_j = 0;
		if(symmetric_cut)
			max_j = signal_histo->GetNbinsX() - i;
		else
			max_j = signal_histo->GetNbinsX();

		if(calc_method == "diff"){
			sig_sum = signal_histo->GetBinContent(i);
			bg_sum = background_histo->GetBinContent(i);
			sig_err = signal_histo->GetBinError(i);
			bg_err = background_histo->GetBinError(i);
			//		std::cout << " BG ERR " <<bg_err << "  " << bg_sum<< " SIG ERR " <<sig_err<<"  " << sig_sum<< std::endl;
		}
		else if(calc_method == "int"){
			if(max_requirement){
				for(int j=(i-1); j >= 1; --j){
					sig_sum += signal_histo->GetBinContent(j);
					bg_sum += background_histo->GetBinContent(j);
					//sig_err += signal_histo->GetBinError(j);
					//bg_err += background_histo->GetBinError(j);
				}
			}else{			
				for(int j=i; j <= max_j; ++j){
					sig_sum += signal_histo->GetBinContent(j);
					bg_sum += background_histo->GetBinContent(j);
					sig_err += signal_histo->GetBinError(j);
					bg_err += background_histo->GetBinError(j);
				}
			}
			//FIXME: find out how to calculate these errors
			sig_err = sig_err/((max_j+1)-i);
			bg_err = bg_err/((max_j+1)-i);
		}
		else
			std::cout << "ERROR: no method set: " << calc_method << std::endl;

		double unscaled_sig_sum = 0,unscaled_bg_sum = 0;
		if(signal_histo->Integral() != 0){
			eff_y_axis[i-1] = sig_sum/ nttbar_produced;
			//eff_y_axis[i-1] = sig_sum/ signal_histo->Integral();
			
			double histo_entries_after_scaling = signal_histo->Integral();
			double events_before_scaling_and_before_selection = signal_histo->GetEntries() / (histo_entries_after_scaling/nttbar_produced);

			unscaled_sig_sum = events_before_scaling_and_before_selection*(sig_sum/ nttbar_produced);
			//unscaled_sig_sum = signal_histo->GetEntries()*(sig_sum/ signal_histo->Integral());
			if(!external_eff){
				std::cout << "EffXPur: external eff == NULL " << std::endl;
				exit(1);
			}
			std::cout << "Getting Efficiency error from CP" << std::endl;
			//std::vector<double> eff_err_cp = external_eff->calc_CP_eff(signal_histo->GetEntries(),unscaled_sig_sum,((1-0.682)/2));
			std::vector<double> eff_err_cp = external_eff->calc_CP_eff(events_before_scaling_and_before_selection,unscaled_sig_sum,((1-0.682)/2));
			std::cout << "CP eff: " << eff_err_cp[0] << " + " <<eff_err_cp[1] << " - " << eff_err_cp[2]<< std::endl;
			std::cout << "2nd eff: " << eff_y_axis[i-1] << " +/- " <<sqrt((eff_y_axis[i-1]*(1 - eff_y_axis[i-1]))/(signal_histo->GetEntries()))<< std::endl;


			eff_y_ERRl[i-1] = eff_err_cp[1];
			eff_y_ERRh[i-1] = eff_err_cp[2];
			//eff_y_ERR[i-1] =  sqrt((eff_y_axis[i-1]*(1 - eff_y_axis[i-1]))/(signal_histo->GetEntries()));
			
		}else{
			eff_y_axis[i-1] = 0;
			eff_y_ERRl[i-1] =  0;
			eff_y_ERRh[i-1] =  0;
		}

		if(sig_sum != 0 ||  bg_sum != 0){
			pur_y_axis[i-1] = sig_sum / (sig_sum + bg_sum);

			unscaled_bg_sum = background_histo->GetEntries()*(bg_sum/ background_histo->Integral());

			std::vector<double> pur_err_cp = external_eff->calc_CP_eff(unscaled_sig_sum+unscaled_bg_sum,unscaled_sig_sum,((1-0.682)/2));
			std::cout << "CP pur: " <<pur_y_axis[i-1]  << " + " <<pur_err_cp[1] << " - " << pur_err_cp[2]<< std::endl;

			double sig_err_sig_sum = sqrt(unscaled_sig_sum)/unscaled_sig_sum;
			double bg_err_bg_sum = sqrt(unscaled_bg_sum)/unscaled_bg_sum;
			double sig_err2 = sig_err_sig_sum*sig_sum;
			double bg_err2 = bg_err_bg_sum*bg_sum;

			pur_y_ERRl[i-1] = pur_err_cp[1];
			pur_y_ERRh[i-1] = pur_err_cp[2];
		}else{
			pur_y_axis[i-1] = 0;
			pur_y_ERRl[i-1] = 0;
			pur_y_ERRh[i-1] = 0;

		}
		purxeff_y_axis[i-1] = pur_y_axis[i-1]*eff_y_axis[i-1];
		if(eff_y_ERRl[i-1] == 0 || pur_y_ERRl[i-1] == 0){
			purxeff_y_ERRl[i-1] = 0;
		}else{
			purxeff_y_ERRl[i-1] = purxeff_y_axis[i-1]*sqrt((eff_y_ERRl[i-1]/eff_y_axis[i-1])*(eff_y_ERRl[i-1]/eff_y_axis[i-1])+(pur_y_ERRl[i-1]/pur_y_axis[i-1])*(pur_y_ERRl[i-1]/pur_y_axis[i-1]));
		}
		if(eff_y_ERRh[i-1] == 0 || pur_y_ERRh[i-1] == 0){
			purxeff_y_ERRh[i-1] = 0;
		}else{
			purxeff_y_ERRh[i-1] = purxeff_y_axis[i-1]*sqrt((eff_y_ERRh[i-1]/eff_y_axis[i-1])*(eff_y_ERRh[i-1]/eff_y_axis[i-1])+(pur_y_ERRh[i-1]/pur_y_axis[i-1])*(pur_y_ERRh[i-1]/pur_y_axis[i-1]));
		}

		std::cout << "step: " << i << " value: " << x_axis[i-1] << " eff: " << eff_y_axis[i-1] << " pur: " << pur_y_axis[i-1] << " pxe: " << purxeff_y_axis[i-1] << std::endl;
		if(i == 1){
			std::cout << " TTbar produced: " << nttbar_produced <<  std::endl;
			std::cout << " Sig Int: " <<signal_histo->Integral() <<  std::endl;
			std::cout << " Bkg Int: " <<background_histo->Integral() <<  std::endl;
		}

		if(pur_y_axis[i-1] > max_pur){
		   max_pur = pur_y_axis[i-1];
		   max_pur_errl = pur_y_ERRl[i-1];
		   max_pur_errh = pur_y_ERRh[i-1];
		   optimal_cut_pur = signal_histo->GetBinCenter(i);
		}
		if(eff_y_axis[i-1] > max_eff){
		   max_eff = eff_y_axis[i-1];
		   max_eff_errl = eff_y_ERRl[i-1];
		   max_eff_errh = eff_y_ERRh[i-1];
		   optimal_cut_eff = signal_histo->GetBinCenter(i);
		}
		if(eff_y_axis[i-1]*pur_y_axis[i-1] > max_purxeff){
			eff_at_max_purxeff = eff_y_axis[i-1];
			eff_at_max_purxeff_errl = eff_y_ERRl[i-1];
			eff_at_max_purxeff_errh = eff_y_ERRh[i-1];

			pur_at_max_purxeff = pur_y_axis[i-1];
			pur_at_max_purxeff_errl = pur_y_ERRl[i-1];
			pur_at_max_purxeff_errh = pur_y_ERRh[i-1];

			max_purxeff = purxeff_y_axis[i-1];
			max_purxeff_errl = purxeff_y_ERRl[i-1];
			max_purxeff_errh = purxeff_y_ERRh[i-1];
			// max_purxeff = (pur_y_axis[i-1]*eff_y_axis[i-1])/(1-pur_y_axis[i-1]);
			optimal_cut_purxeff = signal_histo->GetBinCenter(i);
		}

	

	}

	std::cout <<"********Optimal Purity: " <<  max_pur << " + " << max_pur_errh << " - " << max_pur_errl << " Cut: " << optimal_cut_pur << std::endl;
	std::cout <<"********Optimal Efficiency: " <<  max_eff << " + " << max_eff_errh << " - " << max_eff_errl <<  " Cut: " <<  optimal_cut_eff << std::endl;
	std::cout <<"********Optimal Eff*Pur: " <<  max_purxeff << " + " << max_purxeff_errh << " - " << max_purxeff_errl <<  " Cut: " <<  optimal_cut_purxeff << std::endl;
	std::cout <<"********Optimal Eff*Pur, Eff: " << eff_at_max_purxeff << " + " << eff_at_max_purxeff_errh << " - " << eff_at_max_purxeff_errl  << " Pur: " <<  pur_at_max_purxeff << " + " << pur_at_max_purxeff_errh << " - " << pur_at_max_purxeff_errl  << std::endl;


	TGraphAsymmErrors eff_vs_pur(nbins,x_axis,purxeff_y_axis,x_ERRl,x_ERRh,purxeff_y_ERRl,purxeff_y_ERRh);
	TGraphAsymmErrors eff(nbins,x_axis,eff_y_axis,x_ERRl,x_ERRh,eff_y_ERRl,eff_y_ERRh);
	TGraphAsymmErrors pur(nbins,x_axis,pur_y_axis,x_ERRl,x_ERRh,pur_y_ERRl,pur_y_ERRh);


	std::string drawopt = "pz";

	int marker_style = 7;
	int marker_size = 1;

	std::vector<int> *colours = new std::vector<int>();
	colours->push_back(kBlue);
	colours->push_back(kRed);
	colours->push_back(kGreen);
	std::vector<int> *mark_styles = new std::vector<int>();
	mark_styles->push_back(1);
	std::string ep_title = "All_" + (std::string)signal_histo->GetName();
	cholders[ep_title] = new CanvasHolder();
 	cholders[ep_title]->setCanvasTitle(ep_title);
 	//cholders[ep_title]->setBordersY(0.04,1.3);
 	cholders[ep_title]->setBordersX(0,0.5);
 	cholders[ep_title]->setTitleX(signal_histo->GetXaxis()->GetTitle());
 	cholders[ep_title]->setLegendOptions(0.25,0.15,"UpperRight");
	cholders[ep_title]->addGraph(&eff_vs_pur, "#epsilon * #pi","#epsilon * #pi",drawopt);
	cholders[ep_title]->addGraph(&eff, "efficiency #epsilon", "efficiency #epsilon",drawopt);
	cholders[ep_title]->addGraph(&pur, "purity #pi","purity #pi",drawopt);
	cholders[ep_title]->setMarkerSizeGraph(marker_size);
	cholders[ep_title]->setMarkerStylesGraph(marker_style);
	cholders[ep_title]->setLineSizeGraph(marker_size);
	//cholders[ep_title]->setBordersY(0,1.2);
	//cholders[ep_title]->setBordersY(0,1.2);
	cholders[ep_title]->addVLine(optimal_cut_purxeff, "max. #epsilon #times #pi ");
	cholders[ep_title]->setLineColors(*colours);
	cholders[ep_title]->setOptStat(000000);
	cholders[ep_title]->write(outfile);
	cholders[ep_title]->save("png");

	ep_title = "EffvsPur_" + (std::string)signal_histo->GetName();
	cholders[ep_title] = new CanvasHolder();
 	cholders[ep_title]->setCanvasTitle(ep_title);
	cholders[ep_title]->addGraph(&eff_vs_pur, "#epsilon * #pi","#epsilon * #pi",drawopt);
	cholders[ep_title]->setMarkerSizeGraph(marker_size);
	cholders[ep_title]->setMarkerStylesGraph(marker_style);
	cholders[ep_title]->setLineSizeGraph(marker_size);
 	cholders[ep_title]->setBordersY(0.08,0.13);
 	cholders[ep_title]->setBordersX(0,0.5);
 	cholders[ep_title]->setTitleX(signal_histo->GetXaxis()->GetTitle());
 	cholders[ep_title]->setLegendOptions(0.25,0.15,"UpperRight");
	cholders[ep_title]->addVLine(optimal_cut_purxeff, "max. #epsilon #times #pi ");
	cholders[ep_title]->addHLine(max_purxeff, "max. #epsilon #times #pi ");
	cholders[ep_title]->setLineColors(kBlue);
	cholders[ep_title]->setOptStat(000000);
	cholders[ep_title]->write(outfile);
	cholders[ep_title]->save("png");

	ep_title = "Pur_" + (std::string)signal_histo->GetName();
	cholders[ep_title] = new CanvasHolder();
 	cholders[ep_title]->setCanvasTitle(ep_title);
 	cholders[ep_title]->setTitleX(signal_histo->GetXaxis()->GetTitle());
 	cholders[ep_title]->setLegendOptions(0.25,0.15,"UpperRight");
	cholders[ep_title]->addGraph(&pur, "purity #pi","purity #pi",drawopt);
	cholders[ep_title]->setMarkerSizeGraph(marker_size);
	cholders[ep_title]->setMarkerStylesGraph(marker_style);
	cholders[ep_title]->setLineSizeGraph(marker_size);
	cholders[ep_title]->addVLine(optimal_cut_purxeff, "max. #epsilon #times #pi ");
	cholders[ep_title]->setOptStat(000000);
	cholders[ep_title]->write(outfile);
	cholders[ep_title]->save("png");

	ep_title = "Eff_" + (std::string)signal_histo->GetName();
	cholders[ep_title] = new CanvasHolder();
 	cholders[ep_title]->setCanvasTitle(ep_title);
 	cholders[ep_title]->setTitleX(signal_histo->GetXaxis()->GetTitle());
 	cholders[ep_title]->setLegendOptions(0.25,0.15,"UpperRight");
	cholders[ep_title]->addGraph(&eff, "efficiency #epsilon", "efficiency #epsilon",drawopt);
	cholders[ep_title]->setMarkerSizeGraph(marker_size);
	cholders[ep_title]->setMarkerStylesGraph(marker_style);
	cholders[ep_title]->setLineSizeGraph(marker_size);
	cholders[ep_title]->addVLine(optimal_cut_purxeff, "max. #epsilon #times #pi ");
	cholders[ep_title]->setOptStat(000000);
	cholders[ep_title]->write(outfile);
	cholders[ep_title]->save("png");

	delete colours;


	       
}

