#ifndef CALCULATEEFF_H
#define CALCULATEEFF_H
#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "HistoMerger/MergeHistos.h"
#include "EfficiencyPurity.h"
#include "BTagEfficiency.h"
#include "TriggerEfficiency.h"
#include "EfficiencyXPurity.h"

template <class EffObj>
class CalculateEff{
 public:
	CalculateEff(TFile *outfile);
	~CalculateEff();
	void calculator();
	void set_e_mu(std::string e_mu,std::string d_channel);
	void set_mass(std::string mass);
	void set_cutset(std::string cutset);
	void set_version(std::string version);
	inline void set_luminosity(double lumi){luminosity = lumi; eff->set_luminosity(lumi); }
	inline void set_analysis(std::string analysis){ this->analysis = analysis; eff->set_cross_section_set(analysis); }
	inline void set_input_directory(std::string input_directory){this->input_directory = input_directory;} //< Directory containing input root files

	void set_histo_names(std::vector<std::string> *histo_names);
	inline void set_background_file_names(std::vector<std::string> *file_names){background_file_names = file_names;}
	void set_calc_method(std::string method);
	void set_eff_prefix(std::string prefix);
	void set_cut_for_systematics(double cut);
	void set_symmetric_cuts(std::vector<std::string> *sym_cuts);
        void set_max_requirements(std::vector<std::string> *max_cuts);


 private:
	EffObj *eff;
	MergeHistos *sig_mhistos;
	MergeHistos *bg_mhistos;
	
	TFile *outfile;
	std::string e_mu;
	std::string d_channel;
	std::string mass;
	std::string cutset;
	std::string version;
	std::string analysis;
	std::string input_directory;
	double luminosity;

	std::vector<std::string> *histo_names;
	std::vector<std::string> *background_file_names;
	std::vector<std::string> *symmetric_cuts;
	std::vector<std::string> *max_requirements;
	std::string calc_method;
	std::string eff_prefix;
	double cut_to_set;
};
#endif
