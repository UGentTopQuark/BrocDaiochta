#ifndef EFFICIENCYPURITY_H
#define EFFICIENCYPURITY_H
#include "TH1F.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "math.h"
#include <iostream>
#include "CanvasHolder/Canvas_Holder.h"
#include "Efficiency/Efficiency.h"
#include "HistoMerger/CrossSectionProvider/CrossSectionProvider.h"

class EfficiencyPurity{
	public:
		EfficiencyPurity();
		~EfficiencyPurity();
		void set_symmetric_cut(bool symmetric);
		inline void set_max_requirement(bool max){max_requirement = max;}
		void set_histo_rebin(double rebin);
		void set_calc_method(std::string method);
		void set_signal_histo(TH1F *signal);
		void set_background_histo(TH1F *background);
		//only used in btag but gets called for all of them
		void set_cut_for_systematics(double cut = -1);
		void set_error_on_eff(double error = -1);
		inline void set_luminosity(double lumi = -1){ this->luminosity = lumi; };
		inline void set_cross_section_set(std::string cross_section_set){ this->cross_section_set = cross_section_set; cs_prov->set_analysis(cross_section_set);};
		virtual void calculate() = 0;

	protected:
		TH1F *background_histo;
		TH1F *signal_histo;
		bool symmetric_cut;
		bool max_requirement;
		double rebin_histo;
		std::string calc_method;

		std::string cross_section_set;
		double luminosity;
		CrossSectionProvider *cs_prov;

		double error_on_eff;
		double cut_for_systematics;
                std::map<std::string,CanvasHolder*> cholders;
		
		Efficiency *external_eff;

};

#endif
