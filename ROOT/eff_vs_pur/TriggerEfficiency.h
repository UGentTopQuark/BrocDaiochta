#ifndef TRIGGEREFFICIENCY_H
#define TRIGGEREFFICIENCY_H
#include "TH1F.h"
#include "TFile.h"
#include "math.h"
#include <iostream>
#include "EfficiencyPurity.h"

class TriggerEfficiency:public EfficiencyPurity{
	public:
		TriggerEfficiency(TFile *outfile);
		~TriggerEfficiency();
		virtual void calculate();
	private:
		TFile *outfile;

};

#endif
