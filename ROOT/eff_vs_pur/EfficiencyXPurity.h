#ifndef EFFICIENCYXPURITY_H
#define EFFICIENCYXPURITY_H
#include "TH1F.h"
#include "TFile.h"
#include "math.h"
#include <iostream>
#include "EfficiencyPurity.h"

class EfficiencyXPurity:public EfficiencyPurity{
	public:
		EfficiencyXPurity(TFile *outfile);
		~EfficiencyXPurity();
		virtual void calculate();
	private:
		TFile *outfile;
};

#endif
