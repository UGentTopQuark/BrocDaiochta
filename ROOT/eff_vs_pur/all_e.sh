for i in `seq 1 9`; do
	echo "cutset 0${i}"
	./eff_vs_pur e 0${i}_cutset 2>&1 | grep "Optimal Eff\*Pur:" 
done

for i in `seq 10 24`; do
	echo "cutset ${i}"
	./eff_vs_pur e ${i}_cutset 2>&1 | grep "Optimal Eff\*Pur:"
done
