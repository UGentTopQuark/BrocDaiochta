#include "EfficiencyPurity.h"

EfficiencyPurity::EfficiencyPurity()
{
	signal_histo = NULL;
	background_histo = NULL;
	rebin_histo = -1;
	calc_method = "none";
	error_on_eff = -1;
	cut_for_systematics = -1;
	symmetric_cut = false;
	max_requirement = false;
	external_eff = new Efficiency();

	cs_prov = new CrossSectionProvider();
}

EfficiencyPurity::~EfficiencyPurity()
{
	if(external_eff){
		delete external_eff;
		external_eff = NULL;
	}

	if(cs_prov){ delete cs_prov; cs_prov = NULL; }
}

void EfficiencyPurity::set_signal_histo(TH1F *signal)
{
	if(rebin_histo != -1){
		const char * sname = signal->GetName();
		signal_histo = (TH1F*)signal->Rebin(rebin_histo,sname);}
	else
		signal_histo = signal;
}

void EfficiencyPurity::set_background_histo(TH1F *background)
{
	if(rebin_histo != -1){
		const char * bname = background->GetName();
		background_histo = (TH1F*)background->Rebin(rebin_histo,bname);}
	else
		background_histo = background;
}

void EfficiencyPurity::set_symmetric_cut(bool symmetric)
{
	symmetric_cut = symmetric;
}

void EfficiencyPurity::set_histo_rebin(double rebin)
{
	rebin_histo = rebin;
}

void EfficiencyPurity::set_calc_method(std::string method)
{
	calc_method = method;
}

void EfficiencyPurity::set_error_on_eff(double err)
{
	error_on_eff = err;
}
void EfficiencyPurity::set_cut_for_systematics(double cut)
{
	cut_for_systematics = cut;
}


