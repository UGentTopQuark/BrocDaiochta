#include "BTagEfficiency.h"

BTagEfficiency::BTagEfficiency(TFile *outfile)
{
	this->outfile = outfile;
	eff_plus_cut = -1;
	eff_less_cut = -1;
	
}

BTagEfficiency::~BTagEfficiency()
{
}

void BTagEfficiency::calculate()
{
	if(background_histo == NULL || signal_histo == NULL)
		return;
	outfile->cd();

	//std::string hname = (std::string) "EfficiencyXPurity_"+signal_histo->GetName();
	std::string hname = (std::string) "";
	TH1F eff_vs_pur(hname.c_str(),hname.c_str(), signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	hname = (std::string) "Efficiency_"+signal_histo->GetName();
	hname = (std::string) "";
	TH1F eff(hname.c_str(),hname.c_str(), signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	hname = (std::string) "Purity_"+signal_histo->GetName();
	TH1F pur(hname.c_str(),hname.c_str(), signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	eff_vs_pur.SetXTitle(signal_histo->GetXaxis()->GetTitle());
	eff.SetYTitle("Efficiency");
	eff.SetXTitle("bTag Discriminator Value");


	int max_i=0;
	if(symmetric_cut)
		max_i =  (int) ((signal_histo->GetNbinsX()/2.0)+0.5);
	else
		max_i = signal_histo->GetNbinsX();

	//Already calculated in mergehistos
	//	signal_histo->Sumw2();
	//	background_histo->Sumw2();

	double max_purxeff = -1,max_pur = -1,max_eff = -1;
	double optimal_cut_purxeff = -1,optimal_cut_pur = -1,optimal_cut_eff = -1;
	for(int i = 1; i <= max_i; ++i){
		double sig_sum=0,sig_err = 0,eff_err = 0,pur_err = 0;
		double bg_sum=0,bg_err = 0;
		int max_j = 0;
		if(symmetric_cut)
			max_j = signal_histo->GetNbinsX() - i;
		else
			max_j = signal_histo->GetNbinsX();
		
		if(calc_method == "diff"){
			std::cout << "WARNING: diff method doesn't make sense for btag eff" <<std::endl;
			sig_sum = signal_histo->GetBinContent(i);
			bg_sum = background_histo->GetBinContent(i);
			//should be binomial. see trigger eff
			sig_err = signal_histo->GetBinError(i);
			bg_err = background_histo->GetBinError(i);
		}
		else if(calc_method == "int"){
			for(int j=i; j <= max_j; ++j){
				sig_sum += signal_histo->GetBinContent(j);
				bg_sum += background_histo->GetBinContent(j);
				sig_err += signal_histo->GetBinError(j);
				bg_err += background_histo->GetBinError(j);
			}
			//FIXME: find out how to calculate these errors
			sig_err = sig_err/((max_j+1)-i);
			bg_err = bg_err/((max_j+1)-i);
		}
		else
			std::cout << "ERROR: no method set: " << calc_method << std::endl;

		double efficiency = 0;
		//Integral should be number of events before scaling
		if(signal_histo->Integral() != 0){
			efficiency = sig_sum/ signal_histo->Integral();
			eff_err =  sqrt((efficiency*(1 - efficiency))/(signal_histo->GetEntries()));
		}
		
		double purity = 0;
		if((sig_sum + bg_sum) != 0){
			purity = sig_sum /(sig_sum +bg_sum);
			pur_err = purity*sqrt((sig_err/sig_sum)*(sig_err/sig_sum) + ((sqrt((sig_err*sig_err)+(bg_err*bg_err)))/(sig_sum+bg_sum))*((sqrt((sig_err*sig_err)+(bg_err*bg_err)))/(sig_sum+bg_sum)));
			
		}
		
		//std::cout << "step: " << i << " value: " << signal_histo->GetBinCenter(i) << " eff: " << efficiency << " pur: " << purity << " exp: " << efficiency*purity << std::endl;

		/****************For TGraphErrors: btag_eff*
 		std::cout << "x_axis[" << i-1 <<"] = " << signal_histo->GetBinLowEdge(i)<<";"<<  std::endl;
 		std::cout << "x_ERR[" << i-1 << "] = 0" <<";"<<  std::endl;
 		std::cout << "y_axis[" << i-1 << "] = " << efficiency<< ";"<< std::endl;
 		std::cout << "y_ERR[" << i-1 << "] = " << eff_err<< ";"<< std::endl;
		*********************************************/
		/****************For TGraphErrors: b_vs_non_eff*
		std::cout << "WARNING: check if the .find command is working.Always pass != npos even if substring not present" << std::endl;
	 	std::string sig_hist_name = signal_histo->GetName();
		int foundnonb = sig_hist_name.find("nonbjet_bDiscrim_cut_");
		int foundb = sig_hist_name.find("bjet_bDiscrim_cut_");
 		int pos_mu = sig_hist_name.find("|mu");
 		int pos_e = sig_hist_name.find("|e");
 		int pos_cut = sig_hist_name.find("cut_");
		int range_algo_name = (pos_mu-6)-(pos_cut+4);
		if(pos_e != int(std::string::npos))
			range_algo_name = (pos_e-6)-(pos_cut+4);
 		std::string name_b_algo = sig_hist_name.substr((pos_cut+4),range_algo_name);
 		if(foundb != int(std::string::npos) && foundnonb == int(std::string::npos))
 			{
 				std::cout << "x_axis_map[\""<<name_b_algo <<"\"][" << i-1 <<"] = "  << efficiency<<";"<<  std::endl;
 				std::cout << "x_ERR_map[\""<<name_b_algo <<"\"][" << i-1 << "] = " <<eff_err<<";"<<  std::endl;
 			}
 		else if(foundnonb != int(std::string::npos))
 			{
 				std::cout << "y_axis_map[\""<<name_b_algo <<"\"][" << i-1 << "] = "<< efficiency<< ";"<< std::endl;
 				std::cout << "y_ERR_map[\""<<name_b_algo <<"\"][" << i-1 << "] = " << eff_err<<";"<< std::endl;
 			}
		**********************************************/

		eff_vs_pur.SetBinContent(i,efficiency*purity);
		eff_vs_pur.SetBinError(i,0);
		eff.SetBinContent(i,efficiency);
		eff.SetBinError(i,eff_err);
		pur.SetBinContent(i,purity);
		pur.SetBinError(i,pur_err);


		if(purity > max_pur){
		  // max_purxeff = (purity*efficiency)/(1-purity);
		   max_pur = purity;
		   optimal_cut_pur = signal_histo->GetBinCenter(i);
		}
		if(efficiency > max_eff){
		   max_eff = efficiency;
		   optimal_cut_eff = signal_histo->GetBinCenter(i);
		}
		if(efficiency*purity > max_purxeff){
		   max_purxeff = efficiency*purity;
		   optimal_cut_purxeff = signal_histo->GetBinCenter(i);
		}


		//Calculate cuts for systematic error
		eff_x_y_coords[i] = efficiency;
		if (cut_for_systematics != -1 && error_on_eff != -1 && i == max_i ){
			calculate_eff_sys_error();		
		}

	}

	std::cout <<"****************Optimal Purity Cut:" << optimal_cut_pur << std::endl;
	std::cout <<"****************Optimal Efficiency Cut:" << optimal_cut_eff << std::endl;
	std::cout <<"****************Optimal Cut:" << optimal_cut_purxeff << std::endl;
	CanvasHolder cholder;
	cholder.addHisto(&eff, "#epsilon", "*L");
	std::string htitle = ("Eff:"+(std::string)signal_histo->GetTitle());
 	cholder.setCanvasTitle(htitle);
 	cholder.setBordersY(0,1.2);
// 	cholder.addHisto(&pur, "#pi", "L");
// 	htitle = ("Pur:"+(std::string)signal_histo->GetTitle());
// 	cholder.setCanvasTitle(htitle);
// 	cholder.setBordersY(0,1.2);
// 	cholder.addHisto(&eff_vs_pur, "#epsilon * #pi", "L");
// 	htitle = ("EffvsPur:"+(std::string)signal_histo->GetTitle());
// 	cholder.setCanvasTitle(htitle);
// 	cholder.setBordersY(0,1.2);
// 	cholder.addVLine(optimal_cut_purxeff, "max.#epsilon * #pi ");


	if(calc_method == "int"){
		if(cut_for_systematics != -1){
			cholder.addVLine(eff_plus_cut,"+11% of #epsilon at five",1,4);
			cholder.addVLine(cut_for_systematics, "five",1,2);
			cholder.addVLine(eff_less_cut,"-11% of #epsilon at five",1,4);
			//For TGraphErrors:btag_eff
			std::cout<<"double eff_plus_cut = " <<eff_plus_cut << ";"<< std::endl; 
			std::cout<<"double cut_for_systematics = " << cut_for_systematics<<";"<<  std::endl;
      			std::cout<<"double eff_less_cut = " << eff_less_cut <<";"<< std::endl; 

			//cholder.addLine(draw_line[1].first,draw_line[2].first,draw_line[1].second,draw_line[2].second);
			//cholder.addLine(draw_line[3].first,draw_line[4].first,draw_line[3].second,draw_line[4].second);
			//cholder.addLine(draw_line[5].first,draw_line[6].first,draw_line[5].second,draw_line[6].second);
		}
	       
	}
	cholder.setOptStat(000000);
	cholder.write(outfile);
}

//Given percent error and knowing the cut you want to apply calculate cut which would give expected error
void BTagEfficiency::calculate_eff_sys_error()
{
	int max_i = signal_histo->GetNbinsX();
	double x1,x2,y1,y2,slope1,yintercept1,eff_less_err = -1,eff_plus_err = -1,eff_at_cut = -1;

	//find bin within which cut is. Use x, y coords to left and right to find eqn for line. Use line to get eff at cut.
	for(int i=0;i < max_i;i++){
		if(signal_histo->GetBinLowEdge(i) <= cut_for_systematics && signal_histo->GetBinLowEdge(i+1) > cut_for_systematics){
			x1 = signal_histo->GetBinLowEdge(i); y1 = eff_x_y_coords[i];
			x2 = signal_histo->GetBinLowEdge(i+1); y2 = eff_x_y_coords[i+1];
			slope1 = (y2-y1)/(x2-x1);
			yintercept1 = y1 - slope1*x1;

			eff_at_cut = cut_for_systematics*slope1 +yintercept1;
			draw_line[3] =  std::pair<double,double>(x1,y1);
			draw_line[4] =  std::pair<double,double>(x2,y2);
	
			double abs_error_on_eff = (eff_at_cut/100)*error_on_eff;
			eff_plus_err = eff_at_cut + abs_error_on_eff;
			eff_less_err = eff_at_cut - abs_error_on_eff;
			break;
		}
	       
	}

	//Same as above except now use line to find cuts from eff´s
	for(int i=0;i< max_i;i++){
		if(eff_x_y_coords[i] >= eff_plus_err && eff_x_y_coords[i+1] < eff_plus_err){

			x1 = signal_histo->GetBinLowEdge(i); y1 = eff_x_y_coords[i];
			x2 = signal_histo->GetBinLowEdge(i+1); y2 = eff_x_y_coords[i+1];
			slope1 = (y2-y1)/(x2-x1);
			yintercept1 = y1 - slope1*x1;

			eff_plus_cut = (eff_plus_err - yintercept1)/slope1;
			draw_line[1] = std::pair<double,double>(x1,y1);
			draw_line[2] = std::pair<double,double>(x2,y2); 
		}

		if(eff_x_y_coords[i] >= eff_less_err && eff_x_y_coords[i+1] < eff_less_err){

			x1 = signal_histo->GetBinLowEdge(i); y1 = eff_x_y_coords[i];
			x2 = signal_histo->GetBinLowEdge(i+1); y2 = eff_x_y_coords[i+1];
			slope1 = (y2-y1)/(x2-x1);
			yintercept1 = y1 - slope1*x1;

			eff_less_cut = (eff_less_err - yintercept1)/slope1;
			draw_line[5] = std::pair<double,double>(x1,y1);
			draw_line[6] = std::pair<double,double>(x2,y2); 
			break;
		}

	}
	
	double eff_less_err_err =  sqrt((eff_less_err*(1 -eff_less_err))/(signal_histo->GetEntries()));
	double eff_at_cut_err =  sqrt((eff_at_cut*(1 -eff_at_cut))/(signal_histo->GetEntries()));
	double eff_plus_err_err =  sqrt((eff_plus_err*(1 -eff_plus_err))/(signal_histo->GetEntries()));

	std::cout <<"For efficiency: "<< eff_less_err <<" pm "<<eff_less_err_err <<  " cut: " << eff_less_cut <<std::endl; 
	std::cout <<"For efficiency: "<< eff_at_cut << " pm "<<eff_at_cut_err << " cut: " << cut_for_systematics <<std::endl; 
	std::cout <<"For efficiency: "<< eff_plus_err << " pm "<<eff_plus_err_err << " cut: " << eff_plus_cut <<std::endl; 
	
}
