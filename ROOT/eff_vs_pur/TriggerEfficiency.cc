#include "TriggerEfficiency.h"

TriggerEfficiency::TriggerEfficiency(TFile *outfile)
{
	this->outfile = outfile;
	
}

TriggerEfficiency::~TriggerEfficiency()
{
}

void TriggerEfficiency::calculate()
{
	if(background_histo == NULL || signal_histo == NULL)
		return;
	outfile->cd();

	//std::string hname = (std::string) "Efficiency_"+signal_histo->GetName();
	std::string hname = (std::string) "";
	TH1F eff(hname.c_str(),hname.c_str(),signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	eff.SetYTitle("Trigger Efficiency");
	//set x-axis title
	std::string sig_hist_name = signal_histo->GetName();
	int foundpt = sig_hist_name.find("_pt_");
	int foundmu = sig_hist_name.find("mu");
	if(foundmu != int(std::string::npos)){
		if(foundpt != int(std::string::npos))
			eff.SetXTitle("p_{T}^{#mu} [GeV/c]");
		else
			eff.SetXTitle("#eta^{#mu}");
	}
	else{
		if(foundpt != int(std::string::npos))
			eff.SetXTitle("p_{T}^{e} [GeV/c]");
		else
			eff.SetXTitle("#eta^{e}");
	}




	int max_i=0;
	if(symmetric_cut)
		max_i =  (int) ((signal_histo->GetNbinsX()/2.0)+0.5);
	else
		max_i = signal_histo->GetNbinsX();

	//done in mergehisto
	//signal_histo->Sumw2();
	//background_histo->Sumw2();

	double max_eff = -1;
	double optimal_cut_eff = -1;
	for(int i = 1; i <= max_i; ++i){
		double sig_sum=0,eff_err = 0;
		double bg_sum=0;
		int max_j = 0;
		if(symmetric_cut)
			{
				std::cout << " Symmetric cut " << std::endl;
				max_j = signal_histo->GetNbinsX() - i;
			}
		else
			max_j = signal_histo->GetNbinsX();
		
		if(calc_method == "diff"){
			sig_sum = signal_histo->GetBinContent(i);
			bg_sum = background_histo->GetBinContent(i);
		}
		else if(calc_method == "int"){
			std::cout << "WARNING: Int method doesnt make sense for trigger eff" << std::endl;
			for(int j=i; j <= max_j; ++j){
				sig_sum += signal_histo->GetBinContent(j);
				bg_sum += background_histo->GetBinContent(j);
			}
		}
		else
			std::cout << "ERROR: no method set: " << calc_method << std::endl;

		//Trigger efficiency = sig/bkg. sig =trig. bkg = untrig (which really = trig+untrig)
		double efficiency = 0;
		if(bg_sum != 0 && calc_method == "diff"){
			efficiency = sig_sum/bg_sum;
			//std::cout<<"sig sum "<< sig_sum << " bg sum " << bg_sum <<std::endl;
			if(efficiency <= 1)
				eff_err =  sqrt((efficiency*(1 - efficiency))/bg_sum);
		}
		
		//	std::cout << "step: " << i << " value: " << signal_histo->GetBinCenter(i) << " eff: " << efficiency <<" +/- " << eff_err <<  std::endl;

		eff.SetBinContent(i,efficiency);
		eff.SetBinError(i,eff_err);
	
		if(efficiency > max_eff){
		   max_eff = efficiency;
		   optimal_cut_eff = signal_histo->GetBinCenter(i);
		}
		/***********for trigger_eff.cc when overlaying plots ********/
	 	std::string sig_hist_name = signal_histo->GetName();
 		int pos_cut = sig_hist_name.find("on|");
		int range_algo_name = (pos_cut+3);
 		std::string name_b_algo = sig_hist_name.substr((pos_cut+3),range_algo_name);
		std::cout << "y_axis_map[\""<<name_b_algo <<"\"][" << i-1 <<"] = "  << efficiency<<";"<<  std::endl;
		std::cout << "y_ERR_map[\""<<name_b_algo <<"\"][" << i-1 << "] = " <<eff_err<<";"<<  std::endl;

		std::cout << "x_axis_map[\""<<name_b_algo <<"\"][" << i-1 <<"] = "  << signal_histo->GetBinCenter(i) <<";"<<  std::endl;
		std::cout << "x_ERR_map[\""<<name_b_algo <<"\"][" << i-1 << "] = 0;"<<  std::endl;

		/*********************************************************/
	}

	std::cout <<"****************Optimal Efficiency Cut:" << optimal_cut_eff << std::endl;
	CanvasHolder cholder;
	cholder.setLegDraw(false);
	cholder.addHisto(&eff, "#epsilon", "*L");
	std::string htitle = ("Eff_"+(std::string)signal_histo->GetName());
 	cholder.setCanvasTitle(htitle);
 	cholder.setBordersY(0,1.2);
	//cholder.addVLine(optimal_cut_eff, "max.#epsilon * #pi ");
 
//	cholder.setLogY();
	cholder.setOptStat(000000);
	cholder.write(outfile);
}
