
#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "CalculateEff.h"
#include "BTagEfficiency.h"
#include "TriggerEfficiency.h"
#include "EfficiencyXPurity.h"
#include "Efficiency/Efficiency.h"

int main(int argc, char **argv)
{
	//set to trigger,btag or dont set for effxpur
	std::string analysis = "";
	std::string version = "_plots";

	TFile *outfile = new TFile("outfile.root", "RECREATE");

	std::string e_mu =argv[1];
	std::string mass = "175"; //175t for trigger studies
	std::string cutset = argv[2];
	std::string d_channel ="electron"; 
	std::string analysis_cross_section = "Summer11_pfJets_emuisoveryloose";
	double luminosity = 1000.0;
	std::string input_files_directory = ""; 
	std::vector<std::string> *histo_names = new std::vector<std::string>();

	if(e_mu != "e")
		d_channel = "muon";

	std::vector<std::string> *bkg_file_names = new std::vector<std::string>();
 	bkg_file_names->push_back("TTbar");
 	bkg_file_names->push_back("Wjets");
 	bkg_file_names->push_back("Zjets");
 	bkg_file_names->push_back("TtChan");
 	bkg_file_names->push_back("TsChan");
 	bkg_file_names->push_back("TtWChan");
	if(e_mu == "mu")
		bkg_file_names->push_back("MuQCD");
	else if(e_mu == "e"){
		bkg_file_names->push_back("QCDbctoe20to30");
		bkg_file_names->push_back("QCDbctoe30to80");
		bkg_file_names->push_back("QCDbctoe80to170");
		bkg_file_names->push_back("QCDem20to30");
		bkg_file_names->push_back("QCDem30to80");
		bkg_file_names->push_back("QCDem80to170");
	}


	if(analysis == "btag")
		{
			CalculateEff<BTagEfficiency> *calc_eff = new CalculateEff<BTagEfficiency>(outfile);
			calc_eff->set_mass(mass);
			calc_eff->set_cutset(cutset);
			calc_eff->set_e_mu(e_mu,d_channel);
			calc_eff->set_version(version);
			calc_eff->set_input_directory(input_files_directory);
			calc_eff->set_analysis(analysis_cross_section);
			calc_eff->set_luminosity(luminosity);
			calc_eff->set_background_file_names(bkg_file_names);

			//for btagging eff the background will start with ´´.For pur set to 'non'. For trigger set it to ´un´
			std::string eff_prefix = "";
			calc_eff->set_eff_prefix(eff_prefix);	
			calc_eff->set_cut_for_systematics(4);
			 //set to diff or int
			calc_eff->set_calc_method("int");

 			histo_names->push_back("bjet_bDiscrim_cut_HighEff_");
// 			//for bjet vs non-bjet
// 			histo_names->push_back("bjet_bDiscrim_cut_HighPur_");
// 			histo_names->push_back("bjet_bDiscrim_cut_JetProb_");
// 			histo_names->push_back("bjet_bDiscrim_cut_JetBProb_");
// 			histo_names->push_back("bjet_bDiscrim_cut_SSVertex_");
// 			histo_names->push_back("bjet_bDiscrim_cut_CSVertex_");

			if(eff_prefix == ""){ 
				//	histo_names->push_back("nonbjet_bDiscrim_cut_HighEff_");
				//histo_names->push_back("lightjet_bDiscrim_cut_HighEff_");

				//for bjet vs non-bjet
// 				histo_names->push_back("nonbjet_bDiscrim_cut_HighPur_");
// 				histo_names->push_back("nonbjet_bDiscrim_cut_JetProb_");
// 				histo_names->push_back("nonbjet_bDiscrim_cut_JetBProb_");
// 				histo_names->push_back("nonbjet_bDiscrim_cut_SSVertex_");
// 				histo_names->push_back("nonbjet_bDiscrim_cut_CSVertex_");
			}

			calc_eff->set_histo_names(histo_names);
			calc_eff->calculator();
		}
	else if(analysis == "trigger")
		{
			CalculateEff<TriggerEfficiency> *calc_eff = new CalculateEff<TriggerEfficiency>(outfile);
			calc_eff->set_mass(mass);
			calc_eff->set_cutset(cutset);
			calc_eff->set_e_mu(e_mu,d_channel);
			calc_eff->set_version(version);
			calc_eff->set_input_directory(input_files_directory);
			calc_eff->set_analysis(analysis_cross_section);
			calc_eff->set_luminosity(luminosity);
			calc_eff->set_background_file_names(bkg_file_names);

			calc_eff->set_eff_prefix("un");
			calc_eff->set_calc_method("diff");

			if(e_mu == "mu"){
				histo_names->push_back("trig_mu15_pt_");
				histo_names->push_back("trig_mu15_eta_");
				histo_names->push_back("trig_acc_mu15_pt_");
				histo_names->push_back("trig_acc_mu15_eta_");
			}
			if(e_mu == "e"){
				histo_names->push_back("trig_e15_pt_");
				histo_names->push_back("trig_e15_eta_");
				histo_names->push_back("trig_acc_e15_pt_");
				histo_names->push_back("trig_acc_e15_eta_");
			}
			calc_eff->set_histo_names(histo_names);
			calc_eff->calculator();
		}
	else
	{   
		CalculateEff<EfficiencyXPurity> *calc_eff = new CalculateEff<EfficiencyXPurity>(outfile);
		calc_eff->set_mass(mass);
		calc_eff->set_cutset(cutset);
		calc_eff->set_e_mu(e_mu,d_channel);
		calc_eff->set_version(version);
		calc_eff->set_input_directory(input_files_directory);
		calc_eff->set_analysis(analysis_cross_section);
		calc_eff->set_luminosity(luminosity);
		calc_eff->set_background_file_names(bkg_file_names);

		calc_eff->set_calc_method("int");

		std::vector<std::string> *symmetric_cuts = new std::vector<std::string>();//Implementation not optimal
		symmetric_cuts->push_back("muon_eta");
		symmetric_cuts->push_back("electron_eta");
		symmetric_cuts->push_back("jet_eta");
		std::vector<std::string> *max_requirements = new std::vector<std::string>();//Programs default all cuts are min_value
		max_requirements->push_back("mu_PFrelIso_");
		max_requirements->push_back("e_PFrelIso_");

		histo_names->push_back("mu_PFrelIso_");
//		histo_names->push_back("mu_PFrelIso0.05_");
//		histo_names->push_back("mu_PFrelIso0.1_");
//		histo_names->push_back("mu_PFrelIso0.15_");
//		histo_names->push_back("mu_PFrelIso0.2_");
//		histo_names->push_back("mu_PFrelIso0.25_");
//		histo_names->push_back("mu_PFrelIso0.3_");
//		histo_names->push_back("mu_PFrelIso0.35_");
//		histo_names->push_back("mu_PFrelIso0.4_");
//		histo_names->push_back("mu_PFrelIso0.45_");
//		histo_names->push_back("mu_PFrelIso0.5_");
//		histo_names->push_back("e_PFrelIso_");

		calc_eff->set_symmetric_cuts(symmetric_cuts);
		calc_eff->set_max_requirements(max_requirements);

		//		histo_names->push_back("muon_pt_");
// 		histo_names->push_back("muon_eta_");
// 		histo_names->push_back("jet1_pt_");
// 		histo_names->push_back("jet2_pt_");
		
		calc_eff->set_histo_names(histo_names);
		calc_eff->calculator();
	}

	delete outfile;
	outfile = NULL;
        delete histo_names;
        histo_names = NULL;
        delete bkg_file_names;
        bkg_file_names = NULL;
}
