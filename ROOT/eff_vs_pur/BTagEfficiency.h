#ifndef BTAGEFFICIENCY_H
#define BTAGEFFICIENCY_H
#include "TH1F.h"
#include "TFile.h"
#include "math.h"
#include <iostream>
#include "EfficiencyPurity.h"

class BTagEfficiency:public EfficiencyPurity{
	public:
		BTagEfficiency(TFile *outfile);
		~BTagEfficiency();
		virtual void calculate();
		
	private:
		TFile *outfile;
		void calculate_eff_sys_error();

		double eff_plus_cut;
		double eff_less_cut;
		std::map<int,double> eff_x_y_coords;
		std::map<int,std::pair<double,double > > draw_line;
};

#endif
