#!/usr/bin/perl

use Math::Round qw/nearest/;

open(FILE, "<muon.txt");
@lines = <FILE>;
close(FILE);

foreach $line(@lines){
	chomp $line;
	#print "LooseMC" if($line =~ /cutset 04/);
	print "MediumMC" if($line =~ /cutset 05/);
	print "TightMC" if($line =~ /cutset 06/);
	print "SuperTightMC" if($line =~ /cutset 07/);
	print "HyperTightMC" if($line =~ /cutset 08/);

	#print "VeryLooseMC" if($line =~ /cutset 09/);
	#print "LooseMC" if($line =~ /cutset 10/);
	next if($line =~ /cutset 09/);
	next if($line =~ /cutset 10/);
	print "MediumMC" if($line =~ /cutset 11/);
	print "TightMC" if($line =~ /cutset 12/);
	print "SuperTightMC" if($line =~ /cutset 13/);
	print "HyperTightMC" if($line =~ /cutset 14/);
	if($line =~ /Optimal Eff\*Pur: ([\d\.]+) \+ ([\d\.]+) - ([\d\.]+)/){
		print " & \$";
		print nearest(0.0001, $1);
		print " \\pm ";
		print nearest(0.0001, $2);
		#print " - ";
		#print nearest(0.0001, $3);
		print "\$ \\\\\n";
	}
}
