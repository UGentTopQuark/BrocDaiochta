#include "EfficiencyManager.h"

tpg::EfficiencyManager::EfficiencyManager(TFile *outfile)
{
  this->outfile = outfile;
  config_reader = NULL;
}

tpg::EfficiencyManager::~EfficiencyManager(){
  for(std::map<std::string, tpg::HistoReader*>::iterator histo_reader = histo_readers.begin();
      histo_reader != histo_readers.end();
      ++histo_reader){
    delete histo_reader->second;
  }
  histo_readers.clear();
}

void tpg::EfficiencyManager::run_trigger_monitor()
{
  std::cout<<"Getting histos....\n";
  get_histos_from_files();
  std::cout<<"Making plots....\n";
  create_overlay_plots();
}

void tpg::EfficiencyManager::create_overlay_plots()
{
        outfile->cd();
 
  
	std::vector<std::string> global_vars = config_reader->get_variables_for_section("global");
	for(std::vector<std::string>::iterator var = global_vars.begin();
	    var != global_vars.end();
	    ++var){
		if(var->find("overlay") != std::string::npos){
			std::vector<std::string> sections_to_overlay = config_reader->get_vec_var(*var,"global",true);
			
			std::vector<std::string> filenames;
			for(std::vector<std::string>::iterator section = sections_to_overlay.begin();
			    section != sections_to_overlay.end(); section++){
				filenames.push_back(config_reader->get_var("file_name", *section, true));
			}
			
			
			
			if(filenames.size() == 0){ return;}
			if(histo_readers.find(filenames[0]) == histo_readers.end()){
				std::cerr << "ERROR: file: " << filenames[0] << " does not have histo reader " << std::endl;
				return;
			}
			
			std::map<std::string, TGraphAsymmErrors*> saved_eff_graphs = histo_readers[filenames[0]]->get_all_graphs();
			
			for(std::map<std::string, TGraphAsymmErrors*>::iterator graph = saved_eff_graphs.begin();
			    graph != saved_eff_graphs.end(); graph++){
				std::string graph_name = graph->first;
				
				CanvasHolder *cholder = new CanvasHolder();
				cholder->addHLine(1,"",2);
				cholder->setOptStat(000000);
				
				std::string ctitle = graph_name+"_overlay";
				bool first_graph = true;
				int i = 0;
				for(std::vector<std::string>::iterator filename = filenames.begin();
				    filename != filenames.end(); filename++){
					
					if(histo_readers.find(*filename) == histo_readers.end()){
						std::cerr << "Error: No histo reader for " << *filename << std::endl;
						continue;
					}
					if(histo_readers[*filename]->get_efficiency_graph(graph_name) == NULL){
						std::cerr << "Error: graph " << graph_name;
						std::cerr << "not found for " << *filename << std::endl;
						continue;
					}
					std::string section = sections_to_overlay[i];
					if(first_graph){
						
						cholder->setTitleY("Efficiency");
						std::string title = config_reader->get_var("title",section,true);
						cholder->addGraph(histo_readers[*filename]->get_efficiency_graph(graph_name), title, title, "APz");
						//cholder->setTitleX(config_reader->get_var("xtitle", section,true));
						cholder->setBordersY(atof(config_reader->get_var("ymin", section,true).c_str()), atof(config_reader->get_var("ymax", section,true).c_str()));
						cholder->setLumi(atof(config_reader->get_var("lumi", section,true).c_str()));
						//cholder->setDescription(config_reader->get_var("description", section,false).c_str());
						first_graph = false;
					}else{
						std::string title = config_reader->get_var("title",sections_to_overlay[i],true);
						cholder->addGraph(histo_readers[*filename]->get_efficiency_graph(graph_name), title, title, "Pz");
					}
					ctitle = section+"_"+ctitle;
					
					i++;
				}
				
				cholder->setCanvasTitle(ctitle);
				
				//std::vector<int> styles;
				//styles.push_back(1);
				//styles.push_back(2);
				
				//cholder->setLineStylesGraph(styles);
				cholder->setMarkerSizeGraph(1.2);
				cholder->setLineSizeGraph(2.5);
				cholder->setLegendOptions(0.40, 0.25, "LowerRight");
				//cholder->setLegendOptions(0.40, 0.14, "LowerRight");
				cholder->save("png");
				cholder->write(outfile);
		
				delete cholder;
				
			}
		}
	}
	
	
}

void tpg::EfficiencyManager::get_histos_from_files()
{
  std::vector<std::string> *sections = config_reader->get_sections();
  
  std::string filename_var = "file_name";
  
  // loop over all sections and make sure all root files are opened
  for(std::vector<std::string>::iterator section = sections->begin();
      section != sections->end();
      ++section){
    if(*section != "global"){
      std::string filename = config_reader->get_var(filename_var, *section, true);
      
      // if this is a new file, book a HistoReader
      if(histo_readers.find(filename) == histo_readers.end()){
	      TFile *infile = new TFile(filename.c_str(), "OPEN");
	      open_files.push_back(infile);
	      tpg::HistoReader *histo_reader = new tpg::HistoReader(outfile);
	      histo_reader->set_config_reader(config_reader);
	      histo_reader->set_section(*section);
	      histo_readers[filename] = histo_reader;
	      histo_reader->read(infile);
      }
      
      // add variable to HistoReader for new sections
      //      histo_readers[filename]->add_section(*section);
    }
  }
}
