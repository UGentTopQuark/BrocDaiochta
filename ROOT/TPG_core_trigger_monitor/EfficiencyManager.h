#ifndef TPG_EFFICIENCYMANAGER_H
#define TPG_EFFICIENCYMANAGER_H

/**
 * \class tpg::EfficiencyManager
 * 
 * \brief Manages read in files, histos available in each file,  prints plots and overlays.
 *
 * The main class of counting_tag_and_probe. For each file a HistoReader is created. 
 * The HistoReaders process the individual files and return efficiency histograms.
 * These are then returned to EfficiencyManager, formatted, and overlayed if necessary.
 *
 */

#include "ConfigReader/ConfigReader.h"
#include "CanvasHolder/Canvas_Holder.h"
#include "TFile.h"
#include "TTree.h"
#include "HistoReader.h"
#include <map>

namespace tpg{
	class EfficiencyManager{
		public:
			EfficiencyManager(TFile *outfile);
			~EfficiencyManager();

			inline void set_config_reader(eire::ConfigReader *config_reader) { this->config_reader = config_reader; };
			/// main function, run this after setting ConfigReader
			void run_trigger_monitor();
		private:
			std::vector<std::vector<std::pair<std::string,std::string> > > get_overlays();
			/// after processing of histos, create plots
			//void create_plots();
			eire::ConfigReader *config_reader;
			/// for each file create a HistoReader to process the file
			void get_histos_from_files();
			void create_overlay_plots();

			std::map<std::string, tpg::HistoReader*> histo_readers;
			std::vector<TFile*> open_files;
			TFile *outfile;
	};
}

#endif
