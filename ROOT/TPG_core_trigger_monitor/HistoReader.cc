#include "HistoReader.h"

tpg::HistoReader::HistoReader(TFile *outfile)
{
  this->outfile = outfile;
  efficiency = new Efficiency();
  efficiency->set_outfile(outfile);
  
  num_denoms.push_back(std::pair<std::string,std::string>("_HLTPass","_All"));
  num_denoms.push_back(std::pair<std::string,std::string>("_HLTPass","_L1Pass"));
  num_denoms.push_back(std::pair<std::string,std::string>("_L1Pass","_All"));
}

tpg::HistoReader::~HistoReader()
{
}


// main function, process all events for tree
void tpg::HistoReader::read(TFile *infile)
{
	TKey *key;

	infile->cd();
	std::cout << " Searching in infile "<<std::endl;
	TIter nextkey(infile->GetListOfKeys());
	std::cout << " Found list of keys" << std::endl;
	
	while ((key = (TKey*)nextkey())){
		std::cout << "processing: " << key->GetName() << std::endl;
		std::cout << "class: " << key->GetClassName() << std::endl;
		
		std::string histo_name = key->GetName();
		
		if((std::string) key->GetClassName() == "TH1F"){
			//Get trigger name, x-axis variable and trigger level from histo name
			std::string tmp_name = histo_name;
			size_t level_pos = tmp_name.rfind("_");
			std::string level = tmp_name.substr(level_pos);
			tmp_name = tmp_name.erase(level_pos);
			size_t var_pos = tmp_name.rfind("_");
			std::string var = tmp_name.substr(var_pos);
			std::string trigger_name = tmp_name.erase(var_pos);
			//Make sure var isnt in vector already
 			if(trigger_vars.find(trigger_name) == trigger_vars.end() || 
			   (find(trigger_vars[trigger_name].begin(),trigger_vars[trigger_name].end(),var)
			    == trigger_vars[trigger_name].end())){
				trigger_vars[trigger_name].push_back(var);
			}
 			if(trigger_levels.find(trigger_name) == trigger_levels.end() || 
			   (find(trigger_levels[trigger_name].begin(),trigger_levels[trigger_name].end(),level)
			    == trigger_levels[trigger_name].end())){
				trigger_levels[trigger_name].push_back(level);
			}
		}
		if((std::string) key->GetClassName() == "TH2F"){
			std::cerr << "Error: HisotReader not set up for 2D histos" << std::endl;
		}
	
	}

	for(std::map<std::string,std::vector<std::string> >::iterator trig_var = trigger_vars.begin();
	    trig_var != trigger_vars.end(); trig_var++){
		std::string trigger = trig_var->first;

		std::vector<std::string> triggers_to_plot = config_reader->get_vec_var("triggers",config_section,true);

		if(find(triggers_to_plot.begin(),triggers_to_plot.end(),trigger) == triggers_to_plot.end()){
			continue;
		}
		
		std::string rebin_str = config_reader->get_var(trigger+"_rebin",config_section,false);
		double rebin = -1;
		if(rebin_str != ""){
			rebin = atof(rebin_str.c_str());
		}

		std::vector<std::string> vars = trig_var->second;
		std::vector<std::string> levels = trigger_levels[trigger];

// 		std::cout << "trigger: " << trigger << std::endl;
// 		for(size_t i = 0; i < levels.size();i++){
// 			std::cout << "level: " << levels[i] << std::endl;
// 			if(i < vars.size())
// 				std::cout << "var: " << vars[i] << std::endl;
// 		}
				
		std::map<std::string,bool> rebinned_histos;
		for(std::vector<std::pair<std::string,std::string> >::iterator iter_num_denom = num_denoms.begin();
		    iter_num_denom != num_denoms.end(); iter_num_denom++){
			std::pair<std::string,std::string> num_denom = *iter_num_denom;
			//Check if both numerator and denominator exist for this trigger.
			if(find(levels.begin(),levels.end(),num_denom.first) != levels.end() &&
			   find(levels.begin(),levels.end(),num_denom.second) != levels.end()){

				for(std::vector<std::string>::iterator var = vars.begin();
				    var != vars.end();var++){
					std::string num_histo = trigger+*var+num_denom.first;
					std::string denom_histo = trigger+*var+num_denom.second;
					std::string eff_graph = "Eff_"+trigger+*var+num_denom.first+num_denom.second;
					
					TH1F *num_h = (TH1F *) infile->Get(num_histo.c_str());
					TH1F *denom_h = (TH1F *) infile->Get(denom_histo.c_str());
					if(!num_h || !denom_h){ 
						std::cerr << "NULL histos for " << trigger << " " << *var << " ";
						std::cerr << num_denom.first << " " << num_denom.second << std::endl;
						continue;
					}
					if(rebin != -1 && rebinned_histos.find(num_histo) == rebinned_histos.end()){
						num_h->Rebin(rebin);
						rebinned_histos[num_histo] = true;
					}
					if(rebin != -1 && rebinned_histos.find(denom_histo) == rebinned_histos.end()){
						denom_h->Rebin(rebin);
						rebinned_histos[denom_histo] = true;
					}
						

					std::cout << "Creating TGraph for: " << eff_graph << std::endl; 
					efficiency_graphs[eff_graph] = efficiency->calc_bayes_eff(denom_h,num_h,eff_graph);
					outfile->cd();
					efficiency_graphs[eff_graph]->Write(); 

				}
	
			}
 		}
	}
		

}

TGraphAsymmErrors* tpg::HistoReader::get_efficiency_graph(std::string graph_name)
{

	if(efficiency_graphs.find(graph_name) == efficiency_graphs.end()){
		return NULL;
	}else{
		return efficiency_graphs[graph_name];
	}
}
