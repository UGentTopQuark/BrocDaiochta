#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include "ConfigReader/ConfigReader.h"
#include "EfficiencyManager.h"
#include "TFile.h"

int main(int argc, char **argv)
{
  if(argc < 2){
    std::cerr << "usage: ./counting_tag_and_probe <config file>" << std::endl;
    exit(1);
  }
  
  // read config file name as parameter from command line
  std::string filename(argv[1]);
  
  eire::ConfigReader *config_reader = new eire::ConfigReader();
  config_reader->read_config_from_file(filename);
  
  std::cout << "finished reading file" << std::endl;
  std::string outfile_name = config_reader->get_var("outfile", "global", true);
  TFile *outfile = new TFile(outfile_name.c_str(), "RECREATE");
  tpg::EfficiencyManager *manager = new tpg::EfficiencyManager(outfile);
  
  manager->set_config_reader(config_reader);
  std::cout<<"Running monitor....\n";
  manager->run_trigger_monitor();
  
  outfile->Write();
  outfile->Close();
  
  std::cout<<"TEST4\n";

    if(config_reader){
    delete config_reader;
    config_reader = NULL;
  }
  if(manager){
    delete manager;
    manager = NULL;
  }

  return 0;
}
