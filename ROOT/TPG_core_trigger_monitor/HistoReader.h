#ifndef TPG_HISTOREADER_H
#define TPG_HISTOREADER_H

/**
 * \class tpg::HistoReader
 *
 * \brief Find all histos in a file and creates efficency graphs
 *
 * \author walsh
 */

#include "TTree.h"
#include "TFile.h"
#include "TKey.h"
#include "TDirectory.h"
#include "TGraphAsymmErrors.h"
#include "Efficiency/Efficiency.h"
//#include "VariablePlotter2D.h"
#include "ConfigReader/ConfigReader.h"
#include <map>

namespace tpg{
	class HistoReader{
		public:
			HistoReader(TFile *outfile);
			~HistoReader();
					
			inline void set_config_reader(eire::ConfigReader *config_reader){ this->config_reader = config_reader; };
			inline void set_section(std::string section){config_section = section;}
			/// adds new variable for plotting (which equals a section in the config file)
			//	void add_section(std::string section);
			void read(TFile *infile);
			inline std::map<std::string, TGraphAsymmErrors*> get_all_graphs(){return efficiency_graphs;}
			TGraphAsymmErrors* get_efficiency_graph(std::string name);
			/// returns efficiency graph for combination of plots in TagAndProbeManager
			//TGraphAsymmErrors *get_efficiency_graph(std::string section);
			//TH2F *get_efficiency_graph2D(std::string section);
		private:
			TFile *outfile;
			eire::ConfigReader *config_reader;
			Efficiency *efficiency;
			/// holds map<section name for variable, VariablePlotter* for variable>
			//std::map<std::string, tpg::VariablePlotter1D*> variables;
                        //std::map<std::string, tpg::VariablePlotter2D*> variables2;
			std::string config_section;

			std::map<std::string,std::vector<std::string> > trigger_vars;
			std::map<std::string,std::vector<std::string> > trigger_levels;
			std::vector<std::pair<std::string,std::string> > num_denoms;
			std::map<std::string, TGraphAsymmErrors*> efficiency_graphs;
			
	};
}

#endif
