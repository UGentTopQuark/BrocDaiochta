#include "FitManager.h"

using namespace RooFit;

FitManager::FitManager()
{
	fm_outfile = NULL;
	fm_output_dir = "./";
	fm_run_full_sys_study = false;
	fm_run_multi_rebin_study = false;
	fm_smear_template_shapes = false;
	fm_vary_constraint_mean = false;
	fm_use_real_data = false;
	fm_n_pseudo_exp = 500;
	fm_sys_study_name = "";
	fm_nominal_fit_setup = NULL;
	fm_fit_setup = NULL;
	hh1d = NULL;
	hh2d = NULL;

	fm_data_fit_res = NULL;
	fm_fit_par_data = NULL;

	fm_id_label["tt_total"] = "t #bar{t}";
	fm_id_label["wzj"] = "V + Jets";
	fm_id_label["wzq"] = "V + Jets, QCD";
	fm_id_label["wzqmm"] = "V + Jets, QCD";
	fm_id_label["wj"] = "W + Jets";
	fm_id_label["zj"] = "Z + Jets";
	fm_id_label["qcd"] = "QCD";
	fm_id_label["qcdmm"] = "QCD";
	fm_id_label["qcdmmmu"] = "QCD #mu";
	fm_id_label["qcdmme"] = "QCD e";
	fm_id_label["st"] = "Single Top";

	fm_id_label["muon_eta_"] = "muon #eta";
	fm_id_label["muon_eta_tf_"] = "muon #eta";
	fm_id_label["muon_abseta_"] = "muon |#eta|";
	fm_id_label["electron_eta_"] = "electron #eta";
	fm_id_label["electron_eta_tf_"] = "electron #eta";
	fm_id_label["electron_abseta_"] = "electron |#eta|";
	fm_id_label["top_mass_"] = "M3 [GeV]";	
	fm_id_label["top_mass_tf_"] = "M3 [GeV]";
	fm_id_label["event_discriminator_"] = "Event Discriminator";
	TEMP_data_hist_in = NULL;

	//	setTDRStyle();

}

FitManager::~FitManager()
{
	std::cout << "FM: deleting everything " << std::endl;
	delete_all(true);
	std::cout << "FM: deleting nominal fit setup " << std::endl;
	if(fm_nominal_fit_setup){
		//		delete fm_nominal_fit_setup;
		//fm_nominal_fit_setup = NULL;
	}
	std::cout << "FM: End of destructor " << std::endl;
}

void FitManager::set_outfile(TFile *tmp_outfile)
{
	fm_outfile = tmp_outfile;
}
void FitManager::set_output_dir(std::string dir)
{
	fm_output_dir = dir;
}

void FitManager::set_n_pseudo_exp(int n_exp)
{
	fm_n_pseudo_exp = n_exp;
}

void FitManager::set_run_full_sys_study(bool run_study)
{
	fm_run_full_sys_study = run_study;
}
void FitManager::set_run_multi_rebin_study(bool run_study)
{
	fm_run_multi_rebin_study = run_study;
}

void FitManager::set_sys_study_name(std::string study_name)
{
	fm_sys_study_name = study_name;
	fm_sys_study_names.push_back(study_name);
}

void FitManager::set_use_real_data(bool fit_real_data)
{
	fm_use_real_data = fit_real_data;
}

void FitManager::set_fit_setup(FitSetup *fs)
{
	fm_fit_setup = fs;

}

void FitManager::set_nominal_fit_setup(FitSetup *fs)
{
	fm_nominal_fit_setup = fs;
}
	

//sys_study_name == "" when running a default mc_study which will be compared with the 
//results of jes,btag etc sys studies
//RooMCStudy(template you'll fit to, x axis variable,FitModel = template from MC)  
void FitManager::doFit(bool have_data, bool is_real_data,bool data_from_nominal,bool external_pseudodata)
{
	if(verbose) std::cout << " FM: Setting up doFit " << std::endl;
	if(verbose && have_data) std::cout << " FM: Using separate data histo " << std::endl;
	if(verbose && is_real_data) std::cout << " FM: Using real data" << std::endl;
	if(verbose && fm_smear_template_shapes) std::cout << " FM: Smearing template shapes " << std::endl;
	if(verbose && is_real_data &&fm_vary_constraint_mean) std::cout << " FM: Varying constraint mean in data fit" << std::endl;
	initialise_fit_output_histos();

	FitSetup *fs = fm_fit_setup;
	std::string fit_id = fs->fit_id();
	RooAbsPdf *data_model = NULL;
	if(!is_real_data && !external_pseudodata){
		if(data_from_nominal){
			data_model = fm_nominal_fit_setup->get_template("MC");
			std::cout << "FM Fit: Taking data sample from nominal" << std::endl;
		}else if(have_data){
			//This is only used for the display. data_hist taken directly from fs->datahist
			data_model = fs->get_template("data");
			std::cout << "FM Fit: Taking separate data sample. not real data" << std::endl;
		}else{ 
			data_model = fs->get_template("MC");
			std::cout << "FM Fit: Data model from same MC as templates" << std::endl;
		}
	}else{ 
		data_model = NULL;
		if(is_real_data) std::cout << "FM Fit: Using real data.Data model NULL" << std::endl;
		if(external_pseudodata) std::cout << "FM Fit: Using external_pseudodata.Data model NULL" << std::endl;
	}

	RooAbsPdf *mc_model = fs->get_template("MC");

	//Save the parameters so that they can be reinitialised before each fit
	std::map<std::string,RooArgSet> dependants;
	std::cout << "Adding dependants"<< std::endl;
	std::map<std::string,RooRealVar* > obs = fs->observables();
	for(std::map<std::string,RooRealVar* >::iterator o = obs.begin();
	    o != obs.end();o++){
		dependants[fit_id].add(*o->second);
	}
	std::map<std::string,RooArgSet*> params;
	if(data_model) params["gen_"+fit_id+"_"+fm_sys_study_name] = data_model->getParameters(&(dependants[fit_id]));
	if(data_model) params["gen_init_"+fit_id+"_"+fm_sys_study_name] = (RooArgSet*) params["gen_"+fit_id+"_"+fm_sys_study_name]->snapshot(kFALSE) ;
	
	params["fit_"+fit_id+"_"+fm_sys_study_name]  = mc_model->getParameters(&(dependants[fit_id])) ;
	params["fit_init_"+fit_id+"_"+fm_sys_study_name]  = (RooArgSet*) params["fit_"+fit_id+"_"+fm_sys_study_name]->snapshot(kTRUE) ;

	//RooDataSet simplifies storing and plotting of results.
	RooRealVar *nllVar = new RooRealVar("NLL","NLL",0);
	RooRealVar *nMCData = new RooRealVar("nMCData","nMCData",0);
	RooRealVar *ngenVar = new RooRealVar("ngen","ngen",0);
	RooRealVar *ngenVar2 = new RooRealVar("ngen2","ngen2",0);
	RooRealVar *ngenVar3 = new RooRealVar("ngen3","ngen3",0);
	RooRealVar *ngenVar4 = new RooRealVar("ngen4","ngen4",0);
	RooRealVar *ngenVarall = new RooRealVar("ngenall","ngenall",0);
	RooArgSet tmp(*params["fit_"+fit_id+"_"+fm_sys_study_name]);
	tmp.add(*nllVar); tmp.add(*ngenVar);
	tmp.add(*ngenVar2); tmp.add(*ngenVar3); tmp.add(*ngenVar4); tmp.add(*ngenVarall);tmp.add(*nMCData);
	tmp.setAttribAll("StoreError",kTRUE) ;
	tmp.setAttribAll("StoreAsymmError",kTRUE) ;
	fm_fit_par_data = new RooDataSet("fit_par_data","Fit Parameter Data",tmp);

	//List constraints to be used
	std::vector<std::string> constraints_used = fs->gauss_constraints_used();
	TRandom3 *gen_random = new TRandom3();
	//Verbose output. No other use.
	if(constraints_used.size() > 0){
		if(verbose) std::cout << " FM: mgr with gaussian constraints" << std::endl;
		for(std::vector<std::string>::iterator constraint = constraints_used.begin();
		    constraint != constraints_used.end();constraint++){
			std::cout << "Will be adding constraint: " << *constraint << std::endl;
		}
	}

	//RooRandom::randomGenerator()->SetSeed(4783);
	RooDataHist *data_hist = NULL;  
	RooFitResult *fit_result = NULL;
	if(is_real_data && !fm_smear_template_shapes&& !fm_vary_constraint_mean) fm_n_pseudo_exp = 1;
	bool fit_v = false;
 	for(int i = 0;i < fm_n_pseudo_exp;i++){
 		if(i % 100 == 0)
 			std::cout << "Exp: " << i << "***********************************************************" << std::endl;
		
		//Reinitialise input parameters
		if(fit_v && data_model) std::cout << "reinit g " <<  std::endl;
		if(data_model) *(params["gen_"+fit_id+"_"+fm_sys_study_name]) = *(params["gen_init_"+fit_id+"_"+fm_sys_study_name]);

		if(fit_v) std::cout << "reinit f " << std::endl;
		*(params["fit_"+fit_id+"_"+fm_sys_study_name]) = *(params["fit_init_"+fit_id+"_"+fm_sys_study_name]);

		//Get constraints to apply. If fitting to MC vary mean.
		RooArgSet *constraint_gauss_set_external = new RooArgSet();
		std::map<std::string,RooGaussian*> gauss_constraints;
		if(fit_v && constraints_used.size() > 0) std::cout << "varying mean of constraints " << std::endl;
		for(std::vector<std::string>::iterator constraint = constraints_used.begin();
		    constraint != constraints_used.end();constraint++){
			double rndm_mean;
			if(is_real_data && !fm_vary_constraint_mean)
				rndm_mean = fs->constraint_var(*constraint+"_mean");
			else
				rndm_mean = gen_random->Gaus(fs->constraint_var(*constraint+"_mean"),fs->constraint_var(*constraint+"_sigma"));
			
			if(i == 0) std::cout << "Checking nfit for " << *constraint << " mean: " << fs->constraint_var(*constraint+"_mean") << " sigma: " <<fs->constraint_var(*constraint+"_sigma")<< " random mean: " << rndm_mean << std::endl;
			if(fs->nfit(fs->template_name_for_constraint(*constraint),false,true) != NULL){
				gauss_constraints[*constraint] = new RooGaussian((*constraint+"_gconstraint").c_str(),(*constraint+"_gconstraint").c_str(),*fs->nfit(fs->template_name_for_constraint(*constraint),true,false),RooConst(rndm_mean),RooConst(fs->constraint_var(*constraint+"_sigma")));
				if(fit_v) std::cout << "Adding constraint with realvar: " << *constraint << std::endl;
				constraint_gauss_set_external->add(*gauss_constraints[*constraint]);
			}
			else if(fs->rooform_constraint(*constraint,false,true) != NULL){
				if(fit_v) std::cout << "Found nfit form for " << *constraint << std::endl;
				if(fit_v) std::cout << "Adding constraint with formula: " << *constraint << std::endl;
				//rndm_mean = 16.0;//fs->constraint_var(*constraint+"_mean");
				
				gauss_constraints[*constraint] = new RooGaussian((*constraint+"_gconstraint").c_str(),(*constraint+"_gconstraint").c_str(),*fs->rooform_constraint(*constraint,true,false),RooConst(rndm_mean),RooConst(fs->constraint_var(*constraint+"_sigma")));
				constraint_gauss_set_external->add(*gauss_constraints[*constraint]);
			}else{
				std::cout << "ERROR: Can't find RooRealVar or RooFormula for constraint: " << *constraint << std::endl;
				exit(1);
			}
			fm_per_fit_res[*constraint+"_mean"] = rndm_mean;
			if(fit_v) std::cout << "Added constraint: " << *constraint << std::endl;
		}
		
		//Poisson fluctuate template shapes. This means a new mc_model has to be built.
		//The original get_templates("MC") must remain unchanged. 
		RooAbsPdf *mc_model_var = NULL;
		RooAbsPdf *data_model_var = NULL;
		if(fm_smear_template_shapes){
			if(hh1d) mc_model_var = hh1d->smeared_mc_template(fs,fm_do_not_smear_patterns);
			else if(hh2d) mc_model_var = hh2d->smeared_mc_template(fs,fm_do_not_smear_patterns);
			else{
				std::cout << "ERROR: template shape smearing requested but neither hh1d or hh2d set.\n Exiting.\n" <<std::endl;
				exit(1);
			}
				
		}else{
			mc_model_var = mc_model;
		}
	
		//Generate pseudo data if not using real data
 	        if(fit_v) std::cout << "gensample " << std::endl;
 		double nExpGen = 0;
		if(data_model){
			data_model_var = data_model;
			if(have_data && !data_from_nominal) nExpGen = fs->nscaled("data",true);
			else if(data_from_nominal){
				std::vector<std::string> templates = fm_nominal_fit_setup->template_norm_names();
				for(std::vector<std::string>::iterator id = templates.begin();
				    id != templates.end(); id++){
					nExpGen += fm_nominal_fit_setup->nscaled(*id,true);
				}
			}else{
				std::vector<std::string> templates = fs->template_norm_names();
				for(std::vector<std::string>::iterator id = templates.begin();
				    id != templates.end(); id++){
					nExpGen += fs->nscaled(*id,true);
				}
			}
			if(i == 0) std::cout << " Nexp gen: " << nExpGen << std::endl;
			nMCData->setVal(nExpGen);
			data_hist = data_model_var->generateBinned(dependants[fit_id],nExpGen,Extended()) ;
			ngenVarall->setVal(data_hist->sumEntries());
			if(i == 0){ //For display purposes only. Creates TH1 which is displayed in overlay of results to pseudo-data
				RooDataHist *tmp_data_hist = data_model_var->generateBinned(dependants[fit_id],nExpGen,kTRUE,kFALSE) ;
				RooRealVar tmp_x(*fs->observables()[fs->observable_id("x")]);
				if(hh1d)fm_output_histos["gen_data_"+fit_id+"_"+fm_sys_study_name] = dynamic_cast<TH1F*> (tmp_data_hist->createHistogram(("gen_data_"+fit_id+"_"+fm_sys_study_name).c_str(),tmp_x));
				if(hh2d){
					RooRealVar tmp_y(*fs->observables()[fs->observable_id("y")]);
					fm_output_histos_2D["gen_data_"+fit_id+"_"+fm_sys_study_name] = dynamic_cast<TH2F*> (tmp_data_hist->createHistogram(("gen_data_"+fit_id+"_"+fm_sys_study_name).c_str(),tmp_x,YVar(tmp_y)));
				}
				delete tmp_data_hist;
			}
		}else if(external_pseudodata){
			data_hist = fs->pseudodata_datahist(i);
			if(i == 0){ //For display purposes only. Creates TH1 which is displayed in overlay of results to pseudo-data
				RooRealVar tmp_x(*fs->observables()[fs->observable_id("x")]);
				if(hh1d)fm_output_histos["gen_data_"+fit_id+"_"+fm_sys_study_name] = dynamic_cast<TH1F*> (data_hist->createHistogram(("gen_data_"+fit_id+"_"+fm_sys_study_name).c_str(),tmp_x));
			}
		}else if(is_real_data){
			data_hist = fs->datahist("data",true);
		}
		
		//Fit
		if(fit_v){ std::cout << "fit " << std::endl;
			if(mc_model_var)std::cout << "mc model not NULL " <<mc_model_var<<std::endl; 
			if(fit_result)std::cout << "fitr not NULL " <<  std::endl;
			if(data_hist)std::cout << "dhist not NULL " << data_hist << std::endl;
			if(constraint_gauss_set_external)std::cout << "cge not NULL " << constraint_gauss_set_external<< std::endl;
		}	       
		
		//RooMsgService::instance().Print(); 
		RooMsgService::instance().setStreamStatus(1,kFALSE);
		//RooMsgService::instance().setStreamStatus(0,kFALSE);
		Int_t printlevel = -1;
		if(is_real_data && !fm_smear_template_shapes && !fm_vary_constraint_mean)
			printlevel = 2;
		if(constraints_used.size() > 0){
			if(fit_v){ std::cout << "fit with constraint" << std::endl;}
			//fs->get_template("MC")->fitTo(*fs->datahist("data",true),Minos(kTRUE),Save(kTRUE),Extended(kTRUE),PrintLevel(printlevel));
			fit_result = mc_model_var->fitTo(*data_hist,Minos(kTRUE),Save(kTRUE),Extended(kTRUE),PrintLevel(printlevel),ExternalConstraints(*constraint_gauss_set_external));
			//fit_result = mc_model_var->fitTo(*data_hist,Minos(kTRUE),Save(kTRUE),Extended(kTRUE),PrintLevel(printlevel),ExternalConstraints(*constraint_gauss_set_external),SumW2Error(kTRUE));
		}
		else
			fit_result = mc_model_var->fitTo(*data_hist,Minos(kTRUE),Extended(),Save(kTRUE),Extended(kTRUE),PrintLevel(printlevel));
		if(fit_v){ std::cout << "nfit st: "; std::cout << fs->nfit("st")->getVal() << std::endl;}
		if(fit_v) std::cout << "fill " << std::endl;
		
		//	if(fs->nfit("tt_total")->getVal() == fs->nscaled("tt_total")){
		//	std::cout << "***$$$*** FIT DIDNT RUN ***$$$*** " << std::endl;
		//Save fit results
		fill_fit_output_histos();
		//nllVar->setVal(fit_result->minNll());
		RooArgSet tmp(*(params["fit_"+fit_id+"_"+fm_sys_study_name]));
		tmp.add(*nllVar);
		tmp.add(*ngenVar);
		tmp.add(*ngenVar2); 
		tmp.add(*ngenVar3); 
		tmp.add(*ngenVar4);
		tmp.add(*ngenVarall);
		tmp.add(*nMCData);
		if(fit_v) std::cout << "save " << std::endl;
		fm_fit_par_data->add(tmp);
		//}
		
		if(fit_v) std::cout << "delete " << std::endl;
		for(std::map<std::string,RooGaussian*>::iterator dc = gauss_constraints.begin();
		    dc != gauss_constraints.end();dc++){
			delete dc->second;
			dc->second = NULL;
		}
		gauss_constraints.clear();
		delete constraint_gauss_set_external; 
		constraint_gauss_set_external = NULL;
		if(!is_real_data && !external_pseudodata){
			delete data_hist; 
			data_hist = NULL;
			delete fit_result;
			fit_result = NULL;
		}else if(is_real_data && !fm_smear_template_shapes&& !fm_vary_constraint_mean){
			fm_data_fit_res = fit_result;
		}else if(is_real_data && (fm_smear_template_shapes || fm_vary_constraint_mean)){
			delete fit_result;
			fit_result = NULL;
		}
		if(hh1d){ hh1d->delete_smeared_template();} //e.g. smeared template shapes and external pseudodata datahists
		else if(hh2d){ hh2d->delete_smeared_template();}
	
		if(fit_v) std::cout << "finished fit " << i <<std::endl;
	}
	//Reinitialise input parameters
	if(fm_run_full_sys_study && (!is_real_data || fm_smear_template_shapes|| fm_vary_constraint_mean)){ 
		//FIXME:May be unnecessary to initialise both of these.
	   if(fit_v && data_model) std::cout << "reinit g " <<  std::endl;
	   if(data_model) *(params["gen_"+fit_id+"_"+fm_sys_study_name]) = *(params["gen_init_"+fit_id+"_"+fm_sys_study_name]);
	   
	   if(fit_v) std::cout << "reinit f " << std::endl;
	   *(params["fit_"+fit_id+"_"+fm_sys_study_name]) = *(params["fit_init_"+fit_id+"_"+fm_sys_study_name]);
	}
	
	delete gen_random;

}	

void FitManager::initialise_fit_output_histos()
{
	delete_output_histos(false);
	
	FitSetup *fs = fm_fit_setup;
	std::string fit_id =fs->fit_id();
	std::vector<std::string> fm_template_norm_names = fs->template_norm_names();
	for(std::vector<std::string>::iterator id = fm_template_norm_names.begin();
	    id != fm_template_norm_names.end();id++){
		std::cout << "Initialising nfit histos for: " << fit_id+"_"+fm_sys_study_name << std::endl;
		fm_output_histos["nfit_evts_"+*id+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("Nfit "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),("Nfit "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),100,fs->range_low(*id),fs->range_high(*id));
		fm_output_histos["nfit_err_"+*id+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("Error "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),("Error "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),4000,0,fs->range_high(*id)-fs->range_low(*id));
		fm_output_histos["nfit_errhi_"+*id+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("Error hi"+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),("Error hi"+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),200,0,fs->range_high(*id));
		fm_output_histos["nfit_errlo_"+*id+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("Error lo"+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),("Error lo "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),200,0,fs->range_high(*id));
		
		fm_output_histos_2D["nfit_evts_vs_err_"+*id+"_"+fit_id+"_"+fm_sys_study_name] = new TH2F(("Nfit vs Error "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),("Error "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),fs->nfit(*id)->getBins()*2,fs->range_low(*id),fs->range_high(*id),200,-1,fs->range_high(*id));
		
		if(id->find("tt_total") != std::string::npos){
			fm_output_histos["rel_precision_"+*id+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("Rel precision "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),("rel precision "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),100,-0.4,0.4);
			
			double ntt = fs->nscaled("tt_total");
			double sqrtntt = sqrt(fs->nscaled("tt_total"));
			double cs = 157.5;
			double stat_err_cs = 3*(sqrtntt/ntt)*cs;
			fm_output_histos["sigma_err_"+*id+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("sigma err "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),("sigma err "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),150,stat_err_cs,stat_err_cs+0.6);
		}
		
		
		fm_output_histos_2D["nfit_evts_vs_err_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->SetXTitle((*id+" nevents").c_str());
		fm_output_histos_2D["nfit_evts_vs_err_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->SetYTitle((*id+" nevents error").c_str());
		
		fm_output_histos["nfit_pull_"+*id+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("Pull "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),("Pull "+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),100,-4,4);

		fm_output_histos_2D["nfit_pull_vs_nevts_"+*id+"_"+fit_id+"_"+fm_sys_study_name] = new TH2F(("Pull_vs_nevts_"+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),("Pull _vs_nevts_"+*id+"_"+fit_id+"_"+fm_sys_study_name).c_str(),200,-4,4,200,fs->range_low(*id),fs->range_high(*id));
		fm_output_histos_2D["nfit_pull_vs_nevts_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->SetXTitle((*id+" pull").c_str());
		fm_output_histos_2D["nfit_pull_vs_nevts_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->SetYTitle((*id+" nevents").c_str());
		
		std::vector<std::string> constraints_used = fs->gauss_constraints_used();
		for(std::vector<std::string>::iterator c = constraints_used.begin();
		    c != constraints_used.end();c++){
			fm_output_histos["constraint_mean_"+*c+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("constraint_mean_"+*c).c_str(),("constraint mean "+*c).c_str(),100,fs->range_low(*c),fs->range_high(*c));
			fm_output_histos["constraint_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("constraint_evts_"+*c).c_str(),("constraint evts after fit "+*c).c_str(),100,fs->range_low(*c),fs->range_high(*c));
			double exp_range = (fs->constraint_var(*c+"_sigma")/fs->constraint_var(*c+"_mean"))*10;
			fm_output_histos["diff_constraint_mean_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name] = new TH1F(("diff_constraint_mean_evts_"+*c).c_str(),("constraint sampled mean - nevts after fit "+*c).c_str(),100,-exp_range,exp_range);
			fm_output_histos_2D["constraint_mean_vs_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name] = new TH2F(("constraint_mean_vs_evts_"+*c).c_str(),("constraint mean vs nevts "+*c).c_str(),100,fs->range_low(*c),fs->range_high(*c),100,fs->range_low(*c),fs->range_high(*c));	
			fm_output_histos_2D["constraint_mean_vs_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->SetXTitle((*c+" constraint mean").c_str());
			fm_output_histos_2D["constraint_mean_vs_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->SetYTitle((*c+" after fit").c_str());
		}
	}
}

void FitManager::fill_fit_output_histos()
{
	FitSetup *fs = fm_fit_setup;
	std::string fit_id = fs->fit_id();
	std::vector<std::string> fm_template_norm_names = fs->template_norm_names();
	for(std::vector<std::string>::iterator id = fm_template_norm_names.begin();
	    id != fm_template_norm_names.end();id++){
		double nevts = -1,err= -1,errhi= -1,errlo= -1,pull= -1,pullhi= -1,pulllo= -1;
		
		nevts = fs->nfit(*id)->getVal();
		err = fs->nfit(*id)->getError();
		errhi = fs->nfit(*id)->getErrorHi();
		errlo = -(fs->nfit(*id)->getErrorLo());
		pull = calculate_pull(fit_id,*id,err);
		pullhi = calculate_pull(fit_id,*id,errhi);
		pulllo = calculate_pull(fit_id,*id,errlo);
		
		fm_output_histos["nfit_evts_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(nevts);
		fm_output_histos["nfit_err_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(err);
		fm_output_histos["nfit_errhi_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(errhi);
		fm_output_histos["nfit_errlo_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(errlo);
		
		fm_output_histos_2D["nfit_evts_vs_err_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(nevts,err);
		//fm_output_histos_2D["nfit_evts_vs_errhi_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(nevts,errhi);
		//fm_output_histos_2D["nfit_evts_vs_errlo_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(nevts,errlo);
		
		fm_output_histos["nfit_pull_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(pull);
		
		fm_output_histos_2D["nfit_pull_vs_nevts_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(pull,nevts);
		
		
		if(id->find("tt_total") != std::string::npos){
			double cs_exp = 157.5;
			double nexp_evts = fs->nscaled(*id);
			double cs_found = nevts*(cs_exp/nexp_evts);
			
			double rel_precision = (cs_found - cs_exp)/cs_exp;
			double cs_found_err = err*(cs_exp/nexp_evts);
			fm_output_histos["rel_precision_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(rel_precision);
			fm_output_histos["sigma_err_"+*id+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cs_found_err);
			
			
		}
		
	}
	std::vector<std::string> constraints_used = fs->gauss_constraints_used();
	for(std::vector<std::string>::iterator c = constraints_used.begin();
	    c != constraints_used.end();c++){
		double nevts = -1,cmean = -1;
		if(fs->nfit(*c,false,true) != NULL){
			nevts =  fs->nfit(*c)->getVal();
		}
		else if(fs->rooform_constraint(*c,false,true) != NULL){
			nevts = fs->rooform_constraint(*c,true,true)->getVal();
		}
		if(fm_per_fit_res.find(*c+"_mean") == fm_per_fit_res.end())
			std::cout << " ERROR: " << *c << "_mean does not exist in fm_per_fit_res " << std::endl;
		else
			cmean = fm_per_fit_res[*c+"_mean"];
		
		fm_output_histos["constraint_mean_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cmean);
		fm_output_histos["constraint_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(nevts);
		fm_output_histos["diff_constraint_mean_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill((cmean-nevts)/cmean);
		fm_output_histos_2D["constraint_mean_vs_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cmean,nevts);
		//fm_output_histos_2D["constraint_mean_vs_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cmean,nevts);
		
		if(*c == "wjzj"){
			fm_output_histos_2D["constraint_mean_vs_zj_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cmean,fs->nfit("zj")->getVal());
			fm_output_histos_2D["constraint_mean_vs_zj_err_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cmean,fs->nfit("zj")->getError());
			fm_output_histos_2D["constraint_mean_vs_zj_relerr_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cmean,fs->nfit("zj")->getError()/fs->nfit("zj")->getVal());
			fm_output_histos_2D["constraint_mean_vs_wj_evts_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cmean,fs->nfit("wj")->getVal());
			fm_output_histos_2D["constraint_mean_vs_wj_err_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cmean,fs->nfit("wj")->getError());
			fm_output_histos_2D["constraint_mean_vs_wj_relerr_"+*c+"_"+fit_id+"_"+fm_sys_study_name]->Fill(cmean,fs->nfit("wj")->getError()/fs->nfit("wj")->getVal());
		}
		
		
	}
}

void FitManager::initialise_sys_study_histos()
{

	FitSetup *fs = fm_fit_setup;
	std::string fit_id = fs->fit_id();
	for(std::vector<std::string>::iterator id = fm_nominal_template_names.begin();
	    id != fm_nominal_template_names.end();id++){
		std::cout << "FM: Initialising sys histos for fit: " << fit_id << " " << *id << " sys: " << fm_sys_study_name << std::endl;
		std::string nevts_h = "Sys_nevents_"+*id;
		std::string err_h = "Sys_error_"+*id;
		std::string relerr_h = "Sys_relerror_"+*id;
		
		fm_output_histos[nevts_h] = new TH1F(("Sys Nevts_"+fit_id).c_str(),("Sys Nevts_"+fit_id).c_str(),100,0,100);
		fm_output_histos[nevts_h]->SetYTitle(("Nevents "+*id).c_str());
		fm_output_histos[err_h] = new TH1F(("Sys Err_"+fit_id).c_str(),("Sys Err_"+fit_id).c_str(),100,0,100);
		fm_output_histos[err_h]->SetYTitle(("Error "+*id).c_str());
		
		fm_output_histos[relerr_h] = new TH1F(("Sys RelErr_"+fit_id).c_str(),("Sys RelErr_"+fit_id).c_str(),100,0,100);
		fm_output_histos[relerr_h]->SetYTitle(("Rel. Error "+*id).c_str());
		
	
	}

}
void FitManager::save_sys_study_results()
{
	FitSetup *fs = fm_fit_setup;
	std::string fit_id = fs->fit_id();
	std::vector<std::string> fm_template_norm_names = fs->template_norm_names();
	int i = 0;
	for(std::vector<std::string>::iterator id = fm_template_norm_names.begin();
	    id != fm_template_norm_names.end();id++){
		std::cout << "FM: Saving results for fit: " << fit_id << " " << *id << " sys: " << fm_sys_study_name << std::endl;
		std::string nominal_id = *id;
		if(fm_nominal_template_names.size() > 0)
			nominal_id = fm_nominal_template_names[i];
		double nevts = 0,nevts_err = 0,err = 0, err_err = 0, relerr = 0, relerr_err = 0;
		double cs = 0,cs_err = 0,err_pb = 0,err_err_pb = 0;
		//Give each sys result its own TH1F. That way they will each have legend entries.
		std::string nevts_h = "Sys_nevents_"+nominal_id;
		std::string err_h = "Sys_error_"+nominal_id;
		std::string relerr_h = "Sys_relerror_"+nominal_id;
		
		if(fm_use_real_data && !fm_smear_template_shapes&& !fm_vary_constraint_mean){
			nevts = fs->nfit(*id)->getVal();
			nevts_err = fs->nfit(*id)->getError();
			err = nevts_err;
			err_err = 0;
			relerr = err/nevts;
			relerr_err = 0;
		}else{
			std::cout << "FM: save_sys: Getting gauss fit results" << std::endl;
			nevts = ngauss_evts[*id+"_mean_"+fit_id+"_"+fm_sys_study_name]->getVal();
			nevts_err = ngauss_evts[*id+"_mean_"+fit_id+"_"+fm_sys_study_name]->getError();
			err = ngauss_evts[*id+"_sigma_"+fit_id+"_"+fm_sys_study_name]->getVal();
			err_err = ngauss_evts[*id+"_sigma_"+fit_id+"_"+fm_sys_study_name]->getError();
			relerr = err/nevts;
			relerr_err = relerr*sqrt((err_err/err)*(err_err/err) + (nevts_err/nevts)*(nevts_err/nevts));
		}
		double cs_exp = 157.5;
		double nexp_evts = fs->nscaled(*id);
		cs = nevts*(cs_exp/nexp_evts);
		cs_err = nevts_err*(cs_exp/nexp_evts);
		err_pb = err*(cs_exp/nexp_evts);
		err_err_pb = err_err*(cs_exp/nexp_evts);
		
		
		fm_sys_study_results[fm_sys_study_name+fit_id].push_back(std::pair<double,double>(nevts,err));
		
		if(!fm_run_multi_rebin_study){
			std::cout << "FM: save_sys: Filling histograms" << std::endl;
			fm_output_histos[nevts_h]->Fill(fm_sys_study_name.c_str(),nevts);
			fm_output_histos[nevts_h]->SetBinError(fm_output_histos[nevts_h]->GetXaxis()->FindBin(fm_sys_study_name.c_str()),nevts_err);
			fm_output_histos[err_h]->Fill(fm_sys_study_name.c_str(),err);
			fm_output_histos[err_h]->SetBinError(fm_output_histos[err_h]->GetXaxis()->FindBin(fm_sys_study_name.c_str()),err_err);
			fm_output_histos[relerr_h]->Fill(fm_sys_study_name.c_str(),relerr);
			fm_output_histos[relerr_h]->SetBinError(fm_output_histos[relerr_h]->GetXaxis()->FindBin(fm_sys_study_name.c_str()),relerr_err);
		}
		
		//Attempt 2:
		std::cout << "FM: save_sys: Filling results2 map" << std::endl;
		fm_sys_study_results2["Nevts_"+nominal_id][fm_sys_study_name] = nevts;
		fm_sys_study_results2["NevtsErr_"+nominal_id][fm_sys_study_name] = nevts_err;
		fm_sys_study_results2["CS_"+nominal_id][fm_sys_study_name] = cs;
		fm_sys_study_results2["CSErr_"+nominal_id][fm_sys_study_name] = cs_err;
		fm_sys_study_results2["Err_"+nominal_id][fm_sys_study_name] = err;
		fm_sys_study_results2["ErrErr_"+nominal_id][fm_sys_study_name] = err_err;
		fm_sys_study_results2["Errpb_"+nominal_id][fm_sys_study_name] = err_pb;
		fm_sys_study_results2["ErrErrpb_"+nominal_id][fm_sys_study_name] = err_err_pb;
		fm_sys_study_results2["RelErr_"+nominal_id][fm_sys_study_name] = relerr;
		fm_sys_study_results2["RelErrErr_"+nominal_id][fm_sys_study_name] = relerr_err;
		double bw = 1;
		if(hh1d) bw = hh1d->histo(*id)->GetBinWidth(1);
		else if(hh2d) bw = hh2d->histo(*id)->GetBinWidth(1);
		fm_sys_study_results2["BinWidth_"+nominal_id][fm_sys_study_name] = bw;
		//Save a copy of nscaled events from FitSetup for each systematic fit.
		fm_sys_study_results2["NScaled_"+nominal_id][fm_sys_study_name] = fs->nscaled(*id,true);
		
		i++;
	}
	
	//For correlation plots.
	std::cout << "FM: save_sys: saving correlations" << std::endl;
	for(size_t i = 0; i != fm_template_norm_names.size();i++){
		for(size_t j = 0; j != fm_template_norm_names.size();j++){
			if(i != j){
				std::string corr_h;
				if(fm_nominal_template_names.size() > 0){
					corr_h = "Sys_Corr_nevts_"+fm_nominal_template_names[i]+"_vs_nevts_"+fm_nominal_template_names[j];
				}else{
					corr_h = "Sys_Corr_nevts_"+fm_template_norm_names[i]+"_vs_nevts_"+fm_template_norm_names[j];
				}
				if(fm_output_histos.find(corr_h) == fm_output_histos.end()){
					fm_output_histos[corr_h] = new TH1F(("Corr_Nevts_"+fit_id).c_str(),("Corr_"+fit_id).c_str(),100,0,100);
					fm_output_histos[corr_h]->SetYTitle(("Nevents Correlation "+fm_template_norm_names[i]+ " " + fm_template_norm_names[j]).c_str());
				}
				
				double corr = 0;

				if(fm_use_real_data && !fm_smear_template_shapes&& !fm_vary_constraint_mean){
					corr = data_fit_res()->correlation(("nevts_"+fm_template_norm_names[i]).c_str(),("nevts_"+fm_template_norm_names[j]).c_str());
				}else{
					corr = fit_par_data()->correlation(*(fs->nfit(fm_template_norm_names[i],true)),*(fs->nfit(fm_template_norm_names[j],true)));
				}
				
				fm_output_histos[corr_h]->Fill(fm_sys_study_name.c_str(),corr);
				fm_output_histos[corr_h]->SetBinError(fm_output_histos[corr_h]->GetXaxis()->FindBin(fm_sys_study_name.c_str()),0);
				
				fm_sys_study_results2["Nevts_"+fm_template_norm_names[i]+"_vs_Nevts_"+fm_template_norm_names[j]][fm_sys_study_name] = corr;
			}
			
		}
	}
}



//Do this just before calculating gaussians
void FitManager::initialise_ngauss_evts()
{
	if(verbose) std::cout << "FM: Initialising gaussian variables" << std::endl;
	delete_ngauss_evts();

	FitSetup *fs = fm_fit_setup;
	std::string fit_id = fs->fit_id();
	std::vector<std::string> fm_template_norm_names = fs->template_norm_names();
	//Get sigma from n fitted events to initialise the gaussian variables
	for(std::vector<std::string>::iterator id = fm_template_norm_names.begin();
	    id != fm_template_norm_names.end();id++){
		TH1F* tmp = dynamic_cast<TH1F*> (output_histo("nfit_evts_"+*id+"_"+fit_id+"_"+fm_sys_study_name)->Clone());
		double_t rms = tmp->GetRMS(1);
		
		if(verbose) std::cout << "Initialising ngauss entry: " << *id << "_gmean_" << fit_id << std::endl;
		//ngauss _evts[*id+"_mean"+"_"+fit_id+"_"+fm_sys_study_name] = new RooRealVar((*id+"_gmean"+"_"+fit_id+"_"+fm_sys_study_name).c_str(),"Mean of Nevents", fs->nscaled(*id),fs->range_low(*id),fs->range_high(*id));
		// 			ngauss_evts[*id+"_sigma"+"_"+fit_id+"_"+fm_sys_study_name] = new RooRealVar((*id+"_gsigma"+"_"+fit_id+"_"+fm_sys_study_name).c_str(),"Width of Nevents",rms,rms/2,rms*2);
		ngauss_evts[*id+"_mean"+"_"+fit_id+"_"+fm_sys_study_name] = new RooRealVar("mean","Mean of Nevents", fs->nscaled(*id),fs->range_low(*id),fs->range_high(*id));
		ngauss_evts[*id+"_sigma"+"_"+fit_id+"_"+fm_sys_study_name] = new RooRealVar("sigma","Width of Nevents",rms,rms/2,rms*2);
		delete tmp;
		tmp = NULL;
		
	}
	
	
	if(verbose) std::cout << "FM: gaussian variables initialised" << std::endl;
}

double FitManager::calculate_pull(std::string fit_id, std::string id, double err)
{
	FitSetup *fs = fm_fit_setup;

	double delta = fs->nfit(id)->getVal() - fs->nscaled(id);
	double pull;
	if(err == -999)
		pull = delta/fs->nfit(id)->getError();
	else 
		pull = delta/err;
	return pull;


}
/******************** Variable Access ****************************/

TFile* FitManager::outfile()
{
	if(fm_outfile == NULL)
		std::cout << " ERROR:FitManager, NULL outfile " << std::endl;

	return fm_outfile;
}

std::string FitManager::output_dir()
{
	return fm_output_dir;
}

TH1F* FitManager::output_histo(std::string id, bool need)
{
// 	for(std::map<std::string,TH1F*>::iterator s = fm_output_histos.begin();
// 	    s != fm_output_histos.end();s++)
// 	    std::cout << "Avail output_histo: " << s->first << std::endl;

	if(fm_output_histos.find(id) == fm_output_histos.end()){
		if(!need)
			return NULL;
		else {
			std::cout << " ERROR:FitManager, No output histo entry for " << id << ". available histos:"<< std::endl;
			for(std::map<std::string,TH1F*>::iterator h = fm_output_histos.begin();
			    h != fm_output_histos.end();h++){
				std::cout << h->first << std::endl;
			}
			exit(1);
		}
	}
	else if(fm_output_histos[id] == NULL){
		if(!need)
			return NULL;
		else{ 
			std::cout << " ERROR:FitManager, NULL output histo entry for " << id <<". available histos:"<< std::endl;
			for(std::map<std::string,TH1F*>::iterator h = fm_output_histos.begin();
			    h != fm_output_histos.end();h++){
				std::cout << h->first << std::endl;
			}
			exit(1);
		}
	}

	return fm_output_histos[id];	

}

std::map<std::string, TH1F*> FitManager::output_histos_1D()
{
	return fm_output_histos;
}

TH2F* FitManager::output_histo_2D(std::string id, bool need)
{
	if(fm_output_histos_2D.find(id) == fm_output_histos_2D.end()){
		if(!need)
			return NULL;
		else {
			std::cout << " ERROR:FitManager, No 2D output histo entry for " << id << std::endl;
			exit(1);
		}
	}
	else if(fm_output_histos_2D[id] == NULL){
		if(!need)
			return NULL;
		else{ 
			std::cout << " ERROR:FitManager, NULL 2D output histo entry for " << id << std::endl;
			exit(1);
		}
	}

	return fm_output_histos_2D[id];	

}

std::map<std::string, TH2F*> FitManager::output_histos_2D()
{
	return fm_output_histos_2D;
}

RooRealVar* FitManager::ngauss(std::string id)
{
	if(ngauss_evts.find(id) == ngauss_evts.end()){
		std::cout << " ERROR:FitManager, No ngauss entry for " << id <<".Available: " << std::endl;
		for(std::map<std::string,RooRealVar*>::iterator g = ngauss_evts.begin();
		    g != ngauss_evts.end();g++){
			std::cout << g->first << std::endl;
		}
		exit(1);
	}
	else if(ngauss_evts[id] == NULL)
		std::cout << " ERROR:FitManager, NULL ngauss entry for " << id << std::endl;

	return ngauss_evts[id];
}


int FitManager::n_pseudo_exp()
{
	return fm_n_pseudo_exp;
}
bool FitManager::run_full_sys_study()
{
	return fm_run_full_sys_study;
}
bool FitManager::run_multi_rebin_study()
{
	return fm_run_multi_rebin_study;
}

bool FitManager::use_real_data()
{
	return fm_use_real_data;
}

std::string FitManager::sys_study_name()
{
	return fm_sys_study_name;
}

std::vector<std::string> FitManager::sys_study_names()
{
	return fm_sys_study_names;
}

RooFitResult* FitManager::data_fit_res()
{
	if(fm_data_fit_res == NULL){
		std::cout << "Error: Data fit results not saved. Returning NULL " << std::endl;
		return NULL;
	}else
		return fm_data_fit_res;			

}

RooDataSet* FitManager::fit_par_data()
{
	if(fm_fit_par_data == NULL){
		std::cout << "Error: fit par data NULL " << std::endl;
		return NULL;
	}else
		return fm_fit_par_data;			

}

FitSetup* FitManager::fit_setup()
{
	return fm_fit_setup;
}

std::string FitManager::id_label(std::string id)
{
	if(fm_id_label.find(id) == fm_id_label.end())
		return id;
	else
		return fm_id_label[id];
}

std::map<std::string,std::vector<std::pair<double,double> > > FitManager::get_sys_study_results()
{
	return fm_sys_study_results;
}

std::map<std::string,std::map<std::string,double> > FitManager::get_sys_study_results2()
{
	return fm_sys_study_results2;
}


/************************** Delete ******************************/

void FitManager::delete_all(bool delete_data)
{
	std::cout << "FM: Deleting everything, ";
	if(delete_data) std::cout << " including variables for data" << std::endl;
	else std::cout << " except data variables" << std::endl;
	delete_output_histos(true);std::cout << " output deleted " << std::endl;
	delete_ngauss_evts();std::cout << " ngauss deleted " << std::endl;
	//delete_fit_setup();std::cout << " fit_setup deleted " << std::endl;//deleted in InputManager

	if(fm_data_fit_res) delete fm_data_fit_res; std::cout << " data fit res deleted " << std::endl;
}

void FitManager::delete_output_histos(bool del_sys)
{
	std::cout << "FM: Deleting output histograms" <<std::endl;
	for(std::map<std::string,TH1F*>::iterator iter = fm_output_histos.begin();iter != fm_output_histos.end();++iter){
		if(!del_sys && (iter->first).find("Sys") != std::string::npos){
			continue;
		}
		delete fm_output_histos[iter->first];
		fm_output_histos[iter->first] = NULL;
	}
	if(del_sys) fm_output_histos.clear();

	for(std::map<std::string,TH2F*>::iterator iter = fm_output_histos_2D.begin();iter != fm_output_histos_2D.end();++iter){
		delete fm_output_histos_2D[iter->first];
		fm_output_histos_2D[iter->first] = NULL;
	}
	fm_output_histos_2D.clear();
}

void FitManager::delete_ngauss_evts()
{
	for(std::map<std::string,RooRealVar*>::iterator iter = ngauss_evts.begin();iter != ngauss_evts.end();++iter){
		delete ngauss_evts[iter->first];
		ngauss_evts[iter->first] = NULL;
	}
	
	ngauss_evts.clear();
}

void FitManager::delete_fit_setup()
{
	if(fm_fit_setup){
		delete fm_fit_setup;
		fm_fit_setup = NULL;
	}
}


