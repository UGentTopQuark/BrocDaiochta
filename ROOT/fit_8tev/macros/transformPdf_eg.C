/////////////////////////////////////////////////////////////////////////
//
// 'MULTIDIMENSIONAL MODELS' RooFit tutorial macro #305
// 
// Multi-dimensional p.d.f.s with conditional p.d.fs in product
// 
// pdf = gauss(x,f(y),sx | y ) * gauss(y,ms,sx)    with f(y) = a0 + a1*y
// 
//
// 07/2008 - Wouter Verkerke 
//
/////////////////////////////////////////////////////////////////////////

#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooPolyVar.h"
#include "RooProdPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TH1.h"
#include "TH1F.h"
using namespace RooFit ;



void transformPdf_eg()
{
	RooRealVar x_1("x1","x1",2,0,2) ;
	x_1.setBins(2);

	TH1F sig_h_1 ("sig_histo", "signal_histogram", 2,0,2);
	TH1F bkg_h_1("bkg_histo", "background_histogram", 2,0,2);
	sig_h_1.SetBinContent(1,200);
 	sig_h_1.SetBinContent(2,100);
	bkg_h_1.SetBinContent(1,800);
 	bkg_h_1.SetBinContent(2,600);

	TH1F *sum_h_1 = new TH1F("sum_histo", "sum_histogram", 2,0,2);
	TH1F *diff_h_1 = new TH1F("diff_histo", "diff_histogram", 2,0,2);
	sum_h_1->SetBinContent(1,91.6);
 	sum_h_1->SetBinContent(2,108.3);
	diff_h_1->SetBinContent(1,46.6);
 	diff_h_1->SetBinContent(2,0);

	TH1F comb_h ("comb_histo", "comb_histogram", 2,0,2);
	comb_h.SetBinContent(1,300);
 	comb_h.SetBinContent(2,1400);

//         TCanvas *cz = new TCanvas("cz", "canvas", 640, 480);
//         cz->cd();
//         diff_h_1->Draw();

        TH1F *res1 = new TH1F("res1", "res1", 100,50,1500);
        TH1F *res2 = new TH1F("res2", "res2", 100,50,2500);
        TH1F *sum = new TH1F("sum", "sum", 100,-100,2000);
        TH1F *diff = new TH1F("diff", "diff", 100,-100,2500);
        TH1F *sumpdiff = new TH1F("sumpdiff", "spd", 100,50,1500);
        TH1F *summdiff = new TH1F("summdiff", "smd", 100,50,2500);
        TH1F *res3 = new TH1F("res3", "res3", 100,50,1500);
        TH1F *res4 = new TH1F("res4", "res4", 100,50,2500);
        for(int i = 0; i < 1000;i++){
		//build model1
		RooRealVar nsig1("nsig1","fitted number of signal events",300, 0, 500) ;
		RooRealVar nbkg1("nbkg1","fitted number of bkg events",1400,0,2500);
		
		RooDataHist sig1_hist("sig_hist","signal histogram ",RooArgList(x_1),&sig_h_1);
		RooDataHist bkg1_hist("bkg_hist","background histogram ",RooArgList(x_1),&bkg_h_1);
		
		RooHistPdf sig1_pdf("sig_pdf", "sig pdf" , x_1, sig1_hist);
		RooHistPdf bkg1_pdf("bkg_pdf", "bkg pdf" , x_1, bkg1_hist);
		
		RooAddPdf model1("model1","model1",RooArgList(sig1_pdf,bkg1_pdf),RooArgList(nsig1,nbkg1)) ;
		
		double nExpGen1 = model1.expectedEvents(RooArgSet(x_1));

		//	RooDataHist *datagen = model1.generateBinned(RooArgSet(x_1),nExpGen1,Extended());
		RooDataHist *datagen = model1.generateBinned(RooArgSet(x_1),nExpGen1,Extended());
		
		std::cout << "Fitting" << std::endl;
		model1.fitTo(*datagen,PrintLevel(-1));
// 		std::cout << "Printing" << std::endl;
// 		std::cout << "nsig1: " << nsig1.getVal() << std::endl;
// 		std::cout << "nbkg1: " << nbkg1.getVal() << std::endl;
// 		std::cout << "model1 tot: " << nsig1.getVal()+nbkg1.getVal() << " exp: " << nExpGen1 <<std::endl;

                res1->Fill(nsig1.getVal());
                res2->Fill(nbkg1.getVal());


		//build sum diff model
		RooRealVar nsum("nsum","fitted number of signal events",400, 0, 2500) ;
		RooRealVar ndiff("ndiff","fitted number of bkg events",400,-200,2500);
		
		RooDataHist sum_hist("sum_hist","signal histogram ",RooArgList(x_1),sum_h_1);
		RooDataHist diff_hist("diff_hist","background histogram ",RooArgList(x_1),diff_h_1);
		
		RooHistPdf sum_pdf("sum_pdf", "sig pdf" , x_1, sum_hist);
		RooHistPdf diff_pdf("diff_pdf", "bkg pdf" , x_1, diff_hist);
		
		if(i == 0){
			RooPlot* x1frame = x_1.frame() ;
			TCanvas *ca = new TCanvas("ca", "canvas", 640, 480);
			ca->cd();
			sum_pdf.plotOn(x1frame);
			x1frame->Draw();
			RooPlot* x2frame = x_1.frame() ;
			TCanvas *cb = new TCanvas("cb", "canvas", 640, 480);
			cb->cd();
			diff_pdf.plotOn(x2frame);
			x2frame->Draw();
// 			RooPlot* x3frame = x_1.frame() ;
// 			TCanvas *cc = new TCanvas("cc", "canvas", 640, 480);
// 			cc->cd();
// 			diff_hist.plotOn(x3frame);
// 			x3frame->Draw();
		}
		RooAddPdf model2("model2","model2",RooArgList(sum_pdf,diff_pdf),RooArgList(nsum,ndiff)) ;

		model2.fitTo(*datagen,PrintLevel(-1));

		sum->Fill(nsum.getVal());
		diff->Fill(ndiff.getVal());
		sumpdiff->Fill((nsum.getVal()+ndiff.getVal())*.5*(1/sig_h_1.Integral()));
		summdiff->Fill((nsum.getVal()-ndiff.getVal())*.5*(1/bkg_h_1.Integral()));

		//build combined model
		RooRealVar ncomb("ncomb","fitted number of signal events",1700, 0, 2500) ;
		
		RooDataHist comb_hist("comb_hist","signal histogram ",RooArgList(x_1),&comb_h);
		
		RooHistPdf comb_pdf("comb_pdf", "sig pdf" , x_1, comb_hist);
		
 		RooAddPdf model3("model3","model3",RooArgList(comb_pdf),RooArgList(ncomb)) ;
		
		std::cout << "Fitting" << std::endl;
		model3.fitTo(*datagen,PrintLevel(-1));
// 		std::cout << "Printing" << std::endl;
// 		std::cout << "nsig1: " << nsig1.getVal() << std::endl;
// 		std::cout << "nbkg1: " << nbkg1.getVal() << std::endl;
// 		std::cout << "model1 tot: " << nsig1.getVal()+nbkg1.getVal() << " exp: " << nExpGen1 <<std::endl;

                res3->Fill(ncomb.getVal()*(sig_h_1.Integral()/(sig_h_1.Integral()+bkg_h_1.Integral())));
                res4->Fill(ncomb.getVal()*(bkg_h_1.Integral()/(sig_h_1.Integral()+bkg_h_1.Integral())));


	}

        TCanvas *c0 = new TCanvas("c0", "canvas", 640, 480);
        c0->cd();
        sum->Draw();
        TCanvas *c1 = new TCanvas("c1", "canvas", 640, 480);
        c1->cd();
        diff->Draw();
        TCanvas *c2 = new TCanvas("c2", "canvas", 640, 480);
        c2->cd();
        sumpdiff->Draw();
        TCanvas *c3 = new TCanvas("c3", "canvas", 640, 480);
        c3->cd();
        summdiff->Draw();

        TCanvas *c4 = new TCanvas("c4", "canvas", 640, 480);
        c4->cd();
        res1->Draw();
        TCanvas *c5 = new TCanvas("c5", "canvas", 640, 480);
        c5->cd();
        res2->Draw();

        TCanvas *c6 = new TCanvas("c6", "canvas", 640, 480);
        c6->cd();
        res3->Draw();
        TCanvas *c7 = new TCanvas("c7", "canvas", 640, 480);
        c7->cd();
        res4->Draw();





//   // C r e a t e   c o n d i t i o n a l   p d f   g x ( x | y ) 
//   // -----------------------------------------------------------

//   // Create observables
//   RooRealVar x("x","x",-5,5) ;
//   RooRealVar y("y","y",-5,5) ;

//   // Create function f(y) = a0 + a1*y
//   RooRealVar a0("a0","a0",-0.5,-5,5) ;
//   RooRealVar a1("a1","a1",-0.5,-1,1) ;
//   // RooPolyVar fy("fy","fy",y,RooArgSet(a0,a1)) ;
//   RooFormulaVar fy("fy","@0/@1",RooArgList(a0,a1)) ;

//   // Create gaussx(x,f(y),sx)
//   RooRealVar sigmax("sigma","width of gaussian",0.5) ;
//   RooGaussian gaussx("gaussx","Gaussian in x with shifting mean in y",x,fy,sigmax) ;  



//   // C r e a t e   p d f   g y ( y ) 
//   // -----------------------------------------------------------

//   // Create gaussy(y,0,5)
//   RooGaussian gaussy("gaussy","Gaussian in y",y,RooConst(0),RooConst(3)) ;



//   // C r e a t e   p r o d u c t   g x ( x | y ) * g y ( y )
//   // -------------------------------------------------------

//   // Create gaussx(x,sx|y) * gaussy(y)
//   RooProdPdf model("model","gaussx(x|y)*gaussy(y)",gaussy,Conditional(gaussx,x)) ;



//   // S a m p l e ,   f i t   a n d   p l o t   p r o d u c t   p d f
//   // ---------------------------------------------------------------

//   // Generate 1000 events in x and y from model
//   RooDataSet *data = model.generate(RooArgSet(x,y),10000) ;

//   model.fitTo(*data);

//   // Plot x distribution of data and projection of model on x = Int(dy) model(x,y)
//   RooPlot* xframe = x.frame() ;
//   data->plotOn(xframe) ;
//   model.plotOn(xframe) ; 

//   // Plot x distribution of data and projection of model on y = Int(dx) model(x,y)
//   RooPlot* yframe = y.frame() ;
//   data->plotOn(yframe) ;
//   model.plotOn(yframe) ; 

//   // Make two-dimensional plot in x vs y
//   TH1* hh_model = model.createHistogram("hh_model",x,Binning(50),YVar(y,Binning(50))) ;
//   hh_model->SetLineColor(kBlue) ;



//   // Make canvas and draw RooPlots
//   TCanvas *c = new TCanvas("rf305_condcorrprod","rf05_condcorrprod",1200, 400);
//   c->Divide(3);
//   c->cd(1) ; gPad->SetLeftMargin(0.15) ; xframe->GetYaxis()->SetTitleOffset(1.6) ; xframe->Draw() ;
//   c->cd(2) ; gPad->SetLeftMargin(0.15) ; yframe->GetYaxis()->SetTitleOffset(1.6) ; yframe->Draw() ;
//   c->cd(3) ; gPad->SetLeftMargin(0.20) ; hh_model->GetZaxis()->SetTitleOffset(2.5) ; hh_model->Draw("surf") ;

}



