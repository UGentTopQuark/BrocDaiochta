#include "FitPlots.h"


using namespace RooFit;

FitPlots::FitPlots(FitManager* fm)
{
	this->fm = fm;
	//	setTDRStyle();
	theStyle = NULL;
}
FitPlots::~FitPlots()
{
	for(std::map<std::string,CanvasHolder*>::iterator c = choldersDNR.begin();
	    c != choldersDNR.end(); c++){
		delete c->second;
		c->second = NULL;
	}
	choldersDNR.clear();
	if(theStyle){
		delete theStyle;
		theStyle = NULL;
	}
}

void FitPlots::fit_nevts_gaussian(bool plot_gauss)
{
	std::cout << "FP: Fitting N events gaussian" << std::endl;
	outfile = fm->outfile();

	/**************************************
          Apply Gaussian fits to N Events distributions
          to get mean and sigma
	 *************************************/
	//Add name to plots so can output for each sys study.
	std::string sys_name = fm->sys_study_name();
	fm->initialise_ngauss_evts();

	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();
	std::cout << "FP: N events gaussian for: " << fit_id << " " << sys_name << std::endl;
	std::vector<std::string> templates_n = fs->template_norm_names();
	std::cout << "N templates: " << templates_n.size() << std::endl;
	for(std::vector<std::string>::iterator id = templates_n.begin();
	    id != templates_n.end();id++){
		
		std::cout << "Fitting gaussian to " << *id << std::endl;
		TH1F* Nevents_h =dynamic_cast<TH1F*> (fm->output_histo("nfit_evts_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
		
		//Initialise Variables for fitting gaussian to Nevents
		RooRealVar gnevts("gnevts",("Number of "+fm->id_label(*id)+" events").c_str(),fs->range_low(*id),fs->range_high(*id));
		RooGaussian gauss("gauss","gaussian pdf", gnevts,*(fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)),*(fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)));
		
		RooDataHist* Nevents_dhist = new RooDataHist("nevts_dhist","DataHist for fitted Nevents",RooArgList(gnevts),Nevents_h); 
		std::cout << "NGauss before fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
		
		gauss.fitTo(*Nevents_dhist,RooFit::PrintLevel(-1)) ;
		
		if(plot_gauss){
			std::cout << "Plotting for " << *id << std::endl;
			set_default_style();
			TCanvas *c_g = new TCanvas(("NFitted_"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
			set_canvas_style(c_g);
			
			RooPlot* frame_g = gnevts.frame(Name("NEvents"),Title("Gaussian"));
			Nevents_dhist->plotOn(frame_g);
			gauss.plotOn(frame_g);
			gauss.paramOn(frame_g,Layout(0.55,0.97,0.92),Format("NEU",AutoPrecision(0)));
			int y_max = Nevents_h->GetMaximum();
			frame_g->SetAxisRange(0,y_max*1.3,"Y");
			c_g->cd(); frame_g->Draw();
			c_g->SaveAs((fm->output_dir()+"Gaussian_nevts_"+*id+"_"+fit_id+"_"+sys_name+".pdf").c_str());
			c_g->Write();
			delete c_g;
			delete frame_g;
		}
		delete Nevents_h;
		delete Nevents_dhist;
		std::cout << "NGauss after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
	
	}

	delete_canvas_holders();
}

void FitPlots::plot_fit_error_gaussian(bool plot_gauss)
{
	outfile = fm->outfile();
	std::string sys_name = fm->sys_study_name();
	std::cout << "*********************** Fit Error ********************** " << std::endl;;
	
	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();
	std::vector<std::string> templates_n = fs->template_norm_names();
	for(std::vector<std::string>::iterator id = templates_n.begin();
	    id != templates_n.end();id++){
		std::cout << "Fitting gaussian to error distribution for: " << *id << std::endl;
		
		std::string plot_id = "nfit_err_"+*id+"_"+fit_id+"_"+sys_name;
		
		RooAbsArg *err_var = (RooAbsArg*) fs->nfit(*id,true)->errorVar();
		RooAbsRealLValue *err_var2 = (RooAbsRealLValue*) fs->nfit(*id,true)->errorVar();
		const RooAbsBinning *binning  = fs->nfit(*id,true)->errorVar()->getBinningPtr("0");
		if(!binning)
			std::cout << "NULL binning " << std::endl;
		else
			std::cout << " not null binning " << std::endl;
		std::cout << "low: " << binning->lowBound() << " high " << binning->highBound() << std::endl;
		TH1F* FitErr = new TH1F(plot_id.c_str(),plot_id.c_str(),1000,0,fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()*2);
		std::cout << *id << " Integral: " << FitErr->Integral() << std::endl;
		fm->fit_par_data()->fillHistogram(FitErr,RooArgList(*err_var));
		std::cout << *id << " Integral2: " << FitErr->Integral() << std::endl;
		
		RooRealVar gerr("gerr",("Fit error on number of "+fm->id_label(*id)+" events").c_str(),(FitErr->GetMean(1)-FitErr->GetRMS(1)*4),(FitErr->GetMean(1)+FitErr->GetRMS(1)*4));
		RooRealVar gerr_mean("mean","Mean of Error",FitErr->GetMean(1),FitErr->GetMean(1)-FitErr->GetRMS(1)/2,FitErr->GetMean(1)+FitErr->GetRMS(1)/2);
		RooRealVar gerr_sigma("sigma","Sigma of Error",FitErr->GetRMS(1),0,FitErr->GetRMS(1)*2);

		std::cout << *id <<  " RMS: " << FitErr->GetRMS(1) << std::endl;
		
		RooGaussian err_gauss("err_gauss","gaussian pdf", gerr,gerr_mean,gerr_sigma);
		RooDataHist* FitErr_dhist = new RooDataHist("err_dhist","DataHist for Error from Fit",RooArgList(gerr),FitErr);
		if(plot_gauss){
			err_gauss.fitTo(*FitErr_dhist,RooFit::PrintLevel(-1));
		}
		TCanvas *c_g_e = new TCanvas(("FitError"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
		RooPlot* frame_g_err = gerr.frame(Name("Error"),Title("Gaussian"));
		FitErr_dhist->plotOn(frame_g_err);
		if(plot_gauss){
			err_gauss.plotOn(frame_g_err);
			err_gauss.paramOn(frame_g_err,Layout(0.55,0.97,0.92),Format("NEU",AutoPrecision(0)));
		}
		int y_max = FitErr->GetMaximum();
		frame_g_err->SetAxisRange(0,y_max*1.4,"Y");
		c_g_e->cd(); frame_g_err->Draw();
		c_g_e->SaveAs((fm->output_dir()+"Gaussian_error_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
		c_g_e->Write();
		delete FitErr;
		delete FitErr_dhist;
		delete c_g_e;
		delete frame_g_err;	
		
	}
	
	delete_canvas_holders();
}

void FitPlots::plot_fit_pull_gaussian()
{
	outfile = fm->outfile();
	std::string sys_name = fm->sys_study_name();
	
	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();
	std::vector<std::string> templates_n = fs->template_norm_names();
	for(std::vector<std::string>::iterator id = templates_n.begin();
	    id != templates_n.end();id++){
		std::cout << "Fitting gaussian to pull distribution for: " << *id << std::endl;
		
		TH1F* FitPull = dynamic_cast<TH1F*> (fm->output_histo("nfit_pull_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
		RooRealVar gpull("gpull",(fm->id_label(*id)+ " pull").c_str(),-4,4);
		RooRealVar gpull_mean("mean","Mean of Pull",0,-4,4);
		RooRealVar gpull_sigma("sigma","Sigma of Pull",1,0,5);
		
		RooGaussian pull_gauss("pull_gauss","gaussian pdf", gpull,gpull_mean,gpull_sigma);
		RooDataHist* FitPull_dhist = new RooDataHist("pull_dhist","DataHist for Pull from Fit",RooArgList(gpull),FitPull);
		
		pull_gauss.fitTo(*FitPull_dhist,RooFit::PrintLevel(-1));
		
		TCanvas *c_g_p = new TCanvas(("FitPull"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
		RooPlot* frame_g_pull = gpull.frame(Name("Pull"),Title("Gaussian"));
		FitPull_dhist->plotOn(frame_g_pull);
		pull_gauss.plotOn(frame_g_pull);
		pull_gauss.paramOn(frame_g_pull,Layout(0.57,0.97,0.92),Format("NEU",AutoPrecision(0)));
		int y_max = FitPull->GetMaximum();
		frame_g_pull->SetAxisRange(0,y_max*1.3,"Y");
		c_g_p->cd(); frame_g_pull->Draw();
		c_g_p->SaveAs((fm->output_dir()+"Gaussian_pull_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
		c_g_p->Write();
		
		delete FitPull;
		delete FitPull_dhist;
		delete c_g_p;
		delete frame_g_pull;
		
		std::cout << "NGauss1.2 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
		
	}

	delete_canvas_holders();
}

void FitPlots::plot_constraint_mean()
{
	outfile = fm->outfile();
	std::cout << "FP: plot constraint mean" << std::endl;
	std::string sys_name = fm->sys_study_name();
	
	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();
	std::vector<std::string> constraints_used = fs->gauss_constraints_used();
	for(std::vector<std::string>::iterator c = constraints_used.begin();
	    c != constraints_used.end();c++){
		
		std::cout << "Fitting gaussian to constraint mean distribution for: " << *c << std::endl;
		
		TH1F* CMean = dynamic_cast<TH1F*> (fm->output_histo("constraint_mean_"+*c+"_"+fit_id+"_"+sys_name)->Clone());
		RooRealVar gcmean("gcmean",("sampled constraint mean for "+*c+"_"+fit_id+"_"+sys_name).c_str(),fs->range_low(*c),fs->range_high(*c));
		RooRealVar gcmean_mean("mean","Mean of Cmean",0,fs->range_low(*c),fs->range_high(*c));
		RooRealVar gcmean_sigma("sigma","Sigma of Cmean",fs->constraint_var(*c+"_sigma"),0,fs->constraint_var(*c+"_sigma")*2);
		
		RooGaussian cmean_gauss("cmean_gauss","gaussian pdf", gcmean,gcmean_mean,gcmean_sigma);
		RooDataHist* CMean_dhist = new RooDataHist("cmean_dhist","DataHist for Constriant mean",RooArgList(gcmean),CMean);
		
		cmean_gauss.fitTo(*CMean_dhist,RooFit::PrintLevel(-1));
		
		TCanvas *c_g_p = new TCanvas(("Cmean"+*c+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
		RooPlot* frame_g_cmean = gcmean.frame(Name("Cmean"),Title("Gaussian"));
		CMean_dhist->plotOn(frame_g_cmean);
		cmean_gauss.plotOn(frame_g_cmean);
		cmean_gauss.paramOn(frame_g_cmean,Layout(0.55,0.97,0.92),Format("NEU",AutoPrecision(0)));
		c_g_p->cd(); frame_g_cmean->Draw();
		c_g_p->SaveAs((fm->output_dir()+"Gaussian_constriant_mean_"+*c+"_"+fit_id+sys_name+".pdf").c_str());
		c_g_p->Write();
		
		delete CMean;
		delete CMean_dhist;
		delete c_g_p;
		delete frame_g_cmean;
		
		RooRealVar gcmean2("gcmean2",("Nfit events for "+*c+"_"+fit_id+"_"+sys_name).c_str(),fs->range_low(*c),fs->range_high(*c));
		RooGaussian cmean_gauss2("cmean_gauss","gaussian pdf", gcmean2,gcmean_mean,gcmean_sigma);
		
		CMean = dynamic_cast<TH1F*> (fm->output_histo("constraint_evts_"+*c+"_"+fit_id+"_"+sys_name)->Clone());
		CMean_dhist = new RooDataHist("cmean_dhist","DataHist for Constriant mean",RooArgList(gcmean2),CMean);
		
		cmean_gauss2.fitTo(*CMean_dhist,RooFit::PrintLevel(-1));
		
		c_g_p = new TCanvas(("Cmean"+*c+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
		frame_g_cmean = gcmean2.frame(Name("Cmean"),Title("Gaussian"));
		CMean_dhist->plotOn(frame_g_cmean);
		cmean_gauss2.plotOn(frame_g_cmean);
		cmean_gauss2.paramOn(frame_g_cmean,Layout(0.55,0.97,0.92),Format("NEU",AutoPrecision(0)));
		c_g_p->cd(); frame_g_cmean->Draw();
		c_g_p->SaveAs((fm->output_dir()+"Gaussian_constriant_evts_"+*c+"_"+fit_id+sys_name+".pdf").c_str());
		c_g_p->Write();
		
		delete CMean;
		delete CMean_dhist;
		delete c_g_p;
		delete frame_g_cmean;
		
		double exp_range = (fs->constraint_var(*c+"_sigma")/fs->constraint_var(*c+"_mean"))*10;
		RooRealVar gcmean3("gcmean3",("(Sampled mean - Nfit events)/Sampled mean for "+*c+"_"+fit_id+"_"+sys_name).c_str(),-exp_range,exp_range);
		RooRealVar gcmean_mean3("mean","Mean of Cmean",0,-exp_range/10,+exp_range/10);
		RooRealVar gcmean_sigma3("sigma","Sigma of Cmean",exp_range/30,0,exp_range/5);
		RooGaussian cmean_gauss3("cmean_gauss3","gaussian pdf", gcmean3,gcmean_mean3,gcmean_sigma3);
		
		CMean = dynamic_cast<TH1F*> (fm->output_histo("diff_constraint_mean_evts_"+*c+"_"+fit_id+"_"+sys_name)->Clone());
		CMean_dhist = new RooDataHist("cmean_dhist","DataHist for Constriant mean",RooArgList(gcmean3),CMean);
		
		c_g_p = new TCanvas(("Diff_Cmean_nevts_"+*c+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
		frame_g_cmean = gcmean3.frame(Name("Cmean"),Title("Gaussian"));
		CMean_dhist->plotOn(frame_g_cmean);

		c_g_p->cd(); frame_g_cmean->Draw();
		c_g_p->SaveAs((fm->output_dir()+"Gaussian_diff_constriant_mean_evts_"+*c+"_"+fit_id+sys_name+".pdf").c_str());
		c_g_p->Write();
		
		delete CMean;
		delete CMean_dhist;
		delete c_g_p;
		delete frame_g_cmean;
		
	}
	delete_canvas_holders();
}
void FitPlots::plot_2D_histos()
{
	outfile = fm->outfile();
	std::cout << "FP: plot 2D histos" << std::endl;
	std::string sys_name = fm->sys_study_name();
	std::map<std::string, TH2F*> histos = fm->output_histos_2D();
	for(std::map<std::string,TH2F*>::iterator h = histos.begin();
	    h != histos.end(); h++){

		std::string name = h->first;
		cholders[name+sys_name] = new CanvasHolder();
		cholders[name+sys_name]->setCanvasTitle(fm->output_dir()+"_"+name+"_"+sys_name);
		cholders[name+sys_name]->addHisto2D(h->second,"");
		cholders[name+sys_name]->setXaxisOff(1);
		cholders[name+sys_name]->setYaxisOff(1);
      
		cholders[name+sys_name]->save("pdf");
		delete_canvas_holders();
		
	}
}


void FitPlots::plot_print_correlation()
{
	std::cout << "FP: Plot Correlations" << std::endl;
	std::string sys_name = fm->sys_study_name();
	
	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();
	
	std::vector<std::string> templates_n = fs->template_norm_names();
	std::map<std::string, TH2F*> corr_plots;
	//Scatter plots
	for(size_t i = 0; i != templates_n.size();i++){
		for(size_t j = 0; j != templates_n.size();j++){
			if(i != j){
				std::string plot_id = "N"+templates_n[i]+"_vs_N"+templates_n[j]+"_"+fit_id+"_"+sys_name;
				TCanvas *c_corr1 = new TCanvas(plot_id.c_str(), "canvas", 640, 480);
				corr_plots[plot_id] = dynamic_cast<TH2F*> (fs->nfit(templates_n[i],true)->createHistogram(("nevts_"+templates_n[i]+"_vs_nevts_"+templates_n[j]).c_str(),*(fs->nfit(templates_n[j],true))));
				fm->fit_par_data()->fillHistogram(corr_plots[plot_id],RooArgList(*(fs->nfit(templates_n[i],true)),*(fs->nfit(templates_n[j],true))));
			}
		}
	}
	delete_canvas_holders();
}

template <>
void FitPlots::plot_sample_event<TH2D>(HistoHandler<TH2D> *hh, bool is_real_data)
{ 
	std::cout << "FitPlots: plot_sample_event not set up for 2D fit" << std::endl;

	//Below all the required information is obtained however nothing is output.
	// It is up to you to display the information as you see fit.

	//Add name to plots so can output for each sys study.
	std::string sys_name = fm->sys_study_name();

	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();
	std::vector<std::string> templates_n = fs->template_norm_names();
	std::vector<std::string> templates_s = fs->template_shape_names();
	
	//This is the input data histogram either for real of pseudo-data
	TH2D* realdata = NULL;
	TH2F* gendata = NULL;
	if(hh->histo("data",false) != NULL){
		
		realdata = dynamic_cast<TH2D*> (hh->histo("data",true)->Clone());
		std::cout << "FP: data integral " << realdata->Integral() << std::endl;
	}else{
		gendata = dynamic_cast<TH2F*> (fm->output_histo_2D("gen_data_"+fit_id+"_"+sys_name)->Clone());
		std::cout << "FP: pseudo-data example integral " << gendata->Integral() << std::endl;
	}

	int i = 0;
	//Loop over each of the input templates
	for(std::vector<std::string>::iterator id = templates_n.begin();
	    id != templates_n.end();id++){
		//Get the number of events resulting from the fit for each template.
		//When fitting to real data nevents is taken directly from the fit results.
		//When fitting to pseudodata nevents is taken from the mean of the gaussian fit to the nevents distribution
		Double_t nevts_0 = 0;
		if(!is_real_data){
			nevts_0 = fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal();
		}else{
			nevts_0 = fs->nfit(*id)->getVal();
		}
		
		//Get the template shape
		RooRealVar tmp_x(*fs->observables()[fs->observable_id("x")]);
		RooRealVar tmp_y(*fs->observables()[fs->observable_id("y")]);
		TH2F* h_template = dynamic_cast<TH2F*> (fs->pdf(templates_s[i])->createHistogram(("template_"+*id).c_str(),tmp_x,YVar(tmp_y)));
		i++;
		delete h_template;
	}

	//Now you have all the info needed to scale templates to fit results and overlay with data.


}

template <>
void FitPlots::plot_sample_event<TH1D>(HistoHandler<TH1D> *hh, bool is_real_data)
{ 
	outfile = fm->outfile();
	if(verbose) std::cout << " FitPlots:plot_sample_event" << std::endl;

	/****************************************************************
	  Output: Plot PDF and pseudo data from sample 1 toyMC
// 	***************************************************************/
	std::vector<int> *colours = new std::vector<int>();
	colours->push_back(kRed);
	colours->push_back(kBlue);
	colours->push_back(kGreen);
	colours->push_back(kBlack);
	colours->push_back(kGray+2);

	std::vector<int> *styles = new std::vector<int>();
	styles->push_back(2);
	styles->push_back(9);
	styles->push_back(7);
	styles->push_back(1);
	styles->push_back(1);

	//Add name to plots so can output for each sys study.
	std::string sys_name = fm->sys_study_name();

	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();
	std::vector<std::string> templates_n = fs->template_norm_names();
	std::vector<std::string> templates_s = fs->template_shape_names();
	
	std::string input_templates_pid = "input_templates_"+fit_id+"_"+sys_name;
	std::string gendata_pid = "gendata_"+fit_id+"_"+sys_name;
	std::string template_data_pid = "templates_data_"+fit_id+"_"+sys_name;
	
	cholders[input_templates_pid] = new CanvasHolder();
	cholders[gendata_pid] = new CanvasHolder();
	cholders[template_data_pid] = new CanvasHolder();
	cholders[input_templates_pid]->setCanvasTitle(fm->output_dir()+input_templates_pid);
	cholders[gendata_pid]->setCanvasTitle(fm->output_dir()+gendata_pid);
	cholders[template_data_pid]->setCanvasTitle(fm->output_dir()+template_data_pid);
	
	cholders[input_templates_pid]->setOptStat(000000);
	cholders[gendata_pid]->setOptStat(000000);
	cholders[template_data_pid]->setOptStat(000000);
	
	TH1D* realdata = NULL;
	TH1F* gendata = NULL;
	if(hh->histo("data",false) != NULL){
		
		realdata = dynamic_cast<TH1D*> (hh->histo("data",true)->Clone());
		std::cout << "FP: data integral " << realdata->Integral() << std::endl;
	}else{
		gendata = dynamic_cast<TH1F*> (fm->output_histo("gen_data_"+fit_id+"_"+sys_name)->Clone());
		std::cout << "FP: pseudo-data example integral " << gendata->Integral() << std::endl;
	}
	
	std::cout << "Plot eg: " << fit_id << " " << sys_name << std::endl;
	int i = 0;
	for(std::vector<std::string>::iterator id = templates_n.begin();
	    id != templates_n.end();id++){
		//std::cout << "NGauss3 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
		//Get Fitted Nevents for scaling the p.d.f's
		Double_t nevts_0 = 0;
		if(!is_real_data){
			nevts_0 = fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal();
		}else{
			nevts_0 = fs->nfit(*id)->getVal();
		}
		RooRealVar tmp_x(*fs->observables()[fs->observable_id("x")]);
		TH1F* h_template = dynamic_cast<TH1F*> (fs->pdf(templates_s[i])->createHistogram(("template_"+*id).c_str(),tmp_x));
		// h_template->GetXaxis()->SetBinLabel(1,"0");
		// h_template->GetXaxis()->SetBinLabel(17,"500");
		// h_template->GetXaxis()->SetBinLabel(33,"1000");
		// h_template->GetXaxis()->SetBinLabel(47,"0");
		// h_template->GetXaxis()->SetBinLabel(63,"500");
		// h_template->GetXaxis()->SetBinLabel(80,"1000");
		// h_template->LabelsOption("h","X");
		//Overlay p.d.f's to show input
		cholders[input_templates_pid]->addHisto(h_template,fm->id_label(*id));
		if(sys_name != ""){//< template overlays for sys study
			std::vector<std::string> templates_nom = fm->nominal_template_names();
			std::string sys_templates_pid = "Sys_templates_"+templates_nom[i];
			if(choldersDNR.find(sys_templates_pid) == choldersDNR.end()){
				choldersDNR[sys_templates_pid] = new CanvasHolder();
				choldersDNR[sys_templates_pid]->setCanvasTitle(fm->output_dir()+sys_templates_pid);
				choldersDNR[sys_templates_pid]->setOptStat(000000);
				choldersDNR[sys_templates_pid]->setLegTitle(fm->id_label(templates_nom[i]));
			}
			std::string sys_name_label = sys_name;
			if(sys_name == "none") sys_name_label = "Nominal";
			choldersDNR[sys_templates_pid]->addHisto(h_template,fm->id_label(sys_name_label));
			//choldersDNR[sys_templates_pid]->addHisto(h_template,"");
			
		}
		h_template->Scale(nevts_0);
		cholders[template_data_pid]->addHistoStacked(h_template,fm->id_label(*id));
		
		std::cout << *id << " nscaled: " << fs->nscaled(*id) << " nfit: " <<  fm->output_histo("nfit_evts_"+*id+"_"+fit_id+"_"+sys_name)->GetMean() << " nfit gauss: " << nevts_0 << std::endl;
		i++;
		delete h_template;
		
	}		
	std::string x_id = fm->id_label(fs->observable_id("x"));
	
	cholders[template_data_pid]->set_eff_histos(true);
	if(realdata != 0){
		cholders[gendata_pid]->addHisto(realdata,"Data","Ep");
		cholders[template_data_pid]->addHisto(realdata, "Data","Ep");
	}else{
		cholders[gendata_pid]->addHisto(gendata,"Pseudo-data","Ep");
		cholders[template_data_pid]->addHisto(gendata, "Pseudo-data","Ep");
	}
	
	cholders[input_templates_pid]->setLineColors(*colours);
	cholders[template_data_pid]->setLineColors(*colours);
	cholders[input_templates_pid]->setLineStyles(*styles);
	cholders[template_data_pid]->setLineStyles(*styles);
	
	cholders[template_data_pid]->setTitleX(x_id);
	cholders[template_data_pid]->setTitleY("Events/bin");
	cholders[template_data_pid]->setXaxisOff(1.1);
	cholders[template_data_pid]->setYaxisOff(2.0);
	//cholders[template_data_pid]->addVLine(1410,"",6,1,1);
	cholders[template_data_pid]->setLegendOptions(.3,.2);
	
	
	cholders[input_templates_pid]->setLegDrawSymbol("lp");
	cholders[input_templates_pid]->setTitleX(x_id);
	cholders[input_templates_pid]->setTitleY("");
	cholders[input_templates_pid]->setXaxisOff(1.1);
	cholders[input_templates_pid]->setYaxisOff(1.3);
	cholders[input_templates_pid]->setLegendOptions(.3,.2);
	
	cholders[input_templates_pid]->save("pdf");
	cholders[template_data_pid]->save("pdf");
	cholders[gendata_pid]->save("pdf");
	
	if(realdata)delete realdata;
	if(gendata)delete gendata;

	
	delete colours;
	delete styles;
	delete_canvas_holders();
}
void FitPlots::plot_rel_precision_gaussian()
{
	outfile = fm->outfile();

	/**************************************
          Apply Gaussian fits to N Events distributions
          to get mean and sigma
	 *************************************/
	std::cout << "Fitting Relative Precision" << std::endl;
	//Add name to plots so can output for each sys study.
	std::string sys_name = fm->sys_study_name();

	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();
	std::vector<std::string> templates_n = fs->template_norm_names();
	std::cout << "N templates: " << templates_n.size() << std::endl;
	for(std::vector<std::string>::iterator id = templates_n.begin();
	    id != templates_n.end();id++){
		std::cout << "Fitting rel precision for " << *id << std::endl;
		
		std::cout << "NGauss2 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
		if(id->find("tt_total") != std::string::npos){
			
			TH1F* Rel_pre_h =dynamic_cast<TH1F*> (fm->output_histo("rel_precision_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
			
			//Initialise Variables for fitting gaussian to Nevents
			RooRealVar grp("gnevts",("Rel precision for "+fm->id_label(*id)).c_str(),-1,1);
			RooRealVar grp_mean("mean","Mean of Error",Rel_pre_h->GetMean(1),Rel_pre_h->GetMean(1)-Rel_pre_h->GetRMS(1)/2,Rel_pre_h->GetMean(1)+Rel_pre_h->GetRMS(1)/2);
			RooRealVar grp_sigma("sigma","Sigma of Error",Rel_pre_h->GetRMS(1),0,Rel_pre_h->GetRMS(1)*2);
			
			RooGaussian rpgauss("rpgauss","gaussian pdf", grp,grp_mean,grp_sigma);
			
			RooDataHist* rp_dhist = new RooDataHist("nrp_dhist","DataHist for rp",RooArgList(grp),Rel_pre_h); 
			
			rpgauss.fitTo(*rp_dhist,RooFit::PrintLevel(-1)) ;
			
			std::cout << "Plotting for " << *id << std::endl;
			TCanvas *c_g = new TCanvas(("Rel_precision_"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
			RooPlot* frame_g = grp.frame(Name("Rel precision"),Title("Gaussian"));
			rp_dhist->plotOn(frame_g);
			rpgauss.plotOn(frame_g);
			rpgauss.paramOn(frame_g,Layout(0.55,0.97,0.92),Format("NEU",AutoPrecision(0)));
			int y_max = Rel_pre_h->GetMaximum();
			frame_g->SetAxisRange(0,y_max*1.3,"Y");
			c_g->cd(); frame_g->Draw();
			c_g->SaveAs((fm->output_dir()+"Relative_precision_"+*id+"_"+fit_id+"_"+sys_name+".pdf").c_str());
			c_g->Write();
			delete c_g;
			delete frame_g;
			delete Rel_pre_h;
			delete rp_dhist;
			
			std::cout << "Fitting  cs err for " << *id << std::endl;
			TH1F* cs_err_h =dynamic_cast<TH1F*> (fm->output_histo("sigma_err_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
			std::cout << "NGauss2.1 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
			
			//Initialise Variables for fitting gaussian to Nevents
			double ntt = fs->nscaled("tt_total");
			double sqrtntt = sqrt(fs->nscaled("tt_total"));
			double cs = 157.5;
			double stat_err_cs = 3*(sqrtntt/ntt)*cs;
			double hmean = cs_err_h->GetMean(1);
			double hrms = cs_err_h->GetRMS(1);
			RooRealVar cs_err("gnevts",("CS error for "+fm->id_label(*id)).c_str(),100,hmean-(hrms*3),hmean+(hrms*3));
			RooRealVar cs_err_mean("mean","Mean of Error",hmean,hmean-hrms/2,hmean+hrms/2);
			RooRealVar cs_err_sigma("sigma","Sigma of Error",hrms,0,hrms*2);
			
			RooGaussian *cs_errgauss = new RooGaussian("cs_errgauss","gaussian pdf", cs_err,cs_err_mean,cs_err_sigma);
			
			RooDataHist* cs_err_dhist = new RooDataHist("ncs_err_dhist","DataHist for cs_err",RooArgList(cs_err),cs_err_h); 
			
			//FIXME: causing crash in smear sys study.
			//cs_errgauss->fitTo(*cs_err_dhist,RooFit::PrintLevel(-1)) ;
			
			//				std::cout << "cs_errgauss: " <<  cs_errgauss<< std::endl;
			std::cout << "cs_err_dhist: " << cs_err_dhist << std::endl;
			std::cout << "cs_err_h: " << cs_err_h << std::endl;
			
			std::cout << "NGauss2.2 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
			std::cout << "Plotting for " << *id << std::endl;
			c_g = new TCanvas(("Rel_precision_"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
			frame_g = cs_err.frame(Name("Rel precision"),Title("Gaussian"));
			cs_err_dhist->plotOn(frame_g);
			//cs_errgauss.plotOn(frame_g);
			//cs_errgauss.paramOn(frame_g,Layout(0.55,0.97,0.92),Format("NEU",AutoPrecision(0)));
			y_max = cs_err_h->GetMaximum();
			frame_g->SetAxisRange(0,y_max*1.5,"Y");
			////
			////			//frame_g->SetAxisRange(6,8,"X"); // For 1085/pb
			c_g->cd(); frame_g->Draw();
			c_g->SaveAs((fm->output_dir()+"CS_fit_err_"+*id+"_"+fit_id+"_"+sys_name+".pdf").c_str());
			c_g->Write();
			delete c_g;
			delete frame_g;
			delete cs_err_h;
			delete cs_err_dhist;
			delete cs_errgauss;
			////		std::cout << "NGauss2.3 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
		}
		
	}
	
 	delete_canvas_holders();
}

 void FitPlots::plot_fit_checks()
  {
 	std::string sys_name = fm->sys_study_name();

	  std::cout << " Plotting ngenall events " << std::endl;
	TH1* h_genall = fm->fit_par_data()->createHistogram("ngenall",-40); 
	//h_genall->SetAxisRange(0.0,200.0,"Y");
	TCanvas *c_genall = new TCanvas("NGenall", "canvas",800,800);
	c_genall->cd();h_genall->Draw();
	c_genall->SaveAs((fm->output_dir()+"NGeneratedAll_"+sys_name+".pdf").c_str());
	c_genall->Write();
	delete h_genall;

}

void FitPlots::plot_numbers_vs_sys()
{ 
	outfile = fm->outfile();
	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();

	
	std::cout << "Plotting sys results" << std::endl;
	std::vector<std::string> templates = fs->template_norm_names();
        std::map<std::string,std::vector<std::pair<double,double> > > sys_study_results = fm->get_sys_study_results();
	std::vector<std::string> study_names = fm->sys_study_names();//give you the order in which the studies were input.

	cholders["template_histos"] = new CanvasHolder();
	cholders["template_histos"]->setCanvasTitle(fm->output_dir()+"template_histos");


	std::cout << "getting sys histos from fm " << std::endl;
	std::map<std::string, TH1F*> histos = fm->output_histos_1D();

	for(std::map<std::string, TH1F*>::iterator h = histos.begin();
	    h!= histos.end();h++){
		if((h->first).find("Sys_") == std::string::npos){
			continue;
		}
		cholders[h->first] = new CanvasHolder();
		cholders[h->first]->setCanvasTitle(fm->output_dir()+h->first);
		h->second->LabelsDeflate("X");
		cholders[h->first]->addHisto(h->second,"","PE");
		cholders[h->first]->setOptStat(000000);
		//	cholders["Sys_nevents_"+*id]->setBordersX(0,study_names.size()+4);
		cholders[h->first]->save("pdf");
	}

	delete_canvas_holders();

			
}

void FitPlots::plot_numbers_vs_bin_width()
{ 
	outfile = fm->outfile();
	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();

	std::cout << "Plotting Rebin study results" << std::endl;
	std::vector<std::string> templates = fs->template_norm_names();
        std::map<std::string,std::vector<std::pair<double,double> > > sys_study_results = fm->get_sys_study_results();
	std::vector<std::string> study_names = fm->sys_study_names();//give you the order in which the studies were input.

        std::map<std::string,std::map<std::string,double> > sys_study_results2 = fm->get_sys_study_results2();

	double max_x = -1;
	double max_y = -1;

	for(size_t k = 0;k < templates.size();k++){
		std::map<std::string,double> nevts_res = sys_study_results2["Nevts_"+templates[k]];

		const int nbins = nevts_res.size();
		double y_axis[nbins];
		double y_ERR[nbins];
		double x_axis[nbins];
		double x_ERR[nbins];
		double ye_axis[nbins];
		double ye_ERR[nbins];
		double yre_axis[nbins];
		double yre_ERR[nbins];
		double x2_axis[nbins];
		double ycs_axis[nbins];
		double ycs_ERR[nbins];
		double ycse_axis[nbins];
		double ycse_ERR[nbins];
		double x_label_axis[nbins];

		int l = 0;
		for(std::map<std::string,double>::iterator study = nevts_res.begin();
		study != nevts_res.end();study++){
				x_axis[l] = sys_study_results2["BinWidth_"+templates[k]][study->first];
				x_ERR[l] = 0;
				y_axis[l] = sys_study_results2["Nevts_"+templates[k]][study->first];
				y_ERR[l] = sys_study_results2["Err_"+templates[k]][study->first];
				
				ye_axis[l] = sys_study_results2["Err_"+templates[k]][study->first];
				ye_ERR[l] = sys_study_results2["ErrErr_"+templates[k]][study->first];
				
				yre_axis[l] = sys_study_results2["RelErr_"+templates[k]][study->first];
				yre_ERR[l] = sys_study_results2["RelErrErr_"+templates[k]][study->first];

				ycs_axis[l] = sys_study_results2["CS_"+templates[k]][study->first];
				ycs_ERR[l] = sys_study_results2["CSErr_"+templates[k]][study->first];
				
				ycse_axis[l] = sys_study_results2["Errpb_"+templates[k]][study->first];
				ycse_ERR[l] = sys_study_results2["ErrErrpb_"+templates[k]][study->first];

				x_label_axis[l] = l;

				l++;
		}
		cholders["Sys2_nevents_"+templates[k]] = new CanvasHolder();
		cholders["Sys2_err_"+templates[k]] = new CanvasHolder();
		cholders["Sys2_relerr_"+templates[k]] = new CanvasHolder();
		cholders["Sys2_cs_"+templates[k]] = new CanvasHolder();
		cholders["Sys2_cserr_"+templates[k]] = new CanvasHolder();
		std::string drawopt = "p";
		
		//		ch.setTitle("");
// 		che.setTitle("");
// 		chre.setTitle("");
		
		cholders["Sys2_nevents_"+templates[k]]->setTitleX("Bin Width");
		cholders["Sys2_err_"+templates[k]]->setTitleX("Bin Width");
		cholders["Sys2_relerr_"+templates[k]]->setTitleX("Bin Width");
		cholders["Sys2_cs_"+templates[k]]->setTitleX("Bin Width");
		cholders["Sys2_cserr_"+templates[k]]->setTitleX("Bin Width");
		
		cholders["Sys2_nevents_"+templates[k]]->setTitleY(("Number of "+fm->id_label(templates[k])+" events").c_str());
		cholders["Sys2_err_"+templates[k]]->setTitleY(("Error on N "+fm->id_label(templates[k])+" events").c_str());
		cholders["Sys2_relerr_"+templates[k]]->setTitleY(("Rel. error on N "+fm->id_label(templates[k])+" events").c_str());
		cholders["Sys2_cs_"+templates[k]]->setTitleY((fm->id_label(templates[k])+" cross section").c_str());
		cholders["Sys2_cserr_"+templates[k]]->setTitleY(("Error on "+fm->id_label(templates[k])+" cross section").c_str());


		cholders["Sys2_nevents_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_nevents_"+templates[k]).c_str());
		cholders["Sys2_err_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_error_"+templates[k]).c_str());
		cholders["Sys2_relerr_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_relerr_"+templates[k]).c_str());

		cholders["Sys2_cs_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_cs_"+templates[k]).c_str());
		cholders["Sys2_cserr_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_cserror_"+templates[k]).c_str());

		TGraphErrors g_n(nbins,x_axis ,y_axis ,x_ERR ,y_ERR);
		TGraphErrors g_e(nbins,x_axis ,ye_axis ,x_ERR ,ye_ERR);
		TGraphErrors g_re(nbins,x_axis ,yre_axis ,x_ERR ,yre_ERR);
		TGraphErrors g_cs(nbins,x_axis ,ycs_axis ,x_ERR ,ycs_ERR);
		TGraphErrors g_cse(nbins,x_axis ,ycse_axis ,x_ERR ,ycse_ERR);
				std::cout << "a6" << std::endl;

		cholders["Sys2_nevents_"+templates[k]]->addGraph(&g_n,"name1","",drawopt); 
		cholders["Sys2_nevents_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_nevents_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_nevents_"+templates[k]]->setLineSizeGraph(1);
		//cholders["Sys2_nevents_"+templates[k]]->setLineColors(colour);

		cholders["Sys2_err_"+templates[k]]->addGraph(&g_e,"name21","",drawopt); 
		cholders["Sys2_err_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_err_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_err_"+templates[k]]->setLineSizeGraph(1);

		cholders["Sys2_relerr_"+templates[k]]->addGraph(&g_re,"name31","",drawopt); 
		cholders["Sys2_relerr_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_relerr_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_relerr_"+templates[k]]->setLineSizeGraph(1);

		cholders["Sys2_cs_"+templates[k]]->addGraph(&g_cs,"name41","",drawopt); 
		cholders["Sys2_cs_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_cs_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_cs_"+templates[k]]->setLineSizeGraph(1);

		cholders["Sys2_cserr_"+templates[k]]->addGraph(&g_cse,"name51","",drawopt); 
		cholders["Sys2_cserr_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_cserr_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_cserr_"+templates[k]]->setLineSizeGraph(1);

		cholders["Sys2_nevents_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_nevents_"+templates[k]]->setYaxisOff(2.1);
		cholders["Sys2_err_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_err_"+templates[k]]->setYaxisOff(2.0);
		cholders["Sys2_relerr_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_relerr_"+templates[k]]->setYaxisOff(1.5);

		cholders["Sys2_cs_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_cs_"+templates[k]]->setYaxisOff(2.1);
		cholders["Sys2_cs_"+templates[k]]->setBordersY(140,200);
		

		cholders["Sys2_cserr_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_cserr_"+templates[k]]->setYaxisOff(2.1);

   
		cholders["Sys2_nevents_"+templates[k]]->setOptStat(00000);
// 		cholders["Sys2_nevents_"+templates[k]]->setBordersY(0,10);
// 		cholders["Sys2_nevents_"+templates[k]]->setBordersX(0,10);
		cholders["Sys2_nevents_"+templates[k]]->save("pdf");

		cholders["Sys2_err_"+templates[k]]->setOptStat(00000);
		//cholders["Sys2_err_"+templates[k]]->setBordersY(0,);
		//cholders["Sys2_err_"+templates[k]]->setBordersX(0,0);
		cholders["Sys2_err_"+templates[k]]->save("pdf");

		cholders["Sys2_relerr_"+templates[k]]->setOptStat(00000);
		//cholders["Sys2_relerr_"+templates[k]]->setBordersY(0,);
		//cholders["Sys2_relerr_"+templates[k]]->setBordersX(0,0);
		cholders["Sys2_relerr_"+templates[k]]->save("pdf");

		cholders["Sys2_cs_"+templates[k]]->setOptStat(00000);
		cholders["Sys2_cs_"+templates[k]]->save("pdf");

		cholders["Sys2_cserr_"+templates[k]]->setOptStat(00000);
		cholders["Sys2_cserr_"+templates[k]]->save("pdf");
		
		std::cout << "FitPlots:plot_sy_vs_numbers. Plotting correlation vs bin width" << std::endl;
		//Plot correlation vs bin width
		for(size_t m = 0;m < templates.size();m++){
			if(k != m){
				double ycorr_axis[nbins];
				double ycorr_ERR[nbins];
				int l = 0;
				for(std::map<std::string,double>::iterator study = nevts_res.begin();
				    study != nevts_res.end();study++){
					ycorr_axis[l] = sys_study_results2["Nevts_"+templates[k]+"_vs_Nevts_"+templates[m]][study->first];
					ycorr_ERR[l] = 0;
					l++;
				}
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]] = new CanvasHolder();
				std::string drawopt = "p";
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setTitleX("Bin Width");
				
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setTitleY(("Correlation "+fm->id_label(templates[k])+" nevts "+fm->id_label(templates[m])+" nevts").c_str());
				
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setCanvasTitle((fm->output_dir()+"Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]).c_str());
				
				TGraphErrors gcorr_n(nbins,x_axis ,ycorr_axis ,x_ERR ,ycorr_ERR);

				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->addGraph(&gcorr_n,"name1","",drawopt); 
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setMarkerSizeGraph(1);
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setMarkerStylesGraph(4);
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setLineSizeGraph(1);
				
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setXaxisOff(1.1);
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setYaxisOff(1.5);
				
				
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setOptStat(00000);
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->save("pdf");
			}
		}

	}
	delete_canvas_holders();

}

void FitPlots::plot_sys_templates_overlay()
{
	FitSetup *fs = fm->fit_setup();
	std::string fit_id = fs->fit_id();

        std::vector<int> *colours = new std::vector<int>();
        colours->push_back(kRed);
        colours->push_back(kBlue);
        colours->push_back(kGreen);
        colours->push_back(kRed+3);
        colours->push_back(kGray+3);
        colours->push_back(kMagenta+2);
        colours->push_back(kOrange+7);
        colours->push_back(kOrange-7);
        colours->push_back(kViolet-7);
	std::string x_id = fm->id_label(fs->observable_id("x"));

	for(std::map<std::string,CanvasHolder*>::iterator c = choldersDNR.begin();
	    c != choldersDNR.end(); c++){
                c->second->setLineColors(*colours);
                c->second->setTitleX(x_id);
                c->second->setTitleY("");
                c->second->setXaxisOff(1.1);
                c->second->setYaxisOff(1.3);
		c->second->setLegDrawSymbol("lp");
                c->second->setLegendOptions(.3,.2);

                c->second->save("pdf");

	}
}

void FitPlots::delete_canvas_holders()
{
	for(std::map<std::string,CanvasHolder*>::iterator c = cholders.begin();
	    c != cholders.end(); c++){
		delete c->second;
		c->second = NULL;
	}
	cholders.clear();
}

void FitPlots::set_canvas_style(TCanvas *c)
{
  c->SetFillColor(0);
  c->SetBorderMode(0);
  c->SetBorderSize(2);
  c->SetTickx();
  c->SetTicky();

  c->SetFrameFillStyle(0);
  c->SetFrameBorderMode(0);


}

void FitPlots::set_default_style()
{
  if(!theStyle) {
    theStyle = new TStyle(getRnd().c_str(),getRnd().c_str());
  }
  theStyle->SetCanvasDefH(800);
  theStyle->SetCanvasDefW(800);

  // Set pad grid and tick marks on opposite side of the axis
  //theStyle->SetPadGridX(1);
  //theStyle->SetPadGridY(1);

  theStyle->SetPadTickX(1);
  theStyle->SetPadTickY(1);

  // Set background color
  theStyle->SetCanvasColor(0);
  theStyle->SetStatColor(kWhite);

  // Title config
  theStyle->SetOptTitle(1);
  //  theStyle->SetTitleW(0.41);
  //  theStyle->SetTitleH(0.05);

//  theStyle->SetTitleX(0.16);
//  theStyle->SetTitleY(0.93);
  theStyle->SetTitleX(0.01);
  theStyle->SetTitleY(0.99);
  theStyle->SetTitleColor(1);
  theStyle->SetTitleTextColor(1);
  theStyle->SetTitleFillColor(0);
  theStyle->SetTitleBorderSize(1);
  theStyle->SetStatFont(42);
  theStyle->SetStatFontSize(0.025);
  theStyle->SetTitleFont(42);

  theStyle->SetTitleFontSize(0.04);
  double theCanleftmargin   = 0.15;
  double theCanrightmargin  = 0.07;
  double theCantopmargin    = 0.07;
  double theCanbottommargin = 0.15;
  theStyle->SetPadLeftMargin(theCanleftmargin);
  theStyle->SetPadRightMargin(theCanrightmargin);
  theStyle->SetPadTopMargin(theCantopmargin);
  theStyle->SetPadBottomMargin(theCanbottommargin);
  theStyle->SetOptStat(0);
  //theStyle->SetOptStat(theOptStat);


  theStyle->cd();
}

std::string FitPlots::getRnd(){
  int r=rand()%10000;
  char buf[7];
  sprintf (buf,"_R%d",r);
  return std::string(buf);
}

