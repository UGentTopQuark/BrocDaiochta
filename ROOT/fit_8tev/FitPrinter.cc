#include "FitPrinter.h"

FitPrinter::FitPrinter(FitManager* fm)
{
	this->fm = fm;
}

void FitPrinter::print_real_data_results()
{		
	FitSetup *fs = fm->fit_setup();;
	std::string fit_id = fs->fit_id();

		std::cout << "%\\begin{tabular}"<< std::endl;
		std::cout << "\\newpage"<< std::endl;
		std::cout << "N Data Events (Integral): " << fs->nscaled("data") << std::endl;
		std::cout << "%\\end{tabular}" << std::endl;
	//	std::cout << "\\multicolumn{3}{|c|}{" << histo_name << "} \\\\" <<std::endl;

	//	double fit_signal_tt = (fs->nscaled("tt_sig")/fs->nscaled("tt_total"))*(fs->nfit("tt_total")->getVal());
	//double err_fit_signal_tt = (fs->nfit("tt_total")->getError())/(fs->nfit("tt_total")->getVal()*(fit_signal_tt));
        std::cout << "**********************N Events Table*********************************************"<<std::endl;
	std::cout << "\\documentclass[a4paper,10pt]{article} \n \\pagestyle{empty} \n \\title{Template Fitting}"<<std::endl;
	std::cout << "\\begin{document} \n \\begin{tabular}{|c|r|r|} " <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "Template & N Expected & N Observed \\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout.precision(6);

	std::vector<std::string> templates = fs->template_norm_names();
        for(std::vector<std::string>::iterator id = templates.begin();
            id != templates.end();id++){
		if(*id == "wzq" && fs->nscaled("wj",false) != -1 && fs->nscaled("zj",false) != -1 && fs->nscaled("qcd",false) != -1)
			std::cout << *id << " & " << fs->nscaled("wj") << "(W) + " <<fs->nscaled("zj") << "(Z) + " <<  fs->nscaled("qcd") << "(QCD) = " << fs->nscaled(*id) << " & $" <<  fs->nfit(*id)->getVal()  <<  " + " << fs->nfit(*id)->getErrorHi()  <<  " " << fs->nfit(*id)->getErrorLo()  <<"$\\\\"<<std::endl;
		else if(*id == "tt_total" && fs->nscaled("tt_sig",false) != -1 && fs->nscaled("tt_other",false) != -1)
			std::cout << *id << " & " << fs->nscaled("tt_sig") << "(signal) + "<< fs->nscaled("tt_other") << "(other) = "<< fs->nscaled(*id) << " & $" <<  fs->nfit(*id)->getVal()  <<  " + " << fs->nfit(*id)->getErrorHi() <<  " " << fs->nfit(*id)->getErrorLo()  <<"$\\\\"<<std::endl;
		else
			std::cout << *id << " & " << fs->nscaled(*id) << " & $" <<  fs->nfit(*id)->getVal()  <<  " + " << fs->nfit(*id)->getErrorHi()  << " " << fs->nfit(*id)->getErrorLo()  <<"$\\\\"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	}

	std::cout << "\\hline"<<std::endl;
	std::cout << "\\end{tabular} \n \\vspace{0.1cm}\\\\ \n \\end{document}" << std::endl;	
        std::cout << "************************************************************************************"<<std::endl;



}

void FitPrinter::print_results_table()
{
        /**************************************
          Print Output ready for .tex
	***************************************/
	std::string sys_name = fm->sys_study_name();
	FitSetup *fs = fm->fit_setup();;
	std::string fit_id = fs->fit_id();
	
	std::cout << "**********************N Events Table*********************************************"<<std::endl;
	std::cout << "\\documentclass[a4paper,10pt]{article} \n \\pagestyle{empty} \n \\title{Template Fitting}"<<std::endl;
	std::cout << "\\begin{document} \n\\begin{tabular}{|c|r|r|} " <<std::endl;
	//std::cout << "\\hline"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\multicolumn{3}{|c|}{" << fit_id << " " << sys_name << "} \\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "Template & N Expected & N Observed \\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout.precision(6);
	
	std::vector<std::string> templates = fs->template_norm_names();
	for(std::vector<std::string>::iterator id = templates.begin();
	    id != templates.end();id++){
		if(*id == "wzq" && fs->nscaled("wj",false) != -1 && fs->nscaled("zj",false) != -1 && fs->nscaled("qcd",false) != -1)
			std::cout << *id << " & " << fs->nscaled("wj") <<"(W)+"<<fs->nscaled("zj")<<"(Z)+"<<fs->nscaled("qcd")<<"(QCD) = "<<fs->nscaled(*id) <<" & $" << fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() <<  " \\pm " << fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()  <<"$\\\\"<<std::endl;
		else if(*id == "tt_total" && fs->nscaled("tt_sig",false) != -1 && fs->nscaled("tt_other",false) != -1)
			std::cout << *id << " & " << fs->nscaled("tt_sig") << "(signal) + "<< fs->nscaled("tt_other") << "(other) = "<< fs->nscaled(*id) << " & $" <<  fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() <<  " \\pm " << fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()   << "$\\\\"<<std::endl;
		else
			std::cout << *id << " & "<< fs->nscaled(*id) << " & $" <<  fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() <<  " \\pm " << fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()   <<"$\\\\"<<std::endl;
		std::cout << "\\hline"<<std::endl;
	}
	
	std::cout << "\\end{tabular} \n \\vspace{0.1cm}\\\\ \n \\end{document}" << std::endl;	
	std::cout << "************************************************************************************"<<std::endl;

}

void FitPrinter::print_cross_section_table(eire::ConfigReader *config_reader, bool is_real_data)
{
        /**************************************
          Print Output ready for .tex
	***************************************/
	std::cout << "Printing table of cross sections " << std::endl;
	std::cout << "WARNING: There are a lot of opportunities for the code to crash in this function" << std::endl;
	
	bool verbose = false;

        CrossSectionProvider *cs_prov = new CrossSectionProvider();
        BranchingRatio *br_ratio = new BranchingRatio();
	std::string sys_name = fm->sys_study_name();

	FitSetup *fs = fm->fit_setup();;
	std::string fit_id = fs->fit_id();
	std::string section = "global";
	
	std::cout << "**********************Cross Section Table*********************************************"<<std::endl;
	std::cout << "\\documentclass[a4paper,10pt]{article} \n \\pagestyle{empty} \n \\title{Template Fitting}"<<std::endl;
	std::cout << "\\begin{document} \n\\begin{tabular}{|c|r|r|r|} " <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\multicolumn{3}{|c|}{" << fit_id << " " << sys_name << "} \\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "Template & CS Expected (pb) & CS Observed (pb) & Ratio \\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout.precision(5);

	std::string mc_analysis = config_reader->get_var("mc_analysis",section,true); 
	
	cs_prov->set_analysis(mc_analysis);
	
	std::vector<std::string> templates = fs->template_norm_names();
	for(std::vector<std::string>::iterator id = templates.begin();
	    id != templates.end();id++){
		std::cout << *id << " & $";
		std::vector<std::string> sig_input = config_reader->get_vec_var("sig_channel",*id,false);
		std::vector<std::string> bkg_input = config_reader->get_vec_var("bkg_channel",*id,false);
		std::vector<double> cross_sections;				
		
		bool added_tt_cs = false;
		for(std::vector<std::string>::iterator h_in = sig_input.begin();
		    h_in != sig_input.end();h_in++){
			
			double cs = cs_prov->get_cross_section(*h_in);
			
			cross_sections.push_back(cs);
			if(h_in->find("TT") != std::string::npos){
				added_tt_cs = true;
			}
			if(verbose) std::cout << "Added cs for " << *h_in << " cs " << cs << std::endl; 					
		}
		
		for(std::vector<std::string>::iterator h_in = bkg_input.begin();
		    h_in != bkg_input.end();h_in++){
			if(id->find("qcdmm") != std::string::npos){
				//Should instead get nscaled from nominal
				cross_sections.push_back(fs->nscaled(*id));
				//cross_sections.push_back(fs->nscaled(*id)/lumi);
				continue;
			}
			if(h_in->find("TT") != std::string::npos && added_tt_cs){
				continue;
			}
			
			double cs = cs_prov->get_cross_section(*h_in);
			cross_sections.push_back(cs);
			
			if(verbose) std::cout << "Added cs for " << *h_in << " cs " << cs << std::endl;  
		}
		
		//Now you have all the cross sections for this id. Print them and calculate the total 
		int ncs = cross_sections.size();
		double cs_sum = 0.;
		for(int i = 0;i < ncs; i++){
			if(i > 0) std::cout << " + ";
			std::cout << cross_sections[i];
			cs_sum += cross_sections[i];
		}
		if(ncs > 1){
			std::cout << " = "<< cs_sum;
		}
		std::cout <<  "$ & $ ";
		//Now calculate the cross section estimated by the fit
		double meas_nevts, meas_errhi,meas_errlo;
		if(is_real_data){
			meas_nevts = fs->nfit(*id)->getVal();
			meas_errhi = fs->nfit(*id)->getErrorHi();
			meas_errlo = fs->nfit(*id)->getErrorLo();
		}else{
			meas_nevts = fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal();
			meas_errhi = fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal();
			meas_errlo = meas_errhi;
		}			
		
		double nscaled_id = fs->nscaled(*id);
		if(verbose) std::cout << "Measured: " << meas_nevts << " Nscaled : " << nscaled_id << std::endl;
		
		double meas_cs = meas_nevts*(cs_sum/fs->nscaled(*id));
		double meas_cspe = meas_errhi*(cs_sum/fs->nscaled(*id));
		double meas_csme = meas_errlo*(cs_sum/fs->nscaled(*id));
		
		if(id->find("qcdmm") != std::string::npos){
			meas_cs = meas_nevts;
			meas_cspe = meas_errhi;
			meas_csme = meas_errlo;
		}
		
		std::cout << meas_cs;
		if(meas_cspe == meas_csme)
			std::cout << " \\pm " << meas_cspe;
		else
			std::cout << " + " << meas_cspe << " " << meas_csme;
		
		std::cout.precision(4);
		std::cout << " $ & $ " << meas_cs/cs_sum; 
		std::cout.precision(2);
		if(meas_cspe == meas_csme)
			std::cout << " \\pm " << meas_cspe/cs_sum;
		else
			std::cout << " + " << meas_cspe/cs_sum << " " << meas_csme/cs_sum;
		std::cout.precision(5);
		
		std::cout <<"$\\\\"<< std::endl;
		std::cout << "\\hline"<<std::endl;
		
	}
	
	std::cout << "\\end{tabular} \n \\vspace{0.1cm}\\\\ \n \\end{document}" << std::endl;	
	std::cout << "************************************************************************************"<<std::endl;

	
	delete cs_prov;
	cs_prov = NULL;
	delete br_ratio;
	br_ratio = NULL;
	
}


void FitPrinter::print_sys_cross_section_table(eire::ConfigReader *config_reader, bool is_real_data)
{
        /**************************************
          Print Output ready for .tex
	***************************************/
	std::cout << "Printing table of cross sections " << std::endl;
	std::cout << "WARNING: There are a lot of opportunities for the code to crash in this function" << std::endl;

	bool verbose = false;

        CrossSectionProvider *cs_prov = new CrossSectionProvider();		

	std::map<std::string,std::vector<double> > syst_cross_sections;
	std::vector<double> syst_cross_sections_pdfs;
	std::vector<double> syst_cross_sections_pdfs_nom_eff;
	std::vector<double> syst_cross_sections_pdfs_sys_eff_nom_N;

	FitSetup *fs = fm->fit_setup();;
	std::string fit_id = fs->fit_id();
	std::string section = "global";
	
	std::string mc_analysis = config_reader->get_var("mc_analysis",section,true);
	//double lumi = atof(config_reader->get_var("lumi",section,true).c_str()); 
	std::string sys_study = config_reader->get_var("sys_study",section,true);
	std::vector<std::string> sys_template_sections = config_reader->get_vec_var("sys_template_sections",sys_study,true);
	std::vector<std::string> sys_study_names = config_reader->get_vec_var("sys_study_names",sys_study,true);
	std::string nominal_study_name = *(sys_study_names.begin());
	std::vector<std::string> templates = fm->nominal_template_names();
	if(templates.size() == 0){
		templates = fs->template_norm_names();
	}
	
	cs_prov->set_analysis(mc_analysis);
	
	
	std::cout << "**********************Cross Section Table*********************************************"<<std::endl;
	std::cout << "\\begin{sideways} \n \\begin{tabular}{|c";
	for(size_t i = 0; i < templates.size();i++){
		std::cout << "|c|c";
	}
	std::cout <<"|} %sideways" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	int ncolumns = templates.size()*2 + 1;
	std::cout << "\\multicolumn{" << ncolumns << "}{|c|}{" << fit_id << "} \\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "Study  ";
	for(std::vector<std::string>::iterator id = templates.begin();
	    id != templates.end();id++){
		std::cout << " & " << *id << ",sys e & " << *id << ",nom e";
	}
	std::cout << "\\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout.precision(5);
	
	//*** Get summed cross section from nominal sample
	std::vector<double> summed_cross_sections;
	std::cout << "Expected ";
	for(std::vector<std::string>::iterator id = templates.begin();
	    id != templates.end();id++){
		
		std::vector<std::string> sig_input = config_reader->get_vec_var("sig_channel",*id,false);
		std::vector<std::string> bkg_input = config_reader->get_vec_var("bkg_channel",*id,false);
		std::vector<double> cross_sections;				
		
		bool added_tt_cs = false;
		for(std::vector<std::string>::iterator h_in = sig_input.begin();
		    h_in != sig_input.end();h_in++){
			
			double cs = cs_prov->get_cross_section(*h_in);
			
			if(h_in->find("TT") != std::string::npos){
				added_tt_cs = true;
			}
			cross_sections.push_back(cs);
			if(verbose) std::cout << "Added cs for " << *h_in << " cs " << cs << std::endl; 					
		}
		
		for(std::vector<std::string>::iterator h_in = bkg_input.begin();
		    h_in != bkg_input.end();h_in++){
			if(id->find("qcdmm")  != std::string::npos){
				//Should instead get nscaled from nominal
				cross_sections.push_back(fs->nscaled(*id));
				//cross_sections.push_back(fs->nscaled(*id)/lumi);
				continue;
			}
			if(h_in->find("TT") != std::string::npos && added_tt_cs){
				continue;
			}
				
			double cs = cs_prov->get_cross_section(*h_in);
			cross_sections.push_back(cs);
			
			if(verbose) std::cout << "Added cs for " << *h_in << " cs " << cs << std::endl;  
		}
		
		//Now you have all the cross sections for this id. Print them and calculate the total 
		int ncs = cross_sections.size();
		double cs_sum = 0.;
		for(int i = 0;i < ncs; i++){
			if(i == 0) std::cout << " & $ " ;
			//else std::cout << "+";
			//std::cout << cross_sections[i];
			cs_sum +=cross_sections[i];
		}
		summed_cross_sections.push_back(cs_sum);

		std::cout << cs_sum;
		syst_cross_sections["Expected"].push_back(cs_sum);
		syst_cross_sections["Expected"].push_back(cs_sum);

		std::cout << "$ & " ;
	}
	std::cout << "\\\\  " << std::endl;
	std::cout << "\\hline " << std::endl;
	//*** End get summed cross section from nominal sample
	
	std::map<std::string,std::vector<std::pair<double,double> > > sys_study_results = fm->get_sys_study_results();
	std::map<std::string,std::map<std::string,double> > sys_study_results2 = fm->get_sys_study_results2();
	
	for(std::vector<std::string>::iterator sys_name = sys_study_names.begin();
	    sys_name != sys_study_names.end(); sys_name++){
		std::cout << *sys_name;
		
		if(sys_study_results.find(*sys_name+fit_id) == sys_study_results.end()){
			std::cout << "FPrinter: print sys cs table. Trying to access results for ";
			std::cout  <<  *sys_name << fit_id << ". Study not found" << std::endl;
			exit(1);
		}
		int k = 0;
		for(std::vector<std::string>::iterator id = templates.begin();
		    id != templates.end();id++){
			
			if(sys_study_results2.find("NScaled_"+*id) == sys_study_results2.end()){
				std::cout << "ERROR: FPrint: no sys_study_results2 entry for: NScaled_" << *id << std::endl;
				exit(1);
			}
			std::vector<std::pair<double,double> > sys_study_result = sys_study_results[*sys_name+fit_id];
			if((int) sys_study_result.size() < k){
				std::cout << "FP:print sys cs table. Error: Trying to access element " << k << " of vector which has size: " <<  sys_study_result.size() << std::endl;
				exit(1);
			}
			std::pair<double,double> id_results = sys_study_result[k];
			
			//Now calculate the cross section estimated by the fit
			double meas_nevts = id_results.first;
			
			std::map<std::string,double> id_nscaled = sys_study_results2["NScaled_"+*id];
			if(id_nscaled.find(nominal_study_name) == id_nscaled.end()){
				std::cout << "ERROR: FPrint: no NScaled_" << *id << " entry for " << nominal_study_name << std::endl;
				exit(1);
			}
			if(id_nscaled.find(*sys_name) == id_nscaled.end()){
				std::cout << "ERROR: FPrint: no NScaled_" << *id << " entry for " << *sys_name<< std::endl;
				exit(1);
			}
			
			double nscaled_nom = id_nscaled[nominal_study_name];
			double nscaled_sys = id_nscaled[*sys_name];
			if(verbose) std::cout << "Measured: " << meas_nevts << " Nscaled nominal: " << nscaled_nom << " Nscaled syst: " << nscaled_sys<< std::endl;
			
			double meas_cs_sys_seleff = (meas_nevts/id_nscaled[*sys_name])*summed_cross_sections[k];
			double meas_cs_nom_seleff = (meas_nevts/id_nscaled[nominal_study_name])*summed_cross_sections[k];
			std::vector<std::pair<double,double> > nom_study_result = sys_study_results[nominal_study_name+fit_id];
			std::pair<double,double> nom_id_result = nom_study_result[k];
			double meas_cs_sys_seleff_nom_N = (nom_id_result.first/id_nscaled[*sys_name])*summed_cross_sections[k];
			
			if(id->find("qcdmm") != std::string::npos){
				meas_cs_sys_seleff = (meas_nevts);
				meas_cs_nom_seleff = meas_cs_sys_seleff;
			}
			
			std::cout << "  & $" << meas_cs_sys_seleff;
			std::cout << " $ & $" << meas_cs_nom_seleff << "$";
			if(k == 0)syst_cross_sections_pdfs.push_back(meas_cs_sys_seleff);
			if(k == 0)syst_cross_sections_pdfs_nom_eff.push_back(meas_cs_nom_seleff);
			if(k == 0)syst_cross_sections_pdfs_sys_eff_nom_N.push_back(meas_cs_sys_seleff_nom_N);
			syst_cross_sections[*sys_name].push_back(meas_cs_sys_seleff);
			syst_cross_sections[*sys_name].push_back(meas_cs_nom_seleff);
			k++;
		}
		std::cout <<"\\\\"<< std::endl;
		std::cout << "\\hline"<<std::endl;
	}
	
	std::cout << "\\end{tabular} \n \\vspace{0.1cm}\\\\ \n \\end{sideways} \n" << std::endl;
	std::cout << "************************************************************************************"<<std::endl;
	
	double max_diff = -99;
	double min_diff = -99;
	
	std::cout << "********************** Rel. Cross Section ******************************************"<<std::endl;
	std::cout << "\\begin{sideways} \n \\begin{tabular}{|c";
	for(size_t i = 0; i < templates.size();i++){
		std::cout << "|c|c";
	}
	std::cout <<"|} %sideways" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\multicolumn{" << ncolumns << "}{|c|}{" << fit_id << "} \\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "Study  ";
	for(std::vector<std::string>::iterator id = templates.begin();
	    id != templates.end();id++){
		std::cout << " & " << *id << ",sys e & " << *id << ",nom e";
	}
	std::cout << "\\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout.precision(5);
	for(std::map<std::string,std::vector<double> >::iterator cs_saved = syst_cross_sections.begin();
	    cs_saved != syst_cross_sections.end();cs_saved++){
		std::cout << cs_saved->first;
		int n_cs_saved = templates.size()*2;
		for(int k = 0; k < n_cs_saved;k++){
			double exp_cs = syst_cross_sections[nominal_study_name][k];
			double meas_cs = syst_cross_sections[cs_saved->first][k];
			double diff = meas_cs - exp_cs;
			std::cout << " & $ " << diff << "$"; 
			
			//k == 0 means just considering the cs values using sys sel eff
			if(k == 0 && cs_saved->first != "Expected"){
				//tt must be first template for this to work
				if((max_diff == -99 || diff > max_diff) && diff >=0)
					max_diff = diff;
				if((min_diff == -99 || diff < min_diff) && diff < 0)
					min_diff = diff;
			}
			
		}
		std::cout <<"\\\\"<< std::endl;
		std::cout << "\\hline"<<std::endl;
	}
	std::cout << "\\end{tabular} \n \\vspace{0.1cm}\\\\ \n \\end{sideways} \n" << std::endl;
	std::cout << "************************************************************************************"<<std::endl;
	
	std::cout << "%\\begin{tabular}"<< std::endl;
	std::cout << "\\newpage"<< std::endl;
	std::cout << "%\\end{tabular}" << std::endl;
	
	std::cout << "**** Cross Section: sys e & N, nom e & sys N, sys e & nom N **********************"<<std::endl;
	std::cout << "\\begin{sideways} \n \\begin{tabular}{|c|c|c|c|} %sideways" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "Study  & tt_total (sys e, N) & tt_total (nom e, sys N) &  tt_total (sys e, nom N)\\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout.precision(5);
	for(int i = 0;i < sys_study_names.size();i++){
		for(std::map<std::string,std::vector<double> >::iterator cs_saved = syst_cross_sections.begin();
		    cs_saved != syst_cross_sections.end();cs_saved++){
			//Extra step is to ensure order is the same as previous table
			if(cs_saved->first == sys_study_names[i]){
				std::cout << sys_study_names[i] << " & $ " << syst_cross_sections_pdfs[i] ;
				std::cout << "$ & $ " << syst_cross_sections_pdfs_nom_eff[i] ;
				std::cout << "$ & $ " << syst_cross_sections_pdfs_sys_eff_nom_N[i] << "$ \\\\" << std::endl;
				std::cout << "\\hline"<<std::endl;
			}
		}
	}
	std::cout << "\\end{tabular} \n \\vspace{0.1cm}\\\\ \n \\end{sideways} \n" << std::endl;
	std::cout << "************************************************************************************"<<std::endl;
	
	std::cout << "**** Rel. Cross Section: sys e & N, nom e & sys N, sys e & nom N **********************"<<std::endl;
	std::cout << "\\begin{sideways} \n \\begin{tabular}{|c|c|c|c|} %sideways" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "Study  & tt_total (sys e, N) & tt_total (nom e, sys N) &  tt_total (sys e, nom N)\\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout.precision(5);
	for(int i = 0;i < sys_study_names.size();i++){
		for(std::map<std::string,std::vector<double> >::iterator cs_saved = syst_cross_sections.begin();
		    cs_saved != syst_cross_sections.end();cs_saved++){
			double exp_cs = syst_cross_sections[nominal_study_name][0];
			//Extra step is to ensure order is the same as previous table
			if(cs_saved->first == sys_study_names[i]){
				std::cout << sys_study_names[i] << " & $ " << syst_cross_sections_pdfs[i] - exp_cs;
				std::cout << "$ & $ " << syst_cross_sections_pdfs_nom_eff[i] - exp_cs;
				std::cout << "$ & $ " << syst_cross_sections_pdfs_sys_eff_nom_N[i] - exp_cs << "$ \\\\" << std::endl;
				std::cout << "\\hline"<<std::endl;
			}
		}
	}
	std::cout << "\\end{tabular} \n \\vspace{0.1cm}\\\\ \n \\end{sideways} \n" << std::endl;
	std::cout << "************************************************************************************"<<std::endl;	
	
	std::cout << "%\\begin{tabular}"<< std::endl;
	std::cout << "\\newpage"<< std::endl;
	std::cout << "Max tt deviation: + " << max_diff << " " << min_diff << ", total: " << max_diff-min_diff << std::endl;
	std::cout << "%\\end{tabular}" << std::endl;
	

	std::cout << "*************** Raw Cross Sections (Nominal sel Eff. For testing only. not final numbers)*****"<<std::endl;
	for(std::vector<double>::iterator l = syst_cross_sections_pdfs_nom_eff.begin();
	    l != syst_cross_sections_pdfs_nom_eff.end();l++){
			std::cout << *l  << std::endl;
	}
	std::cout << "************************************************************************************"<<std::endl;

	std::cout << "*************** Raw Cross Sections (Nominal N,Sys sel Eff. For testing only. not final numbers)*****"<<std::endl;
	for(std::vector<double>::iterator l = syst_cross_sections_pdfs_sys_eff_nom_N.begin();
	    l != syst_cross_sections_pdfs_sys_eff_nom_N.end();l++){
			std::cout << *l  << std::endl;
	}
	std::cout << "************************************************************************************"<<std::endl;
	std::cout << "********************** Raw Cross Sections ******************************************"<<std::endl;
	for(std::vector<double>::iterator l = syst_cross_sections_pdfs.begin();
	    l != syst_cross_sections_pdfs.end();l++){
			std::cout << *l  << std::endl;
	}
	std::cout << "************************************************************************************"<<std::endl;

	delete cs_prov;
	cs_prov = NULL;
}

void FitPrinter::print_sys_table_numbers(eire::ConfigReader *config_reader)
{
	FitSetup *fs = fm->fit_setup();;
	std::string fit_id = fs->fit_id();

	std::string section = "global";
	std::string sys_study = config_reader->get_var("sys_study",section,true);
	std::vector<std::string> sys_study_names = config_reader->get_vec_var("sys_study_names",sys_study,true);
	std::string nominal_study_name = *(sys_study_names.begin());
	std::map<std::string,std::vector<std::pair<double,double> > > sys_study_results = fm->get_sys_study_results();
	std::map<std::string,std::map<std::string,double> > sys_study_results2 = fm->get_sys_study_results2();
	std::vector<std::string> templates = fm->nominal_template_names();
	if(templates.size() == 0){
		templates = fs->template_norm_names();
	}
	//Calculate branching ratio, Expected sigma from MC, Observed sigma from fit and stat error on observed
	//Acc from MC
	std::string histo_name = "histo";

	
        std::cout << "**********************Sys Nevents Table*********************************************"<<std::endl;
	std::cout << "\\documentclass[a4paper,10pt]{article} \n \\pagestyle{empty} \n \\title{Template Fitting}"<<std::endl;
	std::cout << "\\begin{document} \n \\begin{tabular}{|c";
	for(size_t i = 0;i < templates.size(); i++)
		std::cout << "|c";
	std::cout <<"|} " <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "Study ";
        for(std::vector<std::string>::iterator id = templates.begin();
            id != templates.end();id++)
		std::cout << "& N " << *id;
	std::cout <<"\\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	std::cout.precision(6);
	std::cout << "Expected ";
        for(std::vector<std::string>::iterator id = templates.begin();
            id != templates.end();id++)
		std::cout << "& $ " << sys_study_results2["NScaled_"+*id][nominal_study_name] << "$";
	std::cout <<"\\\\"<<std::endl;
	std::cout << "\\hline"<<std::endl;
	
	for( std::map<std::string,std::vector<std::pair<double,double> > >::iterator iter_sys = sys_study_results.begin();iter_sys != sys_study_results.end();++iter_sys)
		{
			//calculate cross section
			//double sys_sigma_obs = -1;
			int signal_iter = -1;
			for(size_t i = 0;i < templates.size(); i++)
				if(templates[i].find("tt") != std::string::npos)
					//if(templates[i] == "tt_total")
					signal_iter = i;

			//if(signal_iter != -1)
				//sys_sigma_obs = (*iter_sys).second[signal_iter].first/(Acc_tot_exp*200);

			std::cout << (*iter_sys).first ;
			for(std::vector<std::pair<double,double> >::iterator res = (*iter_sys).second.begin();
			    res !=  (*iter_sys).second.end();res++){
				std::cout << " & $"<< res->first << " \\pm " << res->second << "$ ";
			}
			std::cout << "\\\\"<<std::endl;
			std::cout << "\\hline"<<std::endl;
		}
			
	std::cout << "\\end{tabular} \n \\vspace{0.1cm}\\\\ \n \\end{document}" << std::endl;	
        std::cout << "************************************************************************************"<<std::endl;

	for( std::map<std::string,std::vector<std::pair<double,double> > >::iterator iter_sys = sys_study_results.begin();iter_sys != sys_study_results.end();++iter_sys)
		{
		  for(std::vector<std::pair<double,double> >::iterator res = (*iter_sys).second.begin();
			    res !=  (*iter_sys).second.end();res++){
		    std::cout << res->first << " ";
			}
		  std::cout << "" << std::endl;
		}
}

void FitPrinter::print_correlation(bool is_real_data)
{
	std::string sys_name = fm->sys_study_name();
	
	FitSetup *fs = fm->fit_setup();;
	std::string fit_id = fs->fit_id();
	std::vector<std::string> templates = fs->template_norm_names();

	//Print correlation matrix
	std::cout << "************************Correlation Matrix************************" << std::endl; 
	std::cout << "\\begin{tabular}{|c|r";
	for(size_t i = 0; i != templates.size();i++){
		std::cout << "|r";}
	std::cout << "|} \n \\hline" <<std::endl;
	std::cout << "& Nevents ";
	for(size_t i = 0; i != templates.size();i++){
		std::cout << " & " << templates[i];}
	std::cout <<  "\\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;

	for(size_t i = 0; i != templates.size();i++){
		std::cout << templates[i];
		double nevts = -1, errhi = -1, errlo = -1;
		if(is_real_data){
			nevts = fs->nfit(templates[i])->getVal();
			errhi = fs->nfit(templates[i])->getErrorHi();
			errlo = fs->nfit(templates[i])->getErrorLo();
		}else{
			nevts = fm->ngauss(templates[i]+"_mean_"+fit_id+"_"+sys_name)->getVal();
			errhi = errlo = fm->ngauss(templates[i]+"_sigma_"+fit_id+"_"+sys_name)->getVal();
		}
		std::cout << " & $" << nevts;
		if(errhi != errlo) std::cout << " +" << errhi << " " << errlo;  
		else std::cout << " \\pm " << errhi;
		std::cout << "$ ";

		for(size_t j = 0; j != templates.size();j++){
			double corr = 0;
			if(i == j){
				corr = 1;
			}
			else if(is_real_data){
				corr = fm->data_fit_res()->correlation(("nevts_"+templates[i]).c_str(),("nevts_"+templates[j]).c_str());
			}else{
				corr = fm->fit_par_data()->correlation(*(fs->nfit(templates[i],true)),*(fs->nfit(templates[j],true)));
			}
			std::cout << " & " << corr;
		}
	std::cout <<  "\\\\" <<std::endl;
	std::cout << "\\hline"<<std::endl;
	}
	std::cout << "\\end{tabular} " <<std::endl;
	std::cout << "***************************************************************" << std::endl; 

	std::cout << "Print compact tree: " << std::endl;

}
