#ifndef FITMANAGER_H
#define FITMANAGER_H

#include "TF1.h"
#include "TH1F.h"
#include "TF2.h"
#include "TH2F.h"
#include "TH2.h"
#include "TFile.h"
#include "TCanvas.h"
//#include "tdrstyle.h"
#include "CanvasHolder/Canvas_Holder.h"

#include <RooRandom.h>
#include <RooRandomizeParamMCSModule.h>
#include <RooGaussian.h>
#include <RooPoisson.h>
#include <RooGlobalFunc.h>
#include <RooRealVar.h>
#include <RooChi2Var.h>
#include <RooPoisson.h>
#include <RooGaussian.h>
#include <RooPlot.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooExponential.h>
#include <RooAddPdf.h>
#include <RooProdPdf.h>
#include <RooFitResult.h>
#include <RooArgusBG.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooLandau.h>
#include <RooMinuit.h>
#include <RooMCStudy.h>
#include <RooHistPdf.h>
#include <RooArgSet.h>
#include <RooAbsArg.h>
#include <RooArgList.h>
#include <RooConstVar.h>
#include <RooWorkspace.h>
#include <RooGenFitStudy.h>
#include <RooStudyManager.h>
#include "RooSimultaneous.h"
#include "RooCategory.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "FitSetup.h"
#include "HistoHandler.h"


class FitManager{
        public:
	        FitManager();
	        ~FitManager();
		void set_outfile(TFile *outfile);
		void set_output_dir(std::string dir);
	        void set_n_pseudo_exp(int n_exp); //<How many pseudo exps you want to run
		void set_sys_study_name(std::string study_name); //<set name for current sys study.eg. btag010
		void set_run_full_sys_study(bool run_study = true); //<run study for multiple systematic data histos
		void set_run_multi_rebin_study(bool run = true); //<run study rebinning histos each time
		void set_use_real_data(bool fit_real_data = true); //<Set if fitting real data
		void set_fit_setup(FitSetup *fs);//<Set id for category of simultaneous fit being set
		inline void set_histo_handler(HistoHandler<TH1D> *hh){hh1d = hh;}//<Only used within FitManager when smearing template shapes (for shape stat. sys)
		inline void set_histo_handler(HistoHandler<TH2D> *hh){hh2d = hh;}//<Only used within FitManager when smearing template shapes (for shape stat. sys)
		inline void set_nominal_template_names(std::vector<std::string> names){fm_nominal_template_names = names;}//< Save template ids from nominal fit 
		void set_nominal_fit_setup(FitSetup *fs);
		inline void set_smear_template_shapes(bool smear){fm_smear_template_shapes = smear;}//< Smear template shapes per data fit
		inline void set_do_not_smear_patterns(std::vector<std::string> nosmear){fm_do_not_smear_patterns = nosmear;}//< Patterns found in template ids you don't want to smear
		inline void set_vary_constraint_mean(bool vary){fm_vary_constraint_mean = vary;}//< Vary constraint mean per data fit. Done automatically for pseudo-exps (regardless of bool).
		

		/***
		    After fitting fitplots->fit_nevents_gauss() applies a gaussian fit to the nfit
		    distribution. This is what we take as the result of the fit
		***/
		void doFit(bool separate_pseudodata = false,bool is_real_data = false,bool data_from_nominal = false,bool external_pseudodata = false); 
		void initialise_fit_output_histos();//<Histos containing results, filled per fit.
		void initialise_sys_study_histos();
		void save_sys_study_results(); //<If running full sys study,save ngauss results after each run
		void fill_fit_output_histos();//<Fill results of each fit
		void initialise_ngauss_evts();//<Initialise RooRealVars to be used when fitting gauss to nevts.

		/***
		   These variables might change during fitting so need to call 
		   here each time you access an element.
		***/
		TFile* outfile();
		TH1F* output_histo(std::string id, bool need = true);
		TH2F* output_histo_2D(std::string id, bool need = true);
		std::map<std::string, TH1F*> output_histos_1D();
		std::map<std::string, TH2F*> output_histos_2D();
		RooRealVar* ngauss(std::string id);
		int n_pseudo_exp();
		bool run_full_sys_study();
		bool run_multi_rebin_study();
		bool use_real_data();
		RooFitResult* data_fit_res();
		RooDataSet* fit_par_data();
		std::string output_dir();
		std::string sys_study_name();
		std::vector<std::string> sys_study_names();
		inline std::vector<std::string> nominal_template_names(){return fm_nominal_template_names;}
		inline bool smear_template_shapes(){return fm_smear_template_shapes;}
		inline bool vary_constraint_mean(){return fm_vary_constraint_mean;}
		FitSetup* fit_setup();
		std::string id_label(std::string id);
	      

		///Get results for sys studies once all fits have finished running
		std::map<std::string,std::vector<std::pair<double,double> > > get_sys_study_results();
		std::map<std::string,std::map<std::string,double> > get_sys_study_results2();

		void delete_all(bool delete_data = true);
		/***
		    These functions are used individually when new mc histos are set
		    At the end of the study from delete_all() they are all called with data deleted
		 ***/
		void delete_output_histos(bool sys = false);
		void delete_ngauss_evts();
		void delete_fit_setup();

        private:
		double calculate_pull(std::string fit_id,std::string var_id,double err = -999);

		TFile *fm_outfile;		
		std::string fm_output_dir;

		std::vector<std::string> fm_sys_study_names;
		std::vector<std::string> fm_do_not_smear_patterns;

		std::map<std::string, TH1F*> fm_output_histos;//<Histos containing results from fits
		std::map<std::string, TH2F*> fm_output_histos_2D;//<Histos containing results from fits
		std::map<std::string,RooRealVar*> ngauss_evts; //<results of gaussian fit to nevts from fit 
		std::map<std::string,std::string> fm_id_label; //<Specify label in plot to be use with an id.

		std::map<std::string,std::vector<std::pair<double,double> > > fm_sys_study_results;
		std::map<std::string,std::map<std::string,double> > fm_sys_study_results2;
		std::vector<std::string> fm_nominal_template_names;
		FitSetup* fm_nominal_fit_setup;
		FitSetup* fm_fit_setup;
		HistoHandler<TH1D> *hh1d;
		HistoHandler<TH2D> *hh2d;

		std::map<std::string,double> fm_per_fit_res;//<Contains some doubles we want to plot per fit e.g. constraint mean.

		RooFitResult *fm_data_fit_res;
		RooCategory *fm_category;
		RooSimultaneous *fm_simPdf;
		RooDataSet *fm_fit_par_data; //<Same as fitParData in RooMCStudy.Holds Results for all fits.

		bool fm_run_full_sys_study;
		bool fm_run_multi_rebin_study;
		bool fm_use_real_data;
		bool fm_smear_template_shapes;
		bool fm_vary_constraint_mean;

		int fm_n_pseudo_exp;
		std::string fm_sys_study_name;

		TH1F *TEMP_data_hist_in;
		double TEMP_nExpData;


		static const bool verbose = true;
};

#endif
