[global]
datadir=./
output_dir=./
version=_plots
cutset=01_cutset
channel=mu
histo_name=top_mass_
use_real_data=false
npseudo_exp=5000
lumi =39.1
mc_analysis =Fall107TeV
rebin=2
gauss_constraints=st #Any gaussian constraint you want to apply. Must be available in FitManager.cc avail_gauss_constraints
template_shape_ids =tt_total:wj:st # Must be set
template_normalisation_ids =tt_total:wzq:st #Not necessary. Set these if you want to use a different shape and normalisation for the templates
mc_histos =tt_total:wzq:st:wj:zj:qcd:tt_sig:tt_other #Not necessary. For display or printout
histo_ranges =36pb_muon_4jet # ranges for nevents from fit. Important that these make sense
sys_study=rebin_sys_study # If you want to run a systematic study specify it here
do_not_scale = QCD #vector of datasets which should not be scaled.

[mc]
use_real_data=false

[data]
use_real_data=true

[rebin_sys_study]
run_multi_rebin_sys_study = true
multi_rebin = -1:2:3:4:5:6:7:8:9:10:11:12:13:14:15:16:17:18:19:20:25

[full_sys_study]
sys_study_names =none:JES_110:JES_090:btag_plus:btag_minus #You can choose any name for these ids. no spaces though
#you can specfiy versions or cutsets or both. default means it will take the version/cutset specified in global
sys_study_versions =default:_plots_110:_plots_090:default:default 
sys_study_cutsets =default:default:default:04_cutset:06_cutset
sys_histo_section =all_samples #This histograms will be merged and used as the distribution the templates specified in global are fit to.

[tt_total]
sig_channel=TTbar
bkg_channel=TTbar
[wzq]
bkg_channel=Wjets:DY50:MuQCD
[st]
bkg_channel=TtChan:TsChan:TtWChan
[wj]
bkg_channel=Wjets
[zj]
bkg_channel=DY50
[qcd]
bkg_channel=MuQCD
[tt_sig]
sig_channel=TTbar
[tt_other]
bkg_channel=TTbar
[all_samples]
sig_channel=TTbar
bkg_channel=TTbar:Wjets:DY50:MuQCD:TtChan:TsChan:TtWChan

[36pb_muon_4jet]
tt_total=0:450
wzq=-50:350
wj=-50:300
st=0:30
[36pb_muon_3jet]
tt_total=-100:600
wzq=300:1000
st=-5:60
[337pb_muon_4jet]
tt_total=500:4000
wzq=100:3000
st=10:20
[36pb_muon_4jet_stconstr]
tt_total=0:500
wzq=-100:350
st=-50:500

