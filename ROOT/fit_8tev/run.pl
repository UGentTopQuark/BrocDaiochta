#!/usr/bin/perl -w

#use strict;

$run_cfg = 'run.cfg';
die "can't find $run_cfg, exiting..." unless(-e $run_cfg);

do "$run_cfg";

print "Error define config\n" and die if!(defined($config));
$config_dir = "./" if!(defined($config_dir));
print "Error $config_dir/$config does not exist\n" and die if(!-e $config_dir."/".$config);

$datadir_e = "./" if!(defined($datadir_e));
print "Error Datadir: $datadir_e does not exist\n" and die if(!-d $datadir_e);
$datadir_mu = "./" if!(defined($datadir_mu)); 
print "Error Datadir: $datadir_mu does not exist\n" and die if(!-d $datadir_mu); 
$output_dir = "./" if!(defined($output_dir));
print "Error Outputdir: $output_dir does not exist\n" and die if(!-d $output_dir); 
$public_html_dir = "~/public_html/TEST_fit" if!(defined($public_html_dir));
print "making directory $public_html_dir\n" and `mkdir $public_html_dir` if(!-d $public_html_dir); 
$force_time = 0 if(!defined($force_time));
$force_ident = 0 if(!defined($force_ident));
$identifier = ask_for_ident($identifier) if(!defined($identifier) || $identifier eq "" || $force_ident);
$use_cluster = 0 if!(defined($use_cluster));
$use_real_data = 0 if!(defined($use_real_data));
$smear_template_shapes = 0 if!(defined($smear_template_shapes));
$vary_constraint_mean = 0 if!(defined($vary_constraint_mean));
$fit_2D_template = 0 if!(defined($fit_2D_template));
$use_default_qcd_cutset = 1 if!(defined($use_default_qcd_cutset));
print "Error all_histos_to_fit must be defined\n" and die if!(defined(@all_histos_to_fit));
print "Error all_histo_ranges must be defined\n" and die if!(defined(@all_histo_ranges));
$do_not_smear_patterns = "" if!(defined($do_not_smear_patterns));
@all_sys_studies = ("") if!(defined(@all_sys_studies));
$create_summary_pdf = 1 if!(defined($create_summary_pdf));

$nfits = @all_histos_to_fit;
$nranges = @all_histo_ranges;
if($nfits != $nranges){
    print "all_histos_to_fit and all_histo_ranges have different number of elements $nfits $nranges";
    die;
}

my @data_options = ();
@data_options = ("false") if ($use_real_data == 0);
@data_options = ("true") if ($use_real_data == 1);
@data_options = ("true","false") if ($use_real_data == 2);

foreach my $sys_study(@all_sys_studies){
    foreach my $real_data(@data_options){
	$i = 0;
	foreach my $histos_to_fit(@all_histos_to_fit){
	    my $data_mc = "mc";
	    $data_mc = "data" if($real_data eq "true");

	    my $sys_id;
	    if($sys_study eq ""){
		$sys_id = "central";
	    }else{
		$sys_id = $sys_study;
	    }
	    my $emu_id = "";
	    if($histos_to_fit =~ m/:/){
		$emu_id = "_emu";
	    }

	    my $dir_id = $identifier.$emu_id."_".$histos_to_fit."_".$data_mc."_".$sys_id;
	    if($smear_template_shapes){
		$dir_id = $identifier.$emu_id."_smear_".$histos_to_fit."_".$data_mc."_".$sys_id;
	    }
	    if($vary_constraint_mean){
		$dir_id = $identifier.$emu_id."_varcmean_".$histos_to_fit."_".$data_mc."_".$sys_id;
	    }
	    if($fit_2D_template){
		$dir_id = $identifier.$emu_id."_2D_".$histos_to_fit."_".$data_mc."_".$sys_id;
	    }
	    #replace colon with _
	    $dir_id =~ s/:/_/g;

	    my $dir_name = create_directory($dir_id,$output_dir,$force_time);
            #copy fit_tag_and_probe to output dir
	    my $output_dir_path = $output_dir."/".$dir_name;
	    
	    system("make -j2");
	    system("cp fit_template $output_dir_path/");
	    $histo_ranges = $all_histo_ranges[$i];
	    my $config_out = prepare_config($config_dir,$config,$datadir_e,$datadir_mu,$output_dir_path,$sys_study,$real_data,$histos_to_fit,$histo_ranges);   
	    if(!$use_cluster){
		print "time $output_dir_path/fit_template $config_out 2>&1 | tee $output_dir_path/out.txt";
		system("time $output_dir_path/fit_template $config_out 2>&1 | tee $output_dir_path/out.txt");
		my $summary_tex_dir = $output_dir."/summaries";
		my $summary_tex = create_summary_tex($summary_tex_dir,$config_out,$output_dir,$dir_name,$data_mc,$sys_id,$histos_to_fit);
	    
		my $summary_tex_path = $summary_tex_dir."/".$summary_tex;
		if(-e $summary_tex_path && $create_summary_pdf && $summary_tex_path =~ m/tex/){
		    add_tables_to_tex($output_dir_path,$summary_tex_path);
		    print "pdflatex $summary_tex_path \n";
		    system("pdflatex $summary_tex_path");
		    $summary_tex =~ s/.tex//g;	
		    print "Removing $summary_tex .aux/.log \n";
		    system("rm $summary_tex\.aux");
		    system("rm $summary_tex\.log");	 
		    system("mv $summary_tex\.pdf $summary_tex_dir/$summary_tex\.pdf");
		    system("cp $summary_tex_dir/$summary_tex\.pdf $public_html_dir/")
#		    system("cp $summary_tex_dir/$summary_tex\.pdf ~/public_html/TEST_fit/")
		    #system("xpdf $summary_tex_dir/$summary_tex\.pdf &");
		}	 		
	    }else{
		my $cluster_sh = create_cluster_sh_file($output_dir_path,$config_out);
		print "qsub -o $output_dir_path -e $output_dir_path -q localgrid\@cream01 $cluster_sh";
		system("qsub -o $output_dir_path -e $output_dir_path -q localgrid\@cream01 $cluster_sh");
	    }

	    $i++;
	}
    }
}

sub create_directory
{
	my($identifier, $output_dir, $force_time) = @_;

	my $date = sprintf("%04d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3]);
	my $dir_name = $date."_0_$identifier";
	my $dir_to_check = "$output_dir/$dir_name";
	if(-d "$dir_to_check" or $force_time){
		$date = sprintf("%04d%02d%02d_%02d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3], (localtime)[2], (localtime)[1], (localtime)[0]);
		$dir_name = $date."_$identifier";
	}

	system("mkdir -p $output_dir/$dir_name");

	return $dir_name;
}

sub create_cluster_sh_file
{
        my($output_dir,$config_name) = @_;
	
        my @output = `pwd`;
        my $here = pop @output;
        chomp $here;

	my $outfile = $output_dir."/cluster.sh";
        open(SUB, ">$outfile") or die "cannot open $outfile: $!";
        print SUB <<EOF;
#!/bin/bash
cd $here
$output_dir/fit_template $config_name | tee $output_dir/out.txt
EOF
        close(SUB);
        `chmod 755 $outfile`;
        return $outfile;
}

sub prepare_config
{
    my($config_dir,$config,$datadir_e,$datadir_mu,$output_dir,$sys_study,$real_data,$histos_to_fit,$histo_ranges) = @_;
    my $split_qcd = "false";
    if($histos_to_fit =~ m/:/){
	$split_qcd = "true";
    }
    my $smear_template_shapes_bool = "false";
    if($smear_template_shapes){
	$smear_template_shapes_bool = "true";
    }
    my $vary_constraint_mean_bool = "false";
    if($vary_constraint_mean){
	$vary_constraint_mean_bool = "true";
    }
    my $fit_2D_template_bool = "false";
    if($fit_2D_template){
	$fit_2D_template_bool = "true";
    }
    my $use_default_qcd_cutset_bool = "false";
    if($use_default_qcd_cutset){
	$use_default_qcd_cutset_bool = "true";
    }

    my $config_out = $output_dir."/".$config;
    
    open(FILE, "<$config_dir/$config") or die "cannot open $config_dir/$config: $!";;    
    
    @write_lines = ();
    my $found_global = 0;
    my $finished_global = 0;
    my $filled_vars = 0;
    while(my $line = <FILE>){
        if(!$finished_global && !($line =~ m/^\s*datadir/ || $line =~ m/^\s*output_dir/ || $line =~ m/^\s*histos_to_fit/ || $line =~ m/^\s*use_real_data/ ||$line =~ m/^\s*sys_study/ ||$line =~ m/^\s*histo_ranges/ ||$line =~ m/^\s*split_qcdmm/ ||$line =~ m/^\s*use_default_qcd_cutset/||$line =~ m/^\s*smear_template_shapes/||$line =~ m/^\s*vary_constraint_mean/ ||$line =~ m/^\s*fit_2D_template/ ||$line =~ m/^\s*do_not_smear_patterns/)){
	    $line =~ s/(\#.*)//g;
	    if($line =~ m/\s*\[global\].*/){
		$found_global = 1; 
		push(@write_lines,$line);
		next;
	    }
	    #print "$real_data \n";
	    #die;
	    
	    if($found_global == 1 and $filled_vars == 0){
		push(@write_lines,"datadir_e = $datadir_e \n");
		push(@write_lines,"datadir_mu = $datadir_mu \n");
		push(@write_lines,"output_dir = $output_dir \n");
		push(@write_lines,"use_real_data = $real_data \n");
		push(@write_lines,"histos_to_fit = $histos_to_fit \n");
		push(@write_lines,"histo_ranges = $histo_ranges \n");
		push(@write_lines,"split_qcdmm =  $split_qcd\n");
		push(@write_lines,"use_default_qcd_cutset =  $use_default_qcd_cutset_bool\n");
		push(@write_lines,"smear_template_shapes =  $smear_template_shapes_bool\n");
		push(@write_lines,"do_not_smear_patterns =  $do_not_smear_patterns\n");
		push(@write_lines,"vary_constraint_mean =  $vary_constraint_mean_bool\n");
		push(@write_lines,"fit_2D_template =  $fit_2D_template_bool\n");
		if($sys_study ne ""){
		    push(@write_lines,"sys_study = $sys_study \n");
		}
		$filled_vars = 1;	    
	    }
	    push(@write_lines,$line);

	    # Assume global is always first section.
	    # For all subsequent section all variables are filled
	    if($found_global && $line =~ m/\s*\[.*\]/){
		$finished_global = 1;
	    }

	}
	elsif($finished_global){
	    push(@write_lines,$line);
	}
	    


    }
    close FILE;

    if($found_global == 0){
	push(@write_lines,"[global]");
	push(@write_lines,"datadir_e = $datadir_e \n");
	push(@write_lines,"datadir_mu = $datadir_mu \n");
	push(@write_lines,"output_dir = $output_dir \n");
	push(@write_lines,"use_real_data = $real_data \n");
	push(@write_lines,"histos_to_fit = $histos_to_fit \n");
	push(@write_lines,"histo_ranges = $histo_ranges \n");
	push(@write_lines,"split_qcdmm =  $split_qcd\n");
	push(@write_lines,"use_default_qcd_cutset =  $use_default_qcd_cutset_bool\n");
	push(@write_lines,"smear_template_shapes =  $smear_template_shapes_bool\n");
	push(@write_lines,"do_not_smear_patterns =  $do_not_smear_patterns\n");
	push(@write_lines,"vary_constraint_mean =  $vary_constraint_mean_bool\n");
	push(@write_lines,"fit_2D_template =  $fit_2D_template_bool\n");
	if($sys_study ne ""){
	    push(@write_lines,"sys_study = $sys_study \n");
	}
    }
    #now write to output file
    print "Error: $output_dir/$config_out already exists" if(-e $output_dir."/".$config_out);
    open(FILE, ">$config_out");
    print FILE @write_lines;
    close FILE;

    return $config_out;
}

sub ask_for_ident
{
	my($default_ident) = @_;

	print "Please enter ident for run [$default_ident]:\n";
	my $ident = <STDIN>;

	chomp $ident;

	$ident = $default_ident if($ident !~ /[\d\w]+/);

	return $ident;
}


sub create_summary_tex
{
	my($summary_tex_dir,$config_out,$output_dir,$dir_name,$data_mc,$sys_id,$histo_to_fit) = @_;

	my $output_dir_path = $output_dir."/".$dir_name;
        # find available file names
	my @available_pdfs = ();
	opendir(DIR, "$output_dir_path") or die "can't open directory: $output_dir_path\n";
	while(defined(my $cur_file = readdir(DIR))){
	    push(@available_pdfs, $cur_file) if($cur_file =~ /\.pdf/);
	}
	closedir(DIR);


	my $mc_frame = "results_frame_mc_AUTO.tex";
	my $data_frame = "results_frame_data_AUTO.tex";
	my $sys_frame = "results_frame_sys_AUTO.tex";
	my $my_frame = "";

	`mkdir $summary_tex_dir` if!(-d $summary_tex_dir);

	#if($sys_id eq "central"){
	#    if($data_mc eq "data" && !$smear_template_shapes&& !$vary_constraint_mean){
	#	$my_frame = $data_frame;
	#    }else{
	#	$my_frame = $mc_frame;
	#	if($split_qcdmm){
	#	    $my_frame =~ s/_AUTO/_qcdmmmue_AUTO/g;
	#	}
	#    }
	#}else{
	if($histo_to_fit =~ m/:/){
	    $sys_frame =~ s/_AUTO/_qcdmmmue_AUTO/g;
	}
	if($fit_2D_template){
	    $sys_frame =~ s/_AUTO/_2D_AUTO/g;
	}
	return create_sys_summary_tex($summary_tex_dir,$config_out,$output_dir,$dir_name,$sys_id,$sys_frame,$data_mc);
	
#}
#
#	my $tex_out = $dir_name.".tex";
#	my $tex_out_path = $summary_tex_dir."/".$tex_out;
#	
#	open(FILE, "<$my_frame") or die "cannot open $my_frame: $!";  
#	#If it's a sys study want to read config to get sys names 	
#	#open(FILE, "<$config_out") or die "cannot open $config_out: $!";    
#	
#	my @write_lines = ();
#	while(my $line = <FILE>){
#	    $line =~ s/DIR_NAME/$output_dir_path/g; 
#	    push(@write_lines,$line);
#	}
#	close FILE;
#	
#	#now write to output file
#	print "Error: $tex_out_path already exists" if(-e $tex_out_path);
#	open(FILE, ">$tex_out_path");
#	print FILE @write_lines;
#	close FILE;
#	
#	return $tex_out;
#		
}
sub create_sys_summary_tex
{
	my($summary_tex_dir,$config_out,$output_dir,$dir_name,$sys_id, $my_frame,$data_mc) = @_;

	
	my $output_dir_path = $output_dir."/".$dir_name;
        # find available file names
	my @available_pdfs = ();
	opendir(DIR, "$output_dir_path") or die "can't open directory: $output_dir_path\n";
	while(defined(my $cur_file = readdir(DIR))){
	    push(@available_pdfs, $cur_file) if($cur_file =~ /\.pdf/);
	}
	closedir(DIR);



	#Read in config_out to get sys names
	open(FILE, "<$config_out") or die "cannot open $config_out: $!"; 
	@study_names = ();
	my $found_sys_id = "false";
	my $found_study_names = "false";
	my $found_rebin_names = "false";
	if($sys_id ne "central"){
	    while(my $line = <FILE>){
		if($line =~ m/\[$sys_id\]/){
		    $found_sys_id = "true";
		}
		if($found_study_names eq "false" && $found_sys_id eq "true" &&
		   $line =~ m/\s*sys_study_names\s*/ && !($line =~ m/^;/ || $line =~ m/^#/)){
		    $line =~ s/\s*sys_study_names =\s*//g;
		    @study_names = split(/:/,$line);
		    foreach my $id(@study_names){
			$id =~ s/\s*//g;
		    }
		    $found_study_names = "true";
		    $found_sys_id = "false";
		}
		if($found_rebin_names eq "false" && $found_sys_id eq "true" &&
		   $line =~ m/^\s*multi_rebin\s*/ && !($line =~ m/^;/ || $line =~ m/^#/)){
		    $line =~ s/\s*multi_rebin =\s*//g;
		    @study_names = split(/:/,$line);
		    foreach my $id(@study_names){
			$id =~ s/\s*//g;
		    }
		    $found_rebin_names = "true";
		    $found_sys_id = "false";
		}
	    }
	    close FILE;
	    print "Study names: @study_names \n";
	}

	open(FILE, "<$my_frame") or die "cannot open $my_frame: $!";
	my @write_lines = (); 
	my $in_section = "false";
	my $finished_section = "false";
	my @section_lines = ();
	my $mc_only_section = "false";
	my $rebin_only_section = "false";
	my $sys_only_section = "false";
	my $prev_section_sys = "false";
	while(my $line = <FILE>){
	    if($line =~ m/^\s*\%/ && !($line =~ m/^\s*\%%%%/)){
		print "Line skipped: $line"; 
		next;
	    }
	    $line =~ s/DIR_NAME/$output_dir_path/g;
	    if($found_rebin_names eq "true" && !($line =~ /templates/)){
		$line =~ s/Sys\_/Sys2\_/g;
	    }		
	    if($line =~ m/END SECTION/){
		$in_section = "false";
		$rebin_only_section = "false";
		$sys_only_section = "false";
		$mc_only_section = "false";
		$finished_section = "true";
	    }	
	    

	    if($in_section eq "true" && 
	       ($mc_only_section eq "false" || $data_mc eq "mc" || $smear_template_shapes || $vary_constraint_mean) &&
	       ($rebin_only_section eq "false" || $found_rebin_names eq "true") && 
	       ($sys_only_section eq "false" || $sys_id ne "central")){
		push(@section_lines,$line);
		
	    }
	    if($finished_section eq "true"){

		if($prev_section_sys ne "true" && $sys_id ne "central"){
		    foreach my $sys(@study_names){
			foreach my $s_line(@section_lines){
			    my $sys_line = $s_line;
			    $sys_line =~ s/SYS_NAME/$sys/g;
			    if($sys_line =~ m/caption/){
				$sys_line =~ s/_/\\\_/g;
			    }		
			    if($sys_line =~ /includegraphics/){
				
				my $pdf_replacement = "";
				print "LINE: $sys_line\n";
				if($sys_line =~ m/$output_dir_path\/([\w_\.]+)\}/){
				    my $pdf_pattern = $1;
				    print "FOUND pattern: $1\n";
				    foreach my $pdf(@available_pdfs){
					if(($pdf =~ /$pdf_pattern/ && $pdf =~ /$sys/) || $pdf eq $pdf_pattern){
					    $pdf_replacement = $pdf;
					    print "PDF replacement: $pdf_replacement\n";
					    last;
					}
				    }
				}
				if($pdf_replacement eq ""){
				    print "No pdf available for: $sys_line and $sys\n";
				    die;
				}else{
				    $sys_line =~ s/$output_dir_path\/([\w_]+)\}/$output_dir_path\/$pdf_replacement\}/g;
				    print "SYS line out: $sys_line\n"; 
				}
			    }
			    push(@write_lines,$sys_line);
			}
		    }
		}else{		    
		    foreach my $s_line(@section_lines){
			my $sys_line = $s_line;
			if($sys_id eq "central"){
			    $sys_line =~ s/SYS_NAME\://g;
			}
			if($sys_line =~ m/caption/){
			    $sys_line =~ s/_/\\\_/g;
			}		
			if($sys_line =~ /includegraphics/){
			    
			    my $pdf_replacement = "";
			    print "LINE: $sys_line\n";
			    if($sys_line =~ m/$output_dir_path\/([\w_\.]+)\}/){
				my $pdf_pattern = $1;
				print "FOUND pattern: $1\n";
				foreach my $pdf(@available_pdfs){
				    if($pdf =~ /$pdf_pattern/ || $pdf eq $pdf_pattern){
					$pdf_replacement = $pdf;
					print "PDF replacement: $pdf_replacement\n";
					last;
				    }
				}
			    }
			    if($pdf_replacement eq ""){
				print "No pdf available for: $sys_line. \n";
				die;
			    }else{
				$sys_line =~ s/$output_dir_path\/([\w_]+)\}/$output_dir_path\/$pdf_replacement\}/g;
				print "SYS line out: $sys_line\n"; 
				}
			}
			push(@write_lines,$sys_line);
					
		    }
		}
		$finished_section = "false";
		$prev_section_sys = "false";
		
	    }

	    if($in_section eq "false"){
		push(@write_lines,$line);
	    }		    


	    if($line =~ m/BEGIN SECTION/){
		$in_section = "true";
		if($line =~ m/MC/){
		    $mc_only_section = "true";
		}
		if($line =~ m/REBIN/){
		    $rebin_only_section = "true";
		}
		if($line =~ m/SYS/){
		    $sys_only_section = "true";
		    $prev_section_sys = "true";
		}
		@section_lines = ();
	    }
	}
	close FILE;

	my $tex_out = $dir_name.".tex";
	my $tex_out_path = $summary_tex_dir."/".$tex_out;
	#now write to output file
	print "Error: $tex_out_path already exists" if(-e $tex_out_path);
	open(FILE, ">$tex_out_path");
	print FILE @write_lines;
	close FILE;
	print "Returning $tex_out \n";
	return $tex_out;
	


}
sub add_tables_to_tex
{
	my($output_dir_path,$summary_tex_path) = @_;

	open(FILE, "<$output_dir_path/out.txt") or die "cannot open $output_dir_path/out.txt: $!"; 

		
	my @write_lines = ();
	my $found_table = "false";
	my $sideways = "false";
	while(my $line = <FILE>){
	    if($line =~ m/begin{tabular}/){
		if($line =~ m/sideways/){
		    my $side_line = "\\small{ \n\\begin{sideways} \n";
		    push(@write_lines,$side_line);
		    $sideways = "true";
		}		
		$found_table = "true";
	    }

	    if($found_table eq "true"){
		$line =~ s/_/\\\_/g;
		push(@write_lines,$line);
	    }

	    if($line =~ m/end{tabular}/){
		$found_table = "false";
		my $space_l = "\n \\vspace{0.1cm} \n \n ";
		push(@write_lines,$space_l);
		if($sideways eq "true"){
		    my $side_line = "\\end{sideways} \n } \n";
		    push(@write_lines,$side_line);
		    $sideways = "false";
		}
		    
	    }
	}
	close FILE;
	
	#now write to output file
	open(FILE, "<$summary_tex_path") or die "cannot open $summary_tex_path: $!"; 
	@all_lines = ();
	my $found_doc_start = "false";
	while(my $line = <FILE>){
	    if($found_doc_start eq "true"){
		foreach my $write_l(@write_lines){
		    push(@all_lines,$write_l);
		}
		$found_doc_start = "false";
	    }

	    if($line =~ m/begin{document}/){ 
		$found_doc_start = "true";   
	    }
	    push(@all_lines,$line);
	}
	close FILE;

	system("rm $summary_tex_path");
	open(FILE, ">$summary_tex_path"); 
	print FILE @all_lines;
	close FILE;
			
}
