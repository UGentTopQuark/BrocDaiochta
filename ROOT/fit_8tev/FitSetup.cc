#include "FitSetup.h"

using namespace RooFit;

FitSetup::FitSetup()
{
	fs_ranges_set = false;
	fs_external_constraint_argset = NULL;
	x_obs = "x";
	y_obs = "";
	gen_random = new TRandom3();
}


FitSetup::~FitSetup()
{
	std::cout << "Saving smear test plot " << std::endl;

        delete_all(true);
	if(gen_random){
		delete gen_random;
		gen_random = NULL;
	}
	std::cout << "Finished with FS destructor" << std::endl;
}


void FitSetup::set_fit_id(std::string id)
{
	fs_fit_id = id;
}

	
void FitSetup::set_observable_ids(std::string x,std::string y)
{
	x_obs = x;
	y_obs = y;
}

	
void FitSetup::set_template_shape_names(std::vector<std::string> names)
{
	fs_template_shape_names = names;
}


void FitSetup::set_template_norm_names(std::vector<std::string> names)
{
	fs_template_norm_names = names;
}


void FitSetup::set_gaussian_constraints(std::vector<std::string> constraints)
{
	fs_gauss_constraints = constraints;
}


void FitSetup::initialise_avail_gauss_constraints()
{
	if (verbose) std::cout << " FS: Initialising available gauss constraints " << std::endl; 

	if(find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "st") != fs_template_norm_names.end()){
		avail_gauss_constraints.push_back("st");
		fs_template_names_for_constraint["st"] = "st";
		fs_constraint_vars["st_mean"] = nscaled_evts["st"];
		fs_constraint_vars["st_sigma"] = 0.3*(nscaled_evts["st"]);
			//fs_constraint_vars["st_sigma"] = 0.07*(nscaled_evts["st"]);
	}
	if(find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "qcdmm") != fs_template_norm_names.end()){
		avail_gauss_constraints.push_back("qcdmm");
		fs_template_names_for_constraint["qcdmm"] = "qcdmm";
		fs_constraint_vars["qcdmm_mean"] = nscaled_evts["qcdmm"];
		fs_constraint_vars["qcdmm_sigma"] =0.5*(nscaled_evts["qcdmm"]);
	}
	if(find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "qcd") != fs_template_norm_names.end()){
		avail_gauss_constraints.push_back("qcd");
		fs_template_names_for_constraint["qcd"] = "qcd";
		fs_constraint_vars["qcd_mean"] = nscaled_evts["qcd"];
		fs_constraint_vars["qcd_sigma"] = 0.5*(nscaled_evts["qcd"]);
	}
	for(std::vector<std::string>::iterator tname = fs_template_norm_names.begin();
	    tname != fs_template_norm_names.end();tname++){
		if(tname->find("wzj") != std::string::npos){
			avail_gauss_constraints.push_back("wzj");
			fs_template_names_for_constraint["wzj"] = *tname;
			fs_constraint_vars["wzj_mean"] = nscaled_evts[*tname];
			fs_constraint_vars["wzj_sigma"] = 0.0001*(nscaled_evts[*tname]);
		}
	}

	if(find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "zj") != fs_template_norm_names.end() &&
	   find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "wj") != fs_template_norm_names.end()){
		avail_gauss_constraints.push_back("wjzj");
		fs_template_names_for_constraint["wjzj"] = "wjzj";
	        fs_rooform_constraint["wjzj"] = new RooFormulaVar("wjzj","@0/@1",RooArgList(*nfit_evts["wj"],*nfit_evts["zj"])); 
		fs_constraint_vars["wjzj_mean"] = nscaled_evts["wj"]/nscaled_evts["zj"];
		fs_constraint_vars["wjzj_sigma"] = 0.3*(nscaled_evts["wj"]/nscaled_evts["zj"]);

		//Range will be used for initialisation of output plot. Not directly in fit.
		if(h_ranges_low.find("wjzj") == h_ranges_low.end() && h_ranges_high.find("wjzj") == h_ranges_high.end()){
			h_ranges_low["wjzj"] = 0;
			h_ranges_high["wjzj"] = fs_constraint_vars["wjzj_mean"] + fs_constraint_vars["wjzj_sigma"]*4;
		}

	}
	//Check all requested constraints are available
	for(std::vector<std::string>::iterator c = fs_gauss_constraints.begin();
	    c != fs_gauss_constraints.end();c++){
		if(find(avail_gauss_constraints.begin(),avail_gauss_constraints.end(),*c) == avail_gauss_constraints.end()){
			std::cout << "ERROR: The gaussian constraint requested, " << *c << ", is not in the list of available constraints." << std::endl;
			std::cout << "Add manually in FitSetup.cc initialise_avail_gauss_constraints(). " << std::endl;
			exit(1);
		}
	}
}

//Ranges set should vary depending on expected variation in fitted nevts.
void FitSetup::set_ranges(std::map<std::string,int> lranges, std::map<std::string,int> hranges)
{
	for(std::map<std::string,int>::iterator lr = lranges.begin();lr != lranges.end();lr++)
		h_ranges_low[lr->first] = lr->second;

	for(std::map<std::string,int>::iterator hr = hranges.begin();hr != hranges.end();hr++)
		h_ranges_high[hr->first] = hr->second;
	
	fs_ranges_set = true;
}

void FitSetup::initialise_nfit_mc()
{
	if(verbose) std::cout << " Initialising nfit vars for norm templates only: "<< fs_fit_id << std::endl;
	if(nscaled_evts.size() < fs_template_norm_names.size()){
		std::cout << "ERROR: Need to initialise nscaled before initialising nfit " << std::endl;
		std::cout << " n nscaled: " << nscaled_evts.size() << " n norm templates: " << fs_template_norm_names.size() << std::endl;
		exit(1);
	}

	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){
		if(h_ranges_low.find(*id) == h_ranges_low.end() || h_ranges_high.find(*id) == h_ranges_high.end()){
			std::cout << "ERROR: Ranges not set for: "<< *id << ". Check ranges section of config" << std::endl;
			exit(1);
		} 
		if(verbose) std::cout << " Initialising nfit vars for "<< *id << " nscaled: " <<nscaled_evts[*id];
		if(verbose) std::cout << " rlow: " << h_ranges_low[*id]  << " rhigh: " << h_ranges_high[*id]<< std::endl;
		if(h_ranges_low[*id] >= nscaled_evts[*id] || h_ranges_high[*id] <= nscaled_evts[*id]){
			std::cout << "ERROR:Invalid range, N scaled events not within allowed range. "<< std::endl;
			exit(1);
		} 

		nfit_evts[*id] = new RooRealVar(("nevts_"+*id).c_str(),("Number of "+*id+" events").c_str(),nscaled_evts[*id],h_ranges_low[*id],h_ranges_high[*id]);

	}

	if(verbose) std::cout << " nfit vars initialised"<<std::endl;
   
}


void FitSetup::initialise_nfit_form_mc(std::map<std::string,RooRealVar*> fit1_vars,std::map<std::string,double> fit1_nscaled)
{
	if(fit1_vars.size() != fs_template_norm_names.size() || fit1_nscaled.size() < fit1_vars.size()){
		std::cout << "ERROR: FS: Need to have same number of templates set for each fit. Also templates must have same names "<< std::endl;
		std::cout << "fit1_vars: " << fit1_vars.size() << " fit2_vars: " << fs_template_norm_names.size() << " fit1_nscaled: " << fit1_nscaled.size() << std::endl;
		exit(1);
	}

	initialise_form_const_vars(fit1_nscaled);

	if(verbose) std::cout << " Initialising nfit form vars for norm templates only: "<< fs_fit_id << std::endl;
	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){
		
		if(verbose) std::cout << " Initialising nfit form vars for "<< *id << " const_var: " <<form_const_vars[*id]->getVal();
		nfit_form_evts[*id] = new RooFormulaVar(("nevts_form_"+*id).c_str(),"@0/@1",RooArgList(*fit1_vars[*id],*form_const_vars[*id]));
	}

	if(verbose) std::cout << " nfit form vars initialised"<<std::endl;
}


void FitSetup::initialise_form_const_vars(std::map<std::string,double> fit1_nscaled)
{
	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){

		if(fit1_nscaled.find(*id) == fit1_nscaled.end() || fit1_nscaled[*id] == 0
		   || nscaled_evts.find(*id) == nscaled_evts.end() || nscaled_evts[*id] == 0){
			std::cout << "ERROR: " << *id << " is a norm template for fit 2 but not for fit1" << std::endl;
			std::cout << " when doing simultaneous fit templates must exist in both fit setups " << std::endl;
			exit(1);
		}
		else{
			form_const_vars[*id] = new RooConstVar((*id+"_a/"+*id+"_b const").c_str(),"constant value ", fit1_nscaled[*id]/nscaled_evts[*id]);
			
		}
	}
}


//Could be real or pseudo-data.
void FitSetup::initialise_nfit_data()
{

	if(verbose) std::cout << "FS: Initialising scaled and fit variables data" << std::endl;
	if(nfit_evts.find("data") != nfit_evts.end() && nfit_evts["data"])
		delete nfit_evts["data"];

	if(h_ranges_low.find("data") == h_ranges_low.end() || h_ranges_high.find("data") == h_ranges_high.end()){ 
		h_ranges_low["data"] = 0;
		h_ranges_high["data"] = nscaled_evts["data"]*2;
	}

	nfit_evts["data"] = new RooRealVar("nevts_data","Number of data events",nscaled_evts["data"],h_ranges_low["data"],h_ranges_high["data"]);

}

void FitSetup::initialise_histpdfs()
{
	if (verbose) std::cout << "FS: Convert DataHists to pdf's " << std::endl; 
	delete_pdfs(false);

	//Convert histograms to pdf's to create template
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		if(verbose) std::cout << "Id: " << *id <<  std::endl;
		pdfs[*id] = new RooHistPdf((*id+"_pdf").c_str(),(*id+"_pdf").c_str(),observable_argset(),*datahists[*id]);

	}
}


void FitSetup::initialise_mc_template()
{
	if(fs_observables[x_obs] == NULL){
	   std::cout << "ERROR: FS: observable " << x_obs << "  not initialised" << std::cout;
	   exit(1);
	}
	if(y_obs != "" && fs_observables[y_obs] == NULL){
	   std::cout << "ERROR: FS: observable " << y_obs << "  not initialised" << std::cout;
	   exit(1);
	}

	initialise_histpdfs();

	if(verbose) std::cout << " FS: Initialising mc templates " << std::endl;
	delete_templates(false);

	RooArgList *template_pdfs = new RooArgList();
	RooArgList *template_nfit_evts = new RooArgList();
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		template_pdfs->add(*pdfs[*id]);
	}
	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){
		if(nfit_evts.find(*id) != nfit_evts.end())
			template_nfit_evts->add(*nfit_evts[*id]);
		else if(nfit_form_evts.find(*id) != nfit_form_evts.end())
			template_nfit_evts->add(*nfit_form_evts[*id]);
		else{
			std::cout << "ERROR: no nfit or nfit form for: " << *id << std::endl;
			exit(1);
		}
	}

	//Add the pdf's. input for mc study
	templates["MC"] = new RooAddPdf((fs_fit_id+"_template").c_str(), (fs_fit_id+" template").c_str(), *template_pdfs,*template_nfit_evts);
	
	initialise_avail_gauss_constraints();

	delete template_pdfs;
	template_pdfs = NULL;
	delete template_nfit_evts;
	template_nfit_evts = NULL;
}


//void FitSetup::initialise_for_data_template()
//{
//	if(verbose) std::cout << " FS: Initialising data pdf and template " << std::endl;
//	if(fs_observables.find(x_obs) == fs_observables.end() || fs_observables[x_obs] == NULL)
//		std::cout << "ERROR: FS: initialise_for_data_template observable " << x_obs << " not initialised" << std::endl;
//
//      if(datahists("data") == NULL){
//std::cout << "ERROR FS: trying to initialise data templates without data histogram. " std::endl;
//exit(1);
//}
//
//	if( (pdfs.find("data") != pdfs.end() && pdfs["data"] )|| 
//	    (templates.find("data") != templates.end() && templates["data"])){
//		std::cout << "***********************ERROR**************************:";
//		std::cout << "FS: initialise_for_data_template. Things which should be NULL are not " << std::endl;
//		exit(1);
//	}
//
//	std::cout << " FS: |||||||||||| initialise for data template |||||||||||||||| " << std::endl;
//	RooArgList obs;
//	obs.add(*fs_observables[x_obs]);
//	if(fs_observables.find(y_obs) != fs_observables.end() && fs_observables[y_obs] != NULL)
//		obs.add(*fs_observables[y_obs]);
//
//	pdfs["data"] = new RooHistPdf("data_pdf","data_pdf",obs,*datahists["data"]);
//	templates["data"] = new RooAddPdf("data_template", "data template", RooArgList(*pdfs["data"]),RooArgList(*nfit_evts["data"]));
//
//	if(verbose) std::cout << "FS: Data template initialised" << std::endl;
//}
//
/************************ Variable Access ****************************/


RooArgSet FitSetup::observable_argset()
{
	//	if(!fs_obs_argset){
	RooArgSet obs;
	int i = 0;
	for(std::map<std::string,RooRealVar*>::iterator o = fs_observables.begin();
	    o != fs_observables.end();o++){

		obs.add(*(o->second));
		i++;
	}

	return obs;
}

std::string FitSetup::observable_id(std::string xy)
{
	if(xy == "y")
		return y_obs;
	else
		return x_obs;
}


std::map<std::string,RooRealVar*> FitSetup::observables()
{
	return fs_observables;
}

RooDataHist* FitSetup::datahist(std::string id, bool need)
{
	if(datahists.find(id) == datahists.end()){
		if(!need)
			return NULL;
		else {
			std::cout << " ERROR:FitSetup, No datahist entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}
	}
	else if(datahists[id] == NULL){
		if(!need)
			return NULL;
		else{ 
			std::cout << " ERROR:FitSetup, NULL datahist entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}
	}

	return datahists[id];	

}

RooDataHist* FitSetup::pseudodata_datahist(int iter_npseudoexp)
{
	if(pseudodata_datahists.size() < iter_npseudoexp){
		std::cout << "ERROR FitSetup. Trying to access " << iter_npseudoexp << " th element of pseudodata_datahists but size = " << pseudodata_datahists.size() << std::endl;
		exit(1);
	}else if(pseudodata_datahists[iter_npseudoexp] == NULL){
		std::cout << "ERROR FitSetup. Element " << iter_npseudoexp << " of pseudodata_datahists is NULL. " << std::endl;
		exit(1);
	}else{
		return pseudodata_datahists[iter_npseudoexp];
	} 
}


double FitSetup::nscaled(std::string id,bool need)
{
	if(nscaled_evts.find(id) == nscaled_evts.end()){
		if(need) std::cout << " ERROR:FitSetup, No nscaled entry for " << id << " in " << fs_fit_id << std::endl;
		if(!need)
			return -1;
		else
			exit(1);
	}

	return nscaled_evts[id];
}


double FitSetup::constraint_var(std::string v)
{
	if(fs_constraint_vars.find(v) == fs_constraint_vars.end()){
		std::cout << "FitSetup: constriant_vars(). Constraint variable: " << v << " has not been set " <<" in " << fs_fit_id <<  std::endl;
		exit(1);
	}
	else 
		return fs_constraint_vars[v];

}

std::string FitSetup::template_name_for_constraint(std::string c)
{
	//FIXME: unsafe. constraint may not exist in map. Bad soln to issue anyway. 
	if(fs_template_names_for_constraint.find(c) != fs_template_names_for_constraint.end()){
		return fs_template_names_for_constraint[c];
	}else{
		std::cout << "WARNING FitSetup: No template name available for this constraint" << std::endl;
		return "";
	}

}

std::map<std::string,double> FitSetup::nscaled_map()
{
	return nscaled_evts;
}


int FitSetup::range_low(std::string id)
{
	if(h_ranges_low.find(id) == h_ranges_low.end())
		std::cout << " ERROR:FitSetup, No range_low entry for " << id << " in " << fs_fit_id << std::endl;

	return h_ranges_low[id];
}


int FitSetup::range_high(std::string id)
{
	if(h_ranges_high.find(id) == h_ranges_high.end())
		std::cout << " ERROR:FitSetup, No range_high entry for " << id << " in " << fs_fit_id << std::endl;

	return h_ranges_high[id];
}


RooRealVar* FitSetup::nfit(std::string id, bool need,bool quiet)
{
	if(nfit_evts.find(id) == nfit_evts.end()){
		if(!need){
			if(!quiet) std::cout << " WARNING:FitSetup, No nfit entry for " << id <<" in " << fs_fit_id <<  std::endl;
			return NULL;
		}else{ 
			std::cout << " ERROR:FitSetup, No nfit entry for " << id << " in " << fs_fit_id << std::endl;
			exit(1);
		}
	}
	else if(nfit_evts[id] == NULL){
		if(!need){
			if(!quiet) std::cout << " WARNING:FitSetup, NULL nfit entry for " << id << " in " << fs_fit_id << std::endl;
			return NULL;
		}else{ 
			std::cout << " ERROR:FitSetup, NULL nfit entry for " << id << " in " << fs_fit_id << std::endl;
			exit(1);
		}
	}

	return nfit_evts[id];
}


std::map<std::string, RooRealVar*> FitSetup::nfit_map()
{
	return nfit_evts;
}


RooFormulaVar* FitSetup::nfit_form(std::string id, bool need)
{
	if(nfit_form_evts.find(id) == nfit_form_evts.end()){
		if(need){
			std::cout << " ERROR:FitSetup, No nfit form entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}else{ 
			std::cout << " WARNING:FitSetup, No nfit form entry for " << id << " in " << fs_fit_id << std::endl;
			return NULL;
		}
	}

	if(nfit_form_evts[id] == NULL ){
		std::cout << " WARNING:FitSetup, NULL nfit form entry for " << id <<" in " << fs_fit_id <<  std::endl;
		if(need)
			exit(1);
	}
	
	return nfit_form_evts[id];
}


RooFormulaVar* FitSetup::rooform_constraint(std::string id, bool need,bool quiet)
{
	if(fs_rooform_constraint.find(id) == fs_rooform_constraint.end() ){
		if(need){
			std::cout << " ERROR:FitSetup, No rooform constraint entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}else{ 
			if(!quiet) std::cout << " WARNING:FitSetup, No rooform constraint entry for " << id <<" in " << fs_fit_id <<  std::endl;
			return NULL;
		}
	}
	
	if(fs_rooform_constraint[id] == NULL){
		if(need){
			std::cout << " ERROR:FitSetup, NULL nfit form entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}else if(!quiet)
			std::cout << " WARNING:FitSetup, NULL nfit form entry for " << id <<" in " << fs_fit_id <<  std::endl;
	}

	return fs_rooform_constraint[id];
}


RooHistPdf* FitSetup::pdf(std::string id)
{
	if(pdfs.find(id) == pdfs.end() || pdfs[id] == NULL)
		std::cout << " ERROR:FitSetup, No pdf entry for " << id <<" in " << fs_fit_id <<  std::endl;

	return pdfs[id];
}


RooAddPdf* FitSetup::get_template(std::string id)
{
	if(templates.find(id) == templates.end() || templates[id] == NULL )
		std::cout << " ERROR:FitSetup, No template entry for " << id <<" in " << fs_fit_id <<  std::endl;

	return templates[id];
}


std::vector<std::string> FitSetup::template_shape_names()
{
	return fs_template_shape_names;
}


std::vector<std::string> FitSetup::template_norm_names()
{
	return fs_template_norm_names;
}


std::vector<std::string> FitSetup::gauss_constraints_used()
{
	return fs_gauss_constraints;
}


bool FitSetup::constrained(std::string c)
{
	if(find(fs_gauss_constraints.begin(), fs_gauss_constraints.end(), c) != fs_gauss_constraints.end())
		return true;
	else
		return false;
}


std::string FitSetup::fit_id()
{
	return fs_fit_id;
}


bool FitSetup::ranges_set()
{
	return fs_ranges_set;
}


/************************** Delete ******************************/


void FitSetup::delete_all(bool delete_data)
{
	std::cout << "Fs: Deleting everything, ";
	if(delete_data) std::cout << " including variables for data" << std::endl;
	else std::cout << " except data variables" << std::endl;
	delete_nfit_evts(delete_data); std::cout << " nfit deleted " << std::endl;
	delete_pdfs(delete_data);std::cout << " pdfs deleted " << std::endl;
	delete_templates(delete_data);std::cout << " templates deleted " << std::endl;
	delete_gauss_constraints();std::cout << " constraints deleted " << std::endl;

}


void FitSetup::delete_nfit_evts(bool delete_data)
{
	for(std::map<std::string,RooRealVar*>::iterator iter = nfit_evts.begin();iter != nfit_evts.end();++iter){
		if(!delete_data && iter->first == "data")
			continue;
		else{
			delete nfit_evts[iter->first];
			nfit_evts[iter->first] = NULL;
		}
	}

	nfit_evts.clear();

}

void FitSetup::delete_pdfs(bool delete_data)
{

	for(std::map<std::string,RooHistPdf*>::iterator iter = pdfs.begin();iter != pdfs.end();++iter){
		if(!delete_data && iter->first == "data")
			continue;
		else{
			delete pdfs[iter->first];
			pdfs[iter->first] = NULL;
		}
	}
	if(delete_data) pdfs.clear();

}


void FitSetup::delete_templates(bool delete_data)
{
	for(std::map<std::string,RooAddPdf*>::iterator iter = templates.begin();iter != templates.end();++iter){
		if(!delete_data && iter->first == "data")
			continue;
		else{
			delete templates[iter->first];
			templates[iter->first] = NULL;
		}
	}
	if(delete_data) templates.clear();
}

void FitSetup::delete_gauss_constraints()
{

	for(std::map<std::string,RooFormulaVar*>::iterator iter = fs_rooform_constraint.begin();iter != fs_rooform_constraint.end();++iter){
		delete fs_rooform_constraint[iter->first];
		fs_rooform_constraint[iter->first] = NULL;
	}
	fs_rooform_constraint.clear();
}
