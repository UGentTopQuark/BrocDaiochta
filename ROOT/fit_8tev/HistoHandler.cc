#include "HistoHandler.h"

using namespace RooFit;

template <class HISTO>
HistoHandler<HISTO>::HistoHandler()
{
	x_obs = "x";
	y_obs = "";
	fit_id = "";
	gen_random = new TRandom3();
}

template <class HISTO>
HistoHandler<HISTO>::~HistoHandler()
{
	std::cout << "Saving smear test plot " << std::endl;
        delete_all(true);
	std::cout << "Finished with FS destructor" << std::endl;
}

template <class HISTO>	
void HistoHandler<HISTO>::set_observable_ids(std::string x,std::string y)
{
	x_obs = x;
	y_obs = y;
}

template <class HISTO>	
void HistoHandler<HISTO>::set_template_shape_names(std::vector<std::string> names)
{
	fs_template_shape_names = names;
}

template <class HISTO>
void HistoHandler<HISTO>::set_template_norm_names(std::vector<std::string> names)
{
	fs_template_norm_names = names;
}

template <class HISTO>
void HistoHandler<HISTO>::initialise_observable(double low_range, double high_range, std::string obs_id)
{
	if (verbose) std::cout << " HH: Initialising observable: " << obs_id << std::endl; 

	if(hh_observables.find(obs_id) != hh_observables.end() && hh_observables[obs_id] != NULL){
		std::cout << "WARNING: HH: Why are you reinitialising " << obs_id << "?" <<std::endl;
		delete hh_observables[obs_id];
	}

	hh_observables[obs_id] = new RooRealVar(obs_id.c_str(),obs_id.c_str(),low_range,high_range);

	if(verbose) std::cout << "Observable " << obs_id << " initialised with min " << low_range << " max " << high_range << std::endl;

}

template <class HISTO>
void HistoHandler<HISTO>::add_mc_histo(HISTO* histo,std::string id)
{
	if(verbose) std::cout << " HH: add_mc_histo. Adding " << id << std::endl;
	
	if(fs_histos.find(id) != fs_histos.end()){
		std::cout << "ERROR: Histo with id: " << id << " already exists in fs_histo. Exiting" << std::endl;
		exit(1);
	}
	if(histo == NULL){
		std::cout << "ERROR: NULL histo: " << id << " .Exiting" << std::endl;
		exit(1);
	}		

	fs_mc_histo_names.push_back(id);
	fs_histos[id] = histo;
	
	//Initialise x if has not been initialised
	if(hh_observables.find(x_obs) == hh_observables.end() || hh_observables[x_obs] == NULL){
		initialise_observable(fs_histos[id]->GetXaxis()->GetXmin(),fs_histos[id]->GetXaxis()->GetXmax(),x_obs);
		hh_observables[x_obs]->setBins(fs_histos[id]->GetNbinsX());
	}
	if(class_is_2D() && (hh_observables.find(y_obs) == hh_observables.end() || hh_observables[y_obs] == NULL)){
		initialise_observable(fs_histos[id]->GetYaxis()->GetXmin(),fs_histos[id]->GetYaxis()->GetXmax(),y_obs);
		hh_observables[y_obs]->setBins(fs_histos[id]->GetNbinsY());
	}
}

template <class HISTO>
void HistoHandler<HISTO>::create_simultaneous_histos(std::vector<HISTO*> histos,int npseudo_exp)
{
	if(pseudodata_histos.size() > 0){
		std::cout << "ERROR: HistoHandler pseudodata_histos have already been filled." <<std::endl;
		exit(1);
	}

	if(histos.size() != 2*npseudo_exp){
		std::cout << "ERROR HistoHandler create_simultaneous_histos: histos vector should be exactly twice npseudoexp. \n size: " << histos.size() << ", npseudo_exp: " << npseudo_exp << std::endl;
		exit(1);
	}

	for(int i = 0;i < npseudo_exp;i++){
		std::vector<HISTO*> tmp_hvec;
		tmp_hvec[0] = histos[i];
		tmp_hvec[1] = histos[i+npseudo_exp];
		int iter_npseudoexp = i+1;
		std::ostringstream i_s; i_s << iter_npseudoexp;
		std::string id = "pseudodata_"+i_s.str();
		pseudodata_histos.push_back(create_simultaneous_histo(tmp_hvec,id));
	}
	
	//Don't need original histos anymore since simultaneous histo is used
	for(typename std::vector<HISTO*>::iterator h = histos.begin();
	    h != histos.end();h++){if(*h){delete *h; *h = NULL; }}
}

template <class HISTO>
HISTO* HistoHandler<HISTO>::create_simultaneous_histo(std::vector<HISTO*> histos,std::string id)
{
	if(verbose) std::cout << " FM: Creating simultaneous histo: "<< id << std::endl;

	if(histos.size() < 1){
		std::cout << "ERROR:HistoHandler;add_simultaneous_histo.Empty vector of histos " << std::endl;
		exit(1);
	}

	bool is_2D = class_is_2D();
	
	//first get this information to construct your final histogram
	int nbinsXtot = 0;
	for(typename std::vector<HISTO*>::iterator h = histos.begin();
	    h != histos.end();h++){
		if(*h == NULL){
			std::cout << "ERROR: NULL histo: " << id << " .Exiting" << std::endl;
			exit(1);
		}

		nbinsXtot += (*h)->GetNbinsX();
	}

	HISTO *sim_histo = initialise_sim_histo(histos[0], nbinsXtot,id);
	
	//Now fill the histogram. 
	int bin_offset = 0;
	double total_entries = 0;
	for(typename std::vector<HISTO*>::iterator histo = histos.begin();
	    histo != histos.end();histo++){
		total_entries += (*histo)->GetEntries();
		for(int i = 1;i <= (*histo)->GetNbinsX(); i++){
			if(is_2D){
				for(int j = 1;j <= (*histo)->GetNbinsY(); j++){
					sim_histo->SetBinContent(bin_offset+i,j, (*histo)->GetBinContent(i,j));
				}
			}else
				sim_histo->SetBinContent(bin_offset+i, (*histo)->GetBinContent(i));		    
		}
		bin_offset += (*histo)->GetNbinsX();
	}
	sim_histo->SetEntries(total_entries);

	if(verbose){
		std::cout << " Combined simultaneous pdf has " << sim_histo->GetNbinsX() << " x bins" << std::endl;
		std::cout << "Min X: " << sim_histo->GetXaxis()->GetXmin() << " max_x: " << sim_histo->GetXaxis()->GetXmax() << std::endl;
		if(is_2D) std::cout << "Nbins y " <<  sim_histo->GetNbinsY()  << " Min Y: " << sim_histo->GetYaxis()->GetXmin() << " max_y: " << sim_histo->GetYaxis()->GetXmax() << std::endl;
		std::cout << "Combined Histo Integral: " << sim_histo->Integral() << std::endl;
		std::cout << "Combined Histo Entries: " << sim_histo->GetEntries() << std::endl;

		for(typename std::vector<HISTO*>::iterator histo = histos.begin();
		    histo != histos.end();histo++){
			std::cout << "Input H x bins " << (*histo)->GetNbinsX() << " minX " << (*histo)->GetXaxis()->GetXmin();
			std::cout << " maxX " << (*histo)->GetXaxis()->GetXmax() << std::endl;
			if(is_2D) std::cout << "Input H y bins " << (*histo)->GetNbinsY() << " minY " << (*histo)->GetYaxis()->GetXmin() << " maxY " << (*histo)->GetYaxis()->GetXmax() << std::endl;
			std::cout << "Integral: " << (*histo)->Integral() << std::endl;

		}
	}
	return sim_histo;

}

template <>
TH1D* HistoHandler<TH1D>::initialise_sim_histo(TH1D *h, int nbinsXtot, std::string id)
{
	return new TH1D((id+"_histo").c_str(),(id+"_histo").c_str(),nbinsXtot,h->GetXaxis()->GetXmin(),h->GetXaxis()->GetBinWidth(1)*nbinsXtot);
}

template <>
TH2D* HistoHandler<TH2D>::initialise_sim_histo(TH2D *h, int nbinsXtot, std::string id)
{
	int nbinsY = h->GetNbinsY();
	return new TH2D((id+"_histo").c_str(),(id+"_histo").c_str(),nbinsXtot,h->GetXaxis()->GetXmin(),h->GetXaxis()->GetBinWidth(1)*nbinsXtot,nbinsY,h->GetYaxis()->GetXmin(),h->GetYaxis()->GetBinWidth(1)*nbinsY);
}

template <class HISTO>
void HistoHandler<HISTO>::set_pseudodata_histos(std::vector<HISTO*> histos)
{
	pseudodata_histos = histos;
}

template <class HISTO>
void HistoHandler<HISTO>::set_data_histo(HISTO *data_histo,std::string id)
{
	if(fs_histos.find("data") != fs_histos.end() && fs_histos["data"] != NULL){
		if(verbose) std::cout << "FM: deleting old data histo " << std::endl;
		delete fs_histos["data"];

		if(hh_datahists.find("data") != hh_datahists.end() && hh_datahists["data"])
			delete hh_datahists["data"];
	}
	
	fs_histos["data"] = data_histo;
       
}

template <class HISTO>
//All the above must be set first. this should be called from within fitter
//Initialise variables which will not vary depending with different analyses applied
void HistoHandler<HISTO>::initialise_nscaled()
{
        std::cout.precision(6);
	if(verbose) std::cout << "HH: Initialising nscaled. Getting integrals: " << std::endl;
	for(std::vector<std::string>::iterator id = fs_mc_histo_names.begin();
	    id != fs_mc_histo_names.end();id++){
		nscaled_evts[*id] =  fs_histos[*id]->Integral();
		if(verbose) std::cout << *id << " " << nscaled_evts[*id] << "; ";
	}
	if(verbose) std::cout << "" << std::endl;

	if(fs_histos.find("data") != fs_histos.end() && fs_histos["data"] != NULL){
		nscaled_evts["data"] =  fs_histos["data"]->Integral();
	}
}

template <class HISTO>
void HistoHandler<HISTO>::initialise_datahists()
{
	if (verbose) std::cout << "HH: Creating RooDataHists " << std::endl; 
	delete_datahists(false);
		
	//RooDataHist constructs a binned data hist from the TH1. Must ensure data hist exist for entire lifespan of histpdf
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		if(verbose) std::cout << "Id: " << *id <<  std::endl;

		hh_datahists[*id] = new RooDataHist((*id+"_hist").c_str(),"Binned Input Histogram",observable_argset(),fs_histos[*id]);
		if(verbose)std::cout << "HH: created datahist " << std::endl;

	}
	if(fs_histos.find("data") != fs_histos.end() && fs_histos["data"] != NULL){
		std::cout << " HH: |||||||||||| initialise for data template |||||||||||||||| " << std::endl;
		std::cout << " HH: data hist integral: " << fs_histos["data"]->Integral() << std::endl;
		
		hh_datahists["data"] = new RooDataHist("data_hist","Binned Input Histogram",observable_argset(),fs_histos["data"]);
	}

	if(pseudodata_histos.size() > 0){
		int i = 1;
		for(typename std::vector<HISTO* >::iterator h = pseudodata_histos.begin();
		    h != pseudodata_histos.end();h++){
			std::ostringstream i_s; i_s << i;
			std::string id = "pseudodata_"+i_s.str();
			hh_pseudodata_datahists.push_back(new RooDataHist(id.c_str(),"Binned Input Histogram",observable_argset(),*h));
			i++;
		}
	}

}

template <class HISTO>
RooAddPdf* HistoHandler<HISTO>::smeared_mc_template(FitSetup *fs, std::vector<std::string> unsmeared_patterns)
{
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		bool smear_this_id = true;
		for(std::vector<std::string>::iterator no_smear = unsmeared_patterns.begin();
		    no_smear != unsmeared_patterns.end(); no_smear++){
			if(id->find(*no_smear) != std::string::npos){
				smear_this_id = false;
				break;
			}
		}
		if(!smear_this_id){
			smear_datahists[*id] = (RooDataHist*) hh_datahists[*id]->Clone();
		}else{
			//TEST manual smearing
			smear_histos[*id] = (HISTO*) fs_histos[*id]->Clone();
			double integral = smear_histos[*id]->Integral();
			double entries = smear_histos[*id]->GetEntries();
			double scale_up = entries/integral;
			double scale_down = integral/entries;
			bool v = false;
			for(int i = 1; i < smear_histos[*id]->GetNbinsX()+1;i++){
				if(v)	std::cout << "before smearing: " << smear_histos[*id]->GetBinContent(i) << std::endl;
				double bin_unscaled_entries = smear_histos[*id]->GetBinContent(i)*scale_up;

				if(v)std::cout << "Expected var entries: " << bin_unscaled_entries<< " +/- " << sqrt(bin_unscaled_entries) << std::endl;
				if(v)std::cout << "Expected var scaled: " << smear_histos[*id]->GetBinContent(i) << " +/- " << sqrt(bin_unscaled_entries)*scale_down << std::endl;
				double bin_nev_unscaled_rndm = gen_random->PoissonD(bin_unscaled_entries);
				//double bin_nev_unscaled_rndm = gen_random->Gaus(bin_unscaled_entries,sqrt(bin_unscaled_entries)/20);
				double bin_nev_scaled_rndm = bin_nev_unscaled_rndm*scale_down;
				
				
				smear_histos[*id]->SetBinContent(i,bin_nev_scaled_rndm);
				
					if(v)std::cout << " after smearing: " << smear_histos[*id]->GetBinContent(i) << std::endl;

			} 
			if(v)std::cout << "b4 smearing integral: " << integral << " after smear: " << smear_histos[*id]->Integral() <<  std::endl;
 			if(v)std::cout << "b4 smearing entries: " << entries << " after smear: " << smear_histos[*id]->GetEntries() <<  std::endl;
			smear_datahists[*id]=new RooDataHist((*id+"_hist").c_str(),"Binned Input Histogram",observable_argset(),smear_histos[*id]);

			//smear_datahists[*id] = pdfs[*id]->generateBinned(observable_argset(),histo(*id,true)->GetEntries(),Extended());
		}
		smear_pdfs[*id] = new RooHistPdf((*id+"smear_pdf").c_str(),(*id+"smear_pdf").c_str(),observable_argset(),*smear_datahists[*id]);

	}

	RooArgList *template_pdfs = new RooArgList();
	RooArgList *template_nfit_evts = new RooArgList();
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		template_pdfs->add(*smear_pdfs[*id]);
	}
	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){
		if(fs->nfit(*id,false) != NULL)
			template_nfit_evts->add(*(fs->nfit(*id,true)));
		else if(fs->nfit_form(*id,false) != NULL)
			template_nfit_evts->add(*(fs->nfit_form(*id,true)));
		else{
			std::cout << "ERROR: no nfit or nfit form for: " << *id << std::endl;
			exit(1);
		}
	}

	//Add the pdf's. input for mc study
	smear_templates["MC"] = new RooAddPdf((fit_id+"_template_smeared").c_str(), (fit_id+" template smeared").c_str(), *template_pdfs,*template_nfit_evts);

	return smear_templates["MC"];
}


/************************ Variable Access ****************************/

template <class HISTO>
RooArgSet HistoHandler<HISTO>::observable_argset()
{
	RooArgSet obs;
	int i = 0;
	for(std::map<std::string,RooRealVar*>::iterator o = hh_observables.begin();
	    o != hh_observables.end();o++){
		if(verbose) std::cout << "HH: Obs arg: " << i << " id: " << o->first << std::endl;
		obs.add(*(o->second));
		i++;
	}
	return obs;
}

template <class HISTO>
std::string HistoHandler<HISTO>::observable_id(std::string xy)
{
	if(xy == "y")
		return y_obs;
	else
		return x_obs;
}

template <class HISTO>
HISTO* HistoHandler<HISTO>::histo(std::string id, bool need)
{
	if(fs_histos.find(id) == fs_histos.end()){
		if(!need)
			return NULL;
		else {
			std::cout << " ERROR:HistoHandler, No histo entry for " << id << " in " << fit_id <<  std::endl;
			exit(1);
		}
	}
	else if(fs_histos[id] == NULL){
		if(!need)
			return NULL;
		else{ 
			std::cout << " ERROR:HistoHandler, NULL histo entry for " << id <<" in " << fit_id <<  std::endl;
			exit(1);
		}
	}

	return fs_histos[id];	

}


/************************** Delete ******************************/

template <class HISTO>
void HistoHandler<HISTO>::delete_all(bool delete_data)
{
	std::cout << "Fs: Deleting everything, ";
	if(delete_data) std::cout << " including variables for data" << std::endl;
	else std::cout << " except data variables" << std::endl;
	delete_histos(delete_data);std::cout << " histos deleted " << std::endl;
	delete_observables();std::cout << " observables deleted " << std::endl;
	delete_datahists(delete_data);std::cout << " datahists deleted " << std::endl;

}

template <class HISTO>
void HistoHandler<HISTO>::delete_observables()
{
	for(std::map<std::string,RooRealVar*>::iterator iter = hh_observables.begin();iter != hh_observables.end();++iter){
		delete hh_observables[iter->first];
		hh_observables[iter->first] = NULL;
	}
	hh_observables.clear();
}

template <class HISTO>
void HistoHandler<HISTO>::delete_histos(bool delete_data)
{
	for(typename std::map<std::string,HISTO*>::iterator iter = fs_histos.begin();iter != fs_histos.end();++iter){
		if(!delete_data && iter->first == "data")
			continue;
		else
			delete fs_histos[iter->first];
	}
	if(delete_data) fs_histos.clear();

	for(typename std::vector<HISTO* >::iterator h = pseudodata_histos.begin();
	    h != pseudodata_histos.end();h++){if(*h){delete *h; *h = NULL;}}

}
template <class HISTO>
void HistoHandler<HISTO>::delete_datahists(bool delete_data)
{
	
	for(std::map<std::string,RooDataHist*>::iterator iter = hh_datahists.begin();iter != hh_datahists.end();++iter){
		if(iter->first == "data"){
			continue;
		}else{
			delete hh_datahists[iter->first];
			hh_datahists[iter->first] = NULL;
		}
	}
	if(delete_data) hh_datahists.clear();

	for(std::vector<RooDataHist* >::iterator h = hh_pseudodata_datahists.begin();
	    h != hh_pseudodata_datahists.end();h++){if(*h){delete *h; *h = NULL;}}
}

template <class HISTO>
void HistoHandler<HISTO>::delete_smeared_template()
{
	for(typename std::map<std::string,HISTO*>::iterator iter = smear_histos.begin();iter != smear_histos.end();++iter){
		delete smear_histos[iter->first];
		smear_histos[iter->first] = NULL;
	}
	smear_histos.clear();
	for(std::map<std::string,RooDataHist*>::iterator iter = smear_datahists.begin();iter != smear_datahists.end();++iter){
		delete smear_datahists[iter->first];
		smear_datahists[iter->first] = NULL;
	}
	smear_datahists.clear();
	for(std::map<std::string,RooHistPdf*>::iterator iter = smear_pdfs.begin();iter != smear_pdfs.end();++iter){
		delete smear_pdfs[iter->first];
		smear_pdfs[iter->first] = NULL;
	}
	smear_pdfs.clear();
	for(std::map<std::string,RooAddPdf*>::iterator iter = smear_templates.begin();iter != smear_templates.end();++iter){
			delete smear_templates[iter->first];
			smear_templates[iter->first] = NULL;

	}
	smear_templates.clear();
}

template <>
bool HistoHandler<TH1D>::class_is_2D()
{
	return false;
}

template <>
bool HistoHandler<TH2D>::class_is_2D()
{
	return true;
}

template class HistoHandler<TH1D>;
template class HistoHandler<TH2D>;
