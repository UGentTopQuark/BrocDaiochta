#!/usr/bin/perl -w

use Getopt::Std;
#use strict;

getopts("p:");

my $fit_output_directory = "./output/";
my $output_dir = "./pdf_output/";
my $output_txtfile_name = "out.txt";

die "need input pattern..." unless(defined($opt_p));
my $input_pattern = $opt_p;
my $default_pattern = "pdf_sys_study";
my @directories = ();

my @txt_files_created = ();

my $master_out_file = $output_dir.$input_pattern."_".$default_pattern.".txt";

# find input directories to process
opendir(DIR, "$fit_output_directory") or die "can't open directory: $fit_output_directory\n";
while(defined(my $cur_dir = readdir(DIR))){
    push(@directories, $cur_dir) if($cur_dir =~ /$input_pattern/ && $cur_dir =~ /$default_pattern/);
}
closedir(DIR);


foreach my $dir(@directories){
	my $file_to_open = "$fit_output_directory/$dir/$output_txtfile_name";
	print "Opening ".$file_to_open."\n";
	open(FILE, "<$file_to_open") or die "can't open file: $file_to_open\n";
	my @input = <FILE>;
	close(FILE);

	analyse_input_file(\@input, $dir);
}

my @master_lines_out = ();
foreach my $file(@txt_files_created){

    
    print "python template_fit_pdf.py $file\n";
    $pdf_script_output = `python template_fit_pdf.py $file`;
    
    my $separator = $file."  \*\*\*\*\*\*\*\*\*\*\*\n";
    push(@master_lines_out, $separator);
    push(@master_lines_out, $pdf_script_output."\n");

}
#now write to output file
`rm $master_out_file` if(-e $master_out_file);
open(FILE, ">$master_out_file");
print FILE @master_lines_out;
close FILE;


sub analyse_input_file
{
	my ($input_ref, $dir) = @_;

	my @input = @$input_ref;

	
	my $start_block_def = '\*\*\* Raw Cross Sections \*\*\*';
	my $start_block_sys_e = '\*\*\* Raw Cross Sections \(Nominal N,Sys sel Eff\.';
	my $start_block_sys_N = '\*\*\* Raw Cross Sections \(Nominal sel Eff\.';
	my $end_block = '\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*';

	my $output_txt_nom = $output_dir.$dir."_nominal.txt";
	my $output_txt_sys_e = $output_dir.$dir."_sys_e.txt";
	my $output_txt_sys_N = $output_dir.$dir."_sys_N.txt";

	my @lines_nom = ();
	my @lines_sys_e = ();
	my @lines_sys_N = ();

	foreach my $line(@input){
	    #print $line."\n";

	    if($line =~ /$start_block_def/){
		$process_nom = 1;
		next;
	    } 
	    elsif($line =~ /$start_block_sys_e/){
		$process_sys_e = 0;
		next;
	    } 
	    elsif($line =~ /$start_block_sys_N/){
		$process_sys_N = 0;
		next;
	    } 
	    next if(!$process_nom && !$process_sys_e && !$process_sys_N);

		if($line =~ /$end_block/){
		    $process_nom = 0; 
		    $process_sys_e = 0;
		    $process_sys_N = 0;
		    next;
		}

		if($process_nom){
		    #print "found Nominal \n ";
		    push(@lines_nom, $line)
		}
		if($process_sys_e){
		    #print "found sys e \n ";
		    push(@lines_sys_e, $line)
		}
		if($process_sys_N){
		    #print "found sys N \n ";
		    push(@lines_sys_N, $line)
		}
	}
        #now write to output file
        `rm $output_txt_nom` if(-e $output_txt_nom);
        open(FILE, ">$output_txt_nom");
        print FILE @lines_nom;
        close FILE;
        #now write to output file
        `rm $output_txt_sys_e` if(-e $output_txt_sys_e);
        open(FILE, ">$output_txt_sys_e");
        print FILE @lines_sys_e;
        close FILE;
        #now write to output file
        `rm $output_txt_sys_N` if(-e $output_txt_sys_N);
        open(FILE, ">$output_txt_sys_N");
        print FILE @lines_sys_N;
        close FILE;

	push(@txt_files_created,$output_txt_nom);
	push(@txt_files_created,$output_txt_sys_e);
	push(@txt_files_created,$output_txt_sys_N);


}
