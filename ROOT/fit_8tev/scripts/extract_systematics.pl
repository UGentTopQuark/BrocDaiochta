#!/usr/bin/perl

#use strict;
#use warnings;

die "usage: $0 <config file>\n" unless(defined($ARGV[0]));

my $cfg_name = $ARGV[0];

die "can't find $cfg_name, exiting..." unless(-e $cfg_name);

do "$cfg_name";

###################################################
###### END CONFIGURATION
###################################################

my @directories = ();
my %cross_section;
my %sys;

# find input directories to process
foreach my $pattern(@patterns){
	opendir(DIR, "$fit_output_directory") or die "can't open directory: $fit_output_directory\n";
	while(defined(my $cur_dir = readdir(DIR))){
		push(@directories, $cur_dir) if($cur_dir =~ /$pattern/);
	}
	closedir(DIR);
}

# read all .txt files from input directories
my $output_txtfile_name = "out.txt";
foreach my $dir(@directories){
	my $file_to_open = "$fit_output_directory/$dir/$output_txtfile_name";
	open(FILE, "<$file_to_open") or die "can't open file: $file_to_open\n";
	my @input = <FILE>;
	close(FILE);

	my $datamc;
	$datamc = "data" if($dir =~ /data/);
	$datamc = "mc" if($dir =~ /mc/);

	analyse_input_file(\@input, \%sys, $datamc);
}

# sum of squares
my %ss_up;
my %ss_down;

foreach my $datamc (@sets){
	$ss_up{$datamc} = 0;
	$ss_down{$datamc} = 0;

	if($datamc eq "mc"){
		$selected_events_fit{$channel}{$datamc} = $selected_events_theory{$channel};
	}elsif($datamc eq "data"){
		$cross_section{"data"} = $cross_section_theory*$selected_events_fit{$channel}{$datamc}/$selected_events_theory{$channel};
		$cross_section{"mc"} = $cross_section_theory;
	}

	my @chans = ("e", "mu","emu");
	foreach $chan (@chans){
		while(my ($systematic, $unc) = each %{$additional_systematics{$chan}}){

			my $sigma_up = 0.;
			my $sigma_down = 0.;
			# systematic appears for muons only
			if($channel eq "mu" || $channel eq "e"){
				$sigma_up = ($selected_events_fit{$channel}{$datamc}/$selected_events_theory{$channel} * $cross_section_theory/(1+$unc)) - $cross_section{$datamc};
				$sigma_down = ($selected_events_fit{$channel}{$datamc}/$selected_events_theory{$channel} * $cross_section_theory/(1-$unc)) - $cross_section{$datamc};
			}
			elsif($channel eq "emu"){
				if($chan eq "e"){
					$sigma_up = ($selected_events_fit{"emu"}{$datamc}*$cross_section_theory / ((1+$unc)*$selected_events_theory{"e"}+$selected_events_theory{"mu"}) - $cross_section{$datamc});
					$sigma_down = ($selected_events_fit{"emu"}{$datamc}*$cross_section_theory / ((1-$unc)*$selected_events_theory{"e"}+$selected_events_theory{"mu"}) - $cross_section{$datamc});
				}elsif($chan eq "mu"){
					$sigma_up = ($selected_events_fit{"emu"}{$datamc}*$cross_section_theory / ($selected_events_theory{"e"}+(1+$unc)*$selected_events_theory{"mu"}) - $cross_section{$datamc});
					$sigma_down = ($selected_events_fit{"emu"}{$datamc}*$cross_section_theory / ($selected_events_theory{"e"}+(1-$unc)*$selected_events_theory{"mu"}) - $cross_section{$datamc});
				}elsif($chan eq "emu"){
				    $sigma_up = ($selected_events_fit{"emu"}{$datamc}*$cross_section_theory / ((1+$unc)*$selected_events_theory{"e"}+(1+$unc)*$selected_events_theory{"mu"}) - $cross_section{$datamc});
				    $sigma_down = ($selected_events_fit{"emu"}{$datamc}*$cross_section_theory / ((1-$unc)*$selected_events_theory{"e"}+(1-$unc)*$selected_events_theory{"mu"}) - $cross_section{$datamc});
				}
			}
			$sys{$systematic}{$datamc}{"UP"} = $sigma_up;
			$sys{$systematic}{$datamc}{"DOWN"} = $sigma_down;
		}
	}
}

print "\n\n";
foreach my $systematic (keys %sys){
	my $sys_name = $systematic;

	$sys_name = $dictionary{$sys_name} if(defined($dictionary{$sys_name}));

	print "$sys_name ";

	foreach my $datamc (@sets){
		if(defined($sys{$systematic}{$datamc}{"UP"}) && defined($sys{$systematic}{$datamc}{"DOWN"})){
			printf("& \$ %+.1f \$ & \$ %+.1f \$", $sys{$systematic}{$datamc}{"UP"}, $sys{$systematic}{$datamc}{"DOWN"});

			if($sys{$systematic}{$datamc}{"UP"} > 0 && $sys{$systematic}{$datamc}{"DOWN"} > 0){
				if($sys{$systematic}{$datamc}{"UP"} > $sys{$systematic}{$datamc}{"DOWN"}){
					$ss_up{$datamc} += $sys{$systematic}{$datamc}{"UP"}*$sys{$systematic}{$datamc}{"UP"};
				}else{
					$ss_up{$datamc} += $sys{$systematic}{$datamc}{"DOWN"}*$sys{$systematic}{$datamc}{"DOWN"};
				}
			}elsif($sys{$systematic}{$datamc}{"UP"} < 0 && $sys{$systematic}{$datamc}{"DOWN"} < 0){
				if($sys{$systematic}{$datamc}{"UP"} < $sys{$systematic}{$datamc}{"DOWN"}){
					$ss_down{$datamc} += $sys{$systematic}{$datamc}{"UP"}*$sys{$systematic}{$datamc}{"UP"};
				}else{
					$ss_down{$datamc} += $sys{$systematic}{$datamc}{"DOWN"}*$sys{$systematic}{$datamc}{"DOWN"};
				}
			}elsif($sys{$systematic}{$datamc}{"UP"} > 0 && $sys{$systematic}{$datamc}{"DOWN"} < 0){
				$ss_up{$datamc} += $sys{$systematic}{$datamc}{"UP"}*$sys{$systematic}{$datamc}{"UP"};
				$ss_down{$datamc} += $sys{$systematic}{$datamc}{"DOWN"}*$sys{$systematic}{$datamc}{"DOWN"};
			}elsif($sys{$systematic}{$datamc}{"UP"} < 0 && $sys{$systematic}{$datamc}{"DOWN"} > 0){
				$ss_up{$datamc} += $sys{$systematic}{$datamc}{"DOWN"}*$sys{$systematic}{$datamc}{"DOWN"};
				$ss_down{$datamc} += $sys{$systematic}{$datamc}{"UP"}*$sys{$systematic}{$datamc}{"UP"};
			}
		}else{
			print " & & ";
		}
	}
	print "\\\\\n";
}

print "\n\n-------------------------\n";
foreach my $datamc (@sets){
	my $up_sys = sqrt($ss_up{$datamc});
	my $down_sys = sqrt($ss_down{$datamc});

	my $up_sys_percent = sqrt($ss_up{$datamc})/$cross_section{$datamc}*100;
	my $down_sys_percent = sqrt($ss_down{$datamc})/$cross_section{$datamc}*100;
	printf("$datamc: %.1f +%.1f/-%.1f pb (+%.1f%% /-%.1f%%)\n", $cross_section{$datamc}, $up_sys, $down_sys, $up_sys_percent, $down_sys_percent);
	printf("$datamc: sum of squares: +%.1f/-%.1f\n", $ss_up{$datamc}, $ss_down{$datamc});
}
print "-------------------------\n\n";

sub analyse_input_file
{
	my ($input_ref, $sys_ref, $datamc) = @_;

	my @input = @$input_ref;

	my $process_block = 0;
	my $start_block = '\*\*\* Rel\. Cross Section \*\*\*';
	my $end_block = '\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*';
	foreach my $line(@input){
		$line =~ s/_down/DOWN/g;
		$line =~ s/_up/UP/g;

		$process_block = 1 if($line =~ /$start_block/);
		next if(!$process_block);
		$process_block = 0 && exit if($line =~ /$end_block/);

		if($line =~ /(\w+)(DOWN|UP)\s*\&\s*\$\s*([-\d\.]+)/){
			$sys_ref->{$1}{$datamc}{$2} = $3;
		}
	}
}
