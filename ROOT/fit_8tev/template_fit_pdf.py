from decimal import *
import sys
import fileinput
import glob
import numpy as np

if len(sys.argv) < 2:
    sys.exit('usage: \n python template_fit_pdf.py <dir>')

infile = sys.argv[1]
f = open(infile, 'r')
#    elif sys.argv[2] == "abseta":
#        f = open('input/template_fit_pdf_input_e_abseta', 'r')
#elif sys.argv[1] == "mu":
#    if sys.argv[2] == "topmass":
#        f = open('input/template_fit_pdf_input_mu_topmass', 'r')
#    elif sys.argv[2] == "abseta":
#        f = open('input/template_fit_pdf_input_mu_abseta', 'r')
#else:
#    print "Please input correct branching channel"
#
pdf_array = []
ttbar_array = []

for line in f:
    a = line.strip('\n')
    c = [float(s) for s in a.split()]
    pdf_array.append(c)
f.close()

for i in range(0,45):
    ttbar_array.append(pdf_array[i][0])

print "----------------------------"    
print "Calculating weights..."

rawpdf_max  = np.sqrt(sum( ((max(x-ttbar_array[0],y-ttbar_array[0],0))**2 for x,y in zip(ttbar_array[1::2],ttbar_array[2::2]))))
rawpdf_min  = np.sqrt(sum( ((max(ttbar_array[0]-x,ttbar_array[0]-y,0))**2 for x,y in zip(ttbar_array[1::2],ttbar_array[2::2]))))

sigpdf_max = rawpdf_max/ttbar_array[0]
sigpdf_min = rawpdf_min/ttbar_array[0]

print "----------------------------"    

print "X_0   = " + str(ttbar_array[0])
print "X_max = " + str(sigpdf_max)
print "X_min = " + str(sigpdf_min)
print "Abs X_max = " + str(rawpdf_max)
print "Abs X_min = " + str(rawpdf_min)
