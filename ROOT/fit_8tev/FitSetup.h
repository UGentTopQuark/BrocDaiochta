#ifndef FITSETUP_H
#define FITSETUP_H

#include "CanvasHolder/Canvas_Holder.h"

#include <RooRandom.h>
#include <RooRandomizeParamMCSModule.h>
#include <RooGaussian.h>
#include <RooPoisson.h>
#include <RooGlobalFunc.h>
#include <RooRealVar.h>
#include <RooChi2Var.h>
#include <RooPoisson.h>
#include <RooGaussian.h>
#include <RooPlot.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooExponential.h>
#include <RooAddPdf.h>
#include <RooProdPdf.h>
#include <RooFitResult.h>
#include <RooArgusBG.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooLandau.h>
#include <RooMinuit.h>
#include <RooMCStudy.h>
#include <RooHistPdf.h>
#include <RooArgSet.h>
#include <RooAbsArg.h>
#include <RooArgList.h>
#include <RooConstVar.h>
#include <RooWorkspace.h>
#include <RooGenFitStudy.h>
#include <RooStudyManager.h>
#include "RooSimultaneous.h"
#include "RooCategory.h"
#include "TRandom.h"
#include "TRandom3.h"


class FitSetup{
        public:
	        FitSetup();
	        ~FitSetup();
		void set_fit_id(std::string fit_id);
		void set_observable_ids(std::string x = "x",std::string y = "");//< Set if you want to give observable a name other than x
		void set_template_shape_names(std::vector<std::string> names); 
		void set_template_norm_names(std::vector<std::string> names);
		void set_gaussian_constraints(std::vector<std::string> constraints);//<Apply gauss constraints to nfit evts
		
		//Initialised in HistoHandler
		inline void set_nscaled(std::map<std::string, double> nscaled){nscaled_evts = nscaled;}
		inline void set_datahists(std::map<std::string, RooDataHist*> dh, std::vector<RooDataHist* > pdh){datahists = dh; pseudodata_datahists = pdh;}
		inline void set_observables(std::map<std::string,RooRealVar* > observables){fs_observables = observables;}

		/**Roo needs approximate ranges of output n events per histo for fitting
		 These must be set before anything else is initialised**/
		void set_ranges(std::map<std::string,int> lranges, std::map<std::string,int> hranges);

		void initialise_nfit_mc(); 
		void initialise_nfit_form_mc(std::map<std::string,RooRealVar*> fit1_vars,std::map<std::string,double> fit1_nscaled);
		void initialise_nfit_data();		  

		void initialise_mc_template(); //<Create template from pdfs
		void initialise_for_data_template(); //Repeat 3 steps above if data histo is separate

		/**These variables might change during fitting so need to call 
		   here each time you access an element**/
		RooDataHist* datahist(std::string id, bool need = true);
		RooDataHist* pseudodata_datahist(int iter_npseudoexp);
		RooArgSet observable_argset();
		std::string observable_id(std::string xy);
		std::map<std::string,RooRealVar* > observables();
		RooRealVar* nfit(std::string id, bool need = true,bool quiet = false);
		std::map<std::string,RooRealVar*> nfit_map();
		RooFormulaVar* nfit_form(std::string id, bool need = true);
		RooFormulaVar* rooform_constraint(std::string id, bool need = true,bool quiet = false);
		double nscaled(std::string id, bool need = true);
		std::map<std::string, double> nscaled_map();
		int range_low(std::string id);
		int range_high(std::string id);
		RooAddPdf* get_template(std::string id);
		RooHistPdf* pdf(std::string id);
		bool constrained(std::string c);		
		double constraint_var(std::string v);//<Returns mean or sigma used to initialise constraint. Call with arguement *constraint_id+"_mean" or "_sigma".
		std::string template_name_for_constraint(std::string c);
		bool ranges_set();
		std::vector<std::string> template_shape_names();
		std::vector<std::string> template_norm_names();
		std::vector<std::string> gauss_constraints_used();
		std::string fit_id();

		void delete_all(bool delete_data = true);
        private:
		void initialise_avail_gauss_constraints(); 
		void initialise_form_const_vars(std::map<std::string,double> fit1_nscaled);
		void initialise_histpdfs();

		std::string fs_fit_id;
		std::string x_obs;
		std::string y_obs;

		std::vector<std::string> fs_template_shape_names;
		std::vector<std::string> fs_template_norm_names;
		std::vector<std::string> fs_mc_histo_names;
		std::vector<std::string> fs_gauss_constraints;//<constraints to be used in fit
		
		std::map<std::string,RooRealVar*> fs_observables;
		std::map<std::string,RooRealVar*> nfit_evts; //<nevts from fit 
		std::map<std::string,RooFormulaVar*> nfit_form_evts; //<nevts from fit when using formula var 
		std::map<std::string,RooConstVar*> form_const_vars;  
		std::map<std::string,double> nscaled_evts; //<from integral of histogram
		std::map<std::string,double> h_ranges_low;
		std::map<std::string,double> h_ranges_high;
		std::map<std::string,double> fs_constraint_vars; //<Contains mean and sigmas used to initialise gaussian constraints. Will be used to randomise mean of constraint per fit.

		std::map<std::string, RooDataHist* > datahists;
		std::map<std::string, RooHistPdf* > pdfs;
		std::map<std::string, RooAddPdf* > templates;
		TRandom3 *gen_random;

		std::vector<RooDataHist* > pseudodata_datahists;

		std::vector<std::string> avail_gauss_constraints;//<all available gauss constraints.
		std::map<std::string, RooFormulaVar* > fs_rooform_constraint;//<Needed for wj/zj constraint
		std::map<std::string,std::string> fs_template_names_for_constraint;//<Incase template name is different to constriaint e.g. wzj_matching

		RooArgSet *fs_external_constraint_argset;
		//RooArgSet *fs_obs_argset;

		bool fs_ranges_set;

		/**These functions are used individually when new mc histos are set
		   At the end of the study from delete_all() they are all called with data deleted**/
		void delete_nfit_evts(bool delete_data = false);
		void delete_pdfs(bool delete_data = false);
		void delete_templates(bool delete_data = false);
		void delete_gauss_constraints();

		static const bool verbose = true;

};

#endif
