#!/usr/bin/perl

my $dir = "20120106_0_70_fit_template_mu_wo_pdf_rw_jes_jer";
my @datasets = ("Data","QCD", "Wjets", "Zjets", "TtChan", "TsChan", "TtWChan");
my @masses = ("mass161","mass163","mass166","mass169","mass175","mass178","mass181","mass184");


foreach my $ds (@datasets){
    foreach my $ms (@masses){
	print "cp $dir/${ds}_plots.root $dir/${ds}_${ms}.root\n";
	`cp $dir/${ds}_plots.root $dir/${ds}_${ms}.root`;
    }
}
