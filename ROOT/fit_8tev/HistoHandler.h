#ifndef HISTOHANDLER_H
#define HISTOHANDLER_H

#include "TF1.h"
#include "TH1D.h"
#include "TF2.h"
#include "TH2D.h"
#include "TH2.h"
#include "TFile.h"
#include "CanvasHolder/Canvas_Holder.h"
#include <iostream>
#include <sstream>

#include <RooRandom.h>
#include <RooRandomizeParamMCSModule.h>
#include <RooGaussian.h>
#include <RooPoisson.h>
#include <RooGlobalFunc.h>
#include <RooRealVar.h>
#include <RooChi2Var.h>
#include <RooPoisson.h>
#include <RooGaussian.h>
#include <RooPlot.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooExponential.h>
#include <RooAddPdf.h>
#include <RooProdPdf.h>
#include <RooFitResult.h>
#include <RooArgusBG.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooLandau.h>
#include <RooMinuit.h>
#include <RooMCStudy.h>
#include <RooHistPdf.h>
#include <RooArgSet.h>
#include <RooAbsArg.h>
#include <RooArgList.h>
#include <RooConstVar.h>
#include <RooWorkspace.h>
#include <RooGenFitStudy.h>
#include <RooStudyManager.h>
#include "RooSimultaneous.h"
#include "RooCategory.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "FitSetup.h"

template <class HISTO>
class HistoHandler{
        public:
	        HistoHandler();
	        ~HistoHandler();
		void set_observable_ids(std::string x = "x",std::string y = "");//< Set if you want to give observable a name other than x
		void set_template_shape_names(std::vector<std::string> names); 
		void set_template_norm_names(std::vector<std::string> names);

		void add_mc_histo(HISTO* histo,std::string id);
		HISTO* create_simultaneous_histo(std::vector<HISTO*> histos,std::string id);
		void set_data_histo(HISTO *data_histo,std::string id = "data");

		void create_simultaneous_histos(std::vector<HISTO*> histos,int npseudo_exp);//for separate pseudodata read in
		void set_pseudodata_histos(std::vector<HISTO*> histos);

		/**Nscaled is from input histos. used to initialise nfit events
		   nfit will be altered by the study.
		   \note Needs to be set before any nfit is initialised.**/
		void initialise_nscaled();
		void initialise_datahists();	  

		/**These variables might change during fitting so need to call 
		   here each time you access an element**/
		HISTO* histo(std::string id, bool need = true);

		inline std::map<std::string, RooDataHist*> datahists(){return hh_datahists;}
		inline std::vector<RooDataHist*> pseudodata_datahists(){return hh_pseudodata_datahists;}
		RooArgSet observable_argset();
		std::string observable_id(std::string xy);
		inline std::map<std::string,RooRealVar* > observables(){return hh_observables;}
		inline std::map<std::string, double> nscaled_map(){return nscaled_evts;}

		RooAddPdf* smeared_mc_template(FitSetup *fs, std::vector<std::string> unsmeared_patterns);
		
		void delete_all(bool delete_data = true);
		void delete_smeared_template();
        private:
		void initialise_observable(double low_range, double high_range, std::string description = "x variable"); //<observable being studied. set after it has a range
		bool class_is_2D();
		HISTO* initialise_sim_histo(HISTO* h, int nbinsX, std::string id);

		std::string fit_id;
		std::string x_obs;
		std::string y_obs;

		std::vector<std::string> fs_template_shape_names;
		std::vector<std::string> fs_template_norm_names;
		std::vector<std::string> fs_mc_histo_names;
		
		std::map<std::string, HISTO*> fs_histos;//<Input histos.

		std::map<std::string,RooRealVar*> hh_observables;
		std::map<std::string,double> nscaled_evts; //<from integral of histogram

		std::map<std::string, RooDataHist* > hh_datahists;
		std::map<std::string, HISTO* > smear_histos;
		std::map<std::string, RooDataHist* > smear_datahists;
		std::map<std::string, RooHistPdf* > smear_pdfs;
		std::map<std::string, RooAddPdf* > smear_templates;
		TRandom3 *gen_random;

		std::vector<HISTO* > pseudodata_histos;
		std::vector<RooDataHist* > hh_pseudodata_datahists;

		/**These functions are used individually when new mc histos are set
		   At the end of the study from delete_all() they are all called with data deleted**/
		void delete_observables();
		void delete_histos(bool delete_data = false);
		void delete_datahists(bool delete_data = false);

		static const bool verbose = true;

};

#endif
