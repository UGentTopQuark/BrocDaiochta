#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "CanvasHolder/Canvas_Holder.h"
#include <iostream>

int main(int argc, char **argv)
{
	const int npoints = 9;
	double mass[npoints];
	double mass_err[npoints];
	double cs[npoints];
	double cs_err_u[npoints];
	double cs_err_d[npoints];

	double mt_kidonakis[49] = {141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189};
        double mt_kidonakis_err_pl[49] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        double mt_kidonakis_err_mi[49] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        double cs_kidonakis_err_pl[49] = {48.6590752583729, 46.9775521392931, 45.3485766176845, 43.8036772520299, 42.2797969253638, 40.8505022741459, 39.4737552203993, 38.1495557641239, 36.8673943858255, 35.6482901244926, 34.4712239411368, 33.3467053552521, 32.243205808356, 31.1922538589311, 30.1728304679889, 29.205954674518, 28.2811169590241, 27.3772982825187, 26.505008164496, 25.6642466049561, 24.8655231233932, 24.0667996418302, 23.3206237577386, 22.6164859516239, 21.9123481455092, 21.2502484173715, 20.5881486892338, 19.9680870390731, 19.3690444279009, 18.780511336223, 18.2129972835335, 17.6770117893268, 17.1515358146144, 16.6470788788904, 16.163640982155, 15.6802030854195, 15.2282937471668, 14.7868939284084, 14.3665131486384, 13.9461323688684, 13.546770628087, 13.1579184067998, 12.790085224501, 12.4222520422023, 12.0754378988921, 11.7391332750761, 11.4238476902487, 11.098052585927, 10.7932765205937};
        double cs_kidonakis_err_mi[49] = {43.6449645892857, 42.1367152730253, 40.675598747898, 39.2898946885837, 37.9230437457227, 36.6410318269014, 35.4061526992132, 34.2184063626581, 33.0683662590095, 31.9748855047207, 30.9191109833384, 29.9104692530893, 28.9206806392934, 27.9780248166306, 27.0636486686478, 26.196405311798, 25.3668681878548, 24.5561841803648, 23.7737798475547, 23.0196551894245,22.3032367642008, 21.5868183389771, 20.9175327048866, 20.2859533037025, 19.6543739025185, 19.060500734241, 18.4666275659634, 17.9104606305924, 17.3731468116746, 16.8452595509835, 16.3362254067456, 15.8554709371876, 15.3841430258562, 14.9316682309781, 14.4980465525532, 14.0644248741283, 13.6590828703834, 13.263167424865, 12.8861050957999, 12.5090427667348, 12.1508335541229, 11.8020508997377, 11.4721213618058, 11.1421918238738, 10.8311154023951, 10.529465539143, 10.2466687923442, 9.95444548731872, 9.68107529874652};
        double cs_kidonakis[49] = {463.00, 447.00, 431.50, 416.80, 402.30, 388.70, 375.60, 363.00, 350.80, 339.20, 328.00, 317.30, 306.80, 296.80, 287.10, 277.90, 269.10, 260.50, 252.20, 244.20, 236.60, 229.00, 221.90, 215.20, 208.50, 202.20, 195.90, 190.00, 184.30, 178.70, 173.30, 168.20, 163.20, 158.40, 153.80, 149.20, 144.90, 140.70, 136.70, 132.70, 128.90, 125.20, 121.70, 118.20, 114.90, 111.70, 108.70, 105.60, 102.70};

        TGraphAsymmErrors graph_kidonakis(49, mt_kidonakis, cs_kidonakis, mt_kidonakis_err_mi, mt_kidonakis_err_pl, cs_kidonakis_err_mi, cs_kidonakis_err_pl);
        TF1 kidonakis_fit("data","1/(x*x*x*x)*([0]+[1]*x+[2]*x*x+[3]*x*x*x)", 140, 190);
	graph_kidonakis.Fit(&kidonakis_fit,"","",161.5,184.5);

	double unc_u = 1.0;
	double unc_d = 1.0;

	std::string chan(argv[0]);

	if(chan == "mu"){
		unc_u = 0.075;
		unc_d = 0.079;
		mass[0] = 161.5;
		mass_err[0] = 0;
		cs[0] = 159.34;
		cs_err_u[0] = cs[0]*unc_u;
		cs_err_d[0] = cs[0]*unc_d;
		
		mass[1] = 163.5;
		mass_err[1] = 0;
		cs[1] = 162.23;
		cs_err_u[1] = cs[1]*unc_u;
		cs_err_d[1] = cs[1]*unc_d;

		mass[2] = 166.5;
		mass_err[2] = 0;
		cs[2] = 162.14;
		cs_err_u[2] = cs[2]*unc_u;
		cs_err_d[2] = cs[2]*unc_d;
		
		mass[3] = 169.5;
		mass_err[3] = 0;
		cs[3] = 158.4;
		cs_err_u[3] = cs[3]*unc_u;
		cs_err_d[3] = cs[3]*unc_d;
		
		mass[4] = 172.5;
		mass_err[4] = 0;
		cs[4] = 157.42;
		cs_err_u[4] = cs[4]*unc_u;
		cs_err_d[4] = cs[4]*unc_d;
		
		mass[5] = 175.5;
		mass_err[5] = 0;
		cs[5] = 146.73;
		cs_err_u[5] = cs[5]*unc_u;
		cs_err_d[5] = cs[5]*unc_d;
		
		mass[6] = 178.5;
		mass_err[6] = 0;
		cs[6] = 139.49;
		cs_err_u[6] = cs[6]*unc_u;
		cs_err_d[6] = cs[6]*unc_d;
		
		mass[7] = 181.5;
		mass_err[7] = 0;
		cs[7] = 136.22;
		cs_err_u[7] = cs[7]*unc_u;
		cs_err_d[7] = cs[7]*unc_d;
		
		mass[8] = 184.5;
		mass_err[8] = 0;
		cs[8] = 128.55;
		cs_err_u[8] = cs[8]*unc_u;
		cs_err_d[8] = cs[8]*unc_d;
	}
	
	//if(chan == "e"){
		unc_u = 0.072;
		unc_d = 0.084;

		mass[0] = 161.5;
		mass_err[0] = 0;
		cs[0] = 155.77;
		cs_err_u[0] = cs[0]*unc_u;
		cs_err_d[0] = cs[0]*unc_d;
		
		mass[1] = 163.5;
		mass_err[1] = 0;
		cs[1] = 160.67;
		cs_err_u[1] = cs[1]*unc_u;
		cs_err_d[1] = cs[1]*unc_d;

		mass[2] = 166.5;
		mass_err[2] = 0;
		cs[2] = 161.44;
		cs_err_u[2] = cs[2]*unc_u;
		cs_err_d[2] = cs[2]*unc_d;
		
		mass[3] = 169.5;
		mass_err[3] = 0;
		cs[3] = 157.21;
		cs_err_u[3] = cs[3]*unc_u;
		cs_err_d[3] = cs[3]*unc_d;
		
		mass[4] = 172.5;
		mass_err[4] = 0;
		cs[4] = 152.43;
		cs_err_u[4] = cs[4]*unc_u;
		cs_err_d[4] = cs[4]*unc_d;
		
		mass[5] = 175.5;
		mass_err[5] = 0;
		cs[5] = 146.77;
		cs_err_u[5] = cs[5]*unc_u;
		cs_err_d[5] = cs[5]*unc_d;
		
		mass[6] = 178.5;
		mass_err[6] = 0;
		cs[6] = 138.22;
		cs_err_u[6] = cs[6]*unc_u;
		cs_err_d[6] = cs[6]*unc_d;
		
		mass[7] = 181.5;
		mass_err[7] = 0;
		cs[7] = 136.1;
		cs_err_u[7] = cs[7]*unc_u;
		cs_err_d[7] = cs[7]*unc_d;
		
		mass[8] = 184.5;
		mass_err[8] = 0;
		cs[8] = 131.05;
		cs_err_u[8] = cs[8]*unc_u;
		cs_err_d[8] = cs[8]*unc_d;
	//}
	
	if(chan == "emu"){
		unc_u = 0.068;
		unc_d = 0.085;

		mass[0] = 161.5;
		mass_err[0] = 0;
		cs[0] = 156.16;
		cs_err_u[0] = cs[0]*unc_u;
		cs_err_d[0] = cs[0]*unc_d;
		
		mass[1] = 163.5;
		mass_err[1] = 0;
		cs[1] = 160.29;
		cs_err_u[1] = cs[1]*unc_u;
		cs_err_d[1] = cs[1]*unc_d;

		mass[2] = 166.5;
		mass_err[2] = 0;
		cs[2] = 161.39;
		cs_err_u[2] = cs[2]*unc_u;
		cs_err_d[2] = cs[2]*unc_d;
		
		mass[3] = 169.5;
		mass_err[3] = 0;
		cs[3] = 156.86;
		cs_err_u[3] = cs[3]*unc_u;
		cs_err_d[3] = cs[3]*unc_d;
		
		mass[4] = 172.5;
		mass_err[4] = 0;
		cs[4] = 154.47;
		cs_err_u[4] = cs[4]*unc_u;
		cs_err_d[4] = cs[4]*unc_d;
		
		mass[5] = 175.5;
		mass_err[5] = 0;
		cs[5] = 146.34;
		cs_err_u[5] = cs[5]*unc_u;
		cs_err_d[5] = cs[5]*unc_d;
		
		mass[6] = 178.5;
		mass_err[6] = 0;
		cs[6] = 138.47;
		cs_err_u[6] = cs[6]*unc_u;
		cs_err_d[6] = cs[6]*unc_d;
		
		mass[7] = 181.5;
		mass_err[7] = 0;
		cs[7] = 135.47;
		cs_err_u[7] = cs[7]*unc_u;
		cs_err_d[7] = cs[7]*unc_d;
		
		mass[8] = 184.5;
		mass_err[8] = 0;
		cs[8] = 128.74;
		cs_err_u[8] = cs[8]*unc_u;
		cs_err_d[8] = cs[8]*unc_d;
	}
	
	TGraphAsymmErrors graph_data(9, mass, cs, mass_err,mass_err, cs_err_d, cs_err_u);
	TF1 data_fit("data","1/(x*x*x*x)*([0]+[1]*x+[2]*x*x+[3]*x*x*x)", 161.5, 184.5);
        data_fit.SetParameter(0, 1.84988e+09);
        data_fit.SetParameter(1, -2.11857e+10);
        data_fit.SetParameter(2, 2.47708e+08);
        data_fit.SetParameter(3, -6.94962e+05);
        graph_data.Fit(&data_fit,"","",161.5,184.5);

        double ex_mass[1];
        double ex_mass_err_up[1];
        double ex_mass_err_down[1];
        double ex_cs[1];
        double ex_cs_err_up[1];
        double ex_cs_err_down[1];

	/*	//mu
        ex_mass[0] = 178.2;
        ex_mass_err_up[0] = 6.5;
        ex_mass_err_down[0] = 7.4;
        ex_cs[0] = 143.3;
	*/

		//e
        ex_mass[0] = 178.6;
        ex_mass_err_up[0] = 5.4;
        ex_mass_err_down[0] = 7.7;
        ex_cs[0] = 139.35;

	/*	//emu
        ex_mass[0] = 178.4;
        ex_mass_err_up[0] = 6.2;
        ex_mass_err_down[0] = 7.3;
        ex_cs[0] = 141.93;
	*/

        ex_cs_err_up[0] = ex_cs[0]*unc_u;
        ex_cs_err_down[0] = ex_cs[0]*unc_d;
        TGraphAsymmErrors graph_ex_mass(1, ex_mass, ex_cs, ex_mass_err_down, ex_mass_err_up, ex_cs_err_up, ex_cs_err_down);




	CanvasHolder ch;
	std::string drawopt = "E3";
	ch.setMarkerColorsGraph(4);
	ch.setLineColorsGraph(4);
	//graph_data.SetFillStyle(3003);
	graph_data.SetFillColor(kYellow);
	graph_data.SetLineColor(kYellow);
	graph_data.SetMarkerColor(kYellow);
	data_fit.SetLineColor(kYellow);
        data_fit.SetFillColor(kYellow);
        data_fit.SetMarkerColor(kYellow);


	graph_kidonakis.SetFillStyle(3003);
	graph_kidonakis.SetFillColor(kBlue);
	graph_kidonakis.SetLineColor(kBlue);
	graph_kidonakis.SetMarkerColor(kBlue);
	kidonakis_fit.SetLineColor(kBlue);
	kidonakis_fit.SetFillColor(kBlue);
	kidonakis_fit.SetMarkerColor(kBlue);


	ch.setTitleX("Top quark mass (GeV/c^{2})");
	ch.setTitleY("#sigma_{t#bar{t}} (pb)");
	ch.setCanvasTitle("mass_from_cs");
	ch.addGraphFormated(&graph_data,"name1","",drawopt);
        ch.addGraphFormated(&graph_kidonakis,"name1","",drawopt);
	
	ch.addTF1(&kidonakis_fit,"Kidonakis","");
	ch.addTF1(&data_fit,"Measured #sigma_{t#bar{t}}","");

	ch.addGraphFormated(&graph_ex_mass,"name1","Extracted m_{t}","PE0");

	ch.setLegendOptions(0.25,0.12);
	ch.setBordersY(00,600);
	ch.setBordersX(140,190);
//	ch.setMarkerStylesGraph(2);
   	ch.setLineSizeGraph(2);
   	ch.setOptStat(00000);
	ch.save("pdf");

        return 0;
}
