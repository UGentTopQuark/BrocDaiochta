#include "TFile.h"
#include "TDirectory.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TKey.h"
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include "PlotCombiner/TGraphCombiner.h"
#include "PlotCombiner/HistoCombiner.h"
#include "PlotCombiner/HistoCombiner2D.h"



int main(int argc, char **argv)
{

	std::string MC_file_name = "DQM_MC_qcdmuptwj_norel.root";
	std::string data_file_name = "DQM_Data.root";

	bool overlay_cutsets = false; //only works with one input file
	bool overlay_regions = false; //again one input file
	bool overlay_l1_hlt = true; //again one input file

	std::vector<std::string> files_with_content;
	files_with_content.push_back(data_file_name);
	if(!overlay_cutsets && !overlay_regions && !overlay_l1_hlt)files_with_content.push_back(MC_file_name);
	//	if(!overlay_cutsets && !overlay_regions && !overlay_l1_hlt)files_with_content.push_back("DQM_MC_wjets_norel.root");
		
	int origin_of_input = 0; //0 for flat selection input. 1 for DQMOffline code
	bool overlay_eff_histos = false;
	bool overlay_eff_tgraphs = true; //These two bools should not be the same

	std::vector<std::string> cutsets_to_overlay;
 	cutsets_to_overlay.push_back("01_cutset");
	//	cutsets_to_overlay.push_back("02_cutset");
	//cutsets_to_overlay.push_back("03_cutset");

	std::vector<std::string> predots_to_overlay;
	predots_to_overlay.push_back("Eff_HLT_matched.");
	predots_to_overlay.push_back("Eff_L1Seed_matched.");
	predots_to_overlay.push_back("Eff_HLTvsL1Seed_matched.");

	std::vector<std::string> regions_to_overlay;
 	regions_to_overlay.push_back("barrel");
	regions_to_overlay.push_back("endcap");
	regions_to_overlay.push_back("overlap");
//  	regions_to_overlay.push_back("pt20to30");
// 	regions_to_overlay.push_back("pt30to40");
// 	regions_to_overlay.push_back("pt40plus");
	
	std::vector<std::string> cutsets_to_plot;
	cutsets_to_plot.push_back("_");  //Plot all cutsets

	std::string req_trig = "-1"; //HLT_Jet6U
	std::string stud_trig = "-1"; //L1MuOpen
	if(origin_of_input == 1){
		req_trig = argv[1];
		stud_trig = argv[2];
	}

	TFile *outfile = new TFile("outfile.root", "RECREATE");

	//Required and studied triggers or_of_in == 1
	std::map<std::string, std::vector<std::string> > triggers;
	triggers["topMuonPt"+req_trig].push_back(stud_trig);
	//WARNING: If you study more than one trigger at once the output eps files will overwrite eachother
	std::string root_dir = "";
	if(origin_of_input == 0)
		root_dir = "eventselection";
	
	/*********Read in all histo names from MC file**********/
	
	TFile *file = new TFile((files_with_content[0]).c_str(),"open");
	
	TKey *key;

	std::vector<std::string> o0_histo_names;
	std::vector<std::string> o0_histo_names2d;
	std::vector<std::string> o0_histo_namesEff;

 	std::cout << "searching for keys" << std::endl;
	if(overlay_cutsets == false && overlay_regions == false &&  overlay_l1_hlt == false){
		std::cout << " Searching in directory " << root_dir<<std::endl;
		TIter nextkey(file->GetDirectory((root_dir).c_str())->GetListOfKeys());
		std::cout << " Found directory" << std::endl;
		
		while ((key = (TKey*)nextkey())){
			std::cout << "processing: " << key->GetName() << std::endl;
			std::cout << "class: " << key->GetClassName() << std::endl;
			
			std::string histo_name = key->GetName();
			if(histo_name.find("RECO") !=std::string::npos)
				continue;
			for(std::vector<std::string>::iterator cutset_str = cutsets_to_plot.begin();
			    cutset_str != cutsets_to_plot.end();cutset_str++){
				if(histo_name.find(*cutset_str) != std::string::npos && histo_name.find(".") != std::string::npos){
					if(histo_name.find("Eff") != std::string::npos)
						o0_histo_namesEff.push_back(histo_name);
					if((std::string) key->GetClassName() == "TH1F"){
						o0_histo_names.push_back(histo_name);
					}
					if((std::string) key->GetClassName() == "TH2F"){
						if(histo_name.find("Eff") != std::string::npos)
							std::cout << " WARNING: Not set up for 2d Eff histos " << histo_name << std::endl;
						else
							o0_histo_names2d.push_back(histo_name);
					}
				}
			}
		}
	}
	else if(overlay_cutsets == true){
		std::cout << " Searching in directory " << root_dir<<std::endl;
		TIter nextkey(file->GetDirectory((root_dir).c_str())->GetListOfKeys());
		std::cout << " Found directory" << std::endl;
		
		std::map<std::string,bool> names_filled;

		while ((key = (TKey*)nextkey())){
			std::string histo_name = key->GetName();

			//remove cutset from histo_name
			histo_name.erase(histo_name.end() - 9,histo_name.end());
			std::cout << "processing: " << key->GetName() << std::endl;
			std::cout << "class: " << key->GetClassName() << std::endl;
			

			std::cout << "NAME: " << histo_name << std::endl;
			if(names_filled.find(histo_name) == names_filled.end()){
				names_filled[histo_name] = true;
			      
				if(histo_name.find(".") != std::string::npos){
					if(histo_name.find("Eff") != std::string::npos)
						o0_histo_namesEff.push_back(histo_name);
					if((std::string) key->GetClassName() == "TH1F"){
						o0_histo_names.push_back(histo_name);
					}
					if((std::string) key->GetClassName() == "TH2F"){
						if(histo_name.find("Eff") != std::string::npos)
							std::cout << " WARNING: Not set up for 2d Eff histos " << histo_name << std::endl;
						else
							o0_histo_names2d.push_back(histo_name);
					}
				}
			}
			
		}

	}
	else if(overlay_l1_hlt == true){
		std::cout << " Searching in directory  " << root_dir<<std::endl;
		TIter nextkey(file->GetDirectory((root_dir).c_str())->GetListOfKeys());
		std::cout << " Found directory" << std::endl;
		
		std::map<std::string,bool> names_filled;

		while ((key = (TKey*)nextkey())){
			std::string histo_name = key->GetName();
			std::string full_histo_name = key->GetName();
			std::cout << "NAME preclean: " << histo_name << std::endl;

			if(histo_name.find("Eff_HLT_matched.") == std::string::npos && histo_name.find("Eff_L1Seed_matched.") == std::string::npos)
				continue;

			//remove cutset from histo_name
			histo_name.erase(0,histo_name.find(".")+1);
			std::cout << "NAME post clean: " << histo_name << std::endl;
			std::cout << "processing: " << key->GetName() << std::endl;
			std::cout << "class: " << key->GetClassName() << std::endl;
			

			std::cout << "NAME: " << histo_name << std::endl;
			if(names_filled.find(histo_name) == names_filled.end()){
				names_filled[histo_name] = true;
			      
				if(full_histo_name.find(".") != std::string::npos){
					if(full_histo_name.find("Eff") != std::string::npos)
						o0_histo_namesEff.push_back(histo_name);
					if((std::string) key->GetClassName() == "TH1F"){
						o0_histo_names.push_back(histo_name);
					}
					if((std::string) key->GetClassName() == "TH2F"){
						if(histo_name.find("Eff") != std::string::npos)
							std::cout << " WARNING: Not set up for 2d Eff histos " << histo_name << std::endl;
						else
							o0_histo_names2d.push_back(histo_name);
					}
				}
			}
			
		}

	}
	else if(overlay_regions == true){
		std::cout << " Searching in directory " << root_dir<<std::endl;
		TIter nextkey(file->GetDirectory((root_dir).c_str())->GetListOfKeys());
		std::cout << " Found directory" << std::endl;
		
		std::map<std::string,bool> names_filled;

		while ((key = (TKey*)nextkey())){
			std::string histo_name = key->GetName();

			if(histo_name.find("trig_obj") !=std::string::npos || histo_name.find("fixedbins") !=std::string::npos)
				continue;

			bool process_this_histo = false;
			std::string this_region;
			for(std::vector<std::string>::iterator region = regions_to_overlay.begin();region != regions_to_overlay.end();region++){
				if(histo_name.find(*region) != std::string::npos){
					process_this_histo = true;
					this_region = *region;
					break;
				}
			}
			if(!process_this_histo)
				continue;

			//remove region from histo_name
			histo_name.erase(histo_name.find("_"+this_region),(this_region.size()+1));
			std::cout << " NAME: " << histo_name << std::endl;

			std::cout << "processing: " << key->GetName() << std::endl;
			std::cout << "class: " << key->GetClassName() << std::endl;
			

			std::cout << "NAME: " << histo_name << std::endl;
			if(names_filled.find(histo_name) == names_filled.end()){
				names_filled[histo_name] = true;
			      
				if(histo_name.find(".") != std::string::npos){
					if(histo_name.find("Eff") != std::string::npos)
						o0_histo_namesEff.push_back(histo_name);
					if((std::string) key->GetClassName() == "TH1F"){
						o0_histo_names.push_back(histo_name);
					}
					if((std::string) key->GetClassName() == "TH2F"){
						if(histo_name.find("Eff") != std::string::npos)
							std::cout << " WARNING: Not set up for 2d Eff histos " << histo_name << std::endl;
						else
							o0_histo_names2d.push_back(histo_name);
					}
				}
			}
			
		}

	}

	delete file;

	/********Set options for HistoCombiner**********/
	std::map<std::string,std::pair<double,double> > *xaxes = new std::map<std::string,std::pair<double, double> >();
	std::map<std::string,std::pair<double,double> > *yaxes = new std::map<std::string,std::pair<double, double> >();
	std::map<std::string,bool> *logscale = new std::map<std::string,bool>();
        std::map<std::string,int> *rebin = new std::map<std::string,int>();

	//NOTE: Rebinning should be done in data_run_combiner
	// If scaling mc to data x-axis range should also be set in data run combiner
	std::vector<std::string> histo_types;
	histo_types.push_back("eta");
	histo_types.push_back("phi");
	histo_types.push_back("nHits");
	histo_types.push_back("jet_pt");
	histo_types.push_back("jet_eta");
	
	std::vector<std::string> histo_types2;
	histo_types.push_back("eta");
	histo_types.push_back("phi");
	
	std::vector<std::string> histo_types3;
	histo_types.push_back("chi2");
	histo_types.push_back("relIso");


				for(std::vector<std::string>::iterator histo_name = o0_histo_names.begin();histo_name != o0_histo_names.end();histo_name++){
// 			for(std::vector<std::string>::iterator histo_type = histo_types.begin();histo_type != histo_types.end();histo_type++){

// 				if(histo_name->find(*histo_type) != std::string::npos){
// 					(*rebin)[*histo_name] = 2;
// 				}
// 			}
// 			for(std::vector<std::string>::iterator histo_type = histo_types2.begin();histo_type != histo_types2.end();histo_type++){

// 				if(histo_name->find(*histo_type) != std::string::npos){
// 					(*rebin)[*histo_name] = 4;
// 				}
// 			}

// 			if(histo_name->find("relIso") != std::string::npos)
// 				(*xaxes)[*histo_name] = std::pair<double,double>(0.,0.1);
			
// 			if(histo_name->find("chi2") != std::string::npos)
// 				(*xaxes)[*histo_name] = std::pair<double,double>(0.,10.);
// 		}

 			if(histo_name->find("_pt") != std::string::npos)
 				(*xaxes)[*histo_name] = std::pair<double,double>(0.,59.9);
// 			if(histo_name->find(".relIso") != std::string::npos)
// 				(*xaxes)[*histo_name] = std::pair<double,double>(0.,1.0);
			
// 			if(histo_name->find(".chi2") != std::string::npos)
// 				(*xaxes)[*histo_name] = std::pair<double,double>(0.,10.);
			
 		}

 		for(std::vector<std::string>::iterator histo_name = o0_histo_namesEff.begin();histo_name != o0_histo_namesEff.end();histo_name++){
			if(histo_name->find("_pt_") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(0.,59.9);
			if(histo_name->find("jet_number") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(-0.5,5.5);
			if(histo_name->find(".relIso") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(0.,1.0);
			
			if(histo_name->find(".chi2") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(0.,10.);
			if(histo_name->find(".eta") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(-2.1,2.1);
			if(histo_name->find("Eff") != std::string::npos)
				(*yaxes)[*histo_name] = std::pair<double,double>(0.6,1.0);
		
		}


	std::vector<int> *colours = new std::vector<int>();
	if(!overlay_regions && !overlay_l1_hlt &&  MC_file_name.find("MC") != std::string::npos && data_file_name.find("new") == std::string::npos && data_file_name.find("HLTvsL1") == std::string::npos){ 

		colours->push_back(kBlack);
		colours->push_back(kRed);
		colours->push_back(kBlue);
		
	}
	else if(overlay_l1_hlt){   
		colours->push_back(kBlack);
		colours->push_back(kGreen+2);
		colours->push_back(kBlue-7);
	}
	else if(data_file_name.find("new") != std::string::npos){  
		colours->push_back(kBlue+1); 
		colours->push_back(kCyan+2);
	}
	else if(overlay_regions){
		colours->push_back(kMagenta+3);
		colours->push_back(46);
		colours->push_back(39);
	}
	else if(data_file_name.find("HLTvsL1") != std::string::npos){
		colours->push_back(kBlue-7);
	}
	else{
		colours->push_back(kBlack);
		colours->push_back(kRed);
		colours->push_back(kBlue);
	}
	

	HistoCombiner *hc = new HistoCombiner();

	hc->set_colours(colours);
	if(!overlay_cutsets && !overlay_regions && files_with_content.size() > 1&& data_file_name.find("new") == std::string::npos) hc->set_scale_mc_to_data(true);
	hc->set_xaxes(xaxes);
	hc->set_yaxes(yaxes);
	hc->set_logscale(logscale);
	hc->set_rebin(rebin);
	hc->set_overlay_cutsets(overlay_cutsets);
	if(overlay_cutsets || overlay_regions || files_with_content.size() == 1 ||data_file_name.find("new") != std::string::npos) hc->set_add_histos_stacked(false);
	hc->set_calculate_efficiency(false);
	hc->set_input_directory(root_dir);
	if( overlay_cutsets)
		hc->add_plots_by_cutset(files_with_content[0],cutsets_to_overlay,o0_histo_names);			
	else if(overlay_regions)
		hc->add_plots_by_region(files_with_content[0],regions_to_overlay,o0_histo_names);
	else
		hc->add_vplots(files_with_content,o0_histo_names);
	hc->combine_histos();
	hc->write_histos();
	
// 	HistoCombiner2D *hc2D = new HistoCombiner2D();

// 	if(origin_of_input == 0){
// 		hc2D->set_input_directory(root_dir);
// 		hc2D->add_vplots(files_with_content,o0_histo_names2d);
// 		hc2D->combine_histos();
// 		hc2D->write_histos();
// 	}

	if(overlay_eff_histos){
		HistoCombiner *hcE = new HistoCombiner();
		
		hcE->set_colours(colours);
		hcE->set_scale_mc_to_data(false);
		hcE->set_eff_plots(overlay_eff_histos);
		hcE->set_add_histos_stacked(false);
		hcE->set_xaxes(xaxes);
		hcE->set_yaxes(yaxes);
		hcE->set_logscale(logscale);
		hcE->set_calculate_efficiency(false);
		
		hcE->set_input_directory(root_dir);
		if( overlay_cutsets)
			hcE->add_plots_by_cutset(files_with_content[0],cutsets_to_overlay,o0_histo_names);
		else if(overlay_regions)
			hcE->add_plots_by_region(files_with_content[0],regions_to_overlay,o0_histo_names);
		else
			hcE->add_vplots(files_with_content,o0_histo_names);
		
		hcE->combine_histos();
		hcE->write_histos();
		
	}
	else if(overlay_eff_tgraphs){
		TGraphCombiner *tgE = new TGraphCombiner();
		
		tgE->set_colours(colours);
		tgE->set_scale_mc_to_data(false);
		tgE->set_eff_plots(overlay_eff_tgraphs);
		tgE->set_xaxes(xaxes);
		//	tgE->set_yaxes(yaxes);

		if(overlay_cutsets)
			tgE->add_plots_by_cutset(files_with_content[0],cutsets_to_overlay,o0_histo_namesEff);
		else if(overlay_l1_hlt)
			tgE->add_plots_by_predot(files_with_content[0],predots_to_overlay,o0_histo_namesEff);
		else if(overlay_regions)
			tgE->add_plots_by_region(files_with_content[0],regions_to_overlay,o0_histo_namesEff);
		else
			tgE->add_vplots(files_with_content,o0_histo_namesEff);
		
		tgE->combine_graphs();
		tgE->write_graphs();
		
	}

	delete outfile;
	outfile = NULL;
	
}
