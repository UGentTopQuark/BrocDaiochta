#ifndef SYS8_CUT_H
#define SYS8_CUT_H

#include <string>

namespace sys8{
	class Cut{
		public:
			Cut(std::string var_name, double min, double max);
			~Cut(){};

			inline void set_min(double min){this->min_ = min;}
			inline void set_max(double max){this->max_ = max;}
			inline void set_var_name(std::string var_name){this->var_name_ = var_name;}
			inline void set_var_idx(int idx){this->var_idx_ = idx;}

			inline double get_min(){return min_;}
			inline double get_max(){return max_;}
			inline std::string get_var_name(){return var_name_;}
			inline int get_var_idx(){return var_idx_;}

		private:
			std::string var_name_;
			double min_;
			double max_;
			int var_idx_;
	};
}

#endif
