#include "VariableHolder.h"

sys8::VariableHolder::VariableHolder()
{
  next_value_index = 0;
  //  next_pass_bit_index = 0;
}

sys8::VariableHolder::~VariableHolder()
{
}

// double sys8::VariableHolder::get_pass_bit(int index)
// {
//   return bits[index];
// }

double sys8::VariableHolder::get_variable(int index)
{
  return values[index];
}

int sys8::VariableHolder::get_variable_index(std::string variable)
{
  if(value_indices.find(variable) != value_indices.end())
    return value_indices[variable];
  else{
    std::cerr << "ERROR: sys8::VariableHolder::get_variable_index(): variable " << variable << " does not exist." << std::endl;
    for(std::map<std::string,int>::iterator ind = value_indices.begin();
	ind != value_indices.end(); ind++){
	    std::cout << ind->first << " " << ind->second << std::endl;
    }
    exit(1);
  }
}

// int sys8::VariableHolder::get_pass_bit_index(std::string pass_bit)
// {
//   if(pass_bit_indices.find(pass_bit) != pass_bit_indices.end())
//     return pass_bit_indices[pass_bit];
//   else{
//     std::cerr << "ERROR: sys8::VariableHolder::get_pass_bit_index(): pass_bit " << pass_bit << " does not exist." << std::endl;
//     exit(1);
//   }
// }

// void sys8::VariableHolder::add_pass_bit(std::string pass_bit_id, std::string section)
// {
//   if(pass_bit_indices.find(pass_bit_id) == pass_bit_indices.end()){
//     pass_bit_indices[pass_bit_id] = next_pass_bit_index;
//     ++next_pass_bit_index;
//   }
// }

void sys8::VariableHolder::add_variable(std::string variable)
{
  if(value_indices.find(variable) == value_indices.end()){
    value_indices[variable]=next_value_index;
    ++next_value_index;
  }
}

void sys8::VariableHolder::configure_variables(TTree *tree)
{
//   bits.assign(pass_bit_indices.size(), 0);
//   for(std::map<std::string, int>::iterator bit = pass_bit_indices.begin();
//       bit != pass_bit_indices.end();
//       ++bit){
//     tree->SetBranchAddress(bit->first.c_str(), &(bits[bit->second]));
//   }
  
  values.assign(value_indices.size(), 0);
  for(std::map<std::string, int>::iterator value_index = value_indices.begin();
      value_index != value_indices.end();
      ++value_index){
    tree->SetBranchAddress(value_index->first.c_str(), &(values[value_index->second]));
  }
}
