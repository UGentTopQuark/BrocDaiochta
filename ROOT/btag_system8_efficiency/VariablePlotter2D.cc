#include "VariablePlotter2D.h"

comhaireamh::VariablePlotter2D::VariablePlotter2D(std::string identifier, eire::ConfigReader *config_reader, TTree *tree, TFile *outfile, comhaireamh::VariableHolder *var_holder)
{
  this->identifier = identifier;
  this->outfile = outfile;
  this->var_holder = var_holder;
  
  var_name = config_reader->get_var("var", identifier, true);
  var2_name = config_reader->get_var("var2", identifier, true);
  input_file_name = config_reader->get_var("file_name", identifier, true);
  pass_bit_name = config_reader->get_var("pass_bit", identifier, true);
  
  std::vector<std::string> binning = config_reader->get_vec_var("binning", identifier, true);
  std::vector<std::string> binning2 = config_reader->get_vec_var("binning2", identifier, true);
  
  // prepare binning for histograms; allows asymmetric binning
  int nbins = binning.size();
  int n2bins = binning2.size();
  double bins[nbins];
  double bins2[n2bins];
  unsigned int i = 0;
  for(std::vector<std::string>::iterator bin = binning.begin();
      bin != binning.end();
      ++bin){
    bins[i] = atof(bin->c_str());
    std::cout<<bins[i]<<":";
    ++i;
  }
  std::cout<<"\n";
  unsigned int j = 0;
  for(std::vector<std::string>::iterator bin2 = binning2.begin();
      bin2 != binning2.end();
      ++bin2){
    bins2[j] = atof(bin2->c_str());
    std::cout<<bins2[j]<<":";
    ++j;
  }
  std::cout<<"\n";
  std::string histo_title = config_reader->get_var("title", identifier);
  std::string histo_name = identifier;
  
  outfile->cd();
  /*
   * Book histograms for probes that pass/fail trigger matching and for all probes.
   * The efficiency is calculated by pass_histo/all_histo.
   */
  all_histo = new TH2F((histo_name+"_all").c_str(), (histo_name+" All").c_str(), nbins-1, bins, n2bins-1, bins2);
  all_histo->SetXTitle(config_reader->get_var("xtitle", identifier).c_str());
  all_histo->SetYTitle(config_reader->get_var("ytitle", identifier).c_str());
  pass_histo = new TH2F((histo_name+"_pass").c_str(), (histo_name+" Pass").c_str(), nbins-1, bins, n2bins-1, bins2);
  pass_histo->SetXTitle(config_reader->get_var("xtitle", identifier).c_str());
  pass_histo->SetYTitle(config_reader->get_var("ytitle", identifier).c_str());
  fail_histo = new TH2F((histo_name+"_fail").c_str(), (histo_name+" Fail").c_str(), nbins-1, bins, n2bins-1, bins2);
  fail_histo->SetXTitle(config_reader->get_var("xtitle", identifier).c_str());
  fail_histo->SetYTitle(config_reader->get_var("ytitle", identifier).c_str());
  
  efficiency_graph = new TGraphAsymmErrors();
  efficiency_graph2D = new TH2F(("Efficiency "+var_name+" vs. "+var2_name).c_str(), ("Efficiency "+var_name+" VS. "+var2_name).c_str(), nbins-1, bins, n2bins-1, bins2);
  
  pass_bit_index = -1;
  var_index = -1;
  var2_index = -1;
  
  // initialise on-the-fly cuts
  	prepare_cuts(config_reader, identifier);
}

void comhaireamh::VariablePlotter2D::prepare_cuts(eire::ConfigReader *config_reader, std::string identifier)
{
  std::vector<std::string> variables = config_reader->get_variables_for_section(identifier);
  
  /*
   * search in configuration section for variables that are of the format:
   * cut_<var name> = <min cut value>:<max cut value>
   */
  for(std::vector<std::string>::iterator var = variables.begin();
      var != variables.end();
      ++var){
    if(var->find("cut_") != std::string::npos){
      std::string cut_variable(*var);
      std::cout << "cut variable: " << *var << std::endl;
      cut_variable = cut_variable.substr(4,std::string::npos);
      std::cout << "variable: " << cut_variable << std::endl;
      std::vector<std::string> cut_vals = config_reader->get_vec_var(*var,identifier,true);
      if(cut_vals.size() < 2){
	std::cout << "ERROR: cuts must always be of the form cut_<var name> = min:max" << std::endl;
	exit(1);
      }
      double min_cut = atof(cut_vals[0].c_str());
      double max_cut = atof(cut_vals[1].c_str());
      // for each cut_ value create a new Cut object and store it in a vector of Cut objects
      // for each event this vector will be processed and the cuts applied accordingly
      // if an event fails the cut it will not be considered for the efficiency calculation
      comhaireamh::Cut new_cut(cut_variable, min_cut, max_cut);
      var_holder->add_variable(cut_variable);
      cuts.push_back(new_cut);
    }
  }
}

// get variable indices in VariableHolder for each variable that will be used later on
void comhaireamh::VariablePlotter2D::configure()
{
  // variables for plotting
  pass_bit_index = var_holder->get_pass_bit_index(pass_bit_name);
  std::cout<<var_name<<" "<<var2_name<<"\n";
  var_index = var_holder->get_variable_index(var_name);
  var2_index = var_holder->get_variable_index(var2_name);
  
  // variables for on-the-fly cuts
  	for(std::vector<comhaireamh::Cut>::iterator cut = cuts.begin();
  	cut != cuts.end();
  	++cut){
  	cut->set_var_idx(var_holder->get_variable_index(cut->get_var_name()));	
  }
}

comhaireamh::VariablePlotter2D::~VariablePlotter2D()
{
  delete efficiency_graph;
}

TGraphAsymmErrors *comhaireamh::VariablePlotter2D::get_efficiency_graph()
{
  std::cout << "===========================================" << std::endl;
  std::cout << ">>> var / pass_bit / file: " << var_name <<":"<< var2_name<< " / " << pass_bit_name << " / " << input_file_name << std::endl;
  std::cout << ">>> efficiency: " << pass_histo->GetEntries()/all_histo->GetEntries() << std::endl;
  std::cout << "----------------------" << std::endl;
  std::cout << "pass entries: " << pass_histo->GetEntries() << std::endl;
  std::cout << "all entries: " << all_histo->GetEntries() << std::endl;
  std::cout << "----------------------" << std::endl;
  
  return efficiency_graph;
  
}

TH2F *comhaireamh::VariablePlotter2D::get_efficiency_graph2D()
{
  int i,j;  
  for(i=1;i!=(efficiency_graph2D->GetNbinsX())+1;i++)
    {for(j=1;j!=(efficiency_graph2D->GetNbinsY())+1;j++)
	{
	  if(all_histo->GetBinContent(i,j)!=0)
	    {
	      efficiency_graph2D->SetBinContent(i,j,(Double_t)(pass_histo->GetBinContent(i,j)/all_histo->GetBinContent(i,j)));
	      Double_t eff = pass_histo->GetBinContent(i,j)/all_histo->GetBinContent(i,j);
	      efficiency_graph2D->SetBinError(i,j,sqrt((eff*(1-eff))/all_histo->GetBinContent(i,j)));
	    }
	}
    }
  return efficiency_graph2D;
}

void comhaireamh::VariablePlotter2D::plot()
{
  double passed = var_holder->get_pass_bit(pass_bit_index);
  double value = var_holder->get_variable(var_index);
  double value2 = var_holder->get_variable(var2_index);
  // Check if event passes cuts that are applied on the fly.
  for(std::vector<comhaireamh::Cut>::iterator cut = cuts.begin(); //need to implement cuts for 2D plots?
  	cut != cuts.end();
  	++cut){
  	double cut_var_value = var_holder->get_variable(cut->get_var_idx());
  	if(cut_var_value < cut->get_min() || cut_var_value > cut->get_max())
  		return;
  }
  
  if(passed != -1){
    if(passed > 0){
      pass_histo->Fill(value,value2);
    }
    else{
      fail_histo->Fill(value,value2);
    }
    
    // fill this always
    all_histo->Fill(value,value2);
  }
}
