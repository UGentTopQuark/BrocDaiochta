#include "BTagEfficiencyManager.h"

sys8::BTagEfficiencyManager::BTagEfficiencyManager(TFile *outfile)
{
  this->outfile = outfile;
  config_reader = NULL;
}

sys8::BTagEfficiencyManager::~BTagEfficiencyManager()
{
  for(std::map<std::string, sys8::TreeReader*>::iterator tree_reader = tree_readers.begin();
      tree_reader != tree_readers.end();
      ++tree_reader){
    delete tree_reader->second;
  }
  tree_readers.clear();
}

void sys8::BTagEfficiencyManager::run_btag_system8_efficiency()
{
  std::cout<<"Getting trees....\n";
  get_trees_from_files();
  std::cout<<"Configuring Trees...\n";
  configure_trees();
  std::cout<<"Processing Trees.... \n";
  process_trees();
  std::cout<<"Making plots....\n";
  create_plots();
}

void sys8::BTagEfficiencyManager::create_plots()
{
  outfile->cd();
  // print one variable at a time
 
  for(std::map<std::string, sys8::TreeReader*>::iterator tree_reader = tree_readers.begin();
      tree_reader != tree_readers.end();
      ++tree_reader){
    
    std::vector<std::string> *variable_list = tree_reader->second->get_variables();
    std::cout << "v size: " << variable_list->size() << std::endl;
    for(std::vector<std::string>::iterator variable = variable_list->begin();
	variable != variable_list->end();
	++variable){
      
     //  if(config_reader->get_bool_var("2D_plot",*variable,false)){
// 	CanvasHolder *cholder = new CanvasHolder();
// 	cholder->setCanvasTitle(*variable);
// 	std::cout<<*variable<<"\n";
// 	cholder->setTitleX(config_reader->get_var("var",*variable,true));
// 	cholder->setTitleY(config_reader->get_var("var2",*variable,true));
// 	std::string title = config_reader->get_var("title", *variable,true);
// 	//cholder->addHisto2D(tree_reader->second->get_efficiency_graph2D(*variable),title, "COLZtexte");
// 	cholder->addHisto2D(tree_reader->second->get_efficiency_graph2D(*variable),title, "COLZ");
// 	cholder->setBordersZ(0.7999,1.001);
// 	//cholder->setBordersZ(-0.001,1.001);
// 	cholder->setOptStat(000000);
// 	cholder->setLegendOptions(0., 0., "LowerRight");
// 	cholder->save("png");
// 	cholder->write(outfile);
// 	delete cholder;
//       }
//       else{
	    std::string var_name = *variable;
	    CanvasHolder *cholder = new CanvasHolder();
	    cholder->setCanvasTitle(*variable);
	    std::cout<<*variable<<"\n";
	    cholder->setTitleY(config_reader->get_var("ytitle", var_name));
	    cholder->setTitleX(config_reader->get_var("xtitle", var_name));
	    cholder->addHLine(1,"",2);
	    cholder->setOptStat(000000);
	    
	    std::string title = config_reader->get_var("title", var_name);
	    cholder->addGraph(tree_reader->second->get_efficiency_graph(var_name),title, title, "APz");
	    std::cout << "b" << std::endl;
	    std::vector<int> styles;
	    styles.push_back(1);
	    styles.push_back(2);
	    
	    cholder->setLineStylesGraph(styles);
	    //cholder->setBordersX(20.,60.);
	    cholder->setBordersY(atof(config_reader->get_var("ymin", var_name,true).c_str()), atof(config_reader->get_var("ymax", var_name,true).c_str()));
	    cholder->setMarkerSizeGraph(1.2);
	    cholder->setLineSizeGraph(2.5);
	    cholder->setLegendOptions(0.40, 0.14, "LowerRight");
	    cholder->setLumi(atof(config_reader->get_var("lumi", var_name,true).c_str()));
	    cholder->setDescription(config_reader->get_var("description", var_name,false).c_str());
	    cholder->save("png");
	    cholder->write(outfile);
	    delete cholder;
	    // }
    }		
  }
  
  
    // read requests for overlays from config
  std::vector<std::vector<std::pair<std::string,std::string> > > overlays = get_overlays();
  
  // print overlays of variables
  for(std::vector<std::vector<std::pair<std::string,std::string> > >::iterator overlay = overlays.begin();
      overlay != overlays.end();
      ++overlay){
	  CanvasHolder *cholder = new CanvasHolder();
	  cholder->addHLine(1,"",2);
	  cholder->setOptStat(000000);
	  
	  std::string ctitle = "overlay";
	  bool first_graph = true;
	  for(std::vector<std::pair<std::string,std::string> >::iterator variable = overlay->begin();
	      variable != overlay->end();
	      ++variable){
		  if(first_graph){
			  cholder->setTitleY(config_reader->get_var("ytitle", variable->second));
			  std::string title = config_reader->get_var("title", variable->second,true);
			  cholder->addGraph(tree_readers[variable->first]->get_efficiency_graph(variable->second), title, title, "APz");
			  cholder->setTitleX(config_reader->get_var("xtitle", variable->second,true));
			  cholder->setBordersY(atof(config_reader->get_var("ymin", variable->second,true).c_str()), atof(config_reader->get_var("ymax", variable->second,true).c_str()));
			  cholder->setLumi(atof(config_reader->get_var("lumi", variable->second,true).c_str()));
			  cholder->setDescription(config_reader->get_var("description", variable->second,false).c_str());
			  first_graph = false;
		  }else{
			  std::string title = config_reader->get_var("title", variable->second,true);
			  cholder->addGraph(tree_readers[variable->first]->get_efficiency_graph(variable->second), title, title, "Pz");
		  }
		  ctitle = variable->second+"_"+ctitle;
	  }
      
	  cholder->setCanvasTitle(ctitle);
    
	  //std::vector<int> styles;
	  //styles.push_back(1);
	  //styles.push_back(2);
	  
	  //cholder->setLineStylesGraph(styles);
	  cholder->setMarkerSizeGraph(1.2);
	  cholder->setLineSizeGraph(2.5);
	  cholder->setLegendOptions(0.40, 0.25, "LowerRight");
	  //cholder->setLegendOptions(0.40, 0.14, "LowerRight");
	  cholder->save("png");
	  cholder->write(outfile);
	  
	  delete cholder;
  }
}

std::vector<std::vector<std::pair<std::string,std::string> > > sys8::BTagEfficiencyManager::get_overlays()
{
  std::vector<std::string> global_vars = config_reader->get_variables_for_section("global");
  
  // list of overlays: list of pairs of <file, variable> that should be overlaid
  std::vector<std::vector<std::pair<std::string,std::string> > > overlays;
  
  // prepare information about what to overlay for later use
  for(std::vector<std::string>::iterator var = global_vars.begin();
      var != global_vars.end();
      ++var){
    if(var->find("overlay") != std::string::npos){
      std::vector<std::string> vars_to_overlay = config_reader->get_vec_var(*var,"global");
      overlays.push_back(std::vector<std::pair<std::string,std::string> >());
      for(std::vector<std::string>::iterator var_to_overlay = vars_to_overlay.begin();
	  var_to_overlay != vars_to_overlay.end();
	  ++var_to_overlay){
	// fill to last element in vector
	overlays[overlays.size()-1].push_back(std::pair<std::string,std::string>(config_reader->get_var("file_name", *var_to_overlay, true), *var_to_overlay));
      }
    }
  }
  
  return overlays;
}

void sys8::BTagEfficiencyManager::configure_trees()
{
  // for each tree create all variables for sections
  for(std::map<std::string, sys8::TreeReader*>::iterator tree_reader = tree_readers.begin();
      tree_reader != tree_readers.end();
      ++tree_reader){
    tree_reader->second->configure_variables();
  }
}

void sys8::BTagEfficiencyManager::process_trees()
{
	// process all events within a tree
	for(std::map<std::string, sys8::TreeReader*>::iterator tree_reader = tree_readers.begin();
	    tree_reader != tree_readers.end();
	    ++tree_reader){
		tree_reader->second->read();
	}
}

void sys8::BTagEfficiencyManager::get_trees_from_files()
{
  std::vector<std::string> *sections = config_reader->get_sections();
  
  std::string filename_var = "file_name";
  std::string tree_name = config_reader->get_var("tree_name", "global", true);
  std::string dir_name = config_reader->get_var("dir_name", "global", true);
  
  // loop over all sections and make sure all root files are opened
  for(std::vector<std::string>::iterator section = sections->begin();
      section != sections->end();
      ++section){
    if(*section != "global"){
      std::string filename = config_reader->get_var(filename_var, *section, true);
      
      // if this is a new tree, book a TreeReader
      if(tree_readers.find(filename) == tree_readers.end()){
	TFile *infile = new TFile(filename.c_str(), "OPEN");
	open_files.push_back(infile);
	sys8::TreeReader *tree_reader = new sys8::TreeReader(outfile);
	tree_reader->set_tree((TTree*) infile->GetDirectory(dir_name.c_str())->Get(tree_name.c_str()));
	tree_reader->set_config_reader(config_reader);
	tree_readers[filename] = tree_reader;
      }
      
      // add variable to TreeReader for new sections
      tree_readers[filename]->add_section(*section);
    }
  }
}
