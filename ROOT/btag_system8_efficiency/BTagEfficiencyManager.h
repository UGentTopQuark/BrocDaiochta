#ifndef SYS8_BTAGEFFICIENCYMANAGER_H
#define SYS8_BTAGEFFICIENCYMANAGER_H

/**
 * \class sys8::BTagEfficiencyManager
 * 
 * \brief Manages read in files, create TreeReader for each file and prints plots and overlays.
 *
 * The main class of counting_tag_and_probe. For each file (the assumption is
 * that there is only one tree per file) a TreeReader is created. The
 * TreeReader manages then the tree within a file and all variables therein.
 * The TagAndProbeManager lets then the TreeReader objects process the
 * individual files and return efficiency histograms. These are then for all
 * variables returned to TagAndProbeManager, formatted, and overlayed if necessary.
 *
 */

#include "ConfigReader/ConfigReader.h"
#include "CanvasHolder/Canvas_Holder.h"
#include "TFile.h"
#include "TTree.h"
#include "TreeReader.h"
#include <map>

namespace sys8{
	class BTagEfficiencyManager{
		public:
			BTagEfficiencyManager(TFile *outfile);
			~BTagEfficiencyManager();

			inline void set_config_reader(eire::ConfigReader *config_reader) { this->config_reader = config_reader; };
			/// main function, run this after setting ConfigReader
			void run_btag_system8_efficiency();
		private:
			std::vector<std::vector<std::pair<std::string,std::string> > > get_overlays();
			/// setup all variables within trees
			void configure_trees();
			/// process all events in trees
			void process_trees();
			/// after processing of trees, create plots
			void create_plots();
			eire::ConfigReader *config_reader;
			/// for each file create a TreeReader to process the file
			void get_trees_from_files();

			std::map<std::string, sys8::TreeReader*> tree_readers;
			std::vector<TFile*> open_files;
			TFile *outfile;
	};
}

#endif
