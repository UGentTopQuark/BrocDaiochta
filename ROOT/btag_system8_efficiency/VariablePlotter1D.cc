#include "VariablePlotter1D.h"

sys8::VariablePlotter1D::VariablePlotter1D(std::string identifier, eire::ConfigReader *config_reader, TTree *tree, TFile *outfile, sys8::VariableHolder *var_holder)
{
	this->identifier = identifier;
	this->outfile = outfile;
	this->var_holder = var_holder;
	sys8_solve = new System8Solve();
	
	var_name = config_reader->get_var("var", identifier, true);
	//integrate_efficiency = config_reader->get_bool_var("integrate", identifier, false);
	input_file_name = config_reader->get_var("file_name", identifier, true);
	btag_algo_name = config_reader->get_var("btag_algo", identifier, true);
	binning = config_reader->get_vec_var("binning", identifier, true);
	data_per_bin.resize(binning.size()-1);
	//Per pt/eta/etc bin 8 numbers will need to be saved to go into system 8 equation.
	for(std::vector<std::vector<double> >::iterator data_pb = data_per_bin.begin();
	    data_pb != data_per_bin.end(); data_pb++){
		data_pb->resize(8,0);
	}
	
	std::string histo_title = config_reader->get_var("title", identifier);
	std::string histo_name = identifier;
	
	outfile->cd();
	
	var_index = -1;
	
	// initialise on-the-fly cuts
	prepare_cuts(config_reader, identifier);
}

void sys8::VariablePlotter1D::prepare_cuts(eire::ConfigReader *config_reader, std::string identifier)
{
  std::vector<std::string> variables = config_reader->get_variables_for_section(identifier);
  
  /*
   * search in configuration section for variables that are of the format:
   * cut_<var name> = <min cut value>:<max cut value>
   */
  std::vector<std::string> added_vars;
  for(std::vector<std::string>::iterator var = variables.begin();
      var != variables.end();
      ++var){
	  if(var->find("cut_") != std::string::npos){
		  std::string cut_variable(*var);
		  std::cout << "cut variable: " << *var << std::endl;
		  cut_variable = cut_variable.substr(4,std::string::npos);
		  std::cout << "variable: " << cut_variable << std::endl;
		  std::vector<std::string> cut_vals = config_reader->get_vec_var(*var,identifier,true);
		  if(cut_vals.size() < 2){
			  std::cout << "ERROR: cuts must always be of the form cut_<var name> = min:max" << std::endl;
			  exit(1);
		  }
		  double min_cut = atof(cut_vals[0].c_str());
		  double max_cut = atof(cut_vals[1].c_str());
		  // for each cut_ value create a new Cut object and store it in a vector of Cut objects
		  // for each event this vector will be processed and the cuts applied accordingly
		  // if an event fails the cut it will not be considered for the efficiency calculation
		  sys8::Cut new_cut(cut_variable, min_cut, max_cut);
		  var_holder->add_variable(cut_variable);
		  added_vars.push_back(cut_variable);
		  cuts.push_back(new_cut);
	  }
  }
  //Now add variables needed for counting if they haven't already been added
  std::vector<std::string> vars_to_add;
  vars_to_add.push_back(btag_algo_name);
  vars_to_add.push_back("mu_pt_rel");
  vars_to_add.push_back("away_jet_"+btag_algo_name);
  for(std::vector<std::string>::iterator add_v = vars_to_add.begin();
      add_v != vars_to_add.end(); add_v++){
	  if(find(added_vars.begin(),added_vars.end(),*add_v) == added_vars.end()){
		  var_holder->add_variable(*add_v);
	  }
  }

}

// get variable indices in VariableHolder for each variable that will be used later on
void sys8::VariablePlotter1D::configure()
{
  // variables for plotting
	// pass_bit_index = var_holder->get_pass_bit_index(pass_bit_name);
	var_index = var_holder->get_variable_index(var_name);
	
	// variables for on-the-fly cuts
	for(std::vector<sys8::Cut>::iterator cut = cuts.begin();
	    cut != cuts.end();
	    ++cut){
		cut->set_var_idx(var_holder->get_variable_index(cut->get_var_name()));	
	}
}

sys8::VariablePlotter1D::~VariablePlotter1D()
{
	if(efficiency_graph){delete efficiency_graph; efficiency_graph = NULL;}
	if(sys8_solve){delete sys8_solve; sys8_solve = NULL; }
}

// void sys8::VariablePlotter1D::integrate_histo(TH1F* ref_histo, TH1F* int_histo)
// {
// 	int nbinsX = ref_histo->GetNbinsX();
// 	for(int i = 1; i <= nbinsX; ++i){
// 		double int_for_bin=0;	// integral over histo for certain bin
// 		for(int j = 1; j <= i; ++j){
// 			int_for_bin += ref_histo->GetBinContent(j);
// 		}
// 		int_histo->SetBinContent(i, int_for_bin);
// 	}
// }

TGraphErrors* sys8::VariablePlotter1D::get_efficiency_graph()
{
	std::cout << "===========================================" << std::endl;
// 	std::cout << ">>> var / pass_bit / file: " << var_name << " / " << pass_bit_name << " / " << input_file_name << std::endl;

	const int nbins = data_per_bin.size();
	double y_axis[nbins];
	double y_ERR[nbins];
	double x_axis[nbins];
	double x_ERR[nbins];

	int i = 0;
	for(std::vector<std::vector<double> >::iterator data_pb = data_per_bin.begin();
	    data_pb != data_per_bin.end();data_pb++){
		std::vector<double> *eff = new std::vector<double>();
		std::vector<double> *eff_err = new std::vector<double>();
		eff->resize(8);
		eff_err->resize(8);
		
		sys8_solve->system8solve(*data_pb,eff,eff_err);
		
		double bin_lrange = atof(binning[i].c_str());
		double bin_hrange = atof(binning[i+1].c_str());

		//std::cout << " i " << i <<  "binl " <<  bin_lrange << " binh " <<  bin_hrange << std::endl;
		
		x_axis[i] = bin_lrange+((bin_hrange - bin_lrange)/2);
		x_ERR[i] = 0;
		y_axis[i] = (*eff)[0];
		y_ERR[i] = (*eff_err)[0];

		std::cout << "x: " << x_axis[i] << " eff " << y_axis[i] << " err " << y_ERR[i] << std::endl;


		i++;
	
	}
	efficiency_graph = new TGraphErrors(nbins,x_axis ,y_axis ,x_ERR ,y_ERR);
  
	return efficiency_graph;
}

void sys8::VariablePlotter1D::count()
{
	//	double btag_value = var_holder->get_btag_value(btag_algo_name);
	double value = var_holder->get_variable(var_index);
	
	// Check if event passes cuts that are applied on the fly.
	//dR mu jet away jet should be applied here if not already applied
	//if(dR < cut_dR){ return; }
	for(std::vector<sys8::Cut>::iterator cut = cuts.begin();
	    cut != cuts.end();
	    ++cut){
		double cut_var_value = var_holder->get_variable(cut->get_var_idx());
		if(cut_var_value < cut->get_min() || cut_var_value > cut->get_max())
			return;
	}

	//Now find out which value (= pt/eta/..) bin this fits into
	int bin_index = -1;
	bool greater_than_prev = false;
	bool less_than_current = false;
	for(int i = 0; i < (signed int) binning.size();i++){
		
		if(value >= atof(binning[i].c_str())){
			greater_than_prev = true;
		}else{
		        less_than_current = true;
		}
		
		if(greater_than_prev && less_than_current){
			bin_index = i-1;
			break;
		}
	}
  
	if(bin_index == -1){ return; }


	//no count the numbers needed for your 8 equations;
	//n = mu+jets in all-mu (mu+jet and away jet)
	//p = mu+jets in mu/jet (mu+jet and tagged away jet)
	//nmu = n mu tagged
	//pmu = p mu tagged
	//nbtag = n passing algo working point
	//pbtag = p passing algo working point
	//nall = n tagged by one or the other
	//pall = p tagged by one or the other

	
	data_per_bin[bin_index][0]++;

	double mu_pt_rel_value = var_holder->get_variable(var_holder->get_variable_index("mu_pt_rel"));	
	double btag_algo_value = var_holder->get_variable(var_holder->get_variable_index(btag_algo_name));
	double away_jet_btag_algo_value = var_holder->get_variable(var_holder->get_variable_index("away_jet_"+btag_algo_name));

	bool is_p = false;
	if(away_jet_btag_algo_value > 2.30){
		data_per_bin[bin_index][1]++;
		is_p = true;
	}


	if(mu_pt_rel_value > 0.8){
		data_per_bin[bin_index][2]++;
		data_per_bin[bin_index][6]++;
		if(is_p){
			data_per_bin[bin_index][3]++;
			data_per_bin[bin_index][7]++;
		}
	}
	if(btag_algo_value > 5.3){
		data_per_bin[bin_index][4]++;
		data_per_bin[bin_index][6]++;
		if(is_p){
			data_per_bin[bin_index][5]++;
			data_per_bin[bin_index][7]++;
		}
	}

}
