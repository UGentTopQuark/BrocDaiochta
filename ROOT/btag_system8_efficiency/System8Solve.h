#ifndef SYSTEM8SOLVE_H
#define SYSTEM8SOLVE_H

/**
 * \class System8Solve
 *
 * \brief Solves 8 equations to measure btagging efficiencyl.
 *
 */


#include "ConfigReader/ConfigReader.h"

class System8Solve{
	public:
	void system8solve(std::vector<double> data, std::vector<double> *eff, std::vector<double> *err);

        private:
	double sqr(double x);
	const static bool verbose = true;

};


#endif

