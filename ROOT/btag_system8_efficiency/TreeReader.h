#ifndef SYS8_TREEREADER_H
#define SYS8_TREEREADER_H

/**
 * \class sys8::TreeReader
 *
 * \brief Manage a Tree within a file and all variables (and plotting thereof) within the tree
 *
 * \author bklein
 */

#include "TTree.h"
#include "TFile.h"
#include "VariableHolder.h"
#include "VariablePlotter1D.h"
//#include "VariablePlotter2D.h"
#include "ConfigReader/ConfigReader.h"
#include <map>

namespace sys8{
	class TreeReader{
		public:
			TreeReader(TFile *outfile);
			~TreeReader();

		        //bool plot_2D;
					
			inline void set_tree(TTree *tree){ this->tree = tree; };
			inline void set_config_reader(eire::ConfigReader *config_reader){ this->config_reader = config_reader; };
			/// adds new variable for plotting (which equals a section in the config file)
			void add_section(std::string section);
			/// for each VariablePlotter make sure that
			/// VariableHolder knows all variables that the
			/// VariablePlotter will need later on
			void configure_variables();
			void read();
			/// returns efficiency graph for combination of plots in TagAndProbeManager
			TGraphErrors *get_efficiency_graph(std::string section);
			//TH2F *get_efficiency_graph2D(std::string section);
			std::vector<std::string> *get_variables();
		private:
			TFile *outfile;
			TTree *tree;
			eire::ConfigReader *config_reader;
			/// holds map<section name for variable, VariablePlotter* for variable>
			std::map<std::string, sys8::VariablePlotter1D*> variables;
                        //std::map<std::string, sys8::VariablePlotter2D*> variables2;
			/// variables to plot (equals section names in config file!)
			std::vector<std::string> *variable_list;
			
			/**
			 * \brief Store all variables for a file.
			 *
			 * The handling of the variables is a bit tricky in
			 * root. A root variable in a file can be assigned to
			 * only one variable at a time in the analyser code.
			 * Therefore, every time we encounter a new variable,
			 * VariableHolder needs to assign it internally to one
			 * variable and provide it then for several classes.
			 * For example, we want to plot the L1 efficiency and
			 * the HLT efficiency in the same file as function of
			 * eta. We would create two instances of
			 * VariablePlotter and both would need to "share" the
			 * same eta variable from the root file.
			 **/
			sys8::VariableHolder *var_holder;
	};
}

#endif
