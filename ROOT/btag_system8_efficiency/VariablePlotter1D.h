#ifndef SYS8_VARIABLEPLOTTER1D_H
#define SYS8_VARIABLEPLOTTER1D_H

/**
 * \class sys8::VariablePlotter1D
 *
 * \brief Plots 1D efficiency curves.
 *
 * For each variable (section in the config file) one VariablePlotter object is
 * booked and manages the plotting of the variables and the efficiency graph
 * calculation.
 *
 * \author bklein
 */

#include "ConfigReader/ConfigReader.h"
#include "TH1F.h"
#include "TTree.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TFile.h"
#include "VariableHolder.h"
#include "System8Solve.h"
#include "Cut.h"

namespace sys8{
	class VariablePlotter1D{
		public:
			VariablePlotter1D(std::string identifier, eire::ConfigReader *config_reader, TTree *tree, TFile *outfile, sys8::VariableHolder *var_holder);
			~VariablePlotter1D();
			/// called for each event. fills counters needed to solve 8 eqns.
			void count();
			/// return efficiency graph for all events. Call System8Solve per bin			
			TGraphErrors *get_efficiency_graph();
			/// return the name of the variable that this class processes
			inline std::string get_var_name() { return var_name; };
			/// manage variable assignment with VariableHolder
			void configure();

		private:
			void prepare_cuts(eire::ConfigReader *config_reader, std::string identifier);
			//	void integrate_histo(TH1F* ref_histo, TH1F* int_histo);
			std::string identifier;
			//TH1F *all_histo, *pass_histo, *fail_histo, *int_all_histo, *int_pass_histo;
			std::string btag_algo_name;
			TGraphErrors *efficiency_graph;
			TFile *outfile;
			std::string input_file_name;
			std::string var_name;
			//std::string pass_bit_name;
			int var_index;
			//int pass_bit_index;
			//bool integrate_efficiency;
			//double x_min_, x_max_;

			std::vector<std::string> binning;
			std::vector<std::vector<double> > data_per_bin;

			std::vector<sys8::Cut> cuts;

			sys8::VariableHolder *var_holder;
			System8Solve *sys8_solve; 
	};
}

#endif
