#include "EfficiencyPurity.h"

EfficiencyPurity::EfficiencyPurity(TFile *outfile)
{
	this->outfile = outfile;
	signal_histo = NULL;
	background_histo = NULL;
}

EfficiencyPurity::~EfficiencyPurity()
{
}

void EfficiencyPurity::set_background_histo(TH1F *background)
{
	background_histo = background;
}

void EfficiencyPurity::set_signal_histo(TH1F *signal)
{
	signal_histo = signal;
}

void EfficiencyPurity::set_symmetric_cut(bool symmetric)
{
	symmetric_cut = symmetric;
}

void EfficiencyPurity::calculate()
{
	if(background_histo == NULL || signal_histo == NULL)
		return;
	outfile->cd();

	std::string hname = (std::string) "EfficiencyXPurity_"+signal_histo->GetName();
	TH1F eff_vs_pur(hname.c_str(),hname.c_str(), signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	hname = (std::string) "Efficiency_"+signal_histo->GetName();
	TH1F eff(hname.c_str(),hname.c_str(), signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	hname = (std::string) "Purity_"+signal_histo->GetName();
	TH1F pur(hname.c_str(),hname.c_str(), signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	eff_vs_pur.SetXTitle(signal_histo->GetXaxis()->GetTitle());

	int max_i=0;
	if(symmetric_cut)
		max_i =  (int) ((signal_histo->GetNbinsX()/2.0)+0.5);
	else
		max_i = signal_histo->GetNbinsX();

	double max_purxeff = -1;
	double optimal_cut = -1;
	for(int i = 1; i <= max_i; ++i){
		double sig_sum_beyond=0;
		double bg_sum_beyond=0;
		int max_j = 0;
		if(symmetric_cut)
			max_j = signal_histo->GetNbinsX() - i;
		else
			max_j = signal_histo->GetNbinsX();

		for(int j=i; j <= max_j; ++j){
			sig_sum_beyond += signal_histo->GetBinContent(j);
			bg_sum_beyond += background_histo->GetBinContent(j);
		}
		double efficiency = 0;
		if(signal_histo->Integral() != 0)
			efficiency = sig_sum_beyond / signal_histo->Integral();
		double purity = 0;
		if((sig_sum_beyond + bg_sum_beyond) != 0)
			purity = sig_sum_beyond / (sig_sum_beyond + bg_sum_beyond);
		std::cout << "step: " << i << " value: " << signal_histo->GetBinCenter(i) << " eff: " << efficiency << " pur: " << purity << " exp: " << efficiency*purity << std::endl;
		eff_vs_pur.SetBinContent(i,efficiency*purity);
		eff_vs_pur.SetBinError(i,0);
		eff.SetBinContent(i,efficiency);
		eff.SetBinError(i,0);
		pur.SetBinContent(i,purity);
		pur.SetBinError(i,0);

		if(purity > max_purxeff){
		  // max_purxeff = (purity*efficiency)/(1-purity);
		   max_purxeff = purity;
			optimal_cut = signal_histo->GetBinCenter(i);
		}
	}

	CanvasHolder cholder;
	cholder.addHisto(&pur, "#pi", "L");
	cholder.addVLine(optimal_cut, "max.#pi");
//	cholder.addHisto(&eff, "#epsilon", "L");
//	cholder.addHisto(&pur, "#pi", "L");
//	cholder.setLogY();
	cholder.setOptStat(000000);
	cholder.write(outfile);
}
