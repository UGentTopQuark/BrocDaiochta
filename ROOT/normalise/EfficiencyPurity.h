#ifndef EFFICIENCYPURITY_H
#define EFFICIENCYPURITY_H
#include "TH1F.h"
#include "TFile.h"
#include <iostream>
#include "Canvas_Holder.h"

class EfficiencyPurity{
	public:
		EfficiencyPurity(TFile *outfile);
		~EfficiencyPurity();
		void set_background_histo(TH1F *background);
		void set_symmetric_cut(bool symmetric);
		void set_signal_histo(TH1F *signal);
		void calculate();
	private:
		TH1F *background_histo;
		TH1F *signal_histo;
		TFile *outfile;
		bool symmetric_cut;
};

#endif
