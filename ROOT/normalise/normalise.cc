#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "math.h"
#include "TPaveLabel.h"
#include "TLatex.h"
#include "Canvas_Holder.h"
#include <sstream>

int main(int argc, char **argv)
{
	std::string cutset ="07_cutset" ;
	std::string channel = "electron";
	std::string filename_out= ("normalised.root");
	TFile *outfile = new TFile(filename_out.c_str(),"recreate");

	//std::string filename_in= "tprime_300.root";
	//std::string dataset = "Tprime300";
	std::vector<std::pair<std::string,std::string> > filenames_in;
	filenames_in.push_back(std::pair<std::string,std::string>("ttbar_wo_trigger.root","TTbarTrigger"));
	//filenames_in.push_back(std::pair<std::string,std::string>("ttbar.root","TTbar"));
// 	filenames_in.push_back(std::pair<std::string,std::string>("tprime_250.root","Tprime250")); 
// 	filenames_in.push_back(std::pair<std::string,std::string>("tprime_300.root","Tprime300")); 
// 	filenames_in.push_back(std::pair<std::string,std::string>("tprime_350.root","Tprime350")); 
// 	filenames_in.push_back(std::pair<std::string,std::string>("tprime_400.root","Tprime400")); 
	//filenames_in.push_back(std::pair<std::string,std::string>("wjets.root","Wjets"));
	//filenames_in.push_back(std::pair<std::string,std::string>("zjets.root","Zjets"));
	//filenames_in.push_back(std::pair<std::string,std::string>("qcdmupt15.root","Mupt15"));
	//filenames_in.push_back(std::pair<std::string,std::string>("ttbar.root","TTbar"));
	int ttbar_i = 1;
	int filename_count = 0;

	for(std::vector<std::pair<std::string,std::string> >::iterator filename = filenames_in.begin();filename != filenames_in.end();filename++){
		std::pair<std::string,std::string> filename_in = (*filename);
		TFile *file = new TFile((filename_in.first).c_str(),"open");

		std::vector<std::string> *histo_names = new std::vector<std::string>();
		//histo_names->push_back("muon_event_triggered_lepton_not_"+filename_in.second);
		histo_names->push_back("electron_event_triggered_lepton_not_"+filename_in.second);

		//histo_names->push_back("bjet_event_passed_cut_"+filename_in.second);
		//		histo_names->push_back("bjets_in_event_"+filename_in.second);
// 		histo_names->push_back("bjets_in_event_bid_"+filename_in.second);
//		histo_names->push_back("bjets_in_event_allbjets_"+filename_in.second);
// 		histo_names->push_back("bjets_in_event_bidandreco_"+filename_in.second);
// 		histo_names->push_back("top_had_jets_genmatched_"+filename_in.second);
		//histo_names->push_back("top_jets_genmatched_"+filename_in.second);
// 		histo_names->push_back("bjets_bid_is_recomatch_HighEff_"+filename_in.second);
// 		histo_names->push_back("bidreco_disagree_recomatchother_HighEff_"+filename_in.second);
		
		//histo_names->push_back("bjets_highest_is_real_JetBProb_"+filename_in.second);
		//histo_names->push_back("bjets_second_highest_is_real_JetBProb_"+filename_in.second);

// 		histo_names->push_back("bjets_highest_is_real_bid_HighEff_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_bid_HighEff_"+filename_in.second);
//  		histo_names->push_back("bjets_highest_is_real_HighEff_"+filename_in.second);
//  		histo_names->push_back("bjets_second_highest_is_real_HighEff_"+filename_in.second);
//   		histo_names->push_back("bjets_two_highest_are_real_HighEff_"+filename_in.second);

		//	histo_names->push_back("bjets_highest_is_real_HighPur_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_HighPur_"+filename_in.second);
// 		histo_names->push_back("bjets_highest_is_real_SoftMuNoIP_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_SoftMuNoIP_"+filename_in.second);
// 		histo_names->push_back("bjets_highest_is_real_SoftMu_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_SoftMu_"+filename_in.second);
// 		histo_names->push_back("bjets_highest_is_real_SoftEle_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_SoftEle_"+filename_in.second);
		//	histo_names->push_back("bjets_highest_is_real_SSVertex_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_SSVertex_"+filename_in.second);
		//	histo_names->push_back("bjets_highest_is_real_CSVertex_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_CSVertex_"+filename_in.second);
// 		histo_names->push_back("bjets_highest_is_real_CSVertexMVA_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_CSVertexMVA_"+filename_in.second);
		//	histo_names->push_back("bjets_highest_is_real_JetProb_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_JetProb_"+filename_in.second);
		//	histo_names->push_back("bjets_highest_is_real_JetBProb_"+filename_in.second);
// // 		histo_names->push_back("bjets_second_highest_is_real_JetBProb_"+filename_in.second);
// 		histo_names->push_back("bjets_highest_is_real_IPMVA_"+filename_in.second);
// 		histo_names->push_back("bjets_second_highest_is_real_IPMVA_"+filename_in.second);

		CanvasHolder *cholders[histo_names->size()];
		int canvas_count = 0;
		
		for(std::vector<std::string>::iterator histo_name = histo_names->begin();histo_name !=histo_names->end();++histo_name){
			std::string complete_histo_name; 
			//if(filename_in.second == "TTbar" && ttbar_i == 1){
	

			complete_histo_name = (*histo_name+"|"+channel+"|"+cutset);
	
				//	ttbar_i++;}
		//	else
		//	complete_histo_name = (*histo_name+"|mu_background|"+cutset);

			//Unrelated, normalising matched jets counter histogram
			TH1F *histo = new TH1F(*((TH1F *) file->GetDirectory("eventselection")->Get(complete_histo_name.c_str())));
	
			double scale = 1.0;
			double histo_int = histo->Integral();
			if(histo->Integral() != 0){
				histo->Sumw2();
				scale = 1.0/histo->Integral();
			
				histo->Scale(scale);    
				//jets_matched_histo->Scale(cross_section*integrated_lumi*efficiencies);
				
				//print bin content
				std::cout << *histo_name << std::endl;				   
				int numbins = histo->GetNbinsX() + 1; 


				/**********for TgraphErrors: get name of algo and std::out********/
				std::string sig_hist_name = histo->GetName();
				int found = sig_hist_name.find("bjets_highest_is_real_");
			// 	if(found != std::string::npos){
// 					int pos_mu = sig_hist_name.find("|mu");
// 					int pos_real = sig_hist_name.find("real_");
// 					int range_algo_name = 0;
// 					if(filename_in.second == "TTbar")
// 						range_algo_name = (pos_mu-6)-(pos_real+5);
// 					else
// 						range_algo_name = (pos_mu-10)-(pos_real+5);
					
// 					std::string name_b_algo = sig_hist_name.substr((pos_real+5),range_algo_name);
// 					//only output for second bin
// 					int i = 2;
// 					double efficiency = histo->GetBinContent(i);
// 					double binom_err = sqrt((efficiency*(1 - efficiency))/(histo_int));
// 					std::cout  << "y_axis_map[\""<<name_b_algo <<"\"]["<< filename_count << "] = "<<(efficiency*100)<<";" <<std::endl;
// 					std::cout  << "y_ERR_map[\""<<name_b_algo <<"\"]["<< filename_count << "] = "<<(binom_err*100)<<";" <<std::endl;
					
// 				}
				/***************************************************************/
				//	else{

					found = sig_hist_name.find("_real_");
					int foundHLT = sig_hist_name.find("trigger");
					if(found != std::string::npos || foundHLT != std::string::npos){
						for(int i = 1;i<numbins;i++){					
							double efficiency = histo->GetBinContent(i);
							double binom_err = sqrt((efficiency*(1 - efficiency))/(histo_int));
							std::cout <<"binom err " << i <<" :" << (efficiency*100) <<" pm " <<(binom_err*100) <<std::endl;
						}
					}
					else{
						for(int i = 1;i<numbins;i++)
							{		
							double efficiency = histo->GetBinContent(i);
							std::cout <<"sumw2 err " << i <<" :" << (efficiency*100) <<" pm " << histo->GetBinError(i)*100<<std::endl;							
							
							}
					}
					//}
				
				cholders[canvas_count] = new CanvasHolder();
				std::string combined_title = (*histo_name);
				cholders[canvas_count]->setCanvasTitle(combined_title);
				cholders[canvas_count]->addHisto(histo,combined_title.c_str());
				cholders[canvas_count]->setOptStat(000000);
				cholders[canvas_count]->setLineColors(kGray+1);
				cholders[canvas_count]->setBordersY(0,1);
				cholders[canvas_count]->write(outfile);
				cholders[canvas_count]->save("eps");
				++canvas_count;
				
			}
		}

		filename_count ++;

		delete file;
		file=NULL;
	}

  delete outfile;
  outfile=NULL;

}
