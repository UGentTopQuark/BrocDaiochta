#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>
#include "Canvas_Holder.h"
#include "MergeHistos.h"

struct interval{
        double min;
	double centre;
        double max;
        double mean;
};

interval get_x_percent_interval(TH1F *histo, double percentage);
int get_quantile(TH1F* hrecomass, const double quantile);

int main(int argc, char **argv)
{
	std::map<std::string,std::string> dictionary;
	dictionary["top_mass_minimisation_"]="min.diff.M3";
	dictionary["top_mass_"]="M3";
//	dictionary["top_mass_minimisation_w_METpz_"]="min.diff.M3, long. #nu";
	dictionary["top_Had_1btag_chimass_"]="had. t', 1 b-tag";
	dictionary["top_Lep_1btag_chimass_"]="lep. t', 1 b-tag";
	dictionary["top_Had_2btag_chimass_"]="had. t', 2 b-tag";
	dictionary["top_Lep_2btag_chimass_"]="lep. t', 2 b-tag";
	dictionary["top_hadmass_Wb_0btag_"]="had. t', 0 b-tag";
	dictionary["top_hadmass_Wb_1btag_"]="had. t', 1 b-tag";
	dictionary["top_hadmass_Wb_2btag_"]="had. t', 2 b-tag";
	dictionary["top_lepmass_Wb_0btag_"]="lep. t', 0 b-tag";
	dictionary["top_lepmass_Wb_1btag_"]="lep. t', 1 b-tag";
	dictionary["top_lepmass_Wb_2btag_"]="lep. t', 2 b-tag";
	dictionary["top_Had_chimass_"]="had. t', no b-tag";
	dictionary["top_Lep_chimass_"]="lep. t', no b-tag";
	//dictionary["top_mass_w_METpz_"]="M3, long. #nu";
	dictionary["tprime"]="t'";

	std::string mass = "250";
	std::string dataset = "tprime";
	std::string signal_background = "";
	std::string cutset = "08_cutset";

        if(argc > 1){
                mass = argv[1];
                signal_background = argv[2];
        }
	std::string version = "";

	std::map<std::string,std::vector<std::string> > file_names;

	std::vector<std::string> histo_names;
	histo_names.push_back("top_mass_");
	histo_names.push_back("top_mass_minimisation_");
	histo_names.push_back("top_Had_chimass_");
	histo_names.push_back("top_Had_1btag_chimass_");
	histo_names.push_back("top_Had_2btag_chimass_");
	histo_names.push_back("top_Lep_chimass_");
	histo_names.push_back("top_Lep_1btag_chimass_");
	histo_names.push_back("top_Lep_2btag_chimass_");
	histo_names.push_back("top_hadmass_Wb_0btag_");
	histo_names.push_back("top_hadmass_Wb_1btag_");
	histo_names.push_back("top_hadmass_Wb_2btag_");
	histo_names.push_back("top_lepmass_Wb_0btag_");
	histo_names.push_back("top_lepmass_Wb_1btag_");
	histo_names.push_back("top_lepmass_Wb_2btag_");
//	histo_names.push_back("top_mass_w_METpz_");
//	histo_names.push_back("top_mass_minimisation_w_METpz_");
	std::vector<CanvasHolder*> objects_to_delete;

	TFile *outfile = new TFile("outfile.root", "RECREATE");
	MergeHistos *mhistos = new MergeHistos(outfile);

	mhistos->set_cutset(cutset);

	for(std::vector<std::string>::iterator histo_name = histo_names.begin();
		histo_name != histo_names.end();
		++histo_name)
	{
		std::map<std::string,std::vector<std::string> > file_names;
		if(signal_background == "s"){
			if(mass == "175"){
				file_names["ttbar"+version+".root"].push_back(*histo_name+"TTbar|muon|"+cutset);
				file_names["ttbar"+version+".root"].push_back(*histo_name+"TTbar|mu_background|"+cutset);
			}
			else{
				file_names["tprime_"+mass+version+".root"].push_back(*histo_name+"Tprime"+mass+"|muon|"+cutset);
				file_names["tprime_"+mass+version+".root"].push_back(*histo_name+"Tprime"+mass+"|mu_background|"+cutset);
			}
		}else if(signal_background == "b"){
			if(mass != "175"){
				file_names["ttbar"+version+".root"].push_back(*histo_name+"TTbar|mu_background|"+cutset);
			}
			file_names["wjets"+version+".root"].push_back(*histo_name+"Wjets|mu_background|"+cutset);
			file_names["zjets"+version+".root"].push_back(*histo_name+"Zjets|mu_background|"+cutset);
			file_names["qcdmupt15"+version+".root"].push_back(*histo_name+"Mupt15|mu_background|"+cutset);
			file_names["t_t_chan"+version+".root"].push_back(*histo_name+"TtChan|mu_background|"+cutset);
			file_names["t_s_chan"+version+".root"].push_back(*histo_name+"TsChan|mu_background|"+cutset);
			file_names["t_tW_chan"+version+".root"].push_back(*histo_name+"TtWChan|mu_background|"+cutset);
		}
		mhistos->set_file_names(file_names);

		TH1F *histo = mhistos->combine_histo(*histo_name, mass);
	
		interval myint = get_x_percent_interval(histo, 0.68);
	
		CanvasHolder *cholder = new CanvasHolder;
		objects_to_delete.push_back(cholder);
	
	
		cholder->addHisto(histo,dictionary[*histo_name],"");
		cholder->setCanvasTitle(*histo_name);
		std::string tmp_dataset = histo->GetTitle();
		tmp_dataset += ": "+dictionary[dataset]+" ("+mass+" Gev)";
		cholder->setTitle(tmp_dataset);
		cholder->addVLine(myint.min,"smallest 68\% interval",2,1,1);
		cholder->addVLine(myint.mean, "mean of 68\% interval", 2,2,3);
		cholder->addVLine(myint.max,"",2,1,1);
		cholder->setLineColors(17);
		cholder->setOptStat(00000);
		cholder->setTitleX("mass (GeV)");
		cholder->setTitleY("Events [200/pb]");
	
		std::cout << *histo_name << " " << dataset << " width: " << myint.max - myint.min << " mean: " << myint.mean<< std::endl;

		cholder->save("eps");
		cholder->write(outfile);
		histo->Write();
	}

	delete outfile;
	outfile = NULL;
}

interval get_x_percent_interval(TH1F *histo, double percentage)
{
        interval myint;

        int smallest_interval = histo->GetNbinsX();
        double entries = histo->Integral();

        int min_bin=1;
        int max_bin=histo->GetNbinsX();
        int diff_to_med=histo->GetNbinsX(); // variable for balancing the interval around the median

        double current_entries=0.;
        double max_entries=0.;
        int current_diff_to_med = histo->GetNbinsX();
        //int current_diff_to_med = histo->GetMaximumBin();

        int median_bin = get_quantile(histo, 0.5);

        for(int i=1; i <= histo->GetNbinsX(); ++i){
                for(int j=i; j <= histo->GetNbinsX(); ++j){
                        if(current_entries < entries*percentage){
                                current_entries += histo->GetBinContent(j);
                        }
                        if(current_entries > entries*percentage){
                                int current_length = j - i;
                                if(current_length <= smallest_interval){
                                        if(current_length == smallest_interval){
                                                current_diff_to_med=(abs(min_bin-median_bin) - abs(max_bin - median_bin));
                                                if(!(abs(min_bin-median_bin) +
                                                     abs(max_bin-median_bin) <
                                                     abs(j - median_bin) +
                                                     abs(i - median_bin)) &&
                                                     diff_to_med >= current_diff_to_med && 
						     current_entries > max_entries)
                                                {
                                                        smallest_interval = current_length;
                                                        min_bin = i;
                                                        max_bin = j;
							max_entries = current_entries;
                                                        diff_to_med=current_diff_to_med;
							break;
                                                }
                                        }
                                         else{
                                                smallest_interval = current_length;
                                                min_bin = i;
                                                max_bin = j;
						max_entries = current_entries;
						break;
                                         }

                                }
                        }
                }
                current_entries=0.;
        }

        myint.min = histo->GetBinCenter(min_bin)-histo->GetBinWidth(1)/2.0;
        myint.max = histo->GetBinCenter(max_bin)+histo->GetBinWidth(1)/2.0;
        myint.centre=histo->GetBinCenter(min_bin+(max_bin-min_bin)/2);
	double tmp_sum = 0;
	double tmp_bin_contents = 0;
	for(int i = min_bin; i <= max_bin; ++i){
		tmp_sum += histo->GetBinCenter(i)*histo->GetBinContent(i);
		tmp_bin_contents += histo->GetBinContent(i);
	}
        myint.mean= tmp_sum/tmp_bin_contents;

        return myint;
}

int get_quantile(TH1F* hrecomass, const double quantile)
{
        double entries_up_to_bin=0.;
        double overall_entries=hrecomass->Integral();
        int quantile_bin = 0;
        for(int i = 0; i < hrecomass->GetNbinsX(); ++i){
                entries_up_to_bin += hrecomass->GetBinContent(i);
                if(entries_up_to_bin > overall_entries * quantile){
                        quantile_bin=i;
                        break;
                }
        }

        return quantile_bin;
}
