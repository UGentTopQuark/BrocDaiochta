#!/usr/bin/perl

use Tie::IxHash::Easy;

$dictionary{"top_mass_"} = "M3";
$dictionary{"top_mass_minimisation_"} = "min.diff.";
$dictionary{"top_hadmass_Wb_0btag_"} = "Wb";
$dictionary{"top_hadmass_Wb_1btag_"} = "Wb-1b";
$dictionary{"top_hadmass_Wb_2btag_"} = "Wb-2b";
$dictionary{"top_lepmass_Wb_0btag_"} = "lep Wb";
$dictionary{"top_lepmass_Wb_1btag_"} = "lep Wb-1b";
$dictionary{"top_lepmass_Wb_2btag_"} = "lep Wb-2b";
$dictionary{"top_Had_chimass_"} = '$\\chi$';
$dictionary{"top_Had_1btag_chimass_"} = '$\\chi$-1b';
$dictionary{"top_Had_2btag_chimass_"} = '$\\chi$-2b';
$dictionary{"top_Lep_chimass_"} = 'lep $\\chi$';
$dictionary{"top_Lep_1btag_chimass_"} = 'lep $\\chi$-1b';
$dictionary{"top_Lep_2btag_chimass_"} = 'lep $\\chi$-1b';

my %entries = ();
tie %entries, "Tie::IxHash::Easy";
my $mass = 0;
while (<>) 
{
	if(/processing (\d+)/){
		$mass = $1;
	}elsif(/^([\w\d_]+)\s+tprime width: ([\d.]+)/){
		$entries{$mass}{$1} = $2;
	}
}

print_table();

sub print_table
{
	my @keys = keys %entries;
	my $first_mass = pop @keys;
	@methods = keys %{$entries{$first_mass}};
	foreach my $method (@methods){
			print $dictionary{$method};
		foreach my $mass (keys %entries){
			my $value = $entries{$mass}{$method};
			print " & $value";
		}
		print "\\\\\n";
	}
}
