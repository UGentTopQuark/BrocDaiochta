#!/bin/bash

for i in 175 250 300 350 400; do
	echo "processing ${i}...";
	./mass_quality ${i} s 2> /dev/null | grep width;
	mkdir ${i};
	mv *.eps ${i};
	cd ${i}; for j in `ls *.eps`; do
			epstopdf ${j};
		done
		rm *.eps;
		cd ..;
done

echo "processing 0...";
