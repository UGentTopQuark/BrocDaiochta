#include "Combiner.h"

Combiner::Combiner()
{
	// where to get information from about the filter efficiency
	// --> important for scaling of datasets
	// event_counter_histo bin 0: one entry for each processed event
	// event_counter_histo bin 1: one entry for each event passing the cuts
	event_counter_histo_name="event_counter_";

	// sequence for the stacked layers
	sequence=NULL;

	colours=NULL;
	rebin=NULL;
	underflow=NULL;
	logscale=NULL;
	yaxes=NULL;
	xaxes=NULL;
	mass = "";
	combine_eff_plots = false;
	scale_mc_to_data = false;
	overlay_cutsets = false;
	auto_fill_sequence = false;
	generate_pseudo_data = false;
	add_histos_stacked = true;
	do_calculate_efficiency = true;
	luminosity = -1;
	directory_name = "eventselection";
	

	// output file -> to this file all the combined output histograms will be written
	// FIXME: this should be a parameter
	outfile = new TFile("test.root", "recreate");
}


Combiner::~Combiner()
{
}

/*********************************************************
Multiple ways to add plots to be combined on one canvas
*********************************************************/


//Fill a map [histoname][identifier] = TH1F
void Combiner::add_plots(std::map<std::string,std::vector<std::string> > &files_with_content, std::vector<std::string> &plot_names)
{
	// cf. definition in header
	// file_with_content->first: filename
	// file_with_content->second: vector of datasets within file
	// 			--> important because in ttbar.root files
	// 			    3 types of events will be included
	// 			    (ttbar-e, ttbar-mu, ttbar-other)
	for(std::map<std::string, std::vector<std::string> >::iterator file_with_content = files_with_content.begin();
	    file_with_content != files_with_content.end();
	    ++file_with_content){
		// open input file from CMSSW analyser
		TFile *file = new TFile(file_with_content->first.c_str(),"open");

		// loop over vector of names of histograms that shall be read out
		// from the CMSSW root files and shall be combined
		for(std::vector<std::string>::iterator plot_name = plot_names.begin();
		    plot_name != plot_names.end();
		    ++plot_name){

			// loop over the data-type within the root file (ttbar-e, ttbar-mu, ...)
			// this is the postfix within the CMSSW files, eg. njets_muon_ttbar-e
			for(std::vector<std::string>::iterator postfix = file_with_content->second.begin();
			    postfix != file_with_content->second.end();
			    ++postfix){

				std::string complete_name = *plot_name+*postfix; 
				add_plot_to_combine(file, complete_name,*plot_name,*postfix);
				
			}
		}
		delete file;
		file=NULL;
	}
}

void Combiner::add_plots_by_cutset(std::string file_with_content, std::vector<std::string> cutsets, std::vector<std::string> plot_names)
{
	overlay_cutsets = true;
	if(sequence == NULL){
		sequence = new std::vector<std::string>();
		*sequence = cutsets;
	}
	// open input file from CMSSW analyser
	TFile *file = new TFile((file_with_content).c_str(),"open");

	for(std::vector<std::string>::iterator cutset = cutsets.begin();
	    cutset != cutsets.end();
	    ++cutset)
	{

		for(std::vector<std::string>::iterator plot_name = plot_names.begin();
		    plot_name != plot_names.end();
		    ++plot_name)
			{

				std::string full_plot_name = *plot_name + *cutset;
				add_plot_to_combine(file, full_plot_name,*plot_name, *cutset);
			
			}
			
	}
		delete file;
		file=NULL;

}

void Combiner::add_plots_by_predot(std::string file_with_content, std::vector<std::string> predots, std::vector<std::string> plot_names)
{
	if(sequence == NULL){
		sequence = new std::vector<std::string>();
		*sequence = predots;
	}
	// open input file from CMSSW analyser
	TFile *file = new TFile((file_with_content).c_str(),"open");

	for(std::vector<std::string>::iterator predot = predots.begin();
	    predot != predots.end();
	    ++predot)
	{

		for(std::vector<std::string>::iterator plot_name = plot_names.begin();
		    plot_name != plot_names.end();
		    ++plot_name)
			{

				
				std::string full_plot_name = *predot + *plot_name ;
				if(*predot == "Eff_HLTvsL1Seed_matched."){
					std::string tmp_name = *plot_name;
					tmp_name.erase(0,7);
					full_plot_name = *predot+"HLT_Mu9_HLTvsL1Seed"+tmp_name;
				}
				std::cout << "L1/HLT overlay full name " << full_plot_name << std::endl;
				add_plot_to_combine(file, full_plot_name,*plot_name, *predot);
			
			}
			
	}
		delete file;
		file=NULL;

}

void Combiner::add_plots_by_region(std::string file_with_content, std::vector<std::string> regions, std::vector<std::string> plot_names)
{
	if(sequence == NULL){
		sequence = new std::vector<std::string>();
		*sequence = regions;
	}
	// open input file from CMSSW analyser
	TFile *file = new TFile((file_with_content).c_str(),"open");

	for(std::vector<std::string>::iterator region = regions.begin();
	    region != regions.end();
	    ++region)
	{

		for(std::vector<std::string>::iterator plot_name = plot_names.begin();
		    plot_name != plot_names.end();
		    ++plot_name)
			{
				std::string common_plot_name = *plot_name;
				std::string temp_plot_name = *plot_name;
				std::string full_plot_name;
				if(common_plot_name.find("trig_obj") != std::string::npos){
					temp_plot_name.replace(common_plot_name.find("_trig_obj"),1,"_"+*region+"_");
					full_plot_name = temp_plot_name;
				}else{
					std::string sub_str = temp_plot_name.substr(0, temp_plot_name.find("|"));
					full_plot_name = temp_plot_name.insert(sub_str.rfind("_"),"_"+*region);
				}
				if(verbose) std::cout <<"FULL PLOT NAME: " << full_plot_name << std::endl; 
				add_plot_to_combine(file, full_plot_name,common_plot_name, *region);
			
			}
			
	}
		delete file;
		file=NULL;

}

void Combiner::add_vplots(std::vector<std::string> files_with_content, std::vector<std::string> plot_names)
{

	if(sequence == NULL){
		sequence = new std::vector<std::string>();
		auto_fill_sequence = true;
	}

	// cf. definition in header
	// file_with_content->first: filename
	// file_with_content->second: vector of datasets within file
	// 			--> important because in ttbar.root files
	// 			    3 types of events will be included
	// 			    (ttbar-e, ttbar-mu, ttbar-other)
	for(std::vector<std::string>::iterator file_with_content = files_with_content.begin();
	    file_with_content != files_with_content.end();
	    ++file_with_content)
	{
		// open input file from CMSSW analyser
		TFile *file = new TFile((*file_with_content).c_str(),"open");

		std::string cleaned_filename = (*file_with_content).substr(0,(*file_with_content).find(".root"));
		std::string map_entry = cleaned_filename+"_"+directory_name; //Complicated for use with DQMOffline

		if(auto_fill_sequence) 
			sequence->push_back(map_entry);

		for(std::vector<std::string>::iterator plot_name = plot_names.begin();
		    plot_name != plot_names.end();
		    ++plot_name)
			{
				add_plot_to_combine(file, *plot_name,*plot_name, map_entry);
				
				
					
			}
		delete file;
		file=NULL;
				
	}

}


/**************************************************
End of plot adding section
**************************************************/

void Combiner::set_scale_mc_to_data(bool do_scale)
{
	scale_mc_to_data = do_scale;
}

void Combiner::set_calculate_efficiency(bool calc_eff)
{
	do_calculate_efficiency = calc_eff;
}

void Combiner::set_mass(std::string mass)
{
	this->mass = mass;
}

void Combiner::set_colours(std::vector<int> *colour_scheme)
{
	colours = colour_scheme;
}

void Combiner::set_eff_plots(bool eff_plots)
{
        combine_eff_plots = eff_plots;
}

void Combiner::set_input_directory(std::string root_dir)
{
	directory_name = root_dir;
}

void Combiner::set_cross_section(std::string process, double cross_section)
{
	// cross section for data type
	cross_sections[process] = cross_section;
}

void Combiner::set_sequence_to_plot(std::vector<std::string> *sequence_to_plot)
{
	sequence = sequence_to_plot;
}

void Combiner::set_luminosity(double integrated_lumi)
{
	luminosity = integrated_lumi;
}

void Combiner::set_overlay_cutsets(bool overlay_cutsets)
{
	this->overlay_cutsets = overlay_cutsets;
}

void Combiner::set_generate_pseudo_data(bool pseudo_data)
{
	generate_pseudo_data = pseudo_data;
}

void Combiner::set_add_histos_stacked(bool add_stacked)
{
	add_histos_stacked = add_stacked;
}

void Combiner::set_logscale(std::map<std::string,bool> *histos)
{
	logscale=histos;
}

void Combiner::set_underflow(std::map<std::string,bool> *histos)
{
	underflow=histos;
}
void Combiner::set_rebin(std::map<std::string,int> *bins)
{
	rebin=bins;
}

void Combiner::set_yaxes(std::map<std::string, std::pair<double,double> > *yaxes)
{
	this->yaxes = yaxes;
}

void Combiner::set_xaxes(std::map<std::string, std::pair<double,double> > *xaxes)
{
	this->xaxes = xaxes;
}

std::string Combiner::getTitle(std::string id)
{

	if(overlay_cutsets){
		if(id.find("01_cutset") != std::string::npos)
			return "s01";
		if(id.find("02_cutset") != std::string::npos)
			return "s02";
		if(id.find("03_cutset") != std::string::npos)
			return "s03";		   
		if(id.find("04_cutset") != std::string::npos)
			return "s04";
		if(id.find("05_cutset") != std::string::npos)
			return "s05";
		if(id.find("06_cutset") != std::string::npos)
			return "s06";
		if(id.find("07_cutset") != std::string::npos)
			return "s07";
		if(id.find("08_cutset") != std::string::npos)
			return "s08";
		else
			return id.c_str();
	}
	if(id.find("Eff_HLT_matched")  != std::string::npos)
		return "HLT Efficiency";
	if(id.find("Eff_L1Seed_matched")  != std::string::npos)
		return "L1 Efficiency";
	if(id.find("Eff_HLTvsL1Seed_matched")  != std::string::npos)
		return "HLT/L1 Efficiency";

	if(id.find("HLTvsL1") != std::string::npos)
		return "HLT/L1 Efficiency";
	

	if(id.find("barrel")  != std::string::npos)
		return "Barrel";
	if(id.find("endcap")  != std::string::npos)
		return "Endcap";
	if(id.find("overlap")  != std::string::npos)
		return "Overlap";

	if(id.find("pt20to30")  != std::string::npos)
		return "#mu p_{T} 20 - 30 [GeV/c]";
	if(id.find("pt30to40")  != std::string::npos)
		return "#mu p_{T} 30 - 40 [GeV/c]";
	if(id.find("pt40")  != std::string::npos)
		return "#mu p_{T} > 40 [GeV/c]";
		

	if(id.find("Data_new") != std::string::npos){
		return "latest 490nb^{-1}  7TeV Data";
	}
	if(id.find("Data_old") != std::string::npos){
		return "first 290nb^{-1}  7TeV Data";
	}
	else if(id.find("Data") != std::string::npos){
		return "Data";
	}
	if(id.find("wjets") != std::string::npos){
		return "W + jets";
	}
	if(id.find("qcdmupt") != std::string::npos && id.find("qcdmuptwj") == std::string::npos ){
		return "QCD";
	}
	if(id.find("MC") != std::string::npos){
		return "Simulation";
	}
	if(id == "Tprime"+mass+"|muon|"+cutset){
		return "t' #bar{t}'(signal, #mu)";
	}
	if(id == "Tprime"+mass+"|mu_background|"+cutset){
		return "t' #bar{t}'(other, #mu)";
	}
	if(id == "TTbar|muon|"+cutset){
		return "t #bar{t}(signal, #mu)";
	}
	//if(id == "TTbar|mu_background|"+cutset){
	//	return "t #bar{t} (other)";
	//}
	if(id == "TTbar|mu_background|"+cutset){
		return "t #bar{t}";
	}
	if(id == "Wjets|mu_background|"+cutset){
		return "W + Jets";
	}
	if(id == "Zjets|mu_background|"+cutset){
		return "Z + Jets";
	}
	if(id == "DY50|mu_background|"+cutset){
		return "Z + Jets";
	}
	if(id == "Mupt15|mu_background|"+cutset){
		return "QCD";
	}
	if(id == "NewQCD|mu_background|"+cutset){
		return "QCD";
	}
	if(id == "QCD|mu_background|"+cutset){
		return "QCD";
	}
	if(id == "MuQCD|mu_background|"+cutset || id == "QCD|e_background|"+cutset){
		return "QCD";
	}
	if(id == "TsChan|mu_background|"+cutset){
		return "t, s-channel";
	}
	if(id == "TtChan|mu_background|"+cutset){
		return "t, t-channel";
	}
	if(id == "TtWChan|mu_background|"+cutset){
		return "t, tW-channel";
	}

	if(id == "Tprime"+mass+"|electron|"+cutset){
		return "t' #bar{t}'(signal, e)";
	}
	if(id == "Tprime"+mass+"|e_background|"+cutset){
		return "t' #bar{t}'(other, e)";
	}
	if(id == "TTbar|electron|"+cutset){
		return "t #bar{t}(signal, e)";
	}
	//if(id == "TTbar|e_background|"+cutset){
	//	return "t #bar{t}(other)";
	//}
	if(id == "TTbar|e_background|"+cutset){
		return "t #bar{t}";
	}
	if(id == "Wjets|e_background|"+cutset){
		return "W + Jets";
	}
	if(id == "Zjets|e_background|"+cutset){
		return "Z + Jets";
	}
	if(id == "DY50|e_background|"+cutset){
		return "Z + Jets";
	}
	if(id == "QCDpt15|e_background|"+cutset){
		return "QCD";
	}
	if(id == "TsChan|e_background|"+cutset){
		return "t, s-channel";
	}
	if(id == "TtChan|e_background|"+cutset){
		return "t, t-channel";
	}
	if(id == "TtWChan|e_background|"+cutset){
		return "t, tW-channel";
	}
	if(id == "QCDem20to30|e_background|"+cutset){
		return "QCD";
	}
	if(id == "QCDem30to80|e_background|"+cutset){
		return "QCD";
	}
	if(id == "QCDem80to170|e_background|"+cutset){
		return "QCD";
	}
	if(id == "QCDbctoe20to30|e_background|"+cutset){
		return "QCD";
	}
	if(id == "QCDbctoe30to80|e_background|"+cutset){
		return "QCD";
	}
	if(id == "QCDbctoe80to170|e_background|"+cutset){
		return "QCD";
	}
	if(id == "Data|muon|"+cutset){
		return "Data";
	}
	if(id == "Data|electron|"+cutset){
		return "Data";
	}
	if(id == "MinBias|mu_background|"+cutset){
		return "MC Minimum Bias";
	}
	if(id == "MinBias|e_background|"+cutset){
		return "MC Minimum Bias";
	}
	return "unknown";
}

std::string Combiner::getTitleX(std::string id)
{
	if(id.find("Pt") != std::string::npos|| id.find("_pt_") != std::string::npos  ){
		return "p_{T} [GeV/c]";
	}
	if(id.find("Eta") != std::string::npos || id.find("eta") != std::string::npos){
		return "#eta";
	}
	if(id.find("D0") != std::string::npos || id.find("d0") != std::string::npos){
		return "d0 [cm]";
	}
	if(id.find("Z0") != std::string::npos){
		return "z0 [cm]";
	}
	if(id.find("Phi") != std::string::npos || id.find("phi") != std::string::npos){
		return "#phi";
	}
	if(id.find("DeltaR") != std::string::npos){
		return "dR Matched Muons";
	}
	if(id.find("DeltaPhi") != std::string::npos){
		return "Delta #phi Matched Muons";
	}
	if(id.find("Charge") != std::string::npos){
		return "Charge";
	}
	if(id.find("JetMultip") != std::string::npos||id.find("jet_number") != std::string::npos ){
		return "Jet Multiplicity";
	}
	if(id.find("nHits") != std::string::npos ){
		return "nHits";
	}
	if(id.find("chi2") != std::string::npos ){
		return "#chi^{2}";
	}
	if(id.find("relIso") != std::string::npos ){
		return "Relative Isolation";
	}
	if(id.find("nStripHits") != std::string::npos ){
		return "Number of Strip Hits";
	}
	if(id.find("nPixelLayers") != std::string::npos ){
		return "Number Pixel Layers with Measurement";
	}
	if(id.find("nStations") != std::string::npos ){
		return "Number of Stations";
	}
	if(id.find("nLostTrackerHits") != std::string::npos ){
		return "Number of Missing Tracker Hits";
	}
	return "";
}

std::string Combiner::getTitleY(std::string id)
{
	if(id.find("Eff_") != std::string::npos){
		return "Efficiency";
	}
	else 
	return "Entries/bin";
	//return "entries[200/pb]";
}

void Combiner::set_cutset(std::string cutset)
{
	this->cutset = cutset;
}
