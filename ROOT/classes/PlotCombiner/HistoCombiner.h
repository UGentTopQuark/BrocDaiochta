#ifndef HISTOCOMBINER_H
#define HISTOCOMBINER_H

#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TRandom3.h"
#include "Combiner.h"
#include <sstream>
#include <vector>
#include <map>
#include <string>
#include <sstream>

class HistoCombiner:public Combiner{
	public:
		HistoCombiner();
		/*
		 *	add_histos() format:
		 *	first parameter: map<filename,vector of postfixes>
		 *	example: files["my_ttbar_root_file.root"].push_back("muon_events");
		 *		 files["my_ttbar_root_file.root"].push_back("electron_events");
		 *		 files["my_Wjets_root_file.root"].push_back("Wjets_events");
		 *	second parameter: names of the histograms with the different parameters
		 *	example: variables.push_back("muon_pt");
		 *		 variables.push_back("jet_pt");
		 *		 ...
		 */

		void combine_histos();
		void write_histos();
		void scale_to_cross_section();
		
	private:
		void calculate_efficiency(std::string datatype, TH1F *counter_histo);
		virtual void add_plot_to_combine(TFile *file,std::string full_name,std::string common_name, std::string identifier);
		std::map<std::string,std::map<std::string,TH1F*> > histos;
		TH1F* get_combined_mc_histo(std::string common_name, std::map<std::string, TH1F*> histos); //std::map<identifier, histo>
		double get_total_data_nevents(std::string common_name, std::map<std::string, TH1F*> histos);
		void add_pseudo_data_to_canvas(std::string common_name, TH1F* MC_histo);
		bool add_histo_to_canvas(std::string common_name, std::string identifier, TH1F* histo, double mc_scaling_factor);
};

#endif
