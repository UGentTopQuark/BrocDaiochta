#ifndef TGRAPHCOMBINER_H
#define TGRAPHCOMBINER_H

#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TRandom3.h"
#include "TGraphAsymmErrors.h"
#include "Combiner.h"
#include <sstream>
#include <vector>
#include <map>
#include <string>

class TGraphCombiner:public Combiner{
	public:
		TGraphCombiner();
		void combine_graphs();
		void write_graphs();
		void scale_to_cross_section(double integrated_lumi);

	private:
		virtual void add_plot_to_combine(TFile *file,std::string full_name,std::string common_name, std::string identifier);
		void calculate_efficiency(std::string datatype, TGraphAsymmErrors *counter_graph);
		void add_graph_to_canvas(std::string common_name, std::string identifier, TGraphAsymmErrors* graph,bool first_graph);

		std::map<std::string,std::map<std::string,TGraphAsymmErrors*> > graphs;

		const static bool verbose = true;
};

#endif
