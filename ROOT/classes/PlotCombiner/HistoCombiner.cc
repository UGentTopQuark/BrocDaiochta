#include "HistoCombiner.h"

HistoCombiner::HistoCombiner()
{
}


//used by add_histos. does the actually adding to the map
//Fill a map [common_histoname][identifier] = TH1F
void HistoCombiner::add_plot_to_combine(TFile *file,std::string full_name,std::string common_name, std::string identifier)
{
	// complete histo name = same name for all histos (eg. njets)
	// 			 + postfix (eg. muon_ttbar-e)
	// new histogram shall be created within scope of outfile
	outfile->cd();
	// read histogram from CMSSW root file and write it to
	// histos[][]
	if(verbose) std::cout << "opening histo: " << full_name << " in " << directory_name << std::endl;
	TH1F *histo = new TH1F(*((TH1F *) file->GetDirectory(directory_name.c_str())->Get(full_name.c_str())));
	if(verbose) std::cout << "opened: " << full_name.c_str() << std::endl;

	// put content of overflow bin into last bin
	if(!combine_eff_plots){
		int nbins = histo->GetNbinsX();
		histo->SetBinContent(nbins, histo->GetBinContent(nbins)+histo->GetBinContent(nbins+1));
		//put content of underflow bin in first bin
		if (underflow && underflow->find(common_name) != underflow->end() && (*underflow)[common_name] &&histo->GetBinContent(0) != 0){
			std::cout << "WARNING: adding underflow to first bin for: " << full_name << std::endl;
			histo->SetBinContent(1,histo->GetBinContent(0)+histo->GetBinContent(1));
		}
		//Remove content of under/overflow
		histo->SetBinContent(nbins+1,0);
		histo->SetBinContent(0,0);
	}

	
	if(!combine_eff_plots && rebin != NULL && rebin->find(common_name) != rebin->end()){			
		const char * hname = histo->GetName();
		histo = (TH1F *) histo->Rebin((*rebin)[common_name],hname);
	}
	
	histos[common_name][identifier]=histo;

	// the efficiency is for all histograms of a dataset the same
	// -> calculate it once when processing the first histo
	//    of a dataset
	//	if(postfix==file_with_content->second.begin())
	if(efficiencies.find(identifier) == efficiencies.end() && do_calculate_efficiency)
		calculate_efficiency(identifier, (TH1F *) file->GetDirectory(directory_name.c_str())->Get((event_counter_histo_name+identifier).c_str()));
}

void HistoCombiner::calculate_efficiency(std::string datatype, TH1F *counter_histo)
{
	// efficiency = (events in histogram (multiplied later)) / (all events) 
	double eff=0;
	if(counter_histo->GetBinContent(1) != 0)
		eff = 1 / counter_histo->GetBinContent(1);
	std::cout << std::setprecision(9);
	std::cout << " N total : " << counter_histo->GetBinContent(1) << " Npassed: " << counter_histo->GetBinContent(2) << " " <<  datatype << std::endl; 
	// save efficiency for dataset centrally
	efficiencies[datatype]=eff;
}

void HistoCombiner::scale_to_cross_section()
{

	for(std::map<std::string, std::map<std::string,TH1F*> >::iterator variable = histos.begin();
	    variable != histos.end();
	    ++variable){

		for(std::map<std::string,TH1F*>::iterator histo = variable->second.begin();
		    histo != variable->second.end();
		    ++histo){
			
			(histo->second)->Sumw2();
			// scale histogram to cross section
			if(histo->first.find("Data") != std::string::npos || histo->first.find("data") != std::string::npos){
				if(verbose) std::cout << " Not scaling datafile: " << histo->first << std::endl;
			}
			else if(cross_sections.find(histo->first) == cross_sections.end())
				std::cerr << "WARNING: cross section not set for: " << histo->first << " ...ignoring... " << std::endl;
			else if(cross_sections[histo->first] == -1)
				std::cerr << histo->first << " has already been scaled...ignoring... " << std::endl;
			else if(luminosity == -1)
				std::cerr << " ERROR: No luminosity set for " << histo->first << ", lumi = " << luminosity << std::endl;
			else{
;				std::cout << "histo_entries " << histo->second->Integral() << std::endl;
				histo->second->Scale(cross_sections[histo->first]*luminosity*efficiencies[histo->first]);
				std::cout << "Scaling Histo " << histo->second->GetName() << " to " << cross_sections[histo->first] *luminosity* efficiencies[histo->first]<< std::endl;
				std::cout << "1/N events before selection " << efficiencies[histo->first] << std::endl;
				std::cout << "luminosity " << luminosity << std::endl;
				std::cout << "cross_section " << cross_sections[histo->first] << std::endl;


				//std::cout << " N passed: " << histo_entries << std::endl;
			}
		}
	}		
}

void HistoCombiner::combine_histos()
{
	// loop over all histograms of the same type (all p_t, all H_T,...) from the different
	// datasets, book for them a CanvasHolder and add them to the CanvasHolder
	//map < histo_name, map < file_name, histo > >
	for(std::map<std::string,std::map<std::string,TH1F*> >::iterator variable = histos.begin();
	    variable != histos.end();
	    ++variable)
	{
		// variable = p_t, H_t, eta...
		cholders[variable->first] = new CanvasHolder();
		cholders[variable->first]->setCanvasTitle(variable->first);
		//cholders[variable->first]->setTitleY(getTitleY(variable->first));
		//cholders[variable->first]->setTitleX(getTitleX(variable->first));
		if(logscale != NULL && logscale->find(variable->first) != logscale->end() && (*logscale)[variable->first])
			cholders[variable->first]->setLogY();
		if(yaxes != NULL && yaxes->find(variable->first) != yaxes->end())
			cholders[variable->first]->setBordersY((*yaxes)[variable->first].first, (*yaxes)[variable->first].second);
		if(xaxes != NULL && xaxes->find(variable->first) != xaxes->end())
			cholders[variable->first]->setBordersX((*xaxes)[variable->first].first, (*xaxes)[variable->first].second);
		
		if(combine_eff_plots)
			cholders[variable->first]->addHLine(1);

		std::cout << "var first: " << variable->first << std::endl;
		TH1F *combined_MC_histo = get_combined_mc_histo(variable->first, variable->second);
		TH1F *data_histo = NULL;
		bool is_data = false;

		double mc_to_data_scaling_factor = 0;
		if(scale_mc_to_data && !combine_eff_plots){
			double data_nevents = 0;
			data_nevents = get_total_data_nevents(variable->first, variable->second);
			if(combined_MC_histo == NULL){
				std::cout <<"ERROR: Combined MC Histo NULL " << std::endl;
				mc_to_data_scaling_factor = 0;
			}
			else
				mc_to_data_scaling_factor = data_nevents/combined_MC_histo->Integral();
		}
		
		//Here we add the histos to the canvas
		if(sequence == NULL){
			std::cout << " ERROR: No Sequence to plot set. If comparing MC to Data this will cause unpredictable scaling. " << std::endl;

			for(std::map<std::string, TH1F*>::iterator histo = variable->second.begin();
			    histo != variable->second.end();
			    ++histo){

				is_data = add_histo_to_canvas(variable->first, histo->first, histo->second, mc_to_data_scaling_factor);
				if(is_data){ data_histo = histo->second; }
			}
		}
		else{
			//This loop adds the histos to the canvas holder in the right order. 
			for(std::vector<std::string>::iterator item = sequence->begin();
				item != sequence->end();
				++item){

				if(variable->second.find(*item) == variable->second.end()){
					std::cout << "ERROR: This item to plot doesn't exist " <<  *item <<std::endl;
					continue;
				}
				if(verbose) std::cout << "adding histo in sequence: " << *item << std::endl;
				
				is_data = add_histo_to_canvas(variable->first, *item, variable->second[*item], mc_to_data_scaling_factor);
				if(is_data){ data_histo = variable->second[*item]; }
			}
		}
		cholders[variable->first]->setOptStat(000000);
//		cholders[variable->first]->setBordersY(0.0, histo->second->GetYaxis()->GetXmax() *15);
		
		if(generate_pseudo_data){
			if(combined_MC_histo == NULL)
				combined_MC_histo = get_combined_mc_histo(variable->first, variable->second);
				
		        add_pseudo_data_to_canvas(variable->first, combined_MC_histo);
		}

//		if(combined_MC_histo != NULL && data_histo != NULL){
//			double ks = combined_MC_histo->KolmogorovTest(data_histo);
//			std::stringstream ss; 
//			ss << ks;
//			std::string description("KS: "+ss.str());
//			cholders[variable->first]->setDescription(description);
//		}
//
		if(combined_MC_histo != NULL)
			delete combined_MC_histo;
		
		if(colours != NULL)
			cholders[variable->first]->setLineColors(*colours);
		cholders[variable->first]->setLineStyles(1);
		//cholders[variable->first]->setLegTitle("Events @ 200/pb");
		//cholders[variable->first]->setLegTitle("Events");
		//cholders[variable->first]->write(outfile);
		cholders[variable->first]->save("png");
	}
}


bool HistoCombiner::add_histo_to_canvas(std::string common_name, std::string identifier, TH1F* histo, double mc_scaling_factor)
{
	bool data_plotted = false;
	if(identifier.find("Data") != std::string::npos ){
		cholders[common_name]->set_eff_histos(true);
		if(combine_eff_plots)
			cholders[common_name]->addHisto(histo, getTitle(identifier), "E0");
		else
			cholders[common_name]->addHisto(histo, getTitle(identifier), "E1");
		data_plotted = true;
	}else{
		if(scale_mc_to_data){
			if(verbose) std::cout << "INFO: scaling MC down to data by factor: " << mc_scaling_factor << std::endl;
			histo->Scale(mc_scaling_factor);
		}
		if(data_plotted)
			std::cout << "ERROR: MC files plotted after Data plotted " << std::endl;

		if(combine_eff_plots){
			cholders[common_name]->set_eff_histos(true);
			cholders[common_name]->addHisto(histo, getTitle(identifier),"E0");
		}
		else if(add_histos_stacked){
			if(verbose) std::cout << "Adding Histo stacked: " << identifier << std::endl;
			if(verbose) std::cout << "Integral: " << histo->Integral() << std::endl;
			cholders[common_name]->setLegendOptions(0.2,0.25);
			cholders[common_name]->set_eff_histos(false);
			cholders[common_name]->addHistoStacked(histo, getTitle(identifier),"HIST");			
		}
		else{
			cholders[common_name]->set_eff_histos(false);
			cholders[common_name]->addHisto(histo, getTitle(identifier),"HISTE1");
		}
	}

	return data_plotted;
}

TH1F* HistoCombiner::get_combined_mc_histo(std::string common_name, std::map<std::string, TH1F*> histos)
{
	TH1F* local_combined_histo = NULL;
	bool first_histo = true;
	for(std::map<std::string, TH1F*>::iterator histo = histos.begin();
	    histo != histos.end();
	    ++histo)
		{
			if(histo->first.find("Data") == std::string::npos){
				if(!first_histo){
					local_combined_histo->Add(histo->second);
				}
				if(first_histo && (histo->second->Integral() != 0 || histo == histos.end())){
					local_combined_histo = new TH1F(*(histo->second));
					first_histo = false;
				}
			}
		}

	return local_combined_histo;
}

double HistoCombiner::get_total_data_nevents(std::string common_name, std::map<std::string, TH1F*> histos)
{
	double local_data_nevents = 0;
	for(std::map<std::string, TH1F*>::iterator histo = histos.begin();
	    histo != histos.end();
	    ++histo){

		if(histo->first.find("Data") != std::string::npos){
			local_data_nevents += histo->second->Integral();
		}
	}
	
	return local_data_nevents;
}

void HistoCombiner::add_pseudo_data_to_canvas(std::string common_name, TH1F* MC_histo)
{
	TH1F* pseudo_data = new TH1F(*MC_histo);
	TRandom3 *rnd_nr_generator = new TRandom3();
	for(int bin = 1; bin < MC_histo->GetNbinsX(); ++bin){
		double random_number = rnd_nr_generator->PoissonD(MC_histo->GetBinContent(bin));
		pseudo_data->SetBinContent(bin, random_number);
	}
	delete rnd_nr_generator;

	cholders[common_name]->addHisto(pseudo_data, "pseudo data", "E1");
	delete pseudo_data;

}

void HistoCombiner::write_histos()
{
	delete outfile;
	outfile=NULL;
}
