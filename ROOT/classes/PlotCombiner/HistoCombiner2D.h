#ifndef HISTOCOMBINER2D_H
#define HISTOCOMBINER2D_H

#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TRandom3.h"
#include "Combiner.h"
#include <sstream>
#include <vector>
#include <map>
#include <string>

class HistoCombiner2D:public Combiner{
	public:
		HistoCombiner2D();

		void combine_histos();
		void write_histos();
		
	private:
		virtual void add_plot_to_combine(TFile *file,std::string full_name,std::string common_name, std::string identifier);
		
		std::map<std::string,TH2F*> histos;
		
		void add_histo_to_canvas(std::string histo_name, TH2F* histo);


		
};

#endif
