#include "HistoCombiner2D.h"

HistoCombiner2D::HistoCombiner2D()
{
}


//used by add_histos. does the actually adding to the map
//Fill a map [common_histoname][identifier] = TH1F
void HistoCombiner2D::add_plot_to_combine(TFile *file,std::string full_name,std::string common_name, std::string identifier)
{
	// complete histo name = same name for all histos (eg. njets)
	// 			 + postfix (eg. muon_ttbar-e)
	// new histogram shall be created within scope of outfile
	outfile->cd();
	// read histogram from CMSSW root file and write it to
	// histos[][]
	std::cout << "opening histo: " << full_name << " in " << directory_name << std::endl;
	TH2F *histo = new TH2F(*((TH2F *) file->GetDirectory(directory_name.c_str())->Get(full_name.c_str())));
	std::cout << "opened: " << full_name.c_str() << std::endl;
	

	histos[common_name+identifier]=histo;
	
}


void HistoCombiner2D::combine_histos()
{
	// loop over all histograms of the same type (all p_t, all H_T,...) from the different
	// datasets, book for them a CanvasHolder and add them to the CanvasHolder
	//map < histo_name, map < file_name, histo > >
	for(std::map<std::string,TH2F*>::iterator variable = histos.begin();
	    variable != histos.end();
	    ++variable)
	{
		if(verbose) std::cout << " Adding " << variable->first << " to canvas " << std::endl;
		// variable = p_t, H_t, eta...
		cholders[variable->first] = new CanvasHolder();
		cholders[variable->first]->setCanvasTitle(variable->first);

		add_histo_to_canvas(variable->first, variable->second);
		
	
		cholders[variable->first]->setOptStat(000000);
//		cholders[variable->first]->setBordersY(0.0, histo->second->GetYaxis()->GetXmax() *15);
	

		//cholders[variable->first]->setLegTitle("Events @ 200/pb");
		//cholders[variable->first]->setLegTitle("Events");
		//cholders[variable->first]->write(outfile);
		cholders[variable->first]->save("eps");
	}
}


void HistoCombiner2D::add_histo_to_canvas(std::string histo_name, TH2F* histo)
{
	//For 2D plots we don't want two things combined on one plot
	//so common name and id are in the hname
	cholders[histo_name]->set_eff_histos(false);
	cholders[histo_name]->addHisto2D(histo,"","COLZ");
}

void HistoCombiner2D::write_histos()
{
	delete outfile;
	outfile=NULL;
}
