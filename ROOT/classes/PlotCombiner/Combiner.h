#ifndef COMBINER_H
#define COMBINER_H

#include "CanvasHolder/Canvas_Holder.h"
#include "TFile.h"
#include <sstream>
#include <vector>
#include <map>
#include <string>

//Parent class for HistoCombiner and TGraphCombiner
class Combiner{
	public:
	        Combiner();
	        ~Combiner();
		void set_luminosity(double integrated_lumi);
		void set_cross_section(std::string process, double cross_section);
		void set_cutset(std::string cutset); 
		void set_sequence_to_plot(std::vector<std::string> *sequence_to_plot);
		void set_mass(std::string mass);
		void set_input_directory(std::string root_dir); //directory in input file containing plots
		void set_scale_mc_to_data(bool do_scale = true);
		void set_eff_plots(bool eff_plots = true); //tell the program you are combining efficiency plots
		void set_overlay_cutsets(bool overlay_cutsets = true); //tell program you're overlaying plots from different cutsets within one file
		void set_generate_pseudo_data(bool pseudo_data = true);
		void set_add_histos_stacked(bool add_stacked = true); //true by default.for Mc histos
		void set_calculate_efficiency(bool calc_eff = true);//If you dont need scaling turn off eff calculation


		void add_plots(std::map<std::string,std::vector<std::string> > &files_with_content, std::vector<std::string> &plot_names);
		void add_vplots(std::vector<std::string> files_with_content, std::vector<std::string> plot_names);
		void add_plots_by_cutset(std::string file_with_content, std::vector<std::string> cutsets,std::vector<std::string> plot_names);
		void add_plots_by_predot(std::string file_with_content, std::vector<std::string> predots,std::vector<std::string> plot_names);
		void add_plots_by_region(std::string file_with_content, std::vector<std::string> regions,std::vector<std::string> plot_names);

		//CanvasHolder display options
		void set_colours(std::vector<int> *colour_scheme);
		void set_logscale(std::map<std::string, bool> *histos);
		void set_underflow(std::map<std::string, bool> *histos);
		void set_rebin(std::map<std::string, int> *bins);
		void set_yaxes(std::map<std::string, std::pair<double,double> > *yaxes);
		void set_xaxes(std::map<std::string, std::pair<double,double> > *xaxes);

	protected:

		std::string getTitleX(std::string id);
		std::string getTitleY(std::string id);
		std::string getTitle(std::string id);

		std::string mass;
		std::string event_counter_histo_name;
		std::string cutset;
		std::string directory_name;

		bool scale_mc_to_data;
		bool combine_eff_plots;
		bool overlay_cutsets;
		bool auto_fill_sequence;
		bool generate_pseudo_data;	
		bool add_histos_stacked;
		bool do_calculate_efficiency;

		// postfix(process)->cross_section
		std::map<std::string, double> cross_sections;
		std::map<std::string,double> efficiencies;
		std::vector<std::string> *sequence;
	     
		std::map<std::string,CanvasHolder*> cholders;
		std::map<std::string, bool> *logscale;
		std::map<std::string, int> *rebin;
		std::map<std::string, std::pair<double,double> > *yaxes;
		std::map<std::string, std::pair<double,double> > *xaxes;
		std::vector<int> *colours;		
		std::map<std::string, bool> *underflow;

		virtual void add_plot_to_combine(TFile *file,std::string full_name,std::string common_name, std::string identifier) = 0;

		double luminosity;

		TFile *outfile;

		const static bool verbose = true;
};

#endif
