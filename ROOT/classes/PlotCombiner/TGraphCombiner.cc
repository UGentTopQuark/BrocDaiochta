#include "TGraphCombiner.h"

TGraphCombiner::TGraphCombiner()
{
}


//used by add_graphs. does the actually adding to the map
void TGraphCombiner::add_plot_to_combine(TFile *file,std::string full_name,std::string common_name, std::string identifier)
{
	outfile->cd();

	std::cout << "opening graph: " << full_name << std::endl;
	 TGraphAsymmErrors*graph = new TGraphAsymmErrors(*((TGraphAsymmErrors *) file->GetDirectory("eventselection")->Get(full_name.c_str())));
	std::cout << "opened: " << full_name.c_str() << std::endl;

	graphs[common_name][identifier]=graph;	
	
	
}


void TGraphCombiner::combine_graphs()
{

	// loop over all graphgrams of the same type (all p_t, all H_T,...) from the different
	// datasets, book for them a CanvasHolder and add them to the CanvasHolder
	for(std::map<std::string,std::map<std::string,TGraphAsymmErrors*> >::iterator variable = graphs.begin();
	    variable != graphs.end();
	    ++variable)
	{
		
	 	// variable = p_t, H_t, eta...
 		cholders[variable->first] = new CanvasHolder();
 		cholders[variable->first]->setCanvasTitle(variable->first);
		cholders[variable->first]->setTitleY("Efficiency");
		cholders[variable->first]->setTitleX(getTitleX(variable->first));
		cholders[variable->first]->setBordersY(0.6, 1.0);	
		if(yaxes != NULL && yaxes->find(variable->first) != yaxes->end()){
 			if (verbose) std::cout << " Setting y axis for " <<variable->first <<": "  << (*yaxes)[variable->first].first<< " to " <<  (*yaxes)[variable->first].second<< std::endl; 
 			cholders[variable->first]->setBordersY((*yaxes)[variable->first].first, (*yaxes)[variable->first].second);
 		}
 		if(xaxes != NULL && xaxes->find(variable->first) != xaxes->end()){
 			if (verbose) std::cout << " Setting x axis for " <<variable->first <<": "  << (*xaxes)[variable->first].first<< " to " <<  (*xaxes)[variable->first].second<< std::endl; 
 			cholders[variable->first]->setBordersX((*xaxes)[variable->first].first, (*xaxes)[variable->first].second);
 		}
 		if(combine_eff_plots)
 			cholders[variable->first]->addHLine(1,"",2);
		
		bool first_graph = true;
		if(sequence == NULL){
			std::cout << " ERROR: No Sequence to plot set. If comparing MC to Data this will cause unpredictable scaling. " << std::endl;
			for(std::map<std::string,TGraphAsymmErrors*>::iterator graph = variable->second.begin();
			    graph != variable->second.end();
			    ++graph){

				add_graph_to_canvas(variable->first, graph->first,graph->second, first_graph);
				first_graph = false;
			}
		}else{
			for(std::vector<std::string>::iterator item = sequence->begin();
			    item != sequence->end();
			    ++item){
 				if(variable->second.find(*item) == variable->second.end()){
					std::cout << "ERROR: This item to plot doesn't exist: " << *item <<std::endl;
					continue;
				}
				
				add_graph_to_canvas(variable->first,*item, variable->second[*item], first_graph);
				first_graph = false;
								
			}
		}

		cholders[variable->first]->setOptStat(000000); 					
		
 		if(colours != NULL){
 			cholders[variable->first]->setLineColorsGraph(*colours);
 			cholders[variable->first]->setMarkerColorsGraph(*colours);
		}
		std::vector<int> styles;
		styles.push_back(1);
		styles.push_back(2);
		
		cholders[variable->first]->setLineStylesGraph(styles);
		cholders[variable->first]->setMarkerSizeGraph(1.2);
		cholders[variable->first]->setLineSizeGraph(2.5);
		cholders[variable->first]->setLegendOptions(0.40, 0.14, "LowerRight");
 		//cholders[variable->first]->setLegTitle("Events @ 200/pb");
		// 		//	cholders[variable->first]->setLegTitle("Events");
 		cholders[variable->first]->write(outfile);
		cholders[variable->first]->save("eps");
	}		
}

void TGraphCombiner::add_graph_to_canvas(std::string common_name, std::string identifier, TGraphAsymmErrors* graph, bool first_graph)
{

	cholders[common_name]->set_eff_histos(true);
	if(first_graph)
		cholders[common_name]->addGraph(graph,common_name, getTitle(identifier), "APz");
	else 	
		//NOTE: setting all draw options on second graph will remove first graph
		cholders[common_name]->addGraph(graph,common_name, getTitle(identifier), "Pz"); 
       				
}

void TGraphCombiner::write_graphs()
{
	delete outfile;
	outfile=NULL;
}



//  				if(item == sequence->begin())
//  					cholders[variable->first]->setTitleX(variable->second[*item]->GetXaxis()->GetTitle());
// 				std::cout << "Xaxis: " << variable->second[*item]->GetXaxis()->GetTitle() << std::endl;
// 				std::cout << "Yaxis: " << variable->second[*item]->GetYaxis()->GetTitle() << std::endl;
// 				std::cout << "Yaxis: " << variable->first << std::endl;

// 				TCanvas * c1 =new TCanvas("c1","TEST");	
// 				c1->SetFillColor(0);
// 				c1->SetBorderMode(0);
// 				c1->SetBorderSize(2);
// 				c1->SetTickx();
// 				c1->SetTicky();

// 				c1->SetFrameFillStyle(0);
// 				c1->SetFrameBorderMode(0);
// 				c1->SetFrameFillStyle(0);
// 				c1->SetFrameBorderMode(0);
// 				c1->cd();
// 				variable->second[*item]->Draw("APz");
// 				c1->SaveAs((variable->first+".eps").c_str());
// 				c1->Write();	
