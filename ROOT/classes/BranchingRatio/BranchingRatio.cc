#include "BranchingRatio.h"

BranchingRatio::BranchingRatio()
{
	calculated = false;
	br_muon = -1;
	br_electron = -1;
	file_locally_opened = false;
	file = NULL;

	cs_prov = new CrossSectionProvider();
}

BranchingRatio::~BranchingRatio()
{
	delete cs_prov;
	cs_prov = NULL;
}

void BranchingRatio::set_file(TFile *file)
{
	this->file = file;	
	calculated = false;
}

void BranchingRatio::set_file(std::string file_name)
{
	TFile *file = new TFile(file_name.c_str(), "OPEN");
	file_locally_opened = true;
	this->file = file;	
	calculated = false;
}

double BranchingRatio::get_branching_ratio_muon(std::string dataset)
{
	if(!calculated)
		calculate_br(dataset);

	return br_muon;
}

double BranchingRatio::get_branching_ratio_electron(std::string dataset)
{
	if(!calculated)
		calculate_br(dataset);

	return br_electron;
}

void BranchingRatio::calculate_br(std::string dataset)
{
	if(verbose) std::cout << "BranchingRatio::calculate_br(): calculating branching ratio for " << dataset << std::endl;
	//20120920 everything related to reading event_counter histo is outdated
	std::string directory_name = "eventselection";
	std::string event_counter_histo = "event_counter";
	TH1F *evt_counter= new TH1F(*((TH1F*) file->GetDirectory(directory_name.c_str())->Get(event_counter_histo.c_str())));
	
//	double ne, nmu, noth;	//	bins 1,2,3: electron, muon, other
//	nmu = evt_counter->GetBinContent(2);
//	ne = evt_counter->GetBinContent(3);
//	noth = evt_counter->GetBinContent(4);
//	
//	double ne_background = (nmu+noth)/cs_prov->get_filter_efficiency(dataset+"_e_background");
//	double nmu_background = (ne+noth)/cs_prov->get_filter_efficiency(dataset+"_mu_background");
//	
//	nmu /= cs_prov->get_filter_efficiency("TTbar_muon");
//	ne /= cs_prov->get_filter_efficiency("TTbar_electron");
//	
//	br_muon = nmu/(nmu+nmu_background);
//	br_electron = ne/(ne+ne_background);
//
	//Check if ttbar split into signal and background
	//BR is 1 if ttbar not split into muon/electron and mu/e_background
	if(evt_counter->GetBinContent(4) != 0){
		br_muon = 0.146864425858859;
		br_electron = br_muon;
	}else{
		br_muon = 1.;
		br_electron = br_muon;
	}
		


	delete evt_counter;
	evt_counter = NULL;

	if(file_locally_opened) file->Close();

	if(verbose) std::cout << "BranchingRatio::calculate_br(): calculated branching ratios for " << dataset << std::endl;
	if(verbose) std::cout << "BranchingRatio::calculate_br(): branching ratio muon: " << br_muon << std::endl;
	if(verbose) std::cout << "BranchingRatio::calculate_br(): branching ratio electron: " << br_electron << std::endl;

	calculated = true;
}

bool BranchingRatio::signal_file_set()
{
	if(file)
		return true;
	else
		return false;
}

void BranchingRatio::set_analysis(std::string analysis)
{
	cs_prov->set_analysis(analysis);	
}
