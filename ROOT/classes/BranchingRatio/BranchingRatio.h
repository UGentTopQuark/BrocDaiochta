#ifndef BRANCHINGRATIO_H
#define BRANCHINGRATIO_H

#include "CrossSectionProvider/CrossSectionProvider.h"
#include "TFile.h"
#include "TH1F.h"

class BranchingRatio{
	public:
		BranchingRatio();
		~BranchingRatio();
		void set_file(TFile *file);
		void set_file(std::string file_name);
		double get_branching_ratio_muon(std::string dataset="TTbar");
		double get_branching_ratio_electron(std::string dataset="TTbar");
		bool signal_file_set();
		void set_analysis(std::string analysis);
	private:
		void calculate_br(std::string dataset="TTbar");
		CrossSectionProvider *cs_prov;

		bool calculated;
		double br_electron, br_muon;	// store branching ratios for muons and electrons

		bool file_locally_opened;

		static const bool verbose = true;

		TFile *file;
};

#endif
