#ifndef MERGE_H
#define MERGE_H

#include <sstream>
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include "CrossSectionProvider/CrossSectionProvider.h"
#include "BranchingRatio/BranchingRatio.h"
#include "HistoNameParser/HistoNameParser.h"
#include "TCanvas.h"//tmp include for testing

class Merge{
	public:
		Merge();
		~Merge();
		/*************************************************************
                 MergeHistos does one of two things:
                 1/ Using combine_histo  you can make it merge 
                 multiple histos from different files and return the resultant merged histo.
                 2/ Using combine_all_histos you can make it combine histos of the
                 same name in multiple files and output the merges histos to
                 one file. Used in this way you can also make efficiency plots.
		*************************************************************/

		void set_outfile(TFile *outfile);
		void set_file_names(std::map<std::string,std::vector<std::string> > &file_names);
		void set_rebin(int rebin, int rebiny = -1);
		void set_rebin(std::map<std::string, int> *bins);
		void set_xaxes(std::map<std::string, std::pair<double,double> > *xaxes);
		void set_xaxis(std::string histo_name,double xmin, double xmax);
		void set_cutset(std::string cutset);
		void set_alternative_cutset(std::string dataset,std::string cutset);
		void set_signal_file(std::string signal_file_name);
		void set_histo_name(std::string histo_name);
		void set_integrated_luminosity(double integrated_luminosity);
		void set_analysis(std::string analysis); //This determines which cross sections etc will be used
		void do_not_scale(std::string dataset);
		void reset_do_not_scale();
		void set_additional_scale_factor(std::string dataset,std::string channel, double nevents);
		void reset_scale_by_additional_scale_factor();
		void set_input_directory(std::string dir_name);
		void set_input_file_directory(std::string dir_name);//< Input dir containing root file.

		//Functions needed specifically for when mergehistos is
		//combining multiple histos and outputting to a file
		void set_eff_histos(bool eff_histos); //<Save combined histos to make eff graphs later
		void set_output_directory(std::string dir_name);
		void set_vfile_names(std::vector<std::string>  &vfile_names);
		void set_vhisto_names(std::vector<std::string>  &vhisto_names);
		///these needed if histos with different ids are being combined
		void set_dataset_ids(std::map<std::string,std::string>  d_ids);
		void set_cutsets(std::vector<std::string>  cutsets);
		///replace a string in the histo name with another string
		void set_replace_name_string(std::vector<std::pair<std::string,std::string> > string_replace);
		///All. histos will have trigger name added to them
		void set_trigger_to_study(std::string trigger);
		
		

		//0 = flat_selection = default. 1 = DQMOffline output root files
		void set_origin_of_input(int origin_of_input); 
		//again for DQMOffline. mid section of directory is run number
		void set_out_dir_start_end(std::string root_start, std::string root_end);

	protected:
		double get_cross_section(std::string dataset, std::string event_type);
		std::map<TFile*,std::string> open_all_files();

		void check_branching_ratio_provider();
		std::string get_DQM_directory_name(std::string filename, int nfiles);
	
		TFile *outfile;
		int rebin_one;
		int rebin_one_y;
		int origin_of_input;

		std::map<std::string,int> *vrebin;
		std::map<std::string, std::pair<double,double> > *xaxes;
		std::map<std::string, std::pair<double,double> > *internal_xaxes;//<for when xaxes map is created internally
		std::map<std::string,std::vector<std::string> > file_names;
		std::map<std::string,double> efficiencies;
		std::map<std::string,std::string> dataset_ids;

		std::vector<std::string> list_do_not_scale;	// histograms that shall be excluded from scaling with cross section
		std::map<std::string, std::map<std::string,double> > scale_by_additional_scale_factor;	
		std::vector<std::string> vfile_names;
		std::vector<std::string> vhisto_names;
		std::vector<std::string> cutsets;

		std::vector<std::pair<std::string,std::string> > replace_name_string;

		std::string mass;
		std::string cutset;
		std::string histo_name;
		std::string root_start;
		std::string root_end;
		std::string input_directory_name;
		std::string input_file_directory_name;
		std::string output_directory_name;
		std::string event_counter_histo_name;
		std::string trigger_to_study;

		bool combine_eff_histos;
		
		CrossSectionProvider *cs_prov;
		BranchingRatio *br_ratio;
		HistoNameParser *name_parser;

		double integrated_luminosity;

		static const bool verbose = false;
};
#endif
