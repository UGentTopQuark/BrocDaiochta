#include "Merge.h"

Merge::Merge()
{
	integrated_luminosity = -1;
	rebin_one = -1;
	rebin_one_y = -1;
	cs_prov = new CrossSectionProvider();
	br_ratio = new BranchingRatio();
	histo_name = "";
	trigger_to_study = "";
	combine_eff_histos = false;
	origin_of_input = 0; //assumes we're using flat_sel by default
	root_start = "-1";
	root_end = "-1";	
	input_directory_name = output_directory_name ="eventselection";
	input_file_directory_name = ""; 
	xaxes=NULL;
	internal_xaxes=NULL;

	name_parser = new HistoNameParser();
}

Merge::~Merge()
{
	delete name_parser;
	name_parser = NULL;
	delete cs_prov;
	cs_prov = NULL;
	delete br_ratio;
	br_ratio = NULL;
	if(internal_xaxes){
		delete internal_xaxes;
		internal_xaxes = 0;
	}
}


void Merge::set_outfile(TFile *outfile)
{
	this->outfile = outfile;
}


void Merge::set_output_directory(std::string dir_name)
{
	output_directory_name = dir_name;
}

void Merge::set_input_directory(std::string dir_name)
{
	input_directory_name = dir_name;
}

void Merge::set_input_file_directory(std::string dir_name)
{
	input_file_directory_name = dir_name;
}

void Merge::set_trigger_to_study(std::string trigger)
{
	trigger_to_study = trigger;
}


void Merge::set_eff_histos(bool eff_histos)
{
	combine_eff_histos = eff_histos;
}

void Merge::set_integrated_luminosity(double integrated_luminosity)
{
	this->integrated_luminosity = integrated_luminosity;
}

void Merge::set_file_names(std::map<std::string,std::vector<std::string> > &file_names)
{
	this->file_names = file_names;
}

void Merge::set_vfile_names(std::vector<std::string>  &vfile_names)
{
	this->vfile_names = vfile_names;
}

void Merge::set_signal_file(std::string signal_file_name)
{
	if(input_file_directory_name != "")
		br_ratio->set_file(input_file_directory_name+"/"+signal_file_name);
	else
		br_ratio->set_file(signal_file_name);
}

void Merge::set_cutset(std::string cutset)
{
	this->cutset = cutset;
}
void Merge::set_rebin(int rebin_one, int rebin_one_y)
{
	this->rebin_one = rebin_one;
	this->rebin_one_y = rebin_one_y;
}

void Merge::set_rebin(std::map<std::string,int> *vrebin)
{
	this->vrebin = vrebin;
}

void Merge::set_xaxes(std::map<std::string, std::pair<double,double> > *xaxes)
{
	this->xaxes = xaxes;
}

void Merge::set_xaxis(std::string histo_name,double xmin,double xmax)
{
	if(xmin == -1 && xmax == -1 && 
	   xaxes != NULL &&  xaxes->find(histo_name) != xaxes->end()){
		xaxes->erase(histo_name);
		return;
	}else if(xmin == -1 && xmax == -1){
		return;
	}

	if(xaxes == NULL){
		internal_xaxes = new std::map<std::string,std::pair<double, double> >();
		this->xaxes = internal_xaxes;
	}
	(*xaxes)[histo_name] = std::pair<double,double>(xmin,xmax);

}

void Merge::set_histo_name(std::string histo_name)
{
	this->histo_name = histo_name;
}

void Merge::set_vhisto_names(std::vector<std::string>  &vhisto_names)
{
	this->vhisto_names = vhisto_names;
}
void Merge::set_cutsets(std::vector<std::string>  cutsets)
{
	this->cutsets = cutsets;
}
void Merge::set_dataset_ids(std::map<std::string,std::string>  d_ids)
{
	dataset_ids = d_ids;
}

void Merge::do_not_scale(std::string dataset)
{
	list_do_not_scale.push_back(dataset);
}

void Merge::reset_do_not_scale()
{
	list_do_not_scale.clear();
}
void Merge::set_additional_scale_factor(std::string dataset,std::string channel, double nevents)
{
	scale_by_additional_scale_factor[dataset][channel] = nevents;
}

void Merge::reset_scale_by_additional_scale_factor()
{
	scale_by_additional_scale_factor.clear();
}

void Merge::set_analysis(std::string analysis)
{
	cs_prov->set_analysis(analysis);
	br_ratio->set_analysis(analysis);
}

void Merge::set_origin_of_input(int origin_of_input)
{
	this->origin_of_input = origin_of_input;
	
}

void Merge::set_out_dir_start_end(std::string root_start, std::string root_end)
{
	//These only used for origin of input == 1
	this->root_start = root_start;
	this->root_end = root_end;
}

void Merge::set_replace_name_string(std::vector<std::pair<std::string,std::string> > string_replace)
{
	replace_name_string = string_replace;
}

std::map<TFile*,std::string> Merge::open_all_files()
{
	if(vfile_names.size() == 0)
		std::cout << " ERROR: No vector of filenames given " << std::endl;
	
	std::map<TFile*,std::string> files;

	// open all files
	for(std::vector<std::string>::iterator file_name = vfile_names.begin();
	    file_name != vfile_names.end();
	    ++file_name){
		std::string file_path = *file_name;
		if(input_file_directory_name != "")
			file_path = input_file_directory_name+"/"+*file_name;
		if(verbose) std::cout << "opening file: " << file_path << std::endl;
		TFile *file = new TFile((file_path).c_str(),"open");
		files[file] = *file_name;

	}
	return files;
}


void Merge::check_branching_ratio_provider()
{
	if(br_ratio->signal_file_set())
		return;
	else{
		std::cout << "INFO: Merge::check_branching_ratio_provider(): signal file not set in BranchingRatio()... ";

		bool found_possible_signal = false;
		for(std::map<std::string,std::vector<std::string> >::iterator file_name = file_names.begin();
			file_name != file_names.end();
			++file_name){
			if(file_name->first.find("ttbar") != std::string::npos){
				std::cerr << "assuming: " << file_name->first << std::endl;
				br_ratio->set_file(file_name->first);
				found_possible_signal = true;
			}
		}
		if(!found_possible_signal){
			std::cout << std::endl;
			std::cerr << "WARNING: Merge(): ASSUMING TTbar signal BRANCHING RATIO OF 0." << std::endl;
		}
	}
}

double Merge::get_cross_section(std::string dataset, std::string event_type)
{
	double cross_section = -1;
	if(dataset.find("TTbar") != std::string::npos){
		// this is a signal dataset
		// full ttbar cross section
		cross_section = cs_prov->get_cross_section(dataset);

		// now we need the branching ratio
		check_branching_ratio_provider();

        	double e_BR = 0.;
        	double mu_BR = 0.;
		if(br_ratio->signal_file_set()){
        		e_BR = br_ratio->get_branching_ratio_electron();
        		mu_BR = br_ratio->get_branching_ratio_muon();
		}else{
			std::cerr << "WARNING: Merge::set_default_cross_section(): no ttbar signal branching ratio set" << std::endl;
		}
		
		// assign correct branching ratio for event type
		double BR = 0.;
		if(event_type == "electron") BR = e_BR;
		else if(event_type == "e_background") BR = 1.-e_BR;
		else if(event_type == "muon") BR = mu_BR;
		else if(event_type == "mu_background") BR = 1.-mu_BR;
		else std::cerr << "ERROR: Merge::get_cross_section(): unknown event_type: " << event_type << std::endl;

		double filter_efficiency = cs_prov->get_filter_efficiency(dataset+"_"+event_type);

		if(verbose) std::cout << "dataset: " << dataset << " event_type: " << event_type << " branching ratio: " << BR << " cross section " << cross_section << " filter_efficiency: " << filter_efficiency << " BR*CS: " << BR*cross_section << std::endl;

		// cross section only for *signal* events (not ttbar other)
		cross_section *= BR;

		// finally get filter efficiency
		cross_section *= filter_efficiency;
	}else{
	// if dataset is no signal dataset we don't have to bother with the branching ratio
		cross_section = cs_prov->get_cross_section(dataset);	
		// for background samples there is no difference between
		// x_background and x, with x = muon/electron... always
		// background
		cross_section *= cs_prov->get_filter_efficiency(dataset);	
	}

	return cross_section;
}

std::string Merge::get_DQM_directory_name(std::string filename,int nfiles)
{
	std::string dir_name;
	//Get Run number from file named
	std::string root_middle;
	std::string run_number;
	if(filename.find("DQM_V0001_R000000001") == std::string::npos && filename.find("MC") == std::string::npos){
		std::string start_filename = filename.substr(0,filename.find("__DQMTest__Top__Histos.root"));
		run_number = (start_filename).substr(14,start_filename.size());
		
		root_middle = "Run "+run_number; 
	}
	else if((filename.find("DQM_V0001_R000000001") != std::string::npos && nfiles < 2) || filename.find("MC") != std::string::npos){
		root_middle = "Run 1"; 
	}
	else{
		std::cout<< "ERROR: MC file mixed in with data files" << filename << std::endl;
		return filename;
	}
	dir_name = root_start+root_middle+root_end+output_directory_name;
	return dir_name;
}

