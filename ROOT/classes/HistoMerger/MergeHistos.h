#ifndef MERGEHISTOS_H
#define MERGEHISTOS_H

#include "Merge.h"

class MergeHistos:public Merge{
	public:
		MergeHistos();
		~MergeHistos();
		template <class HISTO>
			HISTO* combine_histo(bool is_data = false,bool create_new = false);
		template <class HISTO>
		void combine_all_histos(bool is_data = false,bool create_new = false);
		template <class HISTO>
			std::vector<HISTO*> get_histos_for_eff(std::string which_histos);

	private:
		template <class HISTO>
			void scale_histo(HISTO *histo, std::string histo_name, std::string directory_name, TFile *file, std::string file_name);
		template <typename HISTO>
		void save_combined_histo(std::string hname,HISTO* histo);
		template <class HISTO>
			void rebin_histo(HISTO* h);

		///These are vectors which will be sent to Efficiency.cc to create efficiency plots. 
		std::vector<TH1D*> combined_denom_histos; //<eff = num/denom
		std::vector<TH1D*> combined_num_histos;
		
	
		template <typename HISTO>
		HISTO* get_clean_combined_histo(HISTO* combined_histo);	
};
#endif
