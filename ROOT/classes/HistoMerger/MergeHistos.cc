#include "MergeHistos.h"

template TH1D* MergeHistos::combine_histo<TH1D>(bool is_data = false,bool create_new = false);
template TH2D* MergeHistos::combine_histo<TH2D>(bool is_data = false,bool create_new = false);

template void MergeHistos::combine_all_histos<TH1D>(bool is_data = false,bool create_new = false);
template void MergeHistos::combine_all_histos<TH2D>(bool is_data = false,bool create_new = false);

template void MergeHistos::scale_histo<TH1D>(TH1D *histo, std::string histo_name, std::string directory_name, TFile *file, std::string file_name);
template void MergeHistos::scale_histo<TH2D>(TH2D *histo, std::string histo_name, std::string directory_name, TFile *file, std::string file_name);

MergeHistos::MergeHistos()
{
}
MergeHistos::~MergeHistos()
{	
}

template <class HISTO> 
void MergeHistos::combine_all_histos(bool is_data,bool create_new)
{
	if(verbose && is_data) std::cout << " Is_data set = True. No scaling" << std::endl;
	else if(verbose && !is_data) std::cout << " Is_data set = False. To be scaled if possible." << std::endl;
	if(verbose) std::cout << "Combining histo " << histo_name << " cutset " << cutset << std::endl;
	if(integrated_luminosity == -1 && !is_data)
		std::cout << " WARNING: Integrated luminosity not set " <<std::endl;

	outfile->cd();
	
	std::map<TFile*,std::string> files = open_all_files();

	if(verbose) std::cout << "Opened all files." << std::endl;

	combined_denom_histos.clear();//Used in data_run_combiner for eff.cc
	combined_num_histos.clear();

	std::vector<std::string> channels;
	//channels.push_back("muon");
	//channels.push_back("mu_background");
	channels.push_back("e_background");

	for(std::vector<std::string>::iterator channel = channels.begin();
	    channel != channels.end(); channel++){
  	
		for(std::vector<std::string>::iterator histo_name = vhisto_names.begin();
		    histo_name != vhisto_names.end();
		    ++histo_name){
			
			if(cutsets.size() == 0)
				cutsets.push_back("one_iter");
			
			for(std::vector<std::string>::iterator cutset = cutsets.begin();
			    cutset != cutsets.end();
			    ++cutset){
				
				HISTO *combined_histo=NULL;
				bool first = true;	// first dataset to be processed	
				for(std::map<TFile*, std::string>::iterator file = files.begin();
				    file != files.end();
				    ++file){
				
					//This is for combining e_qcd background since ids are different
					std::string full_histo_name;
					if(dataset_ids.size() != 0 && cutsets[0] != "one_iter"){
						full_histo_name = (*histo_name + "_" + dataset_ids[file->second]+"|"+*channel+"|"+*cutset);
						if(verbose) std::cout << "Reconstructing histo name: " << full_histo_name<< std::endl;
					}
					else
						full_histo_name = *histo_name;
					
					outfile->cd();
					HISTO *histo=NULL;	// histogram from current file
					
					if(origin_of_input == 1) input_directory_name = get_DQM_directory_name(file->second,files.size());
					if(verbose) std::cout << "opening " << input_directory_name << "/" << full_histo_name.c_str() <<std::endl;
					// create a copy of the first histo:
					// to this histogram later on the histograms yet to
					// come are added and it is then afterwards returned as
					// output of this function
					if(first){
						if(verbose) std::cout << "Creating new Th?F for " << full_histo_name.c_str() << std::endl;
						histo = new HISTO(*((HISTO *) file->first->GetDirectory(input_directory_name.c_str())->Get(full_histo_name.c_str())));
					}else{	// if this is not the first histogram we don't
						// make a copy but instead just scale it in memory and add
						// it later to the first histo
						histo = (HISTO *) file->first->GetDirectory(input_directory_name.c_str())->Get(full_histo_name.c_str());
					}
					if(verbose) std::cout << "opened "<< input_directory_name << "/" << full_histo_name.c_str() <<std::endl; 
					
					// put content of overflow bin into last bin
					//If we want to do this should do at display stage. Otherwise risk filling twice
					//Relevant code deleted. Comment kept to avoid reimplementation.
					
					histo->Sumw2();
					
					// ignore empty histograms
					if(file_names.size() > 1 && histo->Integral(0,histo->GetNbinsX()+1) == 0){
						if(verbose) std::cout << full_histo_name <<" was empty, skipping.." <<std::endl;
						continue;
					}
					else if(file_names.size() == 1 && histo->Integral(0,histo->GetNbinsX()+1) == 0){
						//Don't skip empty files if there's only one file. this will return a NULL histo and cause problems
						if(verbose) std::cout << full_histo_name <<" was empty, NOT skipping.." <<std::endl;
					}
					
					// scale histogram to cross section
					if(histo != NULL && histo->Integral(0,histo->GetNbinsX()+1) != 0 && !is_data){
						scale_histo<HISTO>(histo, full_histo_name, input_directory_name, file->first,file->second);
						if(verbose && !is_data) std::cout << " Histos scaled successfully " <<std::endl;
					}
					else if(is_data){
						if(verbose) std::cout << " Skipped scaling for data hist " << file->second << std::endl;
					}
					
					// if first histo that is processed, make this histo the base for the combined histo
					// else just add it to combined histo
					if(verbose) std::cout << "Adding Histo to combined_histo "  << std::endl;
					if(first){
						combined_histo = histo;	
						first = false;
						
						//Replace a string in the file name with another
						std::string name = histo->GetName();
						if(verbose) std::cout << "Name: " << name <<std::endl;
						if(!replace_name_string.empty()){
							for(std::vector<std::pair<std::string,std::string> >::iterator replace = replace_name_string.begin(); replace != replace_name_string.end();replace++){
								if(verbose) std::cout <<"Replacing NAME: " << name << std::endl;
								size_t start_pos = name.rfind(replace->first);
								if(start_pos != std::string::npos)
									name.replace(start_pos,replace->first.size(),replace->second);
								if(verbose) std::cout <<"With NAME: " << name << std::endl;
								
							}
						}
						///For later use in Efficiency.cc. Used in data_run_combiner
						if( combine_eff_histos && trigger_to_study != "" &&
						    name.find("All.HLT") == std::string::npos && name.find("All.") != std::string::npos){
							name.erase(0,4);
							if(verbose) std::cout << "All. removed. Now name = " << name << std::endl; 
							name = "All."+trigger_to_study+"_"+name;
							if(verbose) std::cout << "All.trigger added. Now name = " << name << std::endl; 
						}
						else if(combine_eff_histos && trigger_to_study == "")
							std::cout << "***ERROR***: trigger name not set, needed for reco eff plots " << std::endl;
						
						combined_histo->SetName(name.c_str());
					}else{
						combined_histo->Add(histo);
					}
					if(verbose) std::cout << " Histo added " <<std::endl;
					
				}			
	       			
				if(combined_histo == NULL) std::cout << "***ERROR*** returning NULL histo in MergeHistos " << std::endl;
		
				if(xaxes != NULL && xaxes->find(*histo_name) != xaxes->end()){
					if(verbose) std::cout << "Setting x-axis range for " << *histo_name << std::endl;
					combined_histo->SetAxisRange((*xaxes)[*histo_name].first, (*xaxes)[*histo_name].second,"X");
				}
				//Save histos to be used by Efficiency.cc
				if(combine_eff_histos){
					save_combined_histo(*histo_name, combined_histo);
				}

				//If create_new = true. Output a combined histo which will not display error bars for original MC stats.
				//Note: not set up for 2D histograms
				HISTO *clean_combined_histo=NULL;
				if(create_new){
					clean_combined_histo = get_clean_combined_histo(combined_histo);				
				}
				
				if (verbose) std::cout << " cd to outfile " << std::endl;
				outfile->cd();
				if (verbose) std::cout << " Directory in outfile " << output_directory_name << std::endl;
				outfile->Cd(output_directory_name.c_str());
				if(!create_new)
					combined_histo->Write();
				else
					clean_combined_histo->Write();
				
				if(!combine_eff_histos){
					delete combined_histo;
					combined_histo = NULL;
				}
				if(create_new){
					delete clean_combined_histo;
					clean_combined_histo = NULL;
				}
			}
		}
	}

	// close input files
	for(std::map<TFile*,std::string>::iterator file = files.begin();
	    file != files.end();
	    ++file){
		delete (file->first);
	}
}

//Not implemented correctly for TH2D
template <>
TH2D* MergeHistos::get_clean_combined_histo(TH2D *combined_histo)
{
       	return combined_histo;
}

template <>
TH1D* MergeHistos::get_clean_combined_histo(TH1D *combined_histo)
{ 
	TH1D* clean_histo = new TH1D(combined_histo->GetName(),combined_histo->GetTitle(),combined_histo->GetNbinsX(),combined_histo->GetXaxis()->GetXmin(),combined_histo->GetXaxis()->GetXmax());
	//0 to nbins+1 to include underflow and overflow (ensure not already filled).
	for(int i = 0;i <= combined_histo->GetNbinsX()+1;i++){
		clean_histo->SetBinContent(i,combined_histo->GetBinContent(i));
	}

	return clean_histo;
}

template <class HISTO>
HISTO* MergeHistos::combine_histo(bool is_data,bool create_new)
{

	if(verbose && is_data) std::cout << " Is_data set = True" << std::endl;
	else if(verbose && !is_data) std::cout << " Is_data set = False" << std::endl;
	if(verbose) std::cout << "Combining histo " << histo_name << " cutset " << cutset << std::endl;
	if(integrated_luminosity == -1 && !is_data)
		std::cout << " WARNING: Integrated luminosity not set " <<std::endl;

	outfile->cd();

	HISTO *combined_histo=NULL;
	
	// combine the backgrounds
	
	bool first = true;	// first dataset to be processed
	for(std::map<std::string,std::vector<std::string> >::iterator file_name = file_names.begin();
		file_name != file_names.end();
		++file_name){
		std::string file_path = file_name->first;
		if(input_file_directory_name != "")
			file_path = input_file_directory_name +"/"+file_name->first;
		std::cout << "opening " << file_path <<std::endl;
		TFile *file = new TFile(file_path.c_str(), "open");
		std::cout << "opened " << file_path <<std::endl;

		for(std::vector<std::string>::iterator histo_name = file_name->second.begin();
			histo_name != file_name->second.end();
			++histo_name){
			outfile->cd();
			HISTO *histo=NULL;	// histogram from current file

			if(verbose) std::cout << "opening " << input_directory_name << "/" << histo_name->c_str() <<std::endl;
			// create a copy of the first histo:
			// to this histogram later on the histograms yet to
			// come are added and it is then afterwards returned as
			// output of this function
			if(first){
				if(verbose) std::cout << "Creating new TH1D for " << histo_name->c_str() << std::endl;
				histo = new HISTO(*((HISTO *) file->GetDirectory(input_directory_name.c_str())->Get(histo_name->c_str())));
			}else{	// if this is not the first histogram we don't
				// make a copy but instad just scale it in memory and add
				// it later to the first histo
				histo = (HISTO *) file->GetDirectory(input_directory_name.c_str())->Get(histo_name->c_str());
			}
			if(verbose) std::cout << "opened "<< input_directory_name << "/" << histo_name->c_str() <<std::endl; 

			// put content of overflow bin into last bin
			int nbins = histo->GetNbinsX();
			if (histo->GetBinContent(nbins+1) != 0){
			 	histo->SetBinContent(nbins, histo->GetBinContent(nbins)+histo->GetBinContent(nbins+1));
			}
			//put content of underflow bin in first bin
// 			if (histo->GetBinContent(0) != 0){
// 				if(verbose) std::cout << "WARNING: Adding underflow to first bin for: " << *histo_name << std::endl;
// 				histo->SetBinContent(1,histo->GetBinContent(0)+histo->GetBinContent(1));
// 			}
			histo->SetBinContent(nbins+1,0);
			histo->SetBinContent(0,0);


			histo->Sumw2();

			// ignore empty histograms
			if(file_names.size() > 1 && histo->Integral() == 0){
				if(verbose) std::cout << *histo_name <<" was empty, skipping.." <<std::endl;
				continue;
			}
			else if(file_names.size() == 1 && histo->Integral() == 0){
				//Don't skip empty files. this will return a NULL histo and cause problems
				if(verbose) std::cout << *histo_name <<" was empty, NOT skipping.." <<std::endl;
			}

			// scale histogram to cross section
			if(histo->Integral() != 0 && !is_data)
				scale_histo<HISTO>(histo, *histo_name, input_directory_name, file,file_name->first );
			else if(is_data){
				if(verbose) std::cout << " Skipped scaling for data hist " << file_name->first << std::endl;
			}

			//Rebin histo
			rebin_histo<HISTO>(histo);

			if(verbose && !is_data) std::cout << " Histos scaled successfully " <<std::endl;

			// if first histo that is processed, make this histo the base for the combined histo
			// else just add it to combined histo
			if(first){
				if(verbose) std::cout << "Adding Histo to combined_histo "  << std::endl;
				combined_histo = histo;	
				first = false;
			}else{
				combined_histo->Add(histo);
			}
			if(verbose) std::cout << " Histo added " <<std::endl;
		}

		delete file;
		file = NULL;
	}
	if(combined_histo == NULL) std::cout << "***ERROR*** returning NULL histo in MergeHistos " << std::endl;

	if(xaxes != NULL && xaxes->find(histo_name) != xaxes->end()){
		//to make internal info consistent have to use Set and set nbins manually
		double w = combined_histo->GetBinWidth(1);
		double min = (*xaxes)[histo_name].first;
		double max = (*xaxes)[histo_name].second;

		int nbinsX = (max-min)/w;
		double maxX = min + (w*nbinsX);
		if(verbose) std::cout << "Setting x-axis range for " << histo_name << " " << min<<  " " << maxX << " nbins " << nbinsX << std::endl;
		//combined_histo->SetAxisRange((*xaxes)[histo_name].first, maxX,"X"); 
		combined_histo->GetXaxis()->Set(nbinsX, min, maxX);
	}

	if(!create_new)
		return combined_histo;
	else
		return get_clean_combined_histo(combined_histo);
}

template <class HISTO>
void MergeHistos::scale_histo(HISTO *histo, std::string histo_name, std::string input_directory_name, TFile *file,std::string file_name)
{
	// parse histo name
	if(verbose) std::cout << " Parsing histo name: " << histo_name << std::endl;
	std::string histo_name_parse = histo_name;
	if(histo_name_parse.find(".") != std::string::npos)
		histo_name_parse.erase(0,histo_name_parse.find(".")+1);

	name_parser->set_histo_name(histo_name_parse);
	std::string dataset = name_parser->dataset();
	std::string cutset = name_parser->cutset();
	std::string event_type = name_parser->event_type();
	std::string short_histo_name = name_parser->histo();

	double additional_scale_factor = 1;
	if(scale_by_additional_scale_factor.find(dataset) != scale_by_additional_scale_factor.end()){
		std::map<std::string,double> channel_sf = scale_by_additional_scale_factor[dataset];
		std::cout << "Additional scale factor for: " << dataset << " " << event_type << std::endl; 
		if(channel_sf.find(event_type) != channel_sf.end()){
			std::cout << "Applied" <<  std::endl; 
			additional_scale_factor = channel_sf[event_type];
		}
	}
	// if dataset in the list of datasets that will not be scaled, exit this function
	if(find(list_do_not_scale.begin(), list_do_not_scale.end(), dataset) != list_do_not_scale.end()){
		if(additional_scale_factor != 1)
			histo->Scale(additional_scale_factor);
		return;
	}

	if(efficiencies.find(file_name+"_"+event_type+"_"+cutset) == efficiencies.end()){
		// reconstruct event counter name and open event counter histogram
		std::string event_counter_name = "event_counter_"+dataset+"|"+event_type+"|"+cutset;
		if(verbose) std::cout << "opening event counter: " << event_counter_name << std::endl;
		TH1D *eff_histo = (TH1D *) file->GetDirectory(input_directory_name.c_str())->Get(event_counter_name.c_str());
		if(verbose) std::cout << "done." << std::endl;
		
		if(!eff_histo){
			std::cout << "**ERROR** eff_histo NULL" << std::endl;
			exit(1);
		}
			
		// calculate efficiency for selection step
		if(eff_histo->GetBinContent(1) != 0){
			efficiencies[file_name+"_"+event_type+"_"+cutset] = 1. / eff_histo->GetBinContent(1);
			std::cout.precision(20);
			if(verbose) std::cout << "Total n events: " << eff_histo->GetBinContent(1) << std::endl;
		}else {
			std::cout << "**ERROR** efficieny = 0" << std::endl;
			efficiencies[file_name+"_"+event_type+"_"+cutset] = 0;
		}
	}

        double efficiency = efficiencies[file_name+"_"+event_type+"_"+cutset];
        double scaling_factor = integrated_luminosity*efficiency;

	// get cross section for dataset, in case signal dataset only fraction
	// of actual signal events ( ~15% in semi-lept case)
	// also consider filter efficiency
	double cross_section = get_cross_section(dataset, event_type);

	scaling_factor *= cross_section;
	scaling_factor *= additional_scale_factor;

        if(cross_section != -1)
        {
		std::cout.precision(20);
		if(verbose) std::cout << " Scaling by lumi " << integrated_luminosity << " *(CS*filt_eff) " << cross_section <<  " *eff " << efficiency;
		if(verbose && additional_scale_factor != 1) std::cout << "*additional scale factor " << additional_scale_factor;
		if(verbose) std::cout <<  " = " << scaling_factor << std::endl;
		if(verbose) std::cout << "histo integral " << histo->Integral() << std::endl;


        	histo->Scale(scaling_factor);
        }
        else
                std::cout << "NOTE: " << histo_name << " not scaled " << std::endl;

}

template <>
std::vector<TH1D*> MergeHistos::get_histos_for_eff(std::string which_histos)
{
	if(!combine_eff_histos){
		std::cout << "ERROR: Histos for eff weren't filled because combine_eff_histos = false. Returning empty vector" << std::endl;
		return combined_denom_histos;
	}
	else if(which_histos == "num")
		return combined_num_histos;
	else if(which_histos == "denom")
		return combined_denom_histos;
	else{
		std::cout << "MergeHistos:get_histos_for_eff. Invalid set of histos requested: " << which_histos << " must get num or denom" << std::endl;
		std::vector<TH1D*> empty;
		return empty;
	}

}

template <>
std::vector<TH2D*> MergeHistos::get_histos_for_eff(std::string which_histos)
{
	std::vector<TH2D*> empty;
	std::cout << " Efficiency histos not implemented for 2D histos" <<std::endl;
	return empty;
}

template <>
void MergeHistos::save_combined_histo(std::string histo_name,TH1D* combined_histo)
{
	/********************************************************
             If you want to create efficiency histograms you have to fill two vectors above. 
             One with the _All plots and one with _Filtered plots 
             (after the same plots from all the different data files have been combined) . 
             The Efficiency plot will then auto calculate the Eff for each plot which
             appears in both vectors and append _Eff to the _Filtered name. 
	*******************************************************/
	if(verbose) std::cout << " Adding histogram to vector for efficiency" << std::endl;
	//Fill vectors needed to create efficiency histograms
	if(origin_of_input == 0){
		if(histo_name.find("All.") != std::string::npos)
			combined_denom_histos.push_back(combined_histo);
		else// if(histo_name->find("HLT_matched.") != std::string::npos)
			combined_num_histos.push_back(combined_histo);
	}
	else if(origin_of_input == 1){
		if(histo_name.find("_All") != std::string::npos && histo_name.find("Filtered") == std::string::npos)
			combined_denom_histos.push_back(combined_histo);
		else
			combined_num_histos.push_back(combined_histo);
	}
}

template <>
void MergeHistos::save_combined_histo(std::string histo_name,TH2D* combined_histo)
{
	std::cout << " Efficiency histos not implemented for 2D histos" <<std::endl;

}

template <>
void MergeHistos::rebin_histo(TH2D* histo)
{
	if(rebin_one != -1)histo->RebinX(rebin_one);
	if(rebin_one_y != -1)histo->RebinY(rebin_one_y);
}

template <>
void MergeHistos::rebin_histo(TH1D* histo)
{
	if (rebin_one != -1) histo->Rebin(rebin_one);

}
