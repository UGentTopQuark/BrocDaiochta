#ifndef SmoothHISTOS_H
#define SmoothHISTOS_H

#include "TH1F.h"
#include "TAxis.h"
#include "TH1.h"
#include "TGraphSmooth.h"
#include "TGraph.h"
#include "../CanvasHolder/Canvas_Holder.h"




class SmoothHistos{
	public:
		SmoothHistos();
		~SmoothHistos();
		void smooth_histos(std::vector<std::string> histos_to_smooth,std::string local_datadir, std::string output_directory,std::string output_file_name,std::string histo_name,std::string version,std::string e_mu,std::string cutset,int rebin,std::string qcd_cutset = "");

	private:
		TH1F* smooth_histo(TH1F* histo_in);
		TGraph * create_graph(TH1F* histo_in);
		void plot_overlay(TH1F* hist_in,TH1F* smooth_hist,std::string output_directory,std::string identifier,int rebin);
		void write_smooth_histos(std::vector< TH1F*> histo_to_write,std::string out_file_path,std::string tree_name);
                std::map<std::string,CanvasHolder*> cholders;

};
#endif
