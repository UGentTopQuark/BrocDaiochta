#include "SmoothHistos.h"

SmoothHistos::SmoothHistos()
{
}
SmoothHistos::~SmoothHistos()
{
}

void SmoothHistos::smooth_histos(std::vector<std::string> histos_to_smooth,std::string datadir, std::string output_directory,std::string output_file_name,std::string histo_name,std::string version,std::string e_mu,std::string cutset,int rebin,std::string qcd_cutset)
{
	std::cout << "Smoothing Histograms" << std::endl;
	std::vector<TH1F*> histos_to_write;
	std::string tree_name = "eventselection";
	for(std::vector<std::string>::iterator ds = histos_to_smooth.begin();
	    ds != histos_to_smooth.end(); ds++){
		//ds is the dataset name of the histo you want to smooth, e.g Wjets
		
		std::string in_file_path = datadir+"/"+*ds+version+".root";
                std::cout << "SmoothH: opening " << in_file_path <<std::endl;
                TFile *file = new TFile(in_file_path.c_str(), "open");
                std::cout << "SmoothH: opened " << in_file_path <<std::endl;

		std::string full_histo_name = histo_name+*ds+"|"+e_mu+"_background|"+cutset;
		std::string event_histo_name = "event_counter_"+*ds+"|"+e_mu+"_background|"+cutset;
		if(*ds == "QCD"){ 
			full_histo_name = histo_name+*ds+"|"+e_mu+"_background|"+qcd_cutset;
			event_histo_name = "event_counter_"+*ds+"|"+e_mu+"_background|"+qcd_cutset;
		}
		else if(*ds == "TTbar"){ 
			std::cout << "ERROR: SmoothHistos.cc: Trying to smooth TTbar. Signal smoothing not set up" << std::endl;
			exit(1);
		}

		TH1F* hist_in = new TH1F(*((TH1F *) file->GetDirectory(tree_name.c_str())->Get(full_histo_name.c_str())));

		TH1F* smooth_hist = smooth_histo(hist_in);

		std::string identifier = *ds+"_"+histo_name+e_mu+"_"+cutset;
		if(*ds == "QCD"){identifier = *ds+"_"+e_mu+"_"+histo_name+qcd_cutset;}
		plot_overlay(hist_in,smooth_hist,output_directory,identifier,rebin);

		histos_to_write.push_back(smooth_hist);
		TH1F* evhist_in = new TH1F(*((TH1F *) file->GetDirectory(tree_name.c_str())->Get(event_histo_name.c_str())));
		histos_to_write.push_back(evhist_in);
		
		//FIXME: Currently crashes when writing histo_out clone if close file
		//	file->Close();
	}

	std::string out_file_path = datadir+"/"+output_file_name;
	write_smooth_histos(histos_to_write,out_file_path,tree_name);
}


TH1F* SmoothHistos::smooth_histo(TH1F *histo_in)
{
	std::cout << "In Smooth Histo" << std::endl;
	TH1F *histo_out = new TH1F( *(TH1F*)histo_in->Clone());

	TGraph *gin = create_graph(histo_in);
	TGraph *gout = (TGraph*) gin->Clone();

	std::cout << "Smoothing histo" << std::endl;
	TGraphSmooth *gs = new TGraphSmooth("normal");
	//gout = gs->SmoothKern(gin,"box",2.0);
	gout = gs->SmoothSuper(gin,"",3);
	//gout = gs->SmoothKern(gin,"box",5.0);

	std::cout << "Manipulating output" << std::endl;
	TH1F *tmp_histout = gout->GetHistogram(); 
	Double_t *gyaxis = gout->GetY();
	for(int i = 0;i < gout->GetN();i++){
		tmp_histout->SetBinContent(i+1,gyaxis[i]);
	}
	int local_rebin = gout->GetN()/gin->GetN();
	std::cout << "Rebin: " << local_rebin << " gout n " << gout->GetN()<< " gin n " << gin->GetN() << std::endl;
	tmp_histout->Rebin(local_rebin);
	
	std::cout << "Deleting graphs" << std::endl;
	delete gin;
	std::cout << "done Deleting graphs" << std::endl;

	//Now fill histo_out from this histogram
	if(tmp_histout->GetNbinsX() != histo_out->GetNbinsX()){
		std::cout << " ERROR:SmoothHistos: nbins tmp out != nbins out " << std::endl;
		exit(1);
	}

	std::cout << "Filling Histo out" << std::endl;
	for(int i = 1; i < tmp_histout->GetNbinsX();i++){
		histo_out->SetBinContent(i,tmp_histout->GetBinContent(i));
	}
	std::cout << "Deleting tmp h" << std::endl;
	delete tmp_histout;
	//NB: output histos must have same NEntries and Integral as input to retain statistical and weighting information
	histo_out->SetEntries(histo_in->GetEntries());
	double scale = histo_in->Integral()/histo_out->Integral();
	histo_out->Scale(scale);

	std::cout << "*********** HSmoother output ***********" << std::endl;
	std::cout << " Integral hin: " << histo_in->Integral() << " hout: " << histo_out->Integral() << std::endl;
	std::cout << " Entries hin: " << histo_in->GetEntries() << " hout: " << histo_out->GetEntries() << std::endl;
	std::cout << "Scale: " << scale << std::endl;

	return histo_out;

}

TGraph* SmoothHistos::create_graph(TH1F *histo_in)
{
	std::cout << "Creating Graph" << std::endl;
	const int nbins = histo_in->GetNbinsX();
	double x[nbins];
	double y[nbins];

	for(int i = 0; i < nbins; i++){
		x[i] = i;
		y[i] = histo_in->GetBinContent(i+1);
	}

	TGraph *gin = new TGraph(nbins,x,y);
	std::cout << "Done Creating Graph" << std::endl;
	return gin;
}

void SmoothHistos::write_smooth_histos(std::vector<TH1F*> histos_to_write,std::string out_file_path,std::string tree_name)
{
	std::cout << "Writing smooth histos" << std::endl;
	TFile *file = new TFile(out_file_path.c_str(),"RECREATE");
	file->mkdir(tree_name.c_str());
	file->Cd(tree_name.c_str());

	for(std::vector<TH1F*>::iterator histo = histos_to_write.begin();
	    histo != histos_to_write.end(); histo++){
		if(*histo == NULL){
			std::cout << "ERROR: NULL histo" << std::endl;
			exit(1);
		}
		std::cout << (*histo)->GetEntries();
		std::cout << "Writing Histo" <<std::endl; 
		(*histo)->Write();
		std::cout << "Dione Writing Histo" << std::endl; 
	}
	file->Write();
	file->Close();

}

void SmoothHistos::plot_overlay(TH1F* hist_in,TH1F* smooth_hist,std::string output_directory,std::string identifier,int rebin)
{
	std::cout << "Plotting Overlay for: " << identifier << std::endl;
	std::vector<int> *colours = new std::vector<int>();
	colours->push_back(kRed);
	colours->push_back(kBlue);
	std::vector<int> *styles = new std::vector<int>();
	styles->push_back(2);
	styles->push_back(9);

	TH1F *display_hist = (TH1F*) smooth_hist->Clone();
	if(rebin != -1){
		hist_in->Rebin(rebin);
		display_hist->Rebin(rebin);
	}
	cholders[identifier] = new CanvasHolder();
	cholders[identifier]->setCanvasTitle(output_directory+"/Smooth_overlay_"+identifier);
	cholders[identifier]->setOptStat(000000);

	cholders[identifier]->addHisto(hist_in,"Input Histo","Ep");
	cholders[identifier]->addHisto(display_hist,"Smooth Histo","Ep");

	cholders[identifier]->setLineColors(*colours);
	cholders[identifier]->setLineStyles(*styles);
	
	cholders[identifier]->set_eff_histos(true);
	cholders[identifier]->setTitleX(hist_in->GetXaxis()->GetTitle());
	cholders[identifier]->setTitleY("");
	cholders[identifier]->setXaxisOff(1.1);
	cholders[identifier]->setLegendOptions(.3,.2);
	cholders[identifier]->save("pdf");

	delete display_hist;
	delete cholders[identifier];
	cholders[identifier]= NULL;
        cholders.clear();


}
