#include "LuminosityProvider.h"

LuminosityProvider::LuminosityProvider()
{

	set_luminosities();

}

LuminosityProvider::~LuminosityProvider()
{
}

void LuminosityProvider::set_luminosities()
{
	//testing
	luminosity["identifier"] = 20;

	// --- AUTO GENERATED INTEGRATED LUMINOSITIES
	// --- do not change this comment!
}

double LuminosityProvider::get_luminosity(std::map<std::string,std::vector<std::string> > &files_with_content)
{	
	double sum_lumi = 0.0;
	for(std::map<std::string, std::vector<std::string> >::iterator file_name = files_with_content.begin();
	    file_name != files_with_content.end();
	    ++file_name)
	{
		std::string start_filename;
		if ((file_name->first).find("_") != std::string::npos)
			start_filename = ((file_name->first).substr(0,(file_name->first).find("_")));
		else
			start_filename = ((file_name->first).substr(0,(file_name->first).find(".root")));

		if(verbose) std::cout << "Start filename for: " <<   file_name->first << " = " << start_filename << std::endl;
			
		std::string unique_id;

		if(file_name->first.find("data") != std::string::npos)
			unique_id = (start_filename.substr(4,start_filename.size()));
		else if(file_name->first.find("Data") != std::string::npos){			
			if(verbose) std::cout << "Filename contains Data  "  << std::endl;
			unique_id = (start_filename.substr(4,start_filename.size()));
		}
		else 
			unique_id = "MC";

		if(verbose) std::cout << "Unique ID  = " << unique_id << std::endl;

		if(luminosity.find(unique_id) != luminosity.end())
			sum_lumi += luminosity[unique_id];
		else if (unique_id != "MC"){
			std::cout << "ERROR: LuminosityProvider.cc No luminosity for this data file. "<< std::endl;
			return -1;
		}


	}
	if(verbose) std::cout << "Summed luminosity from LumiProv = " << sum_lumi << std::endl;
	
	return sum_lumi;
}
