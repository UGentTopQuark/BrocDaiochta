#ifndef LUMINOSITYPROVIDER_H
#define LUMINOSITYPROVIDER_H

#include <string>
#include <map>
#include <iostream>
#include <vector>

class LuminosityProvider{
	public:
		LuminosityProvider();
		~LuminosityProvider();
		
		double get_luminosity(std::map<std::string, std::vector<std::string> > &files_with_content);

	private:

		void set_luminosities();

		std::map<std::string, double> luminosity;
		static const bool verbose = true;
};

#endif
