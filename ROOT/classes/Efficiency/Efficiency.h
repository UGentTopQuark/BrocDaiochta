#ifndef EFFICIENCY_H
#define EFFICIENCY_H

#include "SetEffOptions.h"

class Efficiency:public SetEffOptions{
	
 public:
	Efficiency();
	~Efficiency();
	void calculate(std::vector<TH1F*> all,std::vector<TH1F*> filtered);
	TGraphAsymmErrors* calc_bayes_eff(TH1F* all_h, TH1F* filt_h, std::string eff_name);
	std::vector<double> calc_CP_eff(double all_sum, double filt_sum,double alpha_min);

 private:
	void calc_symm_eff(TH1F* all_h, TH1F* filt_h);
	void calc_bayes_eff(TH1F* all_h, TH1F* filt_h);
	void calc_CP_eff(TH1F* all_h, TH1F* filt_h);
	void output_overall_symm_eff(TH1F* all_h, TH1F* filt_h);
	void output_overall_bayes_eff(TH1F* all_h, TH1F* filt_h);
	void output_per_bin_bayes_eff(TH1F* all_h, TH1F* filt_h);
	void output_per_bin_eff_table();

};

#endif
