#ifndef SETEFFOPTIONS_H
#define SETEFFOPTIONS_H


#include "TFile.h"
#include "Math/QuantFuncMathCore.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TH1D.h"
#include "TH2D.h"
#include <sstream>
#include <vector>
#include <map>
#include "math.h"
#include <string>
#include <iostream>

class SetEffOptions{
        public:
	        SetEffOptions();
	        void set_outfile(TFile *outfile);
	        void set_output_dir(std::string dir_name);
	        void set_rebin(std::map<std::string, int> *bins);
		void set_pre_identifier(std::string pre_id);
		void set_post_identifier(std::string post_id);
		void set_method(int method);
		void set_output_total_efficiency(bool text_output = true);
		void set_output_per_bin_efficiency(std::string hname,bool text_output = true,bool table_output = false);
		//This histo will be used to calculate total efficiency
		//set multi_total_eff if this histogram contains the efficiency for multiply process
		//if bool = false will use histo->GetEntries for efficiency
		void set_total_eff_histo(std::string hname,bool multi_total_eff = false);
		void set_eff_histo_bin_names(std::vector<std::string> *bin_names);
		

        protected:
	        TFile *outfile;

		std::vector<std::string> get_multi_eff_names();

	        std::string output_directory_name;
		std::string prename_id;
		std::string postname_id;
		std::string total_eff_histo;
		std::string per_bin_eff_histo;
	        std::map<std::string, int> *rebin;
		std::map<std::string,std::vector<std::string> > pt_bins_per_cutset;
		std::map<std::string,std::map<std::string,std::vector<std::string> > > per_bin_eff_map;
		std::vector<std::string>* eff_histo_bin_names;  
		int method;
		bool output_total_efficiency;
		bool output_per_bin_efficiency;
		bool output_per_bin_table;
		bool multi_total_eff;

		static const bool verbose = false;

};

#endif
