#include "Efficiency.h"

Efficiency::Efficiency()
{

}
Efficiency::~Efficiency()
{
	if(output_per_bin_table)
		output_per_bin_eff_table();
}

//Find histos for which eff cal be calculated then use calc_eff to do the calculation
void Efficiency::calculate(std::vector<TH1F*> all,std::vector<TH1F*> filtered)
{
	if(verbose) std::cout << "Efficiency.cc: calculate() " << std::endl;

	//Throw away file to avoid duplicates in outfile
	TFile *rebin_outfile = new TFile("rebin_outfile.root", "RECREATE");
	rebin_outfile->cd();
	if(all.empty() || filtered.empty()){
		std::cout<< "ERROR: Empty vector for calculating efficiency" << std::endl;
		return;
	}

	for(std::vector<TH1F*>::iterator all_histo = all.begin();all_histo != all.end();all_histo++)
		{
			if(*all_histo == NULL) std::cout << "ERROR: Null histo Efficiency.cc" << std::endl;
			std::string all_name = (*all_histo)->GetName(); 
			//FIXME: this should not be hardcoded. should have option to ignore number histos when filling all vector 
			if(all_name.find("_number_") != std::string::npos && all_name.find("jet_number_") == std::string::npos)
				continue;

			rebin_outfile->cd();
			int rebin_filt = -1; 

			if(rebin != NULL && rebin->find(all_name) != rebin->end()){
				if (verbose) std::cout << " Rebinning: " << all_name << " by " << (*rebin)[all_name] << std::endl;
				const char * hname = (*all_histo)->GetName();
				*all_histo = (TH1F *) (*all_histo)->Rebin((*rebin)[all_name],hname);
				rebin_filt = (*rebin)[all_name];
			}

			for(std::vector<TH1F*>::iterator filt_histo = filtered.begin();filt_histo != filtered.end();filt_histo++)
				{
					std::string filt_name = (*filt_histo)->GetName();
					std::string common_filt_name = (*filt_histo)->GetName();
					std::string common_all_name = (*all_histo)->GetName();

					if(prename_id != ""){
						common_filt_name.erase(0,common_filt_name.find(prename_id));
						common_all_name.erase(0,common_all_name.find(prename_id));
					}
					if(postname_id != ""){
						common_filt_name = common_filt_name.substr(0,common_filt_name.rfind(postname_id));
						common_all_name = common_all_name.substr(0,common_all_name.rfind(postname_id));
					}


					
					//If there is a histo with the same name in _All and Filtered then use it to calculate efficiency
					if(common_all_name == common_filt_name){
						   if( verbose) std::cout << "Calling calc eff for "<< common_all_name << std::endl;
						   if(verbose) std::cout << "All name: " << all_name << ", Filt name: "<< filt_name << std::endl;

						   rebin_outfile->cd();						
						   if(rebin_filt != -1){
							   const char * hname = (*filt_histo)->GetName();
							   *filt_histo = (TH1F *) (*filt_histo)->Rebin((*rebin)[all_name],hname);
						   }

						   switch(method){
						   case 1: 
							   calc_symm_eff(*all_histo, *filt_histo);
							   break;
						   case 2:   
							   calc_bayes_eff(*all_histo, *filt_histo);
							   break;
						   case 3: 
							   calc_CP_eff(*all_histo, *filt_histo);
							   break;
						   default:
							   calc_bayes_eff(*all_histo, *filt_histo);
							   break;
						   }

						   if(output_total_efficiency && total_eff_histo != ""){
							   if(all_name.find(total_eff_histo) != std::string::npos && filt_name.find("_matched.") != std::string::npos){
									   switch(method){
									   case 1: 
										   output_overall_symm_eff(*all_histo, *filt_histo);
										   break;
									   case 2:   
										   output_overall_bayes_eff(*all_histo, *filt_histo);
										   break;
									   default:
										   output_overall_bayes_eff(*all_histo, *filt_histo);
										   break;
									   }
								   }
						   }
						   else if(output_total_efficiency && total_eff_histo == "")
							   std::cout << "ERROR: Eff.cc: histogram for total efficiency not given." << std::endl;
						   
						   if(output_per_bin_efficiency && per_bin_eff_histo != ""){
							   if(all_name.find(per_bin_eff_histo) != std::string::npos && filt_name.find("_matched.") != std::string::npos){
								   output_per_bin_bayes_eff(*all_histo, *filt_histo);
							   }
						   }
							   
						   
					}
				}
			
			
		} 


}

//Output the efficiency in the filtered region
void Efficiency::output_overall_symm_eff(TH1F* all_h, TH1F* filt_h)
{
	std::string filt_name = filt_h->GetName();
	int nbins = all_h->GetNbinsX();
	double eff = 0;
	double eff_err = 0;
	
	std::cout << " Efficiency for " << filt_name << std::endl;

	if(multi_total_eff){
		for(int i = 1;i <= nbins;i++){
			eff = 0;
			eff_err = 0;
			
			if(filt_h->GetBinContent(i) != 0 ){
				eff = filt_h->GetBinContent(i)/all_h->GetBinContent(i);
				eff_err = sqrt((eff*(1-eff))/all_h->GetBinContent(i));
			}
			std::string bin_id = "n/a'";
			if(eff_histo_bin_names != NULL && (int) eff_histo_bin_names->size() > (i-1))
				bin_id = (*eff_histo_bin_names)[i-1];

			std::cout.precision(3);
			std::cout << bin_id << " & " << eff << " \\pm " << eff_err  << " & " <<  all_h->GetBinContent(i) << " & " << filt_h->GetBinContent(i) << " \\\\ " << std::endl;
			
		}
	}
	else{
		if(filt_h->GetEntries() != 0){
			eff = filt_h->GetEntries()/all_h->GetEntries();
			eff_err = sqrt((eff*(1-eff))/all_h->GetEntries());
		}
		
		std::cout << "Eff: " << filt_h->GetEntries() << "/" << all_h->GetEntries() <<" "<<  filt_name << " = " << eff*100  << " \\pm " << eff_err  << std::endl;
	}

}
//Output the efficiency in the filtered region
void Efficiency::output_overall_bayes_eff(TH1F* all_h, TH1F* filt_h)
{
	std::string filt_name = filt_h->GetName();

 	TGraphAsymmErrors *eff_g = new TGraphAsymmErrors();

	eff_g->BayesDivide(filt_h, all_h);
 	
	std::cout << "BAYES Efficiency for " << filt_name << std::endl;
	int nbins = eff_g->GetN();
	
	if(multi_total_eff){
		int j = 1;
		for(int i = 0;i < nbins;i++){
			double eff = 0;
			double eff_errl = 0;
			double eff_errh = 0;
			if(filt_h->GetBinContent(j) != 0 ){
				eff = filt_h->GetBinContent(j)/all_h->GetBinContent(j);
				eff_errl = eff_g->GetErrorYlow(i); 
				eff_errh = eff_g->GetErrorYhigh(i); 
			}
			else {	
				//Since Tgraph only makes a point when nfilt != 0 
				//need to go over this point again if the histo bin was empty
				i--;
				j++;
				if(j > filt_h->GetNbinsX())
					break;
				else
					continue;
			}
			
			std::string bin_id = "n/a'";
			if(eff_histo_bin_names != NULL && (int) eff_histo_bin_names->size() > (j-1))
				bin_id = (*eff_histo_bin_names)[j-1];
			
			std::cout.precision(3);
			std::cout << bin_id << " &$ " << eff << " + " << eff_errh << " - " << eff_errl << " $&$ " <<  all_h->GetBinContent(j) << "$ &$ " << filt_h->GetBinContent(j) << "$ \\\\ " << std::endl;
			j++;
		}
	}
	else{
		double eff = 0;
		double eff_err = 0;
		double n_all = all_h->GetEntries();
	        double n_filt = filt_h->GetEntries();
		  if(filt_h->GetEntries() != 0){
		  eff = filt_h->GetEntries()/all_h->GetEntries();
		  eff_err = sqrt(((n_filt + 1)*(n_filt+2))/((n_all+2)*(n_all+3)) - ((n_filt +1)*(n_filt+1))/((n_all+2)*(n_all+2)));
		  }
		  
		  std::cout << "BAYES Eff: " << filt_h->GetEntries() << "/" << all_h->GetEntries() <<" "<<  filt_name << " = " << eff << " \\pm " << eff_err << std::endl;
	}

 	delete eff_g;
 	eff_g = NULL;

}


void Efficiency::calc_symm_eff(TH1F* all_h, TH1F* filt_h)
{

	std::string filt_name = filt_h->GetName();
	std::string filt_title = filt_h->GetTitle();
	int max_x = all_h->GetNbinsX();

	float  ptBins[100];
	TH1F *eff_h;

	if (filt_name.find("Pt") != std::string::npos || filt_name.find("pt") != std::string::npos){
		if(verbose) std::cout << " TESTING PT BINNING " << std::endl;
		if(verbose) std::cout << " FILENAME: " << filt_name << std::endl;

		int last_bin = 0;
		//If this is a pt histo bin widths are variable. 
		//Loop over all bins to get vector of bin boundaries to give to constructor
		for(int i = 1;i<= max_x;i++)
			{
				double bin_start = all_h->GetBinLowEdge(i);
				ptBins[i-1] = (float) bin_start;
				last_bin = i;

				if(verbose) std::cout << " BIN " << i-1 << ", LOW EDGE " << bin_start << std::endl;
			}
		//Max bin should be last entry of vector
		ptBins[last_bin] = (float) all_h->GetXaxis()->GetXmax();
		
		if(verbose) std::cout << "LAST BIN " << last_bin << ",  SET " << all_h->GetXaxis()->GetXmax() << std::endl;
		eff_h = new TH1F(("Eff_"+filt_name).c_str(),(filt_title).c_str(),max_x,ptBins);
	}
	else
		eff_h = new TH1F(("Eff_"+filt_name).c_str(),(filt_title).c_str(),max_x,all_h->GetXaxis()->GetXmin(),all_h->GetXaxis()->GetXmax());
	
	
	if(verbose) std::cout << " looping over bins "<< max_x << std::endl;
	//Loop over and calculate eff for each bin. Start from 0 to include underflow. Go to max+1 for overflow.
	if(verbose) std::cout << " Efficiency is also calulated in the over and underflow bins " << std::endl ;
	for(int i = 0;i<= (max_x+1);i++)
		{
			double all_sum = 0, filt_sum = 0, eff_err = 0;

			if(verbose) std::cout << " Getting bin content. bin " << i ;
			all_sum = all_h->GetBinContent(i);
			filt_sum = filt_h->GetBinContent(i);
			if(verbose) std::cout << ", all sum: " << all_sum << ", filt_sum: " << filt_sum << std::endl;  

			double eff = 0;
			if(all_sum != 0){
				eff = filt_sum/all_sum;
				if (eff < 1)
					eff_err = sqrt((eff*(1 - eff))/all_sum);

			}
			if(verbose) std::cout << " Eff: " << eff << ", Err: " << eff_err << std::endl ;
		
			
		eff_h->SetBinContent(i,eff);
		eff_h->SetBinError(i,eff_err);
		if(verbose) std::cout << " Bin contents set " << std::endl;

		}

	if(verbose) std::cout << " Done filling bins " << std::endl;
	
	eff_h->SetYTitle("Efficiency");
	eff_h->SetXTitle(filt_h->GetXaxis()->GetTitle());

	if(output_directory_name == ""){
		std::cout << "ERROR: No output dir set in Efficiency " << output_directory_name<<std::endl;
		return;
	}

	if(verbose) std::cout << " Change to correct dir " << std::endl;		
	outfile->cd();
	if(output_directory_name != ""){
		outfile->Cd(output_directory_name.c_str());
	}
	if(verbose) std::cout << " Write histo " << std::endl;
	eff_h->Write();

	delete eff_h;
	eff_h = NULL;

}

void Efficiency::calc_bayes_eff(TH1F* all_h, TH1F* filt_h)
{
	std::string filt_name = filt_h->GetName();
	std::string eff_name = "Eff_"+filt_name;

	TGraphAsymmErrors *eff_g = calc_bayes_eff(all_h,filt_h,eff_name);

	eff_g->Write();
 	
 	delete eff_g;
 	eff_g = NULL;
		
}

TGraphAsymmErrors *Efficiency::calc_bayes_eff(TH1F* all_h, TH1F* filt_h, std::string eff_name)
{
 	TGraphAsymmErrors *eff_g = new TGraphAsymmErrors();
	eff_g->SetName((eff_name).c_str());

	eff_g->BayesDivide(filt_h, all_h);

 	if(verbose) std::cout << " Change to correct dir " << std::endl;		
 	outfile->cd();
	if(output_directory_name != ""){
		outfile->Cd(output_directory_name.c_str());
	}
 	if(verbose) std::cout << " Write histo " << std::endl;
 	eff_g->Draw("APz");
	eff_g->GetYaxis()->SetTitle("Efficiency");
	eff_g->GetXaxis()->SetTitle(filt_h->GetXaxis()->GetTitle());

	return eff_g;
		
}

//ClopperPearsonBinomialInterval
void Efficiency::calc_CP_eff(TH1F* all_h, TH1F* filt_h)
{
	const double alpha = (1-0.682);
	double alpha_min = alpha/2;

	std::string filt_name = filt_h->GetName();
	std::string filt_title = filt_h->GetTitle();
	const int nbins = all_h->GetNbinsX();
	double x[nbins],eff[nbins], eff_errl[nbins],eff_errh[nbins],errXl[nbins],errXh[nbins];

	int last_bin = 0;
	for(int i = 1; i <= nbins;i++)
		{
			double all_sum = 0, filt_sum = 0;
			x[i] = all_h->GetBinCenter(i);
			last_bin = i;
			
			if(verbose) std::cout << " Getting bin content. bin " << i ;
			all_sum = all_h->GetBinContent(i);
			filt_sum = filt_h->GetBinContent(i);
			if(verbose) std::cout << ", all sum: " << all_sum << ", filt_sum: " << filt_sum << std::endl;  
			errXl[i] = errXh[i] = (filt_h->GetBinWidth(i))/2;
			
			std::vector<double> cp_eff = calc_CP_eff(all_sum,filt_sum,alpha_min);
			eff[i] = cp_eff[0];
			eff_errl[i] = cp_eff[1];
			eff_errh[i] = cp_eff[2];

			if(verbose) std::cout << " Eff: " << eff[i] << ", ErrL: " << eff_errl[i] << ", ErrH: " << eff_errh[i] << " X " << x[i] <<" ErrX "<< errXl[i]<< std::endl ;
		
		}
	x[last_bin] = (float) all_h->GetXaxis()->GetXmax();

 	TGraphAsymmErrors *eff_g = new TGraphAsymmErrors(nbins, x, eff, errXl, errXh, eff_errl, eff_errh);
	eff_g->SetName(("Eff_"+filt_name).c_str());

 	if(verbose) std::cout << " Change to correct dir " << std::endl;		
 	outfile->cd();
	outfile->Cd(output_directory_name.c_str());
 	if(verbose) std::cout << " Write histo " << std::endl;
 	eff_g->Draw("APz");
	eff_g->GetYaxis()->SetTitle("Efficiency");
	eff_g->GetXaxis()->SetTitle(filt_h->GetXaxis()->GetTitle());

	eff_g->Write();
 	delete eff_g;
 	eff_g = NULL;
	
}
//ClopperPearsonBinomialInterval
std::vector<double> Efficiency::calc_CP_eff(double all_sum, double filt_sum,double alpha_min)
{
	std::vector<double> result;
	result.resize(3,0);
	double lower = 0, upper = 0;

	if(all_sum != 0){
		result[0] = filt_sum/all_sum;

		if(filt_sum > 0){
			lower = ROOT::Math::beta_quantile(alpha_min, filt_sum,all_sum-filt_sum+1);
		}
		if(all_sum - filt_sum > 0){
			upper = ROOT::Math::beta_quantile_c(alpha_min, filt_sum + 1, all_sum - filt_sum);
		}
		result[1] = result[0] - lower;
		result[2] = upper - result[0];
	}
	return result;
}

void Efficiency::output_per_bin_bayes_eff(TH1F* all_h, TH1F* filt_h)
{
	std::string filt_name = filt_h->GetName();
	std::string eff_name = "Eff_"+filt_name;

 	TGraphAsymmErrors *eff_g = new TGraphAsymmErrors();
	eff_g->SetName(("Eff_"+filt_name).c_str());

	eff_g->BayesDivide(filt_h, all_h);

	if(!output_per_bin_table) std::cout << "BAYES Efficiency for " << filt_name << std::endl;
	int nbins = eff_g->GetN();

	//used to fill information into maps in order to output 2D table at the end
	std::string tmp_f_name = filt_name,tmp2_f_name = filt_name;
	std::string level = tmp_f_name.erase(tmp_f_name.find(".")); 
	std::string cutset = tmp2_f_name.erase(0,tmp2_f_name.find("|")); 
	std::string id = level + cutset;

	int j = 1;
	for(int i = 0;i < nbins;i++){
		double eff = 0;
		double eff_errl = 0;
		double eff_errh = 0;
		if(filt_h->GetBinContent(j) != 0 ){
			eff = filt_h->GetBinContent(j)/all_h->GetBinContent(j);
			eff_errl = eff_g->GetErrorYlow(i); 
			eff_errh = eff_g->GetErrorYhigh(i); 
		}
		else {	
			//Since Tgraph only makes a point when nfilt != 0 
			//need to go over this point again if the histo bin was empty
			i--;
			j++;
			if(j > filt_h->GetNbinsX())
				break;
			else
				continue;
		}
		
		double bin_low = filt_h->GetBinLowEdge(j);
		double bin_high = bin_low + filt_h->GetBinWidth(j); 
			
		if(!output_per_bin_table){
			std::cout.precision(3);
			std::cout << "$p_{T} "<< bin_low << " \\mathrm{to} "<< bin_high <<  " $&$ " << eff << " + " << eff_errh << " - " << eff_errl << " $&$ ";
			std::cout.precision(8);
			std::cout <<  all_h->GetBinContent(j) << "$ &$ " << filt_h->GetBinContent(j) << "$ \\\\ " << std::endl;
		}
		j++;

		//fill information into maps in order to output 2D table at the end
		//Don't look at this, seriously.set = truefalse in combine and think no more
		if(output_per_bin_table){
			//convert doubles to string
			std::ostringstream bl; bl << bin_low; std::string binl = bl.str();
			std::ostringstream bh; bh << bin_high; std::string binh = bh.str();
			std::ostringstream effss; effss << eff; std::string effs = effss.str();
			std::ostringstream eff_errhss; eff_errhss << eff_errh; std::string eff_errhs = eff_errhss.str();
			std::ostringstream eff_errlss; eff_errlss << eff_errl; std::string eff_errls = eff_errlss.str();
			
			std::string pt_range = "$p_{T} " + binl + " to " + binh + "$";
			std::string eff_string = "$" + effs + " + " + eff_errhs + " - " + eff_errls +"$";
			
			if(pt_bins_per_cutset.find(id) == pt_bins_per_cutset.end() || pt_bins_per_cutset[id].size() != (unsigned int) nbins) 
				pt_bins_per_cutset[id].push_back(pt_range);
			std::string eta_region;
			if(filt_name.find("barrel") != std::string::npos) eta_region = "barrel";
			if(filt_name.find("overlap") != std::string::npos) eta_region = "overlap";
			if(filt_name.find("endcap") != std::string::npos) eta_region = "endcap";
			per_bin_eff_map[id][eta_region].push_back(eff_string);
		}
	}
	
 	delete eff_g;
 	eff_g = NULL;
		
}

void Efficiency::output_per_bin_eff_table()
{

	for(std::map<std::string,std::vector<std::string> >::iterator id_pt = pt_bins_per_cutset.begin();
	    id_pt != pt_bins_per_cutset.end(); id_pt++){
		std::cout << "HISTO ID : " << id_pt->first << std::endl;
		for(std::vector<std::string>::iterator pt_range = (id_pt->second).begin();pt_range != (id_pt->second).end(); pt_range++){
			std::cout << " & " << *pt_range ;
		}
		std::cout << " \\\\ " << std::endl;

		for(std::map<std::string,std::map<std::string,std::vector<std::string> > >::iterator id_cutsets_eff = per_bin_eff_map.begin();
		    id_cutsets_eff != per_bin_eff_map.end(); id_cutsets_eff++){
			if(id_cutsets_eff->first == id_pt->first){
				for(std::map<std::string,std::vector<std::string> >::iterator cutsets_eff = (*id_cutsets_eff).second.begin();
				    cutsets_eff != (*id_cutsets_eff).second.end(); cutsets_eff++){
				
					std::cout << cutsets_eff->first ;
					
					for(std::vector<std::string>::iterator eff_str = (*cutsets_eff).second.begin();
					    eff_str != (*cutsets_eff).second.end(); eff_str++){
						
						std::cout.precision(3);
						std::cout << " & " << *eff_str;
						
					}
					std::cout << " \\\\ " << std::endl;
				}
			}
		}
	}

}




// 	TCanvas * c1 =new TCanvas("c1","TEST");	
// 	c1->SetFillColor(0);
// 	c1->SetBorderMode(0);
// 	c1->SetBorderSize(2);
// 	c1->SetTickx();
// 	c1->SetTicky();
// 	c1->SetFrameFillStyle(0);
// 	c1->SetFrameBorderMode(0);
// 	c1->SetFrameFillStyle(0);
// 	c1->SetFrameBorderMode(0);
// 	c1->cd();
// 	eff_g->Draw("APz");
// 	c1->SaveAs(("Eff_"+filt_name+".eps").c_str());
// 	c1->Write();
