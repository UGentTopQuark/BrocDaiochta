#include "SetEffOptions.h"

SetEffOptions::SetEffOptions()
{
	outfile = NULL;
	output_directory_name = "";
	prename_id = "";
	postname_id = "";
	total_eff_histo = "";
	per_bin_eff_histo = "";
	rebin = NULL;
	eff_histo_bin_names = NULL;
	//1 = simple binomial. outputs to histogram
	//2 = TGraphAsymmErrs with Bayes divide
	//3 = TGraphAsymmErrs with Clopper-Pearson Interval
	method = 2;
	output_total_efficiency = false;
	output_per_bin_efficiency = false;
	output_per_bin_table = false;
	multi_total_eff = false;
}

void SetEffOptions::set_outfile(TFile *outfile)
{
	this->outfile = outfile;
}

void SetEffOptions::set_output_dir(std::string dir_name)
{
	output_directory_name = dir_name;

}

void SetEffOptions::set_rebin(std::map<std::string,int> *bins)
{
        rebin = bins;
}

void SetEffOptions::set_pre_identifier(std::string pre_id)
{
        prename_id = pre_id;
}
 
void SetEffOptions::set_post_identifier(std::string post_id)
{
        postname_id = post_id;
}

void SetEffOptions::set_method(int method)
{
	this->method = method;
}

void SetEffOptions::set_output_total_efficiency(bool text_output)
{
	output_total_efficiency = text_output;
}

void SetEffOptions::set_output_per_bin_efficiency(std::string hname,bool text_output,bool table_output)
{
	output_per_bin_efficiency = text_output;
	per_bin_eff_histo = hname;
	output_per_bin_table = table_output;
}


void SetEffOptions::set_total_eff_histo(std::string hname, bool multi_total_eff)
{
	total_eff_histo = hname;
	this->multi_total_eff =  multi_total_eff;
}

void SetEffOptions::set_eff_histo_bin_names(std::vector<std::string> *bin_names)
{
	eff_histo_bin_names = bin_names;
}
