#ifndef CROSSSECTIONPROVIDER_H
#define CROSSSECTIONPROVIDER_H

#include <string>
#include <map>
#include <iostream>

class CrossSectionProvider{
	public:
		CrossSectionProvider();
		~CrossSectionProvider();

		double get_cross_section(std::string process);
		double get_filter_efficiency(std::string process);

		void set_analysis(std::string analysis);
	private:
		void set_cross_sections();
		void set_filter_efficiencies();

		void set_filter_efficiencies_unskimmed();

		void set_cross_sections_Spring11_7TeV();

		void set_cross_sections_Summer12();

		void set_cross_sections_Fall10_7TeV();
		void set_filter_efficiencies_Fall10_7TeV();

		void set_cross_sections_Summer10_7TeV();
		void set_cross_sections_Summer11_7TeV();
		void set_cross_sections_Fall11sw();
		void set_filter_efficiencies_Spring11_7TeV();
		void set_filter_efficiencies_Summer12();
		void set_filter_efficiencies_Summer11_7TeV();
		void set_filter_efficiencies_Summer11_pfJets_muisoloose();
		void set_filter_efficiencies_Summer11_pfNoTau_muisoloose();
		void set_filter_efficiencies_Summer10_7TeV_preselv1();
		void set_filter_efficiencies_Summer11_pfJets_emuisoveryloose();
		void set_filter_efficiencies_Fall11thesis();

		void set_cross_sections_Spring10_7TeV();
		void set_filter_efficiencies_Spring10_7TeV();
		void set_cross_sections_Summer09_7TeV();
		void set_filter_efficiencies_Summer09_7TeV();
		void set_cross_sections_10TeV();
		void set_filter_efficiencies_10TeV();

		std::map<std::string, double> cross_section;
		std::map<std::string, double> filter_efficiency;

		std::string default_analysis;
		std::string current_analysis_set;
		std::string previous_analysis_set;

		static const bool verbose = false;
};

#endif
