#!/usr/bin/perl

my %cross_sections=();
my %filter_efficiencies=();

read_cross_sections(\%cross_sections, \%filter_efficiencies);

while(my ($key, $val) = each %cross_sections){
	print "cross_sec: $key: $val\n";
}
while(my ($key, $val) = each %filter_efficiencies){
	print "filter_eff: $key: $val\n";
}

sub read_cross_sections
{
	my ($cs_ref, $fe_ref) = @_;
	
	$input_file = "CrossSectionProvider.cc";

	open(CSFILE, "<$input_file") or die "can't open cs file";
	my $cs_section = 0;
	my $fe_section = 0;
	while(defined($line = <CSFILE>)){
		$cs_section = 1 if($line =~ m/---BEGIN CROSS SECTIONS---/);
		$cs_section = 0 if($line =~ m/---END CROSS SECTIONS---/);
		$fe_section = 1 if($line =~ m/---BEGIN FILTER EFFICIENCIES---/);
		$fe_section = 0 if($line =~ m/---END FILTER EFFICIENCIES---/);
	
		next unless($cs_section or $fe_section);

		if($cs_section){
			if($line =~ m/cross_section\[\"([\d\w]+)\"\]\s*=\s*([\-\d\.\*]+)\s*\;/){
				my $dataset = $1;
				my $cs = $2;
				$cs_ref->{$dataset} = eval($cs);
			}
		}elsif($fe_section){
			if($line =~ m/filter_efficiency\[\"([\d\w]+)\"\]\s*=\s*([\-\d\.\*]+)\s*\;/){
				my $dataset = $1;
				my $fe = $2;
				$fe_ref->{$dataset} = eval($fe);
			}
		}
	};
	close(CSFILE);
}
