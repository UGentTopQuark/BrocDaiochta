#include "CrossSectionProvider.h"

CrossSectionProvider::CrossSectionProvider()
{
	default_analysis = "Summer12";
	current_analysis_set = default_analysis;
	previous_analysis_set = default_analysis;

	set_cross_sections();
	set_filter_efficiencies();
}

CrossSectionProvider::~CrossSectionProvider()
{
}

void CrossSectionProvider::set_analysis(std::string analysis)
{
	previous_analysis_set = current_analysis_set;
	current_analysis_set = analysis;
}

double CrossSectionProvider::get_cross_section(std::string process)
{
	if(previous_analysis_set != current_analysis_set)
		set_cross_sections();

	if(cross_section.find(process) != cross_section.end()){
		if(verbose) std::cout << "returning cross section " << process << " analysis " << current_analysis_set << ": " << cross_section[process] << std::endl;
		return cross_section[process];
	}else{
		std::cerr << "WARNING: CrossSectionProvider::get_cross_section(): cross section not found for process " << process << std::endl;
		return -1;
	}
}

double CrossSectionProvider::get_filter_efficiency(std::string process)
{
	if(previous_analysis_set != current_analysis_set)
		set_filter_efficiencies();
	

	if(filter_efficiency.find(process) != filter_efficiency.end()){
		if(verbose) std::cout << "returning filter efficiency " << process << " analysis " << current_analysis_set << ": " << filter_efficiency[process] << std::endl;
		return filter_efficiency[process];
	}else{
		std::cerr << "WARNING: CrossSectionProvider::get_filter_efficiency(): filter_efficiency not found for process " << process << std::endl;
		return 1;
	}
}
void CrossSectionProvider::set_cross_sections()
{
	if(!cross_section.empty())
		cross_section.clear();
		
	if(current_analysis_set == "Summer11_7TeV" ||
	   current_analysis_set == "Summer11_pfJets_muisoloose" ||
	   current_analysis_set == "Summer11_pfJets_emuisoveryloose" ||
	   current_analysis_set == "Summer11_pfNoTau_muisoloose" )
		set_cross_sections_Summer11_7TeV();
	else if(current_analysis_set == "Spring117TeVunskimmed")
		set_cross_sections_Spring11_7TeV();
	else if(current_analysis_set == "Spring11_7TeV")
		set_cross_sections_Spring11_7TeV();
	else if(current_analysis_set == "Fall107TeVunskimmed")
		set_cross_sections_Fall10_7TeV();
	else if(current_analysis_set == "Fall10_7TeV")
		set_cross_sections_Fall10_7TeV();
	else if(current_analysis_set == "Summer12")
		set_cross_sections_Summer12();
	else if(current_analysis_set == "Fall11sw")
		set_cross_sections_Fall11sw();
	else 
		std::cerr << "ERROR: CrossSectionProvider No analysis set for cross sections: " << current_analysis_set <<  std::endl;

}

void CrossSectionProvider::set_filter_efficiencies()
{
	if(!filter_efficiency.empty())
		filter_efficiency.clear();
	
	if(current_analysis_set == "Summer11_pfJets_muisoloose")
		set_filter_efficiencies_Summer11_pfJets_muisoloose();
	else if(current_analysis_set == "Summer11_pfNoTau_muisoloose")
		set_filter_efficiencies_Summer11_pfNoTau_muisoloose();
	else if(current_analysis_set == "Summer11_pfJets_emuisoveryloose")
		set_filter_efficiencies_Summer11_pfJets_emuisoveryloose();
	else if(current_analysis_set == "Summer11_7TeV")
		set_filter_efficiencies_Summer11_7TeV();
	else if(current_analysis_set == "Spring117TeVunskimmed")
		set_filter_efficiencies_unskimmed();
	else if(current_analysis_set == "Spring11_7TeV")
		set_filter_efficiencies_Spring11_7TeV();
	else if(current_analysis_set == "Fall107TeVunskimmed")
		set_filter_efficiencies_unskimmed();
	else if(current_analysis_set == "Fall10_7TeV")
		set_filter_efficiencies_Fall10_7TeV();
	else if(current_analysis_set == "Summer12")
		set_filter_efficiencies_Summer12();
	else if(current_analysis_set == "Fall11sw")
		set_filter_efficiencies_Fall11thesis();
	else 
		std::cerr << "ERROR: CrossSectionProvider No analysis set for filter efficiencies: " << current_analysis_set << std::endl;
}


void CrossSectionProvider::set_cross_sections_Summer12()
{
        cross_section["TTbar"] = 225.197; 
        cross_section["TTbarNorm"] = 225.197;                                                                                                                        
        cross_section["TTbarHighISR"] = 225.197;                                                                                                                     
        cross_section["TTbarLowISR"] = 225.197;                                                                                                                      
        cross_section["TTbarMatchingUP"] = 225.197;                                                                                                                  
        cross_section["TTbarMatchingDOWN"] = 225.197;                                                                                                                
        cross_section["TTbarMassUP"] = 225.197;                                                                                                                      
        cross_section["TTbarMassDOWN"] = 225.197;                                                                                                                    
        cross_section["TTbarScaleUP"] = 225.197;                                                                                                                     
        cross_section["TTbarScaleDOWN"] = 225.197;    
        cross_section["TTbarPileUp"] = 225.197;    
        cross_section["TTbarMass161"] = 225.197;     
        cross_section["TTbarMass163"] = 225.197;     
        cross_section["TTbarMass166"] = 225.197;     
        cross_section["TTbarMass169"] = 225.197;   
        cross_section["TTbarMass175"] = 225.197;   
        cross_section["TTbarMass178"] = 225.197;   
        cross_section["TTbarMass181"] = 225.197;   
        cross_section["TTbarMass184"] = 225.197;      
        cross_section["DY50"] = 3503.71; 
        cross_section["Zjets"] = 3503.71; 
        cross_section["Wjets"] = 36257.2; 
        cross_section["ZjetsMatchingUP"] = 3503.71; 
        cross_section["ZjetsMatchingDOWN"] = 3503.71; 
        cross_section["WjetsMatchingUP"] = 36257.2; 
        cross_section["WjetsMatchingDOWN"] = 36257.2; 
        cross_section["ZjetsScaleUP"] = 3503.71; 
        cross_section["ZjetsScaleDOWN"] = 3503.71; 
        cross_section["WjetsScaleUP"] = 36257.2; 
        cross_section["WjetsScaleDOWN"] = 36257.2; 
        cross_section["TtChan"] = 85.5352; 
        cross_section["TtWChan"] = 22.3546;
        cross_section["TsChan"] = 5.6517;
        cross_section["MuQCD"] = 0.;       // LO
        cross_section["QCDbctoe20to30"] = 0.;
        cross_section["QCDbctoe30to80"] = 0.;
        cross_section["QCDbctoe80to170"] = 0.;
        cross_section["QCDem20to30"] =0.;
        cross_section["QCDem30to80"] = 0.;
        cross_section["QCDem80to170"] = 0.;
        cross_section["QCD"] = 1.; //MM QCD already scaled
        cross_section["Gjets"] = 0.;
}


void CrossSectionProvider::set_cross_sections_Summer11_7TeV()
{

	cross_section["TTbar"] = 157.5;
        cross_section["TTbarNorm"] = 157.5;                                                                                                                                                               
        cross_section["TTbarHighISR"] = 157.5;                                                                                                                                                            
        cross_section["TTbarLowISR"] = 157.5;                                                                                                                                                             
        cross_section["TTbarMatchingUP"] = 157.5;                                                                                                                                                         
        cross_section["TTbarMatchingDOWN"] = 157.5;                                                                                                                                                       
        cross_section["TTbarMassUP"] = 157.5;                                                                                                                                                             
        cross_section["TTbarMassDOWN"] = 157.5;                                                                                                                                                           
        cross_section["TTbarScaleUP"] = 157.5;                                                                                                                                                            
        cross_section["TTbarScaleDOWN"] = 157.5;       
        cross_section["TTbarPileUp"] = 157.5;   
	cross_section["TTbarMass161"] = 157.5;    
	cross_section["TTbarMass163"] = 157.5;    
	cross_section["TTbarMass166"] = 157.5;    
	cross_section["TTbarMass169"] = 157.5;  
	cross_section["TTbarMass175"] = 157.5;  
	cross_section["TTbarMass178"] = 157.5;  
	cross_section["TTbarMass181"] = 157.5;  
	cross_section["TTbarMass184"] = 157.5;      
	cross_section["DY50"] = 3048;
	cross_section["Zjets"] = 3048;
	cross_section["Wjets"] = 31314;
	cross_section["ZjetsMatchingUP"] = 3048;
	cross_section["ZjetsMatchingDOWN"] = 3048;
	cross_section["WjetsMatchingUP"] = 31314;
	cross_section["WjetsMatchingDOWN"] = 31314;
	cross_section["ZjetsScaleUP"] = 3048;
	cross_section["ZjetsScaleDOWN"] = 3048;
	cross_section["WjetsScaleUP"] = 31314;
	cross_section["WjetsScaleDOWN"] = 31314;
	cross_section["TtChan"] = 64.6/3;
	cross_section["TtWChan"] = 10.6;
	cross_section["TsChan"] = 4.21;
	cross_section["MuQCD"] = 84679.3;	// LO
	cross_section["QCDbctoe20to30"] = 132160;
	cross_section["QCDbctoe30to80"] = 136804;
	cross_section["QCDbctoe80to170"] = 9360;
	cross_section["QCDem20to30"] = 2454400;
	cross_section["QCDem30to80"] = 3866200;
	cross_section["QCDem80to170"] = 139500;
	cross_section["QCD"] = 1.; //MM QCD already scaled
	cross_section["Gjets"] = 15090000; 

}
void CrossSectionProvider::set_cross_sections_Fall11sw()
{
        /* 
         * DO NOT CHANGE THIS COMMENT 
         * This comment has always to embrace the right cross sections,
         * make that sure! 
         * ---BEGIN CROSS SECTIONS--- 
         */ 

	cross_section["TTbar"] = 165;                                                                                                                                                          
        cross_section["TTbarMatchingUP"] = 165;                                                                                                                                                         
        cross_section["TTbarMatchingDOWN"] = 165;                                                                                                                                                       
        cross_section["TTbarMassUP"] = 165;                                                                                                                                                             
        cross_section["TTbarMassDOWN"] = 165;                                                                                                                                                           
        cross_section["TTbarScaleUP"] = 165;                                                                                                                                                            
        cross_section["TTbarScaleDOWN"] = 165;   
	cross_section["TTbarMass161"] = 165;    
	cross_section["TTbarMass163"] = 165;    
	cross_section["TTbarMass166"] = 165;    
	cross_section["TTbarMass169"] = 165;  
	cross_section["TTbarMass175"] = 165;  
	cross_section["TTbarMass178"] = 165;  
	cross_section["TTbarMass181"] = 165;  
	cross_section["TTbarMass184"] = 165;    
	cross_section["Zjets"] = 3048;
	cross_section["Wjets"] = 31314;
	cross_section["ZjetsMatchingUP"] = 3048;
	cross_section["ZjetsMatchingDOWN"] = 3048;
	cross_section["WjetsMatchingUP"] = 31314;
	cross_section["WjetsMatchingDOWN"] = 31314;
	cross_section["ZjetsScaleUP"] = 3048;
	cross_section["ZjetsScaleDOWN"] = 3048;
	cross_section["WjetsScaleUP"] = 31314;
	cross_section["WjetsScaleDOWN"] = 31314;
	cross_section["TtChan"] = 64.6;
	cross_section["TtWChan"] = 15.7;
	cross_section["TsChan"] = 4.6;
	cross_section["MuQCD"] = 84679.3;	// LO
	cross_section["QCDbctoe20to30"] = 132160;
	cross_section["QCDbctoe30to80"] = 136804;
	cross_section["QCDbctoe80to170"] = 9360;
	cross_section["QCDem20to30"] = 2454400;
	cross_section["QCDem30to80"] = 3866200;
	cross_section["QCDem80to170"] = 139500;
 
        /*
         * DO NOT CHANGE THIS COMMENT
         * ---END CROSS SECTIONS---
         */

}

void CrossSectionProvider::set_cross_sections_Spring11_7TeV()
{
	cross_section["TTbar"] = 157.5;
        cross_section["TTbarNorm"] = 157.5;                                                                                                                                                               
        cross_section["TTbarHighISR"] = 157.5;                                                                                                                                                            
        cross_section["TTbarLowISR"] = 157.5;                                                                                                                                                             
        cross_section["TTbarMatchingUP"] = 157.5;                                                                                                                                                         
        cross_section["TTbarMatchingDOWN"] = 157.5;                                                                                                                                                       
        cross_section["TTbarMassUP"] = 157.5;                                                                                                                                                             
        cross_section["TTbarMassDOWN"] = 157.5;                                                                                                                                                           
        cross_section["TTbarScaleUP"] = 157.5;                                                                                                                                                            
        cross_section["TTbarScaleDOWN"] = 157.5;       
        cross_section["TTbarPileUp"] = 157.5;       
	cross_section["DY50"] = 3048;
	cross_section["Zjets"] = 3048;
	cross_section["Wjets"] = 31314;
	cross_section["ZjetsMatchingUP"] = 3048;
	cross_section["ZjetsMatchingDOWN"] = 3048;
	cross_section["WjetsMatchingUP"] = 31314;
	cross_section["WjetsMatchingDOWN"] = 31314;
	cross_section["ZjetsScaleUP"] = 3048;
	cross_section["ZjetsScaleDOWN"] = 3048;
	cross_section["WjetsScaleUP"] = 31314;
	cross_section["WjetsScaleDOWN"] = 31314;
	cross_section["TtChan"] = 64.6/3;
	cross_section["TtWChan"] = 10.6;
	cross_section["TsChan"] = 4.21;
	cross_section["MuQCD"] = 84679.3;	// LO
	cross_section["QCDbctoe20to30"] = 132160;
	cross_section["QCDbctoe30to80"] = 136804;
	cross_section["QCDbctoe80to170"] = 9360;
	cross_section["QCDem20to30"] = 2454400;
	cross_section["QCDem30to80"] = 3866200;
	cross_section["QCDem80to170"] = 139500;
	cross_section["QCD"] = 1.; //MM QCD already scaled
}


void CrossSectionProvider::set_cross_sections_Fall10_7TeV()
{
	// factor 1.007: https://hypernews.cern.ch/HyperNews/CMS/get/top/1201.html
	cross_section["TTbar"] = 157.5*1.007;
        cross_section["TTbarNorm"] = 157.5*1.007;                                                                                                                                                               
        cross_section["TTbarHighISR"] = 157.5*1.007;                                                                                                                                                            
        cross_section["TTbarLowISR"] = 157.5*1.007;                                                                                                                                                             
        cross_section["TTbarMatchingUP"] = 157.5*1.007;                                                                                                                                                         
        cross_section["TTbarMatchingDOWN"] = 157.5*1.007;                                                                                                                                                       
        cross_section["TTbarMassUP"] = 157.5*1.007;                                                                                                                                                             
        cross_section["TTbarMassDOWN"] = 157.5*1.007;                                                                                                                                                           
        cross_section["TTbarScaleUP"] = 157.5*1.007;                                                                                                                                                            
        cross_section["TTbarScaleDOWN"] = 157.5*1.007;       
        cross_section["TTbarPileUp"] = 157.5*1.007;       
	cross_section["DY50"] = 3048*1.007;
	cross_section["Zjets"] = 3048*1.007;
	cross_section["Wjets"] = 31314*1.007;
	cross_section["ZjetsMatchingUP"] = 3048*1.007;
	cross_section["ZjetsMatchingDOWN"] = 3048*1.007;
	cross_section["WjetsMatchingUP"] = 31314*1.007;
	cross_section["WjetsMatchingDOWN"] = 31314*1.007;
	cross_section["ZjetsScaleUP"] = 3048*1.007;
	cross_section["ZjetsScaleDOWN"] = 3048*1.007;
	cross_section["WjetsScaleUP"] = 31314*1.007;
	cross_section["WjetsScaleDOWN"] = 31314*1.007;

	cross_section["TtChan"] = (64.6/3)*1.007;
	cross_section["TtWChan"] = 10.6*1.007;
	cross_section["TsChan"] = 4.21*1.007;
	cross_section["MuQCD"] = 349988.*1.007;	// LO
	cross_section["QCDbctoe20to30"] = 132160*1.007;
	cross_section["QCDbctoe30to80"] = 136804*1.007;
	cross_section["QCDbctoe80to170"] = 9360*1.007;
	cross_section["QCDem20to30"] = 2454400*1.007;
	cross_section["QCDem30to80"] = 3866200*1.007;
	cross_section["QCDem80to170"] = 139500*1.007;
	cross_section["QCD"] = 1.; //MM QCD already scaled
}

void CrossSectionProvider::set_filter_efficiencies_unskimmed()
{
	filter_efficiency["TTbar_electron"] = 1.;
	filter_efficiency["TTbar_e_background"] = 1.;
	filter_efficiency["TTbar_muon"] = 1.;
	filter_efficiency["TTbar_mu_background"] = 1.;
	filter_efficiency["DY50"] = 1.;
	filter_efficiency["Wjets"] = 1.;
	filter_efficiency["TtChan"] = 1.;
	filter_efficiency["TtWChan"] = 1.;
	filter_efficiency["TsChan"] = 1.;
	filter_efficiency["MuQCD"] = 1.;	
	filter_efficiency["QCDbctoe20to30"] = 1.;
	filter_efficiency["QCDbctoe30to80"] = 1.;
	filter_efficiency["QCDbctoe80to170"] = 1.;
	filter_efficiency["QCDem20to30"] = 1.;
	filter_efficiency["QCDem30to80"] = 1.;
	filter_efficiency["QCDem80to170"] = 1.;
	filter_efficiency["QCD"] = 0.0645802/191.05; //MM electron channel scale factor. Will be multiplied by lumi later.
	//filter_efficiency["QCD"] = 1/191.05; 
}

void CrossSectionProvider::set_filter_efficiencies_Fall11thesis()
{
	/*
	 * DO NOT CHANGE THIS COMMENT
	 * This comment has always to embrace the right filter efficiencies,
	 * make that sure!
	 * ---BEGIN FILTER EFFICIENCIES---
	 */
	filter_efficiency["TTbar_electron"] = 1.;
	filter_efficiency["TTbar_e_background"] = 1.;
	filter_efficiency["TTbar_muon"] = 1.;
	filter_efficiency["TTbar_mu_background"] = 1.;
	filter_efficiency["Wjets"] = 1.;
	filter_efficiency["Zjets"] = 1.;
	filter_efficiency["TtChan"] = 1.;
	filter_efficiency["TtWChan"] = 1.;
	filter_efficiency["TsChan"] = 1.;
	filter_efficiency["MuQCD"] = 0.00388344256361369;
	filter_efficiency["QCDbctoe20to30"] = 0.00195767937149352;
	filter_efficiency["QCDbctoe30to80"] = 0.00388565111995716;
	filter_efficiency["QCDbctoe80to170"] = 0.00423481861399051;
	filter_efficiency["QCDem20to30"] = 0.000579113619393495;
	filter_efficiency["QCDem30to80"] = 0.00103295741025337;
	filter_efficiency["QCDem80to170"] = 0.00263640525888736;


	/*
	 * DO NOT CHANGE THIS COMMENT
	 * ---END FILTER EFFICIENCIES---
	 */

}

void CrossSectionProvider::set_filter_efficiencies_Summer11_pfNoTau_muisoloose()
{
	// // 1mu, 3jets presel
 	filter_efficiency["TTbar_muon"] =0.318646647291277;
 	filter_efficiency["Zjets"] = 0.0447917032891503;
 	filter_efficiency["Wjets"] = 0.0237726339913298;
 	filter_efficiency["TtChan"] = 0.0870613109014964;
 	filter_efficiency["TtWChan"] = 0.271942408738938;
 	filter_efficiency["TsChan"] = 0.0946948833164227;
	filter_efficiency["MuQCD"] = 0.0183112101726787;
}

void CrossSectionProvider::set_filter_efficiencies_Summer11_pfJets_muisoloose()
{
	// 1mu, 3jets presel
	filter_efficiency["TTbar_muon"] = 1.;
	filter_efficiency["TTbar_mu_background"] = 1.;
	filter_efficiency["TTbar_electron"] = 1.;
	filter_efficiency["TTbar_e_background"] = 1.;
	filter_efficiency["Zjets"] = 0.0537238809504456;
	filter_efficiency["Wjets"] = 0.0257261607720266;
	filter_efficiency["TtChan"] = 0.0939721503126958;
	filter_efficiency["TtWChan"] = 0.29104861766866;
	filter_efficiency["TsChan"] = 0.100650219259983;
	filter_efficiency["MuQCD"] = 0.0199290068307903;
}

void CrossSectionProvider::set_filter_efficiencies_Summer11_pfJets_emuisoveryloose()
{

	// 1mu, 3jets presel
	filter_efficiency["TTbar_muon"] = 1.;
	filter_efficiency["TTbar_mu_background"] = 1.;
	filter_efficiency["TTbar_electron"] = 1.;
	filter_efficiency["TTbar_e_background"] = 1.;
	filter_efficiency["Zjets"] = 0.0571514061806433;
	filter_efficiency["Wjets"] = 0.0292835775849493;
	filter_efficiency["TtChan"] = 0.122134644865571;
	filter_efficiency["TtWChan"] = 0.334474508138432;
	filter_efficiency["TsChan"] = 0.132142339469488;
	filter_efficiency["MuQCD"] = 0.0625453533454701;
	filter_efficiency["Gjets"] = 1.;

	filter_efficiency["QCDbctoe20to30"] = 0.0316002748286083;
	filter_efficiency["QCDbctoe30to80"] = 0.0582847667993575;
	filter_efficiency["QCDbctoe80to170"] = 0.0546083785678462;
	filter_efficiency["QCDem20to30"] = 0.00256310798737038;
	filter_efficiency["QCDem30to80"] = 0.00553470294375802;
	filter_efficiency["QCDem80to170"] = 0.0112062032593016;

	filter_efficiency["TTbarMatchingDOWN"] = 1.;
	filter_efficiency["TTbarMatchingUP"] = 1.;
	filter_efficiency["TTbarScaleDOWN"] = 1.;
	filter_efficiency["TTbarScaleUP"] = 1.;

	filter_efficiency["ZjetsMatchingDOWN"] = 0.0546986065910768;
	filter_efficiency["ZjetsMatchingUP"] = 0.0501825612431589;
	filter_efficiency["ZjetsScaleDOWN"] = 0.0872865801283307;
	filter_efficiency["ZjetsScaleUP"] = 0.041242219337473;

	filter_efficiency["WjetsMatchingDOWN"] = 0.0288419461951118;
	filter_efficiency["WjetsMatchingUP"] = 0.0258551825691059;
	filter_efficiency["WjetsScaleUP"] = 0.0206404618868631;
	filter_efficiency["WjetsScaleDOWN"] = 1.;

 
	filter_efficiency["TTbarMass161_muon"] = 0.378099862228345;    
	filter_efficiency["TTbarMass163_muon"] = 0.379247573930151;    
	filter_efficiency["TTbarMass166_muon"] = 0.380251091349847;    
	filter_efficiency["TTbarMass169_muon"] = 0.380650703050599;  
	filter_efficiency["TTbarMass175_muon"] = 0.382111823368769;  
	filter_efficiency["TTbarMass178_muon"] = 0.382630712779167;  
	filter_efficiency["TTbarMass181_muon"] = 0.384077221004594;  
	filter_efficiency["TTbarMass184_muon"] = 0.384185396810106; 

	filter_efficiency["TTbarMass161_electron"] = 0.378099862228345;    
	filter_efficiency["TTbarMass163_electron"] = 0.379247573930151;    
	filter_efficiency["TTbarMass166_electron"] = 0.380251091349847;    
	filter_efficiency["TTbarMass169_electron"] = 0.380650703050599;  
	filter_efficiency["TTbarMass175_electron"] = 0.382111823368769;  
	filter_efficiency["TTbarMass178_electron"] = 0.382630712779167;  
	filter_efficiency["TTbarMass181_electron"] = 0.384077221004594;  
	filter_efficiency["TTbarMass184_electron"] = 0.384185396810106; 

}


void CrossSectionProvider::set_filter_efficiencies_Summer11_7TeV()
{
	// 1mu, 3jets presel
	//filter_efficiency["TTbar_muon"] = 0.708404499;
	//filter_efficiency["TTbar_mu_background"] = 0.0525366883;
	filter_efficiency["TTbar_muon"] = 0.148374331;	// sig*0.15 + bkg*0.85
	filter_efficiency["TTbar_electron"] = 1.;	// FIXME
	filter_efficiency["TTbar_e_background"] = 1.;	// FIXME
	filter_efficiency["Zjets"] = 0.547454344672453;
	filter_efficiency["Wjets"] = 0.00838622107050981;
	filter_efficiency["TtChan"] = 0.031390418522675;
	filter_efficiency["TtWChan"] = 0.123949213521777;
	filter_efficiency["TsChan"] = 0.0389041890129004;
	filter_efficiency["MuQCD"] = 0.00402639671604432;
	filter_efficiency["QCDbctoe20to30"] = 1.;	// FIXME
	filter_efficiency["QCDbctoe30to80"] = 1.;	// FIXME
	filter_efficiency["QCDbctoe80to170"] = 1.;	// FIXME
	filter_efficiency["QCDem20to30"] = 1.;	// FIXME
	filter_efficiency["QCDem30to80"] = 1.;	// FIXME
	filter_efficiency["QCDem80to170"] = 1.;	// FIXME
	filter_efficiency["QCD"] = 0.0645802/191.05; //MM electron channel scale factor. Will be multiplied by lumi later.

	// 1lep, 3jets presel
	//filter_efficiency["TTbar_electron"] = 1.;	// FIXME
	//filter_efficiency["TTbar_e_background"] = 1.;	// FIXME
	//filter_efficiency["TTbar_muon"] = 0.7098196;
	//filter_efficiency["TTbar_mu_background"] = 0.19939979;
	//filter_efficiency["Zjets"] = 0.0219709185849102;
	//filter_efficiency["Wjets"] = 0.0114949134785861;
	//filter_efficiency["TtChan"] = 0.0537065003385875;
	//filter_efficiency["TtWChan"] = 0.222198067312663;
	//filter_efficiency["TsChan"] = 0.066928518591834;
	//filter_efficiency["MuQCD"] = 0.00402639671604432;
	//filter_efficiency["QCDbctoe20to30"] = 1.;	// FIXME
	//filter_efficiency["QCDbctoe30to80"] = 1.;	// FIXME
	//filter_efficiency["QCDbctoe80to170"] = 1.;	// FIXME
	//filter_efficiency["QCDem20to30"] = 1.;	// FIXME
	//filter_efficiency["QCDem30to80"] = 1.;	// FIXME
	//filter_efficiency["QCDem80to170"] = 1.;	// FIXME
	//filter_efficiency["QCD"] = 0.0645802/191.05; //MM electron channel scale factor. Will be multiplied by lumi later.
}

void CrossSectionProvider::set_filter_efficiencies_Spring11_7TeV()
{
	// Only some of the data samples were skimmed for this production.  Information taken from https://twiki.cern.ch/twiki/bin/view/CMS/UGentTopDatasets
	filter_efficiency["TTbar_electron"] = 1.;
	filter_efficiency["TTbar_e_background"] = 1.;
	filter_efficiency["TTbar_muon"] = 1.;
	filter_efficiency["TTbar_mu_background"] = 1.;
	filter_efficiency["TTbarNorm_muon"] = 1.;
	filter_efficiency["TTbarNorm_mu_background"] = 1.;
	filter_efficiency["Zjets"] = 0.510583726466;
	filter_efficiency["Wjets"] = 1.;
	filter_efficiency["TtChan"] = 0.617286421347;
	filter_efficiency["TtWChan"] = 0.535402325624;
	filter_efficiency["TsChan"] = 0.627970809236;
	filter_efficiency["MuQCD"] = 1.;	
	filter_efficiency["QCDbctoe20to30"] = 1.;
	filter_efficiency["QCDbctoe30to80"] = 1.;
	filter_efficiency["QCDbctoe80to170"] = 1.;
	filter_efficiency["QCDem20to30"] = 1.;
	filter_efficiency["QCDem30to80"] = 1.;
	filter_efficiency["QCDem80to170"] = 1.;
	filter_efficiency["QCD"] = 0.0645802/191.05; //MM electron channel scale factor. Will be multiplied by lumi later.
}

void CrossSectionProvider::set_filter_efficiencies_Summer12()
{
        // Only some of the data samples were skimmed for this production.  Information taken from https://twiki.cern.ch/twiki/bin/view/CMS/UGentTopDatasets
        filter_efficiency["TTbar_electron"] = 1.;
        filter_efficiency["TTbar_e_background"] = 1.;
        filter_efficiency["TTbar_muon"] = 1.;
        filter_efficiency["TTbar_mu_background"] = 1.;
        filter_efficiency["TTbarNorm_muon"] = 1.;
        filter_efficiency["TTbarNorm_mu_background"] = 1.;
        filter_efficiency["Zjets"] = 1.;
        filter_efficiency["Wjets"] = 1.;
        filter_efficiency["TtChan"] = 1.;
        filter_efficiency["TtWChan"] = 1.;
        filter_efficiency["TsChan"] = 1.;
        filter_efficiency["MuQCD"] = 1.;
        filter_efficiency["QCDbctoe20to30"] = 1.;
        filter_efficiency["QCDbctoe30to80"] = 1.;
        filter_efficiency["QCDbctoe80to170"] = 1.;
        filter_efficiency["QCDem20to30"] = 1.;
        filter_efficiency["QCDem30to80"] = 1.;
        filter_efficiency["QCDem80to170"] = 1.;
        filter_efficiency["QCD"] = 0.0645802/191.05; //MM electron channel scale factor. Will be multiplied by lumi later.
}

void CrossSectionProvider::set_filter_efficiencies_Fall10_7TeV()
{
	filter_efficiency["TTbar_electron"] = 0.846444390843;
	filter_efficiency["TTbar_e_background"] = 0.554187870672;
	filter_efficiency["TTbar_muon"] = 0.902063758852;
	filter_efficiency["TTbar_mu_background"] = 0.544807621917;

        filter_efficiency["TTbarNorm"] = 1;
        filter_efficiency["TTbarNorm_electron"] = 0.846444390843;
        filter_efficiency["TTbarNorm_e_background"] = 0.554187870672;
        filter_efficiency["TTbarNorm_muon"] = 0.902063758852;
        filter_efficiency["TTbarNorm_mu_background"] = 0.544807621917;

	filter_efficiency["TTbarPileUp_electron"] = 0.846528361345;
	filter_efficiency["TTbarPileUp_e_background"] = 0.553582249227;
	filter_efficiency["TTbarPileUp_muon"] = 0.90147323336;
	filter_efficiency["TTbarPileUp_mu_background"] = 0.544259444842;

        filter_efficiency["TTbarLowISR"] = 1  ;
        filter_efficiency["TTbarLowISR_electron"] = 0.845511989151;
        filter_efficiency["TTbarLowISR_e_background"] = 0.553521122392;
        filter_efficiency["TTbarLowISR_muon"] = 0.900664305223;
        filter_efficiency["TTbarLowISR_mu_background"] = 0.544323037241;

        filter_efficiency["TTbarHighISR"] = 1;
        filter_efficiency["TTbarHighISR_electron"] = 0.840437844316;
        filter_efficiency["TTbarHighISR_e_background"] = 0.537479927798;
        filter_efficiency["TTbarHighISR_muon"] = 0.898238209797;
        filter_efficiency["TTbarHighISR_mu_background"] = 0.527904212657;

        filter_efficiency["TTbarMassUP"] = 1;
        filter_efficiency["TTbarMassUP_electron"] = 0.902524047154;
        filter_efficiency["TTbarMassUP_e_background"] = 0.902524047154;
        filter_efficiency["TTbarMassUP_muon"] = 0.902524047154;
        filter_efficiency["TTbarMassUP_mu_background"] = 0.902524047154;

        filter_efficiency["TTbarMassDOWN"] = 1;
        filter_efficiency["TTbarMassDOWN_electron"] = 0.840098210247;
        filter_efficiency["TTbarMassDOWN_e_background"] = 0.541872101845;
        filter_efficiency["TTbarMassDOWN_muon"] = 0.902524047154;
        filter_efficiency["TTbarMassDOWN_mu_background"] = 0.533619955866;

        filter_efficiency["TTbarMatchingUP"] = 1;
        filter_efficiency["TTbarMatchingUP_electron"] = 0.845585891506;
        filter_efficiency["TTbarMatchingUP_e_background"] = 0.55542050072;
        filter_efficiency["TTbarMatchingUP_muon"] = 0.901893478744;
        filter_efficiency["TTbarMatchingUP_mu_background"] = 0.545928565123;

        filter_efficiency["TTbarMatchingDOWN"] = 1;
        filter_efficiency["TTbarMatchingDOWN_electron"] = 0.84496078874;
        filter_efficiency["TTbarMatchingDOWN_e_background"] = 0.55336714101;
        filter_efficiency["TTbarMatchingDOWN_muon"] = 0.901374650175;
        filter_efficiency["TTbarMatchingDOWN_mu_background"] = 0.543308470735;

        filter_efficiency["TTbarScaleUP"] = 1;
        filter_efficiency["TTbarScaleUP_electron"] = 0.844808730035;
        filter_efficiency["TTbarScaleUP_e_background"] = 0.553202409801;
        filter_efficiency["TTbarScaleUP_muon"] = 0.901163701196;
        filter_efficiency["TTbarScaleUP_mu_background"] = 0.543486168701;

        filter_efficiency["TTbarScaleDOWN"] = 1;
        filter_efficiency["TTbarScaleDOWN_electron"] = 0.846676717527;
	filter_efficiency["TTbarScaleDOWN_e_background"] = 0.558412306592;
	filter_efficiency["TTbarScaleDOWN_muon"] = 0.902230979635;
	filter_efficiency["TTbarScaleDOWN_mu_background"] = 0.548581802296;

        filter_efficiency["WjetsMatchingUP"] = 0.164778257346;
        filter_efficiency["ZjetsMatchingUP"] = 0.464381132864;
	filter_efficiency["WjetsMatchingDOWN"] = 0.166410688685;
	filter_efficiency["ZjetsMatchingDOWN"] = 0.46446550992;
	filter_efficiency["WjetsScaleUP"] = 0.159011992445;
        filter_efficiency["ZjetsScaleUP"] = 0.462737262282;
	filter_efficiency["WjetsScaleDOWN"] = 0.198713758457;
	filter_efficiency["ZjetsScaleDOWN"] = 0.482599478482;

	filter_efficiency["DY50"] = 0.464722118495;
	filter_efficiency["Zjets"] = 0.464722118495;
	filter_efficiency["Wjets"] = 0.167186874452;
	filter_efficiency["TtChan"] = 0.613788604102;
	filter_efficiency["TtWChan"] = 0.541569673172;
	filter_efficiency["TsChan"] = 0.625166951887;
	filter_efficiency["MuQCD"] = 0.222033375653;	
	filter_efficiency["QCDbctoe20to30"] = 0.025735934875;
	filter_efficiency["QCDbctoe30to80"] = 0.179294733856;
	filter_efficiency["QCDbctoe80to170"] = 0.48729739565;
	filter_efficiency["QCDem20to30"] = 0.0449726710722;
	filter_efficiency["QCDem30to80"] = 0.116568875659;
	filter_efficiency["QCDem80to170"] = 0.256726056706;
	filter_efficiency["QCD"] = 0.0163905/36; //MM electron channel scale factor. Will be multiplied by lumi later.
}
