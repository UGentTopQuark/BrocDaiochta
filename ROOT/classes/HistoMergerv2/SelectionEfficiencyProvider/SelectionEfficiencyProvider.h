#ifndef EIRE_SELECTIONEFFICIENCYPROVIDER_H
#define EIRE_SELECTIONEFFICIENCYPROVIDER_H

#include "../HistoNameParser/HistoNameParser.h"
#include "TFile.h"
#include "TH1F.h"
#include "TKey.h"

namespace eire{
	class SelectionEfficiencyProvider{
		public:
			SelectionEfficiencyProvider();
			~SelectionEfficiencyProvider();
			
			void add_dataset(std::string filename);
			double get_efficiency(std::string ident, std::string cutset="");
		private:
			// map of dataset, cutset, selection efficiency
			std::map<std::string,std::map<std::string,double> > effs;

			HistoNameParser *hnparser;
	};
}

#endif
