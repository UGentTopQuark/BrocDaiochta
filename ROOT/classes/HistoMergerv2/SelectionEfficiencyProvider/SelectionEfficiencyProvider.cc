#include "SelectionEfficiencyProvider.h"

eire::SelectionEfficiencyProvider::SelectionEfficiencyProvider()
{
	hnparser = new HistoNameParser();
}

eire::SelectionEfficiencyProvider::~SelectionEfficiencyProvider()
{
	if(hnparser){ delete hnparser; hnparser = NULL; }
}

void eire::SelectionEfficiencyProvider::add_dataset(std::string filename)
{
	std::string directory = "eventselection";

	TFile file(filename.c_str(), "OPEN");

	TKey *key = NULL;
        TIter nextkey(file.GetDirectory(directory.c_str())->GetListOfKeys());
        while ((key = (TKey*)nextkey())){
        //      std::cout << "processing: " << key->GetName() << std::endl;
        //      std::cout << "class: " << key->GetClassName() << std::endl;

                std::string histo_name = key->GetName();
                if((std::string) key->GetClassName() == "TH1F"){
                        std::cout << "histo name: " << histo_name << std::endl;
                        std::string cleaned_name = histo_name.substr(0,13);
			if(cleaned_name == "event_counter"){
				hnparser->set_histo_name(histo_name);
				std::string cutset = hnparser->cutset();
				std::string dataset = hnparser->dataset();
				TH1F *histo = (TH1F*) file.GetDirectory(directory.c_str())->Get(histo_name.c_str());
				double efficiency = 1./histo->GetBinContent(1);
				effs[dataset][cutset] = efficiency;
			}
                }
        }
}

double eire::SelectionEfficiencyProvider::get_efficiency(std::string ident, std::string cutset)
{
	if(effs.find(ident) == effs.end()){
		std::cout << "ERROR: SelectionEfficiencyProvider::get_efficiency(): ident not found: " << ident << std::endl;
		exit(1);
	}
	if(effs[ident].begin() == effs[ident].end()){
		std::cout << "ERROR: SelectionEfficiencyProvider::get_efficiency(): no efficiencies available for ident: " << ident << std::endl;
		exit(1);
	}

	if(cutset == ""){
		return effs[ident].begin()->second;
	}else{
		if(effs[ident].find(cutset) == effs[ident].end()){
			std::cout << "ERROR: SelectionEfficiencyProvider::get_efficiency(): no efficiencies available for ident/cutset: " << ident << "/" << cutset << std::endl;
			exit(1);
		}

		return effs[ident][cutset];
	}
}
