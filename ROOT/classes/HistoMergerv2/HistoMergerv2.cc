#include "HistoMergerv2.h"

template <class THX>
eire::HistoMergerv2<THX>::HistoMergerv2()
{
	hnparser = new HistoNameParser();
	eff_prov = new eire::SelectionEfficiencyProvider();
	cs_prov = new CrossSectionProvider();
	lumi = -1;
	semi_lep_branching_ratio = -1;

	outfile = new TFile("merge_outfile.root", "RECREATE");
}

template <class THX>
eire::HistoMergerv2<THX>::~HistoMergerv2()
{
	if(hnparser){ delete hnparser; hnparser = NULL; }
	if(cs_prov){ delete cs_prov; cs_prov = NULL; }
	if(eff_prov){ delete eff_prov; eff_prov = NULL; }
	if(outfile){ delete outfile; outfile = NULL; }
}

template <class THX>
void eire::HistoMergerv2<THX>::scale_histo(THX* histo)
{
	std::string histo_name = histo->GetName();
	hnparser->set_histo_name(histo_name);
	std::string dataset = hnparser->dataset();
	std::string cutset = hnparser->cutset();
	double eff = eff_prov->get_efficiency(dataset,cutset);
	double cross_section = cs_prov->get_cross_section(dataset);
	double filter_eff = cs_prov->get_filter_efficiency(dataset);

	double scale_factor = eff * lumi * cross_section;
	histo->Scale(scale_factor);
}

template <class THX>
THX eire::HistoMergerv2<THX>::merge_histos(std::vector<THX*> histos)
{
	outfile->cd();
	
	THX *combined_histo = NULL;
	
	bool first = true;	// first dataset to be processed	
	for(typename std::vector<THX*>::iterator histo = histos.begin();
		histo != histos.end();
		++histo){

		// check if data histo
		bool is_data = false;
		hnparser->set_histo_name(histo->GetName());
		if(hnparser->dataset() == "Data"){ is_data = true; }
	
		if(first && (histo->Integral(0,histo->GetNbinsX()+1) != 0 || histos->size() < 2)){
			if(verbose) std::cout << "Creating new Th?F " << std::endl;
			THX *histo = new THX(*histo);
		}

		histo->Sumw2();
		
		// scale histogram to cross section
		if(histo != NULL && histo->Integral(0,histo->GetNbinsX()+1) != 0 && !is_data){
			scale_histo(histo);
			if(verbose && !is_data) std::cout << " Histos scaled successfully " <<std::endl;
		}else if(is_data){
			if(verbose) std::cout << " Skipped scaling for data hist " << std::endl;
		}
		
		// if first histo that is processed, make this histo the base for the combined histo
		// else just add it to combined histo
		if(verbose) std::cout << "Adding Histo to combined_histo "  << std::endl;
		if(first){
			combined_histo = histo;	
			first = false;
		}else{
			combined_histo->Add(histo);
		}
		if(verbose) std::cout << " Histo added " <<std::endl;
	}			
	
	if(combined_histo == NULL) std::cout << "***ERROR*** returning NULL histo in MergeHistos " << std::endl;

	outfile->cd();

	THX *combined_histo_obj = *combined_histo;
	if(combined_histo){ delete combined_histo; combined_histo = NULL; }

	return *combined_histo_obj;
}
