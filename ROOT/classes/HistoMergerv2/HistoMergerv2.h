#ifndef EIRE_HISTOMERGERV2_H
#define EIRE_HISTOMERGERV2_H

#include "SelectionEfficiencyProvider/SelectionEfficiencyProvider.h"
#include "HistoNameParser/HistoNameParser.h"
#include "BranchingRatio/CrossSectionProvider/CrossSectionProvider.h"

#include "TFile.h"

#include <vector>
#include <map>

namespace eire{
	template <class THX>
	class HistoMergerv2{
		public:
			HistoMergerv2();
			~HistoMergerv2();

			inline void set_lumi(double lumi){ this->lumi = lumi; };
			inline void set_branching_ratio(double br){ this->semi_lep_branching_ratio = br; };
			inline void set_cross_section_set(std::string cs_set){ cs_prov->set_analysis(cs_set); };
			inline void set_histos(std::vector<THX*> histos){ this->histos = histos; };
			THX merge_histos(std::vector<THX*> histos);
		private:
			void scale_histo(THX* histo);

			std::vector<THX*> histos;
			HistoNameParser *hnparser;
			eire::SelectionEfficiencyProvider *eff_prov;
			CrossSectionProvider *cs_prov;

			TFile *outfile;

			double lumi;
			double semi_lep_branching_ratio;

			const static bool verbose = true;
	};
}

#endif
