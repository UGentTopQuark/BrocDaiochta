#ifndef EIRE_HISTOSCALER_H
#define EIRE_HISTOSCALER_H

#include "BranchingRatio/BranchingRatio.h"
#include "CrossSectionProvider/CrossSectionProvider.h"
#include "HistoNameParser/HistoNameParser.h"

namespace eire{
	class HistoScaler{
		public:
			HistoScaler();
			~HistoScaler();

			template <class HISTO>
			void scale_histo(HISTO *histo, std::string input_directory_name, TFile *file);

			void set_integrated_luminosity(double lumi){ this->integrated_luminosity = lumi; };
			void set_cross_section_set(std::string cs_set);
		private:
			double get_cross_section(std::string dataset, std::string event_type, TFile *file);

			// identifier, efficiency
			std::map<std::string, double> efficiencies;

			std::vector<std::string> list_do_not_scale;	// histograms that shall be excluded from scaling with cross section

			HistoNameParser *name_parser;
			BranchingRatio *br_ratio;
			CrossSectionProvider *cs_prov;

			double integrated_luminosity;

			const static bool verbose = false;
	};
}

#endif
