#include "HistoScaler.h"

template void eire::HistoScaler::scale_histo<TH1F>(TH1F *histo, std::string input_directory_name, TFile *file);

eire::HistoScaler::HistoScaler()
{
	name_parser = new HistoNameParser();
	br_ratio = new BranchingRatio();
	cs_prov = new CrossSectionProvider();
}

eire::HistoScaler::~HistoScaler()
{
	if(name_parser){ delete name_parser; name_parser = NULL; }
	if(br_ratio){ delete br_ratio; br_ratio = NULL; }
	if(cs_prov){ delete cs_prov; cs_prov = NULL; }
}

void eire::HistoScaler::set_cross_section_set(std::string cs_set)
{
	cs_prov->set_analysis(cs_set);
	br_ratio->set_analysis(cs_set);
}

template <class HISTO>
void eire::HistoScaler::scale_histo(HISTO *histo, std::string input_directory_name, TFile *file)
{
	std::string histo_name = histo->GetName();
	std::string file_name = file->GetName();
	// parse histo name
	if(verbose) std::cout << " Parsing histo name: " << histo_name << std::endl;

	name_parser->set_histo_name(histo_name);
	std::string dataset = name_parser->dataset();
	std::string cutset = name_parser->cutset();
	std::string event_type = name_parser->event_type();
	std::string short_histo_name = name_parser->histo();

	// if dataset in the list of datasets that will not be scaled, exit this function
	if(find(list_do_not_scale.begin(), list_do_not_scale.end(), dataset) != list_do_not_scale.end()) return;

	if(efficiencies.find(file_name+"_"+event_type+"_"+cutset) == efficiencies.end()){

		// reconstruct event counter name and open event counter histogram
		std::string event_counter_name = "event_counter_"+dataset+"|"+event_type+"|"+cutset;
		if(verbose) std::cout << "opening event counter: " << event_counter_name << std::endl;
		HISTO *eff_histo = (HISTO *) file->GetDirectory(input_directory_name.c_str())->Get(event_counter_name.c_str());
		if(verbose) std::cout << "done." << std::endl;
		
		// calculate efficiency for selection step
		if(eff_histo->GetBinContent(1) != 0)
			efficiencies[file_name+"_"+event_type+"_"+cutset] = 1. / eff_histo->GetBinContent(1);
		else {
			std::cout << "**ERROR** efficieny = 0" << std::endl;
			efficiencies[file_name+"_"+event_type+"_"+cutset] = 0;
		}
	}

        double efficiency = efficiencies[file_name+"_"+event_type+"_"+cutset];
        double histo_entries = histo->GetEntries();
        double scaling_factor = integrated_luminosity*efficiency*histo_entries;

	// get cross section for dataset, in case signal dataset only fraction
	// of actual signal events ( ~15% in semi-lept case)
	// also consider filter efficiency
	double cross_section = get_cross_section(dataset, event_type, file);

	scaling_factor *= cross_section;

        if(cross_section != -1)
        {
		if(verbose) std::cout << "Scaling by 1/integral = 1/ " << histo->Integral() << std::endl;
        	histo->Scale(1.0/histo->GetEntries());//Integral only takes what is within the x-axis of the histo
		if(verbose) std::cout << " Scaling by lumi " << integrated_luminosity << " *(CS*filt_eff) " << cross_section <<  " *eff " << efficiency << " *histo_entries " << histo_entries <<  " = " << scaling_factor << std::endl;
        	histo->Scale(scaling_factor);
        }else{
                std::cout << "NOTE: " << histo_name << " not scaled " << std::endl;
	}
}

double eire::HistoScaler::get_cross_section(std::string dataset, std::string event_type, TFile *file)
{
	double cross_section = -1;
	if(dataset.find("TTbar") != std::string::npos){
		// this is a signal dataset
		// full ttbar cross section
		cross_section = cs_prov->get_cross_section(dataset);

		// now we need the branching ratio
		br_ratio->set_file(file);

        	double e_BR = 0.;
        	double mu_BR = 0.;
		if(br_ratio->signal_file_set()){
        		e_BR = br_ratio->get_branching_ratio_electron();
        		mu_BR = br_ratio->get_branching_ratio_muon();
		}else{
			std::cerr << "WARNING: eire::HistoScaler::set_default_cross_section(): no ttbar signal branching ratio set" << std::endl;
		}
		
		// assign correct branching ratio for event type
		double BR = 0.;
		if(event_type == "electron") BR = e_BR;
		else if(event_type == "e_background") BR = 1.-e_BR;
		else if(event_type == "muon") BR = mu_BR;
		else if(event_type == "mu_background") BR = 1.-mu_BR;
		else std::cerr << "ERROR: eire::HistoScaler::get_cross_section(): unknown event_type: " << event_type << std::endl;

		double filter_efficiency = cs_prov->get_filter_efficiency(dataset+"_"+event_type);

		if(verbose) std::cout << "dataset: " << dataset << " event_type: " << event_type << " branching ratio: " << BR << " cross section " << cross_section << " filter_efficiency: " << filter_efficiency << " BR*CS: " << BR*cross_section << std::endl;

		// cross section only for *signal* events (not ttbar other)
		cross_section *= BR;

		// finally get filter efficiency
		cross_section *= filter_efficiency;
	}else{
	// if dataset is no signal dataset we don't have to bother with the branching ratio
		cross_section = cs_prov->get_cross_section(dataset);	
		// for background samples there is no difference between
		// x_background and x, with x = muon/electron... always
		// background
		cross_section *= cs_prov->get_filter_efficiency(dataset);	
	}

	return cross_section;
}
