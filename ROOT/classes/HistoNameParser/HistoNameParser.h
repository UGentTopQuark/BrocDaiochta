#ifndef HISTONAMEPARSER_H
#define HISTONAMEPARSER_H

#include <string>
#include <iostream>
#include <vector>
#include <boost/regex.hpp>

class HistoNameParser{
	public:
		void set_histo_name(std::string histo_name);
		std::string histo(std::string histo_name="");
		std::string dataset(std::string histo_name="");
		std::string event_type(std::string histo_name="");
		std::string cutset(std::string histo_name="");
		
	private:
		void parse_name(std::string histo_name);
		std::string sdataset;	// string dataset
		std::string shisto;	// string histo
		std::string sevent_type;	// string event_type
		std::string scutset;	// string cutset

		std::string previous_histo_name;	// histo name in previous call

		static const bool verbose = false;
};

#endif
