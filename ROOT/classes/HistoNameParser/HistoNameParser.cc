#include "HistoNameParser.h"

std::string HistoNameParser::histo(std::string histo_name)
{
	parse_name(histo_name);
	return shisto;
}

std::string HistoNameParser::dataset(std::string histo_name)
{
	parse_name(histo_name);
	return sdataset;
}

std::string HistoNameParser::event_type(std::string histo_name)
{
	parse_name(histo_name);
	return sevent_type;
}

std::string HistoNameParser::cutset(std::string histo_name)
{
	parse_name(histo_name);
	return scutset;
}

void HistoNameParser::set_histo_name(std::string histo_name)
{
	parse_name(histo_name);
}

void HistoNameParser::parse_name(std::string histo_name)
{
	// if histo name empty, assume previous histo name
	if(histo_name == "") histo_name = previous_histo_name;
	// if we already parsed this histo name we can you the stored result
	if(histo_name == previous_histo_name) return;

        std::string sre; // regular expression
        sre = "([\\w\\d\\.\\_]+_)([\\w\\d]+)\\|([\\w\\_]+)\\|([\\d\\w\\_]+)";
        boost::regex re;	// boost regular expression object
        boost::cmatch matches;	// vector with the matches for the regex
        re.assign(sre, boost::regex_constants::icase);	// asssign string to regex, make regex case insensitive

	// match regex
        bool matched = boost::regex_match(histo_name.c_str(), matches, re);

	// return error if hist name didn't match
        if(!matched){
                std::cerr << "ERROR: HistNameParser::parse_name(): couldn't parse histo name... invalid formatting? histo_name: " << histo_name << std::endl;
                return;
        }

	// after regex match we have one vector with the different matches,
	// fill those to different strings of class
	for (unsigned int i = 1; i < matches.size(); i++)
	{
		// sub_match::first and sub_match::second are iterators that
		// refer to the first and one past the last chars of the
		// matching subexpression
		std::string match(matches[i].first, matches[i].second);
		if(verbose) std::cout << "\tmatches[" << i << "] = " << match << std::endl;
		switch(i){
			case 1:
				shisto = match;
				break;
			case 2:
				sdataset = match;
				break;
			case 3:
				sevent_type = match;
				break;
			case 4:
				scutset = match;
				break;
			// we should never end up in default, if that happens, something went wrong
			default:
				std::cerr << "ERROR: HistoNameParser::parse_name(): problem with matches vector" << std::endl;
				break;
		};
	}

	// store histo name to make sure regex is not executed several times though not necessary
	previous_histo_name = histo_name;
}
