#!/bin/bash

ver="$1"
where="$2"


cd json2cpp/ ; ./json2cpp.pl $where; cd .. ; make
mv broc_draiochta $ver-flat-Data
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make
mv broc_draiochta $ver-flat-MuQCD_pt20_mupt10
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make
mv broc_draiochta $ver-flat-TsChan
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make
mv broc_draiochta $ver-flat-TtChan
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make
mv broc_draiochta $ver-flat-TtWChan
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make
mv broc_draiochta $ver-flat-Wjets
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make
mv broc_draiochta $ver-flat-DY1050
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make
mv broc_draiochta $ver-flat-DY50
cd json2cpp/ ; ./json2cpp.pl ; cd .. ; make
mv broc_draiochta $ver-flat-TTbar
