#include "../../interface/LHCOWriter/LHCOWriter.h"


broc::LHCOWriter::LHCOWriter(eire::HandleHolder *handle_holder)
{
  set_handles(handle_holder);
  id = "_"+handle_holder->get_ident();
  while(id.find("|") != std::string::npos){
    id.replace(id.find("|"),1,"_");
  }

  eire::ConfigReader *config_reader = handle_holder->get_config_reader();
  std::string output_dir = config_reader->get_var("output_directory");  
  std::string suffix = config_reader->get_var("outfile_suffix");

  btag_algo = "NULL";
  btag_working_point = 0;

  btag_algo = config_reader->get_var("btag_algo", "lhco_writer", false);
  btag_working_point = atof(config_reader->get_var("btag_working_point", "lhco_writer", false).c_str());      

  split_lepton_charges = config_reader->get_bool_var("split_lepton_charges", "lhco_writer", false);

  use_hitfit = config_reader->get_bool_var("enable_kinematic_fit_module", "global", false);
  use_hitfit_combi_seed = config_reader->get_bool_var("use_hitfit_combi_seed", "lhco_writer", false);
  use_hitfit_kinematics = config_reader->get_bool_var("use_hitfit_kinematics", "lhco_writer", false);

  if(!use_hitfit && use_hitfit_combi_seed)
	  std::cerr << "WARNING: You have selected to use the HitFit combination, but HitFit is not enabled!" << std::endl;
  
  if(use_hitfit_kinematics && !use_hitfit_combi_seed){
	  std::cerr << "WARNING: You have selected to use the HitFit kinematics, but not the HitFit Jet Combination Seed!  This is not yet supported.  Disabling use.." << std::endl;
	  use_hitfit_kinematics = false;
  }

  if((btag_algo == "NULL") || (btag_working_point == -1))
	  std::cerr << "WARNING: No btag algo or working point defined in runtime config!" << std::endl;

  if(!split_lepton_charges){
      lhcofile_tot_MC = fopen((output_dir + "/input"+id+suffix+".lhco_gen_level").c_str(),"w");
      if(use_hitfit)
	  lhcofile_tot_hitfit = fopen((output_dir + "/input"+id+suffix + ".lhco_hitfit").c_str(), "w");
      lhcofile_tot = fopen((output_dir + "/input" +id+suffix + ".lhco").c_str(), "w");
  }
  if(split_lepton_charges){
      lhcofile_pos_MC = fopen((output_dir + "/input"+id+suffix+"_pos.lhco_gen_level").c_str(),"w");
      lhcofile_neg_MC = fopen((output_dir + "/input"+id+suffix+"_neg.lhco_gen_level").c_str(),"w");
      if(use_hitfit){
	  lhcofile_pos_hitfit = fopen((output_dir + "/input" +id+suffix + "_pos.lhco_hitfit").c_str(), "w");
	  lhcofile_neg_hitfit = fopen((output_dir + "/input" +id+suffix + "_neg.lhco_hitfit").c_str(), "w");
      }
      lhcofile_pos = fopen((output_dir + "/input" +id+suffix + "_pos.lhco").c_str(), "w");
      lhcofile_neg = fopen((output_dir + "/input" +id+suffix + "_neg.lhco").c_str(), "w");
      
  }

  data_only = true;
  gen_filled = 0;
  reco_filled = 0;
  gen_evt = NULL;  
  reco_evt = NULL;

  first = true;
  phadB = gen_event->hadB(); //particle 2                                                                                                                          
  plepB = gen_event->lepB(); //particle 3                                                                                                                          
  pq = gen_event->q(); //particle 4                                                                                                                                
  pqbar = gen_event->qbar(); //particle 5   
}

broc::LHCOWriter::~LHCOWriter()
{
    delete gen_evt;
    delete reco_evt;
    if(split_lepton_charges){
	fclose(lhcofile_neg_MC);
	fclose(lhcofile_pos_MC);
	if(use_hitfit){
	    fclose(lhcofile_neg_hitfit);
	    fclose(lhcofile_pos_hitfit);
	}
	fclose(lhcofile_neg);
	fclose(lhcofile_pos);
    }else{
	fclose(lhcofile_tot_MC);
	if(use_hitfit)
	    fclose(lhcofile_tot_hitfit);
	fclose(lhcofile_tot);
    }
    first = true;
}

void broc::LHCOWriter::set_handles(eire::HandleHolder *handle_holder)
{
  this->handle_holder = handle_holder;
  this->event_information = handle_holder->get_event_information();
  this->electrons = handle_holder->get_tight_electrons();
  this->muons = handle_holder->get_tight_muons();
  this->mets = handle_holder->get_corrected_mets();
  this->jets = handle_holder->get_tight_jets();
  this->gen_event = handle_holder->get_ttbar_gen_evt();
}

void broc::LHCOWriter::book_branches()
{
    tree->Branch("gen_events",&gen_evt);
    tree->Branch("reco_events",&reco_evt);
}

void broc::LHCOWriter::set_header(bool gen, FILE * file)
{
  fprintf(file,"##############################LHCO event file######################################\n");
   std::string dataset = handle_holder->get_ident();
  fprintf(file,("#Dataset processed: " + dataset + " and four jets"+id+"\n").c_str());
   if(gen){  fprintf(file,"#File based on GENERATOR quantities: \n");}
}

int broc::LHCOWriter::is_matched_event()
{
  int matched = 0;
  bool lepmatched = false;
  double dR = 5.0;
  if(electrons->size() > 0){dR = ROOT::Math::VectorUtil::DeltaR(gen_event->lepton()->p4(), (*electrons)[0].p4());}
  else if(muons->size() > 0){dR = ROOT::Math::VectorUtil::DeltaR(gen_event->lepton()->p4(), (*muons)[0].p4());}
  if(dR < 0.3){lepmatched = true;}
  if(gen_event->hadB_matched() && gen_event->lepB_matched() && gen_event->q_matched() && gen_event->qbar_matched() && lepmatched){matched = 1;}
  if(gen_event->lepton()->charge() == 0){std::cout<<"no gen event!!"<<std::endl; matched = -1;}
  return matched;
}

void broc::LHCOWriter::generator_file()
{

    //check the event information to get the event number 
    data_only = false;                                               
    double event_number = event_information->event_number();
    double run_number = event_information->run();
    double event_weight = handle_holder->get_event_weight();

    mor::Particle *plep = gen_event->lepton();
    mor::Particle *pneutrino = gen_event->neutrino();
    float jmass = 0.;
    double phi = 0.;
    int index = -1;
    


    //Filling leaves of lhco_gen_level
    FILE *lhcofile_all_MC = lhcofile_tot_MC;

    if(split_lepton_charges){
      if((-1)*plep->mc_match_id()/fabs(plep->mc_match_id())<0)
        lhcofile_all_MC = lhcofile_neg_MC;
      else if((-1)*plep->mc_match_id()/fabs(plep->mc_match_id())>0)
        lhcofile_all_MC = lhcofile_pos_MC;
      else return;
    }else{
	lhcofile_all_MC = lhcofile_tot_MC;
    }


    
    fprintf(lhcofile_all_MC,"#\ttype\teta\tphi\tpt\tjmass\tntrk\tbtag\thad/em\tev_weight\thel\n");
    fprintf(lhcofile_all_MC,"0\t%.0f\t%.0f\n",event_number,run_number);

    jmass = phadB->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = phadB->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 4, phadB->eta(), phi, phadB->pt(), jmass, 1, 2, 0, event_weight, 0,1,phadB->p4().E(),"gen");
    jmass = pq->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = pq->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 4, pq->eta(), phi, pq->pt(), jmass, 1, 0, 0, event_weight, 0,3,pq->p4().E(),"gen");
    jmass = pqbar->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = pqbar->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 4, pqbar->eta(), phi, pqbar->pt(), jmass, 1, 0, 0, event_weight, 0,4,pqbar->p4().E(),"gen");
    jmass = plepB->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = plepB->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 4, plepB->eta(), phi, plepB->pt(), jmass, 1, 2, 0, event_weight, 0,2,plepB->p4().E(),"gen");
    if(muons->size() >= 1){
	    jmass = plep->p4().M();
	    if(jmass < 0.){jmass = 0.0;}
	    double mindR = 5.;
	    int index = -1;
	    double dR = ROOT::Math::VectorUtil::DeltaR(phadB->p4(), plep->p4());
	    if(dR < mindR){mindR = dR; index = 1;}
	    dR = ROOT::Math::VectorUtil::DeltaR(plepB->p4(), plep->p4());
	    if(dR < mindR){mindR = dR; index = 2;}
	    dR = ROOT::Math::VectorUtil::DeltaR(pq->p4(), plep->p4());
	    if(dR < mindR){mindR = dR; index = 3;}
	    dR = ROOT::Math::VectorUtil::DeltaR(pqbar->p4(), plep->p4());
	    if(dR < mindR){mindR = dR; index = 4;}
	    phi = plep->phi();
	    if(phi < 0){phi = 2*TMath::Pi() + phi;}
	    //    std::cout<<"generated muon charge= "<<(-1)*plep->mc_match_id()<<std::endl;
	    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 2, plep->eta(), phi, plep->pt(), jmass, (-1)*plep->mc_match_id()/fabs(plep->mc_match_id()), index, 0, event_weight, 0,5,plep->p4().E(),"gen");
    }
    
    if(electrons->size() >= 1){
	    jmass = plep->p4().M();
	    if(jmass < 0.){jmass = 0.0;}
	    phi = plep->phi();
	    if(phi < 0){phi = 2*TMath::Pi() + phi;}
	    //std::cout<<"generated electron charge= "<<(-1)*plep->mc_match_id()<<" pt = "<<plep->pt()<<" phi= "<<phi<<" eta= "<<plep->eta()<<std::endl;
	    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 1, plep->eta(), phi, plep->pt(), jmass, (-1)*plep->mc_match_id()/fabs(plep->mc_match_id()), index, 0, event_weight, 0,5,plep->p4().E(),"gen");
    }
    jmass = pneutrino->p4().M();
    if(jmass < 0.){jmass = 0.0;}
    phi = pneutrino->phi();
    if(phi < 0){phi = 2*TMath::Pi() + phi;}
    fill_leaves(1, lhcofile_all_MC,event_number, run_number, 6, pneutrino->eta(), phi, pneutrino->pt(), jmass, 0, 0, 0, event_weight, 0,6,pneutrino->p4().E(),"gen");
    
    
}

// Default constructor to satisify virtual requirement of the class
void broc::LHCOWriter::fill_branches()
{
}

// Fills the lhco file in the event loop
void broc::LHCOWriter::fill_branches(bool use_hitfit_kinematics)
{

  // Set the headers

  if(first){
      first=false;
      if((!event_information->is_real_data())){
	  if(split_lepton_charges){
	      set_header(true,lhcofile_pos_MC);
	      set_header(true,lhcofile_neg_MC);
	  }else
	      set_header(true,lhcofile_tot_MC);
      }
      if(split_lepton_charges){
	  if(use_hitfit){
	      set_header(false,lhcofile_pos_hitfit);
	      set_header(false,lhcofile_neg_hitfit);
	  }
	  set_header(false,lhcofile_pos);
	  set_header(false,lhcofile_neg);
      }else{
	  if(use_hitfit)
	      set_header(false,lhcofile_tot_hitfit);
	  set_header(false,lhcofile_tot);
      }	    
  }
  
  // Only do the matching cut if HitFit is NOT enabled
  if(!use_hitfit){
	  if((!event_information->is_real_data()) && is_matched_event() == 0){return;}
  }

  // Obtain the hitfit collections inside the event loop (where they are set)

  if (use_hitfit_kinematics){
	  this->hitfit_jets = handle_holder->get_hitfit_jets();
	  this->hitfit_electrons = handle_holder->get_hitfit_electrons();
	  this->hitfit_muons = handle_holder->get_hitfit_muons();
	  this->hitfit_mets = handle_holder->get_hitfit_mets();
  }

  this->hitfit_combi_seed = handle_holder->get_hitfit_combination();
  
  // Get the event information

  double event_number = event_information->event_number();
  double event_weight = handle_holder->get_event_weight();
  double run_number = event_information->run();

  FILE *lhcofile_all;

  int partnum = 0;

  // Fill the MC lhco file with the generator information

  if((!event_information->is_real_data())){generator_file();}

  int lep_charge = 0;

  if(muons->size() >= 1){
	  lep_charge = (*muons)[0].charge();
  }if(electrons->size() >= 1){
	  lep_charge = (*electrons)[0].charge();
  }

  if(split_lepton_charges){
      if(lep_charge>0){
	  if(use_hitfit_kinematics)
	      lhcofile_all = lhcofile_pos_hitfit;
	  else 
	      lhcofile_all = lhcofile_pos;
      }
      else if(lep_charge<0){
	  if(use_hitfit_kinematics)
	      lhcofile_all = lhcofile_neg_hitfit;
	  else 
	      lhcofile_all = lhcofile_neg;
      }
      else return;
  }else{
      if (use_hitfit_kinematics)
	  lhcofile_all = lhcofile_tot_hitfit;
      else 
	  lhcofile_all = lhcofile_tot;
  }
  fprintf(lhcofile_all,"#\ttype\teta\tphi\tpt\tjmass\tntrk\tbtag\thad/em\tev_weight\thel\n");
  fprintf(lhcofile_all,"0\t%.0f\t6\n",event_number);

  partnum++;

  // If seeding with HitFit, push back the jets to a new jet collection in the order specified by HitFit.

  std::vector<mor::Jet> sorted_jets;
  std::vector<mor::Particle> sorted_hitfit_jets;

  // Set the order of the jets corresponding to one of two processes based on the charge of the lepton,
  // in the case where MadWeight is used to run over a jet combination without permtutation

  if(use_hitfit && use_hitfit_combi_seed){
	  if (lep_charge == -1){
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[2]]); // HadB
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[0]]); // LightQ
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[1]]); // LightQBar
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[3]]); // LepB
		  if (use_hitfit_kinematics){
			  sorted_hitfit_jets.push_back((*hitfit_jets)[0]); // HadB
			  sorted_hitfit_jets.push_back((*hitfit_jets)[2]); // LightQ
			  sorted_hitfit_jets.push_back((*hitfit_jets)[3]); // LightQBar
			  sorted_hitfit_jets.push_back((*hitfit_jets)[1]); // LepB
		  }
	  }else if (lep_charge == 1){
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[2]]); // HadB
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[0]]); // LightQ
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[1]]); // LightQBar
		  sorted_jets.push_back((*jets)[hitfit_combi_seed[3]]); // LepB
		  if (use_hitfit_kinematics){
			  sorted_hitfit_jets.push_back((*hitfit_jets)[0]); // HadB
			  sorted_hitfit_jets.push_back((*hitfit_jets)[2]); // LightQ
			  sorted_hitfit_jets.push_back((*hitfit_jets)[3]); // LightQBar
			  sorted_hitfit_jets.push_back((*hitfit_jets)[1]); // LepB
		  }
	  }
  }

  // If multiple leptons exist in the event, take the first object in the collection with the highest pt

  if(jets->size() >= 4){

	  // If seeding with HitFit, write out the jets in the order that will be recognised by MadWeight when permutation is turned off.

	  if(use_hitfit && use_hitfit_combi_seed){
		  for(int i=0; i < 4; i++){

			  mor::Jet jet_i = (sorted_jets)[i];
			  mor::Particle hf_jet_i;

			  if (use_hitfit_kinematics)
				  hf_jet_i = (sorted_hitfit_jets)[i];

			  // Declare kinematic variables without filling them
			 
			  double pt = 0;
			  double eta = 0;
			  double phi = 0;
			  float jmass = 0;
			  float energy = 0;
			  int pbtag = 0;

			  // Fill the kinematic variables with either the fitted values from hitfit, or the default ones

			  if (use_hitfit_kinematics){
				  pt = hf_jet_i.pt();
				  eta = hf_jet_i.eta();
				  phi = hf_jet_i.phi();
				  jmass = hf_jet_i.p4().M();
				  energy = hf_jet_i.p4().E();
			  }else{
				  pt = jet_i.pt();
				  eta = jet_i.eta();
				  phi = jet_i.phi();
				  jmass = jet_i.p4().M();
				  energy = jet_i.p4().E();
			  }

			  if(jet_i.bDiscriminator(btag_algo) >= btag_working_point){pbtag = 2;}
			  if(phi < 0){phi = 2*TMath::Pi() + phi;}

			  fill_leaves(0, lhcofile_all,event_number, run_number, 4, eta, phi, pt, jmass, jet_i.cmulti(), pbtag, 0, event_weight, 0,partnum, energy,"reco");
			  partnum++;
		  }
	  }else{
		  // Default option.  Loop over the jets and write the information in decreasing order of jet pt.  MadWeight will need to permutate over the jet combination.

	    std::cout<<"WARNING!  ordering in LHCO file is not optimised, run with permutations on!"<<std::endl;
		  for(int i=0; i < 4; i++){
			  mor::Jet jet_i = (*jets)[i];

			  double pt = jet_i.pt();
			  double eta = jet_i.eta();
			  double phi = jet_i.phi();
			  float jmass = jet_i.p4().M();
			  float energy = jet_i.p4().E();

			  int pbtag = 0;
			  if(jet_i.bDiscriminator(btag_algo) >= btag_working_point){pbtag = 2;}
			  if(phi < 0){phi = 2*TMath::Pi() + phi;}
			  fill_leaves(0, lhcofile_all,event_number, run_number, 4, eta, phi, pt, jmass, jet_i.cmulti(), pbtag, 0, event_weight, (int)fabs(jet_i.mc_match_id()),partnum, energy,"reco");
			  partnum++;
		  }
	  }
  }

  if (electrons->size() + muons->size() > 1)
	  std::cerr << "ERROR!  Both electrons and muons present in the event!  Please restrict to one lepton only in selection.  Code will seg fault (until solution can be found)." << std::endl;

  // Fill the electrons

  if(electrons->size() >= 1){
	  mor::Electron electron = (*electrons)[0];    

	  double pt;
	  double eta;
	  double phi;
	  float jmass;
	  float energy;

	  // Fill the kinematic variables with either the fitted values from hitfit, or the default ones

	  if (use_hitfit_kinematics){
		  pt = (*hitfit_electrons)[0].pt();
		  eta = (*hitfit_electrons)[0].eta();
		  phi = (*hitfit_electrons)[0].phi();
		  //jmass = (*hitfit_electrons)[0].p4().M();
		  energy = (*hitfit_electrons)[0].p4().E();
	  }else{
		  pt = electron.pt();
		  eta = electron.eta();
		  phi = electron.phi();
		  //jmass = electron.p4().M();
		  energy = electron.p4().E();
	  }

	  jmass = electron.p4().M(); // HitFit sets the jmass for the leptons to be zero.  Get the jmass to the selected object here instead
	  double mindR = 5.;
	  int index = -1;

	  // Set the lepton index as the value pointing to the closest jet in dR.

	  if(use_hitfit_combi_seed){
		  if (electron.charge() == 1)
			  index = 0;
		  else if (electron.charge() == -1)
			  index = 3;
	  }else{
		  for(unsigned int j=0; j < jets->size(); j++){
			  double dR = ROOT::Math::VectorUtil::DeltaR((*jets)[j], electron);
			  if(dR <= mindR){mindR = dR; index = j;}
		  }
	  }	    
	  
	  if(phi < 0){phi = 2*TMath::Pi() + phi;}

	  // Fill the electron leaves, 'index+1' assumes jet-jet-jet-jet-lepton-met ordering (with the jet indicies starting at 0)
	  
	  fill_leaves(0, lhcofile_all, event_number, run_number, 1, eta, phi, pt, jmass, (electron.charge())*1, index+1, 0, event_weight, 0, 5, energy,"reco");
	  partnum++;
  }

  // Fill the muons

  if(muons->size() >= 1){
	  mor::Muon muon = (*muons)[0];

	  double pt;
	  double eta;
	  double phi;
	  float jmass;
	  float energy;

	  // Fill the kinematic variables with either the fitted values from hitfit, or the default ones
	  
	  if (use_hitfit_kinematics){
		  pt = (*hitfit_muons)[0].pt();
		  eta = (*hitfit_muons)[0].eta();
		  phi = (*hitfit_muons)[0].phi();
		  //jmass = (*hitfit_muons)[0].p4().M();
		  energy = (*hitfit_muons)[0].p4().E();
	  }else{
		  pt = muon.pt();
		  eta = muon.eta();
		  phi = muon.phi();
		  //jmass = muon.p4().M();
		  energy = muon.p4().E();
	  }

	  jmass = muon.p4().M(); // HitFit sets the jmass for the leptons to be zero.  Get the jmass to the selected object here instead
	  double mindR = 5.;
	  int index = -1;
	  
	  // Set the lepton index as the value pointing to the closest jet in dR.
	  
	  if(use_hitfit_combi_seed){
		  if (muon.charge() == 1)
			  index = 0;
		  else if (muon.charge() == -1)
			  index = 3;
	  }else{
		  for(unsigned int j=0; j < jets->size(); j++){
			  double dR = ROOT::Math::VectorUtil::DeltaR((*jets)[j], muon);
			  if(dR <= mindR){mindR = dR; index = j;}
		  }
	  }	    

	  if(phi < 0){phi = 2*TMath::Pi() + phi;}
	  
	  // Fill the muon leaves, 'index+1' assumes jet-jet-jet-jet-lepton-met ordering (with the jet indicies starting at 0)
	  
	  fill_leaves(0, lhcofile_all,event_number, run_number, 2, eta, phi, pt, jmass, (muon.charge())*1, index+1, 0, event_weight, 0,5,energy,"reco");
	  partnum++;
  }

  // Fill the mets

  if(mets->size() >= 1){
	  mor::MET met = (*mets)[0];

	  double pt;
	  double eta;
	  double phi;
	  float jmass;
	  float energy;
	  
	  // Fill the kinematic variables with either the fitted values from hitfit, or the default ones

	  if (use_hitfit_kinematics){
		  pt = (*hitfit_mets)[0].pt();
		  eta = (*hitfit_mets)[0].eta();
		  phi = (*hitfit_mets)[0].phi();
		  jmass = (*hitfit_mets)[0].p4().M();
		  energy = (*hitfit_mets)[0].p4().E();
	  }else{
		  pt = met.pt();
		  eta = met.eta();
		  phi = met.phi();
		  jmass = met.p4().M();
		  energy = met.p4().E();
	  }

	
	  if(phi < 0){phi = 2*TMath::Pi() + phi;}
	  
	  fill_leaves(0, lhcofile_all,event_number, run_number, 6, eta, phi, pt, jmass, 1, 0, 0, event_weight, 0, 6, energy,"reco"); // met.charge() (after jmass) has been hard-coded to 1 here for the time being due to the charge information for mets not being fully propagated through the broc
	  partnum++;

  }

}

void broc::LHCOWriter::fill_leaves(int is_generated, FILE * file, double pevent_number, double prun_number, int32_t ptyp, float peta, float pphi, float ppt, float pjmass, float pntrk, int pbtag, float phadOverem, double pevent_weight, int pmc_id, int partnum, float pE, std::string event_type)
{  
    //Method called to fill in the lhco text file as well as the output root file containing reco and eventually both gen_level and smeared gen_level information
    
    //Fill the lhco text file
    fprintf(file,"%i\t%i\t%.3f\t%.3f\t%.2f\t%.2f\t%.1f\t%i\t%.2f\t%.2f\t%d\t\n",partnum,ptyp,peta,pphi,ppt,fabs(pjmass),pntrk,pbtag,phadOverem,pevent_weight,pmc_id);
    
    //use the LHCOEvent class to build the output root tree, regrouping all the information
	beag::MyLHCOEvent::LHCOParticle tmp;
	tmp.typ = ptyp; tmp.eta = peta;
	tmp.phi = pphi; tmp.pt = ppt; tmp.jmass = fabs(pjmass); tmp.ntrk = pntrk; tmp.btag = pbtag;
	tmp.hadOverem = phadOverem; tmp.mc_id = pmc_id; tmp.is_gen = is_generated;
	tmp.E = pE;
	
	if(event_type == "gen"){
	    // Fill gen level root tree leaves
	    if(partnum == 1){
		gen_evt->weight = pevent_weight;
		gen_evt->event_number = pevent_number;
	 		gen_evt->Jet1 = tmp;}
	    if(partnum == 2)
		gen_evt->Jet2 = tmp;
	    if(partnum == 3)
		gen_evt->Jet3 = tmp;
	    if(partnum == 4)
		gen_evt->Jet4 = tmp;
	    if(partnum == 5)
		gen_evt->Lepton = tmp;
	    if(partnum == 6)
		gen_evt->MET = tmp;
	    gen_filled = partnum;
	}
	if(event_type == "reco"){
	    //Fill reco level root tree leaves
	    if(partnum == 1){
		reco_evt->Jet1 = tmp;
		reco_evt->event_number = pevent_number;
		reco_evt->weight = pevent_weight;}
	    if(partnum == 2)
		reco_evt->Jet2 = tmp;
	    if(partnum == 3)
		reco_evt->Jet3 = tmp;
	    if(partnum == 4)
		reco_evt->Jet4 = tmp;
	    if(partnum == 5)
		reco_evt->Lepton = tmp;
	    if(partnum == 6)
		reco_evt->MET = tmp;
	    reco_filled = partnum;
	}
	if(reco_filled == 6 && (gen_filled == 6 || data_only)){
	    //Check that all the leaves are filled in, then fill the tree. Each LHCOEvent gives a branch in the final tree
	    tree->Fill();
	    reco_filled = 0;
	    gen_filled = 0;
	}
}
