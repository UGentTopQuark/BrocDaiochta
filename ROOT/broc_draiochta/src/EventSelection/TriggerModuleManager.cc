#include "../../interface/EventSelection/TriggerModuleManager.h"

int broc::TriggerModuleManager::error_counter_trig = 0;
int broc::TriggerModuleManager::error_counter_mod = 0;

broc::TriggerModuleManager::TriggerModuleManager()
{
  trigger_name_checker = NULL;
}

broc::TriggerModuleManager::~TriggerModuleManager()
{
}

void broc::TriggerModuleManager::initialise(mor::IDChecker *trigger_name_checker, std::vector<std::vector<std::string> > *module_names)
{
	this->trigger_name_checker = trigger_name_checker;
	this->module_names = module_names;
	module_mapping.clear();	

	std::vector<std::string> *trigger_names = trigger_name_checker->get_id_vector();
	for(unsigned int ntrigger=0; ntrigger < trigger_names->size(); ++ntrigger){
		module_mapping.push_back(std::map<std::string, int>());	
		for(unsigned int nmodule = 0; nmodule < (*module_names)[ntrigger].size(); ++nmodule){
			std::string module_name = (*module_names)[ntrigger][nmodule];
			module_mapping[ntrigger][module_name] = nmodule;
		}
	}
}

void broc::TriggerModuleManager::list_modules(std::string trigger_name)
{
	int trigger_idx = trigger_name_checker->get_index(trigger_name);

	std::cout << "--------" << std::endl;
	std::cout << "modules available for trigger: " << trigger_name << std::endl;
	for(std::vector<std::string>::iterator module = (*module_names)[trigger_idx].begin();
		module != (*module_names)[trigger_idx].end();
		++module){
		std::cout<< *module << std::endl;
	}
	std::cout << "--------" << std::endl;
}

std::pair<int,int> broc::TriggerModuleManager::get_module_ids(std::string trigger_name, std::string module)
{
	int trigger_idx = trigger_name_checker->get_index(trigger_name);

	std::pair<int,int> ids;
	ids.first = trigger_idx;

	if(module_mapping.size() < (unsigned int) trigger_idx){
		if(error_counter_mod == 0 || error_counter_mod % 10000 == 0){
			std::cout << "WARNING: broc::TriggerModuleManager::get_module_ids(): trigger name not found " << trigger_name << " idx: " << trigger_idx << " max idx size: " << module_mapping.size() << std::endl;
			++error_counter_mod;
		}
	}else{
		if(module_mapping[trigger_idx].find(module) != module_mapping[trigger_idx].end()){
			ids.second = module_mapping[trigger_idx][module];
		}else if(module == ""){
			ids.second = -1;	
		}else{
			std::cerr << "WARNING: broc::TriggerModuleManager::get_module_ids(): module " << module << " not found for trigger " << trigger_name << std::endl;
			list_modules(trigger_name);
		}
	}

	return ids;
}

int broc::TriggerModuleManager::get_trigger_id(std::string trigger_name)
{
	int id = trigger_name_checker->get_index(trigger_name);
	return id;
}

int broc::TriggerModuleManager::get_last_module_index(std::string trigger_name)
{

	if(trigger_name_checker->get_index(trigger_name) >= 0){

		return ((*module_names)[trigger_name_checker->get_index(trigger_name)].size() - 1);
	}else{

		if(error_counter_trig == 0 || error_counter_trig % 10000 == 0){
			std::cerr << "ERROR: TriggerModuleManager::get_trigger_last_module_index(): invalid trigger " << trigger_name << std::endl;
			++error_counter_trig;
		}

		return -1;
	}

}
