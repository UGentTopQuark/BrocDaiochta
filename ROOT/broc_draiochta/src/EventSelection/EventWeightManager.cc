#include "../../interface/EventSelection/EventWeightManager.h"

broc::EventWeightManager::EventWeightManager(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	std::string id = "_"+handle_holder->get_ident();
	while(id.find("|") != std::string::npos){
	  id.replace(id.find("|"),1,"_");
	}
	eire::ConfigReader *config_reader = handle_holder->get_config_reader();
	std::string output_dir = config_reader->get_var("output_directory");
	std::string suffix = config_reader->get_var("outfile_suffix");
	
	write_out = config_reader->get_bool_var("write_output","event_weights",false);
	if(write_out){
	  output = fopen((output_dir + "/event_weight"+id+suffix + ".txt").c_str(), "w");
	  fprintf(output,"event\tPU\tTop pt\tlepton ID/Iso\tlepton ID/Iso Up\tlepton ID/Iso Down\ttrigger\ttrigger Up\ttrigger Down\tbtag\tbtag Up\tbtag Down\tmistag Up\tmistag Down\n");
	}
	pdf_event_weight = NULL;
	pu_event_weight = NULL;
	lep_abseta_event_weight = NULL;
	lep_eta_event_weight = NULL;
	lep_pt_event_weight = NULL;
	lep_pfreliso_event_weight = NULL;
	lep_eta_pfreliso_2D_event_weight = NULL;
	jet_event_weight = NULL;
	e_trig_event_weight = NULL;
	iso_mu_sf_2012 = 0;
	e_sf_thesis = 0;
	top_pt_weight = NULL;
	e_ID_Iso_2012_event_weight = NULL;
	e_rec_event_weight = NULL;
	m_ID_2012_event_weight = NULL;
	m_Iso_2012_event_weight = NULL;
	btag_mc_sf = 0.;
	btag_systematic_shift = 0;
	btag_event_weight = NULL;
	cross_trig_weight = NULL;

	event = -1;
	pu = 1;
	TopPt = 1;
	lepId = 1;
	lepIdU = 1;
	lepIdD = 1;
	trig = 1;
	trigU = 1;
	trigD = 1;
	btag = 1;
	btagU = 1;
	btagD = 1;
	mtagU = 1;
	mtagD = 1;


	if(config_reader->get_bool_var("top_pt_reweighting","event_weights",false)){
	  top_pt_weight = new broc::TopPtWeightProvider(handle_holder);
	}


	if(config_reader->get_bool_var("pu_weights","event_weights",false)){
		//std::string weights_file = config_reader->get_var("pu_weights_file","event_weights",true);
		std::string weights_file = "";
		pu_event_weight = new broc::PileUpEventWeightProvider(weights_file, handle_holder);
	}
	pdf_event_weight = new broc::PDFEventWeight(cuts_set, handle_holder);
	if(config_reader->get_bool_var("lep_eff_weights","event_weights",false)){
		std::string abseta_weights_file = config_reader->get_var("abseta_weights_file","event_weights",false);
		std::string eta_weights_file = config_reader->get_var("eta_weights_file","event_weights",false);
		std::string pt_weights_file = config_reader->get_var("pt_weights_file","event_weights",false);
		std::string pfreliso_weights_file = config_reader->get_var("pfreliso_weights_file","event_weights",false);
		std::string eta_pfreliso_2D_weights_file = config_reader->get_var("eta_pfreliso_2D_weights_file","event_weights",false);
		if(abseta_weights_file == "" && eta_weights_file == "" && pfreliso_weights_file == "" && eta_pfreliso_2D_weights_file == "" && pt_weights_file == ""){
			std::cerr << "ERROR:EventWeightManager: lep_eff_weights is true in config. Must provide a weights file.";
			std::cerr << " Define abseta_weights_file, eta_weights_file,eta_pfreliso_2D_weights_file or pfreliso_weights_file. " << std::endl;
			exit(1);
		}else if(abseta_weights_file != "" && eta_weights_file != ""){
			std::cerr << "ERROR:EventWeightManager: Cannot reweight by abseta and eta. Disable one weights file in config.";
			exit(1);
		}
		if(abseta_weights_file != ""){lep_abseta_event_weight = new broc::LeptonAbsEtaWeightProvider(abseta_weights_file,handle_holder);}
		if(eta_weights_file != ""){lep_eta_event_weight = new broc::LeptonEtaWeightProvider(eta_weights_file,handle_holder);}
		if(pt_weights_file != ""){lep_pt_event_weight = new broc::LeptonPtWeightProvider(pt_weights_file,handle_holder);}
		if(pfreliso_weights_file != ""){lep_pfreliso_event_weight = new broc::LeptonPFrelIsoWeightProvider(pfreliso_weights_file,handle_holder);}
		if(eta_pfreliso_2D_weights_file != ""){
		  lep_eta_pfreliso_2D_event_weight = new broc::LeptonEtaPFrelIso2DWeightProvider(eta_pfreliso_2D_weights_file,handle_holder);}
	}
	jet_2012_eff_weights = config_reader->get_bool_var("jet_2012_eff_weights","event_weights",false);
	if(config_reader->get_bool_var("jet_eff_weights","event_weights",false) ||
	   jet_2012_eff_weights){
	  jet_event_weight = new broc::JetEffWeightProvider(handle_holder);
	}
	if(config_reader->get_bool_var("e_trigger_eff_weights","event_weights",false)){
		e_trig_event_weight = new broc::ETriggerEffWeightProvider(handle_holder);
	}
	if(config_reader->get_bool_var("e_ID_ISO_2012_eff_weights","event_weights",false)){
		e_ID_Iso_2012_event_weight = new broc::ElectronID_Iso_2012EffWeightProvider(handle_holder);
		e_rec_event_weight = new broc::ElectronRecEffWeightProvider(handle_holder);
	}
	if(config_reader->get_bool_var("m_ID_2012_eff_weights","event_weights",false)){
		m_ID_2012_event_weight = new broc::MuonID_2012EffWeightProvider(handle_holder);
	}
	if(config_reader->get_bool_var("m_ISO_2012_eff_weights","event_weights",false)){
		m_Iso_2012_event_weight = new broc::MuonIso_2012EffWeightProvider(handle_holder);
	}
	if(config_reader->get_bool_var("IsoMu24SF_2012","event_weights",false)){
		iso_mu_sf_2012 = new broc::IsoMuSFProvider2012(handle_holder);
	}
	if(config_reader->get_bool_var("ElectronSF_thesis","event_weights",false)){
		e_sf_thesis = new broc::ElectronSFProviderThesis(handle_holder);
	}
	if(config_reader->get_bool_var("btag_weight","event_weights",false)){
	  btag_event_weight = new broc::BTagEfficiencyWeightProvider(handle_holder);
	}
	cross_trig_weight = new broc::CrossTriggerWeightProvider(handle_holder);
}

broc::EventWeightManager::~EventWeightManager()
{
	if(pdf_event_weight){delete pdf_event_weight; pdf_event_weight=NULL;}
	if(pu_event_weight){delete pu_event_weight; pu_event_weight=NULL;}
	if(lep_abseta_event_weight){delete lep_abseta_event_weight; lep_abseta_event_weight=NULL;}
	if(lep_eta_event_weight){delete lep_eta_event_weight; lep_eta_event_weight=NULL;}
	if(lep_pt_event_weight){delete lep_pt_event_weight; lep_pt_event_weight=NULL;}
	if(lep_pfreliso_event_weight){delete lep_pfreliso_event_weight; lep_pfreliso_event_weight=NULL;}
	if(lep_eta_pfreliso_2D_event_weight){delete lep_eta_pfreliso_2D_event_weight; lep_eta_pfreliso_2D_event_weight=NULL;}
	if(jet_event_weight){ delete jet_event_weight; jet_event_weight = NULL; }
	if(e_trig_event_weight){ delete e_trig_event_weight; e_trig_event_weight = NULL; }
	if(iso_mu_sf_2012){ delete iso_mu_sf_2012; iso_mu_sf_2012 = 0; }
	if(e_sf_thesis){ delete e_sf_thesis; e_sf_thesis = 0; }
	if(top_pt_weight){delete top_pt_weight; top_pt_weight = NULL;}
	if(e_ID_Iso_2012_event_weight){delete e_ID_Iso_2012_event_weight; e_ID_Iso_2012_event_weight = NULL;}
	if(e_rec_event_weight){delete e_rec_event_weight; e_rec_event_weight = NULL;}
	if(m_ID_2012_event_weight){delete m_ID_2012_event_weight; m_ID_2012_event_weight = NULL;}
	if(m_Iso_2012_event_weight){delete m_Iso_2012_event_weight; m_Iso_2012_event_weight = NULL;}
	if(btag_event_weight){delete btag_event_weight; btag_event_weight = NULL;}
	if(cross_trig_weight){delete cross_trig_weight; cross_trig_weight = NULL;}
	if(write_out){
	fclose(output);
	}
	write_out = false;
}

double broc::EventWeightManager::get_all_events_weight()
{
  event = -1;
  pu = 1;
  event = handle_holder->get_event_information()->event_number();
  double weight = 1.;
  // MC generator event weights
  weight *= handle_holder->get_event_information()->event_weight();
  
  // fix branching ratio to NLO in ttbar events //No longer necessary in 2012 MC
  //weight *= get_W_BR_correction_weight();
  //weight *= get_W_scale_correction_weight();
  weight *= pdf_event_weight->get_weight();
  
  //	weight *= bquark_mass_correction_weight();
  
  if(pu_event_weight){ weight *= pu_event_weight->get_weight();
    pu = pu_event_weight->get_weight();
  }
  if(!handle_holder->get_event_information()->is_real_data() && top_pt_weight)
    {
      //top pt reweighting on TTbar MC
      weight *= top_pt_weight->get_value_for_event();
      TopPt = top_pt_weight->get_value_for_event();
    }
  
  handle_holder->set_event_weight(weight);
  handle_holder->set_failing_event_weight(weight);
  
  return weight;
}

double broc::EventWeightManager::get_selected_events_weight()
{
	TopPt = 1;
	lepId = 1;
	lepIdU = 1;
	lepIdD = 1;
	trig = 1;
	trigU = 1;
	trigD = 1;
	btag = 1;
	btagU = 1;
	btagD = 1;
	mtagU = 1;
	mtagD = 1;	

	double weight = handle_holder->get_event_weight();
	bool has_btag_cut = handle_holder->get_btag_cut_state();
	if(lep_abseta_event_weight) weight *= lep_abseta_event_weight->get_weight();
	if(lep_eta_event_weight) weight *= lep_eta_event_weight->get_weight();
	if(lep_pt_event_weight) weight *= lep_pt_event_weight->get_weight();
	if(lep_pfreliso_event_weight) weight *= lep_pfreliso_event_weight->get_weight();
	if(lep_eta_pfreliso_2D_event_weight) weight *= lep_eta_pfreliso_2D_event_weight->get_weight();
	if(jet_event_weight && !jet_2012_eff_weights) 
		weight *= jet_event_weight->get_weight();

	if(jet_event_weight && jet_2012_eff_weights)
		weight *= jet_event_weight->get_2012_weight();

	if(e_trig_event_weight){ weight *= e_trig_event_weight->get_weight(); trig = e_trig_event_weight->get_weight(); trigU = e_trig_event_weight->get_weight(1); trigD = e_trig_event_weight->get_weight(-1);}
	if(e_ID_Iso_2012_event_weight){ weight *= e_ID_Iso_2012_event_weight->get_weight(); lepId = e_ID_Iso_2012_event_weight->get_weight(); lepIdU = e_ID_Iso_2012_event_weight->get_weight(1); lepIdD = e_ID_Iso_2012_event_weight->get_weight(-1);}
	if(e_rec_event_weight){ weight *= e_rec_event_weight->get_weight(); lepId *= e_rec_event_weight->get_weight(); lepIdU *= e_rec_event_weight->get_weight(1); lepIdD *= e_rec_event_weight->get_weight(-1);}
	if(m_ID_2012_event_weight){ weight *= m_ID_2012_event_weight->get_weight(); lepId = m_ID_2012_event_weight->get_weight(); lepIdU = m_ID_2012_event_weight->get_weight(1); lepIdD = m_ID_2012_event_weight->get_weight(-1);}
	if(m_Iso_2012_event_weight){ weight *= m_Iso_2012_event_weight->get_weight(); lepId *= m_Iso_2012_event_weight->get_weight(); lepIdU *= m_Iso_2012_event_weight->get_weight(1); lepIdD *= m_Iso_2012_event_weight->get_weight(-1);}

	if(iso_mu_sf_2012){// weight *= cross_trig_weight->get_weight(pu); trig = cross_trig_weight->get_weight(pu); trigU = trig; trigD = trig;}
	  weight *= iso_mu_sf_2012->get_scale_factor_IsoMu24(); trig = iso_mu_sf_2012->get_scale_factor_IsoMu24(); trigU = iso_mu_sf_2012->get_scale_factor_IsoMu24(1); trigD = iso_mu_sf_2012->get_scale_factor_IsoMu24(-1);}
	if(e_sf_thesis) weight *= e_sf_thesis->get_scale_factor();
	
	if(btag_event_weight && has_btag_cut){ weight *= btag_event_weight->get_weight(); btag = btag_event_weight->get_weight(); btagU = btag_event_weight->get_weight(1,0); btagD = btag_event_weight->get_weight(-1,0); mtagU = btag_event_weight->get_weight(0,1); mtagD = btag_event_weight->get_weight(0,-1);}
	
	//jl 31.05.12: automate btagging MC SF
	btag_mc_sf             = handle_holder->get_cuts_set()->get_cut_value("btag_mc_sf");       
	btag_systematic_shift  = handle_holder->get_cuts_set()->get_cut_value("btag_systematic_shift");

	if(!handle_holder->get_event_information()->is_real_data())
	{

	  //	  if(btag_mc_sf != -1)
	  //{
	  //  if(btag_systematic_shift == 1)
	  //    weight *= (btag_mc_sf - 0.06);
	  //  else if(btag_systematic_shift == 2)
	  //    weight *= (btag_mc_sf + 0.06);
	  //  else
	  //    weight *= btag_mc_sf;
	  //}
	  
		//Nothing to do with b-tagging. hijacking is_real_data check.
	  //		double selected_event_sf = handle_holder->get_cuts_set()->get_cut_value("selected_event_sf"); 
	  //if(selected_event_sf != -1){ weight *= selected_event_sf;}
	}

	handle_holder->set_event_weight(weight);
	//	handle_holder->set_failing_event_weight(weight);
	


	return weight;
}

void broc::EventWeightManager::print()
{
  if(write_out){
  fprintf(output,"%i\t%0.4f\t%0.4f\t%0.4f\t%0.4f\t%0.4f\t%0.4f\t%0.4f\t%0.4f\t%0.4f\t%0.4f\t%0.4f\t%0.4f\t%0.4f\n",event,pu,TopPt,lepId,lepIdU,lepIdD,trig,trigU,trigD,btag,btagU,btagD,mtagU,mtagD);
  }
}

double broc::EventWeightManager::get_W_scale_correction_weight()
{
	double decay_mode = handle_holder->get_event_information()->W_decay_mode();
	if(decay_mode == 0){
		return 1.;
	}
	else{
		// https://hypernews.cern.ch/HyperNews/CMS/get/top-crosssection/60/1/1/1/1/1/1/1/1.html
		if(decay_mode == 1){
			return 3;
		}else if(decay_mode == 2){
			return 0.75;
		}else if(decay_mode == 3){
			return 0.75;
		}else{
			return 1.;
		}
	}
}

// in the MC production of top events the wrong W branching ratio is assumed,
// cf.
// https://twiki.cern.ch/twiki/bin/viewauth/CMS/TopLeptonPlusJets2010Systematics
// "W Branching Fraction in some madgraph samples"
double broc::EventWeightManager::get_W_BR_correction_weight()
{
	if(handle_holder->get_ttbar_gen_evt() && handle_holder->get_ttbar_gen_evt()->decay_channel() != 0){
		double ttbar_decay = handle_holder->get_ttbar_gen_evt()->decay_channel();
		if(ttbar_decay == 1 ||	// semi lep e
		   ttbar_decay == 2 ||	// semi lep mu
		   ttbar_decay == 6){	// semi lep tau
			return (0.108*9)*(0.676*1.5);
		}else if(ttbar_decay == 4){
			return (0.108*9)*(0.108*9);
		}else if(ttbar_decay == 5){
			return (0.676*1.5)*(0.676*1.5);
		}else{
			std::cerr << "ERROR: broc::EventWeightManager::get_W_BR_correction_weight(): invalid decay mode: " << ttbar_decay << std::endl;
			std::cerr << "Exiting..." << std::endl;
			exit(1);
		}
	}

	return 1.;
}

double broc::EventWeightManager::bquark_mass_correction_weight()
{
	if(handle_holder->get_ident().find("Data") != std::string::npos || 
	   handle_holder->get_event_information()->is_real_data()){
		return 1.;
	}else if(handle_holder->get_ttbar_gen_evt()->lepB()->mass() < 4.8 || 
		 handle_holder->get_ttbar_gen_evt()->hadB()->mass() < 4.8){
		return 0.;
	}else{
		return 1.;
	}

}
