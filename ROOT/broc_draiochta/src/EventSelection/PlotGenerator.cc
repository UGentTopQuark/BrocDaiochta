#include "../../interface/EventSelection/PlotGenerator.h" 

broc::PlotGenerator::PlotGenerator(eire::HandleHolder *handle_holder)
{
	id = "_"+handle_holder->get_ident();
	this->config_reader = handle_holder->get_config_reader();
	this->handle_holder = handle_holder;

	plot_met = config_reader->get_bool_var("plot_met", "plots", false);
	plot_mass = config_reader->get_bool_var("plot_mass", "plots", false);
	plot_kinematics = config_reader->get_bool_var("plot_kinematics", "plots", false);
	plot_trigger = config_reader->get_bool_var("plot_trigger", "plots", false);
	plot_lept_iso = config_reader->get_bool_var("plot_lept_iso", "plots", false);
	plot_btag = config_reader->get_bool_var("plot_btag", "plots", false);
	plot_ptrel_btag = config_reader->get_bool_var("plot_ptrel_btag", "plots", false);
	plot_abcd = config_reader->get_bool_var("plot_abcd", "plots", false);
	plot_xplusnjets = config_reader->get_bool_var("plot_xplusnjets", "plots", false);
	plot_mm = config_reader->get_bool_var("plot_mm", "plots", false);
	plot_systematics = config_reader->get_bool_var("plot_systematics", "plots", false);
	plot_spin = config_reader->get_bool_var("plot_spin","plots",false);
	plot_gen_evt_info = config_reader->get_bool_var("plot_gen_evt_info","plots",false);
	plot_add_kin = config_reader->get_bool_var("plot_additional_kinematics","plots",false);
	plot_PE = config_reader->get_bool_var("plot_PE","plots",false);

	mass_reco = NULL;	// mass reco from cuts class used

	bjet_finder = new BJetFinder();
	bjet_finder->set_handles(handle_holder);
	mass_jet_match = new MassJetMatch();
	mass_jet_match->set_mass_reco(handle_holder->services()->mass_reco());

	cuts_set = NULL;

	/*
	 *	book plot classes
	 */
	met_plots = NULL;
	if(plot_met){
		met_plots = new METPlots(id);
		met_plots->set_handles(handle_holder);
		met_plots->set_met_handle(handle_holder->get_mets());
	}

	add_kin_plots = NULL;
	if(plot_add_kin){ add_kin_plots = new AdditionalKinematicPlots(handle_holder); }

	PE_plots = 0;
	if(plot_PE){ PE_plots = new broc::PEPlots(handle_holder); }

	spin_plots = NULL;
	if(plot_spin){
	  spin_plots = new SpinPlots(id, plot_gen_evt_info);
	  spin_plots->set_handles(handle_holder);
	  //	  spin_plots->set_met_handle(handle_holder->get_mets());
        }

	abcd_plots = NULL;
	if(plot_abcd){
		abcd_plots = new ABCDMethodPlots(id);
		abcd_plots->set_handles(handle_holder);
	}

	trigger_plots = NULL;
	if(plot_trigger){
		trigger_plots = new TriggerPlots(id);
		trigger_plots->set_muons(handle_holder->get_muons());
		trigger_plots->set_trigger(handle_holder->get_trigger());
		trigger_plots->set_handles(handle_holder);
	}

	lept_iso_plots = NULL;
	if(plot_lept_iso){
		lept_iso_plots = new LeptonIsolationPlots(id);
		lept_iso_plots->set_handles(handle_holder);
	}
	xplusnjets_plots = NULL;
	if(plot_xplusnjets){
		xplusnjets_plots = new XPlusNJetsPlots(id);
		xplusnjets_plots->set_handles(handle_holder);
	}

	systematic_plots = NULL;
	if(plot_systematics){
		systematic_plots = new SystematicPlots(id);
		systematic_plots->set_handles(handle_holder);
	}
	btag_plots = NULL;
	if(plot_btag){		
		btag_plots = new BTagPlots(id);
		btag_plots->set_handles(handle_holder);
		btag_plots->set_bjet_finder(bjet_finder);
	}
	btag_ptrel_plots = NULL;
	if(plot_ptrel_btag){
		btag_ptrel_plots = new BTagPTRelPlots(id);
		btag_ptrel_plots->set_handles(handle_holder);
	}	

	mass_plots = NULL;
	if(plot_mass){
		mass_plots = new MassPlots(handle_holder);
		mass_plots->set_handles(handle_holder);
		mass_plots->set_bjet_finder(bjet_finder);
		mass_plots->set_mass_jet_match(mass_jet_match);
		mass_plots->set_ht_calc(handle_holder->services()->ht_calc());
		mass_plots->set_mass_reco(handle_holder->services()->mass_reco());
	}

	basic_plots = NULL;
	if(plot_kinematics){
		basic_plots = new BasicObjectPlots(handle_holder);
		//basic_plots->set_handles(handle_holder); Set in constructor
		basic_plots->set_ht_calc(handle_holder->services()->ht_calc());
		basic_plots->set_uncut_jets(handle_holder->get_jets());
		basic_plots->set_loose_muons(handle_holder->get_loose_muons());
		basic_plots->set_uncorrected_mets(handle_holder->get_mets());
		basic_plots->set_pvert(handle_holder->get_tight_primary_vertices());
	}

	mm_plots = NULL;	
	if(plot_mm){
		mm_plots = new QCDMMPlots(handle_holder,true); //jl 10.06.11: true to book histos
		mm_plots->set_ht_calc(handle_holder->services()->ht_calc());
		mm_plots->set_mass_reco(handle_holder->services()->mass_reco());
	}

        my_mets = NULL; //jl 20.04.11: make sure we use measured met everywhere if we choose to
	do_met_recalc = false;
        do_met_recalc = config_reader->get_bool_var("recompute_met","global", true);
}

broc::PlotGenerator::~PlotGenerator()
{
	if(trigger_plots){ delete trigger_plots; trigger_plots = NULL; }
	if(btag_plots){ delete btag_plots; btag_plots = NULL; }
	if(btag_ptrel_plots){ delete btag_ptrel_plots; btag_ptrel_plots = NULL; }
	if(basic_plots){ delete basic_plots; basic_plots = NULL; }
	if(mass_plots){ delete mass_plots; mass_plots = NULL; }
	if(lept_iso_plots){ delete lept_iso_plots; lept_iso_plots = NULL; }
	if(met_plots){ delete met_plots; met_plots = NULL; }
	if(add_kin_plots){ delete add_kin_plots; add_kin_plots = NULL; }
	if(abcd_plots){ delete abcd_plots; abcd_plots = NULL; }
	if(xplusnjets_plots){ delete xplusnjets_plots; xplusnjets_plots = NULL; }
	if(systematic_plots){ delete systematic_plots; systematic_plots = NULL; }
	if(mass_jet_match){ delete mass_jet_match; mass_jet_match = NULL; }
	if(bjet_finder){ delete bjet_finder; bjet_finder = NULL; }
	if(mm_plots){ delete mm_plots; mm_plots= NULL; }
	if(spin_plots){ delete spin_plots; spin_plots = NULL; }
	if(PE_plots){ delete PE_plots; PE_plots = 0; }
	if(add_kin_plots){ delete add_kin_plots; add_kin_plots = 0; }
}

void broc::PlotGenerator::plot()
{

	/*
	 *	plot individual plot classes
	 */
	if(plot_spin) spin_plots->plot();
	if(plot_met) met_plots->plot();
	if(plot_add_kin) add_kin_plots->plot();
	if(plot_PE) PE_plots->plot();
	if(plot_abcd) abcd_plots->plot();
	if(plot_xplusnjets) xplusnjets_plots->plot();
	if(plot_systematics) systematic_plots->plot();
	if(plot_trigger) trigger_plots->plot();
	if(plot_lept_iso) lept_iso_plots->plot();
	if(plot_btag) btag_plots->plot();
	if(plot_ptrel_btag) btag_ptrel_plots->plot();
	if(plot_mass) mass_plots->plot();
	if(plot_kinematics) basic_plots->plot();
	if(plot_mm) //jl 15.07.10
	{
		mm_plots->ComputeMMWeights();
		mm_plots->plot();
	}
}
