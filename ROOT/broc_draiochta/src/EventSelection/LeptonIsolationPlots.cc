#include "../../interface/EventSelection/LeptonIsolationPlots.h"

LeptonIsolationPlots::LeptonIsolationPlots(std::string ident)
{
	id = ident;

	gen_evt = NULL;
}

LeptonIsolationPlots::~LeptonIsolationPlots()
{
}

void LeptonIsolationPlots::set_gen_evt(mor::TTbarGenEvent *gen_evt)
{
	this->gen_evt = gen_evt;
}

void LeptonIsolationPlots::plot_all()
{
	plotiso_plotveto();
	plotPFiso_conesize();
}

void LeptonIsolationPlots::book_histos()
{
	//isolation plots
	histos1d[("mu_PFrelIso"+id).c_str()]=histo_writer->create_1d(("mu_PFrelIso"+id).c_str(),"Muon PFrelIso",500,0,5.0, "PFrelIso");
	histos1d[("mu_dbeta_PFrelIso"+id).c_str()]=histo_writer->create_1d(("mu_dbeta_PFrelIso"+id).c_str(),"Muon PFrelIso with #Delta #beta corrections",500,0,5.0, "Muon relative isolation");
	histos1d[("e_PFrelIso"+id).c_str()]=histo_writer->create_1d(("e_PFrelIso"+id).c_str(),"Electron PFrelIso",225,0,1.5, "Electron Particle Flow relative isolation");
	histos1d[("e_EA_PFrelIso"+id).c_str()]=histo_writer->create_1d(("e_EA_PFrelIso"+id).c_str(),"Electron PFrelIso",225,0,1.5, "pile-up corrected Electron Particle Flow relative isolation");
	histos1d[("mu_trackiso"+id).c_str()]=histo_writer->create_1d(("mu_trackiso"+id).c_str(),"Muon TrackIso",50,0,10, "TrackIso (GeV)");
	histos1d[("mu_hcaliso"+id).c_str()]=histo_writer->create_1d(("mu_hcaliso"+id).c_str(),"Muon Hcal Iso",50,0,10, "HCAL Iso (GeV)");
	histos1d[("mu_ecaliso"+id).c_str()]=histo_writer->create_1d(("mu_ecaliso"+id).c_str(),"Muon Ecal Iso",50,0,10, "ECAL Iso (GeV)");
	histos1d[("mu_caloiso"+id).c_str()]=histo_writer->create_1d(("mu_caloiso"+id).c_str(),"Muon CaloIso",50,0,10, "CaloIso (GeV)");
	histos1d[("mu_sumiso"+id).c_str()]=histo_writer->create_1d(("mu_sumiso"+id).c_str(),"Muon Ecal+Hcal+trackIso",100,0,20, "SumIso (GeV)");
	histos1d[("mu_CombRelIso"+id).c_str()]=histo_writer->create_1d(("mu_CombRelIso"+id).c_str(),"Muon CombRelIso",50,0,0.2, "comb.rel.Iso.");
	histos1d[("e_trackiso"+id).c_str()]=histo_writer->create_1d(("e_trackiso"+id).c_str(),"Electron Track Iso",50,0,10, "TrackIso (GeV)");
	histos1d[("e_hcaliso"+id).c_str()]=histo_writer->create_1d(("e_hcaliso"+id).c_str(),"Electron Hcal Iso",50,0,10, "HCAL Iso (GeV)");
	histos1d[("e_ecaliso"+id).c_str()]=histo_writer->create_1d(("e_ecaliso"+id).c_str(),"Electron Ecal Iso",50,0,10, "ECAL Iso (GeV)");
	histos1d[("e_caloiso"+id).c_str()]=histo_writer->create_1d(("e_caloiso"+id).c_str(),"Electron CaloIso",50,0,10, "CaloIso (GeV)");
	histos1d[("e_sumiso"+id).c_str()]=histo_writer->create_1d(("e_sumiso"+id).c_str(),"Electron SumIso",100,0,20, "SumIso (GeV)");
	histos1d[("e_CombRelIso"+id).c_str()]=histo_writer->create_1d(("e_CombRelIso"+id).c_str(),"Electron CombRelIso",50,0,0.2, "comb.rel.Iso.");
	//vetocone plots
	histos1d[("mu_veto_hcalEnergy"+id).c_str()]=histo_writer->create_1d(("mu_veto_hcalEnergy"+id).c_str(),"muon vetocone(size 0.1) hcalEnergy",50,0,100, "HCAL VetoCone (GeV)");
	histos1d[("mu_veto_ecalEnergy"+id).c_str()]=histo_writer->create_1d(("mu_veto_ecalEnergy"+id).c_str(),"muon vetocone(size 0.07) ecalEnergy",50,0,100, "ECAL VetoCone (GeV)");
	histos1d[("e_veto_ecalEnergy"+id).c_str()]=histo_writer->create_1d(("e_veto_ecalEnergy"+id).c_str(),"electron vetocone(size 0.07) ecalEnergy",50,0,100, "ECAL VetoCone (GeV)");
	histos1d[("e_veto_hcalEnergy"+id).c_str()]=histo_writer->create_1d(("e_veto_hcalEnergy"+id).c_str(),"electron vetocone(size(0.1) hcalEnergy",50,0,100, "HCAL VetoCone (GeV)");
	
	histos2d[("e_caloiso_vs_trackiso"+id).c_str()]=histo_writer->create_2d(("e_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60, "TrackIso (GeV)", "CaloIso [GeV");
	histos2d[("mu_caloiso_vs_trackiso"+id).c_str()]=histo_writer->create_2d(("mu_caloiso_vs_trackiso"+id).c_str(),"match calo iso vs trackiso",60,0,60,60,0,60, "TrackIso (GeV)", "CaloIso [GeV");
	histos2d[("e_veto_hcal_vs_veto_ecal"+id).c_str()]=histo_writer->create_2d(("e_veto_hcal_vs_veto_ecal"+id).c_str(),"electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100, "ECAL VetoCone (GeV)", "HCAL VetoCone (GeV)");
	histos2d[("mu_veto_hcal_vs_veto_ecal"+id).c_str()]=histo_writer->create_2d(("mu_veto_hcal_vs_veto_ecal"+id).c_str(),"electron vetocone hcal vs vetocone ecalEnergy",100,0,100,100,0,100, "ECAL VetoCone (GeV)", "HCAL VetoCone (GeV)");

	histos2d[("e_pfreliso_vs_conesize"+id).c_str()]=histo_writer->create_2d(("e_pfreliso_vs_conesize"+id).c_str(),"electron pfreliso vs conesize",12,0.05,0.65, 100,0,1, "Cone size","Pf relIso");
	histos2d[("mu_pfreliso_vs_conesize"+id).c_str()]=histo_writer->create_2d(("mu_pfreliso_vs_conesize"+id).c_str(),"muon pfreliso vs conesize",12,0.05,0.65, 100,0,1, "Cone size","Pf relIso");

	std::vector<std::string> conesizes;
	conesizes.push_back("0.05");
	conesizes.push_back("0.1");
	conesizes.push_back("0.15");
	conesizes.push_back("0.2");
	conesizes.push_back("0.25");
	conesizes.push_back("0.3");
	conesizes.push_back("0.35");
	conesizes.push_back("0.4");
	conesizes.push_back("0.45");
	conesizes.push_back("0.5");
	conesizes.push_back("0.55");
	conesizes.push_back("0.6");

	for(std::vector<std::string>::iterator cs = conesizes.begin();
	    cs != conesizes.end();cs++){
		histos1d[("mu_PFrelIso"+*cs+id).c_str()]=histo_writer->create_1d(("mu_PFrelIso"+*cs+id).c_str(),"Muon PFrelIso",225,0,1.5, "PFrelIso");
		histos1d[("e_PFrelIso"+*cs+id).c_str()]=histo_writer->create_1d(("e_PFrelIso"+*cs+id).c_str(),"Electron PFrelIso",225,0,1.5, "PFrelIso");
	}

}
void LeptonIsolationPlots::plotPFiso_conesize()
{
	std::vector<std::string> conesizes;
	conesizes.push_back("0.05");
	conesizes.push_back("0.1");
	conesizes.push_back("0.15");
	conesizes.push_back("0.2");
	conesizes.push_back("0.25");
	conesizes.push_back("0.3");
	conesizes.push_back("0.35");
	conesizes.push_back("0.4");
	conesizes.push_back("0.45");
	conesizes.push_back("0.5");
	conesizes.push_back("0.55");
	conesizes.push_back("0.6");


	for(std::vector<mor::Muon>::iterator mu = isolated_muons->begin();
	    mu != isolated_muons->end();
	    ++mu){

		for(std::vector<std::string>::iterator cs = conesizes.begin();
		    cs != conesizes.end();cs++){ 

			double reliso = (mu->chargedHadronIso(*cs) +mu->neutralHadronIso(*cs) +mu->photonIso(*cs))/mu->Pt();
			histos2d[("mu_pfreliso_vs_conesize"+id).c_str()]->Fill(atof(cs->c_str()),reliso);
			histos1d[("mu_PFrelIso"+*cs+id).c_str()]->Fill(reliso);
		}
	}


	for(std::vector<mor::Electron>::iterator e = isolated_electrons->begin();
	    e != isolated_electrons->end();
	    ++e){

		for(std::vector<std::string>::iterator cs = conesizes.begin();
		    cs != conesizes.end();cs++){ 

			double reliso = e->PFrelIso();
			histos2d[("e_pfreliso_vs_conesize"+id).c_str()]->Fill(atof(cs->c_str()),reliso);
			histos1d[("e_PFrelIso"+*cs+id).c_str()]->Fill(reliso);
		}
	}
}

//plot isolation and vetocone(vetocone to replace dR cut) for semilep and non semilep events
void LeptonIsolationPlots::plotiso_plotveto()
{
	for(std::vector<mor::Muon>::iterator muon = isolated_muons->begin();
	    muon != isolated_muons->end();
	    ++muon){
		
		histos1d[("mu_hcaliso"+id).c_str()]->Fill(muon->hcalIso());
		histos1d[("mu_ecaliso"+id).c_str()]->Fill(muon->ecalIso());
		histos1d[("mu_trackiso"+id).c_str()]->Fill(muon->trackIso());
		histos1d[("mu_caloiso"+id).c_str()]->Fill(muon->caloIso());
		double mu_sumIso = (muon->trackIso()+muon->hcalIso()+muon->ecalIso());
		double mu_CombRelIso = (mu_sumIso)/(muon->pt());
		histos1d[("mu_CombRelIso"+id).c_str()]->Fill(mu_CombRelIso);
		histos1d[("mu_sumiso"+id).c_str()]->Fill(mu_sumIso);
		histos1d[("mu_veto_hcalEnergy"+id).c_str()]->Fill(muon->hcal_vcone());
		histos1d[("mu_veto_ecalEnergy"+id).c_str()]->Fill(muon->ecal_vcone()); 

		double pfreliso = (muon->photonIso()+muon->neutralHadronIso()+muon->chargedHadronIso())/muon->Pt();
		histos1d[("mu_PFrelIso"+id).c_str()]->Fill(pfreliso);
		histos1d[("mu_dbeta_PFrelIso"+id).c_str()]->Fill(muon->dbeta_PFrelIso());
		
		// 2d plots lepton isolation to see correlations
		histos2d[("mu_caloiso_vs_trackiso"+id).c_str()]->Fill(muon->trackIso(), muon->caloIso());
		histos2d[("mu_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill(muon->ecal_vcone(), muon->hcal_vcone());
	}
	
	for(std::vector<mor::Electron>::iterator electron = isolated_electrons->begin();electron != isolated_electrons->end();++electron){
		
		histos1d[("e_hcaliso"+id).c_str()]->Fill(electron->hcalIso());
		histos1d[("e_ecaliso"+id).c_str()]->Fill(electron->ecalIso());
		histos1d[("e_trackiso"+id).c_str()]->Fill(electron->trackIso());
		histos1d[("e_caloiso"+id).c_str()]->Fill(electron->caloIso());
		double e_sumIso = (electron->trackIso()+electron->hcalIso()+electron->ecalIso());
		double e_CombRelIso = (e_sumIso)/(electron->pt());
		histos1d[("e_CombRelIso"+id).c_str()]->Fill(e_CombRelIso);
		histos1d[("e_sumiso"+id).c_str()]->Fill(e_sumIso);
		histos1d[("e_veto_hcalEnergy"+id).c_str()]->Fill(electron->hcal_vcone());
		histos1d[("e_veto_ecalEnergy"+id).c_str()]->Fill(electron->ecal_vcone());

		double pfreliso = electron->PFrelIso();
		histos1d[("e_PFrelIso"+id).c_str()]->Fill(pfreliso);
		histos1d[("e_EA_PFrelIso"+id).c_str()]->Fill(electron->EA_PFrelIso());
		
		// 2d plots lepton isolation to see correlations
		histos2d[("e_caloiso_vs_trackiso"+id).c_str()]->Fill(electron->trackIso(), electron->caloIso());
		histos2d[("e_veto_hcal_vs_veto_ecal"+id).c_str()]->Fill(electron->ecal_vcone(), electron->hcal_vcone());
	}
}
