#include "../../interface/EventSelection/XPlusNJetsPlots.h"

//NB: Most of what I write here is just f.y.i. To use this program go down to plot_jets() and book_histos(). That is where you add code. You can even ignore the rest for now. 
// Code information in case you don't already know. This is a class. Above you see the header file .h was included. In the header file you declare all variables you will use in different functions in the class e.g. std::string id; is declared under private: . If you will only use a variable within one function you can declare and initialise is within the function , this will be the case for most variables, this is what you were doing in the JMAnalyser. If you declare a variable in the header you should initialise it in the constructor of the class. Here we declare id in the header because it contains information we need in the title of every histogram plotted eg TTbar|muon|01_cutset.  
//All functions must also be declared in the header. Functions declared under public: can be called from other classes. You dont need this for now. XPlusNJetsPlot, plot_all, and book_histos are already called from another class, dont edit the names of these functions. Functions declared under private: can only be used within the class. This is what you will be adding to later. 

//Not necessary info for you to run this but might be interesting for later: The class XPlusNJetsPlots was created as a new objects in another class. When its constructor was called the string 'id' was passed to this class
 
//Constructor
//When this class object is created (from another class), the constructor is called. 
XPlusNJetsPlots::XPlusNJetsPlots(std::string ident)
{
	id = ident;
}

//Destructor
//Some things need to be deleted here if they were declared with new. 
XPlusNJetsPlots::~XPlusNJetsPlots()
{
}

//How to add a function: 
//plot_jets below is a function you will use. If you want to make plots of things other than jets you should create a new function. To do this
//1/ in the .h file (path above) declare the function in the same place, in the same way as plot_jets so eg under private: void plot_muons();
//2/ Add the function here in the same way as plot_jets e.g void XPlusNJetsPlot::plot_muons() {  }
//3/ Within the curly brackets add whatever, so histogram filled here
//4/ In plot_all() add your new function in the same way as plot_jets();

//Here all the functions which will run are called. If you create a function but don't add it here it won't run 
void XPlusNJetsPlots::plot_all()
{
	// different plotting functions here
	plot_jets();
}



//Sample function. If you want to add more plots about jets, the same ones you had in JMAnalyser, you add them here
void XPlusNJetsPlots::plot_jets()
{
  
  int jet_size = 0;

        // plotting code here
	// 'jets' is the vector
	for(std::vector<mor::Jet>::iterator jet = jets->begin();
		jet != jets->end();
		++jet){
		histos1d[("pt_all_jets"+id).c_str()]->Fill(jet->pt());
	}

	// make sure there *is* at least one jet in the event if you want to access it!
	if(jets->size() > 1){
		histos1d[("j1_pt"+id).c_str()]->Fill((*jets)[0].pt());
	}

	// plot for number of Jets in each event
	for (std::vector<mor::Jet>::const_iterator iter = jets->begin();
	     iter!=jets->end();
	     iter++
	     )
	  {
	    if(iter->pt() >=20){
	      jet_size++;
	    histos2d[("Eta_vs_Phi_Jets"+id).c_str()]->Fill(iter->eta(), iter->phi());
	    //histos1d[("Jet_Eta_DB"+id).c_str()]->Fill(iter->eta());
	    //histos1d[("Jet_Phi_DB"+id).c_str()]->Fill(iter->phi());
	    }
	  }
	histos1d[("jet_size"+id).c_str()]->Fill(jet_size);

        for (std::vector<mor::MET>::const_iterator iter = corrected_mets->begin();
             iter!=corrected_mets->end();
             iter++
             )
          {
	    histos2d[("Eta_vs_Phi_MET"+id).c_str()]->Fill(iter->eta(), iter->phi());
	  }
	// Eta vs Phi
	//histos2d[("Eta vs Phi - Jets"+id).c_str()]->Fill(iter->eta(), iter->phi());
}

//Here you book all the histograms you will use, same as in JMAnalyser
void XPlusNJetsPlots::book_histos()
{
  //Always put in +id after the histo name of your choice, otherwise you wont be able to tell what cutset etc is was filled for
  histos1d[("j1_pt"+id).c_str()]=histo_writer->create_1d(("j1_pt"+id).c_str(), // identifier always the same in histos1d["<ident>"]... and ...create_1d(("<ident>"+id ...
							 "pt of leading jet in event", // title of histogram
							 30, // number of bins
							 0., // x axis lower boundary
							 300., // x axis upper boundary
							 "p_{T} leading jet", // x axis caption
							 "Events/bin");	// y axis caption
  histos1d[("pt_all_jets"+id).c_str()]=histo_writer->create_1d(("pt_all_jets"+id).c_str(),
							 "pt of all jets in event", 
							 30, 
							 0., 
							 300., 
							 "p_{T} jets", 
							 "Events/bin");
  
  // Jet size
  histos1d[("jet_size"+id).c_str()]=histo_writer->create_1d(("jet_size"+id).c_str(), 
							    "Number of jets per event", 
							    16, 
							    0., 
							    16., 
							    "# jets", 
							    "Events/bin");

  /*
  // Jet Eta - Double Binning
  histos1d[("Jet_Eta_DB"+id).c_str()]=histo_writer->create_1d(("Jet_Eta_DB"+id).c_str(),
									"Jet Eta",
									60,
									-3,
									3,
									"Eta",
									"Events/bin");

  // Jet Phi - Double Binning                                                                                                                                                                                 
  histos1d[("Jet_Phi_DB"+id).c_str()]=histo_writer->create_1d(("Jet_Phi_DB"+id).c_str(),
									"Jet Phi",
									60,
									-3.14159,
									3.14159,
									"Phi",
									"Events/bin");
  */
  /*
  ////   2D HISTOS  ////
  */
  
  // Jet eta vs phi
  histos2d[("Eta_vs_Phi_Jets"+id).c_str()]=histo_writer->create_2d(("Eta_vs_Phi_Jets"+id).c_str(),
										  "Eta vs Phi - Jets",
										  60,
										  -3,
										  3,
										  90,
										  -3.14159,
										  3.14159, 
										  "Eta ", 
										  "Phi ");
  // MET vs phi vs eta
  histos2d[("Eta_vs_Phi_MET"+id).c_str()]=histo_writer->create_2d(("Eta_vs_Phi_MET"+id).c_str(),
								    "Eta vs Phi - MET",
								    60,
								    -3,
								    3,
								    90,
								    -3.14159,
								    3.14159,
								    "Eta ",
								    "Phi ");
  
}
