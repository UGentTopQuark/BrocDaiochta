#include "../../interface/EventSelection/BTagEfficiencyWeightProvider.h"

broc::BTagEfficiencyWeightProvider::BTagEfficiencyWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	jets = handle_holder->get_tight_jets();
	gen_evt = handle_holder->get_ttbar_gen_evt();

	double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_BTag_unc");
	eire::ConfigReader *config_reader = handle_holder->get_config_reader();
	working_point = config_reader->get_var("working_point","b_tagging",false);
	sample = config_reader->get_var("sample","b_tagging",false);	
	if( !( (sample == "MadGraph" && (working_point=="CSVT"||sample=="CSVM") ) || (sample=="MC@NLO"&&working_point=="CSVM") ) )
	    std::cout<<"WARNING: Working point and/or sample not supported,assuming unity scale factor, 100% b-tagging efficiency and 0% mis-tagging rate"<<std::endl;

	up = false;
	down = false;

	if(study_unc == 1){ down = true; }
	else if(study_unc == 2){ up = true; }
}

broc::BTagEfficiencyWeightProvider::~BTagEfficiencyWeightProvider()
{
}

bool broc::BTagEfficiencyWeightProvider::is_true_b(mor::Jet jet)
{
  bool is_bjet = false;
  mor::Jet *b_had = gen_evt->hadB_jet();
  mor::Jet *b_lep = gen_evt->lepB_jet();
  double dR_had = sqrt(acos(cos((jet.phi() - b_had->phi())*(jet.phi() - b_had->phi()))) + (jet.eta() - b_had->eta())*(jet.eta() - b_had->eta()));
  double dR_lep = sqrt(acos(cos((jet.phi() - b_lep->phi())*(jet.phi() - b_lep->phi()))) + (jet.eta() - b_lep->eta())*(jet.eta() - b_lep->eta()));
  if(dR_had < 0.3 || dR_lep < 0.3){is_bjet = true;}
  //  std::cout<<"b_had pt = "<<b_had->pt()<<" phi = "<<b_had->phi()<<" eta = "<<b_had->eta()<<std::endl;
  //std::cout<<"b_lep pt = "<<b_lep->pt()<<" phi = "<<b_lep->phi()<<" eta = "<<b_lep->eta()<<std::endl;
  //std::cout<<"jet pt = "<<jet.pt()<<" phi = "<<jet.phi()<<" eta = "<<jet.eta()<<std::endl;
  //std::cout<<"gen jet pt = "<<jet.pt_genjet()<<" phi = "<<jet.phi_genjet()<<" eta = "<<jet.eta_genjet()<<std::endl;
  return is_bjet;
}

double broc::BTagEfficiencyWeightProvider::get_Data_Prob(int UpDown_b, int UpDown_light)
{
  double Prob = 1;
  for(unsigned int i = 0; i < jets->size(); i++){
    if((*jets)[i].is_bjet()){
      if(is_true_b((*jets)[i])){
        Prob *= get_SF((*jets)[i].pt(), UpDown_b)*get_BTag_eff((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b);
      }
      else{
        Prob *= get_SF_light((*jets)[i].pt(), (*jets)[i].eta(), UpDown_light)*get_BTag_eff_light((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light);
      }
    }
    else{
      if(is_true_b((*jets)[i])){
        Prob *= (1. - get_SF((*jets)[i].pt(), UpDown_b)*get_BTag_eff((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b));
      }
      else{
        Prob *= (1. - get_SF_light((*jets)[i].pt(), (*jets)[i].eta(), UpDown_light)*get_BTag_eff_light((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light));
      }
    }
  }
  return Prob;
}

double broc::BTagEfficiencyWeightProvider::get_MC_Prob(int UpDown_b, int UpDown_light)
{
  double Prob = 1;
  for(unsigned int i = 0; i < jets->size(); i++){
    if((*jets)[i].is_bjet()){
      if(is_true_b((*jets)[i])){
        Prob *= get_BTag_eff((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b);
      }
      else{
        Prob *= get_BTag_eff_light((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light);
      }
    }
    else{
      if(is_true_b((*jets)[i])){
        Prob *= (1. - get_BTag_eff((*jets)[i].pt(),(*jets)[i].eta(), UpDown_b));
      }
      else{
        Prob *= (1. - get_BTag_eff_light((*jets)[i].pt(),(*jets)[i].eta(), UpDown_light));
      }
    }
  }
  return Prob;
}

double broc::BTagEfficiencyWeightProvider::get_SF(double pt, int UpDown)
{
    if(working_point == "CSVM"){
	//CSVM with eta < 2.4
	double SFb = (0.938887+(0.00017124*pt))+(-2.76366e-07*(pt*pt));
	double ud = 0.;
	if(up || UpDown == 1) ud = 1.;
	else if(down || UpDown == -1) ud = -1;
	if(pt < 20.){ SFb = SFb + ud*0.0415707*2;}
	else if(pt < 30){ SFb = SFb + ud*0.0415707;}
	else if(pt < 40){ SFb = SFb + ud*0.0204209;}
	else if(pt < 50){ SFb = SFb + ud*0.0223227;}
	else if(pt < 60){ SFb = SFb + ud*0.0206655;}
	else if(pt < 70){ SFb = SFb + ud*0.0199325;}
	else if(pt < 80){ SFb = SFb + ud*0.0174121;}
	else if(pt < 100){ SFb = SFb + ud*0.0202332;}
	else if(pt < 120){ SFb = SFb + ud*0.0182446;}
	else if(pt < 160){ SFb = SFb + ud*0.0159777;}
	else if(pt < 210){ SFb = SFb + ud*0.0218531;}
	else if(pt < 260){ SFb = SFb + ud*0.0204688;}
	else if(pt < 320){ SFb = SFb + ud*0.0265191;}
	else if(pt < 400){ SFb = SFb + ud*0.0313175;}
	else if(pt < 500){ SFb = SFb + ud*0.0415417;}
	else if(pt < 600){ SFb = SFb + ud*0.0740446;}
	else if(pt < 800){ SFb = SFb + ud*0.0596716;}
	else{ SFb = SFb + ud*0.0596716*2;}
	return SFb;}
    else if(working_point == "CSVT"){
	double SFb = (0.927563+(1.55479e-05*pt))+(-1.90666e-07*(pt*pt));
	double ud = 0.;
	if(up || UpDown == 1) ud = 1.;
        else if(down || UpDown == -1) ud = -1;
        if(pt < 20.){ SFb = SFb + ud*0.0515703*2;}
        else if(pt < 30){ SFb = SFb + ud*0.0515703;}
        else if(pt < 40){ SFb = SFb + ud*0.0264008;}
        else if(pt < 50){ SFb = SFb + ud*0.0272757;}
        else if(pt < 60){ SFb = SFb + ud*0.0275565;}
        else if(pt < 70){ SFb = SFb + ud*0.0248745;}
        else if(pt < 80){ SFb = SFb + ud*0.0218456;}
        else if(pt < 100){ SFb = SFb + ud*0.0253845;}
        else if(pt < 120){ SFb = SFb + ud*0.0239588;}
        else if(pt < 160){ SFb = SFb + ud*0.0271791;}
        else if(pt < 210){ SFb = SFb + ud*0.0273912;}
        else if(pt < 260){ SFb = SFb + ud*0.0379822;}
        else if(pt < 320){ SFb = SFb + ud*0.0411624;}
        else if(pt < 400){ SFb = SFb + ud*0.0786307;}
        else if(pt < 500){ SFb = SFb + ud*0.0866832;}
        else if(pt < 600){ SFb = SFb + ud*0.0942053;}
        else if(pt < 800){ SFb = SFb + ud*0.102403;}
        else{ SFb = SFb + ud*0.102403*2;}
        return SFb;}
    else
	return 1.;
}

double broc::BTagEfficiencyWeightProvider::get_SF_light(double pt, double eta, int UpDown)
{
  if(working_point == "CSVM"){
      //CSVM                                                                                                                                                                                                   
      double SFb = 1.;
      int ud = 0;
      if(up || UpDown == 1) ud = 1;
      else if(down || UpDown == -1) ud = -1;
      if(pt > 20){
	  if(fabs(eta) < 0.8){
	      switch (ud){
	      case 0:
		  SFb = ((1.07541+(0.00231827*pt))+(-4.74249e-06*(pt*pt)))+(2.70862e-09*(pt*(pt*pt)));
		  break;
	      case 1:
		  SFb = ((1.18638+(0.00314148*pt))+(-6.68993e-06*(pt*pt)))+(3.89288e-09*(pt*(pt*pt)));
		  break;
	      case -1:
		  SFb = ((0.964527+(0.00149055*pt))+(-2.78338e-06*(pt*pt)))+(1.51771e-09*(pt*(pt*pt)));
		  break;
	      }
	  }
	  else if(fabs(eta) < 1.6){
	      switch (ud){
	      case 0:
		  SFb = ((1.05613+(0.00114031*pt))+(-2.56066e-06*(pt*pt)))+(1.67792e-09*(pt*(pt*pt)));
		  break;
	      case 1:
		  SFb = ((1.16624+(0.00151884*pt))+(-3.59041e-06*(pt*pt)))+(2.38681e-09*(pt*(pt*pt)));
		  break;
	      case -1:
		  SFb = ((0.946051+(0.000759584*pt))+(-1.52491e-06*(pt*pt)))+(9.65822e-10*(pt*(pt*pt)));
		  break;
	      }
	  }
	  else if(fabs(eta) < 2.4){
	      switch (ud){
	      case 0:
		  SFb = ((1.05625+(0.000487231*pt))+(-2.22792e-06*(pt*pt)))+(1.70262e-09*(pt*(pt*pt)));
		  break;
	      case 1:
		  SFb = ((1.15575+(0.000693344*pt))+(-3.02661e-06*(pt*pt)))+(2.39752e-09*(pt*(pt*pt)));
		  break;
	      case -1:
		  SFb = ((0.956736+(0.000280197*pt))+(-1.42739e-06*(pt*pt)))+(1.0085e-09*(pt*(pt*pt)));
		  break;
	      }
	  }
      }
      return SFb;}
  else if(working_point == "CSVT"){
      double SFb = 1.;
      int ud = 0;
      if(up || UpDown == 1) ud = 1;
      else if(down || UpDown == -1) ud = -1;
      if(pt > 20){
	  switch (ud){
	  case 0:
	      SFb = ((1.00462+(0.00325971*pt))+(-7.79184e-06*(pt*pt)))+(5.22506e-09*(pt*(pt*pt)));
	      break;
	  case 1:
	      SFb = ((1.16361+(0.00464695*pt))+(-1.09467e-05*(pt*pt)))+(7.21896e-09*(pt*(pt*pt)));
	      break;
	  case -1:
	      SFb = ((0.845757+(0.00186422*pt))+(-4.6133e-06*(pt*pt)))+(3.21723e-09*(pt*(pt*pt)));
	      break;
	  }
      }
      return SFb;}
  else
      return 1.;
}

double broc::BTagEfficiencyWeightProvider::get_BTag_eff(double pt, double eta, int UpDown)
{
    double eff = 0.;
    double ud = 0.;
    if(working_point == "CSVM" && sample == "MC@NLO"){
	if(up || UpDown == 1) ud = 1.;
	else if(down || UpDown == -1) ud = -1;
	//5.3.X TTbar MCatNLO
	if(fabs(eta) < 0.4){
	    if(pt < 20){eff = 49.45 + ud*0.37*2;}
	    else if(pt < 30){eff = 49.45 + ud*0.37;}
	    else if(pt < 35){eff = 55.48 + ud*0.32;}
	    else if(pt < 40){eff = 58.83 + ud*0.29;}
	    else if(pt < 45){eff = 60.48 + ud*0.28;}
	    else if(pt < 50){eff = 63.13 + ud*0.27;}
	    else if(pt < 55){eff = 66.05 + ud*0.26;}
	    else if(pt < 60){eff = 67.28 + ud*0.26;}
	    else if(pt < 65){eff = 68.43 + ud*0.27;}
	    else if(pt < 70){eff = 69.86 + ud*0.28;}
	    else if(pt < 75){eff = 70.53 + ud*0.29;}
	    else if(pt < 80){eff = 71.29 + ud*0.29;}
	    else if(pt < 85){eff = 71.94 + ud*0.31;}
	    else if(pt < 90){eff = 72.84 + ud*0.32;}
	    else if(pt < 95){eff = 72.86 + ud*0.34;}
	    else if(pt < 100){eff = 73.35 + ud*0.36;}
	    else if(pt < 150){eff = 72.98 + ud*0.15;}
	    else if(pt < 200){eff = 70.47 + ud*0.29;}
	    else if(pt > 200){eff = 70.47 + ud*0.29*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 20){eff = 49.91 + ud*0.39*2;}
	    else if(pt < 30){eff = 49.91 + ud*0.39;}
	    else if(pt < 35){eff = 54.98 + ud*0.33;}
	    else if(pt < 40){eff = 59.04 + ud*0.30;}
	    else if(pt < 45){eff = 60.12 + ud*0.29;}
	    else if(pt < 50){eff = 62.98 + ud*0.28;}
	    else if(pt < 55){eff = 66.04 + ud*0.27;}
	    else if(pt < 60){eff = 67.33 + ud*0.28;}
	    else if(pt < 65){eff = 68.35 + ud*0.28;}
	    else if(pt < 70){eff = 69.84 + ud*0.29;}
	    else if(pt < 75){eff = 70.57 + ud*0.30;}
	    else if(pt < 80){eff = 71.67 + ud*0.31;}
	    else if(pt < 85){eff = 72.12 + ud*0.32;}
	    else if(pt < 90){eff = 73.00 + ud*0.34;}
	    else if(pt < 95){eff = 72.94 + ud*0.36;}
	    else if(pt < 100){eff = 72.94 + ud*0.38;}
	    else if(pt < 150){eff = 73.72 + ud*0.16;}
	    else if(pt < 200){eff = 70.92 + ud*0.31;}
	    else if(pt > 200){eff = 70.92 + ud*0.31*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 20){eff = 50.90 + ud*0.43*2;}
	    else if(pt < 30){eff = 50.90 + ud*0.43;}
	    else if(pt < 35){eff = 55.92 + ud*0.36;}
	    else if(pt < 40){eff = 58.73 + ud*0.33;}
	    else if(pt < 45){eff = 60.12 + ud*0.32;}
	    else if(pt < 50){eff = 63.05 + ud*0.31;}
	    else if(pt < 55){eff = 65.28 + ud*0.30;}
	    else if(pt < 60){eff = 66.49 + ud*0.30;}
	    else if(pt < 65){eff = 67.93 + ud*0.31;}
	    else if(pt < 70){eff = 68.36 + ud*0.32;}
	    else if(pt < 75){eff = 69.62 + ud*0.33;}
	    else if(pt < 80){eff = 70.17 + ud*0.34;}
	    else if(pt < 85){eff = 71.26 + ud*0.36;}
	    else if(pt < 90){eff = 71.63 + ud*0.38;}
	    else if(pt < 95){eff = 72.30 + ud*0.39;}
	    else if(pt < 100){eff = 71.85 + ud*0.42;}
	    else if(pt < 150){eff = 72.09 + ud*0.18;}
	    else if(pt < 200){eff = 69.49 + ud*0.35;}
	    else if(pt > 200){eff = 69.49 + ud*0.35*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 20){eff = 49.80 + ud*0.50*2;}
	    else if(pt < 30){eff = 49.80 + ud*0.50;}
	    else if(pt < 35){eff = 56.03 + ud*0.43;}
	    else if(pt < 40){eff = 58.59 + ud*0.39;}
	    else if(pt < 45){eff = 59.65 + ud*0.37;}
	    else if(pt < 50){eff = 62.85 + ud*0.35;}
	    else if(pt < 55){eff = 64.66 + ud*0.35;}
	    else if(pt < 60){eff = 65.54 + ud*0.35;}
	    else if(pt < 65){eff = 66.09 + ud*0.38;}
	    else if(pt < 70){eff = 67.32 + ud*0.37;}
	    else if(pt < 75){eff = 68.09 + ud*0.38;}
	    else if(pt < 80){eff = 68.69 + ud*0.40;}
	    else if(pt < 85){eff = 69.59 + ud*0.42;}
	    else if(pt < 90){eff = 69.66 + ud*0.45;}
	    else if(pt < 95){eff = 69.37 + ud*0.48;}
	    else if(pt < 100){eff = 68.55 + ud*0.51;}
	    else if(pt < 150){eff = 68.79 + ud*0.22;}
	    else if(pt < 200){eff = 66.01 + ud*0.43;}
	    else if(pt > 200){eff = 66.01 + ud*0.43*2;}
	}
	else if(fabs(eta) < 2.0){
	    if(pt < 20){eff = 49.77 + ud*0.62*2;}
	    else if(pt < 30){eff = 49.77 + ud*0.62;}
	    else if(pt < 35){eff = 55.39 + ud*0.54;}
	    else if(pt < 40){eff = 58.15 + ud*0.49;}
	    else if(pt < 45){eff = 60.27 + ud*0.46;}
	    else if(pt < 50){eff = 62.44 + ud*0.44;}
	    else if(pt < 55){eff = 64.25 + ud*0.43;}
	    else if(pt < 60){eff = 65.61 + ud*0.44;}
	    else if(pt < 65){eff = 65.80 + ud*0.45;}
	    else if(pt < 70){eff = 66.57 + ud*0.47;}
	    else if(pt < 75){eff = 67.97 + ud*0.49;}
	    else if(pt < 80){eff = 68.28 + ud*0.52;}
	    else if(pt < 85){eff = 68.84 + ud*0.54;}
	    else if(pt < 90){eff = 69.63 + ud*0.58;}
	    else if(pt < 95){eff = 68.12 + ud*0.61;}
	    else if(pt < 100){eff = 69.16 + ud*0.64;}
	    else if(pt < 150){eff = 68.90 + ud*0.29;}
	    else if(pt < 200){eff = 64.47 + ud*0.59;}
	    else if(pt > 200){eff = 64.47 + ud*0.59*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 20){eff = 50.14 + ud*0.81*2;}
	    else if(pt < 30){eff = 50.14 + ud*0.81;}
	    else if(pt < 35){eff = 54.39 + ud*0.70;}
	    else if(pt < 40){eff = 58.03 + ud*0.62;}
	    else if(pt < 45){eff = 58.51 + ud*0.59;}
	    else if(pt < 50){eff = 61.31 + ud*0.56;}
	    else if(pt < 55){eff = 63.30 + ud*0.55;}
	    else if(pt < 60){eff = 64.69 + ud*0.56;}
	    else if(pt < 65){eff = 64.23 + ud*0.58;}
	    else if(pt < 70){eff = 65.95 + ud*0.60;}
	    else if(pt < 75){eff = 65.60 + ud*0.63;}
	    else if(pt < 80){eff = 66.84 + ud*0.66;}
	    else if(pt < 85){eff = 66.43 + ud*0.71;}
	    else if(pt < 90){eff = 66.50 + ud*0.76;}
	    else if(pt < 95){eff = 66.68 + ud*0.81;}
	    else if(pt < 100){eff = 66.00 + ud*0.86;}
	    else if(pt < 150){eff = 64.83 + ud*0.40;}
	    else if(pt < 200){eff = 61.06 + ud*0.89;}
	    else if(pt > 200){eff = 61.06 + ud*0.89*2;}
	}
	eff *= 0.01;
	return eff;}
    else if(working_point == "CSVT"&& sample == "MadGraph"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 40.0804+ ud*0.609395*2;}
	    else if(pt < 40){eff = 40.0804+ ud*0.609395;}
	    else if(pt < 50){eff = 46.4636+ ud*0.599207;}
	    else if(pt < 60){eff = 50.7989+ ud*0.6138;}
	    else if(pt < 70){eff = 51.9292+ ud*0.645716;}
	    else if(pt < 80){eff = 54.4025+ ud*0.689821;}
	    else if(pt < 90){eff = 56.111+ ud*0.751465;}
	    else if(pt < 100){eff = 54.5742+ ud*0.844511;}
	    else if(pt < 120){eff = 55.1771+ ud*0.691847;}
	    else if(pt < 160){eff = 54.6128+ ud*0.704303;}
	    else if(pt < 210){eff = 46.0526+ ud*1.10033;}
	    else if(pt < 300){eff = 41.2467+ ud*1.79277;}
	    else if (pt > 300){eff = 41.2467+ ud*1.79277*2;}
	}
	if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 41.2001+ ud*0.624284*2;}
	    else if(pt < 40){eff = 41.2001+ ud*0.624284;}
	    else if(pt < 50){eff = 45.6956+ ud*0.614899;}
	    else if(pt < 60){eff = 51.1661+ ud*0.631778;}
	    else if(pt < 70){eff = 51.8005+ ud*0.662243;}
	    else if(pt < 80){eff = 54.4393+ ud*0.70665;}
	    else if(pt < 90){eff = 55.8529+ ud*0.785331;}
	    else if(pt < 100){eff = 54.2751+ ud*0.876819;}
	    else if(pt < 120){eff = 56.2447+ ud*0.722999;}
	    else if(pt < 160){eff = 55.8048+ ud*0.732942;}
	    else if(pt < 210){eff = 45.4264+ ud*1.13189;}
	    else if(pt < 300){eff = 42.1569+ ud*1.84804;}
	    else if (pt > 300){eff = 42.1569+ ud*1.84804*2;}
	}
	if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 40.7651+ ud*0.693625*2;}
	    else if(pt < 40){eff = 40.7651+ ud*0.693625;}
	    else if(pt < 50){eff = 44.9105+ ud*0.679147;}
	    else if(pt < 60){eff = 49.2444+ ud*0.687113;}
	    else if(pt < 70){eff = 51.4316+ ud*0.727865;}
	    else if(pt < 80){eff = 53.6338+ ud*0.793554;}
	    else if(pt < 90){eff = 54.2998+ ud*0.873004;}
	    else if(pt < 100){eff = 52.4027+ ud*0.97533;}
	    else if(pt < 120){eff = 52.9814+ ud*0.808919;}
	    else if(pt < 160){eff = 53.9314+ ud*0.819339;}
	    else if(pt < 210){eff = 46.3821+ ud*1.34145;}
	    else if(pt < 300){eff = 40.3255+ ud*2.08604;}
	    else if (pt > 300){eff = 40.3255+ ud*2.08604*2;}
	}
	if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 39.9174+ ud*0.812834*2;}
	    else if(pt < 40){eff = 39.9174+ ud*0.812834;}
	    else if(pt < 50){eff = 44.1024+ ud*0.79854;}
	    else if(pt < 60){eff = 48.0694+ ud*0.804283;}
	    else if(pt < 70){eff = 50.7517+ ud*0.866882;}
	    else if(pt < 80){eff = 51.5519+ ud*0.94395;}
	    else if(pt < 90){eff = 53.4604+ ud*1.01543;}
	    else if(pt < 100){eff = 50.2426+ ud*1.16089;}
	    else if(pt < 120){eff = 49.703+ ud*0.99502;}
	    else if(pt < 160){eff = 50.8082+ ud*1.04494;}
	    else if(pt < 210){eff = 37.6712+ ud*1.63718;}
	    else if(pt < 300){eff = 31.0448+ ud*2.52787;}
	    else if (pt > 300){eff = 31.0448+ ud*2.52787*2;}
	}
	if(fabs(eta) < 2){
	    if(pt < 30){eff = 40.6673+ ud*1.04301*2;}
	    else if(pt < 40){eff = 40.6673+ ud*1.04301;}
	    else if(pt < 50){eff = 44.103+ ud*1.01181;}
	    else if(pt < 60){eff = 46.4607+ ud*1.03302;}
	    else if(pt < 70){eff = 48.8775+ ud*1.08108;}
	    else if(pt < 80){eff = 48.2932+ ud*1.1821;}
	    else if(pt < 90){eff = 49.4382+ ud*1.32491;}
	    else if(pt < 100){eff = 51.0541+ ud*1.51343;}
	    else if(pt < 120){eff = 47.8082+ ud*1.3073;}
	    else if(pt < 160){eff = 46.3118+ ud*1.37506;}
	    else if(pt < 210){eff = 35.2174+ ud*2.22704;}
	    else if(pt < 300){eff = 34.1463+ ud*4.27572;}
	    else if (pt > 300){eff = 34.1463+ ud*4.27572*2;}
	}
	if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 39.4774+ ud*1.29898;}
	    else if(pt < 40){eff = 39.4774+ ud*1.29898;}
	    else if(pt < 50){eff = 44.4951+ ud*1.26843;}
	    else if(pt < 60){eff = 45.9496+ ud*1.30026;}
	    else if(pt < 70){eff = 45.7785+ ud*1.38661;}
	    else if(pt < 80){eff = 48.5876+ ud*1.53368;}
	    else if(pt < 90){eff = 47.8009+ ud*1.69939;}
	    else if(pt < 100){eff = 45.9542+ ud*1.94726;}
	    else if(pt < 120){eff = 46.9121+ ud*1.71982;}
	    else if(pt < 160){eff = 46.9605+ ud*1.9456;}
	    else if(pt < 210){eff = 26.8293+ ud*3.09454;}
	    else if(pt < 300){eff = 21.1538+ ud*5.66348;}
	    else if (pt > 300){eff = 21.1538+ ud*5.66348*2;}
	}
	eff *= 0.01;
	return eff;}
    else if(working_point == "CSVM" && sample == "MadGraph"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 53.9354+ ud*0.619825*2;}
	    else if(pt < 40){eff = 53.9354+ ud*0.619825;}
	    else if(pt < 50){eff = 59.1368+ ud*0.590597;}
	    else if(pt < 60){eff = 63.7172+ ud*0.590325;}
	    else if(pt < 70){eff = 66.2435+ ud*0.611147;}
	    else if(pt < 80){eff = 69.25+ ud*0.639129;}
	    else if(pt < 90){eff = 71.2222+ ud*0.685557;}
	    else if(pt < 100){eff = 71.5478+ ud*0.765273;}
	    else if(pt < 120){eff = 72.2276+ ud*0.623073;}
	    else if(pt < 160){eff = 73.0238+ ud*0.627867;}
	    else if(pt < 210){eff = 72.6608+ ud*0.983907;}
	    else if(pt < 300){eff = 67.6393+ ud*1.70382;}
	    else if (pt > 300){eff = 67.6393+ ud*1.70382*2;}
	}
	if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 54.6976+ ud*0.631378*2;}
	    else if(pt < 40){eff = 54.6976+ ud*0.631378;}
	    else if(pt < 50){eff = 58.5251+ ud*0.608153;}
	    else if(pt < 60){eff = 64.3131+ ud*0.605504;}
	    else if(pt < 70){eff = 65.5893+ ud*0.62964;}
	    else if(pt < 80){eff = 68.633+ ud*0.658349;}
	    else if(pt < 90){eff = 70.1351+ ud*0.723814;}
	    else if(pt < 100){eff = 71.3755+ ud*0.795567;}
	    else if(pt < 120){eff = 73.2795+ ud*0.644904;}
	    else if(pt < 160){eff = 73.2085+ ud*0.653621;}
	    else if(pt < 210){eff = 70.9561+ ud*1.03201;}
	    else if(pt < 300){eff = 66.5266+ ud*1.76603;}
	    else if (pt > 300){eff = 66.5266+ ud*1.76603*2;}
	}
	if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 54.6125+ ud*0.702758*2;}
	    else if(pt < 40){eff = 54.6125+ ud*0.702758;}
	    else if(pt < 50){eff = 58.352+ ud*0.673102;}
	    else if(pt < 60){eff = 62.0514+ ud*0.666932;}
	    else if(pt < 70){eff = 65.3234+ ud*0.693125;}
	    else if(pt < 80){eff = 67.5361+ ud*0.745118;}
	    else if(pt < 90){eff = 69.6867+ ud*0.80547;}
	    else if(pt < 100){eff = 68.7643+ ud*0.905088;}
	    else if(pt < 120){eff = 70.7644+ ud*0.737177;}
	    else if(pt < 160){eff = 72.629+ ud*0.732893;}
	    else if(pt < 210){eff = 71.9247+ ud*1.20878;}
	    else if(pt < 300){eff = 67.4503+ ud*1.99252;}
	    else if (pt > 300){eff = 67.4503+ ud*1.99252*2;}
	}
	if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 53.168+ ud*0.828215*2;}
	    else if(pt < 40){eff = 53.168+ ud*0.828215;}
	    else if(pt < 50){eff = 57.4237+ ud*0.795241;}
	    else if(pt < 60){eff = 61.2853+ ud*0.784113;}
	    else if(pt < 70){eff = 63.8304+ ud*0.833152;}
	    else if(pt < 80){eff = 65.7153+ ud*0.896545;}
	    else if(pt < 90){eff = 67.5093+ ud*0.953416;}
	    else if(pt < 100){eff = 67.2237+ ud*1.08986;}
	    else if(pt < 120){eff = 67.7624+ ud*0.930133;}
	    else if(pt < 160){eff = 67.9773+ ud*0.975187;}
	    else if(pt < 210){eff = 65.6393+ ud*1.60458;}
	    else if(pt < 300){eff = 61.4925+ ud*2.65865;}
	    else if (pt > 300){eff = 61.4925+ ud*2.65865*2;}
	}
	if(fabs(eta) < 2){
	    if(pt < 30){eff = 52.9757+ ud*1.05979;}
	    else if(pt < 40){eff = 52.9757+ ud*1.05979;}
	    else if(pt < 50){eff = 56.3953+ ud*1.01055;}
	    else if(pt < 60){eff = 61.4329+ ud*1.00818;}
	    else if(pt < 70){eff = 62.9093+ ud*1.04469;}
	    else if(pt < 80){eff = 63.4023+ ud*1.13951;}
	    else if(pt < 90){eff = 64.0449+ ud*1.27165;}
	    else if(pt < 100){eff = 66.8194+ ud*1.42554;}
	    else if(pt < 120){eff = 65.6849+ ud*1.24251;}
	    else if(pt < 160){eff = 63.9544+ ud*1.32403;}
	    else if(pt < 210){eff = 65.4348+ ud*2.21741;}
	    else if(pt < 300){eff = 58.5366+ ud*4.44215;}
	    else if (pt > 300){eff = 58.5366+ ud*4.44215*2;}
	}
	if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 52.7542+ ud*1.32672*2;}
	    else if(pt < 40){eff = 52.7542+ ud*1.32672;}
	    else if(pt < 50){eff = 56.8078+ ud*1.26431;}
	    else if(pt < 60){eff = 59.0197+ ud*1.28314;}
	    else if(pt < 70){eff = 61.6576+ ud*1.35322;}
	    else if(pt < 80){eff = 64.5009+ ud*1.46835;}
	    else if(pt < 90){eff = 62.037+ ud*1.65101;}
	    else if(pt < 100){eff = 64.1221+ ud*1.87412;}
	    else if(pt < 120){eff = 64.133+ ud*1.65285;}
	    else if(pt < 160){eff = 66.7173+ ud*1.83703;}
	    else if(pt < 210){eff = 59.5122+ ud*3.42837;}
	    else if(pt < 300){eff = 44.2308+ ud*6.88744;}
	    else if (pt > 300){eff = 44.2308+ ud*6.88744*2;}
	}
	eff *= 0.01;
	return eff;}
    else
	return 1.;
}

double broc::BTagEfficiencyWeightProvider::get_BTag_eff_light(double pt, double eta, int UpDown)
{
    double eff = 0.;
    double ud = 0.;
    if(up || UpDown == 1) ud = 1.;
    else if(down || UpDown == -1) ud = -1;
    if(working_point=="CSVM"&&sample=="MC@NLO"){
	//5.3.X TTbar MCatNLO
	if(fabs(eta) < 0.4){
	    if(pt < 20){eff = 1.24 + ud*0.05*2;}
	    else if(pt < 30){eff = 1.24 + ud*0.05;}
	    else if(pt < 35){eff = 1.26 + ud*0.05;}
	    else if(pt < 40){eff = 1.33 + ud*0.05;}
	    else if(pt < 45){eff = 1.08 + ud*0.05;}
	    else if(pt < 50){eff = 1.14 + ud*0.05;}
	    else if(pt < 55){eff = 1.23 + ud*0.06;}
	    else if(pt < 60){eff = 1.06 + ud*0.06;}
	    else if(pt < 65){eff = 1.06 + ud*0.06;}
	    else if(pt < 70){eff = 1.19 + ud*0.07;}
	    else if(pt < 75){eff = 1.19 + ud*0.07;}
	    else if(pt < 80){eff = 1.41 + ud*0.09;}
	    else if(pt < 85){eff = 1.57 + ud*0.10;}
	    else if(pt < 90){eff = 1.36 + ud*0.10;}
	    else if(pt < 95){eff = 1.38 + ud*0.10;}
	    else if(pt < 100){eff = 1.72 + ud*0.12;}
	    else if(pt < 150){eff = 1.68 + ud*0.05;}
	    else if(pt < 200){eff = 2.14 + ud*0.10;}
	    else if(pt > 200){eff = 2.14 + ud*0.10*2;}
	}
	else if(fabs(eta) < 0.8){
	    if(pt < 20){eff = 1.17 + ud*0.05*2;}
	    else if(pt < 30){eff = 1.17 + ud*0.05;}
	    else if(pt < 35){eff = 1.43 + ud*0.06;}
	    else if(pt < 40){eff = 1.23 + ud*0.05;}
	    else if(pt < 45){eff = 1.01 + ud*0.05;}
	    else if(pt < 50){eff = 1.08 + ud*0.05;}
	    else if(pt < 55){eff = 1.14 + ud*0.06;}
	    else if(pt < 60){eff = 1.20 + ud*0.06;}
	    else if(pt < 65){eff = 1.29 + ud*0.07;}
	    else if(pt < 70){eff = 1.14 + ud*0.07;}
	    else if(pt < 75){eff = 1.36 + ud*0.08;}
	    else if(pt < 80){eff = 1.26 + ud*0.08;}
	    else if(pt < 85){eff = 1.20 + ud*0.09;}
	    else if(pt < 90){eff = 1.39 + ud*0.10;}
	    else if(pt < 95){eff = 1.61 + ud*0.11;}
	    else if(pt < 100){eff = 1.59 + ud*0.12;}
	    else if(pt < 150){eff = 1.83 + ud*0.06;}
	    else if(pt < 200){eff = 2.25 + ud*0.11;}
	    else if(pt > 200){eff = 2.25 + ud*0.11*2;}
	}
	else if(fabs(eta) < 1.2){
	    if(pt < 20){eff = 1.36 + ud*0.06*2;}
	    else if(pt < 30){eff = 1.36 + ud*0.06;}
	    else if(pt < 35){eff = 1.45 + ud*0.06;}
	    else if(pt < 40){eff = 1.31 + ud*0.06;}
	    else if(pt < 45){eff = 0.99 + ud*0.05;}
	    else if(pt < 50){eff = 1.05 + ud*0.06;}
	    else if(pt < 55){eff = 1.09 + ud*0.06;}
	    else if(pt < 60){eff = 1.30 + ud*0.07;}
	    else if(pt < 65){eff = 1.09 + ud*0.07;}
	    else if(pt < 70){eff = 1.13 + ud*0.08;}
	    else if(pt < 75){eff = 1.32 + ud*0.09;}
	    else if(pt < 80){eff = 1.34 + ud*0.09;}
	    else if(pt < 85){eff = 1.33 + ud*0.10;}
	    else if(pt < 90){eff = 1.32 + ud*0.11;}
	    else if(pt < 95){eff = 1.43 + ud*0.12;}
	    else if(pt < 100){eff = 1.67 + ud*0.13;}
	    else if(pt < 150){eff = 1.78 + ud*0.06;}
	    else if(pt < 200){eff = 2.01 + ud*0.11;}
	    else if(pt > 200){eff = 2.01 + ud*0.11*2;}
	}
	else if(fabs(eta) < 1.6){
	    if(pt < 20){eff = 1.27 + ud*0.07*2;}
	    else if(pt < 30){eff = 1.27 + ud*0.07;}
	    else if(pt < 35){eff = 1.44 + ud*0.07;}
	    else if(pt < 40){eff = 1.33 + ud*0.07;}
	    else if(pt < 45){eff = 1.13 + ud*0.07;}
	    else if(pt < 50){eff = 1.11 + ud*0.07;}
	    else if(pt < 55){eff = 1.29 + ud*0.08;}
	    else if(pt < 60){eff = 1.15 + ud*0.08;}
	    else if(pt < 65){eff = 1.00 + ud*0.08;}
	    else if(pt < 70){eff = 1.17 + ud*0.09;}
	    else if(pt < 75){eff = 1.19 + ud*0.10;}
	    else if(pt < 80){eff = 1.36 + ud*0.11;}
	    else if(pt < 85){eff = 1.44 + ud*0.12;}
	    else if(pt < 90){eff = 1.52 + ud*0.13;}
	    else if(pt < 95){eff = 1.44 + ud*0.13;}
	    else if(pt < 100){eff = 1.37 + ud*0.14;}
	    else if(pt < 150){eff = 1.70 + ud*0.07;}
	    else if(pt < 200){eff = 1.70 + ud*0.11;}
	    else if(pt > 200){eff = 1.70 + ud*0.11*2;}
	}
	else if(fabs(eta) < 2.0){
	    if(pt < 20){eff = 1.28 + ud*0.09*2;}
	    else if(pt < 30){eff = 1.28 + ud*0.09;}
	    else if(pt < 35){eff = 1.32 + ud*0.09;}
	    else if(pt < 40){eff = 1.41 + ud*0.09;}
	    else if(pt < 45){eff = 1.11 + ud*0.09;}
	    else if(pt < 50){eff = 1.22 + ud*0.09;}
	    else if(pt < 55){eff = 1.28 + ud*0.10;}
	    else if(pt < 60){eff = 1.24 + ud*0.10;}
	    else if(pt < 65){eff = 1.15 + ud*0.11;}
	    else if(pt < 70){eff = 1.40 + ud*0.12;}
	    else if(pt < 75){eff = 1.23 + ud*0.12;}
	    else if(pt < 80){eff = 1.64 + ud*0.15;}
	    else if(pt < 85){eff = 1.56 + ud*0.15;}
	    else if(pt < 90){eff = 1.65 + ud*0.17;}
	    else if(pt < 95){eff = 1.61 + ud*0.18;}
	    else if(pt < 100){eff = 1.40 + ud*0.17;}
	    else if(pt < 150){eff = 1.84 + ud*0.08;}
	    else if(pt < 200){eff = 2.40 + ud*0.15;}
	    else if(pt > 200){eff = 2.40 + ud*0.15*2;}
	}
	else if(fabs(eta) < 2.4){
	    if(pt < 20){eff = 1.30 + ud*0.12*2;}
	    else if(pt < 30){eff = 1.30 + ud*0.12;}
	    else if(pt < 35){eff = 1.44 + ud*0.12;}
	    else if(pt < 40){eff = 1.27 + ud*0.12;}
	    else if(pt < 45){eff = 1.18 + ud*0.11;}
	    else if(pt < 50){eff = 1.15 + ud*0.12;}
	    else if(pt < 55){eff = 1.31 + ud*0.13;}
	    else if(pt < 60){eff = 1.39 + ud*0.14;}
	    else if(pt < 65){eff = 1.27 + ud*0.14;}
	    else if(pt < 70){eff = 1.56 + ud*0.17;}
	    else if(pt < 75){eff = 1.43 + ud*0.17;}
	    else if(pt < 80){eff = 1.29 + ud*0.17;}
	    else if(pt < 85){eff = 1.53 + ud*0.19;}
	    else if(pt < 90){eff = 1.44 + ud*0.20;}
	    else if(pt < 95){eff = 1.95 + ud*0.24;}
	    else if(pt < 100){eff = 1.75 + ud*0.24;}
	    else if(pt < 150){eff = 2.10 + ud*0.10;}
	    else if(pt < 200){eff = 2.81 + ud*0.18;}
	    else if(pt > 200){eff = 2.81 + ud*0.18*2;}
	}
	eff = eff*0.01;
	return eff;}
    else if(working_point=="CSVT"&&sample=="MadGraph"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 0.381255+ ud*0.0549244*2;}
	    else if(pt < 40){eff = 0.381255+ ud*0.0549244;}
	    else if(pt < 50){eff = 0.305577+ ud*0.0576603;}
	    else if(pt < 60){eff = 0.47523+ ud*0.0825302;}
	    else if(pt < 70){eff = 0.391619+ ud*0.0873971;}
	    else if(pt < 80){eff = 0.490557+ ud*0.109422;}
	    else if(pt < 90){eff = 0.74224+ ud*0.157658;}
	    else if(pt < 100){eff = 0.752212+ ud*0.181751;}
	    else if(pt < 120){eff = 0.694444+ ud*0.147541;}
	    else if(pt < 160){eff = 0.921659+ ud*0.173373;}
	    else if(pt < 210){eff = 1.49031+ ud*0.330752;}
	    else if(pt < 300){eff = 1.41844+ ud*0.445357;}
	    else if (pt > 300){eff = 1.41844+ ud*0.445357*2;}
	}
	if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 0.305966+ ud*0.0509163*2;}
	    else if(pt < 40){eff = 0.305966+ ud*0.0509163;}
	    else if(pt < 50){eff = 0.223136+ ud*0.0511337;}
	    else if(pt < 60){eff = 0.349544+ ud*0.0727575;}
	    else if(pt < 70){eff = 0.471601+ ud*0.0981035;}
	    else if(pt < 80){eff = 0.621454+ ud*0.129179;}
	    else if(pt < 90){eff = 0.979325+ ud*0.187546;}
	    else if(pt < 100){eff = 0.457457+ ud*0.144329;}
	    else if(pt < 120){eff = 1.09215+ ud*0.19201;}
	    else if(pt < 160){eff = 0.978679+ ud*0.184046;}
	    else if(pt < 210){eff = 1.29683+ ud*0.303677;}
	    else if(pt < 300){eff = 1.34328+ ud*0.444744;}
	    else if (pt > 300){eff = 1.34328+ ud*0.444744*2;}
	}
	if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 0.261042+ ud*0.0521402*2;}
	    else if(pt < 40){eff = 0.261042+ ud*0.0521402;}
	    else if(pt < 50){eff = 0.298084+ ud*0.0649502;}
	    else if(pt < 60){eff = 0.341168+ ud*0.0802767;}
	    else if(pt < 70){eff = 0.321942+ ud*0.0891467;}
	    else if(pt < 80){eff = 0.694444+ ud*0.151013;}
	    else if(pt < 90){eff = 0.473526+ ud*0.142435;}
	    else if(pt < 100){eff = 0.770501+ ud*0.20513;}
	    else if(pt < 120){eff = 0.984817+ ud*0.200033;}
	    else if(pt < 160){eff = 1.13732+ ud*0.217629;}
	    else if(pt < 210){eff = 0.780572+ ud*0.259173;}
	    else if(pt < 300){eff = 1.90641+ ud*0.5693;}
	    else if (pt > 300){eff = 1.90641+ ud*0.5693*2;}
	}
	if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 0.205671+ ud*0.0549112*2;}
	    else if(pt < 40){eff = 0.205671+ ud*0.0549112;}
	    else if(pt < 50){eff = 0.366226+ ud*0.086162;}
	    else if(pt < 60){eff = 0.452489+ ud*0.109496;}
	    else if(pt < 70){eff = 0.282586+ ud*0.0997679;}
	    else if(pt < 80){eff = 0.358584+ ud*0.126551;}
	    else if(pt < 90){eff = 0.831848+ ud*0.221394;}
	    else if(pt < 100){eff = 0.397456+ ud*0.177394;}
	    else if(pt < 120){eff = 0.584795+ ud*0.184387;}
	    else if(pt < 160){eff = 0.705467+ ud*0.202931;}
	    else if(pt < 210){eff = 1.08827+ ud*0.360778;}
	    else if(pt < 300){eff = 0.451467+ ud*0.318514;}
	    else if (pt > 300){eff = 0.451467+ ud*0.318514*2;}
	}
	if(fabs(eta) < 2){
	    if(pt < 30){eff = 0.340301+ ud*0.0907945*2;}
	    else if(pt < 40){eff = 0.340301+ ud*0.0907945;}
	    else if(pt < 50){eff = 0.195122+ ud*0.0795804;}
	    else if(pt < 60){eff = 0.395952+ ud*0.131723;}
	    else if(pt < 70){eff = 0.467017+ ud*0.164729;}
	    else if(pt < 80){eff = 0.743494+ ud*0.234238;}
	    else if(pt < 90){eff = 0.383142+ ud*0.191204;}
	    else if(pt < 100){eff = 0.495049+ ud*0.246911;}
	    else if(pt < 120){eff = 0.91659+ ud*0.28852;}
	    else if(pt < 160){eff = 1.10092+ ud*0.316053;}
	    else if(pt < 210){eff = 0.728597+ ud*0.362969;}
	    else if(pt < 300){eff = 0.332226+ ud*0.331674;}
	    else if (pt > 300){eff = 0.332226+ ud*0.331674*2;}
	}
	if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 0.413394+ ud*0.130456*2;}
	    else if(pt < 40){eff = 0.413394+ ud*0.130456;}
	    else if(pt < 50){eff = 0.287356+ ud*0.128325;}
	    else if(pt < 60){eff = 0.662252+ ud*0.208728;}
	    else if(pt < 70){eff = 0.900901+ ud*0.283604;}
	    else if(pt < 80){eff = 0.249066+ ud*0.175897;}
	    else if(pt < 90){eff = 0.323625+ ud*0.228467;}
	    else if(pt < 100){eff = 0.393701+ ud*0.27784;}
	    else if(pt < 120){eff = 0.393701+ ud*0.27784;}
	    else if(pt < 160){eff = 0.378788+ ud*0.218279;}
	    else if(pt < 210){eff = 0.515464+ ud*0.363547;}
	    else if(pt > 210){eff = 0.515464+ ud*0.363547*2;}
	}

	eff *= 0.01;
	return eff;}
    else if(working_point == "CSVM" && sample == "MadGraph"){
	if(fabs(eta) < 0.4){
	    if(pt < 30){eff = 1.49325+ ud*0.10809*2;}
	    else if(pt < 40){eff = 1.49325+ ud*0.10809;}
	    else if(pt < 50){eff = 1.28779+ ud*0.117785;}
	    else if(pt < 60){eff = 1.6273+ ud*0.151833;}
	    else if(pt < 70){eff = 1.39025+ ud*0.163841;}
	    else if(pt < 80){eff = 1.61884+ ud*0.197646;}
	    else if(pt < 90){eff = 2.02429+ ud*0.258676;}
	    else if(pt < 100){eff = 2.12389+ ud*0.303285;}
	    else if(pt < 120){eff = 2.33586+ ud*0.268348;}
	    else if(pt < 160){eff = 2.63331+ ud*0.290511;}
	    else if(pt < 210){eff = 3.72578+ ud*0.516996;}
	    else if(pt < 300){eff = 6.24113+ ud*0.911052;}
	    else if (pt > 300){eff = 6.24113+ ud*0.911052*2;}
	}
	if(fabs(eta) < 0.8){
	    if(pt < 30){eff = 1.23236+ ud*0.10171*2;}
	    else if(pt < 40){eff = 1.23236+ ud*0.10171;}
	    else if(pt < 50){eff = 0.892543+ ud*0.101924;}
	    else if(pt < 60){eff = 1.18541+ ud*0.133423;}
	    else if(pt < 70){eff = 1.29178+ ud*0.161694;}
	    else if(pt < 80){eff = 1.70224+ ud*0.212629;}
	    else if(pt < 90){eff = 1.88611+ ud*0.259078;}
	    else if(pt < 100){eff = 1.69259+ ud*0.275895;}
	    else if(pt < 120){eff = 2.49147+ ud*0.287949;}
	    else if(pt < 160){eff = 2.76127+ ud*0.306348;}
	    else if(pt < 210){eff = 3.89049+ ud*0.519028;}
	    else if(pt < 300){eff = 4.02985+ ud*0.759758;}
	    else if (pt > 300){eff = 4.02985+ ud*0.759758*2;}
	}
	if(fabs(eta) < 1.2){
	    if(pt < 30){eff = 1.47228+ ud*0.123072*2;}
	    else if(pt < 40){eff = 1.47228+ ud*0.123072;}
	    else if(pt < 50){eff = 1.32009+ ud*0.13598;}
	    else if(pt < 60){eff = 1.19409+ ud*0.14954;}
	    else if(pt < 70){eff = 1.38683+ ud*0.184033;}
	    else if(pt < 80){eff = 1.62037+ ud*0.229598;}
	    else if(pt < 90){eff = 1.50667+ ud*0.252748;}
	    else if(pt < 100){eff = 1.70611+ ud*0.303801;}
	    else if(pt < 120){eff = 2.50308+ ud*0.31645;}
	    else if(pt < 160){eff = 3.07498+ ud*0.354322;}
	    else if(pt < 210){eff = 3.7294+ ud*0.558023;}
	    else if(pt < 300){eff = 5.89255+ ud*0.980338;}
	    else if (pt > 300){eff = 5.89255+ ud*0.980338*2;}
	}
	if(fabs(eta) < 1.6){
	    if(pt < 30){eff = 1.36624+ ud*0.140701*2;}
	    else if(pt < 40){eff = 1.36624+ ud*0.140701;}
	    else if(pt < 50){eff = 1.32248+ ud*0.162945;}
	    else if(pt < 60){eff = 1.251+ ud*0.181332;}
	    else if(pt < 70){eff = 1.13034+ ud*0.198686;}
	    else if(pt < 80){eff = 1.1654+ ud*0.227217;}
	    else if(pt < 90){eff = 1.90137+ ud*0.332907;}
	    else if(pt < 100){eff = 1.51033+ ud*0.343868;}
	    else if(pt < 120){eff = 1.9883+ ud*0.337584;}
	    else if(pt < 160){eff = 2.70429+ ud*0.393298;}
	    else if(pt < 210){eff = 3.62757+ ud*0.650177;}
	    else if(pt < 300){eff = 3.61174+ ud*0.886479;}
	    else if (pt > 300){eff = 3.61174+ ud*0.886479*2;}
	}
	if(fabs(eta) < 2){
	    if(pt < 30){eff = 1.40982+ ud*0.183809*2;}
	    else if(pt < 40){eff = 1.40982+ ud*0.183809;}
	    else if(pt < 50){eff = 1.04065+ ud*0.183003;}
	    else if(pt < 60){eff = 1.89177+ ud*0.285751;}
	    else if(pt < 70){eff = 1.69294+ ud*0.311698;}
	    else if(pt < 80){eff = 2.00743+ ud*0.382434;}
	    else if(pt < 90){eff = 1.62835+ ud*0.391705;}
	    else if(pt < 100){eff = 2.35149+ ud*0.533087;}
	    else if(pt < 120){eff = 2.84143+ ud*0.503033;}
	    else if(pt < 160){eff = 3.21101+ ud*0.533974;}
	    else if(pt < 210){eff = 2.91439+ ud*0.717902;}
	    else if(pt < 300){eff = 3.65449+ ud*1.08155;}
	    else if (pt > 300){eff = 3.65449+ ud*1.08155*2;}
	}
	if(fabs(eta) < 2.4){
	    if(pt < 30){eff = 1.19884+ ud*0.221281*2;}
	    else if(pt < 40){eff = 1.19884+ ud*0.221281;}
	    else if(pt < 50){eff = 1.37931+ ud*0.279602;}
	    else if(pt < 60){eff = 1.5894+ ud*0.321847;}
	    else if(pt < 70){eff = 1.62162+ ud*0.379108;}
	    else if(pt < 80){eff = 1.74346+ ud*0.46188;}
	    else if(pt < 90){eff = 3.39806+ ud*0.72881;}
	    else if(pt < 100){eff = 1.9685+ ud*0.616338;}
	    else if(pt < 120){eff = 1.69014+ ud*0.483761;}
	    else if(pt < 160){eff = 2.52525+ ud*0.557488;}
	    else if(pt < 210){eff = 3.35052+ ud*0.913566;}
	    else if(pt < 300){eff = 3.38164+ ud*1.25634;}
	    else if (pt > 300){eff = 3.38164+ ud*1.25634*2;}
	}
	eff *= 0.01;
	return eff;}

    else{
	return 0;}
}


double broc::BTagEfficiencyWeightProvider::get_weight(int UpDown_b, int UpDown_light)
{
	double weight = 1.;

	if(do_not_reweight)
		weight = 1.;
	else{
	  if(jets->size() >= 1){
	    if(get_MC_Prob(UpDown_b, UpDown_light) > 0){
	      weight = get_Data_Prob(UpDown_b, UpDown_light)/get_MC_Prob(UpDown_b, UpDown_light);
	    }
	  }
	}

	return weight;
}
