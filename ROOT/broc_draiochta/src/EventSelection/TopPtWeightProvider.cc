#include "../../interface/EventSelection/TopPtWeightProvider.h"

broc::TopPtWeightProvider::TopPtWeightProvider(eire::HandleHolder *handle_holder)
{
  this->handle_holder = handle_holder;
}

broc::TopPtWeightProvider::~TopPtWeightProvider()
{
}

double broc::TopPtWeightProvider::get_value_for_event()
{
  mor::TTbarGenEvent *gen_evt = handle_holder->get_ttbar_gen_evt(); 

  if(gen_evt->decay_channel() == NULL){return 1;}

  double lept_pt = gen_evt->lepT()->p4().pt();
  double hadt_pt = gen_evt->hadT()->p4().pt();

  double a = 0.;
  double b = 0.;
  double decay_ch = gen_evt->decay_channel();

  if(decay_ch == 1 || decay_ch == 2 || decay_ch == 6){//lepton+jets
    a = 0.159;
    b = -0.00141;
  }
  else if(decay_ch == 3 || decay_ch == 5){//ttbar other
    a = 0.156;
    b = -0.00137;
  }
  else if(decay_ch == 4){//dilepton
    a = 0.148;
    b = -0.00129;
  }

  double weight = sqrt(exp(a+b*lept_pt)*exp(a+b*hadt_pt));

  return weight;
}
