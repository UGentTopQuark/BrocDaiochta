#include "../../interface/EventSelection/BTagAlgoNameProvider.h"

broc::BTagAlgoNameProvider::BTagAlgoNameProvider()
{
	algo_names[1] = "jetBProbabilityBJetTags";
	algo_names[2] = "jetProbabilityBJetTags";
	algo_names[3] = "trackCountingHighPurBJetTags";
	algo_names[4] = "trackCountingHighEffBJetTags";
	algo_names[5] = "softMuonBJetTags";
	algo_names[6] = "combinedSecondaryVertexMVABJetTags";
	algo_names[7] = "combinedSecondaryVertexBJetTags";
}

broc::BTagAlgoNameProvider::~BTagAlgoNameProvider()
{
}

std::string broc::BTagAlgoNameProvider::get_name(int algo_id)
{
	if(algo_names.find(algo_id) != algo_names.end()){
		return algo_names[algo_id];
	}else{
		std::cerr << "broc::BTagAlgoNameProvider: b-tag algorithm ID " << algo_id << " not available. Available IDs: " << std::endl;
		for(std::map<int, std::string>::iterator algo = algo_names.begin();
			algo != algo_names.end();
			++algo){
			std::cerr << "ID: " << algo->first << " name: " << algo->second << std::endl;		
		}
		std::cerr << "exiting..." << std::endl;
		exit(1);
	}
}
