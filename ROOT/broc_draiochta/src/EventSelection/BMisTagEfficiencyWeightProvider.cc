#include "../../interface/EventSelection/BMisTagEfficiencyWeightProvider.h"

broc::BMisTagEfficiencyWeightProvider::BMisTagEfficiencyWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	jets = handle_holder->get_tight_jets();

	double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_BMisTag_unc");
	
	up = false;
	down = false;

	if(study_unc == 1){ down = true; }
	else if(study_unc == 2){ up = true; }
}

broc::BMisTagEfficiencyWeightProvider::~BMisTagEfficiencyWeightProvider()
{
}

double broc::BMisTagEfficiencyWeightProvider::get_Data_Prob(int UpDown)
{
  double Prob = 1;
  for(unsigned int i = 0; i < jets->size(); i++){
    if((*jets)[i].is_bjet()){
      Prob *= get_SF((*jets)[i].pt(),(*jets)[i].eta(), UpDown)*get_MisTag_eff((*jets)[i].pt(),(*jets)[i].eta(), UpDown);
    }
    else{
      Prob *= (1. - get_SF((*jets)[i].pt(),(*jets)[i].eta(),UpDown)*get_MisTag_eff((*jets)[i].pt(),(*jets)[i].eta(),UpDown));
    }
  }
  return Prob;
}

double broc::BMisTagEfficiencyWeightProvider::get_MC_Prob(int UpDown)
{
  double Prob = 1;
  for(unsigned int i = 0; i < jets->size(); i++){
    if((*jets)[i].is_bjet()){
      Prob *= get_MisTag_eff((*jets)[i].pt(),(*jets)[i].eta(),UpDown);
    }
    else{
      Prob *= (1. - get_MisTag_eff((*jets)[i].pt(),(*jets)[i].eta(),UpDown));
    }
  }
  return Prob;
}

double broc::BMisTagEfficiencyWeightProvider::get_SF(double pt, double eta, int UpDown)
{
  //CSVM
  double SFb = 1.;
  int ud = 0;
  if(up || UpDown == 1) ud = 1;
  else if(down || UpDown == -1) ud = -1;
  if(pt > 20){
    if(fabs(eta) < 0.8){
      switch (ud){
      case 0:
        SFb = ((1.07541+(0.00231827*pt))+(-4.74249e-06*(pt*pt)))+(2.70862e-09*(pt*(pt*pt)));
        break;
      case 1:
        SFb = ((1.18638+(0.00314148*pt))+(-6.68993e-06*(pt*pt)))+(3.89288e-09*(pt*(pt*pt)));
        break;
      case -1:
        SFb = ((0.964527+(0.00149055*pt))+(-2.78338e-06*(pt*pt)))+(1.51771e-09*(pt*(pt*pt)));
        break;
      }
    }
    else if(fabs(eta) < 1.6){
      switch (ud){
      case 0:
        SFb = ((1.05613+(0.00114031*pt))+(-2.56066e-06*(pt*pt)))+(1.67792e-09*(pt*(pt*pt)));
        break;
      case 1:
        SFb = ((1.16624+(0.00151884*pt))+(-3.59041e-06*(pt*pt)))+(2.38681e-09*(pt*(pt*pt)));
        break;
      case -1:
        SFb = ((0.946051+(0.000759584*pt))+(-1.52491e-06*(pt*pt)))+(9.65822e-10*(pt*(pt*pt)));
        break;
      }
    }
    else if(fabs(eta) < 2.4){
      switch (ud){
      case 0:
        SFb = ((1.05625+(0.000487231*pt))+(-2.22792e-06*(pt*pt)))+(1.70262e-09*(pt*(pt*pt)));
        break;
      case 1:
        SFb = ((1.15575+(0.000693344*pt))+(-3.02661e-06*(pt*pt)))+(2.39752e-09*(pt*(pt*pt)));
        break;
      case -1:
        SFb = ((0.956736+(0.000280197*pt))+(-1.42739e-06*(pt*pt)))+(1.0085e-09*(pt*(pt*pt)));
        break;
      }
    }
  }
  return SFb;
}  

double broc::BMisTagEfficiencyWeightProvider::get_MisTag_eff(double pt, double eta, int UpDown)
{
  double eff = 0.;
  double ud = 0.;
  if(up || UpDown == 1) ud = 1.;
  else if(down || UpDown == -1) ud = -1;

  //5.3.X TTbar MCatNLO
  if(fabs(eta) < 0.4){
    if(pt < 20){eff = 1.24 + ud*0.05*2;}
    else if(pt < 30){eff = 1.24 + ud*0.05;}
    else if(pt < 35){eff = 1.26 + ud*0.05;}
    else if(pt < 40){eff = 1.33 + ud*0.05;}
    else if(pt < 45){eff = 1.08 + ud*0.05;}
    else if(pt < 50){eff = 1.14 + ud*0.05;}
    else if(pt < 55){eff = 1.23 + ud*0.06;}
    else if(pt < 60){eff = 1.06 + ud*0.06;}
    else if(pt < 65){eff = 1.06 + ud*0.06;}
    else if(pt < 70){eff = 1.19 + ud*0.07;}
    else if(pt < 75){eff = 1.19 + ud*0.07;}
    else if(pt < 80){eff = 1.41 + ud*0.09;}
    else if(pt < 85){eff = 1.57 + ud*0.10;}
    else if(pt < 90){eff = 1.36 + ud*0.10;}
    else if(pt < 95){eff = 1.38 + ud*0.10;}
    else if(pt < 100){eff = 1.72 + ud*0.12;}
    else if(pt < 150){eff = 1.68 + ud*0.05;}
    else if(pt < 200){eff = 2.14 + ud*0.10;}
    else if(pt > 200){eff = 2.14 + ud*0.10*2;}
  }
  else if(fabs(eta) < 0.8){
    if(pt < 20){eff = 1.17 + ud*0.05*2;}
    else if(pt < 30){eff = 1.17 + ud*0.05;}
    else if(pt < 35){eff = 1.43 + ud*0.06;}
    else if(pt < 40){eff = 1.23 + ud*0.05;}
    else if(pt < 45){eff = 1.01 + ud*0.05;}
    else if(pt < 50){eff = 1.08 + ud*0.05;}
    else if(pt < 55){eff = 1.14 + ud*0.06;}
    else if(pt < 60){eff = 1.20 + ud*0.06;}
    else if(pt < 65){eff = 1.29 + ud*0.07;}
    else if(pt < 70){eff = 1.14 + ud*0.07;}
    else if(pt < 75){eff = 1.36 + ud*0.08;}
    else if(pt < 80){eff = 1.26 + ud*0.08;}
    else if(pt < 85){eff = 1.20 + ud*0.09;}
    else if(pt < 90){eff = 1.39 + ud*0.10;}
    else if(pt < 95){eff = 1.61 + ud*0.11;}
    else if(pt < 100){eff = 1.59 + ud*0.12;}
    else if(pt < 150){eff = 1.83 + ud*0.06;}
    else if(pt < 200){eff = 2.25 + ud*0.11;}
    else if(pt > 200){eff = 2.25 + ud*0.11*2;}
  }
  else if(fabs(eta) < 1.2){
    if(pt < 20){eff = 1.36 + ud*0.06*2;}
    else if(pt < 30){eff = 1.36 + ud*0.06;}
    else if(pt < 35){eff = 1.45 + ud*0.06;}
    else if(pt < 40){eff = 1.31 + ud*0.06;}
    else if(pt < 45){eff = 0.99 + ud*0.05;}
    else if(pt < 50){eff = 1.05 + ud*0.06;}
    else if(pt < 55){eff = 1.09 + ud*0.06;}
    else if(pt < 60){eff = 1.30 + ud*0.07;}
    else if(pt < 65){eff = 1.09 + ud*0.07;}
    else if(pt < 70){eff = 1.13 + ud*0.08;}
    else if(pt < 75){eff = 1.32 + ud*0.09;}
    else if(pt < 80){eff = 1.34 + ud*0.09;}
    else if(pt < 85){eff = 1.33 + ud*0.10;}
    else if(pt < 90){eff = 1.32 + ud*0.11;}
    else if(pt < 95){eff = 1.43 + ud*0.12;}
    else if(pt < 100){eff = 1.67 + ud*0.13;}
    else if(pt < 150){eff = 1.78 + ud*0.06;}
    else if(pt < 200){eff = 2.01 + ud*0.11;}
    else if(pt > 200){eff = 2.01 + ud*0.11*2;}
  }
  else if(fabs(eta) < 1.6){
    if(pt < 20){eff = 1.27 + ud*0.07*2;}
    else if(pt < 30){eff = 1.27 + ud*0.07;}
    else if(pt < 35){eff = 1.44 + ud*0.07;}
    else if(pt < 40){eff = 1.33 + ud*0.07;}
    else if(pt < 45){eff = 1.13 + ud*0.07;}
    else if(pt < 50){eff = 1.11 + ud*0.07;}
    else if(pt < 55){eff = 1.29 + ud*0.08;}
    else if(pt < 60){eff = 1.15 + ud*0.08;}
    else if(pt < 65){eff = 1.00 + ud*0.08;}
    else if(pt < 70){eff = 1.17 + ud*0.09;}
    else if(pt < 75){eff = 1.19 + ud*0.10;}
    else if(pt < 80){eff = 1.36 + ud*0.11;}
    else if(pt < 85){eff = 1.44 + ud*0.12;}
    else if(pt < 90){eff = 1.52 + ud*0.13;}
    else if(pt < 95){eff = 1.44 + ud*0.13;}
    else if(pt < 100){eff = 1.37 + ud*0.14;}
    else if(pt < 150){eff = 1.70 + ud*0.07;}
    else if(pt < 200){eff = 1.70 + ud*0.11;}
    else if(pt > 200){eff = 1.70 + ud*0.11*2;}
  }
  else if(fabs(eta) < 2.0){
    if(pt < 20){eff = 1.28 + ud*0.09*2;}
    else if(pt < 30){eff = 1.28 + ud*0.09;}
    else if(pt < 35){eff = 1.32 + ud*0.09;}
    else if(pt < 40){eff = 1.41 + ud*0.09;}
    else if(pt < 45){eff = 1.11 + ud*0.09;}
    else if(pt < 50){eff = 1.22 + ud*0.09;}
    else if(pt < 55){eff = 1.28 + ud*0.10;}
    else if(pt < 60){eff = 1.24 + ud*0.10;}
    else if(pt < 65){eff = 1.15 + ud*0.11;}
    else if(pt < 70){eff = 1.40 + ud*0.12;}
    else if(pt < 75){eff = 1.23 + ud*0.12;}
    else if(pt < 80){eff = 1.64 + ud*0.15;}
    else if(pt < 85){eff = 1.56 + ud*0.15;}
    else if(pt < 90){eff = 1.65 + ud*0.17;}
    else if(pt < 95){eff = 1.61 + ud*0.18;}
    else if(pt < 100){eff = 1.40 + ud*0.17;}
    else if(pt < 150){eff = 1.84 + ud*0.08;}
    else if(pt < 200){eff = 2.40 + ud*0.15;}
    else if(pt > 200){eff = 2.40 + ud*0.15*2;}
  }
  else if(fabs(eta) < 2.4){
    if(pt < 20){eff = 1.30 + ud*0.12*2;}
    else if(pt < 30){eff = 1.30 + ud*0.12;}
    else if(pt < 35){eff = 1.44 + ud*0.12;}
    else if(pt < 40){eff = 1.27 + ud*0.12;}
    else if(pt < 45){eff = 1.18 + ud*0.11;}
    else if(pt < 50){eff = 1.15 + ud*0.12;}
    else if(pt < 55){eff = 1.31 + ud*0.13;}
    else if(pt < 60){eff = 1.39 + ud*0.14;}
    else if(pt < 65){eff = 1.27 + ud*0.14;}
    else if(pt < 70){eff = 1.56 + ud*0.17;}
    else if(pt < 75){eff = 1.43 + ud*0.17;}
    else if(pt < 80){eff = 1.29 + ud*0.17;}
    else if(pt < 85){eff = 1.53 + ud*0.19;}
    else if(pt < 90){eff = 1.44 + ud*0.20;}
    else if(pt < 95){eff = 1.95 + ud*0.24;}
    else if(pt < 100){eff = 1.75 + ud*0.24;}
    else if(pt < 150){eff = 2.10 + ud*0.10;}
    else if(pt < 200){eff = 2.81 + ud*0.18;}
    else if(pt > 200){eff = 2.81 + ud*0.18*2;}
  }
  eff *= 0.01;
  return eff;
}

double broc::BMisTagEfficiencyWeightProvider::get_weight(int UpDown)
{
  double weight = 1.;
  if(do_not_reweight)
    weight = 1.;
  else{
    if(jets->size() >= 1){
      if(get_MC_Prob(UpDown) > 0){
        weight = get_Data_Prob(UpDown)/get_MC_Prob(UpDown);
      }
    }
  }
  return weight;
}
