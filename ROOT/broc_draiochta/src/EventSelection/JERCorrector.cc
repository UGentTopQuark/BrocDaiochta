#include "../../interface/EventSelection/JERCorrector.h"

broc::JERCorrector::JERCorrector()
{
	minus_shift = false;
	plus_shift = false;
	nominal_shift = false;
}

broc::JERCorrector::~JERCorrector()
{
}

void broc::JERCorrector::correctJER(mor::Jet *jet, double study_jer_unc)
{
	if(study_jer_unc == 1) minus_shift = true;
	if(study_jer_unc == 2) plus_shift = true;

	if(!plus_shift && !minus_shift) nominal_shift = true;

	double nominal = 0., up = 0., down = 0.;
	double genjet_eta = jet->eta_genjet();

	// eta dependent JER shifts
	if(fabs(genjet_eta) < 0.5){
		down = 0.979;
		nominal = 1.052;
		up = 1.126;
	}else if(fabs(genjet_eta) < 1.1){
		down = 0.99;
		nominal = 1.057;
		up = 1.125;
	}else if(fabs(genjet_eta) < 1.7){
		down = 1.017;
		nominal = 1.096;
		up = 1.176;
	}else if(fabs(genjet_eta) < 2.3){
		down = 1.014;
		nominal = 1.134;
		up = 1.256;
	}else if(fabs(genjet_eta) >= 2.3){
		down = 1.008;
		nominal = 1.288;
		up = 1.568;
	}

	double pT = jet->pt();
	double pTgen = jet->pt_genjet();

	double factor = 0.;
	if(minus_shift) factor = down;
	else if(plus_shift) factor = up;
	else factor = nominal;

	double pt_scale_unc = 0.0;
	if(pT > 10){
	  pt_scale_unc = std::max(0.0, (pTgen + factor*(pT - pTgen))/pT);
	}
	*jet *= pt_scale_unc;
}
