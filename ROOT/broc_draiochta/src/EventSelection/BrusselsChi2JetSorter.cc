#include "../../interface/EventSelection/BrusselsChi2JetSorter.h"

broc::BrusselsChi2JetSorter::BrusselsChi2JetSorter(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	this->electrons = handle_holder->get_tight_electrons();
	this->muons = handle_holder->get_tight_muons();
	this->jets = handle_holder->get_tight_jets();
	this->mets = handle_holder->get_selected_mets();

	jet_id1= -1;jet_id2=-1;jet_id3=-1;jet_id4=-1;
	min_chi2 = -1;

	min_lep_b_btag = handle_holder->get_cuts_set()->get_cut_value("min_lep_b_btag");

	double num_name_btag = handle_holder->get_cuts_set()->get_cut_value("name_btag");

	broc::BTagAlgoNameProvider btag_name_prov;
	if (num_name_btag != -1) name_btag = btag_name_prov.get_name((int) num_name_btag);
	else name_btag = "-1";

	std::string id = "_"+handle_holder->get_ident();
	histos1d["bDisc"] = handle_holder->get_histo_writer()->create_1d(("lep_b_bDiscriminator"+id).c_str(),"lep b bDiscriminator", 200,-1, 5, "leptonic b, bDiscriminator");
	histos1d["bDisc_real_b"] = handle_holder->get_histo_writer()->create_1d(("lep_b_bDiscriminator_real_b"+id).c_str(),"lep b bDiscriminator", 200,-1, 5, "leptonic b, bDiscriminator");
	histos1d["dR_jl"] = handle_holder->get_histo_writer()->create_1d(("lep_b_dR_jl"+id).c_str(),"lep b dR(b-jet,l)", 200,-1, 5, "M_{lb} dR(b-jet,l)");

	cnt["nevt"] = 0;
	cnt["n_j4_is_b"] = 0;
	cnt["n_j4_is_lep_b"] = 0;
	cnt["n_all_jets_correct"] = 0;	// allow W jet ambiguity
}

broc::BrusselsChi2JetSorter::~BrusselsChi2JetSorter()
{
	if(cnt["nevt"] > 0){
		std::cout << "correct assignment any b-jet: " << cnt["n_j4_is_b"]/cnt["nevt"] << std::endl;
		std::cout << "correct assignment leptonic b-jet: " << cnt["n_j4_is_lep_b"]/cnt["nevt"] << std::endl;
		std::cout << "lep b-jet among leading 4: " << cnt["lep_b_among_first_4"]/cnt["nevt"] << std::endl;
		std::cout << "had b-jet among leading 4: " << cnt["had_b_among_first_4"]/cnt["nevt"] << std::endl;
		std::cout << "any b-jet among leading 4: " << cnt["any_b_among_first_4"]/cnt["nevt"] << std::endl;
		std::cout << "lep b-jet in event: " << cnt["lep_b_in_event"]/cnt["nevt"] << std::endl;
	}
}

bool broc::BrusselsChi2JetSorter::reconstruct_ttbar()
{
	jet_id1= -1;jet_id2=-1;jet_id3=-1;jet_id4=-1;
	min_chi2 = -1;

	double MW = 84.6;
	double sigma_hadW = 10.3;
	double Mt = 176.5;
	double sigma_hadt = 15.7;

	int njets = jets->size();
	int max_njets = 4; 	// only 4 highest pt jets are considered

  	for(int j=0;j < njets && j < max_njets; ++j){
  	  for(int k=0;k < njets && k < max_njets; ++k){
  	    for(int l=0;l < njets && l < max_njets; ++l){
  	      for(int m=0;m < njets && m < max_njets; ++m){
  	      	if(j != k && j != l && k != l &&
			j != m && k != m && l != m){
  	      	  
  	      	  double hadronic_Mt = ((*jets)[l].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).mass();
  	      	  double hadronic_MW = ((*jets)[j].p4()+ (*jets)[k].p4()).mass();
  	      	  
  	      	  double chi2 = (((hadronic_MW-MW)*(hadronic_MW-MW))/(sigma_hadW*sigma_hadW))+(((hadronic_Mt-Mt)*(hadronic_Mt-Mt))/(sigma_hadt*sigma_hadt));
  	      	  
  	      	  if((chi2 < min_chi2) || (min_chi2 == -1)){
  	      	    jet_id1=l;
  	      	    jet_id2=j;
  	      	    jet_id3=k;
  	      	    jet_id4=m;	// b from leptonic top
  	      	    min_chi2= chi2;
  	      	  }
  	      	}
	      }
  	    }
  	  }
  	}

	if(verbose) debug(jet_id4);

	if(jet_id4 != -1 && min_lep_b_btag != -1){
		double lep_b_btag = (*jets)[jet_id4].bDiscriminator(name_btag);
		if(lep_b_btag < min_lep_b_btag){
		        jet_id1= -1;jet_id2=-1;jet_id3=-1;jet_id4=-1;
			min_chi2 = -1;
		}
	}

	if(jet_id4 != -1){
		if(electrons->begin() != electrons->end()){
			histos1d["dR_jl"]->Fill(ROOT::Math::VectorUtil::DeltaR((*jets)[jet_id4].p4(), electrons->begin()->p4()));
		}else if(muons->begin() != muons->end()){
			histos1d["dR_jl"]->Fill(ROOT::Math::VectorUtil::DeltaR((*jets)[jet_id4].p4(), muons->begin()->p4()));
		}
	}

	if(min_chi2 != -1)
		return true;
	else
		return false;
}

void broc::BrusselsChi2JetSorter::debug(int jet_id4)
{
	if(jet_id4 != -1)
		histos1d["bDisc"]->Fill((*jets)[jet_id4].bDiscriminator(name_btag));


	mor::TTbarGenEvent *ge = handle_holder->get_ttbar_gen_evt();

	if(ge->decay_channel() == 1 && ge->hadB_matched() && ge->lepB_matched() && jet_id4 != -1){
		bool lepB_there = false;
		bool hadB_there = false;
		for(int i = 0; i < jets->size(); ++i){
			if((*jets)[i].eta() == ge->lepB_jet()->eta() &&
		  	(*jets)[i].phi() == ge->lepB_jet()->phi()){
				lepB_there = true;
			}
			if((*jets)[i].eta() == ge->hadB_jet()->eta() &&
		  	(*jets)[i].phi() == ge->hadB_jet()->phi()){
				hadB_there = true;
			}
		}

		if(lepB_there && hadB_there){
	
			cnt["nevt"] = cnt["nevt"] + 1;
	
			if((*jets)[jet_id4].eta() == ge->lepB_jet()->eta() &&
				(*jets)[jet_id4].phi() == ge->lepB_jet()->phi()){
				cnt["n_j4_is_lep_b"] = cnt["n_j4_is_lep_b"] + 1;
				cnt["n_j4_is_b"] = cnt["n_j4_is_b"] + 1;
				histos1d["bDisc_real_b"]->Fill((*jets)[jet_id4].bDiscriminator(name_btag));
			}
			if((*jets)[jet_id4].eta() == ge->hadB_jet()->eta() &&
			  (*jets)[jet_id4].phi() == ge->hadB_jet()->phi()){
				cnt["n_j4_is_b"] = cnt["n_j4_is_b"] + 1;
				histos1d["bDisc_real_b"]->Fill((*jets)[jet_id4].bDiscriminator(name_btag));
			}
	
			for(int i = 0; i < 4; ++i){
				if((*jets)[i].eta() == ge->lepB_jet()->eta() &&
			  	(*jets)[i].phi() == ge->lepB_jet()->phi()){
					cnt["lep_b_among_first_4"]++;
					break;
				}
			}
			for(int i = 0; i < 4; ++i){
				if((*jets)[i].eta() == ge->hadB_jet()->eta() &&
			  	(*jets)[i].phi() == ge->hadB_jet()->phi()){
					cnt["had_b_among_first_4"]++;
					break;
				}
			}
			for(int i = 0; i < 4; ++i){
				if((*jets)[i].eta() == ge->hadB_jet()->eta() &&
			  	(*jets)[i].phi() == ge->hadB_jet()->phi()){
					cnt["any_b_among_first_4"]++;
					break;
				}
				if((*jets)[i].eta() == ge->lepB_jet()->eta() &&
			  	(*jets)[i].phi() == ge->lepB_jet()->phi()){
					cnt["any_b_among_first_4"]++;
					break;
				}
			}
			for(int i = 0; i < jets->size(); ++i){
				if((*jets)[i].eta() == ge->lepB_jet()->eta() &&
			  	(*jets)[i].phi() == ge->lepB_jet()->phi()){
					cnt["lep_b_in_event"]++;
					break;
				}
			}
		}

/*
		std::cout << "gen event lep b pt: " << ge->lepB_jet()->pt() << " eta: " << ge->lepB_jet()->eta() << " phi: " << ge->lepB_jet()->phi() << std::endl;

		for(std::vector<mor::Jet>::iterator jet = jets->begin();
			jet != jets->end();
			++jet){
			std::cout << "pt: " << jet->pt() << " eta: " << jet->eta() << " phi: " << jet->phi() << std::endl;
		}
*/

	}

}
