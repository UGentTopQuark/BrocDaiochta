#include "../../interface/EventSelection/Plots.h"

Plots::Plots()
{
	histos_booked = false;
	handle_holder = NULL;
}

Plots::~Plots()
{
}

void Plots::book_histos()
{
}

void Plots::prepare_objects()
{
}

void Plots::set_handles(eire::HandleHolder *handle_holder)
{
	this->jets = handle_holder->get_tight_jets();
	this->isolated_electrons = handle_holder->get_tight_electrons();
	this->loose_electrons = handle_holder->get_loose_electrons();
	this->isolated_muons = handle_holder->get_tight_muons();
	this->loose_muons = handle_holder->get_loose_muons();
	this->corrected_mets = handle_holder->get_selected_mets();
	this->gen_evt = handle_holder->get_ttbar_gen_evt();
	this->gen_particles = handle_holder->get_gen_particles();
	this->handle_holder = handle_holder;

	this->histo_writer = handle_holder->get_histo_writer();
	book_histos();
	histos_booked = true;
}

void Plots::set_handles(std::vector<mor::Jet>* jets,
		 std::vector<mor::Electron>* isolated_electrons,
		 std::vector<mor::Muon>* isolated_muons,
		 std::vector<mor::MET>* corrected_mets)
{
	this->jets = jets;
	this->isolated_electrons = isolated_electrons;
	this->isolated_muons = isolated_muons;
	this->corrected_mets = corrected_mets;
}

void Plots::set_handles(std::vector<mor::Jet>* jets,
			std::vector<mor::Electron>* isolated_electrons,
			std::vector<mor::Electron>* loose_electrons,
			std::vector<mor::Muon>* isolated_muons,
			std::vector<mor::Muon>* loose_muons,
			std::vector<mor::MET>* corrected_mets)
{
	this->jets = jets;
	this->isolated_electrons = isolated_electrons;
	this->loose_electrons = loose_electrons;
	this->isolated_muons = isolated_muons;
	this->loose_muons = loose_muons;
	this->corrected_mets = corrected_mets;
}

void Plots::plot()
{
	if(!histos_booked){
		std::cerr << "Plots.cc: Histos not booked " << std::endl; 
		book_histos();
		histos_booked = true;
	}
	plot_all();
}
