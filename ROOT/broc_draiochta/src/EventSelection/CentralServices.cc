#include "../../interface/EventSelection/CentralServices.h"

broc::CentralServices::CentralServices(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	handle_holder->set_services(this);

	ht_calc_ = NULL;
	mass_reco_ = NULL;
	dilepton_reco_ = NULL;
	evt_shape_ = NULL;
	event_classifier_ = NULL;
	mva_id_calc_ = 0;

	book();
}

broc::CentralServices::~CentralServices()
{
	if(ht_calc_){delete ht_calc_; ht_calc_ = NULL;}
	if(mass_reco_){delete mass_reco_; mass_reco_ = NULL;}
	if(dilepton_reco_){delete dilepton_reco_; dilepton_reco_ = NULL;}
	if(evt_shape_){delete evt_shape_; evt_shape_ = NULL;}
	if(event_classifier_){delete event_classifier_; event_classifier_ = NULL;}
	if(mva_id_calc_){delete mva_id_calc_; mva_id_calc_ = NULL;}
}

void broc::CentralServices::book()
{
	ht_calc_ = new HtCalculator(handle_holder);
	mass_reco_ = new MassReconstruction(handle_holder);
	dilepton_reco_ = new DiLeptonReconstructor(handle_holder);
	evt_shape_ = new tionscadaldorcha::EventShapeVariableCalculator(handle_holder);
	mva_id_calc_ = new broc::MVAElectronIDCalculator(handle_holder);

	if(handle_holder->get_config_reader()->get_bool_var("classify_events", "MVA", false)){
		event_classifier_ = new tionscadaldorcha::EventClassifier(handle_holder);
		event_classifier_->initialise();
	}
}

void broc::CentralServices::next_event()
{
	ht_calc_->next_event();
	mass_reco_->next_event();
	dilepton_reco_->next_event();
	evt_shape_->next_event();
	event_classifier_->next_event();
}
