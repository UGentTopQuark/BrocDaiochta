#include "../../interface/EventSelection/CutPrinter.h"

broc::CutPrinter::CutPrinter(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder, eire::ConfigReader *config_reader)
{
	this->config_reader = config_reader;
        this->handle_holder = handle_holder;
	this->cuts_set = cuts_set;

        cuts = NULL;

        enable_kinematic_fit_module = 1; //config_reader->get_bool_var("enable_kinematic_fit_module", "global", false);
        enable_hitfit_combi_seed = 1; //config_reader->get_bool_var("use_hitfit_combi_seed", "lhco_writer", false);

	cuts = new broc::Cuts(cuts_set, handle_holder);

        cuts_passed_counter = 0;
        cuts_overall_counter = 0;
	event_weight = 1;

	last_cutset = 0;

}

broc::CutPrinter::~CutPrinter()
{
	if(last_cutset){print_efficiency();}
        //if(cuts){delete cuts; cuts = NULL;}  // Calling the destructor prints the efficiencies twice, commenting out for now..
}

void broc::CutPrinter::calculate_efficiency(bool last_iter)
{
	last_cutset = last_iter;

	identifier=handle_holder->get_ident();

	event_weight = handle_holder->get_event_weight();
	// count events that passed cuts and those that were discarded                                                                                        
	
	cuts_overall_counter += handle_holder->get_failing_event_weight();
	
	if(cuts->cut()) return; // stop here if event does not pass cuts 
	
	// get the hitfit combi seed used for vetoing in the event processing
	hitfit_combi_seed = handle_holder->get_hitfit_combination();
	
        // skip the event if hitfit returns a -1 for the combination (failed to fit the event)                                                                                               
        if (enable_kinematic_fit_module && enable_hitfit_combi_seed && last_cutset){
          if(hitfit_combi_seed[0] == -1 || hitfit_combi_seed[0] == -2){return;}
        }

        cuts_passed_counter += event_weight;

}


void broc::CutPrinter::print_efficiency()
{
	std::cout << "=------------HITFIT-CUTS-----------=" << std::endl;
	std::cout << "identifier: " << identifier << std::endl;
	std::cout << "cuts_passed: " << std::setprecision(20) << cuts_passed_counter << std::endl;
	std::cout << "cuts_overall: " << std::setprecision(20) << cuts_overall_counter << std::endl;
	std::cout << "eff: " << cuts_passed_counter/(cuts_overall_counter) << std::endl;
	std::cout << "=----------------------------------------=" << std::endl;
}

