#include "../../interface/EventSelection/MassPlots.h"

MassPlots::MassPlots(eire::HandleHolder *handle_holder)
{
	id = "_"+handle_holder->get_ident();

	mass_reco = NULL;
	mass_jet_match = NULL;
	bjet_finder = NULL;
	ht_calc = NULL;
	gen_evt = NULL;

	ht = -1;
}

MassPlots::~MassPlots()
{
}

void MassPlots::set_bjet_finder(BJetFinder *bjet_finder)
{
	this->bjet_finder = bjet_finder;
}

void MassPlots::set_ht_calc(HtCalculator *ht_calc)
{
	this->ht_calc = ht_calc;
}

void MassPlots::set_gen_evt_handle(mor::TTbarGenEvent *gen_evt)
{
	this->gen_evt = gen_evt;
}

void MassPlots::set_mass_reco(MassReconstruction *mass_reco)
{
	this->mass_reco = mass_reco;
}

void MassPlots::set_mass_jet_match(MassJetMatch *mass_jet_match)
{
	this->mass_jet_match = mass_jet_match;
}

void MassPlots::plot_all()
{
	ht = ht_calc->get_ht();

	plot_masstop();
	plot_masstop_abseta_charge();
	
	//plot_recogenmatch_top();
	
	//plot_genmass_top();

        
//	plot_massjetmatch_top();

	plot_Wb_mass();
//	plot_minmass_1btag_top();
//	plot_minmass_2btag_top();
	plot_chimass_top();
//	plot_chimass_1btag_top();
//	plot_chimass_2btag_top();
	

	// The current W mass got from http://pdg.lbl.gov
//	double nominal_massW = 80.398;

 //  	plot_massW(nominal_massW);
}

void MassPlots::book_histos()
{
	const int top_mass_bins = 40;
   	const int top_mass_upper = 700;
   	const int W_mass_bins = 100;
   	const int W_mass_upper = 500;
	const int top_mass_bins_tf = 700;

	//truth matched mass histograms used to get sigma for chi² calculation
	// TQAF truth matching
	histos1d[("top_hadmass_genmatch"+id).c_str()]=histo_writer->create_1d(("top_hadmass_genmatch"+id).c_str(),"TQAF Gen matched hadronic top mass",25,0, top_mass_upper, "M^{had}_{truth} (GeV)");
	histos1d[("top_lepmass_genmatch"+id).c_str()]=histo_writer->create_1d(("top_lepmass_genmatch"+id).c_str(),"TQAF Gen matched leptonic top mass",25,0, top_mass_upper, "M^{lep}_{truth} (GeV)");
	histos1d[("top_hadmass_BQP"+id).c_str()]=histo_writer->create_1d(("top_hadmass_BQP"+id).c_str(),"TQAF Gen matched hadronic top mass",25,0, top_mass_upper, "M_{truth} (GeV)");
	histos1d[("top_hadmass_BW"+id).c_str()]=histo_writer->create_1d(("top_hadmass_BW"+id).c_str(),"TQAF Gen matched hadronic top mass",25,0, top_mass_upper, "M_{truth} (GeV)");
	histos1d[("top_lepmass_BLN"+id).c_str()]=histo_writer->create_1d(("top_lepmass_BLN"+id).c_str(),"TQAF Gen matched leptonic top mass",25,0, top_mass_upper, "M_{truth} (GeV)");
	histos1d[("W_hadmass_genmatch"+id).c_str()]=histo_writer->create_1d(("W_hadmass_genmatch"+id).c_str(),"TQAF Gen matched hadronic W mass",W_mass_bins,0, W_mass_upper, "M_{truth}(W) (GeV)");
	histos1d[("W_mass_QQ"+id).c_str()]=histo_writer->create_1d(("W_mass_QQ"+id).c_str(),"TQAF Gen level reconstructed W mass",W_mass_bins,0, W_mass_upper, "M_{truth}(W) (GeV)");

	//truth matched mass histograms used to get sigma for chi² calculation
	// private truth matching
	/*
	histos1d[("top_hadmass_RecoGenMatch"+id).c_str()]=histo_writer->create_1d(("top_hadmass_RecoGenMatch"+id).c_str(),"private gen matched hadronic top mass",100,0, top_mass_upper);
	histos1d[("top_lepmass_RecoGenMatch"+id).c_str()]=histo_writer->create_1d(("top_lepmass_RecoGenMatch"+id).c_str(),"private gen matched leptonic top mass",100,0, top_mass_upper);
	histos1d[("top_lepmass_RecoGenMatch_truthmatch"+id).c_str()]=histo_writer->create_1d(("top_lepmass_RecoGenMatch_truthmatch"+id).c_str(),"private gen matched leptonic top mass",100,0, top_mass_upper);
	histos1d[("W_hadmass_RecoGenMatch"+id).c_str()]=histo_writer->create_1d(("W_hadmass_RecoGenMatch"+id).c_str(),"private gen matched hadronic W mass",W_mass_bins,0, W_mass_upper);
	*/

	// Wb mass
	histos1d[("top_hadmass_Wb_0btag"+id).c_str()]=histo_writer->create_1d(("top_hadmass_Wb_0btag"+id).c_str(),"Wb hadronic top mass",top_mass_bins,0, top_mass_upper, "hadronic Wb mass (GeV)");
	histos1d[("top_lepmass_Wb_0btag"+id).c_str()]=histo_writer->create_1d(("top_lepmass_Wb_0btag"+id).c_str(),"Wb leptonic top mass",top_mass_bins,0, top_mass_upper, "leptonic Wb mass (GeV)");
	histos1d[("W_hadmass_Wb_0btag"+id).c_str()]=histo_writer->create_1d(("W_hadmass_Wb_0btag"+id).c_str(),"Wb hadronic W mass",W_mass_bins,0, W_mass_upper, "hadronic W mass in Wb reconstruction (GeV)");

	histos1d[("top_hadmass_Wb_1btag"+id).c_str()]=histo_writer->create_1d(("top_hadmass_Wb_1btag"+id).c_str(),"Wb hadronic top mass",top_mass_bins,0, top_mass_upper, "hadronic Wb mass 1 b-tag (GeV)");
	histos1d[("top_lepmass_Wb_1btag"+id).c_str()]=histo_writer->create_1d(("top_lepmass_Wb_1btag"+id).c_str(),"Wb leptonic top mass",top_mass_bins,0, top_mass_upper, "leptonic Wb mass 1 b-tag (GeV)");
	histos1d[("W_hadmass_Wb_1btag"+id).c_str()]=histo_writer->create_1d(("W_hadmass_Wb_1btag"+id).c_str(),"Wb hadronic W mass",W_mass_bins,0, W_mass_upper, "hadronic W mass in Wb 1 b-tag reconstruction (GeV)");

	histos1d[("top_hadmass_Wb_2btag"+id).c_str()]=histo_writer->create_1d(("top_hadmass_Wb_2btag"+id).c_str(),"Wb hadronic top mass",top_mass_bins,0, top_mass_upper);
	histos1d[("top_lepmass_Wb_2btag"+id).c_str()]=histo_writer->create_1d(("top_lepmass_Wb_2btag"+id).c_str(),"Wb leptonic top mass",top_mass_bins,0, top_mass_upper);
	histos1d[("W_hadmass_Wb_2btag"+id).c_str()]=histo_writer->create_1d(("W_hadmass_Wb_2btag"+id).c_str(),"Wb hadronic W mass",W_mass_bins,0, W_mass_upper);
	histos1d[("Wb_dR_W_1stb_2btag"+id).c_str()]=histo_writer->create_1d(("Wb_dR_W_1stb"+id).c_str(),"deltaR between hadronic W and highest b-jet",50,0,7);
	histos1d[("Wb_dR_W_2ndb_2btag"+id).c_str()]=histo_writer->create_1d(("Wb_dR_W_2ndb"+id).c_str(),"deltaR between hadronic W and second b-jet",50,0,7);

	// chi^2 from jet sorting
	histos1d[("chi2_wo_btag"+id).c_str()]=histo_writer->create_1d(("chi2_wo_btag"+id).c_str(),"#chi^{2} from jet sorting w/o b-tagging",600,0,30, "#chi^{2} from jet sorting");
	histos1d[("chi2_1_btag"+id).c_str()]=histo_writer->create_1d(("chi2_1_btag"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",125,0,10);
	histos1d[("chi2_2_btag"+id).c_str()]=histo_writer->create_1d(("chi2_2_btag"+id).c_str(),"#chi^{2} from jet sorting w/ 2 b-tag",125,0,10);
	histos1d[("chi2_wo_btag_prob"+id).c_str()]=histo_writer->create_1d(("chi2_wo_btag_prob"+id).c_str(),"#chi^{2} from jet sorting w/o b-tagging",275,0,1.1, "probability of #chi^{2} from jet sorting");
	histos1d[("chi2_1_btag_prob"+id).c_str()]=histo_writer->create_1d(("chi2_1_btag_prob"+id).c_str(),"#chi^{2} from jet sorting w/ 1 b-tag",250,0,1);
	histos1d[("chi2_2_btag_prob"+id).c_str()]=histo_writer->create_1d(("chi2_2_btag_prob"+id).c_str(),"#chi^{2} from jet sorting w/ 2 b-tag",250,0,1);


	histos1d[("M3_minus_Mlj_tf"+id).c_str()]=histo_writer->create_1d(("M3_minus_Mlj_tf"+id).c_str(),"Difference of M3 and Mass of additional jet and charged lepton",1000,-1000, 1000, "M3-M_{lj} (GeV)");
	histos1d[("M3_minus_Mlj_x_charge_tf"+id).c_str()]=histo_writer->create_1d(("M3_minus_Mlj_x_charge_tf"+id).c_str(),"Difference of M3 and Mass of additional jet and charged lepton times charge",1000,-1000, 1000, "charge(e) x (M3-M_{lj}) (GeV)");
	histos1d[("Mlj1_tf"+id).c_str()]=histo_writer->create_1d(("Mlj1_tf"+id).c_str(),"Mass of leading jet and charged lepton",top_mass_bins_tf,0, 1400, "M_{lj_{1}} (GeV)");
	histos1d[("Mlj4thM3jet_tf"+id).c_str()]=histo_writer->create_1d(("Mlj4thM3jet_tf"+id).c_str(),"Mass of fourth M3 jet and charged lepton",top_mass_bins_tf,0, 1400, "M_{lj_{n}} (GeV)");

	//plotting masses
	histos1d[("top_mass"+id).c_str()]=histo_writer->create_1d(("top_mass"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins,50, top_mass_upper, "M3 (GeV)");
	histos1d[("top_mass_tf"+id).c_str()]=histo_writer->create_1d(("top_mass_tf"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins_tf,0, 1400, "M3 (GeV)");
	histos1d[("top_mass_tf_barrel"+id).c_str()]=histo_writer->create_1d(("top_mass_tf_barrel"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins_tf,50, 1400, "M3 barrel (GeV)");
	histos1d[("top_mass_tf_endcap"+id).c_str()]=histo_writer->create_1d(("top_mass_tf_endcap"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins_tf,50, 1400, "M3 endcap (GeV)");
	histos1d[("top_mass_tf_dRje_le_03"+id).c_str()]=histo_writer->create_1d(("top_mass_tf_dRje_le_03"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins_tf,50, 1400, "M3 (GeV)");
	histos1d[("top_mass_tf_dRje_le_03_drop_jet"+id).c_str()]=histo_writer->create_1d(("top_mass_tf_dRje_le_03_drop_jet"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins_tf,50, 1400, "M3 (GeV)");
	histos1d[("top_mass_tf_dRje_ge_03"+id).c_str()]=histo_writer->create_1d(("top_mass_tf_dRje_ge_03"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins_tf,50, 1400, "M3 (GeV)");
	histos1d[("top_mass_tf_dRje_ge_03_validation"+id).c_str()]=histo_writer->create_1d(("top_mass_tf_dRje_ge_03_validation"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",top_mass_bins_tf,50, 1400, "M3 (GeV)");

	histos1d[("lep_b_mass_tf"+id).c_str()]=histo_writer->create_1d(("lep_b_mass_tf"+id).c_str(),"Invariant mass of b and charged hadron from leptonic top",top_mass_bins_tf,0, 1400, "M_{b lep} (GeV)");
	histos1d[("lep_b_chi2"+id).c_str()]=histo_writer->create_1d(("lep_b_chi2"+id).c_str(),"chi2 for invariant mass of b and charged hadron from leptonic top",500,0, 100, "#chi^{2}(M_{b lep}) (GeV)");
/*
*/
	//Correlation plots
	histos2d[("top_mass_vs_electron_abseta"+id).c_str()]=histo_writer->create_2d(("top_mass_vs_electron_abseta"+id).c_str(),"top_mass_vs_electron_abseta",top_mass_bins, 0, 1400, 50, -2.5, 2.5, "M3 mass (GeV)", "electron #eta");
	histos2d[("top_mass_vs_e_reliso"+id).c_str()]=histo_writer->create_2d(("top_mass_vs_e_reliso"+id).c_str(),"top_mass_vs_e_reliso",top_mass_bins, 0, 1400, 50, 0., 0.5, "M3 mass (GeV)", "electron rel. PF iso.");
	histos2d[("e_abseta_vs_e_reliso"+id).c_str()]=histo_writer->create_2d(("e_abseta_vs_e_reliso"+id).c_str(),"e_abseta_vs_e_reliso", 50, -2.5, 2.5, 50, 0., 0.5, "electron #eta", "electron rel. PF iso.");
	histos2d[("min_dR_vs_top_mass"+id).c_str()]=histo_writer->create_2d(("min_dR_vs_top_mass"+id).c_str(),"min_dR_vs_top_mass",200,0.,7.,top_mass_bins/10, 0, 500, "#Delta R(e,jet)", "M3 mass (GeV)");

	histos2d[("top_mass_vs_mu_abseta"+id).c_str()]=histo_writer->create_2d(("top_mass_vs_mu_abseta"+id).c_str(),"top_mass_vs_mu_abseta",top_mass_bins, 0, 1400, 42, 0, 2.1, "M3 (GeV)", "Muon |#eta|");
	histos2d[("top_mass_vs_mu_pt"+id).c_str()]=histo_writer->create_2d(("top_mass_vs_mu_pt"+id).c_str(),"top_mass_vs_mu_pt",top_mass_bins, 0, 1400, 200, 0, 200, "M3 (GeV)", "Muon p_{T} (GeV)");
	histos2d[("top_mass_vs_mu_dbeta_reliso"+id).c_str()]=histo_writer->create_2d(("top_mass_vs_mu_dbeta_reliso"+id).c_str(),"top_mass_vs_mu_dbeta_reliso",top_mass_bins, 0, 1400, 100, 0., 1.0, "M3 (GeV)", "Muon relative isolation");
	histos2d[("mu_abseta_vs_mu_dbeta_reliso"+id).c_str()]=histo_writer->create_2d(("mu_abseta_vs_mu_dbeta_reliso"+id).c_str(),"mu_abseta_vs_mu_dbeta_reliso", 42, 0, 2.1, 100, 0., 1.0, "muon |#eta|", "Muon relative isolation");
	histos2d[("top_mass_vs_min_mu_dR"+id).c_str()]=histo_writer->create_2d(("top_mass_vs_min_mu_dR"+id).c_str(),"top_mass_vs_min_mu_dR",top_mass_bins, 0, 1400,200,0.,2., "#Delta R(#mu,jet)", "M3 (GeV)");




	//Potential discriminating variables for template fit
	histos1d[("top_mass_x_mu_abseta_x_mu_charge"+id).c_str()]=histo_writer->create_1d(("top_mass_x_mu_abseta_x_mu_charge"+id).c_str(),"M3 by muon abseta by muon charge",500,-2500, 2500, "M3*muon |#eta|*muon charge  (GeV)");
	histos1d[("mu_abseta_x_mu_charge_div_top_mass"+id).c_str()]=histo_writer->create_1d(("mu_abseta_x_mu_charge_div_top_mass"+id).c_str(),"muon abseta by muon charge / M3",600,-0.06, 0.06, "(muon |#eta|*muon charge)/M3  (GeV)");
	histos1d[("top_mass_x_e_abseta_x_e_charge"+id).c_str()]=histo_writer->create_1d(("top_mass_x_e_abseta_x_e_charge"+id).c_str(),"M3 by electron abseta by electron charge",500,-2500, 2500, "M3*electron |#eta|*electron charge  (GeV)");
	histos1d[("e_abseta_x_e_charge_div_top_mass"+id).c_str()]=histo_writer->create_1d(("e_abseta_x_e_charge_div_top_mass"+id).c_str(),"electron abseta by electron charge / M3",600,-0.06, 0.06, "(electron |#eta|*electron charge)/M3  (GeV)");

	histos1d[("top_mass_minimisation"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation"+id).c_str(),"Hadronic top mass, minimal Mjjj-Mjln",top_mass_bins,0, top_mass_upper, "Min. Diff Mass [GeV}]");
	histos1d[("top_mass_minimisation_lep"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_lep"+id).c_str(),"Leptonic top mass, minimal Mjjj-Mjln",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_1btag"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_1btag"+id).c_str(),"Hadronic top mass, minimal Mjjj-Mjln,1 btagged jet",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_2btag"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_2btag"+id).c_str(),"Hadronic top mass, minimal Mjjj-Mjln,2 btagged jets",top_mass_bins,0, top_mass_upper);

	// top mass w/o b-tagging
	histos1d[("top_Had_chimass_max_btag_3"+id).c_str()]=histo_writer->create_1d(("top_Had_chimass_max_btag_3"+id).c_str(),"Hadronic top mass using chi squared sorting: events cut by btag > 3",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_1btag_chimass_max_btag_3"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass_max_btag_3"+id).c_str(),"Hadronic top mass, chi squared sorting 1 jet btagged: events cut by btag > 3",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_chimass_max_btag_5"+id).c_str()]=histo_writer->create_1d(("top_Had_chimass_max_btag_5"+id).c_str(),"Hadronic top mass using chi squared sorting: events cut by btag > 5",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_1btag_chimass_max_btag_5"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass_max_btag_5"+id).c_str(),"Hadronic top mass, chi squared sorting 1 jet btagged: events cut by btag > 5",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_chimass_max_btag_18"+id).c_str()]=histo_writer->create_1d(("top_Had_chimass_max_btag_18"+id).c_str(),"Hadronic top mass using chi squared sorting: events cut by btag > 1.8",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_1btag_chimass_max_btag_18"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass_max_btag_18"+id).c_str(),"Hadronic top mass, chi squared sorting 1 jet btagged: events cut by btag > 1.8",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_chimass"+id).c_str()]=histo_writer->create_1d(("top_Had_chimass"+id).c_str(),"Hadronic top mass using chi squared sorting",top_mass_bins,0, top_mass_upper,"M_{jjj} (GeV)" );
	histos1d[("top_Lep_chimass"+id).c_str()]=histo_writer->create_1d(("top_Lep_chimass"+id).c_str(),"Leptonic top mass using chi squared sorting",top_mass_bins,0, top_mass_upper, "M_{jl#nu} (GeV)");
	histos1d[("top_Had_1btag_chimass"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass"+id).c_str(),"Hadronic top mass, chi squared sorting 1 jet btagged",top_mass_bins,0, top_mass_upper,"M_{jjj} (GeV)");
	//jl 02.05.12: finer binning for the chi2 masses
        histos1d[("top_Had_1btag_chimass_tf"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass_tf"+id).c_str(),"Hadronic top mass, chi squared sorting 1 jet btagged",top_mass_bins_tf,0, 1400,"M_{jjj} (GeV)");
        histos1d[("top_Had_2btag_chimass_tf"+id).c_str()]=histo_writer->create_1d(("top_Had_2btag_chimass_tf"+id).c_str(),"Hadronic top mass, chi squared sorting, 2 jets btagged",top_mass_bins_tf,0, 1400,"M_{jjj} (GeV)");	
	histos1d[("top_Lep_1btag_chimass"+id).c_str()]=histo_writer->create_1d(("top_Lep_1btag_chimass"+id).c_str(),"Leptonic top mass, chi squared sorting 1 jet btagged",top_mass_bins,0, top_mass_upper, "M_{jl#nu} (GeV)");
	histos1d[("top_Had_2btag_chimass"+id).c_str()]=histo_writer->create_1d(("top_Had_2btag_chimass"+id).c_str(),"Hadronic top mass, chi squared sorting, 2 jets btagged",top_mass_bins,0, top_mass_upper,"M_{jjj} (GeV)");
	histos1d[("top_Lep_2btag_chimass"+id).c_str()]=histo_writer->create_1d(("top_Lep_2btag_chimass"+id).c_str(),"Leptonic top mass, chi squared sorting, 2 jets btagged",top_mass_bins,0, top_mass_upper, "M_{jl#nu} (GeV)");
	histos1d[("W_mass"+id).c_str()]=histo_writer->create_1d(("W_mass"+id).c_str(),"Invariant mass of 2 jets with vectorially summed pt closest to nominal W mass",W_mass_bins,0, W_mass_upper, "Hadronic W mass (GeV)");
	histos1d[("W_Had_chimass"+id).c_str()]=histo_writer->create_1d(("W_Had_chimass"+id).c_str(),"Hadronic W mass using chi squared method",W_mass_bins,0, W_mass_upper);
	histos1d[("W_Lep_chimass"+id).c_str()]=histo_writer->create_1d(("W_Lep_chimass"+id).c_str(),"Leptonic W mass using chi squared method",W_mass_bins,0, W_mass_upper, "Leptonic W mass (GeV)");
	histos1d[("W_Had_1btag_chimass"+id).c_str()]=histo_writer->create_1d(("W_Had_1btag_chimass"+id).c_str(),"Hadronic W mass, chi squared method 1 jet btagged",W_mass_bins,0, W_mass_upper, "Hadronic W mass (GeV)");
	histos1d[("W_Lep_1btag_chimass"+id).c_str()]=histo_writer->create_1d(("W_Lep_1btag_chimass"+id).c_str(),"Leptonic W mass, chi squared method 1 jet btagged",W_mass_bins,0, W_mass_upper, "Leptonic W mass (GeV)");
	histos1d[("W_Had_2btag_chimass"+id).c_str()]=histo_writer->create_1d(("W_Had_2btag_chimass"+id).c_str(),"Hadronic W mass, chi squared method 2 jets btagged",W_mass_bins,0, W_mass_upper, "Hadronic W mass (GeV)");
	histos1d[("W_Lep_2btag_chimass"+id).c_str()]=histo_writer->create_1d(("W_Lep_2btag_chimass"+id).c_str(),"Leptonic W mass, chi squared method 2 jets btagged",W_mass_bins,0, W_mass_upper, "Leptonic W mass (GeV)");

	//dR between reconstructed Had and Lep masses
	histos1d[("del_top_Had_Lep_chimass"+id).c_str()]=histo_writer->create_1d(("del_top_Had_Lep_chimass"+id).c_str(),"diff between chi squared had and lep mass", 50, -350, 350);
	histos1d[("del_top_Had_Lep_1btag_chimass"+id).c_str()]=histo_writer->create_1d(("del_top_Had_Lep_1btag_chimass"+id).c_str(),"diff between chi squared 1btag had and lep mass", 50, 0, 500);
	histos1d[("del_hadmass_lepmass_Wb_1btag"+id).c_str()]=histo_writer->create_1d(("del_hadmass_lepmass_Wb_1btag"+id).c_str(),"diff between Wb 1btag had and lep mass", 500, 0, 500);
	histos1d[("del_hadmass_lepmass_Wb_0btag"+id).c_str()]=histo_writer->create_1d(("del_hadmass_lepmass_Wb_0btag"+id).c_str(),"diff between Wb had and lep mass", 1000, 0, 1000);

	histos1d[("del_hadmass_lepmass_Wb_0btag_x_charge"+id).c_str()]=histo_writer->create_1d(("del_hadmass_lepmass_Wb_0btag_x_charge"+id).c_str(),"diff between Wb had and lep mass", 2000, -1000, 1000);

	//truth matching jets
	histos1d[("top_mass_3jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_3jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_2jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_2jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_1jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_1jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_0jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_0jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_lostjetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_lostjetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, top_mass_upper);

	histos1d[("top_mass_minimisation_3jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_3jetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_2jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_2jetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_1jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_1jetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_0jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_0jetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_lostjetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_lostjetmatched"+id).c_str(),"M3 mass_minimisation, jetmatched",top_mass_bins,0, top_mass_upper);

	histos1d[("top_Had_chimass_3jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_chimass_3jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_chimass_2jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_chimass_2jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_chimass_1jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_chimass_1jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_chimass_0jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_chimass_0jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_chimass_lostjetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_chimass_lostjetmatched"+id).c_str(),"chimass lostjetmatched",top_mass_bins,0, top_mass_upper);

	histos1d[("top_Had_1btag_chimass_3jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass_3jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_1btag_chimass_2jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass_2jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_1btag_chimass_1jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass_1jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_1btag_chimass_0jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass_0jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_1btag_chimass_lostjetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_1btag_chimass_lostjetmatched"+id).c_str(),"chimass lostjetmatched",top_mass_bins,0, top_mass_upper);

	histos1d[("top_Had_2btag_chimass_3jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_2btag_chimass_3jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_2btag_chimass_2jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_2btag_chimass_2jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_2btag_chimass_1jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_2btag_chimass_1jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_2btag_chimass_0jetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_2btag_chimass_0jetmatched"+id).c_str(),"M3 chimass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_Had_2btag_chimass_lostjetmatched"+id).c_str()]=histo_writer->create_1d(("top_Had_2btag_chimass_lostjetmatched"+id).c_str(),"chimass lostjetmatched",top_mass_bins,0, top_mass_upper);

	histos1d[("top_mass_genmatch_3jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_genmatch_3jetmatched"+id).c_str(),"mass 3 jets jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_genmatch_2jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_genmatch_2jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_genmatch_1jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_genmatch_1jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_genmatch_0jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_genmatch_0jetmatched"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_genmatch"+id).c_str()]=histo_writer->create_1d(("top_mass_genmatch"+id).c_str(),"M3 mass, jetmatched",top_mass_bins,0, top_mass_upper);

	histos1d[("top_mass_minimisation_1btag_3jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_1btag_3jetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_1btag_2jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_1btag_2jetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_1btag_1jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_1btag_1jetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_1btag_0jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_1btag_0jetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_1btag_lostjetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_1btag_lostjetmatched"+id).c_str(),"M3 mass_minimisation 1btag",top_mass_bins,0, top_mass_upper);

	histos1d[("top_mass_minimisation_2btag_3jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_2btag_3jetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_2btag_2jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_2btag_2jetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_2btag_1jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_2btag_1jetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_2btag_0jetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_2btag_0jetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, top_mass_upper);
	histos1d[("top_mass_minimisation_2btag_lostjetmatched"+id).c_str()]=histo_writer->create_1d(("top_mass_minimisation_2btag_lostjetmatched"+id).c_str(),"M3 mass_minimisation 2btag",top_mass_bins,0, top_mass_upper);

	// mass reconstruction plots
	// pt of hadronic and leptonic top
	histos1d[("pt_lept"+id).c_str()]=histo_writer->create_1d(("pt_lept"+id).c_str(),"pt leptonic top",(int)top_mass_bins/2, 0, 400, "p_{T} leptonic top (GeV)");
	histos1d[("pt_hadt"+id).c_str()]=histo_writer->create_1d(("pt_hadt"+id).c_str(),"pt hadronic top",(int)top_mass_bins/2, 0, 400, "p_{T} hadronic top (GeV)");
	histos2d[("pt_Had_vs_pt_Lep"+id).c_str()]=histo_writer->create_2d(("pt_Had_vs_pt_Lep"+id).c_str(),"pt had t vs pt lep t",(int)top_mass_bins/2, 0, 400, (int) top_mass_bins/2,0,top_mass_upper);

	histos1d[("top_pt_tf_dRje_le_03"+id).c_str()]=histo_writer->create_1d(("top_pt_tf_dRje_le_03"+id).c_str(),"pt hadronic top",(int)top_mass_bins/2, 0, 400, "p_{T} hadronic top (GeV)");
	histos1d[("top_mass_tf_pt_drop_jet"+id).c_str()]=histo_writer->create_1d(("top_mass_tf_pt_drop_jet"+id).c_str(),"pt hadronic top",(int)top_mass_bins/2, 0, 400, "p_{T} hadronic top (GeV)");
/*
*/

	// pt first jet vs mass
	histos2d[("pt_1st_jet_vs_Lep_chimass"+id).c_str()]=histo_writer->create_2d(("pt_1st_jet_vs_Lep_chimass"+id).c_str(),"1st jet pt vs had chimass",(int)top_mass_bins/2, 0, 400, top_mass_bins,0,top_mass_upper);
	histos2d[("pt_1st_jet_vs_Had_chimass"+id).c_str()]=histo_writer->create_2d(("pt_1st_jet_vs_Had_chimass"+id).c_str(),"1st jet pt vs had chimass",(int)top_mass_bins/2, 0, 400, top_mass_bins,0,top_mass_upper);
	histos2d[("pt_2nd_jet_vs_Lep_chimass"+id).c_str()]=histo_writer->create_2d(("pt_2nd_jet_vs_Lep_chimass"+id).c_str(),"2nd jet pt vs had chimass",(int)top_mass_bins/2, 0, 400, top_mass_bins,0,top_mass_upper);
	histos2d[("pt_2nd_jet_vs_Had_chimass"+id).c_str()]=histo_writer->create_2d(("pt_2nd_jet_vs_Had_chimass"+id).c_str(),"2nd jet pt vs had chimass",(int)top_mass_bins/2, 0, 400, top_mass_bins,0,top_mass_upper);

	histos2d[("top_Had_vs_Lep_chimass"+id).c_str()]=histo_writer->create_2d(("top_Had_vs_Lep_chimass"+id).c_str(),"correlation between hadronic and leptonic chimass",top_mass_bins, 0, top_mass_upper, top_mass_bins,0,top_mass_upper);
	histos2d[("top_Had_vs_Lep_chimass_1btag"+id).c_str()]=histo_writer->create_2d(("top_Had_vs_Lep_chimass_1btag"+id).c_str(),"correlation between hadronic and leptonic chimass",top_mass_bins, 0, top_mass_upper, top_mass_bins,0,top_mass_upper);
	histos2d[("top_Had_vs_Lep_chimass_2btag"+id).c_str()]=histo_writer->create_2d(("top_Had_vs_Lep_chimass_2btag"+id).c_str(),"correlation between hadronic and leptonic chimass",top_mass_bins, 0, top_mass_upper, top_mass_bins,0,top_mass_upper);

	histos2d[("top_mass_vs_ht"+id).c_str()]=histo_writer->create_2d(("top_mass_vs_ht"+id).c_str(),"M3 mass vs HT",50, 0, 900, 50,0,top_mass_upper);
	histos2d[("top_mass_minimisation_vs_ht"+id).c_str()]=histo_writer->create_2d(("top_mass_minimisation_vs_ht"+id).c_str(),"M3 mass (with min Mjjj-Mjln) vs HT",50, 0, 900, 50,0,top_mass_upper);

}

void MassPlots::plot_minmass_1btag_top()
{
	histos1d[("top_mass_minimisation_1btag"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
}

void MassPlots::plot_minmass_2btag_top()
{
	histos1d[("top_mass_minimisation_2btag"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
}

/*
//Plot top mass of truth matched top candidate, used to get sigma for min chi² measurement 
void MassPlots::plot_genmass_top()
{
	TtSemiLeptonicEvent::HypoKey& hypoClassKey = (TtSemiLeptonicEvent::HypoKey&) *hypoClassKeyHandle;
	// new PAT versions
	//TtEvent::HypoClassKey& hypoClassKey = (TtEvent::HypoClassKey&) *hypoClassKeyHandle;
	
	if( !semiLepEvt->isHypoAvailable(hypoClassKey) ){
		//std::cerr << "Hypothesis not available for this event" << std::endl;
		return;
	}
	if( !semiLepEvt->isHypoValid(hypoClassKey) ){
		//std::cerr << "Hypothesis not valid for this event" << std::endl;
		return;
	}

	const reco::Candidate* HadTop = semiLepEvt->hadronicTop(hypoClassKey);
	if(!(HadTop == NULL))
		histos1d[("top_hadmass_genmatch"+id).c_str()]->Fill(HadTop->mass()); 
	
	const reco::Candidate* LepTop = semiLepEvt->leptonicTop(hypoClassKey);
	if(!(LepTop == NULL))
		histos1d[("top_lepmass_genmatch"+id).c_str()]->Fill(LepTop->mass()); 
	
	const reco::Candidate* HadW = semiLepEvt->hadronicW(hypoClassKey);
	if(!(HadW == NULL))
		histos1d[("W_hadmass_genmatch"+id).c_str()]->Fill(HadW->mass());
	
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > Htop_candidate;
	const reco::GenParticle* genHadB = genEvt->hadronicDecayB();
	const reco::GenParticle* genHadP = genEvt->hadronicDecayQuark();
	const reco::GenParticle* genHadQ = genEvt->hadronicDecayQuarkBar();
	
	if(genHadB != NULL && genHadP != NULL && genHadQ != NULL){
		Htop_candidate = genHadB->p4()+genHadP->p4()+genHadQ->p4();
		histos1d[("top_hadmass_BQP"+id).c_str()]->Fill(Htop_candidate.mass()); 
	}
	
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > HWtop_candidate;
	const reco::GenParticle* genHadW = genEvt->hadronicDecayW();
	if(genHadB != NULL && genHadW != NULL ){
		HWtop_candidate = genHadB->p4()+genHadW->p4();
		histos1d[("top_hadmass_BW"+id).c_str()]->Fill(HWtop_candidate.mass()); 
	}
		
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > Ltop_candidate;
	const reco::GenParticle* genLepB = genEvt->leptonicDecayB();
	const reco::GenParticle* genLep = genEvt->lepton();
	const reco::GenParticle* genNu = genEvt->neutrino();
	if(genLepB != NULL && genLep != NULL && genNu != NULL){
		Ltop_candidate = genLepB->p4()+genLep->p4()+genNu->p4();
		histos1d[("top_lepmass_BLN"+id).c_str()]->Fill(Ltop_candidate.mass()); 
	}
	
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > W_candidate;
	if(genHadP != NULL && genHadQ != NULL){
		W_candidate = genHadP->p4()+genHadQ->p4(); 
		histos1d[("W_mass_QQ"+id).c_str()]->Fill(W_candidate.mass()); 
	}
	
}
*/
/*
void MassPlots::plot_recogenmatch_top()
{
	if(reco_gen_match == NULL || !reco_gen_match->is_geninfo_available())
		return;

	std::map<std::string, int> matching_map = reco_gen_match->get_matched_recojets_id();
	if(matching_map.find("Wquark1") == matching_map.end() ||
	   matching_map.find("Wquark2") == matching_map.end() ||
	   matching_map.find("hadb") == matching_map.end() ||
	   matching_map.find("lepb") == matching_map.end()){
		std::cout << "WARNING: plot_recogenmatch_top(): map not complete" << std::endl;
		return;
	}

	if(matching_map["Wquark1"] == -1 ||
	   matching_map["Wquark2"] == -1 ||
	   matching_map["hadb"] == -1 ||
	   matching_map["lepb"] == -1)
		return;

	if(corrected_mets->begin()->pz() == 0.0){
		return;
	}

	// hadronic gen matched mass
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > W_candidate;
	if(matching_map["Wquark1"] != -1 && matching_map["Wquark2"] != -1 && matching_map["hadb"] != -1){
		W_candidate = (*jets)[matching_map["Wquark1"]].p4() + (*jets)[matching_map["Wquark2"]].p4();
		ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > had_top_candidate;
		had_top_candidate = W_candidate + (*jets)[matching_map["hadb"]].p4();
		histos1d[("top_hadmass_RecoGenMatch"+id).c_str()]->Fill(had_top_candidate.mass());
		histos1d[("W_hadmass_RecoGenMatch"+id).c_str()]->Fill(W_candidate.mass());
	}

	if(matching_map["lepb"] != -1){
		if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
			if(reco_gen_match->lepton_from_top(0,"muon")){
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_top_candidate;
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_W_candidate;
				lep_W_candidate = isolated_muons->begin()->p4()+corrected_mets->begin()->p4();
				if(lep_W_candidate.mass() < 150.0){
					lep_top_candidate = (*jets)[matching_map["lepb"]].p4()+ lep_W_candidate;
					histos1d[("top_lepmass_RecoGenMatch"+id).c_str()]->Fill(lep_top_candidate.mass());
				}
			}
		}
			
		
		if(isolated_muons->size() == 0 && isolated_electrons->size() == 1){
			if(reco_gen_match->lepton_from_top(0,"electron")){				
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_top_candidate;
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_W_candidate;
				lep_W_candidate = isolated_electrons->begin()->p4()+corrected_mets->begin()->p4();
				if(lep_W_candidate.mass() < 150.0){
					lep_top_candidate = (*jets)[matching_map["lepb"]].p4()+ lep_W_candidate;
					histos1d[("top_lepmass_RecoGenMatch"+id).c_str()]->Fill(lep_top_candidate.mass());
				}
			}
		}
		
	}

	if(gen_match == NULL || !gen_match->is_ok())
		return;

	reco::GenParticle *neutrino = NULL;
	if(gen_match->t_decay_is_leptonic()){
		neutrino = gen_match->get_particle("Wplus_dp2");
	}else if(gen_match->tbar_decay_is_leptonic()){
		neutrino = gen_match->get_particle("Wminus_dp2");
	}

	if(ROOT::Math::VectorUtil::DeltaR(corrected_mets->begin()->p4(), neutrino->p4()) > 0.3)
		return;

	if(matching_map["lepb"] != -1){
		if(isolated_muons->size() == 1 && isolated_electrons->size() == 0){
			if(reco_gen_match->lepton_from_top(0,"muon")){
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_top_candidate;
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_W_candidate;
				lep_W_candidate = isolated_muons->begin()->p4()+corrected_mets->begin()->p4();

				if(lep_W_candidate.mass() < 150.0){
					lep_top_candidate = (*jets)[matching_map["lepb"]].p4()+ lep_W_candidate;
					histos1d[("top_lepmass_RecoGenMatch_truthmatch"+id).c_str()]->Fill(lep_top_candidate.mass());
				}
			}
		}
			
		if(isolated_muons->size() == 0 && isolated_electrons->size() == 1){
			if(reco_gen_match->lepton_from_top(0,"electron")){				
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_top_candidate;
				ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lep_W_candidate;
				lep_W_candidate = isolated_electrons->begin()->p4()+corrected_mets->begin()->p4();
				if(lep_W_candidate.mass() < 150.0){
					lep_top_candidate = (*jets)[matching_map["lepb"]].p4()+ lep_W_candidate;
					histos1d[("top_lepmass_RecoGenMatch_truthmatch"+id).c_str()]->Fill(lep_top_candidate.mass());
				}
			}
		}
		
	}
}

*/

//Find combination of jets which minimise chi² and use these to plot mass
void MassPlots::plot_chimass_top()
{
	histos1d[("top_Had_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
	histos1d[("W_Had_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadWmass());
	
	histos1d[("top_Lep_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepTmass());
	histos1d[("W_Lep_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepWmass());
	histos2d[("top_Had_vs_Lep_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass(), mass_reco->calculate_chilepTmass());
	
	histos1d[("chi2_wo_btag"+id).c_str()]->Fill(mass_reco->get_chi2()/2.0);
	histos1d[("chi2_wo_btag_prob"+id).c_str()]->Fill(1-TMath::Prob(mass_reco->get_chi2(),2));


	histos1d[("del_top_Had_Lep_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass()-mass_reco->calculate_chilepTmass());
	/*
	histos1d[("del_top_Had_Lep_1btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top() - mass_reco->calculate_chilepTmass_1btag());

	//mass plots of events which will be cut if btag cut 3 applied
	//
	// FIXME: this section is BROKEN, min_bjet_cuts are not set properly
	std::vector<double> set_min_btag (1,3);
//	bjet_finder->set_min_btag_value(set_min_btag);		
	std::vector<std::pair<int,double> > *btag_ids_this_algo = bjet_finder->get_btag_sorted_jets("trackCountingHighEffBJetTags");
	if(btag_ids_this_algo->begin() == btag_ids_this_algo->end()){
		histos1d[("top_Had_chimass_max_btag_3"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
		histos1d[("top_Had_1btag_chimass_max_btag_3"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top());
	}
	//sys eff -
	set_min_btag.assign(1,5);
//	bjet_finder->set_min_btag_value(set_min_btag);		
	btag_ids_this_algo = bjet_finder->get_btag_sorted_jets("trackCountingHighEffBJetTags");
	if(btag_ids_this_algo->begin() == btag_ids_this_algo->end()){
		histos1d[("top_Had_chimass_max_btag_5"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
		histos1d[("top_Had_1btag_chimass_max_btag_5"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top());
	}
	//sys eff +
	set_min_btag.assign(1,1.8);
//	bjet_finder->set_min_btag_value(set_min_btag);		
	btag_ids_this_algo = bjet_finder->get_btag_sorted_jets("trackCountingHighEffBJetTags");
	if(btag_ids_this_algo->begin() == btag_ids_this_algo->end()){
		histos1d[("top_Had_chimass_max_btag_18"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
		histos1d[("top_Had_1btag_chimass_max_btag_18"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top());
	}
	*/

	if(jets->size() > 2){
		histos2d[("pt_1st_jet_vs_Lep_chimass"+id).c_str()]->Fill((*jets)[0].pt(), mass_reco->calculate_chilepTmass());
		histos2d[("pt_1st_jet_vs_Had_chimass"+id).c_str()]->Fill((*jets)[0].pt(), mass_reco->calculate_chihadTmass());
		histos2d[("pt_2nd_jet_vs_Lep_chimass"+id).c_str()]->Fill((*jets)[1].pt(), mass_reco->calculate_chilepTmass());
		histos2d[("pt_2nd_jet_vs_Had_chimass"+id).c_str()]->Fill((*jets)[1].pt(), mass_reco->calculate_chihadTmass());
	}

	std::vector<int>* reco_mass_ids = mass_reco->get_ids_chi2();
	if(reco_mass_ids->size() <= 4 ||
	   (*reco_mass_ids)[0] == -1 || 
	   (*reco_mass_ids)[1] == -1 || 
	   (*reco_mass_ids)[2] == -1 || 
	   (*reco_mass_ids)[3] == -1 ||
	   jets->size() < 4)
		return;

	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > lept;
	ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> > hadt;

	if(isolated_muons->size() == 1 && isolated_electrons->size() ==0)
		lept = (*jets)[(*reco_mass_ids)[3]].p4() + isolated_muons->begin()->p4() + corrected_mets->begin()->p4();
	else if(isolated_muons->size() == 0 && isolated_electrons->size() ==1)
		lept = (*jets)[(*reco_mass_ids)[3]].p4() + isolated_muons->begin()->p4() + corrected_mets->begin()->p4();
	else
		return;

	hadt = (*jets)[(*reco_mass_ids)[0]].p4() + (*jets)[(*reco_mass_ids)[1]].p4() + (*jets)[(*reco_mass_ids)[2]].p4();

	histos1d[("pt_lept"+id).c_str()]->Fill(lept.pt());
	histos1d[("pt_hadt"+id).c_str()]->Fill(hadt.pt());
	histos2d[("pt_Had_vs_pt_Lep"+id).c_str()]->Fill(hadt.pt(), lept.pt());
}

//Find combination of jets which minimise chi² and use these to plot mass
void MassPlots::plot_chimass_1btag_top()
{
	histos1d[("top_Had_1btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top());
	histos1d[("top_Had_1btag_chimass_tf"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top()); //jl 02.05.12
	histos1d[("top_Lep_1btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepTmass_1btag());

	histos1d[("W_Had_1btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadWmass_1btag());
	histos1d[("W_Lep_1btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepWmass_1btag());

	histos2d[("top_Had_vs_Lep_chimass_1btag"+id).c_str()]->Fill(mass_reco->calculate_chimass_1btag_top(), mass_reco->calculate_chilepTmass_1btag());

	histos1d[("chi2_1_btag"+id).c_str()]->Fill(mass_reco->get_chi2_1btag()/3.0);
	histos1d[("chi2_1_btag_prob"+id).c_str()]->Fill(1-TMath::Prob(mass_reco->get_chi2_1btag(),3));
}

//Find combination of jets which minimise chi² and use these to plot mass
void MassPlots::plot_chimass_2btag_top()
{
	histos1d[("top_Had_2btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chimass_2btag_top());
        histos1d[("top_Had_2btag_chimass_tf"+id).c_str()]->Fill(mass_reco->calculate_chimass_2btag_top()); //jl 02.05.12
	histos1d[("top_Lep_2btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepTmass_2btag());

	histos1d[("W_Had_2btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chihadWmass_2btag());
	histos1d[("W_Lep_2btag_chimass"+id).c_str()]->Fill(mass_reco->calculate_chilepWmass_2btag());

	histos2d[("top_Had_vs_Lep_chimass_2btag"+id).c_str()]->Fill(mass_reco->calculate_chimass_2btag_top(), mass_reco->calculate_chilepTmass_2btag());

	histos1d[("chi2_2_btag"+id).c_str()]->Fill(mass_reco->get_chi2_2btag()/3.0);
	histos1d[("chi2_2_btag_prob"+id).c_str()]->Fill(1-TMath::Prob(mass_reco->get_chi2_2btag(),3));
}

// Plot the invariant mass of the two jets which best corresponds to the nominal value for the W mass, set in plot()
void MassPlots::plot_massW(double nominal_massW)
{
	histos1d[("W_mass"+id).c_str()]->Fill(mass_reco->calculate_massW(nominal_massW));
}

//M3: Plot the invariant mass of the three jets with the highest sum pt for the final selection
void MassPlots::plot_masstop()
{
	if(ht == -1)
		ht = ht_calc->get_ht();
	
	histos1d[("top_mass"+id).c_str()]->Fill(mass_reco->calculate_M3());
	histos1d[("lep_b_mass_tf"+id).c_str()]->Fill(mass_reco->calculate_lep_b_mass());
	histos1d[("lep_b_chi2"+id).c_str()]->Fill(mass_reco->get_lep_b_chi2());
	histos1d[("top_mass_tf"+id).c_str()]->Fill(mass_reco->calculate_M3());
	histos1d[("M3_minus_Mlj_tf"+id).c_str()]->Fill(mass_reco->calculate_M3_minus_Mlj());
	histos1d[("Mlj1_tf"+id).c_str()]->Fill(mass_reco->calculate_Mlj1());
	histos1d[("Mlj4thM3jet_tf"+id).c_str()]->Fill(mass_reco->calculate_Mlj4thM3jet());

	//histos2d[("top_mass_vs_ht"+id).c_str()]->Fill(ht,mass_reco->calculate_M3());
	
	//histos1d[("top_mass_minimisation"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
	//histos1d[("top_mass_minimisation_lep"+id).c_str()]->Fill(mass_reco->calculate_min_diff_lepTmass());
	//histos2d[("top_mass_minimisation_vs_ht"+id).c_str()]->Fill(ht,mass_reco->calculate_min_diff_M3());

	if(isolated_electrons->size() > 0){ 
		histos2d[("top_mass_vs_electron_abseta"+id).c_str()]->Fill(mass_reco->calculate_M3(), std::abs(isolated_electrons->begin()->eta()));
		histos1d[("M3_minus_Mlj_x_charge_tf"+id).c_str()]->Fill(mass_reco->calculate_M3_minus_Mlj()*isolated_electrons->begin()->charge());
		if(std::abs(isolated_electrons->begin()->eta()) < 1.5){
			histos1d[("top_mass_tf_barrel"+id).c_str()]->Fill(mass_reco->calculate_M3());
		}else{
			histos1d[("top_mass_tf_endcap"+id).c_str()]->Fill(mass_reco->calculate_M3());
		}
	}else if(isolated_muons->size() > 0){ 
		histos1d[("M3_minus_Mlj_x_charge_tf"+id).c_str()]->Fill(mass_reco->calculate_M3_minus_Mlj()*isolated_muons->begin()->charge());
		histos2d[("top_mass_vs_mu_abseta"+id).c_str()]->Fill(mass_reco->calculate_M3(), fabs(isolated_muons->begin()->eta()));
		histos2d[("top_mass_vs_mu_pt"+id).c_str()]->Fill(mass_reco->calculate_M3(), isolated_muons->begin()->pt());
		histos2d[("top_mass_vs_mu_dbeta_reliso"+id).c_str()]->Fill(mass_reco->calculate_M3(), isolated_muons->begin()->dbeta_PFrelIso());
		histos2d[("mu_abseta_vs_mu_dbeta_reliso"+id).c_str()]->Fill(fabs(isolated_muons->begin()->eta()), isolated_muons->begin()->dbeta_PFrelIso());

		double min_mu_dR = -1;
		for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();
		    jet_iter!=jets->end();
		    ++jet_iter)
			{
				double dR = ROOT::Math::VectorUtil::DeltaR(isolated_muons->begin()->p4(),jet_iter->p4());
				if ((dR < min_mu_dR) || (min_mu_dR == -1)){
					min_mu_dR = dR;
				}
			}
		histos2d[("top_mass_vs_min_mu_dR"+id).c_str()]->Fill(mass_reco->calculate_M3(), min_mu_dR);
		
		if(std::abs(isolated_muons->begin()->eta()) < 1.5){
			histos1d[("top_mass_tf_barrel"+id).c_str()]->Fill(mass_reco->calculate_M3());
		}else{
			histos1d[("top_mass_tf_endcap"+id).c_str()]->Fill(mass_reco->calculate_M3());
		}
	}

	double min_dR = -1;
	int min_dR_jet_id = -1;
	for(std::vector<mor::Electron>::iterator e_iter = isolated_electrons->begin();
	    e_iter != isolated_electrons->end();
	    e_iter++)
	{

		int njet = 0;
		for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();
		    jet_iter!=jets->end();
		    ++jet_iter)
		{
			double dR = ROOT::Math::VectorUtil::DeltaR(e_iter->p4(),jet_iter->p4());
			if ((dR < min_dR) || (min_dR == -1)){
				min_dR = dR;
				min_dR_jet_id = njet; 
			}
			++njet;
		}
		histos2d[("min_dR_vs_top_mass"+id).c_str()]->Fill(min_dR, mass_reco->calculate_M3());
		histos2d[("top_mass_vs_e_reliso"+id).c_str()]->Fill(mass_reco->calculate_M3(), e_iter->PFrelIso());
		histos2d[("e_abseta_vs_e_reliso"+id).c_str()]->Fill(std::abs(e_iter->eta()), e_iter->PFrelIso());

	}




	double mass = -1;
	
	int njets = jets->size();
	if(jets->size() >= 4){
	double max_pt=-1;
	int jet_id1=-1,jet_id2=-1, jet_id3=-1;
	double old_max_pt=-1;
	int old_jet_id1=-1,old_jet_id2=-1, old_jet_id3=-1;
        // Loop over all 3 jet combinations to see which has highest pt
        for(int i=0;i < njets; ++i){
          for(int j=0;j < njets; ++j){
            for(int k=0;k < njets; ++k){
              if(i != j && i != k && j != k){
                double current_pt = ((*jets)[i].p4()+ (*jets)[j].p4()+ (*jets)[k].p4()).pt();

		if(i != min_dR_jet_id && j != min_dR_jet_id && k != min_dR_jet_id){
                	if(current_pt > max_pt){
                	  jet_id1=i;
                	  jet_id2=j;
                	  jet_id3=k;
                	  max_pt=current_pt;
                	}
		}
                	if(current_pt > old_max_pt){
                	  old_jet_id1=i;
                	  old_jet_id2=j;
                	  old_jet_id3=k;
                	  old_max_pt=current_pt;
                	}
              }
            }
          }
        }
		mass = ((*jets)[jet_id1].p4()+ (*jets)[jet_id2].p4()+ (*jets)[jet_id3].p4()).mass();


		if(min_dR < 0.3){
			histos1d[("top_mass_tf_dRje_le_03"+id).c_str()]->Fill(mass_reco->calculate_M3());
			histos1d[("top_pt_tf_dRje_le_03"+id).c_str()]->Fill(((*jets)[old_jet_id1].p4()+ (*jets)[old_jet_id2].p4()+ (*jets)[old_jet_id3].p4()).pt());

			if(jets->size() >= 5){
				histos1d[("top_mass_tf_dRje_le_03_drop_jet"+id).c_str()]->Fill(mass);
				histos1d[("top_mass_tf_pt_drop_jet"+id).c_str()]->Fill(((*jets)[jet_id1].p4()+ (*jets)[jet_id2].p4()+ (*jets)[jet_id3].p4()).pt());
			}

		}else{
			histos1d[("top_mass_tf_dRje_ge_03"+id).c_str()]->Fill(mass_reco->calculate_M3());
			histos1d[("top_mass_tf_dRje_ge_03_validation"+id).c_str()]->Fill(((*jets)[old_jet_id1].p4()+ (*jets)[old_jet_id2].p4()+ (*jets)[old_jet_id3].p4()).mass());
		}
	}
/*
*/
}
void MassPlots::plot_masstop_abseta_charge()
{
	double m3 = mass_reco->calculate_M3();

        for(std::vector<mor::Muon>::iterator isolated_mu = isolated_muons->begin();
            isolated_mu!=isolated_muons->end();
            ++isolated_mu){
		double abseta = fabs(isolated_mu->Eta());
		histos1d[("top_mass_x_mu_abseta_x_mu_charge"+id).c_str()]->Fill(m3*abseta*isolated_mu->charge());
		histos1d[("mu_abseta_x_mu_charge_div_top_mass"+id).c_str()]->Fill((abseta*isolated_mu->charge())/m3);
	}

        for(std::vector<mor::Electron>::iterator isolated_e = isolated_electrons->begin();isolated_e != is\
olated_electrons->end();++isolated_e){
		double abseta = fabs(isolated_e->Eta());
		histos1d[("top_mass_x_e_abseta_x_e_charge"+id).c_str()]->Fill(m3*abseta*isolated_e->charge());
		histos1d[("e_abseta_x_e_charge_div_top_mass"+id).c_str()]->Fill((abseta*isolated_e->charge())/m3);
	}
}

void MassPlots::plot_Wb_mass()
{
	if(mass_reco == NULL)
		return;

	histos1d[("top_hadmass_Wb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_top());
	histos1d[("W_hadmass_Wb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_W_mass());
	histos1d[("top_lepmass_Wb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_lepTmass_top_2btag());
	histos1d[("Wb_dR_W_1stb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_dR_W_bjet1());
	histos1d[("Wb_dR_W_2ndb_2btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_dR_W_bjet2());

	histos1d[("top_hadmass_Wb_1btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_hadTmass_top_1btag());
	histos1d[("top_lepmass_Wb_1btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_lepTmass_top_1btag());
	histos1d[("W_hadmass_Wb_1btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_hadWmass_1btag());

	histos1d[("top_hadmass_Wb_0btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_hadTmass_top_0btag());
	histos1d[("top_lepmass_Wb_0btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_lepTmass_top_0btag());
	histos1d[("W_hadmass_Wb_0btag"+id).c_str()]->Fill(mass_reco->calculate_Wb_hadWmass_0btag());

	double charge = 1.;
	if(isolated_electrons->size() == 1){ charge = isolated_electrons->begin()->charge(); }
	else if(isolated_muons->size() == 1){ charge = isolated_muons->begin()->charge(); }

	histos1d[("del_hadmass_lepmass_Wb_0btag"+id).c_str()]->Fill(std::abs(mass_reco->calculate_Wb_hadTmass_top_0btag() - mass_reco->calculate_Wb_lepTmass_top_0btag()));
	histos1d[("del_hadmass_lepmass_Wb_0btag_x_charge"+id).c_str()]->Fill(charge*std::abs(mass_reco->calculate_Wb_hadTmass_top_0btag() - mass_reco->calculate_Wb_lepTmass_top_0btag()));
	histos1d[("del_hadmass_lepmass_Wb_1btag"+id).c_str()]->Fill(std::abs(mass_reco->calculate_Wb_hadTmass_top_1btag() - mass_reco->calculate_Wb_lepTmass_top_1btag()));
}

void MassPlots::plot_massjetmatch_top()
{
	if(mass_jet_match == NULL)
		return;
		
	//for jet matching
	//top mass
	
	switch(mass_jet_match->nmatches_m3()){
		case 0:
			histos1d[("top_mass_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
		case 1:
			histos1d[("top_mass_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
		case 2:
			histos1d[("top_mass_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
		case 3:
			histos1d[("top_mass_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
		default:
			histos1d[("top_mass_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_M3());
			break;
	}
	
	//   //top mass minimisation
	switch(mass_jet_match->nmatches_min_diff_m3()){
		case 0:
			histos1d[("top_mass_minimisation_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
		case 1:
			histos1d[("top_mass_minimisation_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
		case 2:
			histos1d[("top_mass_minimisation_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
		case 3:
			histos1d[("top_mass_minimisation_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
		default:
			histos1d[("top_mass_minimisation_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3());
			break;
	}

	//chimass
	switch(mass_jet_match->nmatches_chi2()){
		case 0:
			histos1d[("top_Had_chimass_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
		case 1:
			histos1d[("top_Had_chimass_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
		case 2:
			histos1d[("top_Had_chimass_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
		case 3:
			histos1d[("top_Had_chimass_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
		default:
			histos1d[("top_Had_chimass_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass());
			break;
	}

	//chimass 1btag
	switch(mass_jet_match->nmatches_chi2_1btag()){
		case 0:
			histos1d[("top_Had_1btag_chimass_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
		case 1:
			histos1d[("top_Had_1btag_chimass_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
		case 2:
			histos1d[("top_Had_1btag_chimass_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
		case 3:
			histos1d[("top_Had_1btag_chimass_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
		default:
			histos1d[("top_Had_1btag_chimass_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_1btag());
			break;
	}

	//chimass 2btag
	switch(mass_jet_match->nmatches_chi2_2btag()){
		case 0:
			histos1d[("top_Had_2btag_chimass_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
		case 1:
			histos1d[("top_Had_2btag_chimass_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
		case 2:
			histos1d[("top_Had_2btag_chimass_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
		case 3:
			histos1d[("top_Had_2btag_chimass_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
		default:
			histos1d[("top_Had_2btag_chimass_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_chihadTmass_2btag());
			break;
	}

	//minmass 1btag
	switch(mass_jet_match->nmatches_min_diff_m3_1btag()){
		case 0:
			histos1d[("top_mass_minimisation_1btag_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
		case 1:
			histos1d[("top_mass_minimisation_1btag_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
		case 2:
			histos1d[("top_mass_minimisation_1btag_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
		case 3:
			histos1d[("top_mass_minimisation_1btag_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
		default:
			histos1d[("top_mass_minimisation_1btag_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_1btag_top());
			break;
	}
		
	//minmass 2btag
	switch(mass_jet_match->nmatches_min_diff_m3_2btag()){
		case 0:
			histos1d[("top_mass_minimisation_2btag_0jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
		case 1:
			histos1d[("top_mass_minimisation_2btag_1jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
		case 2:
			histos1d[("top_mass_minimisation_2btag_2jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
		case 3:
			histos1d[("top_mass_minimisation_2btag_3jetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
		default:
			histos1d[("top_mass_minimisation_2btag_lostjetmatched"+id).c_str()]->Fill(mass_reco->calculate_minmass_2btag_top());
			break;
	}
}
