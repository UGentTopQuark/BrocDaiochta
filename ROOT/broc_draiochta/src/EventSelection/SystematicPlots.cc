// src/EventSelection/SystematicPlots.cc
#include "../../interface/EventSelection/SystematicPlots.h"
#include "../../interface/MorObjects/MPrimaryVertex.h"



SystematicPlots::SystematicPlots(std::string ident)
{
  id = ident;
}

SystematicPlots::~SystematicPlots()
{
}

void SystematicPlots::plot_all()
{
  plot_systematics();
}

void SystematicPlots::book_histos()
{
  int top_mass_bins = 260;
  const int top_mass_upper = 700;


  //histos1d[("nprimary_vertices"+id).c_str()]=histo_writer->create_1d(("nprimary_vertices"+id).c_str(),"N_{VTX}",20,0,20,"N_{VTX}","Events/bin");
  histos1d[("syst_muon_abseta"+id).c_str()]=histo_writer->create_1d(("muon_abseta"+id).c_str(),"|eta| of muons in an event",125,0,2.5, "Muon |#eta| ");
  histos1d[("syst_electron_abseta"+id).c_str()]=histo_writer->create_1d(("electron_abseta"+id).c_str(),"|eta| of electrons in an event",125,0,2.5, "Electron |#eta| ");
  histos1d[("top_mass_tf"+id).c_str()]=histo_writer->create_1d(("top_mass_tf"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",260,50, top_mass_upper, "M3 [GeV]");
}
void SystematicPlots::plot_systematics()
{

  //int n_vtx = 0;
  //std::vector<mor::PrimaryVertex> *pvertices = handle_holder->get_primary_vertices();
  //for (std::vector<mor::PrimaryVertex>::const_iterator vertex = pvertices->begin();
  //   vertex!=pvertices->end();
  //  vertex++){
  //    n_vtx++;
  //}
  //histos1d[("syst_nprimary_vertices"+id).c_str()]->Fill(n_vtx);
  
  double M3_mass = handle_holder->services()->mass_reco()->calculate_M3();

  //std::cout << "Plotting systematic plots" << std::endl;
  for(std::vector<mor::Muon>::iterator isolated_mu = isolated_muons->begin();
      isolated_mu != isolated_muons->end();
      isolated_mu++){
    histos1d[("syst_muon_abseta"+id).c_str()]->Fill(fabs(isolated_mu->Eta()));

  }

  for(std::vector<mor::Electron>::iterator isolated_e = isolated_electrons->begin();
      isolated_e != isolated_electrons->end();
      isolated_e++){
    histos1d[("syst_electron_abseta"+id).c_str()]->Fill(fabs(isolated_e->Eta()));
  }


  histos1d[("top_mass_tf"+id).c_str()]->Fill(M3_mass);

}
