#include "../../interface/EventSelection/LeptonEtaWeightProvider.h"

broc::LeptonEtaWeightProvider::LeptonEtaWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder): broc::LeptonEfficiencyWeightProvider(weight_file, handle_holder)
{
}

broc::LeptonEtaWeightProvider::~LeptonEtaWeightProvider()
{
}

double broc::LeptonEtaWeightProvider::get_value_for_event()
{
	double lepton_eta=-999;
	if(process_muons && muons->size() > 0){
		lepton_eta = muons->begin()->eta();
	}else if(process_electrons && electrons->size() > 0){
		lepton_eta = electrons->begin()->eta();
	}

	return lepton_eta;
}
