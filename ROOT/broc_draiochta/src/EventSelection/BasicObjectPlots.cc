#include "../../interface/EventSelection/BasicObjectPlots.h"

BasicObjectPlots::BasicObjectPlots(std::string ident)
{
	id = ident;

	ht = -1;
	uncorrected_mets = NULL;
}

BasicObjectPlots::BasicObjectPlots(eire::HandleHolder *handle_holder)
{
	id = "_"+handle_holder->get_ident();
	cuts_set = handle_holder->get_cuts_set();
	set_handles(handle_holder);

	ht = -1;
	uncorrected_mets = NULL;
}

BasicObjectPlots::~BasicObjectPlots()
{
//	histos1d[("W_jets_assignment"+id).c_str()]->Sumw2();
//	histos1d[("W_jets_assignment"+id).c_str()]->Scale(1.0/histos1d[("W_jets_assignment"+id).c_str()]->Integral());
}

void BasicObjectPlots::plot_all()
{
	ht = -1;
	plot_jets();
	plot_muons();
	plot_electrons();
	plot_ht();
	plot_dR();
	plot_muon_quality();
	plot_electron_quality();
	plot_vertex();
	plot_DiLeptonMass();
	plot_gen_info();

	plot_W();

        histos1d[("MC_event_weights"+id).c_str()]->Fill(handle_holder->get_event_information()->event_weight());
}

void BasicObjectPlots::set_uncut_jets(std::vector<mor::Jet>* uncut_jets)
{
	this->uncut_jets = uncut_jets;
}

void BasicObjectPlots::set_loose_muons(std::vector<mor::Muon>* loose_muons)
{
	this->loose_muons = loose_muons;
}

void BasicObjectPlots::set_ht_calc(HtCalculator *ht_calc)
{
	this->ht_calc = ht_calc;
}

void BasicObjectPlots::set_uncorrected_mets(std::vector<mor::MET> *analyser_mets)
{
	uncorrected_mets = analyser_mets;
}

void BasicObjectPlots::set_pvert(std::vector<mor::PrimaryVertex> *pvert)
{
        this->pvert = pvert;
}

void BasicObjectPlots::book_histos()
{
	histos1d[("W_jets_assignment"+id).c_str()]=histo_writer->create_1d(("W_jets_assignment"+id).c_str(),"correctly identified W jets closest to W mass",5,-0.5,4.5);
	histos1d[("transverse_W_mass"+id).c_str()]=histo_writer->create_1d(("transverse_W_mass"+id).c_str(),"transverse W mass",100,0.,100., "transverse mass m_{T} (#mu, #nu)");

	histos1d[("jet_number"+id).c_str()]=histo_writer->create_1d(("jet_number"+id).c_str(),"jet multiplicity",10,-0.5,9.5, "Jet multiplicity");
	histos1d[("muon_number"+id).c_str()]=histo_writer->create_1d(("muon_number"+id).c_str(),"muon multiplicity ",10,-0.5,9.5, "Muon Multiplicity");
	histos1d[("muon_pt"+id).c_str()]=histo_writer->create_1d(("muon_pt"+id).c_str(),"p_{T} of muons in an event",70,0,350, "Muon p_{T} (GeV)");
	histos1d[("muon_eta_tf"+id).c_str()]=histo_writer->create_1d(("muon_eta_tf"+id).c_str(),"eta of muons in an event",300,-3.0,3.0, "Muon #eta ");
        histos1d[("muon_eta"+id).c_str()]=histo_writer->create_1d(("muon_eta"+id).c_str(),"eta of muons in an event",10,-3,3, "Muon #eta ");
	histos1d[("muon_abseta"+id).c_str()]=histo_writer->create_1d(("muon_abseta"+id).c_str(),"|eta| of muons in an event",150,0,3.0, "Muon |#eta| ");
	histos1d[("muon_abseta_x_charge"+id).c_str()]=histo_writer->create_1d(("muon_abseta_x_charge"+id).c_str(),"charge x |eta| of muons in an event",300,-3.0,3.0, "Muon charge x |#eta| ");
	histos1d[("muon_phi"+id).c_str()]=histo_writer->create_1d(("muon_phi"+id).c_str(),"phi of muons in an event",8,-3.14,3.14, "Muon #phi (rad)");
	histos1d[("muon_d0"+id).c_str()]=histo_writer->create_1d(("muon_d0"+id).c_str(),"d0 of muons in an event",300,-0.3,0.3, "Muon impact parameter d_{0} [cm]");
	histos2d[("muon_d0_vs_phi"+id).c_str()]=histo_writer->create_2d(("muon_d0_vs_phi"+id).c_str(),"d0 vs phi of muons in an event",80,-4,4,80,-0.4,0.4, "Muon #phi (rad)", "Muon impact parameter d_{0}");

	histos1d[("muon_nHits"+id).c_str()]=histo_writer->create_1d(("muon_nHits"+id).c_str(),"number of tracker Hits of muons in an event",30,-0.5,29.5, "Muon number of tracker hits");
	histos1d[("muon_nMuonHits"+id).c_str()]=histo_writer->create_1d(("muon_nMuonHits"+id).c_str(),"number of tracker Hits of muons in an event",50,-0.5,49.5, "Number of Valid Muon Hits");
	histos1d[("muon_nPixelLayers"+id).c_str()]=histo_writer->create_1d(("muon_nPixelLayers"+id).c_str(),"nPixelLayers  ",10,-0.5,9.5, "nPixelLayers ");
	histos1d[("muon_nMatchedSegments"+id).c_str()]=histo_writer->create_1d("muon_nMatchedSegments"+id,"Segments matched to a global muon  ",10,-0.5,9.5, "Segments matched to a global muon");
	histos1d[("muon_nDTstations"+id).c_str()]=histo_writer->create_1d("muon_nDTstations"+id,"n Stations DT ",10,-0.5,9.5, "n Stations DT");
	histos1d[("muon_nstations"+id).c_str()]=histo_writer->create_1d("muon_nstations"+id,"n Stations DT OR CSC ",10,-0.5,9.5, "n Stations DT OR CSC");

	histos1d[("muon_chi2"+id).c_str()]=histo_writer->create_1d(("muon_chi2"+id).c_str(),"normalised #chi^{2} of muons in an event",20,0.0,10, "Muon global normalised #chi^{2}");
	histos1d[("electron_number"+id).c_str()]=histo_writer->create_1d(("electron_number"+id).c_str(),"electron multiplicity ",10,-0.5,9.5, "Electron Multiplicity");
	histos1d[("electron_pt"+id).c_str()]=histo_writer->create_1d(("electron_pt"+id).c_str(),"p_{T} of electron in an event",70,0,350, "Electron p_{T} (GeV)");
	histos1d[("electron_eta"+id).c_str()]=histo_writer->create_1d(("electron_eta"+id).c_str(),"eta of electron in an event",10,-3.,3., "Electron #eta ");
        histos1d[("electron_eta_tf"+id).c_str()]=histo_writer->create_1d(("electron_eta_tf"+id).c_str(),"eta of electron in an event",300,-3.,3., "Electron #eta ");
	histos1d[("electron_phi"+id).c_str()]=histo_writer->create_1d(("electron_phi"+id).c_str(),"phi of electrons in an event",8,-3.14,3.14, "Electron #phi (rad)");
	histos1d[("electron_abseta"+id).c_str()]=histo_writer->create_1d(("electron_abseta"+id).c_str(),"|eta| of electrons in an event",150,0,3.0, "Electron |#eta| ");
	histos1d[("electron_abseta_x_charge"+id).c_str()]=histo_writer->create_1d(("electron_abseta_x_charge"+id).c_str(),"charge x |eta| of electrons in an event",300,-3.0,3.0, "Electron charge x |#eta| ");
	histos1d[("electron_d0"+id).c_str()]=histo_writer->create_1d(("electron_d0"+id).c_str(),"d0 of electrons in an event",300,-0.3,0.3, "Electron impact parameter d_{0} [cm]");
	histos2d[("electron_d0_vs_phi"+id).c_str()]=histo_writer->create_2d(("electron_d0_vs_phi"+id).c_str(),"d0 vs phi of electrons in an event",80,-4,4,80,-0.4,0.4, "Electron #phi (rad)", "Electon impact parameter d_{0} [cm]");	

	histos2d[("electron_eta_vs_reliso"+id).c_str()]=histo_writer->create_2d(("electron_eta_vs_reliso"+id).c_str(),"electron eta vs electron relative isolation",300,-3.0,3.0,225,-0.,1.5, "electron #eta", "electron relative PF isolation");

	histos2d[("e_dR_vs_jet_number"+id).c_str()]=histo_writer->create_2d(("e_dR_vs_jet_number"+id).c_str(),"#Delta R vs jet multiplicity",225,0.0,7.0,10,-0.5,9.5, "#Delta R(e,jet)","Jet Multiplicity");
	
	//jl 03.11.10: dphi(mu,MET)
	histos1d[("muon_dphi_lepmet"+id).c_str()]=histo_writer->create_1d(("muon_dphi_lepmet"+id).c_str(),"DeltaPhi between muon and MET",32,0.,3.2, "DeltaPhi between muon and MET");
	histos1d[("electron_dphi_lepmet"+id).c_str()]=histo_writer->create_1d(("electron_dphi_lepmet"+id).c_str(),"DeltaPhi between electron and MET",32,0.,3.2, "DeltaPhi between electron and MET");

	histos1d[("Ht"+id).c_str()]=histo_writer->create_1d(("Ht"+id).c_str(),"Ht: Sum pt of all jets and muon and MET",80,0,2000, "H_{T}^{MET} (GeV)");
	histos1d[("Ht_wo_MET"+id).c_str()]=histo_writer->create_1d(("Ht_wo_MET"+id).c_str(),"Ht: Sum pt of all jets and muon",80,0,2000, "H_{T} (GeV)");

	//jl 23.05.12: for template fit
        histos1d[("Missing_Ht_tf"+id).c_str()]=histo_writer->create_1d(("Missing_Ht_tf"+id).c_str(),"Missing Ht",200,0,300, "missing H_{T} (GeV)");
        histos1d[("Ht_wo_MET_tf"+id).c_str()]=histo_writer->create_1d(("Ht_wo_MET_tf"+id).c_str(),"Ht: Sum pt of all jets and muon",800,0,2000, "H_{T} (GeV)");


	histos1d[("Missing_Ht"+id).c_str()]=histo_writer->create_1d(("Missing_Ht"+id).c_str(),"Missing Ht",200,0,300, "missing H_{T} (GeV)");

	histos1d[("jet_pt"+id).c_str()]=histo_writer->create_1d(("jet_pt"+id).c_str(),"p_{T} of jets in an event",75,0,300, "Jet p_{T} (GeV)");
	histos1d[("jet_eta"+id).c_str()]=histo_writer->create_1d(("jet_eta"+id).c_str(),"eta of jets in an event",10,-3,3, "Jet #eta ");
	histos1d[("jet_phi"+id).c_str()]=histo_writer->create_1d(("jet_phi"+id).c_str(),"phi of jets in an event",10,-4.0,4.0, "Jet #phi (rad)");


  	histos1d[("jet1_pt"+id).c_str()]=histo_writer->create_1d(("jet1_pt"+id).c_str(),"p_{T} of leading jet",75,0,300, "Jet 1 p_{T} (GeV)");
	histos1d[("jet1_eta"+id).c_str()]=histo_writer->create_1d(("jet1_eta"+id).c_str(),"eta of leading jet",10,-3.0,3.0, "Jet 1 #eta ");
	histos1d[("jet1_phi"+id).c_str()]=histo_writer->create_1d(("jet1_phi"+id).c_str(),"phi of leading jet",10,-4.0,4.0, "Jet 1 #phi (rad)");
	histos1d[("jet1_CSVMVAbdiscr"+id).c_str()]=histo_writer->create_1d(("jet1_CSVMVAbdiscr"+id).c_str(),"Jet 1 CSV MVA tagger output variable",20,0,1, "output var");

	histos1d[("jet2_pt"+id).c_str()]=histo_writer->create_1d(("jet2_pt"+id).c_str(),"p_{T} of jet 2 ",75,0,300, "Jet 2 p_{T} (GeV)");
	histos1d[("jet2_eta"+id).c_str()]=histo_writer->create_1d(("jet2_eta"+id).c_str(),"eta of jet 2 ",10,-3.0,3.0, "Jet 2 #eta ");
	histos1d[("jet2_phi"+id).c_str()]=histo_writer->create_1d(("jet2_phi"+id).c_str(),"phi of jet 2 ",10,-4.0,4.0, "Jet 2 #phi (rad)");
        histos1d[("jet2_CSVMVAbdiscr"+id).c_str()]=histo_writer->create_1d(("jet2_CSVMVAbdiscr"+id).c_str(),"Jet 2 CSV MVA tagger output variable",20,0,1, "output var");

	histos1d[("jet3_pt"+id).c_str()]=histo_writer->create_1d(("jet3_pt"+id).c_str(),"p_{T} of jet 3",30,0,120, "Jet 3 p_{T} (GeV)");
	histos1d[("jet3_eta"+id).c_str()]=histo_writer->create_1d(("jet3_eta"+id).c_str(),"eta of jet 3 ",10,-3.0,3.0, "Jet 3 #eta ");
	histos1d[("jet3_phi"+id).c_str()]=histo_writer->create_1d(("jet3_phi"+id).c_str(),"phi of jet 3 ",10,-4.0,4.0, "Jet 3 #phi (rad)");
        histos1d[("jet3_CSVMVAbdiscr"+id).c_str()]=histo_writer->create_1d(("jet3_CSVMVAbdiscr"+id).c_str(),"Jet 3 CSV MVA tagger output variable",20,0,1, "output var");

	histos1d[("jet4_pt"+id).c_str()]=histo_writer->create_1d(("jet4_pt"+id).c_str(),"p_{T} of jet 4",30,0,120, "Jet 4 p_{T}");
	histos1d[("jet4_eta"+id).c_str()]=histo_writer->create_1d(("jet4_eta"+id).c_str(),"eta of jet 4 ",10,-3.0,3.0, "Jet 4 #eta ");
	histos1d[("jet4_phi"+id).c_str()]=histo_writer->create_1d(("jet4_phi"+id).c_str(),"phi of jet 4 ",10,-4.0,4.0, "Jet 4 #phi (rad)");
        histos1d[("jet4_CSVMVAbdiscr"+id).c_str()]=histo_writer->create_1d(("jet4_CSVMVAbdiscr"+id).c_str(),"Jet 4 CSV MVA tagger output variable",20,0,1, "output var");

	histos1d[("jet5_pt"+id).c_str()]=histo_writer->create_1d(("jet5_pt"+id).c_str(),"p_{T} of jet 5",30,0,120, "Jet 5 p_{T}");
        histos1d[("jet5_eta"+id).c_str()]=histo_writer->create_1d(("jet5_eta"+id).c_str(),"eta of jet 5 ",10,-3.0,3.0, "Jet 5 #eta ");
        histos1d[("jet5_phi"+id).c_str()]=histo_writer->create_1d(("jet5_phi"+id).c_str(),"phi of jet 5 ",10,-4.0,4.0, "Jet 5 #phi (rad)");
        histos1d[("jet5_CSVMVAbdiscr"+id).c_str()]=histo_writer->create_1d(("jet5_CSVMVAbdiscr"+id).c_str(),"Jet 5 CSV MVA tagger output variable",20,0,1, "output var");
	
	
	//jl 22.12.10: add technical distributions such as emf etc.
	histos1d[("jet1_emf"+id).c_str()]=histo_writer->create_1d(("jet1_emf"+id).c_str(),"emf of jet 1 in an event",40,-0.8,1.2, "Jet 1 emf");
	histos1d[("jet1_chf"+id).c_str()]=histo_writer->create_1d(("jet1_chf"+id).c_str(),"chf of jet 1 in an event",40,-0.8,1.2, "Jet 1 chf");
	histos1d[("jet1_fhpd"+id).c_str()]=histo_writer->create_1d(("jet1_fhpd"+id).c_str(),"fHPD of jet 1 in an event",60,-1.2,1.2, "Jet 1 fHPD");
	histos1d[("jet1_n90"+id).c_str()]=histo_writer->create_1d(("jet1_n90"+id).c_str(),"n90Hits of jet 1 in an event",50,0,150, "Jet 1 n90Hits");
	histos1d[("jet1_nconst"+id).c_str()]=histo_writer->create_1d(("jet1_nconst"+id).c_str(),"nconstituents of jet 1 in an event",50,0,50, "Jet 1 nconst");
	histos1d[("jet1_nhf"+id).c_str()]=histo_writer->create_1d(("jet1_nhf"+id).c_str(),"nhf of jet 1 in an event",20,0,1, "Jet 1 nhf");
	histos1d[("jet1_nemf"+id).c_str()]=histo_writer->create_1d(("jet1_nemf"+id).c_str(),"nemf of jet 1 in an event",20,0,1, "Jet 1 nemf");
	histos1d[("jet1_cemf"+id).c_str()]=histo_writer->create_1d(("jet1_cemf"+id).c_str(),"cemf of jet 1 in an event",20,0,1, "Jet 1 cemf");
	histos1d[("jet1_cmult"+id).c_str()]=histo_writer->create_1d(("jet1_cmult"+id).c_str(),"charged multiplicity of jet 1 in an event",40,0,40, "Jet 1 cmult");
	histos1d[("jet2_emf"+id).c_str()]=histo_writer->create_1d(("jet2_emf"+id).c_str(),"emf of jet 2 in an event",40,-0.8,1.2, "Jet 2 emf");
	histos1d[("jet2_chf"+id).c_str()]=histo_writer->create_1d(("jet2_chf"+id).c_str(),"chf of jet 2 in an event",40,-0.8,1.2, "Jet 2 chf");
	histos1d[("jet2_fhpd"+id).c_str()]=histo_writer->create_1d(("jet2_fhpd"+id).c_str(),"fHPD of jet 2 in an event",60,-1.2,1.2, "Jet 2 fHPD");
	histos1d[("jet2_n90"+id).c_str()]=histo_writer->create_1d(("jet2_n90"+id).c_str(),"n90Hits of jet 2 in an event",50,0,150, "Jet 2 n90Hits");
	histos1d[("jet2_nconst"+id).c_str()]=histo_writer->create_1d(("jet2_nconst"+id).c_str(),"nconstituents of jet 2 in an event",50,0,50, "Jet 2 nconst");
	histos1d[("jet2_nhf"+id).c_str()]=histo_writer->create_1d(("jet2_nhf"+id).c_str(),"nhf of jet 2 in an event",20,0,1, "Jet 2 nhf");
	histos1d[("jet2_nemf"+id).c_str()]=histo_writer->create_1d(("jet2_nemf"+id).c_str(),"nemf of jet 2 in an event",20,0,1, "Jet 2 nemf");
	histos1d[("jet2_cemf"+id).c_str()]=histo_writer->create_1d(("jet2_cemf"+id).c_str(),"cemf of jet 2 in an event",20,0,1, "Jet 2 cemf");
	histos1d[("jet2_cmult"+id).c_str()]=histo_writer->create_1d(("jet2_cmult"+id).c_str(),"charged multiplicity of jet 2 in an event",40,0,40, "Jet 2 cmult");
	histos1d[("jet3_emf"+id).c_str()]=histo_writer->create_1d(("jet3_emf"+id).c_str(),"emf of jet 3 in an event",40,-0.8,1.2, "Jet 3 emf");
	histos1d[("jet3_chf"+id).c_str()]=histo_writer->create_1d(("jet3_chf"+id).c_str(),"chf of jet 3 in an event",40,-0.8,1.2, "Jet 3 chf");
	histos1d[("jet3_fhpd"+id).c_str()]=histo_writer->create_1d(("jet3_fhpd"+id).c_str(),"fHPD of jet 3 in an event",60,-1.2,1.2, "Jet 3 fHPD");
	histos1d[("jet3_n90"+id).c_str()]=histo_writer->create_1d(("jet3_n90"+id).c_str(),"n90Hits of jet 3 in an event",50,0,150, "Jet 3 n90Hits");
	histos1d[("jet3_nconst"+id).c_str()]=histo_writer->create_1d(("jet3_nconst"+id).c_str(),"nconstituents of jet 3 in an event",50,0,50, "Jet 3 nconst");
	histos1d[("jet3_nhf"+id).c_str()]=histo_writer->create_1d(("jet3_nhf"+id).c_str(),"nhf of jet 3 in an event",20,0,1, "Jet 3 nhf");
	histos1d[("jet3_nemf"+id).c_str()]=histo_writer->create_1d(("jet3_nemf"+id).c_str(),"nemf of jet 3 in an event",20,0,1, "Jet 3 nemf");
	histos1d[("jet3_cemf"+id).c_str()]=histo_writer->create_1d(("jet3_cemf"+id).c_str(),"cemf of jet 3 in an event",20,0,1, "Jet 3 cemf");
	histos1d[("jet3_cmult"+id).c_str()]=histo_writer->create_1d(("jet3_cmult"+id).c_str(),"charged multiplicity of jet 3 in an event",40,0,40, "Jet 3 cmult");
	histos1d[("jet4_emf"+id).c_str()]=histo_writer->create_1d(("jet4_emf"+id).c_str(),"emf of jet 4 in an event",40,-0.8,1.2, "Jet 4 emf");
	histos1d[("jet4_chf"+id).c_str()]=histo_writer->create_1d(("jet4_chf"+id).c_str(),"chf of jet 4 in an event",40,-0.8,1.2, "Jet 4 chf");
	histos1d[("jet4_fhpd"+id).c_str()]=histo_writer->create_1d(("jet4_fhpd"+id).c_str(),"fHPD of jet 4 in an event",60,-1.2,1.2, "Jet 4 fHPD");
	histos1d[("jet4_n90"+id).c_str()]=histo_writer->create_1d(("jet4_n90"+id).c_str(),"n90Hits of jet 4 in an event",50,0,150, "Jet 4 n90Hits");
	histos1d[("jet4_nconst"+id).c_str()]=histo_writer->create_1d(("jet4_nconst"+id).c_str(),"nconstituents of jet 4 in an event",50,0,50, "Jet 4 nconst");
	histos1d[("jet4_nhf"+id).c_str()]=histo_writer->create_1d(("jet4_nhf"+id).c_str(),"nhf of jet 4 in an event",20,0,1, "Jet 4 nhf");
	histos1d[("jet4_nemf"+id).c_str()]=histo_writer->create_1d(("jet4_nemf"+id).c_str(),"nemf of jet 4 in an event",20,0,1, "Jet 4 nemf");
	histos1d[("jet4_cemf"+id).c_str()]=histo_writer->create_1d(("jet4_cemf"+id).c_str(),"cemf of jet 4 in an event",20,0,1, "Jet 4 cemf");
	histos1d[("jet4_cmult"+id).c_str()]=histo_writer->create_1d(("jet4_cmult"+id).c_str(),"charged multiplicity of jet 4 in an event",40,0,40, "Jet 4 cmult");
	

	histos2d[("jet_pt_vs_et"+id).c_str()]=histo_writer->create_2d(("jet_pt_vs_et"+id).c_str(),"pt vs et for all jets",85, 0, 500, 85,0,500, "Jet p_{T} (GeV)", "Jet E_{T} (GeV)");
	histos2d[("jet1_pt_vs_jet2_pt"+id).c_str()]=histo_writer->create_2d(("jet1_pt_vs_jet2_pt"+id).c_str(),"1st jet pt vs 2nd jet pt",85, 0, 500, 85,0,500, "Jet 1 p_{T}", "Jet 2 p_{T}");

	histos2d[("jet_eta_vs_phi"+id).c_str()]=histo_writer->create_2d(("jet_eta_vs_phi"+id).c_str(),"eta vs phi for all jets",66, -3.3, 3.3, 66,-3.3,3.3, "Jet #eta", "Jet #phi");

	histos2d[("min_mu_dR_vs_PFreliso"+id).c_str()]=histo_writer->create_2d(("min_mu_dR_vs_PFreliso"+id).c_str(),"dR(mu,jet) vs muon PF relative isolation",100, 0, 1, 100,0,5, "muon relative isolation (GeV)", "min dR(mu,jet)");

	histos2d[("min_mu_dR_vs_lep_pt"+id).c_str()]=histo_writer->create_2d(("min_mu_dR_vs_lep_pt"+id).c_str(),"dR(mu,jet) vs muon pt",100, 0, 350, 100,0,5, "muon p_{T} (GeV)", "min dR(mu,jet)");
	histos2d[("min_mu_dR_jet_pt_vs_lep_pt"+id).c_str()]=histo_writer->create_2d(("min_mu_dR_jet_pt_vs_lep_pt"+id).c_str(),"jet pt vs muon pt",100, 0, 350, 100,0,350, "muon p_{T} (GeV)", "jet p_{T} (GeV)");

	histos1d[("jet_pt_id_truth"+id).c_str()]=histo_writer->create_1d(("jet_pt_id_truth"+id).c_str(),"id of truth matched jets in event",10,-0.5,9.5, "Top decay jets id");

	histos1d[("min_mu_dR"+id).c_str()]=histo_writer->create_1d(("min_mu_dR"+id).c_str(),"deltaR between muon(s) and closest jet",500,0,7,"#Delta R(#mu, closest jet)");

	histos1d[("min_e_dR"+id).c_str()]=histo_writer->create_1d(("min_e_dR"+id).c_str(),"deltaR for electron(s) and closest jet",500,0,7, "#Delta R(e, closest jet)");
	histos2d[("min_e_dR_vs_lep_pt"+id).c_str()]=histo_writer->create_2d(("min_e_dR_vs_lep_pt"+id).c_str(),"dR(e,jet) vs electron pt",100, 0, 350, 100,0,5, "electron p_{T} (GeV)", "min dR(e,jet)");
	histos2d[("min_e_dR_vs_PFreliso"+id).c_str()]=histo_writer->create_2d(("min_e_dR_vs_PFreliso"+id).c_str(),"dR(e,jet) vs electron PF relative isolation",100, 0, 1, 100,0,5, "electron PF relative isolation (GeV)", "min dR(e,jet)");
	histos2d[("min_e_dR_jet_pt_vs_lep_pt"+id).c_str()]=histo_writer->create_2d(("min_e_dR_jet_pt_vs_lep_pt"+id).c_str(),"jet pt vs electron pt",100, 0, 350, 100,0,350, "electron p_{T} (GeV)", "jet p_{T} (GeV)");

	histos1d[("jet_e_dR"+id).c_str()]=histo_writer->create_1d(("jet_e_dR"+id).c_str(),"deltaR for uncut electrons and uncut jet",50,0,7, "#Delta R(e, closest jet) (w/o selection cuts)");

 	histos1d[("top_had_jets_genmatched"+id).c_str()]=histo_writer->create_1d(("top_had_jets_genmatched"+id).c_str(),"Jets passing event which are really from semi-lep hadronic decay channel",10,-0.5, 9.5, "Number of jets from hadronic top decay after selection cuts");
 	histos1d[("top_jets_genmatched"+id).c_str()]=histo_writer->create_1d(("top_jets_genmatched"+id).c_str(),"Jets passing event which are really from semi-lep decay channel",10,-0.5, 9.5, "Number of jets from t#bar{t} decay after selection cuts");

	histos1d[("dilepton_mass"+id).c_str()]=histo_writer->create_1d(("dilepton_mass"+id).c_str(),"Dilepton mass reconstructed from two same-type leptons of opposite charge",3000,0, 1500, "reconstructed dilepton mass (GeV)");
	histos1d[("dilepton_mass_zoom"+id).c_str()]=histo_writer->create_1d(("dilepton_mass_zoom"+id).c_str(),"Di-muon mass",120,60, 120, "Di-muon mass (GeV)"); //FIXME: tmp duplication for formatting. should not be checked in . can be removed by anyone.
	
	//jl 23.12.10: added PV plots
	histos1d[("npv"+id).c_str()]=histo_writer->create_1d(("npv"+id).c_str(),"# PV",50,0.,50,"Number of primary vertices");
	histos1d[("npu_truth"+id).c_str()]=histo_writer->create_1d(("npu_truth"+id).c_str(),"# PU truth",50,0.,50,"nPU truth");
	histos1d[("pv_isfake"+id).c_str()]=histo_writer->create_1d(("pv_isfake"+id).c_str(),"PV isfake",2,0,2,"is fake");
	histos1d[("pv_z"+id).c_str()]=histo_writer->create_1d(("pv_z"+id).c_str(),"Z PV",15,0,30,"Z");
	histos1d[("pv_rho"+id).c_str()]=histo_writer->create_1d(("pv_rho"+id).c_str(),"Rho PV",40,0,2,"#Rho");
	histos1d[("pv_ndof"+id).c_str()]=histo_writer->create_1d(("pv_ndof"+id).c_str(),"PV ndof",50,0,250,"ndof");

	//jl 19.04.11: added deta(lep,jet) and dphi(lep,jet) plots
        histos1d[("min_mu_dEta"+id).c_str()]=histo_writer->create_1d(("min_mu_dEta"+id).c_str(),"deltaEta between muon(s) and closest jet",50,0,7,"#Delta Eta(#mu, closest jet)");
        histos1d[("min_e_dEta"+id).c_str()]=histo_writer->create_1d(("min_e_dEta"+id).c_str(),"deltaEta for electron(s) and closest jet",50,0,7, "#Delta Eta(e, closest jet)");
        histos1d[("min_mu_dPhi"+id).c_str()]=histo_writer->create_1d(("min_mu_dPhi"+id).c_str(),"deltaPhi between muon(s) and closest jet",50,0,7,"#Delta Phi(#mu, closest jet)");
        histos1d[("min_e_dPhi"+id).c_str()]=histo_writer->create_1d(("min_e_dPhi"+id).c_str(),"deltaPhi for electron(s) and closest jet",50,0,7, "#Delta Phi(e, closest jet)");

        histos1d[("MC_event_weights"+id).c_str()]=histo_writer->create_1d(("MC_event_weights"+id).c_str(),"MC generator event weights",5000,-0.001,0.001, "weight");

	//jl 04.04.12: added b-tagging output variable plot here
	 histos1d[("jet_CSVMVAbdiscr"+id).c_str()]=histo_writer->create_1d(("jet_CSVMVAbdiscr"+id).c_str(),"CSV MVA tagger output variable",20,0,1, "output var");
	//jl 31.05.12: eID mva output
	histos1d[("electron_mvaid"+id).c_str()]=histo_writer->create_1d(("electron_mvaid"+id).c_str(),"eID MVA output",20,-2,2, "output var");
	//jl 06.06.12: event weights
	histos1d[("event_weight_PDFrw"+id).c_str()]=histo_writer->create_1d(("event_weight_PDFrw"+id).c_str(),"pdfrw event weights",20000,0,20, "weight");

 	histos1d[("gen_lep_eta"+id).c_str()]=histo_writer->create_1d(("gen_lep_eta"+id).c_str(),"Generator lepton eta",150,-5., 5., "#eta of generator level lepton");
 	histos1d[("gen_lep_pt"+id).c_str()]=histo_writer->create_1d(("gen_lep_pt"+id).c_str(),"Generator lepton pt",150,0., 150., "generator lepton p_{T}");
 	histos1d[("gen_quark_eta"+id).c_str()]=histo_writer->create_1d(("gen_quark_eta"+id).c_str(),"Generator lepton eta",150,-5., 5., "#eta of generator level quark");
 	histos1d[("gen_quark_pt"+id).c_str()]=histo_writer->create_1d(("gen_quark_pt"+id).c_str(),"Generator quark pt",150,0., 150., "generator quark p_{T}");

        histos1d[("gen_leptop_eta"+id).c_str()]=histo_writer->create_1d(("gen_leptop_eta"+id).c_str(),"Generator leptonic top eta",150,-5., 5., "#eta of generator level leptonic top");
        histos1d[("gen_leptop_pt"+id).c_str()]=histo_writer->create_1d(("gen_leptop_pt"+id).c_str(),"Generator leptonic top pt",350,0., 350., "generator leptonic top p_{T}");
        histos1d[("gen_hadtop_eta"+id).c_str()]=histo_writer->create_1d(("gen_hadtop_eta"+id).c_str(),"Generator hadronic top eta",150,-5., 5., "#eta of generator level hadronic top");
        histos1d[("gen_hadtop_pt"+id).c_str()]=histo_writer->create_1d(("gen_hadtop_pt"+id).c_str(),"Generator hadronic top pt",350,0., 350., "generator hadronic top p_{T}");
        histos1d[("gen_leptop_y"+id).c_str()]=histo_writer->create_1d(("gen_leptop_y"+id).c_str(),"Generator leptonic top y",150,-5., 5., "#y of generator level leptonic top");
        histos1d[("gen_hadtop_y"+id).c_str()]=histo_writer->create_1d(("gen_hadtop_y"+id).c_str(),"Generator hadronic top y",150,-5., 5., "#y of generator level hadronic top");

	histos1d[("eMVA_trig_frac_sel_e"+id).c_str()]=histo_writer->create_1d(("eMVA_trig_frac_sel_e"+id).c_str(),"amount of electrons in triggering vs non-triggering MVA for selected electrons",5,-0.5,4.5, "0=non-trig, 1=trig");
}

void BasicObjectPlots::plot_DiLeptonMass()
{

	std::vector<mor::Particle> *candidates = handle_holder->services()->dilepton_reco()->get_candidates();
	if(candidates->size() > 0){
		histos1d[("dilepton_mass"+id).c_str()]->Fill(candidates->begin()->mass());
		histos1d[("dilepton_mass_zoom"+id).c_str()]->Fill(candidates->begin()->mass());
	}

// 	double Z_mass = 90.0;
// 	double Z_width = 30.0;


// 	if(isolated_muons->size() >= 1 && loose_muons->size() >= 1){
// 		for(std::vector<mor::Muon>::iterator muon1 = isolated_muons->begin();
// 		    muon1 != isolated_muons->end();
// 		    ++muon1)
// 		{
// 			for(std::vector<mor::Muon>::iterator muon2 =
// 				loose_muons->begin();
// 			    muon2 != loose_muons->end();
// 			    ++muon2)
// 			{
// 				// don't use the same lepton twice
// 				if(muon1->Pt() == muon2->Pt() &&
// 					muon1->Eta() == muon2->Eta())
// 					continue;

// 				double dilepton_mass = (muon1->p4() + muon2->p4()).mass();
// 				if((muon1->charge() != muon2->charge()) && (dilepton_mass - Z_mass) < Z_width){
// 					histos1d[("dilepton_mass"+id).c_str()]->Fill(dilepton_mass);
// 					return;
// 				}
// 			}
// 		}
// 	}
}

void BasicObjectPlots::plot_W()
{
	std::vector<mor::Muon>::iterator mu_iter = isolated_muons->begin();
	std::vector<mor::MET>::iterator met_iter = uncorrected_mets->begin();
	if(mu_iter != isolated_muons->end() && met_iter != uncorrected_mets->end())
	{
		histos1d[("transverse_W_mass"+id).c_str()]->Fill((mu_iter->p4()+met_iter->p4()).mt());
	}
}

// Plot delta R between muon and closest jet
void BasicObjectPlots::plot_dR()
{
	for(std::vector<mor::Muon>::iterator mu_iter = isolated_muons->begin();
	    mu_iter != isolated_muons->end();
	    mu_iter++)
	{
		double min_dR = -1;
		double min_deta = -1;
		double min_dphi = -1;
		double min_dR_jet_pt = -1;

		for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();
		    jet_iter!=jets->end();
		    ++jet_iter)
		{
			double dR = ROOT::Math::VectorUtil::DeltaR(mu_iter->p4(),jet_iter->p4());
			if ((dR < min_dR) || (min_dR == -1)){
				min_dR = dR;
				min_dR_jet_pt = jet_iter->pt();
			}
			double deta = fabs(mu_iter->Eta()-jet_iter->Eta());
			if ((deta < min_deta) || (min_deta == -1))
				min_deta = deta;
			double dphi = fabs(ROOT::Math::VectorUtil::DeltaPhi(mu_iter->p4(),jet_iter->p4()));
			if ((dphi < min_dphi) || (min_dphi == -1))
				min_dphi = dphi;
		}

		histos1d[("min_mu_dR"+id).c_str()]->Fill(min_dR);
                histos1d[("min_mu_dEta"+id).c_str()]->Fill(min_deta);
                histos1d[("min_mu_dPhi"+id).c_str()]->Fill(min_dphi);

		histos2d[("min_mu_dR_vs_PFreliso"+id).c_str()]->Fill(mu_iter->PFrelIso(), min_dR);
		histos2d[("min_mu_dR_vs_lep_pt"+id).c_str()]->Fill(mu_iter->pt(), min_dR);
		if(min_dR < 0.3)
			histos2d[("min_mu_dR_jet_pt_vs_lep_pt"+id).c_str()]->Fill(mu_iter->pt(), min_dR_jet_pt);
	}

	for(std::vector<mor::Electron>::iterator e_iter = isolated_electrons->begin();
	    e_iter != isolated_electrons->end();
	    e_iter++)
	{
		double min_dR = -1;
		double min_deta = -1;
		double min_dphi = -1;
		double min_dR_jet_pt = -1;

		for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();
		    jet_iter!=jets->end();
		    ++jet_iter)
		{
			double dR = ROOT::Math::VectorUtil::DeltaR(e_iter->p4(),jet_iter->p4());
			if ((dR < min_dR) || (min_dR == -1)){
				min_dR = dR;
				min_dR_jet_pt = jet_iter->pt();
			}
			double deta = fabs(e_iter->Eta()-jet_iter->Eta());
			if ((deta < min_deta) || (min_deta == -1))
				min_deta = deta;
			double dphi = fabs(ROOT::Math::VectorUtil::DeltaPhi(e_iter->p4(),jet_iter->p4()));
			if ((dphi < min_dphi) || (min_dphi == -1))
				min_dphi = dphi;
		}

		histos1d[("min_e_dR"+id).c_str()]->Fill(min_dR);
	        histos1d[("min_e_dEta"+id).c_str()]->Fill(min_deta);
        	histos1d[("min_e_dPhi"+id).c_str()]->Fill(min_dphi);

		histos2d[("min_mu_dR_vs_PFreliso"+id).c_str()]->Fill(e_iter->PFrelIso(), min_dR);
		histos2d[("min_e_dR_vs_lep_pt"+id).c_str()]->Fill(e_iter->pt(), min_dR);
		if(min_dR < 0.3)
			histos2d[("min_e_dR_jet_pt_vs_lep_pt"+id).c_str()]->Fill(e_iter->pt(), min_dR_jet_pt);

		histos2d[("e_dR_vs_jet_number"+id).c_str()]->Fill(min_dR,jets->size());
	}
}

void BasicObjectPlots::analyse_recogen_matchedjets()
{
	if(!gen_evt) return;
	
	int num_had_jets=0;
	int num_top_jets=0;
	for(std::vector<mor::Jet>::iterator jet = jets->begin();
		jet != jets->end();
		++jet){
		int decay_prod = jet->ttbar_decay_product();
		if(decay_prod > 0 && decay_prod < 4) ++num_had_jets;
		if(decay_prod > 0 ) ++num_top_jets;
	}

	// is out of 3 had top jets
	histos1d[("top_had_jets_genmatched"+id).c_str()]->Fill(num_had_jets);
	// event contains all 4 jets from top
	histos1d[("top_jets_genmatched"+id).c_str()]->Fill(num_top_jets);
	
	/*
	 *	test hypothesis: 2 jets closest to W mass in event are jets
	 *	that come really from W decay
	 */

	int njets = jets->size();

	if(njets < 2)
		return;

	double min_mass_diff = -1;
	int j1_id = -1;
	int j2_id = -1;
	double W_mass = 80.0;
	for(int i = 0; i < njets; ++i){
		for(int j = i+1; j < njets; ++j){
			if(i != j){
				double curr_diff = ((*jets)[i].p4() + (*jets)[j].p4()).mass() - W_mass;
				if(curr_diff < min_mass_diff || min_mass_diff == -1){
					min_mass_diff = curr_diff;
					j1_id = i;
					j2_id = j;
				}
			}
		}
	}
	
	int ncorrect_Wjets=0;

	if((*jets)[j1_id].ttbar_decay_product()) ++ncorrect_Wjets;
	if((*jets)[j2_id].ttbar_decay_product()) ++ncorrect_Wjets;

	histos1d[("W_jets_assignment"+id).c_str()]->Fill(ncorrect_Wjets);

	/*
	 *	test hypothesis: highest 4 pt jets in event are from top decay
	 */

	for(unsigned int i = 0; i <= jets->size() && i < 4; ++i){
		if((*jets)[i].ttbar_decay_product())
			histos1d[("jet_pt_id_truth"+id).c_str()]->Fill(i);
	}
}

// Plot transverse energy:sum of all jets and the muon pt
void BasicObjectPlots::plot_ht()
{
	if(ht == -1)
		ht = ht_calc->get_ht();
	
	//histos1d[("Ht"+id).c_str()]->Fill(ht+corrected_mets->begin()->Pt());
	histos1d[("Ht_wo_MET"+id).c_str()]->Fill(ht);

	histos1d[("Missing_Ht"+id).c_str()]->Fill(ht_calc->get_missing_ht());
	//jl 23.05.12: for template fit
        histos1d[("Ht_wo_MET_tf"+id).c_str()]->Fill(ht);
        histos1d[("Missing_Ht_tf"+id).c_str()]->Fill(ht_calc->get_missing_ht());

}

void BasicObjectPlots::plot_muon_quality()
{
	for(std::vector<mor::Muon>::iterator muon_iter = isolated_muons->begin();
		muon_iter != isolated_muons->end();
		++muon_iter){

		double d0 = -1000;
		if(muon_iter->track_available()){
			histos1d[("muon_chi2"+id).c_str()]->Fill(muon_iter->chi2());
			histos1d[("muon_nHits"+id).c_str()]->Fill(muon_iter->nHits());
			histos1d[("muon_nMuonHits"+id).c_str()]->Fill(muon_iter->nMuonHits());

			d0 = muon_iter->d0();
			histos2d[("muon_d0_vs_phi"+id).c_str()]->Fill(muon_iter->Phi(),d0);
			histos1d[("muon_d0"+id).c_str()]->Fill(d0);
		}


		histos1d[("muon_nPixelLayers"+id).c_str()]->Fill(muon_iter->nPixelLayers());
		histos1d[("muon_nMatchedSegments"+id).c_str()]->Fill(muon_iter->nMatchedSegments());
		histos1d[("muon_nDTstations"+id).c_str()]->Fill(muon_iter->nDTstations());
		histos1d[("muon_nstations"+id).c_str()]->Fill(muon_iter->nDTstations()+muon_iter->nCSCstations());
		

	}
}


void BasicObjectPlots::plot_electron_quality()
{
	for(std::vector<mor::Electron>::iterator electron_iter = isolated_electrons->begin();
		electron_iter != isolated_electrons->end();
		++electron_iter){

		double d0 = -1000;
		if(electron_iter->track_available()){
			d0 = electron_iter->d0();
			histos2d[("electron_d0_vs_phi"+id).c_str()]->Fill(electron_iter->Phi(),d0);
			histos1d[("electron_d0"+id).c_str()]->Fill(d0);
		}
	}
}
//plot pt, eta, and numbet of jets
void BasicObjectPlots::plot_jets()
{
	int njets = jets->size();
	
	if(njets >= 1){
/*		double weight[20] = {1.37913, 1.23924, 1.19121, 0.958258, 0.894289, 0.772286, 0.92382, 0.945628, 1.12208, 0.832407, 1.12058, 0.641771, 1.00918, 0.875203, 0.691409, 0, 0, 0, 0, 0,};
		double n = (*jets)[0].nemf();
		int index = 19;
		for(int i = 1 ; i<21 ; i++)
		{
			if(n*20 < i) 
			{
				index = i-1; 
				break;
			}
		}*/
		//std::cout << "nemf = " << n << " index = " << index << std::endl;
		histos1d[("jet1_pt"+id).c_str()]->Fill((*jets)[0].Pt());//,weight[index]);
		histos1d[("jet1_eta"+id).c_str()]->Fill((*jets)[0].Eta());
		histos1d[("jet1_phi"+id).c_str()]->Fill((*jets)[0].Phi());
		histos1d[("jet1_emf"+id).c_str()]   ->Fill((*jets)[0].emf());
		histos1d[("jet1_chf"+id).c_str()]   ->Fill((*jets)[0].chf());
		histos1d[("jet1_fhpd"+id).c_str()]  ->Fill((*jets)[0].fHPD());
		histos1d[("jet1_n90"+id).c_str()]   ->Fill((*jets)[0].n90Hits());
		histos1d[("jet1_nconst"+id).c_str()]->Fill((*jets)[0].nconstituents());
		histos1d[("jet1_nhf"+id).c_str()]   ->Fill((*jets)[0].nhf());
		histos1d[("jet1_nemf"+id).c_str()]  ->Fill((*jets)[0].nemf());//,weight[index]);
		histos1d[("jet1_cemf"+id).c_str()]  ->Fill((*jets)[0].cemf());
		histos1d[("jet1_cmult"+id).c_str()] ->Fill((*jets)[0].cmulti());
		histos1d[("jet1_CSVMVAbdiscr"+id).c_str()]->Fill((*jets)[0].bDiscriminator("combinedSecondaryVertexMVABJetTags"));
	}

	if(njets >= 2){
		histos1d[("jet2_pt"+id).c_str()]->Fill((*jets)[1].Pt());
		histos1d[("jet2_eta"+id).c_str()]->Fill((*jets)[1].Eta());
		histos1d[("jet2_phi"+id).c_str()]->Fill((*jets)[1].Phi());
		histos1d[("jet2_emf"+id).c_str()]   ->Fill((*jets)[1].emf());
		histos1d[("jet2_chf"+id).c_str()]   ->Fill((*jets)[1].chf());
		histos1d[("jet2_fhpd"+id).c_str()]  ->Fill((*jets)[1].fHPD());
		histos1d[("jet2_n90"+id).c_str()]   ->Fill((*jets)[1].n90Hits());
		histos1d[("jet2_nconst"+id).c_str()]->Fill((*jets)[1].nconstituents());
		histos1d[("jet2_nhf"+id).c_str()]   ->Fill((*jets)[1].nhf());
		histos1d[("jet2_nemf"+id).c_str()]  ->Fill((*jets)[1].nemf());
		histos1d[("jet2_cemf"+id).c_str()]  ->Fill((*jets)[1].cemf());
		histos1d[("jet2_cmult"+id).c_str()] ->Fill((*jets)[1].cmulti());
		histos1d[("jet2_CSVMVAbdiscr"+id).c_str()]->Fill((*jets)[1].bDiscriminator("combinedSecondaryVertexMVABJetTags"));
	}

	if(njets >= 3){
		histos1d[("jet3_pt"+id).c_str()]->Fill((*jets)[2].Pt());
		histos1d[("jet3_eta"+id).c_str()]->Fill((*jets)[2].Eta());
		histos1d[("jet3_phi"+id).c_str()]->Fill((*jets)[2].Phi());
		histos1d[("jet3_emf"+id).c_str()]   ->Fill((*jets)[2].emf());
		histos1d[("jet3_chf"+id).c_str()]   ->Fill((*jets)[2].chf());
		histos1d[("jet3_fhpd"+id).c_str()]  ->Fill((*jets)[2].fHPD());
		histos1d[("jet3_n90"+id).c_str()]   ->Fill((*jets)[2].n90Hits());
		histos1d[("jet3_nconst"+id).c_str()]->Fill((*jets)[2].nconstituents());
		histos1d[("jet3_nhf"+id).c_str()]   ->Fill((*jets)[2].nhf());
		histos1d[("jet3_nemf"+id).c_str()]  ->Fill((*jets)[2].nemf());
		histos1d[("jet3_cemf"+id).c_str()]  ->Fill((*jets)[2].cemf());
		histos1d[("jet3_cmult"+id).c_str()] ->Fill((*jets)[2].cmulti());
		histos1d[("jet3_CSVMVAbdiscr"+id).c_str()]->Fill((*jets)[2].bDiscriminator("combinedSecondaryVertexMVABJetTags"));
	}
	if(njets >= 4){
	        histos1d[("jet4_pt"+id).c_str()]->Fill((*jets)[3].Pt());
		histos1d[("jet4_eta"+id).c_str()]->Fill((*jets)[3].Eta());
		histos1d[("jet4_phi"+id).c_str()]->Fill((*jets)[3].Phi());
		histos1d[("jet4_emf"+id).c_str()]   ->Fill((*jets)[3].emf());
		histos1d[("jet4_chf"+id).c_str()]   ->Fill((*jets)[3].chf());
		histos1d[("jet4_fhpd"+id).c_str()]  ->Fill((*jets)[3].fHPD());
		histos1d[("jet4_n90"+id).c_str()]   ->Fill((*jets)[3].n90Hits());
		histos1d[("jet4_nconst"+id).c_str()]->Fill((*jets)[3].nconstituents());
		histos1d[("jet4_nhf"+id).c_str()]   ->Fill((*jets)[3].nhf());
		histos1d[("jet4_nemf"+id).c_str()]  ->Fill((*jets)[3].nemf());
		histos1d[("jet4_cemf"+id).c_str()]  ->Fill((*jets)[3].cemf());
		histos1d[("jet4_cmult"+id).c_str()] ->Fill((*jets)[3].cmulti());
		histos1d[("jet4_CSVMVAbdiscr"+id).c_str()]->Fill((*jets)[3].bDiscriminator("combinedSecondaryVertexMVABJetTags"));
	}
	if(njets >= 5){
	    histos1d[("jet5_pt"+id).c_str()]->Fill((*jets)[4].Pt());
	    histos1d[("jet5_eta"+id).c_str()]->Fill((*jets)[4].Eta());
	    histos1d[("jet5_phi"+id).c_str()]->Fill((*jets)[4].Phi());
	    histos1d[("jet5_CSVMVAbdiscr"+id).c_str()]->Fill((*jets)[4].bDiscriminator("combinedSecondaryVertexMVABJetTags"));
        }


	for(std::vector<mor::Jet>::/*const_*/iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter){
		histos2d[("jet_pt_vs_et"+id).c_str()]->Fill(jet_iter->Pt(),jet_iter->Et());
		histos2d[("jet_eta_vs_phi"+id).c_str()]->Fill(jet_iter->Eta(), jet_iter->Phi());
		histos1d[("jet_pt"+id).c_str()]->Fill(jet_iter->Pt());
		histos1d[("jet_eta"+id).c_str()]->Fill(jet_iter->Eta());
		histos1d[("jet_phi"+id).c_str()]->Fill(jet_iter->Phi());
		double bdisc = jet_iter->bDiscriminator("combinedSecondaryVertexMVABJetTags");
		histos1d[("jet_CSVMVAbdiscr"+id).c_str()]->Fill(bdisc);
	}
	
	histos1d[("jet_number"+id).c_str()]->Fill(njets);
}

 // Plot eta,pt and number of muons
void BasicObjectPlots::plot_muons()
{
	// Get back vector of muons that passed all cuts in LeptonSelector class
	double 	dphi = 0.;
   
	for(std::vector<mor::Muon>::iterator isolated_mu = isolated_muons->begin();
	    isolated_mu!=isolated_muons->end(); 
	    ++isolated_mu){			
		histos1d[("muon_pt"+id).c_str()]->Fill(isolated_mu->Pt());
	        histos1d[("muon_eta"+id).c_str()]->Fill(isolated_mu->Eta());
                histos1d[("muon_eta_tf"+id).c_str()]->Fill(isolated_mu->Eta());
	        histos1d[("muon_abseta"+id).c_str()]->Fill(fabs(isolated_mu->Eta()));
	        histos1d[("muon_abseta_x_charge"+id).c_str()]->Fill(isolated_mu->charge()*fabs(isolated_mu->Eta()));
		histos1d[("muon_phi"+id).c_str()]->Fill(isolated_mu->Phi());
		//jl 03.11.10: added dphi(lep/MET)
		for(std::vector<mor::MET>::iterator met = corrected_mets->begin();met!=corrected_mets->end();++met)
		{
			dphi = isolated_mu->Phi() - met->Phi();
			if (dphi>TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
			if (dphi<-TMath::Pi()) dphi = 2*TMath::Pi() + dphi;
		}
		histos1d[("muon_dphi_lepmet"+id).c_str()]->Fill(fabs(dphi));
        }
	
	histos1d[("muon_number"+id).c_str()]->Fill(isolated_muons->size());
        histos1d[("event_weight_PDFrw"+id).c_str()]->Fill(handle_holder->get_event_information()->event_weight());
}

void BasicObjectPlots::plot_gen_info()
{
	if(handle_holder->get_ttbar_gen_evt()){
		if(handle_holder->get_ttbar_gen_evt()->decay_channel() == 1 ||
			 handle_holder->get_ttbar_gen_evt()->decay_channel() == 2){
 			histos1d[("gen_lep_eta"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->lepton()->eta());
 			histos1d[("gen_lep_pt"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->lepton()->pt());

 			histos1d[("gen_quark_eta"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->q()->eta());
 			histos1d[("gen_quark_eta"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->qbar()->eta());
 			histos1d[("gen_quark_pt"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->q()->pt());
 			histos1d[("gen_quark_pt"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->qbar()->pt());

			histos1d[("gen_leptop_pt"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->lepT()->pt());
			histos1d[("gen_leptop_eta"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->lepT()->eta());
			histos1d[("gen_hadtop_pt"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->hadT()->pt());
			histos1d[("gen_hadtop_eta"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->hadT()->eta());

			histos1d[("gen_leptop_y"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->lepT()->Rapidity());
			histos1d[("gen_hadtop_y"+id).c_str()]->Fill(handle_holder->get_ttbar_gen_evt()->hadT()->Rapidity());
		}
	}
}

// Plot eta,pt and number of electrons
void BasicObjectPlots::plot_electrons()
{
	// Get back vector of electrons that passed all cuts in LeptonSelector class
	double 	dphi = 0.;
	
	for(std::vector<mor::Electron>::iterator isolated_e = isolated_electrons->begin();isolated_e != isolated_electrons->end();++isolated_e){
                histos1d[("electron_pt"+id).c_str()]->Fill(isolated_e->Pt());
	        histos1d[("electron_eta"+id).c_str()]->Fill(isolated_e->Eta());
                histos1d[("electron_eta_tf"+id).c_str()]->Fill(isolated_e->Eta());
		histos1d[("electron_abseta"+id).c_str()]->Fill(fabs(isolated_e->Eta()));
		histos1d[("electron_abseta_x_charge"+id).c_str()]->Fill(isolated_e->charge()*fabs(isolated_e->Eta()));
	        histos1d[("electron_phi"+id).c_str()]->Fill(isolated_e->Phi());
		//jl 03.12.10: added dphi(lep/MET)
		for(std::vector<mor::MET>::iterator met = corrected_mets->begin();met!=corrected_mets->end();++met)
		{
			dphi = isolated_e->Phi() - met->Phi();
			if (dphi>TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
			if (dphi<-TMath::Pi()) dphi = 2*TMath::Pi() + dphi;
		}
		histos1d[("electron_dphi_lepmet"+id).c_str()]->Fill(fabs(dphi));

		histos2d[("electron_eta_vs_reliso"+id).c_str()]->Fill(isolated_e->eta(), isolated_e->PFrelIso());	
		histos1d[("electron_mvaid"+id).c_str()]->Fill(handle_holder->services()->mva_id_calc()->discriminator(*isolated_e));

		if(is_trig_e(*isolated_e))
			histos1d[("eMVA_trig_frac_sel_e"+id).c_str()]->Fill(1);
		else
			histos1d[("eMVA_trig_frac_sel_e"+id).c_str()]->Fill(0);
        }

	histos1d[("electron_number"+id).c_str()]->Fill(isolated_electrons->size());
	histos1d[("event_weight_PDFrw"+id).c_str()]->Fill(handle_holder->get_event_information()->event_weight());
}

bool BasicObjectPlots::is_trig_e(mor::Electron& ele)
{
  bool myTrigPresel = false;
  if(fabs(ele.supercluster_eta()) < 1.485) {
    if(ele.sigmaIetaIeta() < 0.014 &&
       ele.hadronicOverEm() < 0.15 &&
       ele.trackIso()/ele.pt() < 0.2 &&
       ele.ecalIso()/ele.pt() < 0.2 &&
       ele.hcalIso()/ele.pt() < 0.2 &&
       ele.nLostTrackerHits() == 0)
      myTrigPresel = true;
  }
  else {
    if(ele.sigmaIetaIeta() < 0.035 &&
       ele.hadronicOverEm() < 0.10 &&
       ele.trackIso()/ele.pt() < 0.2 &&
       ele.ecalIso()/ele.pt() < 0.2 &&
       ele.hcalIso()/ele.pt() < 0.2 &&
       ele.nLostTrackerHits() == 0)
      myTrigPresel = true;
  }
  
  
  return myTrigPresel;
}


void BasicObjectPlots::plot_vertex()
{
	for(std::vector<mor::PrimaryVertex>::iterator pvit = pvert->begin(); pvit != pvert->end(); ++pvit)
	{
		histos1d[("pv_isfake"+id).c_str()]->Fill(pvit->is_fake());
		histos1d[("pv_z"+id).c_str()]     ->Fill(pvit->z());
		histos1d[("pv_rho"+id).c_str()]   ->Fill(pvit->rho());
		histos1d[("pv_ndof"+id).c_str()]  ->Fill(pvit->ndof());
		break;
	}
	histos1d[("npv"+id).c_str()]->Fill(pvert->size());
	histos1d[("npu_truth"+id).c_str()]->Fill(handle_holder->get_event_information()->true_npu());
}
