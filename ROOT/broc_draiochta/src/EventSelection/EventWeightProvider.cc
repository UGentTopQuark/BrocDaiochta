#include "../../interface/EventSelection/EventWeightProvider.h"

broc::EventWeightProvider::EventWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;

	scale_factor_2D = false;
	nbins2 = 1;
	range_min2 = -1;
	range_max2 = -1;
	bin_width2 = -1;
	weight_file_reader = new eire::ConfigReader();
	weight_file_reader->read_config_from_file(weight_file);
	read_weight_file();
}

broc::EventWeightProvider::~EventWeightProvider()
{
}

void broc::EventWeightProvider::read_weight_file()
{
	std::string dataset = get_dataset_name();
	if(!weight_file_reader->section_defined(dataset)){
		if(!weight_file_reader->section_defined("global")){
			std::cerr << "WARNING: broc::EventWeightProvider::read_weight_file(): problem with weight file and dataset: " << dataset << std::endl;	
		}
		else
			dataset = "global";
	}
	scale_factor_2D = weight_file_reader->get_bool_var("scale_factor_2D", dataset, false);
	if(scale_factor_2D){
		nbins2 = atof(weight_file_reader->get_var("nbins2", dataset, true).c_str());
		std::vector<std::string> range_vec2 = weight_file_reader->get_vec_var("range2", dataset, true);
		assert(range_vec2.size() == 2);
		range_min2 = atof(range_vec2[0].c_str());
		range_max2 = atof(range_vec2[1].c_str());
		bin_width2 = (range_max2 - range_min2)/nbins2;
	}
	nbins = atof(weight_file_reader->get_var("nbins", dataset, true).c_str());
	std::vector<std::string> range_vec = weight_file_reader->get_vec_var("range", dataset, true);
	assert(range_vec.size() == 2);
	range_min = atof(range_vec[0].c_str());
	range_max = atof(range_vec[1].c_str());
	bin_width = (range_max - range_min)/nbins;
	
	std::vector<std::string> variables = weight_file_reader->get_variables_for_section(dataset);
	
	// find the size for the weights vector
	int max = -1;
	for(std::vector<std::string>::iterator var = variables.begin();
	    var != variables.end();
	    ++var){
		if(*var == "nbins" || *var == "range"|| *var == "nbins2" || *var == "range2") continue;
		int current = atoi(var->c_str());
		if(current > max) max = current;
	}
	if((max != nbins && !scale_factor_2D) || (scale_factor_2D && max != nbins*nbins2)){ 
		std::cerr << "Error: Max of scale factors set != nbins for. max: " << max << " nbins: " << nbins*nbins2 << std::endl;
		exit(1);
	}
	weights.clear();
	weights.assign(max+1, 0);
		
	// fill the weights vector
	for(std::vector<std::string>::iterator var = variables.begin();
	    var != variables.end();
	    ++var){
		if(*var == "nbins" || *var == "range") continue;
		int current_bin = atoi(var->c_str());
		double current_weight = atof(weight_file_reader->get_var(*var, dataset, true).c_str());
		weights[current_bin] = current_weight;
	}
}

double broc::EventWeightProvider::get_weight()
{	
	if(!scale_factor_2D){
		double value = get_value_for_event();
		return weights[find_bin(value)];
	}else{
		double value = get_value_for_event();
		double value2 = get_2nd_value_for_event();
		return weights[find_bin(value,value2)];
	}
}

int broc::EventWeightProvider::find_bin(double value)
{
	if(value < range_min || value > range_max)
		return -1;
	if(range_min > 0)
		value -= range_min;
	else if(range_min < 0)
		value += fabs(range_min);
	unsigned int assigned_bin = int(value / bin_width);

	if(assigned_bin < 0 || assigned_bin+1 > weights.size()){
		std::cerr << "WARNING: broc::EventWeightProvider::find_bin(): found invalid weight bin: " << assigned_bin << std::endl;
		exit(1);
	}

	return assigned_bin+1;
}

int broc::EventWeightProvider::find_bin(double value, double value2)
{
	if(value < range_min || value > range_max || value2 < range_min2 || value2 > range_max2)
		return -1;

	if(range_min > 0)
		value -= range_min;
	else if(range_min < 0)
		value += fabs(range_min);
	int var1_bin= int(value / bin_width) + 1;
	if(range_min2 > 0)
		value2 -= range_min2;
	else if(range_min2 < 0)
		value2 += fabs(range_min2);
	int var2_bin = int(value2 / bin_width2) + 1;

	unsigned int assigned_bin = int(var1_bin + ((var2_bin-1)*nbins)); 

	if(assigned_bin < 0 || assigned_bin > weights.size()){
		std::cerr << "WARNING: broc::EventWeightProvider::find_bin(): found invalid weight bin: " << assigned_bin << std::endl;
		exit(1);
	}

	return assigned_bin;
}

std::string broc::EventWeightProvider::get_dataset_name()
{
	std::string dataset_name = handle_holder->get_ident();

	dataset_name = dataset_name.substr(1,dataset_name.find("|"));

	return dataset_name;
}

double broc::EventWeightProvider::get_2nd_value_for_event()
{
	return -999;
}
