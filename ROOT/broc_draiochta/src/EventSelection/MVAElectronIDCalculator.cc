#include "../../interface/EventSelection/MVAElectronIDCalculator.h"

broc::MVAElectronIDCalculator::MVAElectronIDCalculator(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	this->config_reader = handle_holder->get_config_reader();
	this->id = "_"+handle_holder->get_ident();

	myMVANonTrig = 0;
	myMVATrig = 0;

	initialise();

	histo_writer = handle_holder->get_histo_writer();

	if(control_plots){
		book_histos();
	}
}

broc::MVAElectronIDCalculator::~MVAElectronIDCalculator()
{
	if(myMVANonTrig){ delete myMVANonTrig; myMVANonTrig = 0; }
	if(myMVATrig){ delete myMVATrig; myMVATrig = 0; }
}

bool broc::MVAElectronIDCalculator::is_trig_e(mor::Electron& ele)
{
  bool myTrigPresel = false;
  if(fabs(ele.supercluster_eta()) < 1.485) {
    if(ele.sigmaIetaIeta() < 0.014 &&
       ele.hadronicOverEm() < 0.15 &&
       ele.trackIso()/ele.pt() < 0.2 &&
       ele.ecalIso()/ele.pt() < 0.2 &&
       ele.hcalIso()/ele.pt() < 0.2 &&
       ele.nLostTrackerHits() == 0)
      myTrigPresel = true;
  }
  else {
    if(ele.sigmaIetaIeta() < 0.035 &&
       ele.hadronicOverEm() < 0.10 &&
       ele.trackIso()/ele.pt() < 0.2 &&
       ele.ecalIso()/ele.pt() < 0.2 &&
       ele.hcalIso()/ele.pt() < 0.2 &&
       ele.nLostTrackerHits() == 0)
      myTrigPresel = true;
  }
  
  
  return myTrigPresel;
}

double broc::MVAElectronIDCalculator::discriminator(mor::Electron& ele)
{
	if(!myMVATrig || !myMVANonTrig)
		return -999;

	double myMVAVar_fbrem = ele.myMVAVar_fbrem(); 
	double myMVAVar_kfchi2 = ele.myMVAVar_kfchi2();
	int myMVAVar_kfhits = (int) ele.myMVAVar_kfhits();
	double myMVAVar_gsfchi2 = ele.myMVAVar_gsfchi2();
	double myMVAVar_deta = ele.myMVAVar_deta();
	double myMVAVar_dphi = ele.myMVAVar_dphi();
	double myMVAVar_detacalo = ele.myMVAVar_detacalo();
	//double myMVAVar_dphicalo = ele.myMVAVar_dphicalo();
	double myMVAVar_see = ele.myMVAVar_see();
	double myMVAVar_spp = ele.myMVAVar_spp();
	double myMVAVar_etawidth = ele.myMVAVar_etawidth();
	double myMVAVar_phiwidth = ele.myMVAVar_phiwidth();
	double myMVAVar_e1x5e5x5 = ele.myMVAVar_e1x5e5x5();
	double myMVAVar_R9 = ele.myMVAVar_R9();
	//double myMVAVar_nbrems = ele.myMVAVar_nbrems();
	double myMVAVar_HoE = ele.myMVAVar_HoE();
	double myMVAVar_EoP = ele.myMVAVar_EoP();
	double myMVAVar_IoEmIoP = ele.myMVAVar_IoEmIoP();
	double myMVAVar_eleEoPout = ele.myMVAVar_eleEoPout();
	double myMVAVar_PreShowerOverRaw = ele.myMVAVar_PreShowerOverRaw();
	//double myMVAVar_EoPout = ele.myMVAVar_EoPout();
	double myMVAVar_d0 = ele.myMVAVar_d0();
	double myMVAVar_ip3d = ele.myMVAVar_ip3d();
	double myMVAVar_eta = ele.myMVAVar_eta();
	double myMVAVar_pt = ele.myMVAVar_pt();

	bool debugMyVar = false;

	double mva_output=0.;
	if(is_trig_e(ele)){
		mva_output = myMVATrig->mvaValue( myMVAVar_fbrem, 
						    myMVAVar_kfchi2,
						    myMVAVar_kfhits,
						    myMVAVar_gsfchi2,
						    myMVAVar_deta,
						    myMVAVar_dphi,
						    myMVAVar_detacalo,
						    // myMVAVar_dphicalo,
						    myMVAVar_see,
						    myMVAVar_spp,
						    myMVAVar_etawidth,
						    myMVAVar_phiwidth,
						    myMVAVar_e1x5e5x5,
						    myMVAVar_R9,
						    //myMVAVar_nbrems,
						    myMVAVar_HoE,
						    myMVAVar_EoP,
						    myMVAVar_IoEmIoP,
						    myMVAVar_eleEoPout,
						    myMVAVar_PreShowerOverRaw,
						    // myMVAVar_EoPout,
						    myMVAVar_d0,
						    myMVAVar_ip3d,
						    myMVAVar_eta,
						    myMVAVar_pt,
						    debugMyVar);
		if(control_plots) histos1d[("eMVA_trig_frac"+id).c_str()]->Fill(1);
	}else{
		mva_output = myMVANonTrig->mvaValue(myMVAVar_fbrem,
						    myMVAVar_kfchi2,
						    myMVAVar_kfhits,
						    myMVAVar_gsfchi2,
						    myMVAVar_deta,
						    myMVAVar_dphi,
						    myMVAVar_detacalo,
						    // myMVAVar_dphicalo,
						    myMVAVar_see,
						    myMVAVar_spp,
						    myMVAVar_etawidth,
						    myMVAVar_phiwidth,
						    myMVAVar_e1x5e5x5,
						    myMVAVar_R9,
						    //myMVAVar_nbrems,
						    myMVAVar_HoE,
						    myMVAVar_EoP,
						    myMVAVar_IoEmIoP,
						    myMVAVar_eleEoPout,
						    myMVAVar_PreShowerOverRaw,
						    // myMVAVar_EoPout,
						    myMVAVar_d0,
						    myMVAVar_ip3d,
						    myMVAVar_eta,
						    myMVAVar_pt,
						    debugMyVar);
		if(control_plots) histos1d[("eMVA_trig_frac"+id).c_str()]->Fill(0);
	}

	return mva_output;
}

void broc::MVAElectronIDCalculator::initialise()
{	
	std::string directory = "weights/";

	if(config_reader->section_defined("e_mva_id")){
		std::vector<std::string> weight_files = config_reader->get_vec_var("non_trig_weights", "e_mva_id");
		std::vector<std::string> myManualCatWeigths;

		for(std::vector<std::string>::iterator weight_file = weight_files.begin();
			weight_file != weight_files.end();
			++weight_file){
			myManualCatWeigths.push_back(directory+*weight_file);
		}
		
		Bool_t manualCat = true;
		
		myMVANonTrig = new EGammaMvaEleEstimator();
		myMVANonTrig->initialize("BDT",
		      		   EGammaMvaEleEstimator::kNonTrig,
		      		   manualCat, 
		      		   myManualCatWeigths);
		
		// NOTE: it is better if you copy the MVA weight files locally
		weight_files = config_reader->get_vec_var("trig_weights", "e_mva_id");

		std::vector<std::string> myManualCatWeigthsTrig;

		for(std::vector<std::string>::iterator weight_file = weight_files.begin();
			weight_file != weight_files.end();
			++weight_file){
			myManualCatWeigthsTrig.push_back(directory+*weight_file);
		}
		
		myMVATrig = new EGammaMvaEleEstimator();
		myMVATrig->initialize("BDT",
				EGammaMvaEleEstimator::kTrig,
				manualCat,
				myManualCatWeigthsTrig);
	}
}

void broc::MVAElectronIDCalculator::book_histos()
{
	histos1d[("eMVA_trig_frac"+id).c_str()]=histo_writer->create_1d(("eMVA_trig_frac"+id).c_str(),"amount of electrons in triggering vs non-triggering MVA",5,-0.5,4.5, "0=non-trig, 1=trig");
}
