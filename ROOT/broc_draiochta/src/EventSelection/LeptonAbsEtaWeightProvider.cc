#include "../../interface/EventSelection/LeptonAbsEtaWeightProvider.h"

broc::LeptonAbsEtaWeightProvider::LeptonAbsEtaWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder): broc::LeptonEfficiencyWeightProvider(weight_file, handle_holder)
{
}

broc::LeptonAbsEtaWeightProvider::~LeptonAbsEtaWeightProvider()
{
}

double broc::LeptonAbsEtaWeightProvider::get_value_for_event()
{
	double lepton_eta=-999;
	if(process_muons && muons->size() > 0){
		lepton_eta = fabs(muons->begin()->eta());
	}else if(process_electrons && electrons->size() > 0){
		lepton_eta = fabs(electrons->begin()->eta());
	}

	return lepton_eta;
}
