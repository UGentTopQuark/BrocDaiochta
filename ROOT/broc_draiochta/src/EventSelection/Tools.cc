#include "../../interface/EventSelection/Tools.h"

double Tools::deltaR(MyLorentzVector p4_1,
              MyLorentzVector p4_2)
{
        double R;               // delta R between objects
        double delta_phi = ROOT::Math::VectorUtil::DeltaPhi(p4_1,p4_2);
        double delta_eta = p4_1.eta()-p4_2.eta();
        R = sqrt(delta_eta * delta_eta + delta_phi * delta_phi);
        return R;
}

std::string Tools::stringify(double x)
{
        std::ostringstream o;
        if (!(o << x))
                return "stringify(double) error";
        return o.str();
}
