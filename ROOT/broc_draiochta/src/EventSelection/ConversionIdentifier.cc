#include "../../interface/EventSelection/ConversionIdentifier.h"

eire::ConversionIdentifier::ConversionIdentifier()
{
	max_dcot = -1;
	max_dist = -1;
	max_nLostTrackerHits = -1;

	is_conv = 0;
	is_not_conv = 0;
}

eire::ConversionIdentifier::~ConversionIdentifier()
{
	if(verbose && is_conv+is_not_conv > 0){
		std::cout << "conversion electron selection efficiency: " << is_not_conv/(is_conv+is_not_conv) << std::endl;
	}
}

void eire::ConversionIdentifier::set_cuts(broc::CutsSet *cuts_set, std::string lepton_type)
{
	max_dcot = cuts_set->get_cut_value("max_"+lepton_type+"_dcot");
	max_dist = cuts_set->get_cut_value("max_"+lepton_type+"_dist");

	std::vector<double> *lost_hits = cuts_set->get_vcut_value("max_"+lepton_type+"_nLostTrackerHits");
	if(lost_hits != NULL && lost_hits->size() > 0)
		max_nLostTrackerHits = (*(cuts_set->get_vcut_value("max_"+lepton_type+"_nLostTrackerHits")))[0];
	else
		max_nLostTrackerHits = -1;
}

bool eire::ConversionIdentifier::from_conversion(mor::Electron *electron)
{
	bool is_conversion = false;

	if(max_dcot != -1 && max_dist != -1 && (fabs(electron->dcot()) < max_dcot) && (fabs(electron->dist()) < max_dist))
		is_conversion = true;
	else if(max_dcot != -1 && max_dist == -1 && (fabs(electron->dcot()) < max_dcot))
		is_conversion = true;
	else if(max_dcot == -1 && max_dist != -1 && (fabs(electron->dist()) < max_dist))
		is_conversion = true;

	if(max_nLostTrackerHits != -1 && electron->nLostTrackerHits() > max_nLostTrackerHits)
		is_conversion = true;

	if(is_conversion) is_conv++;
	else is_not_conv++;

	return is_conversion;
}
