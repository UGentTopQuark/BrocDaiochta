#include "../../interface/EventSelection/QCDMMPlots.h"

using namespace std;


QCDMMPlots::QCDMMPlots(eire::HandleHolder *handle_holder, bool bookmyhists)
{
	id        = handle_holder->get_ident();
	set_handles(handle_holder);

	ht        = -1; 
	mass_reco = NULL; //jl 17.12.10
	ht_calc   = NULL;
	pv        = NULL; //jl 23.12.10

	weight = -1.;
	w1 = -1.;
	w2 = -1.;
	
	uncut_jets = handle_holder->get_jets();	
	pv = handle_holder->get_primary_vertices();
	jets = handle_holder->get_tight_jets();
	isolated_electrons = handle_holder->get_tight_electrons();
	loose_electrons = handle_holder->get_loose_mm_electrons();
	isolated_muons = handle_holder->get_tight_muons();
	loose_muons = handle_holder->get_loose_mm_muons();
	corrected_mets = handle_holder->get_selected_mets();

	isData = true;
	isMuon = true; 
	cout << "[QCDMMPlots]: cutset id is " << id << endl;
	//jl 19.08.10: steer the code according to input and lepton types
	//this is dirty but a working fix for now....
	//of course this is meaningful only for data, ignore when running over MC
        if(id.find("Data") != string ::npos)
	{
                cout << "[QCDMMPlots]: Running over data, MM plots will be filled" << endl;
		if(id.find("muon") != string ::npos)
			cout << "[QCDMMPlots]: Muon channel - I'll use the muon parameters" << endl;	
		else
		{
			cout << "[QCMDMPlots]: Electron channel - I'll use the electron parameters" << endl;
			isMuon = false;
		}
	}
	else
	{
		cout << "[QCMDMPlots]: !WARNING! NOT running over data" << endl;
		cout << "[QCDMMPlots]: !WARNING! MM plots will NOT be filled" << endl;
		isData = false;
	}

	if(bookmyhists)	bookhistos();
}

QCDMMPlots::~QCDMMPlots()
{
}

void QCDMMPlots::plot_all()
{
	//let's not plot junk...
	if(!isData) return;
	if(isMuon && (loose_muons->size() == 0 || loose_muons->size() >1)) return;
	if(!isMuon && (loose_electrons->size() == 0 || loose_electrons->size() >1)) return;
	
	plot_QCDjets();	
	plot_QCDmuons();
	plot_QCDelectrons();
	plot_QCDht();
	plot_QCDdR();
	plot_QCDmuon_quality();
	plot_QCDelectron_quality();
	plot_QCDiso();
	plot_QCDMET_MtW();
	plot_top();
	plot_PV(); // jl 23.12.10
        //this plot is relevant for the "loose" cutset only, ie that which contains exactly one loose and at most one tight lepton
        histos1d[("QCDMM_weights_"+id).c_str()]->Fill(weight);
}

void QCDMMPlots::set_uncut_jets(std::vector<mor::Jet>* uncut_jets)
{
	this->uncut_jets = uncut_jets;
}

void QCDMMPlots::set_ht_calc(HtCalculator *ht_calc)
{
	this->ht_calc = ht_calc;
}
//jl 17.12.10
void QCDMMPlots::set_mass_reco(MassReconstruction *mass_reco)
{
	this->mass_reco = mass_reco;
}
//jl 23.12.10
void QCDMMPlots::set_PV(std::vector<mor::PrimaryVertex> *pvert)
{
	this->pv = pvert;
}

void QCDMMPlots::bookhistos()
{
	//////////jets	
	histos1d[("QCDMM_jet_pt_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet_pt_"+id).c_str(),"p_{T} of jets in an event",75,0,300, "Jet p_{T} [GeV]");//30,0,120
	histos1d[("QCDMM_jet_eta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet_eta_"+id).c_str(),"eta of jets in an event",10,-3,3, "Jet #eta");
	histos1d[("QCDMM_jet_phi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet_phi_"+id).c_str(),"phi of jets in an event",10,-4.0,4.0, "Jet #phi");
	histos1d[("QCDMM_jet_number_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet_number_"+id).c_str(),"jet multiplicity",10,-0.5,9.5, "Jet Multiplicity");
	histos1d[("QCDMM_jet1_pt_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_pt_"+id).c_str(),"p_{T} of leading jet",75,0,300, "Jet 1 p_{T} [GeV]");//50,0,500
	histos1d[("QCDMM_jet1_eta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_eta_"+id).c_str(),"eta of leading jet",10,-3.,3, "Jet 1 #eta");//90,-6,6
	histos1d[("QCDMM_jet2_pt_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_pt_"+id).c_str(),"p_{T} of jet 2 ",75,0,300, "Jet 2 p_{T} [GeV]"); //50,0,500
	histos1d[("QCDMM_jet2_eta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_eta_"+id).c_str(),"eta of jet 2 ",10,-3.,3, "Jet 2 #eta");
	histos1d[("QCDMM_jet3_pt_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_pt_"+id).c_str(),"p_{T} of jet 3",30,0,120, "Jet 3 p_{T} [GeV]");//50,0,500
	histos1d[("QCDMM_jet3_eta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_eta_"+id).c_str(),"eta of jet 3 ",10,-3.,3, "Jet 3 #eta");
	histos1d[("QCDMM_jet4_pt_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_pt_"+id).c_str(),"p_{T} of jet 4",30,0,120, "Jet 4 p_{T}");//50,0,500
	histos1d[("QCDMM_jet4_eta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_eta_"+id).c_str(),"eta of jet 4 ",10,-3.,3, "Jet 4 #eta");
	histos1d[("QCDMM_jet1_phi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_phi_"+id).c_str(),"phi of jet 1 in an event",10,-4.0,4.0, "Jet 1 #phi");
	histos1d[("QCDMM_jet2_phi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_phi_"+id).c_str(),"phi of jet 2 in an event",10,-4.0,4.0, "Jet 2 #phi");
	histos1d[("QCDMM_jet3_phi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_phi_"+id).c_str(),"phi of jet 3 in an event",10,-4.0,4.0, "Jet 3  #phi");
	histos1d[("QCDMM_jet4_phi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_phi_"+id).c_str(),"phi of jet 4 in an event",10,-4.0,4.0, "Jet 4 #phi");	
	histos1d[("QCDMM_jet1_emf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_emf_"+id).c_str(),"emf of jet 1 in an event",40,-0.8,1.2, "Jet 1 emf");
	histos1d[("QCDMM_jet1_chf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_chf_"+id).c_str(),"chf of jet 1 in an event",40,-0.8,1.2, "Jet 1 chf");
	histos1d[("QCDMM_jet1_fhpd_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_fhpd_"+id).c_str(),"fHPD of jet 1 in an event",60,-1.2,1.2, "Jet 1 fHPD");
	histos1d[("QCDMM_jet1_n90_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_n90_"+id).c_str(),"n90Hits of jet 1 in an event",50,0,100, "Jet 1 n90Hits");
	histos1d[("QCDMM_jet1_nconst_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_nconst_"+id).c_str(),"nconstituents of jet 1 in an event",50,0,50, "Jet 1 nconst");
	histos1d[("QCDMM_jet1_nhf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_nhf_"+id).c_str(),"nhf of jet 1 in an event",20,0,1, "Jet 1 nhf");
	histos1d[("QCDMM_jet1_nemf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_nemf_"+id).c_str(),"nemf of jet 1 in an event",20,0,1, "Jet 1 nemf");
	histos1d[("QCDMM_jet1_cemf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_cemf_"+id).c_str(),"cemf of jet 1 in an event",20,0,1, "Jet 1 cemf");
	histos1d[("QCDMM_jet1_cmult_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet1_cmult_"+id).c_str(),"charged multiplicity of jet 1 in an event",40,0,40, "Jet 1 cmult");
	histos1d[("QCDMM_jet2_emf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_emf_"+id).c_str(),"emf of jet 2 in an event",40,-0.8,1.2, "Jet 2 emf");
	histos1d[("QCDMM_jet2_chf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_chf_"+id).c_str(),"chf of jet 2 in an event",40,-0.8,1.2, "Jet 2 chf");
	histos1d[("QCDMM_jet2_fhpd_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_fhpd_"+id).c_str(),"fHPD of jet 2 in an event",60,-1.2,1.2, "Jet 2 fHPD");
	histos1d[("QCDMM_jet2_n90_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_n90_"+id).c_str(),"n90Hits of jet 2 in an event",50,0,100, "Jet 2 n90Hits");
	histos1d[("QCDMM_jet2_nconst_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_nconst_"+id).c_str(),"nconstituents of jet 2 in an event",50,0,50, "Jet 2 nconst");
	histos1d[("QCDMM_jet2_nhf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_nhf_"+id).c_str(),"nhf of jet 2 in an event",20,0,1, "Jet 2 nhf");
	histos1d[("QCDMM_jet2_nemf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_nemf_"+id).c_str(),"nemf of jet 2 in an event",20,0,1, "Jet 2 nemf");
	histos1d[("QCDMM_jet2_cemf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_cemf_"+id).c_str(),"cemf of jet 2 in an event",20,0,1, "Jet 2 cemf");
	histos1d[("QCDMM_jet2_cmult_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet2_cmult_"+id).c_str(),"charged multiplicity of jet 2 in an event",40,0,40, "Jet 2 cmult");
	histos1d[("QCDMM_jet3_emf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_emf_"+id).c_str(),"emf of jet 3 in an event",40,-0.8,1.2, "Jet 3 emf");
	histos1d[("QCDMM_jet3_chf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_chf_"+id).c_str(),"chf of jet 3 in an event",40,-0.8,1.2, "Jet 3 chf");
	histos1d[("QCDMM_jet3_fhpd_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_fhpd_"+id).c_str(),"fHPD of jet 3 in an event",60,-1.2,1.2, "Jet 3 fHPD");
	histos1d[("QCDMM_jet3_n90_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_n90_"+id).c_str(),"n90Hits of jet 3 in an event",50,0,100, "Jet 3 n90Hits");
	histos1d[("QCDMM_jet3_nconst_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_nconst_"+id).c_str(),"nconstituents of jet 3 in an event",50,0,50, "Jet 3 nconst");
	histos1d[("QCDMM_jet3_nhf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_nhf_"+id).c_str(),"nhf of jet 3 in an event",20,0,1, "Jet 3 nhf");
	histos1d[("QCDMM_jet3_nemf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_nemf_"+id).c_str(),"nemf of jet 3 in an event",20,0,1, "Jet 3 nemf");
	histos1d[("QCDMM_jet3_cemf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_cemf_"+id).c_str(),"cemf of jet 3 in an event",20,0,1, "Jet 3 cemf");
	histos1d[("QCDMM_jet3_cmult_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet3_cmult_"+id).c_str(),"charged multiplicity of jet 3 in an event",40,0,40, "Jet 3 cmult");
	histos1d[("QCDMM_jet4_emf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_emf_"+id).c_str(),"emf of jet 4 in an event",40,-0.8,1.2, "Jet 4 emf");
	histos1d[("QCDMM_jet4_chf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_chf_"+id).c_str(),"chf of jet 4 in an event",40,-0.8,1.2, "Jet 4 chf");
	histos1d[("QCDMM_jet4_fhpd_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_fhpd_"+id).c_str(),"fHPD of jet 4 in an event",60,-1.2,1.2, "Jet 4 fHPD");
	histos1d[("QCDMM_jet4_n90_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_n90_"+id).c_str(),"n90Hits of jet 4 in an event",50,0,100, "Jet 4 n90Hits");
	histos1d[("QCDMM_jet4_nconst_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_nconst_"+id).c_str(),"nconstituents of jet 4 in an event",50,0,50, "Jet 4 nconst");
	histos1d[("QCDMM_jet4_nhf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_nhf_"+id).c_str(),"nhf of jet 4 in an event",20,0,1, "Jet 4 nhf");
	histos1d[("QCDMM_jet4_nemf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_nemf_"+id).c_str(),"nemf of jet 4 in an event",20,0,1, "Jet 4 nemf");
	histos1d[("QCDMM_jet4_cemf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_cemf_"+id).c_str(),"cemf of jet 4 in an event",20,0,1, "Jet 4 cemf");
	histos1d[("QCDMM_jet4_cmult_"+id).c_str()]=histo_writer->create_1d(("QCDMM_jet4_cmult_"+id).c_str(),"charged multiplicity of jet 4 in an event",40,0,40, "Jet 4 cmult");
	
	//////////muons
	histos1d[("QCDMM_muon_number_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_number_"+id).c_str(),"muon multiplicity ",10,-0.5,9.5, "Muon Multiplicity");
	histos1d[("QCDMM_muon_pt_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_pt_"+id).c_str(),"p_{T} of muons in an event",70,0,350, "Muon p_{T} [GeV]"); //50,0,100  //70,0,350
	histos1d[("QCDMM_muon_eta_tf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_eta_tf_"+id).c_str(),"eta of muons in an event",300,-3,3, "Muon #eta");
        histos1d[("QCDMM_muon_eta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_eta_"+id).c_str(),"eta of muons in an event",10,-3,3, "Muon #eta");
	histos1d[("QCDMM_muon_abseta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_abseta_"+id).c_str(),"abs eta of muons in an event",150,0,3, "Muon #eta");
	histos1d[("QCDMM_muon_phi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_phi_"+id).c_str(),"phi of muons in an event",10,-4.0,4.0, "Muon #phi");
	histos1d[("QCDMM_muon_d0_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_d0_"+id).c_str(),"d0 of muons in an event",30,-0.3,0.3, "Muon impact parameter d_{0} [cm]");				
	histos1d[("QCDMM_muon_nHits_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_nHits_"+id).c_str(),"number of Hits in the tracker of muons in an event",30,-0.5,29.5, "Muon number of tracker hits");
	histos1d[("QCDMM_muon_chi2_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_chi2_"+id).c_str(),"normalised #chi^{2} of muons in an event",20,0.0,10, "Muon global normalised #chi^{2}");
	
	/////////electrons
	histos1d[("QCDMM_electron_number_"+id).c_str()]=histo_writer->create_1d(("QCDMM_electron_number_"+id).c_str(),"electron multiplicity ",10,-0.5,9.5, "Electron Multiplicity");
	histos1d[("QCDMM_electron_pt_"+id).c_str()]=histo_writer->create_1d(("QCDMM_electron_pt_"+id).c_str(),"p_{T} of electron in an event",70,0,350, "Electron p_{T} [GeV]");
	histos1d[("QCDMM_electron_eta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_electron_eta_"+id).c_str(),"eta of electron in an event",10,-3.0,3.0, "Electron #eta");
        histos1d[("QCDMM_electron_eta_tf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_electron_eta_tf_"+id).c_str(),"eta of electron in an event",300,-3.0,3.0, "Electron #eta");
	histos1d[("QCDMM_electron_abseta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_electron_abseta_"+id).c_str(),"abs eta of electron in an event",150,0,3.0, "Electron #eta");
	histos1d[("QCDMM_electron_phi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_electron_phi_"+id).c_str(),"phi of electrons in an event",10,-4.0,4.0, "Electron #phi");
	histos1d[("QCDMM_electron_d0_"+id).c_str()]=histo_writer->create_1d(("QCDMM_electron_d0_"+id).c_str(),"d0 of electrons in an event",30,-0.3,0.3, "Electron impact parameter d_{0} [cm]");
	
	///////////Ht, Et
	histos1d[("QCDMM_Ht_"+id).c_str()]=histo_writer->create_1d(("QCDMM_Ht_"+id).c_str(),"Ht: Sum pt of all jets and muon and MET",80,0,2000, "H_{T}^{MET} [GeV]");
	histos1d[("QCDMM_Ht_wo_MET_"+id).c_str()]=histo_writer->create_1d(("QCDMM_Ht_wo_MET_"+id).c_str(),"Ht: Sum pt of all jets and muon",80,0,2000, "H_{T} [GeV]");
	
	///////////MET, MW
	histos1d[("QCDMM_Event_MET_"+id).c_str()]=histo_writer->create_1d(("QCDMM_Event_MET_"+id).c_str(),"missing Et in event",30,0,240, "MET [GeV]"); //30,0,120
	histos1d[("QCDMM_Event_MET_pt_"+id).c_str()]=histo_writer->create_1d(("QCDMM_Event_MET_pt_"+id).c_str(),"Neutrino pt in event",40,0,400, "MET [GeV]");
	histos1d[("QCDMM_muon_MtW_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_MtW_"+id).c_str(),"Leptonic Transverse Mass with muon",30,0, 300, "M_{T}(W) [GeV]");//30,0,200
	histos1d[("QCDMM_electron_MtW_"+id).c_str()]=histo_writer->create_1d(("QCDMM_electron_MtW_"+id).c_str(),"Leptonic Transverse Mass with electron",60,0, 400, "M_{T}(W) [GeV]");
	//jl 03.11.10: dphi(mu,MET)
        histos1d[("QCDMM_muon_dphi_lepmet_"+id).c_str()]=histo_writer->create_1d(("QCDMM_muon_dphi_lepmet_"+id).c_str(),"DeltaPhi between muon and MET",32,0.,3.2, "DeltaPhi between muon and MET");
        histos1d[("QCDMM_electron_dphi_lepmet_"+id).c_str()]=histo_writer->create_1d(("QCDMM_electron_dphi_lepmet_"+id).c_str(),"DeltaPhi between electron and MET",32,0.,3.2, "DeltaPhi between electron and MET");

	//jl 08.11.10: MET phi
	histos1d[("QCDMM_Event_MET_phi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_MET_phi_"+id).c_str(),"phi of MET",20,-4.,4., "MET #phi");        
	//jl 17.12.10: add MET px, py
	histos1d[("QCDMM_Event_MET_px_"+id).c_str()]=histo_writer->create_1d(("QCDMM_Event_MET_px_"+id).c_str(),"Missing px",50,-200,200, "Missing px (GeV)");
	histos1d[("QCDMM_Event_MET_py_"+id).c_str()]=histo_writer->create_1d(("QCDMM_Event_MET_py_"+id).c_str(),"Missing py",50,-200,200, "Missing py (GeV)");

	//////////dR
	histos1d[("QCDMM_min_mu_dR_"+id).c_str()]=histo_writer->create_1d(("QCDMM_min_mu_dR_"+id).c_str(),"deltaR between muon(s) and closest jet",50,0,7,"#Delta R(#mu, closest jet)");
	histos1d[("QCDMM_min_e_dR_"+id).c_str()]=histo_writer->create_1d(("QCDMM_min_e_dR_"+id).c_str(),"deltaR for electron(s) and closest jet",50,0,7, "#Delta R(e, closest jet)");
        //jl 19.04.11: added deta(lep,jet) and dphi(lep,jet) plots
        histos1d[("QCDMM_min_mu_dEta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_min_mu_dEta_"+id).c_str(),"deltaEta between muon(s) and closest jet",50,0,7,"#Delta Eta(#mu, closest jet)");
        histos1d[("QCDMM_min_e_dEta_"+id).c_str()]=histo_writer->create_1d(("QCDMM_min_e_dEta_"+id).c_str(),"deltaEta for electron(s) and closest jet",50,0,7, "#Delta Eta(e, closest jet)");
        histos1d[("QCDMM_min_mu_dPhi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_min_mu_dPhi_"+id).c_str(),"deltaPhi between muon(s) and closest jet",50,0,7,"#Delta Phi(#mu, closest jet)");
        histos1d[("QCDMM_min_e_dPhi_"+id).c_str()]=histo_writer->create_1d(("QCDMM_min_e_dPhi_"+id).c_str(),"deltaPhi for electron(s) and closest jet",50,0,7, "#Delta Phi(e, closest jet)");
	
	//////////isolation
	histos1d[("QCDMM_unmatch_mu_trackiso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_mu_trackiso_"+id).c_str(),"unmatched Muon TrackIso",50,0,10, "TrackIso [GeV]");
	histos1d[("QCDMM_unmatch_mu_hcaliso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_mu_hcaliso_"+id).c_str(),"unmatched Muon Hcal Iso",50,0,10, "HCAL Iso [GeV]");
	histos1d[("QCDMM_unmatch_mu_ecaliso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_mu_ecaliso_"+id).c_str(),"unmatched Muon Ecal Iso",50,0,10, "ECAL Iso [GeV]");
	histos1d[("QCDMM_unmatch_mu_caloiso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_mu_caloiso_"+id).c_str(),"unmatched Muon CaloIso",50,0,10, "CaloIso [GeV]");
	histos1d[("QCDMM_unmatch_mu_sumiso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_mu_sumiso_"+id).c_str(),"unmatched Muon Ecal+Hcal+trackIso",100,0,20, "SumIso [GeV]");
	histos1d[("QCDMM_unmatch_mu_CombRelIso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_mu_CombRelIso_"+id).c_str(),"unmatched Muon CombRelIso",50,0,0.2, "comb.rel.Iso.");
	histos1d[("QCDMM_unmatch_e_trackiso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_e_trackiso_"+id).c_str(),"unmatched Electron Track Iso",50,0,10, "TrackIso [GeV]");
	histos1d[("QCDMM_unmatch_e_hcaliso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_e_hcaliso_"+id).c_str(),"unmatched Electron Hcal Iso",50,0,10, "HCAL Iso [GeV]");
	histos1d[("QCDMM_unmatch_e_ecaliso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_e_ecaliso_"+id).c_str(),"unmatched Electron Ecal Iso",50,0,10, "ECAL Iso [GeV]");
	histos1d[("QCDMM_unmatch_e_caloiso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_e_caloiso_"+id).c_str(),"unmatched Electron CaloIso",50,0,10, "CaloIso [GeV]");
	histos1d[("QCDMM_unmatch_e_sumiso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_e_sumiso_"+id).c_str(),"unmatched Electron SumIso",100,0,20, "SumIso [GeV]");
	histos1d[("QCDMM_unmatch_e_CombRelIso_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_e_CombRelIso_"+id).c_str(),"unmatched Electron CombRelIso",50,0,0.2, "comb.rel.Iso.");
	//vetocone plots
	histos1d[("QCDMM_unmatch_mu_veto_hcalEnergy_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_mu_veto_hcalEnergy_"+id).c_str(),"unmatched muon vetocone(size 0.1) hcalEnergy",50,0,100, "HCAL VetoCone [GeV]");
	histos1d[("QCDMM_unmatch_mu_veto_ecalEnergy_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_mu_veto_ecalEnergy_"+id).c_str(),"unmatched muon vetocone(size 0.07) ecalEnergy",50,0,100, "ECAL VetoCone [GeV]");
	histos1d[("QCDMM_unmatch_e_veto_ecalEnergy_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_e_veto_ecalEnergy_"+id).c_str(),"unmatched electron vetocone(size 0.07) ecalEnergy",50,0,100, "ECAL VetoCone [GeV]");
	histos1d[("QCDMM_unmatch_e_veto_hcalEnergy_"+id).c_str()]=histo_writer->create_1d(("QCDMM_unmatch_e_veto_hcalEnergy_"+id).c_str(),"unmatched electron vetocone(size(0.1) hcalEnergy",50,0,100, "HCAL VetoCone [GeV]");
	
	/////////top mass
	histos1d[("QCDMM_top_mass_tf_"+id).c_str()]=histo_writer->create_1d(("QCDMM_top_mass_tf_"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",260,0, 1400, "M3 [GeV]");
        histos1d[("QCDMM_top_mass_"+id).c_str()]=histo_writer->create_1d(("QCDMM_top_mass_"+id).c_str(),"Invariant mass of 3 jets with highest vectorially summed Pt",40,50, 700, "M3 [GeV]");
	histos1d[("QCDMM_top_mass_minimisation_"+id).c_str()]=histo_writer->create_1d(("QCDMM_top_mass_minimisation_"+id).c_str(),"Hadronic top mass, minimal Mjjj-Mjln",110,0, 1000, "Min. Diff Mass [GeV}]");
	histos1d[("QCDMM_top_mass_minimisation_lep_"+id).c_str()]=histo_writer->create_1d(("QCDMM_top_mass_minimisation_lep_"+id).c_str(),"Leptonic top mass, minimal Mjjj-Mjln",110,0, 1000);

	////////PV jl 23.12.10
        histos1d[("QCDMM_npv_"+id).c_str()]=histo_writer->create_1d(("QCDMM_npv_"+id).c_str(),"# PV",20,0.,20,"nPV");
        histos1d[("QCDMM_pv_isfake_"+id).c_str()]=histo_writer->create_1d(("QCDMM_pv_isfake_"+id).c_str(),"PV isfake",2,0,2,"is fake");
        histos1d[("QCDMM_pv_z_"+id).c_str()]=histo_writer->create_1d(("QCDMM_pv_z_"+id).c_str(),"Z PV",15,0,30,"Z");
        histos1d[("QCDMM_pv_rho_"+id).c_str()]=histo_writer->create_1d(("QCDMM_pv_rho_"+id).c_str(),"Rho PV",40,0,2,"#Rho");
        histos1d[("QCDMM_pv_ndof_"+id).c_str()]=histo_writer->create_1d(("QCDMM_pv_ndof_"+id).c_str(),"PV ndof",50,0,250,"ndof");
		
	/////////QCD technical
	histos1d[("QCDMM_weights_"+id).c_str()]=histo_writer->create_1d(("QCDMM_weights_"+id).c_str(),"MM weights",1200,-3,3, "");
}


//plot pt, eta, and numbet of jets
void QCDMMPlots::plot_QCDjets()
{
	int njets = jets->size();
	if(njets >= 1)
	{
		//cout << "--------FILLING JETS------   " << weight << endl;
		histos1d[("QCDMM_jet1_pt_"+id).c_str()]    ->Fill((*jets)[0].Pt(),weight);
		histos1d[("QCDMM_jet1_eta_"+id).c_str()]   ->Fill((*jets)[0].Eta(),weight);
		histos1d[("QCDMM_jet1_phi_"+id).c_str()]   ->Fill((*jets)[0].Phi(),weight);
		histos1d[("QCDMM_jet1_emf_"+id).c_str()]   ->Fill((*jets)[0].emf(),weight);
		histos1d[("QCDMM_jet1_chf_"+id).c_str()]   ->Fill((*jets)[0].chf(),weight);
		histos1d[("QCDMM_jet1_fhpd_"+id).c_str()]  ->Fill((*jets)[0].fHPD(),weight);
		histos1d[("QCDMM_jet1_n90_"+id).c_str()]   ->Fill((*jets)[0].n90Hits(),weight);
		histos1d[("QCDMM_jet1_nconst_"+id).c_str()]->Fill((*jets)[0].nconstituents(),weight);
		histos1d[("QCDMM_jet1_nhf_"+id).c_str()]   ->Fill((*jets)[0].nhf(),weight);
		histos1d[("QCDMM_jet1_nemf_"+id).c_str()]  ->Fill((*jets)[0].nemf(),weight);
		histos1d[("QCDMM_jet1_cemf_"+id).c_str()]  ->Fill((*jets)[0].cemf(),weight);
		histos1d[("QCDMM_jet1_cmult_"+id).c_str()] ->Fill((*jets)[0].cmulti(),weight);
	}

	if(njets >= 2)
	{
		histos1d[("QCDMM_jet2_pt_"+id).c_str()]    ->Fill((*jets)[1].Pt(),weight);
		histos1d[("QCDMM_jet2_eta_"+id).c_str()]   ->Fill((*jets)[1].Eta(),weight);
		histos1d[("QCDMM_jet2_phi_"+id).c_str()]   ->Fill((*jets)[1].Phi(),weight);
		histos1d[("QCDMM_jet2_emf_"+id).c_str()]   ->Fill((*jets)[1].emf(),weight);
		histos1d[("QCDMM_jet2_chf_"+id).c_str()]   ->Fill((*jets)[1].chf(),weight);
		histos1d[("QCDMM_jet2_fhpd_"+id).c_str()]  ->Fill((*jets)[1].fHPD(),weight);
		histos1d[("QCDMM_jet2_n90_"+id).c_str()]   ->Fill((*jets)[1].n90Hits(),weight);
		histos1d[("QCDMM_jet2_nconst_"+id).c_str()]->Fill((*jets)[1].nconstituents(),weight);
		histos1d[("QCDMM_jet2_nhf_"+id).c_str()]   ->Fill((*jets)[1].nhf(),weight);
		histos1d[("QCDMM_jet2_nemf_"+id).c_str()]  ->Fill((*jets)[1].nemf(),weight);
		histos1d[("QCDMM_jet2_cemf_"+id).c_str()]  ->Fill((*jets)[1].cemf(),weight);
		histos1d[("QCDMM_jet2_cmult_"+id).c_str()] ->Fill((*jets)[1].cmulti(),weight);	
	}

	if(njets >= 3)
	{
		histos1d[("QCDMM_jet3_pt_"+id).c_str()]    ->Fill((*jets)[2].Pt(),weight);
		histos1d[("QCDMM_jet3_eta_"+id).c_str()]   ->Fill((*jets)[2].Eta(),weight);
		histos1d[("QCDMM_jet3_phi_"+id).c_str()]   ->Fill((*jets)[2].Phi(),weight);
		histos1d[("QCDMM_jet3_emf_"+id).c_str()]   ->Fill((*jets)[2].emf(),weight);
		histos1d[("QCDMM_jet3_chf_"+id).c_str()]   ->Fill((*jets)[2].chf(),weight);
		histos1d[("QCDMM_jet3_fhpd_"+id).c_str()]  ->Fill((*jets)[2].fHPD(),weight);
		histos1d[("QCDMM_jet3_n90_"+id).c_str()]   ->Fill((*jets)[2].n90Hits(),weight);
		histos1d[("QCDMM_jet3_nconst_"+id).c_str()]->Fill((*jets)[2].nconstituents(),weight);
		histos1d[("QCDMM_jet3_nhf_"+id).c_str()]   ->Fill((*jets)[2].nhf(),weight);
		histos1d[("QCDMM_jet3_nemf_"+id).c_str()]  ->Fill((*jets)[2].nemf(),weight);
		histos1d[("QCDMM_jet3_cemf_"+id).c_str()]  ->Fill((*jets)[2].cemf(),weight);
		histos1d[("QCDMM_jet3_cmult_"+id).c_str()] ->Fill((*jets)[2].cmulti(),weight);
	}
	
	if(njets >= 4)
	{
		histos1d[("QCDMM_jet4_pt_"+id).c_str()]    ->Fill((*jets)[3].Pt(),weight);
		histos1d[("QCDMM_jet4_eta_"+id).c_str()]   ->Fill((*jets)[3].Eta(),weight);
		histos1d[("QCDMM_jet4_phi_"+id).c_str()]   ->Fill((*jets)[3].Phi(),weight);
		histos1d[("QCDMM_jet4_emf_"+id).c_str()]   ->Fill((*jets)[3].emf(),weight);
		histos1d[("QCDMM_jet4_chf_"+id).c_str()]   ->Fill((*jets)[3].chf(),weight);
		histos1d[("QCDMM_jet4_fhpd_"+id).c_str()]  ->Fill((*jets)[3].fHPD(),weight);
		histos1d[("QCDMM_jet4_n90_"+id).c_str()]   ->Fill((*jets)[3].n90Hits(),weight);
		histos1d[("QCDMM_jet4_nconst_"+id).c_str()]->Fill((*jets)[3].nconstituents(),weight);
		histos1d[("QCDMM_jet4_nhf_"+id).c_str()]   ->Fill((*jets)[3].nhf(),weight);
		histos1d[("QCDMM_jet4_nemf_"+id).c_str()]  ->Fill((*jets)[3].nemf(),weight);
		histos1d[("QCDMM_jet4_cemf_"+id).c_str()]  ->Fill((*jets)[3].cemf(),weight);
		histos1d[("QCDMM_jet4_cmult_"+id).c_str()] ->Fill((*jets)[3].cmulti(),weight);
	}
	
	for(std::vector<mor::Jet>::const_iterator jet_iter = jets->begin(); jet_iter!=jets->end(); ++jet_iter)
	{
		histos1d[("QCDMM_jet_pt_"+id).c_str()]->Fill(jet_iter->Pt(),weight);
		histos1d[("QCDMM_jet_eta_"+id).c_str()]->Fill(jet_iter->Eta(),weight);
		histos1d[("QCDMM_jet_phi_"+id).c_str()]->Fill(jet_iter->Phi(),weight);
	}	
	
	histos1d[("QCDMM_jet_number_"+id).c_str()]->Fill(njets,weight);
}

 // Plot eta,pt and number of muons
 //here we use the LOOSE muons because we need a non-0 value when the event is loose, and because tight=loose when tight
void QCDMMPlots::plot_QCDmuons()
{
	for(std::vector<mor::Muon>::iterator mu = loose_muons->begin();mu!=loose_muons->end();++mu)
	{
		histos1d[("QCDMM_muon_pt_"+id).c_str()]->Fill(mu->Pt(),weight);
		histos1d[("QCDMM_muon_eta_"+id).c_str()]->Fill(mu->Eta(),weight);
		histos1d[("QCDMM_muon_eta_tf_"+id).c_str()]->Fill(mu->Eta(),weight);
		histos1d[("QCDMM_muon_abseta_"+id).c_str()]->Fill(fabs(mu->Eta()),weight);
		histos1d[("QCDMM_muon_phi_"+id).c_str()]->Fill(mu->Phi(),weight);
	}
	
	histos1d[("QCDMM_muon_number_"+id).c_str()]->Fill(loose_muons->size(),weight);
}


void QCDMMPlots::plot_QCDmuon_quality()
{
	for(std::vector<mor::Muon>::iterator muon = loose_muons->begin();muon != loose_muons->end();++muon)
	{
		double d0 = -1000;
		if(muon->track_available())
		{
			histos1d[("QCDMM_muon_chi2_"+id).c_str()]->Fill(muon->chi2(),weight);
			histos1d[("QCDMM_muon_nHits_"+id).c_str()]->Fill(muon->nHits(),weight);
			d0 = muon->d0();
			histos1d[("QCDMM_muon_d0_"+id).c_str()]->Fill(d0,weight);
		}
	}
}


// Plot eta,pt and number of electrons
 //here we use the LOOSE electrons because we need a non-0 value when the event is loose, and because tight=loose when tight
void QCDMMPlots::plot_QCDelectrons()
{
	for(std::vector<mor::Electron>::iterator e = loose_electrons->begin();e != loose_electrons->end();++e)
	{
		histos1d[("QCDMM_electron_pt_"+id).c_str()]->Fill(e->Pt(),weight);
		histos1d[("QCDMM_electron_eta_"+id).c_str()]->Fill(e->Eta(),weight);
		histos1d[("QCDMM_electron_abseta_"+id).c_str()]->Fill(fabs(e->Eta()),weight);
                histos1d[("QCDMM_electron_eta_tf_"+id).c_str()]->Fill(e->Eta(),weight);
		histos1d[("QCDMM_electron_phi_"+id).c_str()]->Fill(e->Phi(),weight);
	}

	histos1d[("QCDMM_electron_number_"+id).c_str()]->Fill(loose_electrons->size(),weight);

}

void QCDMMPlots::plot_QCDelectron_quality()
{
	for(std::vector<mor::Electron>::iterator electron = loose_electrons->begin();electron != loose_electrons->end();++electron)
	{
		double d0 = -1000;
		if(electron->track_available())
		{
			d0 = electron->d0();
			histos1d[("QCDMM_electron_d0_"+id).c_str()]->Fill(d0,weight);
		}
	}
}

// Plot delta R between lepton and closest jet
 //here we use the LOOSE electrons because we need a non-0 value when the event is loose, and because tight=loose when tight
void QCDMMPlots::plot_QCDdR()
{
	for(std::vector<mor::Muon>::iterator mu = loose_muons->begin();mu != loose_muons->end();mu++)
	{
		double min_dR = -1;
		double min_deta = -1;
		double min_dphi = -1;
		for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();jet_iter!=jets->end();++jet_iter)
		{
			double dR = ROOT::Math::VectorUtil::DeltaR(*mu,*jet_iter);
			if ((dR < min_dR) || (min_dR == -1))
				min_dR = dR;
			double deta = fabs(mu->Eta()-jet_iter->Eta());
			if ((deta < min_deta) || (min_deta == -1))
				min_deta = deta;
			double dphi = fabs(ROOT::Math::VectorUtil::DeltaPhi(mu->p4(),jet_iter->p4()));
			if ((dphi < min_dphi) || (min_dphi == -1))
				min_dphi = dphi;
				
		}
		histos1d[("QCDMM_min_mu_dR_"+id).c_str()]->Fill(min_dR,weight);
                histos1d[("QCDMM_min_mu_dEta_"+id).c_str()]->Fill(min_deta,weight);
                histos1d[("QCDMM_min_mu_dPhi_"+id).c_str()]->Fill(min_dphi,weight);
		//if(min_dR <=0.5) cout << "[QCDMMPlots]: !WARNING! min_mu_dr is below the requested cut! min_mu_dr " << min_dR << " id " << id << endl;
		return;
	}

	for(std::vector<mor::Electron>::iterator e = loose_electrons->begin();e != loose_electrons->end();e++)
	{
		double min_dR = -1;
                double min_deta = -1;
                double min_dphi = -1;
		for(std::vector<mor::Jet>::iterator jet_iter = jets->begin();jet_iter!=jets->end();++jet_iter)
		{
			double dR = ROOT::Math::VectorUtil::DeltaR(e->p4(),jet_iter->p4());
			if ((dR < min_dR) || (min_dR = -1))
				min_dR = dR;
                        double deta = fabs(e->Eta()-jet_iter->Eta());
                        if ((deta < min_deta) || (min_deta == -1))
                                min_deta = deta;
                        double dphi = fabs(ROOT::Math::VectorUtil::DeltaPhi(e->p4(),jet_iter->p4()));
                        if ((dphi < min_dphi) || (min_dphi == -1))
                                min_dphi = dphi;
		}
		histos1d[("QCDMM_min_e_dR_"+id).c_str()]->Fill(min_dR,weight);
                histos1d[("QCDMM_min_e_dEta_"+id).c_str()]->Fill(min_deta,weight);
                histos1d[("QCDMM_min_e_dPhi_"+id).c_str()]->Fill(min_dphi,weight);
	}
	
}

// Plot transverse energy:sum of all jets and the muon pt
void QCDMMPlots::plot_QCDht()
{
	ht = ht_calc->get_ht();

	//histos1d[("QCDMM_Ht_"+id).c_str()]->Fill(ht+corrected_mets->begin()->Pt(),weight);
	histos1d[("QCDMM_Ht_wo_MET_"+id).c_str()]->Fill(ht,weight);
}

// Plot leptonic transverse mass and MET 
void QCDMMPlots::plot_QCDMET_MtW()
{
	if(corrected_mets->size() < 1)
		return;

	// Loop to get total MET, should only be one iteration of loop.
	double e_MtW=0.0,mu_MtW=0.0,dphi=0., trival = -1.;;
	for(std::vector<mor::MET>::iterator met_iter = corrected_mets->begin();met_iter!=corrected_mets->end();++met_iter)
	{
		// Check for at least one lepton before calculating Mt	
		// do this with the LOOSE leptons 
		if(loose_muons->size() > 0)
		{
			mu_MtW = ((*loose_muons)[0].p4()+met_iter->p4()).Mt(); 
			histos1d[("QCDMM_muon_MtW_"+id).c_str()]->Fill(mu_MtW,weight);
                	//jl 03.11.10: added dphi(lep/MET)
			dphi = (*loose_muons)[0].Phi() - met_iter->Phi();
                        if (dphi>TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
	                if (dphi<-TMath::Pi()) dphi = 2*TMath::Pi() + dphi;
	                histos1d[("QCDMM_muon_dphi_lepmet_"+id).c_str()]->Fill(fabs(dphi),weight);
		}
		       
		if(loose_electrons->size() > 0)
		{
			e_MtW = ((*loose_electrons)[0].p4()+met_iter->p4()).Mt();                        
			dphi = (*loose_electrons)[0].Phi() - met_iter->Phi();
                        if (dphi>TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
                        if (dphi<-TMath::Pi()) dphi = 2*TMath::Pi() + dphi;
                        histos1d[("QCDMM_electron_dphi_lepmet_"+id).c_str()]->Fill(fabs(dphi),weight);

			histos1d[("QCDMM_electron_MtW_"+id).c_str()]->Fill(e_MtW,weight);
		//	trival = 100 - 0.5*met_iter->Et();
			//cout << " QCD trival = " << trival << " mW = " << e_MtW << endl;
		}
	}
	histos1d[("QCDMM_Event_MET_"+id).c_str()]->Fill(corrected_mets->begin()->Et(),weight);
	histos1d[("QCDMM_Event_MET_pt_"+id).c_str()]->Fill(corrected_mets->begin()->pt(),weight);	
	histos1d[("QCDMM_Event_MET_phi_"+id).c_str()]->Fill(corrected_mets->begin()->Phi(),weight);
	histos1d[("QCDMM_Event_MET_px_"+id).c_str()]->Fill(corrected_mets->begin()->px(),weight);
	histos1d[("QCDMM_Event_MET_py_"+id).c_str()]->Fill(corrected_mets->begin()->py(),weight);

}


//plot isolation and vetocone(vetocone to replace dR cut) for semilep and non semilep events
void QCDMMPlots::plot_QCDiso()
{
	for(std::vector<mor::Muon>::iterator muon = loose_muons->begin();muon != loose_muons->end();++muon)
	{
		histos1d[("QCDMM_unmatch_mu_hcaliso_"+id).c_str()]->Fill(muon->hcalIso(),weight);
		histos1d[("QCDMM_unmatch_mu_ecaliso_"+id).c_str()]->Fill(muon->ecalIso(),weight);
		histos1d[("QCDMM_unmatch_mu_trackiso_"+id).c_str()]->Fill(muon->trackIso(),weight);
		histos1d[("QCDMM_unmatch_mu_caloiso_"+id).c_str()]->Fill(muon->caloIso(),weight);
		double unmatch_mu_sumIso = (muon->trackIso()+muon->hcalIso()+muon->ecalIso());
		double unmatch_mu_CombRelIso = (unmatch_mu_sumIso)/(muon->pt());
		histos1d[("QCDMM_unmatch_mu_CombRelIso_"+id).c_str()]->Fill(unmatch_mu_CombRelIso,weight);
		histos1d[("QCDMM_unmatch_mu_sumiso_"+id).c_str()]->Fill(unmatch_mu_sumIso,weight);
		histos1d[("QCDMM_unmatch_mu_veto_hcalEnergy_"+id).c_str()]->Fill(muon->hcal_vcone(),weight);
		histos1d[("QCDMM_unmatch_mu_veto_ecalEnergy_"+id).c_str()]->Fill(muon->ecal_vcone(),weight); 
	}
	
	for(std::vector<mor::Electron>::iterator electron = loose_electrons->begin();electron != loose_electrons->end();++electron)
	{
		histos1d[("QCDMM_unmatch_e_hcaliso_"+id).c_str()]->Fill(electron->hcalIso(),weight);
		histos1d[("QCDMM_unmatch_e_ecaliso_"+id).c_str()]->Fill(electron->ecalIso(),weight);
		histos1d[("QCDMM_unmatch_e_trackiso_"+id).c_str()]->Fill(electron->trackIso(),weight);
		histos1d[("QCDMM_unmatch_e_caloiso_"+id).c_str()]->Fill(electron->caloIso(),weight);
		double unmatch_e_sumIso = (electron->trackIso()+electron->hcalIso()+electron->ecalIso());
		double unmatch_e_CombRelIso = (unmatch_e_sumIso)/(electron->pt());
		histos1d[("QCDMM_unmatch_e_CombRelIso_"+id).c_str()]->Fill(unmatch_e_CombRelIso,weight);
		histos1d[("QCDMM_unmatch_e_sumiso_"+id).c_str()]->Fill(unmatch_e_sumIso,weight);
		histos1d[("QCDMM_unmatch_e_veto_hcalEnergy_"+id).c_str()]->Fill(electron->hcal_vcone(),weight);
		histos1d[("QCDMM_unmatch_e_veto_ecalEnergy_"+id).c_str()]->Fill(electron->ecal_vcone(),weight);
	}
}

//jl 17.12.10: add mass plots
void QCDMMPlots::plot_top()
{
	if(ht == -1)
		ht = ht_calc->get_ht();
	histos1d[("QCDMM_top_mass_"+id).c_str()]->Fill(mass_reco->calculate_M3(),weight);
	histos1d[("QCDMM_top_mass_tf_"+id).c_str()]->Fill(mass_reco->calculate_M3(),weight);
	histos1d[("QCDMM_top_mass_minimisation_"+id).c_str()]->Fill(mass_reco->calculate_min_diff_M3(),weight);
	histos1d[("QCDMM_top_mass_minimisation_lep_"+id).c_str()]->Fill(mass_reco->calculate_min_diff_lepTmass(),weight);
}

//jl 23.12.10: plot PV
void QCDMMPlots::plot_PV()
{
        int npv = 0;
        for(std::vector<mor::PrimaryVertex>::iterator pvit = pv->begin(); pvit != pv->end(); ++pvit)
        {
                histos1d[("QCDMM_pv_isfake_"+id).c_str()]->Fill(pvit->is_fake(),weight);
                histos1d[("QCDMM_pv_z_"+id).c_str()]     ->Fill(pvit->z(),weight);
                histos1d[("QCDMM_pv_rho_"+id).c_str()]   ->Fill(pvit->rho(),weight);
                histos1d[("QCDMM_pv_ndof_"+id).c_str()]  ->Fill(pvit->ndof(),weight);
		npv++;
		break;
        }
        histos1d[("QCDMM_npv_"+id).c_str()]->Fill(npv,weight);
}


//Compute MM weights based on lepton pT, eps_qcd and eps_signal
//so far, valid for MUON case only
double QCDMMPlots::ComputeMMWeights()
{
	double leppt  = -1.;
	double lepeta = -1.;
	double dphi   = -1.;
	double tpt    = -1.;
	bool isTight  = false;
	weight = w1 = w2 = -1.;
	
	if(isMuon)
	{
		//get pT...
		for(std::vector<mor::Muon>::iterator mu_iter = loose_muons->begin();mu_iter != loose_muons->end();mu_iter++)
		{
			leppt  = mu_iter->Pt();		
			lepeta = fabs(mu_iter->Eta());
        		for(std::vector<mor::MET>::iterator met_iter = corrected_mets->begin();met_iter!=corrected_mets->end();++met_iter)
		        {
                		dphi = mu_iter->Phi() - met_iter->Phi();
		                if (dphi>TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
                		if (dphi<-TMath::Pi()) dphi = 2*TMath::Pi() + dphi;
		                dphi = fabs(dphi);
		        }
		}
		for(std::vector<mor::Muon>::iterator mu_iter = isolated_muons->begin();mu_iter != isolated_muons->end();mu_iter++)
			tpt = mu_iter->Pt();
	
		//how many muons do we have?
		if(isolated_muons->size() > 0) isTight = true;
		///////////////if(isData && loose_muons->size() > 1) cout << "[QCDMMPlots]: !WARNING! Your loose selection contains more than one muon! Don't expect much from MM QCD plots..." << endl;
		////////////////if(isData && isolated_muons->size() > 1) cout << "[QCDMMPlots]: !WARNING! Your tight selection contains more than one muon! Don't expect much from MM QCD plots..." << endl;
	
		//add here a check that the two muons are really the same....
//////////		if (isData && leppt != tpt && isTight)
//////////		{ 
//////////////			cout << "[QCDMMPlots]: !WARNING! The loose and tight muons are not the same!" << endl;
//////////////			cout << "[QCDMMPlots]:           loose pt is " << leppt << " and tight pt is "<< tpt << endl;
//////////		}
		//compute epsSignal and epsQCD
		/////// = previously used
		//v4
		//////epsSignal              = 0.688754 + 0.0993925*log(leppt);
		//v8
		//if(mupt<18.) epsSignal = -0.0185048 + 0.327524*log(mupt);
		//else epsSignal         = 0.931619;
		//v5
		//epsQCD                 = 0.395458 - 0.0655657*log(leppt);
		//v14
		//epsQCD                 = 0.496482 - 0.0865273*log(leppt);
		//v15
                /////epsQCD                 = 0.496966 - 0.0860680*log(leppt);
		//v18
		//epsSignal = 1.73699e-01*leppt - 1.12325e-02*leppt*leppt + 2.57815e-04*leppt*leppt*leppt;
		//if(leppt >18.) epsSignal = 9.94665e-01;
		//epsQCD    = 4.64531e-01 + 2.79799e-02*log(leppt);
		//v22
		//epsSignal   = 1.70475e-01*leppt - 1.13136e-02*leppt*leppt + 2.75335e-04*leppt*leppt*leppt;
		//if(leppt > 17.) epsSignal = 9.78694e-01;
		//epsQCD      = 4.01696e-01 + 4.23933e-02*log(leppt);
		//v24
		//epsSignal     = 1.92910e-01*leppt - 1.32537e-02*leppt*leppt + 3.14485e-04*leppt*leppt*leppt;
		//if(leppt > 17) epsSignal = 1.;
		//epsQCD        = 4.76088e-01 + 4.13254e-02*log(leppt);
		//v26
		//epsSignal = 1.78530e-01*leppt - 1.16224e-02*leppt*leppt + 2.61796e-04*leppt*leppt*leppt;
		//if(leppt>19) epsSignal = 9.94176e-01;
		//epsQCD    = 4.60365e-01 + 3.98896e-02*log(leppt);
		//v26+1sigma
		//epsQCD    = (4.76088e-01 + 2.10775e-02) + (4.13254e-02 + 1.08965e-02)*log(leppt);
		//epsSignal = (1.78530e-01 + 8.50739e-03)*leppt - (1.16224e-02 + 1.12489e-03)*leppt*leppt + (2.61796e-04 + 3.64371e-05)*leppt*leppt*leppt;
		//if(leppt>19) epsSignal = 9.94176e-01 + 1.40931e-03;
		//v26-1sigma
                //epsQCD    = (4.76088e-01 - 2.10775e-02) + (4.13254e-02 - 1.08965e-02)*log(leppt);
                //epsSignal = (1.78530e-01 - 8.50739e-03)*leppt - (1.16224e-02 - 1.12489e-03)*leppt*leppt + (2.61796e-04 - 3.64371e-05)*leppt*leppt*leppt;
                //if(leppt>19) epsSignal = 9.94176e-01 - 1.40931e-03;
		//v56
		//epsQCD    = 4.60365e-01 + 3.98896e-02*log(leppt);
		//epsSignal = 4.44434e-01 + 9.23494e-02*leppt - 5.41352e-03*leppt*leppt + 1.08818e-04*leppt*leppt*leppt;
		//if(leppt>20) epsSignal = 9.99653e-01;
		//v56 + 1sigma
                //epsQCD    = (4.60365e-01+2.10775e-02) + (3.98896e-02+1.08965e-02)*log(leppt);
                //epsSignal = (4.44434e-01+5.46698e-03) + (9.23494e-02+5.59346e-04)*leppt - (5.41352e-03+3.16744e-05)*leppt*leppt + (1.08818e-04+1.30053e-06)*leppt*leppt*leppt;
		//if(leppt>20) epsSignal = 9.99653e-01+6.30305e-05;
		//v56 - 1sigma
                //epsQCD    = (4.60365e-01-2.10775e-02) + (3.98896e-02-1.08965e-02)*log(leppt);
                //epsSignal = (4.44434e-01-5.46698e-03) + (9.23494e-02-5.59346e-04)*leppt - (5.41352e-03-3.16744e-05)*leppt*leppt + (1.08818e-04-1.30053e-06)*leppt*leppt*leppt;
                //if(leppt>20) epsSignal = 9.99653e-01-6.30305e-05;
		//v78
                //epsQCD = 2.16404e-01*log(leppt);
                //epsSignal = 4.44434e-01 + 9.23494e-02*leppt - 5.41352e-03*leppt*leppt + 1.08818e-04*leppt*leppt*leppt;
                //if(leppt>20) epsSignal = 9.99653e-01;
		//v80
		//epsQCD    = 2.16259e-01*log(leppt);
                /////epsQCD = epsQCD / 1.05;
		//epsSignal = 6.69194e-01 + 4.35432e-02*leppt - 1.98654e-03*leppt*leppt +  3.07422e-05*leppt*leppt*leppt;
		//if(leppt>25) epsSignal = 9.95399e-01;// + 1.04772e-04*leppt; //jl dangerous since eff can go to 1 and cause div/0
		//v88
		//epsQCD    = 7.56455e-02 + 1.85880e-01*log(leppt);
		//epsSignal = 7.14076e-01 + 3.61713e-02*leppt - 1.59540e-03*leppt*leppt + 2.40150e-05*leppt*leppt*leppt;
		//if(leppt>25) epsSignal = 9.99527e-01;
		//v93 NO contamination removal
		//epsQCD      = 5.94020e-02 + 1.92628e-01*log(leppt);
                //epsSignal   = 7.14076e-01 + 3.61713e-02*leppt - 1.59540e-03*leppt*leppt + 2.40150e-05*leppt*leppt*leppt;
                //if(leppt>25) epsSignal = 9.99527e-01;
		//v93 WITH contamination removal
		//epsQCD      = 1.58135e-01 + 1.52308e-01*log(leppt);
		//epsSignal   = 7.14076e-01 + 3.61713e-02*leppt - 1.59540e-03*leppt*leppt + 2.40150e-05*leppt*leppt*leppt;
                //if(leppt>25) epsSignal = 9.99527e-01;
		//v104
		//epsQCD    = -3.45818e-01 + 3.38998e-01*log(leppt);
		//if(leppt > 32.) epsQCD = -3.45818e-01 + 3.38998e-01*log(32.); //capping
		//epsSignal = 5.76126e-01 + 1.58618e-01*log(leppt) - 8.15155e-06*leppt*leppt*leppt;
		//if(leppt > 20.) epsSignal = 5.76126e-01 + 1.58618e-01*log(20.) - 8.15155e-06*20*20*20; //capping
		//v111
		//epsQCD    = -3.63615e-01 + 3.42561e-01*log(leppt);
		//if(leppt > 32.) epsQCD = -3.63615e-01 + 3.42561e-01*log(32.);
		//epsSignal = 9.43790e-01 + 1.61861e-02*log(leppt) - 5.73521e-08*leppt*leppt*leppt;
		//v111_conta
		//epsQCD    = 2.11160e-01*log(leppt);
		//if(leppt > 35.) epsQCD = 2.11160e-01*log(35.);
             	//epsSignal = 9.43790e-01 + 1.61861e-02*log(leppt) - 5.73521e-08*leppt*leppt*leppt;
		//v115_conta
		//epsQCD      = 8.55127e-02*log(leppt);
		//if(leppt > 32.) epsQCD = 8.55127e-02*log(32.);
		//epsSignal   = 2.70453e-01 + 1.88439e-01*log(leppt) - 2.58888e-07*leppt*leppt*leppt;
		//v126
		//if(lepeta < 0.7)
		//{
		//	epsQCD    = 7.59222e-02*log(leppt); //30
		//	if(leppt>30.) epsQCD = 7.59222e-02*log(30.);
		//	epsSignal = 1.66041e-01 + 2.16446e-01*log(leppt) - 3.20603e-07*leppt*leppt*leppt; //60
		//}
		//else
		//{
		//	epsQCD    = 8.97141e-02*log(leppt); //35
		//	if(leppt>35.) epsQCD =  8.97141e-02*log(35.);
		//	epsSignal = 1.94507e-01 + 2.12208e-01*log(leppt) - 3.52953e-07*leppt*leppt*leppt; //60
		//}
		//v129
		/*if(dphi <= 1.)
		{
			epsQCD = 1.17091e-02 + 5.54557e-02*log(leppt) + 7.82106e-06*leppt*leppt*leppt;
			if(leppt>35.) epsQCD = 1.17091e-02 + 5.54557e-02*log(35.) + 7.82106e-06*35*35*35;
			epsSignal = 3.65215e-01 + 1.67769e-01*log(leppt) - 3.51021e-07*leppt*leppt*leppt;
		}
		else if(dphi > 1. && dphi < 2.5)
                {
                        epsQCD = 4.95838e-01 - 1.06992e-01*log(leppt) + 1.22227e-05*leppt*leppt*leppt;
                        if(leppt>35.) epsQCD = 4.95838e-01 - 1.06992e-01*log(35.) + 1.22227e-05*35*35*35;
                        epsSignal = 2.08691e-01 + 2.12952e-01*log(leppt) - 7.20988e-07*leppt*leppt*leppt;
			if(leppt>50.) epsSignal = 2.08691e-01 + 2.12952e-01*log(50.) - 7.20988e-07*50*50*50;
                }
		else if(dphi>= 2.5 && dphi <= TMath::Pi())
                {
                        epsQCD = 4.23849e-01 - 8.40591e-02*log(leppt) + 1.15234e-05*leppt*leppt*leppt;
                        if(leppt>35.) epsQCD = 4.23849e-01 - 8.40591e-02*log(35.) + 1.15234e-05*35*35*35;
                        epsSignal = -2.10024e-01 + 3.38710e-01*log(leppt) - 1.15466e-06*leppt*leppt*leppt;
			if(leppt>50.) epsSignal = -2.10024e-01 + 3.38710e-01*log(50.) - 1.15466e-06*50*50*50;
                }
		else
		{
			cout << "[QCDMMPlots]: !WARNING! dphi(lep,MET) is out of bounds" << endl;
			cout << "[QCDMMPlots]: !WARNING! leppt " << leppt << " lepeta " << lepeta << " dphi(lep,MET) " << dphi << endl;
		}*/
		//v136
		//epsQCD    = 2.24529e-01*log(leppt);
		//if(leppt > 35.) epsQCD = 2.24529e-01*log(35.);
		//epsSignal = 5.80480e-01 + 1.01578e-01*log(leppt);
		//v138
		//epsQCD    = 7.60141e-01;
		//epsSignal = 7.20721e-01 + 6.67102e-02*log(leppt);
		//v148
		//epsQCD    = 2.27239e-01*log(leppt);
		//if(leppt>35.) epsQCD = 2.27239e-01*log(35.);
		//epsSignal = 5.62606e-01 + 1.07503e-01*log(leppt) - 5.32139e-08*leppt*leppt*leppt;
		//v150
		//epsQCD    = 2.29683e-01*log(leppt);
		//if(leppt > 40.) epsQCD = 2.29683e-01*log(40.);
		//epsSignal = 5.62606e-01 + 1.07503e-01*log(leppt) - 5.32139e-08*leppt*leppt*leppt;
		//v151
		//epsQCD = 2.26859e-01*log(leppt);
		//if(leppt > 40.) epsQCD = 2.26859e-01*log(40.);
		//v154
		//epsQCD    = 2.46861e-01*log(leppt); 
		//if(leppt > 35.) epsQCD = 2.46861e-01*log(leppt);
		//epsSignal = 6.61381e-01 + 7.08637e-02*log(leppt) + 1.23583e-07*leppt*leppt*leppt;
		/////v151//////epsSignal = 5.62606e-01 + 1.07503e-01*log(leppt) - 5.32139e-08*leppt*leppt*leppt;
		//v158
		//epsQCD    = 1.99356e-01*log(leppt);
		//epsSignal = 5.22512e-01 + 1.06603e-01*log(leppt) + 7.07616e-08*leppt*leppt*leppt;
		//v161
		//epsQCD     = -2.73905e-01 + 3.21620e-01*log(leppt);
		//if(leppt > 38.) epsQCD = -2.73905e-01 + 3.21620e-01*log(38.);
		//epsSignal  = 5.58146e-01 + 1.09064e-01*log(leppt) - 6.255889e-08*leppt*leppt*leppt; 
		//v166
		//epsQCD    = -4.04910e-0 + 3.64036e-01*log(leppt);
		//if(leppt> 35.) epsQCD = -4.04910e-0 + 3.64036e-01*log(35.);
		//epsSignal = 5.35872e-01 + 1.16000e-01*log(leppt) - 1.01684e-07*leppt*leppt*leppt;
		//v170
		//epsQCD    = -4.2843e-01 + 3.72598e-01*log(leppt);
		//if(leppt> 35.) epsQCD = -4.2843e-01 + 3.72598e-01*log(35.);
		//epsSignal = 5.61222e-01 + 1.07973e-01*log(leppt) - 5.70884e-08*leppt*leppt*leppt;
		//v171
		//epsQCD    = 1.01155e-01 + 5.04395e-04*leppt*leppt;
		//if(leppt> 35.) epsQCD = 1.01155e-01 + 5.04395e-04*35*35;
		//epsSignal = 2.70446e-01 + 1.88488e-01*log(leppt) - 2.60790e-07*leppt*leppt*leppt;
		//v176-177-178
		//if(dphi < 1.)
		//{
		//epsQCD    =  -4.84907e-01 + 3.84459e-01*log(leppt);
		//if(leppt> 35.) epsQCD =  -4.84907e-01 + 3.84459e-01*log(35.);
		//epsSignal =  6.22049e-01 +  9.43204e-02*log(leppt); 
		//}
		//else if(1. < dphi && dphi < 2.8)
		//{
		//	epsQCD    = -3.87260e-01 + 3.63629e-01*log(leppt);
		//	if(leppt> 35.) epsQCD = -3.87260e-01 + 3.63629e-01*log(35.);
		//	epsSignal =  5.91881e-01 + 9.98271e-02*log(leppt);
		//}
		//else 	
		//{
		//	epsQCD    = -2.52435e-01 + 3.16207e-01*log(leppt);
		//	if(leppt> 35.) epsQCD = -2.52435e-01 + 3.16207e-01*log(35.);
		//	epsSignal = 5.47141e-01 + 1.10022e-01*log(leppt);	
		//}
		//v181
		//epsQCD    = 4.41150e-02 + 7.63232e-01*log(leppt);
		//if(leppt > 35.) epsQCD = 4.41150e-02 + 7.63232e-01*log(35.);
		//v181b
		//epsQCD    = 2.49607e-01*log(leppt);
		//if(leppt > 35.) epsQCD = 2.49607e-01*log(35.);
		//epsSignal = 6.19572e-01 + 8.44883e-02*log(leppt);
		//v186
		//epsQCD    = 2.52374e-01*log(leppt);
		//if(leppt > 35.) epsQCD = 2.52374e-01*log(35.);
		//epsSignal = 6.55326e-01 + 7.26943e-02*log(leppt) + 1.13908e-07*leppt*leppt*leppt;
		//v195
		//epsQCD    = 0.2;
		//epsSignal = 2.40043e-01*log(leppt);
		//v216
		//epsQCD    = 3.81527e-01;
		//epsSignal = 2.40043e-01*log(leppt);
		//v216b
		//epsQCD    = 3.81527e-01;
		//epsSignal = 5.96655e-01 + 8.29788e-02*log(leppt);
		//v285 //////////FINAL FOR 2010
		/////////////////////epsQCD    = 4.46865e-01;
                //epsQCD    = 5.15437e-01; //new, 285debug // 285b, correct subtraction
		//epsSignal = 5.52913e-01 + 9.88649e-02*log(leppt) - 2.59743e-08*leppt*leppt*leppt;
		//v285+1sigma
		//epsQCD    = 5.15437e-01 + 3.99990e-02;
		//epsSignal = (5.52913e-01 + 1.51252e-02) + (9.88649e-02 + 4.08286e-03)*log(leppt) - (2.59743e-08 + 4.23651e-09)*leppt*leppt*leppt;
		//v285-1sigma
		//epsQCD    = 5.15437e-01 - 3.99990e-02;
		//epsSignal = (5.52913e-01 - 1.51252e-02) + (9.88649e-02 - 4.08286e-03)*log(leppt) - (2.59743e-08 - 4.23651e-09)*leppt*leppt*leppt;
		//v441
		//epsQCD    = -5.84569e-01 + 3.62264e-01*log(leppt);
		//if(leppt > 70.) epsQCD    = -5.84569e-01 + 3.62264e-01*log(70.);
		//v441b
		//epsQCD    = 3.92423e-01 + 6.38960e-03*leppt;
		//if(leppt>85.) epsQCD    = 3.92423e-01 + 6.38960e-03*85.;
		//v441c
		//epsSignal = 4.37136e-02 + 2.20547e-01*log(leppt) - 8.52823e-08*leppt*leppt*leppt;
		//if(leppt>100.) epsSignal = 4.37136e-02 + 2.20547e-01*log(100.) - 8.52823e-08*100.*100.*100.;
		//epsSignal = 9.59392e-02 + 2.05407e-01*log(leppt) - 5.53186e-08*leppt*leppt*leppt;
		//if(leppt > 130.) epsSignal = 9.59392e-02 + 2.05407e-01*log(130.) - 5.53186e-08*130*130*130;
		//v441, good
		//epsQCD = 3.92423e-01 + 6.38960e-03*leppt;
                //if(leppt>85.) epsQCD    = 3.92423e-01 + 6.38960e-03*85.;
                //epsSignal = 4.37136e-02 + 2.20547e-01*log(leppt) - 8.52823e-08*leppt*leppt*leppt;
                //if(leppt>100.) epsSignal = 4.37136e-02 + 2.20547e-01*log(100.) - 8.52823e-08*100.*100.*100.;
		//v441, new eff = v441d
                //epsQCD = 3.92423e-01 + 6.38960e-03*leppt;
                //if(leppt>85.) epsQCD    = 3.92423e-01 + 6.38960e-03*85.;
                //epsSignal = 1.34666e-01 + 1.94655e-01*log(leppt) - 4.17732e-08*leppt*leppt*leppt;
                //if(leppt>140.) epsSignal = 1.34666e-01 + 1.94655e-01*log(140) - 4.17732e-08*140.*140.*140.;
		//v441f
		//epsQCD = 3.92423e-01 + 6.38960e-03*leppt;
                //if(leppt>85.) epsQCD    = 3.92423e-01 + 6.38960e-03*85.;
		//epsSignal = 2.11304e-01 + 1.71760e-01*leppt; //let it reach it one
		//v441h ////////FINAL FOR 2011
                //epsQCD = 3.92423e-01 + 6.38960e-03*leppt;
                //if(leppt>60.) epsQCD    = 3.92423e-01 + 6.38960e-03*60.;
		//epsSignal = 2.11304e-01 + 1.71760e-01*leppt; //let it reach it one
		//v441h +1sigma
		//epsQCD = (3.92423e-01 + 3.84090e-02) + (6.38960e-03 + 8.78193e-04)*leppt;
                //if(leppt>60.) epsQCD    = (3.92423e-01 + 3.84090e-02) + (6.38960e-03 + 8.78193e-04)*60.;
                //epsSignal = (2.11304e-01 + 1.04586e-02) + (1.71760e-01 + 2.62968e-03)*leppt;
                //v441h -1sigma
                //epsQCD = (3.92423e-01 - 3.84090e-02) + (6.38960e-03 - 8.78193e-04)*leppt;
                //if(leppt>60.) epsQCD    = (3.92423e-01 - 3.84090e-02) + (6.38960e-03 - 8.78193e-04)*60.;
                //epsSignal = (2.11304e-01 - 1.04586e-02) + (1.71760e-01 - 2.62968e-03)*leppt;
		//v484
		//epsQCD    = -3.40021e-01 + 1.65741e-01*log(leppt);
		//epsSignal = -7.60795e-01 + 3.78156e-01*log(leppt) -5.91761e-08*leppt*leppt*leppt;
		//if(leppt > 130.) epsSignal = -7.60795e-01 + 3.78156e-01*log(130.) -5.91761e-08*130.*130.*130.;
		//v527 3JET
		//epsQCD    = -9.24643e-01 + 4.48857e-01*log(leppt);
		//epsSignal = -2.00039e-02*sqrt(leppt) + 2.60071e-01*log(leppt);
		//v527+1 sigma
		//epsQCD    = (-9.24643e-01+1.11223e-01) + (4.48857e-01+2.97143e-02)*log(leppt); 
                //epsSignal = (-2.00039e-02+3.68364e-03)*sqrt(leppt) + (2.60071e-01+7.08805e-03)*log(leppt);
                //v527+1 sigma
                //epsQCD    = (-9.24643e-01-1.11223e-01) + (4.48857e-01-2.97143e-02)*log(leppt);
                //epsSignal = (-2.00039e-02-3.68364e-03)*sqrt(leppt) + (2.60071e-01-7.08805e-03)*log(leppt);
		//553/555
		//epsQCD    = 2.09716e-02*leppt; 
		//if(leppt > 40.) epsQCD =  2.09716e-02*40.;
		//555a
		////////epsQCD = 2.19402e-02*leppt;
		////////if(leppt > 42.) epsQCD =  2.19402e-02*42.;
		//epsSignal = -7.33095e-02*sqrt(leppt)+3.73122e-01*log(leppt);
		//if(leppt > 100.) epsSignal =  -7.33095e-02*sqrt(100.)+3.73122e-01*log(100.);		
		//554/556
		//epsQCD    = 8.56206e-02 + 1.71737e-02*leppt;
		//if(leppt > 51.) epsQCD = 8.56206e-02 + 1.71737e-02*51.;
		//epsSignal =  -7.41801e-02*sqrt(leppt) + 3.77426e-01*log(leppt);
		//if(leppt > 100.)  epsSignal =  -7.41801e-02*sqrt(100.) + 3.77426e-01*log(100.);
		//563
		//epsQCD    = 2.19109e-02*leppt; 
		//if(leppt > 40.) epsQCD    = 2.19109e-02*40.; 
		//epsSignal =  -7.03231e-02*sqrt(leppt) + 3.68640e-01*log(leppt);
		//if(leppt > 100.)  epsSignal =  -7.03231e-02*sqrt(100.) + 3.68640e-01*log(100.);
		//567
		//epsQCD     = 2.12152e-02*leppt;
		//if(leppt > 42.)  epsQCD     = 2.12152e-02*42.;
                //epsSignal =  -7.03231e-02*sqrt(leppt) + 3.68640e-01*log(leppt);
                //if(leppt > 100.)  epsSignal =  -7.03231e-02*sqrt(100.) + 3.68640e-01*log(100.);		
		//574
		//epsQCD = 2.15490e-02*leppt;
		//if(leppt > 43.) epsQCD = 2.15490e-02*43.;
		//epsSignal = 3.63359e-01*log(leppt) - 6.73616e-02*sqrt(leppt);
		//if(leppt > 100.) epsSignal = 3.63359e-01*log(100.) - 6.73616e-02*sqrt(100.); 
		//577
		//epsQCD =  2.09460e-02*leppt;
		//if(leppt> 41.) epsQCD =  2.09460e-02*41.;
                //epsSignal = 3.63359e-01*log(leppt) - 6.73616e-02*sqrt(leppt);
                //if(leppt > 100.) epsSignal = 3.63359e-01*log(100.) - 6.73616e-02*sqrt(100.);
		//582
		epsQCD = 2.09028e-02*leppt;
		if(leppt> 41.)  epsQCD = 2.09028e-02*41.;
		epsSignal = 3.63401e-01*log(leppt) - 6.73816e-02*sqrt(leppt);
		if(leppt > 100.)  epsSignal = 3.63401e-01*log(100.) - 6.73816e-02*sqrt(100.);
		//585
		//epsQCD = 2.04923e-02*leppt;
		//if(leppt > 41.)  epsQCD = 2.04923e-02*41.;
		//epsSignal = 3.43099e-01*log(leppt) - 5.96678e-02*sqrt(leppt);
		//if(leppt > 120.) epsSignal = 3.43099e-01*log(120.) - 5.96678e-02*sqrt(120.);
		
		//literal values, jl 06.10.10: this sets the values of epsQCD and epsSignal to the real ratios (no fit)
		if(literal) 
		{
			GetLiteralValues(leppt, dphi);
		}
		//sanity measure	
		if(epsSignal >1.) epsSignal = 1.;
		//jl 07.09.10
		if(epsQCD >1.)    epsQCD    = 1.;
		if(epsQCD >= epsSignal) cout << "[QCDMMPlots]: !WARNING! epsQCD is greater than epsSignal! epsQCD = " << epsQCD << " epsSignal = " << epsSignal << " leppt = " << leppt << endl;
		//for now, compute weights ONLY for tight distributions, ie the "tight" weights
		w1 = epsQCD * (epsSignal - 1.) / (epsSignal - epsQCD); //tight weight for tight event
		w2 = epsQCD * (epsSignal     ) / (epsSignal - epsQCD); //tight weight for loose event

/*        if(loose_muons->size() > 0)
{
        cout << " leppt " << leppt << "  epsQCD " << epsQCD << " epsSignal " << epsSignal << endl;
	cout << " w1 " << w1 << " w2 " << w2 << endl;
	cout << " istight " << isTight << endl;
	
}*/


		if(epsSignal == epsQCD)
		{
			cout << "[QCDMMPlots]: !WARNING! epsSignal = epsQCD! Division by 0...." <<endl;
			cout << "[QCDMMPlots]: !WARNING! epsSignal " << epsSignal << " epsQCD " << epsQCD << endl;
		}
		if( (w1 > 100 || w2 > 100) && isData)
		{
			cout << "[QCDMMPlots]: !WARNING! w is abnormally high!" << endl;
			cout << "[QCDMMPlots]: !WARNING! w1 " << w1 << " w2 " << w2 << endl;
			cout << "[QCDMMPlots]: !WARNING! leppt " << leppt << endl;
			cout << "[QCDMMPlots]: !WARNING! w1 and w2 are set to 0 for this event" << endl;
			w1 = 0.; w2 = 0.;
		}
	}
	else
	{
		//get pT...
		for(std::vector<mor::Electron>::iterator e_iter = loose_electrons->begin();e_iter != loose_electrons->end();e_iter++)
		{
			leppt  = e_iter->Pt();
			lepeta = fabs(e_iter->Eta());
		}
		for(std::vector<mor::Electron>::iterator e_iter = isolated_electrons->begin();e_iter != isolated_electrons->end();e_iter++)
			tpt = e_iter->Pt();
	
		//how many electrons do we have?
		if(isolated_electrons->size() > 0) isTight = true;
		//if(isData && loose_electrons->size() > 1) cout << "[QCDMMPlots]: !WARNING! Your loose selection contains more than one electron! Don't expect much from MM QCD plots..." << endl;
		//if(isData && isolated_electrons->size() > 1) cout << "[QCDMMPlots]: !WARNING! Your tight selection contains more than one electron! Don't expect much from MM QCD plots..." << endl;
	
		//add here a check that the two electrons are really the same....
		//if (isData && leppt != tpt && isTight)
		//{ 
		//	cout << "[QCDMMPlots]: !WARNING! The loose and tight electrons are not the same!" << endl;
		//	cout << "[QCDMMPlots]: !WARNING! loose pt is " << leppt << " and tight pt is "<< tpt << endl;
		//}
		//compute epsSignal and epsQCD
		//v40
		//epsQCD    = -5.61482e-01 + 3.15302e-01*log(leppt) - 1.18736e-06*leppt*leppt*leppt;
		//if(leppt < 9.) epsQCD    = 1.33028e-01;
		//epsSignal = -1.49183e-02*leppt + 2.20010e-01*sqrt(leppt);
		//v46
		//ok for pT > 10 GeV
		//epsSignal =  2.35724e-01 + 1.51430e-01*log(leppt); 
		//epsQCD    = -5.51823e-01 + 3.01861e-01*log(leppt);
		//v50
		//epsQCD    = -9.43057e-01 + 5.15836e-01*log(leppt);
		//epsSignal =  2.35256e-01 + 2.05619e-01*log(leppt);
		//v51
		////epsQCD = 4.17177e-02 + 3.47910e-02*leppt;
		////epsSignal = 0.5;
		//v54
		//epsQCD    = 4.17177e-02 + 3.47910e-02*leppt;
		//epsSignal = 9.95054e-01;
		//v68
		//epsQCD    = 1.42851e-01 + 1.53684e-03*leppt*leppt - 1.74379e-05*leppt*leppt*leppt;
		//if(leppt > 25) epsQCD = 0.8;
		//epsSignal = -3.52481e-01 + 1.83336e-01*leppt - 8.62371e-03*leppt*leppt + 1.36382e-04*leppt*leppt*leppt;
		//if(leppt > 25) epsSignal = 9.68677e-01 + 5.84539e-04*leppt;
		//v119
		//epsQCD    = 1.48575e-01*log(leppt);
		//if(leppt > 60.) epsQCD = 1.48575e-01*log(60.);
		//epsSignal = 7.33358e-01 + 7.35123e-02*log(leppt) - 1.78932e-07*leppt*leppt*leppt;
		//if(leppt > 60.) epsSignal = 7.33358e-01 + 7.35123e-02*log(60) - 1.78932e-07*60*60*60;
		//v121
		//epsQCD    = 1.47098e-02*leppt - 1.06677e-04*leppt*leppt;	
		//if(leppt>80.) epsQCD = 1.47098e-02*80 - 1.06677e-04*80*80;
		//epsSignal = 3.52928e-01 + 1.69659e-01*log(leppt) - 2.81018e-07*leppt*leppt*leppt;
		//if(leppt>60.) epsSignal = 3.52928e-01 + 1.69659e-01*log(60) - 2.81018e-07*60*60*60;
		//epsQCD    = 1.51395e-02*leppt - 1.16316e-04*leppt*leppt;
		//epsSignal = 4.77001e-01 + 1.32753e-01*log(leppt) - 1.39464e-07*leppt*leppt*leppt;
		//if(leppt> 65.)
		//{
                //epsQCD    = 1.51395e-02*65 - 1.16316e-04*65*65;
                //epsSignal = 4.77001e-01 + 1.32753e-01*log(65) - 1.39464e-07*65*65*65;
		//}
		//v204
		//epsQCD = 3.48020e-01;
		//epsSignal = 4.27901e-01 + 1.47106e-01*log(leppt) - 1.78011e-07*leppt*leppt*leppt;
		//v206
		//epsQCD = 9.05814e-02*log(leppt);
		//epsSignal = 3.42469e-01 + 1.71618e-01*log(leppt) - 2.47100e-07*leppt*leppt*leppt;
		//v209
		//epsQCD    = 7.88310e-01;
		//epsSignal = 6.99000e-01 + 7.62404e-02*log(leppt) - 6.94715e-08*leppt*leppt*leppt;
		//if(leppt > 70.) epsSignal = 6.99000e-01 + 7.62404e-02*log(70.) - 6.94715e-08*70.*70.*70.;
		//v211
		//epsQCD    = -3.27166e-01 + 1.61094e-01*log(leppt);
		//if(leppt > 40.) epsQCD = -3.27166e-01 + 1.61094e-01*log(40.);
		//epsSignal = -3.25817e-01 + 3.27018e-01*log(leppt) - 3.07403e-07*leppt*leppt*leppt;
		//if(leppt > 70.) epsSignal = -3.25817e-01 + 3.27018e-01*log(70.) - 3.07403e-07*70.*70.*70.;
		//v220
		//epsQCD    = 1.75996e-01*log(leppt);
		//if(leppt > 140.) epsQCD      = 1.75996e-01*log(140.);
		//v220b
		//epsQCD = 0.2;
		//epsSignal = 7.29462e-01 + 6.13490e-02*log(leppt) - 2.08905e-08*leppt*leppt*leppt;
		//if(leppt > 100.) epsSignal = 7.29462e-01 + 6.13490e-02*log(100.) - 2.08905e-08*100.*100.*100.;
		//v227
		//epsQCD    = 7.50300e-01;
		//epsSignal = 6.35230e-01 + 8.74362e-02*log(leppt) - 6.13478e-08*leppt*leppt*leppt;
		//if(leppt>90.) epsSignal = 6.35230e-01 + 8.74362e-02*log(90.) - 6.13478e-08*90.*90.*90.;
		//v232
		//epsQCD    = 7.26279e-01;
		//epsSignal = 8.14315e-01 + 4.35757e-02*log(leppt) - 2.26750e-08*leppt*leppt*leppt;
		//if(leppt > 90.) epsSignal = 8.14315e-01 + 4.35757e-02*log(90.) - 2.26750e-08*90.*90.*90.;
		//v235
		//epsQCD    = 2.52324e-01;
		//epsSignal = 4.59749e-01 + 1.29999e-01*log(leppt) - 1.01345e-07*leppt*leppt*leppt;
		//if(leppt > 90.) 4.59749e-01 + 1.29999e-01*log(90.) - 1.01345e-07*90.*90.*90.;
		//v237
		//epsQCD    = -1.42792e-01 + 7.72037e-03*leppt;
		//if(leppt > 80.) epsQCD  = -1.42792e-01 + 7.72037e-03*80.;
		//v237b
		//epsQCD    =  0.22324;
		//epsSignal =  5.81826e-01 + 1.00540e-01*log(leppt) - 7.16539e-08*leppt*leppt*leppt;
                //if(leppt > 90.) epsSignal =  5.81826e-01 + 1.00540e-01*log(90.) - 7.16539e-08*90.*90.*90.;
		//v237b/235
		//epsQCD   = 0.22324;
                //epsSignal = 4.59749e-01 + 1.29999e-01*log(leppt) - 1.01345e-07*leppt*leppt*leppt;
                //if(leppt > 90.) epsSignal = 4.59749e-01 + 1.29999e-01*log(90.) - 1.01345e-07*90.*90.*90.;
		//v242
		//epsQCD   = 0.75871;
		//epsSignal = 7.99557e-01 + 4.74387e-02*log(leppt) - 2.90691e-08*leppt*leppt*leppt;
		//if(leppt > 90.) epsSignal = 7.99557e-01 + 4.74387e-02*log(90.) - 2.90691e-08*90.*90.*90.;
		//v293
		//epsQCD    = 5.78603e-01;
		//epsSignal = 8.41331e-01 + 3.56294e-02*log(leppt) - 1.00623e-08*leppt*leppt*leppt;
		//v295
		//epsQCD    = 5.83858e-01;
		//epsSignal = 8.41331e-01 + 3.56294e-02*log(leppt) - 1.00623e-08*leppt*leppt*leppt;
		//v297
		//epsQCD    = 4.41090e-01;
		//epsSignal = 7.63421e-01 + 5.29081e-02*log(leppt);
		//if(leppt > 80.) epsSignal = 7.63421e-01 + 5.29081e-02*log(80.);
		//v300
		//epsQCD    = 4.70714e-01;
		//epsSignal = 7.40118e-01 + 6.01528e-02*leppt - 2.58074e-08*log(leppt);
		//if(leppt > 80.) epsSignal = 7.40118e-01 + 6.01528e-02*80. - 2.58074e-08*log(80.);
		//v308
		//epsQCD    = 2.82617e-01 + 3.44749e-03*leppt;
		//epsSignal = 6.82431e-01 + 7.37490e-02*log(leppt) - 3.55859e-08*leppt*leppt*leppt;
		//if(leppt > 125.)
		//{
		//	epsQCD    = 2.82617e-01 + 3.44749e-03*125.;
		//	epsSignal = 6.82431e-01 + 7.37490e-02*log(125.) - 3.55859e-08*125.*125.*125.;
		//}
		//v311
		//epsQCD    = -7.45163e-02 + 5.97552e-03*leppt;
		//epsSignal = 6.40087e-01 + 8.42057e-02*log(leppt) - 4.56625e-08*leppt*leppt*leppt;
		//if(leppt > 125.)
		//{
	        //        epsQCD    = -7.45163e-02 + 5.97552e-03*125.;
        	//        epsSignal = 6.40087e-01 + 8.42057e-02*log(125.) - 4.56625e-08*125.*125.*125.;
		//}
		//v328
		//epsQCD    = 2.84591e-01 + 3.37969e-03*leppt;
		//if(leppt >80.) epsQCD = 2.84591e-01 + 3.37969e-03*80.;
		//epsSignal = 6.82431e-01 + 7.37490e-02*log(leppt) - 3.55859e-08*leppt*leppt*leppt;
		//if(leppt >125.) epsSignal = 6.82431e-01 + 7.37490e-02*log(125.) - 3.55859e-08*125.*125.*125.;
		//v330
		//epsQCD    = -7.31973e-02 + 6.03298e-03*leppt;
		//if(leppt > 80.) epsQCD = -7.31973e-02 + 6.03298e-03*80.;
		//epsSignal = 6.29998e-01 + 8.59234e-02*log(leppt) - 4.13212e-08*leppt*leppt*leppt;
		//if(leppt > 125.) epsSignal = 6.29998e-01 + 8.59234e-02*log(125.) - 4.13212e-08*125.*125.*125.;
		//v356
		//epsQCD    = -2.88589e-01 + 9.29386e-03*leppt;
		//if(leppt > 80.) epsQCD    = -2.88589e-01 + 9.29386e-03*80.;
		//v356b
		//epsQCD    = 0.155;
		//v356c
		//epsQCD    = -7.98072e-02 + 4.32528e-03*leppt;
		//if(leppt > 80.) epsQCD = -7.98072e-02 + 4.32528e-03*80.;
		//d356d
		//epsQCD    = 2.43016e-03*leppt; 
		//if(leppt > 80.) epsQCD = 2.43016e-03*80.;
		//epsSignal = 4.56261e-01 + 1.22738e-01*log(leppt);
		//if(leppt > 70.) epsSignal = 4.56261e-01 + 1.22738e-01*log(70.);
                //v393
                //epsQCD    = 5.63010e-01;
                //epsSignal = 6.53369e-01 + 8.11465e-02*log(leppt) - 4.45092e-08*leppt*leppt*leppt;
                //if(leppt> 100.) epsSignal = 6.53369e-01 + 8.11465e-02*log(100.) - 4.45092e-08*100.*100.*100.;
                //v395
                //epsQCD    = -3.07542e-01 + 9.44642e-03*leppt;
                //if(leppt > 80.) epsQCD = -3.07542e-01 + 9.44642e-03*80.;
                //v395b 
                //epsQCD    = -1.94180e-01 + 6.78796e-03*leppt;
                //if(leppt> 70.) epsQCD = -1.94180e-01 + 6.78796e-03*70.;
                //epsSignal = 3.90864e-01 + 1.39888e-01*log(leppt);
                //if(epsSignal >9.94142e-01) epsSignal = 9.94142e-01; //fit from 70 upwards, cap efficiency with this value
		//v395b +1 sigma
                //epsQCD    = (-1.94180e-01+8.77522e-02) + (6.78796e-03+ 2.38194e-03)*leppt;
                //if(leppt> 70.) epsQCD = (-1.94180e-01+8.77522e-02) + (6.78796e-03+ 2.38194e-03)*70.;
                //epsSignal = (3.90864e-01+9.88197e-02) + (1.39888e-01+ 2.42501e-02)*log(leppt);
                //if(leppt >70) epsSignal = (3.90864e-01+9.88197e-02) + (1.39888e-01+ 2.42501e-02)*log(70.);
		//v395b -1 sigma
                //epsQCD    = (-1.94180e-01-8.77522e-02) + (6.78796e-03- 2.38194e-03)*leppt;
                //if(leppt> 70.) epsQCD = (-1.94180e-01-8.77522e-02) + (6.78796e-03- 2.38194e-03)*70.;
                //epsSignal = (3.90864e-01-9.88197e-02) + (1.39888e-01- 2.42501e-02)*log(leppt);
                //if(leppt >70) epsSignal = (3.90864e-01-9.88197e-02) + (1.39888e-01- 2.42501e-02)*log(70.);
		//v395c /////////FINAL 2010
		//epsQCD    = -2.27899e-01+ 7.27134e-03*leppt; 
		//if(leppt > 60.) epsQCD    = -2.27899e-01+ 7.27134e-03*60.;
		//epsSignal = 3.90864e-01 + 1.39888e-01*log(leppt);
                //if(leppt >70) epsSignal = 3.90864e-01 + 1.39888e-01*log(70.);
		//v395c+1sigma
                //epsQCD    = (-2.27899e-01+3.08052e-02)+ (7.27134e-03+7.79326e-04)*leppt;
                //if(leppt > 60.) epsQCD    = (-2.27899e-01+3.08052e-02)+ (7.27134e-03+7.79326e-04)*60.;
                //epsSignal = (3.90864e-01+9.88197e-02) + (1.39888e-01+ 2.42501e-02)*log(leppt);
                //if(leppt >70) epsSignal = (3.90864e-01+9.88197e-02) + (1.39888e-01+ 2.42501e-02)*log(70.);
		//v395c-1sigma
                //epsQCD    = (-2.27899e-01-3.08052e-02)+ (7.27134e-03-7.79326e-04)*leppt;
                //if(leppt > 60.) epsQCD    = (-2.27899e-01-3.08052e-02)+ (7.27134e-03-7.79326e-04)*60.;
                //epsSignal = (3.90864e-01-9.88197e-02) + (1.39888e-01- 2.42501e-02)*log(leppt);
                //if(leppt >70) epsSignal = (3.90864e-01-9.88197e-02) + (1.39888e-01- 2.42501e-02)*log(70.);
                //v398
                //epsQCD    = -2.12328e-01 + 9.69514e-03*leppt;
                //if(leppt>70.) epsQCD    = -2.12328e-01 + 9.69514e-03*70.;
                //epsSignal = 3.90864e-01 + 1.39888e-01*log(leppt);
                //if(epsSignal >9.94142e-01) epsSignal = 9.94142e-01; //fit from 70 upwards, cap efficiency with this value
                //v400
/*              if(dphi <= 1.)
                {
                        epsQCD    = -6.75602e-02 + 5.21264e-03*leppt;
                        epsSignal = 4.48923e-01 + 1.26272e-01*log(leppt);
                        if(leppt > 70.)
                                epsQCD    = -6.75602e-02 + 5.21264e-03*70.;
                        if(epsSignal > 9.83071e-01) epsSignal = 9.83071e-01;
                        
                }
                else if(dphi <= 2. && dphi > 1.)
                {
                        epsQCD    = 7.49455e-02 + 2.95945e-03*leppt;
                        epsSignal = 3.49047e-01 + 1.48351e-01*log(leppt);
                        if(leppt > 70.)
                                epsQCD    = 7.49455e-02 + 2.95945e-03*70.;
                        if(epsSignal > 9.91270e-01) epsSignal = 9.91270e-01;
                        
                }
                else if(dphi > 2.)
                {
                        epsQCD    = -3.18297e-01 + 1.11907e-02*leppt;
                        epsSignal = 4.14862e-01 + 1.33733e-01*log(leppt);
                        if(leppt > 70.)
                                epsQCD    = -3.18297e-01 + 1.11907e-02*70.;
                        if(epsSignal > 1.) epsSignal = 1.;
                }
 
*/
                //v402
                /*if(dphi <= 1.)
                {
                        epsQCD    = -1.19697e-01 + 5.91841e-03*leppt;
                        epsSignal = 5.04478e-01+ 1.11120e-01*log(leppt);
                        if(leppt > 70.)
                                epsQCD    = -1.19697e-01 + 5.91841e-03*70.;
                        if(epsSignal > 9.82421e-01) epsSignal = 9.82421e-01;
                        
                }
                else if(dphi <= 2. && dphi > 1.)
               {
                        epsQCD    = -2.49561e-01 + 1.10120e-02*leppt;
                        epsSignal =  3.91799e-01+ 1.39096e-01*log(leppt);
                        if(leppt > 70.)
                                epsQCD    = -2.49561e-01 + 1.10120e-02*70.;
                        if(epsSignal > 9.91170e-01) epsSignal = 9.91170e-01;
                        
                }
                else if(dphi > 2.)
                {
                        epsQCD    =  -2.28967e-01 + 9.61064e-03*leppt;
                        epsSignal =  3.47295e-01 + 1.50120e-01*log(leppt);
                        if(leppt > 70.)
                                epsQCD    =  -2.28967e-01 + 9.61064e-03*70.;
                        if(epsSignal > 1.) epsSignal = 1.;
                }
*/
                //v433/434
                //epsQCD    = 4.10284e-01;
                //epsSignal = 1.64948e-01 + 1.87733e-01*log(leppt);
                //if(leppt > 85.) epsSignal = 1.64948e-01 + 1.87733e-01*log(85.);
		//v447
		//epsQCD    = -2.42539e-01 + 1.70925e-01*log(leppt);
		//if(leppt > 110.)  epsQCD = -2.42539e-01 + 1.70925e-01*log(110.);
		//epsSignal = 2.37843e-01*log(leppt) - 1.51751e-07*leppt*leppt*leppt;
		//if(leppt > 70.) epsSignal = 2.37843e-01*log(70.) - 1.51751e-07*70.*70.*70.;

//		epsQCD = epsQCD * 0.32;

		//v452
/*                if(dphi <= 1.)
                {
                        epsQCD    = -4.88806e-01 + 1.76491e-02*leppt;
                        epsSignal = 5.48870e-01 + 8.69749e-02*log(leppt);
                        if(leppt > 70.)
                                epsQCD    = -4.88806e-01 + 1.76491e-02*70.;
                        if(leppt > 150.) 
				epsSignal = 5.48870e-01 + 8.69749e-02*log(150.);                        
                }
                else if(dphi <= 2. && dphi > 1.)
               {
                        epsQCD    = 4.29908e-01;
                        epsSignal = 5.74970e-01 + 8.53665e-02*log(leppt);
                        if(leppt > 150.)
                                epsSignal    = 5.74970e-01 + 8.53665e-02*log(150.);
                }
                else if(dphi > 2.)
                {
                        epsQCD    =  5.27080e-01;
                        epsSignal =  2.82151e-01 + 1.58781e-01*log(leppt);
                        if(leppt > 100.)
                                epsSignal =  2.82151e-01 + 1.58781e-01*log(100.);                        
                }

*/
		//v471
//		epsQCD    = -1.95706e-03 + 1.06007e-01*log(leppt);
//		epsSignal = 6.48196e-01 + 7.66068e-02*log(leppt);
		//v474
		//epsQCD    = -3.09871e-01 + 1.90865e-01*log(leppt);
		//if(leppt > 110.) epsQCD    =-3.09871e-01 + 1.90865e-01*log(110.); 
		//epsSignal = 6.02002e-01 + 8.96929e-02*log(leppt) - 1.94652e-08*leppt*leppt*leppt;
		//if(leppt>110.) epsSignal = 6.02002e-01 + 8.96929e-02*log(110.) - 1.94652e-08*110.*110.*110.;
		//v482
		//epsQCD    = 1.11714e-01*log(leppt);
		//epsSignal = 6.02359e-01 + 8.39414e-02*log(leppt);
		//if(leppt > 110.) epsSignal = 6.02359e-01 + 8.39414e-02*log(110.);
		//v486
/*		if(lepeta <=1.)
		{
			epsQCD    =  -1.09139 + 3.83510e-01*log(leppt);
			if(leppt > 90.)  epsQCD =  -1.09139 + 3.83510e-01*log(90.); 
			epsSignal = 2.29682e-01*log(leppt) - 8.76230e-08*leppt*leppt*leppt;
			if(leppt > 110.) epsSignal = 2.29682e-01*log(110.) - 8.76230e-08*110.*110.*110.;
		}
		else
		{
			epsQCD    = 5.19859e-01;
			epsSignal = 6.16497e-01 +  7.88100e-02*log(leppt); 
			if(leppt>110.) epsSignal = 6.16497e-01 +  7.88100e-02*log(110.);
		}*/
		//v488
//                if(lepeta <=1.8)
//                {
//                        epsQCD    =  -3.72310e-01 + 2.01494e-01*log(leppt);
//                        if(leppt > 150.)  epsQCD =  -3.72310e-01 + 2.01494e-01*log(150.);
//                        epsSignal = 3.02025e-01 + 1.49175e-01*leppt;
//                        if(leppt > 100.) epsSignal = 3.02025e-01 + 1.49175e-01*100.;
//                }
//                else
//                {
//                        epsQCD    = 6.56716e-01;
//                        epsSignal = 7.56330e-01 +  5.35856e-02*log(leppt);
//                        if(leppt>100.) epsSignal = 7.56330e-01 +  5.35856e-02*log(100.);
//                }
		//v488b 
/*                if(lepeta <=1.8)
                {
                        epsQCD    =   5.81982e-02*sqrt(leppt);
                        if(leppt > 135.)  epsQCD =  5.81982e-02*sqrt(135.);
                        epsSignal = 3.02025e-01 + 1.49175e-01*leppt;
                        if(leppt > 100.) epsSignal = 3.02025e-01 + 1.49175e-01*100.;
                }
                else
                {
                        epsQCD    = 6.56716e-01;
                        epsSignal = 8.61996e-01+2.06484e-03*leppt;
                        if(leppt>80.) epsSignal = 8.61996e-01+2.06484e-03*80.;
                }
*/
                //v488b+1 sigma
/*                if(lepeta <=1.8)
                {
                        epsQCD    =   (5.81982e-02+ 2.95923e-03)*sqrt(leppt);
                        if(leppt > 135.)  epsQCD =  (5.81982e-02+ 2.95923e-03)*sqrt(135.);
                        epsSignal = (3.02025e-01+7.15290e-02) + (1.49175e-01+7.15290e-02)*leppt;
                        if(leppt > 100.) epsSignal = (3.02025e-01+7.15290e-02) + (1.49175e-01+7.15290e-02)*100.;
                }
                else
                {
                        epsQCD    = 6.56716e-01+3.41998e-02;
                        epsSignal = (8.61996e-01+4.34809e-02)+(2.06484e-03+8.51430e-04)*leppt;
                        if(leppt>80.) epsSignal = (8.61996e-01+4.34809e-02)+(2.06484e-03+8.51430e-04)*80.;
                }*/
                //v488b-1 sigma 
/*                if(lepeta <=1.8)
                { 
                        epsQCD    =   (5.81982e-02- 2.95923e-03)*sqrt(leppt);
                        if(leppt > 135.)  epsQCD =  (5.81982e-02- 2.95923e-03)*sqrt(135.);
                        epsSignal = (3.02025e-01-7.15290e-02) + (1.49175e-01-7.15290e-02)*leppt;
                        if(leppt > 100.) epsSignal = (3.02025e-01-7.15290e-02) + (1.49175e-01-7.15290e-02)*100.;
                } 
                else 
                { 
                        epsQCD    = 6.56716e-01-3.41998e-02;
                        epsSignal = (8.61996e-01-4.34809e-02)+(2.06484e-03-8.51430e-04)*leppt;
                        if(leppt>80.) epsSignal = (8.61996e-01-4.34809e-02)+(2.06484e-03-8.51430e-04)*80.;
                }
*/
		//v509 FINAL 2011
  		if(lepeta <= 1.45)
                { 
                        epsQCD = (7.95157e-03)*leppt;
                        if(leppt> 80.) epsQCD = epsQCD = (7.95157e-03)*80.;
                        epsSignal = (3.46971e-01) + (1.36916e-01)*log(leppt);
                        if(leppt > 110.)  epsSignal = (3.46971e-01) + (1.36916e-01)*log(110.);
                } 
                else 
                { 
                        epsQCD = (6.45072e-01);
                        epsSignal = (7.45471e-01) + (5.14007e-02)*log(leppt);
                        if(leppt > 110.) epsSignal = (7.45471e-01) + (5.14007e-02)*log(110.);
                }
		//v509+1sigma
/*		if(lepeta <= 1.45)
		{
			epsQCD = (7.95157e-03+ 5.13977e-04)*leppt;
			if(leppt> 80.) epsQCD = epsQCD = (7.95157e-03+ 5.13977e-04)*80.;
			epsSignal = (3.46971e-01+6.15274e-02) + (1.36916e-01+1.44295e-02)*log(leppt);
			if(leppt > 110.)  epsSignal = (3.46971e-01+6.15274e-02) + (1.36916e-01+1.44295e-02)*log(110.);
		}
		else
		{
			epsQCD = (6.45072e-01 + 3.02282e-02);
			epsSignal = (7.45471e-01 + 7.03821e-02) + (5.14007e-02 + 1.69149e-02)*log(leppt);
                        if(leppt > 110.) epsSignal = (7.45471e-01 + 7.03821e-02) + (5.14007e-02 + 1.69149e-02)*log(110.);
		}*/
                //v509-1sigma
/*                if(lepeta <= 1.45)
                { 
                        epsQCD = (7.95157e-03- 5.13977e-04)*leppt;
                        if(leppt> 80.) epsQCD = epsQCD = (7.95157e-03- 5.13977e-04)*80.;
                        epsSignal = (3.46971e-01-6.15274e-02) + (1.36916e-01-1.44295e-02)*log(leppt);
                        if(leppt > 110.)  epsSignal = (3.46971e-01-6.15274e-02) + (1.36916e-01-1.44295e-02)*log(110.);
                } 
                else 
                { 
                        epsQCD = (6.45072e-01 - 3.02282e-02);
                        epsSignal = (7.45471e-01 - 7.03821e-02) + (5.14007e-02 - 1.69149e-02)*log(leppt);
                        if(leppt > 110.) epsSignal = (7.45471e-01 - 7.03821e-02) + (5.14007e-02 - 1.69149e-02)*log(110.);
                } 

*/
		//v518 3-JET
//		epsQCD    = 3.27618e-01 + 1.55276e-03*leppt;
//		epsSignal = 6.23595e-01 + 7.90410e-02*log(leppt);
		//v520 3-JET
/*		if(lepeta <= 1.45)
		{
			epsQCD    = 2.12826e-01 + 2.10366e-03*leppt;
			epsSignal = -5.93420e-02*sqrt(leppt) + 3.43152e-01*log(leppt);
			if(leppt > 110.) epsSignal = -5.93420e-02*sqrt(110.) + 3.43152e-01*log(110.);
		}
		else
		{
			epsQCD    = 6.03657e-01;
			epsSignal = -9.04552e-02*sqrt(leppt) + 4.10083e-01*log(leppt);
			if(leppt > 100.) epsSignal = -9.04552e-02*sqrt(100.) + 4.10083e-01*log(100.);
		}
*/
		//v520+1sigma
/*		if(lepeta <= 1.45)
                {
                        epsQCD    = (2.12826e-01+2.39804e-02) + (2.10366e-03+4.35656e-04)*leppt;
                        epsSignal = (-5.93420e-02+3.82634e-03)*sqrt(leppt) + (3.43152e-01+7.55884e-03)*log(leppt);
                        if(leppt > 110.) epsSignal = (-5.93420e-02+3.82634e-03)*sqrt(110.) + (3.43152e-01+7.55884e-03)*log(110.);
                }
                else
                {
                        epsQCD    = (6.03657e-01+1.40489e-02);
                        epsSignal = (-9.04552e-02+6.11058e-03)*sqrt(leppt) + (4.10083e-01+1.16665e-02)*log(leppt);
                        if(leppt > 100.) epsSignal = (-9.04552e-02+6.11058e-03)*sqrt(100.) + (4.10083e-01+1.16665e-02)*log(100.);
                }*/
                //v520-1sigma
/*                if(lepeta <= 1.45)
                {
                        epsQCD    = (2.12826e-01-2.39804e-02) + (2.10366e-03-4.35656e-04)*leppt;
                        epsSignal = (-5.93420e-02-3.82634e-03)*sqrt(leppt) + (3.43152e-01-7.55884e-03)*log(leppt);
                        if(leppt > 110.) epsSignal = (-5.93420e-02-3.82634e-03)*sqrt(110.) + (3.43152e-01-7.55884e-03)*log(110.);
                }
                else 
                {
                        epsQCD    = (6.03657e-01-1.40489e-02);
                        epsSignal = (-9.04552e-02-6.11058e-03)*sqrt(leppt) + (4.10083e-01-1.16665e-02)*log(leppt);
                        if(leppt > 100.) epsSignal = (-9.04552e-02-6.11058e-03)*sqrt(100.) + (4.10083e-01-1.16665e-02)*log(100.);
                }*/
                //v549 - 2010 3JETS 
                //epsQCD    =  6.99643e-03*leppt;
                //if(leppt > 65.) epsQCD =  6.99643e-03*65.;
                //epsSignal =  7.43970e-01 + 5.59384e-02*log(leppt);
                //if(leppt > 100.) epsSignal =  7.43970e-01 + 5.59384e-02*log(100.);
		//v549+1sigma
		//epsQCD    =  (6.99643e-03+6.16778e-04)*leppt;
		//if(leppt > 65.) epsQCD =  (6.99643e-03+6.16778e-04)*65.;
		//epsSignal =  (7.43970e-01+5.47266e-02) + (5.59384e-02+1.32684e-02)*log(leppt);
		//if(leppt > 100.) epsSignal =  (7.43970e-01+5.47266e-02) + (5.59384e-02+1.32684e-02)*log(100.);
                //v549-1sigma 
                //epsQCD    =  (6.99643e-03-6.16778e-04)*leppt;
                //if(leppt > 65.) epsQCD =  (6.99643e-03-6.16778e-04)*65.;
                //epsSignal =  (7.43970e-01-5.47266e-02) + (5.59384e-02-1.32684e-02)*log(leppt);
                //if(leppt > 100.) epsSignal =  (7.43970e-01-5.47266e-02) + (5.59384e-02-1.32684e-02)*log(100.);
		//sanity measure	
		if(epsSignal >1.) epsSignal = 1.;
		//jl 07.09.10
		if(epsQCD >1.)    epsQCD    = 1.;
		//for now, compute weights ONLY for tight distributions, ie the "tight" weights
		if(epsSignal == epsQCD)
		{
			w1 = w2 = 0.;
		}
		else
		{
			w1 = epsQCD * (epsSignal - 1.) / (epsSignal - epsQCD); //tight weight for tight event		
			w2 = epsQCD * (epsSignal     ) / (epsSignal - epsQCD); //tight weight for loose event	
		}

/*    if(loose_electrons->size() > 0)
{ 
        cout << " leppt " << leppt << "  epsQCD " << epsQCD << " epsSignal " << epsSignal << endl;
        cout << " w1 " << w1 << " w2 " << w2 << endl;
        cout << " istight " << isTight << endl;
        
}*/
	}
	
	if(isTight) weight = w1;
	else weight = w2;	
//	cout << "Nl, Nt, isTight, leppt, e, f, w, w1, w2 " << loose_muons->size() << "  " << isolated_muons->size() << "  " << isTight << "  " << leppt << "  " << epsSignal << "  " << epsQCD << "  " << weight << "  " << w1 << "  " << w2 << endl;
	//this plot is relevant for the "loose" cutset only, ie that which contains exactly one loose and at most one tight lepton
	//histos1d[("QCD_weights_"+id).c_str()]->Fill(weight);
//cout << "QCDMMPLots returns " << weight << endl;
	return weight;
}

//jl added 06.10.10
void QCDMMPlots::GetLiteralValues(double pt, double dphi)
{
	const int nbins = 50;
	//v93	
//	double fakeValue[30]={0,0,0.522074,0.540567,0.558715,0.526832,0.540718,0.558865,0.633333,0.58,0.625,0.692308,0.777778,1,1,0,1,1,1,1,1,0,0,1,0,1,0,1,0,1};
//	double effValue[30]={0,0,0.814815,0.854111,0.927553,0.951787,0.965222,0.978448,0.985989,0.990923,0.992059,0.993932,0.996506,0.996092,0.998065,0.998265,0.999087,0.999563,0.999577,1.00009,1,0.999529,1,0.99972,1.00034,1.00084,1.00053,1,1,1};
	//v93 with contamination removal
	//double fakeValue[30] = {0,0,0.522074,0.540566,0.558585,0.526454,0.539802,0.556768,0.629864,0.57188,0.607288,0.671491,0.733945,1.00218,0.983697,2.9527,1,0.939701,1.01103,0.990122,1,1,1,1,1,1,1,1,1};
        //double effValue[30]={0,0,0.814815,0.854111,0.927553,0.951787,0.965222,0.978448,0.985989,0.990923,0.992059,0.993932,0.996506,0.996092,0.998065,0.998265,0.999087,0.999563,0.999577,1.00009,1,0.999529,1,0.99972,1.00034,1.00084,1.00053,1,1,1};
	//v129 with dphi-dependence
	double fakeValue[nbins];
	double effValue[nbins];
        double fakeValue_a[nbins] = {0,0,0.278961,0.270993,0.272539,0.23709,0.183763,0.195541,0.177908,0.283431,0.245595,0.401488,0.296382,0.350756,0.578331,0.53084,0.0311548,0.283651,0.49085,1.0456,1.04963,0.888866,1.024,1.02384,0.935513,1,0.97869,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        double effValue_a[nbins]  = {0,0,0.565298,0.622124,0.661489,0.711368,0.773953,0.81084,0.83049,0.851436,0.878016,0.883686,0.928367,0.921697,0.915937,0.924453,0.947475,0.92,0.947631,0.956204,0.968338,0.960725,0.978495,0.988806,0.956863,0.972376,0.964103,0.984375,0.976879,0.987654,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        double fakeValue_b[nbins] = {0,0,0.277374,0.269618,0.281256,0.240576,0.2246,0.247967,0.270166,0.212624,0.335827,0.238759,0.396904,0.344473,0.590392,0.678207,0.491224,0.795698,1.01361,0.636046,0.97135,1.01572,0.994252,1.00288,1,1.03247,1.02772,0.979896,0.950995,0.994968,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        double effValue_b[nbins]  = {0,0,0.612782,0.661355,0.646943,0.647368,0.72028,0.8,0.79,0.774648,0.932432,0.857143,0.852459,0.875,0.842105,0.884615,0.910714,0.954545,0.97561,0.977273,0.964286,0.928571,0.962963,0.888889,1,0.888889,1,0.9375,0.928571,0.9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
       double fakeValue_c[nbins] = {0,0,0.281984,0.288513,0.282396,0.274339,0.227768,0.245891,0.21112,0.253026,0.302008,0.315815,0.467336,0.44241,0.396363,0.714841,1.01625,0.590374,0.932134,0.553718,1.01601,1.14847,1.01676,1.14384,1.00196,0.914776,0.949013,1.01533,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        double effValue_c[nbins]  = {0,0,0.512195,0.63786,0.648,0.634146,0.785714,0.627907,0.828125,0.829787,0.689655,0.888889,0.837838,0.789474,0.90625,0.944444,1,0.888889,0.909091,0.952381,1,1,0.846154,1,1,1,1,0.333333,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

        if(dphi <= 1.)
        {
		for(int i=0; i<nbins; i++)
		{
			fakeValue[i] = fakeValue_a[i];
			effValue[i]  = effValue_a[i];
		}
        }
        else if(dphi > 1. && dphi < 2.5)
        {
                for(int i=0; i<nbins; i++)
                {
                        fakeValue[i] = fakeValue_b[i];
                        effValue[i]  = effValue_b[i];
                }
        }
        else if(dphi>= 2.5 && dphi <= TMath::Pi())
        {
                for(int i=0; i<nbins; i++)
                {
                        fakeValue[i] = fakeValue_c[i];
                        effValue[i]  = effValue_c[i];
                }
        }

	
	int index           = 0;
	double i            = 2.;//1 bin = 2 GeV
	double lastGoodFake = -1.;
	double lastGoodEff  = -1.;
	while(1)
	{
		if(index>1)
		{
			if(fakeValue[index] != 0. && fakeValue[index] < 1.) lastGoodFake = fakeValue[index];
			if(effValue[index]  != 0. && effValue[index]  < 1.) lastGoodEff  = effValue[index];
		}
		if(pt < i)	
		{
			epsQCD    = fakeValue[index];
			epsSignal = effValue[index];
			if(epsQCD    == 0. || epsQCD    >= 1.) epsQCD    = lastGoodFake; //jl 07.10.10: >= to counter fluctuations when removing contamination...
			if(epsSignal == 0. || epsSignal == 1.) epsSignal = lastGoodEff;
			break;
		}
		i = i+2;
		index++;
		if(index > 29)
		{
			cout << "[QCDMMPlots]: !WARNING! GetLitteralValues returned an index greater than nbins!" << endl;
			cout << "[QCDMMPlots]: !WARNING! I'll use index=0...." << endl;
			index=0; break;
		}		
	}
//	cout <<  " pT " << pt << " index " << index << " fake " << fakeValue[index] << " eff " << effValue[index] << endl;
}
