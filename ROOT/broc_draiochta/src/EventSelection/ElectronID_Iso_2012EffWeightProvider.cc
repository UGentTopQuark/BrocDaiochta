#include "../../interface/EventSelection/ElectronID_Iso_2012EffWeightProvider.h"

broc::ElectronID_Iso_2012EffWeightProvider::ElectronID_Iso_2012EffWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	electrons = handle_holder->get_tight_electrons();

	double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_e_ID_ISO_unc");
	
	up = false;
	down = false;

	if(study_unc == 1){ down = true; }
	else if(study_unc == 2){ up = true; }
}

broc::ElectronID_Iso_2012EffWeightProvider::~ElectronID_Iso_2012EffWeightProvider()
{
}

double broc::ElectronID_Iso_2012EffWeightProvider::get_weight(int UpDown)
{
	double weight = 1.;

	if(do_not_reweight)
		weight = 1.;
	else{
		if(electrons->size() >= 1){
			std::vector<mor::Electron>::iterator e = electrons->begin();
			double eta = e->supercluster_eta();
			double abseta = std::abs(eta);
			double pt = e->pt();
			double ud = 0.; // up-down switch for systematics
			if(up || UpDown == 1) ud = 1.;
			else if(down || UpDown == -1) ud = -1.;
			if(abseta < 0.8){
			  if(pt > 50.){ weight = 0.962+ud*0.001; }
			  else if(pt < 20.){ weight = 0.; }
			  else if(pt < 30.){ weight = 0.962+ud*0.005; }
			  else if(pt < 40.){ weight = 0.948+ud*0.002; }
			  else { weight = 0.961+ud*0.001; }
			}else if(abseta < 1.478){
			  if(pt > 50.){ weight = 0.961+ud*0.002; }
			  else if(pt < 20.){ weight = 0.; }
			  else if(pt < 30.){ weight = 0.940+ud*0.010; }
			  else if(pt < 40.){ weight = 0.930+ud*0.001; }
			  else { weight = 0.965+ud*0.002; }
			}else if(abseta < 2.5){
			  if(pt > 50.){ weight = 0.960+ud*0.006; }
			  else if(pt < 20.){ weight = 0.; }
			  else if(pt < 30.){ weight = 0.933+ud*0.017; }
			  else if(pt < 40.){ weight = 0.924+ud*0.003; }
			  else { weight = 0.962+ud*0.004; }
			}
			else{
			  weight = 0.;
			}
		}
	}

	return weight;
}
