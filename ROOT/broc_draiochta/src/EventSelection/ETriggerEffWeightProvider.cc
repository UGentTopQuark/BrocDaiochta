#include "../../interface/EventSelection/ETriggerEffWeightProvider.h"

broc::ETriggerEffWeightProvider::ETriggerEffWeightProvider(eire::HandleHolder *handle_holder)
{
	if(handle_holder->get_ident().find("Data") != std::string::npos)
		do_not_reweight = true;
	else
		do_not_reweight = false;

	electrons = handle_holder->get_tight_electrons();

	double study_unc = handle_holder->get_cuts_set()->get_cut_value("study_e_trigger_unc");
	
	up = false;
	down = false;

	if(study_unc == 1){ down = true; }
	else if(study_unc == 2){ up = true; }
}

broc::ETriggerEffWeightProvider::~ETriggerEffWeightProvider()
{
}

double broc::ETriggerEffWeightProvider::get_weight(int UpDown)
{
	double weight = 1.;

	if(do_not_reweight)
		weight = 1.;
	else{
		if(electrons->size() >= 1){
			std::vector<mor::Electron>::iterator e = electrons->begin();
			double eta = e->supercluster_eta();
			double abseta = std::abs(eta);
			double pt = e->pt();
			double ud = 0.; // up-down switch for systematics
			if(up || UpDown == 1) ud = 1.;
			else if(down || UpDown == -1) ud = -1.;

			//for Ele27_WP80, full 2012, no Jan22
			if(abseta < 0.8){
			  if(pt > 50.){ weight = 0.998+ud*0.002; }
			  else if(pt < 30.){ weight = 0.; }
			  else if(pt < 40.){ weight = 0.987+ud*0.017; }
			  else { weight = 0.997+ud*0.001; }
			}else if(abseta < 1.478){
			  if(pt > 50.){ weight = 0.988+ud*0.002; }
			  else if(pt < 30.){ weight = 0.; }
			  else if(pt < 40.){ weight = 0.964+ud*0.002; }
			  else { weight = 0.980+ud*0.001; }
			}else if(abseta < 2.5){
			  if(pt > 50.){ weight = 0.976+ud*0.015; }
			  else if(pt < 30.){ weight = 0.; }
			  else if(pt < 40.){ weight = 1.004+ud*0.006; }
			  else { weight = 1.033+ud*0.007; }
			}
			else{weight = 0.;}
			/*
			if(eta > 0){ //correct for 4/fb
				if(abseta < 1.0){
			 		if(pt > 45.){ weight = 0.953+ud*0.003; }
			 		else if(pt < 30.){ weight = 0.; }
			 		else if(pt < 35.){ weight = 0.85+ud*0.03; }
			 		else if(pt < 40.){ weight = 0.93+ud*0.02; }
			 		else { weight = 0.92+ud*0.02; }
			 	}else if(abseta < 1.5){
			 		if(pt > 45.){ weight = 0.92+ud*0.01; }
			 		else if(pt < 30.){ weight = 0.; }
			 		else if(pt < 35.){ weight = 0.93+ud*0.04; }
			 		else if(pt < 40.){ weight = 0.77+ud*0.06; }
			 		else { weight = 0.83+ud*0.03; }
			 	}else{
			 		if(pt > 45.){ weight = 0.90+ud*0.01; }
			 		else if(pt < 30.){ weight = 0.; }
			 		else if(pt < 35.){ weight = 0.68+ud*0.07; }
			 		else if(pt < 40.){ weight = 0.85+ud*0.05; }
			 		else { weight = 0.97+ud*0.03; }
			 	}
			}else{
			 	if(abseta < 1.0){
			 		if(pt > 45.){ weight = 0.952+ud*0.003; }
			 		else if(pt < 30.){ weight = 0.; }
			 		else if(pt < 35.){ weight = 0.93+ud*0.03; }
			 		else if(pt < 40.){ weight = 0.92+ud*0.03; }
			 		else { weight = 0.90+ud*0.02; }
			 	}else if(abseta < 1.5){
			 		if(pt > 45.){ weight = 0.92+ud*0.01; }
			 		else if(pt < 30.){ weight = 0.; }
			 		else if(pt < 35.){ weight = 0.84+ud*0.05; }
			 		else if(pt < 40.){ weight = 0.80+ud*0.06; }
			 		else { weight = 0.88+ud*0.03; }
			 	}else{
			 		if(pt > 45.){ weight = 0.88+ud*0.01; }
			 		else if(pt < 30.){ weight = 0.; }
			 		else if(pt < 35.){ weight = 0.67+ud*0.07; }
			 		else if(pt < 40.){ weight = 0.81+ud*0.07; }
			 		else { weight = 0.83+ud*0.05; }
			 	}
			}
			*/
			// if(eta > 0){ //for 2/fb. error not correct
			// 	if(abseta < 1.0){
			// 		if(pt > 45.){ weight = 0.953+ud*0.003; }
			// 		else if(pt < 30.){ weight = 0.; }
			// 		else if(pt < 35.){ weight = 0.85+ud*0.03; }
			// 		else if(pt < 40.){ weight = 0.96+ud*0.02; }
			// 		else { weight = 0.91+ud*0.02; }
			// 	}else if(abseta < 1.5){
			// 		if(pt > 45.){ weight = 0.90+ud*0.01; }
			// 		else if(pt < 30.){ weight = 0.; }
			// 		else if(pt < 35.){ weight = 1.0+ud*0.04; }
			// 		else if(pt < 40.){ weight = 0.68+ud*0.06; }
			// 		else { weight = 0.84+ud*0.03; }
			// 	}else{
			// 		if(pt > 45.){ weight = 0.84+ud*0.01; }
			// 		else if(pt < 30.){ weight = 0.; }
			// 		else if(pt < 35.){ weight = 0.78+ud*0.07; }
			// 		else if(pt < 40.){ weight = 0.83+ud*0.05; }
			// 		else { weight = 0.92+ud*0.03; }
			// 	}
			// }else{
			// 	if(abseta < 1.0){
			// 		if(pt > 45.){ weight = 0.952+ud*0.003; }
			// 		else if(pt < 30.){ weight = 0.; }
			// 		else if(pt < 35.){ weight = 1.0+ud*0.03; }
			// 		else if(pt < 40.){ weight = 0.91+ud*0.03; }
			// 		else { weight = 0.92+ud*0.02; }
			// 	}else if(abseta < 1.5){
			// 		if(pt > 45.){ weight = 0.91+ud*0.01; }
			// 		else if(pt < 30.){ weight = 0.; }
			// 		else if(pt < 35.){ weight = 0.92+ud*0.05; }
			// 		else if(pt < 40.){ weight = 0.75+ud*0.06; }
			// 		else { weight = 0.88+ud*0.03; }
			// 	}else{
			// 		if(pt > 45.){ weight = 0.85+ud*0.01; }
			// 		else if(pt < 30.){ weight = 0.; }
			// 		else if(pt < 35.){ weight = 0.71+ud*0.07; }
			// 		else if(pt < 40.){ weight = 0.88+ud*0.07; }
			// 		else { weight = 0.86+ud*0.05; }
			// 	}
			// }
		}
	}

	return weight;
}
