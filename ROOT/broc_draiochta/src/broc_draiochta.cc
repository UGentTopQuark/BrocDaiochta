#include "TFile.h"
#include <vector>
#include <sstream>
#include <iostream>
#include "../interface/NTupleReader/Analyser.h"
#include "ConfigReader/ConfigReader.h"
#include "../interface/AnalysisTools/DatasetNameProvider.h"

int main(int argc, char **argv)
{
	if(argc <= 1){
		std::cerr << "usage: ./broc_draiochta <config file>" << std::endl;
		return 1;
	}
	
	std::string version = "_plots";

	std::string config_file = argv[1];
	//Check first argument not rootfile and assign outfile name
	int is_rootfile = config_file.find(".root");
	if(is_rootfile != int(std::string::npos)){
		std::cout << "ERROR: No config file input.Arg1 = " << config_file << std::endl;
		return 0;
	}

	std::string default_config = "share/default_config.txt";
	eire::ConfigReader *config_reader = new eire::ConfigReader();
	//jl 21.01.11: don't die if default_config not found
//	bool read_default_config = config_reader->read_config_from_file(default_config);
	bool read_config = config_reader->read_config_from_file(config_file, true);
//	if(!read_default_config && !read_config) 
	if(!read_config) 
	{
		std::cout << "[broc_draiochta]: No valid config file found!" << std::endl;
		std::cout << "Neither " << default_config << " nor " <<  config_file << " exist; dying..." << std::endl;
		return 2;
	}
	std::string dataset_name = config_reader->get_var("dataset");
	std::string output_dir = config_reader->get_var("output_directory");
	config_reader->read_list_from_file(config_reader->get_var("input_files"));
	version = version+config_reader->get_var("outfile_suffix");
	int max_events = atoi(config_reader->get_var("max_events").c_str());

	std::string outfile_name = output_dir+"/"+dataset_name+version+".root";
	TFile *outfile = new TFile(outfile_name.c_str(), "RECREATE");
        outfile->cd();
        std::vector<std::string> *file_names = config_reader->get_list();
	std::cout << "files to process:" << std::endl;
	for(std::vector<std::string>::iterator file_name = file_names->begin();
		file_name != file_names->end();
		++file_name){
		std::cout << *file_name << std::endl;
	}

	// all data files are mapped to dataset name "Data"
	if(dataset_name.find("Data") != std::string::npos || dataset_name.find("data") != std::string::npos){
		dataset_name = "Data";
	}else{
		if(config_reader->get_bool_var("apply_dataset_name_mapping","global", false)){
			broc::DatasetNameProvider dnprovider(config_reader);
			dataset_name = dnprovider.get_name(dataset_name);
		}
	}

	bool process_mc = false;
	if(dataset_name == "Data")
		process_mc = false;
	else
		process_mc = true;

        Analyser *analyser = new Analyser(dataset_name, config_reader, outfile, process_mc);
        analyser->set_file_names(file_names);
        analyser->set_max_events(max_events);
        analyser->analyse();

        delete analyser;
        analyser = NULL;

	delete config_reader;
	config_reader = NULL;

	return 0;
}
