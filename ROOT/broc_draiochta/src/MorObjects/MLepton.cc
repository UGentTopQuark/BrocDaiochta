#include "../../interface/MorObjects/MLepton.h"

mor::Lepton::Lepton()
{
	rho_ = 0.;
	real_data_ = false;
}

bool mor::Lepton::triggered(std::string trigger_name)
{
	return trigger_id_checker->id_passed(trigger_name, trigger);
}

void mor::Lepton::set_beag_info(beag::Lepton &beag_lepton)
{
	mor::Particle::set_beag_info(beag_lepton);

	ecal_iso = beag_lepton.ecal_iso;
	hcal_iso = beag_lepton.hcal_iso;
	track_iso = beag_lepton.track_iso;

	d0_val = beag_lepton.d0;
	d0_sigma_val = beag_lepton.d0_sigma;

	hcal_vcone_val = beag_lepton.hcal_vcone;
	ecal_vcone_val = beag_lepton.ecal_vcone;

	lepton_id = beag_lepton.lepton_id;
	track_available_val = beag_lepton.track_available;

	photonIso_ = beag_lepton.photonIso;
	neutralHadronIso_ = beag_lepton.neutralHadronIso;
	chargedHadronIso_ = beag_lepton.chargedHadronIso;
	puChargedHadronIso_ = beag_lepton.puChargedHadronIso;

	trigger = beag_lepton.trigger;

	nLostTrackerHits_val = beag_lepton.nLostTrackerHits;
	nPixelHits_val = beag_lepton.nPixelHits;

	z0_val = beag_lepton.z0;
	z0_pv_val = beag_lepton.z0_pv;

	vz_val = beag_lepton.vz;
}

bool mor::Lepton::lepton_id_passed(std::string ID_name)
{
	return lepton_id_checker->id_passed(ID_name, lepton_id);
}

double mor::Lepton::photonIso(std::string conesize)
{
       	return pfiso_conesize_checker->id_value<double>(conesize, &photonIsos_);
}

double mor::Lepton::neutralHadronIso(std::string conesize)
{
       	return pfiso_conesize_checker->id_value<double>(conesize, &neutralHadronIsos_);
}

double mor::Lepton::chargedHadronIso(std::string conesize)
{
       	return pfiso_conesize_checker->id_value<double>(conesize, &chargedHadronIsos_);
}

void mor::Lepton::set_lepton_id_checker(mor::IDChecker *lepton_id_checker)
{
	this->lepton_id_checker = lepton_id_checker;
}

void mor::Lepton::set_trigger_id_checker(mor::IDChecker *trigger_id_checker)
{
	this->trigger_id_checker = trigger_id_checker;
}

void mor::Lepton::set_pfiso_conesize_checker(mor::IDChecker *pfiso_conesize_checker)
{
	this->pfiso_conesize_checker = pfiso_conesize_checker;
}
