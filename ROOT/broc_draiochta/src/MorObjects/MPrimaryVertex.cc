#include "../../interface/MorObjects/MPrimaryVertex.h"

mor::PrimaryVertex::PrimaryVertex(beag::PrimaryVertex &beag_primary_vertex)
{
	set_beag_info(beag_primary_vertex);
}

void mor::PrimaryVertex::set_beag_info(beag::PrimaryVertex &beag_primary_vertex)
{
	z_val = beag_primary_vertex.z;	
	rho_val = beag_primary_vertex.rho;	
	ndof_val = beag_primary_vertex.ndof;	
	is_fake_val = beag_primary_vertex.is_fake;	
}
