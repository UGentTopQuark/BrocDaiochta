#include "../../interface/MorObjects/MElectron.h"

mor::Electron::Electron()
{
}

mor::Electron::Electron(beag::Electron &beag_lepton)
{
	set_beag_info(beag_lepton);
}

void mor::Electron::set_year(int year)
{
  rhocor_year = year;
}

void mor::Electron::set_beag_info(beag::Electron &beag_lepton)
{
	mor::Particle::set_beag_info(beag_lepton);
	mor::Lepton::set_beag_info(beag_lepton);

	ecal_iso = beag_lepton.ecal_iso;
	hcal_iso = beag_lepton.hcal_iso;
	track_iso = beag_lepton.track_iso;

	d0_val = beag_lepton.d0;
	d0_sigma_val = beag_lepton.d0_sigma;

	d0_pv_val = beag_lepton.d0_pv;
	d0_sigma_pv_val = beag_lepton.d0_sigma_pv;

	hcal_vcone_val = beag_lepton.hcal_vcone;
	ecal_vcone_val = beag_lepton.ecal_vcone;

	sc_eta_val = beag_lepton.sc_eta;
	sc_phi_val = beag_lepton.sc_phi;

	lepton_id = beag_lepton.lepton_id;
	track_available_val = beag_lepton.track_available;

	trigger = beag_lepton.trigger;

	nLostTrackerHits_val = beag_lepton.nLostTrackerHits;
	nPixelHits_val = beag_lepton.nPixelHits;

	z0_val = beag_lepton.z0;
	z0_pv_val = beag_lepton.z0_pv;

	vz_val = beag_lepton.vz;

	photonIso_ = beag_lepton.photonIso;
	neutralHadronIso_ = beag_lepton.neutralHadronIso;
	chargedHadronIso_ = beag_lepton.chargedHadronIso;

	dist_val = beag_lepton.dist;
	dcot_val = beag_lepton.dcot;

	passconversionveto_val = beag_lepton.passconversionveto; //jl 04.05.12

	photonIsos_.clear();
	neutralHadronIsos_.clear();
	chargedHadronIsos_.clear();
	photonIsos_ = beag_lepton.photonIsos;
	neutralHadronIsos_ = beag_lepton.neutralHadronIsos;
	chargedHadronIsos_ = beag_lepton.chargedHadronIsos;
	puChargedHadronIso_ = beag_lepton.puChargedHadronIso;
	
	sigmaIetaIeta_ = beag_lepton.sigmaIetaIeta;
	deltaPhiSuperClusterTrackAtVtx_ = beag_lepton.deltaPhiSuperClusterTrackAtVtx;
	deltaEtaSuperClusterTrackAtVtx_ = beag_lepton.deltaEtaSuperClusterTrackAtVtx;
	hadronicOverEm_ = beag_lepton.hadronicOverEm;

	myMVAVar_fbrem_ = beag_lepton.myMVAVar_fbrem;
	myMVAVar_kfchi2_ = beag_lepton.myMVAVar_kfchi2;
	myMVAVar_kfhits_ = beag_lepton.myMVAVar_kfhits; 
	myMVAVar_kfvalidhits_ = beag_lepton.myMVAVar_kfvalidhits; 
	myMVAVar_gsfchi2_ = beag_lepton.myMVAVar_gsfchi2;  // to be checked 
	
	myMVAVar_deta_ = beag_lepton.myMVAVar_deta;
	myMVAVar_dphi_ = beag_lepton.myMVAVar_dphi;
	myMVAVar_detacalo_ = beag_lepton.myMVAVar_detacalo;
	myMVAVar_dphicalo_ = beag_lepton.myMVAVar_dphicalo;   
	
	myMVAVar_see_ = beag_lepton.myMVAVar_see;    //EleSigmaIEtaIEta
	
	myMVAVar_spp_ = beag_lepton.myMVAVar_spp;
	myMVAVar_etawidth_ = beag_lepton.myMVAVar_etawidth;
	myMVAVar_phiwidth_ = beag_lepton.myMVAVar_phiwidth;
	
	myMVAVar_e1x5e5x5_ = beag_lepton.myMVAVar_e1x5e5x5;
	myMVAVar_R9_ = beag_lepton.myMVAVar_R9;
	myMVAVar_nbrems_ = beag_lepton.myMVAVar_nbrems;

	myMVAVar_HoE_ = beag_lepton.myMVAVar_HoE;
	myMVAVar_EoP_ = beag_lepton.myMVAVar_EoP;
	myMVAVar_IoEmIoP_ = beag_lepton.myMVAVar_IoEmIoP;  // in the future to be changed with ele.gsfTrack()->p()
	myMVAVar_eleEoPout_ = beag_lepton.myMVAVar_eleEoPout;
	myMVAVar_EoPout_ = beag_lepton.myMVAVar_EoPout;
	myMVAVar_PreShowerOverRaw_ = beag_lepton.myMVAVar_PreShowerOverRaw;
	
	myMVAVar_eta_ = beag_lepton.myMVAVar_eta;         
	myMVAVar_pt_ = beag_lepton.myMVAVar_pt;       
	
	myMVAVar_d0_ = beag_lepton.myMVAVar_d0;
	myMVAVar_ip3d_ = beag_lepton.myMVAVar_ip3d;
}

double mor::Electron::EA_PFrelIso()
{
	double Aeff = 0;

	double eta = std::abs(this->eta());
	if(rhocor_year == 2012){
		if(eta < 1.0)
		  Aeff = 0.13;
		else if(eta < 1.479)
		  Aeff = 0.14;
		else if(eta < 2.0)
		  Aeff = 0.07;
		else if(eta < 2.2)
		  Aeff = 0.09;
		else if(eta < 2.3)
		  Aeff = 0.11;
		else if(eta < 2.4)
		  Aeff = 0.11;
		else
		  Aeff = 0.14;
	}else if(rhocor_year == 2011 && real_data_){		// 0.3 isolation cone
	            if(eta < 1.0)
			Aeff = 0.10;
		else if(eta < 1.479)
			Aeff = 0.12;
		else if(eta < 2.0)
			Aeff = 0.085;
		else if(eta < 2.2)
			Aeff = 0.11;
		else if(eta < 2.3)
			Aeff = 0.12;
		else if(eta < 2.4)
			Aeff = 0.12;
		else
			Aeff = 0.13;
	}else if(rhocor_year == 2011 && !real_data_){
		if(eta < 1.0)
			Aeff = 0.11;
		else if(eta < 1.479)
			Aeff = 0.13;
		else if(eta < 2.0)
			Aeff = 0.089;
		else if(eta < 2.2)
			Aeff = 0.13;
		else if(eta < 2.3)
			Aeff = 0.15;
		else if(eta < 2.4)
			Aeff = 0.16;
		else
			Aeff = 0.19;
	}
	double relIso = (this->chargedHadronIso_+std::max(this->photonIso_+this->neutralHadronIso_-rho_*Aeff, 0.))/this->pt();
	return relIso;
}
