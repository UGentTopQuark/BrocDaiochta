#include "../../interface/TopTrigger/TopComSyncEx.h"

truicear::TopComSyncEx::TopComSyncEx(std::string ident)
{
	this->id = ident;
}

truicear::TopComSyncEx::~TopComSyncEx()
{
}

void truicear::TopComSyncEx::set_handles(mor::EventInformation *evt_info, std::vector<mor::Muon> *reco_muons)
{
	this->evt_info = evt_info;
	this->reco_muons = reco_muons;
	
	muon_matches.clear();
	for(unsigned int nmuon = 0; nmuon < reco_muons->size(); ++nmuon){
		muon_matches[nmuon].first = false;	
		muon_matches[nmuon].second = false;	
	}
}

void truicear::TopComSyncEx::set_matches(std::vector<std::pair<int,int> > *matches, bool l1seed)
{
	for(std::vector<std::pair<int,int> >::iterator match = matches->begin();
		match != matches->end();
		++match){
		if(l1seed)
			muon_matches[match->first].first = true;
		else
			muon_matches[match->first].second = true;
	}
}

void truicear::TopComSyncEx::print()
{
	for(std::map<int, std::pair<bool,bool> >::iterator muon_match = muon_matches.begin();
		muon_match != muon_matches.end();
		++muon_match){
		/*
		 *	check for each jet-number in the reco vector
		 *	(muon_match->first) if either l1seed is matched
		 *	(muon_match->second->first) or hlt is matched
		 *	(muon_match->second->second)
		 */
		std::cout //<< id << " "
			  << "SELECTED:"
			  << " Run  " << std::setprecision(10) << evt_info->run()
			  << " lumiSec  " << std::setprecision(10) << evt_info->lumi_block()
			  << " Event  " << std::setprecision(10) << evt_info->event_number()
			  << " Cand #  " << std::setprecision(10) << muon_match->first 
			  << "   Pt " << std::setprecision(5) << std::setw(8) << (*reco_muons)[muon_match->first].pt()
			  << "  Eta " << std::setprecision(5) << std::setw(8) << (*reco_muons)[muon_match->first].eta()
			  << "  Phi " << std::setprecision(5) << std::setw(8) << (*reco_muons)[muon_match->first].phi()
			  << "  foundL1Seed " << std::setw(4) <<  muon_match->second.first
			  << "  foundHLTObj " << std::setw(4) <<  muon_match->second.second
			  << std::endl;
	}

	muon_matches.clear();
	for(unsigned int nmuon = 0; nmuon < reco_muons->size(); ++nmuon){
		muon_matches[nmuon].first = false;	
		muon_matches[nmuon].second = false;	
	}
}
