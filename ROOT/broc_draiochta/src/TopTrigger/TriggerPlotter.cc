#include "../../include/TopTrigger/TriggerPlotter.h"

truicear::TriggerPlotter::TriggerPlotter(HistoWriter *histo_writer, std::string name, int nbinsx, double minx, double maxx)
{
	this->histo_writer = histo_writer;
	pass_1d = histo_writer->create_1d(name+"_pass", name+"_pass", bins, minx, max);
	all_1d = histo_writer->create_1d(name+"_all", name+"_all", bins, minx, max);
}

truicear::TriggerPlotter::~TriggerPlotter()
{
}

truicear::TriggerPlotter::fill(double value, bool pass)
{
	if(pass) pass_1d->fill(value);
	all_1d->fill(value);	
}
