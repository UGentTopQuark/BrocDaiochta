#include "../../interface/TopTrigger/BasicElectronPlots.h"

truicear::BasicElectronPlots::BasicElectronPlots()
{
}

truicear::BasicElectronPlots::~BasicElectronPlots()
{
}

void truicear::BasicElectronPlots::book_histos()
{
	int nptBins = 14;
	float  ptBins[100] = { 0,5,6,7,8,9,10,11,13,16,20,25,40,60,100 };
	int netaBins = 12 ;
	float  etaBins[100] = { -2.4, -2.1, -1.6, -1.2, -0.9, -0.4, 0., 0.4,  0.9, 1.2, 1.6, 2.1, 2.4};

	histos1d[pre+"e_phi"+id]=histo_writer->create_1d(pre+"e_phi"+id,"#phi electrons "+pre,20,-3.3,3.3, "#phi");
	histos1d[pre+"e_pt"+id]=histo_writer->create_1d(pre+"e_pt"+id,"p_{t} electrons "+pre,nptBins,ptBins, "p_{t} [GeV]");
	histos1d[pre+"e_pt_fine"+id]=histo_writer->create_1d(pre+"e_pt_fine"+id,"p_{t} electrons "+pre,50,0.,25., "p_{t} [GeV]");
	histos1d[pre+"e_eta"+id]=histo_writer->create_1d(pre+"e_eta"+id,"#eta electrons "+pre,netaBins,etaBins, "#eta");

	histos1d[pre+"e_number"+id]=histo_writer->create_1d(pre+"e_number"+id,"muon multiplicity "+pre,10,-0.5,9.5, "Electron Multiplicity");

}

void truicear::BasicElectronPlots::plot(std::vector<mor::Electron> *reco_electrons, std::vector<mor::Jet> *jets, mor::EventInformation *event_information)
{
	for(std::vector<mor::Electron>::iterator reco_electron = reco_electrons->begin();
		reco_electron != reco_electrons->end();
		++reco_electron){
		histos1d[pre+"e_pt"+id]->Fill(reco_electron->pt());
		histos1d[pre+"e_pt_fine"+id]->Fill(reco_electron->pt());
		histos1d[pre+"e_eta"+id]->Fill(reco_electron->eta());
		histos1d[pre+"e_phi"+id]->Fill(reco_electron->phi());
	}

	if(reco_electrons->size() > 0) histos1d[pre+"e_number"+id]->Fill(reco_electrons->size());

	//Gap region 1.460 < |eta| < 1.558 
}
