#include "../../interface/TopTrigger/TriggerStudyManager.h"

truicear::TriggerStudyManager::TriggerStudyManager(std::string ident)
{
	id = ident;

	jets=NULL;
	isolated_muons = NULL;
	isolated_electrons = NULL;
	corrected_mets = NULL;

	trigger_study_type = -1;
	min_hlt_pt = NULL;
	min_hlt_eta = NULL;
	max_hlt_eta = NULL;
	max_hlt_dR = NULL;
	max_hlt_dPt = NULL;
	max_hlt_dEta = NULL;
	max_L1_dR = NULL;
	max_L1_dPt = NULL;
	max_L1_dEta = NULL;
	max_HLTL1_dR = NULL;
	max_HLTL1_dPt = NULL;
	max_HLTL1_dEta = NULL;
	hlt_lep_triggers = NULL;

	nlep_found = 0;

	do_sync = false;

	sync_ex = new truicear::TopComSyncEx(ident);

	all_basic_jet_plots = new truicear::BasicJetPlots;
	all_basic_mu_plots = new truicear::BasicMuonPlots;
	all_basic_e_plots = new truicear::BasicElectronPlots;
}

truicear::TriggerStudyManager::~TriggerStudyManager()
{
	for(std::vector<truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>* >::iterator electron_trigger_study = electron_trigger_studies.begin();
		electron_trigger_study != electron_trigger_studies.end();
		++electron_trigger_study){
		delete (*electron_trigger_study);
	}
	for(std::vector<truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>* >::iterator muon_trigger_study = muon_trigger_studies.begin();
		muon_trigger_study != muon_trigger_studies.end();
		++muon_trigger_study){
		delete (*muon_trigger_study);
	}

	if(sync_ex){
		delete sync_ex;
		sync_ex = NULL;
	}

 	if(all_basic_jet_plots != NULL){
 		delete all_basic_jet_plots;
 		all_basic_jet_plots = NULL;	
 	}

 	if(all_basic_mu_plots != NULL){
 		delete all_basic_mu_plots;
 		all_basic_mu_plots = NULL;	
 	}

 	if(all_basic_e_plots != NULL){
 		delete all_basic_e_plots;
 		all_basic_e_plots= NULL;
 	}

	// delete the matching criteria
	for(std::vector<std::map<std::string, truicear::MatchingCriteria*>* >::iterator matching_crit_map = matching_criteria_to_delete.begin();
		matching_crit_map != matching_criteria_to_delete.end();
		++matching_crit_map){
		for(std::map<std::string, truicear::MatchingCriteria*>::iterator matching_criterion = (*matching_crit_map)->begin();
			matching_criterion != (*matching_crit_map)->end();
			++matching_criterion){
			delete matching_criterion->second;
		}
		delete *matching_crit_map;
	}
}

void truicear::TriggerStudyManager::set_handles(std::vector<mor::Jet> *jets,
						std::vector<mor::Electron> *electrons,
						std::vector<mor::Muon> *muons,			
						std::vector<mor::MET> *mets,
						mor::EventInformation *event_information,
						std::vector<mor::TriggerObject> *trigger_objects,
						mor::Trigger *HLTR)
{
        isolated_electrons = electrons;
        isolated_muons = muons;
        this->jets = jets;
        corrected_mets = mets;
	this->event_information = event_information;
	this->trigger_objects = trigger_objects;
	this->HLTR = HLTR;

	sync_ex->set_handles(event_information, isolated_muons);
	for(std::vector<truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>* >::iterator electron_trigger_study = electron_trigger_studies.begin();
		electron_trigger_study != electron_trigger_studies.end();
		++electron_trigger_study){
		(*electron_trigger_study)->set_handles(isolated_electrons, trigger_objects, jets, HLTR, event_information);
	}
	for(std::vector<truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>* >::iterator muon_trigger_study = muon_trigger_studies.begin();
		muon_trigger_study != muon_trigger_studies.end();
		++muon_trigger_study){
		(*muon_trigger_study)->set_handles(isolated_muons, trigger_objects, jets, HLTR, event_information);
	}
}

void truicear::TriggerStudyManager::set_histo_writer(HistoWriter *histo_writer)
{
	this->histo_writer = histo_writer;

	book_plot_objects();
}

void truicear::TriggerStudyManager::set_trigger_study_cuts(broc::CutsSet* cuts_set)
{
	hlt_lep_triggers = cuts_set->get_vcut_value("hlt_lep_triggers");
	min_hlt_pt = cuts_set->get_vcut_value("min_hlt_pt");
	max_hlt_eta = cuts_set->get_vcut_value("max_hlt_eta");
	min_hlt_eta = cuts_set->get_vcut_value("min_hlt_eta");
	max_hlt_dR = cuts_set->get_vcut_value("max_hlt_dR");
	max_hlt_dPt = cuts_set->get_vcut_value("max_hlt_dPt");
	max_hlt_dEta = cuts_set->get_vcut_value("max_hlt_dEta");
	max_L1_dR = cuts_set->get_vcut_value("max_L1_dR");
	max_L1_dPt = cuts_set->get_vcut_value("max_L1_dPt");
	max_L1_dEta = cuts_set->get_vcut_value("max_L1_dEta");
	max_HLTL1_dR = cuts_set->get_vcut_value("max_HLTL1_dR");
	max_HLTL1_dPt = cuts_set->get_vcut_value("max_HLTL1_dPt");
	max_HLTL1_dEta = cuts_set->get_vcut_value("max_HLTL1_dEta");
	trigger_study_type = cuts_set->get_cut_value("trigger_study_type");

	eire::TriggerNameProvider trig_name_prov;

	unsigned int ntrig = 0;
	for(std::vector<double>::iterator trig = hlt_lep_triggers->begin();trig != hlt_lep_triggers->end();trig++){
		// collect all matching criteria for this trigger
		std::map<std::string, truicear::MatchingCriteria*> *matching_criteria = new std::map<std::string, truicear::MatchingCriteria*>();
		matching_criteria_to_delete.push_back(matching_criteria);
		// hlt matching criteria
		truicear::MatchingCriteria *hlt_matching_criteria = new truicear::MatchingCriteria();
		if(max_hlt_dR->size() > ntrig) hlt_matching_criteria->set_maxdR((*max_hlt_dR)[ntrig]);
		if(max_hlt_dEta->size() > ntrig) hlt_matching_criteria->set_maxdEta((*max_hlt_dEta)[ntrig]);
		if(max_hlt_dPt->size() > ntrig) hlt_matching_criteria->set_maxdPt((*max_hlt_dPt)[ntrig]);
		if(min_hlt_pt->size() > ntrig) hlt_matching_criteria->set_minPt((*min_hlt_pt)[ntrig]);
		if(max_hlt_eta->size() > ntrig) hlt_matching_criteria->set_maxEta((*max_hlt_eta)[ntrig]);
		if(min_hlt_eta->size() > ntrig) hlt_matching_criteria->set_minEta((*min_hlt_eta)[ntrig]);

		if(max_hlt_dR->size() > ntrig && !(max_hlt_dPt->size() > ntrig)) hlt_matching_criteria->set_matching_method(0);	// dR matching
		else if(max_hlt_dR->size() > ntrig && max_hlt_dPt->size() > ntrig) hlt_matching_criteria->set_matching_method(2);	// dR matching with dPt

		// FIXME: clean this up, the trigger level id should not be hardcoded
		// 	  but therefore all the cuts should be unified
		(*matching_criteria)["HLT"] = hlt_matching_criteria;

		// l1 matching criteria
		truicear::MatchingCriteria *l1_matching_criteria = new truicear::MatchingCriteria();
		if(max_L1_dR->size() > ntrig) l1_matching_criteria->set_maxdR((*max_L1_dR)[ntrig]);
		if(max_L1_dEta->size() > ntrig) l1_matching_criteria->set_maxdEta((*max_L1_dEta)[ntrig]);
		if(max_L1_dPt->size() > ntrig) l1_matching_criteria->set_maxdPt((*max_L1_dPt)[ntrig]);
		if(max_L1_dEta->size() > ntrig)l1_matching_criteria->set_matching_method(1);	// dEta matching
		else if(max_L1_dR->size() > ntrig) l1_matching_criteria->set_matching_method(3);
		else if(max_L1_dR->size() > ntrig && max_L1_dPt->size() > ntrig) l1_matching_criteria->set_matching_method(4);

		(*matching_criteria)["L1Seed"] = l1_matching_criteria;

		// hlt-vs-l1 matching criteria
		truicear::MatchingCriteria *hlt_l1_matching_criteria = new truicear::MatchingCriteria();
		if(max_HLTL1_dR->size() > ntrig) hlt_l1_matching_criteria->set_maxdR((*max_HLTL1_dR)[ntrig]);
		if(max_HLTL1_dEta->size() > ntrig) hlt_l1_matching_criteria->set_maxdEta((*max_HLTL1_dEta)[ntrig]);
		if(max_HLTL1_dPt->size() > ntrig) hlt_l1_matching_criteria->set_maxdPt((*max_HLTL1_dPt)[ntrig]);
		if(max_HLTL1_dR->size() > ntrig && !(max_HLTL1_dPt->size() > ntrig))hlt_l1_matching_criteria->set_matching_method(0);	// dR matching
		else if(max_HLTL1_dR->size() > ntrig && max_HLTL1_dPt->size() > ntrig)hlt_l1_matching_criteria->set_matching_method(2);	// dR,dPt matching

		//WARNING: If using this first make sure l1seed name is being set correctly
		(*matching_criteria)["HLTvsL1Seed"] = hlt_l1_matching_criteria;

		// prepare TriggerStudy for the current trigger
		// FIXME: clean this up
		// if trigger is an electron trigger
		if(*trig < 77){
			truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots> *trigger_study = new truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>(id, trig_name_prov.hlt_name(*trig),trig_name_prov.hlt_name(*trig));
			trigger_study->set_matching_criteria(matching_criteria);
			trigger_study->set_handles(isolated_electrons, trigger_objects, jets, HLTR, event_information);
			trigger_study->set_histo_writer(histo_writer);
			trigger_study->initialise_trigger_levels();
			electron_trigger_studies.push_back(trigger_study);
		}else{ // trigger is a muon trigger
			truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots> *trigger_study = new truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>(id, trig_name_prov.hlt_name(*trig),trig_name_prov.hlt_name(*trig));
			trigger_study->set_matching_criteria(matching_criteria);
			trigger_study->set_handles(isolated_muons, trigger_objects, jets, HLTR, event_information);
			trigger_study->set_histo_writer(histo_writer);
			trigger_study->initialise_trigger_levels();
			muon_trigger_studies.push_back(trigger_study);
		}

		++ntrig;
	}
}

void truicear::TriggerStudyManager::book_plot_objects()
{
	//Book plots for all events passing cuts. Then additional plots for each trigger defined, then for each match type.
	std::string all_reco_id = "All.";
	all_basic_jet_plots->set_id(id, all_reco_id);
	all_basic_jet_plots->set_histo_writer(histo_writer);

	all_basic_mu_plots->set_id(id, all_reco_id);
	all_basic_mu_plots->set_histo_writer(histo_writer);

	all_basic_e_plots->set_id(id, all_reco_id);
	all_basic_e_plots->set_histo_writer(histo_writer);
}
  
void truicear::TriggerStudyManager::study_triggers()
{
	if(trigger_objects == NULL) 
		std::cout <<"ERROR: TSG.cc No trigger objects" << std::endl;

	//First fill the histograms for all leptons which pass the reco cuts applied.
	all_basic_e_plots->plot(isolated_electrons, jets, event_information);
	all_basic_mu_plots->plot(isolated_muons, jets, event_information);
	all_basic_jet_plots->plot(jets);

	for(std::vector<truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>* >::iterator electron_trigger_study = electron_trigger_studies.begin();
		electron_trigger_study != electron_trigger_studies.end();
		++electron_trigger_study){
		(*electron_trigger_study)->plot();
	}
	for(std::vector<truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>* >::iterator muon_trigger_study = muon_trigger_studies.begin();
		muon_trigger_study != muon_trigger_studies.end();
		++muon_trigger_study){
		(*muon_trigger_study)->plot();
	}
}
