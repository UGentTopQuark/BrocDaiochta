#include "../../interface/TopTrigger/TriggerStudy.h"

template void truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::set_histo_writer(HistoWriter *histo_writer);
template void truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::set_matching_criteria(std::map<std::string, truicear::MatchingCriteria*> *matching_criteria);
template truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::TriggerStudy(std::string id, std::string trigger_name, std::string l1seed_name);
template truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::~TriggerStudy();
template void truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::set_handles(std::vector<mor::Muon> *reco_leptons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<mor::Jet> *jets, mor::Trigger *HLTR, mor::EventInformation *event_information);
template void truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::plot();
template void truicear::TriggerStudy<mor::Muon, truicear::BasicMuonPlots, truicear::MatchedMuonPlots>::initialise_trigger_levels();

template void truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::set_histo_writer(HistoWriter *histo_writer);
template void truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::set_matching_criteria(std::map<std::string, truicear::MatchingCriteria*> *matching_criteria);
template truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::TriggerStudy(std::string id, std::string trigger_name,std::string l1seed_name);
template truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::~TriggerStudy();
template void truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::set_handles(std::vector<mor::Electron> *reco_leptons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<mor::Jet> *jets, mor::Trigger *HLTR, mor::EventInformation *event_information);
template void truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::plot();
template void truicear::TriggerStudy<mor::Electron, truicear::BasicElectronPlots, truicear::MatchedElectronPlots>::initialise_trigger_levels();

template <typename LeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
truicear::TriggerStudy<LeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::TriggerStudy(std::string id, std::string trigger_name, std::string l1seed_name)
{
	this->id = id;
	this->trigger_name = trigger_name;
	this->l1seed_name = l1seed_name;
	hlt_vs_l1_study = NULL;
	all_l1_trig_obj_plots = new BasicLeptonPlotsType();
	// FIXME: this needs to be set to a reasonable id for data run combiner
	std::string extra_id = "_HLTvsL1Seed";
	all_l1_trig_obj_plots->set_id(id, "All."+trigger_name+extra_id+"_");
	all_jet_l1_plots = new truicear::BasicJetPlots();
	all_jet_l1_plots->set_id(id, "All."+trigger_name+extra_id+"_");
	l1_matched_reco = NULL;
	event_information = NULL;

	jets = NULL;
	HLTR = NULL;
	reco_leptons = NULL;
	trigger_objects = NULL;
}

template <typename LeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
truicear::TriggerStudy<LeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::~TriggerStudy()
{
	std::cout << "#### Lepton Info: " << id << "  ####" << std::endl;
	std::cout << "nlep_found: " << nlep_found << std::endl;
	for(typename std::vector<truicear::TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType> *>::iterator level = trigger_level_studies.begin();
		level != trigger_level_studies.end();
		++level){
		std::cout << "--------------" << std::endl;
		delete (*level);
	}
	if(hlt_vs_l1_study){
		std::cout << "--------------" << std::endl;
		delete hlt_vs_l1_study;
		hlt_vs_l1_study = NULL;
	}
	std::cout << "##############################################" << std::endl;

	if(all_l1_trig_obj_plots){ delete all_l1_trig_obj_plots; all_l1_trig_obj_plots = NULL; }
	if(all_jet_l1_plots){ delete all_jet_l1_plots; all_jet_l1_plots = NULL; }
	if(l1_matched_reco){ delete l1_matched_reco; l1_matched_reco = NULL; }
}

template <typename LeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerStudy<LeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::set_histo_writer(HistoWriter *histo_writer)
{
	this->histo_writer = histo_writer;
	all_l1_trig_obj_plots->set_histo_writer(histo_writer);
	all_jet_l1_plots->set_histo_writer(histo_writer);
}

template <typename LeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerStudy<LeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::set_handles(std::vector<LeptonType> *reco_leptons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<mor::Jet> *jets, mor::Trigger *HLTR, mor::EventInformation *event_information)
{
	this->reco_leptons = reco_leptons;
	this->trigger_objects = trigger_objects;
	this->jets = jets;
	this->HLTR = HLTR;
	this->event_information = event_information;
	for(typename std::vector<truicear::TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType> *>::iterator level = trigger_level_studies.begin();
		level != trigger_level_studies.end();
		++level){
		(*level)->set_handles(reco_leptons, trigger_objects, jets, event_information);
	}
}

template <typename LeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerStudy<LeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::initialise_trigger_levels()
{
	truicear::TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType> *hlt_study = NULL;
	truicear::TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType> *l1_study = NULL;
	// process all trigger levels (hlt, l1, ...)
	for(std::map<std::string, truicear::MatchingCriteria*>::iterator trigger_level = matching_criteria->begin();
		trigger_level != matching_criteria->end();
		++trigger_level){

		// hlt vs l1 down below treated separately
		if(trigger_level->first == "HLTvsL1Seed") continue;

		// book trigger level
		TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType> *level = new TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType>(id, trigger_name, trigger_level->first, l1seed_name);
		level->set_matching_criteria(trigger_level->second);
		level->set_handles(reco_leptons, trigger_objects, jets, event_information);
		level->set_histo_writer(histo_writer);

		// add trigger level to the list of levels
		trigger_level_studies.push_back(level);
		if(trigger_level->first == "L1Seed") l1_study = level;
		if(trigger_level->first == "HLT") hlt_study = level;
	}

	// l1 vs hlt matching
	// check if l1 and hlt level exist
	if(matching_criteria->find("HLT") != matching_criteria->end() &&
		matching_criteria->find("L1Seed") != matching_criteria->end() &&
		matching_criteria->find("HLTvsL1Seed") != matching_criteria->end()){
		// get matches reco-hlt
		std::vector<mor::TriggerObject> *reco_matched_hlt_objs = hlt_study->get_matched_trigger_objects();
		// get matches reco-l1
		l1_matched_reco = l1_study->get_matched_reco_objects();
		// create new TriggerLevelStudy
		hlt_vs_l1_study = new TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType>(id, trigger_name, "HLTvsL1Seed",l1seed_name);
		// initialise matching
		hlt_vs_l1_study->set_matching_criteria((*matching_criteria)["HLTvsL1Seed"]);
		// set handles (l1 objects, hlt objects)
		hlt_vs_l1_study->set_handles(l1_matched_reco, reco_matched_hlt_objs, jets, event_information);
		// set histo writer
		hlt_vs_l1_study->set_histo_writer(histo_writer);
	}
}

template <typename LeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerStudy<LeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::plot()
{
	if(HLTR->trigger_available(trigger_name)){
		// plot all trigger level studies
		for(typename std::vector<truicear::TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType> *>::iterator level = trigger_level_studies.begin();
			level != trigger_level_studies.end();
			++level){
			(*level)->plot();
		}
		if(hlt_vs_l1_study){
			hlt_vs_l1_study->plot();
			all_l1_trig_obj_plots->plot(l1_matched_reco, jets, event_information);
			if(l1_matched_reco->size() > 0) all_jet_l1_plots->plot(jets);
		}
		nlep_found += reco_leptons->size();

	}
}

template <typename LeptonType, typename BasicLeptonPlotsType, typename MatchedLeptonPlotsType>
void truicear::TriggerStudy<LeptonType, BasicLeptonPlotsType, MatchedLeptonPlotsType>::set_matching_criteria(std::map<std::string, truicear::MatchingCriteria*> *matching_criteria)
{
	this->matching_criteria = matching_criteria;
	nlep_found = 0;
}
