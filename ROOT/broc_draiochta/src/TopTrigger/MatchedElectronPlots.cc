#include "../../interface/TopTrigger/MatchedElectronPlots.h"

truicear::MatchedElectronPlots::MatchedElectronPlots()
{
}

truicear::MatchedElectronPlots::~MatchedElectronPlots()
{
}

void truicear::MatchedElectronPlots::book_histos()
{
	histos1d[pre+"testpt"+id]=histo_writer->create_1d(pre+"testpt"+id,"p_{t} electrons "+pre,60,0,100, "p_{t} [GeV]");
}

void truicear::MatchedElectronPlots::set_trigger_matcher(truicear::TriggerMatcher<mor::Electron, mor::TriggerObject> *trigger_matcher)
{
	this->trigger_matcher = trigger_matcher;
}

void truicear::MatchedElectronPlots::plot(std::vector<mor::Electron> *reco_electrons, std::vector<mor::TriggerObject> *trigger_objects,std::vector<std::pair<int, int> >* match_results)
{
	//histos1d[pre+"testpt"+id]->Fill(reco_electron->pt());	
}
