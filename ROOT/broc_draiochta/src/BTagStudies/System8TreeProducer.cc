#include "../../interface/BTagStudies/System8TreeProducer.h"

bclib::System8TreeProducer::System8TreeProducer(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{
	outfile = NULL;	
	this->handle_holder = handle_holder;
	
	//The mu jet is the probe
	pt = -100;
	eta = -100;
        phi = -100;
	TCHE = -100;
	mu_pt_rel = -100;
	mu_dR = -100;

	away_jet_pt = -100;
	away_jet_eta = -100;
	away_jet_TCHE = -100;
	away_jet_mu_pt_rel = -100;
	away_jet_mu_dR = -100;

	dR_mu_jet_away_jet = -100;
	njets = -100;
	

}

bclib::System8TreeProducer::~System8TreeProducer()
{
	if(outfile){
		outfile->cd();
		if(directory_name != "") outfile->Cd(directory_name.c_str());
		tree->Write();
	}
	if(tree){
		delete tree;
		tree = NULL;
	}
}

void bclib::System8TreeProducer::set_handles()
{
	this->muons = handle_holder->get_tight_muons();
	this->jets = handle_holder->get_tight_jets();
}
	
void bclib::System8TreeProducer::book_branches()
{
	tree->Branch("pt",&pt, "pt/F");
	tree->Branch("eta",&eta, "eta/F");
	tree->Branch("phi",&phi, "phi/F");
	tree->Branch("TCHE",&TCHE, "TCHE/F");
	tree->Branch("mu_pt_rel",&mu_pt_rel, "mu_pt_rel/F");
	tree->Branch("mu_dR",&mu_dR, "mu_dR/F");

	tree->Branch("away_jet_pt",&away_jet_pt, "away_jet_pt/F");
	tree->Branch("away_jet_eta",&away_jet_eta, "away_jet_eta/F");
	tree->Branch("away_jet_phi",&away_jet_phi, "away_jet_phi/F");
	tree->Branch("away_jet_TCHE",&away_jet_TCHE, "away_jet_TCHE/F");
	tree->Branch("away_jet_mu_pt_rel",&away_jet_mu_pt_rel, "away_jet_mu_pt_rel/F");
	tree->Branch("away_jet_mu_dR",&away_jet_mu_dR, "away_jet_mu_dR/F");

	tree->Branch("dR_mu_jet_away_jet",&dR_mu_jet_away_jet, "dR_mu_jet_away_jet/F");
	tree->Branch("njets",&njets, "njets/F");
}

void bclib::System8TreeProducer::set_outfile(TFile *ntuple_file, std::string directory_name)
{
        this->outfile = ntuple_file;
	this->directory_name = directory_name;
}

void bclib::System8TreeProducer::create_tree(std::string tree_name)
{
        if(outfile){
		outfile->cd();
		if(directory_name != "") outfile->Cd(directory_name.c_str());
		tree = new TTree(tree_name.c_str(),tree_name.c_str());
	}else{
		std::cerr << "ERROR: bclib::System8TreeProducer::create_tree(): no outfile set" << std::endl;
	}
}
	
void bclib::System8TreeProducer::fill_branches()
{	
	//Take the highest pt jet with a mu within dR 0.4 as our mu jet (probe)
	//The other jet is our away jet. See CMS AN 2007/046
	int mu_jet_id = -1,mu_jet_mu_id = -1;
	double min_mu_jet_mu_dR = -1;
	for(size_t i = 0; i < jets->size(); i++){

		int closest_mu = -1;
		double min_dR = -1;
		for(size_t j = 0; j < muons->size(); j++){
			double dR = ROOT::Math::VectorUtil::DeltaR((*muons)[j].p4(),(*jets)[i].p4());
			if(min_dR == -1 || dR < min_dR){
				min_dR = dR;
				closest_mu = j;
			}
		}
		
		if(min_dR != -1 && min_dR < 0.4){
			mu_jet_id = i;
			mu_jet_mu_id = closest_mu;
			min_mu_jet_mu_dR = min_dR;
			break;
		}
	}

	if(mu_jet_id == -1)
		return;

	//Now we want to find the away jet. It should be opposite so for now
	//we find the jet with the max dR to the mu jet. If the jet pt cut is removed 
	//this may not be suitable. Also may want a cut on this dR in external program.
	int away_jet_id = -1;
	double max_jets_dR = -1;
	for(size_t i = 0; i < jets->size(); i++){
		if(i == mu_jet_id)
			continue;

		double dR = ROOT::Math::VectorUtil::DeltaR((*jets)[mu_jet_id].p4(),(*jets)[i].p4());

		if(max_jets_dR == -1 || dR > max_jets_dR){
			max_jets_dR = dR;
			away_jet_id = i;
		}
	}	

	if(away_jet_id == -1)
		return;

	//Find closest muon to away jet
	int away_jet_mu_id = -1;
	double min_away_jet_mu_dR = -1;
	for(size_t j = 0; j < muons->size(); j++){
		double dR = ROOT::Math::VectorUtil::DeltaR((*muons)[j].p4(),(*jets)[away_jet_id].p4());
		if(min_away_jet_mu_dR == -1 || dR < min_away_jet_mu_dR){
			min_away_jet_mu_dR = dR;
			away_jet_mu_id = j;
		}
	}
	


	pt = (*jets)[mu_jet_id].pt();
	eta = (*jets)[mu_jet_id].eta();
	phi = (*jets)[mu_jet_id].phi();
	TCHE = (*jets)[mu_jet_id].bDiscriminator("trackCountingHighEffBJetTags");
	mu_pt_rel = (*muons)[mu_jet_mu_id].pt()/(*jets)[mu_jet_id].pt();
	mu_dR = min_mu_jet_mu_dR ;
	
	away_jet_pt = (*jets)[away_jet_id].pt();;
	away_jet_eta = (*jets)[away_jet_id].eta();
	away_jet_phi = (*jets)[away_jet_id].phi();
	away_jet_TCHE = (*jets)[away_jet_id].bDiscriminator("trackCountingHighEffBJetTags");
	if(away_jet_mu_id != -1){
		away_jet_mu_pt_rel = (*muons)[away_jet_mu_id].pt()/(*jets)[away_jet_id].pt();
		away_jet_mu_dR = min_away_jet_mu_dR;
	}
	
	dR_mu_jet_away_jet = max_jets_dR;
	njets = jets->size();


        tree->Fill();

}
