#include "../../interface/HitFit/HitFitManager.h"


hitfit::HitFitManager::HitFitManager(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder)
{


  eire::ConfigReader *config_reader = handle_holder->get_config_reader();
  std::string output_dir = config_reader->get_var("output_directory");
  std::string suffix = config_reader->get_var("outfile_suffix");
  hitfit_producer = NULL;
  ntuple_file = NULL;
  this->handle_holder = handle_holder;

  chisq_cut = atof(config_reader->get_var("chisq_cut","hitfit",false).c_str());//check which config file
  correct_FSR = config_reader->get_bool_var("correct_FSR","hitfit",false);

  std::string filename = output_dir+"/"+"HitFit_"+handle_holder->get_ident()+suffix+".root";
  while(filename.find("|") != std::string::npos)
	  filename.replace(filename.find("|"),1,"_");
  
  std::string directory_name = "HitFit";
  hitfit_producer = new hitfit::HitFitProducer(handle_holder);	  
  
  ntuple_file = new TFile(filename.c_str(), "RECREATE");
  if(directory_name != ""){
	  ntuple_file->mkdir(directory_name.c_str());
	  ntuple_file->Cd(directory_name.c_str());
  }

  
  hitfit_producer->set_outfile(ntuple_file, directory_name);
  hitfit_producer->create_tree("HitFitResults");
  hitfit_producer->set_handles(handle_holder); 
  hitfit_producer->book_branches();

}

hitfit::HitFitManager::~HitFitManager()
{
        if(hitfit_producer){ delete hitfit_producer; hitfit_producer = NULL; }

        if(ntuple_file){
	  ntuple_file->Write();
	  ntuple_file->Close();
	  delete ntuple_file;
	  ntuple_file = NULL;
        }

}

void hitfit::HitFitManager::fill_trees()
{
	hitfit_producer->fill_branches();
	std::vector<int> combination = hitfit_producer->get_hitfit_combination();
	std::vector<mor::Particle> *jets = hitfit_producer->get_hitfit_jets();
	std::vector<mor::Particle> *muons = hitfit_producer->get_hitfit_muons();
	std::vector<mor::Particle> *electrons = hitfit_producer->get_hitfit_electrons();
	std::vector<mor::Particle> *mets = hitfit_producer->get_hitfit_mets();
	double chisq = hitfit_producer->get_hitfit_Chisq();

	std::vector<mor::Jet> *jets_orig = handle_holder->get_tight_jets();
	//determine which jet is the spare jet                                                                                                                                                         
	int index_jet_tobe_combined = -1;
	//check if the four leading jets are all in the combination, if not the remaining one is the spare jet                                                                                        
           	for(int i = 0; i < 4; i++){
	  if(i != (combination)[i]){
	    index_jet_tobe_combined = i;
	  }
	}

	//if the four leading jets are all in the combination, and if there are over 4 jets, then take the 5th as spare jet                                                                                        
	if(index_jet_tobe_combined == -1 && jets_orig->size() > 4){
	  index_jet_tobe_combined = 4;
	}


	double chisq_FSR = -1;
	std::vector<mor::Particle> *FSR_jets;
	std::vector<mor::Particle> *FSR_muons;
	std::vector<mor::Particle> *FSR_electrons;
	std::vector<mor::Particle> *FSR_mets;
	std::vector<int> FSR_combination;
	if(correct_FSR)
	  {
	    double chi = -1;
	    //combine with q
	    hitfit_producer->combine_jet(index_jet_tobe_combined,(combination)[0]);
	    hitfit_producer->fill_branches();
	    chi = hitfit_producer->get_hitfit_Chisq();
	    if(chi != -1 && (chi < chisq || chi < chisq_FSR)){
	      chisq_FSR = chi;
	      FSR_jets = hitfit_producer->get_hitfit_jets();
	      FSR_muons = hitfit_producer->get_hitfit_muons();
	      FSR_electrons = hitfit_producer->get_hitfit_electrons();
	      FSR_mets = hitfit_producer->get_hitfit_mets();
	      FSR_combination = hitfit_producer->get_hitfit_combination();
	    }
	    //combine with qbar
	    hitfit_producer->combine_jet(index_jet_tobe_combined,(combination)[1]);
	    hitfit_producer->fill_branches();
	    chi = hitfit_producer->get_hitfit_Chisq();
	    if(chi != -1 && (chi < chisq || chi < chisq_FSR)){
	      chisq_FSR = chi;
	      FSR_jets = hitfit_producer->get_hitfit_jets();
	      FSR_muons = hitfit_producer->get_hitfit_muons();
	      FSR_electrons = hitfit_producer->get_hitfit_electrons();
	      FSR_mets = hitfit_producer->get_hitfit_mets();
	      FSR_combination = hitfit_producer->get_hitfit_combination();
	    }
	    //combine with had_b
	    hitfit_producer->combine_jet(index_jet_tobe_combined,(combination)[2]);
	    hitfit_producer->fill_branches();
	    chi = hitfit_producer->get_hitfit_Chisq();
	    if(chi != -1 && (chi < chisq || chi < chisq_FSR)){
	      chisq_FSR = chi;
	      FSR_jets = hitfit_producer->get_hitfit_jets();
	      FSR_muons = hitfit_producer->get_hitfit_muons();
	      FSR_electrons = hitfit_producer->get_hitfit_electrons();
	      FSR_mets = hitfit_producer->get_hitfit_mets();
	      FSR_combination = hitfit_producer->get_hitfit_combination();
	    }
	    //combine with lep_b
	    hitfit_producer->combine_jet(index_jet_tobe_combined,(combination)[3]);
	    hitfit_producer->fill_branches();
	    chi = hitfit_producer->get_hitfit_Chisq();
	    if(chi != -1 && (chi < chisq || chi < chisq_FSR)){
	      chisq_FSR = chi;
	      FSR_jets = hitfit_producer->get_hitfit_jets();
	      FSR_muons = hitfit_producer->get_hitfit_muons();
	      FSR_electrons = hitfit_producer->get_hitfit_electrons();
	      FSR_mets = hitfit_producer->get_hitfit_mets();
	      FSR_combination = hitfit_producer->get_hitfit_combination();
	    }
	    if(chisq_FSR != -1 && chisq_FSR < chisq){
	      std::cout<<"FSR corrected is better"<<std::endl;
	      hitfit_producer->set_hitfit_combination(FSR_combination);
	      hitfit_producer->set_hitfit_jets(FSR_jets);
	      hitfit_producer->set_hitfit_muons(FSR_muons);
	      hitfit_producer->set_hitfit_electrons(FSR_electrons);
	      hitfit_producer->set_hitfit_mets(FSR_mets);
	    }
	    else{
	      hitfit_producer->set_hitfit_combination(combination);
	      hitfit_producer->set_hitfit_jets(jets);
	      hitfit_producer->set_hitfit_muons(muons);
	      hitfit_producer->set_hitfit_electrons(electrons);
	      hitfit_producer->set_hitfit_mets(mets);
	    }
	  }

}

