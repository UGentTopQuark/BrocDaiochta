//
//     $Id: MorMETHitFitTranslator.cc,v 1.7 2011/01/29 22:21:38 haryo Exp $
//

/**
    @file MorMETHitFitTranslator.cc

    @brief Specialization of template class METTranslatorBase in the
    package HitFit for mor::MET

    @author Haryo Sumowidagdo <Suharyo.Sumowidagdo@cern.ch>

    @par Created
    Sat Jun 27 17:49:32 2009 UTC

    @version $Id: MorMETHitFitTranslator.cc,v 1.7 2011/01/29 22:21:38 haryo Exp $
 */


#include "../../../interface/HitFit/Core/METTranslatorBase.hpp"
#include "../../../interface/MorObjects/MMET.h"

#include <cmath>

namespace hitfit {


template<>
METTranslatorBase<mor::MET>::METTranslatorBase()
{
	resolution_ = Resolution(std::string("0,0,12"));
} // METTranslatorBase<mor::MET>::METTranslatorBase()


template<>
METTranslatorBase<mor::MET>::METTranslatorBase(const std::string& ifile)
{
    const Defaults_Text defs(ifile);
    std::string resolution_string(defs.get_string("met_resolution"));
    resolution_ = Resolution(resolution_string);

} // METTranslatorBase<mor::MET>::METTranslatorBase(const std::string& ifile)


template<>
METTranslatorBase<mor::MET>::~METTranslatorBase()
{
} // METTranslatorBase<mor::MET>::~METTranslatorBase()


template<>
Fourvec
METTranslatorBase<mor::MET>::operator()(const mor::MET& m,
                                        bool useObjEmbRes /* = false */)
{
    double px = m.px();
    double py = m.py();

    return Fourvec (px,py,0.0,sqrt(px*px + py*py));
} // Fourvec METTranslatorBase<mor::MET>::operator()(const mor::MET& m)



template<>
Resolution
METTranslatorBase<mor::MET>::KtResolution(const mor::MET& m,
                                          bool useObjEmbRes /* = false */) const
{
    return resolution_;
} // Resolution METTranslatorBase<mor::MET>::KtResolution(const mor::MET& m)



template<>
Resolution
METTranslatorBase<mor::MET>::METResolution(const mor::MET& m,
                                           bool useObjEmbRes /* = false */) const
{
    return KtResolution(m,useObjEmbRes);
} // Resolution METTranslatorBase<mor::MET>::METResolution(const mor::MET& m)


} // namespace hitfit
