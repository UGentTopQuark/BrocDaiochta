#include "../../interface/NTupleReader/NameMappingRunner.h"

template void NameMappingRunner::assign_mapper(std::vector<std::string> *&mapper, std::string branch_name);
template void NameMappingRunner::assign_mapper(std::vector<unsigned int> *&mapper, std::string branch_name);
template void NameMappingRunner::assign_mapper(std::vector<double> *&mapper, std::string branch_name);
template void NameMappingRunner::assign_mapper(std::vector<std::vector<double> > *&mapper, std::string branch_name);
template void NameMappingRunner::assign_mapper(std::vector<std::vector<std::string> > *&mapper, std::string branch_name);

NameMappingRunner::NameMappingRunner()
{
	file_runner = NULL;
        current_file_event = 0;
        current_file_max_events = 0;
	tree = NULL;
}

NameMappingRunner::~NameMappingRunner()
{
}

void NameMappingRunner::set_file_runner(FileRunner *file_runner, std::string tree_name)
{
	this->file_runner = file_runner;
	this->tree_name = tree_name;
}

template <class beagObj>
void NameMappingRunner::assign_mapper(beagObj *&mapper, std::string branch_name)
{
        tree = file_runner->get_next_tree(tree_name.c_str(), false);
        file_runner->cd_infile();
        current_file_max_events = tree->GetEntries();
        if(verbose) std::cout << "entries in tree: " << current_file_max_events << std::endl;
        current_file_event = 0;

	tree->SetBranchAddress(branch_name.c_str(),&mapper);
}

void NameMappingRunner::next_mapping()
{
        if((current_file_event < current_file_max_events)){
		if(verbose) std::cout << "changing mapping..." << std::endl;
                tree->GetEntry(current_file_event);
                ++current_file_event;
        }else{
		std::cerr << "WARNING: NameMappingRunner::next_mapping(): no new mapping available" << std::endl;
	}
}
