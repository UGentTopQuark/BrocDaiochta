#include "../../interface/NTupleReader/Analyser.h"

template void Analyser::prepare_mor_collections<beag::TriggerObject, mor::TriggerObject>(std::vector<beag::TriggerObject> *beag_objs, std::vector<mor::TriggerObject> *mor_objs);
template void Analyser::prepare_mor_collections<beag::Jet, mor::Jet>(std::vector<beag::Jet> *beag_objs, std::vector<mor::Jet> *mor_objs);
template void Analyser::prepare_mor_collections<beag::Muon, mor::Muon>(std::vector<beag::Muon> *beag_objs, std::vector<mor::Muon> *mor_objs);
template void Analyser::prepare_mor_collections<beag::Electron, mor::Electron>(std::vector<beag::Electron> *beag_objs, std::vector<mor::Electron> *mor_objs);
template void Analyser::prepare_mor_collections<beag::MET, mor::MET>(std::vector<beag::MET> *beag_objs, std::vector<mor::MET> *mor_objs);
template void Analyser::prepare_mor_collections<beag::PrimaryVertex, mor::PrimaryVertex>(std::vector<beag::PrimaryVertex> *beag_objs, std::vector<mor::PrimaryVertex> *mor_objs);

template void Analyser::assign_lepton_id_checkers(std::vector<mor::Muon> *mor_objs, mor::IDChecker *id_checker, mor::IDChecker *trigger_checker, mor::IDChecker *pfiso_conesize_checker);
template void Analyser::assign_lepton_id_checkers(std::vector<mor::Electron> *mor_objs, mor::IDChecker *id_checker, mor::IDChecker *trigger_checker, mor::IDChecker *pfiso_conesize_checker);

Analyser::Analyser(std::string dataset_name, eire::ConfigReader *config_reader, TFile *outfile, bool process_mc)
{
	this->config_reader = config_reader;

        this->outfile = outfile;

	this->process_mc = process_mc;

	process_prescales = config_reader->get_bool_var("process_prescales", "global", false);		// do not process trigger prescales by default for the moment
	process_trigger_objects = config_reader->get_bool_var("process_trigger_objects", "global", false);		// run over collection of trigger objects
	process_gen_particles = false;		// run over collection of gen particles
	pdf_set = "cteq66";				// name of pdf-set to process (eg. cteq66)
	obj_type = -1;				// jl 14.01.11: 1=pf obj., 2=tc, 0=calo
	process_noniso_muons = config_reader->get_bool_var("process_noniso_muons","global",false);
	process_noniso_electrons = config_reader->get_bool_var("process_noniso_electrons","global",false);

	
	// taken from share/default_config.txt
	process_all_pdf_sets = config_reader->get_bool_var("pdf_weights","event_weights",false); // clone cutssets for all pdf sets and weights
	read_pdf_weights = !config_reader->get_bool_var("suppress_pdf_offset_correction","event_weights",false); // weight events for pdf uncertainties
	obj_type = atoi(config_reader->get_var("object_type","global",true).c_str());

	std::cout << "[Analyser]: process_mc = " << process_mc << " process_prescales = " << process_prescales << " process_trigger_objects = " << process_trigger_objects << " process_gen_particles " << process_gen_particles << " read_pdf_weights = " << read_pdf_weights << " process_all_pdf_sets = " << process_all_pdf_sets << " pdf_set = " << pdf_set << " obj_type = " << obj_type << std::endl;

	handle_holder = new eire::HandleHolder();

	// read in events from ROOT file
        evt_runner = new EventRunner();
	// keep track of available triggers for processed data
        trigger_name_runner = new NameMappingRunner();
	trigger_name_runner->set_file_runner(evt_runner->get_file_runner(), "trigger_name_mapping");
	// keep track of available tags like electron ID and b-tag algorithms
        tag_name_runner = new NameMappingRunner();
	tag_name_runner->set_file_runner(evt_runner->get_file_runner(), "tag_name_mapping");
        weight_runner = new NameMappingRunner();
	weight_runner->set_file_runner(evt_runner->get_file_runner(), "tree");
	// physics objects as stored in ROOT files
	beag_jets = new std::vector<beag::Jet>();
	beag_electrons = new std::vector<beag::Electron>();
	beag_muons = new std::vector<beag::Muon>();
	beag_mets = new std::vector<beag::MET>();
	beag_trigger_objects = NULL;
	if(process_trigger_objects) beag_trigger_objects = new std::vector<beag::TriggerObject>();
	beag_gen_particles = NULL;
	if(process_gen_particles) beag_gen_particles = new std::vector<beag::Particle>();
	// information if event passed trigger
	beag_triggers = new std::vector<beag::Trigger>();
	// primary vertices
	beag_pvertices = new std::vector<beag::PrimaryVertex>();
	beag_event_information = new std::vector<beag::EventInformation>();
	trigger_names = new std::vector<std::string>();
	trigger_module_names = new std::vector<std::vector<std::string> >();
	if(process_prescales){
		l1_prescales = new std::vector<unsigned int>();
		hlt_prescales = new std::vector<unsigned int>();
	}
	if(read_pdf_weights) pdf_evt_weights = new std::vector<std::vector<double> >();
	btag_names = new std::vector<std::string>();
	// store isolation for different consizes
        pfiso_conesize_names = new std::vector<std::string>();
	eID_names = new std::vector<std::string>();
	muID_names = new std::vector<std::string>();
	pdf_names = new std::vector<std::string>();

	// nonisolated lepton collections (disabling PF2PAT jet reclustering)
	if(process_noniso_muons){
		std::cout << "processing noniso muons" << std::endl;
		beag_nomu_jets = new std::vector<beag::Jet>();
		beag_noniso_muons = new std::vector<beag::Muon>();
		uncorrected_nomu_jets = new std::vector<mor::Jet>();
		nomu_jets = new std::vector<mor::Jet>();
		noniso_muons = new std::vector<mor::Muon>();
	}else{
		beag_nomu_jets = 0;
		beag_noniso_muons = 0;
		uncorrected_nomu_jets = 0;
		nomu_jets = 0;
		noniso_muons = 0;
	}

	if(process_noniso_electrons){
		std::cout << "processing noniso electrons" << std::endl;
		beag_noe_jets = new std::vector<beag::Jet>();
		beag_noniso_electrons = new std::vector<beag::Electron>();
		uncorrected_noe_jets = new std::vector<mor::Jet>();
		noe_jets = new std::vector<mor::Jet>();
		noniso_electrons = new std::vector<mor::Electron>();
	}else{
		beag_noe_jets = 0;
		beag_noniso_electrons = 0;
		uncorrected_noe_jets = 0;
		noe_jets = 0;
		noniso_electrons = 0;
	}

	uncorrected_jets = new std::vector<mor::Jet>();	// no jet corrections
	jets = new std::vector<mor::Jet>();		// with jet corrections
	electrons = new std::vector<mor::Electron>();
	muons = new std::vector<mor::Muon>();
	mets = new std::vector<mor::MET>();
	pvertices = new std::vector<mor::PrimaryVertex>();
	trigger_objects = NULL;
	if(process_trigger_objects) trigger_objects = new std::vector<mor::TriggerObject>();
	gen_particles = NULL;
	if(process_gen_particles) gen_particles = new std::vector<mor::Particle>();

	// check if lepton/event passes ID cut/trigger/etc
	eID_checker = new mor::IDChecker();
	muID_checker = new mor::IDChecker();
	btag_checker = new mor::IDChecker();
	pfiso_conesize_checker = new mor::IDChecker();
	trigger_checker = new mor::IDChecker();
	l1seed_checker = new mor::IDChecker();
	pdf_checker = new mor::IDChecker();

	// trigger prescale information
	prescale_prov = new broc::PrescaleProvider();
	prescale_prov->set_trigger_names(trigger_names);
	prescale_prov->set_prescales(l1_prescales, hlt_prescales);

	// trigger objects, optional
	if(process_trigger_objects){
		trigger_module_manager = new broc::TriggerModuleManager();
		handle_holder->set_trigger_module_manager(trigger_module_manager);
	}else{
		handle_holder->set_trigger_module_manager(NULL);
	}

	// parton distribution function reweighting
	pdf_weight_prov = new eire::PDFWeightProvider();
	pdf_weight_prov->set_pdf_set_provider(pdf_checker);
	pdf_weight_prov->set_pdf_evt_weights(pdf_evt_weights);
	handle_holder->set_pdf_weight_provider(pdf_weight_prov);

	beag_gen_evts = NULL;
	beag_triggers = NULL;
	gen_evt = NULL;

	trigger = new mor::Trigger();

	// event information (run number, event number, etc)
	event_information = new mor::EventInformation();

	// apply certified JSON file
	run_selector = new RunSelector();
	run_selector->set_event_information(event_information);

	identifier = dataset_name;

	// automate storage and writing of histograms to output file
	histo_writer = new HistoWriter();
	histo_writer->set_outfile(outfile);

	// jet energy corrections
	jet_corrector = new eire::JetCorrectionService();
	jet_corrector->initialise(config_reader);

	handle_holder->set_jets(jets);
	handle_holder->set_noe_jets(noe_jets);
	handle_holder->set_nomu_jets(nomu_jets);
	handle_holder->set_electrons(electrons);
	handle_holder->set_noniso_electrons(noniso_electrons);
	handle_holder->set_mets(mets);
	handle_holder->set_muons(muons); 
	handle_holder->set_noniso_muons(noniso_muons); 
	handle_holder->set_histo_writer(histo_writer);
	handle_holder->set_primary_vertices(pvertices);
	handle_holder->set_trigger_objects(trigger_objects);
	handle_holder->set_trigger(trigger);
	handle_holder->set_event_information(event_information);
	handle_holder->set_gen_particles(gen_particles);
	handle_holder->set_config_reader(config_reader);
	handle_holder->set_prescale_provider(prescale_prov);

	handle_holder->set_prescale_provider(prescale_prov);

	handle_holder->set_ident(identifier);

	if(process_mc) run_selector->accept_all(true);	 // no JSON file on MC
	if(process_mc) beag_gen_evts = new std::vector<beag::TTbarGenEvent>();
	if(process_mc) gen_evt = new mor::TTbarGenEvent(jet_corrector);
	std::cout << "process mc: " << process_mc << std::endl;
	handle_holder->set_ttbar_gen_evt(gen_evt);

	// manage individual selection steps
	cut_selector = new CutSelector(handle_holder, config_reader);
	cut_selector->set_process_all_pdf_sets(process_all_pdf_sets, pdf_set); 

        event_number = 0;
        max_events = -1;
	first_event_call = true;
}

Analyser::~Analyser()
{
	outfile->cd();

	if(pdf_checker){ delete pdf_checker; pdf_checker = NULL; }
	if(pdf_weight_prov){ delete pdf_weight_prov; pdf_weight_prov = NULL; }
	if(cut_selector){ delete cut_selector; cut_selector = NULL; }
	if(run_selector){ delete run_selector; run_selector = NULL; }
	if(histo_writer){ delete histo_writer; histo_writer = NULL; }
	if(uncorrected_jets){ delete uncorrected_jets; uncorrected_jets = NULL; }
	if(jet_corrector){ delete jet_corrector; jet_corrector = NULL; }
	if(handle_holder){ delete handle_holder; handle_holder = NULL; }
	if(prescale_prov){ delete prescale_prov; prescale_prov = NULL; }
}

void Analyser::set_file_names(std::vector<std::string> *file_names)
{
        evt_runner->set_file_names(file_names);
}

void Analyser::set_max_events(int max_events)
{
	this->max_events = max_events;
}

void Analyser::analyse()
{
	while(evt_runner->has_next() && (event_number < max_events-1 || max_events == -1)){
                if(change_event()) cut_selector->plot();
	}
}

// re-initialise all information if a new event is read-in
bool Analyser::change_event()
{
        event_number = evt_runner->current_event();
        if(evt_runner->end_of_file()){
                std::cout << identifier << " assigning collections..." << std::endl;
                evt_runner->assign_collection<beag::EventInformation>(beag_event_information, "evt_info");
		if(evt_runner->empty_file()) return false;
                if(process_trigger_objects) evt_runner->assign_collection<beag::TriggerObject>(beag_trigger_objects, "trigger_objects");
                if(process_gen_particles) evt_runner->assign_collection<beag::Particle>(beag_gen_particles, "gen_particles");

		// switch between isolated/nonisolated lepton collections
		if(obj_type == 1)
		{
                	evt_runner->assign_collection<beag::Jet>(beag_jets, "jets");
	                evt_runner->assign_collection<beag::Electron>(beag_electrons, "electrons");
	                evt_runner->assign_collection<beag::Muon>(beag_muons, "nonisomuons");
        	        evt_runner->assign_collection<beag::MET>(beag_mets, "mets");
		}
		else if(obj_type == 0) //default
		{
                        evt_runner->assign_collection<beag::Jet>(beag_jets, "jets");
                        evt_runner->assign_collection<beag::Electron>(beag_electrons, "electrons");
                        evt_runner->assign_collection<beag::Muon>(beag_muons, "muons");
                        evt_runner->assign_collection<beag::MET>(beag_mets, "mets");
		}
		else if(obj_type == 2)
		{
                        evt_runner->assign_collection<beag::Jet>(beag_jets, "jets");
                        evt_runner->assign_collection<beag::Electron>(beag_electrons, "nonisoelectrons");
                        evt_runner->assign_collection<beag::Muon>(beag_muons, "muons");
                        evt_runner->assign_collection<beag::MET>(beag_mets, "mets");

		}
		else if(obj_type == 3)
		{
                	evt_runner->assign_collection<beag::Jet>(beag_jets, "nomujets");
	                evt_runner->assign_collection<beag::Electron>(beag_electrons, "electrons");
	                evt_runner->assign_collection<beag::Muon>(beag_muons, "nonisomuons");
        	        evt_runner->assign_collection<beag::MET>(beag_mets, "mets");
		}
		else if(obj_type == 4)
		{
                        evt_runner->assign_collection<beag::Jet>(beag_jets, "noejets");
                        evt_runner->assign_collection<beag::Electron>(beag_electrons, "nonisoelectrons");
                        evt_runner->assign_collection<beag::Muon>(beag_muons, "muons");
                        evt_runner->assign_collection<beag::MET>(beag_mets, "mets");

		}
		else
		{
			std::cout << "[Analyser.cc]:change_event(): you chose obj_type = " << obj_type << " which is not recognised; I'll crash now..." << std::endl;
			return false;
		}
		if(process_noniso_muons){
			evt_runner->assign_collection<beag::Muon>(beag_noniso_muons, "nonisomuons");
			evt_runner->assign_collection<beag::Jet>(beag_nomu_jets, "nomujets");
		}
		if(process_noniso_electrons){
			evt_runner->assign_collection<beag::Electron>(beag_noniso_electrons, "nonisoelectrons");
			evt_runner->assign_collection<beag::Jet>(beag_noe_jets, "noejets");
		}

                evt_runner->assign_collection<beag::PrimaryVertex>(beag_pvertices, "pvertices");
                evt_runner->assign_collection<beag::Trigger>(beag_triggers, "trigger");
		trigger_name_runner->assign_mapper<std::vector<std::string> >(trigger_names, "trigger_names");
		if(process_trigger_objects) trigger_name_runner->assign_mapper<std::vector<std::vector<std::string> > >(trigger_module_names, "trigger_names_modules");
		if(process_prescales){
			trigger_name_runner->assign_mapper<std::vector<unsigned int> >(hlt_prescales, "hlt_prescales");
			trigger_name_runner->assign_mapper<std::vector<unsigned int> >(l1_prescales, "l1_prescales");
		}
		if(read_pdf_weights) weight_runner->assign_mapper<std::vector<std::vector<double> > >(pdf_evt_weights, "pdf_evt_weights");
		tag_name_runner->assign_mapper<std::vector<std::string> >(btag_names, "btag_names");
		tag_name_runner->assign_mapper<std::vector<std::string> >(pfiso_conesize_names, "pfiso_conesize_names");
		tag_name_runner->assign_mapper<std::vector<std::string> >(eID_names, "eID_names");
		tag_name_runner->assign_mapper<std::vector<std::string> >(muID_names, "muID_names");
		tag_name_runner->assign_mapper<std::vector<std::string> >(pdf_names, "pdf_names");
		// update tags (eID, muID, btag algos) once per file
		tag_name_runner->next_mapping();
                if(process_mc) evt_runner->assign_collection<beag::TTbarGenEvent>(beag_gen_evts, "gen_evt");

		eID_checker->set_id_vector(eID_names);
		muID_checker->set_id_vector(muID_names);
		btag_checker->set_id_vector(btag_names);
		pfiso_conesize_checker->set_id_vector(pfiso_conesize_names);
		pdf_checker->set_id_vector(pdf_names);
        }

        if(event_number % 500 == 0){
                std::cout << identifier << " processing event: " << event_number << std::endl;
	}
        
        evt_runner->next_event();

	// initialise all selection steps
	if(first_event_call){
		cut_selector->set_all_cuts();
	}

	event_information->set_beag_info(*beag_event_information->begin());
	first_event_call = false;

	trigger->set_beag_info(*beag_triggers->begin());
	if(event_information->trigger_changed()){
		trigger_name_runner->next_mapping();
		std::vector<std::string> *unversioned_trigger_names = fill_unversioned_trigger_names(trigger_names);
		trigger->set_trigger_names(unversioned_trigger_names);
		trigger_checker->set_id_vector(unversioned_trigger_names);
		if(process_trigger_objects) trigger_module_manager->initialise(trigger_checker, trigger_module_names);
		prescale_prov->set_trigger_names(unversioned_trigger_names);
		//l1seed_checker->set_id_vector(trigger_module_names);
	}
	if(!run_selector->is_good()) return false; // stop here if event not in certified JSON file

	if(process_trigger_objects) prepare_mor_collections(beag_trigger_objects, trigger_objects);
	if(process_gen_particles) prepare_mor_collections(beag_gen_particles, gen_particles);

	prepare_mor_collections(beag_jets, uncorrected_jets);
	prepare_mor_collections(beag_muons, muons);
	prepare_mor_collections(beag_electrons, electrons);
	prepare_mor_collections(beag_mets, mets);
	prepare_mor_collections(beag_pvertices, pvertices);

	if(process_mc && beag_gen_evts != NULL && beag_gen_evts->size() > 0) gen_evt->set_beag_info(*beag_gen_evts->begin(), event_information->rho());

	assign_lepton_id_checkers(muons, muID_checker, trigger_checker,pfiso_conesize_checker);
	assign_lepton_id_checkers(electrons, eID_checker, trigger_checker,pfiso_conesize_checker);
	assign_jet_id_checkers(uncorrected_jets, btag_checker);
	jet_corrector->correct_jets(uncorrected_jets, jets, event_information->rho());

	// rho for electron EA pile-up corrections
	assign_el_iso_rho(electrons);

	if(process_noniso_muons){
		prepare_mor_collections(beag_noniso_muons, noniso_muons);
		prepare_mor_collections(beag_nomu_jets, uncorrected_nomu_jets);
		assign_lepton_id_checkers(noniso_muons, muID_checker, trigger_checker,pfiso_conesize_checker); 
		assign_jet_id_checkers(uncorrected_nomu_jets, btag_checker);
		jet_corrector->correct_jets(uncorrected_nomu_jets, nomu_jets, event_information->rho());
	}
	if(process_noniso_electrons){
		prepare_mor_collections(beag_noniso_electrons, noniso_electrons);
		prepare_mor_collections(beag_noe_jets, uncorrected_noe_jets);
		assign_lepton_id_checkers(noniso_electrons, eID_checker, trigger_checker,pfiso_conesize_checker); 
		assign_jet_id_checkers(uncorrected_noe_jets, btag_checker);
		jet_corrector->correct_jets(uncorrected_noe_jets, noe_jets, event_information->rho());
		assign_el_iso_rho(noniso_electrons);
	}

	return true;
}

/*
 * leptons share classes that check if they pass a given lepton ID
 * assign to each lepton this class
 */
template <class mor_type>
void Analyser::assign_lepton_id_checkers(typename std::vector<mor_type> *mor_objs, mor::IDChecker *id_checker, mor::IDChecker *trigger_checker,mor::IDChecker *pfiso_conesize_checker)
{
	for(typename std::vector<mor_type>::iterator mor_obj = mor_objs->begin();
		mor_obj != mor_objs->end();
		++mor_obj){
		mor_obj->set_lepton_id_checker(id_checker);
		mor_obj->set_trigger_id_checker(trigger_checker);
		mor_obj->set_pfiso_conesize_checker(pfiso_conesize_checker);
	}
}

void Analyser::assign_el_iso_rho(std::vector<mor::Electron> *electrons)
{
	for(std::vector<mor::Electron>::iterator e = electrons->begin();
		e != electrons->end();
		++e){
		e->set_rho(handle_holder->get_event_information()->e_iso_rho());
		e->set_is_real_data(handle_holder->get_event_information()->is_real_data());
	}
}

void Analyser::assign_jet_id_checkers(std::vector<mor::Jet> *mor_jets, mor::IDChecker *btag_checker)
{
	for(std::vector<mor::Jet>::iterator jet = mor_jets->begin();
		jet != mor_jets->end();
		++jet){
		jet->set_bDiscriminator_checker(btag_checker);
	}
}

template <class beag_type, class mor_type>
void Analyser::prepare_mor_collections(typename std::vector<beag_type> *beag_objs, typename std::vector<mor_type> *mor_objs)
{
	// remove all mor objects from the previous event
	mor_objs->clear();

	// for each beag object in the .root file
	for(typename std::vector<beag_type>::iterator beag_obj = beag_objs->begin();
		beag_obj != beag_objs->end();
		++beag_obj){
		// create an according mor object
		mor_type mor_obj(*beag_obj);
		// add it to the vector of mor objects
		mor_objs->push_back(mor_obj);
	}
}

/*
 * triggers are stored with version names, eg. HLT_IsoMu24_v5
 * this removes the _v5 and stores the trigger name without version information, eg. HLT_IsoMu24
 */
std::vector<std::string>* Analyser::fill_unversioned_trigger_names(std::vector<std::string> *names)
{
	std::vector<std::string> versioned_names = *names;	
	names->clear();
	const boost::regex regexp("_v[0-9]+$");

	for(std::vector<std::string>::iterator v_name = versioned_names.begin();
	    v_name != versioned_names.end(); v_name++){
		std::string unv_name = boost::regex_replace(*v_name,regexp,"");
		names->push_back(unv_name);
	}

	return names;
}

