#include "../../interface/AnalysisTools/ETH2D.h"

double eire::TH2D::global_weight = 1;

eire::TH2D::TH2D(const char *name,const char *title,Int_t nbinsx,Double_t xlow,Double_t xup
           ,Int_t nbinsy,Double_t ylow,Double_t yup) : ::TH2D(name,title,nbinsx, xlow, xup, nbinsy, ylow, yup)
{
//	::TH2D(name,title,nbinsx, xlow, xup, nbinsy, ylow, yup);
}

void eire::TH2D::set_global_weight(double weight)
{
	eire::TH2D::global_weight = weight;
}

Int_t eire::TH2D::Fill(double xvalue, double yvalue, double weight)
{
	if(weight == -1)
		return this->::TH2D::Fill(xvalue, yvalue, global_weight);
	else
		return this->::TH2D::Fill(xvalue, yvalue, weight);
}
