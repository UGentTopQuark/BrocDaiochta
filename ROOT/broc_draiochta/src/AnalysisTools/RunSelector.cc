#include "../../interface/AnalysisTools/RunSelector.h"

RunSelector::RunSelector()
{
	accept_all_events = false;
}

RunSelector::~RunSelector()
{
}

void RunSelector::set_event_information(mor::EventInformation *evt_info)
{
	this->evt_info = evt_info;
}

bool RunSelector::is_good()
{
	if(accept_all_events) return true;
	// JSON AUTO FILL BEGIN -- do not edit this comment
	double cur_run_number = evt_info->run();
	double cur_lumi_block = evt_info->lumi_block();
	double MIN=0;
	double MAX=10000000;
	if(	( cur_run_number == 190645 && (
	  (cur_lumi_block >= 10 && cur_lumi_block <= 110) ) ) ||
	( cur_run_number == 190646 && (
	  (cur_lumi_block >= 92 && cur_lumi_block <= 111) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 7 && cur_lumi_block <= 8) ||
	  (cur_lumi_block >= 15 && cur_lumi_block <= 18) ) ) ||
	( cur_run_number == 190659 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 44) ||
	  (cur_lumi_block >= 90 && cur_lumi_block <= 131) ||
	  (cur_lumi_block >= 85 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 71 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 64 && cur_lumi_block <= 67) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 59) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 167) ) ) ||
	( cur_run_number == 190679 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 55) ) ) ||
	( cur_run_number == 190688 && (
	  (cur_lumi_block >= 69 && cur_lumi_block <= 249) ) ) ||
	( cur_run_number == 190702 && (
	  (cur_lumi_block >= 124 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 53) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 122) ) ) ||
	( cur_run_number == 190703 && (
	  (cur_lumi_block >= 238 && cur_lumi_block <= 251) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 234 && cur_lumi_block <= 235) ||
	  (cur_lumi_block >= 207 && cur_lumi_block <= 209) ||
	  (cur_lumi_block >= 134 && cur_lumi_block <= 143) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 132) ||
	  (cur_lumi_block >= 7 && cur_lumi_block <= 32) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 232) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 223) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 51) ||
	  (cur_lumi_block >= 145 && cur_lumi_block <= 204) ) ) ||
	( cur_run_number == 190704 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 3) ) ) ||
	( cur_run_number == 190705 && (
	  (cur_lumi_block >= 189 && cur_lumi_block <= 194) ||
	  (cur_lumi_block >= 217 && cur_lumi_block <= 218) ||
	  (cur_lumi_block >= 204 && cur_lumi_block <= 215) ||
	  (cur_lumi_block >= 198 && cur_lumi_block <= 200) ||
	  (cur_lumi_block >= 7 && cur_lumi_block <= 65) ||
	  (cur_lumi_block >= 347 && cur_lumi_block <= 350) ||
	  (cur_lumi_block >= 331 && cur_lumi_block <= 336) ||
	  (cur_lumi_block >= 353 && cur_lumi_block <= 383) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 5) ||
	  (cur_lumi_block >= 338 && cur_lumi_block <= 345) ||
	  (cur_lumi_block >= 128 && cur_lumi_block <= 171) ||
	  (cur_lumi_block >= 221 && cur_lumi_block <= 247) ||
	  (cur_lumi_block >= 250 && cur_lumi_block <= 255) ||
	  (cur_lumi_block >= 323 && cur_lumi_block <= 327) ||
	  (cur_lumi_block >= 258 && cur_lumi_block <= 321) ||
	  (cur_lumi_block >= 81 && cur_lumi_block <= 126) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 186) ) ) ||
	( cur_run_number == 190706 && (
	  (cur_lumi_block >= 91 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 18 && cur_lumi_block <= 18) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 31) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 7) ||
	  (cur_lumi_block >= 50 && cur_lumi_block <= 51) ||
	  (cur_lumi_block >= 112 && cur_lumi_block <= 126) ||
	  (cur_lumi_block >= 103 && cur_lumi_block <= 108) ||
	  (cur_lumi_block >= 4 && cur_lumi_block <= 4) ) ) ||
	( cur_run_number == 190707 && (
	  (cur_lumi_block >= 239 && cur_lumi_block <= 257) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 237) ) ) ||
	( cur_run_number == 190708 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 189) ) ) ||
	( cur_run_number == 190733 && (
	  (cur_lumi_block >= 71 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 99 && cur_lumi_block <= 389) ||
	  (cur_lumi_block >= 392 && cur_lumi_block <= 460) ) ) ||
	( cur_run_number == 190736 && (
	  (cur_lumi_block >= 104 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 32 && cur_lumi_block <= 34) ||
	  (cur_lumi_block >= 129 && cur_lumi_block <= 144) ||
	  (cur_lumi_block >= 78 && cur_lumi_block <= 80) ||
	  (cur_lumi_block >= 18 && cur_lumi_block <= 21) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 16) ||
	  (cur_lumi_block >= 25 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 100) ||
	  (cur_lumi_block >= 41 && cur_lumi_block <= 74) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 185) ||
	  (cur_lumi_block >= 4 && cur_lumi_block <= 14) ||
	  (cur_lumi_block >= 148 && cur_lumi_block <= 180) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 76) ) ) ||
	( cur_run_number == 190738 && (
	  (cur_lumi_block >= 309 && cur_lumi_block <= 317) ||
	  (cur_lumi_block >= 322 && cur_lumi_block <= 349) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 320 && cur_lumi_block <= 320) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 226) ||
	  (cur_lumi_block >= 229 && cur_lumi_block <= 305) ) ) ||
	( cur_run_number == 190782 && (
	  (cur_lumi_block >= 245 && cur_lumi_block <= 252) ||
	  (cur_lumi_block >= 139 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 322 && cur_lumi_block <= 333) ||
	  (cur_lumi_block >= 336 && cur_lumi_block <= 351) ||
	  (cur_lumi_block >= 401 && cur_lumi_block <= 409) ||
	  (cur_lumi_block >= 354 && cur_lumi_block <= 396) ||
	  (cur_lumi_block >= 399 && cur_lumi_block <= 399) ||
	  (cur_lumi_block >= 264 && cur_lumi_block <= 319) ||
	  (cur_lumi_block >= 255 && cur_lumi_block <= 261) ||
	  (cur_lumi_block >= 222 && cur_lumi_block <= 233) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 236 && cur_lumi_block <= 242) ||
	  (cur_lumi_block >= 184 && cur_lumi_block <= 219) ) ) ||
	( cur_run_number == 190895 && (
	  (cur_lumi_block >= 718 && cur_lumi_block <= 720) ||
	  (cur_lumi_block >= 643 && cur_lumi_block <= 669) ||
	  (cur_lumi_block >= 578 && cur_lumi_block <= 584) ||
	  (cur_lumi_block >= 673 && cur_lumi_block <= 675) ||
	  (cur_lumi_block >= 370 && cur_lumi_block <= 395) ||
	  (cur_lumi_block >= 762 && cur_lumi_block <= 763) ||
	  (cur_lumi_block >= 607 && cur_lumi_block <= 627) ||
	  (cur_lumi_block >= 808 && cur_lumi_block <= 837) ||
	  (cur_lumi_block >= 682 && cur_lumi_block <= 703) ||
	  (cur_lumi_block >= 464 && cur_lumi_block <= 466) ||
	  (cur_lumi_block >= 457 && cur_lumi_block <= 462) ||
	  (cur_lumi_block >= 707 && cur_lumi_block <= 715) ||
	  (cur_lumi_block >= 637 && cur_lumi_block <= 641) ||
	  (cur_lumi_block >= 399 && cur_lumi_block <= 453) ||
	  (cur_lumi_block >= 587 && cur_lumi_block <= 605) ||
	  (cur_lumi_block >= 572 && cur_lumi_block <= 575) ||
	  (cur_lumi_block >= 221 && cur_lumi_block <= 302) ||
	  (cur_lumi_block >= 486 && cur_lumi_block <= 496) ||
	  (cur_lumi_block >= 210 && cur_lumi_block <= 216) ||
	  (cur_lumi_block >= 64 && cur_lumi_block <= 202) ||
	  (cur_lumi_block >= 305 && cur_lumi_block <= 366) ||
	  (cur_lumi_block >= 748 && cur_lumi_block <= 760) ||
	  (cur_lumi_block >= 472 && cur_lumi_block <= 482) ||
	  (cur_lumi_block >= 766 && cur_lumi_block <= 804) ||
	  (cur_lumi_block >= 633 && cur_lumi_block <= 634) ||
	  (cur_lumi_block >= 510 && cur_lumi_block <= 570) ||
	  (cur_lumi_block >= 498 && cur_lumi_block <= 501) ||
	  (cur_lumi_block >= 841 && cur_lumi_block <= 948) ||
	  (cur_lumi_block >= 722 && cur_lumi_block <= 744) ) ) ||
	( cur_run_number == 190906 && (
	  (cur_lumi_block >= 365 && cur_lumi_block <= 407) ||
	  (cur_lumi_block >= 152 && cur_lumi_block <= 152) ||
	  (cur_lumi_block >= 328 && cur_lumi_block <= 354) ||
	  (cur_lumi_block >= 239 && cur_lumi_block <= 242) ||
	  (cur_lumi_block >= 281 && cur_lumi_block <= 283) ||
	  (cur_lumi_block >= 154 && cur_lumi_block <= 183) ||
	  (cur_lumi_block >= 292 && cur_lumi_block <= 308) ||
	  (cur_lumi_block >= 214 && cur_lumi_block <= 216) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 193) ||
	  (cur_lumi_block >= 100 && cur_lumi_block <= 148) ||
	  (cur_lumi_block >= 286 && cur_lumi_block <= 287) ||
	  (cur_lumi_block >= 259 && cur_lumi_block <= 265) ||
	  (cur_lumi_block >= 279 && cur_lumi_block <= 279) ||
	  (cur_lumi_block >= 268 && cur_lumi_block <= 277) ||
	  (cur_lumi_block >= 218 && cur_lumi_block <= 229) ||
	  (cur_lumi_block >= 356 && cur_lumi_block <= 360) ||
	  (cur_lumi_block >= 411 && cur_lumi_block <= 496) ||
	  (cur_lumi_block >= 232 && cur_lumi_block <= 236) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 96) ||
	  (cur_lumi_block >= 196 && cur_lumi_block <= 206) ||
	  (cur_lumi_block >= 311 && cur_lumi_block <= 325) ) ) ||
	( cur_run_number == 190945 && (
	  (cur_lumi_block >= 124 && cur_lumi_block <= 152) ||
	  (cur_lumi_block >= 155 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 185 && cur_lumi_block <= 207) ) ) ||
	( cur_run_number == 190949 && (
	  (cur_lumi_block >= 22 && cur_lumi_block <= 22) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 9) ||
	  (cur_lumi_block >= 13 && cur_lumi_block <= 19) ||
	  (cur_lumi_block >= 24 && cur_lumi_block <= 81) ) ) ||
	( cur_run_number == 191043 && (
	  (cur_lumi_block >= 45 && cur_lumi_block <= 46) ) ) ||
	( cur_run_number == 191046 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 38) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 21 && cur_lumi_block <= 21) ||
	  (cur_lumi_block >= 129 && cur_lumi_block <= 157) ||
	  (cur_lumi_block >= 78 && cur_lumi_block <= 81) ||
	  (cur_lumi_block >= 119 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 166 && cur_lumi_block <= 180) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 18) ||
	  (cur_lumi_block >= 116 && cur_lumi_block <= 116) ||
	  (cur_lumi_block >= 88 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 159 && cur_lumi_block <= 164) ||
	  (cur_lumi_block >= 194 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 185 && cur_lumi_block <= 191) ||
	  (cur_lumi_block >= 57 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 183 && cur_lumi_block <= 183) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 62) ||
	  (cur_lumi_block >= 111 && cur_lumi_block <= 114) ||
	  (cur_lumi_block >= 108 && cur_lumi_block <= 108) ||
	  (cur_lumi_block >= 218 && cur_lumi_block <= 239) ||
	  (cur_lumi_block >= 41 && cur_lumi_block <= 49) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 214) ||
	  (cur_lumi_block >= 92 && cur_lumi_block <= 105) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 24 && cur_lumi_block <= 27) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 86) ) ) ||
	( cur_run_number == 191056 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 17) ||
	  (cur_lumi_block >= 4 && cur_lumi_block <= 9) ||
	  (cur_lumi_block >= 19 && cur_lumi_block <= 19) ) ) ||
	( cur_run_number == 191057 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 4 && cur_lumi_block <= 40) ) ) ||
	( cur_run_number == 191062 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 37) ||
	  (cur_lumi_block >= 492 && cur_lumi_block <= 541) ||
	  (cur_lumi_block >= 26 && cur_lumi_block <= 29) ||
	  (cur_lumi_block >= 17 && cur_lumi_block <= 24) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 352 && cur_lumi_block <= 469) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 39 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 228 && cur_lumi_block <= 349) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 148) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 3) ||
	  (cur_lumi_block >= 471 && cur_lumi_block <= 488) ||
	  (cur_lumi_block >= 202 && cur_lumi_block <= 214) ||
	  (cur_lumi_block >= 216 && cur_lumi_block <= 225) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 199) ||
	  (cur_lumi_block >= 5 && cur_lumi_block <= 14) ) ) ||
	( cur_run_number == 191090 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 33) ||
	  (cur_lumi_block >= 17 && cur_lumi_block <= 18) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 23) ||
	  (cur_lumi_block >= 13 && cur_lumi_block <= 14) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 3) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 9) ||
	  (cur_lumi_block >= 5 && cur_lumi_block <= 6) ) ) ||
	( cur_run_number == 191201 && (
	  (cur_lumi_block >= 52 && cur_lumi_block <= 79) ||
	  (cur_lumi_block >= 38 && cur_lumi_block <= 49) ) ) ||
	( cur_run_number == 191202 && (
	  (cur_lumi_block >= 87 && cur_lumi_block <= 105) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 108 && cur_lumi_block <= 118) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 68) ) ) ||
	( cur_run_number == 191226 && (
	  (cur_lumi_block >= 743 && cur_lumi_block <= 744) ||
	  (cur_lumi_block >= 428 && cur_lumi_block <= 428) ||
	  (cur_lumi_block >= 386 && cur_lumi_block <= 386) ||
	  (cur_lumi_block >= 1026 && cur_lumi_block <= 1026) ||
	  (cur_lumi_block >= 737 && cur_lumi_block <= 737) ||
	  (cur_lumi_block >= 244 && cur_lumi_block <= 244) ||
	  (cur_lumi_block >= 240 && cur_lumi_block <= 240) ||
	  (cur_lumi_block >= 1206 && cur_lumi_block <= 1207) ||
	  (cur_lumi_block >= 272 && cur_lumi_block <= 272) ||
	  (cur_lumi_block >= 190 && cur_lumi_block <= 191) ||
	  (cur_lumi_block >= 182 && cur_lumi_block <= 185) ||
	  (cur_lumi_block >= 488 && cur_lumi_block <= 489) ||
	  (cur_lumi_block >= 298 && cur_lumi_block <= 298) ||
	  (cur_lumi_block >= 746 && cur_lumi_block <= 746) ||
	  (cur_lumi_block >= 714 && cur_lumi_block <= 714) ||
	  (cur_lumi_block >= 1192 && cur_lumi_block <= 1193) ||
	  (cur_lumi_block >= 779 && cur_lumi_block <= 779) ||
	  (cur_lumi_block >= 414 && cur_lumi_block <= 414) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 162) ||
	  (cur_lumi_block >= 89 && cur_lumi_block <= 91) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 175) ||
	  (cur_lumi_block >= 1164 && cur_lumi_block <= 1165) ||
	  (cur_lumi_block >= 1095 && cur_lumi_block <= 1095) ||
	  (cur_lumi_block >= 934 && cur_lumi_block <= 934) ||
	  (cur_lumi_block >= 342 && cur_lumi_block <= 342) ||
	  (cur_lumi_block >= 700 && cur_lumi_block <= 701) ||
	  (cur_lumi_block >= 287 && cur_lumi_block <= 288) ||
	  (cur_lumi_block >= 266 && cur_lumi_block <= 268) ||
	  (cur_lumi_block >= 263 && cur_lumi_block <= 263) ||
	  (cur_lumi_block >= 394 && cur_lumi_block <= 394) ||
	  (cur_lumi_block >= 167 && cur_lumi_block <= 167) ||
	  (cur_lumi_block >= 107 && cur_lumi_block <= 107) ||
	  (cur_lumi_block >= 397 && cur_lumi_block <= 397) ||
	  (cur_lumi_block >= 87 && cur_lumi_block <= 87) ||
	  (cur_lumi_block >= 929 && cur_lumi_block <= 929) ||
	  (cur_lumi_block >= 820 && cur_lumi_block <= 820) ||
	  (cur_lumi_block >= 953 && cur_lumi_block <= 953) ||
	  (cur_lumi_block >= 817 && cur_lumi_block <= 817) ||
	  (cur_lumi_block >= 304 && cur_lumi_block <= 304) ||
	  (cur_lumi_block >= 221 && cur_lumi_block <= 221) ||
	  (cur_lumi_block >= 1198 && cur_lumi_block <= 1199) ||
	  (cur_lumi_block >= 97 && cur_lumi_block <= 99) ||
	  (cur_lumi_block >= 114 && cur_lumi_block <= 120) ||
	  (cur_lumi_block >= 199 && cur_lumi_block <= 199) ||
	  (cur_lumi_block >= 751 && cur_lumi_block <= 751) ||
	  (cur_lumi_block >= 731 && cur_lumi_block <= 732) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 226) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 213) ||
	  (cur_lumi_block >= 897 && cur_lumi_block <= 898) ||
	  (cur_lumi_block >= 766 && cur_lumi_block <= 768) ||
	  (cur_lumi_block >= 321 && cur_lumi_block <= 321) ||
	  (cur_lumi_block >= 101 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 405 && cur_lumi_block <= 405) ||
	  (cur_lumi_block >= 823 && cur_lumi_block <= 823) ||
	  (cur_lumi_block >= 311 && cur_lumi_block <= 311) ||
	  (cur_lumi_block >= 293 && cur_lumi_block <= 293) ||
	  (cur_lumi_block >= 1158 && cur_lumi_block <= 1159) ||
	  (cur_lumi_block >= 903 && cur_lumi_block <= 903) ||
	  (cur_lumi_block >= 237 && cur_lumi_block <= 237) ||
	  (cur_lumi_block >= 693 && cur_lumi_block <= 694) ||
	  (cur_lumi_block >= 1041 && cur_lumi_block <= 1042) ||
	  (cur_lumi_block >= 825 && cur_lumi_block <= 827) ||
	  (cur_lumi_block >= 188 && cur_lumi_block <= 188) ||
	  (cur_lumi_block >= 911 && cur_lumi_block <= 911) ||
	  (cur_lumi_block >= 1059 && cur_lumi_block <= 1060) ||
	  (cur_lumi_block >= 353 && cur_lumi_block <= 353) ||
	  (cur_lumi_block >= 136 && cur_lumi_block <= 138) ||
	  (cur_lumi_block >= 939 && cur_lumi_block <= 940) ||
	  (cur_lumi_block >= 1176 && cur_lumi_block <= 1177) ||
	  (cur_lumi_block >= 739 && cur_lumi_block <= 741) ||
	  (cur_lumi_block >= 708 && cur_lumi_block <= 709) ||
	  (cur_lumi_block >= 965 && cur_lumi_block <= 965) ||
	  (cur_lumi_block >= 110 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 1154 && cur_lumi_block <= 1155) ||
	  (cur_lumi_block >= 883 && cur_lumi_block <= 885) ||
	  (cur_lumi_block >= 228 && cur_lumi_block <= 230) ||
	  (cur_lumi_block >= 280 && cur_lumi_block <= 280) ||
	  (cur_lumi_block >= 283 && cur_lumi_block <= 284) ||
	  (cur_lumi_block >= 706 && cur_lumi_block <= 706) ||
	  (cur_lumi_block >= 202 && cur_lumi_block <= 203) ||
	  (cur_lumi_block >= 829 && cur_lumi_block <= 830) ||
	  (cur_lumi_block >= 215 && cur_lumi_block <= 217) ||
	  (cur_lumi_block >= 254 && cur_lumi_block <= 254) ||
	  (cur_lumi_block >= 392 && cur_lumi_block <= 392) ||
	  (cur_lumi_block >= 177 && cur_lumi_block <= 180) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 141) ||
	  (cur_lumi_block >= 223 && cur_lumi_block <= 223) ||
	  (cur_lumi_block >= 772 && cur_lumi_block <= 772) ||
	  (cur_lumi_block >= 412 && cur_lumi_block <= 412) ||
	  (cur_lumi_block >= 234 && cur_lumi_block <= 235) ||
	  (cur_lumi_block >= 1216 && cur_lumi_block <= 1217) ||
	  (cur_lumi_block >= 122 && cur_lumi_block <= 124) ||
	  (cur_lumi_block >= 974 && cur_lumi_block <= 975) ||
	  (cur_lumi_block >= 143 && cur_lumi_block <= 144) ||
	  (cur_lumi_block >= 205 && cur_lumi_block <= 208) ||
	  (cur_lumi_block >= 900 && cur_lumi_block <= 900) ||
	  (cur_lumi_block >= 219 && cur_lumi_block <= 219) ||
	  (cur_lumi_block >= 972 && cur_lumi_block <= 972) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 134) ||
	  (cur_lumi_block >= 782 && cur_lumi_block <= 782) ||
	  (cur_lumi_block >= 251 && cur_lumi_block <= 252) ||
	  (cur_lumi_block >= 436 && cur_lumi_block <= 436) ||
	  (cur_lumi_block >= 248 && cur_lumi_block <= 249) ||
	  (cur_lumi_block >= 146 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 681 && cur_lumi_block <= 681) ||
	  (cur_lumi_block >= 408 && cur_lumi_block <= 408) ||
	  (cur_lumi_block >= 362 && cur_lumi_block <= 362) ||
	  (cur_lumi_block >= 169 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 164) ||
	  (cur_lumi_block >= 372 && cur_lumi_block <= 372) ||
	  (cur_lumi_block >= 195 && cur_lumi_block <= 197) ||
	  (cur_lumi_block >= 242 && cur_lumi_block <= 242) ) ) ||
	( cur_run_number == 191247 && (
	  (cur_lumi_block >= 71 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 102 && cur_lumi_block <= 103) ||
	  (cur_lumi_block >= 887 && cur_lumi_block <= 888) ||
	  (cur_lumi_block >= 26 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 358 && cur_lumi_block <= 363) ||
	  (cur_lumi_block >= 1120 && cur_lumi_block <= 1123) ||
	  (cur_lumi_block >= 1045 && cur_lumi_block <= 1046) ||
	  (cur_lumi_block >= 891 && cur_lumi_block <= 892) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 185) ||
	  (cur_lumi_block >= 1193 && cur_lumi_block <= 1196) ||
	  (cur_lumi_block >= 264 && cur_lumi_block <= 266) ||
	  (cur_lumi_block >= 44 && cur_lumi_block <= 55) ||
	  (cur_lumi_block >= 1187 && cur_lumi_block <= 1187) ||
	  (cur_lumi_block >= 587 && cur_lumi_block <= 587) ||
	  (cur_lumi_block >= 74 && cur_lumi_block <= 76) ||
	  (cur_lumi_block >= 741 && cur_lumi_block <= 742) ||
	  (cur_lumi_block >= 190 && cur_lumi_block <= 190) ||
	  (cur_lumi_block >= 161 && cur_lumi_block <= 161) ||
	  (cur_lumi_block >= 986 && cur_lumi_block <= 989) ||
	  (cur_lumi_block >= 918 && cur_lumi_block <= 918) ||
	  (cur_lumi_block >= 298 && cur_lumi_block <= 298) ||
	  (cur_lumi_block >= 57 && cur_lumi_block <= 58) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 67) ||
	  (cur_lumi_block >= 108 && cur_lumi_block <= 108) ||
	  (cur_lumi_block >= 604 && cur_lumi_block <= 605) ||
	  (cur_lumi_block >= 871 && cur_lumi_block <= 871) ||
	  (cur_lumi_block >= 379 && cur_lumi_block <= 379) ||
	  (cur_lumi_block >= 89 && cur_lumi_block <= 90) ||
	  (cur_lumi_block >= 611 && cur_lumi_block <= 611) ||
	  (cur_lumi_block >= 31 && cur_lumi_block <= 32) ||
	  (cur_lumi_block >= 753 && cur_lumi_block <= 753) ||
	  (cur_lumi_block >= 343 && cur_lumi_block <= 343) ||
	  (cur_lumi_block >= 578 && cur_lumi_block <= 578) ||
	  (cur_lumi_block >= 225 && cur_lumi_block <= 229) ||
	  (cur_lumi_block >= 295 && cur_lumi_block <= 295) ||
	  (cur_lumi_block >= 287 && cur_lumi_block <= 288) ||
	  (cur_lumi_block >= 208 && cur_lumi_block <= 221) ||
	  (cur_lumi_block >= 78 && cur_lumi_block <= 80) ||
	  (cur_lumi_block >= 505 && cur_lumi_block <= 505) ||
	  (cur_lumi_block >= 764 && cur_lumi_block <= 765) ||
	  (cur_lumi_block >= 93 && cur_lumi_block <= 94) ||
	  (cur_lumi_block >= 995 && cur_lumi_block <= 996) ||
	  (cur_lumi_block >= 905 && cur_lumi_block <= 906) ||
	  (cur_lumi_block >= 39 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 291 && cur_lumi_block <= 291) ||
	  (cur_lumi_block >= 114 && cur_lumi_block <= 115) ||
	  (cur_lumi_block >= 668 && cur_lumi_block <= 669) ||
	  (cur_lumi_block >= 153 && cur_lumi_block <= 153) ||
	  (cur_lumi_block >= 15 && cur_lumi_block <= 17) ||
	  (cur_lumi_block >= 1023 && cur_lumi_block <= 1030) ||
	  (cur_lumi_block >= 312 && cur_lumi_block <= 312) ||
	  (cur_lumi_block >= 173 && cur_lumi_block <= 177) ||
	  (cur_lumi_block >= 1199 && cur_lumi_block <= 1200) ||
	  (cur_lumi_block >= 624 && cur_lumi_block <= 624) ||
	  (cur_lumi_block >= 340 && cur_lumi_block <= 340) ||
	  (cur_lumi_block >= 1116 && cur_lumi_block <= 1117) ||
	  (cur_lumi_block >= 365 && cur_lumi_block <= 365) ||
	  (cur_lumi_block >= 1079 && cur_lumi_block <= 1080) ||
	  (cur_lumi_block >= 370 && cur_lumi_block <= 370) ||
	  (cur_lumi_block >= 241 && cur_lumi_block <= 246) ||
	  (cur_lumi_block >= 129 && cur_lumi_block <= 130) ||
	  (cur_lumi_block >= 1063 && cur_lumi_block <= 1064) ||
	  (cur_lumi_block >= 322 && cur_lumi_block <= 322) ||
	  (cur_lumi_block >= 315 && cur_lumi_block <= 315) ||
	  (cur_lumi_block >= 495 && cur_lumi_block <= 495) ||
	  (cur_lumi_block >= 166 && cur_lumi_block <= 166) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ||
	  (cur_lumi_block >= 338 && cur_lumi_block <= 338) ||
	  (cur_lumi_block >= 1083 && cur_lumi_block <= 1086) ||
	  (cur_lumi_block >= 1110 && cur_lumi_block <= 1111) ||
	  (cur_lumi_block >= 1039 && cur_lumi_block <= 1040) ||
	  (cur_lumi_block >= 965 && cur_lumi_block <= 967) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 87) ||
	  (cur_lumi_block >= 147 && cur_lumi_block <= 147) ||
	  (cur_lumi_block >= 1005 && cur_lumi_block <= 1006) ||
	  (cur_lumi_block >= 992 && cur_lumi_block <= 993) ||
	  (cur_lumi_block >= 957 && cur_lumi_block <= 958) ||
	  (cur_lumi_block >= 618 && cur_lumi_block <= 618) ||
	  (cur_lumi_block >= 590 && cur_lumi_block <= 590) ||
	  (cur_lumi_block >= 484 && cur_lumi_block <= 484) ||
	  (cur_lumi_block >= 192 && cur_lumi_block <= 206) ||
	  (cur_lumi_block >= 236 && cur_lumi_block <= 236) ||
	  (cur_lumi_block >= 168 && cur_lumi_block <= 168) ||
	  (cur_lumi_block >= 928 && cur_lumi_block <= 929) ||
	  (cur_lumi_block >= 135 && cur_lumi_block <= 137) ||
	  (cur_lumi_block >= 69 && cur_lumi_block <= 69) ||
	  (cur_lumi_block >= 278 && cur_lumi_block <= 280) ||
	  (cur_lumi_block >= 140 && cur_lumi_block <= 142) ||
	  (cur_lumi_block >= 627 && cur_lumi_block <= 627) ||
	  (cur_lumi_block >= 756 && cur_lumi_block <= 756) ||
	  (cur_lumi_block >= 239 && cur_lumi_block <= 239) ||
	  (cur_lumi_block >= 269 && cur_lumi_block <= 274) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 24) ||
	  (cur_lumi_block >= 354 && cur_lumi_block <= 354) ||
	  (cur_lumi_block >= 1073 && cur_lumi_block <= 1074) ||
	  (cur_lumi_block >= 972 && cur_lumi_block <= 973) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 105) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 127) ||
	  (cur_lumi_block >= 747 && cur_lumi_block <= 748) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 7) ||
	  (cur_lumi_block >= 873 && cur_lumi_block <= 873) ||
	  (cur_lumi_block >= 253 && cur_lumi_block <= 254) ||
	  (cur_lumi_block >= 248 && cur_lumi_block <= 249) ||
	  (cur_lumi_block >= 111 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 9 && cur_lumi_block <= 13) ||
	  (cur_lumi_block >= 1103 && cur_lumi_block <= 1106) ||
	  (cur_lumi_block >= 1015 && cur_lumi_block <= 1016) ||
	  (cur_lumi_block >= 367 && cur_lumi_block <= 368) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 100) ||
	  (cur_lumi_block >= 317 && cur_lumi_block <= 317) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 37) ||
	  (cur_lumi_block >= 256 && cur_lumi_block <= 262) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 164) ||
	  (cur_lumi_block >= 132 && cur_lumi_block <= 132) ||
	  (cur_lumi_block >= 940 && cur_lumi_block <= 941) ||
	  (cur_lumi_block >= 810 && cur_lumi_block <= 811) ||
	  (cur_lumi_block >= 372 && cur_lumi_block <= 372) ||
	  (cur_lumi_block >= 117 && cur_lumi_block <= 124) ||
	  (cur_lumi_block >= 171 && cur_lumi_block <= 171) ) ) ||
	( cur_run_number == 191248 && (
	  (cur_lumi_block >= 84 && cur_lumi_block <= 91) ||
	  (cur_lumi_block >= 93 && cur_lumi_block <= 95) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 78) ||
	  (cur_lumi_block >= 100 && cur_lumi_block <= 103) ) ) ||
	( cur_run_number == 191264 && (
	  (cur_lumi_block >= 82 && cur_lumi_block <= 152) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 79) ||
	  (cur_lumi_block >= 155 && cur_lumi_block <= 189) ) ) ||
	( cur_run_number == 191271 && (
	  (cur_lumi_block >= 118 && cur_lumi_block <= 140) ||
	  (cur_lumi_block >= 351 && cur_lumi_block <= 363) ||
	  (cur_lumi_block >= 329 && cur_lumi_block <= 341) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 225 && cur_lumi_block <= 284) ||
	  (cur_lumi_block >= 142 && cur_lumi_block <= 223) ||
	  (cur_lumi_block >= 295 && cur_lumi_block <= 302) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 68 && cur_lumi_block <= 82) ||
	  (cur_lumi_block >= 322 && cur_lumi_block <= 326) ||
	  (cur_lumi_block >= 286 && cur_lumi_block <= 293) ||
	  (cur_lumi_block >= 305 && cur_lumi_block <= 316) ||
	  (cur_lumi_block >= 344 && cur_lumi_block <= 348) ||
	  (cur_lumi_block >= 318 && cur_lumi_block <= 320) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 116) ) ) ||
	( cur_run_number == 191276 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 16) ) ) ||
	( cur_run_number == 191277 && (
	  (cur_lumi_block >= 778 && cur_lumi_block <= 782) ||
	  (cur_lumi_block >= 21 && cur_lumi_block <= 22) ||
	  (cur_lumi_block >= 680 && cur_lumi_block <= 681) ||
	  (cur_lumi_block >= 455 && cur_lumi_block <= 457) ||
	  (cur_lumi_block >= 7 && cur_lumi_block <= 19) ||
	  (cur_lumi_block >= 445 && cur_lumi_block <= 446) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 180) ||
	  (cur_lumi_block >= 785 && cur_lumi_block <= 788) ||
	  (cur_lumi_block >= 255 && cur_lumi_block <= 258) ||
	  (cur_lumi_block >= 805 && cur_lumi_block <= 805) ||
	  (cur_lumi_block >= 595 && cur_lumi_block <= 596) ||
	  (cur_lumi_block >= 74 && cur_lumi_block <= 74) ||
	  (cur_lumi_block >= 437 && cur_lumi_block <= 440) ||
	  (cur_lumi_block >= 463 && cur_lumi_block <= 464) ||
	  (cur_lumi_block >= 676 && cur_lumi_block <= 677) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 601 && cur_lumi_block <= 619) ||
	  (cur_lumi_block >= 243 && cur_lumi_block <= 244) ||
	  (cur_lumi_block >= 841 && cur_lumi_block <= 847) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 175 && cur_lumi_block <= 176) ||
	  (cur_lumi_block >= 113 && cur_lumi_block <= 115) ||
	  (cur_lumi_block >= 35 && cur_lumi_block <= 35) ||
	  (cur_lumi_block >= 189 && cur_lumi_block <= 194) ||
	  (cur_lumi_block >= 579 && cur_lumi_block <= 588) ||
	  (cur_lumi_block >= 295 && cur_lumi_block <= 296) ||
	  (cur_lumi_block >= 287 && cur_lumi_block <= 292) ||
	  (cur_lumi_block >= 263 && cur_lumi_block <= 272) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 91) ||
	  (cur_lumi_block >= 475 && cur_lumi_block <= 476) ||
	  (cur_lumi_block >= 167 && cur_lumi_block <= 167) ||
	  (cur_lumi_block >= 48 && cur_lumi_block <= 51) ||
	  (cur_lumi_block >= 701 && cur_lumi_block <= 702) ||
	  (cur_lumi_block >= 467 && cur_lumi_block <= 468) ||
	  (cur_lumi_block >= 413 && cur_lumi_block <= 418) ||
	  (cur_lumi_block >= 820 && cur_lumi_block <= 827) ||
	  (cur_lumi_block >= 764 && cur_lumi_block <= 769) ||
	  (cur_lumi_block >= 93 && cur_lumi_block <= 99) ||
	  (cur_lumi_block >= 275 && cur_lumi_block <= 276) ||
	  (cur_lumi_block >= 157 && cur_lumi_block <= 158) ||
	  (cur_lumi_block >= 541 && cur_lumi_block <= 542) ||
	  (cur_lumi_block >= 513 && cur_lumi_block <= 524) ||
	  (cur_lumi_block >= 123 && cur_lumi_block <= 128) ||
	  (cur_lumi_block >= 817 && cur_lumi_block <= 817) ||
	  (cur_lumi_block >= 199 && cur_lumi_block <= 200) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 59) ||
	  (cur_lumi_block >= 684 && cur_lumi_block <= 685) ||
	  (cur_lumi_block >= 153 && cur_lumi_block <= 153) ||
	  (cur_lumi_block >= 688 && cur_lumi_block <= 689) ||
	  (cur_lumi_block >= 431 && cur_lumi_block <= 432) ||
	  (cur_lumi_block >= 393 && cur_lumi_block <= 398) ||
	  (cur_lumi_block >= 409 && cur_lumi_block <= 410) ||
	  (cur_lumi_block >= 229 && cur_lumi_block <= 240) ||
	  (cur_lumi_block >= 101 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 507 && cur_lumi_block <= 510) ||
	  (cur_lumi_block >= 449 && cur_lumi_block <= 452) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 573 && cur_lumi_block <= 576) ||
	  (cur_lumi_block >= 311 && cur_lumi_block <= 324) ||
	  (cur_lumi_block >= 247 && cur_lumi_block <= 253) ||
	  (cur_lumi_block >= 693 && cur_lumi_block <= 696) ||
	  (cur_lumi_block >= 503 && cur_lumi_block <= 504) ||
	  (cur_lumi_block >= 729 && cur_lumi_block <= 733) ||
	  (cur_lumi_block >= 808 && cur_lumi_block <= 811) ||
	  (cur_lumi_block >= 401 && cur_lumi_block <= 406) ||
	  (cur_lumi_block >= 327 && cur_lumi_block <= 340) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 4) ||
	  (cur_lumi_block >= 375 && cur_lumi_block <= 376) ||
	  (cur_lumi_block >= 136 && cur_lumi_block <= 145) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 33) ||
	  (cur_lumi_block >= 761 && cur_lumi_block <= 761) ||
	  (cur_lumi_block >= 82 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 640 && cur_lumi_block <= 649) ||
	  (cur_lumi_block >= 147 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 662 && cur_lumi_block <= 672) ||
	  (cur_lumi_block >= 28 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 283 && cur_lumi_block <= 284) ||
	  (cur_lumi_block >= 800 && cur_lumi_block <= 802) ||
	  (cur_lumi_block >= 749 && cur_lumi_block <= 750) ||
	  (cur_lumi_block >= 471 && cur_lumi_block <= 472) ||
	  (cur_lumi_block >= 622 && cur_lumi_block <= 625) ||
	  (cur_lumi_block >= 361 && cur_lumi_block <= 362) ||
	  (cur_lumi_block >= 345 && cur_lumi_block <= 352) ||
	  (cur_lumi_block >= 545 && cur_lumi_block <= 548) ||
	  (cur_lumi_block >= 24 && cur_lumi_block <= 26) ||
	  (cur_lumi_block >= 721 && cur_lumi_block <= 726) ||
	  (cur_lumi_block >= 130 && cur_lumi_block <= 134) ||
	  (cur_lumi_block >= 756 && cur_lumi_block <= 756) ||
	  (cur_lumi_block >= 217 && cur_lumi_block <= 226) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 56) ||
	  (cur_lumi_block >= 385 && cur_lumi_block <= 388) ||
	  (cur_lumi_block >= 773 && cur_lumi_block <= 775) ||
	  (cur_lumi_block >= 813 && cur_lumi_block <= 814) ||
	  (cur_lumi_block >= 491 && cur_lumi_block <= 498) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 79) ||
	  (cur_lumi_block >= 628 && cur_lumi_block <= 635) ||
	  (cur_lumi_block >= 205 && cur_lumi_block <= 210) ||
	  (cur_lumi_block >= 715 && cur_lumi_block <= 715) ||
	  (cur_lumi_block >= 754 && cur_lumi_block <= 754) ||
	  (cur_lumi_block >= 487 && cur_lumi_block <= 488) ||
	  (cur_lumi_block >= 301 && cur_lumi_block <= 308) ||
	  (cur_lumi_block >= 553 && cur_lumi_block <= 565) ||
	  (cur_lumi_block >= 160 && cur_lumi_block <= 162) ||
	  (cur_lumi_block >= 569 && cur_lumi_block <= 571) ||
	  (cur_lumi_block >= 425 && cur_lumi_block <= 426) ||
	  (cur_lumi_block >= 279 && cur_lumi_block <= 280) ||
	  (cur_lumi_block >= 367 && cur_lumi_block <= 372) ||
	  (cur_lumi_block >= 591 && cur_lumi_block <= 592) ||
	  (cur_lumi_block >= 652 && cur_lumi_block <= 659) ||
	  (cur_lumi_block >= 483 && cur_lumi_block <= 484) ||
	  (cur_lumi_block >= 528 && cur_lumi_block <= 533) ||
	  (cur_lumi_block >= 736 && cur_lumi_block <= 738) ||
	  (cur_lumi_block >= 711 && cur_lumi_block <= 712) ||
	  (cur_lumi_block >= 357 && cur_lumi_block <= 358) ||
	  (cur_lumi_block >= 169 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 164) ||
	  (cur_lumi_block >= 37 && cur_lumi_block <= 46) ||
	  (cur_lumi_block >= 791 && cur_lumi_block <= 797) ||
	  (cur_lumi_block >= 117 && cur_lumi_block <= 119) ||
	  (cur_lumi_block >= 705 && cur_lumi_block <= 708) ||
	  (cur_lumi_block >= 698 && cur_lumi_block <= 699) ) ) ||
	( cur_run_number == 191367 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 2) ) ) ||
	( cur_run_number == 191411 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 23) ) ) ||
	( cur_run_number == 191695 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ) ) ||
	( cur_run_number == 191718 && (
	  (cur_lumi_block >= 77 && cur_lumi_block <= 86) ||
	  (cur_lumi_block >= 101 && cur_lumi_block <= 126) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 75) ||
	  (cur_lumi_block >= 88 && cur_lumi_block <= 95) ||
	  (cur_lumi_block >= 128 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 183 && cur_lumi_block <= 207) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 99) ||
	  (cur_lumi_block >= 43 && cur_lumi_block <= 57) ) ) ||
	( cur_run_number == 191720 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 32) ||
	  (cur_lumi_block >= 63 && cur_lumi_block <= 74) ||
	  (cur_lumi_block >= 21 && cur_lumi_block <= 24) ||
	  (cur_lumi_block >= 26 && cur_lumi_block <= 26) ||
	  (cur_lumi_block >= 99 && cur_lumi_block <= 101) ||
	  (cur_lumi_block >= 93 && cur_lumi_block <= 97) ||
	  (cur_lumi_block >= 157 && cur_lumi_block <= 158) ||
	  (cur_lumi_block >= 149 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 61) ||
	  (cur_lumi_block >= 153 && cur_lumi_block <= 154) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 56) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 81) ||
	  (cur_lumi_block >= 17 && cur_lumi_block <= 19) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 28 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 91) ||
	  (cur_lumi_block >= 124 && cur_lumi_block <= 124) ||
	  (cur_lumi_block >= 104 && cur_lumi_block <= 115) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 147) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 6) ||
	  (cur_lumi_block >= 160 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 14) ||
	  (cur_lumi_block >= 34 && cur_lumi_block <= 50) ||
	  (cur_lumi_block >= 117 && cur_lumi_block <= 122) ) ) ||
	( cur_run_number == 191721 && (
	  (cur_lumi_block >= 61 && cur_lumi_block <= 183) ||
	  (cur_lumi_block >= 31 && cur_lumi_block <= 34) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 189) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 36 && cur_lumi_block <= 58) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 19) ) ) ||
	( cur_run_number == 191810 && (
	  (cur_lumi_block >= 15 && cur_lumi_block <= 15) ||
	  (cur_lumi_block >= 52 && cur_lumi_block <= 60) ||
	  (cur_lumi_block >= 62 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 92) ||
	  (cur_lumi_block >= 22 && cur_lumi_block <= 41) ||
	  (cur_lumi_block >= 43 && cur_lumi_block <= 49) ) ) ||
	( cur_run_number == 191830 && (
	  (cur_lumi_block >= 391 && cur_lumi_block <= 391) ||
	  (cur_lumi_block >= 99 && cur_lumi_block <= 101) ||
	  (cur_lumi_block >= 74 && cur_lumi_block <= 77) ||
	  (cur_lumi_block >= 368 && cur_lumi_block <= 369) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 67) ||
	  (cur_lumi_block >= 304 && cur_lumi_block <= 366) ||
	  (cur_lumi_block >= 81 && cur_lumi_block <= 97) ||
	  (cur_lumi_block >= 393 && cur_lumi_block <= 393) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 371 && cur_lumi_block <= 383) ||
	  (cur_lumi_block >= 389 && cur_lumi_block <= 389) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 242) ||
	  (cur_lumi_block >= 69 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 104 && cur_lumi_block <= 142) ||
	  (cur_lumi_block >= 385 && cur_lumi_block <= 386) ||
	  (cur_lumi_block >= 245 && cur_lumi_block <= 301) ||
	  (cur_lumi_block >= 79 && cur_lumi_block <= 79) ) ) ||
	( cur_run_number == 191833 && (
	  (cur_lumi_block >= 102 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 7 && cur_lumi_block <= 10) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 82) ||
	  (cur_lumi_block >= 84 && cur_lumi_block <= 84) ||
	  (cur_lumi_block >= 89 && cur_lumi_block <= 93) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 64) ||
	  (cur_lumi_block >= 41 && cur_lumi_block <= 49) ||
	  (cur_lumi_block >= 12 && cur_lumi_block <= 39) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 68) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 87) ||
	  (cur_lumi_block >= 70 && cur_lumi_block <= 70) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 1) ||
	  (cur_lumi_block >= 100 && cur_lumi_block <= 100) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 56) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 5) ||
	  (cur_lumi_block >= 51 && cur_lumi_block <= 51) ) ) ||
	( cur_run_number == 191834 && (
	  (cur_lumi_block >= 33 && cur_lumi_block <= 52) ||
	  (cur_lumi_block >= 90 && cur_lumi_block <= 90) ||
	  (cur_lumi_block >= 63 && cur_lumi_block <= 70) ||
	  (cur_lumi_block >= 7 && cur_lumi_block <= 10) ||
	  (cur_lumi_block >= 324 && cur_lumi_block <= 332) ||
	  (cur_lumi_block >= 180 && cur_lumi_block <= 181) ||
	  (cur_lumi_block >= 244 && cur_lumi_block <= 244) ||
	  (cur_lumi_block >= 162 && cur_lumi_block <= 163) ||
	  (cur_lumi_block >= 72 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 351 && cur_lumi_block <= 352) ||
	  (cur_lumi_block >= 255 && cur_lumi_block <= 277) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 28) ||
	  (cur_lumi_block >= 297 && cur_lumi_block <= 299) ||
	  (cur_lumi_block >= 55 && cur_lumi_block <= 58) ||
	  (cur_lumi_block >= 240 && cur_lumi_block <= 242) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 61) ||
	  (cur_lumi_block >= 108 && cur_lumi_block <= 109) ||
	  (cur_lumi_block >= 220 && cur_lumi_block <= 220) ||
	  (cur_lumi_block >= 115 && cur_lumi_block <= 121) ||
	  (cur_lumi_block >= 231 && cur_lumi_block <= 233) ||
	  (cur_lumi_block >= 148 && cur_lumi_block <= 148) ||
	  (cur_lumi_block >= 113 && cur_lumi_block <= 113) ||
	  (cur_lumi_block >= 225 && cur_lumi_block <= 226) ||
	  (cur_lumi_block >= 207 && cur_lumi_block <= 207) ||
	  (cur_lumi_block >= 347 && cur_lumi_block <= 349) ||
	  (cur_lumi_block >= 295 && cur_lumi_block <= 295) ||
	  (cur_lumi_block >= 93 && cur_lumi_block <= 99) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 83) ||
	  (cur_lumi_block >= 106 && cur_lumi_block <= 106) ||
	  (cur_lumi_block >= 123 && cur_lumi_block <= 124) ||
	  (cur_lumi_block >= 291 && cur_lumi_block <= 293) ||
	  (cur_lumi_block >= 210 && cur_lumi_block <= 214) ||
	  (cur_lumi_block >= 12 && cur_lumi_block <= 12) ||
	  (cur_lumi_block >= 153 && cur_lumi_block <= 160) ||
	  (cur_lumi_block >= 302 && cur_lumi_block <= 307) ||
	  (cur_lumi_block >= 321 && cur_lumi_block <= 322) ||
	  (cur_lumi_block >= 229 && cur_lumi_block <= 229) ||
	  (cur_lumi_block >= 101 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 86) ||
	  (cur_lumi_block >= 309 && cur_lumi_block <= 312) ||
	  (cur_lumi_block >= 289 && cur_lumi_block <= 289) ||
	  (cur_lumi_block >= 165 && cur_lumi_block <= 169) ||
	  (cur_lumi_block >= 198 && cur_lumi_block <= 200) ||
	  (cur_lumi_block >= 188 && cur_lumi_block <= 189) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 5) ||
	  (cur_lumi_block >= 88 && cur_lumi_block <= 88) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 186) ||
	  (cur_lumi_block >= 144 && cur_lumi_block <= 146) ||
	  (cur_lumi_block >= 30 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 222 && cur_lumi_block <= 223) ||
	  (cur_lumi_block >= 280 && cur_lumi_block <= 286) ||
	  (cur_lumi_block >= 250 && cur_lumi_block <= 250) ||
	  (cur_lumi_block >= 14 && cur_lumi_block <= 14) ||
	  (cur_lumi_block >= 191 && cur_lumi_block <= 196) ||
	  (cur_lumi_block >= 150 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 104 && cur_lumi_block <= 104) ||
	  (cur_lumi_block >= 131 && cur_lumi_block <= 142) ||
	  (cur_lumi_block >= 314 && cur_lumi_block <= 318) ||
	  (cur_lumi_block >= 205 && cur_lumi_block <= 205) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 126) ||
	  (cur_lumi_block >= 183 && cur_lumi_block <= 183) ||
	  (cur_lumi_block >= 111 && cur_lumi_block <= 111) ||
	  (cur_lumi_block >= 335 && cur_lumi_block <= 344) ||
	  (cur_lumi_block >= 216 && cur_lumi_block <= 217) ||
	  (cur_lumi_block >= 171 && cur_lumi_block <= 178) ) ) ||
	( cur_run_number == 191837 && (
	  (cur_lumi_block >= 32 && cur_lumi_block <= 44) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 65) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 30) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 53) ) ) ||
	( cur_run_number == 191856 && (
	  (cur_lumi_block >= 103 && cur_lumi_block <= 124) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 92) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 74) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 133) ||
	  (cur_lumi_block >= 94 && cur_lumi_block <= 101) ) ) ||
	( cur_run_number == 191859 && (
	  (cur_lumi_block >= 31 && cur_lumi_block <= 125) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 28) ) ) ||
	( cur_run_number == 193093 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 33) ) ) ||
	( cur_run_number == 193123 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 27) ) ) ||
	( cur_run_number == 193124 && (
	  (cur_lumi_block >= 1 && cur_lumi_block <= 52) ) ) ||
	( cur_run_number == 193192 && (
	  (cur_lumi_block >= 71 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 58 && cur_lumi_block <= 58) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 76) ) ) ||
	( cur_run_number == 193193 && (
	  (cur_lumi_block >= 276 && cur_lumi_block <= 344) ||
	  (cur_lumi_block >= 102 && cur_lumi_block <= 120) ||
	  (cur_lumi_block >= 162 && cur_lumi_block <= 274) ||
	  (cur_lumi_block >= 11 && cur_lumi_block <= 83) ||
	  (cur_lumi_block >= 86 && cur_lumi_block <= 100) ||
	  (cur_lumi_block >= 346 && cur_lumi_block <= 495) ||
	  (cur_lumi_block >= 1 && cur_lumi_block <= 6) ||
	  (cur_lumi_block >= 122 && cur_lumi_block <= 160) ||
	  (cur_lumi_block >= 497 && cur_lumi_block <= 506) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 8) ) ) ||
	( cur_run_number == 193207 && (
	  (cur_lumi_block >= 64 && cur_lumi_block <= 151) ||
	  (cur_lumi_block >= 153 && cur_lumi_block <= 182) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 54) ) ) ||
	( cur_run_number == 193334 && (
	  (cur_lumi_block >= 80 && cur_lumi_block <= 95) ||
	  (cur_lumi_block >= 119 && cur_lumi_block <= 122) ||
	  (cur_lumi_block >= 31 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 106 && cur_lumi_block <= 115) ||
	  (cur_lumi_block >= 29 && cur_lumi_block <= 29) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 78) ||
	  (cur_lumi_block >= 145 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 124 && cur_lumi_block <= 124) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 142) ||
	  (cur_lumi_block >= 98 && cur_lumi_block <= 103) ) ) ||
	( cur_run_number == 193336 && (
	  (cur_lumi_block >= 63 && cur_lumi_block <= 66) ||
	  (cur_lumi_block >= 796 && cur_lumi_block <= 799) ||
	  (cur_lumi_block >= 179 && cur_lumi_block <= 184) ||
	  (cur_lumi_block >= 880 && cur_lumi_block <= 882) ||
	  (cur_lumi_block >= 16 && cur_lumi_block <= 18) ||
	  (cur_lumi_block >= 790 && cur_lumi_block <= 794) ||
	  (cur_lumi_block >= 27 && cur_lumi_block <= 29) ||
	  (cur_lumi_block >= 161 && cur_lumi_block <= 164) ||
	  (cur_lumi_block >= 230 && cur_lumi_block <= 264) ||
	  (cur_lumi_block >= 115 && cur_lumi_block <= 118) ||
	  (cur_lumi_block >= 313 && cur_lumi_block <= 393) ||
	  (cur_lumi_block >= 20 && cur_lumi_block <= 20) ||
	  (cur_lumi_block >= 888 && cur_lumi_block <= 893) ||
	  (cur_lumi_block >= 151 && cur_lumi_block <= 156) ||
	  (cur_lumi_block >= 148 && cur_lumi_block <= 149) ||
	  (cur_lumi_block >= 35 && cur_lumi_block <= 36) ||
	  (cur_lumi_block >= 189 && cur_lumi_block <= 192) ||
	  (cur_lumi_block >= 511 && cur_lumi_block <= 638) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 80) ||
	  (cur_lumi_block >= 133 && cur_lumi_block <= 136) ||
	  (cur_lumi_block >= 39 && cur_lumi_block <= 42) ||
	  (cur_lumi_block >= 138 && cur_lumi_block <= 142) ||
	  (cur_lumi_block >= 226 && cur_lumi_block <= 227) ||
	  (cur_lumi_block >= 396 && cur_lumi_block <= 477) ||
	  (cur_lumi_block >= 861 && cur_lumi_block <= 878) ||
	  (cur_lumi_block >= 774 && cur_lumi_block <= 779) ||
	  (cur_lumi_block >= 101 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 309 && cur_lumi_block <= 310) ||
	  (cur_lumi_block >= 911 && cur_lumi_block <= 941) ||
	  (cur_lumi_block >= 495 && cur_lumi_block <= 509) ||
	  (cur_lumi_block >= 166 && cur_lumi_block <= 166) ||
	  (cur_lumi_block >= 186 && cur_lumi_block <= 187) ||
	  (cur_lumi_block >= 761 && cur_lumi_block <= 767) ||
	  (cur_lumi_block >= 732 && cur_lumi_block <= 754) ||
	  (cur_lumi_block >= 640 && cur_lumi_block <= 684) ||
	  (cur_lumi_block >= 804 && cur_lumi_block <= 857) ||
	  (cur_lumi_block >= 283 && cur_lumi_block <= 306) ||
	  (cur_lumi_block >= 83 && cur_lumi_block <= 90) ||
	  (cur_lumi_block >= 895 && cur_lumi_block <= 909) ||
	  (cur_lumi_block >= 783 && cur_lumi_block <= 783) ||
	  (cur_lumi_block >= 14 && cur_lumi_block <= 14) ||
	  (cur_lumi_block >= 69 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 60) ||
	  (cur_lumi_block >= 687 && cur_lumi_block <= 698) ||
	  (cur_lumi_block >= 145 && cur_lumi_block <= 145) ||
	  (cur_lumi_block >= 124 && cur_lumi_block <= 128) ||
	  (cur_lumi_block >= 53 && cur_lumi_block <= 56) ||
	  (cur_lumi_block >= 703 && cur_lumi_block <= 729) ||
	  (cur_lumi_block >= 267 && cur_lumi_block <= 268) ||
	  (cur_lumi_block >= 801 && cur_lumi_block <= 802) ||
	  (cur_lumi_block >= 121 && cur_lumi_block <= 122) ||
	  (cur_lumi_block >= 105 && cur_lumi_block <= 108) ||
	  (cur_lumi_block >= 479 && cur_lumi_block <= 492) ||
	  (cur_lumi_block >= 3 && cur_lumi_block <= 4) ||
	  (cur_lumi_block >= 94 && cur_lumi_block <= 98) ||
	  (cur_lumi_block >= 111 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 8 && cur_lumi_block <= 11) ||
	  (cur_lumi_block >= 947 && cur_lumi_block <= 951) ||
	  (cur_lumi_block >= 169 && cur_lumi_block <= 176) ||
	  (cur_lumi_block >= 270 && cur_lumi_block <= 280) ||
	  (cur_lumi_block >= 195 && cur_lumi_block <= 224) ) ) ||
	( cur_run_number == 193541 && (
	  (cur_lumi_block >= 554 && cur_lumi_block <= 557) ||
	  (cur_lumi_block >= 560 && cur_lumi_block <= 575) ||
	  (cur_lumi_block >= 522 && cur_lumi_block <= 525) ||
	  (cur_lumi_block >= 103 && cur_lumi_block <= 253) ||
	  (cur_lumi_block >= 578 && cur_lumi_block <= 602) ||
	  (cur_lumi_block >= 612 && cur_lumi_block <= 619) ||
	  (cur_lumi_block >= 468 && cur_lumi_block <= 473) ||
	  (cur_lumi_block >= 492 && cur_lumi_block <= 519) ||
	  (cur_lumi_block >= 330 && cur_lumi_block <= 335) ||
	  (cur_lumi_block >= 295 && cur_lumi_block <= 311) ||
	  (cur_lumi_block >= 394 && cur_lumi_block <= 399) ||
	  (cur_lumi_block >= 438 && cur_lumi_block <= 441) ||
	  (cur_lumi_block >= 77 && cur_lumi_block <= 101) ||
	  (cur_lumi_block >= 444 && cur_lumi_block <= 461) ||
	  (cur_lumi_block >= 476 && cur_lumi_block <= 483) ||
	  (cur_lumi_block >= 606 && cur_lumi_block <= 608) ||
	  (cur_lumi_block >= 402 && cur_lumi_block <= 413) ||
	  (cur_lumi_block >= 416 && cur_lumi_block <= 431) ||
	  (cur_lumi_block >= 338 && cur_lumi_block <= 341) ||
	  (cur_lumi_block >= 314 && cur_lumi_block <= 327) ||
	  (cur_lumi_block >= 464 && cur_lumi_block <= 465) ||
	  (cur_lumi_block >= 344 && cur_lumi_block <= 361) ||
	  (cur_lumi_block >= 364 && cur_lumi_block <= 391) ||
	  (cur_lumi_block >= 486 && cur_lumi_block <= 489) ||
	  (cur_lumi_block >= 528 && cur_lumi_block <= 543) ||
	  (cur_lumi_block >= 256 && cur_lumi_block <= 292) ) ) ||
	( cur_run_number == 193556 && (
	  (cur_lumi_block >= 61 && cur_lumi_block <= 62) ||
	  (cur_lumi_block >= 64 && cur_lumi_block <= 65) ||
	  (cur_lumi_block >= 41 && cur_lumi_block <= 49) ||
	  (cur_lumi_block >= 56 && cur_lumi_block <= 57) ||
	  (cur_lumi_block >= 76 && cur_lumi_block <= 82) ||
	  (cur_lumi_block >= 54 && cur_lumi_block <= 54) ||
	  (cur_lumi_block >= 67 && cur_lumi_block <= 67) ||
	  (cur_lumi_block >= 69 && cur_lumi_block <= 72) ||
	  (cur_lumi_block >= 59 && cur_lumi_block <= 59) ) ) ||
	( cur_run_number == 193557 && (
	  (cur_lumi_block >= 2 && cur_lumi_block <= 3) ||
	  (cur_lumi_block >= 14 && cur_lumi_block <= 45) ||
	  (cur_lumi_block >= 6 && cur_lumi_block <= 10) ||
	  (cur_lumi_block >= 47 && cur_lumi_block <= 84) ) ) ||
	( cur_run_number == 193575 && (
	  (cur_lumi_block >= 660 && cur_lumi_block <= 669) ||
	  (cur_lumi_block >= 524 && cur_lumi_block <= 531) ||
	  (cur_lumi_block >= 102 && cur_lumi_block <= 102) ||
	  (cur_lumi_block >= 80 && cur_lumi_block <= 87) ||
	  (cur_lumi_block >= 180 && cur_lumi_block <= 205) ||
	  (cur_lumi_block >= 500 && cur_lumi_block <= 507) ||
	  (cur_lumi_block >= 351 && cur_lumi_block <= 360) ||
	  (cur_lumi_block >= 61 && cur_lumi_block <= 61) ||
	  (cur_lumi_block >= 534 && cur_lumi_block <= 658) ||
	  (cur_lumi_block >= 299 && cur_lumi_block <= 300) ||
	  (cur_lumi_block >= 115 && cur_lumi_block <= 120) ||
	  (cur_lumi_block >= 277 && cur_lumi_block <= 281) ||
	  (cur_lumi_block >= 379 && cur_lumi_block <= 394) ||
	  (cur_lumi_block >= 89 && cur_lumi_block <= 91) ||
	  (cur_lumi_block >= 207 && cur_lumi_block <= 207) ||
	  (cur_lumi_block >= 142 && cur_lumi_block <= 150) ||
	  (cur_lumi_block >= 295 && cur_lumi_block <= 297) ||
	  (cur_lumi_block >= 78 && cur_lumi_block <= 78) ||
	  (cur_lumi_block >= 48 && cur_lumi_block <= 56) ||
	  (cur_lumi_block >= 434 && cur_lumi_block <= 497) ||
	  (cur_lumi_block >= 397 && cur_lumi_block <= 399) ||
	  (cur_lumi_block >= 672 && cur_lumi_block <= 685) ||
	  (cur_lumi_block >= 514 && cur_lumi_block <= 521) ||
	  (cur_lumi_block >= 93 && cur_lumi_block <= 100) ||
	  (cur_lumi_block >= 422 && cur_lumi_block <= 431) ||
	  (cur_lumi_block >= 291 && cur_lumi_block <= 292) ||
	  (cur_lumi_block >= 417 && cur_lumi_block <= 419) ||
	  (cur_lumi_block >= 153 && cur_lumi_block <= 153) ||
	  (cur_lumi_block >= 688 && cur_lumi_block <= 701) ||
	  (cur_lumi_block >= 510 && cur_lumi_block <= 511) ||
	  (cur_lumi_block >= 73 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 69) ||
	  (cur_lumi_block >= 284 && cur_lumi_block <= 289) ||
	  (cur_lumi_block >= 402 && cur_lumi_block <= 405) ||
	  (cur_lumi_block >= 320 && cur_lumi_block <= 336) ||
	  (cur_lumi_block >= 738 && cur_lumi_block <= 752) ||
	  (cur_lumi_block >= 339 && cur_lumi_block <= 349) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 76) ||
	  (cur_lumi_block >= 303 && cur_lumi_block <= 317) ||
	  (cur_lumi_block >= 135 && cur_lumi_block <= 140) ||
	  (cur_lumi_block >= 104 && cur_lumi_block <= 113) ||
	  (cur_lumi_block >= 155 && cur_lumi_block <= 162) ||
	  (cur_lumi_block >= 412 && cur_lumi_block <= 415) ||
	  (cur_lumi_block >= 703 && cur_lumi_block <= 735) ||
	  (cur_lumi_block >= 122 && cur_lumi_block <= 124) ||
	  (cur_lumi_block >= 363 && cur_lumi_block <= 372) ||
	  (cur_lumi_block >= 126 && cur_lumi_block <= 133) ||
	  (cur_lumi_block >= 253 && cur_lumi_block <= 253) ||
	  (cur_lumi_block >= 176 && cur_lumi_block <= 178) ||
	  (cur_lumi_block >= 408 && cur_lumi_block <= 409) ||
	  (cur_lumi_block >= 209 && cur_lumi_block <= 251) ||
	  (cur_lumi_block >= 256 && cur_lumi_block <= 275) ||
	  (cur_lumi_block >= 164 && cur_lumi_block <= 173) ) ) ||
	( cur_run_number == 193621 && (
	  (cur_lumi_block >= 276 && cur_lumi_block <= 282) ||
	  (cur_lumi_block >= 118 && cur_lumi_block <= 133) ||
	  (cur_lumi_block >= 71 && cur_lumi_block <= 73) ||
	  (cur_lumi_block >= 679 && cur_lumi_block <= 682) ||
	  (cur_lumi_block >= 546 && cur_lumi_block <= 549) ||
	  (cur_lumi_block >= 227 && cur_lumi_block <= 230) ||
	  (cur_lumi_block >= 80 && cur_lumi_block <= 89) ||
	  (cur_lumi_block >= 193 && cur_lumi_block <= 195) ||
	  (cur_lumi_block >= 560 && cur_lumi_block <= 561) ||
	  (cur_lumi_block >= 647 && cur_lumi_block <= 652) ||
	  (cur_lumi_block >= 377 && cur_lumi_block <= 434) ||
	  (cur_lumi_block >= 109 && cur_lumi_block <= 112) ||
	  (cur_lumi_block >= 871 && cur_lumi_block <= 884) ||
	  (cur_lumi_block >= 841 && cur_lumi_block <= 846) ||
	  (cur_lumi_block >= 717 && cur_lumi_block <= 718) ||
	  (cur_lumi_block >= 551 && cur_lumi_block <= 555) ||
	  (cur_lumi_block >= 232 && cur_lumi_block <= 235) ||
	  (cur_lumi_block >= 148 && cur_lumi_block <= 150) ||
	  (cur_lumi_block >= 557 && cur_lumi_block <= 558) ||
	  (cur_lumi_block >= 1139 && cur_lumi_block <= 1193) ||
	  (cur_lumi_block >= 849 && cur_lumi_block <= 862) ||
	  (cur_lumi_block >= 492 && cur_lumi_block <= 493) ||
	  (cur_lumi_block >= 342 && cur_lumi_block <= 345) ||
	  (cur_lumi_block >= 700 && cur_lumi_block <= 703) ||
	  (cur_lumi_block >= 347 && cur_lumi_block <= 368) ||
	  (cur_lumi_block >= 91 && cur_lumi_block <= 94) ||
	  (cur_lumi_block >= 1056 && cur_lumi_block <= 1137) ||
	  (cur_lumi_block >= 467 && cur_lumi_block <= 489) ||
	  (cur_lumi_block >= 742 && cur_lumi_block <= 745) ||
	  (cur_lumi_block >= 214 && cur_lumi_block <= 216) ||
	  (cur_lumi_block >= 725 && cur_lumi_block <= 726) ||
	  (cur_lumi_block >= 663 && cur_lumi_block <= 665) ||
	  (cur_lumi_block >= 197 && cur_lumi_block <= 201) ||
	  (cur_lumi_block >= 563 && cur_lumi_block <= 570) ||
	  (cur_lumi_block >= 261 && cur_lumi_block <= 263) ||
	  (cur_lumi_block >= 712 && cur_lumi_block <= 712) ||
	  (cur_lumi_block >= 374 && cur_lumi_block <= 374) ||
	  (cur_lumi_block >= 114 && cur_lumi_block <= 116) ||
	  (cur_lumi_block >= 751 && cur_lumi_block <= 754) ||
	  (cur_lumi_block >= 731 && cur_lumi_block <= 732) ||
	  (cur_lumi_block >= 606 && cur_lumi_block <= 619) ||
	  (cur_lumi_block >= 211 && cur_lumi_block <= 211) ||
	  (cur_lumi_block >= 153 && cur_lumi_block <= 153) ||
	  (cur_lumi_block >= 688 && cur_lumi_block <= 691) ||
	  (cur_lumi_block >= 527 && cur_lumi_block <= 540) ||
	  (cur_lumi_block >= 60 && cur_lumi_block <= 60) ||
	  (cur_lumi_block >= 66 && cur_lumi_block <= 66) ||
	  (cur_lumi_block >= 340 && cur_lumi_block <= 340) ||
	  (cur_lumi_block >= 284 && cur_lumi_block <= 317) ||
	  (cur_lumi_block >= 903 && cur_lumi_block <= 906) ||
	  (cur_lumi_block >= 237 && cur_lumi_block <= 259) ||
	  (cur_lumi_block >= 519 && cur_lumi_block <= 525) ||
	  (cur_lumi_block >= 370 && cur_lumi_block <= 372) ||
	  (cur_lumi_block >= 204 && cur_lumi_block <= 209) ||
	  (cur_lumi_block >= 812 && cur_lumi_block <= 822) ||
	  (cur_lumi_block >= 795 && cur_lumi_block <= 796) ||
	  (cur_lumi_block >= 1195 && cur_lumi_block <= 1371) ||
	  (cur_lumi_block >= 825 && cur_lumi_block <= 826) ||
	  (cur_lumi_block >= 677 && cur_lumi_block <= 677) ||
	  (cur_lumi_block >= 641 && cur_lumi_block <= 642) ||
	  (cur_lumi_block >= 893 && cur_lumi_block <= 898) ||
	  (cur_lumi_block >= 909 && cur_lumi_block <= 910) ||
	  (cur_lumi_block >= 979 && cur_lumi_block <= 1053) ||
	  (cur_lumi_block >= 674 && cur_lumi_block <= 675) ||
	  (cur_lumi_block >= 913 && cur_lumi_block <= 920) ||
	  (cur_lumi_block >= 333 && cur_lumi_block <= 338) ||
	  (cur_lumi_block >= 156 && cur_lumi_block <= 170) ||
	  (cur_lumi_block >= 75 && cur_lumi_block <= 78) ||
	  (cur_lumi_block >= 783 && cur_lumi_block <= 784) ||
	  (cur_lumi_block >= 706 && cur_lumi_block <= 709) ||
	  (cur_lumi_block >= 622 && cur_lumi_block <= 638) ||
	  (cur_lumi_block >= 218 && cur_lumi_block <= 225) ||
	  (cur_lumi_block >= 829 && cur_lumi_block <= 836) ||
	  (cur_lumi_block >= 135 && cur_lumi_block <= 146) ||
	  (cur_lumi_block >= 319 && cur_lumi_block <= 331) ||
	  (cur_lumi_block >= 191 && cur_lumi_block <= 191) ||
	  (cur_lumi_block >= 172 && cur_lumi_block <= 172) ||
	  (cur_lumi_block >= 496 && cur_lumi_block <= 511) ||
	  (cur_lumi_block >= 735 && cur_lumi_block <= 735) ||
	  (cur_lumi_block >= 772 && cur_lumi_block <= 780) ||
	  (cur_lumi_block >= 721 && cur_lumi_block <= 722) ||
	  (cur_lumi_block >= 516 && cur_lumi_block <= 516) ||
	  (cur_lumi_block >= 1373 && cur_lumi_block <= 1654) ||
	  (cur_lumi_block >= 543 && cur_lumi_block <= 544) ||
	  (cur_lumi_block >= 269 && cur_lumi_block <= 273) ||
	  (cur_lumi_block >= 759 && cur_lumi_block <= 766) ||
	  (cur_lumi_block >= 799 && cur_lumi_block <= 806) ||
	  (cur_lumi_block >= 96 && cur_lumi_block <= 107) ||
	  (cur_lumi_block >= 925 && cur_lumi_block <= 976) ||
	  (cur_lumi_block >= 694 && cur_lumi_block <= 697) ||
	  (cur_lumi_block >= 436 && cur_lumi_block <= 465) ||
	  (cur_lumi_block >= 809 && cur_lumi_block <= 809) ||
	  (cur_lumi_block >= 176 && cur_lumi_block <= 189) ||
	  (cur_lumi_block >= 748 && cur_lumi_block <= 748) ||
	  (cur_lumi_block >= 265 && cur_lumi_block <= 267) ||
	  (cur_lumi_block >= 791 && cur_lumi_block <= 792) ||
	  (cur_lumi_block >= 686 && cur_lumi_block <= 686) ||
	  (cur_lumi_block >= 574 && cur_lumi_block <= 601) ) ) )
		return true;
	else
		return false;
	// JSON AUTO FILL END -- do not edit this comment
	
	return true;
}

void RunSelector::accept_all(bool accept)
{
	this->accept_all_events = accept;
}
