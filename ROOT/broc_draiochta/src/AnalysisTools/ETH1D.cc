#include "../../interface/AnalysisTools/ETH1D.h"

double eire::TH1D::global_weight = 1;

eire::TH1D::TH1D(const char *name,const char *title,Int_t nbins,const Double_t *xbins) : ::TH1D(name,title,nbins,xbins)
{
}

eire::TH1D::TH1D(const char *name,const char *title,Int_t nbins,const Float_t *xbins) : ::TH1D(name,title,nbins,xbins)
{
}

eire::TH1D::TH1D(const char *name,const char *title,Int_t nbins,Double_t xlow,Double_t xup) : ::TH1D(name,title,nbins,xlow,xup)
{
}

void eire::TH1D::set_global_weight(double weight)
{
	eire::TH1D::global_weight = weight;
}

Int_t eire::TH1D::Fill(double value, double weight)
{
	if(weight == -1){
		return this->::TH1D::Fill(value, global_weight);
	}else{
		return this->::TH1D::Fill(value, weight);
	}
}
