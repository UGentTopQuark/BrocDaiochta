#include "../../interface/TagAndProbe/LepTriggerTreeProducer.h"

/*
 * FIXME: this class has 500 lines and keeps growing... the optimal value is
 * 150 lines per class and the "maximum" 300 lines...
 */

template <class LEPTON>
clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::LepTriggerTreeProducer(eire::HandleHolder *handle_holder)
{
	this->handle_holder = handle_holder;
	lep_selector_has_cuts_set = false;

	l1_trigger_matcher = new truicear::TriggerMatcher<LEPTON, mor::TriggerObject>();
	l2_trigger_matcher = new truicear::TriggerMatcher<LEPTON, mor::TriggerObject>();
	l3_trigger_matcher = new truicear::TriggerMatcher<LEPTON, mor::TriggerObject>();
	hlt_trigger_matcher = new truicear::TriggerMatcher<LEPTON, mor::TriggerObject>();
		trigger_obj_selector = NULL;
		//trigger_obj_selector = new truicear::TriggerObjectSelector(handle_holder);

	trig_name_prov = new eire::TriggerNameProvider();

	tag_and_probe_lepton = new std::vector<LEPTON>();

	mva_id_calc = handle_holder->services()->mva_id_calc();
		
	/*
	 * FIXME: Comment in only when you want to test lepton reweighting
	 * should cause scale factors to be flat. 3 more lines to be commented in in 
	 * fill_special_variables. Only implemented for electon.
	 */
	//	std::string weights_file = handle_holder->get_config_reader()->get_var("eta_weights_file","event_weights",false);
	//weight_prov = new broc::LeptonEtaWeightProvider(weights_file, handle_holder);
//   	std::string weights_file = handle_holder->get_config_reader()->get_var("eta_pfreliso_2D_weights_file","event_weights",false);
// 	weight_prov = new broc::LeptonEtaPFrelIso2DWeightProvider(weights_file, handle_holder);
	 

	dilepton_reconstructor = NULL;
	event_information = NULL;
	
	pt=0;
	mass=0;
	eta=0;
	phi=0;
	abseta=0;
	sc_eta=0;	// eta super cluster (electrons only)
	sc_phi=0;
	mindR = 0;
	njets=0;
	njets_asymm=0;
	jet1_pt=0;
	jet2_pt=0;
	jet3_pt=0;
	jet4_pt=0;
	reliso=0;
	pfreliso=0;
	EApfreliso=0;
	dbeta_pfreliso=0;
	ecaliso=0;
	hcaliso=0;
	trackiso=0;
	combcaloiso=0;
	npvertices=0;
	run=0;
	lumi_section=0;
	passing_iso = 0;
	passing_id = 0;
	passing_idx = 0;
	passing_l1 = 0;
	passing_l2wrtl1 = 0;
	passing_l3wrtl2 = 0;
	passing_hltwrtprev = 0;
	passing_hltwrtl1 = 0;
	passing_refhlt = -1;
	event_weight = 1;
	passing_event_weight = 1;
	mva_id = 99;

	primary_vertices = NULL;
	jets = NULL;

	pvertex_cuts_set = false;

	module_index = -1;
	hlt_name = "";
	hlt_dilep_name = "";

	hlt_name_id = -1;
	hlt_dilep_name_id = -1;
	ref_hlt_name_id = -1;
}

template <typename LEPTON>
clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::~LepTriggerTreeProducer()
{
	if(tag_and_probe_lepton){ delete tag_and_probe_lepton; tag_and_probe_lepton = NULL; }
	if(l1_trigger_matcher){ delete l1_trigger_matcher; l1_trigger_matcher = NULL; }
	if(l2_trigger_matcher){ delete l2_trigger_matcher; l2_trigger_matcher = NULL; }
	if(l3_trigger_matcher){ delete l3_trigger_matcher; l3_trigger_matcher = NULL; }
	if(hlt_trigger_matcher){ delete hlt_trigger_matcher; hlt_trigger_matcher = NULL; }
	if(trigger_obj_selector){ delete trigger_obj_selector; trigger_obj_selector = NULL; }
}

template <typename LEPTON>
void clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::book_branches()
{
	tree->Branch("pt",&pt, "pt/F");
	tree->Branch("eta",&eta, "eta/F");
	tree->Branch("phi",&phi, "phi/F");
	tree->Branch("mass",&mass, "mass/F");
	tree->Branch("njets",&njets, "njets/F");
	tree->Branch("njets_asymm",&njets_asymm, "njets_asymm/F");
	tree->Branch("jet1_pt",&jet1_pt, "jet1_pt/F");
	tree->Branch("jet2_pt",&jet2_pt, "jet2_pt/F");
	tree->Branch("jet3_pt",&jet3_pt, "jet3_pt/F");
	tree->Branch("jet4_pt",&jet4_pt, "jet4_pt/F");
	tree->Branch("abseta",&abseta, "abseta/F");
	tree->Branch("reliso",&reliso, "reliso/F");
	tree->Branch("pfreliso",&pfreliso, "pfreliso/F");
	tree->Branch("EApfreliso",&EApfreliso, "EApfreliso/F");
	tree->Branch("dbeta_pfreliso",&dbeta_pfreliso, "dbeta_pfreliso/F");
	tree->Branch("ecaliso",&ecaliso, "ecaliso/F");
	tree->Branch("hcaliso",&hcaliso, "hcaliso/F");
	tree->Branch("trackiso",&trackiso, "trackiso/F");
	tree->Branch("combcaloiso",&combcaloiso, "combcaloiso/F");
	tree->Branch("run",&run, "run/F");
	tree->Branch("lumi_section",&lumi_section, "lumi_section/F");
	tree->Branch("mindR",&mindR, "mindR/F");
	tree->Branch("npvertices",&npvertices, "npvertices/F");
	tree->Branch("event_weight",&event_weight, "event_weight/F");
	tree->Branch("passing_event_weight",&passing_event_weight, "passing_event_weight/F");
	tree->Branch("passing_iso",&passing_iso,"passing_iso/I");
	tree->Branch("passing_id",&passing_id,"passing_id/I");
	tree->Branch("passing_idx",&passing_idx,"passing_idx/I");
	tree->Branch("passing_l1",&passing_l1,"passing_l1/I");
	tree->Branch("passing_l2wrtl1",&passing_l2wrtl1,"passing_l2wrtl1/I");
	tree->Branch("passing_l3wrtl2",&passing_l3wrtl2,"passing_l3wrtl2/I");
	tree->Branch("passing_hltwrtprev",&passing_hltwrtprev,"passing_hltwrtprev/I");
	tree->Branch("passing_hltwrtl1",&passing_hltwrtl1,"passing_hltwrtl1/I");
	tree->Branch("passing_refhlt",&passing_refhlt,"passing_refhlt/I");

	book_special_branches();
}

template <typename LEPTON>
void clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::book_special_branches()
{
}

namespace clibisfiosraigh{
template <>
void clibisfiosraigh::LepTriggerTreeProducer<mor::Electron>::book_special_branches()
{
	tree->Branch("sc_eta",&sc_eta,"sc_eta/F");
	tree->Branch("sc_phi",&sc_phi,"sc_phi/F");
	tree->Branch("mva_id",&mva_id,"mva_id/F");
}
}

namespace clibisfiosraigh{

template <typename LEPTON>
void clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::initialise_lepton_cuts()
{
	/*
	 * set matching criteria
	 */
	std::vector<double> *max_hlt_dR = cuts_set->get_vcut_value("max_hlt_dR");
	std::vector<double> *max_hlt_dPt = cuts_set->get_vcut_value("max_hlt_dPt");
	std::vector<double> *max_hlt_dEta = cuts_set->get_vcut_value("max_hlt_dEta");

	if(max_hlt_dR->size() > 0){
		l2_trigger_matcher->set_max_dR((*max_hlt_dR)[0]);
		l3_trigger_matcher->set_max_dR((*max_hlt_dR)[0]);
		hlt_trigger_matcher->set_max_dR((*max_hlt_dR)[0]);
	}
	if(max_hlt_dEta->size() > 0){
		l2_trigger_matcher->set_max_dEta((*max_hlt_dEta)[0]);
		l3_trigger_matcher->set_max_dEta((*max_hlt_dEta)[0]);
		hlt_trigger_matcher->set_max_dEta((*max_hlt_dEta)[0]);
	}
	if(max_hlt_dPt->size() > 0){
		l2_trigger_matcher->set_max_dPt((*max_hlt_dPt)[0]);
		l3_trigger_matcher->set_max_dPt((*max_hlt_dPt)[0]);
		hlt_trigger_matcher->set_max_dPt((*max_hlt_dPt)[0]);
	}
	if(max_hlt_dR->size() > 0){
		l2_trigger_matcher->set_matching_method(0);
		l3_trigger_matcher->set_matching_method(0);
		hlt_trigger_matcher->set_matching_method(0);
	}
	else if(max_hlt_dR->size() > 0 && max_hlt_dPt->size() > 0){
		l2_trigger_matcher->set_matching_method(2);
		l3_trigger_matcher->set_matching_method(2);
		hlt_trigger_matcher->set_matching_method(2);
	}

	std::vector<double> *max_L1_dR = cuts_set->get_vcut_value("max_L1_dR");
	std::vector<double> *max_L1_dPt = cuts_set->get_vcut_value("max_L1_dPt");
	std::vector<double> *max_L1_dEta = cuts_set->get_vcut_value("max_L1_dEta");

	if(max_L1_dR->size() > 0) l1_trigger_matcher->set_max_dR((*max_L1_dR)[0]);
	if(max_L1_dEta->size() > 0) l1_trigger_matcher->set_max_dEta((*max_L1_dEta)[0]);
	if(max_L1_dPt->size() > 0) l1_trigger_matcher->set_max_dPt((*max_L1_dPt)[0]);
	if(max_L1_dEta->size() > 0) l1_trigger_matcher->set_matching_method(1);	// dEta matching
	else if(max_L1_dR->size() > 0) l1_trigger_matcher->set_matching_method(3);
	else if(max_L1_dR->size() > 0 && max_L1_dPt->size() > 0) l1_trigger_matcher->set_matching_method(4);
}

template <typename LEPTON>
std::vector<LEPTON>* clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::get_probes()
{
	std::cerr << "ERROR: clibisfiosraigh::LepTriggerTreeProducer<>::get_probes(): invalid lepton type" << std::endl;
	return NULL;
}

template <>
std::vector<mor::Muon>* clibisfiosraigh::LepTriggerTreeProducer<mor::Muon>::get_probes()
{
	return dilepton_reconstructor->get_loose_muon_legs();
}

template <>
std::vector<mor::Electron>* clibisfiosraigh::LepTriggerTreeProducer<mor::Electron>::get_probes()
{
	return dilepton_reconstructor->get_loose_electron_legs();
}

template <typename LEPTON>
std::vector<LEPTON>* clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::get_tags()
{
	std::cerr << "ERROR: clibisfiosraigh::LepTriggerTreeProducer<>::get_tags(): invalid lepton type" << std::endl;
	return NULL;
}

template <>
std::vector<mor::Muon>* clibisfiosraigh::LepTriggerTreeProducer<mor::Muon>::get_tags()
{
	return dilepton_reconstructor->get_tight_muon_legs();
}

template <>
std::vector<mor::Electron>* clibisfiosraigh::LepTriggerTreeProducer<mor::Electron>::get_tags()
{
	return dilepton_reconstructor->get_tight_electron_legs();
}
}	// end namespace clibisfiosraigh

template <typename LEPTON>
void clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::fill_branches()
{
	if(!lep_selector_has_cuts_set){
		/*
		 *	initialise lepton selector
		 */
		initialise_lepton_cuts();
		
		/*
		 *	translate trigger ids to names
		 */
		if(cuts_set->get_vcut_value("hlt_lep_triggers")->size() < 1 && cuts_set->get_vcut_value("hlt_dilep_triggers")->size() < 1 ){
			std::cerr << "ERROR: LepTriggerTreeProducer::fill_branches(): hlt_lep_triggers not set" << std::endl;
			return;
		}
		else{
			if(cuts_set->get_vcut_value("hlt_lep_triggers")->size() >= 1){
				hlt_name_id = (*(cuts_set->get_vcut_value("hlt_lep_triggers")))[0];
			}
			if(cuts_set->get_vcut_value("hlt_dilep_triggers")->size() >= 1){
				hlt_dilep_name_id = (*(cuts_set->get_vcut_value("hlt_dilep_triggers")))[0];
			}
			// allow measurement of one trigger relative to other trigger
			if(cuts_set->get_vcut_value("hlt_lep_ref_trigger")->size() >= 1)
				ref_hlt_name_id = (*(cuts_set->get_vcut_value("hlt_lep_ref_trigger")))[0];
			
			lep_selector_has_cuts_set = true;
		}
	}
	
	std::vector<mor::Particle> *candidates = dilepton_reconstructor->get_candidates();
	std::vector<LEPTON> *tight_leptons = get_tags();
	std::vector<LEPTON> *loose_leptons = get_probes();
	
	if(candidates->size() > 1 || loose_leptons->size() > 1)
		std::cerr << "WARNING: LepTriggerTreeProducer::fill_branches(): more than one Z candidate found, taking first one..." << std::endl;
	if(candidates->size() < 1){
		std::cerr << "WARNING: LepTriggerTreeProducer::fill_branches(): no Z candidate found, ignoring event..." << std::endl;
		return;
	}
	if(loose_leptons->size() < 1){
		std::cerr << "WARNING: LepTriggerTreeProducer::fill_branches(): no probe leptons found, ignoring event..." << std::endl;
		return;
	}

	// FIXME: temporary workaround
	// LEPTON probe_lepton = (*loose_leptons)[0];
	// mor::Particle candidate = (*candidates)[0];
	
	// fill_probe_branch(probe_lepton,candidate,-1,-1,-1);			

	// if(probe_is_tight_lepton(probe_lepton)){
	// 	probe_lepton = (*tight_leptons)[0];
	// 	fill_probe_branch(probe_lepton,candidate,-1,-1,-1);			
	// }
	// FIXME: temporary workaround END
	
			
	tag_and_probe_lepton->clear();
	tag_and_probe_lepton->push_back((*tight_leptons)[0]);
	tag_and_probe_lepton->push_back((*loose_leptons)[0]);
	if(cuts_set->get_vcut_value("hlt_lep_triggers")->size() >=1)
		hlt_name = trig_name_prov->hlt_name((int) hlt_name_id);
	if(cuts_set->get_vcut_value("hlt_dilep_triggers")->size() >=1)
		hlt_dilep_name = trig_name_prov->hlt_name((int) hlt_dilep_name_id);
	if(cuts_set->get_cut_value("module_index") != -1)
	  module_index = (int) cuts_set->get_cut_value("module_index");

	//	if(cuts_set->get_vcut_value("hlt_lep_triggers")->size() >=1){
	//	broc::TriggerModuleManager *trigger_module_manager = handle_holder->get_trigger_module_manager();
	//std::cout<<"Matching to trigger: "<<std::endl;
	//trigger_module_manager->list_modules(hlt_name);
	//std::cout<<"You have selected matching to module index "<<module_index<<std::endl;
	//if(module_index == -1){
	// std::cout<<"This coincides with matching the full trigger"<<std::endl;
	//}
	//else{
	//if(module_index == 0){ std::cout<<"This module index coincides with matching to the first module, which is L1 "<<std::endl;}
	//if(module_index == 1){ std::cout<<"This module index coincides with matching to the second module"<<std::endl;}
	//if(module_index == 2){ std::cout<<"This module index coincides with matching to the third module"<<std::endl;}
	//if(module_index > 2){ std::cout<<"This module index coincides with matching to the "<<module_index+1<<" th module"<<std::endl;}
	//}
	//	if(trigger_module_manager){delete trigger_module_manager; trigger_module_manager = NULL;}
	//	}

	if(ref_hlt_name_id != -1) ref_hlt_name = trig_name_prov->hlt_name((int) ref_hlt_name_id);
	
	std::pair<int32_t, int32_t> hlt_matches (1,1);
	std::pair<int32_t, int32_t> l1_matches (1,1);
	std::pair<int32_t, int32_t> l2_matches (-1,-1);
	std::pair<int32_t, int32_t> l3_matches (-1,-1);
	std::pair<int32_t, int32_t> hlt_dilep_matches (-1,-1); 
	std::pair<int32_t, int32_t> ref_hlt_matches (-1,-1);
	if(hlt_name != "PASS" && hlt_name != ""){
		int last_module_index = handle_holder->get_trigger_module_manager()->get_last_module_index(hlt_name);
	  if(verbose) std::cout << "Last module index: " << last_module_index << std::endl;
		hlt_matches = matches_trigger(tag_and_probe_lepton, (*tight_leptons)[0], (*loose_leptons)[0], hlt_trigger_matcher, hlt_name, module_index);
		if(hlt_dilep_name != ""){
			hlt_dilep_matches = matches_trigger(tag_and_probe_lepton, (*tight_leptons)[0], (*loose_leptons)[0], hlt_trigger_matcher, hlt_dilep_name, -1);
		}
		l1_matches = matches_trigger(tag_and_probe_lepton, (*tight_leptons)[0], (*loose_leptons)[0], l1_trigger_matcher, hlt_name, 0);
		if(1 < module_index){
			if(verbose) std::cout << "Matching l2 module. index 1" << std::endl;
			l2_matches = matches_trigger(tag_and_probe_lepton, (*tight_leptons)[0], (*loose_leptons)[0], l2_trigger_matcher, hlt_name, 1);
			if(2 < module_index){
				if(verbose) std::cout << "Matching l3 module. index 2" << std::endl;
				l3_matches = matches_trigger(tag_and_probe_lepton, (*tight_leptons)[0], (*loose_leptons)[0], l3_trigger_matcher, hlt_name, 2);
			}
		}
	}
	if(ref_hlt_name_id != -1) 
		ref_hlt_matches = matches_trigger(tag_and_probe_lepton, (*tight_leptons)[0], (*loose_leptons)[0], hlt_trigger_matcher, ref_hlt_name, -1);
	
	LEPTON probe_lepton = (*loose_leptons)[0];
	mor::Particle candidate = (*candidates)[0];

	if(hlt_dilep_name == ""){
		//If the tight muon passes the trigger it becomes the tag. 
		//The you can fill the tree with the probe information.
		if(hlt_matches.first == 1 && l1_matches.first == 1){
			fill_probe_branch(probe_lepton,candidate,hlt_matches.second,l1_matches.second,ref_hlt_matches.second,l2_matches.second,l3_matches.second);
		}
		//Now check if the probe also passed the tight selection.
		//If it did and it passes the trigger requirement then it can be used as a tag.
		//The tight muon then becomes the probe as it will always pass the loose selection.
		//(Unless you messed up something when setting cuts so that the tight muons collection is
		//not a subset of the loose muon collection)
		if(probe_is_tight_lepton(probe_lepton) && ( hlt_matches.second == 1 && l1_matches.second == 1)){
			probe_lepton = (*tight_leptons)[0];
			fill_probe_branch(probe_lepton,candidate,hlt_matches.first,l1_matches.first,ref_hlt_matches.first,l2_matches.first,l3_matches.first);
		}
	}
	else{
		if(hlt_matches.first == 1 && hlt_dilep_matches.second == 1){

			//if(l1_matches.first == 1)
			fill_probe_branch(probe_lepton,candidate,1,l1_matches.second,ref_hlt_matches.second,l2_matches.second,l3_matches.second);
		//if(probe_is_tight_lepton(probe_lepton) && l1_matches.second == 1){
		//  probe_lepton = (*tight_leptons)[0];
		//  fill_probe_branch(probe_lepton,candidate,1,l1_matches.first,ref_hlt_matches.first);}
		}else if(hlt_matches.first == 1){
			//if(l1_matches.first == 1)
			fill_probe_branch(probe_lepton,candidate,0,l1_matches.second,ref_hlt_matches.second,l2_matches.first,l3_matches.first);
	//		if(probe_is_tight_lepton(probe_lepton) && l1_matches.second == 1){
	//		  probe_lepton = (*tight_leptons)[0];
	//		  fill_probe_branch(probe_lepton,candidate,0,l1_matches.first,ref_hlt_matches.first);
	//		  }
		}else{
			fill_probe_branch(probe_lepton,candidate,-1,-1,-1,-1,-1);
		}			
	}

}

template <typename LEPTON>
void clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::fill_probe_branch(LEPTON &probe_lepton,mor::Particle &candidate,int32_t hlt_matched, int32_t l1_matched,int32_t ref_hlt_matched, int32_t l2_matched, int32_t l3_matched)
{
	if(verbose) std::cout << " T&P: Filling probe lepton info " << std::endl;
	mass = candidate.mass();
	pt = probe_lepton.pt();
	eta = probe_lepton.eta();
	phi = probe_lepton.phi();
	abseta = std::abs(probe_lepton.eta());
	reliso = probe_lepton.relIso();
	pfreliso = probe_lepton.PFrelIso();
	dbeta_pfreliso = probe_lepton.dbeta_PFrelIso();	
	ecaliso=probe_lepton.ecalIso();
	hcaliso=probe_lepton.hcalIso();
	trackiso=probe_lepton.trackIso();
	combcaloiso=probe_lepton.caloIso();
	run = event_information->run();
	lumi_section = event_information->lumi_block();
	njets = jets->size();
	if(njets > 0) jet1_pt = (*jets)[0].pt(); else jet1_pt = -1;
	if(njets > 1) jet2_pt = (*jets)[1].pt(); else jet2_pt = -1;
	if(njets > 2) jet3_pt = (*jets)[2].pt(); else jet3_pt = -1;
	if(njets > 3) jet4_pt = (*jets)[3].pt(); else jet4_pt = -1;

	int i = 0;
	njets_asymm = 0.;
	for(std::vector<mor::Jet>::iterator jet = jets->begin();
		jet != jets->end();
		++jet){
		if(i == 0 && jet1_pt > 50.) ++njets_asymm;
		else if(i == 1 && jet2_pt > 50.) ++njets_asymm;
		else if(i == 2 && jet3_pt > 40.) ++njets_asymm;
		else if(i > 2 && jet->pt() > 30.) ++njets_asymm;
		else break;

		++i;
	}

	npvertices = count_primary_vertices();
	mindR = min_lep_jet_dR(&probe_lepton);

	passing_id = check_passing_id_iso(probe_lepton,"id"); 

	event_weight = handle_holder->get_failing_event_weight();

	//FIXME: should be hlt_matched or passing_id, depends which passing_event weight you want applied
	if(hlt_matched == 1){
		fill_special_variables(probe_lepton, true);
	}else{
		fill_special_variables(probe_lepton, false);
	}

	if(passing_id != 1)
		passing_iso = -1;
	else
		passing_iso = check_passing_id_iso(probe_lepton,"iso"); 

	if(passing_id != 1 || passing_iso != 1){
		passing_idx = -1;
		passing_l1 = -1;
		passing_hltwrtl1 = -1;
		passing_refhlt = -1;
		passing_l2wrtl1 = -1;
		passing_l3wrtl2 = -1;
		passing_hltwrtprev = -1;
	}else{
		if(l1_matched == 0 && hlt_matched == 1){
			passing_idx = -1;
			passing_l1 = -1;
			passing_hltwrtl1 = -1;
			passing_refhlt = -1;
			passing_l2wrtl1 = -1;
			passing_l3wrtl2 = -1;
			passing_hltwrtprev = -1;
		}else{
			passing_idx = hlt_matched;
			passing_l1 = l1_matched;
		}
		
		
		if(passing_l1 == 1){
			passing_hltwrtl1 = passing_idx;
			passing_l2wrtl1 = l2_matched;

			if(l2_matched == 1)
				passing_l3wrtl2 = l3_matched;
			else
				passing_l3wrtl2 = -1;

			if((passing_l3wrtl2 == 1) ||
			   (passing_l3wrtl2 == -1 && passing_l2wrtl1 == 1) ||
			   (passing_l2wrtl1 == -1)){
				passing_hltwrtprev = passing_idx;
			}else{
				passing_hltwrtprev = -1;
			}
				
			
		}else{
			passing_hltwrtl1 = -1;
			passing_l2wrtl1 = -1;
			passing_l3wrtl2 = -1;
			passing_hltwrtprev = -1;
		}		
		
		if(ref_hlt_matched == 1 && passing_idx == 1)
			passing_refhlt = 1;
		else if(ref_hlt_matched == 1 && passing_idx == 0)
			passing_refhlt = 0;
		else
			passing_refhlt = -1;
	}
	
	if(verbose) std::cout << "filling: mass: " << mass << " pt: " << pt << " eta: " << eta << std::endl;

	tree->Fill();
}

template <typename LEPTON>
void clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::fill_special_variables(LEPTON &probe_lepton, bool passed)
{
	/*
	 * FIXME: Comment in only when you want to test lepton reweighting
	 * should cause scale factors to be flat. 
	 */
//      	if(passed && event_information != NULL 
//     	   && !event_information->is_real_data()){
//     	  	passing_event_weight = weight_prov->get_weight(&probe_lepton,false);//use false when applying 2D weight		

//   	}else{
//     		passing_event_weight = 1;
//    	}
	

}

namespace clibisfiosraigh{
template <>
void clibisfiosraigh::LepTriggerTreeProducer<mor::Electron>::fill_special_variables(mor::Electron &probe_lepton, bool passed)
{
	sc_eta = probe_lepton.supercluster_eta();
	sc_phi = probe_lepton.supercluster_phi();
	mva_id = mva_id_calc->discriminator(probe_lepton);
	EApfreliso = probe_lepton.EA_PFrelIso();
	//std::cout<<"mva_id= "<<mva_id<<std::endl;
}
}

template <typename LEPTON>
int32_t clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::count_primary_vertices()
{
	if(!pvertex_cuts_set){
		pvertex_max_z = cuts_set->get_cut_value("pvertex_max_z");
		pvertex_min_ndof = cuts_set->get_cut_value("pvertex_min_ndof");
		pvertex_max_rho = cuts_set->get_cut_value("pvertex_max_rho");
		pvertex_not_fake = cuts_set->get_cut_value("pvertex_not_fake");
		pvertex_cuts_set = true;
	}

	int ngood_vertices=0;
	if(primary_vertices->size() > 0){
		for(std::vector<mor::PrimaryVertex>::iterator vertex = primary_vertices->begin();
			vertex != primary_vertices->end();
			++vertex){
			if(((pvertex_max_z == -1) || pvertex_max_z > fabs(vertex->z())) &&
			   ((pvertex_min_ndof == -1) || pvertex_min_ndof < vertex->ndof()) &&
			   ((pvertex_max_rho == -1) || pvertex_max_rho > vertex->rho()) &&
			   ((pvertex_not_fake == -1) || pvertex_not_fake != vertex->is_fake())){
				ngood_vertices++;
			}
		}
	}

	return ngood_vertices;
}

template <typename LEPTON>
std::pair<int32_t,int32_t> clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::matches_trigger(std::vector<LEPTON> *leptons, LEPTON &tag_lepton, LEPTON &probe_lepton, truicear::TriggerMatcher<LEPTON, mor::TriggerObject> *trigger_matcher, std::string trigger_name, int module)
{
	std::pair<int32_t, int32_t> matched;
	if(module != -1){
		trigger_obj_selector->set_trigger_name(trigger_name, module);
	}else{
		trigger_obj_selector->set_trigger_name(trigger_name);
	}

	if(trigger_name == "PASS"){
		matched.first = 1;
		matched.second = 1;
		return matched;
	}


	std::vector<mor::TriggerObject> *trigger_objects = trigger_obj_selector->get_selected_objects(this->trigger_objects);
	std::vector<LEPTON> *matched_leptons = trigger_matcher->match(leptons, trigger_objects);

	//	for(unsigned int i=0; i < trigger_objects->size(); i++){
	//std::cout<<"trigger object of module "<<module<<" "<<i<<" : pt = "<<(*trigger_objects)[i].pt()<<" , phi = "<<(*trigger_objects)[i].phi()<<" , eta = "<<(*trigger_objects)[i].eta()<<std::endl;
	//}

	//std::cout<<"tag object "<<" : pt = "<<(tag_lepton).pt()<<" , phi = "<<(tag_lepton).phi()<<" , eta = "<<(tag_lepton).eta()<<std::endl;
	//std::cout<<"probe object "<<" : pt = "<<(probe_lepton).pt()<<" , phi = "<<(probe_lepton).phi()<<" , eta = "<<(probe_lepton).eta()<<std::endl;

	int32_t probe_matched = 0;
	int32_t tag_matched = 0;
	for(typename std::vector<LEPTON>::iterator match = matched_leptons->begin();
		match != matched_leptons->end();
		++match){
		if(match->eta() == probe_lepton.eta() && match->pt() == probe_lepton.pt()){
			probe_matched = 1;
		}else if(match->eta() == tag_lepton.eta() && match->pt() == tag_lepton.pt()){
			tag_matched = 1;
		}
	}
	//	std::cout<<leptons->end()-leptons->begin()<<"/"<<matched_leptons->end()-matched_leptons->begin()<<"\n";

	matched.first = tag_matched;
	matched.second = probe_matched;

	return matched;
}

template <typename LEPTON>
int32_t clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::check_passing_id_iso(LEPTON &probe_lepton,std::string id_iso)
{
	int32_t probe_passed = 0;
	std::vector<LEPTON> *probe_v = new std::vector<LEPTON>();
	probe_v->push_back(probe_lepton);
	
	// FIXME: per *lepton* a lepton selector is booked and destroyed...
	// this is extremely slow and should be fixed

	LeptonSelector<LEPTON> *iso_lep_selector = new LeptonSelector<LEPTON>(handle_holder);
	iso_lep_selector->set_primary_vertices(handle_holder->get_tight_primary_vertices());
	iso_lep_selector->set_cuts_set(cuts_set,lepton_type(),id_iso+"probe_");
		
	std::vector<LEPTON> *passing_lep = iso_lep_selector->get_leptons(probe_v,jets,mets);

	if(passing_lep->size() > 0)
		probe_passed = 1;

	delete iso_lep_selector;
	iso_lep_selector = NULL;
	delete probe_v;
	probe_v = NULL;

	return probe_passed;
}

template <typename LEPTON>
double clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::min_lep_jet_dR(LEPTON *lepton)
{
	double min_dR = -1;
	for(std::vector<mor::Jet>::iterator jet = jets->begin();
		jet != jets->end();
		++jet){
		double dR = ROOT::Math::VectorUtil::DeltaR(*jet, *lepton);
		if(min_dR == -1 || dR < min_dR){
			min_dR = dR;
		}
	}

	return min_dR;
}

template <class LEPTON>
void clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::set_primary_vertices()
{
	this->primary_vertices = handle_holder->get_primary_vertices();
	this->event_information = handle_holder->get_event_information();
	trigger_obj_selector = new truicear::TriggerObjectSelector(handle_holder);
}

template <class LEPTON>
void clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::set_dilepton_reconstructor(DiLeptonReconstructor *dilepton_reconstructor)
{
        this->dilepton_reconstructor = dilepton_reconstructor;
}

//template <class LEPTON>
//void clibisfiosraigh::LepTriggerTreeProducer<LEPTON>::set_TriggerObjectSelector(eire::HandleHolder *handle_holder)
//{
//this->trigger_obj_selector = new truicear::TriggerObjectSelector(handle_holder);
//}

namespace clibisfiosraigh{
template <>
std::string clibisfiosraigh::LepTriggerTreeProducer<mor::Muon>::lepton_type()
{
	return "mu";
}
template <>
std::string clibisfiosraigh::LepTriggerTreeProducer<mor::Electron>::lepton_type()
{
	return "e";
}
template <>
bool clibisfiosraigh::LepTriggerTreeProducer<mor::Muon>::probe_is_tight_lepton(mor::Muon &probe)
{
	for(std::vector<mor::Muon>::iterator tmu = muons->begin();
	    tmu != muons->end();tmu++)
		if(tmu->pt() == probe.pt() && tmu->eta() == probe.eta())
			return true;

	return false;
}
template <>
bool clibisfiosraigh::LepTriggerTreeProducer<mor::Electron>::probe_is_tight_lepton(mor::Electron &probe)
{
	for(std::vector<mor::Electron>::iterator te = electrons->begin();
	    te != electrons->end();te++)
		if(te->pt() == probe.pt() && te->eta() == probe.eta())
			return true;

	return false;
}
}

template class clibisfiosraigh::LepTriggerTreeProducer<mor::Electron>;
template class clibisfiosraigh::LepTriggerTreeProducer<mor::Muon>;
