[global]
dataset = Data
input_files = Data_input_files.txt
output_directory = .
max_events = -1
object_type = 0 #0 = calo or default, 1 = pf
recompute_met = false
allowed_cuts_file = share/allowed_cuts.txt
allowed_vcuts_file = share/allowed_vcuts.txt
JEC_dir = share/JEC/
JEC_uncertainties = START42_V17_AK5PFchs_Uncertainty.txt
dont_split_ttbar = false

enable_mva_module = false
enable_tag_and_probe_module = false
enable_plots_module = true

produce_tree = false

[tag_and_probe]
fill_electron_trigger_trees = false
fill_muon_trigger_trees = false

[plots]
plot_met = true
plot_mass = true
plot_kinematics = true
plot_trigger = false
plot_lept_iso = true
plot_btag = false
plot_ptrel_btag = false
plot_abcd = false
plot_xplusnjets = false
plot_mm = false
plot_additional_kinematics = true

[event_weights]
;pdf_weights = true
;read_pdf_weights = true
;process_all_pdf_sets = true
pdf_name = cteq66
suppress_pdf_offset_correction = false

jet_eff_weights = false
lep_eff_weights = true
process_electron_efficiencies = false
process_muon_efficiencies = true
eta_weights_file = share/sw_thesis_eta_scale_factors_muon_id.txt
eta_pfreliso_2D_weights_file = share/sw_thesis_eta_pfreliso_2D_scale_factors_muon_idx.txt
pu_weights = true
mc_pu_file = share/Fall11RW_PU_distribution_thesis.root
data_pu_file = share/SingleMu_DataPileUp_thesis.root
data_pu_file_sys_down = share/SingleMu_DataPileUp_thesis_sys_down.root
data_pu_file_sys_up = share/SingleMu_DataPileUp_thesis_sys_up.root

[MVA]
method_name = NeuroBayes
weights_file = /localgrid/walsh/ROOT/broc_draiochta/weights/TMVAClassification_NeuroBayes_electron.weights.xml
;weights_file = /localgrid/walsh/ROOT/broc_draiochta/weights/TMVAClassification_NeuroBayes_electron.weights.xml
classify_events = false
create_tree = false
