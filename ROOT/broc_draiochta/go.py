#!/usr/bin/env python

from optparse import OptionParser
import ConfigParser, os, sys, re, time, subprocess

parser = OptionParser()
parser.add_option("-i", "--identifier", dest="identifier", help="run identifier, on output directory")
parser.add_option("-j", "--jobs", dest="jobs", help="number of processes of broc_draiochta that run in parallel")
parser.add_option("-y", "--yes", dest="yes", action="store_false", help="assume yes on questions")
parser.add_option("-d", "--display", dest="display", help="display information about available data files")
parser.add_option("-s", "--setname", dest="name", help="set name")
parser.add_option("-x", "--eXecute", dest="execute", help="define alternative config to run")

(options, args) = parser.parse_args()

config_name = "go_py.cfg"

datasets = args

if options.execute:
	config_name = options.execute

if not os.path.exists(config_name):
	print "cannot find configuation:", config_name, ", exiting..."
	sys.exit()

config = ConfigParser.RawConfigParser()
config.read(config_name)

if not len(datasets) > 0:
	datasets = config.get('default', 'datasets').split(":")

f = open('python/allowed_datasets.txt', 'r')
allowed_datasets = [line.rstrip('\n') for line in f.readlines()]
f.close()

f = open('python/dataset_name_mapping.txt', 'r')
name_mapping = {}
for line in f.readlines():
	this_mapping = line.rstrip('\n').split(" = ")
	name_mapping[this_mapping[0]] = this_mapping[1]
f.close()

def check_if_datasets_valid(datasets, allowed_datasets):
	for dataset in datasets:
		if re.search('Data', dataset):
			continue
		if not dataset in allowed_datasets:
        		print "Exiting... no valid dataset:",dataset
			print "Allowed datasets:"
        		for allowed_ds in allowed_datasets:
    				print "-",allowed_ds;
			sys.exit()

# print selected parameters and datasets and ask for confirmation
def confirm_datasets(datasets, confirmation, cuts_definition, json_file):
	print "Process the following datasets?"
	print "Config: >>>",cuts_definition,"<<<"
	if json_file:
		print "JSON file: >>>", json_file, "<<<"

	for dataset in datasets:
		print " -",dataset

	print "[y/N]: "
	confirmation = sys.stdin.readline()	
	if not re.search('y(es)?', confirmation):
		sys.exit()

# print broc draiochta logo
def print_logo():
	f = open('share/logo.txt')
	print f.read()
	f.close()

# ask for identifier used in directory name
def ask_for_ident(default_ident):
	print "Please enter ident for run [",default_ident,"]:"
	ident = sys.stdin.readline().rstrip('\n')

	if len(ident) < 1:
		ident = default_ident

	return ident

# set current cuts set in CutSelector.cc
def set_config(config, cutset_gen_dir):
	cutset_gen_executable = "./change_cuts.pl "+config
	
	print "setting cuts according to",config,'...'	
	command = "cd "+cutset_gen_dir+"/ && "+cutset_gen_executable+" && cd -"
	exitcode = os.system(command)
	if exitcode > 0:
		print "ERROR: failed at setting cuts"
		sys.exit()

# apply selected json file in RunSelector.cc
def set_json(json, json2cpp_dir):
	json2cpp_executable = "./json2cpp.pl "+json
	
	print "setting json file to ",json,"..."
	command = "cd "+json2cpp_dir+"/ && "+json2cpp_executable+" && cd -"
	exitcode = os.system(command)
	if exitcode > 0:
		print "ERROR: failed at setting json file"
		sys.exit()

# compile broc before submission
def run_make(nthreads):
	make = "make -j "+nthreads
	print "running make..."
	exitcode = os.system(make)
	if exitcode > 0:
		print "ERROR: failed compiling broc_draiochta"
		sys.exit()

# create output directory for submission
def create_directory(identifier, output_dir, force_time):
	date = time.strftime("%Y%m%d")
	dir_name = date+"_"+identifier
	dir_to_check = output_dir+"/"+dir_name
	if os.path.exists(dir_to_check) or force_time:
		date = time.strftime("%Y%m%d_%H%M%S")
		dir_name = date+"_"+identifier

	os.system("mkdir -p "+output_dir+"/"+dir_name)
	
	return dir_name

# create config file for call ./broc_draiochta <config>
def create_config(dataset, input_files, output_dir, postfix, config):
	config_name = output_dir+"/"+dataset+postfix+".cfg"
	default_config = "share/default_config.txt"
	if config.has_option('default', 'parent_config'):
		default_config = "share/"+config.get('default', 'parent_config')

	def_config = open(default_config, 'r')
	out_config = open(config_name, 'w')

	for line in def_config.readlines():
		print >>out_config, line.rstrip('\n');
	def_config.close()

	jec_for_dataset = "";
	if config.has_option('jec', 'default'):
		jec_for_dataset = config.get('jec', 'default').rstrip('\n')
	if config.has_option('jec', dataset):
		jec_for_dataset = config.get('jec', dataset).rstrip('\n')
	jec_for_dataset = "JEC = "+jec_for_dataset;


	print >>out_config, '''

[global]
'''
	if config.has_option('lhco_writer', 'JES_variation'):
		JES_var = config.get('lhco_writer', 'JES_variation').rstrip('\n')
		JES_var = "JES_variation = "+JES_var;

	print >>out_config, "output_directory =",output_dir
	print >>out_config, "outfile_suffix =",postfix
	print >>out_config, "input_files =",input_files
	print >>out_config, "dataset =",dataset
	print >>out_config, jec_for_dataset
	if config.has_option('lhco_writer', 'JES_variation'):
		print >>out_config, '''
		
		[lhco_writer]
		'''
		print >>out_config, JES_var
	out_config.close()

	return config_name;

# create submission script that is run on the node of the cluster
def create_submission_script(output_dir, config_name, config):
	matchObj = re.search('.*/(.*)\.cfg',config_name)
	ident = matchObj.group(1)

	here,there = output_dir.split('/output')

	outfile = output_dir+"/submission_"+ident+".sh";
	sub = open(outfile, "w") 
	matchObj = re.search('.*/(.*)',config_name)
	config_name = matchObj.group(1)

	local_node_directory = "/scratch"

	# maximum number of jobs that are allowed to copy cluster-wide
	# at the same time
	globalmaxnjobs = 30

	parent_config = 'share/default_config.txt'
	if config.has_option('default', 'parent_config'):
		parent_config = "share/"+config.get('default', 'parent_config')

	print >>sub, '''
#!/bin/bash
'''
	print >>sub, "mkdir -p "+local_node_directory+"/`whoami`/$PBS_JOBID"
	print >>sub, "mkdir -p "+local_node_directory+"/`whoami`/$PBS_JOBID/share"
	print >>sub, "cd "+local_node_directory+"/`whoami`/$PBS_JOBID"
	print >>sub, "cp "+output_dir+"/broc_draiochta ."
	print >>sub, "cp "+output_dir+"/"+config_name+" ."
	print >>sub, "cp "+output_dir+"/input_remote_files_"+ident+".txt ."
	print >>sub, "cp "+here+"/"+parent_config+" share/"
	print >>sub, "cp -r "+here+"/weights ."
	print >>sub, "cp -r "+here+"/share/JEC ./share"
	print >>sub, "cp -r "+here+"/share/*.txt ./share" 
	print >>sub, "cp -r "+here+"/share/*.root ./share"
	print >>sub, "cp -r "+here+"/share/TopQuarkAnalysis/ ./share"
            
	print >>sub, """
while [ ! -e /nasmount/Gent/tmp/running_jobs/`whoami`.$PBS_JOBID ]; do
"""
        print >>sub, "if [ `ls -l /nasmount/Gent/tmp/running_jobs | wc -l` -le "+str(globalmaxnjobs)+" ]; then"
	print >>sub, """
                echo "starting job"
                touch /nasmount/Gent/tmp/running_jobs/`whoami`.$PBS_JOBID;

		# cp all input files to node
		mkdir input/
		hostname 1>&2
		pwd 1>&2
		df -h 1>&2
"""
	print >>sub, "for i in `cat input_remote_files_"+ident+".txt`; do"
	print >>sub, """
		echo "cp ${i} input/" 1>&2
		ls -alh ${i} 1>&2
		cp ${i} input/;
	done
        else
                sleep 15
        fi
done

rm /nasmount/Gent/tmp/running_jobs/`whoami`.$PBS_JOBID;


echo "files in input/" 1>&2
ls input/ 1>&2
echo "files requested by job in input_remote_files.txt" 1>&2
"""
	print >>sub, "cat input_remote_files_"+ident+".txt 1>&2"
	print >>sub, "./broc_draiochta "+config_name+" 2>&1 | tee "+ident+"_summary.txt"
	print >>sub, "cp "+ident+"_summary.txt "+output_dir
	print >>sub, "cp *.root "+output_dir
	print >>sub, "rm -rf "+local_node_directory+"/`whoami`/$PBS_JOBID"

	sub.close()
	os.system('chmod 755 '+outfile)
	return outfile

# split all available datafiles in individual jobs and submit to cluster
def get_datafiles_and_run(dataset, config, name_mapping, output_dir):
	datadirs = config.get('default', 'datadirs').split(':')
	filenames_arr = []
	filenames = ""
	# search files to process in all specified directories
	for datadir in datadirs:
		for file in os.listdir(datadir):
			if not re.search('.root', file):
				continue
			dataset_filename = dataset
			if name_mapping.has_key(dataset):
				dataset_filename = name_mapping[dataset]
			if not re.search(dataset_filename, file):
				continue
 			filenames_arr.append(datadir+"/"+file)

	filenames_arr.sort()

	# Check if the list of datasets being supplied to the broc isn't empty (will cause horrible ROOT errors otherwise)
	if len(filenames_arr) == 0:
			print '\n \nError:  I could not find the dataset: '+str(dataset)+' in any of the directories you provided: \n'
			print ', '.join(dir for dir in datadirs)
			sys.exit()
		
 	# cluster submission mode enabled by defining files_per_job in config
 	if config.has_option('default','files_per_job'):
		files_per_job = config.getint('default','files_per_job')
 		file_counter = 0
 		cfg_counter = 0
 		these_input_files = []
 		config_names = []

 		# loop over all file names
 		for filename in filenames_arr:
			these_input_files.append(filename)

 			# if the amounts of files processed so far is larger or equal the maximum number
			# of files per job, create a new job for the following files
 			if file_counter >= files_per_job:
 				cfg_counter += 1
 				postfix = "_"+str(cfg_counter)
 
 				remote_input_files = output_dir+"/input_remote_files_"+dataset+postfix+".txt"

 				# file listing on /nasmount/Gent
 				filelisting = open(remote_input_files, "w")
 				for input_file in these_input_files:
 					print >>filelisting, input_file
				filelisting.close()
 
 				# copy files from localgrid to /scratch
 				input_files = output_dir+"/"+dataset+"_input_files"+postfix+".txt"
 				filelisting = open(input_files, "w")
 				for input_file in these_input_files:
					input_file = re.sub('.*/','',input_file)
 					input_file = "input/"+input_file
 					print >>filelisting, input_file
				filelisting.close()
 
 				# for each job create a new config
 				config_names.append(create_config(dataset, input_files, output_dir, postfix, config))
 				these_input_files = []
 				file_counter = 0
 
 			file_counter += 1

		# if after the loop there are still files left that haven't been assigned to
		# a job, create a final job for all remaining files
 		if len(these_input_files) > 0:
 			cfg_counter += 1
 			postfix = "_"+str(cfg_counter)
 
 			remote_input_files = output_dir+"/input_remote_files_"+dataset+postfix+".txt"

 			# file listing on /nasmount/Gent
			filelisting = open(remote_input_files, "w")
 			for input_file in these_input_files:
				print >>filelisting, input_file
			filelisting.close()
 
 			# copy files from localgrid to /scratch
 			input_files = output_dir+"/"+dataset+"_input_files"+postfix+".txt"
			filelisting = open(input_files, "w")
 			for input_file in these_input_files:
				input_file = re.sub('.*/','',input_file)
 				input_file = "input/"+input_file
 				print >>filelisting, input_file
			filelisting.close()
 
 			# add final config
 			config_names.append(create_config(dataset, input_files, output_dir, postfix, config))

		walltime = "01:00:00"
		if config.has_option('default', 'walltime'):
			walltime = config.get('default', 'walltime')

		jobids = open(output_dir+"/"+dataset+"_job_ids.txt", 'w')

		# for each config (= job), run submission
 		for config_name in config_names:
 			submission_script = create_submission_script(output_dir, config_name, config)
 			command = "qsub -l walltime="+walltime+" -o "+output_dir+" -e "+output_dir+" -q localgrid@cream02 "+submission_script
 			print command
 			jobid = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
 			print >>jobids, jobid.stdout.readline().rstrip('\n')

		jobids.close()
 	else: # alternative mode, local processing without cluster submission
		# only one 'job' per dataset
	       	input_files = output_dir+"/"+dataset+"_input_files.txt"
	
	       	f = open(input_files,'w')
	
	       	for input_file in filenames_arr:
	       		print >>f, input_file
		f.close()
	
	       	postfix = ""
	       	config_name = create_config(dataset, input_files, output_dir, postfix, config)
	       	
	       	os.system("time ./broc_draiochta "+config_name+" 2>&1 | tee "+output_dir+"/"+dataset+".txt")

# copy all necessary files to new output directory, create all configs and submit to cluster
def run_broc_draiochta(datasets, config_name, config, name_mapping, identifier):
	if config.has_option('default', 'force_time'):
		force_time = config.getboolean('default', 'force_time')
	else:
		force_time = False

	output_parent = config.get('default', 'output_dir')	
	output_dir = create_directory(identifier, output_parent, force_time)
	output_dir = output_parent+"/"+output_dir

	os.system("cp broc_draiochta "+output_dir)
	os.system("cp cutset_generator/"+config.get('default','cuts_definition')+" "+output_dir)
	os.system("cp json2cpp/"+config.get('default','json_file')+" "+output_dir)
	os.system("cp "+config_name+" "+output_dir+"/run.config")

	default_config = "share/default_config.txt"
	if config.has_option('default', 'parent_config'):
		default_config = "share/"+config.get('default', 'parent_config')
	os.system("cp "+default_config+" "+output_dir+"/parent_config.config")

	if not config.has_option('default', 'disable_svn_log_for_submission'):
#		os.system("svnversion > "+output_dir+"/svnversion")
		os.system("git diff src/ interface/ `VAR=\"\"; for i in \`ls -d interface/*/ src/*/\`; do VAR=\"${VAR} ${i}\"; done; echo $VAR` > "+output_dir+"/git_diff_to_HEAD.txt")
	
	for dataset in datasets:
		get_datafiles_and_run(dataset, config, name_mapping, output_dir)

################################################################################
################################################################################
###
### Main control flow
###
################################################################################
################################################################################

print_logo()
# check if requested datasets are valid (eg. no typos)
check_if_datasets_valid(datasets, allowed_datasets)
confirm_datasets(datasets, options, config.get('default', 'cuts_definition'), config.get('default', 'json_file'))
# get identifier either from command line parameter or from config
identifier = ""
if options.identifier:
	identifier = options.identifier
else:
	identifier = ask_for_ident(config.get('default', 'identifier'))
# set cuts in CutSelector.cc
set_config(config.get('default','cuts_definition'), "cutset_generator")
# set json file in RunSelector.cc
set_json(config.get('default','json_file'), "json2cpp")
# re-comptile code before submission
run_make(config.get('default','nthreads'))
# submit jobs to cluster or run locally
run_broc_draiochta(datasets, config_name, config, name_mapping, identifier)
