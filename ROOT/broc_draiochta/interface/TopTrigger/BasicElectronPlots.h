#ifndef BASICELECTRONPLOTS_H
#define BASICELECTRONPLOTS_H

#include "TriggerPlot.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MEventInformation.h"
#include "../AnalysisTools/HistoWriter.h"
#include "TH1D.h"
#include "TH2D.h"

namespace truicear{
	class BasicElectronPlots: public TriggerPlot{
		public:
			BasicElectronPlots();
			~BasicElectronPlots();

			void plot(std::vector<mor::Electron> *reco_electrons, std::vector<mor::Jet> *jets, mor::EventInformation *event_information);
		private:
			virtual void book_histos();
	};
}

#endif
