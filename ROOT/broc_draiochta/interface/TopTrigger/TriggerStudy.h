#ifndef TRUICEAR_TRIGGERSTUDY_H
#define TRUICEAR_TRIGGERSTUDY_H

#include "BasicMuonPlots.h"
#include "BasicElectronPlots.h"
#include "BasicJetPlots.h"
#include "MatchedMuonPlots.h"
#include "MatchedElectronPlots.h"
#include "MatchedTriggerObjectPlots.h"
#include "TriggerObjectSelector.h"
#include "TopComSyncEx.h"
#include "MatchingCriteria.h"
#include "TriggerLevelStudy.h"
#include "../MorObjects/MTrigger.h"

#include "../AnalysisTools/HistoWriter.h"
#include "../AnalysisTools/ETH1D.h"
#include "../AnalysisTools/ETH2D.h"
#include "TriggerMatcher.h"
#include <map>
#include "math.h"

namespace truicear{
	template <class LeptonType, class BasicLeptonPlotsType, class MatchedLeptonPlotsType>
	class TriggerStudy{
		public:
		TriggerStudy(std::string id, std::string trigger_name, std::string l1seed_name);
			~TriggerStudy();
			void book_plot_objects();
			void set_matching_criteria(std::map<std::string, truicear::MatchingCriteria*> *matching_criteria);
			void set_histo_writer(HistoWriter *histo_writer);
			void initialise_trigger_levels();
			void plot();
			void set_handles(std::vector<LeptonType> *reco_leptons, std::vector<mor::TriggerObject> *trigger_objects, std::vector<mor::Jet> *jets, mor::Trigger *HLTR, mor::EventInformation *event_information);
		private:
			std::string id;

			HistoWriter *histo_writer;

			std::string trigger_name;
			std::string l1seed_name;

			std::vector<LeptonType> *reco_leptons;
			std::vector<mor::TriggerObject> *trigger_objects;
			std::vector<mor::Jet> *jets;
			mor::Trigger *HLTR;
			mor::EventInformation *event_information;
			//counters for event info
			int nlep_found;

			// plots for all l1 reco matched trigger objects
			BasicLeptonPlotsType *all_l1_trig_obj_plots;
			truicear::BasicJetPlots *all_jet_l1_plots;
			std::vector<LeptonType> *l1_matched_reco;

			// matching criteria for hlt/l1/etc matching
			std::map<std::string, truicear::MatchingCriteria*> *matching_criteria;

			std::vector<TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType> *> trigger_level_studies;
			TriggerLevelStudy<LeptonType,mor::TriggerObject,BasicLeptonPlotsType, MatchedLeptonPlotsType> *hlt_vs_l1_study;
	};
}

#endif
