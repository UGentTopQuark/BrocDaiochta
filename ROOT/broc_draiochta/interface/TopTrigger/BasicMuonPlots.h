#ifndef BASICMUONPLOTS_H
#define BASICMUONPLOTS_H

#include "TriggerPlot.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MEventInformation.h"
#include "../AnalysisTools/HistoWriter.h"
#include "TH1D.h"
#include "TH2D.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

namespace truicear{
	class BasicMuonPlots: public TriggerPlot{
		public:
			BasicMuonPlots();
			~BasicMuonPlots();

			void plot(std::vector<mor::Muon> *reco_muons, std::vector<mor::Jet> *jets, mor::EventInformation *event_information);

		private:
			virtual void book_histos();
			
	};
}

#endif
