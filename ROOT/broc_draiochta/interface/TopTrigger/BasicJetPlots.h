#ifndef BASICJETPLOTS_H
#define BASICJETPLOTS_H

#include "TriggerPlot.h"
#include "../MorObjects/MJet.h"
#include "../AnalysisTools/HistoWriter.h"
#include "TH1D.h"
#include "TH2D.h"

namespace truicear{
	class BasicJetPlots: public TriggerPlot{
		public:
			BasicJetPlots();
			~BasicJetPlots();

			void plot(std::vector<mor::Jet> *jets);
		private:
			virtual void book_histos();
	};
}

#endif
