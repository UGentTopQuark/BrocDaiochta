#ifndef BASICTRIGGEROBJECTPLOTS_H
#define BASICTRIGGEROBJECTPLOTS_H

#include "TriggerPlot.h"
#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MEventInformation.h"
#include "../AnalysisTools/HistoWriter.h"
#include "TH1D.h"
#include "TH2D.h"

namespace truicear{
	class BasicTriggerObjectPlots: public TriggerPlot{
		public:
			BasicTriggerObjectPlots();
			~BasicTriggerObjectPlots();

			void plot(std::vector<mor::TriggerObject> *trigger_objects, std::vector<mor::Jet> *jets, mor::EventInformation *event_information);

		private:
			virtual void book_histos();
	};
}

#endif
