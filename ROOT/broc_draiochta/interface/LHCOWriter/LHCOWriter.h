#ifndef BROC_LHCOWRITER_H
#define BROC_LHCOWRITER_H

#include "../MorObjects/MMET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MElectron.h"
#include "../BeagObjects/MyLHCOEvent.h"
#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/Plots.h"
#include "../EventSelection/Tools.h"
#include "../TagAndProbe/TreeProducer.h"
#include "../EventSelection/LeptonSelector.h"
#include "../EventSelection/CentralServices.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <TString.h>
#include <fstream>
#include <string>
#include "TH2.h"
#include "TF1.h"

namespace broc{
	class LHCOWriter: public clibisfiosraigh::TreeProducer{
		//class LHCOWriter{
    public:
	LHCOWriter(eire::HandleHolder *handle_holder);
	~LHCOWriter();
	
	void set_handles(eire::HandleHolder *handle_holder);
	virtual void book_branches();
	int is_matched_event();
	
	void fill_branches();
	void fill_branches(bool use_hitfit_kinematics = false);
	
	void fill_leaves(int is_generated, FILE * file, double pevent_number, double prun_number, int32_t ptyp, float peta, float pphi, float ppt, float pjmass, float pntrk, int32_t pbtag, float phadOverem, double pevent_weight, int pmc_id, int partnum, float E, std::string event_type);
	
	void set_header(bool gen, FILE * file);
	void generator_file();
	
    private:
	bool data_only;

	FILE *lhcofile_tot;
	FILE *lhcofile_tot_MC;
	FILE *lhcofile_tot_hitfit;

	FILE *lhcofile_pos;
	FILE *lhcofile_pos_MC;
	FILE *lhcofile_pos_hitfit;
	
	FILE *lhcofile_neg;
	FILE *lhcofile_neg_MC;
	FILE *lhcofile_neg_hitfit;
	
	bool split_lepton_charges;

	std::vector<mor::Electron> *electrons;
	std::vector<mor::Muon> *muons;
	std::vector<mor::Jet> *jets;
	std::vector<mor::Jet> *sorted_jets;
	std::vector<mor::MET> *mets;
	std::vector<mor::Particle> *hitfit_jets;
	std::vector<mor::Particle> *hitfit_electrons;
	std::vector<mor::Particle> *hitfit_muons;
	std::vector<mor::Particle> *hitfit_mets;
	std::vector<int> hitfit_combi_seed;

	std::string lhconame;
	std::string id;
	std::string btag_algo;
	double btag_working_point;

	bool first;
	bool use_hitfit;
	bool use_hitfit_combi_seed;
	bool use_hitfit_kinematics;

	int32_t gen_filled;
	int32_t reco_filled;

	mor::TTbarGenEvent *gen_event;
	mor::EventInformation *event_information;   
	mor::Particle *phadB;
	mor::Particle *plepB;
	mor::Particle *pq;
	mor::Particle *pqbar;
	beag::MyLHCOEvent *gen_evt;
	beag::MyLHCOEvent *reco_evt;
    };
    
}

#endif


