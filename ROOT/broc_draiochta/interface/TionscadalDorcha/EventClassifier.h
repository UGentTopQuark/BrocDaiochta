#ifndef TIONSCADALDORCHA_EVENTCLASSIFIER_H
#define TIONSCADALDORCHA_EVENTCLASSIFIER_H

#include "EventShapeVariableCalculator.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#include "../../src/ConfigReader/ConfigReader.h"
//jl 07.04.11: SPR
//#include "../StatPatternRecognition/SprAbsTrainedClassifier.hh"
//#include "../StatPatternRecognition/SprClassifierReader.hh"
#include "../../interface/EventSelection/M3Mass.h"

#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/HtCalculator.h"
#include "../EventSelection/CentralServices.h"
#include "../TionscadalDorcha/ClassificationVariableCalculator.h"

namespace broc{
	class CentralServices;
}

namespace tionscadaldorcha{
	class EventClassifier{
		public:
			EventClassifier(eire::HandleHolder *handle_holder);
			~EventClassifier();
			void set_event_shape_calc(tionscadaldorcha::EventShapeVariableCalculator *evt_shape){this->evt_shape = evt_shape;}
			void initialise();
			double classify();
			void next_event(){};	// FIXME:  implement a function to stop recalculation of variable for same event
		private:
			tionscadaldorcha::EventShapeVariableCalculator *evt_shape;
			broc::ClassificationVariableCalculator *var_calc;
			TMVA::Reader *reader;

			std::string method_name;

			eire::ConfigReader *config_reader;

			Float_t sphericity;
			Float_t aplanarity;
			Float_t HlT;
			Float_t H3T;
			Float_t Mevent;
			Float_t Mj2nulT;
			
			//jl 07.04.11: new variables
			float m3;
			float mu1_pt;
			float mu1_eta;
			float mu1_abseta_x_charge;
			float dphimmet;
			float drjm;
			float detajm;
			float detaje;
			float jet1_pt;
			float jet2_pt;
			float jet3_pt;
			float jet4_pt;
			float jet1_eta;
			float jet2_eta;
			float jet3_eta;
			float jet4_eta;
			float njets;
			float MET;
			float drje;
			float dphiemet;
			float e1_eta;
			float e1_pt;
			float e1_abseta_x_charge;
			float HT;
			float MHT;
			float mchi2_hadT;
			float mchi2_lepT;
			float mchi2_prob;
			float mchi2_dm;
			MassReconstruction *tmass;
			eire::HandleHolder *handle_holder;
			HtCalculator *ht_calc;
	};
}

#endif
