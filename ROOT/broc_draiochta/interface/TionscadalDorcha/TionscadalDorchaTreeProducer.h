#ifndef TionscadalDorcha_TionscadalDorchaTREEPRODUCER_H
#define TionscadalDorcha_TionscadalDorchaTREEPRODUCER_H

#include "../../interface/EventSelection/M3Mass.h"
#include "../TagAndProbe/TreeProducer.h"
#include "../AnalysisTools/HistoWriter.h"
#include "../EventSelection/HandleHolder.h"
#include "EventShapeVariableCalculator.h"
#include "Math/VectorUtil.h"
#include "../EventSelection/CentralServices.h"
#include "../EventSelection/HtCalculator.h"
#include "../TionscadalDorcha/ClassificationVariableCalculator.h"

namespace tionscadaldorcha{
	class TionscadalDorchaTreeProducer:public clibisfiosraigh::TreeProducer{
		public:
			TionscadalDorchaTreeProducer(eire::HandleHolder *handle_holder);
			~TionscadalDorchaTreeProducer();

			void fill_branches();
			void book_branches();
		private:
			std::string id;

			tionscadaldorcha::EventShapeVariableCalculator *evt_shape;
			broc::ClassificationVariableCalculator *var_calc;
			bool evt_shape_initialised;

			mutable float jet1_pt;
			mutable float jet2_pt;
			mutable float jet3_pt;
			mutable float jet4_pt;
			mutable float jet1_eta;
			mutable float jet2_eta;
			mutable float jet3_eta;
			mutable float jet4_eta;

			mutable float njets;	

			mutable float mu1_pt;
			mutable float mu1_eta;
			mutable float mu1_abseta_x_charge;
			mutable float mu1_charge;
			mutable float loose_mu1_pt;
			mutable float loose_mu1_eta;

			mutable float e1_pt;
			mutable float e1_eta;
			mutable float e1_abseta_x_charge;
			mutable float e1_charge;
			mutable float loose_e1_pt;
			mutable float loose_e1_eta;

			mutable float aplanarity;
			mutable float sphericity;
			mutable float HT;
			mutable float HlT;
			mutable float H3T;
			mutable float MjetT;
			mutable float Mevent;
			mutable float Mj2nulT;

			//jl 23.03.11
		        mutable float MET;
		        mutable float MHT;
	        	mutable float m3;
        		mutable float drje;
        		mutable float drjm;
        		mutable float detaje;
        		mutable float detajm;
        		mutable float dphiemet;
        		mutable float dphimmet;

        		mutable float mchi2_hadT;
        		mutable float mchi2_lepT;
        		mutable float mchi2_prob;
        		mutable float mchi2_dm;

        		mutable float weight;

			HtCalculator *ht_calc;

			MassReconstruction *tmass;
	};
}

#endif
