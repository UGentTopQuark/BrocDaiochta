#ifndef TAGANDPROBE_H
#define TAGANDPROBE_H

/**
 * \class TagAndProbe
 * 
 * \brief Control class for lepton efficiency measurement using tag and probe.
 *
 * This class controls the setup of various LepTriggerTreeProducer instances
 * and will in the future also be extended for selection efficiency
 * measurements.
 * 
 * \authors walsh, klein
 */


#include "TFile.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MTrigger.h"
#include "../EventSelection/DiLeptonReconstructor.h"
#include "../EventSelection/CutsSet.h"
#include "../EventSelection/HandleHolder.h"

#include "LepTriggerTreeProducer.h"

namespace clibisfiosraigh{
	class TagAndProbe{
		public:
			TagAndProbe(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			~TagAndProbe();

			void fill_trees();
		private:
			TFile *ntuple_file;
			LepTriggerTreeProducer<mor::Muon> *mu_trigger_tree_producer;
			LepTriggerTreeProducer<mor::Electron> *e_trigger_tree_producer;
			DiLeptonReconstructor *dilepton_reconstructor;

			bool do_electron_trigger_efficiency;
			bool do_muon_trigger_efficiency;
	};
}

#endif
