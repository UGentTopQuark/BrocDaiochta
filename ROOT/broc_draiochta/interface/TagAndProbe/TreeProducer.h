#ifndef TREEPRODUCER_H
#define TREEPRODUCER_H

/**
 * \class clibisfiosraigh::TreeProducer
 * 
 * \brief Abstract class to offer common interface for the creation of ROOT files with trees for selected events.
 *
 * The class offers basic functionality to write out simple ROOT trees based on
 * a selection and containing variables calculated in broc_draiochta. A
 * specialised class is usually booked which inherits from
 * clibisfiosraigh::TreeProducer and implements the function where the
 * variables for a certain tree are booked and filled.
 *
 * \authors klein
 */


#include "TFile.h"
#include "TTree.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MTrigger.h"

#include "../EventSelection/CutsSet.h"
#include "../EventSelection/HandleHolder.h"

#include <iostream>

namespace clibisfiosraigh{
	class TreeProducer{
		public:
			TreeProducer();
			virtual ~TreeProducer();

			void set_handles(eire::HandleHolder *handle_holder);
	
			void set_outfile(TFile *ntuple_file, std::string directory_name="");
			void create_tree(std::string tree_name);
			virtual void book_branches()=0;
			virtual void fill_branches()=0;
		protected:
			TTree *tree;
			TFile *outfile;

			std::string directory_name;
			broc::CutsSet *cuts_set;

			std::vector<mor::Muon> *muons;
			std::vector<mor::Muon> *loose_muons;
			std::vector<mor::Jet> *jets;
			std::vector<mor::Electron> *electrons;
			std::vector<mor::Electron> *loose_electrons;
			std::vector<mor::MET> *mets;
			mor::Trigger *trigger;
			std::vector<mor::TriggerObject> *trigger_objects;
			std::vector<mor::Particle> *gen_particles;

			eire::HandleHolder *handle_holder;
	};
}

#endif
