#ifndef MUTRIGGERTREEPRODUCER_H
#define MUTRIGGERTREEPRODUCER_H

#include "TreeProducer.h"
#include "../EventSelection/LeptonSelector.h"
#include "../EventSelection/TriggerNameProvider.h"

#include "../TopTrigger/TriggerMatcher.h"
#include "../TopTrigger/TriggerObjectSelector.h"
#include "../MorObjects/MPrimaryVertex.h"
#include "../MorObjects/MJet.h"
#include "../EventSelection/CentralServices.h"

#include "../EventSelection/DiLeptonReconstructor.h"
#include "../EventSelection/HandleHolder.h"
#include "../EventSelection/LeptonEtaWeightProvider.h"
#include "../EventSelection/LeptonEtaPFrelIso2DWeightProvider.h"


#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

namespace clibisfiosraigh{
	template <typename LEPTON>
	class LepTriggerTreeProducer: public TreeProducer{
		public:
			LepTriggerTreeProducer(eire::HandleHolder *handle_holder);
			~LepTriggerTreeProducer();

			virtual void book_branches();
			virtual void fill_branches();

			void set_dilepton_reconstructor(DiLeptonReconstructor *dilepton_reconstructor);
			//			void set_TriggerObjectSelector(eire::HandleHolder *handle_holder);
			void set_primary_vertices();
		private:
			std::pair<int32_t,int32_t> matches_trigger(std::vector<LEPTON> *leptons, LEPTON &tag_lepton, LEPTON &probe_lepton, truicear::TriggerMatcher<LEPTON, mor::TriggerObject> *trigger_matcher, std::string trigger_name, int module);
			int32_t check_passing_id_iso(LEPTON &probe_lepton,std::string id_iso);
			std::string lepton_type();
			double min_lep_jet_dR(LEPTON *electron);
			int32_t count_primary_vertices();
			void initialise_lepton_cuts();
			std::vector<LEPTON>* get_probes();
			std::vector<LEPTON>* get_tags();
			void book_special_branches();
			void fill_special_variables(LEPTON &probe_lepton, bool passed);
			void fill_probe_branch(LEPTON &probe_lepton,mor::Particle &candidate,int32_t hlt_matched, int32_t l1_matched,int32_t ref_hlt_matched,int32_t l2_matched,int32_t l3_matched);
			bool probe_is_tight_lepton(LEPTON &probe_lepton);

			std::vector<LEPTON> *tight_probe_leptons;
			std::vector<LEPTON> *tag_and_probe_lepton;

			//broc::LeptonEtaWeightProvider *weight_prov;
			broc::LeptonEtaPFrelIso2DWeightProvider *weight_prov;

			truicear::TriggerMatcher<LEPTON, mor::TriggerObject> *l1_trigger_matcher;
			truicear::TriggerMatcher<LEPTON, mor::TriggerObject> *l2_trigger_matcher;
			truicear::TriggerMatcher<LEPTON, mor::TriggerObject> *l3_trigger_matcher;
			truicear::TriggerMatcher<LEPTON, mor::TriggerObject> *hlt_trigger_matcher;
			truicear::TriggerObjectSelector *trigger_obj_selector;
			eire::TriggerNameProvider *trig_name_prov;
			broc::MVAElectronIDCalculator *mva_id_calc;
			
			std::vector<mor::PrimaryVertex> *primary_vertices;

			DiLeptonReconstructor *dilepton_reconstructor;

			mutable float pt;
			mutable float mass;
			mutable float eta;
			mutable float sc_eta;
			mutable float sc_phi;
			mutable float phi;
			mutable int32_t passing_iso;
			mutable int32_t passing_id;
			mutable int32_t passing_idx;
			mutable int32_t passing_l1;
			mutable int32_t passing_l2wrtl1;
			mutable int32_t passing_l3wrtl2;
			mutable int32_t passing_hltwrtprev;
			mutable int32_t passing_hltwrtl1;
			mutable int32_t passing_refhlt;	//< hlt efficiency of one hlt relative to other hlt
			mutable float abseta;
			mutable float njets;
			mutable float njets_asymm;
			mutable float jet1_pt;
			mutable float jet2_pt;
			mutable float jet3_pt;
			mutable float jet4_pt;
			mutable float npvertices;
			mutable float reliso;
			mutable float EApfreliso;
			mutable float pfreliso;
			mutable float dbeta_pfreliso;
			mutable float ecaliso;
			mutable float hcaliso;
			mutable float trackiso;
			mutable float combcaloiso;
			mutable float mindR;
			mutable float run;
			mutable float lumi_section;
			mutable float event_weight;
			mutable float passing_event_weight;
			mutable float mva_id;

			mor::EventInformation *event_information;

			double hlt_name_id;
			double ref_hlt_name_id;
			double hlt_dilep_name_id;
			int module_index;
			std::string hlt_name;
			std::string ref_hlt_name;
			std::string hlt_dilep_name;

			double pvertex_max_z;
			double pvertex_min_ndof;
			double pvertex_max_rho;
			double pvertex_not_fake;

			bool pvertex_cuts_set;

			std::map<std::string,TH2D*> histos;

			bool lep_selector_has_cuts_set;

			static const bool verbose = false;

			double tmp_pass_counter;
			double tmp_fail_counter;
	};
}

#endif
