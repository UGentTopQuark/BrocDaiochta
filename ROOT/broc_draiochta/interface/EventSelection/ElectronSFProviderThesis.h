#ifndef BROC_ESFPROVIDERTHESIS_H
#define BROC_ESFPROVIDERTHESIS_H

#include "HandleHolder.h"

namespace broc{
	class ElectronSFProviderThesis{
		public:
			ElectronSFProviderThesis(eire::HandleHolder *handle_holder);
			~ElectronSFProviderThesis();

			double get_scale_factor();
		private:
			eire::HandleHolder *handle_holder;
			bool first_warning;

			bool do_not_reweight;
	};
}

#endif
