#ifndef BROC_JERCORRECTOR_H
#define BROC_JERCORRECTOR_H

#include "../MorObjects/MJet.h"

namespace broc{
	class JERCorrector{
		public:
			JERCorrector();
			~JERCorrector();

			void correctJER(mor::Jet *jet, double study_jer_unc);
		private:
			bool minus_shift;
			bool plus_shift;
			bool nominal_shift;
	};
}

#endif
