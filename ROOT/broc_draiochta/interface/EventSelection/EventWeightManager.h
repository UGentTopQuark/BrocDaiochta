#ifndef BROC_EVENTWEIGHTMANAGER_H
#define BROC_EVENTWEIGHTMANAGER_H

/**
 * \class broc::EventWeightManager
 * 
 * \brief Calculates for each event a certain event weight.
 *
 * For each type of event weight (pdf weighs, mc weights, pile up weights, etc)
 * a new module should be defined. This module should inherit from
 * broc::EventWeight to guarantee a common interface. broc::EventWeightManager
 * executes then all broc::EventWeight modules and calculates a global weight
 * for the event.
 *
 * \author klein
 */

#include "HandleHolder.h"
#include "CutsSet.h"
#include "PDFEventWeight.h"
#include "PileUpEventWeightProvider.h"
#include "PVPileUpEventWeightProvider.h"
#include "LeptonAbsEtaWeightProvider.h"
#include "LeptonEtaWeightProvider.h"
#include "LeptonPtWeightProvider.h"
#include "LeptonPFrelIsoWeightProvider.h"
#include "LeptonEtaPFrelIso2DWeightProvider.h"
#include "JetEffWeightProvider.h"
#include "ETriggerEffWeightProvider.h"
#include "HandleHolder.h"
#include "../../src/ConfigReader/ConfigReader.h"
#include "IsoMuSFProvider2012.h"
#include "ElectronSFProviderThesis.h"
#include "TopPtWeightProvider.h"
#include "ElectronID_Iso_2012EffWeightProvider.h"
#include "MuonID_2012EffWeightProvider.h"
#include "MuonIso_2012EffWeightProvider.h"
#include "BTagEfficiencyWeightProvider.h"
#include "ElectronRecEffWeightProvider.h"
#include "CrossTriggerWeightProvider.h"

namespace broc{
	class EventWeightManager{
		public:
			EventWeightManager(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			~EventWeightManager();

			double get_all_events_weight();//< Does not include lepton scale factors.
			double get_selected_events_weight();//< Includes weights applied to all events. Must be called after all_events_weight
			void print();
		private:
			double get_W_BR_correction_weight();
			double get_W_scale_correction_weight();
			double bquark_mass_correction_weight();
			double btag_mc_sf;
			double btag_systematic_shift;

			broc::PDFEventWeight *pdf_event_weight;
			broc::PileUpEventWeightProvider *pu_event_weight;
			broc::LeptonAbsEtaWeightProvider *lep_abseta_event_weight;
			broc::LeptonEtaWeightProvider *lep_eta_event_weight;
			broc::LeptonPtWeightProvider *lep_pt_event_weight;
			broc::LeptonPFrelIsoWeightProvider *lep_pfreliso_event_weight;
			broc::LeptonEtaPFrelIso2DWeightProvider *lep_eta_pfreliso_2D_event_weight;
			broc::JetEffWeightProvider *jet_event_weight;
			broc::ETriggerEffWeightProvider *e_trig_event_weight;
			broc::IsoMuSFProvider2012 *iso_mu_sf_2012;
			broc::ElectronSFProviderThesis *e_sf_thesis;
			eire::HandleHolder *handle_holder;
			broc::TopPtWeightProvider *top_pt_weight;
			broc::ElectronID_Iso_2012EffWeightProvider *e_ID_Iso_2012_event_weight;
			broc::MuonID_2012EffWeightProvider *m_ID_2012_event_weight;
			broc::MuonIso_2012EffWeightProvider *m_Iso_2012_event_weight;
			broc::BTagEfficiencyWeightProvider *btag_event_weight;
			broc::ElectronRecEffWeightProvider *e_rec_event_weight;
			broc::CrossTriggerWeightProvider *cross_trig_weight;

			bool jet_2012_eff_weights;
			FILE *output;
			bool write_out;
			int event;
			double pu;
			double TopPt;
			double lepId;
			double lepIdU;
			double lepIdD;
			double trig;
			double trigU;
			double trigD;
			double btag;
			double btagU;
			double btagD;
			double mtagU;
			double mtagD;

	};
}

#endif
