#ifndef BROC_LEPPTEVENTWEIGHTPROVIDER_H
#define BROC_LEPPTEVENTWEIGHTPROVIDER_H

/** \class broc::LeptonPtWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on Pt of lepton in event.
 *
 * \author klein
 */

#include "LeptonEfficiencyWeightProvider.h"

namespace broc{
	class LeptonPtWeightProvider : public LeptonEfficiencyWeightProvider{
		public:
			LeptonPtWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder);
			~LeptonPtWeightProvider();
		protected:
			virtual double get_value_for_event();
	};
}

#endif
