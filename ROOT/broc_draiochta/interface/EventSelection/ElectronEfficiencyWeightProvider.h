#ifndef broc_ElectronEfficiencyWeightProvider_h
#define broc_ElectronEfficiencyWeightProvider_h

namespace broc{
	class ElectronEfficiencyWeightProvider{
		public:
			ElectronEfficiencyWeightProvider();
			~ElectronEfficiencyWeightProvider();

			double get_weight();
		private:
	};
}

#endif
