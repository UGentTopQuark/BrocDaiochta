#ifndef EIRE_JETCORRECTIONSERVICE_H
#define EIRE_JETCORRECTIONSERVICE_H

/** \class eire::JetCorrectionService
 * 
 * \brief Interface to CMSSW jet corrections for on-the-fly jet corrections.
 *
 * The class is an interface to the CMSSW jet corrector. It takes as input a
 * list of uncorrected jets and a pointer to a vector of mor::Jet objects. The
 * latter one is emptied and then filled with the same jets as in the
 * uncorrected jets vector but with applied jet corrections. The corrected jets
 * are then pt sorted after the correction step.
 *
 * \param uncorrected_jets vector of mor::Jet objects without jet corrections
 * applied \param corrected_jets vector of mor::Jet objects which will be
 * emptied and then filled with jets with jet corrections applied
 * 
 * \authors klein
 */


#include "../../share/CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"
#include "../../share/CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
#include "../../src/ConfigReader/ConfigReader.h"
#include <vector>

#include "TMath.h"

#include "../MorObjects/MJet.h"

namespace eire{
	class JetCorrectionService{
		public:
			JetCorrectionService();
			~JetCorrectionService();
		
			void initialise(eire::ConfigReader *config_reader);
			void correct_jets(std::vector<mor::Jet> *uncorrected_jets, std::vector<mor::Jet> *corrected_jets, double rho=-1);
			double get_correction(mor::Jet *jet, double rho=-1);

		private:
			std::vector<JetCorrectorParameters> jec_parameters;
			double JES_variation;
			FactorizedJetCorrector *JEC;

			std::vector<mor::Jet> *corrected_jets;
	};
}

#endif
