#ifndef MASSJETMATCH_H
#define MASSJETMATCH_H

#include "../MorObjects/MMET.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MTTbarGenEvent.h"

#include "MassReconstruction.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class MassJetMatch{
	public:
		MassJetMatch();
		~MassJetMatch();
		void set_handles(std::vector<mor::Jet>* jets, std::vector<mor::Electron>* electrons, std::vector<mor::Muon>* muons, std::vector<mor::MET> *mets, mor::TTbarGenEvent *gen_evt, std::vector<mor::Muon> *uncut_muons,std::vector<mor::Electron> *uncut_electrons);
		void set_mass_reco(MassReconstruction *mass_reco);
		int nmatches_chi2();
		int nmatches_chi2_1btag();
		int nmatches_chi2_2btag();
		int nmatches_m3();
		int nmatches_Wb();
		int nmatches_min_diff_m3();
		int nmatches_min_diff_m3_1btag();
		int nmatches_min_diff_m3_2btag();
	
	private:
		int match_jets(int jet_id1, int jet_id2, int jet_id3);
		void reset_values();
		void get_GenMatch_quarks();

		bool is_hadW_quark(int jet_id);
		bool is_hadb(int jet_id);
		bool is_lepb(int jet_id);

		void get_matched_recojets_id();
		bool equal_p4(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > p4_1,
				ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > p4_2);

		short int found_id_Wquark1;
		short int found_id_Wquark2;
		short int found_id_hadb;
		short int found_id_lepb;
	
		double min_dRq1;
		double min_dRq2;
		double min_dRq3;

		bool geninfo_available;
		
		mor::TTbarGenEvent *gen_evt;
		MassReconstruction *mass_reco;

		std::vector<mor::Jet>* jets;
		std::vector<mor::Electron>* electrons;
		std::vector<mor::Muon>* muons;
		std::vector<mor::MET>* mets;
		std::vector<mor::Muon> *uncut_muons;
		std::vector<mor::Electron> *uncut_electrons;
};

#endif
