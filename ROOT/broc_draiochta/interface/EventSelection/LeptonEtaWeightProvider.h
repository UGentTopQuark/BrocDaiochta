#ifndef BROC_LEPETAEVENTWEIGHTPROVIDER_H
#define BROC_LEPETAEVENTWEIGHTPROVIDER_H

/** \class broc::LeptonEtaWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on abseta of lepton in event.
 *
 * \author klein
 */

#include "LeptonEfficiencyWeightProvider.h"

namespace broc{
	class LeptonEtaWeightProvider : public LeptonEfficiencyWeightProvider{
		public:
			LeptonEtaWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder);
			~LeptonEtaWeightProvider();
		protected:
			virtual double get_value_for_event();
	};
}

#endif
