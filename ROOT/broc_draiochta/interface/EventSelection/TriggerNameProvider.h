#ifndef EIRE_TRIGGER_NAME_PROVIDER_H
#define EIRE_TRIGGER_NAME_PROVIDER_H

/** \class eire::TriggerNameProvider
 * 
 * \brief Translates predefined trigger IDs (in numerical format) to trigger strings.
 *
 * For simplicity cuts contain at the moment only double numbers and vectors of
 * doubles. Therefore it is necessary to define a mapping (numerical
 * id)-(string) for cuts that select certain criteria according to a string.
 * This is the case for Trigger strings. eire::TriggerNameProvider allows a
 * translation of such predefined numerical IDs to trigger strings as for
 * example HLT_Mu15. Furthermore, for a given trigger also the according L1
 * seed can be returned.
 *
 * 
 * \author walsh, klein
 */

#include <string>
#include <iostream>
#include <map>
#include "../MorObjects/MEventInformation.h"

#include <cstdlib>

namespace eire{
	class TriggerNameProvider{
		public:
			TriggerNameProvider();
			~TriggerNameProvider();

			std::string hlt_name(int id);

			static void set_event_information(mor::EventInformation *event_information);
		private:
			std::map<double,std::string> hlt_names;

			static mor::EventInformation *event_information;
	};
}

#endif
