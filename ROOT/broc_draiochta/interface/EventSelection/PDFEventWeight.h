#ifndef BROC_PDFEVENTWEIGHT_H
#define BROC_PDFEVENTWEIGHT_H

#include "../AnalysisTools/PDFWeightProvider.h"
#include "EventWeight.h"
#include "HandleHolder.h"
#include "CutsSet.h"

namespace broc{
	class PDFEventWeight: public broc::EventWeight{
		public:
			PDFEventWeight(broc::CutsSet *cuts_set, eire::HandleHolder *handle_holder);
			virtual ~PDFEventWeight();

			virtual double get_weight();
		private:
			eire::PDFWeightProvider *pdf_weight_prov;
			int pdf_weight_pos;
			std::string pdf_name;

			bool do_not_reweight;
			bool suppress_pdf_offset_correction;
			bool apply_pdf_weights;
	};
}

#endif
