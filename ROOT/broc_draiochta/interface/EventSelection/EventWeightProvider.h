#ifndef BROC_EVENTWEIGHTPROVIDER_H
#define BROC_EVENTWEIGHTPROVIDER_H

/**
 * \class broc::EventWeightProvider
 * 
 * \brief Abstract class to read event weights from uniform format from text file.
 *
 * Provides common interface to read weight files from uniform text format.
 * Abstract class as daughter classes will have to access relevant variables to
 * get event weights as function of a parameter.
 *
 * \author klein
 */

#include "../../src/ConfigReader/ConfigReader.h"
#include "HandleHolder.h"
#include <string>

namespace broc{
	class EventWeightProvider{
		public:
			EventWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder);
			virtual ~EventWeightProvider();

			double get_weight();

		protected:
			void read_weight_file();
			std::string get_dataset_name();
			int find_bin(double value);
			int find_bin(double value, double value2);

			virtual double get_value_for_event()=0;
			virtual double get_2nd_value_for_event();

			eire::ConfigReader *weight_file_reader;
			eire::HandleHolder *handle_holder;

			std::vector<double> weights;

			bool scale_factor_2D;
                        double nbins;
                        double range_min;
                        double range_max;
                        double bin_width;
			/***
			 * When setting 2D weights number of bins should correspond to nbins*nbins2.
			 * First nbins weights should correspond to the weights for the 
			 * different bins of the first variable in the 1st bins of the second variable.
			 ***/
			double nbins2;
			double range_min2;
			double range_max2;
			double bin_width2;
	};
}

#endif
