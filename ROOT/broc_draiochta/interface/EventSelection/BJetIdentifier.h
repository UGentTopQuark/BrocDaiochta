#ifndef BROC_BJETIDENTIFIER_H
#define BROC_BJETIDENTIFIER_H

/**
 * \class broc::BJetIdentifier
 * 
 * \brief Adds to jets information if they pass the b-tag cut or not.
 *
 * Fills in mor::Jets the bool is_btagged variable, based on the minimum
 * discriminator value defined in the cuts set file.
 *
 * \author klein
 */

#include "HandleHolder.h"
#include "BTagAlgoNameProvider.h"
#include <vector>
#include <string>

namespace broc{
	class BJetIdentifier{
		public:
			BJetIdentifier(eire::HandleHolder *handle_holder);
			~BJetIdentifier();

			void mark_bjets(std::vector<mor::Jet> *jets);
		private:
			std::vector<double> *min_btag;
			std::string name_btag;
	};
}

#endif
