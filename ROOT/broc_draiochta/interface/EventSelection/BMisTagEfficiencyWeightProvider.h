#ifndef BROC_BMISTAGEFFICIENCYWEIGHTPROVIDER_H
#define BROC_BMISTAGEFFICIENCYWEIGHTPROVIDER_H

/** \class broc::LeptonEfficiencyWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on efficiency studies of lepton in event.
 *
 * \author beernaert
 */

#include "../../src/ConfigReader/ConfigReader.h"
#include "HandleHolder.h"
#include "EventWeightProvider.h"
#include "../MorObjects/MJet.h"
#include <string>

namespace broc{
	class BMisTagEfficiencyWeightProvider{
		public:
			BMisTagEfficiencyWeightProvider(eire::HandleHolder *handle_holder);
			~BMisTagEfficiencyWeightProvider();
			double get_weight(int UpDown = 0);
		protected:
	                        double get_MisTag_eff(double pt, double eta, int UpDown);
	                        double get_Data_Prob(int UpDown);
			double get_MC_Prob(int UpDown);
                        	double get_SF(double pt, double eta, int UpDown);
			bool do_not_reweight;
			bool up, down; // systematics switch

			std::vector<mor::Jet> *jets;
	};
}

#endif
