#ifndef BROC_LepBMass_H
#define BROC_LepBMass_H

#include "HandleHolder.h"
#include "BrusselsChi2JetSorter.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"

namespace broc{
	class LepBMass{
		public:
			LepBMass(eire::HandleHolder *handle_holder);
			~LepBMass();

			double get_lep_b_mass(){ return lep_b_mass; };
			inline double get_min_chi2(){ return min_chi2; };
			void next_event();

		private:
			void calc_lep_b_mass();
			std::vector<mor::Jet> *jets;
			std::vector<mor::Electron> *electrons;
			std::vector<mor::Muon> *muons;

			eire::HandleHolder *handle_holder;
			broc::BrusselsChi2JetSorter *chi2_sorter;

			double min_chi2;
			double lep_b_mass;
	};
}

#endif
