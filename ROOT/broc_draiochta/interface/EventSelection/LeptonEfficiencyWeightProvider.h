#ifndef BROC_LEPEFFEVENTWEIGHTPROVIDER_H
#define BROC_LEPEFFEVENTWEIGHTPROVIDER_H

/** \class broc::LeptonEfficiencyWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on efficiency studies of lepton in event.
 *
 * \author klein
 */

#include "../../src/ConfigReader/ConfigReader.h"
#include "HandleHolder.h"
#include "EventWeightProvider.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include <string>

namespace broc{
	class LeptonEfficiencyWeightProvider : public EventWeightProvider{
		public:
			LeptonEfficiencyWeightProvider(std::string weight_file, eire::HandleHolder *handle_holder);
			~LeptonEfficiencyWeightProvider();
			double get_weight();
			double get_weight(mor::Electron *electron,bool is_1D = true);
			double get_weight(mor::Muon *muon,bool is_1D = true);
		protected:
			bool do_not_reweight;

			std::vector<mor::Electron> *electrons;
			std::vector<mor::Muon> *muons;

			bool process_muons;
			bool process_electrons;
	};
}

#endif
