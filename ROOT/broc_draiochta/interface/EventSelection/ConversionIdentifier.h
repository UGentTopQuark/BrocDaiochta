#ifndef EIRE_CONVERSION_IDENTIFIER_H
#define EIRE_CONVERSION_IDENTIFIER_H

/** \class eire::ConversionIdentifier
 * 
 * \brief Based on a list of conversion cuts returns if certain electron results from photon conversion.
 *
 * Based on partner track information and the missing number of inner layers in
 * the electron track for a certain electron it can be decided if the electron
 * comes most likely from conversion or not. Returns bool.
 *
 * \author klein
 */

#include "CutsSet.h"
#include "../MorObjects/MElectron.h"

namespace eire{
	class ConversionIdentifier{
		public:
			ConversionIdentifier();
			~ConversionIdentifier();

			void set_cuts(broc::CutsSet *cuts_set, std::string lepton_type="e");
			bool from_conversion(mor::Electron *electron);
		private:
			double max_dist;		
			double max_dcot;		
			double max_nLostTrackerHits;
			bool init;

			double is_conv;
			double is_not_conv;

			static const bool verbose = false;
	};
};

#endif
