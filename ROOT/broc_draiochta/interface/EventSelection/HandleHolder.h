#ifndef EIRE_HANDLEHOLDER_H
#define EIRE_HANDLEHOLDER_H

#include "../MorObjects/MMuon.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MTrigger.h"
#include "../MorObjects/MTriggerObject.h"
#include "../MorObjects/MTTbarGenEvent.h"
#include "../MorObjects/MEventInformation.h"
#include "../MorObjects/MPrimaryVertex.h"

#include "../AnalysisTools/HistoWriter.h"
#include "../AnalysisTools/PDFWeightProvider.h"
#include "../../src/ConfigReader/ConfigReader.h"
#include "CutsSet.h"
#include "PrescaleProvider.h"
#include "TriggerModuleManager.h"

namespace broc{
	class CentralServices;
}

namespace eire{
	class HandleHolder{
		public:
		        HandleHolder();
		        ~HandleHolder();
			inline void set_trigger_objects(std::vector<mor::TriggerObject> *trigger_objects){ this->trigger_objects = trigger_objects; };
			inline void set_trigger(mor::Trigger *trigger){ this->trigger = trigger; };
			inline void set_jets(std::vector<mor::Jet> *jets){ this->jets = jets; };
			inline void set_electrons(std::vector<mor::Electron> *electrons){ this->electrons = electrons; };
			inline void set_noe_jets(std::vector<mor::Jet> *noe_jets){ this->noe_jets = noe_jets; };
			inline void set_noniso_electrons(std::vector<mor::Electron> *noniso_electrons){ this->noniso_electrons = noniso_electrons; };
			inline void set_muons(std::vector<mor::Muon> *muons){ this->muons = muons; };
			inline void set_nomu_jets(std::vector<mor::Jet> *nomu_jets){ this->nomu_jets = nomu_jets; };
			inline void set_noniso_muons(std::vector<mor::Muon> *noniso_muons){ this->noniso_muons = noniso_muons; };
			inline void set_corrected_mets(std::vector<mor::MET> *corrected_mets){ this->corrected_mets = corrected_mets; };
			inline void set_mets(std::vector<mor::MET> *mets){ this->mets = mets; };

			inline void set_selected_mets(std::vector<mor::MET> *selected_mets){ this->selected_mets = selected_mets; };
			inline void set_tight_jets(std::vector<mor::Jet> *tight_jets){ this->tight_jets = tight_jets; };
			inline void set_tight_electrons(std::vector<mor::Electron> *tight_electrons){ this->tight_electrons = tight_electrons; };
			inline void set_tight_muons(std::vector<mor::Muon> *tight_muons){ this->tight_muons = tight_muons; };
			inline void set_loose_electrons(std::vector<mor::Electron> *loose_electrons){ this->loose_electrons = loose_electrons; };
			inline void set_loose_mm_electrons(std::vector<mor::Electron> *loose_mm_electrons){ this->loose_mm_electrons = loose_mm_electrons; };
			inline void set_loose_muons(std::vector<mor::Muon> *loose_muons){ this->loose_muons = loose_muons; };
			inline void set_loose_mm_muons(std::vector<mor::Muon> *loose_mm_muons){ this->loose_mm_muons = loose_mm_muons; };
			
			inline void set_selected_noe_jets(std::vector<mor::Jet> *selected_noe_jets){ this->selected_noe_jets = selected_noe_jets; };
			inline void set_selected_noniso_electrons(std::vector<mor::Electron> *selected_noniso_electrons){ this->selected_noniso_electrons = selected_noniso_electrons; };
			inline void set_selected_nomu_jets(std::vector<mor::Jet> *selected_nomu_jets){ this->selected_nomu_jets = selected_nomu_jets; };
			inline void set_selected_noniso_muons(std::vector<mor::Muon> *selected_noniso_muons){ this->selected_noniso_muons = selected_noniso_muons; };

			inline void set_primary_vertices(std::vector<mor::PrimaryVertex> *primary_vertices){ this->primary_vertices = primary_vertices; };
			inline void set_tight_primary_vertices(std::vector<mor::PrimaryVertex> *tight_primary_vertices){ this->tight_primary_vertices = tight_primary_vertices; };
			inline void set_gen_particles(std::vector<mor::Particle> *gen_particles){ this->gen_particles = gen_particles; };
			inline void set_event_information(mor::EventInformation *event_information){ this->event_information = event_information; };
			inline void set_histo_writer(HistoWriter *histo_writer){ this->histo_writer = histo_writer; };
			inline void set_ttbar_gen_evt(mor::TTbarGenEvent *gen_evt){ this->gen_evt = gen_evt; };
			inline void set_ident(std::string ident){ this->ident = ident; };
			inline void set_config_reader(eire::ConfigReader *config_reader){ this->config_reader = config_reader; };
			inline void set_event_weight(double event_weight){ this->event_weight = event_weight; };
			inline void set_failing_event_weight(double failing_event_weight){ this->failing_event_weight = failing_event_weight; };
			inline void set_pdf_weight_provider(eire::PDFWeightProvider *pdf_weight_provider){ this->pdf_weight_provider = pdf_weight_provider; };
			inline void set_services(broc::CentralServices *central_services){ this->services_ = central_services; };
			inline void set_cuts_set(broc::CutsSet *cuts_set){ this->cuts_set = cuts_set; };
			inline void set_prescale_provider(broc::PrescaleProvider *prescale_prov){ this->prescale_prov = prescale_prov; };
			inline void set_trigger_module_manager(broc::TriggerModuleManager *module_manager){ this->trigger_module_manager = module_manager; };

                        inline void set_hitfit_jet_combination(std::vector<int> hitfit_combi_seed){ this->hitfit_combi_seed = hitfit_combi_seed; };                                   
			inline void set_hitfit_jets(std::vector<mor::Particle> *hitfit_jets){ this->hitfit_jets = hitfit_jets; };
			inline void set_hitfit_electrons(std::vector<mor::Particle> *hitfit_electrons){ this->hitfit_electrons = hitfit_electrons; };
			inline void set_hitfit_muons(std::vector<mor::Particle> *hitfit_muons){ this->hitfit_muons = hitfit_muons; };
			inline void set_hitfit_mets(std::vector<mor::Particle> *hitfit_mets){ this->hitfit_mets = hitfit_mets; };

			inline void set_btag_cut_state(bool has_btag_cut){this->has_btag_cut = has_btag_cut;};

			inline mor::Trigger* get_trigger(){ return trigger; };
			inline std::vector<mor::TriggerObject>* get_trigger_objects(){ return trigger_objects; };
			inline std::vector<mor::Jet>* get_jets(){ return jets; };
			inline std::vector<mor::Jet>* get_noe_jets(){ return noe_jets; };
			inline std::vector<mor::Jet>* get_nomu_jets(){ return nomu_jets; };
			inline std::vector<mor::Electron>* get_electrons(){ return electrons; };
			inline std::vector<mor::Electron>* get_noniso_electrons(){ return noniso_electrons; };
			inline std::vector<mor::Muon>* get_muons(){ return muons; };
			inline std::vector<mor::Muon>* get_noniso_muons(){ return noniso_muons; };
			inline std::vector<mor::MET>* get_corrected_mets(){ return corrected_mets; };
 			inline std::vector<mor::MET>* get_mets(){ return mets; };
			inline std::vector<mor::MET>* get_selected_mets(){ return selected_mets; };

			inline std::vector<mor::Jet>* get_tight_jets(){ return tight_jets; };
			inline std::vector<mor::Electron>* get_tight_electrons(){ return tight_electrons; };
			inline std::vector<mor::Muon>* get_tight_muons(){ return tight_muons; };
			inline std::vector<mor::Electron>* get_loose_electrons(){ return loose_electrons; };
			inline std::vector<mor::Electron>* get_loose_mm_electrons(){ return loose_mm_electrons; };
			inline std::vector<mor::Muon>* get_loose_muons(){ return loose_muons; };
			inline std::vector<mor::Muon>* get_loose_mm_muons(){ return loose_mm_muons; };
			inline std::vector<mor::PrimaryVertex>* get_primary_vertices(){ return primary_vertices; };
			inline std::vector<mor::PrimaryVertex>* get_tight_primary_vertices(){ return tight_primary_vertices; };
			inline std::vector<mor::Jet>* get_selected_noe_jets(){ return selected_noe_jets; };
			inline std::vector<mor::Electron>* get_selected_noniso_electrons(){ return selected_noniso_electrons; };
			inline std::vector<mor::Jet>* get_selected_nomu_jets(){ return selected_nomu_jets; };
			inline std::vector<mor::Muon>* get_selected_noniso_muons(){ return selected_noniso_muons; };

                        inline std::vector<int> get_hitfit_combination(){ return hitfit_combi_seed; };
			inline std::vector<mor::Particle>* get_hitfit_jets(){ return hitfit_jets; };
			inline std::vector<mor::Particle>* get_hitfit_electrons(){ return hitfit_electrons; };
			inline std::vector<mor::Particle>* get_hitfit_muons(){ return hitfit_muons; };
			inline std::vector<mor::Particle>* get_hitfit_mets(){ return hitfit_mets; };

			inline bool get_btag_cut_state(){ return has_btag_cut; };

			inline mor::EventInformation* get_event_information(){ return event_information; };
			inline std::vector<mor::Particle>* get_gen_particles(){ return gen_particles; };
			inline mor::TTbarGenEvent* get_ttbar_gen_evt(){ return gen_evt; };
			inline double get_event_weight(){ return event_weight; };
			inline double get_failing_event_weight(){ return failing_event_weight; };
			inline eire::PDFWeightProvider *get_pdf_weight_provider(){ return pdf_weight_provider; };
			inline broc::CutsSet* get_cuts_set(){ return cuts_set; }
			inline broc::PrescaleProvider* get_prescale_provider(){ return prescale_prov; };
			inline broc::TriggerModuleManager* get_trigger_module_manager(){ return trigger_module_manager; };

			inline HistoWriter* get_histo_writer(){ return histo_writer; };
			inline eire::ConfigReader* get_config_reader(){ return config_reader; };

			inline broc::CentralServices *services(){ return services_; };

			inline std::string get_ident(){ return ident; };
		private:
			std::vector<mor::Electron> *tight_electrons;
			std::vector<mor::Muon> *tight_muons;
			std::vector<mor::Jet> *tight_jets;

			std::vector<mor::MET> *corrected_mets;
			std::vector<mor::MET> *mets;
			std::vector<mor::MET> *selected_mets;

			std::vector<mor::Jet> *jets;
			std::vector<mor::Electron> *electrons;
			std::vector<mor::Muon> *muons;

			std::vector<mor::Muon>* loose_muons;
			std::vector<mor::Electron>* loose_electrons;

			std::vector<mor::Muon>* loose_mm_muons;
			std::vector<mor::Electron>* loose_mm_electrons;

			std::vector<mor::Jet> *noe_jets;
			std::vector<mor::Electron> *noniso_electrons;
			std::vector<mor::Jet> *selected_noe_jets;
			std::vector<mor::Electron> *selected_noniso_electrons;

			std::vector<mor::Particle> *hitfit_jets;
			std::vector<mor::Particle> *hitfit_electrons;
			std::vector<mor::Particle> *hitfit_muons;
			std::vector<mor::Particle> *hitfit_mets;

			std::vector<mor::Jet> *nomu_jets;
			std::vector<mor::Muon> *noniso_muons;
			std::vector<mor::Jet> *selected_nomu_jets;
			std::vector<mor::Muon> *selected_noniso_muons;

			std::vector<mor::PrimaryVertex>* primary_vertices;
			std::vector<mor::PrimaryVertex>* tight_primary_vertices;
			std::vector<mor::TriggerObject>* trigger_objects;
			std::vector<mor::Particle>* gen_particles;
			mor::EventInformation* event_information;
			mor::Trigger* trigger;

			broc::CutsSet *cuts_set;
			mor::TTbarGenEvent *gen_evt;
			eire::ConfigReader *config_reader;
			eire::PDFWeightProvider *pdf_weight_provider;

			broc::PrescaleProvider *prescale_prov;
			broc::TriggerModuleManager *trigger_module_manager;

			std::vector<int> hitfit_combi_seed;

			bool has_btag_cut;

			HistoWriter *histo_writer;

			broc::CentralServices *services_;

			double event_weight;
			double failing_event_weight;

			std::string ident;
	};
}

#endif
