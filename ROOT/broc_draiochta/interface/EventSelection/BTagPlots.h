#ifndef BTAGPLOTS_H
#define BTAGPLOTS_H

#include "Plots.h"
#include "BJetFinder.h"
#include "../MorObjects/MTTbarGenEvent.h"
#include "Math/LorentzVector.h"
#include "Math/VectorUtil.h"

class BTagPlots : public Plots{
	public:
		BTagPlots(std::string ident);
		~BTagPlots();

		void set_gen_evt(mor::TTbarGenEvent *gen_evt);
		void set_bjet_finder(BJetFinder *bjet_finder);
		void set_btag_cuts(std::vector<double> *min,double n);
	private:
		void book_histos();
		virtual void plot_all();

//		void plot_MCmethod_comparison();
		void plot_bDiscriminator_find_btagcut();
		void analyse_btag_algorithms();
		void plot_bDiscriminator();
		void plot_bDiscriminator_allbjets();
		void plot_bquarks();
		void plot_jet_ids();
		bool equal_p4(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > p4_1,
				ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > p4_2);

		//get these values from Cuts.cc
		std::vector<double> *min_btag;
		std::string name_btag;

		BJetFinder *bjet_finder;
};

#endif
