#ifndef BROC_CROSSTRIGGERWEIGHTPROVIDER_H
#define BROC_CROSSTRIGGERWEIGHTPROVIDER_H

/** \class broc::LeptonEfficiencyWeightProvider
 * 
 * \brief Calculate event weights for reweighting MC for pile up distribution
 * in data.
 *
 * Calculate event weights for compensating MC to data discrepancy in pile-up
 * distribution. Based on efficiency studies of lepton in event.
 *
 * \author klein
 */

#include "../../src/ConfigReader/ConfigReader.h"
#include "HandleHolder.h"
#include "EventWeightProvider.h"
#include "../MorObjects/MElectron.h"
#include "TopTriggerEfficiencyProvider.h"
#include <string>

namespace broc{
	class CrossTriggerWeightProvider{
		public:
			CrossTriggerWeightProvider(eire::HandleHolder *handle_holder);
			~CrossTriggerWeightProvider();
			double get_weight(double pu);
		protected:
			bool do_not_reweight;
			bool up, down; // systematics switch

			std::vector<mor::Electron> *electrons;
			std::vector<mor::Muon> *muons;
			std::vector<mor::Jet> *jets;
			std::vector<mor::PrimaryVertex> *vertices;
			TopTriggerEfficiencyProvider *weight_provider;
			mor::EventInformation *event_information;
	};
}

#endif
