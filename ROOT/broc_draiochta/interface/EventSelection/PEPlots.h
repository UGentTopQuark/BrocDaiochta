#ifndef BROC_PEPLOTS_H
#define BROC_PEPLOTS_H

#include "Plots.h"
#include "MassReconstruction.h"
#include "TMath.h"
#include "Tools.h"
#include <vector>
#include "TRandom3.h"
#include "CentralServices.h"
#include "../AnalysisTools/ETH1D.h"

namespace broc{
	class PEPlots : public Plots{
		public:
			PEPlots(eire::HandleHolder *handle_holder);
			~PEPlots();

			void plot();

		private:
			void book_histos();
			void select_PE();
			virtual void plot_all();
	
			MassReconstruction *mass_reco;

			static int ninstances;
			int instance_id;
			std::string id;

			int NPE;
			double sel_prob;

			bool plot_Mlb;
			bool plot_M3;

			static TRandom3 *rnd;

			static std::vector<bool> fill_PE;
			static std::map<std::string, eire::TH1D*> histos1d;

			std::map<std::string, double> nevents;
	};
}

#endif
