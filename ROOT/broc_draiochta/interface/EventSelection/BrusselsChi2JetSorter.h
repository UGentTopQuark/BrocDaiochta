#ifndef BROC_BRUSSELSCHI2JETSORTER_H
#define BROC_BRUSSELSCHI2JETSORTER_H

#include "HandleHolder.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MMET.h"
#include "../AnalysisTools/ETH1D.h"
#include "BTagAlgoNameProvider.h"

#include "Math/VectorUtil.h"

#include "../MorObjects/MTTbarGenEvent.h"

#include <map>

namespace broc{
	class BrusselsChi2JetSorter{
		public:
			BrusselsChi2JetSorter(eire::HandleHolder *handle_holder);
			~BrusselsChi2JetSorter();

			bool reconstruct_ttbar();	/// returns true if successful reconstruction was possible
			int get_lep_b_id(){ return jet_id4; };
			double get_min_chi2(){ return min_chi2; };
		private:
			void debug(int jet_id4);

			eire::HandleHolder *handle_holder;

			std::vector<mor::Jet> *jets;
			std::vector<mor::Electron> *electrons;
			std::vector<mor::Muon> *muons;
			std::vector<mor::MET> *mets;

			double min_lep_b_btag;

			std::string name_btag;

			std::map<std::string, eire::TH1D*> histos1d;

			std::map<std::string, double> cnt;

			double min_chi2;
			int jet_id1;	/// b from had top
			int jet_id2;	/// had W
			int jet_id3;	/// had W
			int jet_id4;	/// b from lep top

			static const bool verbose = false;
	};
}

#endif
