#ifndef BROC_ISOMUSFPROVIDER2012_H
#define BROC_ISOMUSFPROVIDER2012_H

#include "HandleHolder.h"

namespace broc{
	class IsoMuSFProvider2012{
		public:
			IsoMuSFProvider2012(eire::HandleHolder *handle_holder);
			~IsoMuSFProvider2012();

			double get_scale_factor_IsoMu24(int UpDown = 0);
		private:
			eire::HandleHolder *handle_holder;
			bool up, down; // systematics switch          

			bool do_not_reweight;
	};
}

#endif
