#ifndef PLOTS_H
#define PLOTS_H

/** \class Plots
 * 
 * \brief Mother class for classes implementing plots of different categories.
 *
 * Plots is an common interface for classes which manage the creation of plots.
 * The different sub-classes collect plots which belong in a sense together,
 * eg. MassPlots, IsolatedLeptonPlots, BTagPlots, etc.. The class provides the
 * handling for the different collection handles and a frame for booking
 * histograms.
 *
 * \authors walsh, klein
 */

#include "../MorObjects/MMET.h"
#include "../MorObjects/MElectron.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MTTbarGenEvent.h"
#include "../MorObjects/MPrimaryVertex.h"

#include <iostream>
#include "../AnalysisTools/ETH2D.h"
#include "../AnalysisTools/ETH1D.h"
#include <map>

#include "../AnalysisTools/HistoWriter.h"
#include "HandleHolder.h"
#include "CentralServices.h"


class Plots{
	public:
		Plots();
		virtual ~Plots();
		void set_handles(eire::HandleHolder *handle_holder);

		/// Deprecated. Use set_handles(eire::HandleHolder *handle_holder) instead
		void set_handles(std::vector<mor::Jet>* jets,
				 std::vector<mor::Electron>* isolated_electrons,
				 std::vector<mor::Muon>* isolated_muons,
				 std::vector<mor::MET>* corrected_mets);
		/// Deprecated. Use set_handles(eire::HandleHolder *handle_holder) instead
		void set_handles(std::vector<mor::Jet>* jets,
				 std::vector<mor::Electron>* isolated_electrons,
				 std::vector<mor::Electron>* loose_electrons,				 
				 std::vector<mor::Muon>* isolated_muons,
				 std::vector<mor::Muon>* loose_muons,
				 std::vector<mor::MET>* corrected_mets);
		void plot();
		void set_histo_writer(HistoWriter *histo_writer);
		void set_gen_evt(mor::TTbarGenEvent *gen_evt);
	protected:
		std::string id;
                std::map<std::string,eire::TH1D*> histos1d;
                std::map<std::string,eire::TH2D*> histos2d;

		virtual void prepare_objects();
		virtual void book_histos();
		virtual void plot_all()=0;

		eire::HandleHolder *handle_holder;

		std::vector<mor::Jet>* jets;
		std::vector<mor::Electron>* isolated_electrons;
		std::vector<mor::Electron>* loose_electrons; //jl 19.08.10
		std::vector<mor::Muon>* isolated_muons;
		std::vector<mor::Muon>* loose_muons; //jl 15.07.10
		std::vector<mor::MET>* corrected_mets;
		std::vector<mor::Particle>* gen_particles; //kb 9.01.12

		bool histos_booked;

		HistoWriter *histo_writer;
		mor::TTbarGenEvent *gen_evt;
};

#endif
