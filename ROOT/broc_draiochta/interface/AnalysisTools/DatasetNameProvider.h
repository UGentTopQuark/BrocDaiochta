#ifndef BROC_DATASETNAMEPROVIDER_H
#define BROC_DATASETNAMEPROVIDER_H

/** \class broc::DatasetNameProvider
 * 
 * \brief Allows translation of input identifiers to new identifiers in histogram names.
 *
 * \authors klein
 */

#include "../../src/ConfigReader/ConfigReader.h"

#include <boost/xpressive/xpressive.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>

namespace broc{
	class DatasetNameProvider{
		public:
			DatasetNameProvider(eire::ConfigReader *config_reader);
			~DatasetNameProvider();

			std::string get_name(std::string input_name);
		private:
			void initialise();

			eire::ConfigReader *name_map_reader;
			std::string mapping_file_name;

			std::map<std::string, std::string> name_mapping;
	};
}

#endif
