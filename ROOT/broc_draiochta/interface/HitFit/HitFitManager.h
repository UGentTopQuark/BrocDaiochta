#ifndef HITFIT_HITFITMANAGER_H
#define HITFIT_HITFITMANAGER_H

#include "HitFitProducer.h"

#include "TFile.h"
#include "../MorObjects/MMET.h"
#include "../MorObjects/MJet.h"
#include "../MorObjects/MMuon.h"
#include "../MorObjects/MParticle.h"
#include "../MorObjects/MElectron.h"
#include "../EventSelection/CutsSet.h"
#include "../EventSelection/HandleHolder.h"

namespace hitfit{
	class HitFitManager{
		public:
                	HitFitManager(broc::CutsSet  *cuts_set, eire::HandleHolder *handle_holder);
			~HitFitManager();

			void fill_trees();
		private:
			eire::HandleHolder *handle_holder;
			TFile *ntuple_file;
			HitFitProducer* hitfit_producer;
			bool enable_hitfit;
			float chisq_cut;
			bool correct_FSR;
	};
}

#endif
