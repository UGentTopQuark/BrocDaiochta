#ifndef MOR_ELECTRON_H
#define MOR_ELECTRON_H

#include "MLepton.h"
#include "../BeagObjects/Electron.h"

namespace mor{
	class Electron : public mor::Lepton{
		public:
                        Electron();
			Electron(beag::Electron &beag_electron);
			void set_beag_info(beag::Electron &beag_lepton);
			void set_year(int year = 2011);
			double supercluster_eta();
			double supercluster_phi();
			inline double dist(){ return dist_val; };
			inline double dcot(){ return dcot_val; };
			inline int passconversionveto() { return passconversionveto_val; }; //jl 04.05.12
			inline double sigmaIetaIeta(){ return sigmaIetaIeta_; };
			inline double deltaPhiSuperClusterTrackAtVtx(){ return deltaPhiSuperClusterTrackAtVtx_; };
			inline double deltaEtaSuperClusterTrackAtVtx(){ return deltaEtaSuperClusterTrackAtVtx_; };
			inline double hadronicOverEm(){ return hadronicOverEm_; };

  			inline double myMVAVar_fbrem(){ return myMVAVar_fbrem_; };
  			inline double myMVAVar_kfchi2(){ return myMVAVar_kfchi2_; };
  			inline double myMVAVar_kfhits(){ return myMVAVar_kfhits_; }; 
  			inline double myMVAVar_kfvalidhits(){ return myMVAVar_kfvalidhits_; }; 
  			inline double myMVAVar_gsfchi2(){ return myMVAVar_gsfchi2_; };  // to be checked 

  			inline double myMVAVar_deta(){ return myMVAVar_deta_; };
  			inline double myMVAVar_dphi(){ return myMVAVar_dphi_; };
  			inline double myMVAVar_detacalo(){ return myMVAVar_detacalo_; };
  			inline double myMVAVar_dphicalo(){ return myMVAVar_dphicalo_; };   

  			inline double myMVAVar_see(){ return myMVAVar_see_; };    //EleSigmaIEtaIEta

  			inline double myMVAVar_spp(){ return myMVAVar_spp_; };
  			inline double myMVAVar_etawidth(){ return myMVAVar_etawidth_; };
  			inline double myMVAVar_phiwidth(){ return myMVAVar_phiwidth_; };
  			inline double myMVAVar_e1x5e5x5(){ return myMVAVar_e1x5e5x5_; };
  			inline double myMVAVar_R9(){ return myMVAVar_R9_; };
  			inline double myMVAVar_nbrems(){ return myMVAVar_nbrems_; };

  			inline double myMVAVar_HoE(){ return myMVAVar_HoE_; };
  			inline double myMVAVar_EoP(){ return myMVAVar_EoP_; };
  			inline double myMVAVar_IoEmIoP(){ return myMVAVar_IoEmIoP_; };  // in the future to be changed with ele.gsfTrack()->p()
  			inline double myMVAVar_eleEoPout(){ return myMVAVar_eleEoPout_; };
  			inline double myMVAVar_EoPout(){ return myMVAVar_EoPout_; };
  			inline double myMVAVar_PreShowerOverRaw(){ return myMVAVar_PreShowerOverRaw_; };

  			inline double myMVAVar_eta(){ return myMVAVar_eta_; };         
  			inline double myMVAVar_pt(){ return myMVAVar_pt_; };       

  			inline double myMVAVar_d0(){ return myMVAVar_d0_; };
			inline double myMVAVar_ip3d(){ return myMVAVar_ip3d_; }; 
			
			double EA_PFrelIso();
		private:
			double sc_eta_val;
			double sc_phi_val;
			double dist_val;
			double dcot_val;
			int passconversionveto_val; //jl 04.05.12
			double sigmaIetaIeta_;
			double deltaPhiSuperClusterTrackAtVtx_;
			double deltaEtaSuperClusterTrackAtVtx_;
			double hadronicOverEm_;

  			double myMVAVar_fbrem_;
  			double myMVAVar_kfchi2_;
  			double myMVAVar_kfhits_; 
  			double myMVAVar_kfvalidhits_; 
  			double myMVAVar_gsfchi2_;  // to be checked 

  			double myMVAVar_deta_;
  			double myMVAVar_dphi_;
  			double myMVAVar_detacalo_;
  			double myMVAVar_dphicalo_;   

  			double myMVAVar_see_;    //EleSigmaIEtaIEta

  			double myMVAVar_spp_;
  			double myMVAVar_etawidth_;
  			double myMVAVar_phiwidth_;
  			double myMVAVar_e1x5e5x5_;
  			double myMVAVar_R9_;
  			double myMVAVar_nbrems_;

  			double myMVAVar_HoE_;
  			double myMVAVar_EoP_;
  			double myMVAVar_IoEmIoP_;  // in the future to be changed with ele.gsfTrack()->p()
  			double myMVAVar_eleEoPout_;
  			double myMVAVar_EoPout_;
  			double myMVAVar_PreShowerOverRaw_;

  			double myMVAVar_eta_;         
  			double myMVAVar_pt_;       

  			double myMVAVar_d0_;
			double myMVAVar_ip3d_;
			double rhocor_year;
	};

	inline double Electron::supercluster_eta() { return sc_eta_val; };
	inline double Electron::supercluster_phi() { return sc_phi_val; };
}

#endif
