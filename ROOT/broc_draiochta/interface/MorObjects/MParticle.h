#ifndef MOR_PARTICLE_H
#define MOR_PARTICLE_H

#include "TLorentzVector.h"
#include "../BeagObjects/Particle.h"
#include "Math/LorentzVector.h"
#include "Math/PxPyPzM4D.h"

namespace mor{
	class Particle: public ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >{
		public:
			Particle();
			Particle(ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > p4);
			Particle(beag::Particle &beag_particle);
			~Particle();
			bool mc_matched();
			bool from_ttbar();
			int charge();
			int mc_match_id();
			void set_beag_info(beag::Particle &beag_particle);
			ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > mc_p4();
			ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > p4();
		protected:
			bool mc_matched_val;	// flag if monte carlo truth matched

			bool from_ttbar_decay;	// if particle is from ttbar decay

			ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > mc_match_p4;
			int mc_id;		// id of monte carlo truth matched parton

			signed char charge_val;
	};

	inline bool Particle::mc_matched() { return mc_matched_val; }
	inline bool Particle::from_ttbar() { return from_ttbar_decay; }
	inline int Particle::mc_match_id() { return mc_id; }
	inline ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > Particle::mc_p4() { return mc_match_p4; }
	inline ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> > Particle::p4() { return *dynamic_cast<ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<double> >*>(this); }
	inline int Particle::charge() { return charge_val; }
}

#endif
