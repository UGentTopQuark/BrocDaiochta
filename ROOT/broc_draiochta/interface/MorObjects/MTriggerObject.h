#ifndef MOR_TRIGGEROBJECT_H
#define MOR_TRIGGEROBJECT_H

#include "MParticle.h"
#include "../BeagObjects/TriggerObject.h"
#include <map>
#include "../NTupleReader/MIDChecker.h"

namespace mor{
	class TriggerObject : public Particle{
		public:
			TriggerObject();
			TriggerObject(beag::TriggerObject &beag_trigger_object);
			~TriggerObject();
			void set_beag_info(beag::TriggerObject &beag_trigger_object);

			/*
			 *	modules:
			 *	0: second last filter passed
			 *	1: l1 seed filter passed
			 */
			bool triggered(int trigger_idx, int module_idx);

			inline int quality(){ return quality_val; };
			inline int is_forward(){ return is_forward_val; };
			inline double bx(){ return bx_val; };
			inline double detector(){ return detector_val; };
		private:
			std::vector<std::vector<bool> > path_triggered;	// final filter triggered

			int quality_val;
			int is_forward_val;
			double bx_val;
			double detector_val;
	};
}

#endif
