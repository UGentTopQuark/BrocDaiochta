#ifndef BEAG_MYLHCOEVENT_H
#define BEAG_MYLHCOEVENT_H

#include "TObject.h"
#include <map>

namespace beag{
    class MyLHCOEvent: public TObject{
    public:
	MyLHCOEvent(): Jet1(),Jet2(),Jet3(),Jet4(),Lepton(),MET(),L_signal(), L_bkg(){};
	    ~MyLHCOEvent(){};
	
	    class LHCOParticle{
	    public:
		int typ;
		float eta;
		float phi;
		float pt;
		float jmass;
		float ntrk;
		int btag;
		float hadOverem;
		int is_gen;
		float E;
		int mc_id;

		ClassDef(LHCOParticle, 1)
		    };
	    
	    LHCOParticle Jet1;
	    LHCOParticle Jet2;
	    LHCOParticle Jet3;
	    LHCOParticle Jet4;
	    LHCOParticle Lepton;
	    LHCOParticle MET;
	    double weight;
	    bool accepted;
	    double event_number;
	    double run_number;
	    std::map<std::pair<float,float>, std::vector<float> > L_signal;	
	    std::map<std::pair<float,float>, std::vector<float> > L_bkg;
	    
	    
	ClassDef(MyLHCOEvent, 1);
	
    };
}

#endif
