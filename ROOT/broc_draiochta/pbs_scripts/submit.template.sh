#!/bin/bash

#variables
vers="VERSION"
cutfil="CUTFIL"
sample="SAMPLE"
script="SCRIPT"
dest="VERSION-SAMPLE-PROCESS"
indir="MYINDIR"
pathtorel="MYREL"
boost="MYBOOST"
subpath="SUBPATH"
isdata="ISDATA"

h="HEAD"
t="TAIL"

#setup 
#export SCRAM_ARCH=slc5_ia32_gcc434
#source $VO_CMS_SW_DIR/cmsset_default.sh
#cd $pathtorel
#eval `scramv1 runtime -sh`
#export CPLUS_INCLUDE_PATH=$boost/include:${CPLUS_INCLUDE_PATH}
#export CPLUS_INCLUDE_PATH=${CPLUS_INCLUDE_PATH}:~/CMS/ttAnalysis/broc_draiochta/share/
#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$boost/lib
#export LIBRARY_PATH=$boost/lib:${LIBRARY_PATH}
export CLHEPSYS=/localgrid_mnt/localgrid/bklein/software/clhep_2_1_2_0
export NEUROBAYES=/localgrid/bklein/software/neurobayes/10.12/slc4_amd64_gcc34/
export LD_LIBRARY_PATH=$NEUROBAYES/lib:$LD_LIBRARY_PATH
export PATH=$NEUROBAYES/external:$PATH
export ROOTSYS=/localgrid/bklein/software/root_5_28c_NB/
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${ROOTSYS}/lib/root
export PATH=${ROOTSYS}/bin:${PATH}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/localgrid/bklein/software/boost_1_44/lib:${CLHEPSYS}/lib
export CPLUS_INCLUDE_PATH=/localgrid/bklein/software/boost_1_44/include/:${CPLUS_INCLUDE_PATH}:/user/bklein/localgrid/broc_draiochta/share:${CLHEPSYS}/include
export LIBRARY_PATH=/localgrid/bklein/software/boost_1_44/lib:${CLHEPSYS}/lib
cd $subpath

#create directory for job to run in and copy necessary files
cd /scratch
echo "scratch"
pwd
mkdir $dest
dccp /localgrid/jlellouc/jobs_broc-draiochta/$vers-flat-$sample $dest/.
dccp /localgrid/jlellouc/jobs_broc-draiochta/$vers-flat-${sample}_wc $dest/.
dccp /localgrid/jlellouc/jobs_broc-draiochta/$script.sh $dest/.
echo "dccp done"
#cp TrainTMVA_RF.weights.txt $dest/.
#cp default_config_sed.txt $dest/.
#cp allowed*.txt $dest/.
#cp pu_pv_event_weights*.txt $dest/.
#cp -r $vers-share $dest/share
cd $dest
mkdir weights
dccp  /localgrid/jlellouc/jobs_broc-draiochta/$vers-weights/TMVAClassification_NeuroBayes_electron.weights.xml weights/.
dccp  /localgrid/jlellouc/jobs_broc-draiochta/$vers-weights/TMVAClassification_NeuroBayes_electron.class.C weights/.
dccp  /localgrid/jlellouc/jobs_broc-draiochta/$vers-weights/TMVAClassification_NeuroBayes_electron.NB_weights.nb weights/.
pwd
echo 'about to ln version '$vers$' in '$dest' '
ln -s /localgrid/jlellouc/jobs_broc-draiochta/$vers-share ./share
echo 'ln done'

#run the job
time ./$script.sh $vers $sample $h $t $indir $isdata

#then clean after ourselves
echo "copy back and clean up"
mkdir /localgrid/jlellouc/jobs_broc-draiochta/$dest-loc
dccp ${vers}-${sample}.txt /localgrid/jlellouc/jobs_broc-draiochta/$dest-loc/.
dccp ${vers}-${sample}.root /localgrid/jlellouc/jobs_broc-draiochta/$dest-loc/.
dccp /localgrid/jlellouc/jobs_broc-draiochta/${vers}-share/default_config_sed_data.txt /localgrid/jlellouc/jobs_broc-draiochta/$dest-loc/.
dccp /localgrid/jlellouc/jobs_broc-draiochta/${vers}-share/default_config_sed_mc.txt /localgrid/jlellouc/jobs_broc-draiochta/$dest-loc/.
cd /scratch
rm -rf $dest
