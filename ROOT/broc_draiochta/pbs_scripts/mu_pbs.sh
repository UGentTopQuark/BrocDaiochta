#!/bin/bash

if  [ "$1" = ''  -o  "$2" = ''  -o  "$3" = ''  -o  "$4" = '' -o "$5" = '' -o "$6" = '' ] 
then
	echo
	echo './mu.sh <vers> <process> <head> <tail> <indir> <isdata>'
	echo
	exit 1
fi

vers="$1"
process="$2"
h="$3"
t="$4"
indir="$5"
isdata="$6"

here=`pwd`
cd ${indir}
ls ${process}*.root -1 | sed -e "s/\n/\ /g" | head --lines=${h} | tail -${t} > $here/${process}_input.txt
cd $here

ls $indir/${process}*.root -1 | head --lines=${h} | tail -${t} > $here/copyme_input.txt

cat copyme_input.txt | sed -e "s/\/nas/dccp\ \/nas/" | sed -e "s/root/root\ ./" > copyme
echo "copying junk"

myfilename=`whoami`.$RANDOM.$RANDOM.$RANDOM
nrj=`ls -l /nasmount/Gent/tmp/running_jobs | wc -l`
while [ "$nrj" -ge 30 ]
do
        echo "There are already more than 30 copying jobs, sleeping 10s before re-trying..."
        sleep 10
        nrj=`ls -l /nasmount/Gent/tmp/running_jobs | wc -l`
done
touch /nasmount/Gent/tmp/running_jobs/$myfilename
time source copyme
rm /nasmount/Gent/tmp/running_jobs/$myfilename

fil=""

if [ $isdata = "1" ]
then
	fil="share/default_config_sed_data.txt"
else
	fil="share/default_config_sed_mc.txt"
fi

cat $fil | sed "s/PROCESS/$process/g"  | sed "s/VERS/$vers/g" > $vers-${process}_config.txt 

echo "running"
time ./${vers}-flat-${process} $vers-${process}_config.txt >& ${vers}-${process}.txt
mv ${process}_plots.root ${vers}-${process}.root
echo "done"
