#!/usr/bin/perl

use threads;
use Getopt::Std;
use Thread::Semaphore;
use perl::Digest::Adler32;

getopts("j:hm:i:c:ys:x:");

@errors = ();
push(@errors, "\n=========== ERROR SUMMARY ===========\n");

$run_cfg = 'run.cfg';
$run_cfg = $opt_x if($opt_x);
die "can't find $run_cfg, exiting..." unless(-e $run_cfg);

do "$run_cfg";

my $cutselector = "src/EventSelection/CutSelector.cc";		# where to find CutSelector.cc
my $cut_gen_dir = "cutset_generator";				# where is cutset_generator?
my $json2cpp_dir = "json2cpp";				# where is cutset_generator?
my $user_id = `whoami`;
chomp $user_id;
my $computer_id = `hostname`;
chomp $computer_id;
my $log = "$user_id\_$computer_id\_logfile.log";				# specify logfile

my $local_node_directory = "/scratch/";

my $globalmaxnjobs = 30;

my $logo = "share/logo.txt";

$nthreads = $opt_j if(defined($opt_j));

$files_per_job = 0 if(!defined($files_per_job));
$walltime = "01:00:00" if(!defined($walltime));

$force_time = 0 if(!defined($force_time));
$disable_svn_log_for_submission = 1 if(!defined($disable_svn_log_for_submission));

if(defined($opt_c)){
	$cuts_definition = $opt_c;
	$set_cuts = 1;
}

if(defined($opt_h)){
	print <<EOF;
usage: $0 [-h] [-i identifier] [-j <# processes>] [-m <message>] [-c <cutset_definition>] [-y]
	-c	set cuts according to config file (relative to cutset_generator/ directory)
	-h	show this help
	-i	run identifier, on output directory
	-j	number of processes of broc_draiochta that run in parallel
	-m	message/comment about submission for log-file
	-y	assume yes on questions
	-d	display information about available data files
	-s	set name
	-x	define alternative config to run.cfg
EOF
	exit;
}

$comment = $opt_m if(defined($opt_m));

$identifier = $opt_i if(defined($opt_i));
$force_ident = 0 if(!defined($force_ident));


if(defined($opt_s)){
	if(defined(@{ $sets{$opt_s} })){
		@datasets = @{ $sets{$opt_s} };
	}else{
		die "invalid parameter -s: $opt_s\n";
	}
}

@datasets = @ARGV if(@ARGV);

my $allowed_datasets = ();

$allowed_datasets{"job"} = 1;
$allowed_datasets{"TTbar"} = 1;
$allowed_datasets{"TTbarMass161"} = 1;
$allowed_datasets{"TTbarMass163"} = 1;
$allowed_datasets{"TTbarMass166"} = 1;
$allowed_datasets{"TTbarMass169"} = 1;
$allowed_datasets{"TTbarMass175"} = 1;
$allowed_datasets{"TTbarMass178"} = 1;
$allowed_datasets{"TTbarMass181"} = 1;
$allowed_datasets{"TTbarMass184"} = 1;
$allowed_datasets{"Wjets"} = 1;
$allowed_datasets{"Zjets"} = 1;
$allowed_datasets{"TsChan"} = 1;
$allowed_datasets{"TtChan"} = 1;
$allowed_datasets{"TtWChan"} = 1;
$allowed_datasets{"Mupt15"} = 1;
$allowed_datasets{"MuQCD"} = 1;
$allowed_datasets{"DYToMuMu"} = 1;
$allowed_datasets{"DYToEE"} = 1;
$allowed_datasets{"QCDem20to30"} = 1;
$allowed_datasets{"QCDem30to80"} = 1;
$allowed_datasets{"QCDem80to170"} = 1;
$allowed_datasets{"QCDbctoe20to30"} = 1;
$allowed_datasets{"QCDbctoe30to80"} = 1;
$allowed_datasets{"QCDbctoe80to170"} = 1;
$allowed_datasets{"TTbarNorm"} = 1;
$allowed_datasets{"TTbarLowISR"} = 1;
$allowed_datasets{"TTbarHighISR"} = 1;
$allowed_datasets{"TTbarMassDOWN"} = 1;
$allowed_datasets{"TTbarMassUP"} = 1;
$allowed_datasets{"TTbarScaleDOWN"} = 1;
$allowed_datasets{"TTbarScaleUP"} = 1;
$allowed_datasets{"WjetsMatchingDOWN"} = 1;
$allowed_datasets{"WjetsMatchingUP"} = 1;
$allowed_datasets{"WjetsScaleDOWN"} = 1;
$allowed_datasets{"WjetsScaleUP"} = 1;
$allowed_datasets{"ZjetsMatchingDOWN"} = 1;
$allowed_datasets{"ZjetsMatchingUP"} = 1;
$allowed_datasets{"ZjetsScaleDOWN"} = 1;
$allowed_datasets{"ZjetsScaleUP"} = 1;
$allowed_datasets{"TTbarMatchingDOWN"} = 1;
$allowed_datasets{"TTbarMatchingUP"} = 1;
$allowed_datasets{"TTbarPileUp"} = 1;
$allowed_datasets{"EQCD_EM_2030"} = 1;
$allowed_datasets{"EQCD_EM_3080"} = 1;
$allowed_datasets{"EQCD_EM_80170"} = 1;
$allowed_datasets{"EQCD_BC2E_2030"} = 1;
$allowed_datasets{"EQCD_BC2E_3080"} = 1;
$allowed_datasets{"EQCD_BC2E_80170"} = 1;
$allowed_datasets{"TTbarMCatNLO"} = 1;
$allowed_datasets{"DYJetsToLL"} = 1;
$allowed_datasets{"DYtoEE"} = 1;
$allowed_datasets{"Data"} = 1; 
$allowed_datasets{"EData"} = 1; 
$allowed_datasets{"QCDPt15"} = 1; 
$allowed_datasets{"QCDEn"} = 1; 
$allowed_datasets{"QCDPt30"} = 1; 
$allowed_datasets{"QCDInclMuPt5"} = 1; 
$allowed_datasets{"QCDMuPt20to30"} = 1;
$allowed_datasets{"QCDMuPt30to50"} = 1;
$allowed_datasets{"QCDMuPt50to80"} = 1;
$allowed_datasets{"DYmumu_pow"} = 1; 
$allowed_datasets{"DYmumu_py"} = 1; 
$allowed_datasets{"DYJetsToLL"} = 1; 
$allowed_datasets{"DY50"} = 1; 
$allowed_datasets{"MuQCD"} = 1; 
$allowed_datasets{"RelValZMM_hltpatch1"} = 1; 
$allowed_datasets{"Gjets"} = 1; 
$allowed_datasets{"RelValZMM_default"} = 1; 


my $filenames = ();
$filenames{"QCDbctoe20to30"} = "EQCD_BC2E_2030";
$filenames{"EData"} = "Data_ePD_2010BRRNov4_job";
$filenames{"QCDbctoe30to80"} = "EQCD_BC2E_3080";
$filenames{"QCDbctoe80to170"} = "EQCD_BC2E_80170";
$filenames{"QCDem20to30"} = "EQCD_EM_2030";
$filenames{"QCDem30to80"} = "EQCD_EM_3080";
$filenames{"QCDem80to170"} = "EQCD_EM_80170";
#$filenames{"Zjets"} = "DY50";
$filenames{"DY50"} = "DY50";
$filenames{"Mupt15"} = "MuQCD_pt20_mupt10";
$filenames{"TTbarNorm"} = "norm-TTbar";
$filenames{"TTbarLowISR"} = "TTbar_ISRFSR_down";
$filenames{"TTbarHighISR"} = "TTbar_ISRFSR_up";
$filenames{"TTbarMassDOWN"} = "TTbar_mass_down";
$filenames{"TTbarMassUP"} = "TTbar_mass_up";
#$filenames{"TTbarMatchingDOWN"} = "TTbar_matching_down";
#$filenames{"TTbarMatchingUP"} = "TTbar_matching_up";
$filenames{"TTbarMatchingDOWN"} = "TTbar_matchingdown";
$filenames{"TTbarMatchingUP"} = "TTbar_matchingup";
#$filenames{"TTbarScaleDOWN"} = "TTbar_scale_down";
#$filenames{"TTbarScaleUP"} = "TTbar_scale_up";
$filenames{"TTbarScaleDOWN"} = "TTbar_scaledown";
$filenames{"TTbarScaleUP"} = "TTbar_scaleup";
#$filenames{"WjetsMatchingDOWN"} = "WJ_matchingdown";
#$filenames{"WjetsMatchingUP"} = "WJ_matchingup";
#$filenames{"WjetsScaleDOWN"} = "WJ_scaledown";
#$filenames{"WjetsScaleUP"} = "WJ_scaleup";
$filenames{"WjetsMatchingDOWN"} = "Wjets_matchingdown";
$filenames{"WjetsMatchingUP"} = "Wjets_matchingup";
$filenames{"WjetsScaleDOWN"} = "Wjets_scaledown";
$filenames{"WjetsScaleUP"} = "Wjets_scaleup";
#$filenames{"ZjetsMatchingDOWN"} = "DY_matchingdown";
#$filenames{"ZjetsMatchingUP"} = "DY_matchingup";
#$filenames{"ZjetsScaleDOWN"} = "DY_scaledown";
#$filenames{"ZjetsScaleUP"} = "DY_scaleup";
$filenames{"ZjetsMatchingDOWN"} = "Zjets_matchingdown";
$filenames{"ZjetsMatchingUP"} = "Zjets_matchingup";
$filenames{"ZjetsScaleDOWN"} = "Zjets_scaledown";
$filenames{"ZjetsScaleUP"} = "Zjets_scaleup";
$filenames{"TTbarMass161"} = "TTbar_mass161";
$filenames{"TTbarMass163"} = "TTbar_mass163";
$filenames{"TTbarMass166"} = "TTbar_mass166";
$filenames{"TTbarMass169"} = "TTbar_mass169";
$filenames{"TTbarMass175"} = "TTbar_mass175";
$filenames{"TTbarMass178"} = "TTbar_mass178";
$filenames{"TTbarMass181"} = "TTbar_mass181";
$filenames{"TTbarMass184"} = "TTbar_mass184";
$filenames{"Data_new"} = "NTuple";

foreach my $dataset(@datasets){
    unless(defined($allowed_datasets{$dataset}) || $dataset =~ /Data/){ 
        print "Exiting... no valid dataset: $dataset\n \nAllowed datasets: \n";
        while(my ($allowed_dataset, $tmp) = each %allowed_datasets){
    	print "- $allowed_dataset\n";
        }
        exit;
    }
}

print_logo($logo);
print "\n\nINFO: It is recommended to define the variable \$json_file in your run.cfg, consider run.cfg.example\n\n\n" unless(defined($json_file));

confirm_datasets(\@datasets, $opt_y, $cuts_definition, $json_file);
$identifier = ask_for_ident($identifier) if(!defined($opt_i) and $force_ident and ($enable_bookkeeping or not defined($enable_bookkeeping)));
set_config($cuts_definition, $cut_gen_dir) if($set_cuts);
set_json($json_file, $json2cpp_dir) if(defined($json_file));
run_make($nthreads);
run_broc_draiochta(\@datasets, $nthreads, \@filenames, \@datadirs, \@errors, $identifier, $output_dir, $files_per_job, $force_time, $cut_gen_dir."/".$cuts_definition, $disable_svn_log_for_submission, $json2cpp_dir."/".$json_file, \%jec, $jec_directory);

push(@errors, "=====================================\n");
if(@errors > 2) { foreach(@errors) { print; } }

sub get_datafiles_and_run
{
	my ($datadirs, $filenames_ref, $dataset, $semaphore, $errors, $output_dir, $files_per_job, $jec, $jec_directory) = @_;

	$semaphore->down();

	my @filenames = @$filenames_ref;

	my $filenames = "";
	my @filenames_arr = ();
	foreach $datadir (@$datadirs){
		opendir(DIR, $datadir);
		while(defined(my $file = readdir(DIR))){
		        next unless($file =~ m/\.root/ && $file !~ m/plots/i);
			my $dataset_filename = $filenames{$dataset};
		       	next if($file !~ m/$dataset.*\.root/i && (!defined($filenames{$dataset}) ||
				(defined($filenames{$dataset}) && $file !~ m/$dataset_filename.*\.root/)));
			if($datadir =~ /^\/pnfs/){
				$filenames .= " dcache:$datadir/$file";
				push(@filenames_arr, "dcache:$datadir/$file");
			}else{
				$filenames .= " $datadir/$file";
				push(@filenames_arr, "$datadir/$file");
			}
		}
		closedir(DIR);
	}
	
	if($files_per_job){
		my $file_counter = 0;
		my $cfg_counter = 0;
		my @these_input_files = ();
		my @config_names = ();
		foreach my $filename (sort @filenames_arr){
			push(@these_input_files, $filename);
			if($file_counter >= $files_per_job){
				$cfg_counter++;

				my $postfix = "_".$cfg_counter;
				my $remote_input_files = $output_dir."/input_remote_files_$dataset$postfix.txt";
				open(FILELISTING, ">$remote_input_files") or die "can't open file $remote_input_files: $!";
				foreach my $input_file (@these_input_files){
					print FILELISTING "$input_file\n";
				}
				close(FILELISTING);

				# copy files from localgrid to /scratch
				my $input_files = $output_dir."/".$dataset."_input_files$postfix.txt";
				open(FILELISTING, ">$input_files") or die "can't open file $input_files: $!";
				foreach my $input_file (@these_input_files){
					$input_file =~ s/.*\///g;
					$input_file = "input/$input_file";
					print FILELISTING "$input_file\n";
				}
				close(FILELISTING);

				push(@config_names, create_config($dataset, $input_files, $output_dir, $postfix, $jec, $jec_directory));
				@these_input_files = ();
				$file_counter = 0;
			}

			$file_counter++;
		}
		if(@these_input_files > 0){
			$cfg_counter++;
			my $postfix = "_".$cfg_counter;

			my $remote_input_files = $output_dir."/input_remote_files_$dataset$postfix.txt";
			open(FILELISTING, ">$remote_input_files") or die "can't open file $remote_input_files: $!";
			foreach my $input_file (@these_input_files){
				print FILELISTING "$input_file\n";
			}
			close(FILELISTING);

			# copy files from localgrid to /scratch
			my $input_files = $output_dir."/".$dataset."_input_files$postfix.txt";
			open(FILELISTING, ">$input_files") or die "can't open file $input_files: $!";
			foreach my $input_file (@these_input_files){
				$input_file =~ s/.*\///g;
				$input_file = "input/$input_file";
				print FILELISTING "$input_file\n";
			}
			close(FILELISTING);

			push(@config_names, create_config($dataset, $input_files, $output_dir, $postfix, $jec, $jec_directory));
		}
		open(JOBIDS, ">$output_dir/${dataset}_job_ids.txt");
		foreach my $config_name (@config_names){
			my $submission_script = create_submission_script($output_dir, $config_name);
			my $command = "qsub -l walltime=$walltime -o $output_dir -e $output_dir -q localgrid\@cream02 $submission_script";
			print "$command\n";
			my $jobid = `$command`;
			print JOBIDS $jobid;
		}
		close(JOBIDS);
	}else{
		my $input_files = $output_dir."/".$dataset."_input_files.txt";
		open(FILELISTING, ">$input_files") or die "can't open file $input_files: $!";
		foreach my $input_file (sort @filenames_arr){
			print FILELISTING "$input_file\n";	
		}
		close(FILELISTING);

		my $postfix = "";
		my $config_name = create_config($dataset, $input_files, $output_dir, $postfix, $jec, $jec_directory);
		
		system("time ./broc_draiochta $config_name 2>&1 | tee $output_dir/$dataset.txt");
	}
	$semaphore->up();
}

sub create_submission_script
{
	my ($output_dir, $config_name) = @_;
	$config_name =~ m/.*\/(.*)\.cfg/g;
	my $ident = $1;
	my @output = `pwd`;
	my $here = pop @output;
	chomp $here;
	my $outfile = $output_dir."/submission_$ident.sh";
	open(SUB, ">$outfile") or die "cannot open $outfile: $!";
	$config_name =~ s/.*\/(\S)/\1/g;
	print SUB <<EOF;
#!/bin/bash
mkdir -p $local_node_directory/`whoami`/\$PBS_JOBID
mkdir -p $local_node_directory/`whoami`/\$PBS_JOBID/share
cd $local_node_directory/`whoami`/\$PBS_JOBID
cp $output_dir/broc_draiochta .
cp $output_dir/$config_name .
cp $output_dir/input_remote_files_$ident.txt .
cp $here/share/$parent_config share/
cp -r $here/weights .
cp -r $here/share/JEC ./share
cp -r $here/share/\*.txt ./share
cp -r $here/share/*.root ./share

while [ ! -e /nasmount/Gent/tmp/running_jobs/`whoami`.\$PBS_JOBID ]; do
        if [ `ls -l /nasmount/Gent/tmp/running_jobs | wc -l` -le $globalmaxnjobs ]; then
                echo "starting job"
                touch /nasmount/Gent/tmp/running_jobs/`whoami`.\$PBS_JOBID;
		# cp all input files to node
		mkdir input/
		hostname 1>&2
		pwd 1>&2
		df -h 1>&2
		for i in `cat input_remote_files_$ident.txt`; do
			echo "time cp \${i} input/" 1>&2
			ls -alh \${i} 1>&2
			RETVAL=1
			while [ \$RETVAL -ne 0 ]; do
				time cp \${i} input/;
				RETVAL=\$?
				if [ \$RETVAL -ne 0 ]; then
					echo "could not retrieve file \${i}, sleeping for 30 seconds..."
					sleep 30
				fi
			done
		done
        else
                sleep 15
        fi
done

rm /nasmount/Gent/tmp/running_jobs/`whoami`.\$PBS_JOBID;

echo "files in input/" 1>&2
ls input/ 1>&2
echo "files requested by job in input_remote_files.txt" 1>&2
cat input_remote_files_$ident.txt 1>&2

./broc_draiochta $config_name 2>&1 | tee ${ident}_summary.txt
cp ${ident}_summary.txt $output_dir
cp *.root $output_dir
rm -rf $local_node_directory/`whoami`/\$PBS_JOBID
# echo "rm -rf $local_node_directory/`whoami`/\$PBS_JOBID"
EOF
	close(SUB);
	`chmod 755 $outfile`;
	return $outfile;
}

sub create_config
{
	my ($dataset, $input_files, $output_dir, $postfix, $jec, $jec_directory) = @_;
	my $config_name = "$output_dir/${dataset}$postfix.cfg";
	my $default_config = "share/default_config.txt";
	$default_config = "share/$parent_config" if(defined($parent_config));	    
	open(DEFAULTCFG, "<$default_config") or die "could not find default config: $default_config";
	open(CFG, ">$config_name");
	foreach my $line (<DEFAULTCFG>){
		print CFG $line;
	}
	close(DEFAULTCFG);
	my $jec_for_dataset = "";
	$jec_for_dataset = $jec{'default'} if(defined($jec{'default'}));
	$jec_for_dataset = $jec{$dataset} if(defined($jec{$dataset}));
	$jec_for_dataset = "JEC = ".$jec_for_dataset;
	print CFG <<EOF;

[global]
output_directory = $output_dir
outfile_suffix = $postfix
input_files = $input_files
dataset = $dataset
$jec_for_dataset
EOF
	close(CFG);

	return $config_name;
}

sub set_config
{
	my ($config, $cutset_gen_dir) = @_;

	my $cutset_gen_executable = "./change_cuts.pl $config";
	
	print "setting cuts according to $config...\n";	
	die "cuts definition file $cutset_gen_dir/$config does not exist, exiting...\n" unless(-e "$cutset_gen_dir/$config"); 
	my $exitcode = system("cd $cutset_gen_dir/ && $cutset_gen_executable && cd -");
	die "ERROR: failed at setting cuts" if($exitcode >> 8);
}

sub set_json
{
	my ($json, $json2cpp_dir) = @_;

	my $json2cpp_executable = "./json2cpp.pl $json";
	
	print "setting json file to $json...\n";	
	die "cuts definition file $json2cpp_dir/$json does not exist, exiting...\n" unless(-e "$json2cpp_dir/$json"); 
	my $exitcode = system("cd $json2cpp_dir/ && $json2cpp_executable && cd -");
	die "ERROR: failed at setting json file" if($exitcode >> 8);
}

sub run_make
{
	my ($nthreads) = @_;
	my $make = "make -j $nthreads";
	print "running make...\n";
	system("$make");
	die "ERROR: failed compiling broc_draiochta" if($exitcode >> 8);
}

sub create_directory
{
	my ($identifier, $output_dir, $force_time) = @_;

	my $date = sprintf("%04d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3]);
	my $dir_name = $date."_0_$identifier";
	my $dir_to_check = "$output_dir/$dir_name";
	if(-d "$dir_to_check" or $force_time){
		$date = sprintf("%04d%02d%02d_%02d%02d%02d",((localtime)[5] +1900),((localtime)[4] +1),(localtime)[3], (localtime)[2], (localtime)[1], (localtime)[0]);
		$dir_name = $date."_$identifier";
	}

	system("mkdir -p $output_dir/$dir_name");

	return $dir_name;
}

sub ask_for_ident
{
	my ($default_ident) = @_;

	print "Please enter ident for run [$default_ident]:\n";
	my $ident = <STDIN>;

	chomp $ident;

	$ident = $default_ident if($ident !~ /[\d\w]+/);

	return $ident;
}

sub confirm_datasets
{
	my ($datasets_ref, $confirmation, $cuts_definition, $json_file) = @_;
	print "Process the following datasets?\n";
	print "Cutset: >>> $cuts_definition <<<\n";
	print "parent config: >>> $default_config <<<\n";
	print "JSON file: >>> $json_file <<<\n" if(defined($json_file) && $json_file ne "");
	foreach $dataset (@$datasets_ref){
		print " - $dataset\n";
	}

	my $confirmation = "";
	if(defined($opt_y)){
		$confirmation = "yes";
	}else{
		print "[y/N]: ";
		$confirmation = <STDIN>;
	}
	if($confirmation !~ /y(es)?/i){
		exit;	
	}
}

sub run_broc_draiochta
{
	my ($dataset_ref, $nthreads, $filenames_ref, $datadir, $errors, $identifier, $output_parent, $files_per_job, $force_time, $cuts_definition, $disable_svn_log_for_submission, $json, $jec, $jec_directory) = @_;
	my @datasets = @$dataset_ref;
	my @threads = ();
	my @commands = ();

	my $output_dir = create_directory($identifier, $output_parent, $force_time);
	$output_dir = $output_parent."/".$output_dir;

	`cp broc_draiochta $output_dir`;
	`cp $cuts_definition $output_dir`;
	`cp $json $output_dir` unless($json =~ /\/$/);
	`cp $run_cfg $output_dir/run.config`;
	my $default_config = "share/default_config.txt";
	$default_config = "share/$parent_config" if(defined($parent_config));	    
	`cp $default_config $output_dir/parent_config.config`;
	unless($disable_svn_log_for_submission){
		`svnversion > $output_dir/svnversion`;
		`svn diff src/ interface/ \`VAR=""; for i in \\\`ls -d interface/*/ src/*/\\\`; do VAR="\${VAR} \${i}"; done; echo \$VAR\` > $output_dir/svn_diff_to_HEAD.txt`;
	}
	
	my $i=0;
	my $semaphore = Thread::Semaphore->new($nthreads);
	foreach my $dataset (@datasets){
		my $thread = threads->create('get_datafiles_and_run', $datadir, $filenames_ref, $dataset, $semaphore, $errors, $output_dir, $files_per_job, $jec, $jec_directory);
		push(@threads, $thread);
	}
	
	foreach my $thread (@threads){
		$thread->join();
	}
}

sub print_logo
{
	my ($logo) = @_;
	open(LOGO, $logo) or die "can't find logo file: $logo\n";
	print while(<LOGO>);
	close(LOGO);
}
