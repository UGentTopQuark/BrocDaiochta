#!/usr/bin/perl

use Getopt::Std;
getopts("f");

die "usage: $0 <config>" unless(@ARGV > 0);

$config = $ARGV[0];
die "invalid config name: $config" unless($config =~ /\.cfg/);

$output_cfg_name = $config;
$output_cfg_name =~ s/\.cfg/_py.cfg/g;

die "file already exists: $output_cfg_name, try $0 -f <config> to force" if(-e $output_cfg_name and not $opt_f);

open(CONFIG, "<$config") or die "can't find config: $config, exiting...";
@config = <CONFIG>;
close(CONFIG);

%jec;
open(OUT, ">$output_cfg_name") or die "can't open config to write: $output_cfg_name";
print OUT "[default]\n";
foreach $line (@config){
	if($line =~ m/\$jec{\"(.*)\"}\s*=\s*\"(.*)\";/){
		$jec{$1} = $2;
		next;
	}

	$line =~ s/\"//g;
	$line =~ s/\'//g;
	$line =~ s/\$//g;
	$line =~ s/\@//g;
	$line =~ s/\(//g;
	$line =~ s/\)//g;
	$line =~ s/\;//g;
	$line =~ s/\s*\,\s*/:/g;
	$line =~ s/\#/\;/g;

	print OUT $line;
}

if(keys %jec > 0){
	print OUT "\n[jec]\n";
	while(($key, $value) = each %jec){
		print OUT "$key = $value\n";
	}
}
close(OUT);
