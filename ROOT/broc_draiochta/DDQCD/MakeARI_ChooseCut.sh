#!/bin/bash

if  [ "$1" = '' -o "$2" = '' -o "$3" = '' ]
then
        echo
        echo './MakeARI.sh <version>  <isele=0/1> <smooth=0/1>'
        echo
        exit 1
fi

vers="$1"
isele="$2"
if [ "$3" = "1" ]
then
	smooth="smooth1"
	smoo=""
else
	smooth=""
	smoo="//"
fi

dovar="1"

name="sl1$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'

name="sl2$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'

name="sl3$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/03/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'

##;name="sl4$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;name="sl5$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;name="sl6$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/06/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;name="sl7$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/07/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;name="sl8$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/08/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;name="sl9$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/09/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;name="sl10$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/10/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;name="sl11$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/11/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;
##;###########################
##;
##;name="cutset12$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/12/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset13$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/13/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset14$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/14/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset15$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/15/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset16$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/16/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset17$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/17/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset18$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/18/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset19$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/19/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset20$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/20/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset21$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/21/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
##;
##;name="cutset22$smooth"
##;fact="-1"
##;cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/22/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
##;root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
##;
