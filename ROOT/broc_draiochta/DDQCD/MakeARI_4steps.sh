#!/bin/bash

if  [ "$1" = '' -o "$2" = '' -o "$3" = '' ]
then
        echo
        echo './MakeARI.sh <version>  <isele=0/1> <smooth=0/1>'
        echo
        exit 1
fi

vers="$1"
isele="$2"
if [ "$3" = "1" ]
then
	smooth="smooth1"
	smoo=""
else
	smooth=""
	smoo="//"
fi

dovar="1"

# name="plustwo_4j$smooth"
# fact="1"
# cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
# root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
# 
# name="plusone_4j$smooth"
# fact="0"
# cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
# root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
# 

FILENAMES=""
OUTPATH="../output/$vers/"

for i in `seq 1 4`; do
	name="0${i}_cutset"
	fact="1.0"
	cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/0${i}/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
	root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

	FILENAMES+=" ${OUTPATH}QCD_${name}_plots.root"
done

hadd -f ${OUTPATH}QCD_plots.root ${FILENAMES}

FILENAMES=""

for i in `seq 1 4`; do
	name="0${i}_cutset_plushalf"
	fact="-1.0"
	cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/0${i}/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
	root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

	FILENAMES+=" ${OUTPATH}QCD_${name}_plots.root"
done

hadd -f ${OUTPATH}QCD_minushalf_plots.root ${FILENAMES}

FILENAMES=""

for i in `seq 1 4`; do
	name="0${i}_cutset_minushalf"
	fact="2.0"
	cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/0${i}/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
	root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

	FILENAMES+=" ${OUTPATH}QCD_${name}_plots.root"
done

hadd -f ${OUTPATH}QCD_plushalf_plots.root ${FILENAMES}
