#!/bin/bash

if  [ "$1" = '' -o "$2" = '' -o "$3" = '' ]
then
        echo
        echo './MakeARI.sh <version>  <isele=0/1> <smooth=0/1>'
        echo
        exit 1
fi

vers="$1"
isele="$2"
if [ "$3" = "1" ]
then
	smooth="smooth1"
	smoo=""
else
	smooth=""
	smoo="//"
fi

dovar="1"

# name="plustwo_4j$smooth"
# fact="1"
# cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
# root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
# 
# name="plusone_4j$smooth"
# fact="0"
# cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
# root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
# 
name="no_cont_rem_4j$smooth"
fact="0.0"
cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

name="plushalf_4j$smooth"
fact="-0.5"
cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

name="central_4j$smooth"
fact="-1"
cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

 name="minushalf_4j$smooth"
 fact="-1.5"
 cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
 root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

# name="central_4j_01$smooth"
# fact="-1"
# cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
# root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

# name="central_4j_02$smooth"
# fact="-1"
# cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
# root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
# 
# name="central_4j_03$smooth"
# fact="-1"
# cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/03/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
# root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
# 
# name="central_4j_04$smooth"
# fact="-1"
# cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
# root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

####	name="central_4j_05$smooth"
####	fact="-1"
####	cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
####	root -b -l -q ProduceQCDNoIso.C'("'$isele'")'

# name="minushalf_4j$smooth"
# fact="-1.5"
# cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
# root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
# 
#  name="minusone_4j$smooth"
#  fact="-2"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="minustwo_4j$smooth"
#  fact="-3"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  
#  
#  name="plustwo_3j$smooth"
#  fact="1" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#   
#  name="plusone_3j$smooth"
#  fact="0" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#  
#  name="plushalf_3j$smooth"
#  fact="-0.5"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="central_3j$smooth"
#  fact="-1" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="minushalf_3j$smooth"
#  fact="-1.5"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="minusone_3j$smooth"
#  fact="-2" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#   
#  name="minustwo_3j$smooth"
#  fact="-3" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  
#  #########################################################################
#  
#  if [ $dovar = '1' ] 
#  then
#  
#  
#  ####JES +
#  name="plustwo_04_4j$smooth"
#  fact="1"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="plusone_04_4j$smooth"
#  fact="0"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="plushalf_04_4j$smooth"
#  fact="-0.5"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="central_04_4j$smooth"
#  fact="-1"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="minushalf_04_4j$smooth"
#  fact="-1.5"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="minusone_04_4j$smooth"
#  fact="-2"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="minustwo_04_4j$smooth"
#  fact="-3"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="plustwo_06_3j$smooth"
#  fact="1"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/06/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="plusone_06_3j$smooth"
#  fact="0"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/06/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="plushalf_06_3j$smooth"
#  fact="-0.5"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/06/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="central_06_3j$smooth"
#  fact="-1"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/06/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="minushalf_06_3j$smooth"
#  fact="-1.5" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/06/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="minusone_06_3j$smooth"
#  fact="-2"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/06/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="minustwo_06_3j$smooth"
#  fact="-3"
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/06/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  ####JES -
#  name="plustwo_05_4j$smooth" 
#  fact="1" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#    
#  name="plusone_05_4j$smooth" 
#  fact="0" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#  
#  name="plushalf_05_4j$smooth"
#  fact="-0.5"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#    
#  name="central_05_4j$smooth" 
#  fact="-1" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#  
#  name="minushalf_05_4j$smooth" 
#  fact="-1.5"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#   
#  name="minusone_05_4j$smooth" 
#  fact="-2" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#    
#  name="minustwo_05_4j$smooth" 
#  fact="-3" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#   
#  name="plustwo_07_3j$smooth" 
#  fact="1" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/07/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#    
#  name="plusone_07_3j$smooth" 
#  fact="0" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/07/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#  
#  name="plushalf_07_3j$smooth" 
#  fact="-0.5"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/07/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#    
#  name="central_07_3j$smooth" 
#  fact="-1" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/07/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#  
#  name="minushalf_07_3j$smooth"
#  fact="-1.5"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/07/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#    
#  name="minusone_07_3j$smooth"
#  fact="-2" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/07/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#  
#  name="minustwo_07_3j$smooth"
#  fact="-3" 
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/07/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#  
#  ####JER +
#  name="plustwo_08_4j$smooth"
#  fact="1"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/08/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'  
#     
#  name="plusone_08_4j$smooth"  
#  fact="0"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/08/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'  
#  
#  name="plushalf_08_4j$smooth"
#  fact="-0.5"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/08/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#     
#  name="central_08_4j$smooth"  
#  fact="-1"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/08/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'  
#  
#  name="minushalf_08_4j$smooth"  
#  fact="-1.5"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/08/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#    
#  name="minusone_08_4j$smooth"  
#  fact="-2"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/08/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'  
#     
#  name="minustwo_08_4j$smooth"  
#  fact="-3"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/08/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#    
#  name="plustwo_10_3j$smooth"  
#  fact="1"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/10/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'  
#     
#  name="plusone_10_3j$smooth"  
#  fact="0"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/10/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'  
#   
#  name="plushalf_10_3j$smooth"  
#  fact="-0.5"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/10/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="central_10_3j$smooth"  
#  fact="-1"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/10/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="minushalf_10_3j$smooth" 
#  fact="-1.5"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/10/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#     
#  name="minusone_10_3j$smooth" 
#  fact="-2"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/10/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'  
#   
#  name="minustwo_10_3j$smooth" 
#  fact="-3"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/10/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  ####JER -
#  name="plustwo_09_4j$smooth" 
#  fact="1"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/09/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#      
#  name="plusone_09_4j$smooth"   
#  fact="0"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/09/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'   
#  
#  name="plushalf_09_4j$smooth"
#  fact="-0.5"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/09/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#      
#  name="central_09_4j$smooth"   
#  fact="-1"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/09/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'   
#  
#  name="minushalf_09_4j$smooth"   
#  fact="-1.5"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/09/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#     
#  name="minusone_09_4j$smooth"   
#  fact="-2"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/09/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'   
#      
#  name="minustwo_09_4j$smooth"   
#  fact="-3"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/09/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#     
#  name="plustwo_11_3j$smooth"   
#  fact="1"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/11/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'   
#      
#  name="plusone_11_3j$smooth"   
#  fact="0"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/11/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'   
#  
#  name="plushalf_11_3j$smooth"
#  fact="-0.5"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/11/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#    
#  name="central_11_3j$smooth"   
#  fact="-1"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/11/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")' 
#      
#  name="minushalf_11_3j$smooth"
#  fact="-1.5"  
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/11/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  name="minusone_11_3j$smooth"  
#  fact="-2"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/11/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'   
#    
#  name="minustwo_11_3j$smooth"  
#  fact="-3"   
#  cat ProduceQCDNoIso.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/11/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso.C
#  root -b -l -q ProduceQCDNoIso.C'("'$isele'")'
#  
#  ########SEDREPLACEME
#  
#  fi
