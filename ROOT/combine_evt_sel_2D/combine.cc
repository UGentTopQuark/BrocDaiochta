#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include "Canvas_Holder.h"
#include <sstream>
#include "HistoCombiner.h"

int main(int argc, char **argv)
{
	std::string e_mu = argv[1];
	std::string c_type = argv[2];
	std::string mass = argv[3];
	std::string cutset = argv[4];
	std::string version = "";

	bool electron = true;
	if(e_mu == "e"){
		std::cout << "plotting for electrons..." << std::endl;
		electron = true;
	}
	else{
		std::cout << "plotting for muons..." << std::endl;
		electron = false;
	}

	std::vector<std::string> *sequence_to_plot = new std::vector<std::string>();

	std::map<std::string,std::vector<std::string> > files_with_content;
	if(electron){
		if(c_type == "c" || c_type == "s"){
			std::cout << "plotting signal..." << std::endl;
			files_with_content["tprime400_"+version+".root"].push_back("Tprime400|electron|"+cutset);
 			sequence_to_plot->push_back("Tprime400|electron|"+cutset);
 			files_with_content["tprime400_"+version+".root"].push_back("Tprime400|e_background|"+cutset);
 			sequence_to_plot->push_back("Tprime400|e_background|"+cutset);
		}
		if(c_type == "c" || c_type == "b"){
			std::cout << "plotting background..." << std::endl;
			files_with_content["mupt15_"+version+".root"].push_back("QCDherwigpt15|e_background|"+cutset);
			sequence_to_plot->push_back("Mupt15|e_background|"+cutset);
			files_with_content["wjets_"+version+".root"].push_back("Wjets|e_background|"+cutset);
			sequence_to_plot->push_back("Wjets|e_background|"+cutset);
			files_with_content["zjets_"+version+".root"].push_back("Zjets|e_background|"+cutset);
			sequence_to_plot->push_back("Zjets|e_background|"+cutset);

			//files_with_content["qcdpt15_"+version+".root"].push_back("QCDpt15|e_background|"+cutset);
			//sequence_to_plot->push_back("QCDpt15|e_background|"+cutset);

			files_with_content["ttbar_"+version+".root"].push_back("TTbar|e_background|"+cutset);
			sequence_to_plot->push_back("TTbar|e_background|"+cutset);

			files_with_content["t_s_chan"+version+".root"].push_back("TsChan|e_background|"+cutset);
			sequence_to_plot->push_back("TsChan|e_background|"+cutset);
			files_with_content["t_t_chan"+version+".root"].push_back("TtChan|e_background|"+cutset);
			sequence_to_plot->push_back("TtChan|e_background|"+cutset);
			files_with_content["t_tW_chan"+version+".root"].push_back("TtWChan|e_background|"+cutset);
			sequence_to_plot->push_back("TtWChan|e_background|"+cutset);
			files_with_content["t_s_chan"+version+".root"].push_back("TsChan|mu_background|"+cutset);
			sequence_to_plot->push_back("TsChan|mu_background|"+cutset);
			files_with_content["t_t_chan"+version+".root"].push_back("TtChan|mu_background|"+cutset);
			sequence_to_plot->push_back("TtChan|mu_background|"+cutset);
			files_with_content["t_tW_chan"+version+".root"].push_back("TtWChan|mu_background|"+cutset);
			sequence_to_plot->push_back("TtWChan|mu_background|"+cutset);
		}
	}else{
		if(c_type == "c" || c_type == "s"){
			std::cout << "plotting signal..." << std::endl;
			if(mass == "175"){
				files_with_content["ttbar.root"].push_back("TTbar|muon|"+cutset);
				sequence_to_plot->push_back("TTbar|muon|"+cutset);
				files_with_content["ttbar.root"].push_back("TTbar|mu_background|"+cutset);
				sequence_to_plot->push_back("TTbar|mu_background|"+cutset);
			}
			else{
				files_with_content["tprime_"+mass+version+".root"].push_back("Tprime"+mass+"|muon|"+cutset);
				sequence_to_plot->push_back("Tprime"+mass+"|muon|"+cutset);
				files_with_content["tprime_"+mass+version+".root"].push_back("Tprime"+mass+"|mu_background|"+cutset);
				sequence_to_plot->push_back("Tprime"+mass+"|mu_background|"+cutset);
			}

		}
		if(c_type == "c" || c_type == "b"){
			std::cout << "plotting background..." << std::endl;
			files_with_content["qcdmupt15"+version+".root"].push_back("Mupt15|mu_background|"+cutset);
			sequence_to_plot->push_back("Mupt15|mu_background|"+cutset);
			files_with_content["wjets"+version+".root"].push_back("Wjets|mu_background|"+cutset);
			sequence_to_plot->push_back("Wjets|mu_background|"+cutset);
			files_with_content["zjets"+version+".root"].push_back("Zjets|mu_background|"+cutset);
			sequence_to_plot->push_back("Zjets|mu_background|"+cutset);
			if(mass != "175"){
				files_with_content["ttbar"+version+".root"].push_back("TTbar|mu_background|"+cutset);
				sequence_to_plot->push_back("TTbar|mu_background|"+cutset);}
			//files_with_content["t_s_chan"+version+".root"].push_back("TsChan|mu_background|"+cutset);
			//sequence_to_plot->push_back("TsChan|mu_background|"+cutset);
			files_with_content["t_t_chan"+version+".root"].push_back("TtChan|mu_background|"+cutset);
			sequence_to_plot->push_back("TtChan|mu_background|"+cutset);
			files_with_content["t_tW_chan"+version+".root"].push_back("TtWChan|mu_background|"+cutset);
			sequence_to_plot->push_back("TtWChan|mu_background|"+cutset);
		}
	}

	std::vector<std::string> histo_names;
	//	histo_names.push_back("isolation_vs_met_muon_");
	//	histo_names.push_back("isolation_vs_met_electron_");
 	//histo_names.push_back("top_mass_vs_ht_");
	//histo_names.push_back("top_kinmass_vs_ht_");
	//histo_names.push_back("top_Had_vs_Lep_chimass_");

	//histo_names.push_back("1st_b_pt_vs_5th_jet_pt_HighEff_");
	//	histo_names.push_back("2nd_b_pt_vs_5th_jet_pt_HighEff_");

	histo_names.push_back("jet1_pt_vs_jet2_pt_");
 	//histo_names.push_back("pt_1st_jet_vs_Had_chimass_");
 	//histo_names.push_back("pt_1st_jet_vs_Lep_chimass_");
 	//histo_names.push_back("pt_2nd_jet_vs_Lep_chimass_");
 	//histo_names.push_back("pt_2nd_jet_vs_Had_chimass_");
 	//histo_names.push_back("pt_Had_vs_pt_Lep_");
	//histo_names.push_back("pt_neutrino_vs_MET_");
 	//histo_names.push_back("pz_neutrino_vs_MET_");


	std::vector<int> *colours = new std::vector<int>();
	if(c_type == "s" || c_type == "c"){
        	colours->push_back(kRed-3);
        	colours->push_back(kYellow-7);
	}
	if(c_type == "b" || c_type == "c"){
        	colours->push_back(kWhite-1);
        	colours->push_back(kGreen-3);
        	colours->push_back(kCyan-6);
        	colours->push_back(kBlue+1);
        	colours->push_back(kBlue-1);
        	colours->push_back(kBlue-2);
        	colours->push_back(kBlue-3);
	}

	std::cout << "booking HistoCombiner()" << std::endl;
	HistoCombiner *hc = new HistoCombiner();
	hc->set_cutset(cutset);
	hc->set_sequence_to_plot(sequence_to_plot);
	hc->set_colours(colours);
	hc->add_histos(files_with_content,histo_names);
	if(electron){
		hc->set_cross_section("Tprime250|electron|"+cutset,35.6*0.15*0.69496503);
		hc->set_cross_section("Tprime250|e_background|"+cutset,35.6*0.85*0.50329298);
		hc->set_cross_section("Tprime275|electron|"+cutset,21.67*0.15*0.70491567);
		hc->set_cross_section("Tprime275|e_background|"+cutset,21.67*0.85*0.5235718);
		hc->set_cross_section("Tprime300|electron|"+cutset,13.65*0.15*0.72061495);
		hc->set_cross_section("Tprime300|e_background|"+cutset,13.65*0.85*0.53974814);
		hc->set_cross_section("Tprime325|electron|"+cutset,8.86*0.15*0.71981808);
		hc->set_cross_section("Tprime325|e_background|"+cutset,8.86*0.85*0.54913073);
		hc->set_cross_section("Tprime350|electron|"+cutset,5.88*0.15*0.73574409);
		hc->set_cross_section("Tprime350|e_background|"+cutset,5.88*0.85*0.57038386);
		hc->set_cross_section("Tprime375|electron|"+cutset,4.03*0.15*0.73002868);
		hc->set_cross_section("Tprime375|e_background|"+cutset,4.03*0.85*0.57165646);
		hc->set_cross_section("Tprime400|electron|"+cutset,2.80*0.15*0.73574409);
		hc->set_cross_section("Tprime400|e_background|"+cutset,2.80*0.85*0.57920676);
//		hc->set_cross_section("TTbar|electron|"+cutset,375*0.15*0.41901874);
		hc->set_cross_section("TTbar|e_background|"+cutset,375*0.41901874);
		hc->set_cross_section("Wjets|e_background|"+cutset,35550*0.0034905503);
		hc->set_cross_section("Zjets|e_background|"+cutset,3540*0.0076099354);
		// FIXME change e- qcd background cross section
		hc->set_cross_section("QCDpt15|e_background|"+cutset,1457159248*0.0002);
		hc->set_cross_section("TsChan|e_background|"+cutset,1.66*0.11759313);
		hc->set_cross_section("TtChan|e_background|"+cutset,63.6*0.088775394);
		hc->set_cross_section("TtWChan|e_background|"+cutset,27.3*0.28042331);
	}else{
		hc->set_cross_section("Tprime250|muon|"+cutset,35.6*0.15*0.70308358);
		hc->set_cross_section("Tprime250|mu_background|"+cutset,35.6*0.85*0.50180462);
		hc->set_cross_section("Tprime275|muon|"+cutset,21.67*0.15*0.7275882);
		hc->set_cross_section("Tprime275|mu_background|"+cutset,21.67*0.85*0.51981123);
		hc->set_cross_section("Tprime300|muon|"+cutset,13.65*0.15*0.72491179);
		hc->set_cross_section("Tprime300|mu_background|"+cutset,13.65*0.85*0.53931405);
		hc->set_cross_section("Tprime325|muon|"+cutset,8.86*0.15*0.72887521);
		hc->set_cross_section("Tprime325|mu_background|"+cutset,8.86*0.85*0.54802165);
		hc->set_cross_section("Tprime350|muon|"+cutset,5.88*0.15*0.74743326);
		hc->set_cross_section("Tprime350|mu_background|"+cutset,5.88*0.85*0.569925);
		hc->set_cross_section("Tprime375|muon|"+cutset,4.03*0.15*0.73504274);
		hc->set_cross_section("Tprime375|mu_background|"+cutset,4.03*0.85*0.57060993);
		hc->set_cross_section("Tprime400|muon|"+cutset,2.80*0.15*0.74743326);
		hc->set_cross_section("Tprime400|mu_background|"+cutset,2.80*0.85*0.57677374);
		hc->set_cross_section("TTbar|muon|"+cutset,375*0.15*0.41901874);
		if(mass == "175")
		hc->set_cross_section("TTbar|mu_background|"+cutset,375*0.85*0.41901874);
		if(mass != "175")
		hc->set_cross_section("TTbar|mu_background|"+cutset,375*0.41901874);
		//		hc->set_cross_section("TTbar|muon|"+cutset,375*0.15*0.41901874);
		hc->set_cross_section("Wjets|mu_background|"+cutset,35550*0.0034905503);
		hc->set_cross_section("Zjets|mu_background|"+cutset,3540*0.0076099354);
		hc->set_cross_section("Mupt15|mu_background|"+cutset,509100000*0.000239*0.10212446);
		hc->set_cross_section("TsChan|mu_background|"+cutset,1.66*0.11759313);
		hc->set_cross_section("TtChan|mu_background|"+cutset,63.6*0.088775394);
		hc->set_cross_section("TtWChan|mu_background|"+cutset,27.3*0.28042331);
	}
	hc->scale_to_cross_section(200);
	hc->combine_histos();
	hc->write_histos();

	delete sequence_to_plot;
	sequence_to_plot=NULL;
	delete colours;
	colours=NULL;

	return 0;
}
