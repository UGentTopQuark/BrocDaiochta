#ifndef EFFICIENCYPURITY_DIFF_H
#define EFFICIENCYPURITY_DIFF_H
#include "TH1F.h"
#include "TFile.h"
#include "math.h"
#include <iostream>
#include "Canvas_Holder.h"

class EfficiencyPurity_diff{
	public:
		EfficiencyPurity_diff(TFile *outfile);
		~EfficiencyPurity_diff();
		void set_background_histo(TH1F *background);
		void set_symmetric_cut(bool symmetric);
		void set_histo_rebin(double rebin);
		void set_signal_histo(TH1F *signal);
		void calculate();
	private:
		TH1F *background_histo;
		TH1F *signal_histo;
		TFile *outfile;
		bool symmetric_cut;
		double rebin_histo;
};

#endif
