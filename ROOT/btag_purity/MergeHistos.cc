#include "MergeHistos.h"

MergeHistos::MergeHistos(TFile *outfile)
{
	this->outfile = outfile;
}

void MergeHistos::set_file_names(std::map<std::string,std::vector<std::string> > &file_names)
{
	this->file_names = file_names;
}

void MergeHistos::set_cutset(std::string cutset)
{
	this->cutset = cutset;
}

TH1F* MergeHistos::combine_histo(std::string histo_name)
{
	std::string directory_name="eventselection";
	double integrated_luminosity = 200.0;

	std::map<std::string,double> cross_section;
	cross_section[histo_name+"Tprime250|muon|"+cutset]=35.6*0.15;
	cross_section[histo_name+"Tprime250|mu_background|"+cutset]=35.6*0.85;
	cross_section[histo_name+"Tprime275|muon|"+cutset]=21.67*0.15;
	cross_section[histo_name+"Tprime275|mu_background|"+cutset]=21.67*0.85;
	cross_section[histo_name+"Tprime300|muon|"+cutset]=13.65*0.15;
	cross_section[histo_name+"Tprime300|mu_background|"+cutset]=13.65*0.85;
	cross_section[histo_name+"Tprime325|muon|"+cutset]=8.86*0.15;
	cross_section[histo_name+"Tprime325|mu_background|"+cutset]=8.86*0.85;
	cross_section[histo_name+"Tprime350|muon|"+cutset]=5.88*0.15;
	cross_section[histo_name+"Tprime350|mu_background|"+cutset]=5.88*0.85;
	cross_section[histo_name+"Tprime375|muon|"+cutset]=4.03*0.15;
	cross_section[histo_name+"Tprime375|mu_background|"+cutset]=4.03*0.85;
	cross_section[histo_name+"Tprime400|muon|"+cutset]=2.80*0.15;
	cross_section[histo_name+"Tprime400|mu_background|"+cutset]=2.80*0.85;
	cross_section[histo_name+"TTbar|muon|"+cutset]=375*0.15;
	cross_section[histo_name+"TTbar|mu_background|"+cutset]=375*0.85;
	cross_section[histo_name+"Zjets|mu_background|"+cutset]=3540;
	cross_section[histo_name+"Wjets|mu_background|"+cutset]=35550;
	cross_section[histo_name+"Mupt15|mu_background|"+cutset]=509100000*0.000239;
	cross_section[histo_name+"TtChan|mu_background|"+cutset]=27.3;
	cross_section[histo_name+"TtWChan|mu_background|"+cutset]=1.66;
	cross_section[histo_name+"TsChan|mu_background|"+cutset]=63.6;

	std::map<std::string,double> filter_efficiencies;
	filter_efficiencies[histo_name+"TTbar|muon|"+cutset]=0.41901874;
	filter_efficiencies[histo_name+"TTbar|mu_background|"+cutset]=0.41901874;
	filter_efficiencies[histo_name+"Zjets|mu_background|"+cutset]=0.0076099354;
	filter_efficiencies[histo_name+"Wjets|mu_background|"+cutset]=0.0034905503;
	filter_efficiencies[histo_name+"Mupt15|mu_background|"+cutset]=0.10212446;
	filter_efficiencies[histo_name+"TtChan|mu_background|"+cutset]=0.088775394;
	filter_efficiencies[histo_name+"TsChan|mu_background|"+cutset]=0.11759313;
	filter_efficiencies[histo_name+"TtWChan|mu_background|"+cutset]=0.28042331;

	std::map<std::string,std::string> event_counters;
	event_counters[histo_name+"Tprime250|muon|"+cutset]="event_counter_Tprime250|muon|"+cutset;
	event_counters[histo_name+"Tprime250|mu_background|"+cutset]="event_counter_Tprime250|mu_background|"+cutset;
	event_counters[histo_name+"Tprime300|muon|"+cutset]="event_counter_Tprime300|muon|"+cutset;
	event_counters[histo_name+"Tprime300|mu_background|"+cutset]="event_counter_Tprime300|mu_background|"+cutset;
	event_counters[histo_name+"TTbar|muon|"+cutset]="event_counter_TTbar|muon|"+cutset;
	event_counters[histo_name+"TTbar|mu_background|"+cutset]="event_counter_TTbar|mu_background|"+cutset;
	event_counters[histo_name+"Zjets|mu_background|"+cutset]="event_counter_Zjets|mu_background|"+cutset;
	event_counters[histo_name+"Mupt15|mu_background|"+cutset]="event_counter_Mupt15|mu_background|"+cutset;
	event_counters[histo_name+"Wjets|mu_background|"+cutset]="event_counter_Wjets|mu_background|"+cutset;
	event_counters[histo_name+"t_t_chan|mu_background|"+cutset]="event_counter_TtChan|mu_background|"+cutset;
	event_counters[histo_name+"t_tW_chan|mu_background|"+cutset]="event_counter_TtWChan|mu_background|"+cutset;
	event_counters[histo_name+"t_s_chan|mu_background|"+cutset]="event_counter_TsChan|mu_background|"+cutset;

	double event_counter=0;
	
	outfile->cd();

	TH1F *combined_histo=NULL;
	/*
	 * combine the backgrounds
	 */
	bool first = true;
	for(std::map<std::string,std::vector<std::string> >::iterator file_name = file_names.begin();
		file_name != file_names.end();
		++file_name){
		TFile *file = new TFile(file_name->first.c_str(), "open");
		for(unsigned int file_nr = 0; file_nr < file_name->second.size();
			++file_nr){
			
			// create a copy of the first histo.
			if(first){
				outfile->cd();
				combined_histo = new TH1F(*((TH1F *) file->GetDirectory(directory_name.c_str())->Get(file_name->second[file_nr].c_str())));

				if(combined_histo->GetEntries() != 0)
					first = false;
				else
					continue;

				if(cross_section.find(file_name->second[file_nr]) != cross_section.end()){
					TH1F *eff_histo = (TH1F *) file->GetDirectory(directory_name.c_str())->Get(event_counters[file_name->second[file_nr].c_str()].c_str());
					double efficiency = 1 / eff_histo->GetBinContent(1);

					double scaling_factor = cross_section[file_name->second[file_nr]]*integrated_luminosity*efficiency;


					if(filter_efficiencies.find(file_name->second[file_nr]) != filter_efficiencies.end()){
						scaling_factor *= filter_efficiencies[file_name->second[file_nr]];
					}

					event_counter = cross_section[file_name->second[file_nr]]*integrated_luminosity;

					double histo_entries = combined_histo->GetEntries();
					combined_histo->Scale(1.0/combined_histo->Integral());
					combined_histo->Scale(scaling_factor*histo_entries);
				}
			}
			else{ 
			
				TH1F *histo_to_add = (TH1F *) file->GetDirectory(directory_name.c_str())->Get(file_name->second[file_nr].c_str()); 

				TH1F *eff_histo = (TH1F *) file->GetDirectory(directory_name.c_str())->Get(event_counters[file_name->second[file_nr].c_str()].c_str());
				double efficiency = 1 / eff_histo->GetBinContent(1);

				double scaling_factor = cross_section[file_name->second[file_nr]]*integrated_luminosity*efficiency;

				if(filter_efficiencies.find(file_name->second[file_nr]) != filter_efficiencies.end()){
					scaling_factor *= filter_efficiencies[file_name->second[file_nr]];
				}

				event_counter += cross_section[file_name->second[file_nr]]*integrated_luminosity;

				double histo_entries = histo_to_add->GetEntries();
				histo_to_add->Scale(1.0/histo_to_add->Integral());
				histo_to_add->Scale(scaling_factor*histo_entries);

				combined_histo->Add(histo_to_add);
			}
		}

		delete file;
		file = NULL;
	}

	return combined_histo;
}
