// ---------------------------------------------------------------
// -----------------------      ExtHisto functions ---------------
// ---------------------------------------------------------------

#include "ExtHisto.h"
#include <vector>
#include <string>

#include <TH1.h>
#include <iostream>
#include <iomanip>

using namespace std;



void ExtHisto::initHistVal(){
  theClones=0;
  theLineColor=theHisto->GetLineColor();
  theLineStyle=theHisto->GetLineStyle();
}

ExtHisto::ExtHisto():theHisto(0), fill_bins(false){
  
}

ExtHisto::ExtHisto(TH1* inHisto):theHisto(inHisto) {
  initHistVal();
  theLegTitle=theHisto->GetTitle();
}

ExtHisto::ExtHisto(TH1* inHisto, const std::string &inLegTitle, std::string inDrawOpt) :
  theHisto(inHisto),theLegTitle(inLegTitle),theDrawOpt(inDrawOpt) {
  initHistVal();
}


void ExtHisto::formatHisto() {
  theHisto->SetLineColor(theLineColor);
  theHisto->SetLineStyle(theLineStyle);
	if(fill_bins){
//		theHisto->SetFillColor(theLineColor);
  		theHisto->SetLineColor(theLineColor);
  		theHisto->SetLineStyle(1);
	}
}

void ExtHisto::setFill() {
	fill_bins=true;
}

void ExtHisto::cloneHisto(){
  if (theClones) cout<<"[CLONE HISTO] histo already cloned in this instance! --> "<<theClones<<endl;
  theHisto=dynamic_cast<TH1*>(theHisto->Clone());
}
