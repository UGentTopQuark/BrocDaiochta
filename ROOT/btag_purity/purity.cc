#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "MergeHistos.h"
#include "EfficiencyPurity_diff.h"
#include "tdrstyle.h"

int main(int argc, char **argv)
{
	TFile *outfile = new TFile("outfile.root", "RECREATE");
	MergeHistos *sig_mhistos = new MergeHistos(outfile);
	MergeHistos *bg_mhistos = new MergeHistos(outfile);
	EfficiencyPurity_diff *eff_pur = new EfficiencyPurity_diff(outfile);

	std::string version="";
	std::string cutset="05_cutset";

	std::vector<std::string> *histo_names = new std::vector<std::string>();
// 	histo_names->push_back("bjet_bDiscrim_");
//  	histo_names->push_back("bjet_highest_bDiscrim_");
//  	histo_names->push_back("bjet_second_highest_bDiscrim_");
// 	histo_names->push_back("bjet_highest_pt_");
// 	histo_names->push_back("bjet_second_highest_pt_");
//	histo_names->push_back("bjet_bDiscrim_HighEff_");
	histo_names->push_back("bjet_highest_bDiscrim_HighEff_");
// 	histo_names->push_back("bjet_second_highest_bDiscrim_HighEff_");
// 	histo_names->push_back("bjet_highest_pt_HighEff_");
// 	histo_names->push_back("bjet_second_highest_pt_HighEff_");
// 	histo_names->push_back("bjet_highest_bDiscrim_bid_");
// 	histo_names->push_back("bjet_second_highest_bDiscrim_bid_");
// 	histo_names->push_back("bjet_highest_pt_bid_");
// 	histo_names->push_back("bjet_second_highest_pt_bid_");
// 	histo_names->push_back("bjet_highest_bDiscrim_bid_HighEff_");
// 	histo_names->push_back("bjet_second_highest_bDiscrim_bid_HighEff_");
// 	histo_names->push_back("bjet_highest_pt_bid_HighEff_");
// 	histo_names->push_back("bjet_second_highest_pt_bid_HighEff_");

	eff_pur->set_symmetric_cut(false);
	//set to -1 if do not want histogram rebinned
	eff_pur->set_histo_rebin(1);

	for(std::vector<std::string>::iterator histo_name = histo_names->begin();histo_name !=histo_names->end();++histo_name){ 
	std::cout << "preparing signal: " << *histo_name << std::endl;
	std::map<std::string,std::vector<std::string> > sig_file_names;
        sig_file_names["ttbar"+version+".root"].push_back(*histo_name+"TTbar|muon|"+cutset);
	//sig_file_names["tprime_250"+version+".root"].push_back(*histo_name+"Tprime250|muon|"+cutset);
	//	sig_file_names["tprime_300"+version+".root"].push_back(*histo_name+"Tprime300|muon|"+cutset);
	//sig_file_names["tprime_350"+version+".root"].push_back(*histo_name+"Tprime350|muon|"+cutset);
	//sig_file_names["tprime_400"+version+".root"].push_back(*histo_name+"Tprime400|muon|"+cutset);

        sig_mhistos->set_file_names(sig_file_names);
	sig_mhistos->set_cutset(cutset);
	sig_file_names.clear();

	std::cout << "preparing background: " << *histo_name << std::endl;
	std::map<std::string,std::vector<std::string> > bg_file_names;
	bg_file_names["ttbar"+version+".root"].push_back("non"+*histo_name+"TTbar|muon|"+cutset);
	//bg_file_names["ttbar"+version+".root"].push_back("non"+*histo_name+"TTbar|mu_background|"+cutset);
// 	bg_file_names["qcdmupt15"+version+".root"].push_back("non"+*histo_name+"Mupt15|mu_background|"+cutset);
// 	bg_file_names["wjets"+version+".root"].push_back("non"+*histo_name+"Wjets|mu_background|"+cutset);
// 	bg_file_names["zjets"+version+".root"].push_back("non"+*histo_name+"Zjets|mu_background|"+cutset);
// 	bg_file_names["t_t_chan"+version+".root"].push_back("non"+*histo_name+"TtChan|mu_background|"+cutset);
// 	bg_file_names["t_s_chan"+version+".root"].push_back("non"+*histo_name+"TsChan|mu_background|"+cutset);
// 	bg_file_names["t_tW_chan"+version+".root"].push_back("non"+*histo_name+"TtWChan|mu_background|"+cutset);
// 	bg_file_names["tprime_250"+version+".root"].push_back("non"+*histo_name+"Tprime250|muon|"+cutset);
// 	bg_file_names["tprime_250"+version+".root"].push_back("non"+*histo_name+"Tprime250|mu_background|"+cutset);
// 	bg_file_names["tprime_300"+version+".root"].push_back("non"+*histo_name+"Tprime300|muon|"+cutset);
// 	bg_file_names["tprime_300"+version+".root"].push_back("non"+*histo_name+"Tprime300|mu_background|"+cutset);
	bg_mhistos->set_file_names(bg_file_names);
	bg_mhistos->set_cutset(cutset);
	bg_file_names.clear();

	setTDRStyle();
	std::cout << "adding signal histo" << std::endl;
	eff_pur->set_signal_histo(sig_mhistos->combine_histo(*histo_name));
	std::cout << "adding background histo" << std::endl;
	eff_pur->set_background_histo(bg_mhistos->combine_histo("non"+*histo_name));
	std::cout << "calculating purity" << std::endl;
	eff_pur->calculate();
	setTDRStyle();

	}


	delete outfile;
	outfile = NULL;	
	delete histo_names;
	histo_names = NULL;
}
