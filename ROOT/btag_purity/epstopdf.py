#! /usr/bin/python

import os, sys, subprocess

folder = str("./")

for dirpath,dirnames,filenms in os.walk(folder):
    for script in filenms:
        if script.endswith('.eps'):
            
            path_to_file = os.path.join(dirpath,script)
            
            sts = os.system('epstopdf '+path_to_file)
            print script, ' converted'
            sts = os.system('rm '+path_to_file)
