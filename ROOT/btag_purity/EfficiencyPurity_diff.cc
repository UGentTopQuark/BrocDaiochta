#include "EfficiencyPurity_diff.h"

EfficiencyPurity_diff::EfficiencyPurity_diff(TFile *outfile)
{
	this->outfile = outfile;
	signal_histo = NULL;
	background_histo = NULL;
	rebin_histo = -1;
}

EfficiencyPurity_diff::~EfficiencyPurity_diff()
{
}

void EfficiencyPurity_diff::set_background_histo(TH1F *background)
{
	if(rebin_histo != -1){
		const char * bname = background->GetName();
		background_histo = (TH1F*)background->Rebin(rebin_histo,bname);}
	else
		background_histo = background;
}

void EfficiencyPurity_diff::set_signal_histo(TH1F *signal)
{
	if(rebin_histo != -1){
		const char * sname = signal->GetName();
		signal_histo = (TH1F*)signal->Rebin(rebin_histo,sname);}
	else
		signal_histo = signal;
}

void EfficiencyPurity_diff::set_symmetric_cut(bool symmetric)
{
	symmetric_cut = symmetric;
}

void EfficiencyPurity_diff::set_histo_rebin(double rebin)
{
	rebin_histo = rebin;
}

void EfficiencyPurity_diff::calculate()
{
	if(background_histo == NULL || signal_histo == NULL)
		return;
	outfile->cd();


	std::string hname = (std::string) "EfficiencyXPurity_"+signal_histo->GetName();
	TH1F eff_vs_pur(hname.c_str(),hname.c_str(), signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	hname = (std::string) "Efficiency_"+signal_histo->GetName();
	TH1F eff(hname.c_str(),hname.c_str(), signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	hname = (std::string) "Purity_"+signal_histo->GetName();
	TH1F pur(hname.c_str(),hname.c_str(), signal_histo->GetNbinsX(), signal_histo->GetXaxis()->GetXmin(), signal_histo->GetXaxis()->GetXmax());
	eff_vs_pur.SetXTitle(signal_histo->GetXaxis()->GetTitle());

	int max_i=0;
	if(symmetric_cut)
		max_i =  (int) ((signal_histo->GetNbinsX()/2.0)+0.5);
	else
		max_i = signal_histo->GetNbinsX();


	signal_histo->Sumw2();
	background_histo->Sumw2();

	double max_purxeff = -1,max_pur = -1,max_eff = -1;
	double optimal_cut_purxeff = -1,optimal_cut_pur = -1,optimal_cut_eff = -1;
	for(int i = 1; i <= max_i; ++i){
		double sig_sum_bin=0,sig_err_bin = 0,eff_err = 0,pur_err = 0;
		double bg_sum_bin=0,bg_err_bin = 0;

	
		sig_sum_bin = signal_histo->GetBinContent(i);
		bg_sum_bin = background_histo->GetBinContent(i);
		sig_err_bin = signal_histo->GetBinError(i);
		bg_err_bin = background_histo->GetBinError(i);
		
		double efficiency = 0;
		if(signal_histo->Integral() != 0){
			efficiency = sig_sum_bin/ signal_histo->Integral();
			eff_err = sig_err_bin/ signal_histo->Integral();

		}
		double purity = 0;
		if((sig_sum_bin + bg_sum_bin) != 0){
			purity = sig_sum_bin / (sig_sum_bin + bg_sum_bin);
			pur_err = purity*sqrt((sig_err_bin/sig_sum_bin)*(sig_err_bin/sig_sum_bin) + ((sqrt((sig_err_bin*sig_err_bin)+(bg_err_bin*bg_err_bin)))/(sig_sum_bin+bg_sum_bin))*((sqrt((sig_err_bin*sig_err_bin)+(bg_err_bin*bg_err_bin)))/(sig_sum_bin+bg_sum_bin)));
			

		}
		std::cout << "bg sum bin: " << bg_sum_bin << std::endl;
		std::cout << "step: " << i << " value: " << signal_histo->GetBinCenter(i) << " eff: " << efficiency << " pur: " << purity << " exp: " << efficiency*purity << std::endl;
		eff_vs_pur.SetBinContent(i,efficiency*purity);
		eff_vs_pur.SetBinError(i,0);
		eff.SetBinContent(i,efficiency);
		eff.SetBinError(i,eff_err);
		pur.SetBinContent(i,purity);
		pur.SetBinError(i,pur_err);

		if(purity > max_pur){
		  // max_purxeff = (purity*efficiency)/(1-purity);
		   max_pur = purity;
		   optimal_cut_pur = signal_histo->GetBinCenter(i);
		}
		if(efficiency > max_eff){
		  // max_purxeff = (purity*efficiency)/(1-purity);
		   max_eff = purity;
		   optimal_cut_eff = signal_histo->GetBinCenter(i);
		}
		if(efficiency*purity > max_purxeff){
		  // max_purxeff = (purity*efficiency)/(1-purity);
		   max_purxeff = efficiency*purity;
		   optimal_cut_purxeff = signal_histo->GetBinCenter(i);
		}
	}

	std::cout <<"****************Optimal Purity Cut:" << optimal_cut_pur << std::endl;
	std::cout <<"****************Optimal Efficiency Cut:" << optimal_cut_eff << std::endl;
	std::cout <<"****************Optimal Cut:" << optimal_cut_purxeff << std::endl;
	CanvasHolder cholder;
	cholder.addHisto(&pur, "#pi", "L");
	std::string htitle = ("Pur:"+(std::string)signal_histo->GetTitle());
	cholder.setCanvasTitle(htitle);
	cholder.setBordersY(0,1.2);
	//	cholder.setBordersX(0,30);
	//cholder.addVLine(optimal_cut, "max.#pi");
	cholder.addHisto(&eff, "#epsilon", "L");
	htitle = ("Eff:"+(std::string)signal_histo->GetTitle());
	cholder.setCanvasTitle(htitle);
	cholder.setBordersY(0,1.2);
//	cholder.addHisto(&pur, "#pi", "L");
//	cholder.setLogY();
	cholder.setOptStat(000000);
	cholder.write(outfile);
}
