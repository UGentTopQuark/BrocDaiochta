#include "ScaleFactorPrinter.h"

comhaireamh::ScaleFactorPrinter::ScaleFactorPrinter()
{
}

comhaireamh::ScaleFactorPrinter::~ScaleFactorPrinter()
{
}

void comhaireamh::ScaleFactorPrinter::print_cpp_2d_sf(TH2D *histo, std::string id)
{
	int nbinsx = histo->GetNbinsX();
	int nbinsy = histo->GetNbinsY();

	std::cout << "const int nbinsx = " << nbinsx << ";" << std::endl;
	std::cout << "const int nbinsy = " << nbinsy << ";" << std::endl;
	std::cout << std::endl;
	std::cout << "double bin_max_sf_x[nbinsx] = {";
	for(int i = 1; i <= nbinsx; ++i){
		if(i != 1) std::cout << ", ";
		std::cout << histo->GetBinCenter(i) + (histo->GetBinWidth(i)/2.);
	}
	std::cout << "};" << std::endl;

	std::cout << "double bin_max_sf_y[nbinsy] = {";
	for(int i = 1; i <= nbinsy; ++i){
		if(i != 1) std::cout << ", ";
		std::cout << histo->GetYaxis()->GetBinCenter(i) + (histo->GetYaxis()->GetBinWidth(i)/2.);
	}
	std::cout << "};" << std::endl;

	std::cout << "double sf[" << nbinsx << "][" << nbinsy << "] = {";
	for(int i = 1; i <= nbinsx; ++i){
		if(i != 1) std::cout << ", ";
		std::cout << "{";
		for(int j = 1; j <= nbinsy; ++j){
			if(j != 1) std::cout << ", ";
			std::cout << histo->GetBinContent(i,j);
		}
		std::cout << "}" << std::endl;
	}
	std::cout << "};" << std::endl;
	std::cout << std::endl;
	std::cout << "double sf_err[" << nbinsx << "][" << nbinsy << "] = {";
	for(int i = 1; i <= nbinsx; ++i){
		if(i != 1) std::cout << ", ";
		std::cout << "{";
		for(int j = 1; j <= nbinsy; ++j){
			if(j != 1) std::cout << ", ";
			std::cout << histo->GetBinError(i,j);
		}
		std::cout << "}" << std::endl;
	}
	std::cout << "};" << std::endl;
}

void comhaireamh::ScaleFactorPrinter::print_cpp_1d_sf(TGraphAsymmErrors *graph, std::string id)
{
	int npoints = graph->GetN();
	Double_t* y = graph->GetY();
	Double_t* err_l = graph->GetEYlow();
	Double_t* err_h = graph->GetEYhigh();
	Double_t* x = graph->GetX();
	Double_t* x_err_l = graph->GetEXlow();
	Double_t* x_err_h = graph->GetEXhigh();

	std::cout << "const int nbinsx = " << npoints << ";" << std::endl;
	std::cout << std::endl;

	std::cout << "double bin_max_sf_x[nbinsx] = {";
	for(int i = 0; i < npoints; ++i){
		if(i != 0) std::cout << ", ";
		else{
			std::cout << x[i]-x_err_l[i] << std::endl;
		}
		std::cout << x[i]+x_err_h[i];
	}
	std::cout << "};" << std::endl;

	std::cout << "double sf_x[nbinsx] = {";
	for(int i = 0; i < npoints; ++i){
		if(i != 0) std::cout << ", ";
		else{
			std::cout << "0, " << std::endl;
		}
		std::cout << y[i];
	}
	std::cout << "};" << std::endl;

/*
	int nbinsx = histo->GetNbinsX();

	std::cout << "double bin_max_sf_x[nbinsx] = {";
	for(int i = 1; i <= nbinsx; ++i){
		if(i != 1) std::cout << ", ";
		std::cout << histo->GetBinCenter(i) + (histo->GetBinWidth(i)/2.);
	}
	std::cout << "};" << std::endl;

	std::cout << "double sf[" << nbinsx << "] = {";
	for(int i = 1; i <= nbinsx; ++i){
		if(i != 1) std::cout << ", ";
		std::cout << histo->GetBinContent(i);
	}
	std::cout << "};" << std::endl;
*/
}

/*
void comhaireamh::ScaleFactorPrinter::print_header(TGraphAsymmErrors *graph, std::string id)
{
	int npoints = graph->GetN();
	Double_t* err_l = graph->GetEXlow();
	Double_t* err_h = graph->GetEXhigh();
	Double_t* x = graph->GetX();

	std::cout << id << " | ";
	for(int i = 0; i < npoints; ++i){
		std::cout << x[i]-err_l[i] << "/" << x[i]+err_h[i];
		std::cout << " |\t-\t|\t+\t"; 
		if(i == npoints-1){
			std::cout << std::endl;
		}else{
			std::cout << " | ";
		}
	}
}

void comhaireamh::ScaleFactorPrinter::print_row(TGraphAsymmErrors *graph, std::string id)
{
}

void comhaireamh::ScaleFactorPrinter::print_latex(TGraphAsymmErrors *graph, std::string id)
{
	int npoints = graph->GetN();
	Double_t* x = graph->GetX();
	Double_t* err_x_l = graph->GetEXlow();
	Double_t* err_x_h = graph->GetEXhigh();

	Double_t* y = graph->GetY();
	Double_t* err_l = graph->GetEYlow();
	Double_t* err_h = graph->GetEYhigh();

	std::cout << id << std::endl;
	std::cout << "bin & value \\" << std::endl;
	for(int i = 0; i < npoints; ++i){
		double x_low = x[i]-err_x_l[i];
		double x_high = x[i]+err_x_h[i];
		std::cout << x_low << "-" << x_high << " & ";
		//printf("$%.3f-%.3f & ", x_low, x_high);

		printf("$%.3f", y[i]);
		printf("^{+%.3f}", err_l[i]);
		printf("_{-%.3f}$", err_h[i]);
		std::cout << "\\\\" << std::endl;
	}
}
*/
