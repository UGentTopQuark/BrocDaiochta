[global]
lumi = -1	# if not set to -1 label with int. lumi will be added to plots
outfile = outfile.root	# all plots are also written to this file

overlay_3 = eta_j3

overlay_7 = eta_PFj3

overlay_10 = pt_PFj3:pt_j3

overlay_11 = pt_PFj3:pt_PFj3_1match_dR01:pt_PFj3_2match_dR01:pt_PFj3_3match_dR01
overlay_12 = pt_j3:pt_j3_1match_dR01:pt_j3_2match_dR01:pt_j3_3match_dR01
overlay_13 = pt_PFj3:pt_PFj3_1match_dR03:pt_PFj3_2match_dR03:pt_PFj3_3match_dR03
overlay_14 = pt_j3:pt_j3_1match_dR03:pt_j3_2match_dR03:pt_j3_3match_dR03
overlay_15 = pt_PFj3:pt_PFj3_1match_dR05:pt_PFj3_2match_dR05:pt_PFj3_3match_dR05
overlay_16 = pt_j3:pt_j3_1match_dR05:pt_j3_2match_dR05:pt_j3_3match_dR05
overlay_17 = pt_j3:pt_j3_3match_dR01:pt_j3_3match_dR03:pt_j3_3match_dR05
overlay_18 = pt_PFj3:pt_PFj3_3match_dR01:pt_PFj3_3match_dR03:pt_PFj3_3match_dR05

overlay_35 = pt_PFj3:pt_PFj3_1match_dR05:pt_PFj3_2match_dR05:pt_PFj3_3match_dR05:pt_PFj3_3match_dR05eclean

overlay_21 = eta_j3:eta_j3_1match_dR05:eta_j3_2match_dR05:eta_j3_3match_dR05
overlay_22 = eta_PFj3:eta_PFj3_1match_dR05:eta_PFj3_2match_dR05:eta_PFj3_3match_dR05

;prescale_variable = prescale_factor

ymax = 1.0	# upper range of efficiency plot
ymin = 0.0	# lower range of efficiency plot
ytitle = efficiency
tree_name = cross_trig	# tree name in input file
dir_name = JetEffs	# directory which contains tree in input file

[eta_PFj3]
file_name = 20120329_165619_jet_trigger_eff_Data_MC_dRje/JetCrossTrigger_Data_electron_01_cutset.root
var = jet_eta
binning = -2.5,-2.0,-1.5,-1.0,-0.5,0.,0.5,1.0,1.5,2.0,2.5
title = PF no matching
pass_bit = passing_cross_trigger
xtitle = \#eta
cut_run = 178420:999999
;description = all jets p_{T}>45GeV, \#eta < 1.5

[eta_PFj3_1match_dR05:eta_PFj3]
title = PF min 1 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch1dR05

[eta_PFj3_2match_dR05:eta_PFj3]
title = PF min 2 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch2dR05

[eta_PFj3_3match_dR05:eta_PFj3]
title = PF min 3 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch3dR05

[eta_j3:eta_PFj3]
title = Calo no matching
cut_run = 173236:178419

[eta_j3_1match_dR05:eta_j3]
title = Calo min 1 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch1dR05

[eta_j3_2match_dR05:eta_j3]
title = Calo min 2 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch2dR05

[eta_j3_3match_dR05:eta_j3]
title = Calo min 3 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch3dR05

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; pt
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[pt_PFj3]
file_name = 20120329_165619_jet_trigger_eff_Data_MC_dRje/JetCrossTrigger_Data_electron_01_cutset.root
var = jet_pt
binning = 30:31:32:33:34:35:36:37:38:39:40:41:42:43:44:45:46:47:48:49:50:55:60:65:70:75:80:85:90
title = PF Data
pass_bit = passing_cross_trigger
xtitle = p_{T}
cut_run = 178420:999999
cut_njets = 3:3
cut_ntrigjets = 3:3
;cut_smallest_dR_trig_jet_on_ele = 0.3:99999
;cut_smallest_dR_trig_jet_off_ele = 0.3:99999
;description = all jets p_{T}>45GeV, \#eta < 1.5
description = exactly 3 online/offline jets

[pt_j3:pt_PFj3]
title = Calo min Data
cut_run = 173236:178419

[pt_PFj3_1match_dR01:pt_PFj3]
title = PF min 1 matches dR<0.1
pass_bit = passing_cross_trigger_nmatch1dR01

[pt_PFj3_2match_dR01:pt_PFj3]
title = PF min 2 matches dR<0.1
pass_bit = passing_cross_trigger_nmatch2dR01

[pt_PFj3_3match_dR01:pt_PFj3]
title = PF min 3 matches dR<0.1
pass_bit = passing_cross_trigger_nmatch3dR01

[pt_j3_1match_dR01:pt_j3]
title = Calo min 1 matches dR<0.1
pass_bit = passing_cross_trigger_nmatch1dR01

[pt_j3_2match_dR01:pt_j3]
title = Calo min 2 matches dR<0.1
pass_bit = passing_cross_trigger_nmatch2dR01

[pt_j3_3match_dR01:pt_j3]
title = Calo min 3 matches dR<0.1
pass_bit = passing_cross_trigger_nmatch3dR01

[pt_PFj3_1match_dR03:pt_PFj3]
title = PF min 1 matches dR<0.3
pass_bit = passing_cross_trigger_nmatch1dR03

[pt_PFj3_2match_dR03:pt_PFj3]
title = PF min 2 matches dR<0.3
pass_bit = passing_cross_trigger_nmatch2dR03

[pt_PFj3_3match_dR03:pt_PFj3]
title = PF min 3 matches dR<0.3
pass_bit = passing_cross_trigger_nmatch3dR03

[pt_PFj3_3match_dR05eclean:pt_PFj3]
title = PF min 3 matches dR<0.5, onl. e/jet cleaning
pass_bit = passing_cross_trigger_nmatch3dR05
cut_smallest_dR_trig_jet_on_ele = 0.1:99999

[pt_j3_1match_dR03:pt_j3]
title = Calo min 1 matches dR<0.3
pass_bit = passing_cross_trigger_nmatch1dR03

[pt_j3_2match_dR03:pt_j3]
title = Calo min 2 matches dR<0.3
pass_bit = passing_cross_trigger_nmatch2dR03

[pt_j3_3match_dR03:pt_j3]
title = Calo min 3 matches dR<0.3
pass_bit = passing_cross_trigger_nmatch3dR03

[pt_PFj3_1match_dR05:pt_PFj3]
title = PF min 1 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch1dR05

[pt_PFj3_2match_dR05:pt_PFj3]
title = PF min 2 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch2dR05

[pt_PFj3_3match_dR05:pt_PFj3]
title = PF min 3 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch3dR05

[pt_j3_1match_dR05:pt_j3]
title = Calo min 1 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch1dR05

[pt_j3_2match_dR05:pt_j3]
title = Calo min 2 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch2dR05

[pt_j3_3match_dR05:pt_j3]
title = Calo min 3 matches dR<0.5
pass_bit = passing_cross_trigger_nmatch3dR05
