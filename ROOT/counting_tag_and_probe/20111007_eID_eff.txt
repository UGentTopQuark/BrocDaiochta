[global]
lumi = -1	# if not set to -1 label with int. lumi will be added to plots
outfile = outfile.root	# all plots are also written to this file

overlay_1 = eIdVL:eIdL:eIdM:eIdT:eIdST:eIdHT1

ymax = 1.0
ymin = 0.0
ytitle = Efficiency
tree_name = fitter_tree	# tree name in input file
dir_name = lepEffs	# directory which contains tree in input file

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; eta iso 2010 vs 2011 pretech vs 2011 posttech
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[eIdVL]
file_name = 20111023_230100_DY_eID_validation/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
var = eta
binning = -2.5:-2.4:-2.3:-2.2:-2.1:-2.0:-1.9:-1.8:-1.7:-1.6:-1.5:-1.4:-1.3:-1.2:-1.1:-1.0:-0.9:-0.8:-0.7:-0.6:-0.5:-0.4:-0.3:-0.2:-0.1:0:0.1:0.2:0.3:0.4:0.5:0.6:0.7:0.8:0.9:1.0:1.1:1.2:1.3:1.4:1.5:1.6:1.7:1.8:1.9:2.0:2.1:2.2:2.3:2.4:2.5
pass_bit = passing_id
xtitle = \#eta
title = VeryLoose
cut_eta = -2.5:2.5
cut_pt = 30:1000
ytitle = Efficiency

[eIdL:eIdVL]
file_name = 20111023_230100_DY_eID_validation/trigger_tag_and_probe_Zjets_e_background_02_cutset.root
title = Loose

[eIdM:eIdVL]
file_name = 20111023_230100_DY_eID_validation/trigger_tag_and_probe_Zjets_e_background_03_cutset.root
title = Medium

[eIdT:eIdVL]
file_name = 20111023_230100_DY_eID_validation/trigger_tag_and_probe_Zjets_e_background_04_cutset.root
title = Tight

[eIdST:eIdVL]
file_name = 20111023_230100_DY_eID_validation/trigger_tag_and_probe_Zjets_e_background_05_cutset.root
title = SuperTight1

[eIdHT1:eIdVL]
file_name = 20111023_230100_DY_eID_validation/trigger_tag_and_probe_Zjets_e_background_06_cutset.root
title = HyperTight1

[eIdHT2:eIdVL]
file_name = 20111023_230100_DY_eID_validation/trigger_tag_and_probe_Zjets_e_background_07_cutset.root
title = HyperTight2

[eIdHT3:eIdVL]
file_name = 20111023_230100_DY_eID_validation/trigger_tag_and_probe_Zjets_e_background_08_cutset.root
title = HyperTight3

[eIdHT4:eIdVL]
file_name = 20111023_230100_DY_eID_validation/trigger_tag_and_probe_Zjets_e_background_09_cutset.root
title = HyperTight4
