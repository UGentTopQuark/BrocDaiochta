#ifndef COMHAIREAMH_TAGANDPROBE_H
#define COMHAIREAMH_TAGANDPROBE_H

/**
 * \class comhaireamh::TagAndProbeManager
 * 
 * \brief Manages read in files, creates TreeReaders for each file and prints plots and overlays.
 *
 * The main class of counting_tag_and_probe. For each file (the assumption is
 * that there is only one tree per file) a TreeReader is created. The
 * TreeReader manages then the tree within a file and all variables therein.
 * The TagAndProbeManager lets then the TreeReader objects process the
 * individual files and return efficiency histograms. These are then for all
 * variables returned to TagAndProbeManager, formatted, and overlayed if necessary.
 *
 */

#include "ConfigReader/ConfigReader.h"
#include "CanvasHolder/Canvas_Holder.h"
#include "TFile.h"
#include "TTree.h"
#include "TreeReader.h"
#include "TGraphAsymmErrors.h"
#include <map>
#include "TablePrinter.h"
#include "ScaleFactorPrinter.h"

namespace comhaireamh{
	class TagAndProbeManager{
		public:
			TagAndProbeManager(TFile *outfile);
			~TagAndProbeManager();

			inline void set_config_reader(eire::ConfigReader *config_reader) { this->config_reader = config_reader; };
			/// main function, run this after setting ConfigReader
			void run_tag_and_probe();
		private:
			TGraphAsymmErrors* get_scale_factor_graph(TGraphAsymmErrors *graph1, TGraphAsymmErrors *graph2);
			std::vector<std::string> get_scale_factor_requests();
			void produce_sf_plots();
			void produce_sf_plots2D();

			std::vector<std::vector<std::pair<std::string,std::string> > > get_overlays();
			/// setup all variables within trees
			void configure_trees();
			/// process all events in trees
			void process_trees();
			/// after processing of trees, create plots
			void create_plots();
			eire::ConfigReader *config_reader;
			/// for each file create a TreeReader to process the file
			void get_trees_from_files();

			std::map<std::string, comhaireamh::TreeReader*> tree_readers;
			std::vector<TFile*> open_files;
			TFile *outfile;
	};
}

#endif
