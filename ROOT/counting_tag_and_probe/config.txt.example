[global]
lumi = -1	# if not set to -1 label with int. lumi will be added to plots
outfile = outfile.root	# all plots are also written to this file

#############################
####	define overlays of variables, the listed efficiencies will be shown
####	together on one single plot
#############################
overlay_1 = pt_Data,pt_MC	# comparison MC vs Data for pt/eta/phi
overlay_2 = eta_Data,eta_MC
overlay_3 = phi_Data,phi_MC
#overlay_4 = eta_MC_HLT,eta_MC_L1,eta_MC_HLTwrtL1 # efficiency on different trigger levels
#overlay_5 = eta_Data_L1,eta_Data_HLT,eta_Data_HLTwrtL1 # efficiency on different trigger levels

ymax = 1.0	# upper range of efficiency plot
ymin = 0.6	# lower range of efficiency plot
ytitle = events
tree_name = fitter_tree	# tree name in input file
dir_name = lepEffs	# directory which contains tree in input file

############################
####	each variable must be defined within a section. A section is started with
####	[new variable name]
############################

[pt_Data]
file_name = trigger_tag_and_probe_Data_muon.root	# file with root tree
var = pt	# name of variable in root tree
binning = 5:10:15:20:25:30:35:40:60:80:100	# binning of variable for final plot
title = Data		# legend entry for variable
pass_bit = passing_idx
xtitle = p_{T}	# title for x axis

###############################
####	colon in section name means [(new variable):(inherit from old variable)]
####	the program will search for parameters that are not specified under
####	(new variable) in the section (old variable)
####	if a parameter is not found in a section the program searches in [global]
####	for the parameter
####	if the parameter can not be found in any section, the program will probably crash
###############################
[pt_MC:pt_Data]
file_name = trigger_tag_and_probe_DYJetsToLL_mu_background.root
title = Simulation

[eta_MC]
file_name = trigger_tag_and_probe_DYJetsToLL_mu_background.root
var = eta
binning = -2.1,-1.2,-0.9,0,0.9,1.2,2.1
title = Simulation
pass_bit = passing_idx
xtitle = \#eta

[eta_Data:eta_MC]
file_name = trigger_tag_and_probe_Data_muon.root
title = Data

[phi_MC]
file_name = trigger_tag_and_probe_DYJetsToLL_mu_background.root
var = phi
binning = -4.0,-3.5,-3.0,-2.5,-2.0,-1.5,-1.0,-0.5,0,0.5,1.0,1.5,2.0,2.5,3.0,3.5
title = Simulation
pass_bit = passing_idx
xtitle = \#phi

[phi_Data:phi_MC]
file_name = trigger_tag_and_probe_Data_muon.root
title = Data
