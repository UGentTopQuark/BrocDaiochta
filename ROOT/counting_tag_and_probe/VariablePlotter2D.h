#ifndef COMHAIREAMH_VARIABLEPLOTTER2D_H
#define COMHAIREAMH_VARIABLEPLOTTER2D_H

/**
 * \class comhaireamh::VariablePlotter2D
 *
 * \brief Plots 2D efficiency curves.
 *
 * For each variable (section in the config file) one VariablePlotter object is
 * booked and manages the plotting of the variables and the efficiency graph
 * calculation.
 *
 * \author bklein
 */

#include "ConfigReader/ConfigReader.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TTree.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TFile.h"
#include "VariableHolder.h"
#include "Cut.h"

namespace comhaireamh{
	class VariablePlotter2D{
		public:
	  VariablePlotter2D(std::string identifier, eire::ConfigReader *config_reader, TTree *tree, TFile *outfile, comhaireamh::VariableHolder *var_holder);
			~VariablePlotter2D();
			/// called for each event and plots event
			void plot();
			TH2D *get_efficiency_graph2D();
			TGraphAsymmErrors *get_efficiency_graph();
			/// return the name of the variables that this class processes
			inline std::string get_var_name() { return var_name; };
			inline std::string get_var2_name() { return var2_name; };
			/// manage variable assignment with VariableHolder
			void configure();
		private:
			void prepare_cuts(eire::ConfigReader *config_reader, std::string identifier);
			std::string identifier;
			TH2D *all_histo, *pass_histo, *fail_histo;
			TH2D *efficiency_graph2D;
			TGraphAsymmErrors *efficiency_graph;
			std::string passing_bit;
			TFile *outfile;
			std::string input_file_name;
			std::string var_name;
			std::string var2_name;
			std::string pass_bit_name;
			int var_index;
			int var2_index;
			int pass_bit_index;
			int weight_index;
			int passing_weight_index;

			std::vector<comhaireamh::Cut> cuts;

			comhaireamh::VariableHolder *var_holder;
	};
}

#endif
