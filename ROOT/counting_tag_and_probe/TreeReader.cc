#include "TreeReader.h"

comhaireamh::TreeReader::TreeReader(TFile *outfile)
{
  this->outfile = outfile;
  variable_list = new std::vector<std::string>();
  
  var_holder = new comhaireamh::VariableHolder();
}

comhaireamh::TreeReader::~TreeReader()
{
  if(variable_list){delete variable_list; variable_list = NULL;}
  
  // delete all VariablePlotters
  for(std::map<std::string, comhaireamh::VariablePlotter1D*>::iterator var = variables.begin();
      var != variables.end();
      ++var){
    delete var->second;
  }
  for(std::map<std::string, comhaireamh::VariablePlotter2D*>::iterator var = variables2.begin();
      var != variables2.end();
      ++var){
    delete var->second;
  }
}

void comhaireamh::TreeReader::configure_variables()
{
  // assign all necessary variables from the ROOT tree to the store
  // system within VariableHolder
  var_holder->configure_variables(tree);
  
  // Each variable plotter initialises the indices it needs to access
  // later on variables from VariableHolder
  
  for(std::map<std::string, comhaireamh::VariablePlotter1D*>::iterator
	var = variables.begin();
      var != variables.end();
      ++var){
    var->second->configure();
  }
  for(std::map<std::string, comhaireamh::VariablePlotter2D*>::iterator
	var = variables2.begin();
      var != variables2.end();
      ++var){
    var->second->configure();
  }
}

// main function, process all events for tree
void comhaireamh::TreeReader::read()
{
  // loop over all events ...
  int nevents = tree->GetEntries();
  for(int event = 0; event < nevents; ++event){
    tree->GetEntry(event);
    int i = 0;
    // ... and plot the variables as defined in the config file
    
    for(std::map<std::string, comhaireamh::VariablePlotter1D*>::iterator var = variables.begin();
	var != variables.end();
	++var){
      var->second->plot();
    }
    for(std::map<std::string, comhaireamh::VariablePlotter2D*>::iterator var = variables2.begin();
	var != variables2.end();
	++var){
      var->second->plot();
    }  
    ++i;
  }
}

std::vector<std::string> *comhaireamh::TreeReader::get_variables()
{
  variable_list->clear();
  
  for(std::map<std::string, comhaireamh::VariablePlotter1D*>::iterator var = variables.begin();
      var != variables.end();
      ++var){
    variable_list->push_back(var->first);
  }
  for(std::map<std::string, comhaireamh::VariablePlotter2D*>::iterator var = variables2.begin();
      var != variables2.end();
      ++var){
    variable_list->push_back(var->first);
  }return variable_list;
}

double comhaireamh::TreeReader::get_efficiency(std::string section)
{
  if(variables.find(section) != variables.end()){
    return variables[section]->get_efficiency();	
  }else
    return 0;
}

TGraphAsymmErrors *comhaireamh::TreeReader::get_efficiency_graph(std::string section)
{
  if(variables.find(section) != variables.end()){
    return variables[section]->get_efficiency_graph();	
  }else
    return NULL;
}

TH2D *comhaireamh::TreeReader::get_efficiency_graph2D(std::string section)

{
   if(variables2.find(section) != variables2.end()){
     return variables2[section]->get_efficiency_graph2D();
   }
   else
     return NULL;
}

void comhaireamh::TreeReader::add_section(std::string section)
{
  // for each variable (section in config) create new VariablePlotter and
  // book the necessary variables in VariableHolder
	if(config_reader->get_bool_var("2D_plot",section,false))
	{
		variables2[section] = new VariablePlotter2D(section, config_reader, tree, outfile, var_holder);
		var_holder->add_variable(config_reader->get_var("var2", section, true));
	}
	else
	{
		variables[section] = new VariablePlotter1D(section, config_reader, tree, outfile, var_holder);
	}
  
	var_holder->add_pass_bit(config_reader->get_var("pass_bit", section, true), section);
	var_holder->add_variable(config_reader->get_var("var", section, true));
}
