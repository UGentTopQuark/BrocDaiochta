#ifndef comhaireamh_SCALEFACTORPRINTER_H
#define comhaireamh_SCALEFACTORPRINTER_H

#include "TGraphAsymmErrors.h"
#include <iostream>
#include "TH2D.h"

namespace comhaireamh{
	class ScaleFactorPrinter{
		public:
			ScaleFactorPrinter();
			~ScaleFactorPrinter();

			void print_cpp_2d_sf(TH2D *histo, std::string id);
			void print_cpp_1d_sf(TGraphAsymmErrors *graph, std::string id);
		private:
	};
}

#endif
