[global]
lumi = -1	# if not set to -1 label with int. lumi will be added to plots
outfile = outfile.root	# all plots are also written to this file

;overlay_1 = eta_IsoMu_2e33_legacy:eta_IsoMu_3e33_legacy:eta_IsoMu_5e33_legacy:eta_IsoMu_5e33
;overlay_11 = eta_IsoMu_2e33_legacy:eta_IsoMu_2e33
overlay_12 = eta_IsoMu_3e33_legacy:eta_IsoMu_3e33_PF2PAT
overlay_13 = pt_IsoMu_3e33_legacy:pt_IsoMu_3e33_PF2PAT
;overlay_13 = eta_IsoMu_5e33_legacy:eta_IsoMu_5e33
erlay_1 = eta_IsoMu_2e33:eta_IsoMu_3e33:eta_IsoMu_5e33
overlay_2 = pt_IsoMu_2e33:pt_IsoMu_3e33:pt_IsoMu_5e33
erlay_3 = eta_Mu_2e33:eta_Mu_3e33:eta_Mu_5e33
overlay_4 = pt_Mu_2e33:pt_Mu_3e33:pt_Mu_5e33

overlay_5 = eta_IsoMu_2e33_hltwrtl1:eta_IsoMu_3e33_hltwrtl1:eta_IsoMu_5e33_hltwrtl1
overlay_6 = eta_Mu_2e33_hltwrtl1:eta_Mu_3e33_hltwrtl1:eta_Mu_5e33_hltwrtl1

overlay_7 = eta_IsoMu_2e33_l1:eta_IsoMu_3e33_l1:eta_IsoMu_5e33_l1
overlay_8 = eta_Mu_2e33_l1:eta_Mu_3e33_l1:eta_Mu_5e33_l1

overlay_9 = npv_IsoMu_2e33:npv_IsoMu_3e33:npv_IsoMu_5e33
overlay_91 = npv_IsoMu_2e33_l1:npv_IsoMu_3e33_l1:npv_IsoMu_5e33_l1
overlay_92 = npv_IsoMu_2e33_hltwrtl1:npv_IsoMu_3e33_hltwrtl1:npv_IsoMu_5e33_hltwrtl1

overlay_19 = npv_Mu_2e33:npv_Mu_3e33:npv_Mu_5e33
overlay_191 = npv_Mu_2e33_l1:npv_Mu_3e33_l1:npv_Mu_5e33_l1
overlay_192 = npv_Mu_2e33_hltwrtl1:npv_Mu_3e33_hltwrtl1:npv_Mu_5e33_hltwrtl1

ymax = 1.0
ymin = 0.4
ytitle = Efficiency
tree_name = fitter_tree	# tree name in input file
dir_name = lepEffs	# directory which contains tree in input file

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; eta iso 2010 vs 2011 pretech vs 2011 posttech
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[eta_IsoMu_3e33_legacy]
file_name = 20111015_211246_data_mu_pretech_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
var = eta
binning = -2.4:-2.1:-1.5:-1.2:-0.9:0:0.9:1.2:1.5:2.1:2.4
pass_bit = passing_idx
xtitle = \#eta
title = 3e33 (standard leptons)
cut_eta = -2.4:2.4
cut_pt = 30:1000
cut_reliso = 0:0.15
cut_run = 173236:178420
ytitle = HLT_IsoMu24 Efficiency
description = p_{T} > 30 GeV/c

[eta_IsoMu_2e33_legacy:eta_IsoMu_3e33_legacy]
file_name = 20111015_211246_data_mu_pretech_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 2e33 (standard leptons)
cut_run = 170826:172802

[eta_IsoMu_5e33_legacy:eta_IsoMu_3e33_legacy]
file_name = 20111020_121837_muonPOG_5e33_more_runs/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 5e33 (standard leptons)
cut_run = 178421:999999

[eta_IsoMu_2e33:eta_IsoMu_3e33_legacy]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 2e33
cut_run = 170826:172802
cut_reliso = 0:999999
;cut_pfreliso = 0:0.125
cut_pfreliso = 0:0.15

[eta_IsoMu_3e33:eta_IsoMu_3e33_legacy]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 3e33
cut_run = 173236:178420
cut_reliso = 0:999999
;cut_pfreliso = 0:0.125
cut_pfreliso = 0:0.15

[eta_IsoMu_3e33_PF2PAT:eta_IsoMu_3e33]
title = 3e33 (PF2PAT)

[eta_IsoMu_5e33:eta_IsoMu_3e33_legacy]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 5e33
cut_run = 178421:999999
cut_reliso = 0:999999
;cut_pfreliso = 0:0.125
cut_pfreliso = 0:0.15

[eta_Mu_3e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
var = eta
binning = -2.4:-2.1:-1.5:-1.2:-0.9:0:0.9:1.2:1.5:2.1:2.4
pass_bit = passing_idx
xtitle = \#eta
title = 3e33
cut_eta = -2.4:2.4
cut_pt = 45:1000
cut_pfreliso = 0:0.15
cut_run = 173236:178420
ytitle = HLT_Mu40 Efficiency
description = p_{T} > 45 GeV/c

[eta_Mu_2e33:eta_Mu_3e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 2e33 (Mu40)
cut_run = 170826:172802

[eta_Mu_5e33:eta_Mu_3e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 5e33 (Mu40)
cut_run = 178421:999999

[eta_Mu_prv4:eta_Mu_5e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 165071-168437
cut_run = 165071:168437

[pt_IsoMu_3e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
var = pt
binning = 0:5:10:15:20:21:22:23:24:25:26:30:35:40:45:50:55:60:65::70:75:80:90:100:110:120:130:140:150
ymin = 0.0
title = 3e33
pass_bit = passing_idx
xtitle = p_{T} [GeV/c]
cut_eta = -2.1:2.1
cut_pfreliso = 0:0.15
cut_run = 173236:178420
2D_plot = false
ytitle = HLT_IsoMu24 Efficiency
description = |\#eta| < 2.1

[pt_IsoMu_3e33_PF2PAT:pt_IsoMu_3e33]
title = 3e33 (PF2PAT)

[pt_IsoMu_3e33_legacy:pt_IsoMu_3e33]
file_name = 20111015_211246_data_mu_pretech_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 3e33 (standard leptons)
cut_pfreliso = 0:9999
cut_reliso = 0:0.15

[pt_IsoMu_2e33:pt_IsoMu_3e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 2e33
cut_run = 170826:172802

[pt_IsoMu_5e33:pt_IsoMu_3e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 5e33
cut_run = 178421:999999

[pt_Mu_3e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
var = pt
binning = 0:5:10:15:20:25:30:35:38:39:40:41:42:43:45:50:55:60:65::70:75:80:90:100:110:120:130:140:150
ymin = 0.0
title = 3e33
pass_bit = passing_idx
xtitle = p_{T} [GeV/c]
cut_eta = -2.1:2.1
cut_pfreliso = 0:0.15
cut_run = 173236:178420
2D_plot = false
ytitle = HLT_40 Efficiency
description = |\#eta| < 2.1

[pt_Mu_2e33:pt_Mu_3e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 2e33
cut_run = 170826:172802

[pt_Mu_5e33:pt_Mu_3e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 5e33
cut_run = 178421:999999

[eta_IsoMu_3e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
var = eta
binning = -2.4:-2.1:-1.5:-1.2:-0.9:0:0.9:1.2:1.5:2.1:2.4
pass_bit = passing_hltwrtl1
xtitle = \#eta
title = 3e33
cut_eta = -2.4:2.4
cut_pt = 30:1000
cut_pfreliso = 0:0.15
cut_run = 173236:178420
ytitle = HLT/L1(IsoMu24) Efficiency
description = p_{T} > 30 GeV/c

[eta_IsoMu_2e33_hltwrtl1:eta_IsoMu_3e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 2e33
cut_run = 170826:172802

[eta_IsoMu_5e33_hltwrtl1:eta_IsoMu_3e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 5e33
cut_run = 178421:999999

[eta_Mu_3e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
var = eta
binning = -2.4:-2.1:-1.5:-1.2:-0.9:0:0.9:1.2:1.5:2.1:2.4
pass_bit = passing_hltwrtl1
xtitle = \#eta
title = 3e33 (Mu40)
cut_eta = -2.4:2.4
cut_pt = 45:1000
cut_pfreliso = 0:0.15
cut_run = 173236:178420
ytitle = HLT/L1(Mu40) Efficiency
description = p_{T} > 45 GeV/c

[eta_Mu_2e33_hltwrtl1:eta_Mu_3e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 2e33 (Mu40)
cut_run = 170826:172802

[eta_Mu_5e33_hltwrtl1:eta_Mu_3e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 5e33 (Mu40)
cut_run = 178421:999999

[eta_IsoMu_3e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
var = eta
binning = -2.4:-2.1:-1.5:-1.2:-0.9:0:0.9:1.2:1.5:2.1:2.4
pass_bit = passing_l1
xtitle = \#eta
title = 3e33
cut_eta = -2.4:2.4
cut_pt = 30:1000
cut_pfreliso = 0:0.15
cut_run = 173236:178420
ytitle = L1Seed(IsoMu24) Efficiency
description = p_{T} > 30 GeV/c

[eta_IsoMu_2e33_l1:eta_IsoMu_3e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 2e33
cut_run = 170826:172802

[eta_IsoMu_5e33_l1:eta_IsoMu_3e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 5e33
cut_run = 178421:999999

[eta_Mu_3e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
var = eta
binning = -2.4:-2.1:-1.5:-1.2:-0.9:0:0.9:1.2:1.5:2.1:2.4
pass_bit = passing_l1
xtitle = \#eta
title = 3e33 (Mu40)
cut_eta = -2.4:2.4
cut_pt = 45:1000
cut_pfreliso = 0:0.15
cut_run = 173236:178420
ytitle = L1Seed(40) Efficiency
description = p_{T} > 45 GeV/c

[eta_Mu_2e33_l1:eta_Mu_3e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 2e33 (Mu40)
cut_run = 170826:172802

[eta_Mu_5e33_l1:eta_Mu_3e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 5e33 (Mu40)
cut_run = 178421:999999

[npv_IsoMu_2e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
var = npvertices
binning = 0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
title = 2e33
pass_bit = passing_idx
xtitle = \# primary vertices
cut_eta = -2.1:2.1
cut_pt = 30:1000
cut_pfreliso = 0:0.15
cut_run = 170826:172802

[npv_IsoMu_3e33:npv_IsoMu_2e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 3e33
cut_run = 173236:178420

[npv_IsoMu_5e33:npv_IsoMu_2e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 5e33
cut_run = 178421:999999

[npv_IsoMu_2e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
var = npvertices
binning = 0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
title = 2e33
ytitle = L1 Efficiency (IsoMu24)
pass_bit = passing_l1
xtitle = \# primary vertices
cut_eta = -2.1:2.1
cut_pt = 30:1000
cut_pfreliso = 0:0.15
cut_run = 170826:172802

[npv_IsoMu_3e33_l1:npv_IsoMu_2e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 3e33
cut_run = 173236:178420

[npv_IsoMu_5e33_l1:npv_IsoMu_2e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 5e33
cut_run = 178421:999999

[npv_IsoMu_2e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
var = npvertices
binning = 0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
title = 2e33
pass_bit = passing_hltwrtl1
ytitle = HLT/L1 Efficiency (IsoMu24)
xtitle = \# primary vertices
cut_eta = -2.1:2.1
cut_pt = 30:1000
cut_pfreliso = 0:0.15
cut_run = 170826:172802

[npv_IsoMu_3e33_hltwrtl1:npv_IsoMu_2e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 3e33
cut_run = 173236:178420

[npv_IsoMu_5e33_hltwrtl1:npv_IsoMu_2e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_01_cutset.root
title = 5e33
cut_run = 178421:999999

[npv_Mu_2e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
var = npvertices
binning = 0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
title = 2e33
pass_bit = passing_idx
xtitle = \# primary vertices
cut_eta = -2.1:2.1
cut_pt = 45:1000
cut_pfreliso = 0:0.15
cut_run = 170826:172802

[npv_Mu_3e33:npv_Mu_2e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 3e33
cut_run = 173236:178420

[npv_Mu_5e33:npv_Mu_2e33]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 5e33
cut_run = 178421:999999

[npv_Mu_2e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
var = npvertices
binning = 0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
title = 2e33
ytitle = L1 Efficiency (Mu24)
pass_bit = passing_l1
xtitle = \# primary vertices
cut_eta = -2.1:2.1
cut_pt = 45:1000
cut_pfreliso = 0:0.15
cut_run = 170826:172802

[npv_Mu_3e33_l1:npv_Mu_2e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 3e33
cut_run = 173236:178420

[npv_Mu_5e33_l1:npv_Mu_2e33_l1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 5e33
cut_run = 178421:999999

[npv_Mu_2e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
var = npvertices
binning = 0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
title = 2e33
pass_bit = passing_hltwrtl1
ytitle = HLT/L1 Efficiency (Mu24)
xtitle = \# primary vertices
cut_eta = -2.1:2.1
cut_pt = 45:1000
cut_pfreliso = 0:0.15
cut_run = 170826:172802

[npv_Mu_3e33_hltwrtl1:npv_Mu_2e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 3e33
cut_run = 173236:178420

[npv_Mu_5e33_hltwrtl1:npv_Mu_2e33_hltwrtl1]
file_name = 20111028_105911_data_mu_tp/trigger_tag_and_probe_Data_muon_02_cutset.root
title = 5e33
cut_run = 178421:999999
