[global]
lumi = -1	# if not set to -1 label with int. lumi will be added to plots
outfile = outfile.root	# all plots are also written to this file

overlay_11 = pt_id_Data:pt_id_MC:pt_vlines
overlay_12 = eta_id_Data:eta_id_MC:eta_vlines
overlay_13 = npv_id_Data:npv_id_MC
overlay_14 = njets_id_Data:njets_id_MC

overlay_21 = pt_iso_Data:pt_iso_MC:pt_iso_Data_noEA:pt_iso_MC_noEA:pt_vlines
overlay_22 = eta_iso_Data:eta_iso_MC:eta_iso_Data_noEA:eta_iso_MC_noEA:eta_vlines
overlay_23 = npv_iso_Data:npv_iso_MC:npv_iso_Data_noEA:npv_iso_MC_noEA
overlay_24 = njets_iso_Data:njets_iso_MC:njets_iso_Data_noEA:njets_iso_MC_noEA

overlay_31 = pt_cr_Data:pt_cr_MC:pt_vlines
overlay_32 = eta_cr_Data:eta_cr_MC:eta_vlines
overlay_33 = npv_cr_Data:npv_cr_MC
overlay_34 = njets_cr_Data:njets_cr_MC

overlay_41 = pt_idx_Data:pt_idx_MC:pt_vlines
overlay_42 = eta_idx_Data:eta_idx_MC:eta_vlines
overlay_43 = npv_idx_Data:npv_idx_MC
overlay_44 = njets_idx_Data:njets_idx_MC

overlay_511 = pt_idx_Data1e33:pt_idx_Data2e33:pt_idx_Data3e33:pt_idx_Data5e33:pt_vlines
overlay_521 = pt_l1_Data1e33:pt_l1_Data2e33:pt_l1_Data3e33:pt_l1_Data5e33:pt_vlines
overlay_531 = pt_hlt_Data1e33:pt_hlt_Data2e33:pt_hlt_Data3e33:pt_hlt_Data5e33:pt_vlines
overlay_512 = eta_idx_Data1e33:eta_idx_Data2e33:eta_idx_Data3e33:eta_idx_Data5e33:eta_vlines
overlay_522 = eta_l1_Data1e33:eta_l1_Data2e33:eta_l1_Data3e33:eta_l1_Data5e33:eta_vlines
overlay_532 = eta_hlt_Data1e33:eta_hlt_Data2e33:eta_hlt_Data3e33:eta_hlt_Data5e33:eta_vlines
overlay_513 = npv_idx_Data1e33:npv_idx_Data2e33:npv_idx_Data3e33:npv_idx_Data5e33
overlay_523 = npv_l1_Data1e33:npv_l1_Data2e33:npv_l1_Data3e33:npv_l1_Data5e33
overlay_533 = npv_hlt_Data1e33:npv_hlt_Data2e33:npv_hlt_Data3e33:npv_hlt_Data5e33

overlay_61 = run_idx_Data:run_hlt_Data:run_l1_Data

ymax = 1.
ymin = 0.75
ytitle = Electron Efficiency
tree_name = fitter_tree	# tree name in input file
dir_name = lepEffs	# directory which contains tree in input file

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;	hlines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[pt_vlines]
vlines = 30.

[eta_vlines]
vlines = -1.5660:-1.4442:1.4442:1.5660

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;	id efficiencies
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[scale_factor_pt_id]
data = pt_id_Data
mc = pt_id_MC
xtitle = p_{T} (GeV/c)
ytitle = Electron ID Scale Factor
title = 
ymax = 1.05
ymin = 0.9
vlines = 30.

[scale_factor_npv_id]
data = npv_id_Data
mc = npv_id_MC
xtitle = Number of Primary Vertices
ytitle = Electron ID Scale Factor
title = 
ymax = 1.05
ymin = 0.9

[scale_factor_eta_id]
data = eta_id_Data
mc = eta_id_MC
xtitle = \#eta Supercluster
ytitle = Electron ID Scale Factor
title = 
ymax = 1.05
ymin = 0.9
vlines = -1.5660:-1.4442:1.4442:1.5660

[scale_factor_njets_id]
data = njets_id_Data
mc = njets_id_MC
xtitle = Number of Jets
ytitle = Electron ID Scale Factor
title = 
ymax = 1.05
ymin = 0.9


;;;;;;;;;; pt ;;;;;;;;;;;;;;;;;;;;

[pt_id_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = pt
binning = 0:5:10:15:20:25:30:35:40:45:50:55:60:65:80:100:200
pass_bit = passing_id
xtitle = p_{T} (GeV/c)
title = Data
cut_abseta = 0:1.5

[pt_id_MC:pt_id_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = Z+jets

[eta_id_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = sc_eta
binning = -2.5:-2.0:-1.5660:-1.4442:-1.0:-0.5:0:0.5:1.0:1.4442:1.5660:2.0:2.5
pass_bit = passing_id
xtitle = \#eta Supercluster
title = Data
cut_pt = 30:999999

[eta_id_MC:eta_id_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
var = sc_eta
title = Z+jets
cut_pt = 30:999999

[npv_id_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = npvertices
binning = -0.5:0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
pass_bit = passing_id
xtitle = Number of Primary Vertices
title = Data
cut_pt = 30:999999
cut_abseta = 0:1.5

[npv_id_MC:npv_id_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = Z+jets

[njets_id_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = njets_asymm
binning = -0.5:0.5:1.5:2.5:3.5:4.5:8.5
pass_bit = passing_id
xtitle = Number of Jets
title = Data
cut_pt = 30:999999
cut_abseta = 0:1.5

[njets_id_MC:njets_id_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = Z+jets

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;	conversion rejection efficiencies
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[scale_factor_pt_cr]
data = pt_cr_Data
mc = pt_cr_MC
xtitle = p_{T} (GeV/c)
ytitle = Conversion Rejection Scale Factor
title = 
ymax = 1.05
ymin = 0.9
vlines = 30.

[scale_factor_npv_cr]
data = npv_cr_Data
mc = npv_cr_MC
xtitle = Number of Primary Vertices
ytitle = Conversion Rejection Scale Factor
title = 
ymax = 1.05
ymin = 0.9

[scale_factor_eta_cr]
data = eta_cr_Data
mc = eta_cr_MC
xtitle = \#eta Supercluster
ytitle = Conversion Rejection Scale Factor
title = 
ymax = 1.05
ymin = 0.9
vlines = -1.5660:-1.4442:1.4442:1.5660

[scale_factor_njets_cr]
data = njets_cr_Data
mc = njets_cr_MC
xtitle = Number of Jets
ytitle = Conversion Rejection Scale Factor
title = 
ymax = 1.05
ymin = 0.9


;;;;;;;;;; pt ;;;;;;;;;;;;;;;;;;;;

[pt_cr_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_02_cutset.root
var = pt
binning = 0:5:10:15:20:25:30:35:40:45:50:55:60:65:80:100:200
pass_bit = passing_id
xtitle = p_{T} (GeV/c)
title = Data
cut_abseta = 0:1.5

[pt_cr_MC:pt_cr_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_02_cutset.root
title = Z+jets

[scale_factor_pt_cr]
data = pt_cr_Data
mc = pt_cr_MC
xtitle = p_{T} (GeV/c)
ytitle = Electron ID Scale Factor
title = 
ymax = 1.05
ymin = 0.9
vlines = 30.

[eta_cr_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_02_cutset.root
var = sc_eta
binning = -2.5:-2.0:-1.5660:-1.4442:-1.0:-0.5:0:0.5:1.0:1.4442:1.5660:2.0:2.5
pass_bit = passing_id
xtitle = \#eta Supercluster
title = Data
cut_pt = 30:999999

[eta_cr_MC:eta_cr_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_02_cutset.root
var = sc_eta
title = Z+jets
cut_pt = 30:999999

[npv_cr_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_02_cutset.root
var = npvertices
binning = -0.5:0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
pass_bit = passing_id
xtitle = Number of Primary Vertices
title = Data
cut_pt = 30:999999
cut_abseta = 0:1.5

[npv_cr_MC:npv_cr_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_02_cutset.root
title = Z+jets

[njets_cr_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_02_cutset.root
var = njets_asymm
binning = -0.5:0.5:1.5:2.5:3.5:4.5:8.5
pass_bit = passing_id
xtitle = Number of Jets
title = Data
cut_pt = 30:999999
cut_abseta = 0:1.5

[njets_cr_MC:njets_cr_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_02_cutset.root
title = Z+jets

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;	iso efficiencies
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[scale_factor_pt_iso]
data = pt_iso_Data
mc = pt_iso_MC
xtitle = p_{T} (GeV/c)
ytitle = Electron Isolation Scale Factor
title = 
ymax = 1.05
ymin = 0.9
vlines = 30.

[scale_factor_npv_iso]
data = npv_iso_Data
mc = npv_iso_MC
xtitle = Number of Primary Vertices
ytitle = Electron Isolation Scale Factor
title = 
ymax = 1.05
ymin = 0.9

[scale_factor_eta_iso]
data = eta_iso_Data
mc = eta_iso_MC
xtitle = \#eta Supercluster
ytitle = Electron Isolation Scale Factor
title = 
ymax = 1.05
ymin = 0.9
vlines = -1.5660:-1.4442:1.4442:1.5660

[scale_factor_njets_iso]
data = njets_iso_Data
mc = njets_iso_MC
xtitle = Number of Jets
ytitle = Electron Isolation Scale Factor
title = 
ymax = 1.05
ymin = 0.9


;;;;;;;;;; pt ;;;;;;;;;;;;;;;;;;;;

[pt_iso_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = pt
binning = 0:5:10:15:20:25:30:35:40:45:50:55:60:65:80:100:200
pass_bit = passing_iso
xtitle = p_{T} (GeV/c)
title = Data
cut_abseta = 0:1.5

[pt_iso_MC:pt_iso_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = Z+jets

[eta_iso_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = sc_eta
binning = -2.5:-2.0:-1.5660:-1.4442:-1.0:-0.5:0:0.5:1.0:1.4442:1.5660:2.0:2.5
pass_bit = passing_iso
xtitle = \#eta Supercluster
title = Data
cut_pt = 30:999999

[eta_iso_MC:eta_iso_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
var = sc_eta
title = Z+jets
cut_pt = 30:999999

[npv_iso_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = npvertices
binning = -0.5:0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
pass_bit = passing_iso
xtitle = Number of Primary Vertices
title = Data
cut_pt = 30:999999
cut_abseta = 0:1.5

[npv_iso_MC:npv_iso_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = Z+jets

[njets_iso_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = njets_asymm
binning = -0.5:0.5:1.5:2.5:3.5:4.5:8.5
pass_bit = passing_iso
xtitle = Number of Jets
title = Data
cut_pt = 30:999999
cut_abseta = 0:1.5

[njets_iso_MC:njets_iso_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = Z+jets

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; npv iso *without* EA corrections
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[pt_iso_Data_noEA]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_03_cutset.root
var = pt
binning = 0:5:10:15:20:25:30:35:40:45:50:55:60:65:80:100:200
pass_bit = passing_iso
xtitle = p_{T} (GeV/c)
title = Data (no EA corrections)
cut_abseta = 0:1.5

[pt_iso_MC_noEA:pt_iso_Data_noEA]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_03_cutset.root
title = Z+jets (no EA corrections)

[eta_iso_Data_noEA]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_03_cutset.root
var = sc_eta
binning = -2.5:-2.0:-1.5660:-1.4442:-1.0:-0.5:0:0.5:1.0:1.4442:1.5660:2.0:2.5
pass_bit = passing_iso
xtitle = \#eta Supercluster
title = Data (no EA corrections)
cut_pt = 30:999999

[eta_iso_MC_noEA:eta_iso_Data_noEA]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_03_cutset.root
var = sc_eta
title = Z+jets (no EA corrections)
cut_pt = 30:999999

[npv_iso_Data_noEA:npv_iso_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_03_cutset.root
title = Data (no EA corrections)

[npv_iso_MC_noEA:npv_iso_Data_noEA]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_03_cutset.root
title = Z+jets (no EA corrections)

[njets_iso_Data_noEA]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_03_cutset.root
var = njets_asymm
binning = -0.5:0.5:1.5:2.5:3.5:4.5:8.5
pass_bit = passing_iso
xtitle = Number of Jets
title = Data
cut_pt = 30:999999
cut_abseta = 0:1.5

[njets_iso_MC_noEA:njets_iso_Data_noEA]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_03_cutset.root
title = Z+jets

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;	trigger efficiencies
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[scale_factor_pt_idx]
data = pt_idx_Data
mc = pt_idx_MC
xtitle = p_{T} (GeV/c)
ytitle = Electron Trigger Scale Factor
title = 
ymax = 1.05
ymin = 0.9
vlines = 30.

[scale_factor_npv_idx]
data = npv_idx_Data
mc = npv_idx_MC
xtitle = Number of Primary Vertices
ytitle = Electron Trigger Scale Factor
title = 
ymax = 1.05
ymin = 0.9

[scale_factor_eta_idx]
data = eta_idx_Data
mc = eta_idx_MC
xtitle = \#eta Supercluster
ytitle = Electron Trigger Scale Factor
title = 
ymax = 1.05
ymin = 0.9
vlines = -1.5660:-1.4442:1.4442:1.5660

[scale_factor_njets_idx]
data = njets_idx_Data
mc = njets_idx_MC
xtitle = Number of Jets
ytitle = Electron Trigger Scale Factor
title = 
ymax = 1.05
ymin = 0.9


;;;;;;;;;; pt ;;;;;;;;;;;;;;;;;;;;

[pt_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = pt
binning = 20:25:30:35:40:45:50:55:60:65:80:100:200
pass_bit = passing_idx
xtitle = p_{T} (GeV/c)
title = Data
cut_abseta = 0:1.5

[pt_idx_MC:pt_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = Z+jets

[eta_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = sc_eta
binning = -2.5:-2.0:-1.5660:-1.4442:-1.0:-0.5:0:0.5:1.0:1.4442:1.5660:2.0:2.5
pass_bit = passing_idx
xtitle = \#eta Supercluster
title = Data
cut_pt = 30:999999

[eta_idx_MC:eta_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
var = sc_eta
title = Z+jets
cut_pt = 30:999999

[npv_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = npvertices
binning = -0.5:0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5:11.5:12.5:13.5:14.5:15.5:16.5:17.5:18.5:19.5:20.5
pass_bit = passing_idx
xtitle = Number of Primary Vertices
title = Data
cut_pt = 30:999999
cut_abseta = 0:1.5

[npv_idx_MC:npv_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = Z+jets

[njets_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = njets_asymm
binning = -0.5:0.5:1.5:2.5:3.5:4.5:8.5
pass_bit = passing_idx
xtitle = Number of Jets
title = Data
cut_pt = 30:999999
cut_abseta = 0:1.5

[njets_idx_MC:njets_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = Z+jets

;;;;;;<<<<<<<<<<<<<<<<<<<<<<<<<<<
;;;;;; pt all menus
;;;;;;<<<<<<<<<<<<<<<<<<<<<<<<<<<

[pt_idx_Data1e33:pt_idx_Data]
title = 1e33 menu
cut_run = 165088:167913

[pt_idx_Data2e33:pt_idx_Data]
title = 2e33 menu
cut_run = 170249:173198

[pt_idx_Data3e33:pt_idx_Data]
title = 3e33 menu
cut_run = 173236:178380

[pt_idx_Data5e33:pt_idx_Data]
title = 5e33 menu
cut_run = 178381:180252

[pt_l1_Data1e33:pt_idx_Data]
pass_bit = passing_l1
title = 1e33 menu
cut_run = 165088:167913

[pt_l1_Data2e33:pt_idx_Data]
pass_bit = passing_l1
title = 2e33 menu
cut_run = 170249:173198

[pt_l1_Data3e33:pt_idx_Data]
pass_bit = passing_l1
title = 3e33 menu
cut_run = 173236:178380

[pt_l1_Data5e33:pt_idx_Data]
pass_bit = passing_l1
title = 5e33 menu
cut_run = 178381:180252

[pt_hlt_Data1e33:pt_idx_Data]
pass_bit = passing_hltwrtl1
title = 1e33 menu
cut_run = 165088:167913

[pt_hlt_Data2e33:pt_idx_Data]
pass_bit = passing_hltwrtl1
title = 2e33 menu
cut_run = 170249:173198

[pt_hlt_Data3e33:pt_idx_Data]
pass_bit = passing_hltwrtl1
title = 3e33 menu
cut_run = 173236:178380

[pt_hlt_Data5e33:pt_idx_Data]
pass_bit = passing_hltwrtl1
title = 5e33 menu
cut_run = 178381:180252

;;;;;;<<<<<<<<<<<<<<<<<<<<<<<<<<<
;;;;;; eta all menus
;;;;;;<<<<<<<<<<<<<<<<<<<<<<<<<<<

[eta_idx_Data1e33:eta_idx_Data]
title = 1e33 menu
cut_run = 165088:167913

[eta_idx_Data2e33:eta_idx_Data]
title = 2e33 menu
cut_run = 170249:173198

[eta_idx_Data3e33:eta_idx_Data]
title = 3e33 menu
cut_run = 173236:178380

[eta_idx_Data5e33:eta_idx_Data]
title = 5e33 menu
cut_run = 178381:180252

[eta_l1_Data1e33:eta_idx_Data]
pass_bit = passing_l1
title = 1e33 menu
cut_run = 165088:167913

[eta_l1_Data2e33:eta_idx_Data]
pass_bit = passing_l1
title = 2e33 menu
cut_run = 170249:173198

[eta_l1_Data3e33:eta_idx_Data]
pass_bit = passing_l1
title = 3e33 menu
cut_run = 173236:178380

[eta_l1_Data5e33:eta_idx_Data]
pass_bit = passing_l1
title = 5e33 menu
cut_run = 178381:180252

[eta_hlt_Data1e33:eta_idx_Data]
pass_bit = passing_hltwrtl1
title = 1e33 menu
cut_run = 165088:167913

[eta_hlt_Data2e33:eta_idx_Data]
pass_bit = passing_hltwrtl1
title = 2e33 menu
cut_run = 170249:173198

[eta_hlt_Data3e33:eta_idx_Data]
pass_bit = passing_hltwrtl1
title = 3e33 menu
cut_run = 173236:178380

[eta_hlt_Data5e33:eta_idx_Data]
pass_bit = passing_hltwrtl1
title = 5e33 menu
cut_run = 178381:180252

;;;;;;<<<<<<<<<<<<<<<<<<<<<<<<<<<
;;;;;; npv all menus
;;;;;;<<<<<<<<<<<<<<<<<<<<<<<<<<<

[npv_idx_Data1e33:npv_idx_Data]
title = 1e33 menu
cut_run = 165088:167913

[npv_idx_Data2e33:npv_idx_Data]
title = 2e33 menu
cut_run = 170249:173198

[npv_idx_Data3e33:npv_idx_Data]
title = 3e33 menu
cut_run = 173236:178380

[npv_idx_Data5e33:npv_idx_Data]
title = 5e33 menu
cut_run = 178381:180252

[npv_l1_Data1e33:npv_idx_Data]
pass_bit = passing_l1
title = 1e33 menu
cut_run = 165088:167913

[npv_l1_Data2e33:npv_idx_Data]
pass_bit = passing_l1
title = 2e33 menu
cut_run = 170249:173198

[npv_l1_Data3e33:npv_idx_Data]
pass_bit = passing_l1
title = 3e33 menu
cut_run = 173236:178380

[npv_l1_Data5e33:npv_idx_Data]
pass_bit = passing_l1
title = 5e33 menu
cut_run = 178381:180252

[npv_hlt_Data1e33:npv_idx_Data]
pass_bit = passing_hltwrtl1
title = 1e33 menu
cut_run = 165088:167913

[npv_hlt_Data2e33:npv_idx_Data]
pass_bit = passing_hltwrtl1
title = 2e33 menu
cut_run = 170249:173198

[npv_hlt_Data3e33:npv_idx_Data]
pass_bit = passing_hltwrtl1
title = 3e33 menu
cut_run = 173236:178380

[npv_hlt_Data5e33:npv_idx_Data]
pass_bit = passing_hltwrtl1
title = 5e33 menu
cut_run = 178381:180252

[run_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = run
binning = 165088:167913:173198:178380:180252
pass_bit = passing_idx
xtitle = CMS run number
title = HLT
cut_pt = 30:999999
cut_abseta = 0:1.5

[run_l1_Data:run_idx_Data]
pass_bit = passing_l1
title = L1

[run_hlt_Data:run_idx_Data]
pass_bit = passing_hltwrtl1
title = HLT/L1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;	trigger 2d
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[pt_eta_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Data_electron_01_cutset.root
var = sc_eta
var2 = pt
binning = -2.5:-2.0:-1.5660:-1.4442:-1.0:-0.5:0:0.5:1.0:1.4442:1.5660:2.0:2.5
binning2 = 30:35:40:45:50:60:80:100
pass_bit = passing_idx
xtitle = Electron Supercluster \#eta
ytitle = Electron p_{T} (GeV)
title = Data
cut_pt = 30:1000
2D_plot = true
ymax = 1.
ymin = 0.75

[pt_eta_idx_MC:pt_eta_idx_Data]
file_name = input/20120913_133655_electron_IDISOTrig_thesis/trigger_tag_and_probe_Zjets_e_background_01_cutset.root
title = MC

[scale_factor_pt_eta]
data = pt_eta_idx_Data
mc = pt_eta_idx_MC
xtitle = Electron Supercluster \#eta
ytitle = Electron p_{T} (GeV)
title = scale factor
zmax = 1.05
zmin = 0.83
2D_plot = true
