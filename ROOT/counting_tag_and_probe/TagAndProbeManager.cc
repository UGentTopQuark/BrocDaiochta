#include "TagAndProbeManager.h"

comhaireamh::TagAndProbeManager::TagAndProbeManager(TFile *outfile)
{
  this->outfile = outfile;
  config_reader = NULL;
}

comhaireamh::TagAndProbeManager::~TagAndProbeManager()
{
  for(std::map<std::string, comhaireamh::TreeReader*>::iterator tree_reader = tree_readers.begin();
      tree_reader != tree_readers.end();
      ++tree_reader){
    delete tree_reader->second;
  }
  tree_readers.clear();
}

void comhaireamh::TagAndProbeManager::run_tag_and_probe()
{
  std::cout<<"Getting trees....\n";
  get_trees_from_files();
  std::cout<<"Configuring Trees...\n";
  configure_trees();
  std::cout<<"Processing Trees.... \n";
  process_trees();
  std::cout<<"Making plots....\n";
  create_plots();
}

void comhaireamh::TagAndProbeManager::create_plots()
{
  outfile->cd();
  // print one variable at a time
 
  for(std::map<std::string, comhaireamh::TreeReader*>::iterator tree_reader = tree_readers.begin();
      tree_reader != tree_readers.end();
      ++tree_reader){
	  
	  std::vector<std::string> *variable_list = tree_reader->second->get_variables();
	  for(std::vector<std::string>::iterator variable = variable_list->begin();
	      variable != variable_list->end();
	      ++variable){
		  
		  if(config_reader->get_bool_var("2D_plot",*variable,false)){
			  CanvasHolder *cholder = new CanvasHolder();
			  cholder->setCanvasTitle(*variable);
			  cholder->setTitleX(config_reader->get_var("var",*variable,true));
			  cholder->setTitleY(config_reader->get_var("var2",*variable,true));
			  std::string title = config_reader->get_var("title", *variable,true);
			  cholder->addHisto2D(tree_reader->second->get_efficiency_graph2D(*variable),title, "COLZtexte");
			  //cholder->addHisto2D(tree_reader->second->get_efficiency_graph2D(*variable),title, "COLZ");
			  double zmin = 0.6;
			  if(config_reader->get_var("zmin", *variable,false) != "")
				  zmin = atof(config_reader->get_var("zmin", *variable,true).c_str());
			  cholder->setBordersZ(zmin,1.001);
			  //cholder->setBordersZ(-0.001,1.001);
			  cholder->setOptStat(000000);
			  cholder->setLegendOptions(0., 0., "LowerRight");
			  cholder->save("png");
			  cholder->write(outfile);
			  delete cholder;
		  }
	  }
  }

	// read requests for overlays from config
	std::vector<std::vector<std::pair<std::string,std::string> > > overlays = get_overlays();

  	bool print_table = config_reader->get_bool_var("print_efficiency_tables","global", false);

	TablePrinter printer;

	// print overlays of variables
	for(std::vector<std::vector<std::pair<std::string,std::string> > >::iterator overlay = overlays.begin();
	    overlay != overlays.end();
	    ++overlay){
	    	if(print_table) std::cout << "------------------------------------------" << std::endl;

	      	CanvasHolder *cholder = new CanvasHolder();
	      	cholder->addHLine(1,"",2);
	      	cholder->setOptStat(000000);
	      	
	      	std::string ctitle = "overlay";
	      	bool first_graph = true;

		std::vector<double> vlines;

	      	for(std::vector<std::pair<std::string,std::string> >::iterator variable = overlay->begin();
	      	    variable != overlay->end();
	      	    ++variable){
		    if(variable->second.find("vlines") != std::string::npos){
		    		std::vector<std::string> lines = config_reader->get_vec_var("vlines", variable->second);	
				for(std::vector<std::string>::iterator line = lines.begin(); line != lines.end(); ++line){
					vlines.push_back(atof(line->c_str()));
				}
				continue;
		    }
	      	  if(first_graph){
		      		cholder->setTitleY(config_reader->get_var("ytitle", variable->second));
				std::string title = config_reader->get_var("title", variable->second,true);
				cholder->addGraph(tree_readers[variable->first]->get_efficiency_graph(variable->second), title, title, "APz");
		  		if(print_table) printer.print_header(tree_readers[variable->first]->get_efficiency_graph(variable->second), config_reader->get_var("xtitle", variable->second,true));
				cholder->setTitleX(config_reader->get_var("xtitle", variable->second,true));
				cholder->setBordersY(atof(config_reader->get_var("ymin", variable->second,true).c_str()), atof(config_reader->get_var("ymax", variable->second,true).c_str()));
				cholder->setLumi(atof(config_reader->get_var("lumi", variable->second,true).c_str()));
				cholder->setDescription(config_reader->get_var("description", variable->second,false).c_str());
				first_graph = false;
	      		}else{
	      			std::string title = config_reader->get_var("title", variable->second,true);
	      			cholder->addGraph(tree_readers[variable->first]->get_efficiency_graph(variable->second), title, title, "Pz");
	      		}

		  	if(print_table) printer.print_row(tree_readers[variable->first]->get_efficiency_graph(variable->second), config_reader->get_var("title", variable->second,true));

	      		ctitle = variable->second+"_"+ctitle;
		}
	    	if(print_table) std::cout << "------------------------------------------" << std::endl;
	    
		cholder->setCanvasTitle(ctitle);
  
		//std::vector<int> styles;
		//styles.push_back(1);
		//styles.push_back(2);
		//
		for(std::vector<double>::iterator line = vlines.begin(); line != vlines.end(); ++line){
			cholder->addVLine(*line);
		}
		
		//cholder->setLineStylesGraph(styles);
		cholder->setMarkerSizeGraph(1.2);
		cholder->setLineSizeGraph(2.5);
		cholder->setLegendOptions(0.40, 0.25, "LowerRight");
		//cholder->setLegendOptions(0.40, 0.14, "LowerRight");
		cholder->save("png");
		cholder->write(outfile);
		
		delete cholder;
	}

	produce_sf_plots();
	produce_sf_plots2D();
}

void comhaireamh::TagAndProbeManager::produce_sf_plots()
{
  	bool print_table = config_reader->get_bool_var("print_sf_tables","global", false);
	std::vector<std::string> sf_sections = get_scale_factor_requests();

	TablePrinter printer;
	comhaireamh::ScaleFactorPrinter sf_printer;

	for(std::vector<std::string>::iterator sf_sec = sf_sections.begin();
		sf_sec != sf_sections.end();
		++sf_sec){
		  if(config_reader->get_bool_var("2D_plot",*sf_sec,false)){
			  continue;}

    		CanvasHolder *cholder = new CanvasHolder();
    		cholder->addHLine(1,"",2);
    		cholder->setOptStat(000000);

		std::cout << "sf 1D: " << *sf_sec << std::endl;

		std::string data_ident = config_reader->get_var("data", *sf_sec, true);
		std::string data_graph_file = config_reader->get_var("file_name", data_ident);
		TGraphAsymmErrors *data_graph = tree_readers[data_graph_file]->get_efficiency_graph(data_ident);
		std::string mc_ident = config_reader->get_var("mc", *sf_sec, true);
		std::string mc_graph_file = config_reader->get_var("file_name", mc_ident);
		TGraphAsymmErrors *mc_graph = tree_readers[mc_graph_file]->get_efficiency_graph(mc_ident);

		Double_t *x = data_graph->GetX();
		Double_t *x_h = data_graph->GetEXhigh();
		Double_t *x_l = data_graph->GetEXlow();
	
		int nbins = data_graph->GetN();
	
		double y[nbins];
		double y_h[nbins];
		double y_l[nbins];
	
		Double_t *y1 = data_graph->GetY();
		Double_t *y1_h = data_graph->GetEYhigh();
		Double_t *y1_l = data_graph->GetEYlow();
	
		Double_t *y2 = mc_graph->GetY();
		Double_t *y2_h = mc_graph->GetEYhigh();
		Double_t *y2_l = mc_graph->GetEYlow();

		double average_scale_factor = 0;
		double sum_of_weights = 0;
	
		for(int i = 0; i < nbins; ++i){
			y[i] = y1[i]/y2[i];
			y_h[i] = sqrt((1./y2[i]*y1_h[i]*y1_h[i])+(y1[i]*y1[i]/(y2[i]*y2[i]*y2[i]*y2[i])*y2_h[i]*y2_h[i]));
			y_l[i] = sqrt((1./y2[i]*y1_l[i]*y1_l[i])+(y1[i]*y1[i]/(y2[i]*y2[i]*y2[i]*y2[i])*y2_l[i]*y2_l[i]));

			average_scale_factor += y[i]/(y_h[i]+y_l[i]);
			sum_of_weights += 1./(y_h[i]+y_l[i]);
		}
		//average_scale_factor = tree_readers[data_graph_file]->get_efficiency(data_ident)/tree_readers[mc_graph_file]->get_efficiency(mc_ident);
		double average_scale_factor_eff = tree_readers[data_graph_file]->get_efficiency(data_ident)/tree_readers[mc_graph_file]->get_efficiency(mc_ident);
		std::cout << "ave scale factor eff: " << average_scale_factor_eff << std::endl;
		average_scale_factor /= sum_of_weights;
		//average_scale_factor /= (double) nbins;
	
		TGraphAsymmErrors *sf_graph = new TGraphAsymmErrors(nbins, x, y, x_l, x_h, y_l, y_h);
		  	if(print_table) printer.print_row(sf_graph, config_reader->get_var("title", *sf_sec,true));

	    	cholder->setTitleY(config_reader->get_var("ytitle", *sf_sec));
		std::string title = config_reader->get_var("title", *sf_sec, false);
		cholder->addGraph(sf_graph, title, title, "APz");
		cholder->setTitleX(config_reader->get_var("xtitle", *sf_sec));
		cholder->setBordersY(atof(config_reader->get_var("ymin", *sf_sec).c_str()), atof(config_reader->get_var("ymax", *sf_sec).c_str()));
		cholder->setLumi(atof(config_reader->get_var("lumi", *sf_sec).c_str()));
		cholder->setDescription(config_reader->get_var("description", *sf_sec, false).c_str());
    		cholder->setMarkerSizeGraph(1.2);
    		cholder->setLineSizeGraph(2.5);
    		cholder->setLegendOptions(0.40, 0.14, "LowerRight");

		std::cout << "ave scale factor: " << average_scale_factor << std::endl;
    		cholder->addHLine(average_scale_factor_eff,"average scale factor",2,4,2);

		std::vector<std::string> lines = config_reader->get_vec_var("vlines", *sf_sec, false);
		for(std::vector<std::string>::iterator line = lines.begin(); line != lines.end(); ++line){
			cholder->addVLine(atof(line->c_str()));
		}
    		
		std::string ctitle = "scale_factor_"+data_ident+"_"+mc_ident;
		cholder->setCanvasTitle(ctitle);
    		cholder->save("png");
    		cholder->write(outfile);

		 if(print_table) printer.print_latex(sf_graph, title);

		sf_printer.print_cpp_1d_sf(sf_graph, title);
    
		delete cholder;
		delete sf_graph;
	}
}
void comhaireamh::TagAndProbeManager::produce_sf_plots2D()
{
	std::vector<std::string> sf_sections = get_scale_factor_requests();

	comhaireamh::ScaleFactorPrinter sf_printer;

	for(std::vector<std::string>::iterator sf_sec = sf_sections.begin();
		sf_sec != sf_sections.end();
		++sf_sec){
		  if(!config_reader->get_bool_var("2D_plot",*sf_sec,false)){
			  continue;}
    		CanvasHolder *cholder = new CanvasHolder();
    		//cholder->addHLine(1,"",2);
    		cholder->setOptStat(000000);

		std::cout << "sf 2D: " << *sf_sec << std::endl;

		std::string data_ident = config_reader->get_var("data", *sf_sec, true);
		std::string data_histo_file = config_reader->get_var("file_name", data_ident);
		TH2D *data_histo = tree_readers[data_histo_file]->get_efficiency_graph2D(data_ident);
		std::string mc_ident = config_reader->get_var("mc", *sf_sec, true);
		std::string mc_histo_file = config_reader->get_var("file_name", mc_ident);
		TH2D *mc_histo = tree_readers[mc_histo_file]->get_efficiency_graph2D(mc_ident);

		int nbinsX = data_histo->GetXaxis()->GetNbins();
		int nbinsY = data_histo->GetYaxis()->GetNbins();
		
		TH2D *sf_histo = new TH2D(*data_histo);
		for(int i = 1; i < nbinsX+1; ++i){
			for(int j = 1; j < nbinsY+1; ++j){
				double eff_d = data_histo->GetBinContent(i,j);
				double err_d = data_histo->GetBinError(i,j);
				double eff_mc = mc_histo->GetBinContent(i,j);
				double err_mc = mc_histo->GetBinError(i,j);

				if(eff_mc != 0){
					double sf = eff_d/eff_mc;
					double err_sf = sf*sqrt((err_d/eff_d)*(err_d/eff_d)+(err_mc/eff_mc)*(err_mc/eff_mc));
					sf_histo->SetBinContent(i,j,sf);
					sf_histo->SetBinError(i,j,err_sf);
				}
			}
		}
		cholder->setTitleX(config_reader->get_var("xtitle",data_ident,true));
		cholder->setTitleY(config_reader->get_var("ytitle",data_ident,true));
		std::string title = config_reader->get_var("title", *sf_sec,true);
		cholder->addHisto2D(sf_histo,title, "COLZtext");

		sf_printer.print_cpp_2d_sf(sf_histo, *sf_sec);
	

		cholder->setBordersZ(atof(config_reader->get_var("zmin", *sf_sec).c_str()),atof(config_reader->get_var("zmax", *sf_sec).c_str()));
		cholder->setLumi(atof(config_reader->get_var("lumi", *sf_sec).c_str()));
		cholder->setDescription(config_reader->get_var("description", *sf_sec, false).c_str());

		std::string ctitle = "scale_factor_"+data_ident+"_"+mc_ident;
    		cholder->setOptStat(000000);
		cholder->setCanvasTitle(ctitle);
		cholder->setLegDraw(false);
    		cholder->save("png");
    		cholder->write(outfile);
    
		delete cholder;
		delete sf_histo;
	}
}


std::vector<std::string> comhaireamh::TagAndProbeManager::get_scale_factor_requests()
{
	std::vector<std::string> *sections = config_reader->get_sections();

	std::vector<std::string> sf_sections; 

	for(std::vector<std::string>::iterator section = sections->begin();
	    section != sections->end();
	    ++section){
		if(section->find("scale_factor") != std::string::npos){
			sf_sections.push_back(*section);	
		}
	}

	return sf_sections;
}

std::vector<std::vector<std::pair<std::string,std::string> > > comhaireamh::TagAndProbeManager::get_overlays()
{
  std::vector<std::string> global_vars = config_reader->get_variables_for_section("global");
  
  // list of overlays: list of pairs of <file, variable> that should be overlaid
  std::vector<std::vector<std::pair<std::string,std::string> > > overlays;
  
  // prepare information about what to overlay for later use
  for(std::vector<std::string>::iterator var = global_vars.begin();
      var != global_vars.end();
      ++var){
    if(var->find("overlay") != std::string::npos){
      std::vector<std::string> vars_to_overlay = config_reader->get_vec_var(*var,"global");
      overlays.push_back(std::vector<std::pair<std::string,std::string> >());
      for(std::vector<std::string>::iterator var_to_overlay = vars_to_overlay.begin();
	  var_to_overlay != vars_to_overlay.end();
	  ++var_to_overlay){
	// fill to last element in vector
	if(var_to_overlay->find("vlines") == std::string::npos){
		overlays[overlays.size()-1].push_back(std::pair<std::string,std::string>(config_reader->get_var("file_name", *var_to_overlay, true), *var_to_overlay));
	}else{
		overlays[overlays.size()-1].push_back(std::pair<std::string,std::string>("", *var_to_overlay));
	}
      }
    }
  }
  
  return overlays;
}

void comhaireamh::TagAndProbeManager::configure_trees()
{
  // for each tree create all variables for sections
  for(std::map<std::string, comhaireamh::TreeReader*>::iterator tree_reader = tree_readers.begin();
      tree_reader != tree_readers.end();
      ++tree_reader){
    tree_reader->second->configure_variables();
  }
}

void comhaireamh::TagAndProbeManager::process_trees()
{
  // process all events within a tree
  for(std::map<std::string, comhaireamh::TreeReader*>::iterator tree_reader = tree_readers.begin();
      tree_reader != tree_readers.end();
      ++tree_reader){
    tree_reader->second->read();
  }
}

TGraphAsymmErrors* comhaireamh::TagAndProbeManager::get_scale_factor_graph(TGraphAsymmErrors *graph1, TGraphAsymmErrors *graph2)
{
	Double_t *x = graph1->GetX();
	Double_t *x_h = graph1->GetEXhigh();
	Double_t *x_l = graph1->GetEXlow();

	int nbins = graph1->GetN();

	double y[nbins];
	double y_h[nbins];
	double y_l[nbins];

	Double_t *y1 = graph1->GetY();
	Double_t *y1_h = graph1->GetEYhigh();
	Double_t *y1_l = graph1->GetEYlow();

	Double_t *y2 = graph2->GetY();
	Double_t *y2_h = graph2->GetEYhigh();
	Double_t *y2_l = graph2->GetEYlow();

	for(int i = 0; i < nbins; ++i){
		y[i] = y1[i]/y2[i];
		y_h[i] = sqrt((1./y2[i]*y1_h[i]*y1_h[i])+(y1[i]*y1[i]/(y2[i]*y2[i]*y2[i]*y2[i])*y2_h[i]*y2_h[i]));
		y_l[i] = sqrt((1./y2[i]*y1_l[i]*y1_l[i])+(y1[i]*y1[i]/(y2[i]*y2[i]*y2[i]*y2[i])*y2_l[i]*y2_l[i]));
	}

	TGraphAsymmErrors *graph = new TGraphAsymmErrors(nbins, x, y, x_l, x_h, y_l, y_h);

	return graph;
}

void comhaireamh::TagAndProbeManager::get_trees_from_files()
{
  std::vector<std::string> *sections = config_reader->get_sections();
  
  std::string filename_var = "file_name";
  // loop over all sections and make sure all root files are opened
  for(std::vector<std::string>::iterator section = sections->begin();
      section != sections->end();
      ++section){
  	std::string tree_name = config_reader->get_var("tree_name", *section, true);
  	std::string dir_name = config_reader->get_var("dir_name", *section, true);
  
    if(*section != "global" && (section->find("scale_factor") == std::string::npos) && (section->find("vlines") == std::string::npos)){
      std::string filename = config_reader->get_var(filename_var, *section, true);
      
      // if this is a new tree, book a TreeReader
      if(tree_readers.find(filename) == tree_readers.end()){
	TFile *infile = new TFile(filename.c_str(), "OPEN");
	open_files.push_back(infile);
	comhaireamh::TreeReader *tree_reader = new comhaireamh::TreeReader(outfile);
	tree_reader->set_tree((TTree*) infile->GetDirectory(dir_name.c_str())->Get(tree_name.c_str()));
	tree_reader->set_config_reader(config_reader);
	tree_readers[filename] = tree_reader;
      }
      
      // add variable to TreeReader for new sections
      tree_readers[filename]->add_section(*section);
    }
  }
}
