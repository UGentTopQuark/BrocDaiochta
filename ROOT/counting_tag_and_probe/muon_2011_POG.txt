[global]
lumi = -1	# if not set to -1 label with int. lumi will be added to plots
outfile = outfile.root	# all plots are also written to this file

overlay_1 = pt_Mu15:pt_Mu15_2010:pt_Mu20:pt_IsoMu17
overlay_2 = eta_Mu15:eta_Mu15_2010:eta_Mu20:eta_IsoMu17
overlay_3 = eta_Mu15_L1:eta_Mu15_2010_L1:eta_Mu20_L1
overlay_4 = eta_Mu15_HLT:eta_Mu15_2010_HLT:eta_Mu20_HLT:eta_IsoMu17_HLT
overlay_5 = eta_Mu15_HLT:eta_Mu15_2010_HLT:eta_Mu15_Spring11_HLT
overlay_51 = eta_Mu15_L1:eta_Mu15_2010_L1:eta_Mu15_Spring11_L1
overlay_6 = eta_IsoMu17_HLT:eta_IsoMu17_Spring11_HLT
overlay_7 = npv_Mu15:npv_IsoMu17:npv_Mu15_2010:npv_IsoMu17_Spring11
overlay_8 = eta_IsoMu17_off_json:eta_IsoMu17_priv_json
overlay_9 = pt_IsoMu17_off_json:pt_IsoMu17_priv_json

ymax = 1.0
ymin = 0.0
ytitle = events
tree_name = fitter_tree	# tree name in input file
dir_name = lepEffs	# directory which contains tree in input file

[pt_Mu15]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu15_pt0.root
var = pt
binning = 0:10:20:30:40:60:100
title = HLT_Mu15
pass_bit = passing_idx
xtitle = p_{T}
cut_eta = -2.1:2.1
cut_reliso = 0:0.05

[pt_Mu20:pt_Mu15]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu20_pt0.root
title = HLT_Mu20
cut_eta = -2.1:2.1
cut_reliso = 0:0.05

[pt_IsoMu17:pt_Mu15]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/isomu17_pt0.root
title = HLT_IsoMu17
cut_eta = -2.1:2.1
cut_reliso = 0:0.05

[pt_IsoMu17_off_json:pt_Mu15]
file_name = muon_pog_meeting/20110406_175234_mu_tp_2011_off_json/isomu17_pt0.root
title = < 161216
cut_eta = -2.1:2.1
cut_reliso = 0:0.05

[pt_IsoMu17_priv_json:pt_Mu15]
file_name = muon_pog_meeting/20110406_175307_mu_tp_2011_priv_json/isomu17_pt0.root
title = > 161216
cut_eta = -2.1:2.1
cut_reliso = 0:0.05

[pt_Mu15_2010:pt_Mu15]
file_name = muon_pog_meeting/20110406_175731_mu_tp_2010/mu15_mc_pt0.root
title = HLT_Mu15 (2010)
cut_eta = -2.1:2.1
cut_reliso = 0:0.05

[eta_Mu15]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu15_pt0.root
var = eta
binning = -2.4:-2.1:-1.2:-0.9:0:0.9:1.2:2.1:2.4
pass_bit = passing_idx
xtitle = \#eta
title = HLT_Mu15
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05
 
[eta_Mu20:eta_Mu15]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu20_pt0.root
title = HLT_Mu20
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_IsoMu17:eta_Mu15]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/isomu17_pt0.root
title = HLT_IsoMu17
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_Mu15_2010:eta_Mu15]
file_name = muon_pog_meeting/20110406_175731_mu_tp_2010/mu15_mc_pt0.root
title = HLT_Mu15 (2010)
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_Mu15_L1]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu15_pt0.root
var = eta
binning = -2.4:-2.1:-1.2:-0.9:0:0.9:1.2:2.1:2.4
pass_bit = passing_l1
xtitle = \#eta
title = L1_SingleMu10
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_Mu20_L1:eta_Mu15_L1]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu20_pt0.root
title = L1_SingleMu12
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_IsoMu17_L1:eta_Mu15_L1]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/isomu17_pt0.root
title = L1_SingleMu10
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_Mu15_2010_L1:eta_Mu15_L1]
file_name = muon_pog_meeting/20110406_175731_mu_tp_2010/mu15_mc_pt0.root
title = L1_SingleMu7 (2010)
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_Mu15_Spring11_L1:eta_Mu15_L1]
file_name = muon_pog_meeting/20110406_175425_mu_tp_Spring11/mu15_mc_pt0.root
title = L1_SingleMu7 (Spring11)
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_Mu15_HLT]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu15_pt0.root
var = eta
binning = -2.4:-2.1:-1.2:-0.9:0:0.9:1.2:2.1:2.4
pass_bit = passing_hltwrtl1
xtitle = \#eta
title = HLT_Mu15/L1
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_IsoMu17_off_json:eta_Mu15]
file_name = muon_pog_meeting/20110406_175234_mu_tp_2011_off_json/isomu17_pt0.root
title = < 161216
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_IsoMu17_priv_json:eta_Mu15]
file_name = muon_pog_meeting/20110406_175307_mu_tp_2011_priv_json/isomu17_pt0.root
title = > 161216
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_Mu20_HLT:eta_Mu15_HLT]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu20_pt0.root
title = HLT_Mu20/L1
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_IsoMu17_HLT:eta_Mu15_HLT]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/isomu17_pt0.root
title = HLT_IsoMu17/L1
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_Mu15_2010_HLT:eta_Mu15_HLT]
file_name = muon_pog_meeting/20110406_175731_mu_tp_2010/mu15_mc_pt0.root
title = HLT_Mu15 (2010)/L1
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_Mu15_Spring11_HLT:eta_Mu15_HLT]
file_name = muon_pog_meeting/20110406_175425_mu_tp_Spring11/mu15_mc_pt0.root
title = HLT_Mu15 Spring11/L1
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[eta_IsoMu17_Spring11_HLT:eta_IsoMu17_HLT]
file_name = muon_pog_meeting/20110406_175425_mu_tp_Spring11/isomu17_mc_pt0.root
title = HLT_IsoMu17 Spring11/L1
cut_eta = -2.4:2.4
cut_pt = 20:1000
cut_reliso = 0:0.05

[npv_Mu15]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu15_pt0.root
var = npvertices
binning = 0.5:1.5:2.5:3.5:4.5:5.5:6.5:7.5:8.5:9.5:10.5
title = HLT_Mu15
pass_bit = passing_idx
xtitle = \# primary vertices
cut_eta = -2.1:2.1
cut_pt = 20:1000
cut_reliso = 0:0.05

[npv_Mu20:npv_Mu15]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/mu20_pt0.root
title = HLT_Mu20
cut_eta = -2.1:2.1
cut_pt = 20:1000
cut_reliso = 0:0.05

[npv_IsoMu17:npv_Mu15]
file_name = muon_pog_meeting/20110406_175339_mu_tp_off_priv_json/isomu17_pt0.root
title = HLT_IsoMu17
cut_eta = -2.1:2.1
cut_pt = 20:200
cut_reliso = 0:0.05

[npv_Mu15_2010:npv_Mu15]
file_name = muon_pog_meeting/20110406_175731_mu_tp_2010/mu15_mc_pt0.root
title = HLT_Mu15 (2010)
cut_eta = -2.1:2.1
cut_pt = 20:200
cut_reliso = 0:0.05

[npv_IsoMu17_Spring11:npv_Mu15]
file_name = muon_pog_meeting/20110406_175425_mu_tp_Spring11/isomu17_mc_pt0.root
title = HLT_IsoMu17 (Spring11)
cut_eta = -2.1:2.1
cut_pt = 20:200
cut_reliso = 0:0.05
