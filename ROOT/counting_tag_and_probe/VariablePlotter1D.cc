#include "VariablePlotter1D.h"

comhaireamh::VariablePlotter1D::VariablePlotter1D(std::string identifier, eire::ConfigReader *config_reader, TTree *tree, TFile *outfile, comhaireamh::VariableHolder *var_holder)
{
	this->identifier = identifier;
	this->outfile = outfile;
	this->var_holder = var_holder;


	var_name = config_reader->get_var("var", identifier, true);
	integrate_efficiency = config_reader->get_bool_var("integrate", identifier, false);
	input_file_name = config_reader->get_var("file_name", identifier, true);
	pass_bit_name = config_reader->get_var("pass_bit", identifier, true);
	std::vector<std::string> binning = config_reader->get_vec_var("binning", identifier, true);

	compensate_prescale = config_reader->get_var("prescale_variable", identifier, false);
	
	// prepare binning for histograms; allows asymmetric binning
	int nbins = binning.size();
	double bins[nbins];
	int i = 0;
	for(std::vector<std::string>::iterator bin = binning.begin();
	    bin != binning.end();
	    ++bin){
	  bins[i] = atof(bin->c_str());
	  ++i;
	}

	x_min_ = 0, x_max_ = 0;
	if(nbins >= 2){ x_min_ = bins[0]; x_max_ = bins[nbins-1]; };
	
	std::string histo_title = config_reader->get_var("title", identifier);
	std::string histo_name = identifier;
	
	outfile->cd();
	/*
	 * Book histograms for probes that pass/fail trigger matching and for all probes.
	 * The efficiency is calculated by pass_histo/all_histo.
	 */
	all_histo = new TH1D((histo_name+"_all").c_str(), (histo_name+" All").c_str(), nbins-1, bins);
	all_histo->SetXTitle(config_reader->get_var("xtitle", identifier).c_str());
	all_histo->SetYTitle(config_reader->get_var("ytitle", identifier).c_str());
	all_histo->Sumw2();
	pass_histo = new TH1D((histo_name+"_pass").c_str(), (histo_name+" Pass").c_str(), nbins-1, bins);
	pass_histo->SetXTitle(config_reader->get_var("xtitle", identifier).c_str());
	pass_histo->SetYTitle(config_reader->get_var("ytitle", identifier).c_str());
	pass_histo->Sumw2();
	fail_histo = new TH1D((histo_name+"_fail").c_str(), (histo_name+" Fail").c_str(), nbins-1, bins);
	fail_histo->SetXTitle(config_reader->get_var("xtitle", identifier).c_str());
	fail_histo->SetYTitle(config_reader->get_var("ytitle", identifier).c_str());
	fail_histo->Sumw2();
	
	int_all_histo = NULL; // integrated histogram
	int_pass_histo = NULL; // integrated histogram
	
	int_all_histo = new TH1D((histo_name+"_all_int").c_str(), (histo_name+" All Integrated").c_str(), nbins-1, bins);
	int_all_histo->SetXTitle(config_reader->get_var("xtitle", identifier).c_str());
	int_all_histo->SetYTitle(config_reader->get_var("ytitle", identifier).c_str());
	int_all_histo->Sumw2();
	int_pass_histo = new TH1D((histo_name+"_pass_int").c_str(), (histo_name+" Pass Integrated").c_str(), nbins-1, bins);
	int_pass_histo->SetXTitle(config_reader->get_var("xtitle", identifier).c_str());
	int_pass_histo->SetYTitle(config_reader->get_var("ytitle", identifier).c_str());
	int_pass_histo->Sumw2();
	
	// efficiency graph to be returned at the end
	efficiency_graph = new TGraphAsymmErrors();
	
	pass_bit_index = -1;
	var_index = -1;
	
	// initialise on-the-fly cuts
	prepare_cuts(config_reader, identifier);
}

void comhaireamh::VariablePlotter1D::prepare_cuts(eire::ConfigReader *config_reader, std::string identifier)
{
  std::vector<std::string> variables = config_reader->get_variables_for_section(identifier);
  
  /*
   * search in configuration section for variables that are of the format:
   * cut_<var name> = <min cut value>:<max cut value>
   */
  for(std::vector<std::string>::iterator var = variables.begin();
      var != variables.end();
      ++var){
    if(var->find("cut_") != std::string::npos){
      std::string cut_variable(*var);
      cut_variable = cut_variable.substr(4,std::string::npos);
      std::vector<std::string> cut_vals = config_reader->get_vec_var(*var,identifier,true);
      if(cut_vals.size() < 2){
	std::cout << "ERROR: cuts must always be of the form cut_<var name> = min:max" << std::endl;
	exit(1);
      }
      double min_cut = atof(cut_vals[0].c_str());
      double max_cut = atof(cut_vals[1].c_str());
      // for each cut_ value create a new Cut object and store it in a vector of Cut objects
      // for each event this vector will be processed and the cuts applied accordingly
      // if an event fails the cut it will not be considered for the efficiency calculation
      comhaireamh::Cut new_cut(cut_variable, min_cut, max_cut);
      var_holder->add_variable(cut_variable);
      cuts.push_back(new_cut);
    }
  }
      var_holder->add_variable("event_weight");
      var_holder->add_variable("passing_event_weight");
      if(compensate_prescale != "") var_holder->add_variable(compensate_prescale);
}

// get variable indices in VariableHolder for each variable that will be used later on
void comhaireamh::VariablePlotter1D::configure()
{
  // variables for plotting
  pass_bit_index = var_holder->get_pass_bit_index(pass_bit_name);
  var_index = var_holder->get_variable_index(var_name);
  weight_index = var_holder->get_variable_index("event_weight");
  passing_weight_index = var_holder->get_variable_index("passing_event_weight");
  if(compensate_prescale != "") prescale_index = var_holder->get_variable_index(compensate_prescale);
  
  // variables for on-the-fly cuts
  for(std::vector<comhaireamh::Cut>::iterator cut = cuts.begin();
      cut != cuts.end();
      ++cut){
    cut->set_var_idx(var_holder->get_variable_index(cut->get_var_name()));	
  }
}

comhaireamh::VariablePlotter1D::~VariablePlotter1D()
{
	if(efficiency_graph){delete efficiency_graph; efficiency_graph = NULL;}
	// if(fail_histo){delete fail_histo; fail_histo = NULL;}
	// if(pass_histo){delete pass_histo; pass_histo = NULL;}
	// if(all_histo){delete all_histo; all_histo = NULL;}
	// if(int_pass_histo){delete int_pass_histo; int_pass_histo = NULL;}
	// if(int_all_histo){delete int_all_histo; int_all_histo = NULL;}
}

void comhaireamh::VariablePlotter1D::integrate_histo(TH1D* ref_histo, TH1D* int_histo)
{
	int nbinsX = ref_histo->GetNbinsX();
	for(int i = 1; i <= nbinsX; ++i){
		double int_for_bin=0;	// integral over histo for certain bin
		for(int j = 1; j <= i; ++j){
			int_for_bin += ref_histo->GetBinContent(j);
		}
		int_histo->SetBinContent(i, int_for_bin);
	}
}

double comhaireamh::VariablePlotter1D::get_efficiency()
{
	return (pass_histo->Integral()/all_histo->Integral());
}

TGraphAsymmErrors *comhaireamh::VariablePlotter1D::get_efficiency_graph()
{
	std::cout << "===========================================" << std::endl;
	std::cout << ">>> var / pass_bit / file: " << var_name << " / " << pass_bit_name << " / " << input_file_name << std::endl;
	std::cout << ">>> efficiency: " << pass_histo->Integral()/all_histo->Integral() << std::endl;
	std::cout << "----------------------" << std::endl;
	std::cout << "pass entries: " << pass_histo->Integral() << std::endl;
	std::cout << "all entries: " << all_histo->Integral() << std::endl;
	std::cout << "----------------------" << std::endl;

	if(integrate_efficiency){
		int_all_histo = new TH1D(*all_histo);
		int_pass_histo = new TH1D(*pass_histo);

		integrate_histo(all_histo, int_all_histo);
		integrate_histo(pass_histo, int_pass_histo);

		efficiency_graph->BayesDivide(int_pass_histo, int_all_histo);
	}else{
		efficiency_graph->BayesDivide(pass_histo, all_histo);
	}

	//calc eff using histos entries, effective entries

//	efficiency_graph->GetXaxis()->SetLimits(x_min_, x_max_);
  
	return efficiency_graph;
}

void comhaireamh::VariablePlotter1D::plot()
{
  int passed = var_holder->get_pass_bit(pass_bit_index);
  double value = var_holder->get_variable(var_index);
  double weight = var_holder->get_variable(weight_index);
  double passing_weight = var_holder->get_variable(passing_weight_index);

  if(passing_weight == 0) passing_weight++;

	if(weight != 1.){
	/*
  	if(value < -1.5){
  		passing_weight *= 1.004;
  	}else if(value < 0.){
  		passing_weight *= 0.995;
  	}else if(value < 1.5){
  		passing_weight *= 0.996;
  	}else if(value < 2.5){
  		passing_weight *= 1.001;
  	}
	*/
  }

  // Check if event passes cuts that are applied on the fly.
  for(std::vector<comhaireamh::Cut>::iterator cut = cuts.begin();
      cut != cuts.end();
      ++cut){
    double cut_var_value = var_holder->get_variable(cut->get_var_idx());
    if(cut_var_value < cut->get_min() || cut_var_value > cut->get_max())
      return;
  }

  if(passed != -1){
    	double prescale = 1;
  	if(compensate_prescale != ""){
		prescale = var_holder->get_variable(prescale_index);
	}
    if(passed > 0){
	    pass_histo->Fill(value, passing_weight*weight);
	    weight /= prescale;
    }
    else{
      weight /= prescale;
      fail_histo->Fill(value, weight);
    }

    // always fill this 
    all_histo->Fill(value, weight);
  }
}
