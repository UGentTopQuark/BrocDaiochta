#ifndef COMHAIREAMH_VARIABLEPLOTTER1D_H
#define COMHAIREAMH_VARIABLEPLOTTER1D_H

/**
 * \class comhaireamh::VariablePlotter1D
 *
 * \brief Plots 1D efficiency curves.
 *
 * For each variable (section in the config file) one VariablePlotter object is
 * booked and manages the plotting of the variables and the efficiency graph
 * calculation.
 *
 * \author bklein
 */

#include "ConfigReader/ConfigReader.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TTree.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TFile.h"
#include "VariableHolder.h"
#include "Cut.h"

namespace comhaireamh{
	class VariablePlotter1D{
		public:
			VariablePlotter1D(std::string identifier, eire::ConfigReader *config_reader, TTree *tree, TFile *outfile, comhaireamh::VariableHolder *var_holder);
			~VariablePlotter1D();
			/// called for each event and plots event
			void plot();
			/// return at the end an efficiency graph for all events
			TGraphAsymmErrors *get_efficiency_graph();
			double get_efficiency();
			/// return the name of the variable that this class processes
			inline std::string get_var_name() { return var_name; };
			/// manage variable assignment with VariableHolder
			void configure();

			double x_min(){ return x_min_; };
			double x_max(){ return x_max_; };
		private:
			void prepare_cuts(eire::ConfigReader *config_reader, std::string identifier);
			void integrate_histo(TH1D* ref_histo, TH1D* int_histo);
			std::string identifier;
			TH1D *all_histo, *pass_histo, *fail_histo, *int_all_histo, *int_pass_histo;
			std::string passing_bit;
			TGraphAsymmErrors *efficiency_graph;
			TFile *outfile;
			std::string input_file_name;
			std::string var_name;
			std::string pass_bit_name;
			int var_index;
			int pass_bit_index;
			int weight_index;
			int passing_weight_index;
			int prescale_index;
			bool integrate_efficiency;
			double x_min_, x_max_;

			std::string compensate_prescale;

			std::vector<comhaireamh::Cut> cuts;

			comhaireamh::VariableHolder *var_holder;
	};
}

#endif
