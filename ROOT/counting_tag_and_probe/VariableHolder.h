#ifndef comhaireamh_VARIABLEHOLDER_H
#define comhaireamh_VARIABLEHOLDER_H

/**
 * \class comhaireamh::VariableHolder
 *
 * \brief Holds all variables for a certain ROOT tree
 *
 * The handling of the variables is a bit tricky in
 * root. A root variable in a file can be assigned to
 * only one variable at a time in the analyser code.
 * Therefore, every time we encounter a new variable,
 * VariableHolder needs to assign it internally to one
 * variable and provide it then for several classes.
 * For example, we want to plot the L1 efficiency and
 * the HLT efficiency in the same file as function of
 * eta. We would create two instances of
 * VariablePlotter and both would need to "share" the
 * same eta variable from the root file.
 *
 * To avoid having to make for each event a string lookup for a certain
 * variable, the different variables are stored in a vector and the index of
 * the position within the vector can be returned by passing the string. In
 * practice this means that ones per run the classes that use VariableHolder
 * can get the indices for the variables they want to lookup later and then per
 * event get the variable information just by passing this index rather than
 * the string name.
 *
 * There are two types of variables stored, variables from the probe muon (eta,
 * pt, etc) and passing indices which store if a given probe muon passed the
 * trigger requirement or not.
 *
 * \author bklein
 */

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include "TTree.h"

#include <cstdlib>


namespace comhaireamh{
	class VariableHolder{
		public:
			VariableHolder();
			~VariableHolder();
			/// add new pass bit
			void add_pass_bit(std::string pass_bit_id, std::string section);
			/// add new variable
			void add_variable(std::string variable);
			/// assign all variables that should be booked from the ROOT tree to the storage system
			void configure_variables(TTree *tree);
			/// get an index to access a certain pass bit for each event
			int get_pass_bit_index(std::string pass_bit);
			/// get an index to access a certain variable for each event
			int get_variable_index(std::string variable);
			/// get a pass bit by accessing it via its index
			double get_pass_bit(int index);
			/// get a variable by accessing it via its index
			double get_variable(int index);

		private:
			std::map<std::string, int> pass_bit_indices;	// pass bit, var names
			std::vector<int> bits;
			std::map<std::string, int> value_indices;	// index of value in vector
			std::vector<Float_t> values;	// numerical values of variables

			int next_value_index;
			int next_pass_bit_index;
	};
}

#endif
