#ifndef comhaireamh_TABLEPRINTER_H
#define comhaireamh_TABLEPRINTER_H

#include "TGraphAsymmErrors.h"
#include <iostream>

namespace comhaireamh{
	class TablePrinter{
		public:
			TablePrinter();
			~TablePrinter();

			void print_header(TGraphAsymmErrors *graph, std::string id);
			void print_row(TGraphAsymmErrors *graph, std::string id);
			void print_latex(TGraphAsymmErrors *graph, std::string id);
		private:
	};
}

#endif
