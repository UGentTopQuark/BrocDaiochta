#include "TablePrinter.h"

comhaireamh::TablePrinter::TablePrinter()
{
}

comhaireamh::TablePrinter::~TablePrinter()
{
}

void comhaireamh::TablePrinter::print_header(TGraphAsymmErrors *graph, std::string id)
{
	int npoints = graph->GetN();
	Double_t* err_l = graph->GetEXlow();
	Double_t* err_h = graph->GetEXhigh();
	Double_t* x = graph->GetX();

	std::cout << id << " | ";
	for(int i = 0; i < npoints; ++i){
		std::cout << x[i]-err_l[i] << "/" << x[i]+err_h[i];
//		std::cout << " |\t-\t|\t+\t"; 
		if(i == npoints-1){
			std::cout << std::endl;
		}else{
			std::cout << " | ";
		}
	}
}

void comhaireamh::TablePrinter::print_row(TGraphAsymmErrors *graph, std::string id)
{
	int npoints = graph->GetN();
	Double_t* y = graph->GetY();
	Double_t* err_l = graph->GetEYlow();
	Double_t* err_h = graph->GetEYhigh();

	std::cout << id << " | ";
	for(int i = 0; i < npoints; ++i){
		printf("%.3f ", y[i]);
//		printf("| %.3f ", err_l[i]);
//		printf("| %.3f ", err_h[i]);
		if(i == npoints-1){
			std::cout << std::endl;
		}else{
			std::cout << " | ";
		}
	}
}

void comhaireamh::TablePrinter::print_latex(TGraphAsymmErrors *graph, std::string id)
{
	int npoints = graph->GetN();
	Double_t* x = graph->GetX();
	Double_t* err_x_l = graph->GetEXlow();
	Double_t* err_x_h = graph->GetEXhigh();

	Double_t* y = graph->GetY();
	Double_t* err_l = graph->GetEYlow();
	Double_t* err_h = graph->GetEYhigh();

	std::cout << id << std::endl;
	std::cout << "bin & value \\" << std::endl;
	for(int i = 0; i < npoints; ++i){
		double x_low = x[i]-err_x_l[i];
		double x_high = x[i]+err_x_h[i];
		std::cout << x_low << "-" << x_high << " & ";
		//printf("$%.3f-%.3f & ", x_low, x_high);

		printf("$%.3f", y[i]);
		printf("^{+%.3f}", err_l[i]);
		printf("_{-%.3f}$", err_h[i]);
		std::cout << "\\\\" << std::endl;
	}
}
