#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include <sstream>
#include <iostream>
#include <vector>
#include <map>
#include <string>

int main(int argc, char **argv)
{
	TFile *outfile = new TFile("test.root", "recreate");
	std::string e_mu = argv[1];
	std::string c_type = argv[2];
	std::string cutset = argv[3];
	std::string version = "_plots";

	std::map<std::string,std::vector<std::string> > files_with_content;

	std::string sig_bars;
	std::string bkg_bars = e_mu+"_background";
	if(e_mu == "e"){
		std::cout << "plotting for electrons..." << std::endl;
		sig_bars = "electron";
	}
	else{
		std::cout << "plotting for muons..." << std::endl;
		sig_bars = "muon";
	}

	std::string sig_id = "TTbar";

	std::vector<std::string> bkg_ids;
	bkg_ids.push_back("Mupt15");
	bkg_ids.push_back("Wjets");
	bkg_ids.push_back("Zjets");
	bkg_ids.push_back("TsChan");
	bkg_ids.push_back("TtChan");
	bkg_ids.push_back("TtWChan");
	
	if((c_type == "s") || (c_type == "c")){// && (cutset != "00_cutset" && cutset != "01_cutset" && cutset != "02_cutset"))){
		std::cout << "plotting signal..." << std::endl;
		files_with_content[sig_id+version+".root"].push_back(sig_id+"|"+sig_bars+"|"+cutset);
		files_with_content[sig_id+version+".root"].push_back(sig_id+"|"+bkg_bars+"|"+cutset);
	}			
	if(c_type == "c" || c_type == "b"){
		std::cout << "plotting background..." << std::endl;
		for(std::vector<std::string>::iterator bkg_id = bkg_ids.begin(); bkg_id != bkg_ids.end();bkg_id++){
			
			files_with_content[*bkg_id+version+".root"].push_back(*bkg_id+"|"+bkg_bars+"|"+cutset);
		}
	}

	std::vector<std::string> histo_names;
	histo_names.push_back("jet1_pt_vs_jet2_pt_");

	std::map<std::string,TFile*> files;

	for(std::map<std::string, std::vector<std::string> >::iterator file_with_content = files_with_content.begin();
	    file_with_content != files_with_content.end();
	    ++file_with_content)
	{
		// open input file from CMSSW analyser
		TFile *file = new TFile(file_with_content->first.c_str(),"open");
		files[file_with_content->first] = file;
	}

	// loop over vector of names of histograms that shall be read out
	// from the CMSSW root files and shall be combined
	for(std::vector<std::string>::iterator histo_name = histo_names.begin();
	    histo_name != histo_names.end();
	    ++histo_name)
		{

			std::cout << " Correlation Factor for: " << *histo_name << std::endl;

			for(std::map<std::string, std::vector<std::string> >::iterator file_with_content = files_with_content.begin();
			    file_with_content != files_with_content.end();
			    ++file_with_content)
				{
					// loop over the data-type within the root file (ttbar-e, ttbar-mu, ...)
					// this is the postfix within the CMSSW files, eg. njets_muon_ttbar-e
					for(std::vector<std::string>::iterator postfix = file_with_content->second.begin();
					    postfix != file_with_content->second.end();
					    ++postfix)
						{
							
							std::string complete_name = (*histo_name)+(*postfix);
							outfile->cd();
							TH2F *histo = new TH2F(*((TH2F *) files[file_with_content->first]->GetDirectory("eventselection")->Get(complete_name.c_str())));
							
							double corr_factor = histo->GetCorrelationFactor(1,2);

							std::cout << *postfix << " : " << corr_factor << std::endl;
						}
				}
		}




	for(std::map<std::string,TFile*>::iterator file = files.begin(); file != files.end(); file++){

		delete file->second;
		file->second = NULL;
	}

	delete outfile;
	outfile = NULL;
	return 0;

}

	
		

