#! /usr/bin/python

import os, sys, subprocess

folder = str(sys.argv[1])
sys.path.append(folder)

for dirpath,dirnames,filenms in os.walk(folder):
    for script in filenms:
        if script.endswith('__DQMTest__Top__Histos.root'):
            #print script
            combine_format = str("files_with_content.push_back(\""+script+"\");");
            if not script.endswith('_R000000001__DQMTest__Top__Histos.root'):
                print combine_format
