#include "TFile.h"
#include "TDirectory.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TKey.h"
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include "HistoMerger/MergeHistos.h"
#include "Efficiency/Efficiency.h"



int main(int argc, char **argv)
{

	std::string type = argv[1]; //MC or Data
	bool is_data = true; //We dont want scaling here. stops scaling in HC
	bool verbose = false;

	TFile *outfile = new TFile(("DQM_"+type+"_test2.root").c_str(), "RECREATE");
	std::cout << " Outfile name " << ("DQM_"+type+".root").c_str() <<std::endl;

	std::cout << "cd to outfile" << std::endl;
	outfile->cd();
	std::string histo_name_contains = "muon";
	if(type == "MC")histo_name_contains = "mu_background";
		

	bool output_eff_table = true; //eff in pt ranges and eta ranges
	bool output_per_bin_eff = true; //eff in pt,eta ranges 2D
	bool output_per_bin_eff_table = false; //table eff in pt,eta ranges 2D
	std::string trigger2study = "HLT_Mu9";
	
	std::vector<std::string> files_with_content;  
	if(type == "Data") files_with_content.push_back("Data_plots.root");
	else if(type == "MC") files_with_content.push_back("Wjets_plots_pt.root");
	//else if(type == "MC")files_with_content.push_back("QCDMuPtWj_plots.root");
	//else if(type == "MC") files_with_content.push_back("TTbar_plots.root");
	

	std::vector<std::pair<std::string,std::string> > replace_name_string;
	if(type == "MC") replace_name_string.push_back(std::pair<std::string,std::string>("Wjets|mu_background|","Data|muon|"));
	//if(type == "MC") replace_name_string.push_back(std::pair<std::string,std::string>("QCDMuPt20to30|mu_background|","Data|muon|"));
	//if(type == "MC") replace_name_string.push_back(std::pair<std::string,std::string>("TTbar|muon|","Data|muon|"));

	
	/*********Read in all histo names from MC file**********/
	std::cout << "Opening file: " <<files_with_content[0] << std::endl;
	TFile *file = new TFile((files_with_content[0]).c_str(),"open");
	
	TKey *key;

	std::vector<std::string> o0_histo_names;
	std::vector<std::string> o0_histo_names2d;
	
	std::cout << "searching for keys" << std::endl;

	std::string root_dir = "eventselection";
	std::cout << "Making dir: " <<  root_dir << std::endl;
	outfile->mkdir((root_dir).c_str());
	
	file->cd();
	std::cout << " Searching in directory " << root_dir<<std::endl;
	TIter nextkey(file->GetDirectory((root_dir).c_str())->GetListOfKeys());
	std::cout << " Found directory" << std::endl;
	
	while ((key = (TKey*)nextkey())){
		if(verbose) std::cout << "processing: " << key->GetName() << std::endl;
		if(verbose) std::cout << "class: " << key->GetClassName() << std::endl;
		
		std::string histo_name = key->GetName();
		
		if(histo_name.find(".") != std::string::npos && histo_name.find(histo_name_contains) != std::string::npos){
			if((std::string) key->GetClassName() == "TH1F"){
				o0_histo_names.push_back(histo_name);
			}
			if((std::string) key->GetClassName() == "TH2F"){
				o0_histo_names2d.push_back(histo_name);
			}
		}
	}
	
	delete file;
	
	/** Setting HC Options **/
	std::map<std::string,std::pair<double,double> > *xaxes = new std::map<std::string,std::pair<double, double> >();
        std::map<std::string,int> *rebin = new std::map<std::string,int>();


 		for(std::vector<std::string>::iterator histo_name = o0_histo_names.begin();histo_name != o0_histo_names.end();histo_name++){
    				
			if(histo_name->find("_pt_") != std::string::npos || histo_name->find(".pt_") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(0.,59.9);
			if(histo_name->find("relIso") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(0.,1.0);
			if(histo_name->find(".chi2") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(0.,10.);
			if(histo_name->find("jet_number") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(-0.5,5.5);
			if(histo_name->find(".eta") != std::string::npos)
				(*xaxes)[*histo_name] = std::pair<double,double>(-2.4,2.4);
		}



	std::vector<std::string> *effh_bin_ids = new std::vector<std::string>();
	effh_bin_ids->push_back(" Overall ");
	effh_bin_ids->push_back(" Barrel ");
	effh_bin_ids->push_back(" Overlap ");
	effh_bin_ids->push_back(" Endcap" );
	effh_bin_ids->push_back(" pt20to30 ");
	effh_bin_ids->push_back(" pt30to40 ");
	effh_bin_ids->push_back(" pt $>$ 40 ");
		
	Efficiency *eff = new Efficiency();
	eff->set_outfile(outfile);
	eff->set_rebin(rebin);
	eff->set_pre_identifier(".");
	eff->set_method(2);//bayes
	eff->set_output_total_efficiency(output_eff_table);
	eff->set_output_per_bin_efficiency("_fixedbins",output_per_bin_eff,output_per_bin_eff_table);
	eff->set_total_eff_histo("overall_efficiency",true);
	eff->set_eff_histo_bin_names(effh_bin_ids);
	eff->set_output_dir("eventselection");

	std::cout << "booking histocombiner" << std::endl;
	MergeHistos *hc = new MergeHistos();
	std::cout << "setting outfile" << std::endl;
	hc->set_outfile(outfile);
	std::cout << "adding histos" << std::endl;
	hc->set_eff_histos(true);
	hc->set_xaxes(xaxes);
	hc->set_rebin(rebin);
	hc->set_trigger_to_study(trigger2study);
	hc->set_replace_name_string(replace_name_string);
	hc->set_vfile_names(files_with_content);

	hc->set_vhisto_names(o0_histo_names);
	hc->set_output_directory("eventselection");
	std::cout << "MAKING 1D histos" << std::endl;
	hc->combine_all_histos<TH1F>(is_data);
	
	//Now pass the combined histos to Efficiency.cc to get Eff graphs.	
	eff->calculate(hc->get_histos_for_eff<TH1F>("denom"),hc->get_histos_for_eff<TH1F>("num"));


	std::cout << "booking histocombiner 2d" << std::endl;
	MergeHistos *hc2d = new MergeHistos();
	std::cout << "setting outfile 2d" << std::endl;
	hc2d->set_outfile(outfile);
	hc2d->set_trigger_to_study(trigger2study);
	hc2d->set_replace_name_string(replace_name_string);
	hc2d->set_vfile_names(files_with_content);
	std::cout << "adding histos 2d" << std::endl;

	hc2d->set_vhisto_names(o0_histo_names2d);
	hc2d->set_output_directory("eventselection");
	std::cout << "MAKING 2D histos" << std::endl;
	hc2d->combine_all_histos<TH2F>(is_data);
	
	
	std::cout << "writing output" << std::endl;


	outfile->Write();
	outfile->Close();
	delete hc;
	hc = NULL;
	delete eff;
	eff = NULL;
	delete hc2d;
	hc2d = NULL;

	return 0;
	
}
