#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "MergeHistos.h"
#include <iostream>
#include <map>
#include <vector>

int main(int argc, char **argv)
{
	TFile *outfile = new TFile("outfile.root", "recreate");
	MergeHistos *mhistos = new MergeHistos(outfile);	

	std::string mass="";
	std::string version="";
	std::string histo_name="";
	std::string cutset = "";
	std::string signal_background = "";
	if(argc > 2){
		mass = argv[1];
		version = argv[2];
		histo_name = argv[3];
		cutset = argv[4];
		signal_background = argv[5];
	}
	bool verbose=false;
	//std::string version="";
	//std::string histo_name="top_mass_";
	//std::string histo_name="top_Had_1btag_chimass_";
//	std::string histo_name="top_Had_chimass_";
//   std::string histo_name="Ht_";
//   std::string histo_name="Ht_wo_MET_";
	std::string directory_name="eventselection";
	//std::string cutset = "05_cutset";
	
	std::map<std::string,std::vector<std::string> > file_names;
	if(signal_background == "s"){
		file_names["tprime_"+mass+version+".root"].push_back(histo_name+"Tprime"+mass+"|muon|"+cutset);
		file_names["tprime_"+mass+version+".root"].push_back(histo_name+"Tprime"+mass+"|mu_background|"+cutset);
		// file_names["tprime_"+mass+version+"175"+".root"].push_back(histo_name+"Tprime"+mass+"|muon|"+cutset);
		// file_names["tprime_"+mass+version+"175"+".root"].push_back(histo_name+"Tprime"+mass+"|mu_background|"+cutset);
	}else if(signal_background == "b"){
 		file_names["wjets"+version+".root"].push_back(histo_name+"Wjets|mu_background|"+cutset);
 		file_names["zjets"+version+".root"].push_back(histo_name+"Zjets|mu_background|"+cutset);
 		file_names["ttbar"+version+".root"].push_back(histo_name+"TTbar|mu_background|"+cutset);
 		file_names["qcdmupt15"+version+".root"].push_back(histo_name+"Mupt15|mu_background|"+cutset);
 		file_names["t_t_chan"+version+".root"].push_back(histo_name+"TtChan|mu_background|"+cutset);
 		file_names["t_s_chan"+version+".root"].push_back(histo_name+"TsChan|mu_background|"+cutset);
 		file_names["t_tW_chan"+version+".root"].push_back(histo_name+"TtWChan|mu_background|"+cutset);
 		// file_names["wjets"+version+"175"+".root"].push_back(histo_name+"Wjets|mu_background|"+cutset);
 		// file_names["zjets"+version+"175"+".root"].push_back(histo_name+"Zjets|mu_background|"+cutset);
 		// file_names["ttbar"+version+"175"+".root"].push_back(histo_name+"TTbar|mu_background|"+cutset);
 		// file_names["qcdmupt15"+version+"175"+".root"].push_back(histo_name+"Mupt15|mu_background|"+cutset);
 		// file_names["t_t_chan"+version+"175"+".root"].push_back(histo_name+"TtChan|mu_background|"+cutset);
 		// file_names["t_s_chan"+version+"175"+".root"].push_back(histo_name+"TsChan|mu_background|"+cutset);
 		// file_names["t_tW_chan"+version+"175"+".root"].push_back(histo_name+"TtWChan|mu_background|"+cutset);
	}else{
		std::cout << "ERROR: can't find signal_background information" << std::endl;
	}


	mhistos->set_file_names(file_names);

	TH1F *combined_histo=mhistos->combine_histo(mass, cutset, histo_name);

	double event_counter=mhistos->get_nevents_uncut();

	//sum over bins
	double final_entries=0;
	for(int nbinx=1; nbinx <= combined_histo->GetNbinsX(); ++nbinx){
		for(int nbiny=1; nbiny <= combined_histo->GetNbinsY(); ++nbiny){
			//final_entries += int(0.5 + combined_histo->GetBinContent(nbinx,nbiny));
			final_entries += combined_histo->GetBinContent(nbinx,nbiny);
		}
	}

	if(verbose){
		printf("number of events (s1): %i \n", int(0.5 + event_counter));
		std::cout << "number of events (s5): " << final_entries << std::endl;
		std::cout << "mass range: " << combined_histo->GetXaxis()->GetXmin() << " " << combined_histo->GetXaxis()->GetXmax() << std::endl;
		std::cout << "number of bins: " << combined_histo->GetNbinsX() << std::endl;
	}else{
		//printf("%i\n", int(0.5 +event_counter));
		//std::cout << int(0.5 + final_entries) << std::endl;
		printf("%f\n", event_counter);
		std::cout << final_entries << std::endl;
		std::cout << combined_histo->GetXaxis()->GetXmin() << " " << combined_histo->GetXaxis()->GetXmax() << std::endl;
		std::cout << combined_histo->GetNbinsX() << std::endl;
	}

	for(int nbinx=1; nbinx <= combined_histo->GetNbinsX(); ++nbinx){
		for(int nbiny=1; nbiny <= combined_histo->GetNbinsY(); ++nbiny){
			if(verbose)
				std::cout << "binx: " << nbinx << " biny: " << nbiny << " entries: " << int(0.5 + combined_histo->GetBinContent(nbinx,nbiny)) << std::endl;
			else
				//std::cout << int (0.5 + combined_histo->GetBinContent(nbinx,nbiny)) << std::endl;
				std::cout << combined_histo->GetBinContent(nbinx,nbiny) << std::endl;
		}
	}

	delete outfile;
	outfile = NULL;
	delete mhistos;
	mhistos = NULL;
}
