#!/usr/bin/perl

#@versions = ('_20091026_175_jes_00', '_20091026_175_jes_02');
#$jec_version = "_JEC_1_02";
#@versions = ('_20091026_175_jes_00', '_20091026_175_jes_05');
#$jec_version = "_JEC_1_05";
@versions = ('_20091026_175_jes_00', '_20091026_175_jes_10');
$jec_version = "_JEC_1_10";

#@versions = ('');
@signals = (250, 300, 350, 400);
#@signals = (250);
$suffix = "";

@cutsets = ('07_cutset');
#@versions = ('_20091026_250_jes_00');
#@cutsets = ('07_cutset', '06_cutset', '08_cutset');
#$jec_version = "_BTAG_SYS";

$signal_background = 's';

$type = '';

$type = $ARGV[0] if($ARGV[0]);
die "usage: $0 [ht|ht_wo_MET|m3|mindiff|chi0b|chi1b|chi2b|Wb|Wb1b]\n" unless(@ARGV > 0);


foreach $mass(@signals){
	foreach $cutset(@cutsets){
		foreach $version(@versions){
			my $histo = get_histo($type);

			run_command("getting bin content","./get_bin_contents $mass $version $histo $cutset $signal_background > output.txt");
			my $outfile = get_outfile($type, $mass, "$version$cutset", $signal_background);
			$outfiles = $outfiles." $outfile";
			run_command("moving outfile", "mv output.txt $outfile");
		}
	}
	if(@versions > 1){
		my $outfile = get_outfile($type, $mass, $jec_version, $signal_background);
		run_command("combining outfiles", "cat $outfiles > $outfile");
		$outfiles = "";
	}elsif(@cutsets > 1){
		my $outfile = get_outfile($type, $mass, $jec_version, $signal_background);
		run_command("combining outfiles", "cat $outfiles > $outfile");
		$outfiles = "";
	}
}

sub run_command
{
        my($message, $command) = @_;
        print $message if($message);
        if($command){
                print " ( $command )...";
                system("$command 2>&1");
        }

        print " done."."\n";
}

sub get_histo
{
	my $type = pop @_;
	return "top_mass_" if($type eq 'm3');
	return "top_mass_minimisation_" if($type eq 'mindiff');
	return "top_Had_2btag_chimass_" if($type eq 'chi2b');
	return "top_Had_1btag_chimass_" if($type eq 'chi1b');
	return "top_Had_chimass_" if($type eq 'chi0b');
	return "Ht_" if($type eq 'ht');
	return "Ht_wo_MET_" if($type eq 'ht_wo_MET');
	return "top_hadmass_Wb_0btag_" if($type eq 'Wb');
	return "top_hadmass_Wb_1btag_" if($type eq 'Wb1b');

	die "ERROR: no histo name found\n";
}

sub get_outfile
{
	my ($type, $mass, $version, $signal_background) = @_;
	my $signal = '';
	$signal = "signal" if($signal_background eq 's');
	$signal = "background" if($signal_background eq 'b');
	return "tprime_".$mass."_".$signal."_mux_mindifmass$suffix$version.txt" if($type eq 'mindiff');
	return "tprime_".$mass."_".$signal."_mux_m3mass$suffix$version.txt" if($type eq 'm3');
	return "tprime_".$mass."_".$signal."_mux_ht$suffix$version.txt" if($type eq 'ht');
	return "tprime_".$mass."_".$signal."_mux_ht_wo_MET$suffix$version.txt" if($type eq 'ht_wo_MET');
	return "tprime_".$mass."_".$signal."_mux_chi_2b$suffix$version.txt" if($type eq 'chi2b');
	return "tprime_".$mass."_".$signal."_mux_chi_1b$suffix$version.txt" if($type eq 'chi1b');
	return "tprime_".$mass."_".$signal."_mux_chi_0b$suffix$version.txt" if($type eq 'chi0b');
	return "tprime_".$mass."_".$signal."_mux_Wb_0b$suffix$version.txt" if($type eq 'Wb');
	return "tprime_".$mass."_".$signal."_mux_Wb_1b$suffix$version.txt" if($type eq 'Wb1b');

	die "ERROR: no outfile name found\n";
}
