#ifndef MERGEHISTOS_H
#define MERGEHISTOS_H

#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

class MergeHistos{
	public:
		MergeHistos(TFile *outfile);
		TH1F* combine_histo(std::string mass, std::string cutset, std::string histo_name);
		void set_file_names(std::map<std::string,std::vector<std::string> > names);
		void set_integrated_luminosity(double value);
		double get_nevents_uncut();	// events expected at integrated luminosity scaled to cross section

	private:
		TFile *outfile;
		std::map<std::string,std::vector<std::string> > file_names;

		double event_counter;
		double integrated_luminosity;

};
#endif
