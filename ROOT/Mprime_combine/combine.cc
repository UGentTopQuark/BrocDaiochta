#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include "Canvas_Holder.h"
#include <sstream>
#include "HistoCombiner.h"

int main(int argc, char **argv)
{
	std::string e_mu = argv[1];
	std::string c_type = argv[2];
	std::string cutset = argv[3];
	std::string version = "0428_1450";

	bool electron = true;
	if(e_mu == "e"){
		std::cout << "plotting for electrons..." << std::endl;
		electron = true;
	}
	else{
		std::cout << "plotting for muons..." << std::endl;
		electron = false;
	}

	std::vector<std::string> *sequence_to_plot = new std::vector<std::string>();

	std::map<std::string,std::vector<std::string> > files_with_content;
	if(electron){
		if(c_type == "s" || c_type == "c"){
			std::cout << "plotting signal..." << std::endl;
			files_with_content["tprime_"+version+".root"].push_back("Tprime|electron|"+cutset);
			sequence_to_plot->push_back("Tprime|electron|"+cutset);
			files_with_content["tprime275_"+version+".root"].push_back("Tprime_275|electron|"+cutset);
			sequence_to_plot->push_back("Tprime_275|electron|"+cutset);
		}
		if(c_type == "c"){
			std::cout << "plotting background..." << std::endl;
			files_with_content["tprime_"+version+".root"].push_back("Tprime|e_background|"+cutset);
			sequence_to_plot->push_back("Tprime|e_background|"+cutset);
			files_with_content["tprime275_"+version+".root"].push_back("Tprime_275|e_background|"+cutset);
			sequence_to_plot->push_back("Tprime_275|e_background|"+cutset);


		}
	}else{
		if(c_type == "s" || c_type == "c"){
		  std::cout << "plotting signal..." << std::endl;
		  files_with_content["tprime_"+version+".root"].push_back("Tprime|muon|"+cutset);
		  sequence_to_plot->push_back("Tprime|muon|"+cutset);
		  files_with_content["tprime275_"+version+".root"].push_back("Tprime_275|muon|"+cutset);
		  sequence_to_plot->push_back("Tprime_275|muon|"+cutset);
		}
		if(c_type == "c"){
		  std::cout << "plotting background..." << std::endl;
		  files_with_content["tprime_"+version+".root"].push_back("Tprime|mu_background|"+cutset);
		  sequence_to_plot->push_back("Tprime|mu_background|"+cutset);
		  files_with_content["tprime275_"+version+".root"].push_back("Tprime_275|mu_background|"+cutset);
		  sequence_to_plot->push_back("Tprime_275|mu_background|"+cutset);
		}
		
	}

	std::vector<std::string> histo_names;
	histo_names.push_back("jet_number_");
	histo_names.push_back("muon_number_");
	histo_names.push_back("muon_pt_");
	histo_names.push_back("muon_eta_");
	histo_names.push_back("muon_phi_");
	histo_names.push_back("electron_number_");
	histo_names.push_back("electron_pt_");
	histo_names.push_back("electron_eta_");
	histo_names.push_back("electron_phi_");

	histo_names.push_back("Muon_Ht_");
	histo_names.push_back("Electron_Ht_");
	histo_names.push_back("Event_MET_");

	histo_names.push_back("jet_pt_");
	histo_names.push_back("jet_eta_");
	histo_names.push_back("jet_phi_");
	histo_names.push_back("jet1_pt_");
	histo_names.push_back("jet2_pt_");
	histo_names.push_back("jet3_pt_");
	histo_names.push_back("jet4_pt_");

	histo_names.push_back("top_mass_");
	histo_names.push_back("W_mass_");
	histo_names.push_back("muon_MtW_");
	histo_names.push_back("electron_MtW_");

	histo_names.push_back("min_mu_dR_");
	histo_names.push_back("min_e_dR_");

	histo_names.push_back("jet_unmatch_btag_");
	histo_names.push_back("jet_match_btag_");
	histo_names.push_back("jet1_eta_");
	histo_names.push_back("jet2_eta_");
	histo_names.push_back("match_mu_caloiso_");
	histo_names.push_back("match_mu_trackiso_");
	histo_names.push_back("match_mu_CombRelIso_");
	histo_names.push_back("unmatch_mu_CombRelIso_");
	histo_names.push_back("unmatch_mu_caloiso_");
	histo_names.push_back("unmatch_mu_trackiso_");
	histo_names.push_back("match_e_caloiso_");
	histo_names.push_back("match_e_trackiso_");
	histo_names.push_back("unmatch_e_caloiso_");
	histo_names.push_back("unmatch_e_trackiso_");
	histo_names.push_back("match_e_CombRelIso_");
	histo_names.push_back("unmatch_e_CombRelIso_");


	std::vector<int> *colours = new std::vector<int>();
	if(c_type == "s" || c_type == "c"){
        	colours->push_back(kRed-3);
        	colours->push_back(kOrange+1);
	}
	if(c_type == "c"){
        	colours->push_back(kSpring-6);
        	colours->push_back(kSpring-7);
        // 	colours->push_back(kBlue-2);
//         	colours->push_back(kBlue+4);
//         	colours->push_back(kRed+4);
//         	colours->push_back(kOrange-3);
	}

	std::cout << "booking HistoCombiner()" << std::endl;
	HistoCombiner *hc = new HistoCombiner();
	hc->set_cutset(cutset);
	hc->set_sequence_to_plot(sequence_to_plot);
	hc->set_colours(colours);
	hc->add_histos(files_with_content,histo_names);
	if(electron){
		hc->set_cross_section("Tprime|electron|"+cutset,36*0.15);
		hc->set_cross_section("Tprime|e_background|"+cutset,36*0.85);
		hc->set_cross_section("Tprime_275|electron|"+cutset,22*0.15);
		hc->set_cross_section("Tprime_275|e_background|"+cutset,22*0.85);
	// 	hc->set_cross_section("Wjets|e_background|"+cutset,35550*0.0052);
// 		hc->set_cross_section("Zjets|e_background|"+cutset,3540*0.0088);
// 		hc->set_cross_section("QCDherwigpt15|mu_background|"+cutset,1186700000);
	}else{
		hc->set_cross_section("Tprime|muon|"+cutset,36*0.15);
		hc->set_cross_section("Tprime|mu_background|"+cutset,36*0.85);
		hc->set_cross_section("Tprime_275|muon|"+cutset,22*0.15);
		hc->set_cross_section("Tprime_275|mu_background|"+cutset,22*0.85);
	// 	hc->set_cross_section("Wjets|mu_background|"+cutset,35550*0.0052);
// 		hc->set_cross_section("Zjets|mu_background|"+cutset,3540*0.0088);
// 		hc->set_cross_section("Mupt15|mu_background|"+cutset,509100000*0.000239*0.1612);
	}
	hc->scale_to_cross_section(200);
	hc->combine_histos();
	hc->write_histos();

	delete sequence_to_plot;
	sequence_to_plot=NULL;
	delete colours;
	colours=NULL;

	return 0;
}
