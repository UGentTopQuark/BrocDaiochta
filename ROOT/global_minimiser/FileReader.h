#ifndef FILEREADER_H
#define FILEREADER_H

#include "TFile.h"
#include "TH1D.h"
#include "TTree.h"
#include "TBranch.h"

typedef std::vector<std::vector<std::vector<std::vector<std::vector<unsigned int> > > > > vector5d;

class FileReader{
	public:
		FileReader();
		~FileReader();
		std::map<std::string, vector5d*> get_signal();
		std::map<std::string, vector5d*> get_background();
		std::map<std::string, int> get_nsignal_events();
		std::map<std::string, int> get_nbackground_events();
		void set_files(std::vector<std::string> *signal_files, std::vector<std::string> *background_files);
		void read_branches();

	private:
		void close_files();

		// place to store later on the different vectors with the events per
		// cut value
		std::map<std::string, vector5d*> signal;
		std::map<std::string, int> nsignal_events;
		std::map<std::string, vector5d*> background;
		std::map<std::string, int> nbackground_events;

		std::vector<std::string> *signal_files;
		std::vector<std::string> *background_files;

		std::vector<TFile*> files_to_close;
};

#endif
