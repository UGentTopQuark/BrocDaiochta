#include "FileReader.h"

FileReader::FileReader()
{
}

FileReader::~FileReader()
{
	close_files();
}

void FileReader::close_files()
{
	for(std::vector<TFile*>::iterator file = files_to_close.begin();
		file != files_to_close.end();
		++file){
		(*file)->Close();
		delete *file;
	}
}

void FileReader::read_branches()
{
	std::string evt_counter_histo = "evt_counter";

	// read in branches from files
	// first the signal branches
	for(std::vector<std::string>::iterator file_iter = signal_files->begin();
		file_iter != signal_files->end();
		++file_iter){

		signal[*file_iter] = new vector5d();

		TFile *file = new TFile(file_iter->c_str(),"OPEN");
		files_to_close.push_back(file);

		nsignal_events[*file_iter] = ((TH1F*) file->Get(evt_counter_histo.c_str()))->GetEntries();

		TTree *t = (TTree*) file->Get("t");
		
		TBranch *branch = t->GetBranch("Bronch");
		t->SetBranchAddress("Bronch",&(signal[*file_iter]));
		branch->GetEntry(0);
	}
	// background events
	for(std::vector<std::string>::iterator file_iter = signal_files->begin();
		file_iter != signal_files->end();
		++file_iter){

		background[*file_iter] = new vector5d();

		TFile *file = new TFile(file_iter->c_str(),"OPEN");
		files_to_close.push_back(file);

		nbackground_events[*file_iter] = ((TH1F*) file->Get(evt_counter_histo.c_str()))->GetEntries();

		TTree *t = (TTree*) file->Get("t");
		
		TBranch *branch = t->GetBranch("Bronch");
		t->SetBranchAddress("Bronch",&(background[*file_iter]));
		branch->GetEntry(0);
	}
}

std::map<std::string, vector5d*> FileReader::get_signal()
{
	return signal;
}

std::map<std::string, vector5d*> FileReader::get_background()
{
	return background;
}

std::map<std::string, int> FileReader::get_nsignal_events()
{
	return nsignal_events;
}

std::map<std::string, int> FileReader::get_nbackground_events()
{
	return nbackground_events;
}

void FileReader::set_files(std::vector<std::string> *signal_files, std::vector<std::string> *background_files)
{
	this->signal_files = signal_files;
	this->background_files = background_files;
}
