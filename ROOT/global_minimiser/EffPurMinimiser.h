#ifndef EFFPURMINIMISER_H
#define EFFPURMINIMISER_H

#include "FileReader.h"
#include <iostream>

class EffPurMinimiser{
	public:
		EffPurMinimiser();
		~EffPurMinimiser();
		void set_files(std::vector<std::string> *signal_files, std::vector<std::string> *background_files);
		void find_optimal_efficiency_times_purity();
		void set_integrated_luminosity(double lumi);
		void set_cross_section(std::string dataset, double cs);
		void set_integrate(bool integrate);

	private:
		typedef std::vector<std::vector<std::vector<std::vector<std::vector<unsigned int> > > > > vector5d;

		void print_result(int optimal_point[5]);
		// integrated eff x pur
		double calculate_eff_x_pur_int(int d1, int d2, int d3, int d4, int d5);
		// differential eff x pur
		double calculate_eff_x_pur_diff(int d1, int d2, int d3, int d4, int d5);
		int calculate_integral(std::map<std::string, vector5d*> datasets, std::string dataset_name, 
					int d1, int d2, int d3, int d4, int d5, int noverall_events);
		double scale_events(std::string dataset, int nevents, int overall_events);

		FileReader *file_reader;

		// place to store later on the different vectors with the events per
		// cut value
		std::map<std::string, vector5d*> signal;
		std::map<std::string, vector5d*> background;

		// overall number of entries in signal and background events
		std::map<std::string, int> nsignal_events;
		std::map<std::string, int> nbackground_events;

		double luminosity; // integrated luminosity

		const int static NDIM;

		double integrate; // itegrate events to higher values or show differential eff x pur

		// cross section for certain dataset
		std::map<std::string, double> cross_section;

		static const bool verbose = false;

		int nbins; // # bins in each dimension of histogram
};

#endif
