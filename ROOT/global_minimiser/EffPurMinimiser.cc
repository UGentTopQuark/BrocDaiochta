#include "EffPurMinimiser.h"

const int EffPurMinimiser::NDIM=5;

EffPurMinimiser::EffPurMinimiser()
{
	file_reader = new FileReader();
	integrate = false;
}

EffPurMinimiser::~EffPurMinimiser()
{
}

void EffPurMinimiser::set_files(std::vector<std::string> *signal_files, std::vector<std::string> *background_files)
{
	file_reader->set_files(signal_files, background_files = background_files);
	file_reader->read_branches();
	signal = file_reader->get_signal();
	background = file_reader->get_background();
	nsignal_events = file_reader->get_nsignal_events();
	nbackground_events = file_reader->get_nbackground_events();
}

void EffPurMinimiser::find_optimal_efficiency_times_purity()
{
	if(signal.begin() != signal.end()){
		nbins = signal.begin()->second->size();
	}
	else{
		std::cerr << "ERROR: EffPurMinimiser::find_optimal_eff_times_purity(): NO singal set" << std::endl;
		return;
	}

	int optimal_point[NDIM] = {0};
	double max_effxpur = -1;

	// loop over all points in 5d space
	for(int d1 = 0; d1 < nbins; ++d1)
		for(int d2 = 0; d2 < nbins; ++d2)
			for(int d3 = 0; d3 < nbins; ++d3){
				for(int d4 = 0; d4 < nbins; ++d4)
					for(int d5 = 0; d5 < nbins; ++d5){
						double curr_effxpur = -1;
						if(integrate)
							curr_effxpur = calculate_eff_x_pur_int(d1, d2, d3, d4, d5);
						else // differntial
							curr_effxpur = calculate_eff_x_pur_diff(d1, d2, d3, d4, d5);

						if(curr_effxpur > max_effxpur){
							optimal_point[0] = d1;
							optimal_point[1] = d2;
							optimal_point[2] = d3;
							optimal_point[3] = d4;
							optimal_point[4] = d5;
							max_effxpur = curr_effxpur;
						}
					}
				std::cout << "d1: " << d1 << "/" << nbins-1 << " " << "d2: " << d2 << "/" << nbins-1 << " " << "d3: " << d3 << "/" << nbins-1 << std::endl;
			}

	print_result(optimal_point);
}

void EffPurMinimiser::print_result(int optimal_point[NDIM])
{
	std::cout << "-------------------" << std::endl;
	for(int i = 0; i < NDIM; ++i)
	{
		std::cout << "bin " << i << " : " << optimal_point[i] << std::endl;
	}
	std::cout << "-------------------" << std::endl;
}

double EffPurMinimiser::calculate_eff_x_pur_int(int d1, int d2, int d3, int d4, int d5)
{
	double signal_events = 0;
	double overall_signal_events = 0;
	for(std::map<std::string,vector5d*>::iterator dataset = signal.begin();
		dataset != signal.end();
		++dataset){
		int nevents = calculate_integral(signal, dataset->first, d1, d2, d3, d4, d5, nsignal_events[dataset->first]);
		signal_events += scale_events(dataset->first, nevents, nsignal_events[dataset->first]);
		// efficiecny = 1 anyway for overall events
		overall_signal_events += scale_events(dataset->first, 1,1);
	}
	double background_events = 0;
	for(std::map<std::string,vector5d*>::iterator dataset = background.begin();
		dataset != background.end();
		++dataset){
		int nevents = calculate_integral(background, dataset->first, d1, d2, d3, d4, d5, nbackground_events[dataset->first]);
		background_events += scale_events(dataset->first, nevents, nbackground_events[dataset->first]);
	}

	double efficiency = signal_events/overall_signal_events;
	double purity = signal_events/(signal_events + background_events);

	return efficiency*purity;
}

double EffPurMinimiser::calculate_eff_x_pur_diff(int d1, int d2, int d3, int d4, int d5)
{
	double signal_events = 0;
	double overall_signal_events = 0;
	for(std::map<std::string,vector5d*>::iterator dataset = signal.begin();
		dataset != signal.end();
		++dataset){
		int nevents = (*signal[dataset->first])[d1][d2][d3][d4][d5];
		signal_events += scale_events(dataset->first, nevents, nsignal_events[dataset->first]);
		overall_signal_events += scale_events(dataset->first, 1,1);
	}

	double background_events = 0;
	for(std::map<std::string,vector5d*>::iterator dataset = background.begin();
		dataset != background.end();
		++dataset){
		int nevents = (*background[dataset->first])[d1][d2][d3][d4][d5];
		background_events += scale_events(dataset->first, nevents, nbackground_events[dataset->first]);
	}

	double efficiency = -1;
	if(overall_signal_events != 0)
		efficiency = signal_events/overall_signal_events;
	
	double purity = -1; 
	if(signal_events != -1)
		purity = signal_events/(signal_events + background_events);

	if(verbose){
		std::cout << "efficiency: " << efficiency << std::endl;
		std::cout << "purity: " << purity << std::endl;
	}

	return efficiency*purity;
}

double EffPurMinimiser::scale_events(std::string dataset, int nevents, int overall_events)
{
	double scaled_events = -1;
	double efficiency = ((double) nevents)/((double) overall_events);

	scaled_events = efficiency * cross_section[dataset] * luminosity;

	return scaled_events;
}

int EffPurMinimiser::calculate_integral(std::map<std::string, vector5d*> datasets, std::string dataset_name, 
					int d1, int d2, int d3, int d4, int d5, int noverall_events)
{
	int sum = 0;
	for(int i1 = 0; i1 <= d1; ++i1)
		for(int i2 = 0; i2 <= d2; ++i2)
			for(int i3 = 0; i3 <= d3; ++i3)
				for(int i4 = 0; i4 <= d4; ++i4)
					for(int i5 = 0; i5 <= d5; ++i5){
						sum += (*datasets[dataset_name])[i1][i2][i3][i4][i5];
					}

	return noverall_events - sum;
}

void EffPurMinimiser::set_integrated_luminosity(double lumi)
{
	luminosity = lumi;
}

void EffPurMinimiser::set_cross_section(std::string dataset, double cs)
{
	cross_section[dataset] = cs;
}

void EffPurMinimiser::set_integrate(bool integrate)
{
	this->integrate = integrate;
}
