#include <sstream>
#include <iostream>
#include <map>
#include <vector>

#include "EffPurMinimiser.h"

int main(int argc, char **argv)
{
	EffPurMinimiser *eff_pur_min = new EffPurMinimiser();
	std::vector<std::string> *signal_files = new std::vector<std::string>();
	signal_files->push_back("ttbar.root");

	std::vector<std::string> *background_files = new std::vector<std::string>();
	background_files->push_back("wjets.root");
//	background_files->push_back("zjets.root");
//	background_files->push_back("qcd.root");

	eff_pur_min->set_files(signal_files, background_files);
	eff_pur_min->set_cross_section("ttbar.root", 1.0);
	eff_pur_min->set_cross_section("wjets.root", 1.0);
	eff_pur_min->set_integrated_luminosity(200.0);
	eff_pur_min->set_integrate(false);
	eff_pur_min->find_optimal_efficiency_times_purity();

	delete signal_files;
	signal_files = NULL;
	delete background_files;
	background_files = NULL;
	delete eff_pur_min;
	eff_pur_min = NULL;

	return 0;
}
