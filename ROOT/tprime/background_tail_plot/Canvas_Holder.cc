#include "Canvas_Holder.h"

#include <ctime>
#include <cstdlib>
#include <stdlib.h>
#include <math.h>

using namespace std;
// ---------------------------------------------------------------
// ----------------------- Canvas_Holder functions ---------------
// ---------------------------------------------------------------

string CanvasHolder::getRnd(){
  int r=rand()%10000;
  char buf[7];
  sprintf (buf,"_R%d",r);
  return string(buf);
}


CanvasHolder::CanvasHolder() { 
  //cout<<"I am empty"<<endl;
  
  setStandardValues();
  theCanvasTitle=getRnd();
}

CanvasHolder::CanvasHolder(const vector<std::string> &mitems,
			   const vector<std::string> &blacklist,
			   const vector<TH1*> &inhist) {
  setStandardValues();
  
  for (vector<std::string>::const_iterator iter=mitems.begin();iter!=mitems.end();++iter){
    theMatchString+=(*iter);
  }
 
  cout<<"theMatchString: "<<theMatchString<<endl;
  for (vector<TH1*>::const_iterator iter=inhist.begin();iter!=inhist.end();++iter){
    if(iter==inhist.begin()) {theHistoType=(*iter)->IsA()->GetName();}
    //cout<<"type: "<<theHistoType<<" name: "<<(*iter)->GetName()<<endl;
    if (mitmatch((*iter)->GetName(),mitems,blacklist)) {
      if ( (*iter)->IsA()->GetName()!=theHistoType) {
	cerr<<"[ERROR] Histo-Type differs! Not adding "<<(*iter)->GetName()<<endl;
	cerr<<(*iter)->IsA()->GetName()<<"!="<<theHistoType<<endl;
	continue;
      }
      addHisto(*iter,doCutOut((*iter)->GetName(),mitems),"");
      cout<<"Match: "<<(*iter)->GetName()<<endl;
    }
  }

  // TODO: Titel setzen! Automatisch + Funktion
  if (ehistos.begin()!=ehistos.end())
    theCanvasTitle=ehistos.front().theHisto->GetName()+getRnd();    
  //cout<<"new Title:"<<theCanvasTitle<<endl;
}



CanvasHolder::CanvasHolder(const std::vector<std::string> &mitems,
			   const std::vector<std::string> &blacklist,
			   const std::map<std::string,TGraph*> &inmap){
  

  setStandardValues();
  
  for (vector<std::string>::const_iterator iter=mitems.begin();iter!=mitems.end();++iter){
    theMatchString+=(*iter);
  }


  // TODO: Titel setzen! Automatisch + Funktion
  theCanvasTitle = theMatchString;
  
  for (map<std::string,TGraph*>::const_iterator iter=inmap.begin();iter!=inmap.end();++iter){
    if (mitmatch(iter->first,mitems,blacklist)) {
     //  extGraph tmpGraph(iter->second,iter->first,doCutOut(iter->first,mitems),"L");
//       int tmpColor = getNextColor();
//       int tmpStyle = getNextStyle();
//       tmpGraph.theLineColor = tmpColor;
//       tmpGraph.LineStyle = tmpStyle;
//       tmpGraph.theMarkerColor = tmpColor;
//       tmpGraph.theMarkerStyle = tmpStyle+20;

//       theGraphVec.push_back(tmpGraph);

      addGraph(iter->second,iter->first,doCutOut(iter->first,mitems),"L");

    }
  }
}



void CanvasHolder::addGraph(TGraph* inGraph, std::string inName, std::string inLegName, std::string inDrawOpt) {

  extGraph tmpGraph(inGraph,inName,inLegName,inDrawOpt);
  int tmpColor = getNextColor();
  int tmpStyle = getNextStyle();
  tmpGraph.theLineColor = tmpColor;
  tmpGraph.theLineStyle = tmpStyle;
  tmpGraph.theMarkerColor = tmpColor;
  tmpGraph.theMarkerStyle = tmpStyle+20;
  theGraphVec.push_back(tmpGraph); 
}

void CanvasHolder::addGraphFormated(TGraph* inGraph, std::string inName, std::string inLegName, std::string inDrawOpt) {
  extGraph tmpGraph(inGraph,inName,inLegName,inDrawOpt);
  theGraphVec.push_back(tmpGraph); 
}


CanvasHolder::CanvasHolder(const CanvasHolder &inCanvas) {
  setStandardValues(); //Copy Values from other CanvasHolder?
  theHistoType=inCanvas.getHistoType();
  CanvasHolder::add(inCanvas);
  theCanvasTitle = ehistos.front().theHisto->GetName()+getRnd();
  
}

void CanvasHolder::add(const CanvasHolder &inCanvas){  
  if (ehistos.begin()==ehistos.end()) theHistoType=inCanvas.getHistoType();
  if ( inCanvas.getHistoType()!=theHistoType) {
    cerr<<"[ERROR] Histo-Type differs! Not adding CanvasHolder!"<<endl;
    cerr<<inCanvas.getHistoType()<<"!="<<theHistoType<<endl;
    return;
  }
  inCanvas.getExtHistos(ehistos);
}

void CanvasHolder::show(){
  for (vector<ExtHisto>::const_iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    cout<<"[show] "<<(*iter).theLegTitle<<endl;
    TCanvas *CANNE=new TCanvas((*iter).theHisto->GetTitle(),(*iter).theHisto->GetTitle(),800,600);
    CANNE->cd();
    (*iter).theHisto->Draw();
  }
}

void CanvasHolder::doSetError_1_N(const double &scale){
  if (scale==0) {
    cout<<"[ERROR ]  -  [SCALE is ZERO!]"<<endl;
    return;
  }
 
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    string type=ehistos.at(0).theHisto->IsA()->GetName();
    if (type=="TH1D") {
      unsigned int bins=(*iter).theHisto->GetNbinsX();
      for (unsigned int i=0;i<bins;++i)
	{
	  double value=(*iter).theHisto->GetBinContent(i);
	  double newErr=sqrt(1/(scale*value));
	  (*iter).theHisto->SetBinError(i,newErr);
	}
    }
  }
}

void CanvasHolder::setYperBinWidth(bool b){
  //to do: Reverse action? Must save old Hist?/Informations? ... hm...
  if (!b) {
    cerr<<"[Reverse of setYperBinWidth not yet available! - Doing Nothing.]"<<endl;
    return;
  }

  if (theDoYperBinWidth) {
    cout<<"YperBinWidth already set! - DOING NOTHING!"<<endl;
    return;
  }
  else {
    doYperBinWidth();
  }
}


void CanvasHolder::doYperBinWidth(){
  if (theDoYperBinWidth) return;
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    string type=ehistos.at(0).theHisto->IsA()->GetName();
    if (type=="TH1D"){
      int nBins=(*iter).theHisto->GetNbinsX();
      for (int i=1;i<=nBins;++i){
	double width=(*iter).theHisto->GetBinWidth(i);
	double value=(*iter).theHisto->GetBinContent(i);
	double error=(*iter).theHisto->GetBinError(i);
	(*iter).theHisto->SetBinContent(i,value/width);
	(*iter).theHisto->SetBinError(i,error/width);
      }
    }
  }
  theDoYperBinWidth=true;
}

void CanvasHolder::addHisto(TH1* histo, const std::string &legentry, std::string drawopt) {
  ExtHisto tmpHisto(dynamic_cast<TH1*>(histo->Clone()),legentry,drawopt);
  tmpHisto.theLineColor=getNextColor();
  tmpHisto.theLineStyle=getNextStyle();
  ehistos.push_back(tmpHisto);
}

void CanvasHolder::addHistoStacked(TH1* histo, const std::string &legentry, std::string drawopt) {
  ExtHisto tmpHisto(dynamic_cast<TH1*>(histo->Clone()),legentry,drawopt);
  tmpHisto.theLineColor=getNextColor();
  tmpHisto.theLineStyle=getNextStyle();
	tmpHisto.setFill();
	// FIXME: is the upper border of the loop right?
	for(int nbin = 0; nbin < tmpHisto.theHisto->GetNbinsX(); ++nbin){
		double bin_content = tmpHisto.theHisto->GetBinContent(nbin);
		for(std::vector<ExtHisto>::iterator ehisto = ehistos.begin();
			ehisto != ehistos.end();
			++ehisto)
		{
			if(ehisto->theHisto->GetNbinsX() > nbin){
				ehisto->theHisto->SetBinContent(nbin,ehisto->theHisto->GetBinContent(nbin) + bin_content);
			}
		}
		
	}
  ehistos.push_back(tmpHisto);
}

void CanvasHolder::addHistoFormated(TH1* histo, const std::string &legentry,std::string drawopt) {
  ExtHisto tmpHisto(dynamic_cast<TH1*>(histo->Clone()),legentry,drawopt);
  ehistos.push_back(tmpHisto);
}

int CanvasHolder::getNextColor(){
  if(theNextColor == 5) theNextColor++; 
  if(theNextColor == 10) theNextColor+=30; 
  return theNextColor++; 
}; 
int CanvasHolder::getNextStyle(){
  if(theNextStyle == 5) theNextStyle++; 
  if(theNextStyle == 10) theNextStyle+=30; 
  return theNextStyle++; 
};



void CanvasHolder::setDefaultStyle(){

  if(!theStyle) {
    theStyle = new TStyle(getRnd().c_str(),getRnd().c_str());
  }

  theStyle->SetCanvasDefH(800);
  theStyle->SetCanvasDefW(800);

  // Set pad grid and tick marks on opposite side of the axis
  //theStyle->SetPadGridX(1);
  //theStyle->SetPadGridY(1);

  theStyle->SetPadTickX(1);
  theStyle->SetPadTickY(1);

  // Set background color
  theStyle->SetCanvasColor(0);
  theStyle->SetStatColor(kWhite);

  // Title config
  theStyle->SetOptTitle(1);
  //  theStyle->SetTitleW(0.41);
  //  theStyle->SetTitleH(0.05);

//  theStyle->SetTitleX(0.16);
//  theStyle->SetTitleY(0.93);
  theStyle->SetTitleX(0.01);
  theStyle->SetTitleY(0.99);
  theStyle->SetTitleColor(1);
  theStyle->SetTitleTextColor(1);
  theStyle->SetTitleFillColor(0);
  theStyle->SetTitleBorderSize(1);


// Fonts

// Begin Michael
  theStyle->SetStatFont(42);
  theStyle->SetStatFontSize(0.025);
  theStyle->SetTitleFont(42);

  theStyle->SetTitleFontSize(0.04);

//  theStyle->SetTitleFontSize(0.05);

//  theStyle->SetTitleFont(42, "XYZ");
//  theStyle->SetLabelFont(42, "XYZ"); 
  
// End Michael







  // Set margins:
  theStyle->SetPadTopMargin(0.05);
  theStyle->SetPadBottomMargin(0.13);
  theStyle->SetPadLeftMargin(0.14);
  theStyle->SetPadRightMargin(0.08);
}

void CanvasHolder::setStandardValues() {
  //cout<<"StyleName:"<<theStyle->GetName()<<endl;



// FixMe: Shoul be editable from cc
  theCanleftmargin   = 0.18;
  theCanrightmargin  = 0.03;
  theCantopmargin    = 0.10;
  theCanbottommargin = 0.10;


  theLogX=theLogY=theLogZ=false;
  theLeg=0;
  theCan=0;
  theStyle=0;

  //  theOptStat=111110;
  //  theOptStat=0000;
//  optstat=0;
  theOptTitle = 1;
  theDoYperBinWidth=false;

  theDrawOpt = "";

  theLegW =  0.33;
  theLegH =  0.22;
  makeLegPos("UpperRight");
  


//   theLegX1 = 0.623116;
//   theLegY1 = 0.69459;
//   theLegX2 = 0.962312;
//   theLegY2 = 0.921466;

//0.624372,0.673647,0.962312,0.898778


  theDrawLegend = true;
  theLegDrawSymbol = "l";


  theCancelOutBigErr=false;


  theXaxisDec = theYaxisDec = kTRUE;
  theYaxisOff = 1.75;
  theXaxisOff = 1.00;

  theNextColor = 1;
  theNextStyle = 1;

  theYperBinWidth=false;
  
  theFont.set(42);

}



void CanvasHolder::doScaleHistos(const double &factor){
  for (vector<ExtHisto>::const_iterator iter=ehistos.begin();ehistos.end()!=iter;++iter){
    (*iter).theHisto->Scale(factor);
  }
}

void CanvasHolder::doTheCancelOutBigErr(){
    for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
      string type=ehistos.at(0).theHisto->IsA()->GetName();
      if (type=="TH1D"){
	int nBins=(*iter).theHisto->GetNbinsX();
	bool goOn=false;
	for (int i=1;i<=nBins;++i){
	  double value=(*iter).theHisto->GetBinContent(i);
	  double error=(*iter).theHisto->GetBinError(i);
	  if (fabs(error)/fabs(value)>0.5||goOn){
	    goOn=true;
	    (*iter).theHisto->SetBinContent(i,0);
	    (*iter).theHisto->SetBinError(i,0);
	  }

	}
      }
    }
    theCancelOutBigErr=true;
}

void CanvasHolder::setLegend(const std::vector<std::string> &inLeg){
  vector<std::string>::const_iterator LegIt=inLeg.begin();
  for (vector<ExtHisto>::iterator ita=ehistos.begin();ita!=ehistos.end();++ita){
    if (LegIt==inLeg.end()) break;
    //    cout<<"Legend:"<<(*LegIt)<<endl;
    (*ita).theLegTitle=*LegIt;
    //cout<<"++LegIt?"<<endl;
    ++LegIt;
  }
}

void CanvasHolder::prepareCanvas() {

  //CanvasTitle may not be empty, or root won't make a canvas, and our TStyle would
  //break, too
  
  setDefaultStyle();
    
  // Firefox color for TH2
  //  int colorsPa[22];
  for (int i=0;i<20;i++) colorsPa[i]=500+i; 
  theStyle->SetPalette(20,colorsPa);
  
 
  //cout<<"r: "<<r<<" buf:"<<buf<<endl;
  // initColorVec!



  theStyle->SetOptTitle(1);
  theStyle->SetOptStat(theOptStat);


  theStyle->cd();

  theCan=new TCanvas(theCanvasTitle.c_str(),theCanvasTitle.c_str(),900,800);
    
  theCan->SetFillColor(0);
  theCan->SetBorderMode(0);
  theCan->SetBorderSize(2);
  theCan->SetTickx();
  theCan->SetTicky();

  theCan->SetFrameFillStyle(0);
  theCan->SetFrameBorderMode(0);
  theCan->SetFrameFillStyle(0);
  theCan->SetFrameBorderMode(0);

  theCan->SetLeftMargin(theCanleftmargin);
  theCan->SetRightMargin(theCanrightmargin);
  theCan->SetTopMargin(theCantopmargin);
  theCan->SetBottomMargin(theCanbottommargin);


  theCan->SetLogy(theLogY);
  theCan->SetLogx(theLogX);
  if (theHistoType=="TH2D") theCan->SetLogz(theLogZ);
  
  theCan->Update();

}


void CanvasHolder::normalizeHistos() {
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    doNormalizeHisto((*iter).theHisto);
  }
}

    

void CanvasHolder::doNormalizeHisto(TH1 *hist){
  double scale=1/hist->Integral();
  hist->Scale(scale);
}





void CanvasHolder::formatHistos() {

  // get Min and Max for X and Y axis from the histograms
  double tmpMinX = 0;
  double tmpMaxX = 0;
  double tmpMinY = 0;
  double tmpMaxY = 0;
  
  bool getMinMaxStartVal = true;
  for (vector<ExtHisto>::iterator iter=ehistos.begin();
       iter!=ehistos.end();++iter){
    if(getMinMaxStartVal) {
      tmpMaxY = (*iter).theHisto->GetMaximum();
      tmpMinY = (*iter).theHisto->GetMinimum();
      tmpMinX = (*iter).theHisto->GetXaxis()->GetXmin();
      tmpMaxX = (*iter).theHisto->GetXaxis()->GetXmax();
      getMinMaxStartVal=false;
    }else {
      tmpMaxY = max((*iter).theHisto->GetMaximum(),tmpMaxY);
      tmpMinY = min((*iter).theHisto->GetMinimum(),tmpMinY);
      tmpMinX = min((*iter).theHisto->GetXaxis()->GetXmin(),tmpMinX);
      tmpMaxX = max((*iter).theHisto->GetXaxis()->GetXmax(),tmpMinX);
    }
  }

  // get Min and Max for X and Y axis from the graphs
  
  for(std::vector<extGraph>::iterator iter=theGraphVec.begin();
      iter!=theGraphVec.end();++iter) {
    TGraph *tmp = (*iter).theGraph;
    
    if(getMinMaxStartVal) {
      tmpMinX =tmp->GetHistogram()->GetXaxis()->GetXmin();
      tmpMaxX =tmp->GetHistogram()->GetXaxis()->GetXmax();
      tmpMinY =tmp->GetHistogram()->GetYaxis()->GetXmin();
      tmpMaxY =tmp->GetHistogram()->GetYaxis()->GetXmax();
      getMinMaxStartVal=false;
    } else {
      tmpMinX =min(tmp->GetHistogram()->GetXaxis()->GetXmin(),tmpMinX);
      tmpMaxX =max(tmp->GetHistogram()->GetXaxis()->GetXmax(),tmpMaxX);
      tmpMinY =min(tmp->GetHistogram()->GetYaxis()->GetXmin(),tmpMinY);
      tmpMaxY =max(tmp->GetHistogram()->GetYaxis()->GetXmax(),tmpMaxY);
      
    }
  }
  

    
  // set this variable if x axis should be set
  theUserSetXRange = false;
  if (theMinX.getState()&&theMaxX.getState())  theUserSetXRange = true;

  // set this variable if y axis should be set
  theUserSetYRange = false;
  if (theMinY.getState()&&theMaxY.getState())  theUserSetYRange = true;


  if(!theMinY.getState()) theMinY.set(tmpMinY);
  if(!theMaxY.getState()) theMaxY.set(tmpMaxY);


  // are currently only needed for TLines
  if(!theMinX.getState()) theMinX.set(tmpMinX);
  if(!theMaxX.getState()) theMaxX.set(tmpMaxX);



  // TODO: Log Y Check should be implemented also for TGraph!
  
  if( theLogY && (theMinY.getValue() <= 0)) {
    SwitchVal<double> minYLog(5);
    minYLog.unset();
    for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
      int nbinsX=(*iter).theHisto->GetNbinsX();
      for(int i = 1; i < nbinsX; i++) {
	double binContent = (*iter).theHisto->GetBinContent(i);
	if(binContent > 0 ) {
	  if( minYLog.getState() ) {
	    minYLog.set(min(binContent,minYLog.getValue()));
	  } else {
	    minYLog.set(binContent);
	  }
	}
      }
    }
    if(minYLog.getState() ) {
      std::cout<<"MinY changed for LogScale"<<std::endl;
      theMinY.set(minYLog.getValue());
    } else {
      std::cout<<"All Y entries <= 0! Could not changed for LogScale"<<std::endl;
    }
  }

  
  if( !theUserSetYRange) {
    if(theMinY.getValue() > 0) {
      theMinY.set(theMinY.getValue()*0.8);
    } else {
      theMinY.set(theMinY.getValue()*1.2);
    }
    
    if( theMaxY.getValue() > 0 ) {
      theMaxY.set(theMaxY.getValue()*1.2);
        } else {
      theMaxY.set(theMaxY.getValue()*0.8);
    }
  }


  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    formatHisto((*iter).theHisto);  
    (*iter).formatHisto();
  
    if (theUserSetXRange) (*iter).theHisto->GetXaxis()->SetRangeUser(theMinX.getValue(),theMaxX.getValue()); 
    //  if (theUserSetXRange) inHisto->SetAxisRange(theMinX.getValue(),theMaxX.getValue(),"X"); 
    //  if (theUserSetXRange) inHisto->GetXaxis()->SetLimits(theMinX.getValue(),theMaxX.getValue()); 
    
 //    (*iter).theHisto->GetYaxis()->SetRangeUser(theMinY.getValue(),theMaxY.getValue());
//     if (theUserSetXRange) (*iter).theHisto->GetXaxis()->SetRangeUser(theMinX.getValue(),theMaxX.getValue()); 
    
//     (*iter).theHisto->GetXaxis()->SetTitleOffset(theXaxisOff);
//     (*iter).theHisto->GetXaxis()->SetDecimals(theXaxisDec);
//     (*iter).theHisto->GetYaxis()->SetTitleOffset(theYaxisOff);
//     (*iter).theHisto->GetYaxis()->SetDecimals(theYaxisDec);
    
//     if(theTitle.getState()) (*iter).theHisto->SetTitle(theTitle.getValue().c_str());
//     if(theTitleX.getState()) (*iter).theHisto->GetXaxis()->SetTitle(theTitleX.getValue().c_str());
//     if(theTitleY.getState()) (*iter).theHisto->GetYaxis()->SetTitle(theTitleY.getValue().c_str());
    
  }
  
  for(std::vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter) {
  //   (*iter).theGraph->GetHistogram()->GetXaxis()->SetRangeUser(theMinX.getValue(),theMaxX.getValue());
//     (*iter).theGraph->GetHistogram()->GetYaxis()->SetRangeUser(theMinY.getValue(),theMaxY.getValue());
//     if(theTitle.getState()) (*iter).theGraph->GetHistogram()->SetTitle(theTitle.getValue().c_str());
//     if(theTitleX.getState()) (*iter).theGraph->GetHistogram()->GetXaxis()->SetTitle(theTitleX.getValue().c_str());
//     if(theTitleY.getState()) (*iter).theGraph->GetHistogram()->GetYaxis()->SetTitle(theTitleY.getValue().c_str());
    
    formatHisto((*iter).theGraph->GetHistogram()); 
    (*iter).formatGraph();
    // if (theUserSetXRange) (*iter).theHisto->GetXaxis()->SetRangeUser(theMinX.getValue(),theMaxX.getValue()); 
    //  if (theUserSetXRange) inHisto->SetAxisRange(theMinX.getValue(),theMaxX.getValue(),"X"); 
    if (theUserSetXRange) (*iter).theGraph->GetHistogram()->GetXaxis()->SetLimits(theMinX.getValue(),theMaxX.getValue()); 

   
    
  }
  
}



void CanvasHolder::formatHisto(TH1* inHisto) {
  inHisto->UseCurrentStyle();
  inHisto->GetYaxis()->SetRangeUser(theMinY.getValue(),theMaxY.getValue());
  
  inHisto->GetXaxis()->SetTitleOffset(theXaxisOff);
  inHisto->GetXaxis()->SetDecimals(theXaxisDec);
  inHisto->GetYaxis()->SetTitleOffset(theYaxisOff);
  inHisto->GetYaxis()->SetDecimals(theYaxisDec);
  
  if(theFont.getState()) {
    inHisto->SetTitleFont(theFont.getValue());
    inHisto->SetTitleFont(theFont.getValue(),"XYZ");
    inHisto->SetLabelFont(theFont.getValue(),"XYZ");
  }
  if(theTitle.getState()) inHisto->SetTitle(theTitle.getValue().c_str());
  if(theTitleX.getState()) inHisto->GetXaxis()->SetTitle(theTitleX.getValue().c_str());
  if(theTitleY.getState()) inHisto->GetYaxis()->SetTitle(theTitleY.getValue().c_str());
  


  
}


void CanvasHolder::getExtHistos(std::vector<ExtHisto> &eHistVec) const {
  //cout<<" [getExtHistos] adding: "<<ehistos.size()<<" ";
  for (vector<ExtHisto>::const_iterator ita=ehistos.begin();ita!=ehistos.end();++ita){
    ExtHisto tmp(*ita);
    tmp.cloneHisto();
    eHistVec.push_back(tmp);
  }
}


void CanvasHolder::setLineColors(const std::vector<int> &vcolor) {
  std::vector<int>::const_iterator coliter = vcolor.begin();
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    if(coliter != vcolor.end() ) {
      (*iter).theLineColor=*coliter;
      coliter++;
    } else {
      (*iter).theLineColor=1;
    }
  }
}

void CanvasHolder::setLineColors(){
  int counter=0;
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    (*iter).theLineColor=++counter;
  }
  
}

void CanvasHolder::setLineStyles(){
  int counter=0;
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    (*iter).theLineStyle=++counter;
  }
}


// To be fixed!!!
// Not so much internal variables - direct modification of Graphs!

void CanvasHolder::setLineSizeGraph(const int &insize) {
  for (vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter){
    (*iter).theGraph->SetLineWidth(insize);
  }
}




void CanvasHolder::setLineStylesGraph(const int &instyle) {
  for (vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter){
    (*iter).theLineStyle = instyle;
  }
}
void CanvasHolder::setLineColorsGraph(const int &instyle) {
  for (vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter){
    (*iter).theLineColor = instyle;
  }
}

void CanvasHolder::setMarkerStylesGraph(const int &instyle) {
  for (vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter){
    (*iter).theMarkerStyle = instyle;
  }
}
void CanvasHolder::setMarkerColorsGraph(const int &instyle) {
  for (vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter){
    (*iter).theMarkerColor = instyle;
  }
}
void CanvasHolder::setMarkerSizeGraph(const double &instyle) {
  for (vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter){
    (*iter).theMarkerSize = instyle;
  }
}
void CanvasHolder::setDrawOptGraph(std::string instring) {
  for (vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter){
    (*iter).theDrawOpt = instring;
  }    
}






void CanvasHolder::divideHisto(const ExtHisto &divHist) {
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    (*iter).theHisto->Divide(divHist.theHisto);
  }
  theCanvasTitle+="_DivideBy"+divHist.theLegTitle;
}


void CanvasHolder::divideHisto(const std::string &search) {
  //dangerous: Was bei mehreren matches? ungeschickt...

  ExtHisto a;
  bool foundHisto = false;
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter) {
    if( (*iter).theLegTitle.find(search) != std::string::npos) {
      a.theHisto=(*iter).theHisto;
      a.theLegTitle=((*iter).theLegTitle);
      foundHisto = true;
       break;
    }
  }
  if(foundHisto) {
    divideHisto(a);
  } else {
    std::cout<<"Warning: No histo found for "<<search<<std::endl;
  }
}


void CanvasHolder::exchangeAction(char action,CanvasHolder &B,const std::vector<std::string> &NoDiffIn){
  
  //action: "d"=divide, "m"=multiply  
  vector<ExtHisto> ehistosB;
  vector<ExtHisto> ehistosDiv;
  vector<Diff_Histos> ehistosC;
  B.getExtHistos(ehistosB);
  
  bool theDivideNormalized=false;
  
  //now finding all combinations:
  for (vector<ExtHisto>::const_iterator itA=ehistos.begin();itA!=ehistos.end();++itA)
    for (vector<ExtHisto>::const_iterator itB=ehistosB.begin();itB!=ehistosB.end();++itB){
      Diff_Histos temp(*itA,*itB);
      ehistosC.push_back(temp);
    }
  
  //check useful ones:
  for (vector<Diff_Histos>::iterator itA=ehistosC.begin();itA!=ehistosC.end();++itA){
    for (vector<string>::const_iterator itN=NoDiffIn.begin();itN!=NoDiffIn.end();++itN){
      string TName=(*itA).eHistA.theHisto->GetName();
      if (TName.find(*itN)!=string::npos) (*itA).use=false;
      TName=(*itA).eHistB.theHisto->GetName();
      if (TName.find(*itN)!=string::npos) (*itA).use=false;
    }
    if ((*itA).use) {
      double minA,minB,maxA,maxB;
      int    nbinsXA,nbinsXB;
      minA=(*itA).eHistA.theHisto->GetBinCenter(1);
      nbinsXA=(*itA).eHistA.theHisto->GetNbinsX();
      maxA=(*itA).eHistA.theHisto->GetBinCenter(nbinsXA);
      minB=(*itA).eHistB.theHisto->GetBinCenter(1);
      nbinsXB=(*itA).eHistB.theHisto->GetNbinsX();
      maxB=(*itA).eHistB.theHisto->GetBinCenter(nbinsXB);
      double valueA=minA+10*maxA+100*nbinsXA;
      double valueB=minB+10*maxB+100*nbinsXB;
      //cout<<"bins: " <<nbinsXA<<" "<<nbinsXB<<" value-diff:"<<valueA-valueB<<endl;
      if (valueA!=valueB) {
	(*itA).use=false;
	cout<<"[DIVIDING] ERROR: Histos not equal binning!"<<endl;
      }
    }
    
    if ((*itA).use){
      string type=(*itA).eHistA.theHisto->IsA()->GetName();
      if (type=="TH1D"){
	cout<<"DIVIDING: "<<(*itA).eHistA.theHisto->GetName()<<" / "<<(*itA).eHistB.theHisto->GetName()<<endl;
	ExtHisto tmpeH1((*itA).eHistA);
	tmpeH1.cloneHisto();
	TH1D *tmpH1=dynamic_cast<TH1D*>(tmpeH1.theHisto);
	TH1D *tmpH2=dynamic_cast<TH1D*>((*itA).eHistB.theHisto);
	if (theDivideNormalized){
	  doNormalizeHisto(tmpH1);
	  doNormalizeHisto(tmpH2);
	}
	string hname=tmpH2->GetTitle();
	hname +="/";
	hname +=tmpH2->GetTitle();
	//cout<<"[DIVIDE]"<<endl;
	if (action=='d') tmpH1->Divide(tmpH2);
	else if (action=='m') tmpH1->Multiply(tmpH2);
	else cerr<<"[ERROR] wrong action REQUESTED!"<<endl;
	//cout<<"[DIVIDED!]"<<endl;
	tmpH1->SetTitle(hname.c_str());
	
	//int nbins;
	//nbins=tmpH1->GetNbinsX();
	//for (int i=0;i<nbins+1;i++){
	//cout<<"i.:"<<tmpH1->GetBinContent(i)<<endl;
	//}
	//cout<<"#########################################"<<endl;
	tmpeH1.theLegTitle=hname.c_str();
	ehistosDiv.push_back(tmpeH1);
      }
      else {
	cout<<"[DIVIDE-ERROR] Will only divide TH1D!"<<endl;
      }
    }
  }
  cleanExtHistos(ehistos);
  cleanExtHistos(ehistosB);
  for (vector<ExtHisto>::iterator iter=ehistosDiv.begin();iter!=ehistosDiv.end();++iter){
    ehistos.push_back(*iter);
  }
}

void CanvasHolder::cleanExtHistos(vector<ExtHisto> &candidate){
  for (vector<ExtHisto>::iterator iter=candidate.begin();iter!=candidate.end();++iter){
    delete (*iter).theHisto;
  }
  candidate.clear();
}

void CanvasHolder::setLineColors(const int &incolor) {
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    (*iter).theLineColor=incolor;
  }
}

void CanvasHolder::setLineStyles(const std::vector<int> &vstyle) {
  std::vector<int>::const_iterator styeliter = vstyle.begin();
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    if(styeliter != vstyle.end() ) {
      (*iter).theLineStyle=*styeliter;
      styeliter++;
    } else {
      (*iter).theLineStyle=1;
    }
  }
}

void CanvasHolder::setLineStyles(const int &instyle) {
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    (*iter).theLineStyle=instyle;
  }
}


void CanvasHolder::cleanLegend(const std::map<std::string,std::string> &repmap) {
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    (*iter).theLegTitle=doStringRep((*iter).theLegTitle,repmap);

  }

  for (vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter){
    (*iter).theLegName = doStringRep((*iter).theLegName,repmap);
  }    
}



void CanvasHolder::makeLegPos( const std::string &inVal) {
  if(inVal == "UpperRight") {
    theLegX2 = 1-theCanrightmargin;
    theLegX1 = theLegX2 - theLegW;
    theLegY2 = 1-theCantopmargin;
    theLegY1 = theLegY2 - theLegH;
    
  } else if (inVal == "LowerRight") {
    
    theLegX2 = 1-theCanrightmargin;
    theLegX1 = theLegX2 - theLegW;
    theLegY1 = theCanbottommargin;
    theLegY2 = theLegY1 + theLegH;
    
  } else if (inVal == "UpperLeft") {
    
    theLegX1 = theCanleftmargin;
    theLegX2 = theLegX1 + theLegW;
    theLegY2 = 1-theCantopmargin;
    theLegY1 = theLegY2 - theLegH;
    
  } else if (inVal == "LowerLeft") {
    theLegX1 = theCanleftmargin;
    theLegX2 = theLegX1 + theLegW;
    theLegY1 = theCanbottommargin;
    theLegY2 = theLegY1 + theLegH;
  } else {
    std::cerr<<"CanvasHolder::setLegPos: No Settings for position "<<inVal<<" available"<<std::endl;
  }
}



void CanvasHolder::drawPad(TVirtualPad *a, std::string PadDrawOpt){
  cout<<"[DRAW] "<<theCanvasTitle<<endl;

  formatHistos();

  if(theLegPos.getState() )  makeLegPos(theLegPos.getValue());

  theLeg = new TLegend(theLegX1,theLegY1,theLegX2,theLegY2,NULL,"brNDC");
  theLeg->SetBorderSize(1);
  theLeg->SetLineColor(1);
  theLeg->SetLineStyle(1);
  theLeg->SetLineWidth(1);
  theLeg->SetFillColor(0);
  theLeg->SetFillStyle(1001);
  if(theLegTitle.getState() ) {
    theLeg->SetHeader(theLegTitle.getValue().c_str());
  }
  if(theLegNCol.getState() ) {
    theLeg->SetNColumns(theLegNCol.getValue());
  }

  if(theLegMargin.getState() ) {
    theLeg->SetMargin(theLegMargin.getValue());
  }
  if(theFont.getState()) {
    theLeg->SetTextFont(theFont.getValue());
  }
//  leg->SetTextSize(0.03146853);
  

  //TCanvas *CANNE=new TCanvas("bla","blubb",800,600);
  //CANNE->cd();

 

  bool isFirstDraw = true;

  for (vector<ExtHisto>::const_iterator iter=ehistos.begin();iter!=ehistos.end();++iter) {

    if((*iter).theLegTitle != "") {
	// FIXME: f=fill, l=line... distinguish
      theLeg->AddEntry((*iter).theHisto,(*iter).theLegTitle.c_str(),"f");
    }
     if(isFirstDraw ) {
      (*iter).theHisto->Draw( ((*iter).theDrawOpt+PadDrawOpt).c_str() );
      isFirstDraw = false;
    } else {
      (*iter).theHisto->Draw( ((*iter).theDrawOpt+PadDrawOpt+"same").c_str());
    }
  }

  for(std::vector<extGraph>::iterator iter=theGraphVec.begin();iter!=theGraphVec.end();++iter) {
    if((*iter).theLegName != "") {
      theLeg->AddEntry((*iter).theGraph,(*iter).theLegName.c_str(),theLegDrawSymbol.c_str());
    }
    if(isFirstDraw) {
      (*iter).theGraph->Draw(std::string("A"+ (*iter).theDrawOpt).c_str());
      isFirstDraw=false;
    } else {
      (*iter).theGraph->Draw(std::string((*iter).theDrawOpt).c_str());
    }
  }
  

 //  for(std::vector<TLine>::iterator iter=theLines.begin(); iter!=theLines.end();++iter) {  
//     (*iter).Draw("same");
//   }
  
 //  for(std::map<TLine*,std::string>::iterator iter=theLines.begin(); iter!=theLines.end();++iter) {  
//     if(iter->second != "") {
//       theLeg->AddEntry(iter->first,iter->second.c_str(),theLegDrawSymbol.c_str());
//     }
//     iter->first->Draw("same");
//   }
  
  for(std::vector<extLine>::iterator iter=theLines.begin(); iter!=theLines.end();++iter) {  
    (*iter).makeLine(theMinX.getValue(),theMaxX.getValue(),theMinY.getValue(),theMaxY.getValue());
    if((*iter).theLegEntry != "") {
      theLeg->AddEntry((*iter).theLine,(*iter).theLegEntry.c_str(),theLegDrawSymbol.c_str());
    }
    (*iter).theLine->Draw("same");
  }


    

  for(std::vector<extTF1>::iterator iter=theTF1Vec.begin();
      iter!=theTF1Vec.end(); ++iter) {
    theLeg->AddEntry((*iter).theTF1,(*iter).theLegName.c_str());
    (*iter).formatTF1();
    if(isFirstDraw) {
      (*iter).theTF1->Draw();
      isFirstDraw=false;
    }else {
      (*iter).theTF1->Draw("LSAME");
    }
  }
  

  if (theDrawLegend) theLeg->Draw("NDC");
}


bool CanvasHolder::draw(std::string opt){

  // scheint nicht mehr noetig zu sein
  // entfernt bei Umstellung auf TGraph
//   if( ehistos.begin() == ehistos.end() ) {
//     std::cout<<"Could not draw histos for "<<theCanvasTitle<<". No histos!"<<std::endl;
//     return false;
//   }
  prepareCanvas();
  theDrawOpt = opt + theDrawOpt;;
  drawPad(theCan->cd(),theDrawOpt);
  return true;
}



void CanvasHolder::write(TFile *file, std::string opt){
  if( CanvasHolder::draw(opt) ) {
    theCan->Update();
    theCan->Write();
  }
}


void CanvasHolder::save(const std::string format,std::string opt){
  if( CanvasHolder::draw(opt) ) {
    theCan->Update();
    std::string tmpString = theCanvasTitle+"."+format;
    theCan->SaveAs(tmpString.c_str());
  }
}


/// String manipulation

std::string CanvasHolder::doStringRep(const std::string &instring, 
				    const std::string &cutIt,
				    const std::string &rep) {
  std::string retstring=instring;
  long unsigned int pos=retstring.find(cutIt);
  if (pos!=std::string::npos) {
    std::string temp1=retstring.replace(pos,cutIt.length(),rep);
    retstring=temp1;
  }
  return retstring;
}




std::string CanvasHolder::doCutOut(const std::string &instring,const std::vector<string> &cutIt){
  std::string retstring=instring;
  for (vector<string>::const_iterator iter=cutIt.begin();iter!=cutIt.end();++iter){
    retstring=doStringRep(retstring,*iter,"");
  }
  return retstring;
}

std::string CanvasHolder::doStringRep(std::string instring, 
				    const map<string,string> &repmap) {
  for( map<string,string>::const_iterator iter=repmap.begin();
       iter!=repmap.end();++iter) {
    instring = doStringRep(instring,iter->first,iter->second);
  }
  return instring;
}



  


bool CanvasHolder::mitmatch(const string &hina,const vector<string> &items,const vector<string> &blacklist){
  bool retval=true;
  //cout<<"mitmatch - ";
  for (vector<string>::const_iterator iter=items.begin();iter!=items.end();++iter){
    if ((hina.find(*iter)!=string::npos)&&retval) {
      retval=true;
      for (vector<string>::const_iterator iterb=blacklist.begin();iterb!=blacklist.end();++iterb){
	if ((hina.find(*iterb)!=string::npos)) {
	  retval=false;
	}
      }
    }
    else {
      retval=false;
    }
    if (!retval) break;
  }
  //cout<<"return mitmatch: "<<retval<<endl;
  return retval;
}



void CanvasHolder::fillQcdBinning(vector<double> &binnings)
{
  binnings.push_back(4000);
  binnings.push_back(3750);
  binnings.push_back(3500);
  binnings.push_back(3250);
  binnings.push_back(3000);
  binnings.push_back(2800);
  binnings.push_back(2600);
  binnings.push_back(2400);
  binnings.push_back(2200);
  binnings.push_back(2000);
  binnings.push_back(1800);
  binnings.push_back(1600);
  binnings.push_back(1400);
  binnings.push_back(1200);
  binnings.push_back(1000);
  binnings.push_back(900);
  binnings.push_back(800);
  binnings.push_back(700);
  binnings.push_back(600);
  binnings.push_back(520);
  binnings.push_back(470);
  binnings.push_back(425);
  binnings.push_back(380);
  binnings.push_back(335);
  binnings.push_back(300);
  binnings.push_back(250);
  binnings.push_back(230);
  binnings.push_back(195);
  binnings.push_back(170);
  binnings.push_back(145);
  binnings.push_back(120);
  binnings.push_back(95);
  binnings.push_back(80);
  binnings.push_back(60);
  binnings.push_back(50);
  binnings.push_back(0);
  sort(binnings.begin(),binnings.end());
}


// void CanvasHolder::doNewBinning(){
//   int pos=0;
//   for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
//     string type=ehistos.at(0).theHisto->IsA()->GetName();
//     if (type=="TH1D") {
//       TH1D *histo=dynamic_cast<TH1D*>((*iter).theHisto);
//       double low=histo->GetXaxis()->GetBinLowEdge(1);
//       double max=histo->GetXaxis()->GetBinUpEdge(histo->GetNbinsX());
//       //TH1D *baba=0;
//       //cout<<"low: "<<low<<" max: "<<max<<endl;
//       //cout<<"Name: "<<(*iter)->GetName()<<endl;
//       if ((low!=0) || (max!=4000)) {
// 	cout<<"[BINQCD WARNING] not from 0 to 4000!\n"<<endl;
//       }
      
//       TH1D* changed=binQCD(histo);
//       delete (histo);
//       ehistos.at(pos).theHisto=changed;
//       ++pos;
//     }
//   }
// }

void CanvasHolder::reBinHisto(TH1D *&histo,const vector<double> &binnings){
  int entries=binnings.size();
  double *bins=new double[entries];
  int counter=0;
  for (vector<double>::const_iterator iter=binnings.begin();iter!=binnings.end();iter++){
    bins[counter]=*iter;
    counter++;
  }
  
  double low=histo->GetXaxis()->GetBinLowEdge(1);
  int nbins=histo->GetNbinsX();
//   double max=histo->GetXaxis()->GetBinUpEdge(histo->GetNbinsX());
  double max=histo->GetXaxis()->GetBinUpEdge(nbins);
//   if ((low!=binnings.front()) || (max!=binnings.back())) {
  if (low!=0 || max!=4000) {
    cout<<"[BINQCD WARNING] "<<histo->GetName()<<" not from "<<binnings.front()<<" to "<<binnings.back()<<"! ("<<low<<":"<<max<<") bins:"<<nbins<<"\n"<<endl;
  }
	  

  TString name=histo->GetName();
  name+="_qcdB";
  TString title=histo->GetTitle();
  //cout<<"[BINQCD - rethist]"<<endl;
  TH1D *retHist=new TH1D(name,title,entries-1,bins);
  //cout<<"[BINQCD - rethist] END"<<endl;
  retHist->Sumw2();
  //ev Sumw2 status vom original abschreiben? Wie?
  retHist->SetDirectory(0);
  //Now filling OLD histo into new Histo:
  
  double nbinsold=histo->GetNbinsX();
  for (int i=0;i<=nbinsold;++i){
    //double binWidth=histo->GetBinWidth(i);
    //double startValue=histo->GetBinContent(i)*binWidth;
    //double startBinError=histo->GetBinError(i)*binWidth;
    double startValue=histo->GetBinContent(i);
    double startBinError=histo->GetBinError(i);
    //cout<<"[buildhisto] startBinError:"<<startBinError<<endl;
    double startCenterXValue=histo->GetBinCenter(i);
    //cout<<"BinValue: "<<BinValue<<endl;
    //cout<<"CenterXValue: "<<CenterXValue<<endl;
    int zielBin=retHist->FindBin(startCenterXValue);
    double zielValue=retHist->GetBinContent(zielBin);
    double zielError=retHist->GetBinError(zielBin);
    //cout<<"[buildhisto] zielError:"<<zielError<<endl;
    
    retHist->SetBinContent(zielBin,zielValue+startValue);
    retHist->SetBinError(zielBin,sqrt(startBinError*startBinError+zielError*zielError));
  }
  delete [] bins;
  delete histo;
  histo=retHist;
}



void CanvasHolder::doNewBinning(const vector<double> &binnings){
  for (vector<ExtHisto>::iterator iter=ehistos.begin();iter!=ehistos.end();++iter){
    string type=ehistos.at(0).theHisto->IsA()->GetName();
    if (type=="TH1D") {
      TH1D *histo=dynamic_cast<TH1D*>((*iter).theHisto);
      reBinHisto(histo,binnings);  //tauscht Histo aus! L�scht Original!
      (*iter).theHisto=histo;
    }
  }
}
