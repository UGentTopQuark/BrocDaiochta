#include "TGraphErrors.h"
#include "Canvas_Holder.h"

int main(int argc, char **argv)
{
double ht_A[4];
double ht_AERR[4];
double ht_a[4];
double ht_aERR[4];
double ht_x[4];
double ht_x_err[4];

ht_A[0] = 131.002;
ht_AERR[0] = 11.4454;
ht_a[0] = 38.4484;
ht_aERR[0] = 3.38512;
ht_x[0] = 242;
ht_x_err[0] = 0;

ht_A[1] = 528.988;
ht_AERR[1] = 22.9992;
ht_a[1] = 52.6366;
ht_aERR[1] = 2.41511;
ht_x[1] = 745;
ht_x_err[1] = 0;

ht_A[2] = 870;
ht_AERR[2] = 29.4952;
ht_a[2] = 68.1357;
ht_aERR[2] = 2.63642;
ht_x[2] = 1143;
ht_x_err[2] = 0;

ht_A[3] = 1272.74;
ht_AERR[3] = 35.6728;
ht_a[3] = 92.62;
ht_aERR[3] = 3.45147;
ht_x[3] = 1640;
ht_x_err[3] = 0;

TGraphErrors graph_A_ht(4, ht_x, ht_A, ht_x_err, ht_AERR);
TGraphErrors graph_a_ht(4, ht_x, ht_a, ht_x_err, ht_aERR);

double chi_A[6];
double chi_AERR[6];
double chi_a[6];
double chi_aERR[6];
double chi_x[6];
double chi_x_err[6];

chi_A[0] = 41;
chi_AERR[0] = 6.40302;
chi_a[0] = 45.5159;
chi_aERR[0] = 7.24275;
chi_x[0] = 129;
chi_x_err[0] = 0;

chi_A[1] = 195.983;
chi_AERR[1] = 13.9988;
chi_a[1] = 43.2504;
chi_aERR[1] = 3.15109;
chi_x[1] = 411;
chi_x_err[1] = 0;

chi_A[2] = 407.005;
chi_AERR[2] = 20.1741;
chi_a[2] = 41.172;
chi_aERR[2] = 2.07582;
chi_x[2] = 683;
chi_x_err[2] = 0;

chi_A[3] = 590.945;
chi_AERR[3] = 24.3083;
chi_a[3] = 43.5639;
chi_aERR[3] = 1.83332;
chi_x[3] = 886;
chi_x_err[3] = 0;

chi_A[4] = 729.001;
chi_AERR[4] = 26.9995;
chi_a[4] = 48.1499;
chi_aERR[4] = 1.84998;
chi_x[4] = 1029;
chi_x_err[4] = 0;

chi_A[5] = 1272.74;
chi_AERR[5] = 35.6728;
chi_a[5] = 92.62;
chi_aERR[5] = 3.45147;
chi_x[5] = 1640;
chi_x_err[5] = 0;

TGraphErrors graph_A_chi(6, chi_x, chi_A, chi_x_err, chi_AERR);
TGraphErrors graph_a_chi(6, chi_x, chi_a, chi_x_err, chi_aERR);

double ht_A_m3[4];
double ht_AERR_m3[4];
double ht_a_m3[4];
double ht_aERR_m3[4];
double ht_x_m3[4];
double ht_x_err_m3[4];

ht_A_m3[0] = 238.008;
ht_AERR_m3[0] = 15.4274;
ht_a_m3[0] = 75.6126;
ht_aERR_m3[0] = 5.84605;
ht_x_m3[0] = 242;
ht_x_err_m3[0] = 0;

ht_A_m3[1] = 723;
ht_AERR_m3[1] = 26.8882;
ht_a_m3[1] = 88.3093;
ht_aERR_m3[1] = 4.24506;
ht_x_m3[1] = 745;
ht_x_err_m3[1] = 0;

ht_A_m3[2] = 1091.99;
ht_AERR_m3[2] = 33.0447;
ht_a_m3[2] = 98.9421;
ht_aERR_m3[2] = 4.14432;
ht_x_m3[2] = 1143;
ht_x_err_m3[2] = 0;

ht_A_m3[3] = 1501;
ht_AERR_m3[3] = 38.7419;
ht_a_m3[3] = 120.246;
ht_aERR_m3[3] = 4.90952;
ht_x_m3[3] = 1640;
ht_x_err_m3[3] = 0;

TGraphErrors graph_A_ht_m3(4, ht_x_m3, ht_A_m3, ht_x_err_m3, ht_AERR_m3);
TGraphErrors graph_a_ht_m3(4, ht_x_m3, ht_a_m3, ht_x_err_m3, ht_aERR_m3);

double chi_A_m3[6];
double chi_AERR_m3[6];
double chi_a_m3[6];
double chi_aERR_m3[6];
double chi_x_m3[6];
double chi_x_err_m3[6];

chi_A_m3[0] = 121.928;
chi_AERR_m3[0] = 11.0403;
chi_a_m3[0] = 75.3702;
chi_aERR_m3[0] = 8.11817;
chi_x_m3[0] = 129;
chi_x_err_m3[0] = 0;

chi_A_m3[1] = 385.868;
chi_AERR_m3[1] = 19.6415;
chi_a_m3[1] = 77.8223;
chi_aERR_m3[1] = 4.78829;
chi_x_m3[1] = 411;
chi_x_err_m3[1] = 0;

chi_A_m3[2] = 644.986;
chi_AERR_m3[2] = 25.396;
chi_a_m3[2] = 75.1529;
chi_aERR_m3[2] = 3.52116;
chi_x_m3[2] = 683;
chi_x_err_m3[2] = 0;

chi_A_m3[3] = 843.001;
chi_AERR_m3[3] = 29.034;
chi_a_m3[3] = 76.1794;
chi_aERR_m3[3] = 3.14373;
chi_x_m3[3] = 886;
chi_x_err_m3[3] = 0;

chi_A_m3[4] = 980.994;
chi_AERR_m3[4] = 31.3202;
chi_a_m3[4] = 79.0906;
chi_aERR_m3[4] = 3.08004;
chi_x_m3[4] = 1029;
chi_x_err_m3[4] = 0;

chi_A_m3[5] = 1501;
chi_AERR_m3[5] = 38.7419;
chi_a_m3[5] = 120.246;
chi_aERR_m3[5] = 4.90952;
chi_x_m3[5] = 1640;
chi_x_err_m3[5] = 0;

TGraphErrors graph_A_chi_m3(6, chi_x_m3, chi_A_m3, chi_x_err_m3, chi_AERR_m3);
TGraphErrors graph_a_chi_m3(6, chi_x_m3, chi_a_m3, chi_x_err_m3, chi_aERR_m3);


	CanvasHolder ch;
	std::string drawopt = "*L";
	ch.setTitle("estimated parameter in A #times exp[-x/a]");

	ch.setTitleX("number of events after cut");
	ch.setTitleY("parameter A in A #times exp[-x/a]");
	ch.setCanvasTitle("background_parameters");
	ch.addGraph(&graph_A_ht,"name1","min.diff.M3, H_{T} cuts",drawopt);
	ch.addGraph(&graph_A_ht_m3,"name2","M3, H_{T} cuts",drawopt);
	ch.addGraph(&graph_A_chi,"name3","min.diff.M3, #chi^{2} cuts",drawopt);
	ch.addGraph(&graph_A_chi_m3,"name4","M3, #chi^{2} cuts",drawopt);

/*
	//ch.setTitleX("number of events after cut");
	//ch.setTitleX("H_{T} cut");
	ch.setTitleX("#chi^{2} cut");
//	ch.setTitleY("parameter A in A #times exp[-x/a]");
	ch.setTitleY("parameter a in A #times exp[-x/a]");
	ch.setCanvasTitle("background_parameters");
//	ch.addGraph(&graph_a_ht,"name1","min.diff.M3, H_{T} cuts",drawopt);
//	ch.addGraph(&graph_a_ht_m3,"name2","M3, H_{T} cuts",drawopt);
	ch.addGraph(&graph_a_chi,"name3","min.diff.M3, #chi^{2} cuts",drawopt);
	ch.addGraph(&graph_a_chi_m3,"name4","M3, #chi^{2} cuts",drawopt);
*/

	ch.setMarkerSizeGraph(1.5);
        ch.setLineSizeGraph(2);
        ch.setOptStat(00000);
	ch.setBordersY(00,2300);
//	ch.setBordersY(00,180);
	ch.save("eps");

        return 0;
}
