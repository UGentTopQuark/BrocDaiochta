#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include "TGraph.h"
#include "Canvas_Holder.h"
#include <sstream>
#include <string>

int main(int argc, char **argv)
{
	CanvasHolder ch;
	ch.setCanvasTitle("sigma");
	ch.setTitleX("t' mass [GeV]");
	ch.setTitleY("mass resolution #sigma [GeV]");

	double mass[13];
	double sigma_hadt[13];
	double sigma_lept[13];
	double sigma_hadW[13];

	mass[0] = 175;
	mass[1] = 250;
	mass[2] = 275;
	mass[3] = 300;
	mass[4] = 325;
	mass[5] = 350;
	mass[6] = 375;
	mass[7] = 400;

	sigma_hadt[0] = 20.67;
	sigma_hadt[1] = 26.73;
	sigma_hadt[2] = 27.95;
	sigma_hadt[3] = 29.27;
	sigma_hadt[4] = 26.91;
	sigma_hadt[5] = 30.70;
	sigma_hadt[6] = 35.65;
	sigma_hadt[7] = 30.37;

	sigma_lept[0] = 32.45;
	sigma_lept[1] = 46.40;
	sigma_lept[2] = 50.97;
	sigma_lept[3] = 51.47;
	sigma_lept[4] = 58.93;
	sigma_lept[5] = 61.81;
	sigma_lept[6] = 46.78;
	sigma_lept[7] = 85.65;

	sigma_hadW[0] = 12.45;
	sigma_hadW[1] = 11.26;
	sigma_hadW[2] = 10.84;
	sigma_hadW[3] = 10.52;
	sigma_hadW[4] = 11.84;
	sigma_hadW[5] = 8.57;
	sigma_hadW[6] = 10.68;
	sigma_hadW[7] = 10.74;

	TGraph graph_hadt(8, mass, sigma_hadt);	
	TGraph graph_lept(8, mass, sigma_lept);	
	TGraph graph_hadW(8, mass, sigma_hadW);	
	std::string histo_name("mass resolutions for different t' masses");
	std::string leg_title_hadt("had t");
	std::string leg_title_lept("lep t");
	std::string leg_title_hadW("had W");
	std::string draw_opts("*L");
	ch.addGraph(&graph_hadt, histo_name, leg_title_hadt, draw_opts);
	ch.addGraph(&graph_lept, histo_name, leg_title_lept, draw_opts);
	ch.addGraph(&graph_hadW, histo_name, leg_title_hadW, draw_opts);
	ch.setTitle("mass resolutions for different t' masses");
	ch.setMarkerSizeGraph(0.8);
	ch.setLineSizeGraph(3);
	ch.setOptStat(00000);
	ch.setBordersX(150,425);
	ch.setBordersY(0,150);
//	ch.setLegDraw(false);
//	ch.setLogY();
	ch.save("eps");

	return 0;
}
