#include "FitPlots.h"

using namespace RooFit;

FitPlots::FitPlots(FitManager* fm)
{
	this->fm = fm;
	//	setTDRStyle();
	theStyle = NULL;
}
FitPlots::~FitPlots()
{
	for(std::map<std::string,CanvasHolder*>::iterator c = choldersDNR.begin();
	    c != choldersDNR.end(); c++){
		delete c->second;
		c->second = NULL;
	}
	choldersDNR.clear();
	if(theStyle){
		delete theStyle;
		theStyle = NULL;
	}
}

void FitPlots::fit_nevts_gaussian(bool plot_gauss)
{
	std::cout << "FP: Fitting N events gaussian" << std::endl;
	outfile = fm->outfile();

	/**************************************
          Apply Gaussian fits to N Events distributions
          to get mean and sigma
	 *************************************/
	//Add name to plots so can output for each sys study.
	std::string sys_name = fm->sys_study_name();
	fm->initialise_ngauss_evts();

	std::vector<FitSetup*> fits = fm->fit_setups();
	for(std::vector<FitSetup*>::iterator fit = fits.begin();
	    fit != fits.end();fit++){
		FitSetup *fs = *fit;
		std::string fit_id = fs->fit_id();
		std::cout << "FP: N events gaussian for: " << fit_id << " " << sys_name << std::endl;
		std::vector<std::string> templates_n = fs->template_norm_names();
		std::cout << "N templates: " << templates_n.size() << std::endl;
		for(std::vector<std::string>::iterator id = templates_n.begin();
		    id != templates_n.end();id++){
			
			std::cout << "Fitting gaussian to " << *id << std::endl;
			TH1F* Nevents_h =dynamic_cast<TH1F*> (fm->output_histo("nfit_evts_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
			
			//Initialise Variables for fitting gaussian to Nevents
			RooRealVar gnevts("gnevts",("Number of "+fm->id_label(*id)+" events").c_str(),fs->range_low(*id),fs->range_high(*id));
			//if(*id == "st")
			//gnevts.setBins((fs->range_high(*id)-fs->range_low(*id)));
		
			RooGaussian gauss("gauss","gaussian pdf", gnevts,*(fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)),*(fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)));
			
			RooDataHist* Nevents_dhist = new RooDataHist("nevts_dhist","DataHist for fitted Nevents",RooArgList(gnevts),Nevents_h); 
			std::cout << "NGauss before fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
			
			gauss.fitTo(*Nevents_dhist,RooFit::PrintLevel(-1)) ;
			
			if(plot_gauss){
				std::cout << "Plotting for " << *id << std::endl;
				set_default_style();
				TCanvas *c_g = new TCanvas(("NFitted_"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
				set_canvas_style(c_g);

				RooPlot* frame_g = gnevts.frame(Name("NEvents"),Title("Gaussian"));
				frame_g->SetTitle("");
				frame_g->SetTitleOffset(1.3,"Y");
				Nevents_dhist->plotOn(frame_g);
				gauss.plotOn(frame_g);
				gauss.paramOn(frame_g,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));
				int y_max = Nevents_h->GetMaximum();
				frame_g->SetAxisRange(0,y_max*1.3,"Y");
				c_g->cd(); frame_g->Draw();
				c_g->SaveAs((fm->output_dir()+"Gaussian_nevts_"+*id+"_"+fit_id+"_"+sys_name+".pdf").c_str());
				c_g->Write();
				delete c_g;
				delete frame_g;
			}
			delete Nevents_h;
			delete Nevents_dhist;
			std::cout << "NGauss after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
		}
	}


// 	TH1F* test_cmean =dynamic_cast<TH1F*> (fm->output_histo("nfit_cmean_wj_global")->Clone());
// 	TH1F* test_diff =dynamic_cast<TH1F*> (fm->output_histo("nfit_cmean_nfit_wj_global")->Clone());
// 	TCanvas *tmp1 = new TCanvas("nfit_cmean_wj_global", "canvas",800,800);
// 	TCanvas *tmp2 = new TCanvas("nfit_cmean_nfit_wj_global", "canvas",800,800);
// 	tmp1->cd(); test_cmean->Draw();
// 	tmp1->SaveAs("nfit_cmean_wj_global.pdf");
// 	tmp1->Write();
// 	tmp2->cd(); test_diff->Draw();
// 	tmp2->SaveAs("nfit_cmean_nfit_wj_global.pdf");
// 	tmp2->Write();
	





	//Test
	//	std::cout << "++++++++ Testing Overflow/Underflow Single Top ++++++++++++" << std::endl;
	//std::cout << "Underflow: " << Nevents_st->GetBinContent(0) << std::endl;
	//std::cout << "Overflow:  " << Nevents_st->GetBinContent(Nevents_st->GetNbinsX()+1) << std::endl;
	//std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	delete_canvas_holders();
}

void FitPlots::plot_fit_error_gaussian(bool plot_gauss)
{
	outfile = fm->outfile();
	std::string sys_name = fm->sys_study_name();
	std::cout << "*********************** Fit Error ********************** " << std::endl;;
	
	std::vector<FitSetup*> fits = fm->fit_setups();
	for(std::vector<FitSetup*>::iterator fit = fits.begin();
	    fit != fits.end();fit++){
		FitSetup *fs = *fit;
		std::string fit_id = fs->fit_id();
		std::vector<std::string> templates_n = fs->template_norm_names();
		for(std::vector<std::string>::iterator id = templates_n.begin();
		    id != templates_n.end();id++){
			std::cout << "Fitting gaussian to error distribution for: " << *id << std::endl;

			std::string plot_id = "nfit_err_"+*id+"_"+fit_id+"_"+sys_name;

			RooAbsArg *err_var = (RooAbsArg*) fs->nfit(*id,true)->errorVar();
			RooAbsRealLValue *err_var2 = (RooAbsRealLValue*) fs->nfit(*id,true)->errorVar();
			//TH1F* FitErr = new TH1F(plot_id.c_str(),plot_id.c_str(),1000,0,fs->nfit(*id,true)->getError()*2);
			//std::cout << *id << " min: " << fs->nfit(*id,true)->errorVar()->getMin(fs->nfit(*id,true)->errorVar()->GetName()) << " max: " << fs->nfit(*id,true)->errorVar()->getMax() << std::endl;
			//fm->fit_par_data()->Print();
			//std::cout << "Title: " << fs->nfit(*id,true)->errorVar()->GetName() << std::endl;
			//TH1F* FitErr = new TH1F(plot_id.c_str(),plot_id.c_str(),1000,fs->nfit(*id,true)->errorVar()->getMin(),fs->nfit(*id,true)->errorVar()->getMax());
			const RooAbsBinning *binning  = fs->nfit(*id,true)->errorVar()->getBinningPtr("0");
			if(!binning)
				std::cout << "NULL binning " << std::endl;
			else
				std::cout << " not null binning " << std::endl;
// 			TH1F* FitErr = dynamic_cast<TH1F*> (fs->nfit(*id,true)->errorVar()->createHistogram(plot_id.c_str(), "x axis ",*binning ));
			std::cout << "low: " << binning->lowBound() << " high " << binning->highBound() << std::endl;
			TH1F* FitErr = new TH1F(plot_id.c_str(),plot_id.c_str(),1000,0,fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()*2);
			std::cout << *id << " Integral: " << FitErr->Integral() << std::endl;
			fm->fit_par_data()->fillHistogram(FitErr,RooArgList(*err_var));
			std::cout << *id << " Integral2: " << FitErr->Integral() << std::endl;

 			RooRealVar gerr("gerr",("Uncertainty on number of "+fm->id_label(*id)+" events").c_str(),(FitErr->GetMean(1)-FitErr->GetRMS(1)*4),(FitErr->GetMean(1)+FitErr->GetRMS(1)*4));
			RooRealVar gerr_mean("mean","Mean of Error",FitErr->GetMean(1),FitErr->GetMean(1)-FitErr->GetRMS(1)/2,FitErr->GetMean(1)+FitErr->GetRMS(1)/2);
			RooRealVar gerr_sigma("sigma","Sigma of Error",FitErr->GetRMS(1),0,FitErr->GetRMS(1)*2);

// 			RooRealVar gerr("gerr",("Fit error on number of "+fm->id_label(*id)+" events").c_str(),(FitErr->GetMean(1)-FitErr->GetRMS(1)/2),(FitErr->GetMean(1)+FitErr->GetRMS(1)/2));
// 			RooRealVar gerr_mean("mean","Mean of Error",FitErr->GetMean(1),FitErr->GetMean(1)-FitErr->GetRMS(1)/2,FitErr->GetMean(1)+FitErr->GetRMS(1)/2);
// 			RooRealVar gerr_sigma("sigma","Sigma of Error",FitErr->GetRMS(1),0,FitErr->GetRMS(1)*2);

			std::cout << *id <<  " RMS: " << FitErr->GetRMS(1) << std::endl;
			
			RooGaussian err_gauss("err_gauss","gaussian pdf", gerr,gerr_mean,gerr_sigma);
			RooDataHist* FitErr_dhist = new RooDataHist("err_dhist","DataHist for Error from Fit",RooArgList(gerr),FitErr);
			if(plot_gauss){
				err_gauss.fitTo(*FitErr_dhist,RooFit::PrintLevel(-1));
			}
			TCanvas *c_g_e = new TCanvas(("FitError"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
			RooPlot* frame_g_err = gerr.frame(Name("Error"),Title("Gaussian"));
			frame_g_err->SetTitle("");
			frame_g_err->SetTitleOffset(1.3,"Y");
			FitErr_dhist->plotOn(frame_g_err);
			if(plot_gauss){
				err_gauss.plotOn(frame_g_err);
				err_gauss.paramOn(frame_g_err,Layout(0.5,0.92,0.92),Format("NELU",AutoPrecision(0)));
			}
			int y_max = FitErr->GetMaximum();
			frame_g_err->SetAxisRange(0,y_max*1.4,"Y");
			//double x_min = gerr_mean.getVal() - 5*gerr_sigma.getVal();
			//double x_max = gerr_mean.getVal() + 5*gerr_sigma.getVal();
			//frame_g_err->SetAxisRange(x_min,x_max,"X");
			c_g_e->cd(); frame_g_err->Draw();
			c_g_e->SaveAs((fm->output_dir()+"Gaussian_error_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
			c_g_e->Write();
			delete FitErr;
			delete FitErr_dhist;
			delete c_g_e;
			delete frame_g_err;	



			
// 			if(*id != "st4"){
// 				TH1F* FitErr = dynamic_cast<TH1F*> (fm->output_histo("nfit_err_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
//  				RooRealVar gerr("gerr",("Fit error on number of "+fm->id_label(*id)+" events").c_str(),(fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()-FitErr->GetRMS(1)/4),(fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()+FitErr->GetRMS(1)/4));
//  				//RooRealVar gerr("gerr",("Fit error on number of "+fm->id_label(*id)+" events").c_str(),fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()/2,fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()*2);
// // 				gerr.setBins(4000);
//  				RooRealVar gerr_mean("mean","Mean of Error",fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal(),fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()/2,fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal()*2);
// 				RooRealVar gerr_sigma("sigma","Sigma of Error",FitErr->GetRMS(1),0,FitErr->GetRMS(1)*2);
// 				//RooRealVar gerr("gerr",("Fit error on number of "+fm->id_label(*id)+" events").c_str(),650,700);
// 				//gerr.setBins(4000);
// 				//RooRealVar gerr_mean("mean","Mean of Error",680,660,700);
// 				//RooRealVar gerr_sigma("sigma","Sigma of Error",10,0,50);
// 				std::cout << *id << "Gerr sigma: " << FitErr->GetRMS(1) << std::endl;

// 				RooGaussian err_gauss("err_gauss","gaussian pdf", gerr,gerr_mean,gerr_sigma);
// 				RooDataHist* FitErr_dhist = new RooDataHist("err_dhist","DataHist for Error from Fit",RooArgList(gerr),FitErr);
				
// 				err_gauss.fitTo(*FitErr_dhist,RooFit::PrintLevel(-1));
				
// 				TCanvas *c_g_e = new TCanvas(("FitError"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
// 				RooPlot* frame_g_err = gerr.frame(Name("Error"),Title("Gaussian"));
// 				FitErr_dhist->plotOn(frame_g_err);
// 				err_gauss.plotOn(frame_g_err);
// 				err_gauss.paramOn(frame_g_err,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));
// 				int y_max = FitErr->GetMaximum();
// 				frame_g_err->SetAxisRange(0,y_max*1.3,"Y");
// 				double x_min = gerr_mean.getVal() - 5*gerr_sigma.getVal();
// 				double x_max = gerr_mean.getVal() + 5*gerr_sigma.getVal();
// 				frame_g_err->SetAxisRange(x_min,x_max,"X");
// 				c_g_e->cd(); frame_g_err->Draw();
// 				c_g_e->SaveAs((fm->output_dir()+"Gaussian_error_"+*id+"_"+fit_id+"_"+sys_name+sys_name+".pdf").c_str());
// 				c_g_e->Write();
// 				delete FitErr;
// 				delete FitErr_dhist;
// 				delete c_g_e;
// 				delete frame_g_err;
				
				
// 				TH1F* FitErrHi = dynamic_cast<TH1F*> (fm->output_histo("nfit_errhi_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
// 				RooDataHist* FitErrHi_dhist = new RooDataHist("errhi_dhist","DataHist for Errorhi from Fit",RooArgList(gerr),FitErrHi);
// 				err_gauss.fitTo(*FitErrHi_dhist,RooFit::PrintLevel(-1));

// 				c_g_e = new TCanvas(("FitErrorHi"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
// 				frame_g_err = gerr.frame(Name("ErrorHi"),Title("Gaussian"));
// 				FitErrHi_dhist->plotOn(frame_g_err);
// 				err_gauss.plotOn(frame_g_err);
// 				err_gauss.paramOn(frame_g_err);
// 				c_g_e->cd(); frame_g_err->Draw();
// 				c_g_e->SaveAs((fm->output_dir()+"Gaussian_errorHi_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
// 				c_g_e->Write();
// 				delete FitErrHi;
// 				delete FitErrHi_dhist;
// 				delete c_g_e;
// 				delete frame_g_err;
				
				
// 				TH1F* FitErrLo = dynamic_cast<TH1F*> (fm->output_histo("nfit_errlo_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
// 				RooDataHist* FitErrLo_dhist = new RooDataHist("errlo_dhist","DataHist for Errorlo from Fit",RooArgList(gerr),FitErrLo);
// 				err_gauss.fitTo(*FitErrLo_dhist,RooFit::PrintLevel(-1));

// 				c_g_e = new TCanvas(("FitErrorLo"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
// 				frame_g_err = gerr.frame(Name("ErrorLo"),Title("Gaussian"));
// 				FitErrLo_dhist->plotOn(frame_g_err);
// 				err_gauss.plotOn(frame_g_err);
// 				err_gauss.paramOn(frame_g_err);
// 				c_g_e->cd(); frame_g_err->Draw();
// 				c_g_e->SaveAs((fm->output_dir()+"Gaussian_errorLo_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
// 				c_g_e->Write();
// 				delete FitErrLo;
// 				delete FitErrLo_dhist;
// 				delete c_g_e;
// 				delete frame_g_err;	
				


// 			}
// 			else{
// 				TH1F* FitErr = dynamic_cast<TH1F*> (fm->output_histo("nfit_err_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
// 				RooRealVar gerr("gerr",("Error on number of "+fm->id_label(*id)+" events").c_str(),3.4,4.2);
// 				RooRealVar gerr_mean("mean","Mean of Error",fm->ngauss(*id+"_sigma_"+fit_id+"_"+sys_name)->getVal(),3.7,4.1);
// 				RooRealVar gerr_sigma("sigma","Sigma of Error",0.25,0,0.5);
				
// 				RooGaussian err_gauss("err_gauss","gaussian pdf", gerr,gerr_mean,gerr_sigma);
// 				RooDataHist* FitErr_dhist = new RooDataHist("err_dhist","DataHist for Error from Fit",RooArgList(gerr),FitErr);
				
// 				err_gauss.fitTo(*FitErr_dhist,RooFit::PrintLevel(-1));
				
// 				TCanvas *c_g_e = new TCanvas(("FitError"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
// 				RooPlot* frame_g_err = gerr.frame(Name("Error"),Title("Gaussian"));
// 				FitErr_dhist->plotOn(frame_g_err);
// 				err_gauss.plotOn(frame_g_err);
// 				err_gauss.paramOn(frame_g_err,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));
// 				int y_max = FitErr->GetMaximum();
// 				FitErr->SetAxisRange(0,y_max*1.3,"Y");
// 				c_g_e->cd(); frame_g_err->Draw();
// 				c_g_e->SaveAs((fm->output_dir()+"Gaussian_error_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
// 				c_g_e->Write();
				
// 				delete FitErr;
// 				delete FitErr_dhist;
// 				delete c_g_e;
// 				delete frame_g_err;
				
// 				TH1F* FitErrHi = dynamic_cast<TH1F*> (fm->output_histo("nfit_errhi_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
// 				RooDataHist* FitErrHi_dhist = new RooDataHist("errhi_dhist","DataHist for Errorhi from Fit",RooArgList(gerr),FitErrHi);
// 				err_gauss.fitTo(*FitErrHi_dhist,RooFit::PrintLevel(-1));

// 				c_g_e = new TCanvas(("FitErrorHi"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
// 				frame_g_err = gerr.frame(Name("ErrorHi"),Title("Gaussian"));
// 				FitErrHi_dhist->plotOn(frame_g_err);
// 				err_gauss.plotOn(frame_g_err);
// 				err_gauss.paramOn(frame_g_err);
// 				c_g_e->cd(); frame_g_err->Draw();
// 				c_g_e->SaveAs((fm->output_dir()+"Gaussian_errorHi_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
// 				c_g_e->Write();
// 				delete FitErrHi;
// 				delete FitErrHi_dhist;
// 				delete c_g_e;
// 				delete frame_g_err;
				
				
// 				TH1F* FitErrLo = dynamic_cast<TH1F*> (fm->output_histo("nfit_errlo_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
// 				RooDataHist* FitErrLo_dhist = new RooDataHist("errlo_dhist","DataHist for Errorlo from Fit",RooArgList(gerr),FitErrLo);
// 				err_gauss.fitTo(*FitErrLo_dhist,RooFit::PrintLevel(-1));

// 				c_g_e = new TCanvas(("FitErrorLo"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
// 				frame_g_err = gerr.frame(Name("ErrorLo"),Title("Gaussian"));
// 				FitErrLo_dhist->plotOn(frame_g_err);
// 				err_gauss.plotOn(frame_g_err);
// 				err_gauss.paramOn(frame_g_err);
// 				c_g_e->cd(); frame_g_err->Draw();
// 				c_g_e->SaveAs((fm->output_dir()+"Gaussian_errorLo_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
// 				c_g_e->Write();
// 				delete FitErrLo;
// 				delete FitErrLo_dhist;
// 				delete c_g_e;
// 				delete frame_g_err;	
				//	}
				
		}
	}
	delete_canvas_holders();
}

void FitPlots::plot_fit_pull_gaussian()
{
	outfile = fm->outfile();
	std::string sys_name = fm->sys_study_name();

	std::vector<FitSetup*> fits = fm->fit_setups();
	for(std::vector<FitSetup*>::iterator fit = fits.begin();
	    fit != fits.end();fit++){
		FitSetup *fs = *fit;
		std::string fit_id = fs->fit_id();
		std::vector<std::string> templates_n = fs->template_norm_names();
		for(std::vector<std::string>::iterator id = templates_n.begin();
		    id != templates_n.end();id++){
			std::cout << "Fitting gaussian to pull distribution for: " << *id << std::endl;
			
			TH1F* FitPull = dynamic_cast<TH1F*> (fm->output_histo("nfit_pull_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
			RooRealVar gpull("gpull",(fm->id_label(*id)+ " pull").c_str(),-4,4);
			RooRealVar gpull_mean("mean","Mean of Pull",0,-4,4);
			RooRealVar gpull_sigma("sigma","Sigma of Pull",1,0,5);
			
			RooGaussian pull_gauss("pull_gauss","gaussian pdf", gpull,gpull_mean,gpull_sigma);
			RooDataHist* FitPull_dhist = new RooDataHist("pull_dhist","DataHist for Pull from Fit",RooArgList(gpull),FitPull);
			
			pull_gauss.fitTo(*FitPull_dhist,RooFit::PrintLevel(-1));
			
			TCanvas *c_g_p = new TCanvas(("FitPull"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
			RooPlot* frame_g_pull = gpull.frame(Name("Pull"),Title("Gaussian"));
			frame_g_pull->SetTitle("");
			frame_g_pull->SetTitleOffset(1.3,"Y");
			FitPull_dhist->plotOn(frame_g_pull);
			pull_gauss.plotOn(frame_g_pull);
			pull_gauss.paramOn(frame_g_pull,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));
			int y_max = FitPull->GetMaximum();
			frame_g_pull->SetAxisRange(0,y_max*1.3,"Y");
			c_g_p->cd(); frame_g_pull->Draw();
			c_g_p->SaveAs((fm->output_dir()+"Gaussian_pull_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
			c_g_p->Write();
			
			delete FitPull;
			delete FitPull_dhist;
			delete c_g_p;
			delete frame_g_pull;
			
			std::cout << "NGauss1.2 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
// 			TH1F* FitPullHi = dynamic_cast<TH1F*> (fm->output_histo("nfit_pullhi_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
// 			RooDataHist* FitPullHi_dhist = new RooDataHist("pull_dhist","DataHist for Pull from Fit",RooArgList(gpull),FitPullHi);
			
// 			pull_gauss.fitTo(*FitPullHi_dhist,RooFit::PrintLevel(-1));
			
// 			c_g_p = new TCanvas(("FitPullHi"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
// 			frame_g_pull = gpull.frame(Name("PullHi"),Title("Gaussian"));
// 			FitPullHi_dhist->plotOn(frame_g_pull);
// 			pull_gauss.plotOn(frame_g_pull);
// 			pull_gauss.paramOn(frame_g_pull);
// 			c_g_p->cd(); frame_g_pull->Draw();
// 			c_g_p->SaveAs((fm->output_dir()+"Gaussian_pullHi_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
// 			c_g_p->Write();
			
// 			delete FitPullHi;
// 			delete FitPullHi_dhist;
// 			delete c_g_p;
// 			delete frame_g_pull;
			
// 			TH1F* FitPullLo = dynamic_cast<TH1F*> (fm->output_histo("nfit_pulllo_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
// 			RooDataHist* FitPullLo_dhist = new RooDataHist("pull_dhist","DataHist for Pull from Fit",RooArgList(gpull),FitPullLo);
			
// 			pull_gauss.fitTo(*FitPullLo_dhist,RooFit::PrintLevel(-1));
			
// 			c_g_p = new TCanvas(("FitPullLo"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
// 			frame_g_pull = gpull.frame(Name("PullLo"),Title("Gaussian"));
// 			FitPullLo_dhist->plotOn(frame_g_pull);
// 			pull_gauss.plotOn(frame_g_pull);
// 			pull_gauss.paramOn(frame_g_pull);
// 			c_g_p->cd(); frame_g_pull->Draw();
// 			c_g_p->SaveAs((fm->output_dir()+"Gaussian_pullLo_"+*id+"_"+fit_id+sys_name+".pdf").c_str());
// 			c_g_p->Write();
			
// 			delete FitPullLo;
// 			delete FitPullLo_dhist;
// 			delete c_g_p;
// 			delete frame_g_pull;
		}
	}
	delete_canvas_holders();
}

void FitPlots::plot_constraint_mean()
{
	outfile = fm->outfile();
	std::cout << "FP: plot constraint mean" << std::endl;
	std::string sys_name = fm->sys_study_name();
	
	std::vector<FitSetup*> fits = fm->fit_setups();
	for(std::vector<FitSetup*>::iterator fit = fits.begin();
	    fit != fits.end();fit++){
		FitSetup *fs = *fit;
		std::string fit_id = fs->fit_id();
		std::vector<std::string> constraints_used = fs->gauss_constraints_used();
		for(std::vector<std::string>::iterator c = constraints_used.begin();
		    c != constraints_used.end();c++){

			std::cout << "Fitting gaussian to constraint mean distribution for: " << *c << std::endl;
			
			TH1F* CMean = dynamic_cast<TH1F*> (fm->output_histo("constraint_mean_"+*c+"_"+fit_id+"_"+sys_name)->Clone());
			RooRealVar gcmean("gcmean",("sampled constraint mean for "+*c+"_"+fit_id+"_"+sys_name).c_str(),fs->range_low(*c),fs->range_high(*c));
			RooRealVar gcmean_mean("mean","Mean of Cmean",0,fs->range_low(*c),fs->range_high(*c));
			RooRealVar gcmean_sigma("sigma","Sigma of Cmean",fs->constraint_var(*c+"_sigma"),0,fs->constraint_var(*c+"_sigma")*2);
			
			RooGaussian cmean_gauss("cmean_gauss","gaussian pdf", gcmean,gcmean_mean,gcmean_sigma);
			RooDataHist* CMean_dhist = new RooDataHist("cmean_dhist","DataHist for Constriant mean",RooArgList(gcmean),CMean);
			
			cmean_gauss.fitTo(*CMean_dhist,RooFit::PrintLevel(-1));
			
			TCanvas *c_g_p = new TCanvas(("Cmean"+*c+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
			RooPlot* frame_g_cmean = gcmean.frame(Name("Cmean"),Title("Gaussian"));
			frame_g_cmean->SetTitle("");
			frame_g_cmean->SetTitleOffset(1.3,"Y");
			CMean_dhist->plotOn(frame_g_cmean);
			cmean_gauss.plotOn(frame_g_cmean);
			cmean_gauss.paramOn(frame_g_cmean,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));
			c_g_p->cd(); frame_g_cmean->Draw();
			c_g_p->SaveAs((fm->output_dir()+"Gaussian_constriant_mean_"+*c+"_"+fit_id+sys_name+".pdf").c_str());
			c_g_p->Write();
			
			delete CMean;
			delete CMean_dhist;
			delete c_g_p;
			delete frame_g_cmean;
			
			RooRealVar gcmean2("gcmean2",("Nfit events for "+*c+"_"+fit_id+"_"+sys_name).c_str(),fs->range_low(*c),fs->range_high(*c));
			RooGaussian cmean_gauss2("cmean_gauss","gaussian pdf", gcmean2,gcmean_mean,gcmean_sigma);

			CMean = dynamic_cast<TH1F*> (fm->output_histo("constraint_evts_"+*c+"_"+fit_id+"_"+sys_name)->Clone());
			CMean_dhist = new RooDataHist("cmean_dhist","DataHist for Constriant mean",RooArgList(gcmean2),CMean);
			
			cmean_gauss2.fitTo(*CMean_dhist,RooFit::PrintLevel(-1));
			
			c_g_p = new TCanvas(("Cmean"+*c+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
			frame_g_cmean = gcmean2.frame(Name("Cmean"),Title("Gaussian"));
			CMean_dhist->plotOn(frame_g_cmean);
			cmean_gauss2.plotOn(frame_g_cmean);
			cmean_gauss2.paramOn(frame_g_cmean,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));
			c_g_p->cd(); frame_g_cmean->Draw();
			c_g_p->SaveAs((fm->output_dir()+"Gaussian_constriant_evts_"+*c+"_"+fit_id+sys_name+".pdf").c_str());
			c_g_p->Write();
			
			delete CMean;
			delete CMean_dhist;
			delete c_g_p;
			delete frame_g_cmean;
			
			double exp_range = (fs->constraint_var(*c+"_sigma")/fs->constraint_var(*c+"_mean"))*10;
			RooRealVar gcmean3("gcmean3",("(Sampled mean - Nfit events)/Sampled mean for "+*c+"_"+fit_id+"_"+sys_name).c_str(),-exp_range,exp_range);
			RooRealVar gcmean_mean3("mean","Mean of Cmean",0,-exp_range/10,+exp_range/10);
			RooRealVar gcmean_sigma3("sigma","Sigma of Cmean",exp_range/30,0,exp_range/5);
			RooGaussian cmean_gauss3("cmean_gauss3","gaussian pdf", gcmean3,gcmean_mean3,gcmean_sigma3);

			CMean = dynamic_cast<TH1F*> (fm->output_histo("diff_constraint_mean_evts_"+*c+"_"+fit_id+"_"+sys_name)->Clone());
			CMean_dhist = new RooDataHist("cmean_dhist","DataHist for Constriant mean",RooArgList(gcmean3),CMean);
			
			//	cmean_gauss3.fitTo(*CMean_dhist,RooFit::PrintLevel(-1));
			
			c_g_p = new TCanvas(("Diff_Cmean_nevts_"+*c+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
			frame_g_cmean = gcmean3.frame(Name("Cmean"),Title("Gaussian"));
			CMean_dhist->plotOn(frame_g_cmean);
			//cmean_gauss3.plotOn(frame_g_cmean);
			//cmean_gauss3.paramOn(frame_g_cmean,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));
			c_g_p->cd(); frame_g_cmean->Draw();
			c_g_p->SaveAs((fm->output_dir()+"Gaussian_diff_constriant_mean_evts_"+*c+"_"+fit_id+sys_name+".pdf").c_str());
			c_g_p->Write();
			
			delete CMean;
			delete CMean_dhist;
			delete c_g_p;
			delete frame_g_cmean;

		}
	}
	delete_canvas_holders();
}
void FitPlots::plot_2D_histos()
{
	outfile = fm->outfile();
	std::cout << "FP: plot 2D histos" << std::endl;
	std::string sys_name = fm->sys_study_name();
	std::map<std::string, TH2F*> histos = fm->output_histos_2D();
	for(std::map<std::string,TH2F*>::iterator h = histos.begin();
	    h != histos.end(); h++){

		std::string name = h->first;
		cholders[name+sys_name] = new CanvasHolder();
		cholders[name+sys_name]->setCanvasTitle(fm->output_dir()+"_"+name+"_"+sys_name);
		cholders[name+sys_name]->addHisto2D(h->second,"");
		cholders[name+sys_name]->setXaxisOff(1);
		cholders[name+sys_name]->setYaxisOff(1);
      
		cholders[name+sys_name]->save("pdf");
		delete_canvas_holders();
		
	}
}

void FitPlots::plot_1D_correlation()
{
	std::cout << "FP: Plot 1D Correlations" << std::endl;
	std::string sys_name = fm->sys_study_name();
	if(verbose)std::cout << "PF: 1D corr, sys study: " << sys_name << std::endl;

	std::vector<FitSetup*> fits = fm->fit_setups();
	std::vector<FitSetup*>::iterator fit = fits.begin();
	FitSetup *fs = *fit;
	std::string fit_id = fs->fit_id();

	std::vector<std::string> corr_ids;
	corr_ids.push_back("ttqcd");
	corr_ids.push_back("ttwzj");
	corr_ids.push_back("qcdwzj");
	corr_ids.push_back("ttst");
	corr_ids.push_back("qcdst");
	corr_ids.push_back("wzjst");
	
	
	//per event correlation Gaussian. To get error on correlation values
	if(!fm->use_real_data() && fs->nfit("tt_total",false,true) != NULL 
	   && fs->nfit("qcdmm",false,true) != NULL && fs->nfit("wzj",false,true) != NULL){
		for(std::vector<std::string>::iterator corr_id = corr_ids.begin();
		    corr_id != corr_ids.end(); corr_id++){
			
			
			TH1* h_corr = fm->fit_par_data()->createHistogram(("corr"+*corr_id).c_str(),0); 
			h_corr->SetAxisRange(0.0,1.0,"X");

			std::vector<std::string> vars;
			
			if(corr_id->find("tt") != std::string::npos){
				vars.push_back("tt_total");
			}
			if(corr_id->find("wzj") != std::string::npos){
				vars.push_back("wzj");
			}
			if(corr_id->find("st") != std::string::npos){
				vars.push_back("st");
			}
			if(corr_id->find("qcd") != std::string::npos){
				vars.push_back("qcdmm");
			}

			//			double mean_corr = fabs(fm->fit_par_data()->correlation(*(fs->nfit("tt_total",true)),*(fs->nfit("qcdmm",true))));
			double mean_corr = 0.5;
			if(vars.size() == 2){
				mean_corr = fabs(fm->fit_par_data()->correlation(*(fs->nfit(vars[0],true)),*(fs->nfit(vars[1],true))));
		}

			RooRealVar gcorr("gcorr",("Correlation_"+*corr_id+"_"+fit_id+"_"+sys_name).c_str(),0,1.0);
	
			RooRealVar gcorr_mean("mean","Mean of Corr",mean_corr,mean_corr-0.3,mean_corr+0.3);
			
			 RooRealVar gcorr_sigma("sigma","Sigma of Corr",0.05,0.0,0.1);
			
			RooGaussian corr_gauss("corr_gauss","gaussian pdf", gcorr,gcorr_mean,gcorr_sigma);
			RooDataHist* Corr_dhist = new RooDataHist("corr_dhist","DataHist for Constriant mean",RooArgList(gcorr),h_corr);
			
			corr_gauss.fitTo(*Corr_dhist,RooFit::PrintLevel(-1));
			
			TCanvas *c_corr = new TCanvas(("Ccorr"+*corr_id).c_str(), "canvas",800,800);
			RooPlot* frame_g_corr = gcorr.frame(Name(("h_corr"+*corr_id).c_str()),Title("Gaussian"));

			frame_g_corr->SetTitle("");
			frame_g_corr->SetTitleOffset(1.3,"Y");
			Corr_dhist->plotOn(frame_g_corr);
			corr_gauss.plotOn(frame_g_corr);
			corr_gauss.paramOn(frame_g_corr,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));

			c_corr->cd();frame_g_corr->Draw();
			//c_corr->cd();h_corr->Draw();
			//c_corr->SaveAs("/user/walsh/public_html/TEST_fit/Correlation_tt_qcd_.pdf");
			c_corr->SaveAs((fm->output_dir()+"Correlation_"+*corr_id+"_"+sys_name+".pdf").c_str());
			c_corr->Write();
			delete h_corr;
	

		}

	}
}
void FitPlots::plot_2D_correlation()
{

	std::cout << "FP: Plot Correlations" << std::endl;
	std::string sys_name = fm->sys_study_name();
	
	std::vector<FitSetup*> fits = fm->fit_setups();
	std::vector<FitSetup*>::iterator fit = fits.begin();
	FitSetup *fs = *fit;
	std::string fit_id = fs->fit_id();



	
	std::vector<std::string> templates_n = fs->template_norm_names();
	//Scatter plots nevents A vs nevents B
	std::map<std::string, TH2F*> corr_plots;
	for(size_t i = 0; i != templates_n.size();i++){
		for(size_t j = 0; j != templates_n.size();j++){
			if(i != j){
				std::string plot_id = "N"+templates_n[i]+"_vs_N"+templates_n[j]+"_"+fit_id+"_"+sys_name;
				TCanvas *c_corr1 = new TCanvas(plot_id.c_str(), "canvas", 640, 480);
				corr_plots[plot_id] = dynamic_cast<TH2F*> (fs->nfit(templates_n[i],true)->createHistogram(("nevts_"+templates_n[i]+"_vs_nevts_"+templates_n[j]).c_str(),*(fs->nfit(templates_n[j],true))));
				fm->fit_par_data()->fillHistogram(corr_plots[plot_id],RooArgList(*(fs->nfit(templates_n[i],true)),*(fs->nfit(templates_n[j],true))));
				//corr_plots[plot_id]->SetName(("nevts_"+templates_n[i]+"_vs_nevts_"+templates_n[j]).c_str()); 
				//c_corr1->cd(); corr_plots[plot_id]->Draw("box");
				//corr_plots[plot_id]->Write();
				//c_corr1->SaveAs((fm->output_dir()+plot_id+".pdf").c_str());
				//c_corr1->Write();
				//delete c_corr1;
				
				// 					plot_id = "Err_"+templates_n[i]+"_vs_Err_"+templates_n[j]+"_"+fit_id+"_"+sys_name;
				// 					c_corr1 = new TCanvas(plot_id.c_str(), "canvas", 640, 480);
				// 					RooAbsArg *erra = (RooAbsArg*) fs->nfit(templates_n[i],true)->errorVar();
				// 					RooAbsArg *errb = (RooAbsArg*) fs->nfit(templates_n[j],true)->errorVar();
				// 					corr_plots[plot_id] = new TH2F(("err_"+templates_n[i]+"_vs_err_"+templates_n[j]).c_str(),("err_"+templates_n[i]+"_vs_err_"+templates_n[j]).c_str(),200,0,fs->nfit(templates_n[i],true)->getError()*2,200,0,fs->nfit(templates_n[j],true)->getError()*4);
				// 					corr_plots[plot_id]->SetXTitle((templates_n[i]+" Error").c_str());
				// 					corr_plots[plot_id]->SetYTitle((templates_n[j]+" Error").c_str());
				// 					corr_plots[plot_id]->SetName(("err_"+templates_n[i]+"_vs_err_"+templates_n[j]).c_str()); 
				// 					c_corr1->cd(); corr_plots[plot_id]->Draw("box");
				// 					corr_plots[plot_id]->Write();
				// 					c_corr1->SaveAs((fm->output_dir()+plot_id+".pdf").c_str());
				// 						c_corr1->Write();
				// 					delete c_corr1;
				// 					delete erra;
				// 					delete errb;
			}
		}
	}
	
 delete_canvas_holders();
}

void FitPlots::plot_sample_event(bool is_real_data)
{ 
	outfile = fm->outfile();
	if(verbose) std::cout << " FitPlots:sample event" << std::endl;

	/****************************************************************
	  Output: Plot PDF and pseudo data from sample 1 toyMC
// 	***************************************************************/
	std::vector<int> *colours = new std::vector<int>();
	 colours->push_back(kRed);
	 colours->push_back(kGreen);
	 colours->push_back(kBlue);
	 colours->push_back(kBlack);
	 colours->push_back(kBlack);

	//SHyFT colour scheme
//;	colours->push_back(633);
//;	colours->push_back(616);
//;	colours->push_back(413);//411,407
//;	colours->push_back(400);
//;	colours->push_back(400);//undefined
//
	std::vector<int> *styles = new std::vector<int>();
	styles->push_back(2);
	styles->push_back(9);
	styles->push_back(7);
	styles->push_back(1);
	styles->push_back(1);

	//Add name to plots so can output for each sys study.
	std::string sys_name = fm->sys_study_name();

	std::vector<FitSetup*> fits = fm->fit_setups();
	for(std::vector<FitSetup*>::iterator fit = fits.begin();
	    fit != fits.end();fit++){
		FitSetup *fs = *fit;
		std::string fit_id = fs->fit_id();
		std::vector<std::string> templates_n = fs->template_norm_names();
		std::vector<std::string> templates_s = fs->template_shape_names();
		
		std::string input_templates_pid = "input_templates_"+fit_id+"_"+sys_name;
		std::string gendata_pid = "gendata_"+fit_id+"_"+sys_name;
		std::string template_data_pid = "templates_data_"+fit_id+"_"+sys_name;

		cholders[input_templates_pid] = new CanvasHolder();
		cholders[gendata_pid] = new CanvasHolder();
		cholders[template_data_pid] = new CanvasHolder();
		cholders[input_templates_pid]->setCanvasTitle(fm->output_dir()+input_templates_pid);
		cholders[gendata_pid]->setCanvasTitle(fm->output_dir()+gendata_pid);
		cholders[template_data_pid]->setCanvasTitle(fm->output_dir()+template_data_pid);
		
		cholders[input_templates_pid]->setOptStat(000000);
		cholders[gendata_pid]->setOptStat(000000);
		cholders[template_data_pid]->setOptStat(000000);
		
		TH1F* gendata = 0;
		TH1D* realdata = 0;
		if(fs->histo("data",false) != NULL){
			realdata = dynamic_cast<TH1D*> (fs->histo("data",true)->Clone());
			std::cout << "FP: data integral " << realdata->Integral() << std::endl;
	        }else{
			gendata = dynamic_cast<TH1F*> (fm->output_histo("gen_data_"+fit_id+"_"+sys_name)->Clone());
			std::cout << "FP: gen data integral " << gendata->Integral() << std::endl;
		}
	

		std::cout << "Plot eg: " << fit_id << " " << sys_name << std::endl;
		int i = 0;
		for(std::vector<std::string>::iterator id = templates_n.begin();
		    id != templates_n.end();id++){
			if(verbose) std::cout << "plot sample event. template id: " << *id  << std::endl; 
			//std::cout << "NGauss3 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
			//Get Fitted Nevents for scaling the p.d.f's
			Double_t nevts_0 = 0;
			if(!is_real_data){
				nevts_0 = fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal();
			}else{
				nevts_0 = fs->nfit(*id)->getVal();
			}
			RooRealVar tmp_x(*fs->observables()[fs->observable_id("x")]);
			if(verbose) std::cout << "plot sample event. create histo " << templates_s[i] << std::endl; 
			TH1F* h_template = dynamic_cast<TH1F*> (fs->pdf(templates_s[i])->createHistogram(("template_"+*id).c_str(),tmp_x));
			if(verbose) std::cout << "plot sample event. created histo " << std::endl; 
//%
//%			 h_template->GetXaxis()->SetBinLabel(1,"0"); //FIXME: bk comb
//%			 h_template->GetXaxis()->SetBinLabel(18,"500");
//%			 h_template->GetXaxis()->SetBinLabel(36,"1000");
//%			 h_template->GetXaxis()->SetBinLabel(50,"0");
//%			 h_template->GetXaxis()->SetBinLabel(69,"500");
//%			 h_template->GetXaxis()->SetBinLabel(89,"1000"); 
		//;	 h_template->GetXaxis()->SetBinLabel(1,"0");
		//;	 h_template->GetXaxis()->SetBinLabel(7,"200");
		//;	 h_template->GetXaxis()->SetBinLabel(13,"400");
		//;	 h_template->GetXaxis()->SetBinLabel(20,"600");
		//;	 h_template->GetXaxis()->SetBinLabel(24,"0");
		//;	 h_template->GetXaxis()->SetBinLabel(30,"200");
		//;	 h_template->GetXaxis()->SetBinLabel(37,"400");
		//;	 h_template->GetXaxis()->SetBinLabel(43,"600");
		//;	 h_template->SetNdivisions(14,"X");
			//%h_template->LabelsOption("h","X");//FIXME: bk comb
			//Overlay p.d.f's to show input
			if(verbose) std::cout << "plot sample event. add histo" << std::endl; 
			if(!h_template)std::cout << "plot sample event. h_template NULL" << std::endl; 
			cholders[input_templates_pid]->addHisto(h_template,fm->id_label(*id));
			if(verbose) std::cout << "plot sample event. Sys overlay templates " << std::endl; 
			if(sys_name != ""){//< template overlays for sys study
				std::vector<std::string> templates_nom = fm->nominal_template_names();
				std::string sys_templates_pid = "Sys_templates_"+templates_nom[i];
				if(choldersDNR.find(sys_templates_pid) == choldersDNR.end()){
					choldersDNR[sys_templates_pid] = new CanvasHolder();
					choldersDNR[sys_templates_pid]->setCanvasTitle(fm->output_dir()+sys_templates_pid);
					choldersDNR[sys_templates_pid]->setOptStat(000000);
					choldersDNR[sys_templates_pid]->setLegTitle(fm->id_label(templates_nom[i]));
					//choldersDNR[sys_templates_pid]->setLegendOptions(.2,.15);
					//					if(templates_nom[i] == "tt_total")
					//choldersDNR[sys_templates_pid]->addVLine(-1,"t #bar{t}",0,0,0);
				}
				std::string sys_name_label = sys_name;
				if(sys_name == "none") sys_name_label = "Nominal";
				choldersDNR[sys_templates_pid]->addHisto(h_template,fm->id_label(sys_name_label));
				//choldersDNR[sys_templates_pid]->addHisto(h_template,""); //for pdf sys study
				
					
			}

			//FIXME: for SHyFT
		//;	 if(*id == "tt_total") nevts_0 = fs->nscaled(*id)*1.014;
		//;	 if(*id == "st") nevts_0 = fs->nscaled(*id)*1.234;
		//;	 if(*id == "wzj") nevts_0 = fs->nscaled(*id)*0.9959;
		//;	 if(*id == "qcdmmmu") nevts_0 = fs->nscaled(*id)*12.03;
		//;	 if(*id == "qcdmme") nevts_0 = fs->nscaled(*id)*1.855;
			h_template->Scale(nevts_0);
			
			std::cout << "ID: " << *id <<  " nevents: " << nevts_0 << "w: " << h_template->GetBinWidth(1) << std::endl;

			cholders[template_data_pid]->addHistoStacked(h_template,fm->id_label(*id));
			

			TH1F* Nevents_h =dynamic_cast<TH1F*> (fm->output_histo("nfit_evts_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
			
			std::cout << *id << " nscaled: " << fs->nscaled(*id) << " nfit: " <<  Nevents_h->GetMean() << " nfit gauss: " << nevts_0 << std::endl;
			i++;
			delete h_template;
			delete Nevents_h;
			
		}		
		std::string x_id = fm->id_label(fs->observable_id("x"));
		cholders[template_data_pid]->set_eff_histos(true);
		if(realdata != 0){
			cholders[gendata_pid]->addHisto(realdata,"Data","Ep");
			cholders[template_data_pid]->addHisto(realdata, "Data","Ep");
		}else{
			cholders[gendata_pid]->addHisto(gendata,"Pseudo-data","Ep");
			cholders[template_data_pid]->addHisto(gendata, "Pseudo-data","Ep");
		}

		cholders[input_templates_pid]->setLineColors(*colours);
		cholders[template_data_pid]->setLineColors(*colours);
		cholders[input_templates_pid]->setLineStyles(*styles);
		cholders[template_data_pid]->setLineStyles(*styles);

		cholders[template_data_pid]->setTitleX(x_id);
		cholders[template_data_pid]->setTitleY("Events/bin");
		cholders[template_data_pid]->setXaxisOff(1.1);
		cholders[template_data_pid]->setYaxisOff(2.0);
		//%cholders[template_data_pid]->addVLine(1390,"",6,1,1); //FIXME: bk comb
		//;cholders[template_data_pid]->addVLine(695,"",6,1,1);
		//;cholders[template_data_pid]->setBordersY(0,15000);
		cholders[template_data_pid]->setLegendOptions(.3,.25);


		cholders[input_templates_pid]->setLegDrawSymbol("lp");
		cholders[input_templates_pid]->setTitleX(x_id);
		cholders[input_templates_pid]->setTitleY("(arb. units)");
		cholders[input_templates_pid]->setXaxisOff(1.1);
		cholders[input_templates_pid]->setYaxisOff(1.5);
		cholders[input_templates_pid]->setLegendOptions(.3,.2);
		
		cholders[input_templates_pid]->save("pdf");
		cholders[template_data_pid]->save("pdf");
		cholders[gendata_pid]->save("pdf");

		if(realdata)delete realdata;
		if(gendata)delete gendata;
	}
	
	delete colours;
	delete styles;
	delete_canvas_holders();
}
void FitPlots::plot_rel_precision_gaussian()
{
	outfile = fm->outfile();



	/**************************************
          Apply Gaussian fits to N Events distributions
          to get mean and sigma
	 *************************************/
	std::cout << "Fitting Relative Precision" << std::endl;
	//Add name to plots so can output for each sys study.
	std::string sys_name = fm->sys_study_name();

	std::vector<FitSetup*> fits = fm->fit_setups();
	for(std::vector<FitSetup*>::iterator fit = fits.begin();
	    fit != fits.end();fit++){
		FitSetup *fs = *fit;
		std::string fit_id = fs->fit_id();
		std::vector<std::string> templates_n = fs->template_norm_names();
		std::cout << "N templates: " << templates_n.size() << std::endl;
		for(std::vector<std::string>::iterator id = templates_n.begin();
		    id != templates_n.end();id++){
			std::cout << "Fitting rel precision for " << *id << std::endl;
	
			std::cout << "NGauss2 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
			if(id->find("tt_total") != std::string::npos){
				
				TH1F* Rel_pre_h =dynamic_cast<TH1F*> (fm->output_histo("rel_precision_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
		
				//Initialise Variables for fitting gaussian to Nevents
 				RooRealVar grp("gnevts",("Rel precision for "+fm->id_label(*id)).c_str(),-1,1);
 				RooRealVar grp_mean("mean","Mean of Error",Rel_pre_h->GetMean(1),Rel_pre_h->GetMean(1)-Rel_pre_h->GetRMS(1)/2,Rel_pre_h->GetMean(1)+Rel_pre_h->GetRMS(1)/2);
 				RooRealVar grp_sigma("sigma","Sigma of Error",Rel_pre_h->GetRMS(1),0,Rel_pre_h->GetRMS(1)*2);
	
 				RooGaussian rpgauss("rpgauss","gaussian pdf", grp,grp_mean,grp_sigma);
		
 				RooDataHist* rp_dhist = new RooDataHist("nrp_dhist","DataHist for rp",RooArgList(grp),Rel_pre_h); 
		
 				rpgauss.fitTo(*rp_dhist,RooFit::PrintLevel(-1)) ;
			
				std::cout << "Plotting for " << *id << std::endl;
				TCanvas *c_g = new TCanvas(("Rel_precision_"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
				RooPlot* frame_g = grp.frame(Name("Rel precision"),Title("Gaussian"));

				frame_g->SetTitle("");
				frame_g->SetTitleOffset(1.3,"Y");
				rp_dhist->plotOn(frame_g);
				rpgauss.plotOn(frame_g);
				rpgauss.paramOn(frame_g,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));
				int y_max = Rel_pre_h->GetMaximum();
				frame_g->SetAxisRange(0,y_max*1.3,"Y");
				c_g->cd(); frame_g->Draw();
				c_g->SaveAs((fm->output_dir()+"Relative_precision_"+*id+"_"+fit_id+"_"+sys_name+".pdf").c_str());
				c_g->Write();
				delete c_g;
				delete frame_g;
				delete Rel_pre_h;
				delete rp_dhist;
				
				std::cout << "Fitting  cs err for " << *id << std::endl;
				TH1F* cs_err_h =dynamic_cast<TH1F*> (fm->output_histo("sigma_err_"+*id+"_"+fit_id+"_"+sys_name)->Clone());
			std::cout << "NGauss2.1 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
		
				//Initialise Variables for fitting gaussian to Nevents
				double ntt = fs->nscaled("tt_total");
				double sqrtntt = sqrt(fs->nscaled("tt_total"));
				//double cs = 157.5;
				double cs = 225;
				double stat_err_cs = 3*(sqrtntt/ntt)*cs;
				double hmean = cs_err_h->GetMean(1);
				double hrms = cs_err_h->GetRMS(1);
   				RooRealVar cs_err("gnevts",("CS error for "+fm->id_label(*id)).c_str(),100,hmean-(hrms*3),hmean+(hrms*3));
 				RooRealVar cs_err_mean("mean","Mean of Error",hmean,hmean-hrms/2,hmean+hrms/2);
 				RooRealVar cs_err_sigma("sigma","Sigma of Error",hrms,0,hrms*2);
	
 				RooGaussian *cs_errgauss = new RooGaussian("cs_errgauss","gaussian pdf", cs_err,cs_err_mean,cs_err_sigma);
		
  				RooDataHist* cs_err_dhist = new RooDataHist("ncs_err_dhist","DataHist for cs_err",RooArgList(cs_err),cs_err_h); 
		
				//FIXME: causing crash in smear sys study.
				//cs_errgauss->fitTo(*cs_err_dhist,RooFit::PrintLevel(-1)) ;

				//				std::cout << "cs_errgauss: " <<  cs_errgauss<< std::endl;
				std::cout << "cs_err_dhist: " << cs_err_dhist << std::endl;
				std::cout << "cs_err_h: " << cs_err_h << std::endl;
			
			std::cout << "NGauss2.2 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
 				std::cout << "Plotting for " << *id << std::endl;
 				c_g = new TCanvas(("Rel_precision_"+*id+"_"+fit_id+"_"+sys_name).c_str(), "canvas",800,800);
 			        frame_g = cs_err.frame(Name("Rel precision"),Title("Gaussian"));
 				cs_err_dhist->plotOn(frame_g);
				//cs_errgauss.plotOn(frame_g);
				//cs_errgauss.paramOn(frame_g,Layout(0.5,0.92,0.92),Format("NEU",AutoPrecision(0)));
				y_max = cs_err_h->GetMaximum();
				frame_g->SetAxisRange(0,y_max*1.5,"Y");
	////
	////			//frame_g->SetAxisRange(6,8,"X"); // For 1085/pb
 				c_g->cd(); frame_g->Draw();
				c_g->SaveAs((fm->output_dir()+"CS_fit_err_"+*id+"_"+fit_id+"_"+sys_name+".pdf").c_str());
 				c_g->Write();
				delete c_g;
				delete frame_g;
				delete cs_err_h;
				delete cs_err_dhist;
				delete cs_errgauss;
	////		std::cout << "NGauss2.3 after fit: " <<fm->ngauss(*id+"_mean_"+fit_id+"_"+sys_name)->getVal() << std::endl;
 			}
		}
	}

 	delete_canvas_holders();
}

 void FitPlots::plot_fit_checks()
  {
 	std::string sys_name = fm->sys_study_name();
// 	  std::cout << " Plotting ngen events " << std::endl;
// 	TH1* h_gen = fm->fit_par_data()->createHistogram("ngen",-40); 
// 	h_gen->SetAxisRange(0.0,200.0,"Y");
// 	TCanvas *c_gen = new TCanvas("NGen", "canvas",800,800);
// 	c_gen->cd();h_gen->Draw();
// 	c_gen->SaveAs((fm->output_dir()+"NGenerated0_1000_"+sys_name+".pdf").c_str());
// 	c_gen->Write();
// 	delete h_gen;

// 	  std::cout << " Plotting ngen2 events " << std::endl;
// 	TH1* h_gen2 = fm->fit_par_data()->createHistogram("ngen2",-40); 
// 	h_gen2->SetAxisRange(0.0,200.0,"Y");
// 	TCanvas *c_gen2 = new TCanvas("NGen2", "canvas",800,800);
// 	c_gen2->cd();h_gen2->Draw();
// 	c_gen2->SaveAs((fm->output_dir()+"NGenerated1000_2000_"+sys_name+".pdf").c_str());
// 	c_gen2->Write();
// 	delete h_gen2;

// 	  std::cout << " Plotting ngen3 events " << std::endl;
// 	TH1* h_gen3 = fm->fit_par_data()->createHistogram("ngen3",-40); 
// 	h_gen3->SetAxisRange(0.0,200.0,"Y");
// 	TCanvas *c_gen3 = new TCanvas("NGen3", "canvas",800,800);
// 	c_gen3->cd();h_gen3->Draw();
// 	c_gen3->SaveAs((fm->output_dir()+"NGenerated2000_3000_"+sys_name+".pdf").c_str());
// 	c_gen3->Write();
// 	delete h_gen3;
// 	  std::cout << " Plotting ngen4 events " << std::endl;
// 	TH1* h_gen4 = fm->fit_par_data()->createHistogram("ngen4",-40); 
// 	h_gen4->SetAxisRange(0.0,200.0,"Y");
// 	TCanvas *c_gen4 = new TCanvas("NGen4", "canvas",800,800);
// 	c_gen4->cd();h_gen4->Draw();
// 	c_gen4->SaveAs((fm->output_dir()+"NGenerated3000_4000_"+sys_name+".pdf").c_str());
// 	c_gen4->Write();
// 	delete h_gen4;
	  std::cout << " Plotting ngenall events " << std::endl;
	TH1* h_genall = fm->fit_par_data()->createHistogram("ngenall",-40); 
	//h_genall->SetAxisRange(0.0,200.0,"Y");
	TCanvas *c_genall = new TCanvas("NGenall", "canvas",800,800);
	c_genall->cd();h_genall->Draw();
	c_genall->SaveAs((fm->output_dir()+"NGeneratedAll_"+sys_name+".pdf").c_str());
	c_genall->Write();
	delete h_genall;
// 	  std::cout << " Plotting nMCData events " << std::endl;
// 	TH1* h_mcdata = fm->fit_par_data()->createHistogram("nMCData",-40); 
// 	TCanvas *c_mcdata = new TCanvas("NMCData", "canvas",800,800);
// 	c_mcdata->cd();h_mcdata->Draw();
// 	c_mcdata->SaveAs((fm->output_dir()+"NExpected_"+sys_name+".pdf").c_str());
// 	c_mcdata->Write();
// 	delete h_mcdata;

// 	 //	NLL distribution
//   	TCanvas *c10 = new TCanvas("NLL_distribution", "canvas",800,800);
//  	RooPlot* frame10 = fm->mgr()->plotNLL();
//  	c10->cd(); frame10->Draw();	
//  	c10->SaveAs((fm->output_dir()+"NLL_distribution.pdf").c_str());
//  	c10->Write();

//  	//TH1* h_nll = fm->mgr()->fitParDataSet().createHistogram("NLL",-40); 
//  	TCanvas *c_nll = new TCanvas("NLL", "canvas",800,800);
//  	c_nll->cd();h_nll->Draw();
//  	c_nll->SaveAs((fm->output_dir()+"NLL.pdf").c_str());
//  	c_nll->Write();
//  	delete h_nll;
//  }
// 	TH1* h_gen = fm->mgr()->fitParDataSet().createHistogram("ngen",-40); 
// 	TCanvas *c_gen = new TCanvas("NGen", "canvas",800,800);
// 	c_gen->cd();h_gen->Draw();
// 	c_gen->SaveAs((fm->output_dir()+"NGenerated.pdf").c_str());
// 	c_gen->Write();
// 	delete h_gen;


// 	fm->mgr()->fitParDataSet().Print();
// 	std::cout << " WARNING: The program will segfault if you didn't use a MCStudy which generated tt_gen, wzq_gen and st_gen separately.\n e.g. if you don't use single top constraint."<<std::endl;
// 	std::vector<std::string> templates_n = fm->template_norm_names();
// 	if(fm->gauss_constraints_used().size() > 0){
// 		for(std::vector<std::string>::iterator id = templates_n.begin();
// 		    id != templates_n.end();id++){
// 			TH1* h_v_gen = fm->mgr()->fitParDataSet().createHistogram(("nevts_"+*id+"_gen").c_str(),-40); 
// 			TCanvas *c_v_gen = new TCanvas(("NGen_"+*id).c_str(), "canvas",800,800);
// 			c_v_gen->cd();h_v_gen->Draw();
// 			c_v_gen->SaveAs((fm->output_dir()+"NGenerated_"+*id+".pdf").c_str());
// 			c_v_gen->Write();
// 			delete h_v_gen;
// 		}
// 	}

// 	// 	std::cout << "ttbar gen: " << fm->mgr()->fitParDataSet().get()->getRealValue("nevts_tt_gen") << " wzq gen: " << fm->mgr()->fitParDataSet().get()->getRealValue("nevts_wzq_gen") << " st gen: " << fm->mgr()->fitParDataSet().get()->getRealValue("nevts_st_gen") << std::endl; 

// 	//Fit gaussian to NGen_st
//         if(find(templates_n.begin(), templates_n.end(), "st") != templates_n.end() && fm->constrained("st")){
		
// 		TH1* h_gst_gen = fm->mgr()->fitParDataSet().createHistogram("nevts_st_gen",-40); 
// 		RooRealVar st_gngen("st_gngen","Number of generated events",0,20);
// 		RooRealVar st_gngen_mean("mean","Mean of Error",10,0,20);
// 		RooRealVar st_gngen_sigma("sigma","Sigma of Error",5,0,15);
// 		RooGaussian stngen_gauss("stngen_gauss","gaussian pdf", st_gngen,st_gngen_mean,st_gngen_sigma);
// 		RooDataHist* NGen_st_dhist = new RooDataHist("ngen_st_dhist","DataHist for N Generated single top",RooArgList(st_gngen),h_gst_gen);
		
// 		stngen_gauss.fitTo(*NGen_st_dhist,RooFit::PrintLevel(-1));
		
// 		TCanvas *c_gst_gen = new TCanvas("NGen_gst", "canvas",800,800);
// 		RooPlot* frame_gst_ngen = st_gngen.frame(Name("NGen"),Title("Gaussian"));
// 		NGen_st_dhist->plotOn(frame_gst_ngen);
// 		stngen_gauss.plotOn(frame_gst_ngen);
// 		stngen_gauss.paramOn(frame_gst_ngen);
// 		c_gst_gen->cd();frame_gst_ngen->Draw();
// 		c_gst_gen->SaveAs((fm->output_dir()+"NGenerated_st_gaussian.pdf").c_str());
// 		c_gst_gen->Write();
// 		delete h_gst_gen;
// 		delete NGen_st_dhist;
// 		delete c_gst_gen;
// 	}
}

// void FitPlots::plot_input_histos()
// {
// 	std::vector<int> *colours = new std::vector<int>();
// 	colours->push_back(kRed);
// 	colours->push_back(kBlue);
// 	colours->push_back(kGreen);
// 	colours->push_back(kBlack);
// 	colours->push_back(kRed);

// 	std::vector<int> *styles = new std::vector<int>();
// 	styles->push_back(2);
// 	styles->push_back(9);
// 	styles->push_back(7);
// 	styles->push_back(1);
// 	styles->push_back(7);
	
// 	cholders["template_histos"] = new CanvasHolder();
// 	cholders["template_histos"]->setCanvasTitle(fm->output_dir()+"template_histos");
	
// 	cholders["template_histos"]->setOptStat(000000);
	
// 	std::vector<std::string> templates_s = fm->template_shape_names();
// 	for(std::vector<std::string>::iterator id = templates_s.begin();
// 	    id != templates_s.end();id++){
// 		cholders["template_histos"]->addHistoStacked(fm->histo(*id),*id,"HISTE");
// 	}
// 	cholders["template_histos"]->setLineColors(*colours);
// 	cholders["template_histos"]->setLineStyles(*styles);
	
// 	cholders["template_histos"]->write(fm->outfile()); 
// 	cholders["template_histos"]->save("pdf");

// }

void FitPlots::plot_numbers_vs_sys()
{ 
	outfile = fm->outfile();
	std::vector<FitSetup*> fits = fm->fit_setups();
	std::vector<FitSetup*>::iterator fit = fits.begin();
	FitSetup *fs = *fit;
	std::string fit_id = fs->fit_id();

	
	std::cout << "Plotting sys results" << std::endl;
	std::vector<std::string> templates = fs->template_norm_names();
        std::map<std::string,std::vector<std::pair<double,double> > > sys_study_results = fm->get_sys_study_results();
	std::vector<std::string> study_names = fm->sys_study_names();//give you the order in which the studies were input.

	cholders["template_histos"] = new CanvasHolder();
	cholders["template_histos"]->setCanvasTitle(fm->output_dir()+"template_histos");

	//	std::vector<TH1F*> sys_error;
	//std::vector<TH1F*> sys_nevents;

	std::cout << "getting sys histos from fm " << std::endl;
	std::map<std::string, TH1F*> histos = fm->output_histos_1D();

	for(std::map<std::string, TH1F*>::iterator h = histos.begin();
	    h!= histos.end();h++){
		if((h->first).find("Sys_") == std::string::npos){
			continue;
		}
		cholders[h->first] = new CanvasHolder();
		cholders[h->first]->setCanvasTitle(fm->output_dir()+h->first);
		h->second->LabelsDeflate("X");
		cholders[h->first]->addHisto(h->second,"","PE");
		cholders[h->first]->setOptStat(000000);
		//	cholders["Sys_nevents_"+*id]->setBordersX(0,study_names.size()+4);
		cholders[h->first]->save("pdf");
		//cholders[h->first]->setCanvasTitle(h->first);
		h->second->Write((h->first).c_str());
	}

	delete_canvas_holders();

			
}

void FitPlots::plot_numbers_vs_bin_width()
{ 
	outfile = fm->outfile();
	std::vector<FitSetup*> fits = fm->fit_setups();
	std::vector<FitSetup*>::iterator fit = fits.begin();
	FitSetup *fs = *fit;
	std::string fit_id = fs->fit_id();

	std::cout << "Plotting Rebin study results" << std::endl;
	std::vector<std::string> templates = fs->template_norm_names();
        std::map<std::string,std::vector<std::pair<double,double> > > sys_study_results = fm->get_sys_study_results();
	std::vector<std::string> study_names = fm->sys_study_names();//give you the order in which the studies were input.

        std::map<std::string,std::map<std::string,double> > sys_study_results2 = fm->get_sys_study_results2();

	double max_x = -1;
	double max_y = -1;

	for(size_t k = 0;k < templates.size();k++){
		std::map<std::string,double> nevts_res = sys_study_results2["Nevts_"+templates[k]];

		const int nbins = nevts_res.size();
		double y_axis[nbins];
		double y_ERR[nbins];
		double x_axis[nbins];
		double x_ERR[nbins];
		double ye_axis[nbins];
		double ye_ERR[nbins];
		double yre_axis[nbins];
		double yre_ERR[nbins];
		double x2_axis[nbins];
		double ycs_axis[nbins];
		double ycs_ERR[nbins];
		double ycse_axis[nbins];
		double ycse_ERR[nbins];
		double x_label_axis[nbins];

		int l = 0;
		for(std::map<std::string,double>::iterator study = nevts_res.begin();
		study != nevts_res.end();study++){
				x_axis[l] = sys_study_results2["BinWidth_"+templates[k]][study->first];
				x_ERR[l] = 0;
				y_axis[l] = sys_study_results2["Nevts_"+templates[k]][study->first];
				y_ERR[l] = sys_study_results2["Err_"+templates[k]][study->first];
				
				ye_axis[l] = sys_study_results2["Err_"+templates[k]][study->first];
				ye_ERR[l] = sys_study_results2["ErrErr_"+templates[k]][study->first];
				
				yre_axis[l] = sys_study_results2["RelErr_"+templates[k]][study->first];
				yre_ERR[l] = sys_study_results2["RelErrErr_"+templates[k]][study->first];

				ycs_axis[l] = sys_study_results2["CS_"+templates[k]][study->first];
				ycs_ERR[l] = sys_study_results2["CSErr_"+templates[k]][study->first];
				
				ycse_axis[l] = sys_study_results2["Errpb_"+templates[k]][study->first];
				ycse_ERR[l] = sys_study_results2["ErrErrpb_"+templates[k]][study->first];

				x_label_axis[l] = l;

				//	std::cout << "NBINS  " << nbins <<std::endl;
				//std::cout << "bin w " << sys_study_results2["BinWidth_"+templates[k]][study->first] <<std::endl;
				//				std::cout << "ne " << sys_study_results2["Nevts_"+templates[k]][study->first] <<std::endl;
				//std::cout << "err " << sys_study_results2["Err_"+templates[k]][study->first] <<std::endl;
				//std::cout << "l " << l<<std::endl;
				
				l++;
		}
		cholders["Sys2_nevents_"+templates[k]] = new CanvasHolder();
		cholders["Sys2_err_"+templates[k]] = new CanvasHolder();
		cholders["Sys2_relerr_"+templates[k]] = new CanvasHolder();
		cholders["Sys2_cs_"+templates[k]] = new CanvasHolder();
		cholders["Sys2_cserr_"+templates[k]] = new CanvasHolder();
		std::string drawopt = "p";
		
		//		ch.setTitle("");
// 		che.setTitle("");
// 		chre.setTitle("");
		
		cholders["Sys2_nevents_"+templates[k]]->setTitleX("Bin Width (GeV)");
		cholders["Sys2_err_"+templates[k]]->setTitleX("Bin Width (GeV)");
		cholders["Sys2_relerr_"+templates[k]]->setTitleX("Bin Width (GeV)");
		cholders["Sys2_cs_"+templates[k]]->setTitleX("Bin Width (GeV)");
		cholders["Sys2_cserr_"+templates[k]]->setTitleX("Bin Width (GeV)");
		
		cholders["Sys2_nevents_"+templates[k]]->setTitleY(("Number of "+fm->id_label(templates[k])+" events").c_str());
		cholders["Sys2_err_"+templates[k]]->setTitleY(("Error on N "+fm->id_label(templates[k])+" events").c_str());
		cholders["Sys2_relerr_"+templates[k]]->setTitleY(("Rel. error on N "+fm->id_label(templates[k])+" events").c_str());
		cholders["Sys2_cs_"+templates[k]]->setTitleY((fm->id_label(templates[k])+" cross section (pb)").c_str());
		cholders["Sys2_cserr_"+templates[k]]->setTitleY(("Error on "+fm->id_label(templates[k])+" cross section (pb)").c_str());


		cholders["Sys2_nevents_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_nevents_"+templates[k]).c_str());
		cholders["Sys2_err_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_error_"+templates[k]).c_str());
		cholders["Sys2_relerr_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_relerr_"+templates[k]).c_str());

		cholders["Sys2_cs_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_cs_"+templates[k]).c_str());
		cholders["Sys2_cserr_"+templates[k]]->setCanvasTitle((fm->output_dir()+"Sys2_cserror_"+templates[k]).c_str());

		TGraphErrors g_n(nbins,x_axis ,y_axis ,x_ERR ,y_ERR);
		TGraphErrors g_e(nbins,x_axis ,ye_axis ,x_ERR ,ye_ERR);
		TGraphErrors g_re(nbins,x_axis ,yre_axis ,x_ERR ,yre_ERR);
		TGraphErrors g_cs(nbins,x_axis ,ycs_axis ,x_ERR ,ycs_ERR);
		TGraphErrors g_cse(nbins,x_axis ,ycse_axis ,x_ERR ,ycse_ERR);
				std::cout << "a6" << std::endl;

		cholders["Sys2_nevents_"+templates[k]]->addGraph(&g_n,"name1","",drawopt); 
		cholders["Sys2_nevents_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_nevents_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_nevents_"+templates[k]]->setLineSizeGraph(1);
		//cholders["Sys2_nevents_"+templates[k]]->setLineColors(colour);

		cholders["Sys2_err_"+templates[k]]->addGraph(&g_e,"name21","",drawopt); 
		cholders["Sys2_err_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_err_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_err_"+templates[k]]->setLineSizeGraph(1);

		cholders["Sys2_relerr_"+templates[k]]->addGraph(&g_re,"name31","",drawopt); 
		cholders["Sys2_relerr_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_relerr_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_relerr_"+templates[k]]->setLineSizeGraph(1);

		cholders["Sys2_cs_"+templates[k]]->addGraph(&g_cs,"name41","",drawopt); 
		cholders["Sys2_cs_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_cs_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_cs_"+templates[k]]->setLineSizeGraph(1);

		cholders["Sys2_cserr_"+templates[k]]->addGraph(&g_cse,"name51","",drawopt); 
		cholders["Sys2_cserr_"+templates[k]]->setMarkerSizeGraph(1);
		cholders["Sys2_cserr_"+templates[k]]->setMarkerStylesGraph(4);
		cholders["Sys2_cserr_"+templates[k]]->setLineSizeGraph(1);

		cholders["Sys2_nevents_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_nevents_"+templates[k]]->setYaxisOff(2.1);
		cholders["Sys2_err_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_err_"+templates[k]]->setYaxisOff(2.0);
		cholders["Sys2_relerr_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_relerr_"+templates[k]]->setYaxisOff(1.5);

		cholders["Sys2_cs_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_cs_"+templates[k]]->setYaxisOff(2.1);
		cholders["Sys2_cs_"+templates[k]]->setBordersY(140,180);
		

		cholders["Sys2_cserr_"+templates[k]]->setXaxisOff(1.1);
		cholders["Sys2_cserr_"+templates[k]]->setYaxisOff(2.1);

   
		cholders["Sys2_nevents_"+templates[k]]->setOptStat(00000);
// 		cholders["Sys2_nevents_"+templates[k]]->setBordersY(0,10);
// 		cholders["Sys2_nevents_"+templates[k]]->setBordersX(0,10);
		cholders["Sys2_nevents_"+templates[k]]->save("pdf");

		cholders["Sys2_err_"+templates[k]]->setOptStat(00000);
		//cholders["Sys2_err_"+templates[k]]->setBordersY(0,);
		//cholders["Sys2_err_"+templates[k]]->setBordersX(0,0);
		cholders["Sys2_err_"+templates[k]]->save("pdf");

		cholders["Sys2_relerr_"+templates[k]]->setOptStat(00000);
		//cholders["Sys2_relerr_"+templates[k]]->setBordersY(0,);
		//cholders["Sys2_relerr_"+templates[k]]->setBordersX(0,0);
		cholders["Sys2_relerr_"+templates[k]]->save("pdf");

		cholders["Sys2_cs_"+templates[k]]->setOptStat(00000);
		cholders["Sys2_cs_"+templates[k]]->save("pdf");

		cholders["Sys2_cserr_"+templates[k]]->setOptStat(00000);
		cholders["Sys2_cserr_"+templates[k]]->save("pdf");
		
		std::cout << "FitPlots:plot_sy_vs_numbers. Plotting correlation vs bin width" << std::endl;
		//Plot correlation vs bin width
		for(size_t m = 0;m < templates.size();m++){
			if(k != m){
				double ycorr_axis[nbins];
				double ycorr_ERR[nbins];
				int l = 0;
				for(std::map<std::string,double>::iterator study = nevts_res.begin();
				    study != nevts_res.end();study++){
					ycorr_axis[l] = sys_study_results2["Nevts_"+templates[k]+"_vs_Nevts_"+templates[m]][study->first];
					ycorr_ERR[l] = 0;
					l++;
				}
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]] = new CanvasHolder();
				std::string drawopt = "p";
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setTitleX("Bin Width (GeV)");
				
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setTitleY(("Correlation "+fm->id_label(templates[k])+" nevts "+fm->id_label(templates[m])+" nevts").c_str());
				
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setCanvasTitle((fm->output_dir()+"Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]).c_str());
				
				TGraphErrors gcorr_n(nbins,x_axis ,ycorr_axis ,x_ERR ,ycorr_ERR);

				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->addGraph(&gcorr_n,"name1","",drawopt); 
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setMarkerSizeGraph(1);
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setMarkerStylesGraph(4);
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setLineSizeGraph(1);
				
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setXaxisOff(1.1);
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setYaxisOff(1.5);
				
				
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->setOptStat(00000);
				cholders["Sys2_Corr_nevts_"+templates[k]+"_vs_nevts_"+templates[m]]->save("pdf");
			}
		}

	}
	delete_canvas_holders();

}

void FitPlots::plot_sys_templates_overlay()
{
	std::vector<FitSetup*> fits = fm->fit_setups();
	std::vector<FitSetup*>::iterator fit = fits.begin();
	FitSetup *fs = *fit;
	std::string fit_id = fs->fit_id();

        std::vector<int> *colours = new std::vector<int>();
        colours->push_back(kRed);
        colours->push_back(kBlue);
        colours->push_back(kGreen);
        colours->push_back(kRed+3);
        colours->push_back(kGray+3);
        colours->push_back(kMagenta+2);
        colours->push_back(kOrange+7);
        colours->push_back(kOrange-7);
        colours->push_back(kViolet-7);
	std::string x_id = fm->id_label(fs->observable_id("x"));

	for(std::map<std::string,CanvasHolder*>::iterator c = choldersDNR.begin();
	    c != choldersDNR.end(); c++){
                c->second->setLineColors(*colours);
                c->second->setTitleX(x_id);
                c->second->setTitleY("(arb. units)");
                c->second->setXaxisOff(1.1);
                c->second->setYaxisOff(1.5);
		c->second->setLegDrawSymbol("lp");
		c->second->setLegendOptions(.3,.2);

                c->second->save("pdf");

	}
}

void FitPlots::delete_canvas_holders()
{
	for(std::map<std::string,CanvasHolder*>::iterator c = cholders.begin();
	    c != cholders.end(); c++){
		delete c->second;
		c->second = NULL;
	}
	cholders.clear();
}

void FitPlots::set_canvas_style(TCanvas *c)
{
  c->SetFillColor(0);
  c->SetBorderMode(0);
  c->SetBorderSize(2);
  c->SetTickx();
  c->SetTicky();

  c->SetFrameFillStyle(0);
  c->SetFrameBorderMode(0);


}

void FitPlots::set_default_style()
{
  if(!theStyle) {
    theStyle = new TStyle(getRnd().c_str(),getRnd().c_str());
  }
  theStyle->SetCanvasDefH(800);
  theStyle->SetCanvasDefW(800);

  // Set pad grid and tick marks on opposite side of the axis
  //theStyle->SetPadGridX(1);
  //theStyle->SetPadGridY(1);

  theStyle->SetPadTickX(1);
  theStyle->SetPadTickY(1);

  // Set background color
  theStyle->SetCanvasColor(0);
  theStyle->SetStatColor(kWhite);

  // Title config
  theStyle->SetOptTitle(1);
  //  theStyle->SetTitleW(0.41);
  //  theStyle->SetTitleH(0.05);

//  theStyle->SetTitleX(0.16);
//  theStyle->SetTitleY(0.93);
  theStyle->SetTitleX(0.01);
  theStyle->SetTitleY(0.99);
  theStyle->SetTitleColor(1);
  theStyle->SetTitleTextColor(1);
  theStyle->SetTitleFillColor(0);
  theStyle->SetTitleBorderSize(1);
  theStyle->SetStatFont(42);
  theStyle->SetStatFontSize(0.025);
  theStyle->SetTitleFont(42);

  theStyle->SetTitleFontSize(0.04);
  double theCanleftmargin   = 0.15;
  double theCanrightmargin  = 0.07;
  double theCantopmargin    = 0.07;
  double theCanbottommargin = 0.15;
  theStyle->SetPadLeftMargin(theCanleftmargin);
  theStyle->SetPadRightMargin(theCanrightmargin);
  theStyle->SetPadTopMargin(theCantopmargin);
  theStyle->SetPadBottomMargin(theCanbottommargin);
  theStyle->SetOptStat(0);
  //theStyle->SetOptStat(theOptStat);


  theStyle->cd();
}

std::string FitPlots::getRnd(){
  int r=rand()%10000;
  char buf[7];
  sprintf (buf,"_R%d",r);
  return string(buf);
}

