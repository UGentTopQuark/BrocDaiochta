#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <sstream>
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include <iostream>
#include <map>
#include <vector>

#include "HistoMerger/MergeHistos.h"
#include "HistoSmoother/SmoothHistos.h"
#include "LuminosityProvider/LuminosityProvider.h"
#include "FitManager.h"
#include "FitPlots.h"
#include "FitPrinter.h"
#include "ConfigReader/ConfigReader.h"
#include "FitSetup.h"

/***
 * 
 * \class InputManager
 *
 * \brief Manages input from config reader
 *
 * Reads in everything from config reader, determines how the
 * fit should be set up. Combines input histograms.
 * 
 *
 ***/


class InputManager{
        public:
	        InputManager(eire::ConfigReader *config_reader,std::string section);
	        ~InputManager();

		void run_fit_templates();

        private:
		void read_config();
		void read_systematic_section(std::string sys_section);
		void setup_fit(int i); //< fills all info needed for the fit to FitSetup
		void set_template_histo_ids(int i);//<reads shape,norm and mc ids. Called per sys study.
		void set_ranges();//<Set ranges for number of fit events
		void check_for_additional_scale_factors(std::string section,int nstudies);
		/***
		 * h_section_id is the section which defines which samples will be merged 
		 * to make up this histogram. i is the sys_study iterator
		***/
		std::vector<TH1D*> scale_and_add_histos(MergeHistos *mhistos,int i,bool use_real_data, std::string h_section_id);
		
		TFile *outfile; 
		std::string mc_analysis;//< analysis set in CSProvider
		std::string output_directory;
		bool use_real_data; //< use data from Dataversion.root file
		/*** 
		 * section(s) describing histogram you want to fit. Section defines
		 * histo_name, cutset and rebin. If two sections defined use simultaneous fit.
		 ***/
		std::vector<std::string> histos_to_fit;

		std::string conf_section; //<global unless otherwise specified
		/***
		 *  ids for template shapes. each id should refer to a section in the config.
		 *  The section contains the list of samples to be merged to make the template.
		 ***/
		std::vector<std::string> template_shape_ids;
		/***
		 *  ids for normalisation of template, only need to set if different
		 *  to ids used to create shape. 
		 ***/
		std::vector<std::string> template_norm_ids; 
		/***
		 *  All histos to be read in. Not necessarily used in fit.
		 ***/
		std::vector<std::string> available_mc_histos;

		/***
		 * To run a systematic study specify your section with sys_study 
		 * in global. In the section specify the study names, which will be used 
		 * to identify the results. 
		 * template_sections and histo_sections can not be defined at same time.
		 * sys_study_versions or sys_study_cutsets can by used in conjunction with 
		 * template_sections or histo_sections.
		 ***/
		bool run_multi_sys_study;	
		std::vector<std::string> sys_study_names;
		/***
		 * If you want the input template shapes to vary specify sections using
		 * sys_template_sections. template_shape_ids must be defined in this section.
		 * If only one section defined will be used for all sys_studies. Otherwise must 
		 * define a section for each study.
		 * separate_data_hist_section can also be defined which determines what should 
		 * make up the pseudo-data. 
		 ***/
		std::vector<std::string> sys_template_sections;
		/***
		 * If you want the template shapes to stay the same and the pseudo-data to vary
		 * specify sections using sys_histo_sections. Each section defines sig_channel
		 * and bkg channel to be used to make up pseudo-data.
		 * If only one section defined will be used for all sys_studies. Otherwise must 
		 * define a section for each study.
		 ***/
		std::vector<std::string> sys_histo_sections;
		/***
		 * If you want the input root files to vary for each sys_study define
		 * the different versions with sys_study_versions. If sys_study_versions not
		 * defined values will be taken from input section (usually global).
		 * If only one version defined will be used for all sys_studies. Otherwise must 
		 * define a version for each study.
		 ***/		
		std::vector<std::string> sys_study_versions;
		/***
		 * If you want the histo cutset to vary for each sys_study define
		 * the different cutsets with 'sys_study_cutsets'.  
		 * This variable must be defined in histos_to_fit section since
		 * different histos may want different cutsets, especially when doing 
		 * simultaneous fitting. If sys_study_cutsets not defined values will 
		 * be taken from histos_to_fit 'cutset'.
		 * If only one cutset defined will be used for all sys_studies. Otherwise must 
		 * define a cutset for each study.
		 ***/		
		std::map<std::string,std::vector<std::string> > sys_histo_to_fit_cutsets;
		/***
		 * To run a multi rebin study specify your section with sys_study 
		 * in global then in the section define multi_rebin = 1:2:etc 
		 * where 1:2:etc is the rebin you want for each study.
		 * The program will run the fit for each rebin value
		 * and produce a plot of nfit events vs bin width
		 ***/
		bool run_multi_rebin_study;	
		std::vector<std::string> multi_rebin_bins;
		bool individual_templates_added; //already scaled and added to fit setup.
		/***
		 * If you want to apply additional scale factors to any of the datasets
		 * you must set apply_additional_scale_factors = true in the config,
		 * either in your sys section or in global.
		 * Then you set the scale factors with e.g scale_factors_Wjets = 1.3
		 * defined in the same section as the apply bool
		 * variables are saved here to be applied during fit setup.
		 * Used in study of W/Z uncertainty.
		 ***/
		std::map<std::string,std::vector<double> > additional_scale_factors;

		eire::ConfigReader *config_reader;
		FitManager *fm;
		FitPlots* fplot;
		FitPrinter* fprint;
		FitSetup *fs;

		static const bool verbose = true;
};

#endif
