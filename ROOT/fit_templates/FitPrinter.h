#ifndef FITPRINTER_H
#define FITPRINTER_H

#include "TF1.h"
#include "TH1D.h"
#include "TF2.h"
#include "TH2D.h"
#include "TH2.h"
#include "TFile.h"
#include "CanvasHolder/Canvas_Holder.h"

#include <RooRandom.h>
#include <RooRandomizeParamMCSModule.h>
#include <RooGaussian.h>
#include <RooPoisson.h>
#include <RooGlobalFunc.h>
#include <RooRealVar.h>
#include <RooChi2Var.h>
#include <RooPoisson.h>
#include <RooGaussian.h>
#include <RooPlot.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooExponential.h>
#include <RooAddPdf.h>
#include <RooProdPdf.h>
#include <RooFitResult.h>
#include <RooArgusBG.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooLandau.h>
#include <RooMinuit.h>
#include <RooMCStudy.h>
#include <RooHistPdf.h>
#include <RooArgSet.h>
#include <RooAbsArg.h>
#include <RooArgList.h>
#include <RooConstVar.h>
#include <RooWorkspace.h>
#include <RooGenFitStudy.h>
#include <RooStudyManager.h>
#include "FitManager.h"
#include "ConfigReader/ConfigReader.h"
#include "HistoMerger/CrossSectionProvider/CrossSectionProvider.h"
#include "HistoMerger/BranchingRatio/BranchingRatio.h"

class FitPrinter{
        public:
		FitPrinter(FitManager* fm);
		void print_results_table();
		void print_cross_section_table(eire::ConfigReader *config_reader, bool is_real_data = false);
		void print_sys_cross_section_table(eire::ConfigReader *config_reader, bool is_real_data = false);
		void print_misc_results();
		void print_cross_sections_real_data();
		void print_sys_table_numbers(eire::ConfigReader *config_reader);
		void print_sys_table_percent();
		void print_real_data_results();
		void print_correlation(bool is_real_data);
        private:
	     
		FitManager *fm;

		static const bool verbose = true;

};

#endif
