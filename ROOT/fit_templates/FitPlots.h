#ifndef FITPLOTS_H
#define FITPLOTS_H


#include "TF1.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TF2.h"
#include "TH2F.h"
#include "TH2.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TStyle.h"
#include "TGraphErrors.h"
#include "CanvasHolder/Canvas_Holder.h"

#include <RooRandom.h>
#include <RooRandomizeParamMCSModule.h>
#include <RooGaussian.h>
#include <RooPoisson.h>
#include <RooGlobalFunc.h>
#include <RooRealVar.h>
#include <RooChi2Var.h>
#include <RooPoisson.h>
#include <RooGaussian.h>
#include <RooPlot.h>
#include <RooDataSet.h>
#include <RooDataHist.h>
#include <RooExponential.h>
#include <RooAddPdf.h>
#include <RooProdPdf.h>
#include <RooFitResult.h>
#include <RooArgusBG.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooLandau.h>
#include <RooMinuit.h>
#include <RooMCStudy.h>
#include <RooHistPdf.h>
#include <RooArgSet.h>
#include <RooAbsArg.h>
#include <RooArgList.h>
#include <RooConstVar.h>
#include <RooWorkspace.h>
#include <RooGenFitStudy.h>
#include <RooStudyManager.h>
#include <RooErrorVar.h>
#include "FitManager.h"

class FitPlots{
        public:
		FitPlots(FitManager* fm);
		~FitPlots();
		void fit_nevts_gaussian(bool plot_gauss = true);
		void plot_fit_error_gaussian(bool plot_gauss = true);
		void plot_rel_precision_gaussian();
		void plot_fit_pull_gaussian();
		void plot_constraint_mean();
		void plot_2D_histos();
		void plot_fit_checks();
		void plot_1D_correlation();
		void plot_2D_correlation();
		void plot_sample_event(bool is_real_data = false);
		void plot_input_histos();
		void plot_sys_templates_overlay();
		void plot_numbers_vs_sys();
		void plot_numbers_vs_bin_width();
        private:
		void delete_canvas_holders();
		void set_canvas_style(TCanvas *c);
		void set_default_style();
		std::string getRnd();

		TFile *outfile;
		std::map<std::string,CanvasHolder*> cholders;
		std::map<std::string,CanvasHolder*> choldersDNR;//<Not to be deleted until destructor
		FitManager *fm;
		TStyle *theStyle;


		static const bool verbose = true;
};

#endif
