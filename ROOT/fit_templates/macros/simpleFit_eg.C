/////////////////////////////////////////////////////////////////////////
//
// 'MULTIDIMENSIONAL MODELS' RooFit tutorial macro #305
// 
// Multi-dimensional p.d.f.s with conditional p.d.fs in product
// 
// pdf = gauss(x,f(y),sx | y ) * gauss(y,ms,sx)    with f(y) = a0 + a1*y
// 
//
// 07/2008 - Wouter Verkerke 
//
/////////////////////////////////////////////////////////////////////////

#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooPolyVar.h"
#include "RooProdPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TH1.h"
#include "TH1F.h"
using namespace RooFit ;



void simpleFit_eg()
{
	RooRealVar x_1("x1","x1",2,0,2) ;
	x_1.setBins(2);

	TH1F sig_h_1 ("sig_histo", "signal_histogram", 2,0,2);

	sig_h_1.SetBinContent(1,1000);
	sig_h_1.SetBinContent(2,600);

	TH1F sig_h_2 ("sig_histo", "signal_histogram", 2,0,2);


	sig_h_2.SetBinContent(1,400);
	sig_h_2.SetBinContent(2,1000);


	std::cout << "Integral2: " << sig_h_2.Integral() << std::endl;
	std::cout << "Integral1: " << sig_h_1.Integral() << std::endl;
	std::cout << "GetEntries: " << sig_h_1.GetEntries() << std::endl;
	std::cout << "Bin 1: " << sig_h_1.GetBinContent(1) << std::endl;
	std::cout << "Bin 2: " << sig_h_1.GetBinContent(2) << std::endl;

	//sig_h_1.Scale(0.01);
	//sig_h_2.Scale(0.1);

	TH1F *bin1 = new TH1F("bin1", "bin1", 100,0,3000);
	TH1F *bin2 = new TH1F("bin2", "bin2", 100,0,3000);
	TH1F *integ = new TH1F("integ", "integ", 100,0,20000);
	TH1F *res = new TH1F("res", "res", 100,0,3000);
	TH1F *res2 = new TH1F("res", "res", 100,0,3000);
	for(int i = 0; i < 1000;i++){

		//build model1
		RooRealVar nsig1("nsig1","fitted number of signal events",1600, 0, 2500) ;
		RooDataHist sig1_hist("sig_hist","signal histogram ",RooArgList(x_1),&sig_h_1);
		RooHistPdf sig1_pdf("sig_pdf", "" , x_1, sig1_hist);
		RooRealVar nsig2("nsig2","fitted number of signal events",1400, 0, 2500) ;
		RooDataHist sig2_hist("sig2_hist","signal histogram ",RooArgList(x_1),&sig_h_2);
		RooHistPdf sig2_pdf("si2g_pdf", "" , x_1, sig2_hist);
		
		if(i == 0){
			std::cout << "Integral: " << sig_h_1.Integral() << std::endl;
			std::cout << "GetEntries: " << sig_h_1.GetEntries() << std::endl;
			std::cout << "Bin 1: " << sig_h_1.GetBinContent(1) << std::endl;
			std::cout << "Bin 2: " << sig_h_1.GetBinContent(2) << std::endl;
			}

		RooAddPdf model("mod","mod",RooArgList(sig1_pdf,sig2_pdf),RooArgList(nsig1,nsig2));

		RooDataHist *combHist = model.generateBinned(x_1,sig_h_1.Integral()+sig_h_2.Integral(),Extended());

		model.fitTo(*combHist,SumW2Error(kFALSE));

		res->Fill(nsig1.getVal());
		res2->Fill(nsig2.getVal());


		if(i == 0){
			RooPlot* x1frame = x_1.frame() ;
			TCanvas *c1 = new TCanvas("c1", "canvas", 640, 480);
			c1->cd();
			combHist->plotOn(x1frame); 
			x1frame->Draw(); 
		}
		
	}
//  	TCanvas *c1 = new TCanvas("c1", "canvas", 640, 480);
//  	c1->cd();
//  	bin1->Draw(); 
//  	TCanvas *c2 = new TCanvas("c2", "canvas", 640, 480);
//  	c2->cd();
//  	bin2->Draw(); 
//  	TCanvas *c3 = new TCanvas("c3", "canvas", 640, 480);
//  	c3->cd();
//  	integ->Draw(); 
 	TCanvas *c4 = new TCanvas("c4", "canvas", 640, 480);
 	c4->cd();
 	res->Draw(); 
 	TCanvas *c5 = new TCanvas("c5", "canvas", 640, 480);
 	c5->cd();
 	res2->Draw(); 
	
	

	// Plot x distribution of data and projection of model on x = Int(dy) model(x,y)
	//RooPlot* x1frame = x_1.frame() ;
 	//TCanvas *c1 = new TCanvas("c1", "canvas", 640, 480);
 	//c1->cd();
 	//pdfh->Draw(); 
 	//tmphist->Draw(); 

// 	TCanvas *c2 = new TCanvas("c2", "canvas", 640, 480);
// 	c2->cd();
//  	double scale = sig_h_1.Integral();
//  	pdfh->Scale(scale);
// 	pdfh->Draw(); 


	// Plot x distribution of data and projection of model on y = Int(dx) model(x,y)
// 	RooPlot* x2frame = x_1.frame() ;
// 	double scale = sig_h_1.Integral();
// 	pdfh->Scale(scale);
// 	pdfh->plotOn(x2frame) ;
	
	// Make canvas and draw RooPlots
// 	TCanvas *c = new TCanvas("rf305_condcorrprod","rf05_condcorrprod",1200, 400);
// 	c->Divide(2);
// 	c->cd(1) ; gPad->SetLeftMargin(0.15) ; x1frame->GetYaxis()->SetTitleOffset(1.6) ; x1frame->Draw() ;
// 	c->cd(2) ; gPad->SetLeftMargin(0.15) ; x2frame->GetYaxis()->SetTitleOffset(1.6) ; x2frame->Draw() ;
 	//c->cd(3) ; gPad->SetLeftMargin(0.20) ; hh_model->GetZaxis()->SetTitleOffset(2.5) ; hh_model->Draw("surf") ;





//   // C r e a t e   c o n d i t i o n a l   p d f   g x ( x | y ) 
//   // -----------------------------------------------------------

//   // Create observables
//   RooRealVar x("x","x",-5,5) ;
//   RooRealVar y("y","y",-5,5) ;

//   // Create function f(y) = a0 + a1*y
//   RooRealVar a0("a0","a0",-0.5,-5,5) ;
//   RooRealVar a1("a1","a1",-0.5,-1,1) ;
//   // RooPolyVar fy("fy","fy",y,RooArgSet(a0,a1)) ;
//   RooFormulaVar fy("fy","@0/@1",RooArgList(a0,a1)) ;

//   // Create gaussx(x,f(y),sx)
//   RooRealVar sigmax("sigma","width of gaussian",0.5) ;
//   RooGaussian gaussx("gaussx","Gaussian in x with shifting mean in y",x,fy,sigmax) ;  



//   // C r e a t e   p d f   g y ( y ) 
//   // -----------------------------------------------------------

//   // Create gaussy(y,0,5)
//   RooGaussian gaussy("gaussy","Gaussian in y",y,RooConst(0),RooConst(3)) ;



//   // C r e a t e   p r o d u c t   g x ( x | y ) * g y ( y )
//   // -------------------------------------------------------

//   // Create gaussx(x,sx|y) * gaussy(y)
//   RooProdPdf model("model","gaussx(x|y)*gaussy(y)",gaussy,Conditional(gaussx,x)) ;



//   // S a m p l e ,   f i t   a n d   p l o t   p r o d u c t   p d f
//   // ---------------------------------------------------------------

//   // Generate 1000 events in x and y from model
//   RooDataSet *data = model.generate(RooArgSet(x,y),10000) ;

//   model.fitTo(*data);

//   // Plot x distribution of data and projection of model on x = Int(dy) model(x,y)
//   RooPlot* xframe = x.frame() ;
//   data->plotOn(xframe) ;
//   model.plotOn(xframe) ; 

//   // Plot x distribution of data and projection of model on y = Int(dx) model(x,y)
//   RooPlot* yframe = y.frame() ;
//   data->plotOn(yframe) ;
//   model.plotOn(yframe) ; 

//   // Make two-dimensional plot in x vs y
//   TH1* hh_model = model.createHistogram("hh_model",x,Binning(50),YVar(y,Binning(50))) ;
//   hh_model->SetLineColor(kBlue) ;



//   // Make canvas and draw RooPlots
//   TCanvas *c = new TCanvas("rf305_condcorrprod","rf05_condcorrprod",1200, 400);
//   c->Divide(3);
//   c->cd(1) ; gPad->SetLeftMargin(0.15) ; xframe->GetYaxis()->SetTitleOffset(1.6) ; xframe->Draw() ;
//   c->cd(2) ; gPad->SetLeftMargin(0.15) ; yframe->GetYaxis()->SetTitleOffset(1.6) ; yframe->Draw() ;
//   c->cd(3) ; gPad->SetLeftMargin(0.20) ; hh_model->GetZaxis()->SetTitleOffset(2.5) ; hh_model->Draw("surf") ;

}



