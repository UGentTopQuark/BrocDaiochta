#!/bin/bash

if  [ "$1" = '' -o "$2" = '' -o "$3" = '' ]
then
        echo
        echo './MakeARI.sh <version>  <isele=0/1> <smooth=0/1>'
        echo
        exit 1
fi

vers="$1"
isele="$2"
if [ "$3" = "1" ]
then
	smooth="smooth1"
	smoo=""
else
	smooth=""
	smoo="//"
fi

dovar="1"

name="central_4j_03$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/01/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'

name="central_4j_0275$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/02/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'

name="central_4j_025$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/03/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'

name="central_4j_0225$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/04/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'

name="central_4j_02$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/05/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'

name="central_4j_0175$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/06/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'

name="central_4j_015$smooth"
fact="-1"
cat ProduceQCDNoIso_Muthesis.template.C | sed s/VERS/$vers/ | sed s/NAME/$name/ | sed s/CUT/07/ | sed -e 's/FACT/'$fact'/' | sed -e 's_SMOOTH_'$smoo'_' > ProduceQCDNoIso_Muthesis.C
root -b -l -q ProduceQCDNoIso_Muthesis.C'("'$isele'")'
