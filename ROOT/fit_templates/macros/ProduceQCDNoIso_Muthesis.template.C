//Root related
#include "TSystem.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TDirectory.h"
#include "TKey.h"
#include "TIterator.h"
#include "TList.h"
#include "TString.h"
#include "TCollection.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TPad.h"
#include "Riostream.h"
#include "TLegend.h"
#include "TH1.h"
#include "TH2.h"
#include "TStyle.h"

using namespace std;

TString ParseInputLine(TString& readline)
{
        string delim = "\n";
        string histoname = "junk";
        string result;
        TObjArray *tokens = readline.Tokenize(delim.c_str());
        TObject *p;

        TIter iter(tokens);
        while(p = iter.Next() )
        {
                TObjString *item  = (TObjString*)p;
                TString pbackitem = item->GetString();
                pbackitem         = pbackitem.Strip(TString::kBoth);
                result            = pbackitem.Data();
        }

        delete tokens;
        if(result != "" && (result.find("//") == std::string:: npos))
                histoname = result;
        return histoname;
}

void ProduceQCDNoIso_Muthesis(const char * isele)
{
        cout << endl << "*****Producing QCD ARI file for version VERS and index NAME*****" << endl;
	cout <<         "     Scale factor: FACT" << endl << endl;
        TFile * in  = new TFile("/localgrid/walsh/ROOT/broc_draiochta/output/VERS/Data_plots.root", "READ");
        TFile * out = new TFile("/localgrid/walsh/ROOT/broc_draiochta/output/VERS/QCD_NAME_plots.root", "RECREATE");
	TFile * tt  = new TFile("/localgrid/walsh/ROOT/broc_draiochta/output/VERS/TTbar_plots.root", "READ");
	TFile * wjj = new TFile("/localgrid/walsh/ROOT/broc_draiochta/output/VERS/Wjets_plots.root", "READ");
	TFile * zjj = new TFile("/localgrid/walsh/ROOT/broc_draiochta/output/VERS/Zjets_plots.root", "READ");
	TFile * sc = new TFile("/localgrid/walsh/ROOT/broc_draiochta/output/VERS/TsChan_plots.root", "READ");
	TFile * tc = new TFile("/localgrid/walsh/ROOT/broc_draiochta/output/VERS/TtChan_plots.root", "READ");
	TFile * tWc = new TFile("/localgrid/walsh/ROOT/broc_draiochta/output/VERS/TtWChan_plots.root", "READ");
        TH1F  * h1, * h2, *h3, *h4, *h5, *h6, *h7, *h8, *wcount, *tcount, *zcount, *sccount, *tccount, *tWccount;
        out->mkdir("eventselection");
        out->cd("eventselection");
         
	ifstream fin("HistosToPlot",ios::in);

        TString line;
        string histname, temp;
        char name[300], namett[300], namettoth[300], namewjj[300], namezjj[300], namesc[300], nametc[300], nametWc[300];
	double lumi = 0., ttscale= 0., ttothscale = 0., wjjscale=0., zjjscale=0., scscale=0., tcscale=0., tWcscale=0.;
	int countme = 0, entw=0;
	double swtt=0., swttoth=0., swwj = 0., swzj = 0., swsc = 0., swtc = 0., swtWc = 0.;
	wcount = (TH1F*) wjj->Get("eventselection/event_counter");
	tcount = (TH1F*) tt->Get("eventselection/event_counter");
	zcount = (TH1F*) zjj->Get("eventselection/event_counter");
	sccount = (TH1F*) sc->Get("eventselection/event_counter");
	tccount = (TH1F*) tc->Get("eventselection/event_counter");
	tWccount = (TH1F*) tWc->Get("eventselection/event_counter");

        double br_m = tcount->GetBinContent(2)/(tcount->GetBinContent(2)+tcount->GetBinContent(3)+tcount->GetBinContent(4));

	cout << "Brancing RATIO: " << br_m  << std::endl;

	ifstream fin("HistosToPlot",ios::in);
	lumi = 4760.401;
	wjjscale = 31314.*lumi / wcount->GetEntries();
	zjjscale = 3048.*lumi / zcount->GetEntries();
	scscale = 4.6*lumi / sccount->GetEntries();
	tcscale = 64.6*lumi / tccount->GetEntries();
	tWcscale = 15.7*lumi / tWccount->GetEntries();
	ttscale  = 165*lumi*br_m / tcount->GetBinContent(2); 
	ttothscale  = 165*lumi*(1.-br_m) / (tcount->GetBinContent(3)+tcount->GetBinContent(4)); 


        while ( fin.good() && fin )
        {
                getline(fin,temp);
                line     = temp.c_str(); 
                histname = ParseInputLine(line);
		//std::cout << "hist: " << histname << std::endl;
                if(histname.find("junk") != std::string:: npos) continue;
                		
		sprintf(name,"eventselection/%s_Data|muon|CUT_cutset",histname.c_str());
		sprintf(namett,"eventselection/%s_TTbar|muon|CUT_cutset",histname.c_str());
		sprintf(namettoth,"eventselection/%s_TTbar|mu_background|CUT_cutset",histname.c_str());
		sprintf(namewjj,"eventselection/%s_Wjets|mu_background|CUT_cutset",histname.c_str());
		sprintf(namezjj,"eventselection/%s_Zjets|mu_background|CUT_cutset",histname.c_str());
		sprintf(namesc,"eventselection/%s_TsChan|mu_background|CUT_cutset",histname.c_str());
		sprintf(nametc,"eventselection/%s_TtChan|mu_background|CUT_cutset",histname.c_str());
		sprintf(nametWc,"eventselection/%s_TtWChan|mu_background|CUT_cutset",histname.c_str());
		
		h1       = (TH1F*)  in->Get(name);
		h2       = (TH1F*)  tt->Get(namett);
		h3       = (TH1F*)  wjj->Get(namewjj);
		h4       = (TH1F*)  zjj->Get(namezjj);
		h5       = (TH1F*)  tt->Get(namettoth);
		h6       = (TH1F*)  sc->Get(namesc);
		h7       = (TH1F*)  tc->Get(nametc);
		h8       = (TH1F*)  tWc->Get(nametWc);
		
		h2->Scale(ttscale);
		h3->Scale(wjjscale);
		h4->Scale(zjjscale);
		h5->Scale(ttothscale);
		h6->Scale(scscale);
		h7->Scale(tcscale);
		h8->Scale(tWcscale);

                if(countme <1)
                {
                        entw = h1->GetEntries();
                        swtt = h2->GetSumOfWeights();
                        swwj = h3->GetSumOfWeights();
                        swzj = h4->GetSumOfWeights();
                        swttoth = h5->GetSumOfWeights();

                        swsc = h6->GetSumOfWeights();
                        swtc = h7->GetSumOfWeights();
                        swtWc = h8->GetSumOfWeights();
                }
                countme++;
		h1->Add(h2, FACT); //tt
		h1->Add(h3, FACT); //wj
		h1->Add(h4, FACT); //zj
		h1->Add(h5, FACT); //ttoth
		h1->Add(h6, FACT); //sc
		h1->Add(h7, FACT); //tc
		h1->Add(h8, FACT); //tWc


                for(int i = 1 ; i<=h1->GetNbinsX() ; i++)
                        if(h1->GetBinContent(i) < 0.) h1->SetBinContent(i,0);
		
		h1->SetEntries((int) h1->GetSumOfWeights());	
		SMOOTHh1->Smooth();

		sprintf(name,"%s_QCD|mu_background|CUT_cutset",histname.c_str());
                
                h1->SetName(name);
                h1->Write();
        }
	
	cout << entw << " data events containing " << swtt << " ("<< swtt*100/entw << "%) signal and " << swwj << "(" << swwj*100/entw << "%) wjj and " << swzj << " (" << (swzj*100/entw) << "%) zjj and " << swttoth << " (" << swttoth*100/entw << "%) tt other and " << swsc+swtc+swtWc << " (" << ((swsc+swtc+swtWc)*100)/entw << "%) single top " << endl;
	cout << "Total contamination: " << swtt+swwj+swzj+swttoth+swsc+swtc+swtWc << " ("<< ((swtt+swwj+swzj+swttoth+swsc+swtc+swtWc)*100)/entw << "%) "<< endl;
	cout << "after contamination removal: " << entw-swtt-swwj-swzj-swttoth-swsc-swtc-swtWc << " data events " << endl;

	sprintf(name,"event_counter_QCD|mu_background|CUT_cutset");
        
        TH1F* n = new TH1F(name,name,1,0,1);
        n->SetBinContent(1,1);
        n->SetEntries(1);
        n->Write();
}
