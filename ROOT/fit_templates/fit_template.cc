#include <sstream>
#include <iostream>
#include <map>
#include <vector>

#include "ConfigReader/ConfigReader.h"
#include "InputManager.h"


int main(int argc, char **argv)
{
	std::cout << "Template fitting" << std::endl;
	std::string config = argv[1];
	std::string section = "global";
        if(argc > 2) section = argv[2]; 

        eire::ConfigReader *config_reader = new eire::ConfigReader();
        config_reader->read_config_from_file(config);


	InputManager *input_manager = new InputManager(config_reader,section);
	input_manager->run_fit_templates();

	if(input_manager){
		std::cout << "deleting input manager " << std::endl;
		delete input_manager;
		input_manager = NULL;
	}
	if(config_reader){
		std::cout << "deleting config reader " << std::endl;
		delete config_reader;
		config_reader = NULL;}

	std::cout << "End program " << std::endl;
}
