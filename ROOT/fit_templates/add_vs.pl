#!/usr/bin/perl

my $dir = "20111125_mu_jl_782_783";
my @datasets = ("Data","TTbar", "Wjets", "Zjets", "TtChan", "TsChan", "TtWChan");
my $id1 = "";
my $id2 = "";
my $id2mv = "782";


foreach my $ds (@datasets){
    if($id1 != "" && $id2 != "" &&$ds eq "QCD_central"){
	my $tid1 = $id1 + 1;
	my $tid2 = $id2 + 1;
	print "hadd $dir/999-$ds.root $dir/$tid1-$ds.root  $dir/$tid2-$ds.root\n";
	`hadd $dir/999-$ds.root $dir/$tid1-$ds.root $dir/$tid2-$ds.root` ;
    }elsif($id1 != "" && $id2 != ""){
	print "hadd $dir/999-$ds.root $dir/$id1-$ds.root  $dir/$id2-$ds.root\n";
	`hadd $dir/999-$ds.root $dir/$id1-$ds.root $dir/$id2-$ds.root` ;
    }
    print "cp $dir/$id2mv-$ds.root $dir/${ds}_plots.root\n";
    `cp $dir/$id2mv-$ds.root $dir/${ds}_plots.root`;
    #print "cp $dir/${ds}_plots.root $dir/${ds}_plushalf.root\n";
    #`cp $dir/${ds}_plots.root $dir/${ds}_plushalf.root`;
}
