#!/usr/bin/perl -w

my $e_d_pos_start_val = 222.0;
my $e_d_neg_start_val = 100.2;
my $e_mc_pos_start_val = 212.0;
my $e_mc_neg_start_val = 94.7;

#smear, st, mass, pdf, qcd slices.
my @e_data_pos = (2.3, 0.9, 2.9, 5.9, 4.8);

my @e_data_neg = (2.3, 0.9, 0.4, 4.8, 0.0);

#smear (data), mass, pdf, qcd slices.
my @e_mc_pos = (2.3, 5.5, 5.8, 3.2);
my @e_mc_neg = (2.3, 0.0, 4.4, 0.0);
my $e_cs_meas = 157.1;
my $cs_exp = 157.5;

print "Electron channel Fit to Data: ******** \n";
print "$e_cs_meas + ";
print_sum_squares(\@e_data_pos,$e_cs_meas,$e_d_pos_start_val);
print "/ - ";
print_sum_squares(\@e_data_neg,$e_cs_meas,$e_d_neg_start_val);
print "\n";

print "Electron channel Fit to MC: ******** \n";
print "$cs_exp + ";
print_sum_squares(\@e_mc_pos,$cs_exp,$e_mc_pos_start_val);
print "/ - ";
print_sum_squares(\@e_mc_neg,$cs_exp,$e_mc_neg_start_val);
print "\n";


my $mu_d_pos_start_val = 84.6;
my $mu_d_neg_start_val = 445.8;
my $mu_mc_pos_start_val = 44.8;
my $mu_mc_neg_start_val = 498.9;

#smear, st, mass, pdf,  qcd slices.
my @mu_data_pos = (2.2, 0.9, 0.0, 4.9, 10.2);
my @mu_data_neg = (2.2, 0.9, 4.5, 4.1, 0.);

#smear (data), mass, pdf, qcd slices.
my @mu_mc_pos = (2.2, 0.0, 5.5, 0.8);
my @mu_mc_neg = (2.2, 4.8, 4.4, 0.0);
my $mu_cs_meas = 161.6;

print "Muon channel Fit to Data: ******** \n";
print "$mu_cs_meas + ";
print_sum_squares(\@mu_data_pos,$mu_cs_meas,$mu_d_pos_start_val);
print "/ - ";
print_sum_squares(\@mu_data_neg,$mu_cs_meas,$mu_d_neg_start_val);
print "\n";

print "Muon channel Fit to MC: ******** \n";
print "$cs_exp + ";
print_sum_squares(\@mu_mc_pos,$cs_exp,$mu_mc_pos_start_val);
print "/ - ";
print_sum_squares(\@mu_mc_neg,$cs_exp,$mu_mc_neg_start_val);
print "\n";

my $emu_d_pos_start_val = 78.9;
my $emu_d_neg_start_val = 183.8;
my $emu_mc_pos_start_val = 46.5;
my $emu_mc_neg_start_val = 167.6;

#smear, st, mass, pdf, qcd slices.
my @emu_data_pos = (1.6, 0.8, 0.9, 5.7, 7.4);
my @emu_data_neg = (1.6, 0.8, 2.6, 4.7, 0.0);

#smear (data), mass, pdf, qcd slices.
my @emu_mc_pos = (1.6, 0.7, 5.7, 1.5);
my @emu_mc_neg = (1.6, 2.7, 5.3, 0.0);
my $emu_cs_meas = 159.7;

print "Combined channel Fit to Data: ******** \n";
print "$emu_cs_meas + ";
print_sum_squares(\@emu_data_pos,$emu_cs_meas,$emu_d_pos_start_val);
print "/ - ";
print_sum_squares(\@emu_data_neg,$emu_cs_meas,$emu_d_neg_start_val);
print "\n";

print "Combined channel Fit to MC: ******** \n";
print "$cs_exp + ";
print_sum_squares(\@emu_mc_pos,$cs_exp,$emu_mc_pos_start_val);
print "/ - ";
print_sum_squares(\@emu_mc_neg,$cs_exp,$emu_mc_neg_start_val);
print "\n";






sub print_sum_squares
{
	my ($input_ref,$cs,$start_sum) = @_;

	my @input = @$input_ref;
	
	my $sum_square = $start_sum;
	foreach my $num(@input){
	    $sum_square += $num*$num;
	}
	#print "sum square:$sum_square\n";
	$sqrt = sqrt($sum_square);
	$percent_sqrt = ($sqrt/$cs)*100;
	print "$sqrt ($percent_sqrt\%)";


}
