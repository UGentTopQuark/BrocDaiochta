#!/usr/bin/perl -w

use Getopt::Std;
#use strict;

getopts("p:");


die "need input pattern..." unless(defined($opt_p));
my $input_pattern = $opt_p;

find_directories($input_pattern);


sub find_directories
{
    my ($pattern) = @_;
    
    my $top_mass_id = "top_mass_sys_study";
    my $smear_id = "_smear_";
    my $data_id = "_data_";
    my $mc_id = "_mc_";
    my $out_txt = "out.txt";
    my $fit_output_directory = "/user/walsh/ROOT/fit_templates/output/";
    my @filenames = ();
    

    opendir(DIR, "$fit_output_directory") or die "can't open directory: $fit_output_directory\n";
    
    while(defined(my $cur_dir = readdir(DIR))){
	if($cur_dir =~ /$top_mass_id/ && $cur_dir =~ /$pattern/){
	    if(($cur_dir =~ /$smear_id/ && $cur_dir =~ /$data_id/) || ($cur_dir =~ /$mc_id/)){
		my $fullfilepath = $fit_output_directory.$cur_dir."/".$out_txt;
		push(@filenames, $fullfilepath);
	    }
	}
    }
    closedir(DIR);

    foreach my $file(@filenames){
	print "Processing $file \n";
	process_file($file);
    }
	    


}

sub process_file
{
	my ($file_to_open) = @_;

	print "Opening ".$file_to_open."\n";
	open(FILE, "<$file_to_open") or die "can't open file: $file_to_open\n";
	my @input = <FILE>;
	close(FILE);
	
	print_channel_data_mc($file_to_open);
	
	if($file_to_open =~ /_smear_/ && $file_to_open =~ /_data_/){
	    print_smeared_cs(\@input);
	}
	elsif($file_to_open =~ /_mc_/){
	    print_raw_cs(\@input);
	}
	print_sel_eff_only_cs(\@input);
}

sub print_channel_data_mc
{
	my ($filename) = @_;
	my $channel = "Not found";
	my $data_mc = "Not found";

	if($filename =~ /_emu_/){
	    $channel = "Combined E/Mu";
	} 
	elsif($filename =~ /m3_mu/){
	    $channel = "Muon";
	} 
	elsif($filename =~ /m3_e_/){
	    $channel = "Electron";
	} 
	if($filename =~ /_data_/){
	    $data_mc = "Data";
	} 
	elsif($filename =~ /_mc_/){
	    $data_mc = "MC";
	} 

	print "*** $channel  $data_mc:\n\n";

}

sub print_smeared_cs
{
	my ($input_ref) = @_;

	my @input = @$input_ref;

	print "Data smeared: \n\n";
	
	my $start_block = '\*\*\*Cross Section Table\*\*\*';
	my $end_block = '\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*';
	my $signal_pattern = "tt_total";


	my @lines_out = ();
	my $in_table = 0;

	foreach my $line(@input){
	   # print $line."\n";

	    if($line =~ /$start_block/){
		$in_table = 1;
		next;
	    } 
	    next if(!$in_table);

	    if($line =~ /$end_block/){
		$in_table = 0; 
		next;
	    }

	    if($in_table && $line =~ /$signal_pattern/){
		#print "Line found : $line \n";
		my $cs = ""; my $id = "";
		if($line =~ m/\$ \& \$ ([\ds.]+ \\pm [\ds.]+)/){
		    $cs = $1;
		  #  print "CS: $cs \n";
		}
		if($line =~ m/([\w_]+) &/){
		    $id = $1;
		}
		print "$id: $cs \n";
	    }
	}


}

sub print_raw_cs
{
	my ($input_ref) = @_;

	my @input = @$input_ref;

	print "Full Cross Section: \n\n";
	
	my $start_block = '\*\*\* Raw Cross Sections \*\*\*';
	my $end_block = '\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*';

	my $in_table = 0;
	foreach my $line(@input){

	    if($line =~ /$start_block/){
		$in_table = 1;
		next;
	    } 
	    next if(!$in_table);

	    if($line =~ /$end_block/){
		$in_table = 0; 
		next;
	    }
	    
	    if($in_table){
		print "$line";
	    }

	}
}

sub print_sel_eff_only_cs
{
	my ($input_ref) = @_;

	my @input = @$input_ref;

	print "\nSelection eff only: \n\n";
	
	my $start_block = '\*\*\* Raw Cross Sections ';
	my $end_block = '\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*';

	my $in_table = 0;
	foreach my $line(@input){

	    if($line =~ /$start_block/ && $line =~ /Sys sel Eff/){
		$in_table = 1;
		next;
	    } 
	    next if(!$in_table);

	    if($line =~ /$end_block/){
		$in_table = 0; 
		next;
	    }
	    
	    if($in_table){
		print "$line";
	    }

	}
}
