[global]
datadir_e = /user/walsh/ROOT/fit_templates/input/20120312_0_100_fit_template_e_match_scale_newlumiQCD 
datadir_mu = /user/walsh/ROOT/fit_templates/input/20120312_0_100_fit_template_mu_match_scale_newlumiQCD 
output_dir = ./output/20120321_0_147_aesthetics_emu_m3_mu_4jet_m3_e_4jet_data_central 
use_real_data = true 
histos_to_fit = m3_mu_4jet:m3_e_4jet 
histo_ranges = 4fb_mu_e_4jet 
split_qcdmm =  true
use_default_qcd_cutset =  true
smear_template_shapes =  false
do_not_smear_patterns =  st:qcd:tt_total
vary_constraint_mean =  false
version=_plots
lumi_mu =4760.401
lumi_e =10
mc_analysis =Fall11sw
npseudo_exp=5000

gauss_constraints=st
template_shape_ids =tt_total:wzj:st:qcdmm
mc_histos =tt_total:st:wj:zj

do_not_scale=QCD
;;separate_data_hist_section = all_samples
;;use_default_qcd_cutset = true
apply_additional_scale_factors = true
scale_factors_QCDmu = 0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149:0.149

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;; Systematic Studies ;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[rebin_sys_study_small]
run_multi_rebin_sys_study = true
multi_rebin = 5:10

[rebin_sys_study]
run_multi_rebin_sys_study = true
multi_rebin = 4:5:6:7:8:9:10:11:12:13:14:15:16:17:18:19:20:21:22:23:24:25
;;multi_rebin = 2:3:4:5:6:7:8:10:11:12:13:14:15

[pu_sys_study]
sys_study_names =none:PU_down:PU_up
sys_study_versions =_plots
sys_template_sections = all_samples

[idsf_sys_study]
sys_study_names =none:IDsf_down:IDsf_up
sys_study_versions =_plots
sys_template_sections = all_samples

[hltsf_sys_study]
sys_study_names =none:HLTsf_down:HLTsf_up
sys_study_versions =_plots
sys_template_sections = all_samples


[jes_sys_study]
sys_study_names =none:JES_down:JES_up
sys_study_versions =_plots
sys_template_sections = all_samples

[jes_jer_sys_study]
sys_study_names =none:JES_down:JES_up:JER_down:JER_up
;sys_study_names =none:JES_down
sys_study_versions =_plots
sys_template_sections = all_samples

[jer_sys_study]
sys_study_names =none:JER_down:JER_up
sys_study_versions =_plots
sys_template_sections = all_samples

[pdf_sys_study]
sys_study_names = none:w01:w02:w03:w04:w05:w06:w07:w08:w09:w10:w11:w12:w13:w14:w15:w16:w17:w18:w19:w20:w21:w22:w23:w24:w25:w26:w27:w28:w29:w30:w31:w32:w33:w34:w35:w36:w37:w38:w39:w40:w41:w42:w43:w44
sys_study_versions = default
sys_template_sections = all_samples

[tt_scale_sys_study]
sys_study_names = none:ttScaleDOWN:ttScaleUP
sys_study_versions = default
sys_template_sections = all_samples:all_samples_tt_scaledown:all_samples_tt_scaleup

[wz_scale_sys_study]
sys_study_names = none:wzScaleDOWN:wzScaleUP
sys_study_versions = default
sys_template_sections = all_samples:all_samples_wz_scaledown:all_samples_wz_scaleup


[top_mass_sys_study]
sys_study_names =none:Mass161:Mass163:Mass166:Mass169:Mass175:Mass178:Mass181:Mass184
sys_study_versions =_plots
sys_template_sections = all_samples:all_samples_mass161:all_samples_mass163:all_samples_mass166:all_samples_mass169:all_samples_mass175:all_samples_mass178:all_samples_mass181:all_samples_mass184


[tt_match_sys_study]
sys_study_names = none:ttMatchingUP:ttMatchingDOWN
sys_study_versions = default
sys_template_sections = all_samples:all_samples_tt_matchingup:all_samples_tt_matchingdown

[wz_match_sys_study]
sys_study_names = none:wzMatchingUP:wzMatchingDOWN
sys_study_versions = default
sys_template_sections = all_samples:all_samples_wz_matchingup:all_samples_wz_matchingdown

[tt_mc_sys_study]
sys_study_names = none:ttPowhegUP
sys_study_versions = default
sys_template_sections = all_samples:all_samples_tt_powheg


[qcd_sys_study]
sys_study_names =none:qcd_up:qcd_down
sys_study_versions =_plots:_plushalf_plots:_minushalf_plots
sys_template_sections = all_samples

[qcd_slices_sys_study]
sys_study_names =none:sl2:sl3:sl4:sl5:sl6:sl7:sl8:sl9:sl10:sl11
sys_study_versions =_plots
sys_template_sections = all_samples

[qcd_three_slices_sys_study]
sys_study_names =none:sl2:sl3
sys_study_versions =_plots
sys_template_sections = all_samples

[qcd_six_slices_sys_study]
sys_study_names =none:sl2:sl3:sl4:sl5:sl6
sys_study_versions =_plots
sys_template_sections = all_samples


[qcd_seven_slices_sys_study]
sys_study_names =none:sl2:sl3:sl4:sl5:sl6:sl7
sys_study_versions =_plots
sys_template_sections = all_samples

[qcd_full_sys_study]
sys_study_names =none:plushalf:minushalf:plusone:minusone:plustwo:minustwo
sys_study_versions =_plots:_plushalf:_minushalf:_plusone:_minusone:_plustwo:_minustwo
sys_template_sections = all_samples

[jes_qcd_sys_study]
sys_study_names =none:JES_down_plushalf:JES_down_minushalf:JES_up_plushalf:JES_up_minushalf
sys_study_versions =_plots:_plushalf:_minushalf:_plushalf:_minushalf
sys_template_sections = all_samples

[wzratio_sys_study]
sys_study_names =none:wzratio_up:wzratio_down
sys_study_versions =_plots
sys_template_sections = all_samples
apply_additional_scale_factors = true
scale_factors_Wjets = 1:1.2:0.8

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;; Histo To Fit ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[m3_mu_4jet]
channel=mu
cutset=01_cutset
histo_name=top_mass_tf_
rebin=13

jes_jer_sys_study_sys_study_cutsets =01_cutset:04_cutset:05_cutset:08_cutset:09_cutset
jer_sys_study_sys_study_cutsets =01_cutset:08_cutset:09_cutset
jes_sys_study_sys_study_cutsets =01_cutset:04_cutset:05_cutset
jes_qcd_sys_study_sys_study_cutsets =01_cutset:04_cutset:04_cutset:05_cutset:05_cutset
qcd_slices_sys_study_sys_study_cutsets =01_cutset:02_cutset:03_cutset:04_cutset:05_cutset:06_cutset:07_cutset:08_cutset:09_cutset:10_cutset:11_cutset
qcd_three_slices_sys_study_sys_study_cutsets =01_cutset:02_cutset:03_cutset
qcd_six_slices_sys_study_sys_study_cutsets =01_cutset:02_cutset:03_cutset:04_cutset:05_cutset:06_cutset
qcd_seven_slices_sys_study_sys_study_cutsets =01_cutset:02_cutset:03_cutset:04_cutset:05_cutset:06_cutset:07_cutset


pu_sys_study_sys_study_cutsets =01_cutset:12_cutset:13_cutset
idsf_sys_study_sys_study_cutsets =01_cutset:16_cutset:17_cutset
hltsf_sys_study_sys_study_cutsets =01_cutset:18_cutset:19_cutset
pdf_sys_study_sys_study_cutsets = 01_cutset_00:01_cutset_01:01_cutset_02:01_cutset_03:01_cutset_04:01_cutset_05:01_cutset_06:01_cutset_07:01_cutset_08:01_cutset_09:01_cutset_10:01_cutset_11:01_cutset_12:01_cutset_13:01_cutset_14:01_cutset_15:01_cutset_16:01_cutset_17:01_cutset_18:01_cutset_19:01_cutset_20:01_cutset_21:01_cutset_22:01_cutset_23:01_cutset_24:01_cutset_25:01_cutset_26:01_cutset_27:01_cutset_28:01_cutset_29:01_cutset_30:01_cutset_31:01_cutset_32:01_cutset_33:01_cutset_34:01_cutset_35:01_cutset_36:01_cutset_37:01_cutset_38:01_cutset_39:01_cutset_40:01_cutset_41:01_cutset_42:01_cutset_43:01_cutset_44


[abseta_mu_4jet]
channel=mu
cutset=01_cutset
histo_name=muon_abseta_
rebin=8


[abseta_charge_mu_4jet]
channel=mu
cutset=01_cutset
histo_name=muon_abseta_x_charge_
rebin=10


[eta_mu_4jet]
channel=mu
cutset=01_cutset
histo_name=muon_eta_tf_
rebin=10

[chi2_mu_4jet]
channel=mu
cutset=01_cutset
histo_name=chi2_wo_btag_
rebin=10


[Had_chimass_mu_4jet]
channel=mu
cutset=01_cutset
histo_name=top_Had_chimass_
;rebin=2


[Lep_chimass_mu_4jet]
channel=mu
cutset=01_cutset
histo_name=top_Lep_chimass_
;rebin=2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;; Template Construction ;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#Input histogram options
[tt_total]
sig_channel=TTbar
[tt_total_scaledown]
sig_channel=TTbarScaleDOWN
[tt_total_scaleup]
sig_channel=TTbarScaleUP
[tt_total_matchingdown]
sig_channel=TTbarMatchingDOWN
[tt_total_matchingup]
sig_channel=TTbarMatchingUP
[tt_total_mass161]
sig_channel=TTbarMass161
[tt_total_mass163]
sig_channel=TTbarMass163
[tt_total_mass166]
sig_channel=TTbarMass166
[tt_total_mass169]
sig_channel=TTbarMass169
[tt_total_mass175]
sig_channel=TTbarMass175
[tt_total_mass178]
sig_channel=TTbarMass178
[tt_total_mass181]
sig_channel=TTbarMass181
[tt_total_mass184]
sig_channel=TTbarMass184
[tt_total_powheg]
bkg_channel=TTpowheg
[tt_total_mcatnlo]
bkg_channel=TTmcatnlo
[wzq]
bkg_channel=Wjets:Zjets:MuQCD
[wzqmm]
bkg_channel=Wjets:Zjets:QCD
[wzj]
bkg_channel=Wjets:Zjets
[wzj_scaledown]
bkg_channel=WjetsScaleDOWN:ZjetsScaleDOWN
[wzj_scaleup]
bkg_channel=WjetsScaleUP:ZjetsScaleUP
[wzj_matchingdown]
bkg_channel=WjetsMatchingDOWN:ZjetsMatchingDOWN
[wzj_matchingup]
bkg_channel=WjetsMatchingUP:ZjetsMatchingUP
[st]
bkg_channel=TtChan:TsChan:TtWChan
[wj]
bkg_channel=Wjets
[zj]
bkg_channel=Zjets
[qcd]
bkg_channel=MuQCD
[qcdmmmu]
bkg_channel=QCD
[qcdmme]
bkg_channel=QCD
[qcdmm]
bkg_channel=QCD
[all_samples]
template_shape_ids =tt_total:wzj:st:qcdmm
sig_channel=TTbar
bkg_channel=Wjets:Zjets:QCD:TtChan:TsChan:TtWChan
[all_samples_tt_scaledown]
template_shape_ids =tt_total_scaledown:wzj:st:qcdmm
[all_samples_tt_scaleup]
template_shape_ids =tt_total_scaleup:wzj:st:qcdmm
[all_samples_wz_scaledown]
template_shape_ids =tt_total:wzj_scaledown:st:qcdmm
[all_samples_wz_scaleup]
template_shape_ids =tt_total:wzj_scaleup:st:qcdmm
[all_samples_matchingup]
template_shape_ids =tt_total_matchingup:wzj_matchingup:st:qcdmm
[all_samples_matchingdown]
template_shape_ids =tt_total_matchingdown:wzj_matchingdown:st:qcdmm
[all_samples_tt_matchingup]
template_shape_ids =tt_total_matchingup:wzj:st:qcdmm
[all_samples_tt_matchingdown]
template_shape_ids =tt_total_matchingdown:wzj:st:qcdmm
[all_samples_wz_matchingup]
template_shape_ids =tt_total:wzj_matchingup:st:qcdmm
[all_samples_wz_matchingdown]
template_shape_ids =tt_total:wzj_matchingdown:st:qcdmm
[all_samples_mass161]
template_shape_ids =tt_total_mass161:wzj:st:qcdmm
[all_samples_mass163]
template_shape_ids =tt_total_mass163:wzj:st:qcdmm
[all_samples_mass166]
template_shape_ids =tt_total_mass166:wzj:st:qcdmm
[all_samples_mass169]
template_shape_ids =tt_total_mass169:wzj:st:qcdmm
[all_samples_mass175]
template_shape_ids =tt_total_mass175:wzj:st:qcdmm
[all_samples_mass178]
template_shape_ids =tt_total_mass178:wzj:st:qcdmm
[all_samples_mass181]
template_shape_ids =tt_total_mass181:wzj:st:qcdmm
[all_samples_mass184]
template_shape_ids =tt_total_mass184:wzj:st:qcdmm
[all_samples_tt_powheg]
template_shape_ids =tt_total_powheg:wzj:st:qcdmm
[all_samples_tt_mcatnlo]
template_shape_ids =tt_total_mcatnlo:wzj:st:qcdmm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; Ranges ;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[4fb_muon_4jet]
tt_total=5000:35000
;tt_total=24000:30000
tt_total_matchingup=5000:35000
tt_total_matchingdown=5000:35000
tt_total_scaleup=5000:35000
tt_total_scaledown=5000:35000
tt_total_mass161=5000:35000
tt_total_mass163=5000:35000
tt_total_mass166=5000:35000
tt_total_mass169=5000:35000
tt_total_mass175=5000:35000
tt_total_mass178=5000:35000
tt_total_mass181=5000:35000
tt_total_mass184=5000:35000
tt_total_powheg=5000:35000
tt_total_mcatnlo=5000:35000
wzj=0:45000
wzj_matchingup=0:45000
wzj_matchingdown=0:45000
wzj_scaleup=0:45000
wzj_scaledown=0:45000
st=-500:3000
qcdmm=-5000:20000


