#include "InputManager.h"

InputManager::InputManager(eire::ConfigReader *config_reader,std::string section)
{
	this->config_reader = config_reader;
	conf_section = section;
	run_multi_rebin_study = false;
	run_multi_sys_study = false;
	individual_templates_added = false;
	fm = new FitManager;
	fplot = new FitPlots(fm);
	fprint = new FitPrinter(fm);
	fs = NULL;

	read_config();
}

InputManager::~InputManager()
{
	if(fm){
		std::cout << "deleting fm " << std::endl;
		delete fm;
		fm = NULL;
		std::cout << "finished deleting fm " << std::endl;
	}
	if(outfile){
		std::cout << "deleting outfile " << std::endl;
		outfile->Write();
		outfile->Close();
		delete outfile;
		outfile = NULL;
	}
}

void InputManager::read_config()
{

	output_directory = config_reader->get_var("output_dir",conf_section);
	output_directory = output_directory + "/";
	outfile = new TFile((output_directory+"outfile.root").c_str(), "RECREATE");
	
	std::cout << "Getting general settings " << std::endl;
	use_real_data = config_reader->get_bool_var("use_real_data",conf_section,true);

	//Set the number of pseudo experiments you want to run. We've used 5000 so far. 
	int npseudo_exp = npseudo_exp = atoi(config_reader->get_var("npseudo_exp",conf_section,true).c_str());

	fm->set_smear_template_shapes(config_reader->get_bool_var("smear_template_shapes",conf_section,true));
	fm->set_do_not_smear_patterns(config_reader->get_vec_var("do_not_smear_patterns",conf_section,false));
	fm->set_vary_constraint_mean(config_reader->get_bool_var("vary_constraint_mean",conf_section,true));
	fm->set_n_pseudo_exp(npseudo_exp);
	fm->set_outfile(outfile); 
	fm->set_output_dir(output_directory);
	fm->set_use_real_data(use_real_data);

	mc_analysis = config_reader->get_var("mc_analysis",conf_section,true); 
	histos_to_fit = config_reader->get_vec_var("histos_to_fit",conf_section,true);

	/****************** Variables for systematic studies *********************/
	std::string sys_section  = config_reader->get_var("sys_study",conf_section,false);
	if(sys_section != ""){
		read_systematic_section(sys_section);
	}

	
		
}

void InputManager::read_systematic_section(std::string sys_section)
{
	std::cout << "Setting variables for systematic studies: " << sys_section << std::endl;
	
	//For the rebin study
	multi_rebin_bins = config_reader->get_vec_var("multi_rebin",sys_section,false);
	if(multi_rebin_bins.size() > 0){
		run_multi_rebin_study = true;
		fm->set_run_multi_rebin_study(true);  
		fm->set_run_full_sys_study(true);  
	}
	
	//For other studies
	sys_study_names = config_reader->get_vec_var("sys_study_names",sys_section,false);
	if(sys_study_names.size() > 0){
                fm->set_run_full_sys_study(true);
		run_multi_sys_study = true;
		sys_study_versions = config_reader->get_vec_var("sys_study_versions",sys_section,true);
		if(config_reader->get_vec_var("sys_study_cutsets",sys_section,false).size() > 0){
			std::cout << "Error: remove 'sys_study_cutsets' defined in " << sys_section ;
			std::cout << " sys_study_cutsets must be defined in the relevant histos_to_fit section ";
			std::cout << " If you don't want the cutset to change per sys_study then don't define ";
			std::cout << " sys_study_cutsets and the cutset will be taken from 'cutset' " << std::endl;
			exit(1);
		}
		for(std::vector<std::string>::iterator h_to_fit = histos_to_fit.begin();
		    h_to_fit != histos_to_fit.end(); h_to_fit++){
			std::vector<std::string> sys_study_cutsets = config_reader->get_vec_var(sys_section+"_sys_study_cutsets",*h_to_fit,false);
			if(sys_study_cutsets.size() == 0 
			   || (sys_study_cutsets.size() == 1 && sys_study_cutsets[0] == "default")){
				sys_study_cutsets.resize(sys_study_names.size(),config_reader->get_var("cutset",*h_to_fit,true));
			}else if(sys_study_cutsets.size() == 1){
				sys_study_cutsets.resize(sys_study_names.size(),sys_study_cutsets[0]);
			}
			if(sys_study_cutsets.size() != sys_study_names.size()){
				std::cout << "Error: InputManager.In " << *h_to_fit << " sys_study_cutsets.size() != sys_study_names.size() " << std::endl;
				std::cout << " cutsets: " <<  sys_study_cutsets.size() << " names: " << sys_study_names.size() << std::endl;
				exit(1);
			}
			sys_histo_to_fit_cutsets[*h_to_fit] = sys_study_cutsets;
		}			   

		check_for_additional_scale_factors(sys_section,sys_study_names.size());


		//All the histos specified in this section will be merged and used as the 'data' which
		//the mc templates, taken from the nominal cutset, will be fit to.
		sys_histo_sections = config_reader->get_vec_var("sys_histo_sections",sys_section,false);
		//For each fit the mc templates are taken from a different sys sample while the histo
		//which makes up the 'data' is taken from the nominal case
		//The nominal case is specified in conf_section and should equal the first element of this vector.
		sys_template_sections = config_reader->get_vec_var("sys_template_sections",sys_section,false);
		
		//Safety checks. 
		//If versions or cutsets for studies not set, the settings in global will be used.
		if(sys_study_versions.size() == 0){
			sys_study_versions.resize(sys_study_names.size(),config_reader->get_var("version",conf_section,true));
		}

		if(sys_histo_sections.size() == 0 && sys_template_sections.size() == 0){
			std::cout << "ERROR: need to set sys_histo_sections or sys_template_sections so the fit knows if";
			std::cout << " it should vary the 'data' histo or the mc templates for the systematic studies." << std::endl;
			exit(1);
		}
		if(sys_histo_sections.size() > 0 && sys_template_sections.size() > 0){
			std::cout << "ERROR: Cannot set both sys_histo and sys_template to be varied during studies.";
			std::cout << "Comment out one of these variables in the config." << std::endl;
			exit(1);
		}
		//If there is one element in a vector it'll be resized so all studies use this element.
		if(sys_study_versions.size() == 1)
			sys_study_versions.resize(sys_study_names.size(),sys_study_versions[0]);
		if(sys_histo_sections.size() == 1)
			sys_histo_sections.resize(sys_study_names.size(),sys_histo_sections[0]);
		if(sys_template_sections.size() == 1)
			sys_template_sections.resize(sys_study_names.size(),sys_template_sections[0]);

		//Backward compatibility. if default is set for version or cutset
		//replace with value taken from global section
		for(std::vector<std::string>::iterator v = sys_study_versions.begin();
		    v != sys_study_versions.end(); v++){
			if(*v == "default"){
				*v = config_reader->get_var("version",conf_section,true);
			}
		}

		//Vectors must have same number of elements as sys_study_names.
		if(sys_study_versions.size() != sys_study_names.size() 
		   || (sys_template_sections.size() != sys_study_names.size() && sys_template_sections.size() != 0) 
		   || (sys_histo_sections.size() != sys_study_names.size() && sys_histo_sections.size() != 0)){
			
			std::cout << "Need to set the same number of vector elements for sys_study_names (" << sys_study_names.size();
			std::cout << ") and sys_study_versions (" << sys_study_versions.size() << ") " << std::endl;
			std::cout << ") and sys_histo_sections (" << sys_histo_sections.size() << ") unless sys_histos == 0" << std::endl;
			std::cout << ") and sys_template_sections (" << sys_template_sections.size() << ") unless sys_templates == 0" << std::endl;
			exit(1);
			
			}
		//End safety checks.
		
	}
	
	if(sys_study_names.size() > 0 && run_multi_rebin_study){
		std::cout << "ERROR: Trying to run multi rebin study at same time as ";
		std::cout << "running other sys studies.Choose one. Exiting" <<std::endl;
		exit(1);
	}

	
}


void InputManager::run_fit_templates()
{
	/**************************************************
          You need initialise the range of values you expect to get as output from the fit.
          If you do not give reasonable values the fit will give minimisation errors.
          This is very important and needs to be optimised for whichever variable you're examining. 
          NB: the strings, "tt_total" etc., for each range must match template_normalisation_ids
	**************************************************/
	int nstudies = 1;
	if(run_multi_sys_study) nstudies = sys_study_names.size();
	else if(run_multi_rebin_study) nstudies = multi_rebin_bins.size();
	for(int i = 0;i < nstudies;i++){
		individual_templates_added = false;
		setup_fit(i);
		if(i == 0){//Used for sys studies where template ids may change
			std::cout << "IM: Setting nominal fit setup " << std::endl;
			fm->set_nominal_template_names(fs->template_norm_names());
			//This will be used to generate pseudo_data
			fm->set_nominal_fit_setup(fs);
			fm->initialise_sys_study_histos();
		}
		
		if(use_real_data){
			fm->doFit(true,true,false);
			if(!fm->smear_template_shapes() && !fm->vary_constraint_mean()){
				fplot->plot_sample_event(true);
				fprint->print_real_data_results();
				fprint->print_cross_section_table(config_reader,true);
				fprint->print_correlation(use_real_data);
			}else if(fm->smear_template_shapes() || fm->vary_constraint_mean()){
				fplot->fit_nevts_gaussian(true);// NB. Do this first after a fit. ngauss used by other functions
				fplot->plot_fit_error_gaussian(false);
				fplot->plot_fit_pull_gaussian();
				 fplot->plot_sample_event(false);
				 fplot->plot_constraint_mean();
				 if(!run_multi_sys_study && !run_multi_rebin_study){
				 	fplot->plot_2D_histos();
				 	fplot->plot_2D_correlation();
				 }
				 fplot->plot_1D_correlation();
				 fprint->print_results_table();
				fprint->print_cross_section_table(config_reader,false);
				 fplot->plot_rel_precision_gaussian();
				fprint->print_correlation(false);
			
			}
		}
		else{
			std::string separate_data_hist_section = config_reader->get_var("separate_data_hist_section",conf_section,false);
			if(run_multi_sys_study && sys_template_sections.size() > 0){
				std::cout << "Running multi-sys study. Will take data from nominal " << std::endl;
				fm->doFit(false,false,true);
				
			}else if((run_multi_sys_study && sys_histo_sections.size() > 0) || separate_data_hist_section != ""){
				fm->doFit(true,false,false);
				
			}else{
				fm->doFit(false,false,false);
			}
			
			fplot->fit_nevts_gaussian(true);// NB. Do this first after a fit. ngauss used by other functions
			fplot->plot_fit_error_gaussian(true);
			fplot->plot_fit_pull_gaussian();
			//fplot->plot_constraint_mean();
			fplot->plot_sample_event(false);
			//fplot->plot_fit_checks();
			fplot->plot_constraint_mean();
			//if(!run_multi_sys_study && sys_histo_sections.size() > 0)
 			if(!run_multi_sys_study && !run_multi_rebin_study){
				fplot->plot_2D_histos();
				fplot->plot_2D_correlation();
			}
			fplot->plot_1D_correlation();
			fprint->print_results_table();
			fprint->print_cross_section_table(config_reader,false);
			//fprint->print_misc_results();
			fprint->print_correlation(use_real_data);
			fplot->plot_rel_precision_gaussian();
			fplot->plot_fit_checks();
		}	
			
		if(run_multi_rebin_study || run_multi_sys_study)
			fm->save_sys_study_results();
			
		//Don't want last fs deleted as it's needed by printer/plotter
		//May want to rethink this in future.
		if(i+1 != nstudies && i != 0){
			//fm->delete_ngauss_evts();
			fm->delete_output_histos(false);
			//FIXME: check what is needed by printer/plotter and maybe save that. or keep in FM.
			//	fm->delete_fit_setups();
		}
		
	}//End loop over multi_rebin/sys_study
	std::cout << "Finished all studies: InputManager:run_fit_templates" << std::endl;
	
	
	
	
	if(run_multi_rebin_study || run_multi_sys_study){
		fplot->plot_sys_templates_overlay();
		if(run_multi_rebin_study)
			fplot->plot_numbers_vs_bin_width();
		else{
			fplot->plot_numbers_vs_sys();
		}

		if(!run_multi_rebin_study){
			fprint->print_sys_table_numbers(config_reader);
			fprint->print_sys_cross_section_table(config_reader,false);
		}
		//fprint->print_sys_table_percent();

	}
	if(fs && nstudies != 1){
		delete fs;
		fs = NULL;
	}
	std::cout << "Exiting InputManager:run_fit_templates" << std::endl;
}

void InputManager::setup_fit(int i)
{
	if(fs && i != 1){
		delete fs;
		fs = NULL;
	}
	fs = new FitSetup();
	std::string fit_id = "";
	if(run_multi_rebin_study){
		fm->set_sys_study_name(multi_rebin_bins[i]);
		fit_id = multi_rebin_bins[i];
	}else if(run_multi_sys_study){
		fm->set_sys_study_name(sys_study_names[i]);
		fit_id = sys_study_names[i];
	}
	//fs->set_fit_id(fit_id);
	fs->set_fit_id("");
		
	std::cout << " ********************************************* " << std::endl;
	std::cout << " Getting settings for section: " << conf_section << "w fit id: " << fit_id << std::endl;

	/************* Manipulation of Input lists + Safety checks****************************/
	set_template_histo_ids(i);
	set_ranges();

	/**************************Merge/Add histograms****************************/
	MergeHistos *mhistos = new MergeHistos();
	mhistos->set_outfile(outfile);
		
	std::cout << " Scaling " << mc_analysis << " MC "  << std::endl;
	mhistos->set_analysis(mc_analysis);
	std::vector<std::string> do_not_scale = config_reader->get_vec_var("do_not_scale",conf_section,false); 
	for(std::vector<std::string>::iterator ds = do_not_scale.begin();
	    ds != do_not_scale.end();ds++){
		mhistos->do_not_scale(*ds);
	} 
	check_for_additional_scale_factors(conf_section,i+1);
	for(std::map<std::string,std::vector<double> >::iterator dataset_sfs = additional_scale_factors.begin();
	    dataset_sfs != additional_scale_factors.end(); dataset_sfs++){
		if((int) dataset_sfs->second.size() < i+1){
			std::cout << "ERROR: InputManager. not enough dataset scale factors defined for " << dataset_sfs->first << " size: " << dataset_sfs->second.size() << " need at least: " << i+1 << std::endl;
			exit(1);
		}
		std::cout << "InputManager: Adding scale factor to templates for: " << dataset_sfs->first << " " << (dataset_sfs->second)[i] << std::endl;
		if(dataset_sfs->first == "QCDe" || dataset_sfs->first == "QCD_e"){
			mhistos->set_additional_scale_factor("QCD","e_background",(dataset_sfs->second)[i]);
		}else if(dataset_sfs->first == "QCDmu" || dataset_sfs->first == "QCD_mu"){
			mhistos->set_additional_scale_factor("QCD","mu_background",(dataset_sfs->second)[i]);
		}else{
			mhistos->set_additional_scale_factor(dataset_sfs->first,"muon",(dataset_sfs->second)[i]);
			mhistos->set_additional_scale_factor(dataset_sfs->first,"electron",(dataset_sfs->second)[i]);
			mhistos->set_additional_scale_factor(dataset_sfs->first,"mu_background",(dataset_sfs->second)[i]);
			mhistos->set_additional_scale_factor(dataset_sfs->first,"e_background",(dataset_sfs->second)[i]);
		}
	}

	
	//Merge histos that make up template shapes and add to FitSetup.
	for(std::vector<std::string>::iterator id = available_mc_histos.begin();
	    id != available_mc_histos.end();id++){

		std::vector<TH1D*> scaled_histos = scale_and_add_histos(mhistos,i,false, *id);
		if(scaled_histos.size() == 1)
			fs->add_mc_histo(scaled_histos[0],*id);
		else
			fs->add_mc_histo(fs->create_simultaneous_histo(scaled_histos,*id),*id);
	}
	
	individual_templates_added = true;
	mhistos->reset_scale_by_additional_scale_factor();
	for(std::map<std::string,std::vector<double> >::iterator dataset_sfs = additional_scale_factors.begin();
	    dataset_sfs != additional_scale_factors.end(); dataset_sfs++){
		if((int) dataset_sfs->second.size() < 1){
			std::cout << "ERROR: InputManager. not enough dataset scale factors defined for " << dataset_sfs->first << " size: " << dataset_sfs->second.size() << " need at least: " << 1 << std::endl;
			exit(1);
		}
		std::cout << "InputManager: Adding scale factor to separate pseudo-data for: " << dataset_sfs->first << " " << (dataset_sfs->second)[0] << std::endl;
		if(dataset_sfs->first == "QCDe" || dataset_sfs->first == "QCD_e"){
			mhistos->set_additional_scale_factor("QCD","e_background",(dataset_sfs->second)[0]);
		}else if(dataset_sfs->first == "QCDmu" || dataset_sfs->first == "QCD_mu"){
			mhistos->set_additional_scale_factor("QCD","mu_background",(dataset_sfs->second)[0]);
		}else{
			mhistos->set_additional_scale_factor(dataset_sfs->first,"muon",(dataset_sfs->second)[0]);
			mhistos->set_additional_scale_factor(dataset_sfs->first,"electron",(dataset_sfs->second)[0]);
			mhistos->set_additional_scale_factor(dataset_sfs->first,"mu_background",(dataset_sfs->second)[0]);
			mhistos->set_additional_scale_factor(dataset_sfs->first,"e_background",(dataset_sfs->second)[0]);
		}
	}
	/************************Add Separate Data Histogram*************************************/
	std::string separate_data_hist_section = config_reader->get_var("separate_data_hist_section",conf_section,false);
	if(use_real_data){
		std::vector<TH1D*> scaled_histos = scale_and_add_histos(mhistos,i,true, "unused_str");
		if(scaled_histos.size() == 1)
			fs->set_data_histo(scaled_histos[0]);
		else
			fs->set_data_histo(fs->create_simultaneous_histo(scaled_histos,"data"));
		fs->initialise_for_data_template(); //Is this unnecessary?
	}
	else if(run_multi_sys_study && sys_histo_sections.size() > 0){
		std::cout << "Using out of date sys_histo_sections option " << std::endl;
		exit(1);
		//std::cout << " *** WARNING ***: sys_histo_section set. No t the default method. sys_templates_section is default" << std::endl;
		// std::cout << "************************ Sys Data Histogram Separate Section: " << sys_histo_sections[i] << std::endl;
		// std::vector<TH1D*> scaled_histos = scale_and_add_histos(mhistos,i,false,sys_histo_sections[i]);
		// if(scaled_histos.size() == 1)
		// 	fs->set_data_histo(scaled_histos[0]);
		// else
		// 	fs->set_data_histo(fs->create_simultaneous_histo(scaled_histos,"data"));
		// fs->initialise_for_data_template(); 
		
	}
	else if(separate_data_hist_section != ""){
		//Separate Data Histogram is made up of templates used in first fit which was performed. 

		std::cout << "*WARNING********************* Separate Data Hist ************************************WARNING* \n  "<< std::endl;
		std::cout << "InputManager.cc: Defined separate data_hist_section \n  " << separate_data_hist_section << std::endl;	
		std::vector<TH1D*> scaled_histos = scale_and_add_histos(mhistos,i,false,separate_data_hist_section);
		if(scaled_histos.size() == 1)
			fs->set_data_histo(scaled_histos[0]);
		else
			fs->set_data_histo(fs->create_simultaneous_histo(scaled_histos,"data"));
		fs->initialise_for_data_template(); 
			
	}
	/************************Finished Adding Histograms*************************************/
	fs->set_gaussian_constraints(config_reader->get_vec_var("gauss_constraints",conf_section,false)); 
		
	fs->initialise_nscaled_mc();
	fs->initialise_nfit_mc();
	
	fs->initialise_mc_template();
	
	fm->add_fit_setup(fs);

	std::cout << "del mhistos " << std::endl;
	delete mhistos;
	mhistos = NULL;
	std::cout << "done del mhistos " << std::endl;
	
}

void InputManager::set_template_histo_ids(int i)
{
	template_shape_ids.clear();
	template_norm_ids.clear();
	available_mc_histos.clear();

	//If running sys study where template shapes should be varied get shape ids from relevant sections
	if(run_multi_sys_study && sys_template_sections.size() > 0){
		template_shape_ids = config_reader->get_vec_var("template_shape_ids",sys_template_sections[i],true);
		template_norm_ids =  config_reader->get_vec_var("template_normalisation_ids",sys_template_sections[i],false);
	}else{
		template_shape_ids = config_reader->get_vec_var("template_shape_ids",conf_section,true);
		template_norm_ids = config_reader->get_vec_var("template_normalisation_ids",conf_section,false);
	}
	bool split_qcdmm = config_reader->get_bool_var("split_qcdmm",conf_section,false);
	std::cout << "Splitting QCD into QCD mu and QCD e" << std::endl;
	if(split_qcdmm){
		for(size_t t_i = 0;t_i < template_shape_ids.size();t_i++){
			if(template_shape_ids[t_i] == "qcdmm"){
				template_shape_ids[t_i] = "qcdmmmu";
				template_shape_ids.push_back("qcdmme");
			}	
		}
	}
	//If normalisation ids not set. assume shape ids == norm ids
	if(template_norm_ids.size() < 1){
		for(std::vector<std::string>::iterator id = template_shape_ids.begin();
		    id != template_shape_ids.end();id++){
			template_norm_ids.push_back(*id);
		}
	}
	
	//Need 1 norm id per shape template. Also must be in same order as shape vector
	if(template_norm_ids.size() != template_shape_ids.size()){
		std::cout << "ERROR: template_shape vector size != norm vector size " << std::endl;
		std::cout << "Shapes: " << template_shape_ids.size() << " Norm: " << template_norm_ids.size() << std::endl;
		exit(1);
	}

	//If running sys study where template shapes should be varied get shape ids from relevant sections
	if(run_multi_sys_study && sys_template_sections.size() > 0){
		available_mc_histos = config_reader->get_vec_var("mc_histos",sys_template_sections[i],false); 
	}else{
		available_mc_histos = config_reader->get_vec_var("mc_histos",conf_section,false); 	
	}

	//If any of the template histos aren't in the available_mc_histos vector add them
	for(std::vector<std::string>::iterator id = template_shape_ids.begin();
	    id != template_shape_ids.end();id++){
		if(available_mc_histos.size() > 0 && find(available_mc_histos.begin(), available_mc_histos.end(), *id) == available_mc_histos.end())
			available_mc_histos.push_back(*id);
	}
	for(std::vector<std::string>::iterator id = template_norm_ids.begin();
	    id != template_norm_ids.end();id++){
		if(available_mc_histos.size() > 0 && find(available_mc_histos.begin(), available_mc_histos.end(), *id) == available_mc_histos.end())
			available_mc_histos.push_back(*id);
	}

	fs->set_template_shape_names(template_shape_ids);
	fs->set_template_norm_names(template_norm_ids);
}

void InputManager::set_ranges()
{
	std::cout << "InputManager: Getting ranges " << std::endl;
	std::string rangeset = config_reader->get_var("histo_ranges",conf_section,true);
	std::map<std::string,int> lranges;
	std::map<std::string,int> hranges;
	for(std::vector<std::string>::iterator id = template_norm_ids.begin();
	    id != template_norm_ids.end();id++){
		std::vector<std::string> id_r = config_reader->get_vec_var(*id,rangeset,true);
		if(id_r.size() != 2){
			std::cout << "ERROR: Not exactly 2 ranges set for " << *id << " size: " <<  id_r.size() << " rangeset: " << rangeset << std::endl;
			exit(1);
		}
		int min = atoi(id_r[0].c_str());
		int max = atoi(id_r[1].c_str());
		if(min >= max){
			std::cout << "ERROR: Invalid range: " << *id << " min: " << min << " max: " << max << std::endl;
		}
		else{
			std::cout << "Setting: " << *id << std::endl;
			lranges[*id] = min;
			hranges[*id] = max;
		}
	}
	fs->set_ranges(lranges,hranges);
	std::cout << "Ranges set " << std::endl;
	
}

std::vector<TH1D*> InputManager::scale_and_add_histos(MergeHistos *mhistos, int i,bool use_real_data, std::string mc_h_section)
{
	
	//input root files names TTbar_plots.root, DY50_plots.root etc.
	std::string default_version = config_reader->get_var("version",conf_section,true); 
	std::string version = default_version;
	if(run_multi_sys_study && sys_template_sections.size() > 0 && sys_study_versions[i] != "default")
		version = sys_study_versions[i];

	if(histos_to_fit.size() > 1){
		std::cout << "Simultaneous fit: Histos defined in histos_to_fit will be combined into one histogram" << std::endl;
	}
	//Histograms to be merged to make up this id. 
	std::vector<std::string> sig_input; 
	std::vector<std::string> bkg_input; 
	if(!use_real_data){
		sig_input = config_reader->get_vec_var("sig_channel",mc_h_section,false);
		bkg_input = config_reader->get_vec_var("bkg_channel",mc_h_section,false);
	}
	bool use_default_qcd_cutset = config_reader->get_bool_var("use_default_qcd_cutset",mc_h_section,false); 
	bool use_default_nonqcd_cutset = config_reader->get_bool_var("use_default_nonqcd_cutset",mc_h_section,false); 

	std::vector<TH1D*> scaled_histos; 
	for(std::vector<std::string>::iterator histo_section = histos_to_fit.begin();
	    histo_section != histos_to_fit.end();histo_section++){
		std::cout << "Checking channel in " << mc_h_section << std::endl;
		std::string e_mu = config_reader->get_var("channel",*histo_section,true); // set = e for electron channel
		std::string d_channel = "muon";
		if(e_mu == "e")
			d_channel = "electron";
		

		std::string local_datadir = config_reader->get_var("datadir_"+e_mu,conf_section,true); 
		mhistos->set_input_file_directory(local_datadir);
		double lumi_for_channel = atof(config_reader->get_var("lumi_"+e_mu,conf_section,true).c_str());
		mhistos->set_integrated_luminosity(lumi_for_channel);

		std::string cutset; 
		std::string default_cutset = config_reader->get_var("cutset",*histo_section,true);
		if(run_multi_sys_study && ((!individual_templates_added && sys_template_sections.size() > 0)
					   || (individual_templates_added && sys_histo_sections.size() > 0))
		   ){
			cutset = sys_histo_to_fit_cutsets[*histo_section][i];
		}else{
			cutset = default_cutset;
		}
		std::string histo_name = config_reader->get_var("histo_name",*histo_section,true);
// 		if(use_real_data && histo_name == "top_mass_")
// 			histo_name = "top_mass_tf_";//FIXME	
		std::string rebin_s = config_reader->get_var("rebin",*histo_section,false);
		int rebin = -1;
		if(rebin_s != "" && !run_multi_rebin_study)
			rebin = atoi(rebin_s.c_str());
		else if(run_multi_rebin_study)
			rebin = atoi(multi_rebin_bins[i].c_str());

		/********************* Observable ids ***************************/
		std::string x_obs = config_reader->get_var("x_observable_id",*histo_section,false);
		std::string y_obs = config_reader->get_var("y_observable_id",*histo_section,false);
		if(x_obs == "")
			x_obs = histo_name;
// 		if(histo_name == "top_mass_tf_"){
// 			x_obs = "top_mass_";	//FIXME		
// 		}
		fs->set_observable_ids(x_obs,y_obs);

		//Here we smooth any input histograms which need smoothing.
		std::vector<std::string> histos_to_smooth = config_reader->get_vec_var("histos_to_smooth",mc_h_section,false);
		if(histos_to_smooth.size() > 0){
		       
			std::string qcd_cutset = "";
			if(use_default_qcd_cutset){qcd_cutset = default_cutset;}
			SmoothHistos *shistos= new SmoothHistos();
			shistos->smooth_histos(histos_to_smooth,local_datadir,output_directory,"tmp_smooth_plots.root",histo_name,version,e_mu,cutset,rebin,qcd_cutset);
			delete shistos;
			shistos = NULL;
		}

		std::map<std::string,std::vector<std::string> > file_names;
		if(!use_real_data){
			if(sig_input.size() == 0 && bkg_input.size() == 0){
				std::cout << "InputManager: scale_and_add_histos: " << std::endl;
				std::cout << "sig_input and bkg_input empty for section: " << mc_h_section << std::endl;
				exit(1);
			}
			for(std::vector<std::string>::iterator h_in = sig_input.begin();
			    h_in != sig_input.end();h_in++){
				if(*h_in != "QCD" && use_default_nonqcd_cutset)
					file_names[*h_in+version+".root"].push_back(histo_name+*h_in+"|"+d_channel+"|"+default_cutset);
				else
					file_names[*h_in+version+".root"].push_back(histo_name+*h_in+"|"+d_channel+"|"+cutset);
				mhistos->set_signal_file(*h_in+version+".root");
			}
			if(sig_input.size() > 1){ 
				std::cout << "WARNING: More than one signal file input. In MergeHistos setting signal file to last file in vector" << std::endl; 
			}
			for(std::vector<std::string>::iterator h_in = bkg_input.begin();
			    h_in != bkg_input.end();h_in++){
				std::string root_file_name = *h_in+version+".root";
				if(find(histos_to_smooth.begin(), histos_to_smooth.end(), *h_in) != histos_to_smooth.end()){
					std::cout << "Switching to smooth root file for: " << *h_in << std::endl;
					root_file_name = "tmp_smooth_plots.root";
				}

				if(*h_in == "QCD" && use_default_qcd_cutset){
					std::cout << "****ADDING 1: " << *h_in << " " << default_cutset << std::endl;
					file_names[root_file_name].push_back(histo_name+*h_in+"|"+e_mu+"_background|"+default_cutset);	
				}else if(*h_in != "QCD" && use_default_nonqcd_cutset){
					std::cout << "****ADDING 1: " << *h_in << " " << default_cutset << std::endl;
					file_names[root_file_name].push_back(histo_name+*h_in+"|"+e_mu+"_background|"+default_cutset);	
				}else{
					std::cout << "****ADDING 2: " << *h_in << " " << cutset << std::endl;
					file_names[root_file_name].push_back(histo_name+*h_in+"|"+e_mu+"_background|"+cutset);
				}
			}
		}else{
			std::cout << "Adding real data histogram" << std::endl;
			file_names["Data"+version+".root"].push_back(histo_name+"Data"+"|"+d_channel+"|"+cutset);
		}

		mhistos->set_rebin(rebin);	
		mhistos->set_cutset(cutset);
		mhistos->set_histo_name(histo_name);
		mhistos->set_file_names(file_names);
		
		std::string xmin_s = config_reader->get_var("xmin",*histo_section,false);
		std::string xmax_s = config_reader->get_var("xmax",*histo_section,false);
		if((xmin_s == "" && xmax_s != "") || (xmax_s == "" && xmin_s != "")){
			std::cout << "WARNING: Only either xmin or xmax is set. Must set both or neither. xmin: " << xmin_s << " xmax: " << xmax_s << std::endl;
			exit(1);
		}else if(xmin_s != "" && xmax_s != ""){
			double xmin = atof(xmin_s.c_str()); double xmax = atof(xmax_s.c_str()); 
			mhistos->set_xaxis(histo_name,xmin,xmax);
		}else{
			mhistos->set_xaxis(histo_name,-1,-1);
		}
		
		//If doing sim fit for e and mu channel. Need empty histo to fill sim histo.
		TH1D* scaled_histo = mhistos->combine_histo<TH1D>(use_real_data,false);
		if((mc_h_section == "qcdmmmu" && e_mu == "e") ||
		   (mc_h_section == "qcdmme" && e_mu == "mu")){
			for(int bin = 0;bin < scaled_histo->GetNbinsX();bin++){
				scaled_histo->SetBinContent(bin,0);
			}
			scaled_histo->SetEntries(0);
		}


		scaled_histos.push_back(scaled_histo);
	}
	
	return scaled_histos;
}		

void InputManager::check_for_additional_scale_factors(std::string section,int nstudies)
{
	bool apply_additional_scale_factors = config_reader->get_bool_var("apply_additional_scale_factors",section,false); 
	if(apply_additional_scale_factors){
		std::vector<std::string> variables = config_reader->get_variables_for_section(section);
		for(std::vector<std::string>::iterator var = variables.begin();
		    var != variables.end();
		    ++var){
			if(var->find("scale_factors_") != std::string::npos){
				std::string sf_variable(*var);
				sf_variable = sf_variable.substr(14,std::string::npos);
				std::vector<std::string> sf_vals = config_reader->get_vec_var(*var,section,true);
				if(sf_vals.size() < 1){
					std::cout << "ERROR: No scale factors defined for" << *var << std::endl;
					exit(1);
				}else if(sf_vals.size() == 1 && nstudies > 1){
					sf_vals.resize(nstudies,sf_vals[0]);
				}else if((int) sf_vals.size() < nstudies){
					std::cout << "ERROR: Not enough scale factors defined for" << *var << " need at least" << nstudies <<std::endl;
					exit(1);

				}else{
					for(std::vector<std::string>::iterator sf = sf_vals.begin();
					    sf != sf_vals.end();sf++){
						additional_scale_factors[sf_variable].push_back(atof(sf->c_str()));
					}
				}
			}
		}
		
	}
}
