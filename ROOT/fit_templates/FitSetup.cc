#include "FitSetup.h"

using namespace RooFit;

FitSetup::FitSetup()
{
	colours = new std::vector<int>();
        colours->push_back(kRed);
        colours->push_back(kRed+4);
        colours->push_back(kBlue);
        colours->push_back(kBlue+4);
	colours->push_back(kGreen);
        colours->push_back(kRed+4);
        styles = new std::vector<int>();
	styles->push_back(2);
        styles->push_back(9);
        styles->push_back(7);
        styles->push_back(1);
	c_wzj = new TCanvas("c_wzj","c",640,480);
	c_tt = new TCanvas("c_tt","c",640,480);
	c_qcd = new TCanvas("c_qcd","c",640,480);
	c_st = new TCanvas("c_st","c",640,480);
	iter_test = 0;
	new_mc_histos = false;
	new_data_histo = false;
	fs_ranges_set = false;
	fs_external_constraint_argset = NULL;
	x_obs = "x";
	y_obs = "";
	gen_random = new TRandom3();
}

FitSetup::~FitSetup()
{
	std::cout << "Saving smear test plot " << std::endl;
	//c_wzj->Draw();f_wzj->Draw();c_wzj->SaveAs("TEST_wzj_smear.pdf");

	//	c_st->Draw();f_st->Draw();c_st->SaveAs("/user/walsh/public_html/TEST_fit/TEST_st_smear.pdf");
	//c_tt->Draw();f_tt->Draw();c_tt->SaveAs("/user/walsh/public_html/TEST_fit/TEST_tt_smear.pdf");
        delete_all(true);
	if(gen_random){
		delete gen_random;
		gen_random = NULL;
	}
	std::cout << "Finished with FS destructor" << std::endl;
}

void FitSetup::set_fit_id(std::string id)
{
	fs_fit_id = id;
}
	
void FitSetup::set_observable_ids(std::string x,std::string y)
{
	x_obs = x;
	y_obs = y;
}
	
void FitSetup::set_template_shape_names(std::vector<std::string> names)
{
	fs_template_shape_names = names;
}

void FitSetup::set_template_norm_names(std::vector<std::string> names)
{
	fs_template_norm_names = names;
}

void FitSetup::set_gaussian_constraints(std::vector<std::string> constraints)
{
	fs_gauss_constraints = constraints;
}

void FitSetup::initialise_avail_gauss_constraints()
{
	if (verbose) std::cout << " FS: Initialising available gauss constraints " << std::endl; 

	if(find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "st") != fs_template_norm_names.end()){
		avail_gauss_constraints.push_back("st");
		fs_template_names_for_constraint["st"] = "st";
		fs_constraint_vars["st_mean"] = nscaled_evts["st"];
		fs_constraint_vars["st_sigma"] = 0.3*(nscaled_evts["st"]);
			//fs_constraint_vars["st_sigma"] = 0.07*(nscaled_evts["st"]);
	}
	if(find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "qcdmm") != fs_template_norm_names.end()){
		avail_gauss_constraints.push_back("qcdmm");
		fs_template_names_for_constraint["qcdmm"] = "qcdmm";
		fs_constraint_vars["qcdmm_mean"] = nscaled_evts["qcdmm"];
		fs_constraint_vars["qcdmm_sigma"] =0.5*(nscaled_evts["qcdmm"]);
	}
	if(find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "qcd") != fs_template_norm_names.end()){
		avail_gauss_constraints.push_back("qcd");
		fs_template_names_for_constraint["qcd"] = "qcd";
		fs_constraint_vars["qcd_mean"] = nscaled_evts["qcd"];
		fs_constraint_vars["qcd_sigma"] = 0.5*(nscaled_evts["qcd"]);
	}
	for(std::vector<std::string>::iterator tname = fs_template_norm_names.begin();
	    tname != fs_template_norm_names.end();tname++){
		if(tname->find("wzj") != std::string::npos){
			avail_gauss_constraints.push_back("wzj");
			fs_template_names_for_constraint["wzj"] = *tname;
			fs_constraint_vars["wzj_mean"] = nscaled_evts[*tname];
			fs_constraint_vars["wzj_sigma"] = 0.05*(nscaled_evts[*tname]);
		}
	}

	if(find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "zj") != fs_template_norm_names.end() &&
	   find(fs_template_norm_names.begin(), fs_template_norm_names.end(), "wj") != fs_template_norm_names.end()){
		avail_gauss_constraints.push_back("wjzj");
		fs_template_names_for_constraint["wjzj"] = "wjzj";
	        fs_rooform_constraint["wjzj"] = new RooFormulaVar("wjzj","@0/@1",RooArgList(*nfit_evts["wj"],*nfit_evts["zj"])); 
		fs_constraint_vars["wjzj_mean"] = nscaled_evts["wj"]/nscaled_evts["zj"];
		fs_constraint_vars["wjzj_sigma"] = 0.3*(nscaled_evts["wj"]/nscaled_evts["zj"]);

		//Range will be used for initialisation of output plot. Not directly in fit.
		if(h_ranges_low.find("wjzj") == h_ranges_low.end() && h_ranges_high.find("wjzj") == h_ranges_high.end()){
			h_ranges_low["wjzj"] = 0;
			h_ranges_high["wjzj"] = fs_constraint_vars["wjzj_mean"] + fs_constraint_vars["wjzj_sigma"]*4;
		}

	}
	//Check all requested constraints are available
	for(std::vector<std::string>::iterator c = fs_gauss_constraints.begin();
	    c != fs_gauss_constraints.end();c++){
		if(find(avail_gauss_constraints.begin(),avail_gauss_constraints.end(),*c) == avail_gauss_constraints.end()){
			std::cout << "ERROR: The gaussian constraint requested, " << *c << ", is not in the list of available constraints." << std::endl;
			std::cout << "Add manually in FitSetup.cc initialise_avail_gauss_constraints(). " << std::endl;
			exit(1);
		}
	}
}

void FitSetup::initialise_observable(double low_range, double high_range, std::string obs_id)
{
	if (verbose) std::cout << " FS: Initialising observable: " << obs_id << std::endl; 

	if(fs_observables.find(obs_id) != fs_observables.end() && fs_observables[obs_id] != NULL){
		std::cout << "WARNING: FS: Why are you reinitialising " << obs_id << "?" <<std::endl;
		delete fs_observables[obs_id];
	}

	h_ranges_low[obs_id] = low_range;
	h_ranges_high[obs_id] = high_range;
	fs_observables[obs_id] = new RooRealVar(obs_id.c_str(),obs_id.c_str(),h_ranges_low[obs_id],h_ranges_high[obs_id]);

	if(verbose) std::cout << "Observable " << obs_id << " initialised with min " << low_range << " max " << high_range << std::endl;

}

void FitSetup::add_mc_histo(TH1D* histo,std::string id)
{
	if(verbose) std::cout << " FS: add_mc_histo. Adding " << id << std::endl;
	
	if(fs_histos.find(id) != fs_histos.end()){
		std::cout << "ERROR: Histo with id: " << id << " already exists in fs_histo. Exiting" << std::endl;
		exit(1);
	}
	if(histo == NULL){
		std::cout << "ERROR: NULL histo: " << id << " .Exiting" << std::endl;
		exit(1);
	}		
	new_mc_histos = true; 

	fs_mc_histo_names.push_back(id);
	//fs_histos[id] = new TH1D(*histo);
	fs_histos[id] = histo;
	
	//Initialise x if has not been initialised
	if(fs_observables.find(x_obs) == fs_observables.end() || fs_observables[x_obs] == NULL){
		initialise_observable(fs_histos[id]->GetXaxis()->GetXmin(),fs_histos[id]->GetXaxis()->GetXmax(),x_obs);
		fs_observables[x_obs]->setBins(fs_histos[id]->GetNbinsX());
	}
}

TH1D* FitSetup::create_simultaneous_histo(std::vector<TH1D*> histos,std::string id)
{
	if(verbose) std::cout << " FM: Creating simultaneous histo: "<< id << std::endl;

	if(histos.size() < 1){
		std::cout << "ERROR:FitSetup;add_simultaneous_histo.Empty vector of histos " << std::endl;
		exit(1);
	}
	
	//first get this information to construct your final histogram
	int nbinsXtot = 0;
	for(std::vector<TH1D*>::iterator histo = histos.begin();
	    histo != histos.end();histo++){
		if(*histo == NULL){
			std::cout << "ERROR: NULL histo: " << id << " .Exiting" << std::endl;
			exit(1);
		}

		nbinsXtot += (*histo)->GetNbinsX();
	}
	int xmin = histos[0]->GetXaxis()->GetXmin();
	int xmax = histos[0]->GetBinWidth(1)*nbinsXtot;
	
	TH1D *sim_histo = new TH1D((id+"_histo").c_str(),(id+"_histo").c_str(),nbinsXtot,xmin,xmax);

	//Now fill the histogram
	int bin_offset = 0;
	double total_entries = 0;
	for(std::vector<TH1D*>::iterator histo = histos.begin();
	    histo != histos.end();histo++){
		total_entries += (*histo)->GetEntries();
		for(int i = 1;i <= (*histo)->GetNbinsX(); i++){
			sim_histo->SetBinContent(bin_offset+i, (*histo)->GetBinContent(i));
		    
		}
		bin_offset += (*histo)->GetNbinsX();
	}
	sim_histo->SetEntries(total_entries);

	if(verbose){
		std::cout << " Combined simultaneous pdf has " << sim_histo->GetNbinsX() << " bins" << std::endl;
		std::cout << "Min X: " << sim_histo->GetXaxis()->GetXmin() << " max_x: " << sim_histo->GetXaxis()->GetXmax() << std::endl;
		std::cout << "Combined Histo Integral: " << sim_histo->Integral() << std::endl;
		std::cout << "Combined Histo Entries: " << sim_histo->GetEntries() << std::endl;

		for(std::vector<TH1D*>::iterator histo = histos.begin();
		    histo != histos.end();histo++){
			std::cout << "Input H bins " << (*histo)->GetNbinsX() << " minX " << (*histo)->GetXaxis()->GetXmin();
			std::cout << " maxX " << (*histo)->GetXaxis()->GetXmax() << std::endl;
			std::cout << "Integral: " << (*histo)->Integral() << std::endl;

		}
	}
	return sim_histo;
}

void FitSetup::set_data_histo(TH1D *data_histo,std::string id)
{
	if(fs_histos.find("data") != fs_histos.end() && fs_histos["data"] != NULL){
		if(verbose) std::cout << "FM: deleting old data histo " << std::endl;
		delete fs_histos["data"];

		if(nfit_evts.find("data") != nfit_evts.end() && nfit_evts["data"])
			delete nfit_evts["data"];
		if(datahists.find("data") != datahists.end() && datahists["data"])
			delete datahists["data"];
		if(pdfs.find("data") != pdfs.end() &&pdfs["data"])
			delete pdfs["data"];
		if(templates.find("data") != templates.end() && templates["data"])
			delete templates["data"];
	}
	
	//fs_histos["data"] = new TH1D(*data_histo);
	fs_histos["data"] = data_histo;
	initialise_nscaled_nfit_data();
       
	new_data_histo = true;
}

//Ranges set should vary depending on expected variation in fitted nevts.
void FitSetup::set_ranges(std::map<std::string,int> lranges, std::map<std::string,int> hranges)
{
	for(std::map<std::string,int>::iterator lr = lranges.begin();lr != lranges.end();lr++)
		h_ranges_low[lr->first] = lr->second;

	for(std::map<std::string,int>::iterator hr = hranges.begin();hr != hranges.end();hr++)
		h_ranges_high[hr->first] = hr->second;
	
	fs_ranges_set = true;
}

//All the above must be set first. this should be called from within fitter
//Initialise variables which will not vary depending with different analyses applied
void FitSetup::initialise_nscaled_mc()
{
        std::cout.precision(6);

	if(verbose) std::cout << "FS: Initialising scaled and fit variables" << std::endl;

	if(nfit_evts.size() > 1)//> 1 because data could be set
		delete_nfit_evts(false);

	if(h_ranges_low.size() < fs_template_norm_names.size() || h_ranges_high.size() < fs_template_norm_names.size())
		std::cout <<"ERROR: FS: not enough histo ranges set. nranges h: " <<  h_ranges_high.size() << " nranges l:" << h_ranges_low.size() << " norm: " <<  fs_template_norm_names.size() << std::endl;

	if(verbose) std::cout << " Getting integrals: " << std::endl;
	for(std::vector<std::string>::iterator id = fs_mc_histo_names.begin();
	    id != fs_mc_histo_names.end();id++){
		nscaled_evts[*id] =  fs_histos[*id]->Integral();
		if(verbose) std::cout << *id << " " << nscaled_evts[*id] << "; ";
	}
	if(verbose) std::cout << "" << std::endl;
}

void FitSetup::initialise_nfit_mc()
{
	if(verbose) std::cout << " Initialising nfit vars for norm templates only: "<< fs_fit_id << std::endl;
	if(nscaled_evts.size() < fs_template_norm_names.size()){
		std::cout << "ERROR: Need to initialise nscaled before initialising nfit " << std::endl;
		std::cout << " n nscaled: " << nscaled_evts.size() << " n norm templates: " << fs_template_norm_names.size() << std::endl;
		exit(1);
	}

	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){
		if(verbose) std::cout << " Initialising nfit vars for "<< *id << " nscaled: " <<nscaled_evts[*id];
		if(verbose) std::cout << " rlow: " << h_ranges_low[*id]  << " rhigh: " << h_ranges_high[*id]<< std::endl;
		if(h_ranges_low[*id] >= nscaled_evts[*id] || h_ranges_high[*id] <= nscaled_evts[*id]){
			std::cout << "ERROR:Invalid range, N scaled events not within allowed range. "<< std::endl;
			exit(1);
		} 

		nfit_evts[*id] = new RooRealVar(("nevts_"+*id).c_str(),("Number of "+*id+" events").c_str(),nscaled_evts[*id],h_ranges_low[*id],h_ranges_high[*id]);
		//		double nbins; 
//		if(*id == "st"){
//			nbins = (h_ranges_high[*id] - h_ranges_low[*id])*10;
//			nfit_evts[*id]->setBins(nbins);
//		}
	}

	if(verbose) std::cout << " nfit vars initialised"<<std::endl;
   
}

void FitSetup::initialise_nfit_form_mc(std::map<std::string,RooRealVar*> fit1_vars,std::map<std::string,double> fit1_nscaled)
{
	if(fit1_vars.size() != fs_template_norm_names.size() || fit1_nscaled.size() < fit1_vars.size()){
		std::cout << "ERROR: FS: Need to have same number of templates set for each fit. Also templates must have same names "<< std::endl;
		std::cout << "fit1_vars: " << fit1_vars.size() << " fit2_vars: " << fs_template_norm_names.size() << " fit1_nscaled: " << fit1_nscaled.size() << std::endl;
		exit(1);
	}

	initialise_form_const_vars(fit1_nscaled);

	if(verbose) std::cout << " Initialising nfit form vars for norm templates only: "<< fs_fit_id << std::endl;
	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){
		
		if(verbose) std::cout << " Initialising nfit form vars for "<< *id << " const_var: " <<form_const_vars[*id]->getVal();
		nfit_form_evts[*id] = new RooFormulaVar(("nevts_form_"+*id).c_str(),"@0/@1",RooArgList(*fit1_vars[*id],*form_const_vars[*id]));
	}

	if(verbose) std::cout << " nfit form vars initialised"<<std::endl;
}

void FitSetup::initialise_form_const_vars(std::map<std::string,double> fit1_nscaled)
{
	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){

		if(fit1_nscaled.find(*id) == fit1_nscaled.end() || fit1_nscaled[*id] == 0
		   || nscaled_evts.find(*id) == nscaled_evts.end() || nscaled_evts[*id] == 0){
			std::cout << "ERROR: " << *id << " is a norm template for fit 2 but not for fit1" << std::endl;
			std::cout << " when doing simultaneous fit templates must exist in both fit setups " << std::endl;
			exit(1);
		}
		else{
			form_const_vars[*id] = new RooConstVar((*id+"_a/"+*id+"_b const").c_str(),"constant value ", fit1_nscaled[*id]/nscaled_evts[*id]);
			
		}
	}
}

//Could be real or pseudo-data.
void FitSetup::initialise_nscaled_nfit_data()
{

	if(verbose) std::cout << "FS: Initialising scaled and fit variables data" << std::endl;
	if(nfit_evts.find("data") != nfit_evts.end() && nfit_evts["data"])
		delete nfit_evts["data"];

	if(fs_histos.find("data") == fs_histos.end() || fs_histos["data"] == NULL)
		std::cout << "ERROR: FS:initialise_nscaled_nfit_data, data histo is NULL" << std::endl;

	if(h_ranges_low.find("data") == h_ranges_low.end() || h_ranges_high.find("data") == h_ranges_high.end()){ 
		h_ranges_low["data"] = 0;
		h_ranges_high["data"] = nscaled_evts["data"]*2;
		//	std::cout << "ERROR: FS:initialise_nscaled_nfit_data, data h_ranges not set" << std::endl;	
		//	exit(1);
	}

	nscaled_evts["data"] =  fs_histos["data"]->Integral();
	nfit_evts["data"] = new RooRealVar("nevts_data","Number of data events",nscaled_evts["data"],h_ranges_low["data"],h_ranges_high["data"]);

}

void FitSetup::initialise_datahists()
{
	if (verbose) std::cout << "FS: Creating RooDataHists " << std::endl; 
	delete_datahists(false);
		
	//RooDataHist constructs a binned data hist from the TH1. Must ensure data hist exist for entire lifespan of histpdf
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		if(verbose) std::cout << "Id: " << *id <<  std::endl;
		//FIXME: test QCD bins
		// if(*id == "qcdmm"){
		// 	std::cout << "HISTOGRAM: pre datahist" << std::endl;
		// 	std::cout << "Bin 5: " << fs_histos[*id]->GetBinContent(5)/fs_histos[*id]->Integral() << std::endl;
		// 	std::cout << "Bin 6: " << fs_histos[*id]->GetBinContent(6)/fs_histos[*id]->Integral() << std::endl;

		// }

		datahists[*id] = new RooDataHist((*id+"_hist").c_str(),"Binned Input Histogram",observable_argset(),fs_histos[*id]);
		std::cout << "FIXME: FS: created datahist " << std::endl;

		//FIXME: post datahist
		// if(*id == "qcdmm"){
		// 	TH1D* tmp_h =  dynamic_cast<TH1D*> (datahists[*id]->createHistogram("testing_qcd",*fs_observables[x_obs]));
		// 	std::cout << "HISTOGRAM: post pdf" << std::endl;
		// 	std::cout << "Bin 5: " << tmp_h->GetBinContent(5)/tmp_h->Integral() << std::endl;
		// 	std::cout << "Bin 6: " << tmp_h->GetBinContent(6)/tmp_h->Integral() << std::endl;
			
		// }
		// if(*id == "tt_total"){
		// 	std::cout << "HERE a" << std::endl;
		// 	c_tt->cd();
		// 	f_tt = fs_observables[x_obs]->frame(Name(("Nev_"+*id).c_str()),Title("TEST"));
		// 	datahists[*id]->plotOn(f_tt,LineColor(kBlack));
		// 	//			c_wzj->Draw();f_wzj->Draw();
		// }
		// if(*id == "wzj"){
		// 	std::cout << "HERE a" << std::endl;
		// 	c_wzj->cd();
		// 	f_wzj = fs_observables[x_obs]->frame(Name(("Nev_"+*id).c_str()),Title("TEST"));
		// 	datahists[*id]->plotOn(f_wzj,LineColor(kBlack));
		// 	//			c_wzj->Draw();f_wzj->Draw();
		// }
		// if(*id == "st"){
		// 	std::cout << "HERE a" << std::endl;
		// 	c_st->cd();
		// 	f_st = fs_observables[x_obs]->frame(Name(("Nev_"+*id).c_str()),Title("TEST"));
		// 	datahists[*id]->plotOn(f_st,LineColor(kBlack));
		// 	//			c_wzj->Draw();f_wzj->Draw();
		// }

	}

}

void FitSetup::initialise_histpdfs()
{
	if (verbose) std::cout << "FS: Convert DataHists to pdf's " << std::endl; 
	delete_pdfs(false);

	//Convert histograms to pdf's to create template
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		if(verbose) std::cout << "Id: " << *id <<  std::endl;
		pdfs[*id] = new RooHistPdf((*id+"_pdf").c_str(),(*id+"_pdf").c_str(),observable_argset(),*datahists[*id]);

		//FIXME: post pdf
		// if(*id == "qcdmm"){
		// 	RooDataHist * tmp_dh = pdfs[*id]->generateBinned(observable_argset(),nscaled_evts["data"],kTRUE,kFALSE);
		// 	TH1D* tmp_h =  dynamic_cast<TH1D*> (tmp_dh->createHistogram("testing_qcd2",*fs_observables[x_obs]));
		// 	std::cout << "HISTOGRAM: post pdf" << std::endl;
		// 	std::cout << "Bin 5: " << tmp_h->GetBinContent(5)/tmp_h->Integral() << std::endl;
		// 	std::cout << "Bin 6: " << tmp_h->GetBinContent(6)/tmp_h->Integral() << std::endl;
			
		// }
	}
}

void FitSetup::initialise_mc_template()
{

	if(fs_observables[x_obs] == NULL){
	   std::cout << "ERROR: FS: observable " << x_obs << "  not initialised" << std::cout;
	   exit(1);
	}
	if(y_obs != "" && fs_observables[y_obs] == NULL){
	   std::cout << "ERROR: FS: observable " << y_obs << "  not initialised" << std::cout;
	   exit(1);
	}

	initialise_datahists();
	initialise_histpdfs();

	if(verbose) std::cout << " FS: Initialising mc templates " << std::endl;
	delete_templates(false);

	RooArgList *template_pdfs = new RooArgList();
	RooArgList *template_nfit_evts = new RooArgList();
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		template_pdfs->add(*pdfs[*id]);
	}
	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){
		if(nfit_evts.find(*id) != nfit_evts.end())
			template_nfit_evts->add(*nfit_evts[*id]);
		else if(nfit_form_evts.find(*id) != nfit_form_evts.end())
			template_nfit_evts->add(*nfit_form_evts[*id]);
		else{
			std::cout << "ERROR: no nfit or nfit form for: " << *id << std::endl;
			exit(1);
		}
	}

	//Add the pdf's. input for mc study
	templates["MC"] = new RooAddPdf((fs_fit_id+"_template").c_str(), (fs_fit_id+" template").c_str(), *template_pdfs,*template_nfit_evts);
	
	initialise_avail_gauss_constraints();

	new_mc_histos = false;

	delete template_pdfs;
	template_pdfs = NULL;
	delete template_nfit_evts;
	template_nfit_evts = NULL;
}

void FitSetup::initialise_for_data_template()
{
	if(verbose) std::cout << " FS: Initialising data hist, pdf and template " << std::endl;
	if(fs_observables.find(x_obs) == fs_observables.end() || fs_observables[x_obs] == NULL)
		std::cout << "ERROR: FS: initialise_for_data_template observable " << x_obs << " not initialised" << std::endl;
	if(fs_histos.find("data") == fs_histos.end() || fs_histos["data"] == NULL)
		std::cout << "ERROR: FS: initialise_for_data_template no data histogram" << std::endl;

	if( (datahists.find("data") != datahists.end() && datahists["data"] ) || 
	    (pdfs.find("data") != pdfs.end() && pdfs["data"] )|| 
	    (templates.find("data") != templates.end() && templates["data"])){
		std::cout << "***********************ERROR**************************:";
		std::cout << "FS: initialise_for_data_template. Things which should be NULL are not " << std::endl;
		exit(1);
	}

	std::cout << " FS: |||||||||||| initialise for data template |||||||||||||||| " << std::endl;
	RooArgList obs;
	obs.add(*fs_observables[x_obs]);
	if(fs_observables.find(y_obs) != fs_observables.end() && fs_observables[y_obs] != NULL)
		obs.add(*fs_observables[y_obs]);

	std::cout << " FS: data hist integral: " << fs_histos["data"]->Integral() << std::endl;

	datahists["data"] = new RooDataHist("data_hist","Binned Input Histogram",obs,fs_histos["data"]);
	pdfs["data"] = new RooHistPdf("data_pdf","data_pdf",obs,*datahists["data"]);
	templates["data"] = new RooAddPdf("data_template", "data template", RooArgList(*pdfs["data"]),RooArgList(*nfit_evts["data"]));
	new_data_histo = false;
					    
}

RooAddPdf* FitSetup::smeared_mc_template(std::vector<std::string> unsmeared_patterns)
{
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		bool smear_this_id = true;
		for(std::vector<std::string>::iterator no_smear = unsmeared_patterns.begin();
		    no_smear != unsmeared_patterns.end(); no_smear++){
			if(id->find(*no_smear) != std::string::npos){
				smear_this_id = false;
				break;
			}
		}
		if(!smear_this_id){
			smear_datahists[*id] = (RooDataHist*) datahists[*id]->Clone();
		}else{
			//TEST manual smearing
			smear_histos[*id] = (TH1D*) fs_histos[*id]->Clone();
			double integral = smear_histos[*id]->Integral();
			double entries = smear_histos[*id]->GetEntries();
			double scale_up = entries/integral;
			double scale_down = integral/entries;
			bool v = false;
			for(int i = 1; i < smear_histos[*id]->GetNbinsX()+1;i++){
				if(v)	std::cout << "before smearing: " << smear_histos[*id]->GetBinContent(i) << std::endl;
				double bin_unscaled_entries = smear_histos[*id]->GetBinContent(i)*scale_up;

				if(v)std::cout << "Expected var entries: " << bin_unscaled_entries<< " +/- " << sqrt(bin_unscaled_entries) << std::endl;
				if(v)std::cout << "Expected var scaled: " << smear_histos[*id]->GetBinContent(i) << " +/- " << sqrt(bin_unscaled_entries)*scale_down << std::endl;
				double bin_nev_unscaled_rndm = gen_random->PoissonD(bin_unscaled_entries);
				//double bin_nev_unscaled_rndm = gen_random->Gaus(bin_unscaled_entries,sqrt(bin_unscaled_entries)/20);
				double bin_nev_scaled_rndm = bin_nev_unscaled_rndm*scale_down;
				
				
				smear_histos[*id]->SetBinContent(i,bin_nev_scaled_rndm);
				
					if(v)std::cout << " after smearing: " << smear_histos[*id]->GetBinContent(i) << std::endl;

			} 
			if(v)std::cout << "b4 smearing integral: " << integral << " after smear: " << smear_histos[*id]->Integral() <<  std::endl;
 			if(v)std::cout << "b4 smearing entries: " << entries << " after smear: " << smear_histos[*id]->GetEntries() <<  std::endl;
			smear_datahists[*id]=new RooDataHist((*id+"_hist").c_str(),"Binned Input Histogram",observable_argset(),smear_histos[*id]);

			//smear_datahists[*id] = pdfs[*id]->generateBinned(observable_argset(),histo(*id,true)->GetEntries(),Extended());
		}
		smear_pdfs[*id] = new RooHistPdf((*id+"smear_pdf").c_str(),(*id+"smear_pdf").c_str(),observable_argset(),*smear_datahists[*id]);
		// if(*id == "wzj" && iter_test < 5){
		//  	smear_pdfs[*id]->plotOn(f_wzj,LineColor((*colours)[iter_test]));
		//  }
		// if(*id == "st"){
		// 	smear_pdfs[*id]->plotOn(f_st,LineColor((*colours)[iter_test]));
		// }
		// if(*id == "tt_total"){
		// 	smear_pdfs[*id]->plotOn(f_tt,LineColor((*colours)[iter_test]));
		// }
//
	}
			iter_test++;
	RooArgList *template_pdfs = new RooArgList();
	RooArgList *template_nfit_evts = new RooArgList();
	for(std::vector<std::string>::iterator id = fs_template_shape_names.begin();
	    id != fs_template_shape_names.end();id++){
		template_pdfs->add(*smear_pdfs[*id]);
	}
	for(std::vector<std::string>::iterator id = fs_template_norm_names.begin();
	    id != fs_template_norm_names.end();id++){
		if(nfit_evts.find(*id) != nfit_evts.end())
			template_nfit_evts->add(*nfit_evts[*id]);
		else if(nfit_form_evts.find(*id) != nfit_form_evts.end())
			template_nfit_evts->add(*nfit_form_evts[*id]);
		else{
			std::cout << "ERROR: no nfit or nfit form for: " << *id << std::endl;
			exit(1);
		}
	}

	//Add the pdf's. input for mc study
	smear_templates["MC"] = new RooAddPdf((fs_fit_id+"_template_smeared").c_str(), (fs_fit_id+" template smeared").c_str(), *template_pdfs,*template_nfit_evts);

	return smear_templates["MC"];
}


/************************ Variable Access ****************************/


RooArgSet FitSetup::observable_argset()
{
	//	if(!fs_obs_argset){
	RooArgSet obs;
	int i = 0;
	for(std::map<std::string,RooRealVar*>::iterator o = fs_observables.begin();
	    o != fs_observables.end();o++){
		//std::cout << " Obs arg: " << i << " id: " << o->first << std::endl;
		obs.add(*(o->second));
		i++;
	}
	//	std::cout << "FS: Returning RooArgSet containing " << i << " fs_observables" << std::endl;
	return obs;
	// 	}
	// 	else
// 		return fs_obs_argset;
}

std::string FitSetup::observable_id(std::string xy)
{
	if(xy == "y")
		return y_obs;
	else
		return x_obs;
}

std::map<std::string,RooRealVar*> FitSetup::observables()
{
	return fs_observables;
}



TH1D* FitSetup::histo(std::string id, bool need)
{
	if(fs_histos.find(id) == fs_histos.end()){
		if(!need)
			return NULL;
		else {
			std::cout << " ERROR:FitSetup, No histo entry for " << id << " in " << fs_fit_id <<  std::endl;
			exit(1);
		}
	}
	else if(fs_histos[id] == NULL){
		if(!need)
			return NULL;
		else{ 
			std::cout << " ERROR:FitSetup, NULL histo entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}
	}

	return fs_histos[id];	

}

RooDataHist* FitSetup::datahist(std::string id, bool need)
{

	//for(std::map<std::string,RooDataHist*>::iterator s = datahists.begin();
	//  s != datahists.end();s++)
		//std::cout << " s: " << s->first << std::endl;

	if(datahists.find(id) == datahists.end()){
		if(!need)
			return NULL;
		else {
			std::cout << " ERROR:FitSetup, No datahist entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}
	}
	else if(datahists[id] == NULL){
		if(!need)
			return NULL;
		else{ 
			std::cout << " ERROR:FitSetup, NULL datahist entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}
	}

	return datahists[id];	

}
double FitSetup::nscaled(std::string id,bool need)
{
	if(nscaled_evts.find(id) == nscaled_evts.end()){
		if(need) std::cout << " ERROR:FitSetup, No nscaled entry for " << id << " in " << fs_fit_id << std::endl;
		if(!need)
			return -1;
		else
			exit(1);
	}

	return nscaled_evts[id];
}

double FitSetup::constraint_var(std::string v)
{
	if(fs_constraint_vars.find(v) == fs_constraint_vars.end()){
		std::cout << "FitSetup: constriant_vars(). Constraint variable: " << v << " has not been set " <<" in " << fs_fit_id <<  std::endl;
		exit(1);
	}
	else 
		return fs_constraint_vars[v];

}
std::string FitSetup::template_name_for_constraint(std::string c)
{
	//FIXME: unsafe. constraint may not exist in map. Bad soln to issue anyway. 
	return fs_template_names_for_constraint[c];

}
std::map<std::string,double> FitSetup::nscaled_map()
{
	return nscaled_evts;
}

int FitSetup::range_low(std::string id)
{
	if(h_ranges_low.find(id) == h_ranges_low.end())
		std::cout << " ERROR:FitSetup, No range_low entry for " << id << " in " << fs_fit_id << std::endl;

	return h_ranges_low[id];
}

int FitSetup::range_high(std::string id)
{
	if(h_ranges_high.find(id) == h_ranges_high.end())
		std::cout << " ERROR:FitSetup, No range_high entry for " << id << " in " << fs_fit_id << std::endl;

	return h_ranges_high[id];
}

RooRealVar* FitSetup::nfit(std::string id, bool need,bool quiet)
{
	if(nfit_evts.find(id) == nfit_evts.end()){
		if(!need){
			if(!quiet) std::cout << " WARNING:FitSetup, No nfit entry for " << id <<" in " << fs_fit_id <<  std::endl;
			return NULL;
		}else{ 
			std::cout << " ERROR:FitSetup, No nfit entry for " << id << " in " << fs_fit_id << std::endl;
			exit(1);
		}
	}
	else if(nfit_evts[id] == NULL){
		if(!need){
			if(!quiet) std::cout << " WARNING:FitSetup, NULL nfit entry for " << id << " in " << fs_fit_id << std::endl;
			return NULL;
		}else{ 
			std::cout << " ERROR:FitSetup, NULL nfit entry for " << id << " in " << fs_fit_id << std::endl;
			exit(1);
		}
	}

	return nfit_evts[id];
}

std::map<std::string, RooRealVar*> FitSetup::nfit_map()
{
	return nfit_evts;
}

RooFormulaVar* FitSetup::nfit_form(std::string id, bool need)
{
	if(nfit_form_evts.find(id) == nfit_form_evts.end()){
		if(need){
			std::cout << " ERROR:FitSetup, No nfit form entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}else{ 
			std::cout << " WARNING:FitSetup, No nfit form entry for " << id << " in " << fs_fit_id << std::endl;
			return NULL;
		}
	}

	if(nfit_form_evts[id] == NULL ){
		std::cout << " WARNING:FitSetup, NULL nfit form entry for " << id <<" in " << fs_fit_id <<  std::endl;
		if(need)
			exit(1);
	}
	
	return nfit_form_evts[id];
}

RooFormulaVar* FitSetup::rooform_constraint(std::string id, bool need,bool quiet)
{
	if(fs_rooform_constraint.find(id) == fs_rooform_constraint.end() ){
		if(need){
			std::cout << " ERROR:FitSetup, No rooform constraint entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}else{ 
			if(!quiet) std::cout << " WARNING:FitSetup, No rooform constraint entry for " << id <<" in " << fs_fit_id <<  std::endl;
			return NULL;
		}
	}
	
	if(fs_rooform_constraint[id] == NULL){
		if(need){
			std::cout << " ERROR:FitSetup, NULL nfit form entry for " << id <<" in " << fs_fit_id <<  std::endl;
			exit(1);
		}else if(!quiet)
			std::cout << " WARNING:FitSetup, NULL nfit form entry for " << id <<" in " << fs_fit_id <<  std::endl;
	}

	return fs_rooform_constraint[id];
}

RooHistPdf* FitSetup::pdf(std::string id)
{
	if(pdfs.find(id) == pdfs.end() || pdfs[id] == NULL)
		std::cout << " ERROR:FitSetup, No pdf entry for " << id <<" in " << fs_fit_id <<  std::endl;

	return pdfs[id];
}

RooAddPdf* FitSetup::get_template(std::string id)
{
	if(templates.find(id) == templates.end() || templates[id] == NULL )
		std::cout << " ERROR:FitSetup, No template entry for " << id <<" in " << fs_fit_id <<  std::endl;

	return templates[id];
}
std::vector<std::string> FitSetup::template_shape_names()
{
	return fs_template_shape_names;
}

std::vector<std::string> FitSetup::template_norm_names()
{
	return fs_template_norm_names;
}

std::vector<std::string> FitSetup::gauss_constraints_used()
{
	return fs_gauss_constraints;
}

bool FitSetup::constrained(std::string c)
{
	if(find(fs_gauss_constraints.begin(), fs_gauss_constraints.end(), c) != fs_gauss_constraints.end())
		return true;
	else
		return false;
}
std::string FitSetup::fit_id()
{
	return fs_fit_id;
}

bool FitSetup::ranges_set()
{
	return fs_ranges_set;
}
bool FitSetup::mc_histos_reset()
{
	return new_mc_histos;
}

bool FitSetup::data_histo_reset()
{
	return new_data_histo;
}


/************************** Delete ******************************/

void FitSetup::delete_all(bool delete_data)
{
	std::cout << "Fs: Deleting everything, ";
	if(delete_data) std::cout << " including variables for data" << std::endl;
	else std::cout << " except data variables" << std::endl;
	delete_nfit_evts(delete_data); std::cout << " nfit deleted " << std::endl;
	delete_histos(delete_data);std::cout << " histos deleted " << std::endl;
	delete_observables();std::cout << " observables deleted " << std::endl;
	delete_datahists(delete_data);std::cout << " datahists deleted " << std::endl;
	delete_pdfs(delete_data);std::cout << " pdfs deleted " << std::endl;
	delete_templates(delete_data);std::cout << " templates deleted " << std::endl;
	delete_gauss_constraints();std::cout << " constraints deleted " << std::endl;

}

void FitSetup::delete_observables()
{
	for(std::map<std::string,RooRealVar*>::iterator iter = fs_observables.begin();iter != fs_observables.end();++iter){
		delete fs_observables[iter->first];
		fs_observables[iter->first] = NULL;
	}
	fs_observables.clear();
}

void FitSetup::delete_nfit_evts(bool delete_data)
{
	for(std::map<std::string,RooRealVar*>::iterator iter = nfit_evts.begin();iter != nfit_evts.end();++iter){
		if(!delete_data && iter->first == "data")
			continue;
		else{
			delete nfit_evts[iter->first];
			nfit_evts[iter->first] = NULL;
		}
	}

	nfit_evts.clear();

}

void FitSetup::delete_histos(bool delete_data)
{
	
	for(std::map<std::string,TH1D*>::iterator iter = fs_histos.begin();iter != fs_histos.end();++iter){
		if(!delete_data && iter->first == "data")
			continue;
		else
			delete fs_histos[iter->first];
	}
	if(delete_data) fs_histos.clear();
}
void FitSetup::delete_datahists(bool delete_data)
{
	
	for(std::map<std::string,RooDataHist*>::iterator iter = datahists.begin();iter != datahists.end();++iter){
		if(iter->first == "data"){
			continue;
		}else{
			delete datahists[iter->first];
			datahists[iter->first] = NULL;
		}
	}
	if(delete_data) datahists.clear();
}
void FitSetup::delete_pdfs(bool delete_data)
{

	for(std::map<std::string,RooHistPdf*>::iterator iter = pdfs.begin();iter != pdfs.end();++iter){
		if(!delete_data && iter->first == "data")
			continue;
		else{
			delete pdfs[iter->first];
			pdfs[iter->first] = NULL;
		}
	}
	if(delete_data) pdfs.clear();

}

void FitSetup::delete_templates(bool delete_data)
{
	for(std::map<std::string,RooAddPdf*>::iterator iter = templates.begin();iter != templates.end();++iter){
		if(!delete_data && iter->first == "data")
			continue;
		else{
			delete templates[iter->first];
			templates[iter->first] = NULL;
		}
	}
	if(delete_data) templates.clear();
}

void FitSetup::delete_smeared_template()
{
	for(std::map<std::string,TH1D*>::iterator iter = smear_histos.begin();iter != smear_histos.end();++iter){
		delete smear_histos[iter->first];
		smear_histos[iter->first] = NULL;
	}
	smear_histos.clear();
	for(std::map<std::string,RooDataHist*>::iterator iter = smear_datahists.begin();iter != smear_datahists.end();++iter){
		delete smear_datahists[iter->first];
		smear_datahists[iter->first] = NULL;
	}
	smear_datahists.clear();
	for(std::map<std::string,RooHistPdf*>::iterator iter = smear_pdfs.begin();iter != smear_pdfs.end();++iter){
		delete smear_pdfs[iter->first];
		smear_pdfs[iter->first] = NULL;
	}
	smear_pdfs.clear();
	for(std::map<std::string,RooAddPdf*>::iterator iter = smear_templates.begin();iter != smear_templates.end();++iter){
			delete smear_templates[iter->first];
			smear_templates[iter->first] = NULL;

	}
	smear_templates.clear();
}

void FitSetup::delete_gauss_constraints()
{

	for(std::map<std::string,RooFormulaVar*>::iterator iter = fs_rooform_constraint.begin();iter != fs_rooform_constraint.end();++iter){
		delete fs_rooform_constraint[iter->first];
		fs_rooform_constraint[iter->first] = NULL;
	}
	fs_rooform_constraint.clear();
}
